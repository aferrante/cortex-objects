// DirectData.h: interface for the CDirectData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIRECTDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
#define AFX_DIRECTDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <winsock2.h>
#include <sys/timeb.h>
#include "../../Common/TXT/BufferUtil.h" 
#include "../../Common/LAN/NetUtil.h" 
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"
#include "../../Common/KEY/LicenseKey.h"


// these are data reference objects, pulled from the database.
/*
class CDirectConnectionObject  
{
public:
	CDirectConnectionObject();
	virtual ~CDirectConnectionObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	char* m_pszServerName;
	char* m_pszClientName;
	char* m_pszDesc;

//	CAConnection* m_pAPIConn;  // pointer to the associated API connection.

	// control
	bool m_bKillConnThread;
	bool m_bConnThreadStarted;
};
*/

typedef struct DiskSpaceObject_t
{
	double dblDiskFree;
	double dblDiskTotal;
	double dblDiskThreshold;
	double dblDiskPercentUtilized;
} DiskSpaceObject_t;

//create table File_Metadata (sys_filename varchar(256) NOT NULL, sys_filepath varchar(256), sys_linked_file varchar(256),
//   sys_description varchar(256), sys_operator varchar(64), sys_type int, sys_duration int, sys_valid_from int, 
//	 sys_expires_after int, sys_ingest_date int, 
//	 sys_file_flags int, sys_file_size float, sys_created_on int, sys_created_by varchar(32),  
//	 sys_last_modified_on int, sys_last_modified_by varchar(32), 
//   direct_last_used int, direct_times_used int);


class CFileMetaDataObject
{
public:
	CFileMetaDataObject();
	virtual ~CFileMetaDataObject();

	bool   m_bUnique;  // false if more than one filename returned.
	char*  m_sz_vc256_sys_filename;
	char*  m_sz_vc256_sys_filepath;
//	char*  m_sz_vc256_sys_linked_file;
	char*  m_sz_vc256_sys_description;
	char*  m_sz_vc64_sys_operator;
	char*  m_sz_n_sys_type;
	char*  m_sz_n_sys_duration;
	char*  m_sz_n_sys_valid_from;
	char*  m_sz_n_sys_expires_after;
	char*  m_sz_n_sys_ingest_date;
	char*  m_sz_n_sys_file_flags;
	char*  m_sz_dbl_sys_file_size;
	char*  m_sz_dbl_sys_file_timestamp;
	char*  m_sz_n_sys_created_on;
	char*  m_sz_vc32_sys_created_by;
	char*  m_sz_n_sys_last_modified_on;
	char*  m_sz_vc32_sys_last_modified_by;
	char*  m_sz_n_direct_last_used;
	char*  m_sz_n_direct_times_used;

	int SetField(char** ppszField, char* pszData);
	int UpdateMetadata(char* pszInfo=NULL);  // increments direct_last_used and direct_times_used
	int UpdateDestinationMetadata(int nEndpointIndex, char* pszHost, char* pszInfo=NULL);  // increments Direct_local_last_used and Direct_local_times_used in Endpoint Destinations_Media table
};

//create table Destinations_Media (host varchar(64), file_name varchar(256), transfer_date int, 
//partition varchar(16), file_size float, Direct_local_last_used int, Direct_local_times_used int);


class CDestinationMediaObject
{
public:
	CDestinationMediaObject();
	virtual ~CDestinationMediaObject();

	char*  m_sz_vc256_file_name;
	char*  m_sz_n_transfer_date; // actually timestamp
	char*  m_sz_vc16_partition;
	char*  m_sz_dbl_file_size;
	char*  m_sz_n_Direct_local_last_used;
	char*  m_sz_n_Direct_local_times_used;

	int SetField(char** ppszField, char* pszData);
};


class CDirectQueueObject  
{
public:
	CDirectQueueObject();
	virtual ~CDirectQueueObject();

	int    m_nItemID;
	char*  m_pszFilenameLocal;
	char*  m_pszFilenameRemote;
	int    m_nActionID;
	char*  m_pszHost;
	double m_dblTimestamp;   // timestamp
	char*  m_pszUsername;
	int    m_nEventItemID;
	char*  m_pszMessage;

	int SetField(char** ppszField, char* pszData);
};

class CDirectRulesObject  
{
public:
	CDirectRulesObject();
	virtual ~CDirectRulesObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned long m_ulType;
	unsigned short m_usComparisonType;
	unsigned long m_ulDestinationType;
	unsigned short m_usSearchType;
	unsigned short m_usActionType;
	int m_nRuleID;

	char* m_pszFieldName;
	char* m_pszParamName;
	char* m_pszCriterion;
	char* m_pszEventLocation;
};


class CDirectChannelDestinationObject  
{
public:
	CDirectChannelDestinationObject();
	virtual ~CDirectChannelDestinationObject();

// the following are the fields
	// server, then list, then dest info.  If a channel not active, its not in the view.
	// if a dest not active, the values are null.

//		id, module_name, server, server_time, server_status, server_basis, server_changed, server_last_update, 
//		list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update,
//		dest_module_name, type, description, destinationid, flags, host.

	CString m_sz_n_listid;
	CString m_sz_vc64_dest_module_name;
	CString m_sz_vc64_host;
	CString m_sz_n_type;
	CString m_sz_vc265_description;
	CString m_sz_n_destinationid;
	CString m_sz_n_flags; // "1" = active, not active = "", "0", or null
	unsigned long  m_ulDestFlags;
	unsigned long  m_ulDestType;
	unsigned long m_ulFlags;
};


class CDirectEventObject  
{
public:
	CDirectEventObject();
	virtual ~CDirectEventObject();

	// data base fields
	CString m_sz_n_itemid;
	CString m_sz_n_channelid;
	CString m_sz_dbl_event_start;
	CString m_sz_n_event_itemid;
	CString m_sz_vc32_event_id;
	CString m_sz_vc64_event_clip;
	CString m_sz_vc64_event_title;
	CString m_sz_vc4096_event_data;
	CString m_sz_n_event_status;
	CString m_sz_vc256_filename;
	CString m_sz_n_destinationid; 
	CString m_sz_n_status;
	CString m_sz_n_transfer_date;
	CString m_sz_n_app_data_aux;
	CString m_sz_n_type;
	CString m_sz_vc64_dest_host; 
	CString m_sz_vc1024_search_files;
	CString m_sz_n_file_index;
	CString m_sz_vc64_module_dbname; 
	CString m_sz_vc64_module_dbqueue; 
	CString m_sz_n_rule_id;


	char* m_pszMainFile; 
	int m_nMainFileStatus;

	int m_nCurrentFileSearch;

	char** m_ppszChildren; 
	int*  m_pnChildrenStatus; 
	int m_nNumChildren; // for use with:

	int AddChild(char* pszChild, int nStatus);  
//	int UtilParseTem(char* pszSourceFile, char*** pppszChildren, unsigned long* pulNumChildren);
};


class CDirectData  
{
public:
	CDirectData();
	virtual ~CDirectData();

	// util objects
	CBufferUtil m_bu;

  // hostname.  have to get it, store it here
	char*	m_pszHost;	// the name of the host
	char*	m_pszCompleteHost;	// the complete name of the host

	char*	m_pszCortexHost;	// the name of the cortex host
	unsigned short m_usCortexCommandPort;
	unsigned short m_usCortexStatusPort;

/*
	CDirectConnectionObject** m_ppConnObj;
	int m_nNumConnectionObjects;
*/

	CDirectRulesObject** m_ppRulesObj; // mapping now
	int m_nNumRulesObjects; // mapping

	CDirectChannelDestinationObject** m_ppChannelDestObj;
	int m_nNumChannelDestObjects;


	CRecordset* m_prsEvents;

	_timeb m_timebAutomationTick; // the last time check inside the thread
	_timeb m_timebAnalysisTick; // the last time check inside the thread


	_timeb m_timebTick; // the last time check inside the thread
	_timeb m_timebLastStatus; // the time of the last status given to cortex
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for cortex global status (each obj has their own as well)

	int m_nEventCheckIndex;
	int m_nEventLastMax;
	int m_nNumberOfEvents;

	int  m_nTypeAutomationInstalled; // which automation module is installed.
	int  m_nIndexAutomationEndpoint; // which automation module is installed.

	int  m_nIndexMetadataEndpoint; // archivist module index


	int m_nSettingsMod;
//	int m_nChannelsMod;
//	int m_nConnectionsMod;
	int m_nRulesMod;
	int m_nLastSettingsMod;
//	int m_nLastChannelsMod;
//	int m_nLastConnectionsMod;
	int m_nLastRulesMod;
	int m_nChannelDestMod;
	int m_nLastChannelDestMod;

	bool m_bProcessSuspended;
	bool m_bAutomationThreadStarted;
	bool m_bAnalysisThreadStarted;
	bool m_bEventsChanged;
	bool m_bChannelDestChanged;


//	CDirectEventObject* m_pTransferEvent;
	CDirectEventObject* m_pCheckEvent;

	char* GetStatusText(unsigned long* pulStatus);  // allocates mem, must free after use
	int		SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError = false);
	int		GetHost();

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;
	CDBUtil* m_pdb2;
	CDBconn* m_pdb2Conn;
	int CheckDatabaseMods(char* pszInfo=NULL);
	int IncrementDatabaseMods(char* pszTableName, char* pszInfo=NULL);
//	int GetConnections(char* pszInfo=NULL);
	int GetRules(char* pszInfo=NULL);

	int ApplyRule(int nRuleIndex, char* pszData, char** ppszEventName=NULL, char** ppszExplicitExtension=NULL);  // returns success (0) if it passes the rule, with event name.  the event will only include extension if explicit, otherwise it is stripped!
	int CheckRules(char* pszData, unsigned short usAutomationType);
	int	GetRuleIndex(int nRuleID);


	int GetChannelDests(char* pszInfo=NULL);
	bool IsChannelDestChannelActive(CString szChannelID);
	unsigned long  IsChannelDestDestinationIDActive(CString szDestinationID);
	unsigned long  IsChannelDestDestinationHostActive(CString szDestinationHost);

	int	ParseFiles(char* pszSourceData, char** pszFile, int* pnStatus, char*** pppszChildren, int** ppnChildrenStatus, int* pnNumChildren);
	int ReleaseRecordSet(bool bResetIncrementor = true);
	int UpdateSearchFiles(CDirectEventObject* pObj, int nStatus);
	int SimpleSetStatus(CDirectEventObject* pObj, int nStatus, bool bUpdateSearchFiles=false, bool bFileIndex=false, int nAppDataAux=0, char* pszInfo=NULL);
	int ScheduleEndpointQueue(CDirectEventObject* pObj, char* pszFilenameLocal, char* pszFilenameRemote, int nAction, char* pszInfo=NULL);
	int RemoveFromEndpointQueue(CDirectEventObject* pObj, int nItemID, char* pszInfo=NULL);
	CDirectQueueObject* ReturnFromEndpointQueue(CDirectEventObject* pObj, char* pszInfo=NULL);
	DiskSpaceObject_t*  ReturnDestinationDiskSpace(CDirectEventObject* pObj, char* pszInfo=NULL);
	CFileMetaDataObject*  ReturnFileMetaDataObject(char* pszFilename, char* pszInfo=NULL);
	CDestinationMediaObject*  ReturnDestinationMediaObject(CDirectEventObject* pObj, char* pszInfo=NULL);
	CDestinationMediaObject*  ReturnDestinationMediaObjectToDelete(CDirectEventObject* pObj, char* pszInfo=NULL);
	char* ReturnPartition(CDirectEventObject* pObj, char* pszFilename, char* pszInfo=NULL);
	unsigned long ReturnChannelFlags(CDirectEventObject* pObj, char* pszInfo=NULL);
	unsigned long ReturnDestinationFlags(CDirectEventObject* pObj, char* pszInfo=NULL);
	int ReturnModuleIndex(CDirectEventObject* pObj, char* pszInfo=NULL);
	int ReturnNumberOfRecords(char* pszInfo=NULL);

	int IncrementGlobalTimesUsed(CDirectEventObject* pObj, char* pszInfo=NULL);
	int IncrementLocalTimesUsed(CDirectEventObject* pObj, char* pszInfo=NULL);


	CLicenseKey m_key;

	bool m_bQuietKill;
	CRITICAL_SECTION m_critSQL;
	CRITICAL_SECTION m_critChannelDest; 


private:
	CRITICAL_SECTION m_critText;
	char* m_pszStatus;	// parseable string

//	unsigned long m_ulReqCount;
//	unsigned long m_ulSvcCount;
};

#endif // !defined(AFX_DIRECTDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
