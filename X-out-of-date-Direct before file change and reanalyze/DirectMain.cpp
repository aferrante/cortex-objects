// DirectMain.cpp: implementation of the CDirectMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "Direct.h"  // just included to have access to windowing environment
#include "DirectDlg.h"  // just included to have access to windowing environment
#include "DirectHandler.h"  // just included to have access to windowing environment

#include "DirectMain.h"
#include <process.h>
#include "../../Common/TXT/BufferUtil.h"
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"

#include "..\Sentinel\SentinelDefines.h"
#include "..\..\Common\API\Harris\ADCDefs.h"
//#include "..\..\Common\API\Miranda\IS2Comm.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;
CDirectMain* g_pdirect=NULL;
//CADC g_adc;
//CIS2Comm g_is2;  // inlined code - problems with CIS2Comm::UtilParseTem memory management . never figured this one out.

extern CMessager* g_pmsgr;  // from Messager.cpp
extern CDirectApp theApp;



//void DirectConnectionThread(void* pvArgs);
//void DirectListThread(void* pvArgs);

void DirectAutomationThread(void* pvArgs);
void DirectAnalysisThread(void* pvArgs);

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirectMain::CDirectMain()
{
}

CDirectMain::~CDirectMain()
{
}

/*

char*	CDirectMain::DirectTranslate(CHTTPHeader* pHeader, char* pszBuffer)			// apply direct scripting language
{
	return pszBuffer;
}

int		CDirectMain::InterpretDirective(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the directive, such as a char buffer.
	return DIRECT_SUCCESS;
}
*/

SOCKET*	CDirectMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // direct initiates a request to an object server
{
	CNetData* pdata = new CNetData;

	pdata->m_ucType = ucType;      // defined type - indicates which protocol to use, structure of data

	pdata->m_ucCmd = ucCmd;       // the command byte
	pdata->m_ucSubCmd = ucSubCmd;       // the subcommand byte

	pdata->m_pucData = pucData;
	pdata->m_ulDataLen = ulDataLen;

	SOCKET* ps = NULL; 
	m_net.SendData(pdata, pchHost, usPort, 5000, 0, NET_SND_CMDTOSVR, ps);
//	m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);

	return ps;
}

int		CDirectMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// direct replies to an object server after receiving data. (usually ack or nak)
{
	return DIRECT_SUCCESS;
}

int		CDirectMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// direct answers a request from an object client
{
	return DIRECT_SUCCESS;
}


void DirectMainThread(void* pvArgs)
{
	CDirectApp* pApp = (CDirectApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment
	//startup.
	g_bThreadStarted = true;

	//create the main objects.

	CDirectMain direct;
	CDBUtil db;
	CDBUtil db2;

	direct.m_data.m_ulFlags &= ~DIRECT_STATUS_THREAD_MASK;
	direct.m_data.m_ulFlags |= DIRECT_STATUS_THREAD_START;
	direct.m_data.m_ulFlags &= ~DIRECT_ICON_MASK;
	direct.m_data.m_ulFlags |= DIRECT_STATUS_UNINIT;

	direct.m_data.GetHost();

	g_pdirect = &direct;


	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
//	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\Direct\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}



//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

//		AfxMessageBox( pApp->m_lpCmdLine );

	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// check here for Cortex IP.
			if(strlen(pch))
			{
//		AfxMessageBox( pch );
				if(direct.m_data.m_bu.CountChar(pch, strlen(pch), ':')>=1)  // must be of form "hostname:cmdport" where hostname can be name or ip
				{
					direct.m_data.m_pszCortexHost = (char*)malloc(strlen(pch)+1);
					if(direct.m_data.m_pszCortexHost)
					{
						strcpy(direct.m_data.m_pszCortexHost, pch);
//		AfxMessageBox( direct.m_data.m_pszCortexHost );

						char* pchd = strchr(direct.m_data.m_pszCortexHost, ':');
						if(pchd)
						{
							*pchd = 0;  // null term the host name;
							pchd++;
//		AfxMessageBox( direct.m_data.m_pszCortexHost );

							char* pchd2 = strchr(pchd, ':');
							if(pchd2)
							{
								*pchd2 = 0;  // null term the cmd port;
								pchd2++;
								if(strlen(pchd2)) direct.m_data.m_usCortexStatusPort = atoi(pchd2);
							}

							if(strlen(pchd)) direct.m_data.m_usCortexCommandPort = atoi(pchd);
						}
					}

				}
			}
			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }

	// get settings.
	direct.m_settings.Settings(true); //read
/////////////////////////////////////////////////
// would comment this part out, but need to get dependencies here... once.
	char pszFilename[MAX_PATH];

	strcpy(pszFilename, DIRECT_SETTINGS_FILE_DEFAULT);  // direct settings file

	CFileUtil file;
	file.GetSettings(pszFilename, false); 
	char* pszParams = NULL;
// load up the values on the settings object
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
//		direct.m_settings.m_pszName = file.GetIniString("Main", "Name", "Direct");
//		direct.m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");
//
/////////////////////////////////////////////////


		// now, get the dependencies.... (get only, not set at end).
		direct.m_settings.m_nNumEndpointsInstalled = 0;
		int nNumEndpoints = file.GetIniInt("Dependencies", "Number", 0); 
		int nDep=0;
		char szKey[64];
		while(nDep<nNumEndpoints)
		{
			sprintf(szKey, "Dependency%03d", nDep);
			pszParams = file.GetIniString("Dependencies", szKey, ""); 
			if(pszParams)
			{
				if(strlen(pszParams))
				{
					CDirectEndpointObject* pdeo = new CDirectEndpointObject;
					if(pdeo)
					{
						pdeo->m_pszDBName = file.GetIniString(pszParams, "DBDefault", pszParams); // db name!  real name
						pdeo->m_pszName = file.GetIniString(pszParams, "Label", pszParams); // familiar name, just a label
						pdeo->m_pszExchange = file.GetIniString(pszParams, "DBExchangeTable", "Exchange"); // Exchange table name
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBQueueTable", "Queue");// Queue table name
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszQueue = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBLiveEventsTable", "Events");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszLiveEvents = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBDestinationTable", "Destinations");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszDestination = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBDestinationMediaTable", "Destination_Media");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszDestinationMedia = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBChannelTable", "Channels");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszChannel = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBConnectionsTable", "Connections");//Adaptors
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszChannel = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBMetadataTable", "");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszMetadata = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBFileTypesTable", "");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszFileTypes = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}

						pszParams = file.GetIniString(pdeo->m_pszDBName, "Type", ""); 
						if(pszParams)
						{
							if(stricmp(pszParams, "Archivist")==0) { pdeo->m_usType = DIRECT_DEP_DATA_ARCHIVIST; direct.m_data.m_nIndexMetadataEndpoint = direct.m_settings.m_nNumEndpointsInstalled; }
							else if(stricmp(pszParams, "Helios")==0)  { pdeo->m_usType = DIRECT_DEP_AUTO_HELIOS; direct.m_data.m_nTypeAutomationInstalled=DIRECT_DEP_AUTO_HELIOS; direct.m_data.m_nIndexAutomationEndpoint=direct.m_settings.m_nNumEndpointsInstalled;}
							else if(stricmp(pszParams, "Sentinel")==0){ pdeo->m_usType = DIRECT_DEP_AUTO_SENTINEL; direct.m_data.m_nTypeAutomationInstalled=DIRECT_DEP_AUTO_SENTINEL; direct.m_data.m_nIndexAutomationEndpoint=direct.m_settings.m_nNumEndpointsInstalled;}
							else if(stricmp(pszParams, "Prospero")==0) pdeo->m_usType = DIRECT_DEP_EDGE_PROSPERO;
							else if(stricmp(pszParams, "Luminary")==0) pdeo->m_usType = DIRECT_DEP_EDGE_LUMINARY;
							else if(stricmp(pszParams, "Radiance")==0) pdeo->m_usType = DIRECT_DEP_EDGE_RADIANCE;
							else if(stricmp(pszParams, "Libretto")==0) pdeo->m_usType = DIRECT_DEP_EDGE_LIBRETTO;
							else if(stricmp(pszParams, "Barbero")==0) pdeo->m_usType = DIRECT_DEP_EDGE_BARBERO;
							free(pszParams); pszParams=NULL;
						}


						CDirectEndpointObject** ppObj = new CDirectEndpointObject*[direct.m_settings.m_nNumEndpointsInstalled+1];
						if(ppObj)
						{
							int o=0;
							if((direct.m_settings.m_ppEndpointObject)&&(direct.m_settings.m_nNumEndpointsInstalled>0))
							{
								while(o<direct.m_settings.m_nNumEndpointsInstalled)
								{
									ppObj[o] = direct.m_settings.m_ppEndpointObject[o];
									o++;
								}
								delete [] direct.m_settings.m_ppEndpointObject;

							}
							ppObj[direct.m_settings.m_nNumEndpointsInstalled] = pdeo;
							direct.m_settings.m_ppEndpointObject = ppObj;
							direct.m_settings.m_nNumEndpointsInstalled++;
						}
						else
							delete pdeo;
					}
				}

				free(pszParams); pszParams=NULL;
			}
			nDep++;
		}

		if(direct.m_settings.m_nNumEndpointsInstalled<nNumEndpoints)
		{
			//**MSG hmmmm..cant do till later on...
		}

		if(pszParams) free(pszParams); pszParams=NULL;
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
	}

	bool bUseLog = false;
	if(direct.m_settings.m_bUseLog)
	{
		bUseLog = direct.m_settings.m_bUseLog;

		// for logfiles, we need params, and they must be in this format:
		//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
//		pszParams = file.GetIniString("Messager", "LogFileIni", "Direct|YD||1|");
//		AfxMessageBox(pszParams);
		int nRegisterCode=0;

		nRegisterCode = direct.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, "log", direct.m_settings.m_pszFileSpec, errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
			direct.m_data.SetStatusText(errorstring, (DIRECT_STATUS_FAIL_LOG|DIRECT_STATUS_ERROR));
		}

		if(pszParams) free(pszParams); pszParams=NULL;
	}

	direct.m_msgr.DM(MSG_ICONINFO, NULL, "DiReCT", "--------------------------------------------------\n\
-------------- DiReCT %s start --------------", DIRECT_CURRENT_VERSION);  //(Dispatch message)


//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
//	direct.m_http.InitializeMessaging(&direct.m_msgr);
	direct.m_net.InitializeMessaging(&direct.m_msgr);
//	g_is2.InitializeMessaging(&direct.m_msgr);

// TODO: here, get the DB and pull any override settings.
	//****
	CDBconn* pdbConn = db.CreateNewConnection(direct.m_settings.m_pszDSN, direct.m_settings.m_pszUser, direct.m_settings.m_pszPW);
	direct.m_settings.m_pdbConn = pdbConn;
	direct.m_settings.m_pdb = &db;
	direct.m_data.m_pdbConn = pdbConn;
	direct.m_data.m_pdb = &db;
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			direct.m_data.SetStatusText(errorstring, (DIRECT_STATUS_FAIL_DB|DIRECT_STATUS_ERROR));
			direct.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "DiReCT:database_connect", errorstring);  //(Dispatch message)
		}
		else
		{
			if(direct.m_settings.GetFromDatabase(errorstring)<DIRECT_SUCCESS)
			{
				direct.m_data.SetStatusText(errorstring, (DIRECT_STATUS_FAIL_DB|DIRECT_STATUS_ERROR));
				direct.m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:database_get", errorstring);  //(Dispatch message)
			}
			else
			{
				if((!direct.m_settings.m_bUseLog)&&(bUseLog))
				{
					// reset it
					direct.m_msgr.DM(MSG_ICONINFO, NULL, "DiReCT", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

					Sleep(250); // let the message get there.
					direct.m_msgr.RemoveDestination("log");

				}
			}
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", direct.m_settings.m_pszDSN, direct.m_settings.m_pszUser, direct.m_settings.m_pszPW); 
		direct.m_data.SetStatusText(errorstring, (DIRECT_STATUS_FAIL_DB|DIRECT_STATUS_ERROR));
		direct.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "DiReCT:database_init", errorstring);  //(Dispatch message)

		//**MSG
	}

	// create a secondary connection
	CDBconn* pdb2Conn = db2.CreateNewConnection(direct.m_settings.m_pszDSN, direct.m_settings.m_pszUser, direct.m_settings.m_pszPW);
	direct.m_data.m_pdb2Conn = pdb2Conn;
	direct.m_data.m_pdb2 = &db2;
	if(pdb2Conn)
	{
		if(db2.ConnectDatabase(pdb2Conn, errorstring)<DB_SUCCESS)
		{
			direct.m_data.SetStatusText(errorstring, (DIRECT_STATUS_FAIL_DB|DIRECT_STATUS_ERROR));
			direct.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "DiReCT:database2_connect", errorstring);  //(Dispatch message)
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create secondary connection for %s:%s:%s", direct.m_settings.m_pszDSN, direct.m_settings.m_pszUser, direct.m_settings.m_pszPW); 

		direct.m_data.SetStatusText(errorstring, (DIRECT_STATUS_FAIL_DB|DIRECT_STATUS_ERROR));
		direct.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "DiReCT:database2_init", errorstring);  //(Dispatch message)

		//**MSG
	}


//init command and status listeners.

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing command server on %d", direct.m_settings.m_usCommandPort); 
	direct.m_data.SetStatusText(errorstring, DIRECT_STATUS_CMDSVR_START);
	direct.m_msgr.DM(MSG_ICONNONE, NULL, "DiReCT:command_server_init", errorstring);  //(Dispatch message)

	if(direct.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = direct.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "DirectCommandServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = DirectCommandHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &direct;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &direct.m_net;					// pointer to the object with the Message function.


		if(direct.m_net.StartServer(pServer, &direct.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			direct.m_data.SetStatusText(errorstring, DIRECT_STATUS_CMDSVR_ERROR);
			direct.m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:command_server_init", errorstring);  //(Dispatch message)
			direct.SendMsg(CX_SENDMSG_ERROR, "DiReCT:command_server_init", errorstring);
		}
		else
		{
//AfxMessageBox("Q1");
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Command server listening on %d", direct.m_settings.m_usCommandPort);
			direct.m_data.SetStatusText(errorstring, DIRECT_STATUS_CMDSVR_RUN);
			direct.m_msgr.DM(MSG_ICONNONE, NULL, "DiReCT:command_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing status server on %d", direct.m_settings.m_usStatusPort); 
	direct.m_data.SetStatusText(errorstring, DIRECT_STATUS_STATUSSVR_START);
	direct.m_msgr.DM(MSG_ICONNONE, NULL, "DiReCT:status_server_init", errorstring);  //(Dispatch message)

	if(direct.m_settings.m_usStatusPort>0)
	{
//AfxMessageBox("Q1");
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = direct.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "DirectStatusServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = DirectStatusHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &direct;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &direct.m_net;					// pointer to the object with the Message function.

		if(direct.m_net.StartServer(pServer, &direct.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			direct.m_data.SetStatusText(errorstring, DIRECT_STATUS_STATUSSVR_ERROR);
			direct.m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:status_server_init", errorstring);  //(Dispatch message)
			direct.SendMsg(CX_SENDMSG_ERROR, "DiReCT:status_server_init", errorstring);
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Status server listening on %d", direct.m_settings.m_usStatusPort);
			direct.m_data.SetStatusText(errorstring, DIRECT_STATUS_STATUSSVR_RUN);
			direct.m_msgr.DM(MSG_ICONNONE, NULL, "DiReCT:status_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

//AfxMessageBox("QQ");

	
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Direct is initializing...");
	direct.m_msgr.DM(MSG_ICONNONE, NULL, "DiReCT:init", errorstring);  //(Dispatch message)
	direct.m_data.SetStatusText(errorstring, DIRECT_STATUS_THREAD_SPARK);

	// now connect all the registered servers
	// this is the list of servers in the db that get connected


	if(
		  (!(direct.m_settings.m_ulMainMode&DIRECT_MODE_CLONE))
		&&(direct.m_data.m_key.m_bValid)  // must have a valid license
		&&(
				(!direct.m_data.m_key.m_bExpires)
			||((direct.m_data.m_key.m_bExpires)&&(!direct.m_data.m_key.m_bExpired))
			||((direct.m_data.m_key.m_bExpires)&&(direct.m_data.m_key.m_bExpireForgiveness)&&(direct.m_data.m_key.m_ulExpiryDate+direct.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
			)
		&&(
				(!direct.m_data.m_key.m_bMachineSpecific)
			||((direct.m_data.m_key.m_bMachineSpecific)&&(direct.m_data.m_key.m_bValidMAC))
			)
		)
	{
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "here");  Sleep(250);//(Dispatch message)
		direct.m_data.GetRules();
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "X2");  Sleep(250);//(Dispatch message)
//		direct.m_data.GetChannels();
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "X3");  Sleep(250);//(Dispatch message)
	}
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "X4");  Sleep(250);//(Dispatch message)
	
	if((direct.m_data.m_ulFlags&DIRECT_ICON_MASK) != DIRECT_STATUS_ERROR)
	{
		direct.m_data.m_ulFlags &= ~DIRECT_ICON_MASK;
		direct.m_data.m_ulFlags |= DIRECT_STATUS_OK;  // green - we want run to be blue when something in progress
	}

//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "here2");  Sleep(250);//(Dispatch message)

// initialize direct (make this not persistent for now)
	CNetData* pdata = new CNetData;
	if(pdata)
	{

		pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA;      // defined type - indicates which protocol to use, structure of data

		pdata->m_ucCmd = CX_REQ_HELLO;       // the command byte
		pdata->m_ucSubCmd = 0;       // the subcommand byte

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
		if(pch)
		{
			sprintf(pch, "%s:%d:%d:%d:%s", 
				direct.m_data.m_pszHost,
				direct.m_settings.m_usCommandPort,
				direct.m_settings.m_usStatusPort,
				CX_TYPE_PROCESS,
				direct.m_settings.m_pszName?direct.m_settings.m_pszName:"Direct"
				);

			pdata->m_pucData =  (unsigned char*) pch;
			pdata->m_ulDataLen = strlen(pch);
		}
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "here3");  Sleep(250);//(Dispatch message)

		SOCKET s = NULL; 
//AfxMessageBox("sending");
//AfxMessageBox( direct.m_data.m_pszCortexHost );
//AfxMessageBox( (char*)pdata->m_pucData );
		if(direct.m_net.SendData(pdata, direct.m_data.m_pszCortexHost, direct.m_data.m_usCortexCommandPort, 5000, 0, NET_SND_CMDTOSVR, &s)>=NET_SUCCESS)
		{
			//send ack
//			AfxMessageBox("ack");
			direct.m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);
		}
		else
		{
//			AfxMessageBox("could not send");
		}
		direct.m_net.CloseConnection(s);

		delete pdata;

	}

//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "here4");  Sleep(250);//(Dispatch message)


	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "DiReCT main thread running.");  
	direct.m_data.SetStatusText(errorstring, DIRECT_STATUS_THREAD_RUN);
	direct.m_msgr.DM(MSG_ICONNONE, NULL, "DiReCT:init", "DiReCT is initialized, main thread is running.");  Sleep(50); //(Dispatch message)
	direct.SendMsg(CX_SENDMSG_INFO, "DiReCT:init", "Direct %s main thread running.", DIRECT_CURRENT_VERSION);
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", errorstring);  Sleep(250);//(Dispatch message)


// get mods and channel info stuff

	if((direct.m_settings.m_ppEndpointObject)&&(direct.m_settings.m_nNumEndpointsInstalled>0))
	{
		int ix=0; 
		direct.m_data.m_nChannelDestMod=0;
		while(ix<direct.m_settings.m_nNumEndpointsInstalled)
		{
			if(direct.m_settings.m_ppEndpointObject[ix])
			{
				if(direct.m_settings.m_ppEndpointObject[ix]->m_usType&DIRECT_DEP_AUTO_MASK)
				{
					direct.m_settings.m_ppEndpointObject[ix]->m_nModConnections = direct.m_settings.m_ppEndpointObject[ix]->CheckDatabaseMod(&db, pdbConn, direct.m_settings.m_ppEndpointObject[ix]->m_pszConnections);
					direct.m_data.m_nChannelDestMod += direct.m_settings.m_ppEndpointObject[ix]->m_nModConnections;
					direct.m_settings.m_ppEndpointObject[ix]->m_nModChannel = direct.m_settings.m_ppEndpointObject[ix]->CheckDatabaseMod(&db, pdbConn, direct.m_settings.m_ppEndpointObject[ix]->m_pszChannel);
					direct.m_data.m_nChannelDestMod += direct.m_settings.m_ppEndpointObject[ix]->m_nModChannel;
				}
				if(direct.m_settings.m_ppEndpointObject[ix]->m_usType&DIRECT_DEP_EDGE_MASK)
				{
					direct.m_settings.m_ppEndpointObject[ix]->m_nLastModDestination = direct.m_settings.m_ppEndpointObject[ix]->CheckDatabaseMod(&db, pdbConn, direct.m_settings.m_ppEndpointObject[ix]->m_pszDestination);
					direct.m_data.m_nChannelDestMod += direct.m_settings.m_ppEndpointObject[ix]->m_nLastModDestination;
				}
			}
			ix++;
		}
	}

	if(direct.m_data.GetChannelDests()>=DIRECT_SUCCESS)
	{
		direct.m_data.m_nLastChannelDestMod = direct.m_data.m_nChannelDestMod;

	}

	//have to start the automation and analysis threads
	direct.m_data.m_bAutomationThreadStarted=false;
	direct.m_data.m_bAnalysisThreadStarted=false;

	if(_beginthread(DirectAutomationThread, 0, (void*)&direct)==-1)
	{
		//error.  try again once
		Sleep(1000);
		if(_beginthread(DirectAutomationThread, 0, (void*)&direct)==-1)
		{
			//report failure
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "DiReCT automation thread failure.");  
			direct.m_data.SetStatusText(errorstring, (DIRECT_STATUS_ERROR|DIRECT_STATUS_THREAD_ERROR));
			direct.m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:automation_thread_init", errorstring);  //(Dispatch message)
			direct.SendMsg(CX_SENDMSG_ERROR, "DiReCT:automation_thread_init", errorstring);
		}
		else
		{
			direct.m_msgr.DM(MSG_ICONNONE, NULL, "DiReCT:automation_thread_init", "DiReCT automation thread initialized.");  //(Dispatch message)
		}
	}
	else
	{
		direct.m_msgr.DM(MSG_ICONNONE, NULL, "DiReCT:automation_thread_init", "DiReCT automation thread initialized.");  //(Dispatch message)
	}
	Sleep(250);

	if(_beginthread(DirectAnalysisThread, 0, (void*)&direct)==-1)
	{
		//error.  try again once
		Sleep(1000);
		if(_beginthread(DirectAnalysisThread, 0, (void*)&direct)==-1)
		{
			//report failure
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "DiReCT analysis thread failure.");  
			direct.m_data.SetStatusText(errorstring, (DIRECT_STATUS_ERROR|DIRECT_STATUS_THREAD_ERROR));
			direct.m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:analysis_thread_init", errorstring);  //(Dispatch message)
			direct.SendMsg(CX_SENDMSG_ERROR, "DiReCT:analysis_thread_init", errorstring);
		}
		else
		{
			direct.m_msgr.DM(MSG_ICONNONE, NULL, "DiReCT:analysis_thread_init", "DiReCT analysis thread initialized.");  //(Dispatch message)
		}
	}
	else
	{
		direct.m_msgr.DM(MSG_ICONNONE, NULL, "DiReCT:analysis_thread_init", "DiReCT analysis thread initialized.");  //(Dispatch message)
	}
	Sleep(250);


	_timeb timebCheckMods;
	_ftime( &timebCheckMods );
///AfxMessageBox("xxxxx");

	char szSQL[DB_SQLSTRING_MAXLEN];
	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &direct.m_data.m_timebTick );
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%d.%03d", direct.m_data.m_timebTick.time, direct.m_data.m_timebTick.millitm);   Sleep(250);//(Dispatch message)

// monitor changes in exchange db and react.
		if(
					(direct.m_data.m_timebTick.time > timebCheckMods.time )
				||((direct.m_data.m_timebTick.time == timebCheckMods.time)&&(direct.m_data.m_timebTick.millitm >= timebCheckMods.millitm))
				&&(!g_bKillThread)
			)
		{
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "time to check");  Sleep(250);//(Dispatch message)
			timebCheckMods.time = direct.m_data.m_timebTick.time + direct.m_settings.m_ulModsIntervalMS/1000; 
			timebCheckMods.millitm = direct.m_data.m_timebTick.millitm + (unsigned short)(direct.m_settings.m_ulModsIntervalMS%1000); // fractional second updates
			if(timebCheckMods.millitm>999)
			{
				timebCheckMods.time++;
				timebCheckMods.millitm%=1000;
			}
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "checking db connected");   Sleep(250);//(Dispatch message)
			if((pdbConn)&&(pdbConn->m_bConnected))
			{
//AfxMessageBox("x");
//	direct.m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "checkmods");  //(Dispatch message)
//				direct.m_data.ReleaseRecordSet();
				direct.m_data.CheckDatabaseMods();
				if(direct.m_data.m_nSettingsMod != direct.m_data.m_nLastSettingsMod)
				{
					if(direct.m_settings.GetFromDatabase()>=DIRECT_SUCCESS)
					{
						direct.m_data.m_nLastSettingsMod = direct.m_data.m_nSettingsMod;
					}
				}
				if(
						(direct.m_data.m_key.m_bValid)  // must have a valid license
					&&(
							(!direct.m_data.m_key.m_bExpires)
						||((direct.m_data.m_key.m_bExpires)&&(!direct.m_data.m_key.m_bExpired))
						||((direct.m_data.m_key.m_bExpires)&&(direct.m_data.m_key.m_bExpireForgiveness)&&(direct.m_data.m_key.m_ulExpiryDate+direct.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!direct.m_data.m_key.m_bMachineSpecific)
						||((direct.m_data.m_key.m_bMachineSpecific)&&(direct.m_data.m_key.m_bValidMAC))
						)
					)
				{
					if(direct.m_data.m_nRulesMod != direct.m_data.m_nLastRulesMod)
					{
						if(direct.m_data.GetRules()>=DIRECT_SUCCESS)
						{
							direct.m_data.m_nLastRulesMod = direct.m_data.m_nRulesMod;
						}
					}


					if((direct.m_settings.m_ppEndpointObject)&&(direct.m_settings.m_nNumEndpointsInstalled>0))
					{
						int ix=0; 
						direct.m_data.m_nChannelDestMod=0;
						while(ix<direct.m_settings.m_nNumEndpointsInstalled)
						{
							if(direct.m_settings.m_ppEndpointObject[ix])
							{
								if(direct.m_settings.m_ppEndpointObject[ix]->m_usType&DIRECT_DEP_AUTO_MASK)
								{
									direct.m_settings.m_ppEndpointObject[ix]->m_nModConnections = direct.m_settings.m_ppEndpointObject[ix]->CheckDatabaseMod(&db, pdbConn, direct.m_settings.m_ppEndpointObject[ix]->m_pszConnections);
									direct.m_data.m_nChannelDestMod += direct.m_settings.m_ppEndpointObject[ix]->m_nModConnections;
									direct.m_settings.m_ppEndpointObject[ix]->m_nModChannel = direct.m_settings.m_ppEndpointObject[ix]->CheckDatabaseMod(&db, pdbConn, direct.m_settings.m_ppEndpointObject[ix]->m_pszChannel);
									direct.m_data.m_nChannelDestMod += direct.m_settings.m_ppEndpointObject[ix]->m_nModChannel;
								}
								if(direct.m_settings.m_ppEndpointObject[ix]->m_usType&DIRECT_DEP_EDGE_MASK)
								{
									direct.m_settings.m_ppEndpointObject[ix]->m_nLastModDestination = direct.m_settings.m_ppEndpointObject[ix]->CheckDatabaseMod(&db, pdbConn, direct.m_settings.m_ppEndpointObject[ix]->m_pszDestination);
									direct.m_data.m_nChannelDestMod += direct.m_settings.m_ppEndpointObject[ix]->m_nLastModDestination;
								}
							}
							ix++;
						}
					}

					if(direct.m_data.m_nLastChannelDestMod != direct.m_data.m_nChannelDestMod)
					{
						if(direct.m_data.GetChannelDests()>=DIRECT_SUCCESS)
						{
							direct.m_data.m_nLastChannelDestMod = direct.m_data.m_nChannelDestMod;
						}

						// have to check the Exchange table for DiReCT_Update
						// but do one at a time
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s WHERE %s.dbo.%s.channelid NOT IN (SELECT listid from %s.dbo.%s)",
								direct.m_settings.m_pszDefaultDB?direct.m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
								direct.m_settings.m_pszLiveEvents?direct.m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
								direct.m_settings.m_pszDefaultDB?direct.m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
								direct.m_settings.m_pszLiveEvents?direct.m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
								direct.m_settings.m_pszDefaultDB?direct.m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
								direct.m_settings.m_pszChannelDests?direct.m_settings.m_pszChannelDests:"ChannelInfo" // view
							);

						EnterCriticalSection(&direct.m_data.m_critSQL);
						if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
							//**MSG
direct.m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "%s", errorstring ); 
						}
						LeaveCriticalSection(&direct.m_data.m_critSQL);
					}


					// check metadata modules for updated files.
					if(
							(direct.m_data.m_nIndexMetadataEndpoint>=0)
						&&(direct.m_settings.m_nNumEndpointsInstalled>0)
						&&(direct.m_data.m_nIndexMetadataEndpoint<direct.m_settings.m_nNumEndpointsInstalled)
						&&(direct.m_settings.m_ppEndpointObject)
						&&(direct.m_settings.m_ppEndpointObject[direct.m_data.m_nIndexMetadataEndpoint])
						&&(!direct.m_data.m_bProcessSuspended)
						)
					{
						CDirectEndpointObject* pMDObj = direct.m_settings.m_ppEndpointObject[direct.m_data.m_nIndexMetadataEndpoint];
						char szSQL[DB_SQLSTRING_MAXLEN];

						// have to check the Exchange table for DiReCT_Update
						// but do one at a time
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s.dbo.%s WHERE criterion = 'DiReCT_Update'",
								pMDObj->m_pszDBName?pMDObj->m_pszDBName:"Archivist",			// the Default DB name
								pMDObj->m_pszExchange?pMDObj->m_pszExchange:"Exchange"			// the Exchange table name
							);

						EnterCriticalSection(&direct.m_data.m_critSQL);
						CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
						if(prs)
						{
							if(!prs->IsEOF()) // not while, just do one.
							{
								CString szCriterion;
								CString szMod;
								CString szFlag;
								try
								{
									prs->GetFieldValue("criterion", szCriterion);//HARDCODE
									prs->GetFieldValue("flag", szFlag);//HARDCODE
									prs->GetFieldValue("mod", szMod);//HARDCODE

									prs->Close();
									// have to delete the thing, now that we pulled it.
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s WHERE criterion = 'DiReCT_Update' AND flag = '%s' AND mod = %s",
											pMDObj->m_pszDBName?pMDObj->m_pszDBName:"Archivist",			// the Default DB name
											pMDObj->m_pszExchange?pMDObj->m_pszExchange:"Exchange",			// the Exchange table name
											szFlag,
											szMod
										);

								
									if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
									{
//										direct.m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "could not remove record: %s", errorstring);  //(Dispatch message)
//										direct.SendMsg(CX_SENDMSG_ERROR, "DiReCT:FilePropagation", "could not remove record: %s", errorstring); 
									}

									if((szFlag.GetLength())&&(direct.m_settings.m_bPropagateChangedFiles))
									{
										// need to transfer, so find files anywhere.

										// get just the filename.
										CString szFile="";
										int nSlash = szFlag.ReverseFind('\\');
										if(nSlash<0) nSlash = szFlag.ReverseFind('/');
										if(nSlash>=0) 
										{
											szFile = szFlag.Mid(nSlash+1);
										}

										if(szFile.GetLength())
										{
											char* pchFilePath = db.EncodeQuotes(szFlag);
											char* pchFile = db.EncodeQuotes(szFile);
											int nObject = 0;
											while(nObject<direct.m_settings.m_nNumEndpointsInstalled)
											{
												if((direct.m_settings.m_ppEndpointObject[nObject])&&(direct.m_settings.m_ppEndpointObject[nObject]->m_usType&DIRECT_DEP_EDGE_MASK))
												{
													CDirectEndpointObject* pEdgeObj = direct.m_settings.m_ppEndpointObject[nObject];
													// this is an edge object, find anywhere there are files.

//create table Destinations_Media (host varchar(64), file_name varchar(256), transfer_date int, partition varchar(16), file_size float, Direct_local_last_used int, Direct_local_times_used int);

													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT DISTINCT host, partition FROM %s.dbo.%s WHERE file_name = '%s'",
															pEdgeObj->m_pszDBName?pEdgeObj->m_pszDBName:"Prospero",			// the Default DB name
															pEdgeObj->m_pszDestinationMedia?pEdgeObj->m_pszDestinationMedia:"Destinations_Media",			// the Destinations_Media table name
															pchFile?pchFile:szFile
														);

													prs = db.Retrieve(pdbConn, szSQL, errorstring);
													if(prs)
													{
														while(!prs->IsEOF())
														{
															CString szHost;
															CString szPartition;
															try
															{
																prs->GetFieldValue("host", szHost);//HARDCODE
																prs->GetFieldValue("partition", szPartition);//HARDCODE

																// now, insert transfer requests to each.
char pchEncodedFileRemote[MAX_PATH];
_snprintf(pchEncodedFileRemote, MAX_PATH-1, "%s/%s", szPartition, pchFile?pchFile:szFile);
_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(filename_local, filename_remote, action, host, timestamp, username) \
VALUES ('%s','%s', 1, '%s', %d.%d, 'sys')", //HARDCODE 1= transfer to miranda
													pEdgeObj->m_pszDBName?pEdgeObj->m_pszDBName:"Prospero",			// the Default DB name
													pEdgeObj->m_pszQueue?pEdgeObj->m_pszQueue:"Queue",			// the Queue table name
													pchFilePath?pchFilePath:szFlag,
													pchEncodedFileRemote,
													szHost,
													direct.m_data.m_timebTick.time,
													direct.m_data.m_timebTick.millitm
													);

//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "ScheduleEndpointQueue SQL: %s", szSQL);  Sleep(50); //(Dispatch message)

if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
{
 	direct.m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:FilePropagation", "Error updating destination %s with file %s: %s", szHost, szFile, errorstring);  //(Dispatch message)
	direct.SendMsg(CX_SENDMSG_ERROR, "DiReCT:FilePropagation", "Error updating destination %s with file %s: %s", szHost, szFile, errorstring); 
}


															}
															catch( ... )
															{
															}
															prs->MoveNext();
														}
														prs->Close();
													}

												}
												nObject++;
											}
											if(pchFile) free(pchFile);
											if(pchFilePath) free(pchFilePath);
											

										}

									}
								}
								catch( ... )
								{
								}
//								prs->MoveNext();
							}
							prs->Close();
							delete prs;
							prs = NULL;
						}
						LeaveCriticalSection(&direct.m_data.m_critSQL);

					}
				}
/*
				if(direct.m_data.m_nConnectionsMod != direct.m_data.m_nLastConnectionsMod)
				{
//			direct.m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "getting connections again");   Sleep(250);//(Dispatch message)
					if(direct.m_data.GetConnections()>=DIRECT_SUCCESS)
					{
						direct.m_data.m_nLastConnectionsMod = direct.m_data.m_nConnectionsMod;
					}
				}
				if(direct.m_data.m_nChannelsMod != direct.m_data.m_nLastChannelsMod)
				{
//			direct.m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "getting channels again");   Sleep(250);//(Dispatch message)
					if(direct.m_data.GetChannels()>=DIRECT_SUCCESS)
					{
						direct.m_data.m_nLastChannelsMod = direct.m_data.m_nChannelsMod;
					}
				}
*/
			}
		}  // check mod interval

		MSG msg;

		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();


//AfxMessageBox("zoinks");
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();

		Sleep(1); 
//		Sleep(1000);  // stalls window procedure
	}

	direct.m_data.m_ulFlags &= ~DIRECT_STATUS_THREAD_MASK;
	direct.m_data.m_ulFlags |= DIRECT_STATUS_THREAD_END;

	direct.m_msgr.DM(MSG_ICONINFO, NULL, "DiReCT:uninit", "DiReCT is shutting down.");  //(Dispatch message)
	direct.SendMsg(CX_SENDMSG_INFO, "DiReCT:uninit", "DiReCT %s is shutting down.", DIRECT_CURRENT_VERSION);

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "DiReCT is shutting down.");  
	direct.m_data.m_ulFlags &= ~CX_ICON_MASK;
	direct.m_data.m_ulFlags |= CX_STATUS_UNKNOWN;
	direct.m_data.SetStatusText(errorstring, direct.m_data.m_ulFlags);


	while((direct.m_data.m_bAutomationThreadStarted)||(direct.m_data.m_bAnalysisThreadStarted))
	{
		_ftime( &direct.m_data.m_timebTick );  // we're still alive.
		Sleep(1);
	}



// shut down all the running objects;
/*
	if(direct.m_data.m_ppConnObj)
	{
		int i=0;
		while(i<direct.m_data.m_nNumConnectionObjects)
		{
			if(direct.m_data.m_ppConnObj[i])
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", direct.m_data.m_ppConnObj[i]->m_pszDesc?direct.m_data.m_ppConnObj[i]->m_pszDesc:direct.m_data.m_ppConnObj[i]->m_pszServerName);  

// **** disconnect servers
				direct.m_data.SetStatusText(errorstring, direct.m_data.m_ulFlags);
				direct.m_msgr.DM(MSG_ICONINFO, NULL, "DiReCT:connection_uninit", errorstring);    //(Dispatch message)

				delete direct.m_data.m_ppConnObj[i];
				direct.m_data.m_ppConnObj[i] = NULL;
			}
			i++;
		}
		delete [] direct.m_data.m_ppConnObj;
	}
	if(direct.m_data.m_ppChannelObj)
	{
		int i=0;
		while(i<direct.m_data.m_nNumChannelObjects)
		{
			if(direct.m_data.m_ppChannelObj[i])
			{
				delete direct.m_data.m_ppChannelObj[i];
				direct.m_data.m_ppChannelObj[i] = NULL;
			}
			i++;
		}
		delete [] direct.m_data.m_ppChannelObj;
	}
*/

/*
	// here's a hard coded one for now
	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no need to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = direct.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		direct.m_net.OpenConnection(direct.m_http.m_pszHost, 10888, &s); // branding hardcode
		direct.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		direct.m_net.CloseConnection(s); // branding hardcode
	}
*/

//	m_pDlg->SetProgress(DIRECTDLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
//	direct.m_data.m_ulFlags &= ~DIRECT_STATUS_FILESVR_MASK;
//	direct.m_data.m_ulFlags |= DIRECT_STATUS_FILESVR_END;
//	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down file server....");  
//	direct.m_data.SetStatusText(errorstring, direct.m_data.m_ulFlags);
//	_ftime( &direct.m_data.m_timebTick );
//	direct.m_http.EndServer();
	_ftime( &direct.m_data.m_timebTick );
//	AfxMessageBox("shutting down command server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down command server....");  
	direct.m_msgr.DM(MSG_ICONNONE, NULL, "DiReCT:command_server_uninit", errorstring);  //(Dispatch message)
	direct.m_data.SetStatusText(errorstring, DIRECT_STATUS_CMDSVR_END);
	_ftime( &direct.m_data.m_timebTick );
	direct.m_net.StopServer(direct.m_settings.m_usCommandPort, 5000, errorstring);
	_ftime( &direct.m_data.m_timebTick );
//	AfxMessageBox("shutting down status server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down status server....");  
	direct.m_msgr.DM(MSG_ICONNONE, NULL, "DiReCT:status_server_uninit", errorstring);  //(Dispatch message)
	direct.m_data.SetStatusText(errorstring, DIRECT_STATUS_STATUSSVR_END);
	_ftime( &direct.m_data.m_timebTick );
	direct.m_net.StopServer(direct.m_settings.m_usStatusPort, 5000, errorstring);
	_ftime( &direct.m_data.m_timebTick );
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Direct is exiting.");  
	direct.m_msgr.DM(MSG_ICONNONE, NULL, "DiReCT:uninit", errorstring);  //(Dispatch message)
	direct.m_data.SetStatusText(errorstring, direct.m_data.m_ulFlags);



	// save settings.  // dont save them here.  save them on any changes in the main command loop.
	direct.m_settings.Settings(false); //write


	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "DiReCT is exiting");
	direct.m_data.m_ulFlags &= ~CX_ICON_MASK;
	direct.m_data.m_ulFlags |= CX_STATUS_UNINIT;
	direct.m_data.SetStatusText(errorstring, direct.m_data.m_ulFlags);

	//exiting
	direct.m_msgr.DM(MSG_ICONINFO, NULL, "DiReCT", "-------------- DiReCT %s exit ---------------\n\
--------------------------------------------------\n", DIRECT_CURRENT_VERSION);  //(Dispatch message)
///	m_pDlg->SetProgress(DIRECTDLG_CLEAR); // no point

	_ftime( &direct.m_data.m_timebTick );
	direct.m_data.m_ulFlags &= ~DIRECT_STATUS_THREAD_MASK;
	direct.m_data.m_ulFlags |= DIRECT_STATUS_THREAD_ENDED;



	EnterCriticalSection(&direct.m_data.m_critSQL);
	direct.m_settings.m_pdbConn = NULL;
	direct.m_settings.m_pdb = NULL;
	direct.m_data.m_pdbConn = NULL;
	direct.m_data.m_pdb = NULL;

	db.RemoveConnection(pdbConn);
	db2.RemoveConnection(pdb2Conn);
	LeaveCriticalSection(&direct.m_data.m_critSQL);

	
	int nClock = clock() + 300; // small delay at end
	while(clock()<nClock)	{_ftime( &direct.m_data.m_timebTick );}
	g_bThreadStarted = false;
	nClock = clock() + direct.m_settings.m_nThreadDwellMS; // another small delay at end
	while(clock()<nClock)	{_ftime( &direct.m_data.m_timebTick );}
	g_pdirect = NULL;
	Sleep(300); //one more small delay at end
	_endthread();
}

/*
void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((g_pmsgr)&&(g_pmsgr->m_lpfnDM))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		CMessagingObject msg;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = msg.EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = msg.EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = msg.EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch;
		pch = (char*) malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
* /
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if(pszEncodedMsg) 
			{
				ulBufferLen = strlen(pszEncodedMsg);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedCaller)
			{
				ulBufferLen = strlen(pszEncodedCaller);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedDest)
			{
				ulBufferLen = strlen(pszEncodedDest);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			g_pmsgr->m_lpfnDM(pch, ulFlags);
			free(pch);
		}
	}
}
*/


/*
// this thread is based on void HTTP10HandlerThread(void* pvArgs) from
// HTTP10.cpp.  It makes a special webserver with security, cgi parsing, and dynamic results
void DirectHTTPThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return;}

	CDirectMain* pDirect = (CDirectMain*)(pClient->m_lpObject) ;  // pointer to the global object.

// HTTP service by def is a non-persistent service, so even if the Client type is
// set to persist, we ignore that directive in this handler thread

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;


		CHTTP10* phttp = (CHTTP10*)(&(pDirect->m_http)) ;  // pointer to the global object.
		if(phttp == NULL)// cant do anything!
		{
			shutdown(pClient->m_socket, SD_BOTH);
			closesocket(pClient->m_socket);
			(*(pClient->m_pulConnections))--;
			_endthread(); return;
		}

		CHTTP10 http(phttp->m_pszRoot);  // local object for utility functions ONLY.
		// this allows the local to clone the root setting of the global, 
		// in order to do correct parsing in its own thread

		// the following are just pointers to the global obj.  
		// must NULL these out before object gets destroyed, 
		// so that global resources not freed!   !!!!!!!  important !!!!!
		http.m_pszHost = phttp->m_pszHost;
		http.m_ppszErrors = phttp->m_ppszMime;
		http.m_ppszMime = phttp->m_ppszMime;   
		http.m_ulStatus = phttp->m_ulStatus;   // important for redirect and error
		
		unsigned char* pch;// for use with GetLine().
		unsigned long ulBufLen;// for use with GetLine().
		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		unsigned short usCode = 0;

		// get the raw buffer and process it yourself
		nReturn = http.m_net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_EOLN|EOLN_HTTP, pszStatus);
		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). 
			//was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
		{
			//error.
			if(pClient->m_lpMsgObj)
			{
				_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error receiving data.  %s", pszStatus);
				((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
			}

			usCode = 400;  //  we TRY to send this....
			if(pch!=NULL)
			{
				free(pch);  // must free the incoming data buffer 
				pch = NULL;
			}
			// create a return message buffer.
			pch = (unsigned char*)http.ErrorBuffer(NULL, usCode);   // 400 bad request
			ulBufLen = strlen((char*)pch); // valid from an error page.

			// send malformed request answer if possible  - have to tell the client that the request was not received in good order.
			nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
			if(nReturn<NET_SUCCESS)
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
				}
			}
		}
		else  // successful reception of data.
		{
			// process the data here...
			// in this sample the buffer is loaded into
			// pch, with ulBufLen as length

/*
			FILE* fp = fopen("rec.txt", "ab");
			if (fp)
			{
				fwrite(pch, 1, ulBufLen, fp );
				fflush(fp);
				fclose(fp);
			}
* /
			// parse buffer, format reply.
			CHTTPHeader header;

			if (http.ParseHeader(pch, ulBufLen, &header) < HTTP_SUCCESS)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("header not parsed\r\n\r\n", 1, strlen("header not parsed\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}
* /
				
				if(pch!=NULL) 
				{
					free(pch);  // must free the incoming data buffer 
					pch = NULL;
				}
				// create a return message buffer.
				usCode = 400;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}
			else
			{
				// here we have enough info to check security if desired, and if not, return a 401;
				// else continue....
/*
					FILE* fp = fopen("rec.txt", "ab");
					if (fp)
					{// lets write out the whole header so we can examine its full contents. including args.
						char bufferin[10000];
						sprintf(bufferin,
"header successfully parsed\r\n\
method: %d\r\n\
version: %s\r\n\
URL: %s\r\n\
Path: %s\r\n\
mimetype: %s\r\n\
user: %s\r\n\
password: %s\r\n\
num args: %ld\r\n",
	header.m_ucMethod,  // enumerated values
	header.m_pszVersion==NULL?"(null)":header.m_pszVersion,
	header.m_pszURL==NULL?"(null)":header.m_pszURL,
	header.m_pszPath==NULL?"(null)":header.m_pszPath,
	header.m_pszMimeType==NULL?"(null)":header.m_pszMimeType,
	header.m_pszUser==NULL?"(null)":header.m_pszUser,
	header.m_pszPassword==NULL?"(null)":header.m_pszPassword,
	header.m_ulNumArgs

);
		
	unsigned long k=0;
	while(k<header.m_ulNumArgs)
	{
		if((header.m_ppszArgNames)&&(header.m_ppszArgValues))
		{
			strcat(bufferin, "  ");
			strcat(bufferin, header.m_ppszArgNames[k]==NULL?"(null)":header.m_ppszArgNames[k]);
			strcat(bufferin, "=");
			strcat(bufferin, header.m_ppszArgValues[k]==NULL?"(null)":header.m_ppszArgValues[k]);
			strcat(bufferin, "\r\n");
		}
	k++;
	}

	strcat(bufferin, "headers:\r\n");
	strcat(bufferin, header.m_pszHeaders);
	strcat(bufferin, "\r\n");


			//		fwrite("header successfully parsed\r\n", 1, strlen("header successfully parsed\r\n"), fp );
						fwrite(bufferin, 1, strlen(bufferin), fp );
						fflush(fp);
						fclose(fp);
					}

* /


				if((phttp->m_pSecure)&&((header.m_pszUser==NULL)||(header.m_pszPassword==NULL)))
				{
					usCode = 401;
					pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
					ulBufLen = strlen((char*)pch); // valid from an error page.
					// buffer successfully parsed.
				}
				else 
				{
					if(pch!=NULL)
					{
						free(pch);  // must free the incoming data buffer 
						pch = NULL;
					}

					// create a return message buffer.
					switch(header.m_ucMethod)
					{
					case HTTP_GET://			1  // HTTP/1.0
					case HTTP_HEAD://			2  // HTTP/1.0
						{
							if(
									(strnicmp(header.m_pszPath, http.m_pszRoot, strlen(http.m_pszRoot)) == 0 )  // else not a part of the served file tree
								)
							{
								if(phttp->m_pSecure)
								{
									// have to check the main object for permission.
									// we are going to go by URL (asset level)
									// Loci are going to just be convenient names for the admins to set up.

									if((phttp->m_pSecure->CheckSecure(header.m_pszUser, header.m_pszPassword, NULL, header.m_pszURL))!=SECURE_SUCCESS)
									{
										usCode = 401;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
										ulBufLen = strlen((char*)pch); // valid from an error page.
										break; // get out of the switch from here.
									}
									// else it flows thru....
								}

								DWORD dwAttrib;

								dwAttrib = GetFileAttributes(header.m_pszPath);

								// check for directory (redirect nec.)
								if ( (dwAttrib!=0xffffffff)&&(dwAttrib&FILE_ATTRIBUTE_DIRECTORY) )  // if error, it may be because no file exists, so check in the next part.
								{
									usCode = 301;  // not necessary but why not.
									pch = (unsigned char*)http.RedirectBuffer(&header);
									ulBufLen = strlen((char*)pch); // valid from an redirect page.
								}
								else
								{
									// before we try to open the file, lets spend the time now (not while the file is open)
									// to figure out how long the static text respone buffer will be.
									CBufferUtil bu;
									// notice the sample "Server: DirectServer/3.0.3.1" header included.  Pick the appname and ver, probably best
									unsigned long ulBufferLen = strlen("HTTP/1.0 200 OK\r\nServer: DirectServer/\r\nContent-Length: \r\n\r\n");
									ulBufferLen += strlen(DIRECT_CURRENT_VERSION);

									if((header.m_pszMimeType)&&(strlen(header.m_pszMimeType)>0))
									{
										ulBufferLen += (strlen("Content-Type: \r\n") + strlen(header.m_pszMimeType)); // only add if known!
									}
									else // empty!
									{
										// dont free the "permanent resource", just null the pointer
										header.m_pszMimeType=NULL;
									}

									FILE* pFile = NULL;

									pFile = fopen(header.m_pszPath, "rb");
									if (pFile == NULL)  // file not openable, or not found.
									{
										usCode = 404;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 404 not found
										ulBufLen = strlen((char*)pch); // valid from an error page.
									}
									else
									{
										// this part just assembles the file into a buffer.
										// this may be modified to do dynamic html things by using the 
										// pClient->m_lpObject that refers back to some parser....

										usCode = 200; // OK  (!)
										// determine file size
										fseek(pFile, 0, SEEK_END);
										unsigned long ulFileLen = ftell(pFile);


										// here we have to parse out any directives.  if this is a template file,
										// we may have a variable length.
									//	x


										if(header.m_ucMethod == HTTP_GET)
										{
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += (strlen(length)+ulFileLen);

											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you truncations at zeroed bytes

											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: DirectServer/%s", DIRECT_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
											
											fclose(pFile);
										}
										else
										if(header.m_ucMethod == HTTP_HEAD)
										{
											fclose(pFile);  // we have all we need already.
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += strlen(length);
											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											//  create the header
											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: DirectServer/%s", DIRECT_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												// its identical except for the body.  next line must stay commented out, its just there for ref.
												//fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
										}
									}
								}
							}
							else  // not part of the server service.
							{
								usCode = 403;
								pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 403 forbidden ...
								ulBufLen = strlen((char*)pch); // valid from an error page.
							}

						} break;
					case HTTP_POST://			3  // HTTP/1.0
						{
							usCode = 501;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 501 not implemented ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_PUT://			4  // HTTP/1.1
					case HTTP_DELETE://		5  // HTTP/1.1
					case HTTP_OPTIONS://  6  // HTTP/1.1
					case HTTP_TRACE://	  7  // HTTP/1.1
						{
							usCode = 505;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 505 http version not supported ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_UNDEF://		0  // internal
					default:
						{
							usCode = 400;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					}
				}
			}

			// send a reply, in pch., with strlen pch as length

			if(pch == NULL)  // had a problem forming the request, try to send a last ditch error code.
			{
				usCode = 500;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 500 internal server error
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}

			if(pch != NULL)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("sending:\r\n", 1, strlen("sending:\r\n"), fp );
					fwrite(pch, 1, ulBufLen, fp );
					fwrite("\r\n\r\n", 1, strlen("\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}

* /
				nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
				if(nReturn<NET_SUCCESS)
				{
					//error.
					if(pClient->m_lpMsgObj)
					{
						_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
						((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
					}
				}
			}
		}

//		char foo[34]; sprintf(foo, "code: %d", usCode);	AfxMessageBox(foo);

		http.m_pszHost = NULL;		// but do NOT free resource
		http.m_ppszErrors = NULL; // but do NOT free resource
		http.m_ppszMime = NULL;   // but do NOT free resource

		if(pch!=NULL) free(pch);  // must free the data buffer 

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}
*/


void DirectCommandHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure in Protocol 1  //SECURE with PROTOCOL2 later
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszStatus);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "DiReCT:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "DiReCT:CommandHandlerThread");
						}
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{
//AfxMessageBox("receiving");

					// parse the command:

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					switch(data.m_ucCmd)
					{
					case CX_CMD_GETINFO: // not actually supported yet.  everything thru csf files for now.
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case CX_CMD_BYE:
						{
	g_pdirect->m_msgr.DM(MSG_ICONINFO, NULL, "DiReCT", "The Global Kill command has been received.");  //(Dispatch message)

							bSetGlobalKill = true;
							g_pdirect->m_data.m_bQuietKill = true;
							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					default:
						{
							//just ack
							data.m_ucCmd = NET_CMD_ACK;

		//					char repbuf[64];
							if(data.m_pucData!=NULL)
							{
		/*
								_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

								for(unsigned long q=0; q<data.m_ulDataLen; q++)
								{
									if(strlen(repbuf)<63)
										strncat(repbuf, (char*)(data.m_pucData+q), 1);
								}
								if(strlen(repbuf)<63)
									strcat(repbuf, "]");
								data.m_ulDataLen = strlen(repbuf);
								repbuf[data.m_ulDataLen] = 0;  // just in case
		*/
								free(data.m_pucData);  //destroy the buffer;
							}
		/*
							else
							{
								_snprintf(repbuf, 63, "data was NULL");
								data.m_ulDataLen = strlen(repbuf); 
							}

							data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
							if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
							else 
							{
								data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
								data.m_ulDataLen = 0;
							}
		*/
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					}

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "DiReCT:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "DiReCT:CommandHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.
	if (bSetGlobalKill)	//g_bKillThread = true;
	((CDirectHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}

void DirectStatusHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszStatus);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "DiReCT:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "DiReCT:CommandHandlerThread");
						}
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "DiReCT:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "DiReCT:CommandHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}


int CDirectMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)&&(strlen(m_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			m_settings.m_pszMessages,
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			m_settings.m_pszMessages
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_data.m_critSQL);
		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_data.m_critSQL);
			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return DIRECT_SUCCESS;
		}
		LeaveCriticalSection(&m_data.m_critSQL);

	}
	return DIRECT_ERROR;
}


void DirectAutomationThread(void* pvArgs)
{
	CDirectMain* pdirect = (CDirectMain*) pvArgs;
	if(pdirect == NULL) pdirect = g_pdirect;

	pdirect->m_data.m_bAutomationThreadStarted=true;
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");
	CDBUtil db;

	CDBconn* pdbConn = db.CreateNewConnection(pdirect->m_settings.m_pszDSN, pdirect->m_settings.m_pszUser, pdirect->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			pdirect->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "DiReCT:automation_database_connect", errorstring);  //(Dispatch message)
			pdbConn = pdirect->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", pdirect->m_settings.m_pszDSN, pdirect->m_settings.m_pszUser, pdirect->m_settings.m_pszPW); 
		pdirect->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "DiReCT:automation_database_init", errorstring);  //(Dispatch message)
		pdbConn = pdirect->m_data.m_pdbConn;

		//**MSG
	}



	while(!g_bKillThread)
	{
		// check automation list for changes (mod)
		bool bChanges = false;

//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%d %d %d %d %d.%03d", pdirect->m_data.m_nIndexAutomationEndpoint, pdirect->m_settings.m_nNumEndpointsInstalled, pdirect->m_settings.m_ppEndpointObject, pdirect->m_settings.m_ppEndpointObject[pdirect->m_data.m_nIndexAutomationEndpoint], pdirect->m_data.m_timebTick.time, pdirect->m_data.m_timebTick.millitm);   Sleep(50);//(Dispatch message)

		if(
			  (pdirect->m_data.m_nIndexAutomationEndpoint>=0)
			&&(pdirect->m_settings.m_nNumEndpointsInstalled>0)
			&&(pdirect->m_data.m_nIndexAutomationEndpoint<pdirect->m_settings.m_nNumEndpointsInstalled)
			&&(pdirect->m_settings.m_ppEndpointObject)
			&&(pdirect->m_settings.m_ppEndpointObject[pdirect->m_data.m_nIndexAutomationEndpoint])
			&&(!pdirect->m_data.m_bProcessSuspended)
			&&(pdirect->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!pdirect->m_data.m_key.m_bExpires)
				||((pdirect->m_data.m_key.m_bExpires)&&(!pdirect->m_data.m_key.m_bExpired))
				||((pdirect->m_data.m_key.m_bExpires)&&(pdirect->m_data.m_key.m_bExpireForgiveness)&&(pdirect->m_data.m_key.m_ulExpiryDate+pdirect->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!pdirect->m_data.m_key.m_bMachineSpecific)
				||((pdirect->m_data.m_key.m_bMachineSpecific)&&(pdirect->m_data.m_key.m_bValidMAC))
				)

			)
		{
_ftime( &pdirect->m_data.m_timebAutomationTick ); // the last time check inside the thread
			CDirectEndpointObject* pAObj = pdirect->m_settings.m_ppEndpointObject[pdirect->m_data.m_nIndexAutomationEndpoint];

			if((pAObj->m_pszLiveEvents)&&(strlen(pAObj->m_pszLiveEvents)))
			{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "Checking %s", pAObj->m_pszLiveEvents);   Sleep(50);//(Dispatch message)
				pAObj->m_nModLiveEvents = pAObj->CheckDatabaseMod(&db, pdbConn, pAObj->m_pszLiveEvents);
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "Checked mod %s = %d, %d last", pAObj->m_pszLiveEvents, pAObj->m_nModLiveEvents, pAObj->m_nLastModLiveEvents);   Sleep(50);//(Dispatch message)


				if((pAObj->m_nModLiveEvents>=0)&&(pAObj->m_nModLiveEvents!=pAObj->m_nLastModLiveEvents))
				{
					bChanges = true;
					pdirect->m_data.m_bEventsChanged = true;
				}
			}

			if(bChanges)
			{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "Automation changes exist %d", pAObj->m_nModLiveEvents);   Sleep(50);//(Dispatch message)
				// pdirect->m_data.m_nEventLastMax = 0; // start at the beginning....
// actually dont reset this.
				bool bSQLError = false;
				pdirect->m_data.m_bEventsChanged = true;
				// if changes
				// {
				//   Close the m_prsEvents recordset and delete it, set it back to NULL.

//				pdirect->m_data.ReleaseRecordSet();

				if((pAObj->m_usType&DIRECT_DEP_AUTO_MASK)==DIRECT_DEP_AUTO_HELIOS)
				{
					// first, run a query to update all things that are in the direct list that
					// are also in the helios list.  Update times and status, basically.

// update existing direct event items (itemids exist in both lists)
// **** example in comments not necessarily complete or correct.
// update direct_events set direct_events.channelid = sentinel_events.list_id, 
//direct_events.event_clip = sentinel_events.event_clip, direct_events.event_title = sentinel_events.event_title, 
//direct_events.event_start = sentinel_events.event_start, direct_events.status = sentinel_events.event_status from pdirect->dbo.events as direct_events, sentinel.dbo.events as sentinel_events where direct_events.event_itemid = sentinel_events.itemid

				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE direct_events SET \
direct_events.channelid = helios_events.list_id, \
direct_events.event_id = helios_events.event_id, \
direct_events.event_clip = helios_events.event_clip, \
direct_events.event_title = helios_events.event_title, \
direct_events.event_start = helios_events.event_start, \
direct_events.event_status = helios_events.event_status, \
direct_events.event_data = helios_events.event_data \
FROM %s.dbo.%s \
AS direct_events, \
%s.dbo.%s \
AS helios_events \
WHERE direct_events.event_itemid = helios_events.itemid",
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
						pAObj->m_pszDBName?pAObj->m_pszDBName:"Helios", 
						pAObj->m_pszLiveEvents?pAObj->m_pszLiveEvents:"Events"  // sentinel DB and table name;
						);
if(pdirect->m_settings.m_bDebugInsertSQL) pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug_automation_update", "SQL: %s", szSQL ); //  Sleep(50);//(Dispatch message)

		EnterCriticalSection(&pdirect->m_data.m_critSQL);
					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						//**MSG
pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "%s", errorstring ); //  Sleep(50);//(Dispatch message)
					}
		LeaveCriticalSection(&pdirect->m_data.m_critSQL);

				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.

					// {
					// then, select out of the direct list anything that is no longer in Helios.
					// go thru these and perform any clean-up actions that might be necesarry, 
					// }
					// OR
					// {   I like this one better.  the challenge is there may be multiple tables.  so, need a loop
					// run a query to remove anything from the Miranda queue that is not in Helios (this does the cleanup first, before we get to the stuff below)
					// then run another query to remove things in Direct that are not in Helios.
					// }


/// delete events from direct that are no longer in the all edge device queue lists, then the sentinel events list
// **** example in comments not necessarily complete or correct.
//delete from direct.dbo.events where direct.dbo.events.event_itemid not in (select sentinel.dbo.events.itemid from sentinel.dbo.events);

					int nDestModule=0;

					while(nDestModule<pdirect->m_settings.m_nNumEndpointsInstalled)
					{
				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%d < %d", nDestModule,pdirect->m_settings.m_nNumEndpointsInstalled );   Sleep(50);//(Dispatch message)
						if((pdirect->m_settings.m_ppEndpointObject)&&(pdirect->m_settings.m_ppEndpointObject[nDestModule]))
						{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "double not null" );   Sleep(50);//(Dispatch message)

							if(pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType&DIRECT_DEP_EDGE_MASK)
							{

								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM \
%s.dbo.%s \
WHERE %s.dbo.%s.event_itemid NOT IN (SELECT \
%s.dbo.%s.itemid FROM %s.dbo.%s)",
									pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName:"Prospero", 
									pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszQueue?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszQueue:"Queue",  // edge device DB and table name;
									pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName:"Prospero", 
									pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszQueue?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszQueue:"Queue",  // edge device DB and table name;
									pAObj->m_pszDBName?pAObj->m_pszDBName:"Helios", 
									pAObj->m_pszLiveEvents?pAObj->m_pszLiveEvents:"Events",  // Helios DB and table name;
									pAObj->m_pszDBName?pAObj->m_pszDBName:"Helios", 
									pAObj->m_pszLiveEvents?pAObj->m_pszLiveEvents:"Events"  // Helios DB and table name;
									);
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", szSQL );   Sleep(50);//(Dispatch message)

				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.
		EnterCriticalSection(&pdirect->m_data.m_critSQL);
								if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
								{
									//**MSG
pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "%s", errorstring ); //  Sleep(50);//(Dispatch message)
								}
		LeaveCriticalSection(&pdirect->m_data.m_critSQL);
				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "after SQL: %s", szSQL );   Sleep(50);//(Dispatch message)
							}
						}
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "incrementing: %d", nDestModule );   Sleep(50);//(Dispatch message)
						nDestModule++;
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "incremented: %d", nDestModule );   Sleep(50);//(Dispatch message)
					}


					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM \
%s.dbo.%s \
WHERE %s.dbo.%s.event_itemid NOT IN (SELECT \
%s.dbo.%s.itemid from %s.dbo.%s)",
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
						pAObj->m_pszDBName?pAObj->m_pszDBName:"Helios", 
						pAObj->m_pszLiveEvents?pAObj->m_pszLiveEvents:"Events",  // Helios DB and table name;
						pAObj->m_pszDBName?pAObj->m_pszDBName:"Helios", 
						pAObj->m_pszLiveEvents?pAObj->m_pszLiveEvents:"Events"  // Helios DB and table name;
						);
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", szSQL );   Sleep(50);//(Dispatch message)
if(pdirect->m_settings.m_bDebugInsertSQL) pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug_automation_delete", "SQL: %s", szSQL ); //  Sleep(50);//(Dispatch message)

				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.
		EnterCriticalSection(&pdirect->m_data.m_critSQL);
					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						//**MSG
pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", errorstring );  // Sleep(50);//(Dispatch message)
					}
		LeaveCriticalSection(&pdirect->m_data.m_critSQL);

				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.

					// then run a query to add things to the direct list that are in Harris but not yet in Direct.


//find all events in Helios but not in direct
// **** example in comments not necessarily complete or correct.
//select * from sentinel.dbo.events as sentinel_events left join direct.dbo.events as direct_events on sentinel_events.itemid = direct_events.event_itemid  where direct_events.event_itemid is null and sentinel_events.event_type = 57;
/*
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM \
sentinel.dbo.events AS sentinel_events \
LEFT JOIN \
direct.dbo.events AS direct_events ON sentinel_events.itemid = direct_events.event_itemid \
WHERE direct_events.event_itemid IS NULL AND sentinel_events.event_type = %d",
						pAObj->m_pszDBName?pAObj->m_pszDBName:"Sentinel", 
						pAObj->m_pszLiveEvents?pAObj->m_pszLiveEvents:"Events",  // sentinel DB and table name;
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
						SECDATAEVENT
						);

pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", szSQL );   Sleep(50);//(Dispatch message)

		EnterCriticalSection(&pdirect->m_data.m_critSQL);
					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						bSQLError = true;
						//**MSG
pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", errorstring );   Sleep(50);//(Dispatch message)
					}
		LeaveCriticalSection(&pdirect->m_data.m_critSQL);
*/

//insert the events in Helios but not in direct
// **** example in comments not necessarily complete or correct.
//insert into direct.dbo.events (channelid, event_start, event_itemid, event_id, event_clip, 
//event_title, event_status, filename, destinationid, status, description, transfer_date, 
//app_data_aux, type, dest_host, search_files) select sentinel_events.list_id, sentinel_events.event_start, sentinel_events.itemid, sentinel_events.event_id, sentinel_events.event_clip, sentinel_events.event_title, sentinel_events.event_status, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL 
//from sentinel.dbo.events as sentinel_events left join direct.dbo.events as direct_events 
//on sentinel_events.itemid = direct_events.event_itemid
//where direct_events.event_itemid is null and sentinel_events.event_type = 160;

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO \
%s.dbo.%s (channelid, event_start, event_itemid, event_id, event_clip, \
event_title, event_status, event_data, filename, destinationid, status, transfer_date, \
app_data_aux, type, dest_host, search_files) SELECT helios_events.list_id, helios_events.event_start, \
helios_events.itemid, helios_events.event_id, helios_events.event_clip, helios_events.event_title, \
helios_events.event_status, helios_events.event_data, NULL, NULL, 0, NULL, 0, 0, NULL, NULL \
FROM (SELECT * FROM %s.dbo.%s WHERE event_status > 4 AND event_status < 9 AND list_id in (select distinct id from %s.dbo.%s)) \
AS helios_events LEFT JOIN \
(SELECT * FROM %s.dbo.%s as direct_events JOIN (select distinct id from %s.dbo.%s) as channels \
ON direct_events.channelid= channels.id) as direct_events \
ON helios_events.itemid = direct_events.event_itemid \
WHERE direct_events.event_itemid IS NULL",// AND helios_events.event_type = %d",
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
						pAObj->m_pszDBName?pAObj->m_pszDBName:"Helios", 
						pAObj->m_pszLiveEvents?pAObj->m_pszLiveEvents:"Events",  // helios DB and table name;
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszChannelDests?pdirect->m_settings.m_pszChannelDests:"ChannelInfo", // view
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszChannelDests?pdirect->m_settings.m_pszChannelDests:"ChannelInfo" // view
						//SECDATAEVENT // no such type in Helios
						);


//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", szSQL );   Sleep(50);//(Dispatch message)
if(pdirect->m_settings.m_bDebugInsertSQL) pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug_automation_insert", "SQL: %s", szSQL ); //  Sleep(50);//(Dispatch message)

				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.
		EnterCriticalSection(&pdirect->m_data.m_critSQL);
					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						bSQLError = true;
						//**MSG
pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "%s", errorstring );  // Sleep(50);//(Dispatch message)
					}
		LeaveCriticalSection(&pdirect->m_data.m_critSQL);
				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.

					//   escape with incrementor at -1.  transfer incrementor reset to -1 also.   //done at top of changes
					// }  else...

					pdirect->m_data.m_nNumberOfEvents = pdirect->m_data.ReturnNumberOfRecords(errorstring);
					if(pdirect->m_data.m_nNumberOfEvents<0) bSQLError = true;

					if(!bSQLError) 
					{
						pAObj->m_nLastModLiveEvents = pAObj->m_nModLiveEvents;
						pdirect->m_data.m_nEventLastMax=pdirect->m_data.m_nNumberOfEvents-1; // in case the automation changes reduce the number of events, we still want to analyze below.
					}

					// Sleep(1); 
					// continue;  // breaks out of loop  // dont do this.  need to analyze events below

				}
				else
				if((pAObj->m_usType&DIRECT_DEP_AUTO_MASK)==DIRECT_DEP_AUTO_SENTINEL)
				{

					// first, run a query to update all things that are in the direct list that
					// are also in the sentinel list.  Update times and status, basically.

// update existing direct event items (itemids exist in both lists)
// **** example in comments not necessarily complete or correct.
// update direct_events set direct_events.channelid = sentinel_events.list_id, 
//direct_events.event_clip = sentinel_events.event_clip, direct_events.event_title = sentinel_events.event_title, 
//direct_events.event_start = sentinel_events.event_start, direct_events.status = sentinel_events.event_status from direct.dbo.events as direct_events, sentinel.dbo.events as sentinel_events where direct_events.event_itemid = sentinel_events.itemid

				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE direct_events SET \
direct_events.channelid = sentinel_events.list_id, \
direct_events.event_id = sentinel_events.event_id, \
direct_events.event_clip = sentinel_events.event_clip, \
direct_events.event_title = sentinel_events.event_title, \
direct_events.event_start = sentinel_events.event_start, \
direct_events.event_status = sentinel_events.event_status, \
direct_events.event_data = sentinel_events.event_data \
FROM %s.dbo.%s \
AS direct_events, \
%s.dbo.%s \
AS sentinel_events \
WHERE direct_events.event_itemid = sentinel_events.itemid",
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
						pAObj->m_pszDBName?pAObj->m_pszDBName:"Sentinel", 
						pAObj->m_pszLiveEvents?pAObj->m_pszLiveEvents:"Events"  // sentinel DB and table name;
						);

//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", szSQL );   Sleep(50);//(Dispatch message
if(pdirect->m_settings.m_bDebugInsertSQL) pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug_automation_update", "SQL: %s", szSQL ); //  Sleep(50);//(Dispatch message)

		EnterCriticalSection(&pdirect->m_data.m_critSQL);
					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						//**MSG
pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "%s", errorstring );  // Sleep(50);//(Dispatch message)
					}
		LeaveCriticalSection(&pdirect->m_data.m_critSQL);

				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.

					// {
					// then, select out of the direct list anything that is no longer in harris.
					// go thru these and perform any clean-up actions that might be necesarry, 
					// }
					// OR
					// {   I like this one better.  the challenge is there may be multiple tables.  so, need a loop
					// run a query to remove anything from the Miranda queue that is not in Harris (this does the cleanup first, before we get to the stuff below)
					// then run another query to remove things in Direct that are not in Harris.
					// }


/// delete events from direct that are no longer in the all edge device queue lists, then the sentinel events list
// **** example in comments not necessarily complete or correct.
//delete from direct.dbo.events where direct.dbo.events.event_itemid not in (select sentinel.dbo.events.itemid from sentinel.dbo.events);

					int nDestModule=0;

					while(nDestModule<pdirect->m_settings.m_nNumEndpointsInstalled)
					{
				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%d < %d", nDestModule,pdirect->m_settings.m_nNumEndpointsInstalled );   Sleep(50);//(Dispatch message)
						if((pdirect->m_settings.m_ppEndpointObject)&&(pdirect->m_settings.m_ppEndpointObject[nDestModule]))
						{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "double not null" );   Sleep(50);//(Dispatch message)

							if(pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType&DIRECT_DEP_EDGE_MASK)
							{

								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM \
%s.dbo.%s \
WHERE %s.dbo.%s.event_itemid NOT IN (SELECT \
%s.dbo.%s.itemid FROM %s.dbo.%s)",
									pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName:"Prospero", 
									pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszQueue?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszQueue:"Queue",  // edge device DB and table name;
									pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName:"Prospero", 
									pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszQueue?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszQueue:"Queue",  // edge device DB and table name;
									pAObj->m_pszDBName?pAObj->m_pszDBName:"Sentinel", 
									pAObj->m_pszLiveEvents?pAObj->m_pszLiveEvents:"Events",  // sentinel DB and table name;
									pAObj->m_pszDBName?pAObj->m_pszDBName:"Sentinel", 
									pAObj->m_pszLiveEvents?pAObj->m_pszLiveEvents:"Events"  // sentinel DB and table name;
									);
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", szSQL );   Sleep(50);//(Dispatch message)

				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.
		EnterCriticalSection(&pdirect->m_data.m_critSQL);
								if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
								{
									//**MSG
pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "%s", errorstring ); //  Sleep(50);//(Dispatch message)
								}
		LeaveCriticalSection(&pdirect->m_data.m_critSQL);
				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "after SQL: %s", szSQL );   Sleep(50);//(Dispatch message)
							}
						}
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "incrementing: %d", nDestModule );   Sleep(50);//(Dispatch message)
						nDestModule++;
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "incremented: %d", nDestModule );   Sleep(50);//(Dispatch message)
					}


					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM \
%s.dbo.%s \
WHERE %s.dbo.%s.event_itemid NOT IN (SELECT \
%s.dbo.%s.itemid from %s.dbo.%s)",
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
						pAObj->m_pszDBName?pAObj->m_pszDBName:"Sentinel", 
						pAObj->m_pszLiveEvents?pAObj->m_pszLiveEvents:"Events",  // sentinel DB and table name;
						pAObj->m_pszDBName?pAObj->m_pszDBName:"Sentinel", 
						pAObj->m_pszLiveEvents?pAObj->m_pszLiveEvents:"Events"  // sentinel DB and table name;
						);
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", szSQL );   Sleep(50);//(Dispatch message)
if(pdirect->m_settings.m_bDebugInsertSQL) pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug_automation_delete", "SQL: %s", szSQL ); //  Sleep(50);//(Dispatch message)

				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.
		EnterCriticalSection(&pdirect->m_data.m_critSQL);
					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						//**MSG
pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", errorstring ); //  Sleep(50);//(Dispatch message)
					}
		LeaveCriticalSection(&pdirect->m_data.m_critSQL);

				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.

					// then run a query to add things to the direct list that are in Harris but not yet in Direct.


//find all events in sentinel but not in direct
// **** example in comments not necessarily complete or correct.
//select * from sentinel.dbo.events as sentinel_events left join direct.dbo.events as direct_events on sentinel_events.itemid = direct_events.event_itemid  where direct_events.event_itemid is null and sentinel_events.event_type = 57;
/*
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM \
sentinel.dbo.events AS sentinel_events \
LEFT JOIN \
direct.dbo.events AS direct_events ON sentinel_events.itemid = direct_events.event_itemid \
WHERE direct_events.event_itemid IS NULL AND sentinel_events.event_type = %d",
						pAObj->m_pszDBName?pAObj->m_pszDBName:"Sentinel", 
						pAObj->m_pszLiveEvents?pAObj->m_pszLiveEvents:"Events",  // sentinel DB and table name;
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
						SECDATAEVENT
						);

pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", szSQL );   Sleep(50);//(Dispatch message)

		EnterCriticalSection(&pdirect->m_data.m_critSQL);
					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						bSQLError = true;
						//**MSG
pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", errorstring );   Sleep(50);//(Dispatch message)
					}
		LeaveCriticalSection(&pdirect->m_data.m_critSQL);
*/

//insert the events in sentinel but not in direct
// **** example in comments not necessarily complete or correct.
//insert into direct.dbo.events (channelid, event_start, event_itemid, event_id, event_clip, 
//event_title, event_status, filename, destinationid, status, description, transfer_date, 
//app_data_aux, type, dest_host, search_files) select sentinel_events.list_id, sentinel_events.event_start, sentinel_events.itemid, sentinel_events.event_id, sentinel_events.event_clip, sentinel_events.event_title, sentinel_events.event_status, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL 
//from sentinel.dbo.events as sentinel_events left join direct.dbo.events as direct_events 
//on sentinel_events.itemid = direct_events.event_itemid
//where direct_events.event_itemid is null and sentinel_events.event_type = 160;

					
					if((pdirect->m_settings.m_nFilterType>=0)&&(pdirect->m_settings.m_nTypeComparator>0))
					{
						switch(pdirect->m_settings.m_nTypeComparator)
						{
						case DIRECT_RULE_COMPARE_EQUALS://				0x0001  // == equals
						{
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, " AND sentinel_events.event_type = %d", pdirect->m_settings.m_nFilterType);
						} break;
						case DIRECT_RULE_COMPARE_GT://						0x0002  // >  greater than
						{
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, " AND sentinel_events.event_type > %d", pdirect->m_settings.m_nFilterType);
						} break;
						case DIRECT_RULE_COMPARE_LT://						0x0003  // <  less than
						{
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, " AND sentinel_events.event_type > %d", pdirect->m_settings.m_nFilterType);
						} break;
						case DIRECT_RULE_COMPARE_GTOE	://				0x0004  // >= greater than or equal to
						{
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, " AND (sentinel_events.event_type > %d OR sentinel_events.event_type = %d", pdirect->m_settings.m_nFilterType, pdirect->m_settings.m_nFilterType);
						} break;
						case DIRECT_RULE_COMPARE_LTOE	://				0x0005  // <= less than or equal to
						{
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, " AND (sentinel_events.event_type <= %d OR sentinel_events.event_type = %d", pdirect->m_settings.m_nFilterType, pdirect->m_settings.m_nFilterType);
						} break;
						case DIRECT_RULE_COMPARE_NOTEQUAL://			0x0006  // != equals
						{
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, " AND sentinel_events.event_type <> %d", pdirect->m_settings.m_nFilterType);
						} break;
						case DIRECT_RULE_COMPARE_PARTIAL://				0x0007  // ~= partial comparison
						{
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, " AND sentinel_events.event_type LIKE '%d'", pdirect->m_settings.m_nFilterType);
						} break;
						case DIRECT_RULE_COMPARE_AND://						0x0008  // &  contains any of the bitflags
						{
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, " AND sentinel_events.event_type & %d <> 0", pdirect->m_settings.m_nFilterType);
						} break;
						case DIRECT_RULE_COMPARE_ALL://						0x0009  // =& contains all the bitflags
						{
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, " AND sentinel_events.event_type & %d = %d", pdirect->m_settings.m_nFilterType, pdirect->m_settings.m_nFilterType);
						} break;
						case DIRECT_RULE_COMPARE_NOT://						0x000a  // !& does not contain any the bitflag
						{
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, " AND sentinel_events.event_type & %d = 0", pdirect->m_settings.m_nFilterType);
						} break;
						default:  strcpy(errorstring, ""); break;
						}
					}
					else
					{
						strcpy(errorstring, "");
					}

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO \
%s.dbo.%s (channelid, event_start, event_itemid, event_id, event_clip, \
event_title, event_status, event_data, filename, destinationid, status, transfer_date, \
app_data_aux, type, dest_host, search_files) SELECT sentinel_events.list_id, sentinel_events.event_start, \
sentinel_events.itemid, sentinel_events.event_id, sentinel_events.event_clip, sentinel_events.event_title, \
sentinel_events.event_status, sentinel_events.event_data, NULL, NULL, 0, NULL, 0, 0, NULL, NULL \
FROM (SELECT * FROM %s.dbo.%s WHERE ((event_status & 1) = 0) AND list_id in (select distinct id from %s.dbo.%s)) \
AS sentinel_events LEFT JOIN \
(SELECT * FROM %s.dbo.%s as direct_events JOIN (select distinct id from %s.dbo.%s) as channels \
ON direct_events.channelid= channels.id) as direct_events \
ON sentinel_events.itemid = direct_events.event_itemid \
WHERE direct_events.event_itemid IS NULL%s",
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
						pAObj->m_pszDBName?pAObj->m_pszDBName:"Sentinel", 
						pAObj->m_pszLiveEvents?pAObj->m_pszLiveEvents:"Events",  // sentinel DB and table name;
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszChannelDests?pdirect->m_settings.m_pszChannelDests:"ChannelInfo", // view
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszChannelDests?pdirect->m_settings.m_pszChannelDests:"ChannelInfo", // view
						//SECDATAEVENT  //IS
						//SECAVEVENT
						errorstring
						);
if(pdirect->m_settings.m_bDebugInsertSQL) pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug_automation_insert", "SQL: %s", szSQL ); //  Sleep(50);//(Dispatch message)

					
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", szSQL );   Sleep(50);//(Dispatch message)

				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.
		EnterCriticalSection(&pdirect->m_data.m_critSQL);
					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						bSQLError = true;
						//**MSG
pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "%s", errorstring ); //  Sleep(50);//(Dispatch message)
					}
		LeaveCriticalSection(&pdirect->m_data.m_critSQL);
					_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.

					//   escape with incrementor at -1.  transfer incrementor reset to -1 also.   //done at top of changes
					// }  else...

/*
// can remove this, have excluded them in the insert query
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET type = 2 WHERE (event_status & 1 )= 1",
						
//OR event_status = 513 \
//OR event_status = 257 \
//OR event_status = 129",
						pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
						pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events"   // the LiveEvents table name

						);
//0000000001  1     done
//1000000001  513   missed and done
//0100000001  257   short and done
//0010000001  129   skipped and done
// the keyword here, is done.  dont bother with done elements.

//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "checked rules for %s, none apply SQL: %s",pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title, szSQL);  Sleep(50);//(Dispatch message)
EnterCriticalSection(&pdirect->m_data.m_critSQL);
					if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
					//**MSG
	//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

					}				
LeaveCriticalSection(&pdirect->m_data.m_critSQL);
*/


					pdirect->m_data.m_nNumberOfEvents = pdirect->m_data.ReturnNumberOfRecords(errorstring);
					if(pdirect->m_data.m_nNumberOfEvents<0) bSQLError = true;


					if(!bSQLError) 
					{
						pAObj->m_nLastModLiveEvents = pAObj->m_nModLiveEvents;
						pdirect->m_data.m_nEventLastMax=pdirect->m_data.m_nNumberOfEvents-1; // in case the automation changes reduce the number of events, we still want to analyze below.
					}

					// Sleep(1); 
					// continue;  // breaks out of loop  // dont do this.  need to analyze events below
				}  // type was sentinel
				// else automation type unknown, so nothing
			} // automation changes exist

/////////////////////////////////////////////////////////
//			else  // no automation changes
		} // if license etc


		Sleep(1); // dont peg processor
	} // while(!g_bKillThread)
	pdirect->m_data.m_bAutomationThreadStarted=false;
	_endthread();

}

void DirectAnalysisThread(void* pvArgs)
{
	CDirectMain* pdirect = (CDirectMain*) pvArgs;
	if(pdirect == NULL) pdirect = g_pdirect;

	pdirect->m_data.m_bAnalysisThreadStarted=true;
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");
	CDBUtil db;

	CDBconn* pdbConn = db.CreateNewConnection(pdirect->m_settings.m_pszDSN, pdirect->m_settings.m_pszUser, pdirect->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			pdirect->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "DiReCT:analysis_database_connect", errorstring);  //(Dispatch message)
			pdbConn = pdirect->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", pdirect->m_settings.m_pszDSN, pdirect->m_settings.m_pszUser, pdirect->m_settings.m_pszPW); 
		pdirect->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "DiReCT:analysis_database_init", errorstring);  //(Dispatch message)
		pdbConn = pdirect->m_data.m_pdbConn;

		//**MSG
	}

	while(!g_bKillThread)
	{
		if(
			  (pdirect->m_data.m_nIndexAutomationEndpoint>=0)
			&&(pdirect->m_settings.m_nNumEndpointsInstalled>0)
			&&(pdirect->m_data.m_nIndexAutomationEndpoint<pdirect->m_settings.m_nNumEndpointsInstalled)
			&&(pdirect->m_settings.m_ppEndpointObject)
			&&(pdirect->m_settings.m_ppEndpointObject[pdirect->m_data.m_nIndexAutomationEndpoint])
			&&(!pdirect->m_data.m_bProcessSuspended)
			&&(pdirect->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!pdirect->m_data.m_key.m_bExpires)
				||((pdirect->m_data.m_key.m_bExpires)&&(!pdirect->m_data.m_key.m_bExpired))
				||((pdirect->m_data.m_key.m_bExpires)&&(pdirect->m_data.m_key.m_bExpireForgiveness)&&(pdirect->m_data.m_key.m_ulExpiryDate+pdirect->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!pdirect->m_data.m_key.m_bMachineSpecific)
				||((pdirect->m_data.m_key.m_bMachineSpecific)&&(pdirect->m_data.m_key.m_bValidMAC))
				)

			)
		{
_ftime( &pdirect->m_data.m_timebAnalysisTick ); // the last time check inside the thread
			CDirectEndpointObject* pAObj = pdirect->m_settings.m_ppEndpointObject[pdirect->m_data.m_nIndexAutomationEndpoint];

		//	{
		// {
		//   if m_prsEvents==NULL or incrementor at -1, {pull the list from the top, sorted by event_start asc. set incrementor to 0. (transfer inc will be at -1)}

				if(pdirect->m_data.m_bEventsChanged)
				{
					pdirect->m_data.m_bEventsChanged = false;
					pdirect->m_data.ReleaseRecordSet();
				}


				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.

				if((pdirect->m_data.m_prsEvents == NULL)
					&&(pdirect->m_data.m_nEventCheckIndex<0)
					&&(pdirect->m_data.m_nEventLastMax<pdirect->m_data.m_nNumberOfEvents))
				{
					if((pAObj->m_usType&DIRECT_DEP_AUTO_MASK)==DIRECT_DEP_AUTO_HELIOS)
					{

/*
#define C_ERROR                                                0  // item contains errors
#define C_INITIALIZED                                    1  // item has just been initialized and cannot be used without further work
#define C_UNCHECKED                                        2  // item wasn't checked
#define C_UNALLOCATED                                    3  // item wasn't allocated
#define C_UNAVAILABLE                                    4  // item isn't available
#define C_ALLOCATED                                        5  // item allocated
#define C_CUEING                                            6  // item cueing
#define C_READY                                                7  // item ready for playout
#define C_COMMIT                                            8  // item ready to be committed to air
#define C_ON_AIR                                            9  // item is on air
#define C_HOLD                                                10 // item holding time after playing to air
#define C_DONE                                                11 // item has been played to air


*/

/// lets just get the reasonable ones. not done or errors or alreay playing (its too late by then)

						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d * FROM %s.dbo.%s WHERE event_status > 4 AND \
event_status < 9 AND type < 2 AND ( status < 6 OR status IS NULL ) ORDER BY event_start ASC, event_itemid ASC",
								max(1, pdirect->m_data.m_nEventLastMax+1),
								pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
								pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events"   // the LiveEvents table name
							);
					}
					else  // Harris uses default
					{
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d * FROM %s.dbo.%s WHERE \
(event_status & 1) = 0 \
AND type < 2 AND ( status < 6 OR status IS NULL ) ORDER BY event_start ASC, event_itemid ASC",
								max(1, pdirect->m_data.m_nEventLastMax+1),
								pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
								pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events"   // the LiveEvents table name
							);
//AND event_status <> 513 \
//AND event_status <> 257 \
//AND event_status <> 129 \

//0000000001  1     done
//1000000001  513   missed and done
//0100000001  257   short and done
//0010000001  129   skipped and done
// the keyword here, is done.  dont bother with done elements.
					}

				
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "initial SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
					pdirect->m_data.m_prsEvents = db.Retrieve(pdbConn, szSQL, errorstring);
					if(pdirect->m_data.m_prsEvents)
					{
						pdirect->m_data.m_nEventCheckIndex = 0;
					}
					else
					{
//pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "retrieve was null: %s", errorstring);  Sleep(50);//(Dispatch message)
					}
				}
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got events");  Sleep(50);//(Dispatch message)

		//   check transfer if xfer inc not -1.
				_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.

		//   check item for type.  if unassigned, apply rules. if no rules apply, skip, else

_ftime( &pdirect->m_data.m_timebAnalysisTick ); // the last time check inside the thread
			//get the next one.
				if((pdirect->m_data.m_prsEvents)&&(!pdirect->m_data.m_prsEvents->IsEOF()))
				{
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "checking the recordset");  Sleep(50);//(Dispatch message)
					if(pdirect->m_data.m_pCheckEvent==NULL)
					{
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "** ** ** check event was NULL, creating a new one");  Sleep(50);//(Dispatch message)
						pdirect->m_data.m_pCheckEvent = new CDirectEventObject;
					}
					if(pdirect->m_data.m_pCheckEvent==NULL)	
					{ continue; }  // have to, unfortunately.

					// set values for pdirect->m_data.m_pCheckEvent 
					int nType=-1;
					CString szValue;
					try
					{
// reset values
						pdirect->m_data.m_pCheckEvent->m_sz_n_itemid = "";
						pdirect->m_data.m_pCheckEvent->m_sz_n_channelid = "";
						pdirect->m_data.m_pCheckEvent->m_sz_dbl_event_start = "";
						pdirect->m_data.m_pCheckEvent->m_sz_n_event_itemid = "";
						pdirect->m_data.m_pCheckEvent->m_sz_vc32_event_id = "";
						pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_clip = "";
						pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title = "";
						pdirect->m_data.m_pCheckEvent->m_sz_n_event_status = "0";  // not yet checked value.
						pdirect->m_data.m_pCheckEvent->m_sz_vc4096_event_data = "";
						pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid = ""; 
						pdirect->m_data.m_pCheckEvent->m_sz_n_status = "";
						pdirect->m_data.m_pCheckEvent->m_sz_n_transfer_date = "";
						pdirect->m_data.m_pCheckEvent->m_sz_n_app_data_aux = "";
						pdirect->m_data.m_pCheckEvent->m_sz_n_type = "";
						pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host = ""; 
						pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files = "";
						pdirect->m_data.m_pCheckEvent->m_sz_n_file_index = "-1";
						pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbname = "";
						pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbqueue = "";
						pdirect->m_data.m_pCheckEvent->m_sz_n_rule_id="-1";

						pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch=-1; // index, -1 for file, index of children for subsequent.

						if(pdirect->m_data.m_pCheckEvent->m_ppszChildren)
						{
							int i=0;
							while(i<pdirect->m_data.m_pCheckEvent->m_nNumChildren)
							{
								if(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]) free(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]);
								i++;
							}
							delete [] pdirect->m_data.m_pCheckEvent->m_ppszChildren;
							pdirect->m_data.m_pCheckEvent->m_ppszChildren=NULL;
						}

						if(pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus)
						{
							delete [] pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus;
							pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus=NULL;
						}

						pdirect->m_data.m_pCheckEvent->m_nNumChildren=0;

						if(pdirect->m_data.m_pCheckEvent->m_pszMainFile)
						{
							free(pdirect->m_data.m_pCheckEvent->m_pszMainFile); 
							pdirect->m_data.m_pCheckEvent->m_pszMainFile = NULL;
						}
						pdirect->m_data.m_pCheckEvent->m_nMainFileStatus = -1;

						pdirect->m_data.m_prsEvents->GetFieldValue("itemid", pdirect->m_data.m_pCheckEvent->m_sz_n_itemid);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_n_itemid.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_n_itemid.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got itemid");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("channelid", pdirect->m_data.m_pCheckEvent->m_sz_n_channelid);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_n_channelid.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_n_channelid.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got channelid");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("event_start", pdirect->m_data.m_pCheckEvent->m_sz_dbl_event_start);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_dbl_event_start.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_dbl_event_start.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got event_start");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("event_itemid", pdirect->m_data.m_pCheckEvent->m_sz_n_event_itemid);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_n_event_itemid.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_n_event_itemid.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got event_itemid");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("event_id", pdirect->m_data.m_pCheckEvent->m_sz_vc32_event_id);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_vc32_event_id.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_vc32_event_id.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got event_id");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("event_clip", pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_clip);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_clip.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_clip.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got event_clip");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("event_title", pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got event_title");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("event_status", pdirect->m_data.m_pCheckEvent->m_sz_n_event_status);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_n_event_status.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_n_event_status.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got event_status");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("event_data", pdirect->m_data.m_pCheckEvent->m_sz_vc4096_event_data);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_vc4096_event_data.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_vc4096_event_data.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got event_data");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("filename", pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got filename");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("destinationid", pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got destinationid");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("status", pdirect->m_data.m_pCheckEvent->m_sz_n_status);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_n_status.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_n_status.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got status");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("transfer_date", pdirect->m_data.m_pCheckEvent->m_sz_n_transfer_date);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_n_transfer_date.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_n_transfer_date.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got transfer_date");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("app_data_aux", pdirect->m_data.m_pCheckEvent->m_sz_n_app_data_aux);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_n_app_data_aux.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_n_app_data_aux.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got app_data_aux");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("type", pdirect->m_data.m_pCheckEvent->m_sz_n_type);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_n_type.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_n_type.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got type %s", pdirect->m_data.m_pCheckEvent->m_sz_n_type);  Sleep(50);//(Dispatch message)
						nType = atoi(pdirect->m_data.m_pCheckEvent->m_sz_n_type);

						pdirect->m_data.m_prsEvents->GetFieldValue("dest_host", pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got dest_host");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("search_files", pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got search_files");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("file_index", pdirect->m_data.m_pCheckEvent->m_sz_n_file_index);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_n_file_index.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_n_file_index.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got file_index");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("module_dbname", pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbname);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbname.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbname.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got module_dbname");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("module_dbqueue", pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbqueue);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbqueue.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbqueue.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got module_dbqueue");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_prsEvents->GetFieldValue("rule_id", pdirect->m_data.m_pCheckEvent->m_sz_n_rule_id);//HARDCODE
						pdirect->m_data.m_pCheckEvent->m_sz_n_rule_id.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_n_rule_id.TrimRight();
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got rule_id");  Sleep(50);//(Dispatch message)

						pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch = atoi(pdirect->m_data.m_pCheckEvent->m_sz_n_file_index);

//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "about to parse search files");  Sleep(50);//(Dispatch message)
						if(nType==1) // a direct event
						{
							sprintf(errorstring,"%s", pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files);
							pdirect->m_data.ParseFiles(
								errorstring, 
								&pdirect->m_data.m_pCheckEvent->m_pszMainFile, 
								&pdirect->m_data.m_pCheckEvent->m_nMainFileStatus, 
								&pdirect->m_data.m_pCheckEvent->m_ppszChildren, 
								&pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus, 
								&pdirect->m_data.m_pCheckEvent->m_nNumChildren
								);
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "search files (status %s) [%s] returned %s and %d children", pdirect->m_data.m_pCheckEvent->m_sz_n_status, pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files, pdirect->m_data.m_pCheckEvent->m_pszMainFile, pdirect->m_data.m_pCheckEvent->m_nNumChildren);  Sleep(50);//(Dispatch message)
						}
					}
					catch( ... )
					{
					}

//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "the recordset has title  %s", pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title);  Sleep(50);//(Dispatch message)
					if(pdirect->m_data.m_pCheckEvent->m_sz_n_itemid.GetLength()>0)
					{ // we found something we can work with.
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "the recordset has type  %s (%d) status=%s", pdirect->m_data.m_pCheckEvent->m_sz_n_type, nType, pdirect->m_data.m_pCheckEvent->m_sz_n_status);  Sleep(50);//(Dispatch message)
				  	if(nType == 0) // we have not found this yet.
						{
							pdirect->m_data.ReleaseRecordSet();  // we know we are updating
							char pszTestData[DB_SQLSTRING_MAXLEN];
							memset(pszTestData, 0, DB_SQLSTRING_MAXLEN);  // init that buffer!
							// direct object.
							if((pAObj->m_usType&DIRECT_DEP_AUTO_MASK)==DIRECT_DEP_AUTO_HELIOS)
							{
								// not supported yet, have to use the data field
								// pdirect->m_data.m_pCheckEvent->m_sz_vc4096_event_data
								_snprintf(pszTestData, DB_SQLSTRING_MAXLEN-1, "%s", pdirect->m_data.m_pCheckEvent->m_sz_vc4096_event_data);
							}
							else
							if((pAObj->m_usType&DIRECT_DEP_AUTO_MASK)==DIRECT_DEP_AUTO_SENTINEL)
							{
								_snprintf(pszTestData, DB_SQLSTRING_MAXLEN-1, "%s", pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title);
							}
							// else other automation types.// its other automation we dont know yet.
if(pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES)	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "about to check rules on %s", pszTestData);  //Sleep(50);//(Dispatch message)
//DEMO made the following true always

							if(pdirect->m_data.CheckRules(pszTestData, (pAObj->m_usType&DIRECT_DEP_AUTO_MASK)) == DIRECT_SUCCESS)
							{
								//SQL
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET type = 1, file_index = -1, status = 0 WHERE itemid = %s",
									pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
									pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
									pdirect->m_data.m_pCheckEvent->m_sz_n_itemid
									);

								_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.
if(pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES)	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "setting intial type SQL: %s", szSQL);  //Sleep(50);//(Dispatch message)
		EnterCriticalSection(&pdirect->m_data.m_critSQL);
								if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
								{
								//**MSG
pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "ERROR executing SQL: %s", errorstring);


								}	
		LeaveCriticalSection(&pdirect->m_data.m_critSQL);

							} // if check rules.
							else // else no rules apply
							{
								// set the type to 2 and be done.
if(pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES)	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "checked rules, none apply"); // Sleep(50);//(Dispatch message)
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET type = 2 WHERE itemid = %s",
									pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
									pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
									pdirect->m_data.m_pCheckEvent->m_sz_n_itemid
									);

//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "checked rules for %s, none apply SQL: %s",pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title, szSQL);  Sleep(50);//(Dispatch message)
		EnterCriticalSection(&pdirect->m_data.m_critSQL);
								if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
								{
								//**MSG
pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "ERROR executing SQL: %s", errorstring);//  Sleep(250); //(Dispatch message)

								}				
		LeaveCriticalSection(&pdirect->m_data.m_critSQL);
							} // no rules applied
						}
						else
						if(nType == 1) // this is a direct event, lets check it out how it's doing..
						{
							// if its the transfer event, skip over it,
//							if(
//								  (pdirect->m_data.m_pTransferEvent==NULL)
//							  ||(pdirect->m_data.m_pCheckEvent->m_sz_n_itemid.Compare(pdirect->m_data.m_pTransferEvent->m_sz_n_itemid))
//								)
							{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "type 1 status = %s for %s", pdirect->m_data.m_pCheckEvent->m_sz_n_status,pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename);  Sleep(50);//(Dispatch message)
								// how we doing?
								//   check for the file on edge device, means:
								//     { find the right miranda, queue a check existence, escape with incrementor }

								int nStatus = atoi(pdirect->m_data.m_pCheckEvent->m_sz_n_status);
								switch(nStatus)
								{
								default:
								case -10://     grey     Destination did not exist, so rechecking existence - or channel not active
								case -6: //     orange   Error - rechecking
								case 0: //     none     not yet checked - initial.
									{
										//if search files is blank, we are at the absolute start, BUT we know some rules have applied.
										pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch = -1; // lets just be sure

										// first check channel active.
										bool bActive = false;
										unsigned long ulFlags =  pdirect->m_data.ReturnChannelFlags(pdirect->m_data.m_pCheckEvent);
										if(ulFlags!=DIRECT_FLAGS_INVALID)
										{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "found channel flags: 0x%08x", ulFlags);  Sleep(50);//(Dispatch message)
											if(ulFlags&DIRECT_FLAG_ENABLED)
											{
												bActive = true;
											}
											else
											{
											}
			//DEMO
//	pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 7, false, true); bActive = false;
										}
										else
										{// not active channel
											//**MSG possibly, but repetitive.
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "not active channel");  Sleep(50);//(Dispatch message)
											// make the following just be invisible. (-10, not -7) there is no point in displaying things for an inactive cahnnel, there wont be any events.
											pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -9, false, true);
//											pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -7, false, true);
										}

										if(bActive)
										{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "active channel");  Sleep(50);//(Dispatch messag
											// deal!
											char pszTestData[DB_SQLSTRING_MAXLEN];
											memset(pszTestData, 0, DB_SQLSTRING_MAXLEN);  // init that buffer!
											// direct object.
											if((pAObj->m_usType&DIRECT_DEP_AUTO_MASK)==DIRECT_DEP_AUTO_HELIOS)
											{
												// pdirect->m_data.m_pCheckEvent->m_sz_vc4096_event_data
												_snprintf(pszTestData, DB_SQLSTRING_MAXLEN-1, "%s", pdirect->m_data.m_pCheckEvent->m_sz_vc4096_event_data);
											}
											else
											if((pAObj->m_usType&DIRECT_DEP_AUTO_MASK)==DIRECT_DEP_AUTO_SENTINEL)
											{
												// had to remove following, overwrite later in rules, when I know what rule type.
/*
												if(g_pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulType == DIRECT_RULE_TYPE_IMAGESTORE)
												{
													_snprintf(pszTestData, DB_SQLSTRING_MAXLEN-1, "%s", pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title);
												}
												else if(g_pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulType == DIRECT_RULE_TYPE_FTP)
												{
												_snprintf(pszTestData, DB_SQLSTRING_MAXLEN-1, "%s|%s", pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_clip, pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title);
												}
											*/	
												//default
													_snprintf(pszTestData, DB_SQLSTRING_MAXLEN-1, "%s", pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title);

											}
											bool bUpdateDone = false;
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "checked rules, some apply");  Sleep(50);//(Dispatch message)

											int nRulesApplied = 0;
											int nRuleIndex = 0;

											while(nRuleIndex < pdirect->m_data.m_nNumRulesObjects)
											{
												_ftime(&pdirect->m_data.m_timebTick);
												char pszDefaultExtension[32]; 
												char* pszBaseFilename = NULL;
												char* pszExplicitExtension = NULL;
												char* pszEncodedBaseFilename = NULL;
												char* pszEncodedExtension = NULL;
												char*	pszEncoded_event_id = NULL;
												char* pszEncoded_event_clip = NULL;
												char* pszEncoded_event_title = NULL;
												char* pszEncoded_event_data = NULL;
												memset(pszDefaultExtension, 0, 32);

												_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.

												if(pdirect->m_data.m_ppRulesObj[nRuleIndex])
												{

													if((pAObj->m_usType&DIRECT_DEP_AUTO_MASK)==DIRECT_DEP_AUTO_SENTINEL)
													{
														if(g_pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulType == DIRECT_RULE_TYPE_FTP)
														{
															_snprintf(pszTestData, DB_SQLSTRING_MAXLEN-1, "%s|%s", pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_clip, pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title);
														}
																		
													}

													if(
															(
																((pAObj->m_usType&DIRECT_DEP_AUTO_MASK) == DIRECT_DEP_AUTO_SENTINEL)
															&&
																(
																	(g_pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulType == DIRECT_RULE_TYPE_IMAGESTORE)
																||(g_pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulType == DIRECT_RULE_TYPE_FTP)
																)
															)
														||(
																((pAObj->m_usType&DIRECT_DEP_AUTO_MASK) == DIRECT_DEP_AUTO_HELIOS)
															&&(
																	(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulType == DIRECT_RULE_TYPE_PRESMASTER)
																||(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulType == DIRECT_RULE_TYPE_XML)
																)
															)
														)
													{
//	 pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "*applying rule to %s", pszTestData);// #%d (%d) to %s", nRuleIndex, pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_nRuleID, pTitle);  Sleep(50);//(Dispatch message)
				
														if(pdirect->m_data.ApplyRule(nRuleIndex, pszTestData, &pszBaseFilename, &pszExplicitExtension)>=DIRECT_SUCCESS) // currently only succeeds for direct types
														{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "rule #%d (%d) applies to %s: %s, ext [%s]", nRuleIndex, pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_nRuleID, pszTitle, pszBaseFilename, pszExplicitExtension);  Sleep(50);//(Dispatch message)
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "************ %s => file [%s], ext [%s]", pszTestData, pszBaseFilename, pszExplicitExtension);  Sleep(50);//(Dispatch message)

															if((pszBaseFilename)&&(strlen(pszBaseFilename)))
															{
																// cant do the following line here, in case rule applies to more than one module. - incremement in endpoint loop
																//nRulesApplied++;

																pdirect->m_data.m_pCheckEvent->m_sz_n_rule_id.Format("%d", pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_nRuleID);

																pszEncodedBaseFilename = db.EncodeQuotes(pszBaseFilename);
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "****  ****** %s",pszEncodedBaseFilename);  Sleep(50);//(Dispatch message)
																pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename.Format("%s", pszEncodedBaseFilename?pszEncodedBaseFilename:pszBaseFilename);
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "************ %s",pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename); // Sleep(50);//(Dispatch message)

																// find out what kind of extension search we are doing.
																if((pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_usSearchType==DIRECT_RULE_SEARCH_EXPLICIT)&&(pszExplicitExtension)&&(strlen(pszExplicitExtension)))
																{
																	strcpy(pszDefaultExtension, pszExplicitExtension);
																	pszEncodedExtension = db.EncodeQuotes(pszDefaultExtension);
																}
																else
																{
																	// determine by action and search type.
																	if(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_usActionType == DIRECT_RULE_ACTION_NULLOXT)
																	{
																		CSafeBufferUtil sbu;
																		// search for sound files first.  start with imagestore formats first.
																		switch(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulDestinationType)
																		{
																		default: strcpy(pszDefaultExtension, ""); break;
																		case DIRECT_RULE_DESTTYPE_MIRANDA_IS2: //Imagestore2
																			{
																				if(pdirect->m_settings.m_pszImagestore2SearchExt)
																				{
																					char* pchSearchExt = sbu.Token(pdirect->m_settings.m_pszImagestore2SearchExt, strlen(pdirect->m_settings.m_pszImagestore2SearchExt), ",;");
																					while(pchSearchExt)
																					{ // cycles to the last one.
																						strcpy(pszDefaultExtension, pchSearchExt); 
																						pchSearchExt = sbu.Token(NULL, NULL, ",;");
																					}
																					if(strlen(pszDefaultExtension)<=0) strcpy(pszDefaultExtension, "oxw");
																				}
																				else strcpy(pszDefaultExtension, "oxw"); 
																			} break;  //Imagestore2
																		case DIRECT_RULE_DESTTYPE_MIRANDA_INT: // Intuition
																			{ 
																				// no audio
																				strcpy(pszDefaultExtension, "");
																			} break; // Intuition
																		case DIRECT_RULE_DESTTYPE_MIRANDA_IS300:// Imagestore 300
																			{
																				if(pdirect->m_settings.m_pszImagestore300SearchExt)
																				{
																					char* pchSearchExt = sbu.Token(pdirect->m_settings.m_pszImagestore300SearchExt, strlen(pdirect->m_settings.m_pszImagestore300SearchExt), ",;");
																					while(pchSearchExt)
																					{ // cycles to the last one.
																						strcpy(pszDefaultExtension, pchSearchExt); 
																						pchSearchExt = sbu.Token(NULL, NULL, ",;");
																					}
																					if(strlen(pszDefaultExtension)<=0) strcpy(pszDefaultExtension, "oxe");
																				}
																				else strcpy(pszDefaultExtension, "oxe"); 
																			} break; // Imagestore 300
																		case DIRECT_RULE_DESTTYPE_MIRANDA_ISHD: // Imagestore HD - imagestore with filename conversion
																			{ 
																				if(pdirect->m_settings.m_pszImagestoreHDSearchExt)
																				{
																					char* pchSearchExt = sbu.Token(pdirect->m_settings.m_pszImagestoreHDSearchExt, strlen(pdirect->m_settings.m_pszImagestoreHDSearchExt), ",;");
																					while(pchSearchExt)
																					{ // cycles to the last one.
																						strcpy(pszDefaultExtension, pchSearchExt); 
																						pchSearchExt = sbu.Token(NULL, NULL, ",;");
																					}
																					if(strlen(pszDefaultExtension)<=0) strcpy(pszDefaultExtension, "oxe");
																				}
																				else strcpy(pszDefaultExtension, "oxe"); 
																			} break;  // Imagestore HD
																		case DIRECT_RULE_DESTTYPE_MIRANDA_INTHD: // Intuition HD - intuition with filename conversion
																			{ 
																				if(pdirect->m_settings.m_pszIntuitionHDSearchExt)
																				{
																					char* pchSearchExt = sbu.Token(pdirect->m_settings.m_pszIntuitionHDSearchExt, strlen(pdirect->m_settings.m_pszIntuitionHDSearchExt), ",;");
																					while(pchSearchExt)
																					{ // cycles to the last one.
																						strcpy(pszDefaultExtension, pchSearchExt); 
																						pchSearchExt = sbu.Token(NULL, NULL, ",;");
																					}
																					if(strlen(pszDefaultExtension)<=0) strcpy(pszDefaultExtension, "wav");
																				}
																				else strcpy(pszDefaultExtension, "wav"); 
																			} break;  // Imagestore HD
																		}
																	}
																	else  // we assume normal.
																	{
																		CSafeBufferUtil sbu;

																		switch(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulDestinationType)
																		{
																		default: strcpy(pszDefaultExtension, ""); break;
																		case DIRECT_RULE_DESTTYPE_MIRANDA_INT:
																			{
																				if(pdirect->m_settings.m_pszIntuitionSearchExt)
																				{
																					char* pchSearchExt = sbu.Token(pdirect->m_settings.m_pszIntuitionSearchExt, strlen(pdirect->m_settings.m_pszIntuitionSearchExt), ",;");
																					if(pchSearchExt)
																					{
																						strcpy(pszDefaultExtension, pchSearchExt); 
																					}
																					else strcpy(pszDefaultExtension, "tem"); 
																				}
																				else strcpy(pszDefaultExtension, "tem"); 
																			} break; // Intuition
																		case DIRECT_RULE_DESTTYPE_MIRANDA_IS300:	 // Imagestore 300
																			{
																				if(pdirect->m_settings.m_pszImagestore300SearchExt)
																				{
																					char* pchSearchExt = sbu.Token(pdirect->m_settings.m_pszImagestore300SearchExt, strlen(pdirect->m_settings.m_pszImagestore300SearchExt), ",;");
																					if(pchSearchExt)
																					{
																						strcpy(pszDefaultExtension, pchSearchExt); 
																					}
																					else strcpy(pszDefaultExtension, "oxa"); 
																				}
																				else strcpy(pszDefaultExtension, "oxa"); 
																			} break; // Intuition
																		case DIRECT_RULE_DESTTYPE_MIRANDA_ISHD:		 // Imagestore HD
																			{
																				if(pdirect->m_settings.m_pszImagestoreHDSearchExt)
																				{
																					char* pchSearchExt = sbu.Token(pdirect->m_settings.m_pszImagestoreHDSearchExt, strlen(pdirect->m_settings.m_pszImagestoreHDSearchExt), ",;");
																					if(pchSearchExt)
																					{
																						strcpy(pszDefaultExtension, pchSearchExt); 
																					}
																					else strcpy(pszDefaultExtension, "oxa"); 
																				}
																				else strcpy(pszDefaultExtension, "oxa"); 
																			} break; // Intuition
																		case DIRECT_RULE_DESTTYPE_MIRANDA_IS2:		//Imagestore2
																			{
																				if(pdirect->m_settings.m_pszImagestore2SearchExt)
																				{
																					char* pchSearchExt = sbu.Token(pdirect->m_settings.m_pszImagestore2SearchExt, strlen(pdirect->m_settings.m_pszImagestore2SearchExt), ",;");
																					if(pchSearchExt)
																					{
																						strcpy(pszDefaultExtension, pchSearchExt); 
																					}
																					else strcpy(pszDefaultExtension, "oxa"); 
																				}
																				else strcpy(pszDefaultExtension, "oxa"); 
																			} break; // Intuition
																		case DIRECT_RULE_DESTTYPE_MIRANDA_INTHD: // Intuition HD - intuition with filename conversion
																			{ 
																				if(pdirect->m_settings.m_pszIntuitionHDSearchExt)
																				{
																					char* pchSearchExt = sbu.Token(pdirect->m_settings.m_pszIntuitionHDSearchExt, strlen(pdirect->m_settings.m_pszIntuitionHDSearchExt), ",;");
																					if(pchSearchExt)
																					{
																						strcpy(pszDefaultExtension, pchSearchExt); 
																					}
																					else strcpy(pszDefaultExtension, "tem"); 
																				}
																				else strcpy(pszDefaultExtension, "tem"); 
																			} break;  // Imagestore HD
																		case DIRECT_RULE_DESTTYPE_EVERTZ_9625LG	://		4000 
																		case DIRECT_RULE_DESTTYPE_EVERTZ_9625LGA://			4001
																		case DIRECT_RULE_DESTTYPE_EVERTZ_9725LGA://			4002
																		case DIRECT_RULE_DESTTYPE_EVERTZ_9725LG	://		4003
																		case DIRECT_RULE_DESTTYPE_EVERTZ_HD9625LG://		4004
																		case DIRECT_RULE_DESTTYPE_EVERTZ_HD9625LGA://		4005
																		case DIRECT_RULE_DESTTYPE_EVERTZ_HD9725LG://		4006
																		case DIRECT_RULE_DESTTYPE_EVERTZ_HD9725LGA://		4007
																			{
																			} break;
																		}
																	}


																	pszEncodedExtension = db.EncodeQuotes(pszDefaultExtension);
																}

																// need to get host, dest id, as well as partition(s)
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "get partitions");//  Sleep(50);//(Dispatch message)

																// have to find which module the channel id is in.

																int nDestModule=0;
																while(nDestModule<pdirect->m_settings.m_nNumEndpointsInstalled)
																{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%d < %d", nDestModule,pdirect->m_settings.m_nNumEndpointsInstalled );   Sleep(50);//(Dispatch message)
																	if((pdirect->m_settings.m_ppEndpointObject)&&(pdirect->m_settings.m_ppEndpointObject[nDestModule]))
																	{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "double not null" ); //  Sleep(50);//(Dispatch message)

																		if(( pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType&DIRECT_DEP_EDGE_MASK)==DIRECT_DEP_EDGE_PROSPERO)
																			// only support direct types
																		{

																			pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbname = pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName;
																			pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbqueue = pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszQueue;

																			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT host, destinationid, (SELECT partition FROM \
%s.dbo.%s \
WHERE criterion = '%s') AS partition FROM \
%s.dbo.%s WHERE channelid = %s AND type = %d AND flags = 1", // active
																				pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName:"Propsero", 
																				pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszFileTypes?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszFileTypes:"FileTypeMapping",  // edge device DB and table name;
																				pszEncodedExtension,
																				pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName:"Propsero", 
																				pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination:"Destinations",  // edge device DB and table name;
																				pdirect->m_data.m_pCheckEvent->m_sz_n_channelid?pdirect->m_data.m_pCheckEvent->m_sz_n_channelid:"0",
																				pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulDestinationType // the right type

																				);
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", szSQL );   Sleep(50);//(Dispatch message)

																			//pdirect->m_data.ReleaseRecordSet(); 
																			EnterCriticalSection(&g_pdirect->m_data.m_critSQL);
																			CRecordset* prs = pdirect->m_data.m_pdb->Retrieve(pdirect->m_data.m_pdbConn, szSQL, errorstring);
																			if((prs==NULL)||(prs->IsEOF()))
																			{
																				if(prs)
																				{
																					prs->Close();
																					delete prs;
																					prs = NULL;
																				}
																				// skip, it wasn't this module
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", errorstring );   Sleep(50);//(Dispatch message)
																				LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);
																			}
																			else
																			{
																				// success, lets get the info 
																				//  QUESTION: do we want to break out of the loop, if host is found.
																				// OR, do we want to continue looping in case there is another edge device on another module tha passes the same rule.
																				// for now we let it loop thru.

																				prs->GetFieldValue("host", pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host);//HARDCODE
																				pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host.TrimRight();

																				if(pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host.GetLength()>0)
																				{
																					prs->GetFieldValue("destinationid", pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid);//HARDCODE
																					pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid.TrimRight();

																					CString szPartition;
																					prs->GetFieldValue("partition", szPartition);//HARDCODE

//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "host %s, destid %s, partition %s", pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host, pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid, szPartition );   Sleep(50);//(Dispatch message)

																					prs->Close();
																					delete prs;
																					prs = NULL;
																					LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);

															
																					pszEncoded_event_id = db.EncodeQuotes(pdirect->m_data.m_pCheckEvent->m_sz_vc32_event_id);
																					pszEncoded_event_clip = db.EncodeQuotes(pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_clip);
																					pszEncoded_event_title = db.EncodeQuotes(pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title);
																					pszEncoded_event_data = db.EncodeQuotes(pdirect->m_data.m_pCheckEvent->m_sz_vc4096_event_data);

																					if((pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_usSearchType==DIRECT_RULE_SEARCH_EXPLICIT)&&(pszEncodedExtension)&&(strlen(pszEncodedExtension)))
																					{
																						pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files.Format("0%s/%s",szPartition, pszEncodedBaseFilename);
																					}
																					else
																					{
																						pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files.Format("0%s/%s.%s",szPartition, pszEncodedBaseFilename, pszEncodedExtension);
																					}

																					// THIS IS THE FIRST TIME AROUND (OR POSSIBLY AGAIN), 
																					// so we want to make sure there is only the main, file, all children are redetermined later on  in case 5
																					if(pdirect->m_data.m_pCheckEvent->m_ppszChildren)
																					{
																						int i=0;
																						while(i<pdirect->m_data.m_pCheckEvent->m_nNumChildren)
																						{
																							if(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]) free(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]);
																							i++;
																						}
																						delete [] pdirect->m_data.m_pCheckEvent->m_ppszChildren;
																						pdirect->m_data.m_pCheckEvent->m_ppszChildren = NULL;
																					}
																					if(pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus) delete [] pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus;
																					pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus = NULL;
																					if(pdirect->m_data.m_pCheckEvent->m_pszMainFile) free(pdirect->m_data.m_pCheckEvent->m_pszMainFile);
																					pdirect->m_data.m_pCheckEvent->m_pszMainFile = NULL;
																					pdirect->m_data.m_pCheckEvent->m_nNumChildren = 0;

																					sprintf(errorstring,"%s", pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files);
																					pdirect->m_data.ParseFiles(
																						errorstring, 
																						&pdirect->m_data.m_pCheckEvent->m_pszMainFile, 
																						&pdirect->m_data.m_pCheckEvent->m_nMainFileStatus, 
																						&pdirect->m_data.m_pCheckEvent->m_ppszChildren, 
																						&pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus, 
																						&pdirect->m_data.m_pCheckEvent->m_nNumChildren
																						);

																					
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "****   main file   *****: %s\n****   search files: %s", pdirect->m_data.m_pCheckEvent->m_pszMainFile, pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files);  Sleep(50); //(Dispatch message)

																					if(pdirect->m_data.m_pCheckEvent->m_ppszChildren)
																					{
																						int i=0;
																						while(i<pdirect->m_data.m_pCheckEvent->m_nNumChildren)
																						{
																							if(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]) free(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]);
																							i++;
																						}
																						delete [] pdirect->m_data.m_pCheckEvent->m_ppszChildren;
																					}
																					if(pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus) 
																						delete [] pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus;
																					pdirect->m_data.m_pCheckEvent->m_ppszChildren = NULL;
																					pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus = NULL;
																					pdirect->m_data.m_pCheckEvent->m_nNumChildren = 0;

																					pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 0);

																					nRulesApplied++;
																					if((nRulesApplied>1)&&(nStatus==0)) // have to insert, but only if this is not a recheck on error.
																					{
																						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(channelid, event_start, event_itemid, event_id, event_clip, event_title, event_status, event_data, filename, \
destinationid, status, transfer_date, app_data_aux, type, dest_host, search_files, file_index, module_dbname, module_dbqueue, rule_id) VALUES \
(%s, %s, %s, '%s', '%s', '%s', %s, '%s', '%s', %s, 0, 0, 0, 1, '%s', '%s', -1, '%s', '%s', %s)",
																							pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
																							pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
																							pdirect->m_data.m_pCheckEvent->m_sz_n_channelid,
																							pdirect->m_data.m_pCheckEvent->m_sz_dbl_event_start,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_event_itemid,
																							pszEncoded_event_id?pszEncoded_event_id:pdirect->m_data.m_pCheckEvent->m_sz_vc32_event_id,
																							pszEncoded_event_clip?pszEncoded_event_clip:pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_clip,
																							pszEncoded_event_title?pszEncoded_event_title:pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_event_status,
																							pszEncoded_event_data?pszEncoded_event_data:pdirect->m_data.m_pCheckEvent->m_sz_vc4096_event_data,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbname,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbqueue,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_rule_id
																							);


																					}
																					else // just update existing.
																					{
																						int nAuxData = 	atol(pdirect->m_data.m_pCheckEvent->m_sz_n_app_data_aux);

																						//SQL
																						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET \
filename = '%s', \
destinationid = %s, \
status = %s, \
transfer_date = 0, app_data_aux = %d, type = 1, \
dest_host = '%s', \
search_files = '%s', \
file_index = -1, \
module_dbname = '%s', \
module_dbqueue = '%s', \
rule_id = %s \
WHERE itemid = %s",
																							pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
																							pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
																							pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_status,
																							(nStatus==0)?0:nAuxData,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbname,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbqueue,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_rule_id,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_itemid

																							);

																					}


																					if(pszEncoded_event_id) free(pszEncoded_event_id);       pszEncoded_event_id=NULL;
																					if(pszEncoded_event_clip) free(pszEncoded_event_clip);   pszEncoded_event_clip=NULL;
																					if(pszEncoded_event_title) free(pszEncoded_event_title); pszEncoded_event_title=NULL;
																					if(pszEncoded_event_data) free(pszEncoded_event_data);   pszEncoded_event_data=NULL;


//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "status 1 SQL: %s", szSQL );   Sleep(50);//(Dispatch message)

								_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.
		EnterCriticalSection(&pdirect->m_data.m_critSQL);
																					if(pdirect->m_data.m_pdb->ExecuteSQL(pdirect->m_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
																					{
																					//**MSG
pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "ERROR executing SQL: %s", errorstring);//  Sleep(250); //(Dispatch message)
																					}	
																					else	bUpdateDone=true;
		LeaveCriticalSection(&pdirect->m_data.m_critSQL);
								_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.
																				} // else not a valid host, skip.
																				else
																				{
																					prs->Close();
																					delete prs;
																					prs = NULL;

																					LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);
																				}
																			}
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "after SQL: %s", szSQL );   Sleep(50);//(Dispatch message)
																		}//	if(( pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType&DIRECT_DEP_EDGE_MASK)==DIRECT_DEP_EDGE_PROSPERO) // only support direct types
																		// else if other types

																		else
																		if(( pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType&DIRECT_DEP_EDGE_MASK)==DIRECT_DEP_EDGE_BARBERO)
																			// only support direct types
																		{

																			pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbname = pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName;
																			pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbqueue = pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszQueue;

																			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT host, destinationid, (SELECT partition FROM \
%s.dbo.%s \
WHERE criterion = '%s') AS partition FROM \
%s.dbo.%s WHERE channelid = %s AND type = %d AND flags = 1", // active
																				pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName:"Barbero", 
																				pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszFileTypes?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszFileTypes:"FileTypeMapping",  // edge device DB and table name;
																				pszEncodedExtension,
																				pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName:"Barbero", 
																				pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination?pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination:"Destinations",  // edge device DB and table name;
																				pdirect->m_data.m_pCheckEvent->m_sz_n_channelid?pdirect->m_data.m_pCheckEvent->m_sz_n_channelid:"0",
																				pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulDestinationType // the right type

																				);
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", szSQL ); //  Sleep(50);//(Dispatch message)

																			//pdirect->m_data.ReleaseRecordSet(); 
																			EnterCriticalSection(&g_pdirect->m_data.m_critSQL);
																			CRecordset* prs = pdirect->m_data.m_pdb->Retrieve(pdirect->m_data.m_pdbConn, szSQL, errorstring);
																			if((prs==NULL)||(prs->IsEOF()))
																			{
																				if(prs)
																				{
																					prs->Close();
																					delete prs;
																					prs = NULL;
																				}
																				// skip, it wasn't this module
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%s", errorstring );  // Sleep(50);//(Dispatch message)
																				LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);
																			}
																			else
																			{
																				// success, lets get the info 
																				//  QUESTION: do we want to break out of the loop, if host is found.
																				// OR, do we want to continue looping in case there is another edge device on another module tha passes the same rule.
																				// for now we let it loop thru.

																				prs->GetFieldValue("host", pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host);//HARDCODE
																				pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host.TrimRight();

																				if(pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host.GetLength()>0)
																				{
																					prs->GetFieldValue("destinationid", pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid);//HARDCODE
																					pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid.TrimLeft(); pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid.TrimRight();

																					CString szPartition;
																					prs->GetFieldValue("partition", szPartition);//HARDCODE

//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "host %s, destid %s, partition %s", pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host, pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid, szPartition );  // Sleep(50);//(Dispatch message)

																					prs->Close();
																					delete prs;
																					prs = NULL;
																					LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);

															
																					pszEncoded_event_id = db.EncodeQuotes(pdirect->m_data.m_pCheckEvent->m_sz_vc32_event_id);
																					pszEncoded_event_clip = db.EncodeQuotes(pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_clip);
																					pszEncoded_event_title = db.EncodeQuotes(pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title);
																					pszEncoded_event_data = db.EncodeQuotes(pdirect->m_data.m_pCheckEvent->m_sz_vc4096_event_data);

																					if((pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_usSearchType==DIRECT_RULE_SEARCH_EXPLICIT)&&(pszEncodedExtension)&&(strlen(pszEncodedExtension)))
																					{
																						pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files.Format("0%s/%s",szPartition, pszEncodedBaseFilename);
																					}
																					else
																					{
																						pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files.Format("0%s/%s.%s",szPartition, pszEncodedBaseFilename, pszEncodedExtension);
																					}

																					// THIS IS THE FIRST TIME AROUND (OR POSSIBLY AGAIN), 
																					// so we want to make sure there is only the main, file, all children are redetermined later on  in case 5
																					if(pdirect->m_data.m_pCheckEvent->m_ppszChildren)
																					{
																						int i=0;
																						while(i<pdirect->m_data.m_pCheckEvent->m_nNumChildren)
																						{
																							if(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]) free(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]);
																							i++;
																						}
																						delete [] pdirect->m_data.m_pCheckEvent->m_ppszChildren;
																						pdirect->m_data.m_pCheckEvent->m_ppszChildren = NULL;
																					}
																					if(pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus) delete [] pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus;
																					pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus = NULL;
																					if(pdirect->m_data.m_pCheckEvent->m_pszMainFile) free(pdirect->m_data.m_pCheckEvent->m_pszMainFile);
																					pdirect->m_data.m_pCheckEvent->m_pszMainFile = NULL;
																					pdirect->m_data.m_pCheckEvent->m_nNumChildren = 0;

																					sprintf(errorstring,"%s", pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files);
																					pdirect->m_data.ParseFiles(
																						errorstring, 
																						&pdirect->m_data.m_pCheckEvent->m_pszMainFile, 
																						&pdirect->m_data.m_pCheckEvent->m_nMainFileStatus, 
																						&pdirect->m_data.m_pCheckEvent->m_ppszChildren, 
																						&pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus, 
																						&pdirect->m_data.m_pCheckEvent->m_nNumChildren
																						);

																					
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "****   main file   *****: %s\n****   search files: %s", pdirect->m_data.m_pCheckEvent->m_pszMainFile, pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files);  Sleep(50); //(Dispatch message)

																					if(pdirect->m_data.m_pCheckEvent->m_ppszChildren)
																					{
																						int i=0;
																						while(i<pdirect->m_data.m_pCheckEvent->m_nNumChildren)
																						{
																							if(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]) free(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]);
																							i++;
																						}
																						delete [] pdirect->m_data.m_pCheckEvent->m_ppszChildren;
																					}
																					if(pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus) 
																						delete [] pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus;
																					pdirect->m_data.m_pCheckEvent->m_ppszChildren = NULL;
																					pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus = NULL;
																					pdirect->m_data.m_pCheckEvent->m_nNumChildren = 0;

																					pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 0);

																					nRulesApplied++;
																					if((nRulesApplied>1)&&(nStatus==0)) // have to insert, but only if this is not a recheck on error.
																					{
																						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(channelid, event_start, event_itemid, event_id, event_clip, event_title, event_status, event_data, filename, \
destinationid, status, transfer_date, app_data_aux, type, dest_host, search_files, file_index, module_dbname, module_dbqueue, rule_id) VALUES \
(%s, %s, %s, '%s', '%s', '%s', %s, '%s', '%s', %s, 0, 0, 0, 1, '%s', '%s', -1, '%s', '%s', %s)",
																							pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
																							pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
																							pdirect->m_data.m_pCheckEvent->m_sz_n_channelid,
																							pdirect->m_data.m_pCheckEvent->m_sz_dbl_event_start,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_event_itemid,
																							pszEncoded_event_id?pszEncoded_event_id:pdirect->m_data.m_pCheckEvent->m_sz_vc32_event_id,
																							pszEncoded_event_clip?pszEncoded_event_clip:pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_clip,
																							pszEncoded_event_title?pszEncoded_event_title:pdirect->m_data.m_pCheckEvent->m_sz_vc64_event_title,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_event_status,
																							pszEncoded_event_data?pszEncoded_event_data:pdirect->m_data.m_pCheckEvent->m_sz_vc4096_event_data,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbname,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbqueue,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_rule_id
																							);


																					}
																					else // just update existing.
																					{
																						int nAuxData = 	atol(pdirect->m_data.m_pCheckEvent->m_sz_n_app_data_aux);

																						//SQL
																						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET \
filename = '%s', \
destinationid = %s, \
status = %s, \
transfer_date = 0, app_data_aux = %d, type = 1, \
dest_host = '%s', \
search_files = '%s', \
file_index = -1, \
module_dbname = '%s', \
module_dbqueue = '%s', \
rule_id = %s \
WHERE itemid = %s",
																							pdirect->m_settings.m_pszDefaultDB?pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
																							pdirect->m_settings.m_pszLiveEvents?pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
																							pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_destinationid,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_status,
																							(nStatus==0)?0:nAuxData,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc64_dest_host,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc1024_search_files,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbname,
																							pdirect->m_data.m_pCheckEvent->m_sz_vc64_module_dbqueue,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_rule_id,
																							pdirect->m_data.m_pCheckEvent->m_sz_n_itemid

																							);

																					}


																					if(pszEncoded_event_id) free(pszEncoded_event_id);       pszEncoded_event_id=NULL;
																					if(pszEncoded_event_clip) free(pszEncoded_event_clip);   pszEncoded_event_clip=NULL;
																					if(pszEncoded_event_title) free(pszEncoded_event_title); pszEncoded_event_title=NULL;
																					if(pszEncoded_event_data) free(pszEncoded_event_data);   pszEncoded_event_data=NULL;


//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "status 1 SQL: %s", szSQL );   Sleep(50);//(Dispatch message)

								_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.
		EnterCriticalSection(&pdirect->m_data.m_critSQL);
																					if(pdirect->m_data.m_pdb->ExecuteSQL(pdirect->m_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
																					{
																					//**MSG
pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)
																					}	
																					else	bUpdateDone=true;
		LeaveCriticalSection(&pdirect->m_data.m_critSQL);
								_ftime( &pdirect->m_data.m_timebTick );  // we're still alive.
																				} // else not a valid host, skip.
																				else
																				{
																					prs->Close();
																					delete prs;
																					prs = NULL;

																					LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);
																				}
																			}
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "after SQL: %s", szSQL );   Sleep(50);//(Dispatch message)
																		}//	if(( pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType&DIRECT_DEP_EDGE_MASK)==DIRECT_DEP_EDGE_BARBERO) // only support direct types
																		// else if other types
																	}
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "incrementing: %d", nDestModule );   Sleep(50);//(Dispatch message)
																	nDestModule++;
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "incremented: %d", nDestModule );   Sleep(50);//(Dispatch message)

																}  // while dest modules
															}  // legal filename

															// the filename and extension are only allocated if rule applies
															if(pszBaseFilename) free(pszBaseFilename);
															pszBaseFilename = NULL;

															if(pszExplicitExtension) free(pszExplicitExtension);
															pszExplicitExtension = NULL;
														}// rule applied
													} // if it was a rule appropriate for the automation system
												}//if(pdirect->m_data.m_ppRulesObj[nRuleIndex])

												if(pszBaseFilename) free(pszBaseFilename);
												if(pszExplicitExtension) free(pszExplicitExtension);
												if(pszEncodedBaseFilename) free(pszEncodedBaseFilename);
												if(pszEncodedExtension) free(pszEncodedExtension);
												if(pszEncoded_event_id) free(pszEncoded_event_id);
												if(pszEncoded_event_clip) free(pszEncoded_event_clip);
												if(pszEncoded_event_title) free(pszEncoded_event_title);
												if(pszEncoded_event_data) free(pszEncoded_event_data);

												nRuleIndex++;
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "incremented to %d", nRuleIndex );  Sleep(50);//(Dispatch message)

											} // while rules

											//if we got here and didnt do an update, it's because we don't have an active channel or something.
											if(!bUpdateDone)
											{
												// have to set a status.
												// could not find any destinations mapped to this channel
												pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -9, false, true); // was -8, made it -9 to make it invisible.
												bActive = false;
												// this means we do not have partition info.
											}
										}  // this was, if it was active channel


										// determine what file to start checking with (if any), then update with 1
										// schedule a file check in direct
											

										if(bActive)
										{
											bool bUpdateDone = false;
											ulFlags =  pdirect->m_data.ReturnDestinationFlags(pdirect->m_data.m_pCheckEvent);
											if(ulFlags!=DIRECT_FLAGS_INVALID)
											{
												if(ulFlags&DIRECT_FLAG_ENABLED)
												{
													bActive = true;
												}
											}
											else
											{
												//**MSG possibly, but repetitive.
												pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -9, true, true);
												bUpdateDone = true;
											}

											if(!bUpdateDone)
											{
												if(bActive)
												{
													pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 1);
													pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 1, true, true);
													// put the request into the Queue!
	//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "Queueing: %s", pdirect->m_data.m_pCheckEvent->m_pszMainFile);  Sleep(50); //(Dispatch message)
													pdirect->m_data.ScheduleEndpointQueue(pdirect->m_data.m_pCheckEvent, "", pdirect->m_data.m_pCheckEvent->m_pszMainFile, 64);
												}
												else  // not active destination
												{
													pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -8, true, true);
												}
											}
										}
									} break;
								case -9://     grey     Destination does not exist, or channel not active
									{
										// see if the transfer date is timed out yet, if so, set to -6
										_ftime(&pdirect->m_data.m_timebTick);
//										if(pdirect->m_data.m_pTransferEvent==NULL)
										{
											int nTime = atoi(pdirect->m_data.m_pCheckEvent->m_sz_n_transfer_date);
											if((nTime>0)&&(pdirect->m_data.m_timebTick.time > nTime+pdirect->m_settings.m_nTransferFailureRetryTime))
											{
//												pdirect->m_data.ReleaseRecordSet();
												pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -10);
											}
										}
									} break;
								case -1: //    red      Communications error - could not query destination
								case -2: //    red      Communications error - could not transfer file to destination
								case -3: //    red      Media error - file not found at destination
								case -4: //    red      Media error - file not found at destination or source
								case -5: //    red      Disk error - could not create disk space on destination device
								case -7: //    grey     Channel not active
								case -8://     grey     Destination not active
									{
										// see if the transfer date is timed out yet, if so, set to -6
										_ftime(&pdirect->m_data.m_timebTick);
//										if(pdirect->m_data.m_pTransferEvent==NULL)
										{
											int nTime = atoi(pdirect->m_data.m_pCheckEvent->m_sz_n_transfer_date);
											if((nTime>0)&&(pdirect->m_data.m_timebTick.time > nTime+pdirect->m_settings.m_nTransferFailureRetryTime))
											{
//												pdirect->m_data.ReleaseRecordSet();
												pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -6);
											}
										}
									} break;
								case 1:  //    yellow   Checking for file(s)
									{
										// check the check in pdirect->
										// if it is there, set to 4, update.

										CDirectQueueObject* pQueue = NULL;
										strcpy(errorstring, "OK");
										pQueue = pdirect->m_data.ReturnFromEndpointQueue(pdirect->m_data.m_pCheckEvent, errorstring);
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "** ** queue returned with %s", errorstring);  Sleep(50); //(Dispatch message)

										if((pQueue)&&(pQueue->m_nItemID>0))
										{
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "** ** queue item ID: %d", pQueue->m_nItemID);  Sleep(50); //(Dispatch message)
											if(pQueue->m_nActionID==128)
											{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "** ** queue action was: %d", pQueue->m_nActionID);  Sleep(50); //(Dispatch message)
												//						if it is 128, it is either present or errored out.
											// if it errored out, we maybe should try to transfer it there.
												pdirect->m_data.RemoveFromEndpointQueue(pdirect->m_data.m_pCheckEvent, pQueue->m_nItemID);
												if(pQueue->m_pszMessage[0]=='E') //error.
												{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "** ** queue action was: %d with Error %s", pQueue->m_nActionID, pQueue->m_pszMessage);  Sleep(50); //(Dispatch message)

													// this is not necessarily an error.
													// we have to check it out, based on type.
													// if this is intuition, it is an error for sure.

													int nDestModule= pdirect->m_data.ReturnModuleIndex(pdirect->m_data.m_pCheckEvent, errorstring);

													CString szDestID = "Destinations";  // we didn't find it. just use the default name
													if(nDestModule>=0)
													{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got module");  Sleep(50); //(Dispatch message)
														szDestID = pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination;

														if((pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType&DIRECT_DEP_EDGE_MASK)==DIRECT_DEP_EDGE_PROSPERO)
														{
															// we have the rule ID.
															// let's get the type

															int nRuleID = atoi(pdirect->m_data.m_pCheckEvent->m_sz_n_rule_id);
															int nRuleIndex = pdirect->m_data.GetRuleIndex(nRuleID);

															int nAuxData = 	atol(pdirect->m_data.m_pCheckEvent->m_sz_n_app_data_aux);

															if(nRuleIndex>=0)
															{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got rule index %d with type %d", nRuleIndex,pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulDestinationType);  Sleep(50); //(Dispatch message)

																if(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulDestinationType == DIRECT_RULE_DESTTYPE_MIRANDA_INT)
																{
																	// we have an intuition template or subfiles.
																	// dont send the msg, we will try to transfer it. 																	
																	//pdirect->SendMsg(CX_SENDMSG_ERROR, "DiReCT:CheckFileError", "%s", pQueue->m_pszMessage);
																	pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 4);

																}
																else
																{
																	// if this is an imagestore, we look thru the search extensions string for both audio and video.

																	// need to check if we are doing null oxt or not.
																	if(g_pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_usActionType == DIRECT_RULE_ACTION_NULLOXT)
																	{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "NULLOXT");  //(Dispatch message)
																		if(nAuxData&DIRECT_EVENTSTATUS_AUDIO)
																		{
																			// we already found the audio. so we must have been checking for a video file which was not there.
																			// we have to check what we were looking for, try other things in the string
																			
																			bool bVideoFound=false;
																			//char pszCurrentExtension[32];
																			char* pchExt = NULL;
																			//sprintf(pszFullPath, "%s\\%s", pfmdo->m_sz_vc256_sys_filepath, szFilename);

																			if(pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch<0)
																			{
																				pchExt = strrchr(pdirect->m_data.m_pCheckEvent->m_pszMainFile, '.');
																			}
																			else
																			{
																				pchExt = strrchr(pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch], '.');
																			}

																			if(pchExt)
																			{
																				pchExt++;
																			}

																			char* pszSearchExts = NULL; 

																			switch(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulDestinationType)
																			{
																			case DIRECT_RULE_DESTTYPE_MIRANDA_INT:
																				{
																					pszSearchExts = pdirect->m_settings.m_pszIntuitionSearchExt;
																				} break; // Intuition
																			case DIRECT_RULE_DESTTYPE_MIRANDA_IS300:	 // Imagestore 300
																				{
																					pszSearchExts = pdirect->m_settings.m_pszImagestore300SearchExt;
																				} break; 
																			case DIRECT_RULE_DESTTYPE_MIRANDA_ISHD:		 // Imagestore HD
																				{
																					pszSearchExts = pdirect->m_settings.m_pszImagestoreHDSearchExt;
																				} break; 
																			case DIRECT_RULE_DESTTYPE_MIRANDA_IS2:		//Imagestore2
																				{
																					pszSearchExts = pdirect->m_settings.m_pszImagestore2SearchExt;
																				} break;
																			} // switch

																			if((pszSearchExts)&&(pchExt))
																			{
																				char pszTestFilename[MAX_PATH];
																				CSafeBufferUtil sbu;
																				char* pszExt = sbu.Token(pszSearchExts, strlen(pszSearchExts), ",;");
																				while(pszExt)
																				{ // cycles to the last one.
																					if(stricmp(pszExt, pchExt)==0)
																					{// found it, so go to next.
																						pszExt = sbu.Token(NULL, NULL, ",;");
																						sprintf(pszTestFilename,"%s.%s", pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename, pszExt); 
																						char* pszPartn = pdirect->m_data.ReturnPartition(pdirect->m_data.m_pCheckEvent, pszTestFilename);
																						if((pszPartn)&&(strlen(pszPartn)))
																						{
																							if(strncmp(pszPartn, "$VIDEO", 6)==0)
																							{
																								char* pchFile = (char*)malloc(strlen(pszPartn)+strlen(pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename)+strlen(pszExt)+3);
																								if(pchFile)
																								{
																									sprintf(pchFile, "%s/%s.%s", pszPartn, pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename, pszExt);

																									// success!
																									bVideoFound = true;
																									if(pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch<0)
																									{
																										if(pdirect->m_data.m_pCheckEvent->m_pszMainFile) free(pdirect->m_data.m_pCheckEvent->m_pszMainFile);
																										pdirect->m_data.m_pCheckEvent->m_pszMainFile = pchFile;
																									}
																									else
																									{
																										if(pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch]) free(pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch]);
																										pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch] = pchFile;
																									}

																									pdirect->m_data.ScheduleEndpointQueue(pdirect->m_data.m_pCheckEvent, "", pchFile, 64); 
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "NULLOXT is now queueing %s", pchFile);   //(Dispatch message)
																									pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 1);
																									pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 1, true, true, nAuxData);  //check existence of next

																									if(pszPartn)
																									{
																										free(pszPartn);
																										pszPartn = NULL;
																									}
																									break;

																								}
																							}
																						}
																						if(pszPartn) free(pszPartn);
																					}

																					pszExt = sbu.Token(NULL, NULL, ",;");
																				}
																			}
																			if(!bVideoFound)
																			{
																				// exhausted the possibilities, so try to transfer something.
																				pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 4);
																			}
																		}
																		else
																		{
																			// we didnt find the audio itself!  ERROR;
																			// dont send the msg, we will try to transfer it. 																	
																			//pdirect->SendMsg(CX_SENDMSG_ERROR, "DiReCT:CheckFileError", "%s", pQueue->m_pszMessage);
																			pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 4);
																		}
																	}
																	else  // not null oxt
																	{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "checking, not NULLOXT");  //(Dispatch message)

																		if(nAuxData&DIRECT_EVENTSTATUS_VIDEO)
																		{
																			// we already found the video. so we must have been checking for an audio file which was not there.
																			// in that case we can just go to trying to transfer it.
																			// dont send the msg, we will try to transfer it. 																	
																			//pdirect->SendMsg(CX_SENDMSG_ERROR, "DiReCT:CheckFileError", "%s", pQueue->m_pszMessage);
																			pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 4);
																		}
																		else
																		{
																			//the initial video file not found, not nec an error, lets check for others.
																			bool bVideoFound=false;
																			//char pszCurrentExtension[32];
																			char* pchExt = NULL;
																			//sprintf(pszFullPath, "%s\\%s", pfmdo->m_sz_vc256_sys_filepath, szFilename);

																			if(pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch<0)
																			{
																				pchExt = strrchr(pdirect->m_data.m_pCheckEvent->m_pszMainFile, '.');
																			}
																			else
																			{
																				pchExt = strrchr(pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch], '.');
																			}

																			if(pchExt)
																			{
																				pchExt++;
																			}

																			char* pszSearchExts = NULL; 

																			switch(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulDestinationType)
																			{
																			case DIRECT_RULE_DESTTYPE_MIRANDA_INT:
																				{
																					pszSearchExts = pdirect->m_settings.m_pszIntuitionSearchExt;
																				} break; // Intuition
																			case DIRECT_RULE_DESTTYPE_MIRANDA_IS300:	 // Imagestore 300
																				{
																					pszSearchExts = pdirect->m_settings.m_pszImagestore300SearchExt;
																				} break; 
																			case DIRECT_RULE_DESTTYPE_MIRANDA_ISHD:		 // Imagestore HD
																				{
																					pszSearchExts = pdirect->m_settings.m_pszImagestoreHDSearchExt;
																				} break; 
																			case DIRECT_RULE_DESTTYPE_MIRANDA_IS2:		//Imagestore2
																				{
																					pszSearchExts = pdirect->m_settings.m_pszImagestore2SearchExt;
																				} break;
																			} // switch

																			if((pszSearchExts)&&(pchExt))
																			{
																				char pszTestFilename[MAX_PATH];
																				CSafeBufferUtil sbu;
																				char* pszExt = sbu.Token(pszSearchExts, strlen(pszSearchExts), ",;");
																				while(pszExt)
																				{ // cycles to the last one.
																					if(stricmp(pszExt, pchExt)==0)
																					{// found it, so go to next.
																						pszExt = sbu.Token(NULL, NULL, ",;");
																						sprintf(pszTestFilename,"%s.%s", pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename, pszExt); 
																						char* pszPartn = pdirect->m_data.ReturnPartition(pdirect->m_data.m_pCheckEvent, pszTestFilename);
																						if((pszPartn)&&(strlen(pszPartn)))
																						{
																							if(strncmp(pszPartn, "$VIDEO", 6)==0)
																							{
																								char* pchFile = (char*)malloc(strlen(pszPartn)+strlen(pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename)+strlen(pszExt)+3);
																								if(pchFile)
																								{
																									sprintf(pchFile, "%s/%s.%s", pszPartn, pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename, pszExt);

																									// success!
																									bVideoFound = true;
																									if(pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch<0)
																									{
																										if(pdirect->m_data.m_pCheckEvent->m_pszMainFile) free(pdirect->m_data.m_pCheckEvent->m_pszMainFile);
																										pdirect->m_data.m_pCheckEvent->m_pszMainFile = pchFile;
																									}
																									else
																									{
																										if(pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch]) free(pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch]);
																										pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch] = pchFile;
																									}
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "now checking for %s",pchFile);  //(Dispatch message)

																									pdirect->m_data.ScheduleEndpointQueue(pdirect->m_data.m_pCheckEvent, "", pchFile, 64); 
																									pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 1);
																									pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 1, true, true, nAuxData);  // 1 is checking for files

																									if(pszPartn)
																									{
																										free(pszPartn);
																										pszPartn = NULL;
																									}
																									break;

																								}
																							}
																						}
																						if(pszPartn) free(pszPartn);
																					}

																					pszExt = sbu.Token(NULL, NULL, ",;");
																				}
																			}
																			if(!bVideoFound)
																			{
																				// exhausted the possibilities, so try to transfer something.
																				pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 4);
																			}
																		}
																	}
																}
															} // else rule ID not found
															else
															{
																// set to error.
																pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false, nAuxData); 
															}
														}
														else if((pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType&DIRECT_DEP_EDGE_MASK)==DIRECT_DEP_EDGE_BARBERO)
														{
															// DEMO just set it to found!
															pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 7, false, false);  // 7 = with audio
														}															
														else // else // not prospero or barbero, so not supported yet.
														{
															pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false); 
														}
													}
													else  // not the module
													{
														pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false); 
													}
												}//if(szFilenameRemote.GetAt(0)=='E') //error.
												else// it is there, we update alls well
												{
if(pdirect->m_settings.m_bLogTransfers) pdirect->m_msgr.DM(MSG_ICONINFO, NULL, "DiReCT:CheckFile", "%s", pQueue->m_pszMessage); //Sleep(100); //(Dispatch message)
		//pdirect->SendMsg(int nType, char* pszSender, char* pszMessage, ...);
													// grab the message, MSG it out.
													if(pdirect->m_settings.m_bReportSuccessfulOperation)
														pdirect->SendMsg(CX_SENDMSG_INFO, "DiReCT:CheckFile", "%s", pQueue->m_pszMessage);
													// set up the next xfer if any, otherwise close out and delete
													//SQL: set the direct event status to complete,  update the description, etc
												// need to recompile search files with a code.
													pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 2);
													pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 5, true);
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "calling RemoveFromEndpointQueue");  Sleep(50); //(Dispatch message)
						//					remove from queue if exists, and reset transfer inc to -1.  reset inc to transfer inc.
													pdirect->m_data.RemoveFromEndpointQueue(pdirect->m_data.m_pCheckEvent, pQueue->m_nItemID);
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "called RemoveFromEndpointQueue");  Sleep(50); //(Dispatch message)
													// here, we have to update statistics.
													pdirect->m_data.IncrementGlobalTimesUsed(pdirect->m_data.m_pCheckEvent);
													pdirect->m_data.IncrementLocalTimesUsed(pdirect->m_data.m_pCheckEvent);

												}

											}  // the action was 128 (give info)
											else
											{
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "** ** non 128 queue action was: %d", pQueue->m_nActionID);  Sleep(50); //(Dispatch message)

											}
										}//if((pQueue)&&(pQueue->m_nItemID>0))
										else  
										{
											// couldn't find it so it must have been cancelled
											// we could send an abort...
										}
										if(pQueue) delete pQueue;
									} break;
								case 2:  //    yellow   File transfer queued 
									{
										// check the transfer in pdirect->
										// if it is there, set to 4, update.
										CDirectQueueObject* pQueue = NULL;
										pQueue = pdirect->m_data.ReturnFromEndpointQueue(pdirect->m_data.m_pCheckEvent, errorstring);

										if((pQueue)&&(pQueue->m_nItemID>0))
										{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "** ** status 2 queue action was: %d with msg %s", pQueue->m_nActionID, pQueue->m_pszMessage);  Sleep(50); //(Dispatch message)
											if(pQueue->m_nActionID==128)
											{
												//if it is 128, we have either finished or errored out.
												//if it errored out, we should try again in config # of minutes, save stuff to DB and close record set etc.
												if(pQueue->m_pszMessage[0]=='E') //error.
												{
													// grab the message, MSG it out.
													//pdirect->SendMsg(int nType, char* pszSender, char* pszMessage, ...);
													int nAuxData = 	atol(pdirect->m_data.m_pCheckEvent->m_sz_n_app_data_aux);
													if(!(nAuxData&DIRECT_EVENTSTATUS_ERRORSENT_XFER))
													{
														pdirect->SendMsg(CX_SENDMSG_ERROR, "DiReCT:TransferError", "%s", pQueue->m_pszMessage);
														nAuxData|=DIRECT_EVENTSTATUS_ERRORSENT_XFER;
													}
													pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 5);
													pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -2, true, false, nAuxData);
												}
												else
												{
													//pdirect->SendMsg(int nType, char* pszSender, char* pszMessage, ...);
													// grab the message, MSG it out.
													if(pdirect->m_settings.m_bReportSuccessfulOperation)
														pdirect->SendMsg(CX_SENDMSG_INFO, "DiReCT:Transfer", "%s", pQueue->m_pszMessage);
													// set up the next xfer if any, otherwise close out and delete
													pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 4);
													pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 5, true);

													// here, we have to update statistics.
													pdirect->m_data.IncrementGlobalTimesUsed(pdirect->m_data.m_pCheckEvent);
													pdirect->m_data.IncrementLocalTimesUsed(pdirect->m_data.m_pCheckEvent);

												}
						//					remove from queue if exists, and reset transfer inc to -1.  reset inc to transfer inc.
												pdirect->m_data.RemoveFromEndpointQueue(pdirect->m_data.m_pCheckEvent, pQueue->m_nItemID);

											}  // the action was 128 (give info)
										}
										else
										{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "*null queue obj %s", pQueue?"not null":"NULL");  Sleep(50); //(Dispatch message)
											// couldn't find it so it must have been cancelled
											// we could send an abort...
										}
										if(pQueue) delete pQueue;
										// if pending, skip.
										// if finished transfer, set to 4, update!
									} break;
								case 3: //yellow Deleting files on destination
									{
										// check the check in pdirect->
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "status 3");
										// if it is there, set to 4, update.
										CDirectQueueObject* pQueue = NULL;
										pQueue = pdirect->m_data.ReturnFromEndpointQueue(pdirect->m_data.m_pCheckEvent, errorstring);

										if((pQueue)&&(pQueue->m_nItemID>0))
										{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "status 3 item id %d", pQueue->m_nItemID);
											if(pQueue->m_nActionID==128)
											{

//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "status 3 128 - %s",pQueue->m_pszMessage);
//												pdirect->m_data.ReleaseRecordSet();
												//						if it is 128, it is either present or errored out.
											// if it errored out, we should try to transfer it there.
													// dont need to send msg on actual delete, let direct do it.
/*
												if(pQueue->m_pszMessage[0]=='E') //error.
												{
													// grab the message, MSG it out.
													//pdirect->SendMsg(int nType, char* pszSender, char* pszMessage, ...);
//													pdirect->SendMsg(CX_SENDMSG_ERROR, "DiReCT:DeleteFileError", "%s", pQueue->m_pszMessage);
												}
												else
												{
													pdirect->SendMsg(CX_SENDMSG_INFO, "DiReCT:DeleteFile", "%s", pQueue->m_pszMessage);
												}
*/
												// set it to pending. (4)
						//					remove from queue if exists, and reset transfer inc to -1.  reset inc to transfer inc.
								//      escape
												pdirect->m_data.RemoveFromEndpointQueue(pdirect->m_data.m_pCheckEvent, pQueue->m_nItemID);
												pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 4);
											}  // the action was 128 (give info)
										}//if((nNumFound>0)&&(nItemID>0))
										else  
										{
											// couldn't find it so it must have been cancelled
											// let's move on.
//											pdirect->m_data.ReleaseRecordSet();
											pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 4);
										}
										if(pQueue) delete pQueue;
									} break;
								case 4:  //    yellow   File transfer to be queued pending disk space deletions
									{
										// check to see if disk space is good yet.  
										// get the metadata!  
// get all the info from achivist.
										int nAuxData = 	atol(pdirect->m_data.m_pCheckEvent->m_sz_n_app_data_aux);

										CDirectEndpointObject* pDataObj = NULL;
										if((pdirect->m_settings.m_ppEndpointObject)
											&&(pdirect->m_data.m_nIndexMetadataEndpoint<pdirect->m_settings.m_nNumEndpointsInstalled)
											&&(pdirect->m_settings.m_ppEndpointObject[pdirect->m_data.m_nIndexMetadataEndpoint]))
										{
											pDataObj = pdirect->m_settings.m_ppEndpointObject[pdirect->m_data.m_nIndexMetadataEndpoint];
										}

										if(pDataObj == NULL)
										{
											if(!(nAuxData&DIRECT_EVENTSTATUS_ERRORSENT_META))
											{
												pdirect->SendMsg(CX_SENDMSG_ERROR, "DiReCT:Metadata", "No metadata module available, cannot locate file to transfer.");
												nAuxData|=DIRECT_EVENTSTATUS_ERRORSENT_META;
											  pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -4, false, false, nAuxData); 
											}
										} //if(pDataObj == NULL)
										else  // data obj pointer exists
										{
											// have to figure out the correct filename based, based on rules.

///////////////////////////////////////////////////////
// finding filename based on type

											CString szFilename = "";

											int nDestModule= pdirect->m_data.ReturnModuleIndex(pdirect->m_data.m_pCheckEvent, errorstring);

											CString szDestID = "Destinations";  // we didn't find it. just use the default name
											if(nDestModule>=0)
											{
												szDestID = pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination;

												if((pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType&DIRECT_DEP_EDGE_MASK)==DIRECT_DEP_EDGE_PROSPERO)
												{
												// if this is an intuition template, we want to get it from the miranda and parse it.

													// we have the rule ID.
													// let's get the type
													bool bVideoCheck= false;
													bool bAudioCheck = false;
													bool bTemCheck = false;
													bool bInitialCheck = false;

													int nRuleID = atoi(pdirect->m_data.m_pCheckEvent->m_sz_n_rule_id);
													int nRuleIndex = pdirect->m_data.GetRuleIndex(nRuleID);

													if(nRuleIndex>=0)
													{
											// if this is intuition, we can just use the filename as is.
														if(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulDestinationType == DIRECT_RULE_DESTTYPE_MIRANDA_INT)
														{


															bTemCheck = true;
															// TODO+ error checking on nulls for these
															if(pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch<0)
															{
																szFilename = pdirect->m_data.m_pCheckEvent->m_pszMainFile;
															}
															else
															{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "Intuition index is: %d", pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch);  Sleep(50); //(Dispatch message)
																if(
																	  (pdirect->m_data.m_pCheckEvent->m_ppszChildren)
																	&&(pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch<pdirect->m_data.m_pCheckEvent->m_nNumChildren)
																	&&(pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch])
																	)
																	szFilename = pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch];
																else
																	szFilename = "";
															}
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "status 4 Intuition filename is: %s", szFilename);  Sleep(50); //(Dispatch message)

														}
														else  
														{

											// if this is imagestore type, have to determine if video was there.

															// check which partition it was.
															// TODO+ error checking on nulls for these
															if(pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch<0)
															{
																szFilename = pdirect->m_data.m_pCheckEvent->m_pszMainFile;
															}
															else
															{
																szFilename = pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch];
															}
															
															if(strncmp(szFilename, "$VIDEO", 6)==0)
															{
																bVideoCheck= true;
																bInitialCheck = true;
															}
															else
															if(strncmp(szFilename, "$AUDIO", 6)==0)
															{
																bAudioCheck = true;
																// if it was audio, we can just go ahead and search for the audio file as is.
															}
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "Imagestore filename is: %s", szFilename);  Sleep(50); //(Dispatch message)

														}

														bool bFileMetadataFound = false;
														char pszTestFilename[MAX_PATH]; // for video only

														do
														{

															bool bVideoFound=false;
															if(bVideoCheck)
															{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "videocheck");  Sleep(50); //(Dispatch message)
																// we are here because we were not able to find ANY video files for this item in an imagestore device.
																// we have to search for any video files in the matadata, if found trannsfer one.
																// if nothing found, set to 5 for null oxt, or -4 for error.

																// have to figure out what filename to check for..

																char* pszSearchExts = NULL; 

																switch(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulDestinationType)
																{
																case DIRECT_RULE_DESTTYPE_MIRANDA_INT:
																	{
																		pszSearchExts = pdirect->m_settings.m_pszIntuitionSearchExt;
																	} break; // Intuition
																case DIRECT_RULE_DESTTYPE_MIRANDA_IS300:	 // Imagestore 300
																	{
																		pszSearchExts = pdirect->m_settings.m_pszImagestore300SearchExt;
																	} break; 
																case DIRECT_RULE_DESTTYPE_MIRANDA_ISHD:		 // Imagestore HD
																	{
																		pszSearchExts = pdirect->m_settings.m_pszImagestoreHDSearchExt;
																	} break; 
																case DIRECT_RULE_DESTTYPE_MIRANDA_IS2:		//Imagestore2
																	{
																		pszSearchExts = pdirect->m_settings.m_pszImagestore2SearchExt;
																	} break;
																} // switch

																if(pszSearchExts)
																{
																	if(bInitialCheck)
																	{
																		CSafeBufferUtil sbu;
																		char* pszExt = sbu.Token(pszSearchExts, strlen(pszSearchExts), ",;");
																		while(pszExt)
																		{ 
																			sprintf(pszTestFilename,"%s.%s", pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename, pszExt); 
																			char* pszPartn = pdirect->m_data.ReturnPartition(pdirect->m_data.m_pCheckEvent, pszTestFilename);
																			if((pszPartn)&&(strlen(pszPartn)))
																			{
																				if(strncmp(pszPartn, "$VIDEO", 6)==0)
																				{
																					szFilename = pszTestFilename;
																					bInitialCheck=false;

																					char* pchFile = (char*)malloc(strlen(pszPartn)+strlen(pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename)+strlen(pszExt)+3);
																					if(pchFile)
																					{
																						sprintf(pchFile, "%s/%s.%s", pszPartn, pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename, pszExt);

																						// success!
																						if(pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch<0)
																						{
																							if(pdirect->m_data.m_pCheckEvent->m_pszMainFile) free(pdirect->m_data.m_pCheckEvent->m_pszMainFile);
																							pdirect->m_data.m_pCheckEvent->m_pszMainFile = pchFile;
																						}
																						else
																						{
																							if(pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch]) free(pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch]);
																							pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch] = pchFile;
																						}

																						// success!
																						bVideoFound = true;
																						if(pszPartn)
																						{
																							free(pszPartn);
																							pszPartn = NULL;
																						}
																						break;
																					}
																				}
																			}
																			
																			if(pszPartn) free(pszPartn);

																			pszExt = sbu.Token(NULL, NULL, ",;");
																		}

																	}
																	else // get next
																	{
																		char* pchExt = NULL;

																		pchExt = strrchr(pszTestFilename, '.');

																		if(pchExt)
																		{
																			pchExt++;
																		}

																		CSafeBufferUtil sbu;
																		char* pszExt = sbu.Token(pszSearchExts, strlen(pszSearchExts), ",;");
																		while(pszExt)
																		{ // cycles to the last one.
																			if(stricmp(pszExt, pchExt)==0)
																			{// found it, so go to next.
																				pszExt = sbu.Token(NULL, NULL, ",;");
																				sprintf(pszTestFilename,"%s.%s", pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename, pszExt); 
																				char* pszPartn = pdirect->m_data.ReturnPartition(pdirect->m_data.m_pCheckEvent, pszTestFilename);
																				if((pszPartn)&&(strlen(pszPartn)))
																				{
																					if(strncmp(pszPartn, "$VIDEO", 6)==0)
																					{
																						szFilename = pszTestFilename;
																						char* pchFile = (char*)malloc(strlen(pszPartn)+strlen(pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename)+strlen(pszExt)+3);
																						if(pchFile)
																						{
																							sprintf(pchFile, "%s/%s.%s", pszPartn, pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename, pszExt);

																							// success!
																							bVideoFound = true;
																							if(pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch<0)
																							{
																								if(pdirect->m_data.m_pCheckEvent->m_pszMainFile) free(pdirect->m_data.m_pCheckEvent->m_pszMainFile);
																								pdirect->m_data.m_pCheckEvent->m_pszMainFile = pchFile;
																							}
																							else
																							{
																								if(pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch]) free(pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch]);
																								pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch] = pchFile;
																							}

																							if(pszPartn)
																							{
																								free(pszPartn);
																								pszPartn = NULL;
																							}
																							break;

																						}
																					}
																				}
																				if(pszPartn) free(pszPartn);
																			}

																			pszExt = sbu.Token(NULL, NULL, ",;");
																		} /// while
																	} // not initial
																} // if search extensions exist
																if(!bVideoFound) szFilename="";
															}// if video check


														// have to remove partition.
															CFileMetaDataObject* pfmdo = NULL;
															CString szFile;
															szFile = szFilename;

															if(szFilename.GetLength()>1)
															{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "partition remove from %s", szFilename);  Sleep(50); //(Dispatch message)
																int nSlash = szFilename.ReverseFind('/');
																
																if(nSlash>=0)
																	szFile = szFilename.Mid(nSlash + 1);
						
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "getting metadata for %s", szFile);  Sleep(50); //(Dispatch message)
																pfmdo = pdirect->m_data.ReturnFileMetaDataObject(szFile.GetBuffer(1));
																szFile.ReleaseBuffer();
															}
														
															if(pfmdo==NULL)
															{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "no metadata for %s", szFile);  Sleep(50); //(Dispatch message)
																if(bVideoCheck)
																{
																	if(bVideoFound)
																	{
																		// have to go around again with the filename
																		;
																	}
																	else
																	{
																		bFileMetadataFound = true;
//																		pdirect->m_data.ReleaseRecordSet();
																		// have to make a nulloxt determination
																		if(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_usActionType == DIRECT_RULE_ACTION_NULLOXT)
																		{
//																			pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 6);
//																			pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 5, true);

																			// couldn't find anything, have to use NULL OXT!
																			char* pchChild = (char*)malloc(strlen("$VIDEO/X.oxt")+strlen(pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename));
																			if(pchChild)
																			{
																				sprintf(pchChild, "$VIDEO/%s.oxt", pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename);


																				if(pdirect->m_data.m_pCheckEvent->m_ppszChildren)
																				{
																					int i=0;
																					while(i<pdirect->m_data.m_pCheckEvent->m_nNumChildren)
																					{
																						if(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]) free(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]);
																						i++;
																					}
																					delete [] pdirect->m_data.m_pCheckEvent->m_ppszChildren;
																					pdirect->m_data.m_pCheckEvent->m_ppszChildren=NULL;
																				}

																				if(pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus)
																				{
																					delete [] pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus;
																					pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus=NULL;
																				}

																				pdirect->m_data.m_pCheckEvent->m_nNumChildren=0;


																				if(pdirect->m_data.m_pCheckEvent->AddChild(pchChild, 0)<0)
																				{
																					free(pchChild);
																					pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false, nAuxData); 
																				}
																				else
																				{
																					pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch = 0;

																					if(
																							 (pdirect->m_settings.m_pszImagestoreNullFile)&&(strlen(pdirect->m_settings.m_pszImagestoreNullFile))
																						 &&(pdirect->m_settings.m_pszSystemFolderPath)&&(strlen(pdirect->m_settings.m_pszSystemFolderPath))
																						)
																					{
																					// nothing more to find, have to transfer the NULL OXT.

																						// create a fake metadata record and do the disk space search
																						// first let's check that the file is where we think it is.  otherwise there is no point in scheduling a transfer
																						
																						sprintf(errorstring, "%s%s", pdirect->m_settings.m_pszSystemFolderPath, pdirect->m_settings.m_pszImagestoreNullFile);
																						
																						WIN32_FIND_DATA wfd;
																						HANDLE hfile = NULL;


																						hfile = FindFirstFile(errorstring, &wfd);
																						if(hfile != INVALID_HANDLE_VALUE)
																						{
																							//it's there, lets get it's size.
																							double dblFoundFileSize = -1;
																							dblFoundFileSize = ((double)(wfd.nFileSizeHigh)*(double)(0xffffffff)) + (double)wfd.nFileSizeLow;
																							pfmdo = new CFileMetaDataObject;
																							if(pfmdo)
																							{
																								szFile.Format("%s", pdirect->m_settings.m_pszImagestoreNullFile);
																								pfmdo->m_sz_vc256_sys_filename = (char*)malloc(strlen(pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename)+5);
																								if(pfmdo->m_sz_vc256_sys_filename) sprintf(pfmdo->m_sz_vc256_sys_filename, "%s.oxt", szFile);
																								pfmdo->m_sz_vc256_sys_filepath = (char*)malloc(strlen(pdirect->m_settings.m_pszSystemFolderPath)+1);
																								if(pfmdo->m_sz_vc256_sys_filepath)
																								{
																									sprintf(pfmdo->m_sz_vc256_sys_filepath, "%s", pdirect->m_settings.m_pszSystemFolderPath);
																									// remove trailing slashes, if any
																									while(
																													(strlen(pfmdo->m_sz_vc256_sys_filepath)>0)
																												&&(
																														(pfmdo->m_sz_vc256_sys_filepath[strlen(pfmdo->m_sz_vc256_sys_filepath)-1]=='\\')
																													||(pfmdo->m_sz_vc256_sys_filepath[strlen(pfmdo->m_sz_vc256_sys_filepath)-1]=='/')
																													)
																												)
																									{
																										pfmdo->m_sz_vc256_sys_filepath[strlen(pfmdo->m_sz_vc256_sys_filepath)-1] = 0;
																									}
																								}
																								pfmdo->m_sz_dbl_sys_file_size = (char*)malloc(64); // more than enough for a huge number
																								if(pfmdo->m_sz_dbl_sys_file_size)
																								{
																									sprintf(pfmdo->m_sz_dbl_sys_file_size, "%f", dblFoundFileSize);
																								}

																								bVideoFound = true;
																								bFileMetadataFound = true;
																							}

																							FindClose( hfile); 

																							if(!bVideoFound)
																							{
																								pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 5);
																								pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false, nAuxData); 
																							}
																						}
																						else
																						{
																							if(!(nAuxData&DIRECT_EVENTSTATUS_ERRORSENT_NOXT))
																							{
																								pdirect->SendMsg(CX_SENDMSG_ERROR, "DiReCT:NoFile", "The NULL oxt file was not found at %s.\nThe file could not be transferred.", errorstring );
																								nAuxData|=DIRECT_EVENTSTATUS_ERRORSENT_NOXT;
																							}
																							pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 5);
																							pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -4, true, false, nAuxData); 
																						}
																					}
																					else
																					{
																						pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 5);
																						pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false, nAuxData); 
																					}
																				}
																			}
																			else
																			{
																				pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 5);
																				pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false, nAuxData); 
																			}
																		}
																		else
																		{
																			if(!(nAuxData&DIRECT_EVENTSTATUS_ERRORSENT_META))
																			{
																				pdirect->SendMsg(CX_SENDMSG_ERROR, "DiReCT:Metadata", "No metadata exists for video files for %s, cannot locate a file for transfer.", pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename);
																				nAuxData|=DIRECT_EVENTSTATUS_ERRORSENT_META;
																			}

																			pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 5);
																			pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -4, true, false, nAuxData); 
																		}
																	}
																}
																else // if((bAudioCheck)||(bTemCheck)) and all other types, actually
																{

																	if(
																		  (bAudioCheck)  // if it was an audio search, we only have to error out if explicit, or audio.
																		&&( 
																		    (pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_usSearchType == DIRECT_RULE_SEARCH_VIDEO)
																			||(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_usSearchType == DIRECT_RULE_SEARCH_AV) 
																			)
																		&&(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_usActionType != DIRECT_RULE_ACTION_NULLOXT)  // if null oxt, it would be an error
																		)
																	{
																		// not an error.  just set to skipped and go.
																		bFileMetadataFound = true;
//																		pdirect->m_data.ReleaseRecordSet();
																		pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 6); // skipped
																		pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 5, true);
																	}
																	else
																	{
																		bFileMetadataFound = true;
//																		pdirect->m_data.ReleaseRecordSet();
																		if(!(nAuxData&DIRECT_EVENTSTATUS_ERRORSENT_META))
																		{
																			pdirect->SendMsg(CX_SENDMSG_ERROR, "DiReCT:Metadata", "No metadata found for file %s, cannot locate file for transfer.", szFile);
																			nAuxData|=DIRECT_EVENTSTATUS_ERRORSENT_META;
																		}
																		pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 5); // error
																		pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -4, true, false, nAuxData); 
																	}
																}
															}
															//else  // metadata item found, can continue.
															if(pfmdo!=NULL)  // was else, but now this for create fake metadata for null oxt
															{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "metadata found for %s", szFile);  Sleep(50); //(Dispatch message)
																bFileMetadataFound = true;
																if(!pfmdo->m_bUnique)
																{
																	// there was more than one file, but we dont know which one
																	pdirect->SendMsg(CX_SENDMSG_ERROR, "DiReCT:Metadata", 
																		"Duplicate metadata exists for file %s.  DiReCT will use the file located at %s\\%s.",
																		pfmdo->m_sz_vc256_sys_filename,
																		pfmdo->m_sz_vc256_sys_filepath, pfmdo->m_sz_vc256_sys_filename
																		);
																}
																bool bScheduleTransfer = false;
				// first thing is check the disk space.

																// first find destination table name for module.
																DiskSpaceObject_t*  pDisk = NULL;
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "getting disk space");  Sleep(50); //(Dispatch message)
																pDisk = pdirect->m_data.ReturnDestinationDiskSpace(pdirect->m_data.m_pCheckEvent, errorstring);

																if((pDisk)&&(pDisk->dblDiskTotal>0))
																{

/*
pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "disk space: free %f, total %f, threshold %f, util %f, filesize = %s",	pDisk->dblDiskFree,
	pDisk->dblDiskTotal,
	pDisk->dblDiskThreshold,
	pDisk->dblDiskPercentUtilized,
	pfmdo->m_sz_dbl_sys_file_size?pfmdo->m_sz_dbl_sys_file_size:"(null)");  Sleep(50); //(Dispatch message)
*/
																	double dblFileSize = 0;
																	if(pfmdo->m_sz_dbl_sys_file_size) dblFileSize = atof(pfmdo->m_sz_dbl_sys_file_size);

																	if(dblFileSize>0)
																	{
																		// have to use file metadata
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "disk space for %s: file size=%f, after util = %f, threshold = %f, difference (util-thresh) = %f",	pfmdo->m_sz_vc256_sys_filename, dblFileSize, ((pDisk->dblDiskTotal-pDisk->dblDiskFree+(dblFileSize/1024.0))*100.0/pDisk->dblDiskTotal ), pDisk->dblDiskThreshold, ((pDisk->dblDiskTotal-pDisk->dblDiskFree+(dblFileSize/1024.0))*100.0/pDisk->dblDiskTotal ) - pDisk->dblDiskThreshold );
																		if( ((pDisk->dblDiskTotal-pDisk->dblDiskFree+(dblFileSize/1024.0))*100.0/pDisk->dblDiskTotal ) < pDisk->dblDiskThreshold )
																		{
																			bScheduleTransfer = true;
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "disk space util less than threshold");
																		}
																	}
																	else // couldnt get file size info for calc, have to try to transfer anway
																	{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "couldn't get file size info for calc" );
																		bScheduleTransfer = true;
																	}
																}
																else  // couldnt get disk space, have to try to transfer anway
																{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "couldn't get disk space" );
																	bScheduleTransfer = true;
																}
																if(pDisk) delete pDisk;

//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "%sscheduling transfer", bScheduleTransfer?"":"not ");  Sleep(50); //(Dispatch message)
																
																if(!bScheduleTransfer)  // schedule a delete!
																{
				// find a file to delete.
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "calling ReturnDestinationMediaObjectToDelete" );
																	CDestinationMediaObject*  pdmo = 
																		pdirect->m_data.ReturnDestinationMediaObjectToDelete(pdirect->m_data.m_pCheckEvent);

																	if(pdmo)
																	{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "got %s from ReturnDestinationMediaObjectToDelete", pdmo->m_sz_vc256_file_name);

																		char pszDelPath[MAX_PATH];
																		strcpy(pszDelPath,"");
																		int nDestModule = pdirect->m_data.ReturnModuleIndex(pdirect->m_data.m_pCheckEvent, errorstring);
																		if(nDestModule>=0)
																		{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "ReturnModuleIndex returned %d", nDestModule);
																			if((pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType&DIRECT_DEP_EDGE_MASK)==DIRECT_DEP_EDGE_PROSPERO)
																			{
																	//its pdirect->
																				sprintf(pszDelPath, "%s/%s", pdmo->m_sz_vc16_partition, pdmo->m_sz_vc256_file_name);
																			}
																		// else ... TODO
																		}

																	// update direct, queue the transfer

																		if(strlen(pszDelPath)>0)
																		{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "Scheduling delete for %s", pszDelPath);
																			pdirect->m_data.ScheduleEndpointQueue(pdirect->m_data.m_pCheckEvent, "", pszDelPath, 3);
																			pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 3);
																		}
																		else
																		{
																			bScheduleTransfer = true;  //have to try anyway
																		}
																		delete pdmo;
																	}
																	else
																	{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "couldn't get file to delete" );

																		bScheduleTransfer = true;  //have to try anyway
																	}
																}
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "bScheduleTransfer = %d", bScheduleTransfer );

																if(bScheduleTransfer)  
																{
																	// nothing to remove first from direct
																	// update direct, queue the transfer
																	char pszFullPath[MAX_PATH];
																	sprintf(pszFullPath, "%s\\%s", pfmdo->m_sz_vc256_sys_filepath, szFile);
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "Queueing %s", pszFullPath);  Sleep(50); //(Dispatch message)

																	if(pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch<0)
																	{
																		pdirect->m_data.ScheduleEndpointQueue(pdirect->m_data.m_pCheckEvent, pszFullPath, pdirect->m_data.m_pCheckEvent->m_pszMainFile, 1);
																	}
																	else
																	{
																		pdirect->m_data.ScheduleEndpointQueue(pdirect->m_data.m_pCheckEvent, pszFullPath, pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch], 1);
																	}
																}

																delete pfmdo;
																pfmdo = NULL;

																if(bScheduleTransfer)  // otherwise we override the deletion!
																{
																	// need to recompile search files with a code.
																	pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 3);
																	pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 2, true);
					//												pdirect->m_data.m_pTransferEvent = pdirect->m_data.m_pCheckEvent;
					//												pdirect->m_data.m_pCheckEvent = NULL;
																	//pdirect->SendMsg(int nType, char* pszSender, char* pszMessage, ...);
																}
															}  // metadata item found
														} while(!bFileMetadataFound);

													} // else rule ID not found
													else
													{
														// set to error.
													  pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false, nAuxData); 
													}
												}
												else // else // not prospero, so not supported yet.
												{
												  pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false); 
												}
											}
											else  // not the module
											{
											  pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false); 
											}

////////////////////////////////////////////////////

										} // data obj pointer exists
										// if so, schedule the transfer in pdirect->
										// update 
									} break;
								case 5: //     yellow    File transfer completed  but we arent finished yet, so yellow
									{
										 // have to see if there are further ones, if so set to 1, else set to 7/6
//										pdirect->m_data.ReleaseRecordSet();

//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "current index * * *  %d", pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch);  Sleep(50); //(Dispatch message)
										if(pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch < 0)
										{
											// the first transfer has been successfully conducted.
											// we want to make sure there are no "child events" necessary to look forward to.

											int nDestModule= pdirect->m_data.ReturnModuleIndex(pdirect->m_data.m_pCheckEvent, errorstring);

											CString szDestID = "Destinations";  // we didn't find it. just use the default name
											if(nDestModule>=0)
											{
												szDestID = pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination;

												if((pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType&DIRECT_DEP_EDGE_MASK)==DIRECT_DEP_EDGE_PROSPERO)
												{
												// if this is an intuition template, we want to get it from the miranda and parse it.

													// we have the rule ID.
													// let's get the type

													int nRuleID = atoi(pdirect->m_data.m_pCheckEvent->m_sz_n_rule_id);
													int nRuleIndex = pdirect->m_data.GetRuleIndex(nRuleID);

													int nAuxData = 	atol(pdirect->m_data.m_pCheckEvent->m_sz_n_app_data_aux);

													if(nRuleIndex>=0)
													{
														if(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulDestinationType == DIRECT_RULE_DESTTYPE_MIRANDA_INT)
														{
															// we have an intuition template. (at least, we better).
															char pszParsePath[MAX_PATH];
															sprintf( pszParsePath, "%s%s.tem", pdirect->m_settings.m_pszSystemFolderPath, pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename );
															if(nAuxData&DIRECT_EVENTSTATUS_TEM)
															{
																// we found it before, so this must be the local transfer to system dir succeeding.
																// so we have to parse it
																nAuxData|=DIRECT_EVENTSTATUS_TEMLOC;
																
															//	char** ppszChildren=NULL;
															//	int nNumChildren=0;

																// so we want to make sure there is only the main, file, all children are determined now.
																if(pdirect->m_data.m_pCheckEvent->m_ppszChildren)
																{
																	int i=0;
																	while(i<pdirect->m_data.m_pCheckEvent->m_nNumChildren)
																	{
																		if(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]) free(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]);
																		i++;
																	}
																	delete [] pdirect->m_data.m_pCheckEvent->m_ppszChildren;
																	pdirect->m_data.m_pCheckEvent->m_ppszChildren = NULL;
																}
																if(pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus) delete [] pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus;
																pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus = NULL;
																pdirect->m_data.m_pCheckEvent->m_nNumChildren=0;



/////////////// inline same code for now from CIS2Comm::UtilParseTem
//  CIS2Comm::UtilParseTem

// need to change this code to check for uniqueness so we dont keep rechecking the same stuff over and over.

	FILE* fp = fopen(pszParsePath, "rb");
	if(fp)
	{
		// determine file size
		fseek(fp, 0, SEEK_END);
		unsigned long ulFileLen = ftell(fp);

		char* pch = (char*) malloc(ulFileLen+1); // term zero
		if(pch)
		{
			fseek(fp, 0, SEEK_SET);
			fread(pch, sizeof(char), ulFileLen, fp);
			*(pch+ulFileLen) = 0; // term zero

			// now we have a buffer
			char pszFilename[MAX_PATH];

			char* pchElement = NULL;
			pchElement = strstr(pch, "<Box");
			while(pchElement)
			{
				char* pchFinish = NULL;
				pchFinish = strstr(pchElement, "</Box>");  // end of the box tag
				if(pchFinish)
				{
					// well formed tag, all the info is in there
					// let's parse it.
					char* pchField = NULL;
					pchField = strstr(pchElement, "FileName=\"");  // start of the filename.  Gets font files too.
					if((pchField)&&(pchField<pchFinish))
					{
						// we have an element
						pchField += strlen("FileName=\"");  // start of the filename.  Gets font files too.
						pchElement = strstr(pchField, "\"");  // end of the filename 

						if((pchElement)&&(pchElement<pchFinish))
						{
							int i=pchElement-pchField;
							if(i>0)
							{
								memset(pszFilename, 0, MAX_PATH);
								memcpy(pszFilename, pchField, i);

								char* pszPartn = pdirect->m_data.ReturnPartition(pdirect->m_data.m_pCheckEvent, pszFilename);
								if((pszPartn)&&(strlen(pszPartn)))
								{
									char* pchChild = (char*)malloc(strlen(pszPartn)+strlen(pszFilename)+2);
									if(pchChild)
									{
										sprintf(pchChild, "%s/%s", pszPartn, pszFilename);

										bool bFoundDuplicate = false;
										i=0;
										while((!bFoundDuplicate)&&(i<pdirect->m_data.m_pCheckEvent->m_nNumChildren))
										{
											if((pdirect->m_data.m_pCheckEvent->m_ppszChildren)&&(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]))
											if(stricmp(pchChild,pdirect->m_data.m_pCheckEvent->m_ppszChildren[i])==0)
											{
												bFoundDuplicate=true;
											}
											else i++;
										}

										if(!bFoundDuplicate)
										{
											if(pdirect->m_data.m_pCheckEvent->AddChild(pchChild, 0)<0)
											{
												free(pchChild);
											}
										}
										else
										{
											free(pchChild); // just let it go.
										}
									}
								}
								if(pszPartn) free(pszPartn);
							}
						}
					}
					pchElement = strstr(pchFinish, "<Box");  // next box tag
				}
				else pchElement = NULL ; //breaks
			}
			free(pch);
		}
		fclose(fp);
	}

//  from CIS2Comm::UtilParseTem
////////////////////////////////////////////////////////////////

	_unlink(pszParsePath);  // delete the temp file, we are finished with it.



/*
// the call to CIS2Comm::UtilParseTem causes internal memory problems indie the function.
// no idea why, but it was more efficient in any case to use the inline code above, to avoid a bunch of malloc and free, new and delete.

																if(g_is2.UtilParseTem(pszParsePath, &ppszChildren, &nNumChildren)>=OX_SUCCESS)
																{
																	// get all the partitions etc
																	int nIndex=0;
																	while(nIndex<nNumChildren)
																	{
																		if((ppszChildren)&&(ppszChildren[nIndex]))
																		{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "child %s", ppszChildren[nIndex]);  //Sleep(50); //(Dispatch message)
																			char* pszPartn = pdirect->m_data.ReturnPartition(pdirect->m_data.m_pCheckEvent, ppszChildren[nIndex]);
																			if((pszPartn)&&(strlen(pszPartn)))
																			{
																				char* pchChild = (char*)malloc(strlen(pszPartn)+strlen(ppszChildren[nIndex])+2);
																				if(pchChild)
																				{
																					sprintf(pchChild, "%s/%s", pszPartn, ppszChildren[nIndex]);
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "adding child  %s", pchChild);  Sleep(50); //(Dispatch message)

																					if(pdirect->m_data.m_pCheckEvent->AddChild(pchChild, 0)<0)
																					{
																						free(pchChild);
																					}
																				}
																			}
																			if(pszPartn) free(pszPartn);
																		}
																		nIndex++;
																	}

																	if(ppszChildren)
																	{
																		nIndex=0;
																		while(nIndex<nNumChildren)
																		{
																			if(ppszChildren[nIndex]) free(ppszChildren[nIndex]);
																			nIndex++;
																		}
																		delete [] ppszChildren;
																	}
																}
*/
																if(pdirect->m_data.m_pCheckEvent->m_nNumChildren==0) // we are finished.
																{
																	// weird, because this is a template. but oh well.
																	pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 6, false, true, nAuxData); 
																}
																else  // go to the next.
																{
																	pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch = 0; // the first.
																	pdirect->m_data.ScheduleEndpointQueue(pdirect->m_data.m_pCheckEvent, "", pdirect->m_data.m_pCheckEvent->m_ppszChildren[0], 64); // first file.
																	pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 1);
																	pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 1, true, true, nAuxData);  // 1 = checking event
																}
															}
															else
															{
																// it checked out successfully, but we have to get it and parse it.
																nAuxData|=DIRECT_EVENTSTATUS_TEM;
																pdirect->m_data.ScheduleEndpointQueue(pdirect->m_data.m_pCheckEvent, pszParsePath, pdirect->m_data.m_pCheckEvent->m_pszMainFile, 2);
																pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 2, false, false, nAuxData);  // 2 was transfer queued
															}
														}
														else  // we have checked the first file on an imagestore type.
														{
															bool bVideoFound = false;
															bool bAudioFound = false;

															// check which partition it was.
															if(strncmp(pdirect->m_data.m_pCheckEvent->m_pszMainFile, "$VIDEO", 6)==0)
															{
																bVideoFound= true;
																nAuxData|=DIRECT_EVENTSTATUS_VIDEO;
															}
															else
															if(strncmp(pdirect->m_data.m_pCheckEvent->m_pszMainFile, "$AUDIO", 6)==0)
															{
																bAudioFound = true;
																nAuxData|=DIRECT_EVENTSTATUS_AUDIO;
															}

															// need to check if we are doing null oxt or not.
															if(g_pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_usActionType == DIRECT_RULE_ACTION_NULLOXT)
															{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "NULLOXT");   //(Dispatch message)
																// has to be audio first with null oxt.
																if(bAudioFound)
																{
																	bVideoFound = false;
																	// now look for the video.
																	// find the next video file
																	char* pszSearchExts = NULL; 

																	switch(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulDestinationType)
																	{
																	case DIRECT_RULE_DESTTYPE_MIRANDA_INT:
																		{
																			pszSearchExts = pdirect->m_settings.m_pszIntuitionSearchExt;
																		} break; // Intuition
																	case DIRECT_RULE_DESTTYPE_MIRANDA_IS300:	 // Imagestore 300
																		{
																			pszSearchExts = pdirect->m_settings.m_pszImagestore300SearchExt;
																		} break; 
																	case DIRECT_RULE_DESTTYPE_MIRANDA_ISHD:		 // Imagestore HD
																		{
																			pszSearchExts = pdirect->m_settings.m_pszImagestoreHDSearchExt;
																		} break; 
																	case DIRECT_RULE_DESTTYPE_MIRANDA_IS2:		//Imagestore2
																		{
																			pszSearchExts = pdirect->m_settings.m_pszImagestore2SearchExt;
																		} break;
																	} // switch

																	if(pszSearchExts)
																	{
																		char pszTestFilename[MAX_PATH];
																		CSafeBufferUtil sbu;
																		char* pszExt = sbu.Token(pszSearchExts, strlen(pszSearchExts), ",;");
																		while(pszExt)
																		{ 
																			sprintf(pszTestFilename,"%s.%s", pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename, pszExt); 
																			char* pszPartn = pdirect->m_data.ReturnPartition(pdirect->m_data.m_pCheckEvent, pszTestFilename);
																			if((pszPartn)&&(strlen(pszPartn)))
																			{
																				if(strncmp(pszPartn, "$VIDEO", 6)==0)
																				{
																					char* pchChild = (char*)malloc(strlen(pszPartn)+strlen(pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename)+strlen(pszExt)+3);
																					if(pchChild)
																					{
																						sprintf(pchChild, "%s/%s.%s", pszPartn, pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename, pszExt);
																						if(pdirect->m_data.m_pCheckEvent->AddChild(pchChild, 0)<0)
																						{
																							free(pchChild);
																						}
																						else
																						{
																							// success!
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "NULLOXT is queueing %s", pchChild);   //(Dispatch message)
																							bVideoFound = true;
																							pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch = 0; // the first.
																							pdirect->m_data.ScheduleEndpointQueue(pdirect->m_data.m_pCheckEvent, "", pdirect->m_data.m_pCheckEvent->m_ppszChildren[0], 64); // first file.
																							pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 1);
																							pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 1, true, true, nAuxData);  // check existence

																							if(pszPartn)
																							{
																								free(pszPartn);
																								pszPartn = NULL;
																							}

																							break;
																						}
																					}
																				}
																			}
																			if(pszPartn) free(pszPartn);

																			pszExt = sbu.Token(NULL, NULL, ",;");
																		}
																	}

																	if(!bVideoFound)
																	{
																		if(
																			   (pdirect->m_settings.m_pszImagestoreNullFile)&&(strlen(pdirect->m_settings.m_pszImagestoreNullFile))
																			 &&(pdirect->m_settings.m_pszSystemFolderPath)&&(strlen(pdirect->m_settings.m_pszSystemFolderPath))
																			)
																		{
																		// nothing more to find, have to transfer the NULL OXT.

																			char* pchChild = (char*)malloc(strlen("$VIDEO/X.oxt")+strlen(pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename));
																			if(pchChild)
																			{
																				if(pdirect->m_data.m_pCheckEvent->m_ppszChildren)
																				{
																					int i=0;
																					while(i<pdirect->m_data.m_pCheckEvent->m_nNumChildren)
																					{
																						if(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]) free(pdirect->m_data.m_pCheckEvent->m_ppszChildren[i]);
																						i++;
																					}
																					delete [] pdirect->m_data.m_pCheckEvent->m_ppszChildren;
																					pdirect->m_data.m_pCheckEvent->m_ppszChildren=NULL;
																				}

																				if(pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus)
																				{
																					delete [] pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus;
																					pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus=NULL;
																				}

																				pdirect->m_data.m_pCheckEvent->m_nNumChildren=0;


																				sprintf(pchChild, "$VIDEO/%s.oxt", pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename);
																				if(pdirect->m_data.m_pCheckEvent->AddChild(pchChild, 0)<0)
																				{
																					free(pchChild);
																				}
																				else
																				{
																					bVideoFound = true;
																					pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch = 0; // the first.
																					pdirect->m_data.ScheduleEndpointQueue(pdirect->m_data.m_pCheckEvent, "", pdirect->m_data.m_pCheckEvent->m_ppszChildren[0], 64); // first file.
																					pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 1);
																					pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 1, true, true, nAuxData); // when it isnt found, it will go null oxt
																				}
																			}
																		}
																	}
																	if(!bVideoFound)
																	{
																		// we really had a problem.
																		pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false, nAuxData); 
																	}
																}
																else  //what did we find?
																{
																	pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false, nAuxData); 
																}
															}
															else  // its just regular.
															{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "NORMAL action, not NULLOXT");   //(Dispatch message)
																//we found the first file, what is next?
																if(g_pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_usSearchType==DIRECT_RULE_SEARCH_EXPLICIT)
																{
																	//we are good.//
																	if(bAudioFound) pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 7, false, false, nAuxData); 
																	else  pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 6, false, false, nAuxData); 
																}
																else
																if(g_pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_usSearchType==DIRECT_RULE_SEARCH_AUDIO)
																{
																	if(bAudioFound) pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 7, false, false, nAuxData); 
																	else // ? we found something else???  error?
																	if(bVideoFound) pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 6, false, false, nAuxData); 
																	else pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false, nAuxData); 
																}
																else
																if(g_pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_usSearchType==DIRECT_RULE_SEARCH_VIDEO)
																{
																	if(bVideoFound) pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 6, false, false, nAuxData); 
																	else // ? we found something else???  error?
																	if(bAudioFound) pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 7, false, false, nAuxData); 
																	else pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false, nAuxData); 
																}
																else
																if(g_pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_usSearchType==DIRECT_RULE_SEARCH_AV)
																{
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "search type AV");   //(Dispatch message)
																// video first, then audio.
																	if(bVideoFound)
																	{
//	pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "vid found, looking for audio");   //(Dispatch message)
																		// now look for the audio.
																		// find the next audio file
																		bAudioFound = false;
																		char* pszSearchExts = NULL; 

																		switch(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulDestinationType)
																		{
																		case DIRECT_RULE_DESTTYPE_MIRANDA_INT:
																			{
																				pszSearchExts = pdirect->m_settings.m_pszIntuitionSearchExt;
																			} break; // Intuition
																		case DIRECT_RULE_DESTTYPE_MIRANDA_IS300:	 // Imagestore 300
																			{
																				pszSearchExts = pdirect->m_settings.m_pszImagestore300SearchExt;
																			} break; 
																		case DIRECT_RULE_DESTTYPE_MIRANDA_ISHD:		 // Imagestore HD
																			{
																				pszSearchExts = pdirect->m_settings.m_pszImagestoreHDSearchExt;
																			} break; 
																		case DIRECT_RULE_DESTTYPE_MIRANDA_IS2:		//Imagestore2
																			{
																				pszSearchExts = pdirect->m_settings.m_pszImagestore2SearchExt;
																			} break;
																		} // switch


																		if(pszSearchExts)
																		{
																			char pszTestFilename[MAX_PATH];
																			CSafeBufferUtil sbu;
																			char* pszExt = sbu.Token(pszSearchExts, strlen(pszSearchExts), ",;");
																			while(pszExt)
																			{ // cycles to the last one.
																				sprintf(pszTestFilename,"%s.%s", pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename, pszExt); 
																				char* pszPartn = pdirect->m_data.ReturnPartition(pdirect->m_data.m_pCheckEvent, pszTestFilename);
																				if((pszPartn)&&(strlen(pszPartn)))
																				{
																					if(strncmp(pszPartn, "$AUDIO", 6)==0)
																					{
																						char* pchChild = (char*)malloc(strlen(pszPartn)+strlen(pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename)+strlen(pszExt)+3);
																						if(pchChild)
																						{
																							sprintf(pchChild, "%s/%s.%s", pszPartn, pdirect->m_data.m_pCheckEvent->m_sz_vc256_filename, pszExt);
																							if(pdirect->m_data.m_pCheckEvent->AddChild(pchChild, 0)<0)
																							{
																								free(pchChild);
																							}
																							else
																							{
//pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "looking for %s",pchChild);   //(Dispatch message)

																								// success!
																								bAudioFound = true;
																								pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch = 0; // the first.
																								pdirect->m_data.ScheduleEndpointQueue(pdirect->m_data.m_pCheckEvent, "", pdirect->m_data.m_pCheckEvent->m_ppszChildren[0], 64); // first file.
																								pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 1);
																								pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 1, true, true, nAuxData); // 1 is check for file

																								if(pszPartn)
																								{
																									free(pszPartn);
																									pszPartn = NULL;
																								}

																								break;
																							}
																						}
																					}
																				}
																				if(pszPartn) free(pszPartn);

																				pszExt = sbu.Token(NULL, NULL, ",;");
																			}
																		}
																		if(!bAudioFound)
																		{
																			// nothing more to find, we are fine.  just set status.
																			pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 6, false, false, nAuxData); 
																		}
																	}
																	else  // what did we find?
																	{
																	  pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false, nAuxData); 
																	}
																}
															}
														}
													} // else rule ID not found
													else
													{
														// set to error.
													  pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false, nAuxData); 
													}
												}
												else // else // not prospero, so not supported yet.
												{
												  pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false); 
												}
											}
											else  // not the module
											{
											  pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false); 
											}
										}
										else // not the first file.
										{
											// we have to figure out the deal.
											// if this is an intuition thing, we just have to go to the next file,
											int nDestModule= pdirect->m_data.ReturnModuleIndex(pdirect->m_data.m_pCheckEvent, errorstring);

											CString szDestID = "Destinations";  // we didn't find it. just use the default name
											if(nDestModule>=0)
											{
												szDestID = pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination;

												if((pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType&DIRECT_DEP_EDGE_MASK)==DIRECT_DEP_EDGE_PROSPERO)
												{
													int nRuleID = atoi(pdirect->m_data.m_pCheckEvent->m_sz_n_rule_id);
													int nRuleIndex = pdirect->m_data.GetRuleIndex(nRuleID);

													int nAuxData = 	atol(pdirect->m_data.m_pCheckEvent->m_sz_n_app_data_aux);

													if(nRuleIndex>=0)
													{
														if(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_ulDestinationType == DIRECT_RULE_DESTTYPE_MIRANDA_INT)
														{
															pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch++;
															if (pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch>=pdirect->m_data.m_pCheckEvent->m_nNumChildren)
															{
					//											done!
																	//	case 6: //     green    File(s) checked and present, no audio
																	//  case 7: //     blue     File(s) checked and present, with audio 

																pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 6, false);  //?false?
																// get rid of the transfer object
					//											delete (pdirect->m_data.m_pCheckEvent);
					//											pdirect->m_data.m_pCheckEvent = NULL;
															}
															else
															{
																// go to next file.
//																pdirect->m_data.ReleaseRecordSet();
																pdirect->m_data.UpdateSearchFiles(pdirect->m_data.m_pCheckEvent, 1);
																pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 1, true, true);
																// put the request into the Queue!
																if(pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch<0)
																{
																	pdirect->m_data.ScheduleEndpointQueue(pdirect->m_data.m_pCheckEvent, "", pdirect->m_data.m_pCheckEvent->m_pszMainFile, 64);
																}
																else
																{
																	pdirect->m_data.ScheduleEndpointQueue(pdirect->m_data.m_pCheckEvent, "", pdirect->m_data.m_pCheckEvent->m_ppszChildren[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch], 64);
																}
															}

														}
														else // an imagestore type.
														{
															// we have to figure out the deal.
															// if null oxt, it means we transferred a video file.
															// if not null oxt, it means we transferred an audio file, probably..
															// but either way, we have transferred a second file.  OR SKIPPED IT!
															// so that means, we are done!  for imagestore types, we transfer one of each type and thats it.
															//	case 6: //     green    File(s) checked and present, no audio
															//  case 7: //     blue     File(s) checked and present, with audio 

															//Because of skipped audio, we have to determine if we did that or not.

															if(pdirect->m_data.m_ppRulesObj[nRuleIndex]->m_usActionType == DIRECT_RULE_ACTION_NULLOXT)
															{
																// must be transferred
																pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 7, false, false, nAuxData);
															}
															else
															{
																// regular search, can be transfer or skip, must check
																if(
																	  (pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch<pdirect->m_data.m_pCheckEvent->m_nNumChildren)
																	&&(pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus)
																	)
																{
/*
// file status codes:
0 not yet checked
1 checking
2 checked and present
3 transferring
4 transferred
5 error
6 skipped
*/
																	if(
																		  (pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch]==6) // skipped.  I think this has the only chance of being true.  its either this, or any of 1 thru 4
																		||(pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch]==0)   // we didnt check?  I think this wont ever be true.
																		||(pdirect->m_data.m_pCheckEvent->m_pnChildrenStatus[pdirect->m_data.m_pCheckEvent->m_nCurrentFileSearch]==5)  // error but we dont care.  I think this will never be true.
																		)
																	{
																		pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 6, false, false, nAuxData);  // no audio
																	}
																	else
																	{
																		pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 7, false, false, nAuxData);  //?false?
																	}
																}
																else
																{// oooo assumption.
																	pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, 7, false, false, nAuxData);  //?false?
																}

															}
														}
													} // else rule ID not found
													else
													{
														// set to error.
													  pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false, nAuxData); 
													}
												}
												else // else // not prospero, so not supported yet.
												{
												  pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false); 
												}
											}
											else  // not the module
											{
											  pdirect->m_data.SimpleSetStatus(pdirect->m_data.m_pCheckEvent, -3, false, false); 
											}
										}
										// update.
									} break;
								case 6: //     green    File(s) checked and present, no audio
								case 7: //     blue     File(s) checked and present, with audio 
									{
//										final goodness, just skip
									} break;
								} // switch(nStatus)
							} // else its the transfer event, so skip
						}
						// else its a non direct event, we skip.
					}//if(pdirect->m_data.m_pCheckEvent->m_sz_n_itemid.GetLength()>0)

					if(pdirect->m_data.m_prsEvents)
					{
						pdirect->m_data.m_prsEvents->MoveNext();
						pdirect->m_data.m_nEventCheckIndex++;
						if(pdirect->m_data.m_nEventCheckIndex>pdirect->m_data.m_nEventLastMax) // dont go back, this may simply have been an error check.
							pdirect->m_data.m_nEventLastMax = pdirect->m_data.m_nEventCheckIndex;
					} // else we closed it earlier, so just escape.
				}
				else // we finished everything., recordset was null or eof
				{
// pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "releasing the recordset");  Sleep(50);//(Dispatch message)
					// no need to delete the check event, it will get reset.
					pdirect->m_data.ReleaseRecordSet();  // ok we had it false, but then we never check the last one.
					// so, just keep pulling.  it will be fine.  anyway, automation will be changing.
//					pdirect->m_data.ReleaseRecordSet(false);  
				//			pdirect->m_data.m_nEventCheckIndex = -1; // but dont reset this!  we dont want to pull again until somethign changes in automation - we are finished!
					pdirect->m_data.m_nEventLastMax=0; // have to reset this now too, so it will continue going around
				}

		//   { 
		//     Close the m_prsEvents recordset and delete it, set it back to NULL
		//     go thru all the rules.
		//     rule {
		//            if parses, determine type, and then determine host. (SQL QUERY)
		//            if(direct) 
		//            {
		//              if explicit search, just search for the one file
		//               else
		//              switch action 
		//               NULLOXT: {if intuition error.  search for oxe/oxw first.  if not exists, error.  if exists, search for oxa, if exists, ok, if not search OXT.  if exists ok, else create copy of nulloxt and transfer.}
		//               normal:  {if intuition, search for tem, if found, parse and go thru file list (add to search_files col).  if ISx, search for OXA, OXT, OXE/OXW  }
		//            } else not supported yet
		//          }  // if more than one rule, add additional event to events list event is by dest.
		//       set incrementors to - 1, escape
		//   }
		//   check for status = final decision = good.  
		//   check for the file on edge device, means:
		//     { find the right miranda, queue a check existence, escape with incrementor }
		//   if not there and transfer incrementor is -1,
		//     { check for the file in the store, 
		//       error out if not there, else
		//       check miranda device for disk space (SQL query), 
		//         delete as necessary until space exists. delete means use statistics, schedule delete in queue. escape with incrementor.
		//         if space cannot be made, error out.
		//		   queue file for transfer, set transfer inc to inc.
		//     }
		//   else increment inc (not xfer inc)
		//   escape
		// }

			//}//			else  // no automation changes

		} //		if(
//			  (pdirect->m_data.m_nIndexAutomationEndpoint>=0)
//			&&(pdirect->m_settings.m_nNumEndpointsInstalled>0)
///			&&(pdirect->m_settings.m_ppEndpointObject)
//			&&(pdirect->m_settings.m_ppEndpointObject[pdirect->m_data.m_nIndexAutomationEndpoint])
//			&&(!pdirect->m_data.m_bProcessSuspended)
//			&&(pdirect->m_data.m_key.m_bValid)  // must have a valid license
//			&&(
//					(!pdirect->m_data.m_key.m_bExpires)
//				||((pdirect->m_data.m_key.m_bExpires)&&(!pdirect->m_data.m_key.m_bExpired))
//				||((pdirect->m_data.m_key.m_bExpires)&&(pdirect->m_data.m_key.m_bExpireForgiveness)&&(pdirect->m_data.m_key.m_ulExpiryDate+pdirect->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
//				)
//			&&(
//					(!pdirect->m_data.m_key.m_bMachineSpecific)
//				||((pdirect->m_data.m_key.m_bMachineSpecific)&&(pdirect->m_data.m_key.m_bValidMAC))
//				)
//			)


		Sleep(1); // dont peg processor
	} // while(!g_bKillThread)
	pdirect->m_data.m_bAnalysisThreadStarted=false;
	_endthread();

}
