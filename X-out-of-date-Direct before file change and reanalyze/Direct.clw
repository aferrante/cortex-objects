; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDirectDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Direct.h"
LastPage=0

ClassCount=6
Class1=CAboutDlg
Class2=CDirectApp
Class3=CDirectHandler
Class4=CDirectDlg
Class5=CDirectSettings

ResourceCount=3
Resource1=IDR_MENU1
Resource2=IDD_DIRECT_DIALOG
Resource3=IDD_ABOUTBOX

[CLS:CAboutDlg]
Type=0
HeaderFile=sentineldlg.h
ImplementationFile=sentineldlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CAboutDlg

[CLS:CDirectApp]
Type=0
BaseClass=CWinApp
HeaderFile=Direct.h
ImplementationFile=Direct.cpp
Filter=N
VirtualFilter=AC
LastObject=CDirectApp

[CLS:CDirectHandler]
Type=0
BaseClass=CWnd
HeaderFile=DirectHandler.h
ImplementationFile=DirectHandler.cpp
Filter=W
VirtualFilter=WC
LastObject=CDirectHandler

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=8
Control1=IDC_STATICTEXT_TITLE,static,1342308481
Control2=IDC_STATICTEXT_URL,static,1208090625
Control3=IDOK,button,1342373889
Control4=IDC_STATIC_LOGO,static,1342177294
Control5=IDC_URLFRAME,static,1342177298
Control6=IDC_STATIC_BUILD,static,1342308353
Control7=IDC_STATIC_URL,static,1342177294
Control8=IDC_STATICTEXT_COPYRIGHT,static,1342308353

[MNU:IDR_MENU1]
Type=1
Class=?
Command1=ID_CMD_ABOUT
Command2=ID_CMD_SHOWWND
Command3=ID_CMD_EXIT
Command4=ID_CMD_ABOUT
Command5=ID_CMD_SHOWWND
Command6=ID_CMD_EXIT
CommandCount=6

[CLS:CDirectDlg]
Type=0
HeaderFile=DirectDlg.h
ImplementationFile=DirectDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDCANCEL

[CLS:CDirectSettings]
Type=0
HeaderFile=DirectSettings.h
ImplementationFile=DirectSettings.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CDirectSettings

[DLG:IDD_DIRECT_DIALOG]
Type=1
Class=CDirectDlg
ControlCount=7
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_SETTINGS,button,1208025216
Control4=IDC_LIST1,SysListView32,1350681613
Control5=IDC_STATIC_STATUSTEXT,static,1342308352
Control6=IDC_STATIC_PROGBAR,static,1342177294
Control7=IDC_TREE1,SysTreeView32,1082130487

