// ConnectorData.cpp: implementation of the CConnectorData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <process.h>
#include "Connector.h"
#include "ConnectorHandler.h" 
#include "ConnectorMain.h" 
#include "ConnectorData.h"
//#include "..\Archivist\ArchivistDefines.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CConnectorMain* g_pconnector;
extern CConnectorApp theApp;

extern void ConnectorAutomationThread(void* pvArgs);
//extern void ConnectorFarAnalysisThread(void* pvArgs);
extern void ConnectorNearAnalysisThread(void* pvArgs);
extern void ConnectorTriggerThread(void* pvArgs);


//extern CADC g_adc; 	// the Harris ADC object

/*
//////////////////////////////////////////////////////////////////////
// CConnectorConnectionObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CConnectorConnectionObject::CConnectorConnectionObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_pszClientName = NULL;
	m_ulStatus	= CONNECTOR_STATUS_UNINIT;
	m_ulFlags = CONNECTOR_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
//	m_pAPIConn = NULL;
	m_bKillConnThread = true;
	m_bConnThreadStarted = false;

}

CConnectorConnectionObject::~CConnectorConnectionObject()
{
	m_bKillConnThread = true;
	while(	m_bConnThreadStarted ) Sleep(1);

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
	if(m_pszClientName) free(m_pszClientName); // must use malloc to allocate
}

*/


//////////////////////////////////////////////////////////////////////
// CConnectorMappingObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CConnectorMappingObject::CConnectorMappingObject()
{
	m_nMappingID  =-1;
}

CConnectorMappingObject::~CConnectorMappingObject()
{
//	if(m_pszFieldName) free(m_pszFieldName); // must use malloc to allocate
}

//////////////////////////////////////////////////////////////////////
// CConnectorData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CConnectorData::CConnectorData()
{
	m_bNetClientConnected = false;
	m_socket = NULL;

	InitializeCriticalSection(&m_critText);
	
	// messaging...
	m_bNetworkMessagingInitialized=false;

/*
	m_ppEventsList=NULL;
	m_nNumEventsList=0;
	m_ppEventsNewList=NULL;
	m_nNumEventsNewList=0;
	m_ppEventsPlaying=NULL;
	m_nNumEventsPlaying=0;
*/

//	m_ppRulesObj = NULL;
//	m_nNumRulesObjects = 0;
	m_ppMappingObj = NULL;
	m_nNumMappingObjects = 0;


	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebAutoPurge ); // the last time autopurge was run
	_ftime( &m_timebTick );
	m_pszHost = NULL;			// the name of the host
//	m_pszCortexHost = NULL;			// the name of the cortex host
	m_pszCompleteHost = NULL;	// the complete name of the host
//	m_usCortexCommandPort = CONNECTOR_PORT_CMD;
//	m_usCortexStatusPort = CONNECTOR_PORT_STATUS;

	m_bQuietKill = false;

	m_bProcessSuspended = false;
}

CConnectorData::~CConnectorData()
{
/*
	if(m_ppConnObj)
	{
		int i=0;
		while(i<m_nNumConnectionObjects)
		{
			if(m_ppConnObj[i]) delete m_ppConnObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppConnObj; // delete array of pointers to objects, must use new to allocate
	}
*/


	if(m_ppMappingObj)
	{
		int i=0;
		while(i<m_nNumMappingObjects)
		{
			if(m_ppMappingObj[i]) delete m_ppMappingObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppMappingObj; // delete array of pointers to objects, must use new to allocate
	}
	if(m_pszHost) free(m_pszHost);	// must use malloc to allocate
	if(m_pszCompleteHost) free(m_pszCompleteHost);	// must use malloc to allocate
//	if(m_pszCortexHost) free(m_pszCortexHost);	// must use malloc to allocate

	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);
}

char* CConnectorData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CConnectorData::SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError)
{
	int nRV = CX_ERROR;
	bool bError=false;

	if(!bOverwriteExistingError)
	{
		if(
			  ((m_ulFlags&CONNECTOR_ICON_MASK) == CONNECTOR_STATUS_ERROR)
			||((m_ulFlags&CONNECTOR_STATUS_CMDSVR_MASK) == CONNECTOR_STATUS_CMDSVR_ERROR)
			||((m_ulFlags&CONNECTOR_STATUS_STATUSSVR_MASK) ==  CONNECTOR_STATUS_STATUSSVR_ERROR)
			||((m_ulFlags&CONNECTOR_STATUS_THREAD_MASK) == CONNECTOR_STATUS_THREAD_ERROR)
			||((m_ulFlags&CONNECTOR_STATUS_FAIL_MASK) == CONNECTOR_STATUS_FAIL_DB)
			)	bError=true;
	}

	if (ulStatus&CONNECTOR_ICON_MASK)
	{
		m_ulFlags &= ~CONNECTOR_ICON_MASK;
		m_ulFlags |= (ulStatus&CONNECTOR_ICON_MASK);
	}
	if (ulStatus&CONNECTOR_STATUS_CMDSVR_MASK)
	{
		m_ulFlags &= ~CONNECTOR_STATUS_CMDSVR_MASK;
		m_ulFlags |= (ulStatus&CONNECTOR_STATUS_CMDSVR_MASK);
	}
	if (ulStatus&CONNECTOR_STATUS_STATUSSVR_MASK)
	{
		m_ulFlags &= ~CONNECTOR_STATUS_STATUSSVR_MASK;
		m_ulFlags |= (ulStatus&CONNECTOR_STATUS_STATUSSVR_MASK);
	}
	if (ulStatus&CONNECTOR_STATUS_THREAD_MASK)
	{
		m_ulFlags &= ~CONNECTOR_STATUS_THREAD_MASK;
		m_ulFlags |= (ulStatus&CONNECTOR_STATUS_THREAD_MASK);
	}
	if (ulStatus&CONNECTOR_STATUS_FAIL_MASK)
	{
		m_ulFlags &= ~CONNECTOR_STATUS_FAIL_MASK;
		m_ulFlags |= (ulStatus&CONNECTOR_STATUS_FAIL_MASK);
	}

	if(bError)
	{
		m_ulFlags &= ~CONNECTOR_ICON_MASK;
		m_ulFlags |= CONNECTOR_STATUS_ERROR;
	}

	if((pszText)&&(!bError))
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
//				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
//			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_pconnector->m_settings.m_pszIconPath)&&(strlen(g_pconnector->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_pconnector->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_pconnector->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_pconnector->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_pconnector->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_pconnector->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}

/*
int	CConnectorData::SetStatusText(char* pszText, unsigned long ulStatus)
{
	int nRV = CX_ERROR;
	if(pszText)
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_pconnector->m_settings.m_pszIconPath)&&(strlen(g_pconnector->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_pconnector->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_pconnector->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_pconnector->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_pconnector->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_pconnector->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}
	return nRV;
}
*/
// utility
int	CConnectorData::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszCompleteHost) free(m_pszCompleteHost);
						m_pszCompleteHost=pch;

						pch = (char*) malloc(nValue+1);
						if(pch!=NULL)
						{
							if(m_pszHost) free(m_pszHost);
							m_pszHost=pch;

							strcpy(m_pszHost, m_pszCompleteHost);
							pch = strchr(m_pszHost, '.');
							if(pch)
							{
								*pch = 0;
							}

							return CONNECTOR_SUCCESS;
						}
					}
				}
			}
		}
	}
	return CONNECTOR_ERROR;
}


/*
int CConnectorData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_pconnector)&&(g_pconnector->m_settings.m_pszExchange)&&(strlen(g_pconnector->m_settings.m_pszExchange)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];

		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT MAX(mod) AS MAX FROM %s WHERE criterion = '%s'", 
			g_pconnector->m_settings.m_pszExchange, szTemp		);

		//Sleep(500);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, dberrorstring);
		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				CString szValue;
				prs->GetFieldValue("MAX", szValue);
				if(szValue.GetLength()>0)
				{
					unsigned long ulLastMod = atol(szValue);
					if(ulLastMod>0) ulMod = ulLastMod+1;
				}
			}
			prs->Close();
			delete prs;
		}

		if(ulMod>0)
		{
			if(ulMod > CONNECTOR_DB_MOD_MAX) ulMod = 1;
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET mod = %d WHERE criterion = '%s'", // HARDCODE
				g_pconnector->m_settings.m_pszExchange, ulMod, szTemp		);
			if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
			{
				return CONNECTOR_SUCCESS;
			}
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (criterion, flag, mod) VALUES ('%s', '', 1)", // HARDCODE
				g_pconnector->m_settings.m_pszExchange, szTemp		);
			if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
			{
				return CONNECTOR_SUCCESS;
			}
		}
	}
	return CONNECTOR_ERROR;
}
*/




