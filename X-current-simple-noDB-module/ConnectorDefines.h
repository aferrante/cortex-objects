// ConnectorDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(CONNECTORDEFINES_H_INCLUDED)
#define CONNECTORDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define CONNECTOR_CURRENT_VERSION		"1.0.0.1"


// modes
#define CONNECTOR_MODE_DEFAULT			0x00000000  // exclusive
#define CONNECTOR_MODE_LISTENER		0x00000001  // exclusive
#define CONNECTOR_MODE_CLONE				0x00000002  // exclusive
#define CONNECTOR_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define CONNECTOR_MODE_VOLATILE		0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define CONNECTOR_MODE_MASK				0x0000000f  // 

// default port values.
//#define CONNECTOR_PORT_FILE				80		
#define CONNECTOR_PORT_CMD					20680		
#define CONNECTOR_PORT_STATUS			20681		

#define CONNECTOR_PORT_INVALID			0	

#define CONNECTOR_FLAGS_INVALID			0xffffffff	 
#define CONNECTOR_FLAG_DISABLED			0x0000	 // default
#define CONNECTOR_FLAG_ENABLED			0x0001	
#define CONNECTOR_FLAG_FOUND				0x1000

#define CONNECTOR_FLAG_TRIGGERED			0x01000000
#define CONNECTOR_FLAG_ANALYZED				0x00000010
#define CONNECTOR_FLAG_REANALYZED			0x00000020

#define CONNECTOR_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded

// status
#define CONNECTOR_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define CONNECTOR_STATUS_UNKNOWN							0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define CONNECTOR_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define CONNECTOR_STATUS_ERROR								0x00000020  // error (red icon)
#define CONNECTOR_STATUS_CONN								0x00000030  // ready (green icon)	
#define CONNECTOR_STATUS_OK									0x00000030  // ready (green icon)	
#define CONNECTOR_STATUS_RUN									0x00000040  // in progress, running, owned etc (blue icon);	
#define CONNECTOR_ICON_MASK									0x00000070  // mask	

#define CONNECTOR_STATUS_SUSPEND							0x00000080  // suspended	(yellow icon please)

#define CONNECTOR_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define CONNECTOR_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define CONNECTOR_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define CONNECTOR_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define CONNECTOR_STATUS_CMDSVR_MASK					0x00007000  // command server mask bits

#define CONNECTOR_STATUS_STATUSSVR_START			0x00010000  // starting the status server
#define CONNECTOR_STATUS_STATUSSVR_RUN				0x00020000  // status server running
#define CONNECTOR_STATUS_STATUSSVR_END				0x00030000  // status server shutting down
#define CONNECTOR_STATUS_STATUSSVR_ERROR			0x00040000  // status server error
#define CONNECTOR_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define CONNECTOR_STATUS_THREAD_START				0x00100000  // starting the main thread
#define CONNECTOR_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define CONNECTOR_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define CONNECTOR_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define CONNECTOR_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define CONNECTOR_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define CONNECTOR_STATUS_THREAD_MASK					0x00f00000  // main thread mask bits

// various failures...
#define CONNECTOR_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define CONNECTOR_STATUS_FAIL_DB							0x20000000  // could not get DB
#define CONNECTOR_STATUS_FAIL_MASK						0xf0000000  // failure code mask bits

//return values
#define CONNECTOR_SUCCESS   0
#define CONNECTOR_ERROR	   -1


// default filenames
#define CONNECTOR_SETTINGS_FILE_SETTINGS	  "connector.csr"		// csr = cortex settings redirect
#define CONNECTOR_SETTINGS_FILE_DEFAULT	  "connector.csf"		// csf = cortex settings file

// debug defines
#define CONNECTOR_DEBUG_TRIGGER				0x00000001
#define CONNECTOR_DEBUG_ANALYZE				0x00000002
#define CONNECTOR_DEBUG_RULES					0x00000004
#define CONNECTOR_DEBUG_PARAMS				0x00000008
#define CONNECTOR_DEBUG_PARAMPARSE		0x00000010
#define CONNECTOR_DEBUG_TIMING				0x00000020
#define CONNECTOR_DEBUG_EVENTRULE			0x00000040
#define CONNECTOR_DEBUG_PROCESS				0x00000080
#define CONNECTOR_DEBUG_CHANNELS			0x00000100
#define CONNECTOR_DEBUG_COMM					0x00000200


#endif // !defined(CONNECTORDEFINES_H_INCLUDED)
