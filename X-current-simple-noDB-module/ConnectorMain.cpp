// ConnectorMain.cpp: implementation of the CConnectorMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "Connector.h"  // just included to have access to windowing environment
#include "ConnectorDlg.h"  // just included to have access to windowing environment
#include "ConnectorHandler.h"  // just included to have access to windowing environment

#include "ConnectorMain.h"
#include <process.h>
#include "../../Common/TXT/BufferUtil.h"
//#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"

//#include "..\Sentinel\SentinelDefines.h"
//#include "..\..\Common\API\Harris\ADCDefs.h"
//#include "..\..\Common\API\Miranda\IS2Comm.h"
//#include "..\..\Common\API\Miranda\IS2Core.h"
//#include "..\Radiance\RadianceDefines.h"
//#include "..\Libretto\LibrettoDefines.h"

#include <sys/timeb.h>
#include <time.h>
#include <direct.h>
#include <objsafe.h>
#include <atlbase.h>

// have to add rpcrt4.lib to the linker for UUID stuff


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;
bool g_bKillStatus = false;
CConnectorMain* g_pconnector=NULL;
//CADC g_adc;
//CIS2Comm g_is2;  // inlined code - problems with CIS2Comm::UtilParseTem memory management . never figured this one out.

extern CMessager* g_pmsgr;  // from Messager.cpp
extern CConnectorApp theApp;



//void ConnectorConnectionThread(void* pvArgs);
//void ConnectorListThread(void* pvArgs);

void ConnectorGlobalAutomationThread(void* pvArgs);
void ConnectorGlobalAnalysisThread(void* pvArgs);
void ConnectorAutomationThread(void* pvArgs);
//void ConnectorFarAnalysisThread(void* pvArgs);
void ConnectorTriggerThread(void* pvArgs);
void ConnectorTriggerDelayNotificationThread(void* pvArgs);


#define NOT_USING_NEAR_ANALYSIS

#ifndef NOT_USING_NEAR_ANALYSIS
void ConnectorNearAnalysisThread(void* pvArgs);
#endif //#ifndef NOT_USING_NEAR_ANALYSIS


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CConnectorMain::CConnectorMain()
{
}

CConnectorMain::~CConnectorMain()
{
}

/*

char*	CConnectorMain::ConnectorTranslate(CHTTPHeader* pHeader, char* pszBuffer)			// apply connector scripting language
{
	return pszBuffer;
}

int		CConnectorMain::InterpretConnectorive(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the connectorive, such as a char buffer.
	return CONNECTOR_SUCCESS;
}
*/

SOCKET*	CConnectorMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // connector initiates a request to an object server
{
	CNetData* pdata = new CNetData;

	pdata->m_ucType = ucType;      // defined type - indicates which protocol to use, structure of data

	pdata->m_ucCmd = ucCmd;       // the command byte
	pdata->m_ucSubCmd = ucSubCmd;       // the subcommand byte

	pdata->m_pucData = pucData;
	pdata->m_ulDataLen = ulDataLen;

	SOCKET* ps = NULL; 
	m_net.SendData(pdata, pchHost, usPort, 5000, 0, NET_SND_CMDTOSVR, ps);
//	m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);

	return ps;
}

int		CConnectorMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// connector replies to an object server after receiving data. (usually ack or nak)
{
	return CONNECTOR_SUCCESS;
}

int		CConnectorMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// connector answers a request from an object client
{
	return CONNECTOR_SUCCESS;
}


void ConnectorMainThread(void* pvArgs)
{
	CConnectorApp* pApp = (CConnectorApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment
	//startup.
	g_bThreadStarted = true;

	//create the main objects.

	CConnectorMain connector;
//	CDBUtil db;

	connector.m_data.m_ulFlags &= ~CONNECTOR_STATUS_THREAD_MASK;
	connector.m_data.m_ulFlags |= CONNECTOR_STATUS_THREAD_START;
	connector.m_data.m_ulFlags &= ~CONNECTOR_ICON_MASK;
	connector.m_data.m_ulFlags |= CONNECTOR_STATUS_UNINIT;

	connector.m_data.GetHost();

	g_pconnector = &connector;


	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
//	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\Connector\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}



//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

//		AfxMessageBox( pApp->m_lpCmdLine );
/*
	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// check here for Cortex IP.
			if(strlen(pch))
			{
//		AfxMessageBox( pch );
				if(connector.m_data.m_bu.CountChar(pch, strlen(pch), ':')>=1)  // must be of form "hostname:cmdport" where hostname can be name or ip
				{
					connector.m_data.m_pszCortexHost = (char*)malloc(strlen(pch)+1);
					if(connector.m_data.m_pszCortexHost)
					{
						strcpy(connector.m_data.m_pszCortexHost, pch);
//		AfxMessageBox( connector.m_data.m_pszCortexHost );

						char* pchd = strchr(connector.m_data.m_pszCortexHost, ':');
						if(pchd)
						{
							*pchd = 0;  // null term the host name;
							pchd++;
//		AfxMessageBox( connector.m_data.m_pszCortexHost );

							char* pchd2 = strchr(pchd, ':');
							if(pchd2)
							{
								*pchd2 = 0;  // null term the cmd port;
								pchd2++;
								if(strlen(pchd2)) connector.m_data.m_usCortexStatusPort = atoi(pchd2);
							}

							if(strlen(pchd)) connector.m_data.m_usCortexCommandPort = atoi(pchd);
						}
					}

				}
			}
			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }
*/
	connector.m_settings.Settings(true); //read
/////////////////////////////////////////////////
// would comment this part out, but need to get dependencies here... once.
	char pszFilename[MAX_PATH];

	//	AfxMessageBox("3");

	strcpy(pszFilename, "");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, CONNECTOR_SETTINGS_FILE_DEFAULT);  // cortex settings file

	CFileUtil file;
	file.GetSettings(pszFilename, false); 
	char* pszParams = NULL;
// load up the values on the settings object
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(pszParams) free(pszParams); pszParams=NULL;
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
	}

	bool bUseLog = false;
	bool bUseEmail = false;
	bool bLogNetworkErrors = false;
	if(connector.m_settings.m_bUseLog)
	{
		bUseLog = connector.m_settings.m_bUseLog;

		// for logfiles, we need params, and they must be in this format:
		//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
//		pszParams = file.GetIniString("Messager", "LogFileIni", "Connector|YD||1|");
//		AfxMessageBox(pszParams);
		int nRegisterCode=0;

		nRegisterCode = connector.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
			"log", 
			connector.m_settings.m_pszProcessedFileSpec?connector.m_settings.m_pszProcessedFileSpec:connector.m_settings.m_pszFileSpec, 
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
			connector.m_data.SetStatusText(errorstring, (CONNECTOR_STATUS_FAIL_LOG|CONNECTOR_STATUS_ERROR));
		}

		if(pszParams) free(pszParams); pszParams=NULL;
	}

	connector.m_msgr.DM(MSG_ICONINFO, NULL, "Connector", "--------------------------------------------------\n\
-------------- Connector %s start --------------", CONNECTOR_CURRENT_VERSION);  //(Dispatch message)


	if(connector.m_settings.m_bUseEmail)
	{
		bUseEmail = connector.m_settings.m_bUseEmail;
				// for smtp, we need params, and they must be in this format:
				// SMTP address[: override port (optional)] | distribution lists | from email | subject spec | localhostname | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
				// note on distribution lists:
				// we want to give the option to map message type and severity to different address lists.
				// the distribution lists field format is this:
				// ff:x@x.com,q@q.com;0x:B@b.com
				// the : delimits the type and severity flag specifier from the email address list for that set. 
				// the email addresses are delimited by comma
				// the sets are delimited by ;
				// the delimiters are not allowed in addresses, as described here: http://en.wikipedia.org/wiki/E-mail_address
				// on the type and severity specifiers, it is like this:
				// the subfield is a 2 character hex code, sort of.
				// severity is the first char, possible values are 0,1,2,3,4, for normal, low, medium, high, critical
				// additionally, f means all
				// type is the second char, possible values are 0-8, for the follwing:
				// #define MSG_ICONNONE								0x00000000  // default, nothing
				// #define MSG_ICONQUESTION						0x00000001  // ? icon
				// #define MSG_ICONEXCLAMATION				0x00000002  // ! icon
				// #define MSG_ICONERROR							0x00000003  // X icon
				// #define MSG_ICONSTOP								0x00000004  // stop sign icon
				// #define MSG_ICONHAND								0x00000005  // hand icon
				// #define MSG_ICONINFO								0x00000006  // (i) icon 
				// #define MSG_ICONUSER1							0x00000007  
				// #define MSG_ICONUSER2							0x00000008  
				// ff means send all messages to the address list
				// 00 means send all messages with severity normal and no "icon" to the address list
				// 03 means send all messages with severity normal and error type to the address list
				// 43 means send all messages with severity critical and error type to the address list
				// you can set up different address lists with the same addresses - one email per dispatched message will be sent in the case of overlap.

		int nRegisterCode=0;

		// NOT a default handler!
		nRegisterCode = connector.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
			"email", 
			connector.m_settings.m_pszProcessedMailSpec?connector.m_settings.m_pszProcessedMailSpec:connector.m_settings.m_pszMailSpec,
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
			connector.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
			connector.m_msgr.DM(MSG_ICONERROR, NULL, "Connector:smtp_init", errorstring);  //(Dispatch message)
		}

//		if(pszParams) free(pszParams); pszParams=NULL;
	}

//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
//	connector.m_http.InitializeMessaging(&connector.m_msgr);
//	connector.m_net.InitializeMessaging(&connector.m_msgr);
	if(connector.m_settings.m_bLogNetworkErrors) 
	{
		bLogNetworkErrors = connector.m_settings.m_bLogNetworkErrors;
		if(connector.m_net.InitializeMessaging(&connector.m_msgr)==0)
		{
			connector.m_data.m_bNetworkMessagingInitialized=true;
		}
	}
/*

//init command and status listeners.
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing command server on %d", connector.m_settings.m_usCommandPort); 
	connector.m_data.SetStatusText(errorstring, CONNECTOR_STATUS_CMDSVR_START);
	connector.m_msgr.DM(MSG_ICONNONE, NULL, "Connector:command_server_init", errorstring);  //(Dispatch message)

	if(connector.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = connector.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "ConnectorCommandServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = ConnectorCommandHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &connector;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &connector.m_net;					// pointer to the object with the Message function.


		if(connector.m_net.StartServer(pServer, &connector.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			connector.m_data.SetStatusText(errorstring, CONNECTOR_STATUS_CMDSVR_ERROR);
			connector.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Connector:command_server_init", errorstring);  //(Dispatch message)
			connector.SendMsg(CX_SENDMSG_ERROR, "Connector:command_server_init", errorstring);
		}
		else
		{
//AfxMessageBox("Q1");
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Command server listening on %d", connector.m_settings.m_usCommandPort);
			connector.m_data.SetStatusText(errorstring, CONNECTOR_STATUS_CMDSVR_RUN);
			connector.m_msgr.DM(MSG_ICONNONE, NULL, "Connector:command_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing XML server on %d", connector.m_settings.m_usStatusPort); 
	connector.m_data.SetStatusText(errorstring, CONNECTOR_STATUS_STATUSSVR_START);
	connector.m_msgr.DM(MSG_ICONNONE, NULL, "Connector:xml_server_init", errorstring);  //(Dispatch message)

	if(connector.m_settings.m_usStatusPort>0)
	{
//AfxMessageBox("Q1");
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = connector.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "ConnectorXMLServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = ConnectorXMLHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &connector;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &connector.m_net;					// pointer to the object with the Message function.

		if(connector.m_net.StartServer(pServer, &connector.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			connector.m_data.SetStatusText(errorstring, CONNECTOR_STATUS_STATUSSVR_ERROR);
			connector.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Connector:xml_server_init", errorstring);  //(Dispatch message)
			connector.SendMsg(CX_SENDMSG_ERROR, "Connector:xml_server_init", errorstring);
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML server listening on %d", connector.m_settings.m_usStatusPort);
			connector.m_data.SetStatusText(errorstring, CONNECTOR_STATUS_STATUSSVR_RUN);
			connector.m_msgr.DM(MSG_ICONNONE, NULL, "Connector:xml_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

//AfxMessageBox("QQ");

	*/
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connector is initializing...");
	connector.m_msgr.DM(MSG_ICONNONE, NULL, "Connector:init", errorstring);  //(Dispatch message)
	connector.m_data.SetStatusText(errorstring, CONNECTOR_STATUS_THREAD_SPARK);

	// now connect all the registered servers
	// this is the list of servers in the db that get connected

/*
	if(
		  (!(connector.m_settings.m_ulMainMode&CONNECTOR_MODE_CLONE))
		&&(connector.m_data.m_key.m_bValid)  // must have a valid license
		&&(
				(!connector.m_data.m_key.m_bExpires)
			||((connector.m_data.m_key.m_bExpires)&&(!connector.m_data.m_key.m_bExpired))
			||((connector.m_data.m_key.m_bExpires)&&(connector.m_data.m_key.m_bExpireForgiveness)&&(connector.m_data.m_key.m_ulExpiryDate+connector.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
			)
		&&(
				(!connector.m_data.m_key.m_bMachineSpecific)
			||((connector.m_data.m_key.m_bMachineSpecific)&&(connector.m_data.m_key.m_bValidMAC))
			)
		)
	{
//	connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "X2");  Sleep(250);//(Dispatch message)
//		connector.m_data.GetChannels();
//	connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "X3");  Sleep(250);//(Dispatch message)
	}
//	connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "X4");  Sleep(250);//(Dispatch message)
*/	
	if((connector.m_data.m_ulFlags&CONNECTOR_ICON_MASK) != CONNECTOR_STATUS_ERROR)
	{
		connector.m_data.m_ulFlags &= ~CONNECTOR_ICON_MASK;
		connector.m_data.m_ulFlags |= CONNECTOR_STATUS_OK;  // green - we want run to be blue when something in progress
	}

//	connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "here2");  Sleep(250);//(Dispatch message)
/*
// initialize connector (make this not persistent for now)
	CNetData* pdata = new CNetData;
	if(pdata)
	{

		pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA;      // defined type - indicates which protocol to use, structure of data

		pdata->m_ucCmd = CX_REQ_HELLO;       // the command byte
		pdata->m_ucSubCmd = 0;       // the subcommand byte

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
		if(pch)
		{
			sprintf(pch, "%s:%d:%d:%d:%d:%s", 
				connector.m_data.m_pszHost,
				connector.m_settings.m_usCommandPort,
				connector.m_settings.m_usStatusPort,
				CX_TYPE_PROCESS,
				theApp.m_nPID,
				connector.m_settings.m_pszName?connector.m_settings.m_pszName:"Connector"
				);

			pdata->m_pucData =  (unsigned char*) pch;
			pdata->m_ulDataLen = strlen(pch);
		}
//	connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "here3");  Sleep(250);//(Dispatch message)

		SOCKET s = NULL; 
//AfxMessageBox("sending");
//AfxMessageBox( connector.m_data.m_pszCortexHost );
//AfxMessageBox( (char*)pdata->m_pucData );
		if(connector.m_net.SendData(pdata, connector.m_data.m_pszCortexHost, connector.m_data.m_usCortexCommandPort, 5000, 0, NET_SND_CMDTOSVR, &s)>=NET_SUCCESS)
		{
			//send ack
//			AfxMessageBox("ack");
			connector.m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);
		}
		else
		{
//			AfxMessageBox("could not send");
		}
		connector.m_net.CloseConnection(s);

		delete pdata;

	}

//	connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "here4");  Sleep(250);//(Dispatch message)
	_ftime(&connector.m_data.m_timebAutomationTick); // the last time check inside the thread
//	_ftime(&connector.m_data.m_timebNearTick); // the last time check inside the thread
//	_ftime(&connector.m_data.m_timebFarTick); // the last time check inside the thread
//	_ftime(&connector.m_data.m_timebTriggerTick); // the last time check inside the thread

*/

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connector main thread running.");  
	connector.m_data.SetStatusText(errorstring, CONNECTOR_STATUS_THREAD_RUN);
	connector.m_msgr.DM(MSG_ICONNONE, NULL, "Connector:init", "Connector is initialized, main thread is running.");  Sleep(50); //(Dispatch message)
//	connector.SendMsg(CX_SENDMSG_INFO, "Connector:init", "Connector %s main thread running.", CONNECTOR_CURRENT_VERSION);
//	connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", errorstring);  Sleep(250);//(Dispatch message)

/*

	if(_beginthread(ConnectorFarAnalysisThread, 0, (void*)&connector)==-1)
	{
		//error.  try again once
		Sleep(1000);
		if(_beginthread(ConnectorFarAnalysisThread, 0, (void*)&connector)==-1)
		{
			//report failure
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connector analysis thread failure.");  
			connector.m_data.SetStatusText(errorstring, (CONNECTOR_STATUS_THREAD_ERROR|CONNECTOR_STATUS_ERROR));
			connector.m_msgr.DM(MSG_ICONERROR, NULL, "Connector:analysis_thread_init", errorstring);  //(Dispatch message)
			connector.SendMsg(CX_SENDMSG_ERROR, "Connector:analysis_thread_init", errorstring);
		}
		else
		{
			connector.m_msgr.DM(MSG_ICONNONE, NULL, "Connector:analysis_thread_init", "Connector analysis thread initialized.");  //(Dispatch message)
		}
	}
	else
	{
		connector.m_msgr.DM(MSG_ICONNONE, NULL, "Connector:analysis_thread_init", "Connector analysis thread initialized.");  //(Dispatch message)
	}
	Sleep(250);

*/

	_timeb timebCheckMods;
	_ftime( &timebCheckMods );
///AfxMessageBox("xxxxx");

//	char szSQL[DB_SQLSTRING_MAXLEN];
	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &connector.m_data.m_timebTick );
//	connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "%d.%03d", connector.m_data.m_timebTick.time, connector.m_data.m_timebTick.millitm);   Sleep(250);//(Dispatch message)
/*
// monitor changes in exchange db and react.
		if(
					(connector.m_data.m_timebTick.time > timebCheckMods.time )
				||((connector.m_data.m_timebTick.time == timebCheckMods.time)&&(connector.m_data.m_timebTick.millitm >= timebCheckMods.millitm))
				&&(!g_bKillThread)
			)
		{
//	connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "time to check");  Sleep(250);//(Dispatch message)
			timebCheckMods.time = connector.m_data.m_timebTick.time + connector.m_settings.m_ulModsIntervalMS/1000; 
			timebCheckMods.millitm = connector.m_data.m_timebTick.millitm + (unsigned short)(connector.m_settings.m_ulModsIntervalMS%1000); // fractional second updates
			if(timebCheckMods.millitm>999)
			{
				timebCheckMods.time++;
				timebCheckMods.millitm%=1000;
			}
//	connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "checking db connected");   Sleep(250);//(Dispatch message)
			if(pdbConn)//&&(pdbConn->m_bConnected))
			{
//AfxMessageBox("x");
//	connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "checkmods");  //(Dispatch message)
//				connector.m_data.ReleaseRecordSet();
///				connector.m_data.CheckDatabaseMods();  this was there twice!  2x the db hits, oh no!

				strcpy(errorstring, "");

				if(connector.m_data.CheckDatabaseMods(errorstring)==CONNECTOR_ERROR)
				{
					if(!connector.m_data.m_bCheckModsWarningSent)
					{
						connector.m_msgr.DM(MSG_ICONERROR, NULL, "CheckDatabaseMods", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
						connector.m_data.m_bCheckModsWarningSent=true;
					}
				}
				else
				{
					if(connector.m_data.m_bCheckModsWarningSent)
					{
						connector.m_msgr.DM(MSG_ICONINFO, NULL, "CheckDatabaseMods", "Resumed checking."); // Sleep(50); //(Dispatch message)
					}
					connector.m_data.m_bCheckModsWarningSent=false;
				}

				if(connector.m_data.m_timebTick.time > connector.m_data.m_timebAutoPurge.time + connector.m_settings.m_nAutoPurgeInterval)
				{
					_ftime(&connector.m_data.m_timebAutoPurge);

					if(connector.m_settings.m_nAutoPurgeMessageDays>0)
					{
						if(connector.m_data.CheckMessages(errorstring)==CONNECTOR_ERROR)
						{
							if(!connector.m_data.m_bCheckMsgsWarningSent)
							{
								connector.m_msgr.DM(MSG_ICONERROR, NULL, "CheckMessages", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								connector.m_data.m_bCheckMsgsWarningSent=true;
							}
						}
						else
						if(connector.m_data.m_bCheckMsgsWarningSent)
						{
							connector.m_msgr.DM(MSG_ICONINFO, NULL, "CheckMessages", "Resumed checking messages for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						connector.m_data.m_bCheckMsgsWarningSent=false;
					}

					if(connector.m_settings.m_nAutoPurgeAsRunDays>0)
					{
						if(connector.m_data.CheckAsRun(errorstring)==CONNECTOR_ERROR)
						{
							if(!connector.m_data.m_bCheckAsRunWarningSent)
							{
								connector.m_msgr.DM(MSG_ICONERROR, NULL, "CheckAsRun", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								connector.m_data.m_bCheckAsRunWarningSent=true;
							}
						}
						else
						if(connector.m_data.m_bCheckMsgsWarningSent)
						{
							connector.m_msgr.DM(MSG_ICONINFO, NULL, "CheckAsRun", "Resumed checking as-run for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						connector.m_data.m_bCheckAsRunWarningSent=false;
					}

				}



				if(connector.m_data.m_nSettingsMod != connector.m_data.m_nLastSettingsMod)
				{
					if(connector.m_settings.GetFromDatabase()>=CONNECTOR_SUCCESS)
					{
						connector.m_data.m_nLastSettingsMod = connector.m_data.m_nSettingsMod;


						// check for stuff to change

						// network messaging
						if(connector.m_settings.m_bLogNetworkErrors) 
						{
							if(!connector.m_data.m_bNetworkMessagingInitialized)
							{
								if(connector.m_net.InitializeMessaging(&connector.m_msgr)==0)
								{
									connector.m_data.m_bNetworkMessagingInitialized=true;
								}
							}
						}
						else
						{
							if(connector.m_data.m_bNetworkMessagingInitialized)
							{
								connector.m_net.UninitializeMessaging();  // void return
								connector.m_data.m_bNetworkMessagingInitialized=false;
							}
						}

						// logging and email messaging:

						if(!connector.m_settings.m_bUseEmail)
						{
							if(bUseEmail)
							{
								bUseEmail = false;
								// reset it
								connector.m_msgr.DM(MSG_ICONINFO, NULL, "Connector", "Shutting down email functions.");  //(Dispatch message)

		//						Sleep(250); // let the message get there.
								connector.m_msgr.RemoveDestination("email");
							}
						}
						else
						{
							if(!bUseEmail)
							{
								bUseEmail = true;
								int nRegisterCode=0;

								// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
								nRegisterCode = connector.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
									"email", 
									connector.m_settings.m_pszProcessedMailSpec?connector.m_settings.m_pszProcessedMailSpec:connector.m_settings.m_pszMailSpec,
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
									connector.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
									connector.m_msgr.DM(MSG_ICONERROR, NULL, "Connector:smtp_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=connector.m_msgr.GetDestIndex("email");
								if(nIndex>=0)
								{
									if((connector.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(connector.m_settings.m_pszProcessedMailSpec?connector.m_settings.m_pszProcessedMailSpec:connector.m_settings.m_pszMailSpec))
									{
										if(strcmp(connector.m_msgr.m_ppDest[nIndex]->m_pszParams, (connector.m_settings.m_pszProcessedMailSpec?connector.m_settings.m_pszProcessedMailSpec:connector.m_settings.m_pszMailSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = connector.m_msgr.ModifyDestination(
												"email", 
												connector.m_settings.m_pszProcessedMailSpec?connector.m_settings.m_pszProcessedMailSpec:connector.m_settings.m_pszMailSpec,
												MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify SMTP object!\n code: %d", nRegisterCode); 
												//connector.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												connector.m_msgr.DM(MSG_ICONERROR, NULL, "Connector:smtp_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}

						if(!connector.m_settings.m_bUseLog)
						{
							if(bUseLog)
							{
								bUseLog = false;
								// reset it
								connector.m_msgr.DM(MSG_ICONINFO, NULL, "Connector", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

								Sleep(250); // let the message get there.
								connector.m_msgr.RemoveDestination("log");
							}
						}
						else
						{
							if(!bUseLog)
							{
								bUseLog = true;
								int nRegisterCode=0;

								nRegisterCode = connector.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
									"log", 
									connector.m_settings.m_pszProcessedFileSpec?connector.m_settings.m_pszProcessedFileSpec:connector.m_settings.m_pszFileSpec, 
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
									connector.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_LOG|CX_STATUS_ERROR));
									connector.m_msgr.DM(MSG_ICONERROR, NULL, "Connector:log_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=connector.m_msgr.GetDestIndex("log");
								if(nIndex>=0)
								{
									if((connector.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(connector.m_settings.m_pszProcessedFileSpec?connector.m_settings.m_pszProcessedFileSpec:connector.m_settings.m_pszFileSpec))
									{
										if(strcmp(connector.m_msgr.m_ppDest[nIndex]->m_pszParams, (connector.m_settings.m_pszProcessedFileSpec?connector.m_settings.m_pszProcessedFileSpec:connector.m_settings.m_pszFileSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = connector.m_msgr.ModifyDestination(
												"log", 
												connector.m_settings.m_pszProcessedFileSpec?connector.m_settings.m_pszProcessedFileSpec:connector.m_settings.m_pszFileSpec,
												MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify log object!\n code: %d", nRegisterCode); 
												//connector.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												connector.m_msgr.DM(MSG_ICONERROR, NULL, "Connector:log_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}








					}
				}
				if(
					  (!connector.m_data.m_bProcessSuspended)

					&&(connector.m_data.m_key.m_bValid)  // must have a valid license
					&&(
							(!connector.m_data.m_key.m_bExpires)
						||((connector.m_data.m_key.m_bExpires)&&(!connector.m_data.m_key.m_bExpired))
						||((connector.m_data.m_key.m_bExpires)&&(connector.m_data.m_key.m_bExpireForgiveness)&&(connector.m_data.m_key.m_ulExpiryDate+connector.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!connector.m_data.m_key.m_bMachineSpecific)
						||((connector.m_data.m_key.m_bMachineSpecific)&&(connector.m_data.m_key.m_bValidMAC))
						)
					)
				{
					bool bAnalysisChanges = false;
					if(connector.m_data.m_nMappingMod != connector.m_data.m_nLastMappingMod)
					{
						if(connector.m_data.GetMappings()>=CONNECTOR_SUCCESS)
						{
							connector.m_data.m_nLastMappingMod = connector.m_data.m_nMappingMod;
						}
					}
					if(connector.m_data.m_nEventRulesMod != connector.m_data.m_nLastEventRulesMod)
					{
						// was this:
//						if(connector.m_data.GetEventRules()>=CONNECTOR_SUCCESS)
						// now this, and below timing cols removed
						if((connector.m_data.GetEventRules()>=CONNECTOR_SUCCESS)&&(connector.m_data.GetTimingColumns()>=CONNECTOR_SUCCESS))
						{
							// this was originally not forced reanalyzed... but dont know why?  did I omit accidentally?
if(g_pconnector->m_settings.m_ulDebug&CONNECTOR_DEBUG_PROCESS) 	
	g_pconnector->m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "Analysis change from EventRulesMod %d", connector.m_data.m_nEventRulesMod );  // Sleep(50);//(Dispatch message)
							bAnalysisChanges = true;
							connector.m_data.m_nLastEventRulesMod = connector.m_data.m_nEventRulesMod;
						}
					}
					if(connector.m_data.m_nEventsMod != connector.m_data.m_nLastEventsMod)
					{
//g_pconnector->m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "getting Timing Column names"); // Sleep(100); //(Dispatch message)

/*
						if(connector.m_data.GetTimingColumns()>=CONNECTOR_SUCCESS)
						{
							bAnalysisChanges = true;
* / // just reset it, we do timing cols above now.
							connector.m_data.m_nLastEventsMod = connector.m_data.m_nEventsMod;
/*
						}
* /
					}
					if(connector.m_data.m_nParameterRulesMod != connector.m_data.m_nLastParameterRulesMod)
					{
						if(connector.m_data.GetParameterRules()>=CONNECTOR_SUCCESS)
						{
if(g_pconnector->m_settings.m_ulDebug&CONNECTOR_DEBUG_PROCESS) 	
	g_pconnector->m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "Analysis change from ParameterRulesMod %d", connector.m_data.m_nParameterRulesMod );  // Sleep(50);//(Dispatch message)
							bAnalysisChanges = true;
							connector.m_data.m_nLastParameterRulesMod = connector.m_data.m_nParameterRulesMod;
						}
					}
					if(connector.m_data.m_nConnectionsMod != connector.m_data.m_nLastConnectionsMod)
					{

//			connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "Channel info view has changed, calling GetConnections");   Sleep(10);//(Dispatch message)
						if(connector.m_data.GetConnections()>=CONNECTOR_SUCCESS)
						{
			connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "GetConnections returned successfully"); //  Sleep(10);//(Dispatch message)
							connector.m_data.m_nLastConnectionsMod = connector.m_data.m_nConnectionsMod;
						}
						else
						{
			connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "GetConnections returned an error"); //  Sleep(10);//(Dispatch message)

						}
					}

					if(bAnalysisChanges)
					{
						// go thru and indicate changes to each channel
						connector.m_data.m_bForceAnalysis = true;


/*
// moved to a delay thread.
						if(
							  (connector.m_settings.m_ppEndpointObject)
							&&(connector.m_data.m_nIndexAutomationEndpoint>=0)
							&&(connector.m_data.m_nIndexAutomationEndpoint<connector.m_settings.m_nNumEndpointsInstalled)
							&&(connector.m_settings.m_ppEndpointObject[connector.m_data.m_nIndexAutomationEndpoint])
							)
						{
							CConnectorEndpointObject* pAutoObj = connector.m_settings.m_ppEndpointObject[connector.m_data.m_nIndexAutomationEndpoint];

							if(pAutoObj) pAutoObj->m_nLastModLiveEvents = -1;  // triggers global analysis rules.
// above line now taken care of above by connector.m_data.m_bForceAnalysis = true;

							if((pAutoObj->m_ppChannelObj)&&(pAutoObj->m_nNumChannelObjects))
							{
								int cho=0;
								while(cho<pAutoObj->m_nNumChannelObjects)
								{
									if(pAutoObj->m_ppChannelObj[cho])
									{
										pAutoObj->m_ppChannelObj[cho]->m_bTriggerEventsChanged = true;
//										pAutoObj->m_ppChannelObj[cho]->m_bNearEventsChanged = true;

									}
									cho++;
								}
							}
						}
* /

						if((g_pconnector->m_settings.m_bEnableEventTriggerNotification)&&(!g_pconnector->m_data.m_bDelayingTriggerNotification))
						{
if(g_pconnector->m_settings.m_ulDebug&CONNECTOR_DEBUG_PROCESS) 	
	g_pconnector->m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "  >>* Spawning ConnectorTriggerDelayNotificationThread from main thread");  // Sleep(50);//(Dispatch message)

							if(_beginthread(ConnectorTriggerDelayNotificationThread, 0, (void*)NULL)==-1)
							{
								//error.
				connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "Error starting trigger delay notification thread");  // Sleep(10);//(Dispatch message)

								//**MSG
							}
						}

					}

				}
/*
				if(connector.m_data.m_nConnectionsMod != connector.m_data.m_nLastConnectionsMod)
				{
//			connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "getting connections again");   Sleep(250);//(Dispatch message)
					if(connector.m_data.GetConnections()>=CONNECTOR_SUCCESS)
					{
						connector.m_data.m_nLastConnectionsMod = connector.m_data.m_nConnectionsMod;
					}
				}
				if(connector.m_data.m_nChannelsMod != connector.m_data.m_nLastChannelsMod)
				{
//			connector.m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "getting channels again");   Sleep(250);//(Dispatch message)
					if(connector.m_data.GetChannels()>=CONNECTOR_SUCCESS)
					{
						connector.m_data.m_nLastChannelsMod = connector.m_data.m_nChannelsMod;
					}
				}
* /
			}
		}  // check mod interval
*/
		MSG msg;

//		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
//			AfxGetApp()->PumpMessage();


//AfxMessageBox("zoinks");
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();

		Sleep(1); 
//		Sleep(1000);  // stalls window procedure
	}

	connector.m_data.m_ulFlags &= ~CONNECTOR_STATUS_THREAD_MASK;
	connector.m_data.m_ulFlags |= CONNECTOR_STATUS_THREAD_END;

	connector.m_msgr.DM(MSG_ICONINFO, NULL, "Connector:uninit", "Connector is shutting down.");  //(Dispatch message)
//	connector.SendMsg(CX_SENDMSG_INFO, "Connector:uninit", "Connector %s is shutting down.", CONNECTOR_CURRENT_VERSION);

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connector is shutting down.");  
	connector.m_data.m_ulFlags &= ~CX_ICON_MASK;
	connector.m_data.m_ulFlags |= CX_STATUS_UNKNOWN;
	connector.m_data.SetStatusText(errorstring, connector.m_data.m_ulFlags);


	int kt=0;
	BOOL bAllDone = FALSE;
	while ((kt<100)&&(!bAllDone))
/*
					(connector.m_data.m_bAutomationThreadStarted)
				||(connector.m_data.m_bNearAnalysisThreadStarted)
				||(connector.m_data.m_bFarAnalysisThreadStarted)
				||(connector.m_data.m_bTriggerThreadStarted)
				))
*/

	{

		/*

g_pconnector->m_msgr.DM(MSG_ICONHAND, NULL, "Connector:debug", "ending %d %d %d %d @ %d", 
											connector.m_data.m_bAutomationThreadStarted,
											connector.m_data.m_bNearAnalysisThreadStarted,
											connector.m_data.m_bFarAnalysisThreadStarted,
											connector.m_data.m_bTriggerThreadStarted, clock()
											); 
*/
		
		// have to go thru all endpoints until the threads are down.
		
		
		Sleep(10);
		_ftime( &connector.m_data.m_timebTick );  // we're still alive.
		kt++;
	}



// shut down all the running objects;
/*
	if(connector.m_data.m_ppConnObj)
	{
		int i=0;
		while(i<connector.m_data.m_nNumConnectionObjects)
		{
			if(connector.m_data.m_ppConnObj[i])
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", connector.m_data.m_ppConnObj[i]->m_pszDesc?connector.m_data.m_ppConnObj[i]->m_pszDesc:connector.m_data.m_ppConnObj[i]->m_pszServerName);  

// **** disconnect servers
				connector.m_data.SetStatusText(errorstring, connector.m_data.m_ulFlags);
				connector.m_msgr.DM(MSG_ICONINFO, NULL, "Connector:connection_uninit", errorstring);    //(Dispatch message)

				delete connector.m_data.m_ppConnObj[i];
				connector.m_data.m_ppConnObj[i] = NULL;
			}
			i++;
		}
		delete [] connector.m_data.m_ppConnObj;
	}
	if(connector.m_data.m_ppChannelObj)
	{
		int i=0;
		while(i<connector.m_data.m_nNumChannelObjects)
		{
			if(connector.m_data.m_ppChannelObj[i])
			{
				delete connector.m_data.m_ppChannelObj[i];
				connector.m_data.m_ppChannelObj[i] = NULL;
			}
			i++;
		}
		delete [] connector.m_data.m_ppChannelObj;
	}
*/

/*
	// here's a hard coded one for now
	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no need to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = connector.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		connector.m_net.OpenConnection(connector.m_http.m_pszHost, 10888, &s); // branding hardcode
		connector.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		connector.m_net.CloseConnection(s); // branding hardcode
	}
*/

//	m_pDlg->SetProgress(CONNECTORDLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
//	connector.m_data.m_ulFlags &= ~CONNECTOR_STATUS_FILESVR_MASK;
//	connector.m_data.m_ulFlags |= CONNECTOR_STATUS_FILESVR_END;
//	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down file server....");  
//	connector.m_data.SetStatusText(errorstring, connector.m_data.m_ulFlags);
//	_ftime( &connector.m_data.m_timebTick );
//	connector.m_http.EndServer();
	_ftime( &connector.m_data.m_timebTick );
//	AfxMessageBox("shutting down command server.");
/*
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down command server....");  
	connector.m_msgr.DM(MSG_ICONNONE, NULL, "Connector:command_server_uninit", errorstring);  //(Dispatch message)
	connector.m_data.SetStatusText(errorstring, CONNECTOR_STATUS_CMDSVR_END);
	_ftime( &connector.m_data.m_timebTick );
	connector.m_net.StopServer(connector.m_settings.m_usCommandPort, 5000, errorstring);
	_ftime( &connector.m_data.m_timebTick );
//	AfxMessageBox("shutting down status server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down XML server....");  
	connector.m_msgr.DM(MSG_ICONNONE, NULL, "Connector:xml_server_uninit", errorstring);  //(Dispatch message)
	connector.m_data.SetStatusText(errorstring, CONNECTOR_STATUS_STATUSSVR_END);
	_ftime( &connector.m_data.m_timebTick );
	connector.m_net.StopServer(connector.m_settings.m_usStatusPort, 5000, errorstring);
	_ftime( &connector.m_data.m_timebTick );
*/
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connector is exiting.");  
	connector.m_msgr.DM(MSG_ICONNONE, NULL, "Connector:uninit", errorstring);  //(Dispatch message)
	connector.m_data.SetStatusText(errorstring, connector.m_data.m_ulFlags);



	// save settings.  // dont save them here.  save them on any changes in the main command loop.
	connector.m_settings.Settings(false); //write
	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connector is exiting");
	connector.m_data.m_ulFlags &= ~CX_ICON_MASK;
	connector.m_data.m_ulFlags |= CX_STATUS_UNINIT;
	connector.m_data.SetStatusText(errorstring, connector.m_data.m_ulFlags);

	//exiting
	connector.m_msgr.DM(MSG_ICONINFO, NULL, "Connector", "-------------- Connector %s exit ---------------\n\
--------------------------------------------------\n", CONNECTOR_CURRENT_VERSION);  //(Dispatch message)
///	m_pDlg->SetProgress(CONNECTORDLG_CLEAR); // no point

	_ftime( &connector.m_data.m_timebTick );
	connector.m_data.m_ulFlags &= ~CONNECTOR_STATUS_THREAD_MASK;
	connector.m_data.m_ulFlags |= CONNECTOR_STATUS_THREAD_ENDED;

	g_bKillStatus = true;

	Sleep(50); // let the message get there.
	connector.m_msgr.RemoveDestination("log");
	connector.m_msgr.RemoveDestination("email");

	g_pconnector = NULL;
//	db.RemoveConnection(pdbConn);

	
	int nClock = clock() + 50; // small delay at end
	while(clock()<nClock)	{_ftime( &connector.m_data.m_timebTick );}
	g_bThreadStarted = false;
	g_pconnector = NULL;
	nClock = clock() + 50;//connector.m_settings.m_nThreadDwellMS; // another small delay at end
	while(clock()<nClock)	{_ftime( &connector.m_data.m_timebTick );}
	Sleep(50); //one more small delay at end
	_endthread();
}


#ifdef USE_LISTENERS_AND_MESSAGING


void ConnectorCommandHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure in Protocol 1  //SECURE with PROTOCOL2 later
			strcpy(pszInfo, "");  // clear it
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error %d receiving data.  %s", nReturn, pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					strcpy(pszInfo, "");  // clear it
					nReturn = net.SendData(&data, pClient->m_socket, 2000, 0, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error %d sending NAK reply.  %s", nReturn, pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
						// we had an error receiving data and then anotehr trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{
//AfxMessageBox("receiving");

					// parse the command:

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					switch(data.m_ucCmd)
					{
					case CX_CMD_GETINFO: // not actually supported yet.  everything thru csf files for now.
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case CONNECTOR_CMD_EXSET://				0xea // increments exchange counter
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)
							{	
								data.m_ucSubCmd = (unsigned char) g_pconnector->m_data.IncrementDatabaseMods((char*)data.m_pucData);
								free(data.m_pucData);  //destroy the buffer;
							}
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.

							data.m_ucType = NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case CONNECTOR_CMD_EXGET://				0xeb // gets exchange mod value
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case CONNECTOR_CMD_MODSET://			0xec // sets exchange mod value, skips exchange table
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case CX_CMD_GETSTATUS: //	0x99 // gets status info
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							unsigned long ulStatus;
							// assemble if there is 

							char* pch = g_pconnector->m_data.GetStatusText(&ulStatus);  // allocates mem, must free after use
							if(pch)
							{
								data.m_ucSubCmd = (unsigned char)(CONNECTOR_ICON_MASK&ulStatus);
								data.m_pucData=(unsigned char*)pch;
								data.m_ulDataLen = strlen(pch);
							}
							else
							{
								data.m_ucCmd = NET_CMD_NAK;
								data.m_ucSubCmd = NET_CMD_NAK;
							}

							data.m_ucType = NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case CX_CMD_BYE:
						{
	g_pconnector->m_msgr.DM(MSG_ICONINFO, NULL, "Connector", "The Global Kill command has been received.");  //(Dispatch message)

							bSetGlobalKill = true;
							g_pconnector->m_data.m_bQuietKill = true;
							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					default:
						{
							//just ack
							data.m_ucCmd = NET_CMD_ACK;

		//					char repbuf[64];
							if(data.m_pucData!=NULL)
							{
		/*
								_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

								for(unsigned long q=0; q<data.m_ulDataLen; q++)
								{
									if(strlen(repbuf)<63)
										strncat(repbuf, (char*)(data.m_pucData+q), 1);
								}
								if(strlen(repbuf)<63)
									strcat(repbuf, "]");
								data.m_ulDataLen = strlen(repbuf);
								repbuf[data.m_ulDataLen] = 0;  // just in case
		*/
								free(data.m_pucData);  //destroy the buffer;
							}
		/*
							else
							{
								_snprintf(repbuf, 63, "data was NULL");
								data.m_ulDataLen = strlen(repbuf); 
							}

							data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
							if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
							else 
							{
								data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
								data.m_ulDataLen = 0;
							}
		*/
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					}

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Connector:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Connector:CommandHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.
	if (bSetGlobalKill)	//g_bKillThread = true;
	((CConnectorHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}

void ConnectorStatusHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Connector:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Connector:CommandHandlerThread");
						}
						// we had an error receiving data and then anotehr trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Connector:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Connector:CommandHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}


int CConnectorMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)&&(strlen(m_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			(m_settings.m_pszMessages?m_settings.m_pszMessages:"Messages"),
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			(m_settings.m_pszMessages?m_settings.m_pszMessages:"Messages")
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_data.m_critSQL);
		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_data.m_critSQL);
			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return CONNECTOR_SUCCESS;
		}
		LeaveCriticalSection(&m_data.m_critSQL);

	}
	return CONNECTOR_ERROR;
}

int CConnectorMain::SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszAsRun)&&(strlen(m_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			(m_settings.m_pszAsRun?m_settings.m_pszAsRun:"AsRun_Log"),
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			(m_settings.m_pszAsRun?m_settings.m_pszAsRun:"AsRun_Log")
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_data.m_critSQL);
		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_data.m_critSQL);
			m_data.IncrementDatabaseMods(m_settings.m_pszAsRun, dberrorstring);
			return CONNECTOR_SUCCESS;
		}
		LeaveCriticalSection(&m_data.m_critSQL);

	}
	return CONNECTOR_ERROR;
}


void ConnectorXMLHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;
	
	char szConnectorSource[MAX_PATH]; 
	strcpy(szConnectorSource, "ConnectorXMLHandler");

	CBufferUtil bu;

	HRESULT hRes;
	try
	{  

#if _WIN32_WINNT >= 0x0400
		hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		hRes = CoInitialize(NULL);
#endif
	}
	catch(...)
	{
		if(g_pconnector) g_pconnector->m_msgr.DM(MSG_ICONINFO, NULL, szConnectorSource, "Exception in CoInitialize");  //(Dispatch message)
	}


	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		char dberrorstring[DB_ERRORSTRING_LEN];
//		char xmlerrorstring[MAX_MESSAGE_LENGTH];
		char errorstring[MAX_MESSAGE_LENGTH];
//		char szSQL[DB_SQLSTRING_MAXLEN];
		strcpy(dberrorstring, "");
		strcpy(errorstring, "");

//		char element[MAX_MESSAGE_LENGTH];

//		AfxMessageBox(szConnectorSource);

		CNetUtil net(false); // local object for utility functions.
		CCortexUtil util;  // cortex utility functions.

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		bool bCloseCommand = false;  // make these not persistent unless a persist command comes in.
		bool bPersist = false;  // make these not persistent unless a persist command comes in.
		int nPeriodic = -1;  
		int nTimed = -1;  

		char* pchBuffer = NULL;
		unsigned long ulBufferLen = 0;
		char* pch = NULL;
		char* pchXML = NULL;
		char* pchXMLStream = NULL;
		unsigned long ulAccumulatedBufferLen = 0;
		unsigned long ulRetry = 0;

		unsigned long ulConnLastMessage = 0;
		unsigned long ulConnTimeout = 0;
		
		char filename[MAX_PATH];
		char lastrxfilename[MAX_PATH];
		char lasttxfilename[MAX_PATH];
		int nRxDupes=0;
		int nTxDupes=0;

		FILE* fp = NULL;

		_timeb timestamp;
		_timeb timeactive;
		_timeb timeperiodic;

		_ftime(&timeactive);
		_ftime(&timeperiodic);


	// 	(*(pClient->m_pulConnections))++;//already exists

		IXMLDOMDocumentPtr pDoc(__uuidof(DOMDocument));  // let's have only one object per connection
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread D"); Sleep(100); //(Dispatch message)
		HRESULT hr;//= pDoc.CreateInstance(__uuidof(DOMDocument));
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread E");  //(Dispatch message)
		hr = pDoc->put_async(VARIANT_FALSE);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread F");  //(Dispatch message)
										
		// Remove document safety options
		IObjectSafety* pSafety=NULL;
		DWORD dwSupported, dwEnabled;
			

		if ( SUCCEEDED(pDoc->QueryInterface(IID_IObjectSafety, (void**)&pSafety)))
		{
			pSafety->GetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, &dwSupported, &dwEnabled );
			pSafety->SetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, dwSupported, 0 );
		}

		// initialize random seed
		srand ( time(NULL) );


		CCortexMessage msg;

		// allocate all the static ones
//		char* m_pchResponse[CX_XML_BUFFER_COUNT] = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};

//		AfxMessageBox("formatting");

		msg.FormatContent(CX_XML_BUFFER_CORTEXBEGIN, MAX_MESSAGE_LENGTH, "<cortex src=\"%s\" version=\"%s\">", 
			(g_pconnector->m_settings.m_pszName?g_pconnector->m_settings.m_pszName:"Connector"), 
			CX_XML_VERSION
			);


		msg.SetContent(CX_XML_BUFFER_CORTEXEND, "</cortex>");

		msg.SetContent(CX_XML_BUFFER_FLAGSEND, "</flags>");

		msg.SetContent(CX_XML_BUFFER_DATAEND, "</data>");

		//  ack and nak are the same length so we can allocate it, just replace contents later
		msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

//		AfxMessageBox("set");



		if (FAILED(hRes))
		{
			sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established, but CoInitialize failed.  Unable to process XML.", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,
				pClient->m_socket);

			if(g_pconnector)
			{
				g_pconnector->m_msgr.DM(MSG_ICONERROR, NULL, szConnectorSource, errorstring);  //(Dispatch message)
				g_pconnector->SendMsg(CX_SENDMSG_ERROR, szConnectorSource, errorstring);
			}

			goto cleanup;
		}

		
		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);
		if(g_pconnector)
		{
			g_pconnector->m_msgr.DM(MSG_ICONINFO, NULL, szConnectorSource, errorstring);  //(Dispatch message)
			g_pconnector->SendMsg(CX_SENDMSG_INFO, szConnectorSource, errorstring);
		}



		while ( 
									(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
//								&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
								&&(pClient->m_socket!=NULL)
								&&(!bCloseCommand)
					)
		{


			pchBuffer = NULL;
			ulBufferLen = 0;

			
			int nReturnCode = net.GetLine((unsigned char**)&pchBuffer, &ulBufferLen, pClient->m_socket, NET_RCV_ONCE, errorstring);
			if(nReturnCode == NET_SUCCESS)
			{
				ulRetry = 0;
				//process any received XML.
				// have to keep accumulating until we find a </cortex> tag.
				if((pchBuffer)&&(ulBufferLen)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
				{
					int nLen = 0;
					if(pchXMLStream) nLen = ulAccumulatedBufferLen;
					pch = (char*) malloc(nLen+ulBufferLen+1);  //term 0
					if(pch)
					{
						char* pchEnd = NULL;
						char* pchNext = NULL;
						if(pchXMLStream)  // we have an old buffer.
						{
							memcpy(pch, pchXMLStream, ulAccumulatedBufferLen);
							memcpy(pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen);
							free(pchXMLStream); 
							pchXMLStream = pch;  // reassign!
							ulAccumulatedBufferLen += ulBufferLen;
							*(pch+ulAccumulatedBufferLen) = 0;  // null term
						}
						else
						{
							// this is new.
							// first we have to skip all chars that are not a '<'
							// pchEnd = strchr(pchBuffer, '<');  // have to find first tag, we are only interested in XML.
							// used to use strchr but if there are leading zeros in the buffer, we never get past them

							pchEnd = pchBuffer;
							while((*pchEnd!='<')&&(pchEnd<pchBuffer+ulBufferLen)) pchEnd++;

							if(pchEnd<pchBuffer+ulBufferLen)
							{
								strcpy(pch, pchEnd);
								ulAccumulatedBufferLen += (ulBufferLen-(pchEnd-pchBuffer));
								pchXMLStream = pch;  // reassign!
								*(pch+ulAccumulatedBufferLen) = 0;  // null term
							}
							else  // not found!
							{
								free(pch);
								pchXMLStream = NULL;
							}
						}

						if(pchBuffer) free(pchBuffer);
						pchBuffer = NULL;
						if(pchXMLStream)
						{

							pchEnd = strstr(pchXMLStream, "</cortex>");
							while((pchEnd)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
							{
							// found a token.
								pchEnd+=strlen("</cortex>");

								pchNext = pchEnd;
								while((*pchNext!='<')&&(pchNext<pchEnd+strlen(pchEnd))) pchNext++;

								if(pchNext<pchEnd+strlen(pchEnd))
								{
									// we found a remainder.
									nLen = strlen(pchNext);
									pch = (char*) malloc(nLen+1);  //term 0
									if(pch)
									{
										memcpy(pch, pchNext, nLen);
										*(pch+nLen) = 0;
									}
								}
								else pch = NULL;


								pchXML = pchXMLStream; // just use it.
								*pchEnd = 0; //null terminate it

								pchXMLStream = pch;  // take the rest of the stream.
								if(pchXMLStream) ulAccumulatedBufferLen = strlen(pchXMLStream);
								else ulAccumulatedBufferLen=0;

								// now have to deal with XML found in pchXML.
								//debug file write
								if((g_pconnector)&&(g_pconnector->m_settings.m_bUseXMLClientLog))
								{
//				AfxMessageBox("logging");
if(g_pconnector->m_settings.m_ulDebug&CONNECTOR_DEBUG_COMM) 
	g_pconnector->m_msgr.DM(MSG_ICONHAND, NULL, szConnectorSource, "begin log receive");  //(Dispatch message)

									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_pconnector->m_settings.m_pszName?g_pconnector->m_settings.m_pszName:"Connector"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lastrxfilename, filename)==0)
									{
										nRxDupes++;
									}
									else
									{
										nRxDupes=0;
										strcpy(lastrxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_RX.xml",nRxDupes);

//	AfxMessageBox(filename);

									fp = fopen(filename, "wb");
									if(fp)
									{
//											fwrite(filename, 1, strlen(filename),fp);
										fwrite(pchXML, 1, strlen(pchXML),fp);
										fflush(fp);
										fclose(fp);
										fp = NULL;
									}
if(g_pconnector->m_settings.m_ulDebug&CONNECTOR_DEBUG_COMM) 
	g_pconnector->m_msgr.DM(MSG_ICONHAND, NULL, szConnectorSource, "end log receive");  //(Dispatch message)

								}

								// do stuff here.

								_ftime(&timeactive);  // reset the inactivity timer.


								// first attempt to extract the rxid.
								char* pchRXID = NULL;
								int nIDlen = 0;

								char* pchFind = strstr(pchXML, "<rx rxid=\"");

								if(pchFind)
								{
									pchFind+=strlen("<rx rxid=\"");
									char* pchTerminate = pchFind;
									while(pchTerminate<pchEnd)
									{
										if(
											  (*pchTerminate == '\"')
											||(*pchTerminate == '>')
											||((pchTerminate<pchEnd)&&(*pchTerminate == '/')&&(*(pchTerminate+1) == '>'))
											)
										{
											break;
										}
										else
										{
											pchTerminate+=1;
										}
									}

									nIDlen = pchTerminate-pchFind;
									if(nIDlen>0)
									{
										pchRXID = (char*)malloc(nIDlen+1);
									}
									if(pchRXID)
									{
										memset(pchRXID, 0, nIDlen+1);
										memcpy(pchRXID, pchFind, nIDlen);

										pchTerminate = bu.XMLEncode(pchRXID); // just in case
										if(pchTerminate)
										{
											try{free(pchRXID);} catch(...){}
											pchRXID = pchTerminate;
										}
									}
								}


							//create a response UUID
								UUID uuid;
								UuidCreate(&uuid); // could check response code to be RPC_S_OK ?

					//			CoCreateGuid(&uuid);

								unsigned char* pucUUID = NULL;
								UuidToString(&uuid, &pucUUID);
								// call this later: RpcStringFree(&pucUUID);

								
								bool bResponseSent = false;  // means, sent a response already.  Otherwise have to look at the failure code and deal at the end

								msg.m_nTxStep = CX_XML_BUFFER_CORTEXBEGIN;

								//OK, let's assemble the tx element.

								if(pchRXID)
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUID);
								}


								// then, parse the XML
								msg.m_nFailure=CX_XML_ERR_SUCCESS;
								pDoc->put_async(VARIANT_FALSE);
			//				AfxMessageBox((char*)pchXML);
								hr = pDoc->loadXML((char*)pchXML);
								if(hr!=VARIANT_TRUE)
								{
								//					AfxMessageBox("bad load");
									bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML parse error: [%s]", pchXML );
									g_pconnector->m_msgr.DM(MSG_ICONINFO, NULL, szConnectorSource, errorstring);
									//let's try to send a nak.

									msg.m_nFailure=CX_XML_ERR_XMLPARSE;
									msg.m_nType = CX_XML_TYPE_UNK;
									msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
									msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML parse error %d</msg>", msg.m_nFailure);
									msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");
								}
								else
								{
	//								AfxMessageBox("yirgacheff");

if(g_pconnector->m_settings.m_ulDebug&CONNECTOR_DEBUG_COMM) 
	g_pconnector->m_msgr.DM(MSG_ICONHAND, NULL, szConnectorSource, "begin xml parse");  //(Dispatch message)
									IXMLDOMNodeListPtr pChildNodes = pDoc->GetchildNodes();

									IXMLDOMNodePtr pNodes[CX_XML_NODECOUNT];
									int n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }

									msg.m_nType = CX_XML_TYPE_UNK;
									msg.SetType("unknown");

									bool bCommandHandled = false;
									if (pChildNodes != NULL)
									{
										IXMLDOMNodePtr pChild;
										while(pChild = pChildNodes->nextNode())
										{
											DOMNodeType nodeType;
											pChild->get_nodeType(&nodeType);

											if(nodeType == NODE_ELEMENT)
											{
												char tag[MAX_PATH]; 
												//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
												util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

											//	AfxMessageBox(tag);

												if(strcmp("cortex", tag)==0)
												{
													pNodes[CX_XML_NODE_CORTEX] = pChild;
													// now get subs to cortex

													pChildNodes = pChild->GetchildNodes();
													if (pChildNodes != NULL)
													{
														while(pChild = pChildNodes->nextNode())
														{
															pChild->get_nodeType(&nodeType);

															if(nodeType == NODE_ELEMENT)
															{
																//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
																util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

														//		AfxMessageBox(tag);

																if(strcmp("rx", tag)==0)
																{
																	pNodes[CX_XML_NODE_RX] = pChild;
																	// now get the "real" rxid;

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
																	//	AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
																			//strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));

								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("rxid", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
																				//strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));

																		//		AfxMessageBox("replace rxid.");

																				if(pchRXID)
																				{
																					try{free(pchRXID);} catch(...){}
																					pchRXID = bu.XMLEncode(tag); // just in case
																					if(pchRXID)
																					{
// reassemble tx element
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
																					}
																				}

																			}
																			else
																			if(strcmp("part", tag)==0)
																			{
																				//  multi part message
																			}
																		}
																	}

																}
																else
																if(strcmp("cmd", tag)==0)
																{
																	pNodes[CX_XML_NODE_CMD] = pChild;
																	pNodes[CX_XML_NODE_OPTIONS] = NULL;
																	pNodes[CX_XML_NODE_DATA] = NULL;

																	// now get attribs and then get subs, data and options

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
								//										AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
																			//strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));
								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("type", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
																				//strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));
																				msg.SetType(tag);

//																																		AfxMessageBox(tag);

																				if(strcmp("ack", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_ACK;
																				}
																				else
																				if(strcmp("nak", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_NAK;
																				}
																				else
																				if(strcmp("persist", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PERSIST;
																					bPersist = true;
																				}
																				else
																				if(strcmp("quit", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_QUIT;
																					bPersist = false;
																				}
																				else
																				if(strcmp("status", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_STATUS;
																				}
																				else
																				if(strcmp("bye", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_BYE;
																				}
																				else
																				if(strcmp("get_version", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_GETVERSION;
																				}
																				else
																				if(strcmp("ping", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PING;
																				}
/*
																				else

																					

/////////////////////////////////////////////////////////////////////////////////////////////////
////                                    begin Connector specific commands
//#define SENTINEL_XML_TYPE_GETCONN				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
//#define SENTINEL_XML_TYPE_GETLIST				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
//#define SENTINEL_XML_TYPE_GETEVENT			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
																				if(strcmp("get_conn", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETCONN;
																				}
																				else
																				if(strcmp("get_list", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETLIST;
																				}
																				else
																				if(strcmp("get_event", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETEVENT;
																				}
																					
////                                    end Connector specific commands
/////////////////////////////////////////////////////////////////////////////////////////////////
*/

																				
																			}
																			else
																			if(strcmp("user", tag)==0)
																			{
																			}
																			else
																			if(strcmp("password", tag)==0)
																			{
																			}
																		}
																	}


																	IXMLDOMNodeListPtr pCmdChildNodes = pChild->GetchildNodes();
																	if (pCmdChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pCmdChild;
																		while(pCmdChild = pCmdChildNodes->nextNode())
																		{
																			pCmdChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				//strncpy(tag, W2T(pCmdChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pCmdChild->GetnodeName()));
																				if(strcmp("options", tag)==0)
																				{
																					pNodes[CX_XML_NODE_OPTIONS] = pCmdChild;
																				}
																				else
																				if(strcmp("data", tag)==0)
																				{
																					pNodes[CX_XML_NODE_DATA] = pCmdChild;
																				}
																			}
																		}
																	}
																}
															}
														}
														// by here, all the necessary stuff should be filled out.

//														AfxMessageBox("here");
												

														switch(msg.m_nType)
														{
														case CX_XML_TYPE_ACK://						1	// 4.1 ack Acknowledges a message with success
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the ack

																
															} break; //case CX_XML_TYPE_ACK
														case CX_XML_TYPE_NAK://						2	// 4.2 nak Acknowledges a message with failure, or cancels
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the nak

															} break; //case CX_XML_TYPE_NAK
														case CX_XML_TYPE_PERSIST://				3	// 4.3 persist Requests a persistent connection
															{
																//persistence already done, above
																// set timeout and hearbeat intervals
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"persist\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																if(pNodes[CX_XML_NODE_OPTIONS])
																{

																	// have to return flags
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																	IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																	if (pOptChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pOptChild;
																		while(pOptChild = pOptChildNodes->nextNode())
																		{
																			pOptChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				//strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pOptChild->GetnodeName()));
																				if(strcmp("timeout", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						nTimed = atoi(pchTmp);

																						_snprintf(tag, MAX_PATH, "<timeout>%d</timeout>", nTimed);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;
																					}
																				}
																				else
																				if(strcmp("heartbeat", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						strncpy(tag, pchTmp, MAX_PATH);

																						if(strcmp("yes", tag)==0)
																						{
																							nPeriodic = 30;
																						}
																						else
																						if(strcmp("no", tag)==0)
																						{
																							nPeriodic = -1;
																						}
																						else
																						{
																							nPeriodic = atoi(tag);
																						}

																						_snprintf(tag, MAX_PATH, "<heartbeat>%d</heartbeat>", nPeriodic);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );


																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;

																					}
																				}
																			}
																		}
																	}
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																}
																
															} break; //case CX_XML_TYPE_PERSIST
														case CX_XML_TYPE_QUIT://					4	// 4.4 quit Disconnects a persistent connection
															{
																//already done, above
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"quit\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																
															} break; //case CX_XML_TYPE_QUIT
														case CX_XML_TYPE_STATUS://				5	// 4.5 status Obtains status information regarding the module
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"status\" code=\"0\">");

																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																char* pchStatus = g_pconnector->m_data.GetStatusText(&ulDataLen);
																if(pchStatus)
																{
																	msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<status><code>0x%08x</code><text>%s</text></status>",
																		ulDataLen,
																		pchStatus	
																		);
																}
																else
																{
																	msg.SetContent(CX_XML_BUFFER_DATA, 
																		"<status><code>0xffffffff</code><text>unavailable</text></status>"
																		);

																}

																if(pchStatus){try { free(pchStatus); } catch(...){}  pchStatus=NULL;}

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


															} break; //case CX_XML_TYPE_STATUS
														case CX_XML_TYPE_BYE://						6	// 4.6 bye Causes the module to shut down
															{

g_pconnector->m_msgr.DM(MSG_ICONINFO, NULL, "Connector", "The Global Kill XML command has been received.");  //(Dispatch message)

																bSetGlobalKill = true;
																g_pconnector->m_data.m_bQuietKill = true;
																bCloseCommand = true;

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"bye\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_GETVERSION://		7	// 4.7 get_version gets the version of the module as well as the build date
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_version\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<version>%s</version><build>%s %s</build>",
																		CX_CURRENT_VERSION,
																		 __DATE__,
																		 __TIME__	
																		);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_PING://					8	// 4.8 ping just sends an ack to indicate it is alive
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"ping\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE


////////////////////////////////////////////////////////
// begin Connector specific XML commands
/*
														case SENTINEL_XML_TYPE_GETCONN://				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_conn\" code=\"%d\">", g_pconnector->m_data.m_nNumConnectionObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_pconnector->m_data.m_critConns);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_pconnector->m_data.m_ppConnObj)&&(g_pconnector->m_data.m_nNumConnectionObjects))
																{

																	//let's see if we have any options.

																	char* pchServer = NULL;  //server name search
																	int nActive =-1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{

																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("active", tag)==0)
																					{
																				//		AfxMessageBox("active");
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}
																					//		AfxMessageBox("active 2");

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																				//			AfxMessageBox(tag);

																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_pconnector->m_data.m_nNumConnectionObjects)
																	{

																		if(g_pconnector->m_data.m_ppConnObj[m])
																		{
																			bool bInclude = true;
																			CConnectorConnectionObject* pObj = g_pconnector->m_data.m_ppConnObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																	//		AfxMessageBox("here");
																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																	//			AfxMessageBox("here 2");
																			if(bInclude)
																			{
																				if(pObj->m_pAPIConn)
																				{
																					if((pObj->m_pAPIConn->m_SysData.systemfrx==0x29)
																						&&(pObj->m_pAPIConn->m_SysData.systemdf))
																					{
																						strcpy(tag, "29.97");
																					}
																					else
																					{
																						sprintf(tag, "%02x",pObj->m_pAPIConn->m_SysData.systemfrx);
																					}
																				}

																				_ftime(&timestamp);
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<aconn>\
<config active=\"%d\">\
<server>%s</server>\
<client>%s</client>\
<port>N/A</port>\
<basis>%s</basis>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<server_time>%.03f</server_time>\
<server_status>%d</server_status>\
<server_lists>%d</server_lists>\
<server_changed>%d</server_changed>\
<server_last_update>%.03f</server_last_update>\
<message>%s</message>\
</status>\
</aconn>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pObj->m_pszServerName,
																					pObj->m_pszClientName,
																					(pObj->m_pAPIConn?tag:"N/A"),
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(pObj->m_dblLastServerTimeMS/1000.0),
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? -1 : (pObj->m_pAPIConn?pObj->m_pAPIConn->m_Status:-1)),
																					(pObj->m_pAPIConn?pObj->m_pAPIConn->m_SysData.systemlistcount:-1),
																					(pObj->m_pAPIConn?pObj->m_pAPIConn->m_SysData.syschanged:-1),
																					pObj->m_dblUpdateTime,
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? (((pObj->m_ulStatus&SENTINEL_ICON_MASK) == SENTINEL_STATUS_ERROR)?"connection error":"not connected"):"")

																																										
																				);
																					
																			

																				msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}

																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;

																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_pconnector->m_data.m_critConns);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);


															} break; //case SENTINEL_XML_TYPE_GETCONN

														case SENTINEL_XML_TYPE_GETLIST://				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_list\" code=\"%d\">", g_pconnector->m_data.m_nNumChannelObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																
EnterCriticalSection(&g_pconnector->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_pconnector->m_data.m_ppChannelObj)&&(g_pconnector->m_data.m_nNumChannelObjects))
																{

																	//let's see if we have any options.

																	char* pchServer = NULL;  //server name search
																	int nActive =-1;
																	int nID=-1;
																	int nListNum = -1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{

																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("id", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nID = atoi(pchTmp);
																								if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("list_number", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nListNum = atoi(pchTmp);
																								if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("active", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_pconnector->m_data.m_nNumChannelObjects)
																	{

																		if(g_pconnector->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CConnectorChannelObject* pObj = g_pconnector->m_data.m_ppChannelObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																			if((nID>0)&&(pObj->m_nChannelID != nID))  bInclude = false;
																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				_ftime(&timestamp);

																				tlistdata* ptlist = NULL;
																				if(pObj->m_pAPIConn)
																				{
																					ptlist = &(pObj->m_pAPIConn->m_ListData[pObj->m_nHarrisListID-1]);
																				}
																				
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<alist>\
<config active=\"%d\" id=\"%d\">\
<server>%s</server>\
<list_number>%d</list_number>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<list_state>%d</list_state>\
<list_changed>%d</list_changed>\
<list_display>%d</list_display>\
<list_syschange>%d</list_syschange>\
<list_count>%d</list_count>\
<list_lookahead>%d</list_lookahead>\
<list_last_update>%.3f</list_last_update>\
</status>\
</alist>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pObj->m_nChannelID,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(ptlist?ptlist->liststate:-1),
																					(ptlist?ptlist->listchanged:-1),
																					(ptlist?ptlist->listdisplay:-1),
																					(ptlist?ptlist->listsyschange:-1),
																					(ptlist?ptlist->listcount:-1),
																					(ptlist?ptlist->lookahead:-1),
																					pObj->m_dblUpdateTime
																																										
																				);
																					
																			

																				msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_LIST_MAXPAYLOAD, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}

																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;

																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_pconnector->m_data.m_critChannels);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case SENTINEL_XML_TYPE_GETLIST
														case SENTINEL_XML_TYPE_GETEVENT://			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
															{

																msg.ClearContent(CX_XML_BUFFER_MSG);
																int nGlobalCount =0;
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_pconnector->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_pconnector->m_data.m_ppChannelObj)&&(g_pconnector->m_data.m_nNumChannelObjects))
																{

																	//let's see if we have any options.

//																	char* pchServer = NULL;  //server name search
//																	int nActive =-1;
																	int nID=-1;
																	int nLimitNum = -1;
																	int nDone = -1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{
																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("id", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nID = atoi(pchTmp);
																								if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("limit", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nLimitNum = atoi(pchTmp);
																								if(nLimitNum <=0) nLimitNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<limit>%d</limit>", nLimitNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("done", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nDone = 0;
																								else if(strcmp("1", pchTmp)==0) nDone = 1;
																								else if(strcmp("yes", pchTmp)==0) nDone = 1;
																								else if(strcmp("no", pchTmp)==0) nDone = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<done>%d</done>", nDone);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}

																					/*
																					else
																					if(strcmp("list_number", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nListNum = atoi(pchTmp);
																								if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("active", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																					* /
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_pconnector->m_data.m_nNumChannelObjects)
																	{

																		if(g_pconnector->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CConnectorChannelObject* pObj = g_pconnector->m_data.m_ppChannelObj[m];

/*
																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

* /																		if(
																				//	(nID>0)&&  //commenting this out makes it mandatory!
																					(pObj->m_nChannelID != nID)
																				)  
																				bInclude = false;

//																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				// found a channel that works, now must loop thru the events, filtering if nec.

																				// need to allocate a big buffer and deal with directly, rather than allocating new for every event.
																			
/* // was this, which worked but slowly.  below is new
																				if(pObj->m_ppevents)
																				{
																					 
																					int q = 0;
																					int nCount = 0;
																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;
																						unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));

																						if(pObj->m_ppevents[q])
																						{
																							CConnectorEventObject* pEObj = pObj->m_ppevents[q];

																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;
																							if(bInclude)
																							{

																								_ftime(&timestamp);

																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
																								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																									"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_usControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CConnectorEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																								);
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																								msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );

																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
* /




																				char* pchPayload = (char*)malloc(SENTINEL_XML_EVENT_MAXPAYLOAD);
																				char* pchAppend = pchPayload;

																				// re-use errorstring, why not.
																		//		int nTimes[10] = {0,0,0,0,0,0,0,0,0,0};
																				int nClock = clock();
if(g_pconnector->m_settings.m_ulDebug&CONNECTOR_DEBUG_COMM) 
	g_pconnector->m_msgr.DM(MSG_ICONHAND, NULL, szConnectorSource, "start event payload assembly");  //(Dispatch message)

																				if((pObj->m_ppevents)&&(pchPayload))
																				{
																					unsigned long ulBufferLen = SENTINEL_XML_EVENT_MAXPAYLOAD;
																					char* pchBufferEnd = (pchPayload+SENTINEL_XML_EVENT_MAXPAYLOAD);
																					int nEventLen = 0;
																					unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));
																					int q = 0;
																					int nCount = 0;
																					int nBuffers = 1;
																					int nTempOffset;
																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;

																						if(pObj->m_ppevents[q])
																						{
																					//		nTimes[0] += (clock() - nClock); nClock = clock(); 

																							CConnectorEventObject* pEObj = pObj->m_ppevents[q];
																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;

																				//			nTimes[1] += (clock() - nClock); nClock = clock(); 
																							if(bInclude)
																							{

																							//	_ftime(&timestamp);
																					//		nTimes[2] += (clock() - nClock); nClock = clock(); 

																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
																					//		nTimes[3] += (clock() - nClock); nClock = clock(); 
																								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, 
																									"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_usControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CConnectorEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																								);
																					//		nTimes[4] += (clock() - nClock); nClock = clock(); 
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																						//	nTimes[5] += (clock() - nClock); nClock = clock(); 
																								nEventLen = strlen(errorstring)+1;//+1 for term zero

																								if(pchAppend+nEventLen>pchBufferEnd)  // if must be changed to while if SENTINEL_XML_EVENT_MAXPAYLOAD defined < MAX_MESSAGE_LENGTH (buffer vs errorstring)
																								{
																									// need to realloc
																									nBuffers++;
																									ulBufferLen = SENTINEL_XML_EVENT_MAXPAYLOAD*nBuffers;
																									char* pchBufferNew = (char*)malloc(ulBufferLen);
																									if(pchBufferNew)
																									{
																										nTempOffset = pchAppend+1 - pchPayload;
																										memcpy(pchBufferNew, pchPayload, nTempOffset);
																										try {free(pchPayload);} catch(...){}
																										pchPayload = pchBufferNew;
																										pchBufferEnd = (pchPayload+ulBufferLen);
																										pchAppend = pchPayload+nTempOffset-1;																									

																									}
																								}
																								

																								// append
																								memcpy(pchAppend, errorstring, nEventLen);
																								pchAppend += nEventLen-1; // append over the term 0 next time.


																						//	nTimes[6] += (clock() - nClock); nClock = clock(); 


/*
																								msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
* /
																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
																				msg.ClearContent(CX_XML_BUFFER_DATA);
																				if(pchPayload)
																				{
																					msg.m_pchResponse[CX_XML_BUFFER_DATA] = pchPayload;		
																					ulDataLen = pchAppend - pchPayload;
																				}
if(g_pconnector->m_settings.m_ulDebug&CONNECTOR_DEBUG_COMM) 
	g_pconnector->m_msgr.DM(MSG_ICONHAND, NULL, szConnectorSource, "end event payload assembly");  //(Dispatch message)

																			}

																		}
																		m++;
																	}

/*																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;
* /
																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_pconnector->m_data.m_critChannels);

/* // not needed now, we tally the buffer as we go
																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																* /
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_event\" code=\"%d\">", nGlobalCount);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case SENTINEL_XML_TYPE_GETEVENT

// end Connector specific XML commands
////////////////////////////////////////////////////////
*/
														case CX_XML_TYPE_UNK://						0 // unknown
														default:
															{
																sprintf(errorstring, "Unsupported XML command: [%s]", (msg.m_pchType?msg.m_pchType:"") );
																g_pconnector->m_msgr.DM(MSG_ICONINFO, NULL, szConnectorSource, errorstring);
																//let's try to send a nak.

																msg.m_nType = CX_XML_TYPE_UNK;
																msg.m_nFailure=CX_XML_ERR_BADCMD;
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"%s\" code=\"%d\">", (msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Unsupported XML command [%s] (error %d)</msg>",(msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

															} break; //case CX_XML_TYPE_UNK://						0 // unknown
														}
															
														
														if(!bPersist) bCloseCommand = true;

													}
													else // no child nodes to cortex - can't identify command.
													{
														bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
														sprintf(errorstring, "Cortex XML format error: [%s]", pchXML );
														g_pconnector->m_msgr.DM(MSG_ICONINFO, NULL, szConnectorSource, errorstring);
														//let's try to send a nak.

														msg.m_nType = CX_XML_TYPE_UNK;
														msg.m_nFailure=CX_XML_ERR_NOCMD;
														msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
														msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Cortex XML format error %d</msg>", msg.m_nFailure);
														msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

													}
												}
											}
										}
									}
									else
									{
										// no child nodes, can't do nuttin.
										bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
										sprintf(errorstring, "XML format error: [%s]", pchXML );
										g_pconnector->m_msgr.DM(MSG_ICONINFO, NULL, szConnectorSource, errorstring);
										//let's try to send a nak.

										msg.m_nType = CX_XML_TYPE_UNK;
										msg.m_nFailure=CX_XML_ERR_NOCHILD;
										msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
										msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML format error %d</msg>", msg.m_nFailure);
										msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

									}
if(g_pconnector->m_settings.m_ulDebug&CONNECTOR_DEBUG_COMM) 
	g_pconnector->m_msgr.DM(MSG_ICONHAND, NULL, szConnectorSource, "end xml parse");  //(Dispatch message)

									n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }
								}

								if(!bResponseSent)
								{
if(g_pconnector->m_settings.m_ulDebug&CONNECTOR_DEBUG_COMM) 
	g_pconnector->m_msgr.DM(MSG_ICONHAND, NULL, szConnectorSource, "begin send response");  //(Dispatch message)

								// send it
		_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_pconnector)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending response message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_pconnector->m_msgr.DM(MSG_ICONERROR, NULL, szConnectorSource, errorstring);  //(Dispatch message)
				g_pconnector->SendMsg(CX_SENDMSG_ERROR, szConnectorSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
if(g_pconnector->m_settings.m_ulDebug&CONNECTOR_DEBUG_COMM) 
	g_pconnector->m_msgr.DM(MSG_ICONHAND, NULL, szConnectorSource, "end send response");  //(Dispatch message)

								}

								// log it

								//debug file write
								if((g_pconnector)&&(g_pconnector->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
								{
if(g_pconnector->m_settings.m_ulDebug&CONNECTOR_DEBUG_COMM) 
	g_pconnector->m_msgr.DM(MSG_ICONHAND, NULL, szConnectorSource, "begin log response");  //(Dispatch message)
									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_pconnector->m_settings.m_pszName?g_pconnector->m_settings.m_pszName:"Connector"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lasttxfilename, filename)==0)
									{
										nTxDupes++;
									}
									else
									{
										nTxDupes=0;
										strcpy(lasttxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

									fp = fopen(filename, "wb");
									if(fp)
									{
										int b=CX_XML_BUFFER_CORTEXBEGIN;
										while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
										{
//											fwrite(filename, 1, strlen(filename),fp);
											if(msg.m_pchResponse[b])
											{
												if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

												fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
												fflush(fp);
											}
											b++;

										}
										fclose(fp);
										fp = NULL;
									}
if(g_pconnector->m_settings.m_ulDebug&CONNECTOR_DEBUG_COMM) 
	g_pconnector->m_msgr.DM(MSG_ICONHAND, NULL, szConnectorSource, "end log response");  //(Dispatch message)
								}

//AfxMessageBox("001");
								int b=CX_XML_BUFFER_TX;
								while(b<CX_XML_BUFFER_DATAEND)
								{
									if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
									{
										try { free(msg.m_pchResponse[b]); } catch(...){}
										msg.m_pchResponse[b] = NULL;
									}
									b++;
								}

//AfxMessageBox("002");
								if(pucUUID)
								{
									try { RpcStringFree(&pucUUID); } catch(...){}
								}
								pucUUID = NULL;

//AfxMessageBox("003");
								if(pchXML) 
								{
									try { free(pchXML); } catch(...){}
								}
								pchXML = NULL;
//AfxMessageBox("004");

								if(pchRXID) 
								{
									try { free(pchRXID); } catch(...){}
								}
								pchRXID = NULL;

//AfxMessageBox("005");
								if (pchXMLStream) 
									pchEnd = strstr(pchXMLStream, "</cortex>");
								else pchEnd = NULL;
//AfxMessageBox("006");
							}  // while </cortex> exists.
						}//if(pchXMLStream)
						// dont do a free(pch);  
					}  // else out of mem, so just skip
					else
					{
						// out of mem.. .log error?
					}

				} //if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))

			} // if getline succeeds.
			else
			{
				// here, we either have timed out because theres no data, or, the connection has been lost.
				// if we lost the conn, no need to re-establish, we are a client handler, let the client reconnect if they want.
				if(nReturnCode == NET_ERROR_CONN)		// connection lost
				{
					ulRetry++;

					if(g_pconnector)
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH, "Lost connection from %d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno );

						g_pconnector->m_msgr.DM(MSG_ICONINFO, NULL, szConnectorSource, errorstring);  //(Dispatch message)
						g_pconnector->SendMsg(CX_SENDMSG_INFO, szConnectorSource, errorstring);
					}

					break;  // break out of while loop, closes connection, starts again
				}
				else
				{
//									g_pconnector->m_msgr.DM(MSG_ICONERROR, NULL, szConnectorSource, "check %d", clock());  //(Dispatch message)

					// we prob just havent gotten a message because no data.  
					// so, check to see if its time for publication service
									// no, do this in the stall loop


				}

			} // end of else from if(nReturnCode == NET_SUCCESS)

			// and lets check if we've timed out on the connection, no matter what the success was
			_ftime( &timestamp );
			if(
				  (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				&&(ulConnTimeout>0)
				&&(ulConnLastMessage>0)
				&&((ulConnLastMessage + ulConnTimeout) < (unsigned long)timestamp.time )
				)
			{
				// we timed out, need to disconnect
				ulRetry++;


				sprintf(errorstring, "Connection from %d.%d.%d.%d timed out.", 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno);

				if(g_pconnector)
				{
					g_pconnector->m_msgr.DM(MSG_ICONINFO, NULL, szConnectorSource, errorstring);  //(Dispatch message)
					g_pconnector->SendMsg(CX_SENDMSG_INFO, szConnectorSource, errorstring);
				}

				if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;} //free this memory before breaking out of the while.

				break;  // break out of while loop, closes connection, starts again
			}

			if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;}
			
			if(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) Sleep(1);  // dont peg processor




/*
			// following line fills out the data structure
//			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);
			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Connector:XMLHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Connector:XMLHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Connector:XMLHandlerThread");
						}
					}
				}
			}
*/
			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Connector:XMLHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
//						g_pconnector->m_msgr.DM(MSG_ICONERROR, NULL, szConnectorSource, "check %d", clock());  //(Dispatch message)


						if(nPeriodic>0)
						{
							_ftime( &timestamp );
							if((timeperiodic.time + nPeriodic)< timestamp.time)
							{
								// send asynchronous message

								//create a response UUID
								UUID uuida;
								UuidCreate(&uuida); // could check response code to be RPC_S_OK ?

								unsigned char* pucUUIDa = NULL;
								UuidToString(&uuida, &pucUUIDa);

								msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUIDa);

								if(pucUUIDa)
								{
									try { RpcStringFree(&pucUUIDa); } catch(...){}
								}
								pucUUIDa = NULL;


								msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"system\" code=\"0\">");

								msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
								msg.ClearContent(CX_XML_BUFFER_FLAGS);

								unsigned long ulDataLen = 0;
								char* pchStatus = g_pconnector->m_data.GetStatusText(&ulDataLen);
								if(pchStatus)
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0x%08x</code><text>%s</text></status><time>%d.%03d</time></system>",
										ulDataLen,
										pchStatus,
										timestamp.time, timestamp.millitm
										);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0xffffffff</code><text>unavailable</text></status><time>%d.%03d</time></system>",
										timestamp.time, timestamp.millitm
										);

								}

								if(pchStatus){try { free(pchStatus); } catch(...){} pchStatus=NULL;}

								ulDataLen = 0;
								if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
								{
									ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
								}
								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
									ulDataLen,
									net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
									);

								msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>%s status message</msg>", 
									(g_pconnector->m_settings.m_pszName?g_pconnector->m_settings.m_pszName:"Connector"));

								msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


								// now, send and log.
	////////////////////////////////////////////// code copied from above.  if modified, must be 2 places.  sorry...
									// send it
			_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_pconnector)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending system message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_pconnector->m_msgr.DM(MSG_ICONERROR, NULL, szConnectorSource, errorstring);  //(Dispatch message)
				g_pconnector->SendMsg(CX_SENDMSG_ERROR, szConnectorSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
								
	//AfxMessageBox("oop");
									// log it

									//debug file write
									if((g_pconnector)&&(g_pconnector->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
									{
										strcpy(filename, "Logs");
										_mkdir(filename);  // if exists already np
										strcat(filename, "\\");
										sprintf(errorstring, "%d.%d.%d.%d", 
											pClient->m_si.sin_addr.s_net, 
											pClient->m_si.sin_addr.s_host, 
											pClient->m_si.sin_addr.s_lh, 
											pClient->m_si.sin_addr.s_impno);

										strcat(filename, errorstring);

										_mkdir(filename);  // if exists already np

		//								_ftime( &timestamp );  // use timestamp we just had..

										tm* theTime = localtime( &timestamp.time	);

										sprintf(errorstring, "\\%s_", 
											(g_pconnector->m_settings.m_pszName?g_pconnector->m_settings.m_pszName:"Connector"));

										strcat(filename, errorstring);

										char filenametemp[MAX_PATH];
										strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

										strcat(filename, filenametemp);

										int nOffset = strlen(filename);
										sprintf(filename+nOffset,"%03d",timestamp.millitm);

										if (strcmp(lasttxfilename, filename)==0)
										{
											nTxDupes++;
										}
										else
										{
											nTxDupes=0;
											strcpy(lasttxfilename, filename);
										}
										nOffset = strlen(filename);
										sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

										fp = fopen(filename, "wb");
										if(fp)
										{
											int b=CX_XML_BUFFER_CORTEXBEGIN;
											while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
											{
	//											fwrite(filename, 1, strlen(filename),fp);
												if(msg.m_pchResponse[b])
												{
													if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

													fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
													fflush(fp);
												}
												b++;

											}
											fclose(fp);
											fp = NULL;
										}
									}


									b=CX_XML_BUFFER_TX;
									while(b<CX_XML_BUFFER_DATAEND)
									{
										if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
										{
											try { free(msg.m_pchResponse[b]); } catch(...){}
											msg.m_pchResponse[b] = NULL;
										}
										b++;
									}
	/////////////////////////////////////////////
							}

						}

						//and check timeout
						if(nTimed>0)
						{
							_ftime( &timestamp );
							if((timeactive.time + nTimed) < timestamp.time)
							{
								//kill it

								bCloseCommand = true;
							}
							
						}


						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} 

cleanup:
//		AfxMessageBox("cleanup");
//AfxMessageBox("010");

//		pDoc->Release();  // destructor calls this so don't do it here. 
		if(pSafety) pSafety->Release();

//AfxMessageBox("011");

// cleanup:  //was here but want to uninit com etc
//AfxMessageBox("012");

		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d closed.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);

//AfxMessageBox("013");
		if(g_pconnector)
		{
			g_pconnector->m_msgr.DM(MSG_ICONINFO, NULL, szConnectorSource, errorstring);  //(Dispatch message)
			g_pconnector->SendMsg(CX_SENDMSG_INFO, szConnectorSource, errorstring);
		}

//AfxMessageBox("014");

		if(pch!=NULL) { try{free(pch);} catch(...) {} } // must free the data buffer 

//AfxMessageBox("015");

		shutdown(pClient->m_socket, SD_BOTH);
//AfxMessageBox("016");
		closesocket(pClient->m_socket);
//AfxMessageBox("017");
		(*(pClient->m_pulConnections))--;
//AfxMessageBox("018");

	}
//		AfxMessageBox("end cleanup");
//AfxMessageBox("019");

	try
	{  
		CoUninitialize(); //XML
	}
	catch(...)
	{
		if(g_pconnector) g_pconnector->m_msgr.DM(MSG_ICONERROR, NULL, szConnectorSource, "Exception in CoUninitialize");  //(Dispatch message)
	}

//AfxMessageBox("020");

	try
	{
		delete pClient; // was created with new in the thread that spawned this one.
	}	catch(...) {}

//AfxMessageBox("021");
	if (bSetGlobalKill)	//g_bKillThread = true;
		((CConnectorHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}

#endif //#ifdef USE LISTENERS_AND_MESSAGING



