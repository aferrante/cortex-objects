// ConnectorLicenseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Connector.h"
#include "ConnectorLicenseDlg.h"
#include "../../Common/KEY/LicenseKey.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CConnectorLicenseDlg dialog


CConnectorLicenseDlg::CConnectorLicenseDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConnectorLicenseDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CConnectorLicenseDlg)
	m_szKey = _T("");
	//}}AFX_DATA_INIT
}


void CConnectorLicenseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConnectorLicenseDlg)
	DDX_Text(pDX, IDC_EDIT1, m_szKey);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CConnectorLicenseDlg, CDialog)
	//{{AFX_MSG_MAP(CConnectorLicenseDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConnectorLicenseDlg message handlers

void CConnectorLicenseDlg::OnOK() 
{
	// TODO: Add extra validation here
	CLicenseKey key;
	key.m_pszLicenseString = (char*)malloc(strlen(m_szKey)+1);
	if(key.m_pszLicenseString)
	sprintf(key.m_pszLicenseString, "%s", m_szKey);

	key.InterpretKey();

	if(!key.m_bValid)
	{
		AfxMessageBox("This is not a valid license code."); 
		return;
	}

	CDialog::OnOK();
}
