; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CConnectorDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Connector.h"
LastPage=0

ClassCount=8
Class1=CAboutDlg
Class2=CConnectorApp
Class3=CConnectorHandler
Class4=CConnectorDlg
Class5=CConnectorSettings

ResourceCount=6
Resource1=IDD_CONNECTOR_DIALOG
Resource2=IDD_SETTINGS_DIALOG
Resource3=IDD_LICENSE_DIALOG
Class6=CConnectorSettingsDlg
Resource4=IDD_KEYDIALOG
Class7=CConnectorLicenseDlg
Resource5=IDD_ABOUTBOX
Class8=CConnectorKeySetupDlg
Resource6=IDR_MENU1

[CLS:CAboutDlg]
Type=0
HeaderFile=connectordlg.h
ImplementationFile=connectordlg.cpp
BaseClass=CDialog
LastObject=CAboutDlg

[CLS:CConnectorApp]
Type=0
BaseClass=CWinApp
HeaderFile=Connector.h
ImplementationFile=Connector.cpp
Filter=N
VirtualFilter=AC
LastObject=CConnectorApp

[CLS:CConnectorHandler]
Type=0
BaseClass=CWnd
HeaderFile=ConnectorHandler.h
ImplementationFile=ConnectorHandler.cpp
Filter=W
VirtualFilter=WC
LastObject=CConnectorHandler

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=8
Control1=IDC_STATICTEXT_TITLE,static,1342308481
Control2=IDC_STATICTEXT_URL,static,1208090625
Control3=IDOK,button,1342373889
Control4=IDC_STATIC_LOGO,static,1342177294
Control5=IDC_URLFRAME,static,1342177298
Control6=IDC_STATIC_BUILD,static,1342308353
Control7=IDC_STATIC_URL,static,1342177294
Control8=IDC_STATICTEXT_COPYRIGHT,static,1342308353

[MNU:IDR_MENU1]
Type=1
Class=?
Command1=ID_CMD_ABOUT
Command2=ID_CMD_SHOWWND
Command3=ID_CMD_EXIT
Command4=ID_CMD_ABOUT
Command5=ID_CMD_SHOWWND
Command6=ID_CMD_EXIT
CommandCount=6

[CLS:CConnectorDlg]
Type=0
HeaderFile=ConnectorDlg.h
ImplementationFile=ConnectorDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CConnectorDlg

[CLS:CConnectorSettings]
Type=0
HeaderFile=ConnectorSettings.h
ImplementationFile=ConnectorSettings.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CConnectorSettings

[DLG:IDD_CONNECTOR_DIALOG]
Type=1
Class=CConnectorDlg
ControlCount=9
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_SETTINGS,button,1342177408
Control4=IDC_LIST1,SysListView32,1350681613
Control5=IDC_STATIC_STATUSTEXT,static,1342308352
Control6=IDC_STATIC_PROGBAR,static,1342177294
Control7=IDC_TREE1,SysTreeView32,1082130487
Control8=IDC_CHECK_PUSHPIN,button,1342247043
Control9=IDC_CHECK_KEEPFOCUS,button,1342247043

[DLG:IDD_SETTINGS_DIALOG]
Type=1
Class=CConnectorSettingsDlg
ControlCount=17
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308354
Control4=IDC_STATIC,static,1342308354
Control5=IDC_LIST1,SysListView32,1350631429
Control6=IDC_BUTTON_ADD,button,1342242816
Control7=IDC_BUTTON_EDIT,button,1342242816
Control8=IDC_BUTTON_DELETE,button,1342242816
Control9=IDC_EDIT_PLUGIN,edit,1350631552
Control10=IDC_EDIT_TOKEN,edit,1350631552
Control11=IDC_STATIC,static,1342308352
Control12=IDC_CHECK_SUSPEND,button,1342251011
Control13=IDC_STATIC,static,1342308354
Control14=IDC_EDIT_IP,edit,1350631552
Control15=IDC_STATIC,static,1342308354
Control16=IDC_EDIT_PORT,edit,1350631552
Control17=IDC_CHECK_ASRUN,button,1342242819

[CLS:CConnectorSettingsDlg]
Type=0
HeaderFile=ConnectorSettingsDlg.h
ImplementationFile=ConnectorSettingsDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CConnectorSettingsDlg

[DLG:IDD_LICENSE_DIALOG]
Type=1
Class=CConnectorLicenseDlg
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1073807360
Control3=IDC_EDIT1,edit,1350631552

[CLS:CConnectorLicenseDlg]
Type=0
HeaderFile=ConnectorLicenseDlg.h
ImplementationFile=ConnectorLicenseDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDOK

[DLG:IDD_KEYDIALOG]
Type=1
Class=CConnectorKeySetupDlg
ControlCount=10
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1073807360
Control3=IDC_COMBO_KEYS,combobox,1344340227
Control4=IDC_STATIC,static,1342308352
Control5=IDC_CHECK_ALT,button,1342242819
Control6=IDC_CHECK_SHIFT,button,1342242819
Control7=IDC_CHECK_CTRL,button,1342242819
Control8=IDC_CHECK_APPFOCUS,button,1342242819
Control9=IDC_CHECK_ACTIVE,button,1342242819
Control10=IDC_BUTTON_HELP,button,1342242816

[CLS:CConnectorKeySetupDlg]
Type=0
HeaderFile=ConnectorKeySetupDlg.h
ImplementationFile=ConnectorKeySetupDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CConnectorKeySetupDlg

