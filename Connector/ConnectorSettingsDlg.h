#if !defined(AFX_CONNECTORSETTINGSDLG_H__AB4129D8_57F2_438D_A45C_0E39872691BC__INCLUDED_)
#define AFX_CONNECTORSETTINGSDLG_H__AB4129D8_57F2_438D_A45C_0E39872691BC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ConnectorSettingsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CConnectorSettingsDlg dialog

class CConnectorSettingsDlg : public CDialog
{
// Construction
public:
	CConnectorSettingsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CConnectorSettingsDlg();
	int FindKey(int nCode, int nSysChars);

	void DoList();


	CConnectorMappingObject** m_ppMappingObj;
	int m_nNumMappingObjects;


// Dialog Data
	//{{AFX_DATA(CConnectorSettingsDlg)
	enum { IDD = IDD_SETTINGS_DIALOG };
	CListCtrl	m_lce;
	BOOL	m_bSuspend;
	CString	m_szIP;
	CString	m_szPlugin;
	int		m_nPort;
	CString	m_szToken;
	BOOL	m_bAsRun;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CConnectorSettingsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CConnectorSettingsDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	virtual void OnOK();
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonDelete();
	afx_msg void OnButtonEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONNECTORSETTINGSDLG_H__AB4129D8_57F2_438D_A45C_0E39872691BC__INCLUDED_)
