// ReComConnectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Radiance.h"
#include "ReComConnectDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CReComConnectDlg dialog

CReComConnectDlg::CReComConnectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CReComConnectDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CReComConnectDlg)
	m_reName = _T("hostName");
	m_reOut = _T("");
	m_exportVal = _T("");
	m_scene = _T("");
	m_project = _T("");
	m_scenes = _T("");
	m_anims = _T("");
	m_export = _T("");
	m_status = _T("");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_bManual = FALSE;
	m_bConnected = FALSE;
	m_bConnecting = FALSE;
	m_bGotProjects = FALSE;
	m_bGotScenes = FALSE;

	m_loadedproject = _T("");
	m_activescene = _T("");

}

void CReComConnectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReComConnectDlg)
	DDX_Control(pDX, IDC_ANIMS_COMBO, m_animsCombo);
	DDX_Control(pDX, IDC_EXPORT_COMBO, m_exportsCombo);
	DDX_Control(pDX, IDC_SCENES_COMBO, m_scenesCombo);
	DDX_Control(pDX, IDC_SCENE_COMBO, m_sceneCombo);
	DDX_Control(pDX, IDC_PROJECT_COMBO, m_projectCombo);
	DDX_Control(pDX, IDC_EDIT_SEND, m_reSend);
	DDX_Control(pDX, IDC_RECOM, m_recom);
	DDX_Text(pDX, IDC_EDIT_RE_HOSTNAME, m_reName);
	DDX_Text(pDX, IDC_EDIT_OUTPUT, m_reOut);
	DDX_Text(pDX, IDC_EXPORT_VAL, m_exportVal);
	DDX_CBString(pDX, IDC_SCENE_COMBO, m_scene);
	DDX_CBString(pDX, IDC_PROJECT_COMBO, m_project);
	DDX_CBString(pDX, IDC_SCENES_COMBO, m_scenes);
	DDX_CBString(pDX, IDC_ANIMS_COMBO, m_anims);
	DDX_CBString(pDX, IDC_EXPORT_COMBO, m_export);
	DDX_Text(pDX, IDC_STATIC_STATUS, m_status);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CReComConnectDlg, CDialog)
	//{{AFX_MSG_MAP(CReComConnectDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_EN_KILLFOCUS(IDC_EDIT_RE_HOSTNAME, OnKillfocusEditReHostname)
	ON_BN_CLICKED(IDC_BUTTON_SEND, OnButtonSend)
	ON_BN_CLICKED(IDDISCONNECT, OnDisconnect)
	ON_BN_CLICKED(IDC_LOAD, OnLoad)
	ON_BN_CLICKED(IDC_SEND_EXPORT, OnSendExport)
	ON_BN_CLICKED(IDC_START_ANIM, OnStartAnim)
	ON_CBN_SELCHANGE(IDC_PROJECT_COMBO, OnSelchangeProjectCombo)
	ON_BN_CLICKED(IDC_UNLOAD, OnUnload)
	ON_BN_CLICKED(IDC_UNLOADALL, OnUnloadall)
	ON_BN_CLICKED(IDC_ACTIVATE, OnActivate)
	ON_BN_CLICKED(IDC_STOP_ANIM, OnStopAnim)
	ON_BN_CLICKED(IDC_BUTTON_MANUAL, OnButtonManual)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CReComConnectDlg message handlers

BOOL CReComConnectDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
/*
	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
	*/

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	CBitmap bmp;

  bmp.LoadBitmap(IDB_BITMAP_DOWN);
	m_hbmp[0] = m_bmpu.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmpu.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BITMAP_UP);
	m_hbmp[1] = m_bmpu.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmpu.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();

	// uncomment below to set up buttons
	((CButton*)GetDlgItem(IDC_BUTTON_MANUAL))->SetBitmap(m_hbmp[0]);

	GetWindowRect(&m_rcWin[0]);

	GetDlgItem(IDC_STATIC_CONTRACTED)->GetWindowRect(&m_rcWin[1]);
	m_rcWin[0].right = m_rcWin[1].right;
	m_rcWin[0].bottom = m_rcWin[1].bottom;


	GetWindowRect(&m_rcWin[1]);

	SetWindowPos(NULL, 0, 0, m_rcWin[0].Width(), m_rcWin[0].Height(), SWP_NOZORDER|SWP_NOMOVE);

//	ShowWindow(SW_HIDE);

	return TRUE;  // return TRUE  unless you set the focus to a control
}


// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CReComConnectDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CReComConnectDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

BEGIN_EVENTSINK_MAP(CReComConnectDlg, CDialog)
    //{{AFX_EVENTSINK_MAP(CReComConnectDlg)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 1 /* ReDataIncoming */, OnReDataIncoming, VTS_BSTR)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 2 /* ReSceneLoaded */, OnReSceneLoaded, VTS_BSTR)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 3 /* ReSceneActivated */, OnReSceneActivated, VTS_BSTR)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 4 /* ReSceneUnloaded */, OnReSceneUnloaded, VTS_BSTR)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 5 /* ReTickerDataIn */, OnReTickerDataIn, VTS_BSTR)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 6 /* ReTickerDataOut */, OnReTickerDataOut, VTS_BSTR)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 7 /* ReConnected */, OnReConnected, VTS_NONE)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 8 /* ReDisconnected */, OnReDisconnected, VTS_NONE)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 9 /* ReOnAir */, OnReOnAir, VTS_NONE)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 10 /* ReOffAir */, OnReOffAir, VTS_NONE)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 11 /* ReSceneAnimations */, OnReSceneAnimations, VTS_PVARIANT)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 12 /* ReTimeout */, OnReTimeout, VTS_BSTR VTS_I4)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 13 /* ReSceneExports */, OnReSceneExports, VTS_PVARIANT)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 14 /* ReAnimationStarted */, OnReAnimationStarted, VTS_BSTR)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 15 /* ReAnimationStopped */, OnReAnimationStopped, VTS_BSTR VTS_I4)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 16 /* ReAnimationContinued */, OnReAnimationContinued, VTS_BSTR)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 17 /* ReAnimationRewinded */, OnReAnimationRewinded, VTS_BSTR)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 18 /* ReExportUpdated */, OnReExportUpdated, VTS_BSTR VTS_BSTR)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 19 /* ReTickerStarted */, OnReTickerStarted, VTS_BSTR)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 20 /* ReTickerStopped */, OnReTickerStopped, VTS_BSTR)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 31 /* ReVersion */, OnReVersionRecom, VTS_BSTR)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 28 /* ReProjects */, OnReProjectsRecom, VTS_PVARIANT)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 27 /* ReProjectScenes */, OnReProjectScenesRecom, VTS_BSTR VTS_PVARIANT)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 30 /* ReScenesLoaded */, OnReScenesLoadedRecom, VTS_PVARIANT)
	ON_EVENT(CReComConnectDlg, IDC_RECOM, 32 /* ReActiveScene */, OnReActiveSceneRecom, VTS_BSTR)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

void CReComConnectDlg::OnReDataIncoming(LPCTSTR aData) 
{
	// TODO: Add your control notification handler code here
  UpdateData( TRUE );
  m_reOut += aData;
  m_reOut += _T("\r\n");

  UpdateData( FALSE );
}

void CReComConnectDlg::OnReSceneLoaded(LPCTSTR aSceneName) 
{
  m_exportsCombo.ResetContent();
  m_animsCombo.ResetContent();
  m_recom.askForExports();
  m_recom.askForAnimations();
  m_recom.askForLoadedScenes();
}

void CReComConnectDlg::OnReSceneActivated(LPCTSTR aSceneName) 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  m_reOut += _T("Scene Activated: ");
  m_reOut += aSceneName;
  m_reOut += _T("\r\n");

  UpdateData( FALSE );
  m_exportsCombo.ResetContent();
  m_animsCombo.ResetContent();
  m_recom.askForExports();
  m_recom.askForAnimations();
  m_scenesCombo.SelectString(-1, aSceneName);

	m_activescene = aSceneName;

}

void CReComConnectDlg::OnReSceneUnloaded(LPCTSTR aSceneName) 
{
	// TODO: Add your control notification handler code here
  m_exportsCombo.ResetContent();
  m_animsCombo.ResetContent();
  m_recom.askForActiveScene();
}

void CReComConnectDlg::OnReTickerDataIn(LPCTSTR aTickerName) 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  m_reOut += _T("TickerDataIn: ");
  m_reOut += aTickerName;
  m_reOut += _T("\r\n");

  UpdateData( FALSE );
}

void CReComConnectDlg::OnReTickerDataOut(LPCTSTR aTickerName) 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  m_reOut += _T("TickerDataOut: ");
  m_reOut += aTickerName;
  m_reOut += _T("\r\n");

  UpdateData( FALSE );
}

void CReComConnectDlg::OnReConnected() 
{
	// TODO: Add your control notification handler code here
	m_bConnected = TRUE;
	m_bConnecting = FALSE;
  UpdateData( TRUE );
  m_reOut += _T("Connected ");
  m_reOut += m_recom.GetRE_COM_version();
  m_reOut += _T("\r\n");
  m_recom.askForREVersion();
  m_recom.askForProjects();
  UpdateData( FALSE );

	UpdateData( TRUE );
//	AfxMessageBox(m_reOut);

}

void CReComConnectDlg::OnReDisconnected() 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  m_reOut += _T("Disconnected");
  m_reOut += _T("\r\n");
  m_projectCombo.ResetContent();
  m_sceneCombo.ResetContent();
  m_scenesCombo.ResetContent();
  m_exportsCombo.ResetContent();
  m_animsCombo.ResetContent();
  m_project = "";
  m_scene = "";
  m_scenes = "";
  m_export = "";
  m_anims = "";

  UpdateData( FALSE );
	m_bConnected = FALSE;
	m_bConnecting = FALSE;
	m_bGotProjects = FALSE;
	m_bGotScenes = FALSE;
}

void CReComConnectDlg::OnReOnAir() 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  m_reOut += _T("OnAir");
  m_reOut += _T("\r\n");
//	AfxMessageBox(m_reOut);

  UpdateData( FALSE );
}

void CReComConnectDlg::OnReOffAir() 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  m_reOut += _T("OffAir");
  m_reOut += _T("\r\n");

  UpdateData( FALSE );
}

void CReComConnectDlg::OnReSceneAnimations(VARIANT FAR* pVal) 
{
	// TODO: Add your control notification handler code here
	fillComboBox(m_animsCombo, pVal);
}

void CReComConnectDlg::OnReTimeout(LPCTSTR aTimerName, long aMsecSlice) 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  m_reOut += _T("Timeout:");
  m_reOut += _T("\r\n");

  UpdateData( FALSE );
}

void CReComConnectDlg::OnReSceneExports(VARIANT FAR* pVal) 
{
	// TODO: Add your control notification handler code here
	fillComboBox(m_exportsCombo, pVal);
}

void CReComConnectDlg::OnReAnimationStarted(LPCTSTR anAnimName) 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  m_reOut += _T("AnimationStarted: ");
  m_reOut += anAnimName;
  m_reOut += _T("\r\n");

  UpdateData( FALSE );
}

void CReComConnectDlg::OnReAnimationStopped(LPCTSTR anAnimName, long aFrame) 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  m_reOut += _T("AnimationStopped: ");
  m_reOut += anAnimName;
  m_reOut += _T("\r\n");

  UpdateData( FALSE );
}

void CReComConnectDlg::OnReAnimationContinued(LPCTSTR anAnimationName) 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  m_reOut += _T("AnimationContinued ");
  m_reOut += anAnimationName;
  m_reOut += _T("\r\n");

  UpdateData( FALSE );
}

void CReComConnectDlg::OnReAnimationRewinded(LPCTSTR anAnimationName) 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  m_reOut += _T("AnimationRewinded: ");
  m_reOut += anAnimationName;
  m_reOut += _T("\r\n");

  UpdateData( FALSE );
}

void CReComConnectDlg::OnReExportUpdated(LPCTSTR anExportName, LPCTSTR anExportValue) 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  m_reOut += _T("ExportUpdated: ");
  m_reOut += anExportName;
  m_reOut += _T("\r\n");

  UpdateData( FALSE );
}

void CReComConnectDlg::OnReTickerStarted(LPCTSTR aTickerRunExpName) 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  m_reOut += _T("TickerStarted: ");
  m_reOut += aTickerRunExpName;
  m_reOut += _T("\r\n");

  UpdateData( FALSE );
}

void CReComConnectDlg::OnReTickerStopped(LPCTSTR aTickerRunExpName) 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  m_reOut += _T("TickerStopped: ");
  m_reOut += aTickerRunExpName;
  m_reOut += _T("\r\n");

  UpdateData( FALSE );
}

void CReComConnectDlg::OnButtonConnect() 
{
	CWaitCursor cw;
	// TODO: Add your control notification handler code here
	m_bConnecting = TRUE;

	if (m_recom.GetConnectionStatus())
	{
		m_bConnected = TRUE;
	}
	else
	{
		m_bConnected = FALSE;
	}
	if(!m_bConnected)
	{
		m_recom.connect(m_reName, RADIANCE_PORT_RECOM);
	}
	else
	{
		m_bConnecting = FALSE;
	}
}
void CReComConnectDlg::OnDisconnect() 
{
	CWaitCursor cw;
	// TODO: Add your control notification handler code here
	if(m_bConnected) m_recom.disconnect();
//	m_bConnected = FALSE;
//	m_bConnecting = FALSE;
}

void CReComConnectDlg::OnKillfocusEditReHostname() 
{
	// TODO: Add your control notification handler code here
 CWnd::UpdateData( TRUE );	
}


void CReComConnectDlg::OnButtonSend() 
{
	// TODO: Add your control notification handler code here
  CString buf;
  m_reSend.GetLine(0, buf.GetBuffer(512));
  m_recom.sendCommand(buf);
	
}


void CReComConnectDlg::OnLoad() 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
	m_recom.loadScene(m_project, m_scene);
	m_bGotProjects = FALSE;
	m_bGotScenes = FALSE;

	m_loadedproject = m_project;
	m_activescene = m_scene;

}

void CReComConnectDlg::OnSendExport() 
{
	// TODO: Add your control notification handler code here
  CString exp;
	int n =m_exportsCombo.GetCurSel();
	m_exportsCombo.SetCurSel(n);
	UpdateData( TRUE );

  m_exportsCombo.GetLBText(n, exp);

	m_recom.sendExport(exp, m_exportVal);
	
}

void CReComConnectDlg::OnStartAnim() 
{
	// TODO: Add your control notification handler code here
  CString anim;
	int n =m_animsCombo.GetCurSel();
	m_animsCombo.SetCurSel(n);

  m_animsCombo.GetLBText(n, anim);
  
	m_recom.startAnimation(anim);
	
}

BOOL CReComConnectDlg::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	WORD wNotifyCode = HIWORD(wParam); // notification code 
  WORD wID = LOWORD(wParam);         // item, control, or accelerator identifier

  switch (wNotifyCode)
    {
    case BN_CLICKED:
      switch (wID)
        {
        case IDOK:
			    TRACE(_T("IDOK\n"));
          return 0;
		    //case IDCANCEL:
				}
     break;
    }
	
	return CDialog::OnCommand(wParam, lParam);
}

void CReComConnectDlg::OnReVersionRecom(LPCTSTR aVersion) 
{
	// TODO: Add your control notification handler code here
  m_reOut += _T("TRE version: ");
  m_reOut += aVersion;
  m_reOut += _T("\r\n");
}

void CReComConnectDlg::OnReProjectsRecom(VARIANT FAR* pVal) 
{
	// TODO: Add your control notification handler code here
	fillComboBox(m_projectCombo, pVal);
	UpdateData( TRUE );
	m_bGotProjects = TRUE;
  m_recom.askForScenes(m_project);
}

void CReComConnectDlg::OnReProjectScenesRecom(LPCTSTR aProjectName, VARIANT FAR* pVal) 
{
	fillComboBox(m_sceneCombo, pVal);
	m_bGotScenes = TRUE;
}

void CReComConnectDlg::OnSelchangeProjectCombo() 
{
	// TODO: Add your control notification handler code here
	//UpdateData( TRUE );
  CString proj;
  m_projectCombo.GetLBText(m_projectCombo.GetCurSel(), proj);
  m_recom.askForScenes(proj);
}

void CReComConnectDlg::OnReScenesLoadedRecom(VARIANT FAR* pVal) 
{
	// TODO: Add your control notification handler code here
	fillComboBox(m_scenesCombo, pVal);
	m_bGotScenes = TRUE;
  m_exportsCombo.ResetContent();
  m_animsCombo.ResetContent();
  m_recom.askForActiveScene();
 }

void CReComConnectDlg::fillComboBox(CComboBox &combo, VARIANT FAR* pVal)
{
  combo.ResetContent();
	VARIANT *vProjects = pVal;
  SAFEARRAY *psa;
	psa = vProjects->parray;
  BSTR* bstrArray;
  SafeArrayAccessData(psa, reinterpret_cast<void**>(&bstrArray));
  for (UINT i = 0; i < psa->rgsabound->cElements; i++)
  {
		if (bstrArray[i] != NULL)  
		{
			CString szInput;
			LPSTR   p = (char*)szInput.GetBuffer(::SysStringLen(bstrArray[i]) + 1);
			BOOL    bUsedDefaultChar;

			::WideCharToMultiByte(CP_ACP, 0, bstrArray[i], -1, 
							p, ::SysStringLen(bstrArray[i])+1, 
							NULL, &bUsedDefaultChar);
			szInput.ReleaseBuffer();
	    combo.AddString(szInput);
		}
  }
  SafeArrayUnaccessData(psa);
  combo.SetCurSel(0);
}

void CReComConnectDlg::OnUnload() 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  if (m_scenes == "")
    return;
	m_recom.unloadScene(m_scenes);
  m_scenesCombo.DeleteString(m_scenesCombo.GetCurSel());
	m_recom.askForActiveScene();
  m_exportsCombo.ResetContent();
  m_animsCombo.ResetContent();
  m_export = "";
  m_anims = "";
  UpdateData( FALSE );

}

void CReComConnectDlg::OnUnloadall() 
{
	// TODO: Add your control notification handler code here
while (m_scenesCombo.GetCount())
  {
  m_scenesCombo.SetCurSel(0);
  OnUnload();
  }
m_scenes = "";

UpdateData( FALSE );
}

void CReComConnectDlg::OnActivate() 
{
	// TODO: Add your control notification handler code here
	UpdateData( TRUE );
  if (m_scenes == "")
    return;
  m_recom.activateScene(m_scenes);	
  m_recom.askForActiveScene();
}

void CReComConnectDlg::OnReActiveSceneRecom(LPCTSTR aSceneName) 
{
	// TODO: Add your control notification handler code here
	m_scenesCombo.SelectString(-1, aSceneName);
	m_activescene = aSceneName;
  m_exportsCombo.ResetContent();
  m_animsCombo.ResetContent();
  m_recom.askForExports();
  m_recom.askForAnimations();
}

void CReComConnectDlg::OnStopAnim() 
{
	// TODO: Add your control notification handler code here
  CString anim;
  m_animsCombo.GetLBText(m_animsCombo.GetCurSel(), anim);
  
	m_recom.stopAnimation(anim);
	
}

BOOL CReComConnectDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(CReComConnectDlg::IDD, pParentWnd);
}

void CReComConnectDlg::OnButtonManual() 
{
	if(m_bManual)
	{
		m_bManual = FALSE;
		((CButton*)GetDlgItem(IDC_BUTTON_MANUAL))->SetBitmap(m_hbmp[0]);

		SetWindowPos(NULL, 0, 0, m_rcWin[0].Width(), m_rcWin[0].Height(), SWP_NOZORDER|SWP_NOMOVE);
	}
	else
	{
		m_bManual = TRUE;
		((CButton*)GetDlgItem(IDC_BUTTON_MANUAL))->SetBitmap(m_hbmp[1]);

		SetWindowPos(NULL, 0, 0, m_rcWin[1].Width(), m_rcWin[1].Height(), SWP_NOZORDER|SWP_NOMOVE);
	}

	GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(m_bManual);
	GetDlgItem(IDDISCONNECT)->EnableWindow(m_bManual);
	GetDlgItem(IDC_LOAD)->EnableWindow(m_bManual);
	GetDlgItem(IDC_ACTIVATE)->EnableWindow(m_bManual);
	GetDlgItem(IDC_UNLOAD)->EnableWindow(m_bManual);
	GetDlgItem(IDC_UNLOADALL)->EnableWindow(m_bManual);
	GetDlgItem(IDC_SEND_EXPORT)->EnableWindow(m_bManual);
	GetDlgItem(IDC_START_ANIM)->EnableWindow(m_bManual);
	GetDlgItem(IDC_STOP_ANIM)->EnableWindow(m_bManual);

	GetDlgItem(IDC_PROJECT_COMBO)->EnableWindow(m_bManual);
	GetDlgItem(IDC_SCENE_COMBO)->EnableWindow(m_bManual);
	GetDlgItem(IDC_SCENES_COMBO)->EnableWindow(m_bManual);
	GetDlgItem(IDC_EXPORT_COMBO)->EnableWindow(m_bManual);
	GetDlgItem(IDC_EXPORT_VAL)->EnableWindow(m_bManual);
	GetDlgItem(IDC_ANIMS_COMBO)->EnableWindow(m_bManual);

}

void CReComConnectDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	ShowWindow(SW_HIDE);
	GetParent()->SetFocus();

//	CDialog::OnCancel();
}

void CReComConnectDlg::OnOK() 
{
	//CDialog::OnOK();
}
