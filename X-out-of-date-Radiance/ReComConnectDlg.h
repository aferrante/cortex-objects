// ReComConnectDlg.h : header file
//
//{{AFX_INCLUDES()
#include "ReCom.h"
//}}AFX_INCLUDES

#if !defined(AFX_RECOMCONNECTDLG_H__A68742F0_D65A_454E_9A18_81222E6DD356__INCLUDED_)
#define AFX_RECOMCONNECTDLG_H__A68742F0_D65A_454E_9A18_81222E6DD356__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


/////////////////////////////////////////////////////////////////////////////
// CReComConnectDlg dialog

class CReComConnectDlg : public CDialog
{
// Construction
public:
	CReComConnectDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CReComConnectDlg)
	enum { IDD = IDD_RECOMCONNECT_DIALOG };
	CComboBox	m_animsCombo;
	CComboBox	m_exportsCombo;
	CComboBox	m_scenesCombo;
	CComboBox	m_sceneCombo;
	CComboBox	m_projectCombo;
	CEdit	m_reSend;
	CReCom	m_recom;
	CString	m_reName;
	CString	m_reOut;
	CString	m_exportVal;
	CString	m_scene;
	CString	m_project;
	CString	m_scenes;
	CString	m_anims;
	CString	m_export;
	CString	m_status;
	//}}AFX_DATA

	BOOL m_bConnected;
	BOOL m_bConnecting;
	BOOL m_bGotProjects;
	BOOL m_bGotScenes;

	CString	m_loadedproject;
	CString	m_activescene;

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CReComConnectDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd = NULL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	BOOL m_bManual;
	CBmpUtil m_bmpu;
	HBITMAP m_hbmp[2];
	CRect m_rcWin[2];

public:
	// Generated message map functions
	//{{AFX_MSG(CReComConnectDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnReDataIncoming(LPCTSTR aData);
	afx_msg void OnReSceneLoaded(LPCTSTR aSceneName);
	afx_msg void OnReSceneActivated(LPCTSTR aSceneName);
	afx_msg void OnReSceneUnloaded(LPCTSTR aSceneName);
	afx_msg void OnReTickerDataIn(LPCTSTR aTickerName);
	afx_msg void OnReTickerDataOut(LPCTSTR aTickerName);
	afx_msg void OnReConnected();
	afx_msg void OnReDisconnected();
	afx_msg void OnReOnAir();
	afx_msg void OnReOffAir();
	afx_msg void OnReSceneAnimations(VARIANT FAR* pVal);
	afx_msg void OnReTimeout(LPCTSTR aTimerName, long aMsecSlice);
	afx_msg void OnReSceneExports(VARIANT FAR* pVal);
	afx_msg void OnReAnimationStarted(LPCTSTR anAnimName);
	afx_msg void OnReAnimationStopped(LPCTSTR anAnimName, long aFrame);
	afx_msg void OnReAnimationContinued(LPCTSTR anAnimationName);
	afx_msg void OnReAnimationRewinded(LPCTSTR anAnimationName);
	afx_msg void OnReExportUpdated(LPCTSTR anExportName, LPCTSTR anExportValue);
	afx_msg void OnReTickerStarted(LPCTSTR aTickerRunExpName);
	afx_msg void OnReTickerStopped(LPCTSTR aTickerRunExpName);
	afx_msg void OnButtonConnect();
	afx_msg void OnKillfocusEditReHostname();
	afx_msg void OnButtonSend();
	afx_msg void OnDisconnect();
	afx_msg void OnLoad();
	afx_msg void OnSendExport();
	afx_msg void OnStartAnim();
	afx_msg void OnReVersionRecom(LPCTSTR aVersion);
	afx_msg void OnReProjectsRecom(VARIANT FAR* pVal);
	afx_msg void OnReProjectScenesRecom(LPCTSTR aProjectName, VARIANT FAR* aScenes);
	afx_msg void OnSelchangeProjectCombo();
	afx_msg void OnReScenesLoadedRecom(VARIANT FAR* aScenes);
	afx_msg void OnUnload();
	afx_msg void OnUnloadall();
	afx_msg void OnActivate();
	afx_msg void OnReActiveSceneRecom(LPCTSTR aSceneName);
	afx_msg void OnStopAnim();
	afx_msg void OnButtonManual();
	virtual void OnCancel();
	virtual void OnOK();
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
  void fillComboBox(CComboBox &combo, VARIANT FAR* pVal);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RECOMCONNECTDLG_H__A68742F0_D65A_454E_9A18_81222E6DD356__INCLUDED_)
