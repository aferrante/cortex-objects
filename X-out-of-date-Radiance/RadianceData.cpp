// RadianceData.cpp: implementation of the CRadianceData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <process.h>
#include "Radiance.h"
#include "RadianceHandler.h" 
#include "RadianceMain.h" 
#include "RadianceData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


extern CRadianceMain* g_pradiance;
extern CRadianceApp theApp;
//extern CADC g_adc; 	// the Harris ADC object

extern void RadianceConnectionThread(void* pvArgs);


//////////////////////////////////////////////////////////////////////
// CRadianceDestinationObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRadianceObject::CRadianceObject()
{
	m_pszDesc		= NULL;
	m_usType		= RADIANCE_TYPE_DVG;  // default

	m_pDlg = NULL;
	m_pDlg = new CReComConnectDlg;
	if(m_pDlg)
	{
		if(!m_pDlg->Create(((CRadianceHandler*)theApp.m_pMainWnd)->m_pMainDlg)) AfxMessageBox("Could not create ReCOM object");
		m_ulStatus	= RADIANCE_STATUS_ERROR;
	}
	m_ulStatus	= RADIANCE_STATUS_UNINIT;
	m_ulFlags = RADIANCE_FLAG_DISABLED;  // various states
	m_nChannelID = -1; // unassigned
	m_bKillConnThread = true;
	m_bConnThreadStarted = false;
}

CRadianceObject::~CRadianceObject()
{
	m_bKillConnThread = true;
	while(	m_bConnThreadStarted ) Sleep(1);
	if(m_pDlg) 
	{
		m_pDlg->m_recom.disconnect();
		m_pDlg->DestroyWindow();
		delete m_pDlg;
		m_pDlg = NULL;
	}
	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
}



/*
//////////////////////////////////////////////////////////////////////
// CRadianceChannelObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRadianceChannelObject::CRadianceChannelObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_ulStatus	= RADIANCE_STATUS_UNINIT;
	m_ulFlags = RADIANCE_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
	m_nChannelID = -1; // unassigned
	m_nHarrisListID = -1; // unassigned
	m_pbKillConnThread = NULL;
	m_bKillChannelThread = true;
	m_bChannelThreadStarted = false;
	m_pAPIConn = NULL;
}

CRadianceChannelObject::~CRadianceChannelObject()
{
	m_bKillChannelThread = true;
	while(	m_bChannelThreadStarted ) Sleep(1);

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
}
*/



//////////////////////////////////////////////////////////////////////
// CRadianceData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRadianceData::CRadianceData()
{
	InitializeCriticalSection(&m_critText);
	InitializeCriticalSection(&m_critSQL);

	m_ppDestObj = NULL;
	m_nNumDestinationObjects = 0;
//	m_ppChannelObj = NULL;
//	m_nNumChannelObjects = 0;

	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebTick );
	m_pszHost = NULL;			// the name of the host
	m_pszCortexHost = NULL;			// the name of the cortex host
	m_usCortexCommandPort = RADIANCE_PORT_CMD;
	m_usCortexStatusPort = RADIANCE_PORT_STATUS;
	m_nSettingsMod = -1;
//	m_nChannelsMod = -1;
	m_nDestinationsMod = -1;
	m_nQueueMod = -1;
	m_nLastSettingsMod = -1;
//	m_nLastChannelsMod = -1;
	m_nLastDestinationsMod = -1;
	m_nLastQueueMod = -1;
	m_pdb = NULL;
	m_pdbConn = NULL;
	m_bQuietKill = false;
	m_bProcessSuspended = false;
	m_nMaxLicensedDevices = 2;
}

CRadianceData::~CRadianceData()
{
	if(m_ppDestObj)
	{
		int i=0;
		while(i<m_nNumDestinationObjects)
		{
			if(m_ppDestObj[i]) delete m_ppDestObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppDestObj; // delete array of pointers to objects, must use new to allocate
	}
/*
	if(m_ppChannelObj)
	{
		int i=0;
		while(i<m_nNumChannelObjects)
		{
			if(m_ppChannelObj[i]) delete m_ppChannelObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppChannelObj; // delete array of pointers to objects, must use new to allocate
	}
*/
	if(m_pszHost) free(m_pszHost);	// must use malloc to allocate
	if(m_pszCortexHost) free(m_pszCortexHost);	// must use malloc to allocate

	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critSQL);
}

char* CRadianceData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CRadianceData::SetStatusText(char* pszText, unsigned long ulStatus)
{
	int nRV = CX_ERROR;
	if(pszText)
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_pradiance->m_settings.m_pszIconPath)&&(strlen(g_pradiance->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_pradiance->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_pradiance->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_pradiance->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_pradiance->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_pradiance->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}

// utility
int	CRadianceData::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszHost) free(m_pszHost);
						m_pszHost=pch;
						return RADIANCE_SUCCESS;
					}
				}
			}
		}
	}
	return RADIANCE_ERROR;
}

int CRadianceData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_pradiance)&&(g_pradiance->m_settings.m_pszExchange)&&(strlen(g_pradiance->m_settings.m_pszExchange)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s set mod = \
(SELECT case when max(mod) is null then 1 when max(mod) >= %d \
then 1 else max(mod) + 1 end from %s WHERE criterion = '%s') WHERE criterion = '%s'",
			g_pradiance->m_settings.m_pszExchange,
			RADIANCE_DB_MOD_MAX,
			g_pradiance->m_settings.m_pszExchange,
			szTemp, szTemp		
			);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			return RADIANCE_SUCCESS;
		}
	}
	return RADIANCE_ERROR;
}

int CRadianceData::CheckDatabaseMods(char* pszInfo)
{
	if((g_pradiance)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_pradiance->m_settings.m_pszExchange)&&(strlen(g_pradiance->m_settings.m_pszExchange)))?g_pradiance->m_settings.m_pszExchange:"Exchange");

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = RADIANCE_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCriterion;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
				}

				if((g_pradiance->m_settings.m_pszSettings)&&(strlen(g_pradiance->m_settings.m_pszSettings)))
				{
					szTemp.Format("DBT_%s",g_pradiance->m_settings.m_pszSettings);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nSettingsMod = nReturn;
					}
				}

/*
				if((g_pradiance->m_settings.m_pszChannels)&&(strlen(g_pradiance->m_settings.m_pszChannels)))
				{
					szTemp.Format("DBT_%s",g_pradiance->m_settings.m_pszChannels);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nChannelsMod = nReturn;
					}
				}
*/
				if((g_pradiance->m_settings.m_pszDestinations)&&(strlen(g_pradiance->m_settings.m_pszDestinations)))
				{
					szTemp.Format("DBT_%s",g_pradiance->m_settings.m_pszDestinations);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nDestinationsMod = nReturn;
					}
				}

				if((g_pradiance->m_settings.m_pszQueue)&&(strlen(g_pradiance->m_settings.m_pszQueue)))
				{
					szTemp.Format("DBT_%s",g_pradiance->m_settings.m_pszQueue);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nQueueMod = nReturn;
					}
				}
				// get the suspend

				szTemp.Format("Suspend");
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					nReturn = atoi(szMod);
					if(nReturn>0) m_bProcessSuspended = true;
					else m_bProcessSuspended = false;
				}

				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			delete prs;
			return nReturn;
		}
	}
	return RADIANCE_ERROR;
}


int CRadianceData::GetQueue(char* pszInfo)
{
	// following removed to deal with wait mode below.
/*
	if(g_miranda.m_bTransferring)
	{
		// something in progress, must wait.
		return RADIANCE_ERROR;
	}
*/
	
	if((g_pradiance)&&(m_pdbConn)&&(m_pdb))
	{

		// let's just get the top (oldest timestamp) item in the queue, and deal with it.

		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s ORDER BY timestamp ASC", //HARDCODE
			((g_pradiance->m_settings.m_pszQueue)&&(strlen(g_pradiance->m_settings.m_pszQueue)))?g_pradiance->m_settings.m_pszQueue:"Queue");

//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
//action int, host varchar(64), timestamp float, username varchar(32));

		CString szFilenameLocal = "";
		CString szFilenameRemote = "";
		CString szServer = "";
		CString szUsername = "";
		CString szMessage = "";
		CString szTemp = "";
		int nItemID = -1;
		bool bItemFound = false;
		double dblTimestamp=-57;   // timestamp
		int nAction = -1;
		int nTemp;
		int nEventItemID=-1;

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = RADIANCE_ERROR;
			int nIndex = 0;
//			while ((!prs->IsEOF()))
			if ((!prs->IsEOF()))  // just do the one record, if there is one
			{
				try
				{
					prs->GetFieldValue("itemid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nItemID = atoi(szTemp);
						bItemFound = true;
					}
					prs->GetFieldValue("filename_local", szFilenameLocal);//HARDCODE
					szFilenameLocal.TrimLeft(); szFilenameLocal.TrimRight();
					prs->GetFieldValue("filename_remote", szFilenameRemote);//HARDCODE
					szFilenameRemote.TrimLeft(); szFilenameRemote.TrimRight();
					prs->GetFieldValue("action", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nAction = nTemp;
					}
					prs->GetFieldValue("host", szServer);//HARDCODE
					szServer.TrimLeft(); szServer.TrimRight();

					prs->GetFieldValue("timestamp", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						dblTimestamp = atof(szTemp);
					}
					prs->GetFieldValue("username", szUsername);//HARDCODE
					szUsername.TrimLeft(); szUsername.TrimRight();
					prs->GetFieldValue("event_itemid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nEventItemID = nTemp;
					}

					prs->GetFieldValue("message", szMessage);//HARDCODE
					
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;

//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "got %d things from queue", nIndex);   Sleep(250);//(Dispatch message)

			if((nReturn>0)&&(bItemFound))
			{
				// process the request, then remove it from the queue
//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "got item %d from queue", nItemID);   Sleep(250);//(Dispatch message)

				if(szServer.GetLength()>0)
				{
					char* pchServer = szServer.GetBuffer(1);
					int nDestIndex = ReturnDestinationIndex(pchServer);
//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "got server %s from queue", pchServer);   Sleep(250);//(Dispatch message)

					if((nDestIndex>=0)&&(m_ppDestObj)&&(m_ppDestObj[nDestIndex])&&(m_ppDestObj[nDestIndex]->m_ulFlags&RADIANCE_FLAG_ENABLED))
					{  
						// only do anything if its enabled.
//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "enabled");   Sleep(250);//(Dispatch message)


					}
				}

				// send remove SQL
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE itemid = %d",  //HARDCODE
					((g_pradiance->m_settings.m_pszQueue)&&(strlen(g_pradiance->m_settings.m_pszQueue)))?g_pradiance->m_settings.m_pszQueue:"Queue",
					nItemID
					);


				if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
				{
					//**MSG
//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

				}
			}

			return nReturn;
		}
	}
	return RADIANCE_ERROR;
}

int CRadianceData::ReturnDestinationIndex(char* pszServerName)
{
	if((pszServerName)&&(strlen(pszServerName))&&(g_pradiance)&&(g_pradiance->m_data.m_ppDestObj)&&(g_pradiance->m_data.m_nNumDestinationObjects))
	{
		int i=0;
		while(i<g_pradiance->m_data.m_nNumDestinationObjects)
		{
			if((g_pradiance->m_data.m_ppDestObj[i])&&(g_pradiance->m_data.m_ppDestObj[i]->m_pDlg))
			{
				if(g_pradiance->m_data.m_ppDestObj[i]->m_pDlg->m_reName.CompareNoCase(pszServerName)==0) return i;
			}
			i++;
		}
	}
	return RADIANCE_ERROR;
}

int CRadianceData::GetDestinations(char* pszInfo)
{
	if((g_pradiance)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY channelid", 
			((g_pradiance->m_settings.m_pszDestinations)&&(strlen(g_pradiance->m_settings.m_pszDestinations)))?g_pradiance->m_settings.m_pszDestinations:"Destinations");

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = RADIANCE_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				_ftime( &g_pradiance->m_data.m_timebTick );
				CString szHost="";
				CString szDesc="";
				CString szTemp="";
				unsigned long ulFlags;   // various flags
				unsigned short usType;
				int nChannelID = -1;
				int nTemp;
				bool bFlagsFound = false;
				bool bTypeFound = false;
				bool bFound = false;
				try
				{
					prs->GetFieldValue("host", szHost);//HARDCODE
					szHost.TrimLeft(); szHost.TrimRight();
					prs->GetFieldValue("type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0)
						{
							bTypeFound = true;
							usType = (unsigned short)nTemp;
						}
					}
					prs->GetFieldValue("description", szDesc);//HARDCODE
					prs->GetFieldValue("channelid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nChannelID = nTemp;
					}
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bFlagsFound = true;
						ulFlags = atol(szTemp);
					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				if((g_pradiance->m_data.m_ppDestObj)&&(g_pradiance->m_data.m_nNumDestinationObjects))
				{
					nTemp=0;
					while(nTemp<g_pradiance->m_data.m_nNumDestinationObjects)
					{
						_ftime( &g_pradiance->m_data.m_timebTick );
						if((g_pradiance->m_data.m_ppDestObj[nTemp])&&(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pDlg))
						{
							if(
									(szHost.GetLength()>0)
								&&(szHost.CompareNoCase(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pDlg->m_reName)==0)
								)
							{
								bFound = true;
								bool bRefresh = false;

								if((szDesc.GetLength()>0)&&(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc)&&(szDesc.Compare(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc)))
								{
									free(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc);
									g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc = (char*)malloc(strlen(szDesc)+1); 
									if(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc) sprintf(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc, szDesc);
								}
								if(bTypeFound) g_pradiance->m_data.m_ppDestObj[nTemp]->m_usType = usType;

								if((bFlagsFound)&&(!(ulFlags&RADIANCE_FLAG_ENABLED))&&((g_pradiance->m_data.m_ppDestObj[nTemp]->m_ulFlags)&RADIANCE_FLAG_ENABLED))
								{
									g_pradiance->m_data.m_ppDestObj[nTemp]->m_bKillConnThread = true;

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", 
										g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc?g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc:g_pradiance->m_data.m_ppDestObj[nTemp]->m_pDlg->m_reName);  
									g_pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance:destination_change", errorstring);    //(Dispatch message)
									g_pradiance->m_data.m_ppDestObj[nTemp]->m_pDlg->OnDisconnect();
									g_pradiance->m_data.m_ppDestObj[nTemp]->m_ulStatus = RADIANCE_STATUS_NOTCON;
									while(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pDlg->m_bConnected)
									{
										_ftime( &g_pradiance->m_data.m_timebTick );
										Sleep(50);
									}
									while(g_pradiance->m_data.m_ppDestObj[nTemp]->m_bConnThreadStarted)
									{
										_ftime( &g_pradiance->m_data.m_timebTick );
										Sleep(50);
									}

								}
								if((bFlagsFound)&&(ulFlags&RADIANCE_FLAG_ENABLED)&&(!((g_pradiance->m_data.m_ppDestObj[nTemp]->m_ulFlags)&RADIANCE_FLAG_ENABLED)))
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s...", 
										g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc?g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc:g_pradiance->m_data.m_ppDestObj[nTemp]->m_pDlg->m_reName);  
									g_pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance:destination_change", errorstring);    //(Dispatch message)
									g_pradiance->m_data.m_ppDestObj[nTemp]->m_pDlg->OnButtonConnect();

									bRefresh = true;
									g_pradiance->m_data.m_ppDestObj[nTemp]->m_bKillConnThread = false;
//									if(_beginthread(RadianceConnectionThread, 0, (void*)g_pradiance->m_data.m_ppDestObj[nTemp])<0)
if(0)									{
										//error.

										//**MSG
							
										
									}
									//**** should check return value....

									else
									{
										g_pradiance->m_data.m_ppDestObj[nTemp]->m_ulStatus = RADIANCE_STATUS_CONN;
									}

								}
								


								CString szTitle;
								szTitle.Format("%s", ((g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc)&&(strlen(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc)))?g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc:g_pradiance->m_data.m_ppDestObj[nTemp]->m_pDlg->m_reName);
								g_pradiance->m_data.m_ppDestObj[nTemp]->m_pDlg->SetWindowText(szTitle);

								if(bFlagsFound) 
								{
									g_pradiance->m_data.m_ppDestObj[nTemp]->m_ulFlags = ulFlags|RADIANCE_FLAG_FOUND;
								}
								else
								{
									g_pradiance->m_data.m_ppDestObj[nTemp]->m_ulFlags |= RADIANCE_FLAG_FOUND;
								}

							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szHost.GetLength()>0)) // have to add.
				{

					CRadianceObject* pro = new CRadianceObject;
					if(pro)
					{
						CRadianceObject** ppObj = new CRadianceObject*[g_pradiance->m_data.m_nNumDestinationObjects+1];
						if(ppObj)
						{
							int o=0;
							if((g_pradiance->m_data.m_ppDestObj)&&(g_pradiance->m_data.m_nNumDestinationObjects>0))
							{
								while(o<g_pradiance->m_data.m_nNumDestinationObjects)
								{
									ppObj[o] = g_pradiance->m_data.m_ppDestObj[o];
									o++;
								}
								delete [] g_pradiance->m_data.m_ppDestObj;

							}
							ppObj[g_pradiance->m_data.m_nNumDestinationObjects] = pro;
							g_pradiance->m_data.m_ppDestObj = ppObj;
							g_pradiance->m_data.m_nNumDestinationObjects++;

							if(ppObj[o]->m_pDlg)
							{
								ppObj[o]->m_pDlg->m_reName = szHost; 
								ppObj[o]->m_pDlg->UpdateData(FALSE); // set the host
								if(szDesc.GetLength()>0)
								{
									ppObj[o]->m_pszDesc = (char*)malloc(strlen(szDesc)+1); 
									if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, szDesc);
								}
								if(bTypeFound) ppObj[o]->m_usType = usType;
								if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|RADIANCE_FLAG_FOUND;
								else ppObj[o]->m_ulFlags |= RADIANCE_FLAG_FOUND;
								


								CString szTitle;
								szTitle.Format("%s", ((ppObj[o]->m_pszDesc)&&(strlen(ppObj[o]->m_pszDesc)))?ppObj[o]->m_pszDesc:ppObj[o]->m_pDlg->m_reName);
								ppObj[o]->m_pDlg->SetWindowText(szTitle);

								ppObj[o]->m_ulStatus = RADIANCE_STATUS_OK;
								if(ulFlags&RADIANCE_FLAG_ENABLED)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Creating %s...", 
										g_pradiance->m_data.m_ppDestObj[o]->m_pszDesc?g_pradiance->m_data.m_ppDestObj[o]->m_pszDesc:g_pradiance->m_data.m_ppDestObj[o]->m_pDlg->m_reName);  
									g_pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance:destination_init", errorstring);    //(Dispatch message)
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s...", 
										g_pradiance->m_data.m_ppDestObj[o]->m_pszDesc?g_pradiance->m_data.m_ppDestObj[o]->m_pszDesc:g_pradiance->m_data.m_ppDestObj[o]->m_pDlg->m_reName);  
									ppObj[o]->m_pDlg->OnButtonConnect();

									ppObj[o]->m_bKillConnThread = false;
//									if(_beginthread(RadianceConnectionThread, 0, (void*)ppObj[o])<0)
if(0)									{
										//error.

										//**MSG
							
										
									}
									//**** should check return value....

									else
									{
										ppObj[o]->m_ulStatus = RADIANCE_STATUS_CONN;
									}

								}
								else
								{
									// don't start it.
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Creating %s...", 
										g_pradiance->m_data.m_ppDestObj[o]->m_pszDesc?g_pradiance->m_data.m_ppDestObj[o]->m_pszDesc:g_pradiance->m_data.m_ppDestObj[o]->m_pDlg->m_reName);  
								}
								g_pradiance->m_data.SetStatusText(errorstring, g_pradiance->m_data.m_ulFlags);
								g_pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance:destination_init", errorstring);    //(Dispatch message)
							}

						}
						else
							delete pro;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<g_pradiance->m_data.m_nNumDestinationObjects)
			{
				if((g_pradiance->m_data.m_ppDestObj)&&(g_pradiance->m_data.m_ppDestObj[nIndex]))
				{
					if((g_pradiance->m_data.m_ppDestObj[nIndex]->m_ulFlags)&RADIANCE_FLAG_FOUND)
					{
						(g_pradiance->m_data.m_ppDestObj[nIndex]->m_ulFlags) &= ~RADIANCE_FLAG_FOUND;
					}
					else
					{
						if((g_pradiance->m_data.m_ppDestObj[nIndex]->m_ulFlags)&RADIANCE_FLAG_ENABLED)
						{
							if(g_pradiance->m_data.m_ppDestObj[nIndex]->m_pDlg)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", 
									g_pradiance->m_data.m_ppDestObj[nIndex]->m_pszDesc?g_pradiance->m_data.m_ppDestObj[nIndex]->m_pszDesc:g_pradiance->m_data.m_ppDestObj[nIndex]->m_pDlg->m_reName);  
								g_pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance:destination_remove", errorstring);    //(Dispatch message)
								g_pradiance->m_data.m_ppDestObj[nIndex]->m_pDlg->OnDisconnect();
								g_pradiance->m_data.m_ppDestObj[nIndex]->m_ulStatus = RADIANCE_STATUS_NOTCON;
							}
						}
						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Removing %s...", 
							g_pradiance->m_data.m_ppDestObj[nIndex]->m_pszDesc?g_pradiance->m_data.m_ppDestObj[nIndex]->m_pszDesc:g_pradiance->m_data.m_ppDestObj[nIndex]->m_pDlg->m_reName);  
						g_pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance:destination_remove", errorstring);    //(Dispatch message)
						while(g_pradiance->m_data.m_ppDestObj[nIndex]->m_pDlg->m_bConnected)
						{
							_ftime( &g_pradiance->m_data.m_timebTick );
							Sleep(50);
						}

						g_pradiance->m_data.m_ppDestObj[nIndex]->m_bKillConnThread = true;
						while(g_pradiance->m_data.m_ppDestObj[nIndex]->m_bConnThreadStarted)
						{
							_ftime( &g_pradiance->m_data.m_timebTick );
							Sleep(50);
						}

//						g_pradiance->m_data.m_ppDestObj[nIndex]->m_pDlg->OnCancel();
//						g_pradiance->m_data.m_ppDestObj[nIndex]->m_pDlg = NULL;

						delete g_pradiance->m_data.m_ppDestObj[nIndex];
						g_pradiance->m_data.m_nNumDestinationObjects--;

						int nTemp=nIndex;
						while(nTemp<g_pradiance->m_data.m_nNumDestinationObjects)
						{
							g_pradiance->m_data.m_ppDestObj[nTemp]=g_pradiance->m_data.m_ppDestObj[nTemp+1];
							nTemp++;
						}
						g_pradiance->m_data.m_ppDestObj[nTemp] = NULL;
					}
				}

				nIndex++;
			}

			return nReturn;
		}
	}
	return RADIANCE_ERROR;
}



/*
int CRadianceData::GetDestinations(char* pszInfo)
{
	if((g_pradiance)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d * FROM %s ORDER BY channelid",  //HARDCODE
			g_pradiance->m_data.m_nMaxLicensedDevices,
			((g_pradiance->m_settings.m_pszDestinations)&&(strlen(g_pradiance->m_settings.m_pszDestinations)))?g_pradiance->m_settings.m_pszDestinations:"Destinations");
	
//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "GetDestinations");  Sleep(250); //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "Got Destinations");  Sleep(250); //(Dispatch message)

			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = RADIANCE_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{

// create table Destinations (destinationid int identity(1,1) NOT NULL, host varchar(64) NOT NULL,
//				type int, description varchar(256), diskspace_free float, diskspace_total float, diskspace_threshold real,
//				channelid int, flags int);


				CString szHost="";
//				CString szClient="";
				CString szDesc="";
				CString szTemp;
//				double dblDiskspaceFree=-1.0;
//				double dblDiskspaceTotal=-1.0;
				double dblDiskspaceThreshold=-1.0;
				unsigned long ulFlags;   // various flags
				unsigned short usType;
				int nChannelid=-1;
				int nTemp = -1;
				int nItemID = -1;
				bool bItemFound = false;
				bool bFlagsFound = false;
				bool bFound = false;
				try
				{
					prs->GetFieldValue("destinationid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bItemFound = true;
						nItemID = atoi(szTemp);
					}
					prs->GetFieldValue("host", szHost);//HARDCODE
					szHost.TrimLeft(); szHost.TrimRight();
					prs->GetFieldValue("type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						usType = (unsigned short)atol(szTemp);
					}
					prs->GetFieldValue("description", szDesc);//HARDCODE
					prs->GetFieldValue("diskspace_threshold", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						dblDiskspaceThreshold = atof(szTemp);
					}
					prs->GetFieldValue("channelid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nChannelid = atoi(szTemp);
					}
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bFlagsFound = true;
						ulFlags = atol(szTemp);
					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
* /
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}
//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "Got Destination: %s (0x%08x)", szHost, ulFlags);  Sleep(250); //(Dispatch message)

				if((g_pradiance->m_data.m_ppDestObj)&&(g_pradiance->m_data.m_nNumDestinationObjects))
				{
					nTemp=0;
					while(nTemp<g_pradiance->m_data.m_nNumDestinationObjects)
					{
//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "c2");  Sleep(250); //(Dispatch message)
						if((g_pradiance->m_data.m_ppDestObj[nTemp])&&(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszServerName))
						{
							if((szHost.GetLength()>0)&&(szHost.CompareNoCase(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszServerName)==0))
							{
								bFound = true;
								bool bRefresh = false;

								if(
										(szDesc.GetLength()>0)
									&&((g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc==NULL)||(szDesc.CompareNoCase(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc)))
									)
								{
									if(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc) free(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc);

									g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc) sprintf(g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc, szDesc);
								}

								if(bItemFound) g_pradiance->m_data.m_ppDestObj[nTemp]->m_nItemID = nItemID;
								g_pradiance->m_data.m_ppDestObj[nTemp]->m_usType = usType;

								if(dblDiskspaceThreshold>0.0) g_pradiance->m_data.m_ppDestObj[nTemp]->m_dblDiskPercent = dblDiskspaceThreshold;
//								g_pradiance->m_data.m_ppDestObj[nTemp]->m_dblDiskKBFree = dblDiskspaceFree;
//								g_pradiance->m_data.m_ppDestObj[nTemp]->m_dblDiskKBTotal = dblDiskspaceTotal;

//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "c3");  Sleep(250); //(Dispatch message)

								if(
										((bFlagsFound)&&(!(ulFlags&RADIANCE_FLAG_ENABLED))&&((g_pradiance->m_data.m_ppDestObj[nTemp]->m_ulFlags)&RADIANCE_FLAG_ENABLED))
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
										g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc?g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc:g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszServerName);  
									g_pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance:destination_change", errorstring);  Sleep(20);  //(Dispatch message)
									g_pradiance->m_data.m_ppDestObj[nTemp]->m_bKillConnThread = true;
									g_pradiance->m_data.m_ppDestObj[nTemp]->m_ulStatus = RADIANCE_STATUS_NOTCON;
								}

								if(
										((bFlagsFound)&&(ulFlags&RADIANCE_FLAG_ENABLED)&&(!((g_pradiance->m_data.m_ppDestObj[nTemp]->m_ulFlags)&RADIANCE_FLAG_ENABLED)))
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating %s...", 
//										g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszClientName?g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszClientName:"Radiance",  
										g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc?g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszDesc:g_pradiance->m_data.m_ppDestObj[nTemp]->m_pszServerName);  
									g_pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance:destination_change", errorstring);   Sleep(20);  //(Dispatch message)
//									g_pradiance->m_data.m_ppDestObj[nTemp]->m_pDlg->OnButtonConnect();
									//**** if connect set following status


									bRefresh = true;
//									g_pradiance->m_data.m_ppDestObj[nTemp]->m_bKillConnThread = false;
	//								if(_beginthread(RadianceConnectionThread, 0, (void*)g_pradiance->m_data.m_ppDestObj[nTemp])<0)
	//								{
										//error.

										//**MSG
							
										
		//							}
									//**** should check return value....

									g_pradiance->m_data.m_ppDestObj[nTemp]->m_ulStatus = RADIANCE_STATUS_CONN;
								}

								if(bFlagsFound) g_pradiance->m_data.m_ppDestObj[nTemp]->m_ulFlags = ulFlags|RADIANCE_FLAG_FOUND;
								else g_pradiance->m_data.m_ppDestObj[nTemp]->m_ulFlags |= RADIANCE_FLAG_FOUND;

								if(bRefresh)g_pradiance->m_data.m_ppDestObj[nTemp]->m_ulFlags |= RADIANCE_FLAG_REFRESH;

							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szHost.GetLength()>0)) // have to add.
				{
//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "adding %s", szHost);  Sleep(250); //(Dispatch message)
					CRadianceDestinationObject* pscno = new CRadianceDestinationObject;
					if(pscno)
					{
//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CRadianceDestinationObject** ppObj = new CRadianceDestinationObject*[g_pradiance->m_data.m_nNumDestinationObjects+1];
						if(ppObj)
						{
//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((g_pradiance->m_data.m_ppDestObj)&&(g_pradiance->m_data.m_nNumDestinationObjects>0))
							{
								while(o<g_pradiance->m_data.m_nNumDestinationObjects)
								{
									ppObj[o] = g_pradiance->m_data.m_ppDestObj[o];
									o++;
								}
								delete [] g_pradiance->m_data.m_ppDestObj;

							}
							ppObj[o] = pscno;
							g_pradiance->m_data.m_ppDestObj = ppObj;
							g_pradiance->m_data.m_nNumDestinationObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szHost.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, "%s", szHost);

/*							
							if(szClient.GetLength()<=0)
							{
								szClient = "Radiance";
							}

							if(szClient.GetLength()>0)
							{
								ppObj[o]->m_pszClientName = (char*)malloc(szClient.GetLength()+1); 
								if(ppObj[o]->m_pszClientName) sprintf(ppObj[o]->m_pszClientName, "%s", szClient);
							}
* /
							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}
							if(bItemFound) ppObj[o]->m_nItemID = nItemID;
							ppObj[o]->m_usType = usType;

							if(dblDiskspaceThreshold>0.0) ppObj[o]->m_dblDiskPercent = dblDiskspaceThreshold;
//							ppObj[o]->m_dblDiskKBFree = dblDiskspaceFree;
//							ppObj[o]->m_dblDiskKBTotal = dblDiskspaceTotal;

//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "adding flags: %s (0x%08x)", szHost, ulFlags);  Sleep(250); //(Dispatch message)

							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|RADIANCE_FLAG_FOUND|RADIANCE_FLAG_NEW;
							else ppObj[o]->m_ulFlags |= RADIANCE_FLAG_FOUND|RADIANCE_FLAG_NEW;
								
//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", " added flags: %s (0x%08x) %d", szHost, ppObj[o]->m_ulFlags, ((ppObj[o]->m_ulFlags)&RADIANCE_FLAG_ENABLED));  Sleep(250); //(Dispatch message)

							if((ppObj[o]->m_ulFlags)&RADIANCE_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating %s...", 
										ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName);  
								g_pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance:destination_change", errorstring);   Sleep(20);  //(Dispatch message)

								//**** if connect 

								// create a connection object in g_adc.
								// associate that with the new CRadianceConnectionObject;
								// start the thread.
//								ppObj[o]->m_bKillConnThread = false;

								ppObj[o]->m_ulStatus = RADIANCE_STATUS_CONN;
							}

						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			// and insert media list refresh into the queue for all new destinatons.
			nIndex = 0;
			while(nIndex<g_pradiance->m_data.m_nNumDestinationObjects)
			{
				if((g_pradiance->m_data.m_ppDestObj)&&(g_pradiance->m_data.m_ppDestObj[nIndex]))
				{

					if((g_pradiance->m_data.m_ppDestObj[nIndex]->m_ulFlags)&RADIANCE_FLAG_FOUND)
					{
						(g_pradiance->m_data.m_ppDestObj[nIndex]->m_ulFlags) &= ~RADIANCE_FLAG_FOUND;

						if((g_pradiance->m_data.m_ppDestObj[nIndex]->m_ulFlags)&(RADIANCE_FLAG_NEW|RADIANCE_FLAG_REFRESH))
						{
							(g_pradiance->m_data.m_ppDestObj[nIndex]->m_ulFlags) &= ~(RADIANCE_FLAG_REFRESH|RADIANCE_FLAG_NEW);
							// refresh media.
// insert an immediate destinations media refresh into the queue.
/*
Queue action IDs
1 transfer to Miranda
2 transfer from Miranda
3 delete from Miranda
4 refresh device listing
19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)

Queue table:
//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
//action int, host varchar(64), timestamp float, username varchar(32));
* /

							if((g_pradiance->m_data.m_ppDestObj[nIndex]->m_pszServerName)&&(strlen(g_pradiance->m_data.m_ppDestObj[nIndex]->m_pszServerName)) && ((g_pradiance->m_data.m_ppDestObj[nIndex]->m_ulFlags)&RADIANCE_FLAG_ENABLED)  )
							{
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (filename_local, filename_remote, action, host, timestamp, username) \
VALUES ('','', 4, '%s', 0, 'sys' )", //HARDCODE
								((g_pradiance->m_settings.m_pszQueue)&&(strlen(g_pradiance->m_settings.m_pszQueue)))?g_pradiance->m_settings.m_pszQueue:"Queue",
								g_pradiance->m_data.m_ppDestObj[nIndex]->m_pszServerName
								);


//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", szSQL);  Sleep(250); //(Dispatch message)
//					char errorstring[DB_ERRORSTRING_LEN];
//								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
								{
									//**MSG
//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

								}

// deal with the diskspace info 
							// actually, do this on any transaction from queue.

							}


						}
					
						nIndex++;
					}
					else
					{
						if(g_pradiance->m_data.m_ppDestObj[nIndex])
						{
							if((g_pradiance->m_data.m_ppDestObj[nIndex]->m_ulFlags)&RADIANCE_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
									g_pradiance->m_data.m_ppDestObj[nIndex]->m_pszDesc?g_pradiance->m_data.m_ppDestObj[nIndex]->m_pszDesc:g_pradiance->m_data.m_ppDestObj[nIndex]->m_pszServerName);  
								g_pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance:destination_remove", errorstring);   Sleep(20);  //(Dispatch message)
//									g_pradiance->m_data.m_ppDestObj[nTemp]->m_pDlg->OnDisconnect();

								//**** disconnect

								g_pradiance->m_data.m_ppDestObj[nIndex]->m_bKillConnThread = false;
//								while(g_pradiance->m_data.m_ppDestObj[nIndex]->m_bConnThreadStarted) Sleep(1);

								//g_adc.DisconnectServer(g_pradiance->m_data.m_ppDestObj[nIndex]->m_pszServerName);
								//**** should check return value....

								g_pradiance->m_data.m_ppDestObj[nIndex]->m_ulStatus = RADIANCE_STATUS_NOTCON;
							}

							delete g_pradiance->m_data.m_ppDestObj[nIndex];
							g_pradiance->m_data.m_nNumDestinationObjects--;

							int nTemp=nIndex;
							while(nTemp<g_pradiance->m_data.m_nNumDestinationObjects)
							{
								g_pradiance->m_data.m_ppDestObj[nTemp]=g_pradiance->m_data.m_ppDestObj[nTemp+1];
								nTemp++;
							}
							g_pradiance->m_data.m_ppDestObj[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
	}
//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return RADIANCE_ERROR;
}

*/


