// RadianceMain.cpp: implementation of the CRadianceMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "Radiance.h"  // just included to have access to windowing environment
#include "RadianceDlg.h"  // just included to have access to windowing environment
#include "RadianceHandler.h"  // just included to have access to windowing environment

#include "RadianceMain.h"
#include <process.h>
#include "../../Common/TXT/BufferUtil.h"
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"
//#include "../../Common/API/Miranda/IS2Comm.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;
CRadianceMain* g_pradiance=NULL;
//CADC g_adc;
//CIS2Comm g_miranda;  // global miranda object

extern CMessager* g_pmsgr;  // from Messager.cpp
extern CRadianceApp theApp;

void RadianceConnectionThread(void* pvArgs);

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRadianceMain::CRadianceMain()
{
}

CRadianceMain::~CRadianceMain()
{
}

/*

char*	CRadianceMain::RadianceTranslate(CHTTPHeader* pHeader, char* pszBuffer)			// apply radiance scripting language
{
	return pszBuffer;
}

int		CRadianceMain::InterpretDirective(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the directive, such as a char buffer.
	return RADIANCE_SUCCESS;
}
*/

SOCKET*	CRadianceMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // radiance initiates a request to an object server
{
	CNetData* pdata = new CNetData;

	pdata->m_ucType = ucType;      // defined type - indicates which protocol to use, structure of data

	pdata->m_ucCmd = ucCmd;       // the command byte
	pdata->m_ucSubCmd = ucSubCmd;       // the subcommand byte

	pdata->m_pucData = pucData;
	pdata->m_ulDataLen = ulDataLen;

	SOCKET* ps = NULL; 
	m_net.SendData(pdata, pchHost, usPort, 5000, 0, NET_SND_CMDTOSVR, ps);
//	m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);

	return ps;
}

int		CRadianceMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// radiance replies to an object server after receiving data. (usually ack or nak)
{
	return RADIANCE_SUCCESS;
}

int		CRadianceMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// radiance answers a request from an object client
{
	return RADIANCE_SUCCESS;
}


void RadianceMainThread(void* pvArgs)
{
	CRadianceApp* pApp = (CRadianceApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment
	//startup.
	g_bThreadStarted = true;

	//create the main objects.

	CRadianceMain radiance;
	CDBUtil db;

	radiance.m_data.m_ulFlags &= ~RADIANCE_STATUS_THREAD_MASK;
	radiance.m_data.m_ulFlags |= RADIANCE_STATUS_THREAD_START;
	radiance.m_data.m_ulFlags &= ~RADIANCE_ICON_MASK;
	radiance.m_data.m_ulFlags |= RADIANCE_STATUS_UNINIT;

	radiance.m_data.GetHost();

	g_pradiance = &radiance;


	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
//	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\Radiance\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}



//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

//		AfxMessageBox( pApp->m_lpCmdLine );

	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// check here for Cortex IP.
			if(strlen(pch))
			{
//		AfxMessageBox( pch );
				if(radiance.m_data.m_bu.CountChar(pch, strlen(pch), ':')>=1)  // must be of form "hostname:cmdport" where hostname can be name or ip
				{
					radiance.m_data.m_pszCortexHost = (char*)malloc(strlen(pch)+1);
					if(radiance.m_data.m_pszCortexHost)
					{
						strcpy(radiance.m_data.m_pszCortexHost, pch);
//		AfxMessageBox( radiance.m_data.m_pszCortexHost );

						char* pchd = strchr(radiance.m_data.m_pszCortexHost, ':');
						if(pchd)
						{
							*pchd = 0;  // null term the host name;
							pchd++;
//		AfxMessageBox( radiance.m_data.m_pszCortexHost );

							char* pchd2 = strchr(pchd, ':');
							if(pchd2)
							{
								*pchd2 = 0;  // null term the cmd port;
								pchd2++;
								if(strlen(pchd2)) radiance.m_data.m_usCortexStatusPort = atoi(pchd2);
							}

							if(strlen(pchd)) radiance.m_data.m_usCortexCommandPort = atoi(pchd);
						}
					}

				}
			}
			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }

	// get settings.
	char pszFilename[MAX_PATH];

	strcpy(pszFilename, RADIANCE_SETTINGS_FILE_DEFAULT);  // radiance settings file

	CFileUtil file;
	file.GetSettings(pszFilename, false); 
	char* pszParams = NULL;
// load up the values on the settings object
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		radiance.m_settings.m_pszName = file.GetIniString("Main", "Name", "Radiance");
		radiance.m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");

		// compile license key params
		if(g_pradiance->m_data.m_key.m_pszLicenseString) free(g_pradiance->m_data.m_key.m_pszLicenseString);
		g_pradiance->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(radiance.m_settings.m_pszLicense)+1);
		if(g_pradiance->m_data.m_key.m_pszLicenseString)
			sprintf(g_pradiance->m_data.m_key.m_pszLicenseString, "%s", radiance.m_settings.m_pszLicense);

		g_pradiance->m_data.m_key.InterpretKey();

		if(g_pradiance->m_data.m_key.m_bValid)
		{
			unsigned long i=0;
			while(i<g_pradiance->m_data.m_key.m_ulNumParams)
			{
				if((g_pradiance->m_data.m_key.m_ppszParams)
					&&(g_pradiance->m_data.m_key.m_ppszValues)
					&&(g_pradiance->m_data.m_key.m_ppszParams[i])
					&&(g_pradiance->m_data.m_key.m_ppszValues[i]))
				{
					if(stricmp(g_pradiance->m_data.m_key.m_ppszParams[i], "max")==0)
					{
						g_pradiance->m_data.m_nMaxLicensedDevices = atoi(g_pradiance->m_data.m_key.m_ppszValues[i]);
					}
				}
				i++;
			}

			if(
					(
				    (!radiance.m_data.m_key.m_bExpires)
					||((radiance.m_data.m_key.m_bExpires)&&(!radiance.m_data.m_key.m_bExpired))
					||((radiance.m_data.m_key.m_bExpires)&&(radiance.m_data.m_key.m_bExpireForgiveness)&&(radiance.m_data.m_key.m_ulExpiryDate+radiance.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
				    (!radiance.m_data.m_key.m_bMachineSpecific)
					||((radiance.m_data.m_key.m_bMachineSpecific)&&(radiance.m_data.m_key.m_bValidMAC))
					)
				)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
				radiance.m_data.m_ulFlags &= ~RADIANCE_ICON_MASK;
				radiance.m_data.m_ulFlags |= RADIANCE_STATUS_OK;
				radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				radiance.m_data.m_ulFlags &= ~RADIANCE_ICON_MASK;
				radiance.m_data.m_ulFlags |= RADIANCE_STATUS_ERROR;
				radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
			radiance.m_data.m_ulFlags &= ~RADIANCE_ICON_MASK;
			radiance.m_data.m_ulFlags |= RADIANCE_STATUS_ERROR;
			radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
		}

		radiance.m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", RADIANCE_PORT_CMD);
		radiance.m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", RADIANCE_PORT_STATUS);

		radiance.m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");

		radiance.m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
		radiance.m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
		radiance.m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;

		radiance.m_settings.m_pszDSN = file.GetIniString("Database", "DSN", radiance.m_settings.m_pszName?radiance.m_settings.m_pszName:"Radiance");
		radiance.m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
		radiance.m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
		radiance.m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
		radiance.m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
		radiance.m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name

		radiance.m_settings.m_pszDestinationMedia = file.GetIniString("Database", "DestinationMediaTableName", "Destinations_Media");  // the Destinations table name
		radiance.m_settings.m_pszDestinations = file.GetIniString("Database", "DestinationsTableName", "Destinations");  // the Destinations table name
		radiance.m_settings.m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue");  // the Queue table name
		radiance.m_settings.m_pszDestinationMediaTemp = file.GetIniString("Database", "DestinationMediaTempTableName", "Destinations_Media_Temp");  // the Destinations table name
		radiance.m_settings.m_pszCommandQueue = file.GetIniString("Database", "CommandQueueTableName", "Command_Queue");  // the Command Queue table name


		radiance.m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds

		if(pszParams) free(pszParams); pszParams=NULL;
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
	}

	bool bUseLog = false;
	if(radiance.m_settings.m_bUseLog)
	{
		bUseLog = radiance.m_settings.m_bUseLog;

		// for logfiles, we need params, and they must be in this format:
		//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
		pszParams = file.GetIniString("Messager", "LogFileIni", "Radiance|YD||1|");
		int nRegisterCode=0;

		nRegisterCode = radiance.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, "log", pszParams, errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
			radiance.m_data.m_ulFlags |= (RADIANCE_STATUS_FAIL_LOG|RADIANCE_STATUS_ERROR);
			radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
		}

		if(pszParams) free(pszParams); pszParams=NULL;
	}

	radiance.m_msgr.DM(MSG_ICONINFO, NULL, "Radiance", "--------------------------------------------------\n\
-------------- Radiance %s start --------------", RADIANCE_CURRENT_VERSION);  //(Dispatch message)


//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
//	radiance.m_http.InitializeMessaging(&radiance.m_msgr);
	radiance.m_net.InitializeMessaging(&radiance.m_msgr);

	db.InitializeMessaging(&radiance.m_msgr);
//	g_miranda.InitializeMessaging(&radiance.m_msgr);

// TODO: here, get the DB and pull any override settings.
	//****
	CDBconn* pdbConn = db.CreateNewConnection(radiance.m_settings.m_pszDSN, radiance.m_settings.m_pszUser, radiance.m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			radiance.m_data.m_ulFlags |= (RADIANCE_STATUS_FAIL_DB|RADIANCE_STATUS_ERROR);
			radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
			radiance.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Radiance:database_connect", errorstring);  //(Dispatch message)
		}
		else
		{
			radiance.m_settings.m_pdbConn = pdbConn;
			radiance.m_settings.m_pdb = &db;
			radiance.m_data.m_pdbConn = pdbConn;
			radiance.m_data.m_pdb = &db;
			if(radiance.m_settings.GetFromDatabase(errorstring)<RADIANCE_SUCCESS)
			{
				radiance.m_data.m_ulFlags |= (RADIANCE_STATUS_FAIL_DB|RADIANCE_STATUS_ERROR);
				radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
				radiance.m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:database_get", errorstring);  //(Dispatch message)
			}
			else
			{


				if(
						(radiance.m_settings.m_pszQueue)
					&&(strlen(radiance.m_settings.m_pszQueue)>0)
					)
				{

					char szSQL[DB_SQLSTRING_MAXLEN];
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
														radiance.m_settings.m_pszQueue  // table name
													);
//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "%s", szSQL);  Sleep(250); //(Dispatch message)

					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						//**MSG
					}

				}


				if(
						(radiance.m_settings.m_pszCommandQueue)
					&&(strlen(radiance.m_settings.m_pszCommandQueue)>0)
					)
				{

					char szSQL[DB_SQLSTRING_MAXLEN];
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
														radiance.m_settings.m_pszCommandQueue  // table name
													);
//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "%s", szSQL);  Sleep(250); //(Dispatch message)

					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						//**MSG
					}

				}



				if((!radiance.m_settings.m_bUseLog)&&(bUseLog))
				{
					// reset it
					radiance.m_msgr.DM(MSG_ICONINFO, NULL, "Radiance", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

					Sleep(250); // let the message get there.
					radiance.m_msgr.RemoveDestination("log");

				}
			}
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", radiance.m_settings.m_pszDSN, radiance.m_settings.m_pszUser, radiance.m_settings.m_pszPW); 
		radiance.m_data.m_ulFlags |= (RADIANCE_STATUS_FAIL_DB|RADIANCE_STATUS_ERROR);
		radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
		radiance.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Radiance:database_init", errorstring);  //(Dispatch message)

		//**MSG
	}



//init command and status listeners.
	radiance.m_data.m_ulFlags &= ~RADIANCE_STATUS_CMDSVR_MASK;
	radiance.m_data.m_ulFlags |= RADIANCE_STATUS_CMDSVR_START;

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing command server on %d", radiance.m_settings.m_usCommandPort); 
	radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
	radiance.m_msgr.DM(MSG_ICONNONE, NULL, "Radiance:command_server_init", errorstring);  //(Dispatch message)

	if(radiance.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = radiance.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "RadianceCommandServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = RadianceCommandHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &radiance;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &radiance.m_net;					// pointer to the object with the Message function.


		if(radiance.m_net.StartServer(pServer, &radiance.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			radiance.m_data.m_ulFlags &= ~RADIANCE_STATUS_CMDSVR_MASK;
			radiance.m_data.m_ulFlags |= RADIANCE_STATUS_CMDSVR_ERROR;
			radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
			radiance.m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:command_server_init", errorstring);  //(Dispatch message)
			radiance.SendMsg(CX_SENDMSG_ERROR, "Radiance:command_server_init", errorstring);
		}
		else
		{
//AfxMessageBox("Q1");
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Command server listening on %d", radiance.m_settings.m_usCommandPort);
			radiance.m_data.m_ulFlags &= ~RADIANCE_STATUS_CMDSVR_MASK;
			radiance.m_data.m_ulFlags |= RADIANCE_STATUS_CMDSVR_RUN;
			radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
			radiance.m_msgr.DM(MSG_ICONNONE, NULL, "Radiance:command_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}
	radiance.m_data.m_ulFlags &= ~RADIANCE_STATUS_STATUSSVR_MASK;
	radiance.m_data.m_ulFlags |= RADIANCE_STATUS_STATUSSVR_START;

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing status server on %d", radiance.m_settings.m_usStatusPort); 
	radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
	radiance.m_msgr.DM(MSG_ICONNONE, NULL, "Radiance:status_server_init", errorstring);  //(Dispatch message)

	if(radiance.m_settings.m_usStatusPort>0)
	{
//AfxMessageBox("Q1");
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = radiance.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "RadianceStatusServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = RadianceStatusHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &radiance;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &radiance.m_net;					// pointer to the object with the Message function.

		if(radiance.m_net.StartServer(pServer, &radiance.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			radiance.m_data.m_ulFlags &= ~RADIANCE_STATUS_STATUSSVR_MASK;
			radiance.m_data.m_ulFlags |= RADIANCE_STATUS_STATUSSVR_ERROR;
			radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
			radiance.m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:status_server_init", errorstring);  //(Dispatch message)
			radiance.SendMsg(CX_SENDMSG_ERROR, "Radiance:status_server_init", errorstring);
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Status server listening on %d", radiance.m_settings.m_usStatusPort);
			radiance.m_data.m_ulFlags &= ~RADIANCE_STATUS_STATUSSVR_MASK;
			radiance.m_data.m_ulFlags |= RADIANCE_STATUS_STATUSSVR_RUN;
			radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
			radiance.m_msgr.DM(MSG_ICONNONE, NULL, "Radiance:status_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

//AfxMessageBox("QQ");

	
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Radiance is registering destination devices...");
	radiance.m_msgr.DM(MSG_ICONNONE, NULL, "Radiance:destination_init", errorstring);  //(Dispatch message)
	radiance.m_data.m_ulFlags &= ~RADIANCE_STATUS_THREAD_MASK;
	radiance.m_data.m_ulFlags |= RADIANCE_STATUS_THREAD_SPARK;
	radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);

	// now connect all the registered servers
	// this is the list of destinations in the db that get connected


	if(!(radiance.m_settings.m_ulMainMode&RADIANCE_MODE_CLONE))
	{
		// get connections from database - set up Harris API connections
		radiance.m_data.GetDestinations();
//	radiance.m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "X2");  Sleep(250);//(Dispatch message)
//		radiance.m_data.GetChannels();
//	radiance.m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "X3");  Sleep(250);//(Dispatch message)
	}
//	radiance.m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "X4");  Sleep(250);//(Dispatch message)
	
	if((radiance.m_data.m_ulFlags&RADIANCE_ICON_MASK) != RADIANCE_STATUS_ERROR)
	{
		radiance.m_data.m_ulFlags &= ~RADIANCE_ICON_MASK;
		radiance.m_data.m_ulFlags |= RADIANCE_STATUS_OK;  // green - we want run to be blue when something in progress
	}


// initialize cortex (make this not persistent for now)
	CNetData* pdata = new CNetData;
	if(pdata)
	{

		pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA;      // defined type - indicates which protocol to use, structure of data

		pdata->m_ucCmd = CX_REQ_HELLO;       // the command byte
		pdata->m_ucSubCmd = 0;       // the subcommand byte

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
		if(pch)
		{
			sprintf(pch, "%s:%d:%d:%d:%s", 
				radiance.m_data.m_pszHost,
				radiance.m_settings.m_usCommandPort,
				radiance.m_settings.m_usStatusPort,
				CX_TYPE_RESOURCE,
				radiance.m_settings.m_pszName?radiance.m_settings.m_pszName:"Radiance"
				);

			pdata->m_pucData =  (unsigned char*) pch;
			pdata->m_ulDataLen = strlen(pch);
		}

		SOCKET s = NULL; 
//AfxMessageBox("sending");
//AfxMessageBox( radiance.m_data.m_pszCortexHost );
//AfxMessageBox( (char*)pdata->m_pucData );
		if(radiance.m_net.SendData(pdata, radiance.m_data.m_pszCortexHost, radiance.m_data.m_usCortexCommandPort, 5000, 0, NET_SND_CMDTOSVR, &s)>=NET_SUCCESS)
		{
			//send ack
//			AfxMessageBox("ack");
			radiance.m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);
		}
		else
		{
//			AfxMessageBox("could not send");
		}
		radiance.m_net.CloseConnection(s);

		delete pdata;

	}


	radiance.m_data.m_ulFlags &= ~RADIANCE_STATUS_THREAD_MASK;
	radiance.m_data.m_ulFlags |= RADIANCE_STATUS_THREAD_RUN;

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Radiance main thread running.");  
	radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
//	radiance.m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", errorstring);  Sleep(250);//(Dispatch message)
	radiance.SendMsg(CX_SENDMSG_INFO, "Radiance:init", "Radiance %s main thread running.", RADIANCE_CURRENT_VERSION);

	_timeb timebCheckMods;
	_ftime( &timebCheckMods );
///AfxMessageBox("xxxxx");
	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &radiance.m_data.m_timebTick );
//	radiance.m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "%d.%03d", radiance.m_data.m_timebTick.time, radiance.m_data.m_timebTick.millitm);   Sleep(250);//(Dispatch message)

// monitor changes in exchange db and react.
		if(
					(radiance.m_data.m_timebTick.time > timebCheckMods.time )
				||((radiance.m_data.m_timebTick.time == timebCheckMods.time)&&(radiance.m_data.m_timebTick.millitm >= timebCheckMods.millitm))
				&&(!g_bKillThread)
				&&(!radiance.m_data.m_bProcessSuspended)
/*
// cant do the following here.  need to allow the call to radiance.m_settings.GetFromDatabase()
// with a new license key!
				&&(radiance.m_data.m_key.m_bValid)  // must have a valid license
				&&(
				    (!radiance.m_data.m_key.m_bExpires)
					||((radiance.m_data.m_key.m_bExpires)&&(!radiance.m_data.m_key.m_bExpired))
					||((radiance.m_data.m_key.m_bExpires)&&(radiance.m_data.m_key.m_bExpireForgiveness)&&(radiance.m_data.m_key.m_ulExpiryDate+radiance.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
				    (!radiance.m_data.m_key.m_bMachineSpecific)
					||((radiance.m_data.m_key.m_bMachineSpecific)&&(radiance.m_data.m_key.m_bValidMAC))
					)
*/
			)
		{
//	radiance.m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "time to check");  Sleep(250);//(Dispatch message)
			timebCheckMods.time = radiance.m_data.m_timebTick.time + radiance.m_settings.m_ulModsIntervalMS/1000; 
			timebCheckMods.millitm = radiance.m_data.m_timebTick.millitm + (unsigned short)(radiance.m_settings.m_ulModsIntervalMS%1000); // fractional second updates
			if(timebCheckMods.millitm>999)
			{
				timebCheckMods.time++;
				timebCheckMods.millitm%=1000;
			}
//	radiance.m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "checking db connected");   Sleep(250);//(Dispatch message)
			if((pdbConn)&&(pdbConn->m_bConnected))
			{
//AfxMessageBox("x");
//	radiance.m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "checkmods");  //(Dispatch message)
				radiance.m_data.CheckDatabaseMods();
				if(radiance.m_data.m_nSettingsMod != radiance.m_data.m_nLastSettingsMod)
				{
					if(radiance.m_settings.GetFromDatabase()>=RADIANCE_SUCCESS)
					{
						radiance.m_data.m_nLastSettingsMod = radiance.m_data.m_nSettingsMod;
					}
				}
				if(radiance.m_data.m_nDestinationsMod != radiance.m_data.m_nLastDestinationsMod)
				{
//			radiance.m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "getting destinations again");   Sleep(250);//(Dispatch message)
					if( 
							(radiance.m_data.m_key.m_bValid)  // must have a valid license
						&&(
								(!radiance.m_data.m_key.m_bExpires)
							||((radiance.m_data.m_key.m_bExpires)&&(!radiance.m_data.m_key.m_bExpired))
							||((radiance.m_data.m_key.m_bExpires)&&(radiance.m_data.m_key.m_bExpireForgiveness)&&(radiance.m_data.m_key.m_ulExpiryDate+radiance.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
							)
						&&(
								(!radiance.m_data.m_key.m_bMachineSpecific)
							||((radiance.m_data.m_key.m_bMachineSpecific)&&(radiance.m_data.m_key.m_bValidMAC))
							)
						)
					{
						if(radiance.m_data.GetDestinations()>=RADIANCE_SUCCESS)
						{
							radiance.m_data.m_nLastDestinationsMod = radiance.m_data.m_nDestinationsMod;
						}
					}
				}
			}
		}
//AfxMessageBox("zoinks");

		
		// following section is outside the mods loop, the Queue is really the main loop.
// following line commented out, because we want to check the Queue all the time, for the presence of anything at all to deal with.
//				if(radiance.m_data.m_nQueueMod != radiance.m_data.m_nLastQueueMod)
		if(
				(!radiance.m_data.m_bProcessSuspended)	
			&&(radiance.m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!radiance.m_data.m_key.m_bExpires)
				||((radiance.m_data.m_key.m_bExpires)&&(!radiance.m_data.m_key.m_bExpired))
				||((radiance.m_data.m_key.m_bExpires)&&(radiance.m_data.m_key.m_bExpireForgiveness)&&(radiance.m_data.m_key.m_ulExpiryDate+radiance.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!radiance.m_data.m_key.m_bMachineSpecific)
				||((radiance.m_data.m_key.m_bMachineSpecific)&&(radiance.m_data.m_key.m_bValidMAC))
				)
			)
		{
//			radiance.m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "getting channels again");   Sleep(250);//(Dispatch message)
			if(radiance.m_data.GetQueue()>=RADIANCE_SUCCESS)
			{
				radiance.m_data.m_nLastQueueMod = radiance.m_data.m_nQueueMod;
			}
		}
		
		
		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();

		Sleep(1); 
//		Sleep(1000);  // stalls window procedure
	}

	radiance.m_data.m_ulFlags &= ~RADIANCE_STATUS_THREAD_MASK;
	radiance.m_data.m_ulFlags |= RADIANCE_STATUS_THREAD_END;

	radiance.m_msgr.DM(MSG_ICONINFO, NULL, "Radiance:uninit", "Radiance is shutting down.");  //(Dispatch message)
	radiance.SendMsg(CX_SENDMSG_INFO, "Radiance:uninit", "Radiance %s is shutting down.", RADIANCE_CURRENT_VERSION);

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Radiance is shutting down.");  
	radiance.m_data.m_ulFlags &= ~CX_ICON_MASK;
	radiance.m_data.m_ulFlags |= CX_STATUS_UNKNOWN;
	radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);

/*
	// here's a hard coded one for now
	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no need to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = radiance.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		radiance.m_net.OpenConnection(radiance.m_http.m_pszHost, 10888, &s); // branding hardcode
		radiance.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		radiance.m_net.CloseConnection(s); // branding hardcode
	}
*/

// shut down all the running objects;

	if(radiance.m_data.m_ppDestObj)
	{
		int i=0;
		while(i<radiance.m_data.m_nNumDestinationObjects)
		{
			if(radiance.m_data.m_ppDestObj[i])
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", 
					radiance.m_data.m_ppDestObj[i]->m_pszDesc?radiance.m_data.m_ppDestObj[i]->m_pszDesc:radiance.m_data.m_ppDestObj[i]->m_pDlg->m_reName); 
				
				radiance.m_data.m_ppDestObj[i]->m_pDlg->OnDisconnect();

// **** disconnect servers
				radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
				radiance.m_msgr.DM(MSG_ICONINFO, NULL, "Radiance:connection_uninit", errorstring);    //(Dispatch message)
				while(radiance.m_data.m_ppDestObj[i]->m_pDlg->m_bConnected)
				{
					_ftime( &radiance.m_data.m_timebTick );
					Sleep(50);
				}
				radiance.m_data.m_ppDestObj[i]->m_bKillConnThread = true;
				while(radiance.m_data.m_ppDestObj[i]->m_bConnThreadStarted)
				{
					_ftime( &radiance.m_data.m_timebTick );
					Sleep(50);
					
				}
				Sleep(50);

//				radiance.m_data.m_ppDestObj[i]->m_pDlg->OnCancel();
//				radiance.m_data.m_ppDestObj[i]->m_pDlg = NULL;

				delete radiance.m_data.m_ppDestObj[i];
				radiance.m_data.m_ppDestObj[i] = NULL;
			}
			i++;
		}
		delete [] radiance.m_data.m_ppDestObj;
	}





//	m_pDlg->SetProgress(RADIANCEDLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
//	radiance.m_data.m_ulFlags &= ~RADIANCE_STATUS_FILESVR_MASK;
//	radiance.m_data.m_ulFlags |= RADIANCE_STATUS_FILESVR_END;
//	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down file server....");  
//	radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
//	_ftime( &radiance.m_data.m_timebTick );
//	radiance.m_http.EndServer();
	_ftime( &radiance.m_data.m_timebTick );
//	AfxMessageBox("shutting down command server.");
	radiance.m_data.m_ulFlags &= ~RADIANCE_STATUS_CMDSVR_MASK;
	radiance.m_data.m_ulFlags |= RADIANCE_STATUS_CMDSVR_END;
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down command server....");  
	radiance.m_msgr.DM(MSG_ICONNONE, NULL, "Radiance:command_server_uninit", errorstring);  //(Dispatch message)
	radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
	_ftime( &radiance.m_data.m_timebTick );
	radiance.m_net.StopServer(radiance.m_settings.m_usCommandPort, 5000, errorstring);
	_ftime( &radiance.m_data.m_timebTick );
//	AfxMessageBox("shutting down status server.");
	radiance.m_data.m_ulFlags &= ~RADIANCE_STATUS_STATUSSVR_MASK;
	radiance.m_data.m_ulFlags |= RADIANCE_STATUS_STATUSSVR_END;
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down status server....");  
	radiance.m_msgr.DM(MSG_ICONNONE, NULL, "Radiance:status_server_uninit", errorstring);  //(Dispatch message)
	radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);
	_ftime( &radiance.m_data.m_timebTick );
	radiance.m_net.StopServer(radiance.m_settings.m_usStatusPort, 5000, errorstring);
	_ftime( &radiance.m_data.m_timebTick );
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Radiance is exiting.");  
	radiance.m_msgr.DM(MSG_ICONNONE, NULL, "Radiance:uninit", errorstring);  //(Dispatch message)
	radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);



	// save settings.  // dont save them here.  save them on any changes in the main command loop.

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// these explicts arent necessary - uncomment to write out a full file to edit...
		file.SetIniString("Main", "Name", radiance.m_settings.m_pszName);
		file.SetIniString("License", "Key", radiance.m_settings.m_pszLicense);

		file.SetIniInt("CommandServer", "ListenPort", radiance.m_settings.m_usCommandPort);
		file.SetIniInt("StatusServer", "ListenPort", radiance.m_settings.m_usStatusPort);
		file.SetIniString("FileServer", "IconPath", radiance.m_settings.m_pszIconPath);

		file.SetIniInt("Messager", "UseEmail", radiance.m_settings.m_bUseEmail?1:0);
		file.SetIniInt("Messager", "UseNet", radiance.m_settings.m_bUseNetwork?1:0);
		file.SetIniInt("Messager", "UseLog", radiance.m_settings.m_bUseLog?1:0);

		file.SetIniString("Database", "DSN", radiance.m_settings.m_pszDSN);
		file.SetIniString("Database", "DBUser", radiance.m_settings.m_pszUser);
		file.SetIniString("Database", "DBPassword", radiance.m_settings.m_pszPW);
		file.SetIniString("Database", "SettingsTableName", radiance.m_settings.m_pszSettings);  // the Settings table name
		file.SetIniString("Database", "ExchangeTableName", radiance.m_settings.m_pszExchange);  // the Exchange table name
		file.SetIniString("Database", "MessagesTableName", radiance.m_settings.m_pszMessages);  // the Messages table name

//		file.SetIniInt("HarrisAPI", "UseListCount", radiance.m_settings.m_bUseListCount?1:0); // get all events up until the list count (otherwise just up to the lookahead)

		file.SetIniString("Database", "DestinationsTableName", radiance.m_settings.m_pszDestinations);  // the Destinations table name
		file.SetIniString("Database", "QueueTableName", radiance.m_settings.m_pszQueue);  // the Queue table name
		file.SetIniString("Database", "DestinationMediaTableName", radiance.m_settings.m_pszDestinationMedia);  // the DestinationMedia table name
		file.SetIniString("Database", "CommandQueueTableName", radiance.m_settings.m_pszCommandQueue);  // the Command Queue table name
		file.SetIniString("Database", "DestinationMediaTempTableName", radiance.m_settings.m_pszDestinationMediaTemp );  // the Destinations table name

		file.SetIniInt("Database", "ModificationCheckInterval", radiance.m_settings.m_ulModsIntervalMS);  // in milliseconds

		file.SetSettings(RADIANCE_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

	}

	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "shutting down");
	radiance.m_data.m_ulFlags &= ~RADIANCE_ICON_MASK;
	radiance.m_data.m_ulFlags |= RADIANCE_STATUS_UNINIT;
	radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);

	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Radiance is exiting");
	radiance.m_data.m_ulFlags &= ~CX_ICON_MASK;
	radiance.m_data.m_ulFlags |= CX_STATUS_UNINIT;
	radiance.m_data.SetStatusText(errorstring, radiance.m_data.m_ulFlags);

	//exiting
	radiance.m_msgr.DM(MSG_ICONINFO, NULL, "Radiance", "-------------- Radiance %s exit ---------------\n\
--------------------------------------------------\n", RADIANCE_CURRENT_VERSION);  //(Dispatch message)
///	m_pDlg->SetProgress(RADIANCEDLG_CLEAR); // no point

	_ftime( &radiance.m_data.m_timebTick );
	radiance.m_data.m_ulFlags &= ~RADIANCE_STATUS_THREAD_MASK;
	radiance.m_data.m_ulFlags |= RADIANCE_STATUS_THREAD_ENDED;

//	g_pradiance = NULL;
//	db.RemoveConnection(pdbConn);

	Sleep(500); // small delay at end
	g_bThreadStarted = false;
	Sleep(300); // another small delay at end
	_endthread();
}

/*
void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((g_pmsgr)&&(g_pmsgr->m_lpfnDM))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		CMessagingObject msg;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = msg.EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = msg.EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = msg.EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch;
		pch = (char*) malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
* /
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if(pszEncodedMsg) 
			{
				ulBufferLen = strlen(pszEncodedMsg);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedCaller)
			{
				ulBufferLen = strlen(pszEncodedCaller);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedDest)
			{
				ulBufferLen = strlen(pszEncodedDest);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			g_pmsgr->m_lpfnDM(pch, ulFlags);
			free(pch);
		}
	}
}
*/


/*
// this thread is based on void HTTP10HandlerThread(void* pvArgs) from
// HTTP10.cpp.  It makes a special webserver with security, cgi parsing, and dynamic results
void RadianceHTTPThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return;}

	CRadianceMain* pRadiance = (CRadianceMain*)(pClient->m_lpObject) ;  // pointer to the global object.

// HTTP service by def is a non-persistent service, so even if the Client type is
// set to persist, we ignore that directive in this handler thread

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;


		CHTTP10* phttp = (CHTTP10*)(&(pRadiance->m_http)) ;  // pointer to the global object.
		if(phttp == NULL)// cant do anything!
		{
			shutdown(pClient->m_socket, SD_BOTH);
			closesocket(pClient->m_socket);
			(*(pClient->m_pulConnections))--;
			_endthread(); return;
		}

		CHTTP10 http(phttp->m_pszRoot);  // local object for utility functions ONLY.
		// this allows the local to clone the root setting of the global, 
		// in order to do correct parsing in its own thread

		// the following are just pointers to the global obj.  
		// must NULL these out before object gets destroyed, 
		// so that global resources not freed!   !!!!!!!  important !!!!!
		http.m_pszHost = phttp->m_pszHost;
		http.m_ppszErrors = phttp->m_ppszMime;
		http.m_ppszMime = phttp->m_ppszMime;   
		http.m_ulStatus = phttp->m_ulStatus;   // important for redirect and error
		
		unsigned char* pch;// for use with GetLine().
		unsigned long ulBufLen;// for use with GetLine().
		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		unsigned short usCode = 0;

		// get the raw buffer and process it yourself
		nReturn = http.m_net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_EOLN|EOLN_HTTP, pszStatus);
		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). 
			//was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
		{
			//error.
			if(pClient->m_lpMsgObj)
			{
				_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error receiving data.  %s", pszStatus);
				((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
			}

			usCode = 400;  //  we TRY to send this....
			if(pch!=NULL)
			{
				free(pch);  // must free the incoming data buffer 
				pch = NULL;
			}
			// create a return message buffer.
			pch = (unsigned char*)http.ErrorBuffer(NULL, usCode);   // 400 bad request
			ulBufLen = strlen((char*)pch); // valid from an error page.

			// send malformed request answer if possible  - have to tell the client that the request was not received in good order.
			nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
			if(nReturn<NET_SUCCESS)
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
				}
			}
		}
		else  // successful reception of data.
		{
			// process the data here...
			// in this sample the buffer is loaded into
			// pch, with ulBufLen as length

/*
			FILE* fp = fopen("rec.txt", "ab");
			if (fp)
			{
				fwrite(pch, 1, ulBufLen, fp );
				fflush(fp);
				fclose(fp);
			}
* /
			// parse buffer, format reply.
			CHTTPHeader header;

			if (http.ParseHeader(pch, ulBufLen, &header) < HTTP_SUCCESS)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("header not parsed\r\n\r\n", 1, strlen("header not parsed\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}
* /
				
				if(pch!=NULL) 
				{
					free(pch);  // must free the incoming data buffer 
					pch = NULL;
				}
				// create a return message buffer.
				usCode = 400;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}
			else
			{
				// here we have enough info to check security if desired, and if not, return a 401;
				// else continue....
/*
					FILE* fp = fopen("rec.txt", "ab");
					if (fp)
					{// lets write out the whole header so we can examine its full contents. including args.
						char bufferin[10000];
						sprintf(bufferin,
"header successfully parsed\r\n\
method: %d\r\n\
version: %s\r\n\
URL: %s\r\n\
Path: %s\r\n\
mimetype: %s\r\n\
user: %s\r\n\
password: %s\r\n\
num args: %ld\r\n",
	header.m_ucMethod,  // enumerated values
	header.m_pszVersion==NULL?"(null)":header.m_pszVersion,
	header.m_pszURL==NULL?"(null)":header.m_pszURL,
	header.m_pszPath==NULL?"(null)":header.m_pszPath,
	header.m_pszMimeType==NULL?"(null)":header.m_pszMimeType,
	header.m_pszUser==NULL?"(null)":header.m_pszUser,
	header.m_pszPassword==NULL?"(null)":header.m_pszPassword,
	header.m_ulNumArgs

);
		
	unsigned long k=0;
	while(k<header.m_ulNumArgs)
	{
		if((header.m_ppszArgNames)&&(header.m_ppszArgValues))
		{
			strcat(bufferin, "  ");
			strcat(bufferin, header.m_ppszArgNames[k]==NULL?"(null)":header.m_ppszArgNames[k]);
			strcat(bufferin, "=");
			strcat(bufferin, header.m_ppszArgValues[k]==NULL?"(null)":header.m_ppszArgValues[k]);
			strcat(bufferin, "\r\n");
		}
	k++;
	}

	strcat(bufferin, "headers:\r\n");
	strcat(bufferin, header.m_pszHeaders);
	strcat(bufferin, "\r\n");


			//		fwrite("header successfully parsed\r\n", 1, strlen("header successfully parsed\r\n"), fp );
						fwrite(bufferin, 1, strlen(bufferin), fp );
						fflush(fp);
						fclose(fp);
					}

* /


				if((phttp->m_pSecure)&&((header.m_pszUser==NULL)||(header.m_pszPassword==NULL)))
				{
					usCode = 401;
					pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
					ulBufLen = strlen((char*)pch); // valid from an error page.
					// buffer successfully parsed.
				}
				else 
				{
					if(pch!=NULL)
					{
						free(pch);  // must free the incoming data buffer 
						pch = NULL;
					}

					// create a return message buffer.
					switch(header.m_ucMethod)
					{
					case HTTP_GET://			1  // HTTP/1.0
					case HTTP_HEAD://			2  // HTTP/1.0
						{
							if(
									(strnicmp(header.m_pszPath, http.m_pszRoot, strlen(http.m_pszRoot)) == 0 )  // else not a part of the served file tree
								)
							{
								if(phttp->m_pSecure)
								{
									// have to check the main object for permission.
									// we are going to go by URL (asset level)
									// Loci are going to just be convenient names for the admins to set up.

									if((phttp->m_pSecure->CheckSecure(header.m_pszUser, header.m_pszPassword, NULL, header.m_pszURL))!=SECURE_SUCCESS)
									{
										usCode = 401;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
										ulBufLen = strlen((char*)pch); // valid from an error page.
										break; // get out of the switch from here.
									}
									// else it flows thru....
								}

								DWORD dwAttrib;

								dwAttrib = GetFileAttributes(header.m_pszPath);

								// check for directory (redirect nec.)
								if ( (dwAttrib!=0xffffffff)&&(dwAttrib&FILE_ATTRIBUTE_DIRECTORY) )  // if error, it may be because no file exists, so check in the next part.
								{
									usCode = 301;  // not necessary but why not.
									pch = (unsigned char*)http.RedirectBuffer(&header);
									ulBufLen = strlen((char*)pch); // valid from an redirect page.
								}
								else
								{
									// before we try to open the file, lets spend the time now (not while the file is open)
									// to figure out how long the static text respone buffer will be.
									CBufferUtil bu;
									// notice the sample "Server: RadianceServer/3.0.3.1" header included.  Pick the appname and ver, probably best
									unsigned long ulBufferLen = strlen("HTTP/1.0 200 OK\r\nServer: RadianceServer/\r\nContent-Length: \r\n\r\n");
									ulBufferLen += strlen(RADIANCE_CURRENT_VERSION);

									if((header.m_pszMimeType)&&(strlen(header.m_pszMimeType)>0))
									{
										ulBufferLen += (strlen("Content-Type: \r\n") + strlen(header.m_pszMimeType)); // only add if known!
									}
									else // empty!
									{
										// dont free the "permanent resource", just null the pointer
										header.m_pszMimeType=NULL;
									}

									FILE* pFile = NULL;

									pFile = fopen(header.m_pszPath, "rb");
									if (pFile == NULL)  // file not openable, or not found.
									{
										usCode = 404;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 404 not found
										ulBufLen = strlen((char*)pch); // valid from an error page.
									}
									else
									{
										// this part just assembles the file into a buffer.
										// this may be modified to do dynamic html things by using the 
										// pClient->m_lpObject that refers back to some parser....

										usCode = 200; // OK  (!)
										// determine file size
										fseek(pFile, 0, SEEK_END);
										unsigned long ulFileLen = ftell(pFile);


										// here we have to parse out any directives.  if this is a template file,
										// we may have a variable length.
									//	x


										if(header.m_ucMethod == HTTP_GET)
										{
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += (strlen(length)+ulFileLen);

											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you truncations at zeroed bytes

											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: RadianceServer/%s", RADIANCE_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
											
											fclose(pFile);
										}
										else
										if(header.m_ucMethod == HTTP_HEAD)
										{
											fclose(pFile);  // we have all we need already.
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += strlen(length);
											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											//  create the header
											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: RadianceServer/%s", RADIANCE_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												// its identical except for the body.  next line must stay commented out, its just there for ref.
												//fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
										}
									}
								}
							}
							else  // not part of the server service.
							{
								usCode = 403;
								pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 403 forbidden ...
								ulBufLen = strlen((char*)pch); // valid from an error page.
							}

						} break;
					case HTTP_POST://			3  // HTTP/1.0
						{
							usCode = 501;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 501 not implemented ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_PUT://			4  // HTTP/1.1
					case HTTP_DELETE://		5  // HTTP/1.1
					case HTTP_OPTIONS://  6  // HTTP/1.1
					case HTTP_TRACE://	  7  // HTTP/1.1
						{
							usCode = 505;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 505 http version not supported ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_UNDEF://		0  // internal
					default:
						{
							usCode = 400;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					}
				}
			}

			// send a reply, in pch., with strlen pch as length

			if(pch == NULL)  // had a problem forming the request, try to send a last ditch error code.
			{
				usCode = 500;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 500 internal server error
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}

			if(pch != NULL)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("sending:\r\n", 1, strlen("sending:\r\n"), fp );
					fwrite(pch, 1, ulBufLen, fp );
					fwrite("\r\n\r\n", 1, strlen("\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}

* /
				nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
				if(nReturn<NET_SUCCESS)
				{
					//error.
					if(pClient->m_lpMsgObj)
					{
						_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
						((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
					}
				}
			}
		}

//		char foo[34]; sprintf(foo, "code: %d", usCode);	AfxMessageBox(foo);

		http.m_pszHost = NULL;		// but do NOT free resource
		http.m_ppszErrors = NULL; // but do NOT free resource
		http.m_ppszMime = NULL;   // but do NOT free resource

		if(pch!=NULL) free(pch);  // must free the data buffer 

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}
*/


void RadianceCommandHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	
	CRadianceMain* pradiance = (CRadianceMain*) pClient->m_lpObject;
	if(pradiance==NULL) { _endthread(); return; }  // need the object to deal

	bool bSetGlobalKill = false;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure in Protocol 1  //SECURE with PROTOCOL2 later
			strcpy(pszInfo, "");  // clear it
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error %d receiving data.  %s", nReturn, pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					strcpy(pszInfo, "");  // clear it
					nReturn = net.SendData(&data, pClient->m_socket, 2000, 0, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error %d sending NAK reply.  %s", nReturn, pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

// **** need to put in the "meat" for these commands.
					switch(data.m_ucCmd)
					{
					case RADIANCE_CMD_BYE:
						{
	pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance", "The Global Kill command has been received.");  //(Dispatch message)

							bSetGlobalKill = true;
							g_pradiance->m_data.m_bQuietKill = true;
							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.
							(pClient->m_ucType)&= ~NET_TYPE_KEEPOPEN;  // do not persist if kill

						} break;
					case RADIANCE_CMD_LOADSCENE://				0xb0  // load a project/scene
						{
							if((data.m_ulDataLen)&&(data.m_pucData))
							{
	pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance", "RADIANCE_CMD_LOADSCENE: %s",data.m_pucData);  //(Dispatch message)
								CString szRenderer;
								CString szProject;
								CString szScene;

								char* pch = strchr((char*)data.m_pucData, 28);
								if(pch)
								{
									*pch = 0; pch++;
									szRenderer.Format("%s", data.m_pucData);

									char* pch2 = strchr(pch, 28);
									if(pch2)
									{
										*pch2 = 0; pch2++;
										szProject.Format("%s", pch);
										char* pch3= strchr(pch2, 28);
										if(pch3) *pch3 = 0; // strip any remaining field with a null term

										szScene.Format("%s", pch2);


										if((pradiance->m_data.m_ppDestObj)&&(pradiance->m_data.m_nNumDestinationObjects))
										{
											int x=0;
											while(x<pradiance->m_data.m_nNumDestinationObjects)
											{
												if((pradiance->m_data.m_ppDestObj[x])&&(pradiance->m_data.m_ppDestObj[x]->m_pDlg))
												{
													if(szRenderer.CompareNoCase(pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_reName)==0)
													{

														if(
																(szProject.Compare(pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_loadedproject))
															||(szScene.Compare(pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_activescene))
															)
														{
															if(pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_bConnected)
															{
																pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_project = szProject;
																pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_scene = szScene;
																pradiance->m_data.m_ppDestObj[x]->m_pDlg->UpdateData( FALSE );
																pradiance->m_data.m_ppDestObj[x]->m_pDlg->OnLoad();
																
															}
														}

														x = pradiance->m_data.m_nNumDestinationObjects;  // break out
													}
												}
												x++;
											}
										}
									}
								}
							}

							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.
						} break;
					case RADIANCE_CMD_PLAYANIM://				  0xb1  // play an anim
						{
							if((data.m_ulDataLen)&&(data.m_pucData))
							{
//pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance", "RADIANCE_CMD_PLAYANIM:  %s",data.m_pucData);  //(Dispatch message)
								CString szRenderer;
								CString szAnim;

								char* pch = strchr((char*)data.m_pucData, 28);
								if(pch)
								{
									*pch = 0; pch++;
									szRenderer.Format("%s", data.m_pucData);

									char* pch2= strchr(pch, 28);
									if(pch2) *pch2 = 0; // strip any remaining field with a null term
									szAnim.Format("%s", pch);

									if((pradiance->m_data.m_ppDestObj)&&(pradiance->m_data.m_nNumDestinationObjects))
									{
										int x=0;
										while(x<pradiance->m_data.m_nNumDestinationObjects)
										{
											if((pradiance->m_data.m_ppDestObj[x])&&(pradiance->m_data.m_ppDestObj[x]->m_pDlg))
											{
												if(szRenderer.CompareNoCase(pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_reName)==0)
												{
//pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance", "RADIANCE_CMD_PLAYANIM: host matched: %s",data.m_pucData);  //(Dispatch message)
													if(pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_bConnected)
													{
//pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance", "RADIANCE_CMD_PLAYANIM: connected, sending anim [%s]", szAnim);  //(Dispatch message)

														try
														{
															pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_recom.startAnimation(szAnim); // do it first then set the dlg to match

															if(pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_animsCombo.SelectString( -1, szAnim )!= CB_ERR)
															{
																pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_anims = szAnim;
																pradiance->m_data.m_ppDestObj[x]->m_pDlg->UpdateData( FALSE );
														//		pradiance->m_data.m_ppDestObj[x]->m_pDlg->OnStartAnim();
															}
														}
														catch(CException *e)// CDBException *e, CMemoryException *m)  
														{
															if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
															{
																// The error code is in e->m_nRetCode
//pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "Retrieve: Caught exception: out of memory sending %s.", szAnim);
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
															}
															else 
															{
pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "Retrieve: Caught other exception sending %s.", szAnim);
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
															}
															e->Delete();
														} 
														catch( ... )
														{
pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "Retrieve: Caught exception sending %s.", szAnim);
														}

													}
													x = pradiance->m_data.m_nNumDestinationObjects;  // break out

												}
											}
											x++;
										}
									}
								}
							}

							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.
						} break;
					case RADIANCE_CMD_SETEXPORT://				0xb2  // set an export
						{
							if((data.m_ulDataLen)&&(data.m_pucData))
							{
//pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance", "RADIANCE_CMD_SETEXPORT: %s",data.m_pucData);  //(Dispatch message)
								CString szRenderer;
								CString szExport;
								CString szValue;

								char* pch = strchr((char*)data.m_pucData, 28);
								if(pch)
								{
									*pch = 0; pch++;
									szRenderer.Format("%s", data.m_pucData);

									char* pch2 = strchr(pch, 28);
									if(pch2)
									{
										*pch2 = 0; pch2++;
										szExport.Format("%s", pch);
										char* pch3= strchr(pch2, 28);
										if(pch3) *pch3 = 0; // strip any remaining field with a null term
										szValue.Format("%s", pch2);



										if((pradiance->m_data.m_ppDestObj)&&(pradiance->m_data.m_nNumDestinationObjects))
										{
											int x=0;
											while(x<pradiance->m_data.m_nNumDestinationObjects)
											{
												if((pradiance->m_data.m_ppDestObj[x])&&(pradiance->m_data.m_ppDestObj[x]->m_pDlg))
												{
													if(szRenderer.CompareNoCase(pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_reName)==0)
													{
														if(pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_bConnected)
														{
															try
															{
																pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_recom.sendExport(szExport, szValue);// do it first then set the dlg to match

																if(pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_exportsCombo.SelectString( -1, szExport )!= CB_ERR)
																{
																	pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_exportVal = szValue;
																	pradiance->m_data.m_ppDestObj[x]->m_pDlg->m_export = szExport;
																	pradiance->m_data.m_ppDestObj[x]->m_pDlg->UpdateData( FALSE );
																	//pradiance->m_data.m_ppDestObj[x]->m_pDlg->OnSendExport();
																}
															}
															catch(CException *e)// CDBException *e, CMemoryException *m)  
															{
																if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
																{
																	// The error code is in e->m_nRetCode
	pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "Retrieve: Caught exception: out of memory sending %s:%s.", szExport, szValue);
										//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
																}
																else 
																{
	pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "Retrieve: Caught other exception sending %s:%s.", szExport, szValue);
										//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
																}
																e->Delete();
															} 
															catch( ... )
															{
	pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "Retrieve: Caught exception sending %s:%s.", szExport, szValue);
															}
														}
														x = pradiance->m_data.m_nNumDestinationObjects;  // break out
													}
												}
												x++;
											}
										}
									}
								}
							}

							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.
						} break;
					default:
						{
							//just ack
							data.m_ucCmd = NET_CMD_ACK;

		//					char repbuf[64];
							if(data.m_pucData!=NULL)
							{
		/*
								_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

								for(unsigned long q=0; q<data.m_ulDataLen; q++)
								{
									if(strlen(repbuf)<63)
										strncat(repbuf, (char*)(data.m_pucData+q), 1);
								}
								if(strlen(repbuf)<63)
									strcat(repbuf, "]");
								data.m_ulDataLen = strlen(repbuf);
								repbuf[data.m_ulDataLen] = 0;  // just in case
		*/
								free(data.m_pucData);  //destroy the buffer;
							}
		/*
							else
							{
								_snprintf(repbuf, 63, "data was NULL");
								data.m_ulDataLen = strlen(repbuf); 
							}

							data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
							if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
							else 
							{
								data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
								data.m_ulDataLen = 0;
							}
		*/
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.
	if (bSetGlobalKill)	//g_bKillThread = true;
	((CRadianceHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();
	_endthread();

}


void RadianceStatusHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszStatus);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Radiance:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Radiance:CommandHandlerThread");
						}
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Radiance:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Radiance:CommandHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}


int CRadianceMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)&&(strlen(m_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			m_settings.m_pszMessages,
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			m_settings.m_pszMessages
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return RADIANCE_SUCCESS;
		}
	}
	return RADIANCE_ERROR;
}


void RadianceConnectionThread(void* pvArgs)
{
	CRadianceObject* pConn = (CRadianceObject*) pvArgs;
	if(pConn==NULL) 
	{
		//**MSG
		return;
	}
	if(pConn->m_bKillConnThread) return;
	pConn->m_bConnThreadStarted = true;

	g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:connection", "Command management thread for %s (%s) has begun.",
											pConn->m_pDlg->m_reName, //server_name
											pConn->m_pszDesc
		);  Sleep(100); //(Dispatch message)

	unsigned long ulTick = GetTickCount();
	unsigned long ulElapsedTick = GetTickCount();


//create table Command_Queue (itemid int identity(1,1), local varchar(2048), remote varchar(256), action int, host varchar(64), timestamp float, username varchar(32), event_itemid int, message varchar(512));  


	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");
	CDBUtil db;

	CDBconn* pdbConn = db.CreateNewConnection(g_pradiance->m_settings.m_pszDSN, g_pradiance->m_settings.m_pszUser, g_pradiance->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			g_pradiance->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Promotor:analysis_database_connect", errorstring);  //(Dispatch message)
			pdbConn = g_pradiance->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", g_pradiance->m_settings.m_pszDSN, g_pradiance->m_settings.m_pszUser, g_pradiance->m_settings.m_pszPW); 
		g_pradiance->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Promotor:analysis_database_init", errorstring);  //(Dispatch message)
		pdbConn = g_pradiance->m_data.m_pdbConn;

		//**MSG
	}

	CDBUtil* pdb = &db;

	CDBUtil dbDel;

	CDBconn* pdbConnDel = dbDel.CreateNewConnection(g_pradiance->m_settings.m_pszDSN, g_pradiance->m_settings.m_pszUser, g_pradiance->m_settings.m_pszPW);
	if(pdbConnDel)
	{
		if(dbDel.ConnectDatabase(pdbConnDel, errorstring)<DB_SUCCESS)
		{
			g_pradiance->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Promotor:analysis_database_connect", errorstring);  //(Dispatch message)
			pdbConnDel = g_pradiance->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", g_pradiance->m_settings.m_pszDSN, g_pradiance->m_settings.m_pszUser, g_pradiance->m_settings.m_pszPW); 
		g_pradiance->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Promotor:analysis_database_init", errorstring);  //(Dispatch message)
		pdbConnDel = g_pradiance->m_data.m_pdbConn;

		//**MSG
	}

	CDBUtil* pdbDel = &dbDel;


	while	((!pConn->m_pDlg->m_bConnected)&&(pConn->m_pDlg->m_bConnecting))
	{
			_ftime( &g_pradiance->m_data.m_timebTick );
	}
	// lets make sure we are connected before agoing forward with the command q


	CString szLocal = "";
	CString szRemote = "";
	CString szServer = "";
	CString szUsername = "";
	CString szMessage = "";
	CString szTemp = "";

	CString szName= "";
	CString szValue= "";

	int nItemID = -1;
	bool bItemFound = false;
	double dblTimestamp=-57;   // timestamp
	int nAction = -1;
	int nTemp;
	int nEventItemID=-1;
	CRecordset* prs = NULL;

//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "RadianceConnectionThread2");  Sleep(250); //(Dispatch message)
	while(!pConn->m_bKillConnThread)
	{
	if(
				  (g_pradiance->m_data.m_key.m_bValid)  // must have a valid license
				&&(
						(!g_pradiance->m_data.m_key.m_bExpires)
					||((g_pradiance->m_data.m_key.m_bExpires)&&(!g_pradiance->m_data.m_key.m_bExpired))
					||((g_pradiance->m_data.m_key.m_bExpires)&&(g_pradiance->m_data.m_key.m_bExpireForgiveness)&&(g_pradiance->m_data.m_key.m_ulExpiryDate+g_pradiance->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
						(!g_pradiance->m_data.m_key.m_bMachineSpecific)
					||((g_pradiance->m_data.m_key.m_bMachineSpecific)&&(g_pradiance->m_data.m_key.m_bValidMAC))
					)
				)
	{
//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "getting status %d", clock());  Sleep(50); //(Dispatch message)

		
		if((g_pradiance)&&(pdbConn)&&(pdb))
		{

			// let's just get the top (oldest timestamp) item in the queue, and deal with it.
			_ftime( &g_pradiance->m_data.m_timebTick );

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s WHERE host = '%s' ORDER BY timestamp ASC", //HARDCODE
				((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
				pConn->m_pDlg->m_reName
				);

//create table Command_Queue (itemid int identity(1,1), local varchar(2048), 
			// remote varchar(256), action int, host varchar(64), timestamp float, 
			// username varchar(32), event_itemid int, message varchar(512));  

			szLocal = "";
			szRemote = "";
			szServer = "";
			szUsername = "";
			szMessage = "";
			szTemp = "";

			szName= "";
			szValue= "";

			nItemID = -1;
			bItemFound = false;
			dblTimestamp=-57;   // timestamp
			nAction = -1;
			nTemp;
			nEventItemID=-1;

//			EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
			prs = pdb->Retrieve(pdbConn, szSQL, errorstring);
			if(prs)
			{
				char errorstring[MAX_MESSAGE_LENGTH];
				strcpy(errorstring, "");
				int nReturn = RADIANCE_ERROR;
				int nIndex = 0;
	//			while ((!prs->IsEOF()))
				while (!prs->IsEOF())
				{
					try
					{
						nReturn = RADIANCE_ERROR;
						prs->GetFieldValue("itemid", szTemp);//HARDCODE
						if(szTemp.GetLength())
						{
							nItemID = atoi(szTemp);
							bItemFound = true;
						}
						prs->GetFieldValue("local", szLocal);//HARDCODE
						szLocal.TrimLeft(); szLocal.TrimRight();
						prs->GetFieldValue("remote", szRemote);//HARDCODE
						szRemote.TrimLeft(); szRemote.TrimRight();
						prs->GetFieldValue("action", szTemp);//HARDCODE
						if(szTemp.GetLength())
						{
							nTemp = atoi(szTemp);
							if(nTemp>=0) nAction = nTemp;
						}
						prs->GetFieldValue("host", szServer);//HARDCODE
						szServer.TrimLeft(); szServer.TrimRight();

						prs->GetFieldValue("timestamp", szTemp);//HARDCODE
						if(szTemp.GetLength())
						{
							dblTimestamp = atof(szTemp);
						}
						prs->GetFieldValue("username", szUsername);//HARDCODE
						szUsername.TrimLeft(); szUsername.TrimRight();
						prs->GetFieldValue("event_itemid", szTemp);//HARDCODE
						if(szTemp.GetLength())
						{
							nTemp = atoi(szTemp);
							if(nTemp>0) nEventItemID = nTemp;
						}

						prs->GetFieldValue("message", szMessage);//HARDCODE
						nReturn = RADIANCE_SUCCESS;
						
					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
//							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
	
	//						if(m_pmsgr)
//							{
//								char errorstring[DB_ERRORSTRING_LEN];
//								_snprintf(errorstring, DB_ERRORSTRING_LEN-1, 
								g_pradiance->m_msgr.DM(MSG_PRI_HIGH|MSG_ICONERROR, NULL, "DBUtil:Retrieve", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
//							}
	
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							// The error code is in e->m_nRetCode
//							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
								g_pradiance->m_msgr.DM(MSG_PRI_HIGH|MSG_ICONERROR, NULL, "DBUtil:Retrieve", "Retrieve: Caught exception: out of memory" );
						}
						else 
						{
//							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
								g_pradiance->m_msgr.DM(MSG_PRI_HIGH|MSG_ICONERROR, NULL, "DBUtil:Retrieve", "Retrieve: Caught other exception" );
						}
						e->Delete();
					} 
					catch( ... )
					{
					}

//				LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);

//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "got %d things from queue", nIndex);   Sleep(50);//(Dispatch message)

					if((nReturn>=0)&&(bItemFound))
					{
						bool bRemove = true;
						// process the request, then remove it from the queue
	//	g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "got item %d from queue", nItemID);   Sleep(250);//(Dispatch message)
				_ftime( &g_pradiance->m_data.m_timebTick );

						if(szServer.GetLength()>0)
						{
							char* pchServer = szServer.GetBuffer(1);
	//						int nDestIndex = g_pradiance->m_data.ReturnDestinationIndex(pchServer);
	//	g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "got server %s from queue", pchServer);   Sleep(250);//(Dispatch message)

	//						if((nDestIndex>=0)&&(m_ppDestObj)&&(m_ppDestObj[nDestIndex])&&(m_ppDestObj[nDestIndex]->m_ulFlags&RADIANCE_FLAG_ENABLED))
							if(pConn->m_ulFlags&RADIANCE_FLAG_ENABLED)
							{  
								// only do anything if its enabled.
	//	g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "enabled");   Sleep(250);//(Dispatch message)

	//							int nHostReturn = RADIANCE_ERROR;
								if((!pConn->m_pDlg->m_bConnected)&&(!pConn->m_pDlg->m_bConnecting))
								{
	//								nHostReturn = pConn->m_pDlg->OnDisconnect();  
								//	nHostReturn = 
									pConn->m_pDlg->OnButtonConnect();
								}

				_ftime( &g_pradiance->m_data.m_timebTick );
								if(pConn->m_pDlg->m_bConnected)
								{

									{
									// first thing is, get the disk space statistics.
		/*
										BOOL bReturn;
										
										if(g_miranda.m_lpfnGetDriveInfo)
										{ 
											if(nDirIndex>0)
											{
												sprintf(pchDirAlias, "%s", szFilenameRemote.Left(nDirIndex));
												if(pchDirAlias[0]!='$') // error
													strcpy(pchDirAlias, "$VIDEO"); // default.
											}
											else
											{
												strcpy(pchDirAlias, "$VIDEO"); // default.
											}

											DiskInfo_ diskinfo;

											bReturn = g_miranda.m_lpfnGetDriveInfo(g_miranda.m_pszHost, pchDirAlias, &diskinfo);
									//		AfxMessageBox("Y");
											if(bReturn == FALSE)
											{
												//**MSG
								//				g_pradiance->m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:oxsox_diskinfo", "Could not get disk information for %s partition on %s", pchDirAlias, pchServer );   Sleep(50);//(Dispatch message)
			/*
			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "m_lpfnGetDriveInfo returned FALSE");   Sleep(50);//(Dispatch message)
												g_pradiance->m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:oxsox_diskinfo", "Could not get disk information for %s partition", pchDirAlias );   Sleep(50);//(Dispatch message)
			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "m_lpfnGetDriveInfo returned2 FALSE");   Sleep(50);//(Dispatch message)
		* /
												g_pradiance->m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:oxsox_diskinfo", "Could not get disk information for %s", pchServer );   Sleep(50);//(Dispatch message)
		//	g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "m_lpfnGetDriveInfo returned3 FALSE");   Sleep(50);//(Dispatch message)

											}
											else
											{
												if(nDestIndex>=0)
												{
													//its a host in our list!
													if((m_ppDestObj)&&(pConn))
													{
														pConn->m_dblDiskKBFree = (double)diskinfo.KBytes_Free;  
														pConn->m_dblDiskKBTotal = (double)diskinfo.KBytes_Total;  
														// ok, now update DB.  but do not increment counter, that will cause infinite loop.

														if(pConn->m_nItemID>0) // last known unique item id
														{

			// create table Destinations (destinationid int identity(1,1) NOT NULL, host varchar(64) NOT NULL,
			//				type int, description varchar(256), diskspace_free float, diskspace_total float, diskspace_threshold real,
			//				channelid int, flags int);

															_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET diskspace_free = %f, diskspace_total = %f WHERE destinationid = %d",  //HARDCODE
																((g_pradiance->m_settings.m_pszDestinations)&&(strlen(g_pradiance->m_settings.m_pszDestinations)))?g_pradiance->m_settings.m_pszDestinations:"Destinations",
																pConn->m_dblDiskKBFree,
																pConn->m_dblDiskKBTotal,
																pConn->m_nItemID);
															if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
															{
																//**MSG
											//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

															}
														}
													}
												}
											}
			//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "after m_lpfnGetDriveInfo ");   Sleep(50);//(Dispatch message)

										}
										else
										{
											//**MSG
											g_pradiance->m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:oxsox_diskinfo", "Could not get disk information for %s partition on %s; NULL function address.", pchDirAlias, pchServer );   Sleep(50);//(Dispatch message)
			//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "m_lpfnGetDriveInfo was NULL");   Sleep(50);//(Dispatch message)
										}
		*/
			//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "file local %s, file remote %s, action %d", szFilenameLocal, szFilenameRemote, nAction);  Sleep(250); //(Dispatch message)

		//								char dberrorstring[DB_ERRORSTRING_LEN];
			//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "switching with %d", nAction);   Sleep(50);//(Dispatch message)
										switch(nAction)
										{
										case 256: // recom command
											{
//->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "*** sending %s to %s", szLocal, pchServer);  Sleep(250); //(Dispatch message)

												// a recom command from the DB is formatted like this:
												// pipe delimited: command|name|value
												// where command 0=loadscene, 1=playanim, 2=setexport
												// name is the name of the project, anim, or export
												// value is the name of the scene, nothing, or the value to which to set the export.


												char* buffer = szLocal.GetBuffer(1);
												CSafeBufferUtil sbu;
												char* retbuf = sbu.Token(buffer, strlen(buffer), "|", MODE_SINGLEDELIM);
												szLocal.ReleaseBuffer();

												int nCmd = -1;
												if(retbuf)
												{
													nCmd = atoi(retbuf);
													retbuf = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
													if(retbuf)
													{
														szName = _T(retbuf);
														retbuf = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
														if(retbuf)
														{
															szValue = _T(retbuf);
														}
													}
												}
												
												int nReturn = RADIANCE_SUCCESS; 


												if((nCmd>=0)&&(nCmd<3)) // only supported commands right now
												{
													switch(nCmd)
													{
													case 0:// load project/scene
														{
															if((szName.GetLength())&&(szValue.GetLength()))
															{
																if(
																		(szName.Compare(pConn->m_pDlg->m_loadedproject))
																	||(szValue.Compare(pConn->m_pDlg->m_activescene))
																	)
																{
																	if(pConn->m_pDlg->m_bConnected)
																	{
																		pConn->m_pDlg->m_project = szName;
																		pConn->m_pDlg->m_scene = szValue;
																		pConn->m_pDlg->UpdateData( FALSE );
																		pConn->m_pDlg->OnLoad();
																	}
																	else
																	{
																		sprintf(errorstring, "Host not connected.");
																		nReturn = RADIANCE_ERROR;
																	}
																}
															}
															else
															{
																sprintf(errorstring, "Invalid arguments for load scene %s/%s.", szName, szValue);
																nReturn = RADIANCE_ERROR;
															}
														} break;
													case 1:// play anim
														{
															if(1)//(pConn->m_pDlg->m_bGotProjects)&&(pConn->m_pDlg->m_bGotScenes))
															{

																if(szName.GetLength())
																{
																	if(pConn->m_pDlg->m_bConnected)
																	{
																		if(pConn->m_pDlg->m_animsCombo.SelectString( -1, szName )!= CB_ERR)
																		{
																			pConn->m_pDlg->m_anims = szName;
																			pConn->m_pDlg->OnStartAnim();
																		}
																		else
																		{
																			sprintf(errorstring, "Unknown animation %s.", szName);
																			nReturn = RADIANCE_ERROR;
																		}
																	}
																	else
																	{
																		sprintf(errorstring, "Host not connected.");
																		nReturn = RADIANCE_ERROR;
																	}
																}
																else
																{
																	sprintf(errorstring, "Invalid argument for play anim %s.", szName);
																	nReturn = RADIANCE_ERROR;
																}
															}
															else
															{//stall it.
																bRemove = false;
															}
														} break;
													case 2:// set export
														{
															if(1)//(pConn->m_pDlg->m_bGotProjects)&&(pConn->m_pDlg->m_bGotScenes))
															{
																if(szName.GetLength())
																{
																	if(pConn->m_pDlg->m_bConnected)
																	{
																		if(pConn->m_pDlg->m_exportsCombo.SelectString( -1, szName )!= CB_ERR)
																		{
																			pConn->m_pDlg->m_exportVal = szValue;
																			pConn->m_pDlg->m_export = szName;
																			pConn->m_pDlg->UpdateData( FALSE );
																			pConn->m_pDlg->OnSendExport();
																		}
																		else
																		{
																			sprintf(errorstring, "Unknown export %s.", szName);
																			nReturn = RADIANCE_ERROR;
																		}
																	}
																	else
																	{
																		sprintf(errorstring, "Host not connected.");
																		nReturn = RADIANCE_ERROR;
																	}
																}
																else
																{
																	sprintf(errorstring, "Invalid arguments for set export [%s] -> [%s].", szName, szValue);
																	nReturn = RADIANCE_ERROR;
																}
															}
															else
															{//stall it.
																bRemove = false;
															}
														} break;
													} //switch nCMD

												}
												else
												{
													sprintf(errorstring, "The command %d was not in range." , nCmd);
													nReturn = RADIANCE_ERROR;
												}

												if(nReturn<RADIANCE_SUCCESS)
												{
													//**MSG
													g_pradiance->m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:recom", 
														"ReCOM command not successful on %s: %s", pchServer, errorstring );   Sleep(50);//(Dispatch message)

													if(nEventItemID>0)
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E256:ReCOM command not successful on %s. %s', action = 128, timestamp = 5000000000 WHERE itemid = %d;",  //HARDCODE
															((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
															pchServer, errorstring, nItemID
															);

	//				EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
														if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
														{
															//**MSG
	//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

														}
	//				LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);
														szServer.ReleaseBuffer();
					// do not return, keep checking Queue
	//										return RADIANCE_ERROR; // we dont want to remove the item at the end of this function
														bRemove = false;
														// we also dont want to remove the item at the end of this function
													}
												}
												else
												{
													//while(!g_miranda.m_bTransferring) Sleep(1);  //not good, in case error

	//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "%s on %s succeeded", szLocal, pchServer);  Sleep(250); //(Dispatch message)
													// we have queued the xfer , lets update the thing with a status, then exit without removing
													nReturn = 0;

													if(nEventItemID>0)
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I256:sent ReCOM command to %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
															pchServer, nItemID
															);

								/*
								Queue action IDs
		1 transfer file to Orad Box
		2 transfer file from Orad Box
		3 delete file from Orad Box
		4 refresh file listing of Orad Box
		19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
		32  file is transferring, check for end of transfer
		64  check for existence of file
		128 give results
		256 send telnet command to Orad Box
								*/
													
													
	//				EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
														if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
														{
															//**MSG
	//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

														}
	//				LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);
														bRemove = false;
													}
													szServer.ReleaseBuffer();
					// do not return, keep checking Queue
	//											return RADIANCE_ERROR; // we dont want to remove the item at the end of this function
												}
											} break;
										case 64:// check file on Orad box
											{
		/*
												char pchFileRemote[256];
												sprintf(pchFileRemote, "%s", szFilenameRemote);
			//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "*** checking %s on %s", pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)
												unsigned nSize, nTimestamp;

												int nReturn = g_miranda.OxSoxGetFileStats(pchFileRemote, NULL, &nSize, &nTimestamp);
												if((nReturn<OX_SUCCESS)||(nReturn&OX_NOFILE))
												{
													//**MSG
													g_pradiance->m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:oxsox_checkfile", 
														"%s not found on %s.", pchFileRemote, pchServer );   Sleep(50);//(Dispatch message)

													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E064:%s not found on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d;",  //HARDCODE
														((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
														pchFileRemote, pchServer, nItemID
														);
												}
												else
												{
													//while(!g_miranda.m_bTransferring) Sleep(1);  //not good, in case error

			//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "%s on %s succeeded", pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)
													// we have queued the xfer , lets update the thing with a status, then exit without removing
													
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I064:%s exists on %s.', filename_local = '%d|%d', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
														((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
														pchFileRemote, pchServer, nSize, nTimestamp, nItemID
														);

								/*
								Queue action IDs
		1 transfer file to Orad Box
		2 transfer file from Orad Box
		3 delete file from Orad Box
		4 refresh file listing of Orad Box
		19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
		32  file is transferring, check for end of transfer
		64  check for existence of file
		128 give results
		256 send telnet command to Orad Box
								* /
												}
													
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
			//										if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
												if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
									//		g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

												}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);

												szServer.ReleaseBuffer();
												return RADIANCE_ERROR; // we dont want to remove the item at the end of this function
												*/

											} break;
										case 128:// Give results (no action).
											{
												szServer.ReleaseBuffer();
					// do not return, keep checking Queue
	//										return RADIANCE_ERROR; // we dont want to remove the item at the end of this function
												bRemove = false;
												// the requestor process will remove this.
											} break;

										case 1:// transfer to Orad box
											{
		/*
												if(g_miranda.m_bTransferring) // would like to update queue item with % status.
												{
													// something in progress, must wait. -  just return negative for now
													szServer.ReleaseBuffer();
													return RADIANCE_ERROR;
												}

												char pchFileLocal[256];
												char pchFileRemote[256];
												sprintf(pchFileLocal, "%s", szFilenameLocal);
												sprintf(pchFileRemote, "%s", szFilenameRemote);
			//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "*** transferring %s to %s on %s", pchFileLocal, pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)

												if(g_miranda.OxSoxPutFile(pchFileLocal, pchFileRemote, NULL)<OX_SUCCESS)
												{
													//**MSG
													g_pradiance->m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:oxsox_putfile", 
														"Could not transfer %s to %s on %s.", pchFileLocal, pchFileRemote, pchServer );   Sleep(50);//(Dispatch message)

													// and give the info if it was external.
													if(nEventItemID>0)
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E001:Could not transfer %s to %s on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
																((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
																pchFileLocal, pchFileRemote, pchServer,
																nItemID
																);
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
														if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
														{
															//**MSG
						//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

														}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);
														szServer.ReleaseBuffer();
														return RADIANCE_ERROR;  // because we dont want to increment counter.  // there is no counter.
														// we also dont want to remove the item at the end of this function
													}
												}
												else
												{
													//while(!g_miranda.m_bTransferring) Sleep(1);  //not good, in case error

													Sleep(100); // let the thread begin transferring.
			//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "*** transfer of %s to %s on %s succeeded", pchFileLocal, pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)
													// we have queued the xfer , lets update the thing with a status, then exit without removing
													
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET action = 32, timestamp = -1.0, message = '1' WHERE itemid = %d",  //HARDCODE
														((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
														nItemID
														);

								/*
								Queue action IDs
		1 transfer file to Orad Box
		2 transfer file from Orad Box
		3 delete file from Orad Box
		4 refresh file listing of Orad Box
		19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
		32  file is transferring, check for end of transfer
		64  check for existence of file
		128 give results
		256 send telnet command to Orad Box

								* /

													
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
			//										if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
													if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
													{
														//**MSG
			//									g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

													}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);

													szServer.ReleaseBuffer();
													return RADIANCE_ERROR;  // because we dont want to increment counter.  // there is no counter.
													// we also dont want to remove the item at the end of this function
												}
												*/
											} break;
										case 2:// transfer from Orad box
											{
		/*
												if(g_miranda.m_bTransferring) // would like to update queue item with % status.
												{
													// something in progress, must wait. -  just return negative for now
													szServer.ReleaseBuffer();
													return RADIANCE_ERROR;
												}

												char pchFileLocal[256];
												char pchFileRemote[256];
												sprintf(pchFileLocal, "%s", szFilenameLocal);
												sprintf(pchFileRemote, "%s", szFilenameRemote);
												if(g_miranda.OxSoxGetFile(pchFileLocal, pchFileRemote, NULL/*, OX_REPORT* /)<OX_SUCCESS)
												{
													//**MSG
													g_pradiance->m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:oxsox_getfile", 
														"Could not transfer %s on %s to %s.", pchFileRemote, pchServer, pchFileLocal );   Sleep(50);//(Dispatch message)

													// and give the info if it was external.
													if(nEventItemID>0)
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E002:Could not transfer %s on %s to %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
																((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
																pchFileRemote, pchServer, pchFileLocal,
																nItemID
																);
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
														if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
														{
															//**MSG
						//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

														}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);
														szServer.ReleaseBuffer();
														return RADIANCE_ERROR;  // because we dont want to increment counter.  // there is no counter.
														// we also dont want to remove the item at the end of this function
													}

												}
												else
												{
													//while(!g_miranda.m_bTransferring) Sleep(1);  //not good, in case error

													Sleep(100); // let the thread begin transferring.
													// we have queued the xfer , lets update the thing with a status, then exit without removing
													
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET action = 32, timestamp = -1.0, message = '2' WHERE itemid = %d",  //HARDCODE
														((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
														nItemID
														);

								/*
		1 transfer file to Orad Box
		2 transfer file from Orad Box
		3 delete file from Orad Box
		4 refresh file listing of Orad Box
		19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
		32  file is transferring, check for end of transfer
		64  check for existence of file
		128 give results
		256 send telnet command to Orad Box
								* /

					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
													if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
													{
														//**MSG
									//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

													}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);

													szServer.ReleaseBuffer();
													return RADIANCE_ERROR;  // because we dont want to increment counter. // there is no counter.
													// we also dont want to remove the item at the end of this function
												}
												*/
											} break;
										case 3:// delete from Orad Box
											{
		/*
												if(g_miranda.m_bTransferring) // would like to update queue item with % status.
												{
													// something in progress, must wait. -  just return negative for now
													szServer.ReleaseBuffer();
													return RADIANCE_ERROR;
												}
												char pchFileRemote[256];
												sprintf(pchFileRemote, "%s", szFilenameRemote);
												if(g_miranda.OxSoxDelFile(pchFileRemote, NULL)<OX_SUCCESS)
												{
													//**MSG
													g_pradiance->m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:oxsox_delfile", 
														"Could not delete %s on %s.", pchFileRemote, pchServer );   Sleep(50);//(Dispatch message)
													if(nEventItemID>0)
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E003:Could not delete %s on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
																((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
																szFilenameRemote,
																pchServer, nItemID
																);
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
				//										if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
														if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
														{
															//**MSG
											//		g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

														}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);
														szServer.ReleaseBuffer();
														return RADIANCE_ERROR;  // because we dont want to increment counter.  // there is no counter.
														// we also dont want to remove the item at the end of this function

													}

												}
												else


				//								schedule a refresh
												if((g_pradiance->m_data.pConn->m_pszServerName)&&(strlen(g_pradiance->m_data.pConn->m_pszServerName)) && ((g_pradiance->m_data.pConn->m_ulFlags)&RADIANCE_FLAG_ENABLED)  )
												{
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (filename_local, filename_remote, action, host, timestamp, username) \
		VALUES ('','%s', 4, '%s', -2.0, 'sys' )", //HARDCODE
													((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
													pchDirAlias, g_pradiance->m_data.pConn->m_pszServerName
													);


					//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", szSQL);  Sleep(250); //(Dispatch message)
					//					char errorstring[DB_ERRORSTRING_LEN];
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
					//								if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
													if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
													{
														//**MSG
					//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

													}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);

													// and give the info if it was external.
													if(nEventItemID>0)
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I003:Finished deleting %s on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
																((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
																szFilenameRemote, 
																pchServer, nItemID
																);
														
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
				//										if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
														if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
														{
															//**MSG
											//		g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

														}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);
														szServer.ReleaseBuffer();
														return RADIANCE_ERROR;  // because we dont want to increment counter.  // there is no counter.
														// we also dont want to remove the item at the end of this function

													}
												}

		*/
											} break;
										case 32:// wait on transfer
											{

		/*
												if(g_miranda.m_bTransferring) // would like to update queue item with % status.
												{
													// something in progress, must wait. -  just return negative for now
													szServer.ReleaseBuffer();
													return RADIANCE_ERROR;
												}

												// if we are here, it's because we have finished a transfer!
				//								schedule a refresh
												if((g_pradiance->m_data.pConn->m_pszServerName)&&(strlen(g_pradiance->m_data.pConn->m_pszServerName)) && ((g_pradiance->m_data.pConn->m_ulFlags)&RADIANCE_FLAG_ENABLED)  )
												{
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (filename_local, filename_remote, action, host, timestamp, username) \
		VALUES ('','%s', 4, '%s', -2.0, 'sys' )", //HARDCODE
													((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
													pchDirAlias, g_pradiance->m_data.pConn->m_pszServerName
													);


					//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", szSQL);  Sleep(250); //(Dispatch message)
					//					char errorstring[DB_ERRORSTRING_LEN];
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
					//								if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
													if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
													{
														//**MSG
					//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

													}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);

													// and give the info if it was external.
													if(nEventItemID>0)
													{
														int nDirection = atoi(szMessage);
			//						case 2:// transfer from Miranda
			//						case 1:// transfer to Miranda
														if(g_miranda.m_bTransferError) //error!.
														{
															g_miranda.m_bTransferError = false; // reset the error so we can move on
															if(nDirection>0)
															{
																_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E032:Error transferring %s to %s%s%s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
																	((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
																	(nDirection==1)?szFilenameLocal:szFilenameRemote,
																	(nDirection==1)?szFilenameRemote:szFilenameLocal,
																	(nDirection==1)?" on ":" from ",
																	pchServer, nItemID
																	);
															}
															else
															{
																_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E032:Error transferring %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
																	((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
																	szFilenameRemote,
																	pchServer, nItemID
																	);

															}
														}
														else
														{
															if(nDirection>0)
															{
																_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I032:Finished transferring %s to %s%s%s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
																	((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
																	(nDirection==1)?szFilenameLocal:szFilenameRemote,
																	(nDirection==1)?szFilenameRemote:szFilenameLocal,
																	(nDirection==1)?" on ":" from ",
																	pchServer, nItemID
																	);
															}
															else
															{
																_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I032:Finished transferring %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
																	((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
																	szFilenameRemote,
																	pchServer, nItemID
																	);

															}
														}

			//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "Transfer SQL: %s", szSQL);  Sleep(250); //(Dispatch message)
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
			//										if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
														if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
														{
															//**MSG
			//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

														}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);
														szServer.ReleaseBuffer();
														return RADIANCE_ERROR;  // because we dont want to increment counter.  // there is no counter.
														// we also dont want to remove the item at the end of this function

													}
												}
		*/
											} break;
										case 19:// purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
											{
											}  // not break;  want to go on to refresh device listing.
										case 4:// refresh device listing
											{
			//create table Destinations_Media (host varchar(64), file_name varchar(256), 
			//transfer_date int, partition varchar(16), file_size float, 
			//Direct_local_last_used int, Direct_local_times_used int);
		/*
												// have to do this 3 times, possibly.
			//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "case 4");   Sleep(50);//(Dispatch message)
												int nTimes = 3;
												int nDirs = 0;

												if(szFilenameRemote.GetLength()>0)
												{ // explicit dir
													if(szFilenameRemote.Compare("$VIDEO")==0)
													{
														nDirs = 0; nTimes=1;
													}
													else
													if(szFilenameRemote.Compare("$AUDIO")==0)
													{
														nDirs = 1; nTimes=2; // not really 2 times, just have to be nDirs = nTimes-1;
													}
													else
													if(szFilenameRemote.Compare("$FONTS")==0)
													{
														nDirs = 2; nTimes=3; // not really 3 times, just have to be nDirs = nTimes-1;
													}
												}

												for (int nDir=nDirs; nDir<nTimes; nDir++)
												{
				_ftime( &g_pradiance->m_data.m_timebTick );
													PV3DirEntry_ pv3DirEntry;
													int nNumEntries = 0;

													switch(nDir)
													{
													default:
													case 0:	strcpy(pchDirAlias, "$VIDEO"); break;
													case 1:	strcpy(pchDirAlias, "$AUDIO"); break;
													case 2:	strcpy(pchDirAlias, "$FONTS"); break;
													}
													
													if(g_miranda.OxSoxGetDirInfo(pchDirAlias, &pv3DirEntry, &nNumEntries)<OX_SUCCESS)
													{
														//**MSG
														g_pradiance->m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:oxsox_dirinfo", 
															"Could not get directory information for %s partition on %s.", pchDirAlias, pchServer );   Sleep(50);//(Dispatch message)

														// and give the info if it was external.
														if(nEventItemID>0)
														{
															_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E004:Could not get directory information for %s partition on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
																	((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
																	pchDirAlias, pchServer,
																	nItemID
																	);
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
															if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
															{
																//**MSG
			//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

															}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);
															szServer.ReleaseBuffer();
															return RADIANCE_ERROR;  // because we dont want to increment counter.  // there is no counter.
															// we also dont want to remove the item at the end of this function
														}
													}
													else
													{

														// success, lets update db.
			//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "OxSoxGetDirInfo returned >= OX_SUCCESS" );   Sleep(50);//(Dispatch message)


			// the old way was m_pszDestinationMedia.  now we merge on temp table m_pszDestinationMediaTemp
														// first remove everything from the db.  clear out temp db
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
		host = '%s' AND \
		partition = '%s'",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																pchServer,
																pchDirAlias

															);
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
														if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
														{
															//**MSG
														}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);


														int nFile = 0;
														while(nFile<nNumEntries)
														{
					_ftime( &g_pradiance->m_data.m_timebTick );

															char* pchFilename = pdb->EncodeQuotes(pv3DirEntry[nFile].FileName);
																// add it.
																_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
		host, \
		file_name, \
		transfer_date, \
		partition, \
		file_size) VALUES ('%s', '%s', %d, '%s', %f)",   //HARDCODE
																	((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																	pchServer,
																	pchFilename?pchFilename:pv3DirEntry[nFile].FileName,											
																	pv3DirEntry[nFile].FileTimeStamp,
																	pchDirAlias,
																	(double)pv3DirEntry[nFile].FileSize
																	);

															// }

															if(pchFilename) free(pchFilename);

					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
															if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
															{
																//**MSG
															}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);

				/*
															pv3DirEntry[nFile].FileName
															pv3DirEntry[nFile].FileSize,
															pv3DirEntry[nFile].FileAttribs,
															pv3DirEntry[nFile].FileTimeStamp
				* /
															nFile++;
														}
														if(g_miranda.m_lpfnFreeOxSoxPtr) g_miranda.m_lpfnFreeOxSoxPtr(pv3DirEntry);

														// now everything is in the temp table,
														// update the files with info from the temp table


														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
		%s.transfer_date = %s.transfer_date, \
		%s.file_size = %s.file_size \
		FROM %s, %s \
		WHERE %s.host = '%s' AND \
		%s.file_name = %s.file_name \
		AND %s.partition = %s.partition",
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																pchServer,
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp"
																);

		//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "Updating from temp SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
														if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
														{
															//**MSG
		//g_pradiance->m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:debug", "Updating from temp ERROR: %s", dberrorstring);  Sleep(50); //(Dispatch message)
														}

					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);

														// insert the ones that are in the temp list that are not in the main media list.
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (host, file_name, transfer_date, partition, file_size, direct_local_last_used, direct_local_times_used) \
		SELECT %s.host, %s.file_name, %s.transfer_date, %s.partition, %s.file_size, 0, 0 FROM \
		%s LEFT JOIN (SELECT * FROM %s WHERE host = '%s') AS %s on %s.file_name = %s.file_name and %s.partition = %s.partition WHERE %s.file_name is NULL",
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																pchServer,
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media"
																);

		//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "Updating from temp SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
														if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
														{
															//**MSG
		//g_pradiance->m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:debug", "Updating from temp ERROR: %s", dberrorstring);  Sleep(50); //(Dispatch message)
														}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);


														// remove the ones that are no longer in the temp list
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
		%s.host = '%s' AND \
		%s.partition = '%s' AND \
		cast(%s.file_name AS varchar(256)) + ' ' + cast(%s.partition AS varchar(16)) \
		NOT IN (SELECT cast(%s.file_name as varchar(256)) + ' ' + cast(%s.partition AS varchar(16)) \
		AS fileinfo from %s where host = '%s' and %s.partition = '%s')",
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																pchServer,
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																pchDirAlias,
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																((g_pradiance->m_settings.m_pszDestinationMedia)&&(strlen(g_pradiance->m_settings.m_pszDestinationMedia)))?g_pradiance->m_settings.m_pszDestinationMedia:"Destinations_Media",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																pchServer,
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																pchDirAlias
																);


		//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "Delete extras from media SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
														if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
														{
															//**MSG
		//g_pradiance->m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:debug", "Delete extras from media ERROR: %s", dberrorstring);  Sleep(50); //(Dispatch message)
														}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);



			// clear the temp table
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
		host = '%s' AND \
		partition = '%s'",
																((g_pradiance->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pradiance->m_settings.m_pszDestinationMediaTemp)))?g_pradiance->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
																pchServer,
																pchDirAlias

															);
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
														if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
														{
															//**MSG
														}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);



														// and give the info if it was external.
														if(nEventItemID>0)
														{
															_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I004:Retrieved directory information for %s partition on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
																	((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
																	pchDirAlias, pchServer,
																	nItemID
																	);
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
															if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
															{
																//**MSG
							//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

															}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);
															szServer.ReleaseBuffer();
															return RADIANCE_ERROR;  // because we dont want to increment counter.  // there is no counter.
															// we also dont want to remove the item at the end of this function

														}
													}
												}
												*/
											} break;
										}
										_ftime( &g_pradiance->m_data.m_timebTick );
									}
		/*
									else  // could not ping it for some reason.  Have to error out and delete the thing thing
									{

										g_pradiance->m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:oxsox_ping", 
											"Could not ping %s.  The request cannot be completed.", pchServer );   Sleep(50);//(Dispatch message)

										// and give the info if it was external.
										if(nEventItemID>0)
										{
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E%03d:Could not ping %s.  The request cannot be completed.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
													((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
													nAction, pchServer,
													nItemID
													);
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
											if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
											{
												//**MSG
			//			g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

											}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);
											//return RADIANCE_ERROR;  // because we dont want to increment counter.  // there is no counter.
											// we DO want to remove the item at the end of this function
										}
									}
									*/
								}
								else
								{
									//**MSG
									g_pradiance->m_msgr.DM(MSG_ICONERROR, NULL, "Radiance:connect_host", "Could not set host to %s", pchServer );   Sleep(50);//(Dispatch message)


		/*
									m_bFailed = true;
									m_timebFailure;

									// lets fail this one out for the failure retry time.
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E%03d:%s is unavailable or inactive, retrying after %d seconds.', timestamp = %d WHERE itemid = %d",  //HARDCODE
											((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
											nAction, pchServer,
											nItemID
											);
					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
		//										if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
									if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
									{
										//**MSG
						//		g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

									}
					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);

		*/

									szServer.ReleaseBuffer();
					// do not return, keep checking Queue
	//								return RADIANCE_ERROR; // we dont want to remove the item at the end of this function

			//	g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "getting channels again");   Sleep(250);//(Dispatch message)
								}
							}
							else
							{
								//**MSG

								switch(nAction)
								{
								default:
									{
										g_pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance:Queue", "An event was queued for %s, but that destination is unavailable or inactive.  The event will be ignored and purged from the queue.", pchServer );   Sleep(50);//(Dispatch message)
									} break;  
								case 2:// transfer from Orad box
								case 1:// transfer to Orad box
								case 3:// DELETE FROM Orad box
								case 64:// check file on Orad box
								case 256:// send command to Orad box
									{
										g_pradiance->m_msgr.DM(MSG_ICONINFO, NULL, "Radiance:Queue", "An event was queued for %s, but that destination is unavailable or inactive.  The event will be ignored and purged from the queue.", pchServer );   Sleep(50);//(Dispatch message)

										if(nEventItemID>0) // means, commanded from external, so give info
										{
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E%03d:%s is unavailable or inactive.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
													((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
													nAction, pchServer,
													nItemID
													);
	//				EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
											if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
											{
												//**MSG
								//		g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

											}
	//				LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);
											szServer.ReleaseBuffer();
					// do not return, keep checking Queue
	//										return RADIANCE_ERROR; // we dont want to remove the item at the end of this function
										}
									} break;
								}
							}

							szServer.ReleaseBuffer();
						}

						if(bRemove)
						{
							// send remove SQL
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE itemid = %d",  //HARDCODE
								((g_pradiance->m_settings.m_pszCommandQueue)&&(strlen(g_pradiance->m_settings.m_pszCommandQueue)))?g_pradiance->m_settings.m_pszCommandQueue:"Command_Queue",
								nItemID
								);

		/*
		Queue action IDs
		1 transfer to Miranda
		2 transfer from Miranda
		3 delete from Miranda
		4 refresh device listing
		19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
		32 wait on transfer

		Queue table:
		//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
		//action int, host varchar(64), timestamp float, username varchar(32));
		*/
							if((pdbConnDel)&&(pdbDel))
							{

//g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "SQL: %s", szSQL);  Sleep(250); //(Dispatch message)
	//					EnterCriticalSection(&g_pradiance->m_data.m_critSQL);
								if(pdb->ExecuteSQL(pdbConnDel, szSQL, errorstring)<DB_SUCCESS)
								{
									//**MSG
g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

								}
//					LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);
							}
						}

					} //if((nReturn>0)&&(bItemFound))

					nIndex++;
					prs->MoveNext();
				} //				while ((!prs->IsEOF()))


				nReturn = nIndex;
				prs->Close();

				delete prs;

				// return nReturn;  // do not return, keep checking Queue
			} //if(prs)
//			LeaveCriticalSection(&g_pradiance->m_data.m_critSQL);

		}//if((g_pradiance)&&(pdbConn)&&(pdb))
		
//		ulTick = GetTickCount();
	}//if license
		Sleep(1);
	} // while not kill

	g_pradiance->m_msgr.DM(MSG_ICONHAND, NULL, "Radiance:connection", "Command management thread for %s (%s) has ended.",
											pConn->m_pDlg->m_reName, //server_name
											pConn->m_pszDesc
		);   Sleep(100); //(Dispatch message)
	pConn->m_bConnThreadStarted = false;
//	Sleep(500);
	_endthread();
//	Sleep(500);

}


