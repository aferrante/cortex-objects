; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CRadianceDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Radiance.h"
LastPage=0

ClassCount=5
Class1=CAboutDlg
Class2=CRadianceApp
Class3=CRadianceHandler
Class4=CRadianceDlg
Class5=CRadianceSettings

ResourceCount=4
Resource1=IDR_MENU1
Resource2=IDD_RADIANCE_DIALOG
Resource3=IDD_ABOUTBOX
Resource4=IDD_RECOMCONNECT_DIALOG

[CLS:CAboutDlg]
Type=0
HeaderFile=sentineldlg.h
ImplementationFile=sentineldlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CAboutDlg

[CLS:CRadianceApp]
Type=0
BaseClass=CWinApp
HeaderFile=Radiance.h
ImplementationFile=Radiance.cpp
Filter=N
VirtualFilter=AC
LastObject=CRadianceApp

[CLS:CRadianceHandler]
Type=0
BaseClass=CWnd
HeaderFile=RadianceHandler.h
ImplementationFile=RadianceHandler.cpp
Filter=W
VirtualFilter=WC
LastObject=CRadianceHandler

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=8
Control1=IDC_STATICTEXT_TITLE,static,1342308481
Control2=IDC_STATICTEXT_URL,static,1208090625
Control3=IDOK,button,1342373889
Control4=IDC_STATIC_LOGO,static,1342177294
Control5=IDC_URLFRAME,static,1342177298
Control6=IDC_STATIC_BUILD,static,1342308353
Control7=IDC_STATIC_URL,static,1342177294
Control8=IDC_STATICTEXT_COPYRIGHT,static,1342308353

[MNU:IDR_MENU1]
Type=1
Class=?
Command1=ID_CMD_ABOUT
Command2=ID_CMD_SHOWWND
Command3=ID_CMD_EXIT
Command4=ID_CMD_ABOUT
Command5=ID_CMD_SHOWWND
Command6=ID_CMD_EXIT
CommandCount=6

[CLS:CRadianceDlg]
Type=0
HeaderFile=RadianceDlg.h
ImplementationFile=RadianceDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_LIST1

[CLS:CRadianceSettings]
Type=0
HeaderFile=RadianceSettings.h
ImplementationFile=RadianceSettings.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CRadianceSettings

[DLG:IDD_RADIANCE_DIALOG]
Type=1
Class=CRadianceDlg
ControlCount=7
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_SETTINGS,button,1208025216
Control4=IDC_LIST1,SysListView32,1350681613
Control5=IDC_STATIC_STATUSTEXT,static,1342308352
Control6=IDC_STATIC_PROGBAR,static,1342177294
Control7=IDC_TREE1,SysTreeView32,1082130487

[DLG:IDD_RECOMCONNECT_DIALOG]
Type=1
ControlCount=34
Control1=IDDISCONNECT,button,1476460544
Control2=IDC_BUTTON_CONNECT,button,1476460544
Control3=IDC_EDIT_OUTPUT,edit,1219565636
Control4=IDC_EDIT_SEND,edit,1216417920
Control5=IDC_BUTTON_SEND,button,1208025088
Control6=IDC_EDIT_RE_HOSTNAME,edit,1216417920
Control7=IDC_STATIC,static,1208090624
Control8=IDC_RECOM,{A660922A-18D7-45EB-AAA1-9A4DD783F4CD},1342242816
Control9=IDC_STATIC,static,1342308866
Control10=IDC_STATIC,static,1342308866
Control11=IDC_LOAD,button,1476460544
Control12=IDC_STATIC,static,1342308866
Control13=IDC_STATIC,static,1342308866
Control14=IDC_EXPORT_VAL,edit,1484849280
Control15=IDC_SEND_EXPORT,button,1476460544
Control16=IDC_STATIC,static,1342308866
Control17=IDC_START_ANIM,button,1476460544
Control18=IDC_STATIC,static,1208090624
Control19=IDC_EXPORT_COMBO,combobox,1478557954
Control20=IDC_PROJECT_COMBO,combobox,1478557954
Control21=IDC_SCENE_COMBO,combobox,1478557954
Control22=IDC_STATIC,static,1342308866
Control23=IDC_UNLOAD,button,1476460544
Control24=IDC_SCENES_COMBO,combobox,1478557954
Control25=IDC_UNLOADALL,button,1476460544
Control26=IDC_ACTIVATE,button,1476460544
Control27=IDC_ANIMS_COMBO,combobox,1478557954
Control28=IDC_STOP_ANIM,button,1476460544
Control29=IDC_BUTTON_MANUAL,button,1342242944
Control30=IDC_STATIC_STATUS,static,1342308864
Control31=IDC_STATIC_MANUAL,static,1342177296
Control32=IDC_STATIC_CONTRACTED,static,1207959570
Control33=IDOK,button,1073807361
Control34=IDCANCEL,button,1073807360

