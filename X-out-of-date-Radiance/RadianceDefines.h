// RadianceDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(RADIANCEDEFINES_H_INCLUDED)
#define RADIANCEDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define RADIANCE_CURRENT_VERSION		"1.0.3.2"


// modes
#define RADIANCE_MODE_DEFAULT			0x00000000  // exclusive
#define RADIANCE_MODE_LISTENER		0x00000001  // exclusive
#define RADIANCE_MODE_CLONE				0x00000002  // exclusive
#define RADIANCE_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define RADIANCE_MODE_VOLATILE		0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define RADIANCE_MODE_MASK				0x0000000f  // 

// default port values.
//#define RADIANCE_PORT_FILE				80		
#define RADIANCE_PORT_CMD					10682		
#define RADIANCE_PORT_STATUS			10683		

#define RADIANCE_PORT_INVALID			0	
#define RADIANCE_PORT_RECOM				9600	

#define RADIANCE_TYPE_DVG					0x0000	 // default
#define RADIANCE_TYPE_HDVG				0x0001	

#define RADIANCE_FLAG_SD					0x0000	 // default
#define RADIANCE_FLAG_HD					0x0010	
#define RADIANCE_FLAG_DISABLED		0x0000	 // default
#define RADIANCE_FLAG_ENABLED			0x0001	
#define RADIANCE_FLAG_FOUND				0x1000

#define RADIANCE_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded


// status
#define RADIANCE_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define RADIANCE_STATUS_UNKNOWN							0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define RADIANCE_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define RADIANCE_STATUS_ERROR								0x00000020  // error (red icon)
#define RADIANCE_STATUS_CONN								0x00000030  // ready (green icon)	
#define RADIANCE_STATUS_OK									0x00000030  // ready (green icon)	
#define RADIANCE_STATUS_RUN									0x00000040  // in progress, running, owned etc (blue icon);	
#define RADIANCE_ICON_MASK									0x00000070  // mask	

#define RADIANCE_STATUS_SUSPEND							0x00000080  // suspended	(yellow icon please)

#define RADIANCE_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define RADIANCE_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define RADIANCE_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define RADIANCE_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define RADIANCE_STATUS_CMDSVR_MASK					0x00007000  // command server mask bits

#define RADIANCE_STATUS_STATUSSVR_START			0x00010000  // starting the status server
#define RADIANCE_STATUS_STATUSSVR_RUN				0x00020000  // status server running
#define RADIANCE_STATUS_STATUSSVR_END				0x00030000  // status server shutting down
#define RADIANCE_STATUS_STATUSSVR_ERROR			0x00040000  // status server error
#define RADIANCE_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define RADIANCE_STATUS_THREAD_START				0x00100000  // starting the main thread
#define RADIANCE_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define RADIANCE_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define RADIANCE_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define RADIANCE_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define RADIANCE_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define RADIANCE_STATUS_THREAD_MASK					0x00f00000  // main thread mask bits

// various failures...
#define RADIANCE_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define RADIANCE_STATUS_FAIL_DB							0x20000000  // could not get DB
#define RADIANCE_STATUS_FAIL_MASK						0xf0000000  // failure code mask bits

//return values
#define RADIANCE_SUCCESS   0
#define RADIANCE_ERROR	   -1


// default filenames
#define RADIANCE_SETTINGS_FILE_SETTINGS	  "radiance.csr"		// csr = cortex settings redirect
#define RADIANCE_SETTINGS_FILE_DEFAULT	  "radiance.csf"		// csf = cortex settings file

// client module commands
#define RADIANCE_CMD_BYE						  0x0f  // command module to shut down.
#define RADIANCE_CMD_PING							0xaa  // just check that its there..

#define RADIANCE_CMD_LOADSCENE				0xb0  // load a project/scene
#define RADIANCE_CMD_PLAYANIM				  0xb1  // play an anim
#define RADIANCE_CMD_SETEXPORT				0xb2  // set an export


#endif // !defined(RADIANCEDEFINES_H_INCLUDED)
