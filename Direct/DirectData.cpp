// DirectData.cpp: implementation of the CDirectData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <process.h>
#include "Direct.h"  //DiReCT header
#include "DirectHandler.h" 
#include "DirectMain.h" 
#include "DirectData.h"
#include "..\Archivist\ArchivistDefines.h"
#include <direct.h> // directory header
 
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CDirectMain* g_pdirect;
extern CDirectApp theApp;
//extern CADC g_adc; 	// the Harris ADC object

#define USE_CHANNEL_DEST_VIEW
#define USE_FILE_MAPS_IN_MEM

/*
//////////////////////////////////////////////////////////////////////
// CDirectConnectionObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirectConnectionObject::CDirectConnectionObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_pszClientName = NULL;
	m_ulStatus	= DIRECT_STATUS_UNINIT;
	m_ulFlags = DIRECT_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
//	m_pAPIConn = NULL;
	m_bKillConnThread = true;
	m_bConnThreadStarted = false;

}

CDirectConnectionObject::~CDirectConnectionObject()
{
	m_bKillConnThread = true;
	while(	m_bConnThreadStarted ) Sleep(1);

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
	if(m_pszClientName) free(m_pszClientName); // must use malloc to allocate
}

*/


//////////////////////////////////////////////////////////////////////
// CDestinationMediaObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDestinationMediaObject::CDestinationMediaObject()
{
	m_sz_vc256_file_name;
	m_sz_n_transfer_date; // actually timestamp
	m_sz_vc16_partition;
	m_sz_dbl_file_size;
	m_sz_n_Direct_local_last_used;
	m_sz_n_Direct_local_times_used;
}

CDestinationMediaObject::~CDestinationMediaObject()
{
	if(m_sz_vc256_file_name) free(m_sz_vc256_file_name); // must use malloc to allocate
	if(m_sz_n_transfer_date) free(m_sz_n_transfer_date); // must use malloc to allocate
	if(m_sz_vc16_partition) free(m_sz_vc16_partition); // must use malloc to allocate
	if(m_sz_dbl_file_size) free(m_sz_dbl_file_size); // must use malloc to allocate
	if(m_sz_n_Direct_local_last_used) free(m_sz_n_Direct_local_last_used); // must use malloc to allocate
	if(m_sz_n_Direct_local_times_used) free(m_sz_n_Direct_local_times_used); // must use malloc to allocate
}

int CDestinationMediaObject::SetField(char** ppszField, char* pszData)
{
	if(ppszField)
	{
		if(*ppszField)
		{
			free(*ppszField);
			*ppszField=NULL;
		}
		if(pszData==NULL)
		{
			return DIRECT_SUCCESS;
		}
		char* pch = (char*) malloc(strlen(pszData)+1);
		if(pch)
		{
			strcpy(pch, pszData);
			*ppszField=pch;
			return DIRECT_SUCCESS;
		}
	}
	return DIRECT_ERROR;
}



//////////////////////////////////////////////////////////////////////
// CFileMetaDataObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFileMetaDataObject::CFileMetaDataObject()
{
	m_bUnique=true;  // false if more than one filename returned.
	m_sz_vc256_sys_filename = NULL;
	m_sz_vc256_sys_filepath = NULL;
//	m_sz_vc256_sys_linked_file = NULL;
	m_sz_vc256_sys_description = NULL;
	m_sz_vc64_sys_operator = NULL;
	m_sz_n_sys_type = NULL;
	m_sz_n_sys_duration = NULL;
	m_sz_n_sys_valid_from = NULL;
	m_sz_n_sys_expires_after = NULL;
	m_sz_n_sys_ingest_date = NULL;
	m_sz_n_sys_file_flags = NULL;
	m_sz_dbl_sys_file_size = NULL;
	m_sz_dbl_sys_file_timestamp=NULL;
	m_sz_n_sys_created_on = NULL;
	m_sz_vc32_sys_created_by = NULL;
	m_sz_n_sys_last_modified_on = NULL;
	m_sz_vc32_sys_last_modified_by = NULL;
	m_sz_n_direct_last_used = NULL;
	m_sz_n_direct_times_used = NULL;
}

CFileMetaDataObject::~CFileMetaDataObject()
{
	if(m_sz_vc256_sys_filename) free(m_sz_vc256_sys_filename); // must use malloc to allocate
	if(m_sz_vc256_sys_filepath) free(m_sz_vc256_sys_filepath); // must use malloc to allocate
//	if(m_sz_vc256_sys_linked_file) free(m_sz_vc256_sys_linked_file); // must use malloc to allocate
	if(m_sz_vc256_sys_description) free(m_sz_vc256_sys_description); // must use malloc to allocate
	if(m_sz_vc64_sys_operator) free(m_sz_vc64_sys_operator); // must use malloc to allocate
	if(m_sz_n_sys_type) free(m_sz_n_sys_type); // must use malloc to allocate
	if(m_sz_n_sys_duration) free(m_sz_n_sys_duration); // must use malloc to allocate
	if(m_sz_n_sys_valid_from) free(m_sz_n_sys_valid_from); // must use malloc to allocate
	if(m_sz_n_sys_expires_after) free(m_sz_n_sys_expires_after); // must use malloc to allocate
	if(m_sz_n_sys_ingest_date) free(m_sz_n_sys_ingest_date); // must use malloc to allocate
	if(m_sz_n_sys_file_flags) free(m_sz_n_sys_file_flags); // must use malloc to allocate
	if(m_sz_dbl_sys_file_size) free(m_sz_dbl_sys_file_size); // must use malloc to allocate
	if(m_sz_dbl_sys_file_timestamp) free(m_sz_dbl_sys_file_timestamp); // must use malloc to allocate
	if(m_sz_n_sys_created_on) free(m_sz_n_sys_created_on); // must use malloc to allocate
	if(m_sz_vc32_sys_created_by) free(m_sz_vc32_sys_created_by); // must use malloc to allocate
	if(m_sz_n_sys_last_modified_on) free(m_sz_n_sys_last_modified_on); // must use malloc to allocate
	if(m_sz_vc32_sys_last_modified_by) free(m_sz_vc32_sys_last_modified_by); // must use malloc to allocate
	if(m_sz_n_direct_last_used) free(m_sz_n_direct_last_used); // must use malloc to allocate
	if(m_sz_n_direct_times_used) free(m_sz_n_direct_times_used); // must use malloc to allocate
}

int CFileMetaDataObject::SetField(char** ppszField, char* pszData)
{
	if(ppszField)
	{
		if(*ppszField)
		{
			free(*ppszField);
			*ppszField=NULL;
		}
		if(pszData==NULL)
		{
			return DIRECT_SUCCESS;
		}
		char* pch = (char*) malloc(strlen(pszData)+1);
		if(pch)
		{
			strcpy(pch, pszData);
			*ppszField=pch;
			return DIRECT_SUCCESS;
		}
	}
	return DIRECT_ERROR;
}


int CFileMetaDataObject::UpdateMetadata(char* pszInfo) // increments direct_last_used and direct_times_used
{
	_ftime( &g_pdirect->m_data.m_timebTick );  // we're still alive.
	if((g_pdirect->m_data.m_nIndexMetadataEndpoint>=0)
		&&(g_pdirect->m_settings.m_ppEndpointObject)
		&&(g_pdirect->m_settings.m_ppEndpointObject[g_pdirect->m_data.m_nIndexMetadataEndpoint])
		&&(m_sz_n_direct_times_used)
		&&(m_sz_vc256_sys_filename)
		&&(m_sz_vc256_sys_filepath)  // used for uniqueness.
		&&(g_pdirect->m_data.m_pdb)&&(g_pdirect->m_data.m_pdbConn))
	{

		int nTimesUsed = atol(m_sz_n_direct_times_used);

		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET direct_last_used = %d, direct_times_used = %d \
WHERE sys_filename = '%s' AND sys_filepath = '%s'",
			g_pdirect->m_settings.m_ppEndpointObject[g_pdirect->m_data.m_nIndexMetadataEndpoint]->m_pszDBName?g_pdirect->m_settings.m_ppEndpointObject[g_pdirect->m_data.m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
			g_pdirect->m_settings.m_ppEndpointObject[g_pdirect->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata?g_pdirect->m_settings.m_ppEndpointObject[g_pdirect->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the LiveEvents table name
				g_pdirect->m_data.m_timebTick.time,
				nTimesUsed,
				m_sz_vc256_sys_filename,
				m_sz_vc256_sys_filepath
			);

//		g_pdirect->m_data.ReleaseRecordSet();
		EnterCriticalSection(&g_pdirect->m_data.m_critSQL);
		if(g_pdirect->m_data.m_pdb->ExecuteSQL(g_pdirect->m_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
		//**MSG
g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "ERROR executing SQL: %s", errorstring); 
			LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);
			return DIRECT_ERROR;
		}
		LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);
		return DIRECT_SUCCESS;
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return DIRECT_ERROR;

}

int CFileMetaDataObject::UpdateDestinationMetadata(int nEndpointIndex, char* pszHost, char* pszInfo)  // increments Direct_local_last_used and Direct_local_times_used in Endpoint Destinations_Media table
{
	_ftime( &g_pdirect->m_data.m_timebTick );  // we're still alive.
	if((nEndpointIndex>=0)
		&&(g_pdirect->m_settings.m_ppEndpointObject)
		&&(g_pdirect->m_settings.m_ppEndpointObject[g_pdirect->m_data.m_nIndexMetadataEndpoint])
		&&(pszHost)&&(strlen(pszHost))
		&&(m_sz_vc256_sys_filename)
		&&(g_pdirect->m_data.m_pdb)&&(g_pdirect->m_data.m_pdbConn))
	{
		int nTimesUsed = atol(m_sz_n_direct_times_used);  ///check if there is a query we can autoincrement.

		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET Direct_local_last_used = %d, Direct_local_times_used = %d \
WHERE sys_filename = '%s' AND host = '%s'",
			g_pdirect->m_settings.m_ppEndpointObject[g_pdirect->m_data.m_nIndexMetadataEndpoint]->m_pszDBName?g_pdirect->m_settings.m_ppEndpointObject[g_pdirect->m_data.m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
			g_pdirect->m_settings.m_ppEndpointObject[g_pdirect->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata?g_pdirect->m_settings.m_ppEndpointObject[g_pdirect->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the LiveEvents table name
				g_pdirect->m_data.m_timebTick.time,
				nTimesUsed,
				m_sz_vc256_sys_filename,
				pszHost
			);

//		g_pdirect->m_data.ReleaseRecordSet();
		EnterCriticalSection(&g_pdirect->m_data.m_critSQL);
		if(g_pdirect->m_data.m_pdb->ExecuteSQL(g_pdirect->m_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
		//**MSG
g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)
			LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);
			return DIRECT_ERROR;
		}
		LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);
		return DIRECT_SUCCESS;
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return DIRECT_ERROR;

}


//////////////////////////////////////////////////////////////////////
// CDirectQueueObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirectQueueObject::CDirectQueueObject()
{
	m_nItemID=-1;
	m_pszFilenameLocal=NULL;
	m_pszFilenameRemote=NULL;
	m_nActionID = -1;
	m_pszHost = NULL;
	m_dblTimestamp=-1.0;   // timestamp
	m_pszUsername = NULL;
	m_nEventItemID=-1;
	m_pszMessage = NULL;
}

CDirectQueueObject::~CDirectQueueObject()
{
	if(m_pszFilenameLocal) free(m_pszFilenameLocal); // must use malloc to allocate
	if(m_pszFilenameRemote) free(m_pszFilenameRemote); // must use malloc to allocate
	if(m_pszHost) free(m_pszHost); // must use malloc to allocate
	if(m_pszUsername) free(m_pszUsername); // must use malloc to allocate
	if(m_pszMessage) free(m_pszMessage); // must use malloc to allocate
}

int CDirectQueueObject::SetField(char** ppszField, char* pszData)
{
	if(ppszField)
	{
		if(*ppszField)
		{
			free(*ppszField);
			*ppszField=NULL;
		}
		if(pszData==NULL)
		{
			return DIRECT_SUCCESS;
		}
		char* pch = (char*) malloc(strlen(pszData)+1);
		if(pch)
		{
			strcpy(pch, pszData);
			*ppszField=pch;
			return DIRECT_SUCCESS;
		}
	}
	return DIRECT_ERROR;
}


//////////////////////////////////////////////////////////////////////
// CDirectRulesObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirectRulesObject::CDirectRulesObject()
{
	m_ulStatus	= DIRECT_STATUS_UNINIT;
	m_ulFlags = DIRECT_FLAG_DISABLED;  // various states
	m_ulType = DIRECT_RULE_TYPE_UNKNOWN;
	m_usComparisonType = DIRECT_RULE_COMPARE_UNKNOWN;
	m_ulDestinationType = CX_DESTTYPE_UNKNOWN;
	m_usSearchType = DIRECT_RULE_SEARCH_UNKNOWN;
	m_usActionType = DIRECT_RULE_ACTION_UNKNOWN;
	m_nRuleID  =-1;
	m_pszFieldName = NULL;
	m_pszParamName = NULL;
	m_pszCriterion = NULL;
	m_pszEventLocation = NULL;
}

CDirectRulesObject::~CDirectRulesObject()
{
	if(m_pszFieldName) free(m_pszFieldName); // must use malloc to allocate
	if(m_pszParamName) free(m_pszParamName); // must use malloc to allocate
	if(m_pszCriterion) free(m_pszCriterion); // must use malloc to allocate
	if(m_pszEventLocation) free(m_pszEventLocation); // must use malloc to allocate
}





//////////////////////////////////////////////////////////////////////
// CDirectChannelDestinationObject Construction/Destruction
//////////////////////////////////////////////////////////////////////
CDirectChannelDestinationObject::CDirectChannelDestinationObject()
{
	m_ulDestFlags = DIRECT_FLAG_DISABLED;
	m_ulDestType = DIRECT_RULE_TYPE_UNKNOWN;
	m_ulFlags = DIRECT_FLAG_DISABLED;  // various states
}

CDirectChannelDestinationObject::~CDirectChannelDestinationObject()
{
}


//////////////////////////////////////////////////////////////////////
// CDirectEventObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirectEventObject::CDirectEventObject()
{
	m_ppszChildren=NULL;
	m_pnChildrenStatus = NULL;
	m_nNumChildren = 0;
	m_nCurrentFileSearch = -1;
	m_nMainFileStatus = -1;
	m_pszMainFile = NULL;
}

CDirectEventObject::~CDirectEventObject()
{
	if(m_ppszChildren)
	{
		int i=0;
		while(i<m_nNumChildren)
		{
			if(m_ppszChildren[i]) free(m_ppszChildren[i]);
			i++;
		}
		delete [] m_ppszChildren;
	}
	if(m_pnChildrenStatus) delete [] m_pnChildrenStatus;
	if(m_pszMainFile) free(m_pszMainFile);
}

int CDirectEventObject::AddChild(char* pszChild, int nStatus)
{
	if((pszChild)&&(strlen(pszChild)))
	{
		char** ppszChildren = new char*[m_nNumChildren+1];
		if(ppszChildren)
		{
			int*  pnChildrenStatus = new int[m_nNumChildren+1];
			if(pnChildrenStatus)
			{
				if(m_ppszChildren)
				{
					int n=0;
					while(n<m_nNumChildren)
					{
						ppszChildren[n] = m_ppszChildren[n];
						n++;
					}
					delete [] m_ppszChildren;
				}
				if(m_pnChildrenStatus)
				{
					int n=0;
					while(n<m_nNumChildren)
					{
						pnChildrenStatus[n] = m_pnChildrenStatus[n];
						n++;
					}
					delete [] m_pnChildrenStatus;
				}
				ppszChildren[m_nNumChildren] = pszChild;
				pnChildrenStatus[m_nNumChildren] = nStatus;
				m_nNumChildren++;
				m_ppszChildren=ppszChildren; 
				m_pnChildrenStatus=pnChildrenStatus; 
				return m_nNumChildren;
			}
			else delete [] ppszChildren;
		}
	}
	return -1;
}



//////////////////////////////////////////////////////////////////////
// CDirectData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirectData::CDirectData()
{
	InitializeCriticalSection(&m_critText);
	InitializeCriticalSection(&m_critSQL);
	InitializeCriticalSection(&m_critChannelDest);
	
	// messaging...
	m_bNetworkMessagingInitialized=false;

	m_bCheckModsWarningSent= false;
	m_bCheckMsgsWarningSent = false;
//	m_bCheckAsRunWarningSent = false;

	m_ppChannelDestObj = NULL;
	m_nNumChannelDestObjects=0;

	m_nTopElapseTime = LONG_MAX;
	m_nNumParseFiles=0;
	m_nLastParseFilesCleared=0;

/*
	m_ppConnObj = NULL;
	m_nNumConnectionObjects = 0;
*/
	m_nNumberOfEvents = 0;
	m_ppRulesObj = NULL;
	m_nNumRulesObjects = 0;

	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebAutoPurge ); // the last time autopurge was run
	_ftime( &m_timebTick );
	m_pszHost = NULL;			// the name of the host
	m_pszCompleteHost = NULL;	// the complete name of the host
	m_pszCortexHost = NULL;			// the name of the cortex host
	m_usCortexCommandPort = DIRECT_PORT_CMD;
	m_usCortexStatusPort = DIRECT_PORT_STATUS;
	m_nSettingsMod = -1;
//	m_nChannelsMod = -1;
//	m_nConnectionsMod = -1;
	m_nRulesMod = -1;
	m_nLastRulesMod = -1;
	m_nLastSettingsMod = -1;
//	m_nLastChannelsMod = -1;
//	m_nLastConnectionsMod = -1;
	m_nChannelDestMod=-1;
	m_nLastChannelDestMod=-1000;

	m_pdb = NULL;
	m_pdbConn = NULL;
	m_pdb2 = NULL;
	m_pdb2Conn = NULL;

	m_bQuietKill = false;

	m_nEventCheckIndex = -1;
	m_nEventLastMax = 1;
	m_prsEvents = NULL;

	m_nTypeAutomationInstalled = DIRECT_DEP_UNKNOWN;  // automation system unknown
	m_nIndexAutomationEndpoint=-1; // which automation module is installed.

//	m_pTransferEvent = NULL;
	m_pCheckEvent = NULL;

	m_bProcessSuspended = false;
	m_bAutomationThreadStarted=false;
	m_bAnalysisThreadStarted=false;
	m_bEventsChanged= false;
	m_bChannelDestChanged = true;

	_ftime( &m_timebAutomationTick ); // the last time check inside the thread
	_ftime( &m_timebAnalysisTick ); // the last time check inside the thread


}

CDirectData::~CDirectData()
{
/*
	if(m_ppConnObj)
	{
		int i=0;
		while(i<m_nNumConnectionObjects)
		{
			if(m_ppConnObj[i]) delete m_ppConnObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppConnObj; // delete array of pointers to objects, must use new to allocate
	}

*/
	try
	{
		if(m_ppRulesObj)
		{
			int i=0;
			while(m_nNumRulesObjects>0)
			{
				i=m_nNumRulesObjects-1;
				if(m_ppRulesObj[i]) delete m_ppRulesObj[i]; // delete objects, must use new to allocate
				m_ppRulesObj[i] = NULL;
				m_nNumRulesObjects--;
			}
			delete [] m_ppRulesObj; // delete array of pointers to objects, must use new to allocate
			m_ppRulesObj = NULL;
		}
		if(m_ppChannelDestObj)
		{
			int i=0;
			while(m_nNumChannelDestObjects>0)
			{
				i=m_nNumChannelDestObjects-1;
				if(m_ppChannelDestObj[i]) delete m_ppChannelDestObj[i]; // delete objects, must use new to allocate
				m_ppChannelDestObj[i] = NULL;
				m_nNumChannelDestObjects--;
			}
			delete [] m_ppChannelDestObj; // delete array of pointers to objects, must use new to allocate
			m_ppChannelDestObj = NULL;
		}
		if(m_pszHost) free(m_pszHost);	// must use malloc to allocate
		if(m_pszCompleteHost) free(m_pszCompleteHost);	// must use malloc to allocate
		if(m_pszCortexHost) free(m_pszCortexHost);	// must use malloc to allocate
		if(	m_pCheckEvent ) delete m_pCheckEvent;
	}
	catch(...)
	{
	}
	m_ppChannelDestObj = NULL;
	m_nNumChannelDestObjects=0;


	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critSQL);
	DeleteCriticalSection(&m_critChannelDest);

//	if(	m_pTransferEvent ) delete m_pTransferEvent;

}

char* CDirectData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CDirectData::SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError)
{
	int nRV = CX_ERROR;
	bool bError=false;

	if(!bOverwriteExistingError)
	{
		if(
			  ((m_ulFlags&DIRECT_ICON_MASK) == DIRECT_STATUS_ERROR)
			||((m_ulFlags&DIRECT_STATUS_CMDSVR_MASK) == DIRECT_STATUS_CMDSVR_ERROR)
			||((m_ulFlags&DIRECT_STATUS_STATUSSVR_MASK) ==  DIRECT_STATUS_STATUSSVR_ERROR)
			||((m_ulFlags&DIRECT_STATUS_THREAD_MASK) == DIRECT_STATUS_THREAD_ERROR)
			||((m_ulFlags&DIRECT_STATUS_FAIL_MASK) == DIRECT_STATUS_FAIL_DB)
			)	bError=true;
	}

	if (ulStatus&DIRECT_ICON_MASK)
	{
		m_ulFlags &= ~DIRECT_ICON_MASK;
		m_ulFlags |= (ulStatus&DIRECT_ICON_MASK);
	}
	if (ulStatus&DIRECT_STATUS_CMDSVR_MASK)
	{
		m_ulFlags &= ~DIRECT_STATUS_CMDSVR_MASK;
		m_ulFlags |= (ulStatus&DIRECT_STATUS_CMDSVR_MASK);
	}
	if (ulStatus&DIRECT_STATUS_STATUSSVR_MASK)
	{
		m_ulFlags &= ~DIRECT_STATUS_STATUSSVR_MASK;
		m_ulFlags |= (ulStatus&DIRECT_STATUS_STATUSSVR_MASK);
	}
	if (ulStatus&DIRECT_STATUS_THREAD_MASK)
	{
		m_ulFlags &= ~DIRECT_STATUS_THREAD_MASK;
		m_ulFlags |= (ulStatus&DIRECT_STATUS_THREAD_MASK);
	}
	if (ulStatus&DIRECT_STATUS_FAIL_MASK)
	{
		m_ulFlags &= ~DIRECT_STATUS_FAIL_MASK;
		m_ulFlags |= (ulStatus&DIRECT_STATUS_FAIL_MASK);
	}

	if(bError)
	{
		m_ulFlags &= ~DIRECT_ICON_MASK;
		m_ulFlags |= DIRECT_STATUS_ERROR;
	}

	if((pszText)&&(!bError))
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
//				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
//			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_pdirect->m_settings.m_pszIconPath)&&(strlen(g_pdirect->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_pdirect->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_pdirect->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_pdirect->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_pdirect->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_pdirect->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}

/*
int	CDirectData::SetStatusText(char* pszText, unsigned long ulStatus)
{
	int nRV = CX_ERROR;
	if(pszText)
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_pdirect->m_settings.m_pszIconPath)&&(strlen(g_pdirect->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_pdirect->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_pdirect->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_pdirect->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_pdirect->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_pdirect->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}
	return nRV;
}
*/

// utility
int	CDirectData::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszCompleteHost) free(m_pszCompleteHost);
						m_pszCompleteHost=pch;

						pch = (char*) malloc(nValue+1);
						if(pch!=NULL)
						{
							if(m_pszHost) free(m_pszHost);
							m_pszHost=pch;

							strcpy(m_pszHost, m_pszCompleteHost);
							pch = strchr(m_pszHost, '.');
							if(pch)
							{
								*pch = 0;
							}

							return DIRECT_SUCCESS;
						}
					}
				}
			}
		}
	}
	return DIRECT_ERROR;
}
/*
int CDirectData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_pdirect)&&(g_pdirect->m_settings.m_pszExchange)&&(strlen(g_pdirect->m_settings.m_pszExchange)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];

		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT MAX(mod) AS MAX FROM %s WHERE criterion = '%s'", 
			g_pdirect->m_settings.m_pszExchange, szTemp		);

		//Sleep(500);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, dberrorstring);
		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				CString szValue;
				prs->GetFieldValue("MAX", szValue);
				if(szValue.GetLength()>0)
				{
					unsigned long ulLastMod = atol(szValue);
					if(ulLastMod>0) ulMod = ulLastMod+1;
				}
			}
			prs->Close();
			delete prs;
		}

		if(ulMod>0)
		{
			if(ulMod > DIRECT_DB_MOD_MAX) ulMod = 1;
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET mod = %d WHERE criterion = '%s'", // HARDCODE
				g_pdirect->m_settings.m_pszExchange, ulMod, szTemp		);
			if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
			{
				return DIRECT_SUCCESS;
			}
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (criterion, flag, mod) VALUES ('%s', '', 1)", // HARDCODE
				g_pdirect->m_settings.m_pszExchange, szTemp		);
			if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
			{
				return DIRECT_SUCCESS;
			}
		}
	}
	return DIRECT_ERROR;
}
*/
int CDirectData::IncrementGlobalTimesUsed(CDirectEventObject* pObj, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.
	if(   (m_nIndexMetadataEndpoint>=0)
			&&(g_pdirect->m_settings.m_ppEndpointObject)
			&&(g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint])
			&&(m_pdb)&&(m_pdbConn)
		 )
	{
//		ReleaseRecordSet(); // have to release so we can get new results.
//		char errorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];

		CString szFilename;
		if(pObj->m_nCurrentFileSearch<0)
		{
			szFilename = pObj->m_pszMainFile;
			char* pchPartition = strchr(pObj->m_pszMainFile, '/'); // have to remove partition.
			if(pchPartition)
			{
				szFilename.Format("%s", pchPartition+1);
			}

		}
		else
		{
			szFilename = pObj->m_ppszChildren[pObj->m_nCurrentFileSearch];
			char* pchPartition = strchr(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch], '/'); // have to remove partition.
			if(pchPartition)
			{
				szFilename.Format("%s", pchPartition+1);
			}
		}

		char* pszEncodedFilename = NULL; 
		char* pszEncodedFilepath = NULL;
		CFileMetaDataObject* pfmdo = NULL;

		unsigned long ulType = ReturnDestinationType(pObj->m_sz_n_destinationid);
		if(
			  (ulType == CX_DESTTYPE_MIRANDA_HDIS) // all files on imagestore
			||((ulType == CX_DESTTYPE_MIRANDA_HDINT)&&(pObj->m_nCurrentFileSearch<0)) // only tem file on intuition
			)
		{
			char* pszRenamedFile = RenameFile(szFilename.GetBuffer(1));
			szFilename.ReleaseBuffer();
			if(pszRenamedFile)
			{
				pszEncodedFilename = m_pdb->EncodeQuotes(pszRenamedFile);
				pfmdo = ReturnFileMetaDataObject(pszRenamedFile);
				free(pszRenamedFile);
			}
		}
		else	
		{
			pszEncodedFilename = m_pdb->EncodeQuotes(szFilename);
			pfmdo = ReturnFileMetaDataObject(szFilename.GetBuffer(1));
			szFilename.ReleaseBuffer();
		}
		// need to get filepath.
		if(pfmdo)
		{
			pszEncodedFilepath = m_pdb->EncodeQuotes(pfmdo->m_sz_vc256_sys_filepath);
			delete pfmdo;
			pfmdo = NULL;
		}

		if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RENAME)	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:rename", "updating global times used for %s", pszEncodedFilename);

		if(pszEncodedFilepath)
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET direct_last_used = %d, direct_times_used = \
(SELECT case when max(direct_times_used) is null then 1 when max(direct_times_used) >= %d \
then %d else max(direct_times_used) + 1 end from %s.dbo.%s WHERE sys_filename = '%s' AND sys_filepath = '%s') \
WHERE sys_filename = '%s' AND sys_filepath = '%s'",
				g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
				g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the metadata table name
				(unsigned long)(m_timebTick.time - (m_timebTick.timezone*60) +(m_timebTick.dstflag?3600:0)),  // local time
				INT_MAX, INT_MAX,
				g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
				g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the metadata table name
				pszEncodedFilename?pszEncodedFilename:szFilename, pszEncodedFilepath,
				pszEncodedFilename?pszEncodedFilename:szFilename, pszEncodedFilepath
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET direct_last_used = %d, direct_times_used = \
(SELECT case when max(direct_times_used) is null then 1 when max(direct_times_used) >= %d \
then %d else max(direct_times_used) + 1 end from %s.dbo.%s WHERE sys_filename = '%s') WHERE sys_filename = '%s'",
				g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
				g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the metadata table name
				(unsigned long)(m_timebTick.time - (m_timebTick.timezone*60) +(m_timebTick.dstflag?3600:0)),  // local time
				INT_MAX, INT_MAX,
				g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
				g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the metadata table name
				pszEncodedFilename?pszEncodedFilename:szFilename,
				pszEncodedFilename?pszEncodedFilename:szFilename		
				);
		}

		EnterCriticalSection(&m_critSQL);
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "IncGlobal SQL: %s", szSQL);  Sleep(100); //(Dispatch message)
		char errorstring[DB_ERRORSTRING_LEN];
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)>=DB_SUCCESS)
//		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);

			
/*
// not using increment for archivist.
			g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->IncrementDatabaseMods(
				m_pdb, m_pdbConn, 
				g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata", 
				pszInfo);
*/
			//we need to tell archivist what file record we just modified, so we need to insert into its exchange table.
			if(pszEncodedFilepath)
			{

////////////////////////////////  REMOVED  TODO  this overwhelms archivist.  direct works much faster than arch can comply.
				//need to use stored procedure or other reason to push the stats, they are of secondary importance anyway....
				/*

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s (criterion, flag, mod) VALUES \
('DBT_%s', '%s|%s', %d )",  // the pipe is a delimiter,blank path is before filename
					g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
					g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszExchange?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszExchange:"Exchange",   // the Exchange table name
					g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the metadata table name
					pszEncodedFilepath, pszEncodedFilename?pszEncodedFilename:szFilename,
					(unsigned long)(m_timebTick.time - (m_timebTick.timezone*60) +(m_timebTick.dstflag?3600:0))  // local time
					);
				EnterCriticalSection(&m_critSQL);
				if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
				{
g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "IncGlobal ERROR: %s", errorstring); // Sleep(100); //(Dispatch message)
				}
				LeaveCriticalSection(&m_critSQL);
				*/
			}
			else
			{
				// dont insert it.  it means it was a null oxt transfer,  or there was not metadata for it.  so skip it.
/*
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s (criterion, flag, mod) VALUES \
('DBT_%s', '|%s', %d )",  // the pipe is a delimiter,blank path is before filename
					g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
					g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszExchange?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszExchange:"Exchange",   // the Exchange table name
					g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the metadata table name
					pszEncodedFilename?pszEncodedFilename:szFilename,	
					(unsigned long)(m_timebTick.time - (m_timebTick.timezone*60) +(m_timebTick.dstflag?3600:0))  // local time
					);

				EnterCriticalSection(&m_critSQL);
				if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
				{
					// error
				}
				LeaveCriticalSection(&m_critSQL);
*/
			}

			if(pszEncodedFilename) free(pszEncodedFilename);
			if(pszEncodedFilepath) free(pszEncodedFilepath);


			return DIRECT_SUCCESS;
		}
		else g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "IncGlobal ERROR: %s", errorstring); // Sleep(100); //(Dispatch message)

		if(pszEncodedFilename) free(pszEncodedFilename);
		if(pszEncodedFilepath) free(pszEncodedFilepath);

		LeaveCriticalSection(&m_critSQL);

	}
	return DIRECT_ERROR;
}

int CDirectData::IncrementLocalTimesUsed(CDirectEventObject* pObj, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.
	int nDestModule=ReturnModuleIndex(pObj, pszInfo);

	if(
		  (pObj)&&(m_pdb)&&(m_pdbConn)
			&&(nDestModule>=0)
			&&(g_pdirect->m_settings.m_nNumEndpointsInstalled>0)
			&&(nDestModule<g_pdirect->m_settings.m_nNumEndpointsInstalled)
			&&(g_pdirect->m_settings.m_ppEndpointObject)
			&&(g_pdirect->m_settings.m_ppEndpointObject[nDestModule])
		)
	{
		char pszFilename[MAX_PATH];
		char pszPartition[MAX_PATH];
		strcpy(pszFilename, "");
		memset(pszPartition, 0, MAX_PATH);
		if(g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType == DIRECT_DEP_EDGE_PROSPERO)
		{
			if(pObj->m_nCurrentFileSearch < 0)
			{ // main file
				if((pObj->m_pszMainFile)&&(strlen(pObj->m_pszMainFile)))
				{
					char* pchPartition = strchr(pObj->m_pszMainFile, '/'); // have to remove partition.
					if(pchPartition)
					{
						strcpy(pszFilename, pchPartition+1);
						strncpy(pszPartition, pObj->m_pszMainFile, pchPartition-(pObj->m_pszMainFile));
					}
				}
			}
			else
			if(pObj->m_nCurrentFileSearch < pObj->m_nNumChildren)
			{
				if((pObj->m_ppszChildren)&&(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch])&&(strlen(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch])))
				{
					char* pchPartition = strchr(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch], '/'); // have to remove partition.
					if(pchPartition)
					{
						strcpy(pszFilename, pchPartition+1);
						strncpy(pszPartition, pObj->m_ppszChildren[pObj->m_nCurrentFileSearch], 
							pchPartition-(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch]));
					}
				}
			}

			if(strlen(pszFilename))
			{
//				ReleaseRecordSet(); // have to release so we can get new results.
				char errorstring[DB_ERRORSTRING_LEN];
				char szSQL[DB_SQLSTRING_MAXLEN];
				CString szTemp = "Destinations_Media";  // we didn't find it. just use the default name
				if(nDestModule>=0) szTemp = g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestinationMedia?g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestinationMedia:"Destinations_Media";
				char* pszEncodedFilename = m_pdb->EncodeQuotes(pszFilename);


				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET Direct_local_last_used = %d, Direct_local_times_used = \
(SELECT case when max(Direct_local_times_used) is null then 1 when max(Direct_local_times_used) >= %d \
then %d else max(Direct_local_times_used) + 1 end from %s.dbo.%s WHERE file_name = '%s' AND host = '%s' AND partition = '%s') \
WHERE file_name = '%s' AND host = '%s' AND partition = '%s'",
					pObj->m_sz_vc64_module_dbname,			// the edge device DB name
					szTemp,
					(unsigned long)(m_timebTick.time - (m_timebTick.timezone*60) +(m_timebTick.dstflag?3600:0)), // local time....
					INT_MAX, INT_MAX,
					pObj->m_sz_vc64_module_dbname,			// the edge device DB name
					szTemp,
					pszEncodedFilename?pszEncodedFilename:pszFilename, 
					pObj->m_sz_vc64_dest_host, pszPartition,
					pszEncodedFilename?pszEncodedFilename:pszFilename,	
					pObj->m_sz_vc64_dest_host, pszPartition
					);
				
				if(pszEncodedFilename) free(pszEncodedFilename);
			
				EnterCriticalSection(&m_critSQL);
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "IncLocal SQL: %s", szSQL);  Sleep(100); //(Dispatch message)
				if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)>=DB_SUCCESS)
//				if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
				{
					LeaveCriticalSection(&m_critSQL);

					g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->IncrementDatabaseMods(
						m_pdb, m_pdbConn, 
						szTemp.GetBuffer(1),
						pszInfo);
					szTemp.ReleaseBuffer();

					return DIRECT_SUCCESS;
				}
				else g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "IncLocal ERROR: %s", errorstring);  //(Dispatch message)
				LeaveCriticalSection(&m_critSQL);
			}
		}  // no other types supported yet.
	}
	return DIRECT_ERROR;
}

int CDirectData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_pdirect)&&(g_pdirect->m_settings.m_pszExchange)&&(strlen(g_pdirect->m_settings.m_pszExchange)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s set mod = \
(SELECT case when max(mod) is null then 1 when max(mod) >= %d \
then 1 else max(mod) + 1 end from %s WHERE criterion = '%s') WHERE criterion = '%s'",
			g_pdirect->m_settings.m_pszExchange,
			DIRECT_DB_MOD_MAX,
			g_pdirect->m_settings.m_pszExchange,
			szTemp, szTemp		
			);
//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return DIRECT_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return DIRECT_ERROR;
}
int CDirectData::CheckMessages(char* pszInfo)
{
	if((g_pdirect)&&(m_pdbConn)&&(m_pdb)
		&&(g_pdirect->m_settings.m_pszMessages)&&(strlen(g_pdirect->m_settings.m_pszMessages)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_pdirect->m_settings.m_pszMessages)&&(strlen(g_pdirect->m_settings.m_pszMessages)))?g_pdirect->m_settings.m_pszMessages:"Messages",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_pdirect->m_settings.m_nAutoPurgeMessageDays*86400) // local time....
			);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return DIRECT_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return DIRECT_ERROR;
}
/*
int CDirectData::CheckAsRun(char* pszInfo)
{
	if((g_pdirect)&&(m_pdbConn)&&(m_pdb)
		&&(g_pdirect->m_settings.m_pszAsRun)&&(strlen(g_pdirect->m_settings.m_pszAsRun)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_pdirect->m_settings.m_pszAsRun)&&(strlen(g_pdirect->m_settings.m_pszAsRun)))?g_pdirect->m_settings.m_pszAsRun:"AsRun_Log",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_pdirect->m_settings.m_nAutoPurgeAsRunDays*86400) // local time....
			);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return DIRECT_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return DIRECT_ERROR;
}
*/

int CDirectData::CheckDatabaseMods(char* pszInfo)
{
	if((g_pdirect)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_pdirect->m_settings.m_pszExchange)&&(strlen(g_pdirect->m_settings.m_pszExchange)))?g_pdirect->m_settings.m_pszExchange:"Exchange");

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = DIRECT_SUCCESS;
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				CString szCriterion;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
				}

				if((g_pdirect->m_settings.m_pszSettings)&&(strlen(g_pdirect->m_settings.m_pszSettings)))
				{
					szTemp.Format("DBT_%s",g_pdirect->m_settings.m_pszSettings);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nSettingsMod = nReturn;
					}
				}

				if((g_pdirect->m_settings.m_pszMapping)&&(strlen(g_pdirect->m_settings.m_pszMapping)))
				{
					szTemp.Format("DBT_%s",g_pdirect->m_settings.m_pszMapping);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nRulesMod = nReturn;
					}
				}

				// get the suspend

				szTemp.Format("Suspend");
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					nReturn = atoi(szMod);
					if(nReturn>0)
					{
						if(!m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "DiReCT is suspended.");  
							g_pdirect->m_data.SetStatusText(errorstring, CX_STATUS_UNKNOWN);
							g_pdirect->m_msgr.DM(MSG_ICONNONE, NULL, "DiReCT:suspend", "*** DiReCT has been suspended. ***"); // Sleep(50); //(Dispatch message)
							g_pdirect->SendMsg(CX_SENDMSG_INFO, "DiReCT:suspend", "DiReCT has been suspended");
							m_bProcessSuspended = true;
						}
					}
					else
					{
						if(m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "DiReCT is running.");  
							g_pdirect->m_data.SetStatusText(errorstring, CX_STATUS_OK);
							g_pdirect->m_msgr.DM(MSG_ICONNONE, NULL, "DiReCT:resume", "*** DiReCT has been resumed. ***");//  Sleep(50); //(Dispatch message)
							g_pdirect->SendMsg(CX_SENDMSG_INFO, "DiReCT:resume", "DiReCT has been resumed.");
							m_bProcessSuspended = false;
						}
					}
				}
/*
				if((g_pdirect->m_settings.m_pszChannels)&&(strlen(g_pdirect->m_settings.m_pszChannels)))
				{
					szTemp.Format("DBT_%s",g_pdirect->m_settings.m_pszChannels);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nChannelsMod = nReturn;
					}
				}

				if((g_pdirect->m_settings.m_pszConnections)&&(strlen(g_pdirect->m_settings.m_pszConnections)))
				{
					szTemp.Format("DBT_%s",g_pdirect->m_settings.m_pszConnections);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nConnectionsMod = nReturn;
					}
				}
*/
				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			delete prs;
			prs = NULL;
			LeaveCriticalSection(&m_critSQL);

			return nReturn;
		}
		LeaveCriticalSection(&m_critSQL);

	}
	return DIRECT_ERROR;
}


unsigned long CDirectData::ReturnDestinationType(CString szDestinationID, int nType)
{
	if((m_ppChannelDestObj)&&(m_nNumChannelDestObjects>0)&&(szDestinationID.GetLength()))
	{
		switch(nType)
		{
		case DIRECT_SEARCH_HOST:
			{
				int o=0;
				while(o<m_nNumChannelDestObjects)
				{
					if(
							(m_ppChannelDestObj[o])
						&&(m_ppChannelDestObj[o]->m_sz_vc64_host.Compare(szDestinationID)==0)
						&&(m_ppChannelDestObj[o]->m_ulDestFlags&DIRECT_FLAG_ENABLED)  // has to be enabled.  if not, can't return, could be a backup device of another type on same host ip
						) return m_ppChannelDestObj[o]->m_ulDestType;
					o++;
				}
			} break;
		default:
		case DIRECT_SEARCH_DESTID:
			{
				int o=0;
				while(o<m_nNumChannelDestObjects)
				{
					if(
							(m_ppChannelDestObj[o])
						&&(m_ppChannelDestObj[o]->m_sz_n_destinationid.Compare(szDestinationID)==0)
						) return m_ppChannelDestObj[o]->m_ulDestType;
					o++;
				}
			} break;
		}
	}
	return CX_DESTTYPE_UNKNOWN;
}

char* CDirectData::RenameFile(char* pszFilename)
{
	if(pszFilename)
	{
		int nLen = strlen(pszFilename);
		if(nLen>0)
		{
			char filename[MAX_PATH];
			strcpy(filename, pszFilename);
			char* pch = strrchr(filename, '.');
			if(pch)
			{
				*pch = 0; //null terminate it!
				pch++;
			}

			if(g_pdirect->m_settings.m_pszRenamePrefix)
				nLen += strlen(g_pdirect->m_settings.m_pszRenamePrefix);
			if(g_pdirect->m_settings.m_pszRenameSuffix)
				nLen += strlen(g_pdirect->m_settings.m_pszRenameSuffix);

			char* pszReturn = (char*)malloc(nLen+1);
			if(pszReturn)
			{
				sprintf(pszReturn, "%s%s%s%s%s",
					(g_pdirect->m_settings.m_pszRenamePrefix?g_pdirect->m_settings.m_pszRenamePrefix:""),
					filename,
					(g_pdirect->m_settings.m_pszRenameSuffix?g_pdirect->m_settings.m_pszRenameSuffix:""),
					(pch?".":""),
					(pch?pch:"")
					);
				return pszReturn;
			}
		}
	}
	return NULL;
}


char* CDirectData::PreprocessID(char* pszID)
{
	if((pszID)&&(g_pdirect->m_settings.m_pszIDPreprocessString))
	{
		int nLen = strlen(g_pdirect->m_settings.m_pszIDPreprocessString);
		if(nLen>0)
		{
			char* pszReturn = (char*)malloc(nLen+1);
			if(pszReturn)
			{
				memset(pszReturn, 0, nLen+1);
				int i= strlen(pszID)-1;
				int p = nLen-1;
				while (p>=0)
				{
					char ch = *(g_pdirect->m_settings.m_pszIDPreprocessString+p);
					if( ch =='?')  // only allowed wildcard
					{
						// get from ID
						if(i>=0)
						{
							*(pszReturn+p) = *(pszID+i);
							i--;
						}
						else  // have to compact it!
						{ 
							int j=p;
							while(j<nLen) // want to do the term null as well.
							{
								*(pszReturn+j) = *(pszReturn+j+1);
								j++;
							}

						}
					}
					else
					{
						// use from string.
						*(pszReturn+p) = ch;
					}

					p--;
				}
				return pszReturn;
			}
		}
	}
	return NULL;
}


int CDirectData::GetChannelDests(char* pszInfo)
{
	if((g_pdirect)&&(m_pdbConn)&&(m_pdb))
	{

// the following are the fields
	// server, then list, then dest info.  If a channel not active, its not in the view.
	// if a dest not active, the values are null.

//		id, module_name, server, server_time, server_status, server_basis, server_changed, server_last_update, 
//		list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update,
//		dest_module_name, type, description, destinationid, flags, host.

	
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT id, dest_module_name, host, type, description, destinationid, flags FROM %s", 
			((g_pdirect->m_settings.m_pszChannelDests)&&(strlen(g_pdirect->m_settings.m_pszChannelDests)))?g_pdirect->m_settings.m_pszChannelDests:"ChannelInfo");

//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "rule SQL: %s", szSQL);  Sleep(100); //(Dispatch message)

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = DIRECT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString sz_n_listid;
				CString sz_vc64_dest_module_name;
				CString sz_vc64_host;
				CString sz_n_type;
				CString sz_vc265_description;
				CString sz_n_destinationid;
				CString sz_n_flags; // "1" = active, not active = "", "0", or null
				bool bFound = false;
				try
				{
					prs->GetFieldValue("id", sz_n_listid);//HARDCODE
					sz_n_listid.TrimLeft(); sz_n_listid.TrimRight();

					prs->GetFieldValue("dest_module_name", sz_vc64_dest_module_name);//HARDCODE
					sz_vc64_dest_module_name.TrimLeft(); sz_vc64_dest_module_name.TrimRight();
					
					prs->GetFieldValue("host", sz_vc64_host);//HARDCODE
					sz_vc64_host.TrimLeft(); sz_vc64_host.TrimRight();

					prs->GetFieldValue("type", sz_n_type);//HARDCODE
					sz_n_type.TrimLeft(); sz_n_type.TrimRight();

					prs->GetFieldValue("description", sz_vc265_description);//HARDCODE
					sz_vc265_description.TrimLeft(); sz_vc265_description.TrimRight();

					prs->GetFieldValue("destinationid", sz_n_destinationid);//HARDCODE
					sz_n_destinationid.TrimLeft(); sz_n_destinationid.TrimRight();

					prs->GetFieldValue("flags", sz_n_flags);//HARDCODE
					sz_n_flags.TrimLeft(); sz_n_flags.TrimRight();

//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "rule obtained: %d", nRuleID);  Sleep(100); //(Dispatch message)

				}
				catch( ... )
				{
g_pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "exception obtaining channel destinations"); //(Dispatch message)
				}

				if((m_ppChannelDestObj)&&(m_nNumChannelDestObjects))
				{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "rule obtained: %d, checking exists", nRuleID);  Sleep(100); //(Dispatch message)
					int nTemp=0;
					while(nTemp<m_nNumChannelDestObjects)
					{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "rule obtained: %d, checking exists against #%d", nRuleID, nTemp);  Sleep(100); //(Dispatch message)
						if(m_ppChannelDestObj[nTemp])
						{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "rule obtained: %d against #%d, not null", nRuleID, nTemp);  Sleep(100); //(Dispatch message)
							if(sz_n_destinationid.CompareNoCase(m_ppChannelDestObj[nTemp]->m_sz_n_destinationid)==0) // all nulls can be one. thats ok.
							{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "rule obtained: %d found");  Sleep(100); //(Dispatch message)
								bFound = true;
								// override with the new changes:
								m_ppChannelDestObj[nTemp]->m_sz_n_listid=sz_n_listid;
								m_ppChannelDestObj[nTemp]->m_sz_vc64_dest_module_name=sz_vc64_dest_module_name;
								m_ppChannelDestObj[nTemp]->m_sz_vc64_host=sz_vc64_host;
								m_ppChannelDestObj[nTemp]->m_sz_n_type=sz_n_type;
								m_ppChannelDestObj[nTemp]->m_sz_vc265_description=sz_vc265_description;
								m_ppChannelDestObj[nTemp]->m_sz_n_destinationid=sz_n_destinationid;
								m_ppChannelDestObj[nTemp]->m_sz_n_flags=sz_n_flags; // "1" = active, not active = "", "0", or null

								m_ppChannelDestObj[nTemp]->m_ulFlags = DIRECT_FLAG_FOUND;
								m_ppChannelDestObj[nTemp]->m_ulDestFlags = sz_n_flags.GetLength()?atol(sz_n_flags):DIRECT_FLAG_DISABLED;
								m_ppChannelDestObj[nTemp]->m_ulDestType = sz_n_type.GetLength()?atol(sz_n_type):DIRECT_RULE_TYPE_UNKNOWN;

							}
						}
						nTemp++;
					}
				}
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "finished search for rule %d", nRuleID);  Sleep(100); //(Dispatch message)

				if((!bFound)&&(sz_vc64_host.GetLength()>0)) // have to add.
				{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "adding rule %d", nRuleID);  Sleep(100); //(Dispatch message)

					CDirectChannelDestinationObject* pscho = new CDirectChannelDestinationObject;
					if(pscho)
					{
						CDirectChannelDestinationObject** ppObj = new CDirectChannelDestinationObject*[m_nNumChannelDestObjects+1];
						if(ppObj)
						{
							int o=0;
							if((m_ppChannelDestObj)&&(m_nNumChannelDestObjects>0))
							{
								while(o<m_nNumChannelDestObjects)
								{
									ppObj[o] = m_ppChannelDestObj[o];
									o++;
								}
								delete [] m_ppChannelDestObj;

							}
							ppObj[m_nNumChannelDestObjects] = pscho;
							m_ppChannelDestObj = ppObj;
							m_nNumChannelDestObjects++;

//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "adding rule %d, type is %d", nRuleID, nType);  Sleep(100); //(Dispatch message)
								// override with the new changes:
							pscho->m_sz_n_listid=sz_n_listid;
							pscho->m_sz_vc64_dest_module_name=sz_vc64_dest_module_name;
							pscho->m_sz_vc64_host=sz_vc64_host;
							pscho->m_sz_n_type=sz_n_type;
							pscho->m_sz_vc265_description=sz_vc265_description;
							pscho->m_sz_n_destinationid=sz_n_destinationid;
							pscho->m_sz_n_flags=sz_n_flags; // "1" = active, not active = "", "0", or null

							pscho->m_ulFlags = DIRECT_FLAG_FOUND;
							pscho->m_ulDestFlags = sz_n_flags.GetLength()?atol(sz_n_flags):DIRECT_FLAG_DISABLED;
							pscho->m_ulDestType = sz_n_type.GetLength()?atol(sz_n_type):DIRECT_RULE_TYPE_UNKNOWN;

//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "adding rule %d, szLocation is %s", nRuleID, szLocation);  Sleep(100); //(Dispatch message)
							pscho->m_ulFlags = DIRECT_FLAG_FOUND;


						}
						else
							delete pscho;
					}
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			prs = NULL;
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "removing non found rules");  Sleep(100); //(Dispatch message)
			LeaveCriticalSection(&m_critSQL);

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<m_nNumChannelDestObjects)
			{
				if((m_ppChannelDestObj)&&(m_ppChannelDestObj[nIndex]))
				{
					if((m_ppChannelDestObj[nIndex]->m_ulFlags)&DIRECT_FLAG_FOUND)
					{
						(m_ppChannelDestObj[nIndex]->m_ulFlags) &= ~DIRECT_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(m_ppChannelDestObj[nIndex])
						{
							delete m_ppChannelDestObj[nIndex];
							m_nNumChannelDestObjects--;

							int nTemp=nIndex;
							while(nTemp<m_nNumChannelDestObjects)
							{
								m_ppChannelDestObj[nTemp]=m_ppChannelDestObj[nTemp+1];
								nTemp++;
							}
							m_ppChannelDestObj[nTemp] = NULL;
						} else nIndex++;
					}
				}
				else
					nIndex++;
			}

			return nReturn;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return DIRECT_ERROR;
}

bool CDirectData::IsChannelDestChannelActive(CString szChannelID)
{
	if((m_ppChannelDestObj)&&(m_nNumChannelDestObjects>0)&&(szChannelID.GetLength()))
	{
		int o=0;
		while(o<m_nNumChannelDestObjects)
		{
			if((m_ppChannelDestObj[o])&&(m_ppChannelDestObj[o]->m_sz_n_listid.Compare(szChannelID)==0)) return true; // if it's in the list at all it is active.
			o++;
		}
	}
	return false;
}

unsigned long CDirectData::IsChannelDestDestinationIDActive(CString szDestinationID)
{
	if((m_ppChannelDestObj)&&(m_nNumChannelDestObjects>0)&&(szDestinationID.GetLength()))
	{
		int o=0;
		while(o<m_nNumChannelDestObjects)
		{
			if(
				  (m_ppChannelDestObj[o])
				&&(m_ppChannelDestObj[o]->m_sz_n_destinationid.Compare(szDestinationID)==0)
				) return m_ppChannelDestObj[o]->m_ulDestFlags;
			o++;
		}
	}
	return DIRECT_FLAGS_INVALID;
}

unsigned long CDirectData::IsChannelDestDestinationHostActive(CString szDestinationHost)
{
	if((m_ppChannelDestObj)&&(m_nNumChannelDestObjects>0)&&(szDestinationHost.GetLength()))
	{
		int o=0;
		while(o<m_nNumChannelDestObjects)
		{
			if(
				  (m_ppChannelDestObj[o])
				&&(m_ppChannelDestObj[o]->m_sz_vc64_host.Compare(szDestinationHost)==0)
				) return m_ppChannelDestObj[o]->m_ulDestFlags; 
			o++;
		}
	}
	return false;
}


int CDirectData::GetRules(char* pszInfo)
{
	if((g_pdirect)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_pdirect->m_settings.m_pszMapping)&&(strlen(g_pdirect->m_settings.m_pszMapping)))?g_pdirect->m_settings.m_pszMapping:"Mapping");

//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "rule SQL: %s", szSQL);  Sleep(100); //(Dispatch message)

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = DIRECT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
/*
	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;
	unsigned short m_usComparisonType;
	unsigned short m_usDestinationType;
	unsigned short m_usSearchType;
	unsigned short m_usActionType;
	int m_nRuleID;

	char* m_pszFieldName;
	char* m_pszParamName;
	char* m_pszCriterion;
	char* m_pszEventLocation;

create table Rules (rule_type tinyint, field_name varchar(32), param_name varchar(32), comparison varchar(10), 
criterion varchar(64), event_location varchar(32), dest_type int, search_type tinyint, action tinyint, 
ruleid int IDENTITY (1, 1) NOT NULL);
				
*/
				CString szField = "";
				CString szParam = "";
				CString szCrtierion = "";
				CString szLocation = "";
				CString szTemp = "";
				int nRuleID = -1;
				int nType = -1;
				int nComparisonType = -1;
				int nDestinationType = -1;
				int nSearchType = -1;
				int nActionType = -1;
				int nTemp;
				bool bFound = false;
				try
				{
					prs->GetFieldValue("rule_type", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nType = nTemp;
					}
					prs->GetFieldValue("field_name", szField);//HARDCODE
					szField.TrimLeft(); szField.TrimRight();
					prs->GetFieldValue("param_name", szParam);//HARDCODE
					szParam.TrimLeft(); szParam.TrimRight();
					prs->GetFieldValue("comparison", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();

					if(szTemp.Compare("==")==0) nComparisonType = DIRECT_RULE_COMPARE_EQUALS;
					else if(szTemp.Compare("!=")==0) nComparisonType = DIRECT_RULE_COMPARE_NOTEQUAL;
					else if(szTemp.Compare(">")==0) nComparisonType = DIRECT_RULE_COMPARE_GT;
					else if(szTemp.Compare("<")==0) nComparisonType = DIRECT_RULE_COMPARE_LT;
					else if(szTemp.Compare(">=")==0) nComparisonType = DIRECT_RULE_COMPARE_GTOE;
					else if(szTemp.Compare("<=")==0) nComparisonType = DIRECT_RULE_COMPARE_LTOE;
					else if(szTemp.Compare("!=")==0) nComparisonType = DIRECT_RULE_COMPARE_NOTEQUAL;
					else if(szTemp.Compare("~=")==0) nComparisonType = DIRECT_RULE_COMPARE_PARTIAL;
					else if(szTemp.Compare("&")==0) nComparisonType = DIRECT_RULE_COMPARE_AND;
					else if(szTemp.Compare("=&")==0) nComparisonType = DIRECT_RULE_COMPARE_ALL;
					else if(szTemp.Compare("!&")==0) nComparisonType = DIRECT_RULE_COMPARE_NOT;
/*
#define DIRECT_RULE_COMPARE_EQUALS				0x0001  // == equals
#define DIRECT_RULE_COMPARE_GT						0x0002  // >  greater than
#define DIRECT_RULE_COMPARE_LT						0x0003  // <  less than
#define DIRECT_RULE_COMPARE_GTOE					0x0004  // >= greater than or equal to
#define DIRECT_RULE_COMPARE_LTOE					0x0005  // <= less than or equal to
#define DIRECT_RULE_COMPARE_NOTEQUAL			0x0006  // != equals
#define DIRECT_RULE_COMPARE_PARTIAL				0x0007  // ~= partial comparison
#define DIRECT_RULE_COMPARE_AND						0x0008  // &  contains any of the bitflags
#define DIRECT_RULE_COMPARE_ALL						0x0009  // =& contains all the bitflags
#define DIRECT_RULE_COMPARE_NOT						0x000a  // !& does not contain any the bitflag
*/
					prs->GetFieldValue("criterion", szCrtierion);//HARDCODE
					szCrtierion.TrimLeft(); szCrtierion.TrimRight();
					prs->GetFieldValue("event_location", szLocation);//HARDCODE
					szLocation.TrimLeft(); szLocation.TrimRight();

					prs->GetFieldValue("dest_type", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
/*
// if it were hex.

						char* p=szTemp.GetBuffer(1);
						nTemp = m_bu.xtol(p, strlen(p));
						szTemp.ReleaseBuffer();
*/
						// but we will just make it decimal
						nTemp = atoi(szTemp);
						if(nTemp>0) nDestinationType = nTemp;
					}
					prs->GetFieldValue("search_type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nSearchType = nTemp;
					}
					prs->GetFieldValue("action", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nActionType = nTemp;
					}
					prs->GetFieldValue("ruleid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nRuleID = nTemp;
					}

//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "rule obtained: %d", nRuleID);  Sleep(100); //(Dispatch message)

				}
				catch( ... )
				{
				}

				if((m_ppRulesObj)&&(m_nNumRulesObjects))
				{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "rule obtained: %d, checking exists", nRuleID);  Sleep(100); //(Dispatch message)
					nTemp=0;
					while(nTemp<m_nNumRulesObjects)
					{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "rule obtained: %d, checking exists against #%d", nRuleID, nTemp);  Sleep(100); //(Dispatch message)
						if(m_ppRulesObj[nTemp])
						{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "rule obtained: %d against #%d, not null", nRuleID, nTemp);  Sleep(100); //(Dispatch message)
							if((nRuleID>=0)&&(nRuleID == m_ppRulesObj[nTemp]->m_nRuleID))
							{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "rule obtained: %d found");  Sleep(100); //(Dispatch message)
								bFound = true;
								// override with the new changes:
								if(nType>=0) m_ppRulesObj[nTemp]->m_ulType = (unsigned long) nType;
								if(nComparisonType>=0) m_ppRulesObj[nTemp]->m_usComparisonType = (unsigned short) nComparisonType;
								if(nDestinationType>=0) m_ppRulesObj[nTemp]->m_ulDestinationType = (unsigned long) nDestinationType;
								if(nSearchType>=0) m_ppRulesObj[nTemp]->m_usSearchType = (unsigned short) nSearchType;
								if(nActionType>=0) m_ppRulesObj[nTemp]->m_usActionType = (unsigned short) nActionType;

								if(	(szField.GetLength()>0)&&((m_ppRulesObj[nTemp]->m_pszFieldName==NULL)||(szField.CompareNoCase(m_ppRulesObj[nTemp]->m_pszFieldName))) )
								{
									if(m_ppRulesObj[nTemp]->m_pszFieldName) free(m_ppRulesObj[nTemp]->m_pszFieldName);
									m_ppRulesObj[nTemp]->m_pszFieldName = (char*)malloc(szField.GetLength()+1); 
									if(m_ppRulesObj[nTemp]->m_pszFieldName) sprintf(m_ppRulesObj[nTemp]->m_pszFieldName, szField);
								}

								if(	(szParam.GetLength()>0)&&((m_ppRulesObj[nTemp]->m_pszParamName==NULL)||(szParam.CompareNoCase(m_ppRulesObj[nTemp]->m_pszParamName))) )
								{
									if(m_ppRulesObj[nTemp]->m_pszParamName) free(m_ppRulesObj[nTemp]->m_pszParamName);
									m_ppRulesObj[nTemp]->m_pszParamName = (char*)malloc(szParam.GetLength()+1); 
									if(m_ppRulesObj[nTemp]->m_pszParamName) sprintf(m_ppRulesObj[nTemp]->m_pszParamName, szParam);
								}

								if(	(szCrtierion.GetLength()>0)&&((m_ppRulesObj[nTemp]->m_pszCriterion==NULL)||(szCrtierion.CompareNoCase(m_ppRulesObj[nTemp]->m_pszCriterion))) )
								{
									if(m_ppRulesObj[nTemp]->m_pszCriterion) free(m_ppRulesObj[nTemp]->m_pszCriterion);
									m_ppRulesObj[nTemp]->m_pszCriterion = (char*)malloc(szCrtierion.GetLength()+1); 
									if(m_ppRulesObj[nTemp]->m_pszCriterion) sprintf(m_ppRulesObj[nTemp]->m_pszCriterion, szCrtierion);
								}

								if(	(szLocation.GetLength()>0)&&((m_ppRulesObj[nTemp]->m_pszEventLocation==NULL)||(szLocation.CompareNoCase(m_ppRulesObj[nTemp]->m_pszEventLocation))) )
								{
									if(m_ppRulesObj[nTemp]->m_pszEventLocation) free(m_ppRulesObj[nTemp]->m_pszEventLocation);
									m_ppRulesObj[nTemp]->m_pszEventLocation = (char*)malloc(szLocation.GetLength()+1); 
									if(m_ppRulesObj[nTemp]->m_pszEventLocation) sprintf(m_ppRulesObj[nTemp]->m_pszEventLocation, szLocation);
								}

								m_ppRulesObj[nTemp]->m_ulFlags = DIRECT_FLAG_FOUND;
							}
						}
						nTemp++;
					}
				}
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "finished search for rule %d", nRuleID);  Sleep(100); //(Dispatch message)

				if((!bFound)&&(nRuleID>=0)) // have to add.
				{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "adding rule %d", nRuleID);  Sleep(100); //(Dispatch message)

					CDirectRulesObject* pscho = new CDirectRulesObject;
					if(pscho)
					{
						CDirectRulesObject** ppObj = new CDirectRulesObject*[m_nNumRulesObjects+1];
						if(ppObj)
						{
							int o=0;
							if((m_ppRulesObj)&&(m_nNumRulesObjects>0))
							{
								while(o<m_nNumRulesObjects)
								{
									ppObj[o] = m_ppRulesObj[o];
									o++;
								}
								delete [] m_ppRulesObj;

							}
							ppObj[m_nNumRulesObjects] = pscho;
							m_ppRulesObj = ppObj;
							m_nNumRulesObjects++;

//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "adding rule %d, type is %d", nRuleID, nType);  Sleep(100); //(Dispatch message)
							pscho->m_nRuleID = nRuleID;
							if(nType>=0) pscho->m_ulType = (unsigned long) nType;
							if(nComparisonType>=0) pscho->m_usComparisonType = (unsigned short) nComparisonType;
							if(nDestinationType>=0) pscho->m_ulDestinationType = (unsigned long) nDestinationType;
							if(nSearchType>=0) pscho->m_usSearchType = (unsigned short) nSearchType;
							if(nActionType>=0) pscho->m_usActionType = (unsigned short) nActionType;
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "adding rule %d, nActionType is %d", nRuleID, nComparisonType);  Sleep(100); //(Dispatch message)

							if(szField.GetLength()>0)
							{
								if(pscho->m_pszFieldName) free(pscho->m_pszFieldName);
								pscho->m_pszFieldName = (char*)malloc(szField.GetLength()+1); 
								if(pscho->m_pszFieldName) sprintf(pscho->m_pszFieldName, szField);
							}

//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "adding rule %d, szField is %s", nRuleID, szField);  Sleep(100); //(Dispatch message)
							if(szParam.GetLength()>0)
							{
								if(pscho->m_pszParamName) free(pscho->m_pszParamName);
								pscho->m_pszParamName = (char*)malloc(szParam.GetLength()+1); 
								if(pscho->m_pszParamName) sprintf(pscho->m_pszParamName, szParam);
							}

//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "adding rule %d, szParam is %s", nRuleID, szParam);  Sleep(100); //(Dispatch message)
							if(szCrtierion.GetLength()>0)
							{
								if(pscho->m_pszCriterion) free(pscho->m_pszCriterion);
								pscho->m_pszCriterion = (char*)malloc(szCrtierion.GetLength()+1); 
								if(pscho->m_pszCriterion) sprintf(pscho->m_pszCriterion, szCrtierion);
							}

//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "adding rule %d, szCrtierion is %s", nRuleID, szCrtierion);  Sleep(100); //(Dispatch message)
							if(	szLocation.GetLength()>0)
							{
								if(pscho->m_pszEventLocation) free(pscho->m_pszEventLocation);
								pscho->m_pszEventLocation = (char*)malloc(szLocation.GetLength()+1); 
								if(pscho->m_pszEventLocation) sprintf(pscho->m_pszEventLocation, szLocation);
							}

//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "adding rule %d, szLocation is %s", nRuleID, szLocation);  Sleep(100); //(Dispatch message)
							pscho->m_ulFlags = DIRECT_FLAG_FOUND;


						}
						else
							delete pscho;
					}
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			prs = NULL;
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "removing non found rules");  Sleep(100); //(Dispatch message)
			LeaveCriticalSection(&m_critSQL);

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<m_nNumRulesObjects)
			{
				if((m_ppRulesObj)&&(m_ppRulesObj[nIndex]))
				{
					if((m_ppRulesObj[nIndex]->m_ulFlags)&DIRECT_FLAG_FOUND)
					{
						(m_ppRulesObj[nIndex]->m_ulFlags) &= ~DIRECT_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(m_ppRulesObj[nIndex])
						{
							delete m_ppRulesObj[nIndex];
							m_nNumRulesObjects--;

							int nTemp=nIndex;
							while(nTemp<m_nNumRulesObjects)
							{
								m_ppRulesObj[nTemp]=m_ppRulesObj[nTemp+1];
								nTemp++;
							}
							m_ppRulesObj[nTemp] = NULL;
						} else nIndex++;
					}
				}
				else
					nIndex++;
			}

			return nReturn;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return DIRECT_ERROR;
}

int CDirectData::ApplyRule(int nRuleIndex, char* pszData, char** ppszEventName, char** ppszExplicitExtension)
{
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) 	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "** Applying rule %d to %s", nRuleIndex, pszData); // Sleep(5);//(Dispatch message)
	if((nRuleIndex>=0)&&(m_ppRulesObj)&&(nRuleIndex<m_nNumRulesObjects)&&(pszData)&&(strlen(pszData)>0))
	{
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) 	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "***  applying rule %d to %s", nRuleIndex, pszData); // Sleep(5);//(Dispatch message)
		if((m_ppRulesObj[nRuleIndex])&&(m_ppRulesObj[nRuleIndex]->m_nRuleID>0))
		{
/*
	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned long m_ulType;
	unsigned short m_usComparisonType;
	unsigned long m_ulDestinationType;
	unsigned short m_usSearchType;
	unsigned short m_usActionType;
	int m_nRuleID;

	char* m_pszFieldName;
	char* m_pszParamName;
	char* m_pszCriterion;
	char* m_pszEventLocation;
*/

/*
create table Rules (rule_type tinyint, field_name varchar(32), param_name varchar(32), comparison varchar(10), criterion varchar(64), event_location varchar(32), dest_type int, search_type tinyint, action tinyint, ruleid int IDENTITY (1, 1) NOT NULL);

------------------------------------
rule_type: databased in Direct.RuleTypes (ruletype, name)

values:
1 - Presmaster
2 - XML
3 - Imagestore

------------------------------------
comparison: stored in Direct\util.asp function name: OptionListComparison

values:
<=
>=
==
!=

------------------------------------
dest_type: databased in Prospero.LU_Destination_Types

values:
1000 - DVG
1001 - HDVG
2001 - Imagestore 2
2002 - Intuition
2003 - Imagestore 300
2004 - Imagestore HD

------------------------------------
search_type: stored in Direct\util.asp function name: OptionListSearchTypes

values:
0 - Explicit
1 - Audio
2 - Video
3 - Audio & Video

------------------------------------
action: stored in Direct\util.asp function name: OptionListActionTypes

values:
0 - Normal
1 - NULL OXT

*/
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "before applying rule %d with dest_type = %d to %s", nRuleIndex, m_ppRulesObj[nRuleIndex]->m_ulDestinationType, pszData);  Sleep(50);//(Dispatch message)

			switch(m_ppRulesObj[nRuleIndex]->m_ulType)
			{
			default:
			case DIRECT_RULE_TYPE_UNKNOWN://					0x00000000  // type unknown
				{
					return DIRECT_ERROR;
				} break;
			case DIRECT_RULE_TYPE_PRESMASTER://				0x00000001  // for Helios, with presmaster data in <data> XML field
				{
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "presmaster rule"); // Sleep(50);//(Dispatch message)
					char pszFieldName[MAX_PATH];
					char pszEndFieldName[MAX_PATH];
					if(
						  (m_ppRulesObj[nRuleIndex]->m_pszFieldName)
						&&(strlen(m_ppRulesObj[nRuleIndex]->m_pszFieldName)>0)
						)
					{
						sprintf(pszFieldName,"<%s>", m_ppRulesObj[nRuleIndex]->m_pszFieldName);
						sprintf(pszEndFieldName,"</%s>", m_ppRulesObj[nRuleIndex]->m_pszFieldName);
					}
					else
					{
						strcpy(pszFieldName, "<data>");
						strcpy(pszEndFieldName, "</data>");
					}
					if(
						  (m_ppRulesObj[nRuleIndex]->m_pszParamName)
						&&(strlen(m_ppRulesObj[nRuleIndex]->m_pszParamName)>0)
						)
					{
						char* pchBegin = strstr(pszData, pszFieldName);
						bool bParsed=false;
						while((pchBegin)&&(!bParsed))
						{
							pchBegin += strlen(pszFieldName);
							char* pchEnd = strstr(pchBegin, pszEndFieldName);
							if(pchEnd)
							{
								// search within the data to find the param name.
								// presmaster protocol is like:

								// EventName|Type:Video|Key:1
								int  nKeyer = 0;
								bool bKey = false;
								CSafeBufferUtil sbu;
								char* pchEvent = NULL;
								char pszSafeEvent[MAX_PATH];
								memset(pszSafeEvent, 0, MAX_PATH);
								char pszFieldContents[MAX_PATH];


								if((pchEnd-pchBegin)>0) pchEvent = sbu.Token(pchBegin, pchEnd-pchBegin, "|", MODE_SINGLEDELIM);
								if(pchEvent)
								{
									if(strlen(pchEvent))
									{
										strcpy(pszSafeEvent, pchEvent);
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "presmaster rule with %s", pchEvent); // Sleep(50);//(Dispatch message)
										// have a valid event name, so can continue.
										char* pch = NULL;
										while(pch==NULL)
										{
											char* pchKeyer = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM); 
											if(pchKeyer)
											{
												char pszParamName[MAX_PATH];

												strupr(pchKeyer);
												sprintf(pszParamName, "%s:", m_ppRulesObj[nRuleIndex]->m_pszParamName);
												strupr(pszParamName);
												pch = strstr(pchKeyer, pszParamName);
												if(pch)
												{
													pch+=strlen(pszParamName);
													if(strnicmp(pchKeyer, "KEY", 3)==0)
													{
														nKeyer = atoi(pch);
														bKey = true;
													}
													else
													{
														sprintf(pszFieldContents, "%s",pch );
													}
													bParsed = true;
													break;
												}
											}
											else break;
										}
									}
								}
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "presmaster rule with %s parsed %d", pszSafeEvent, bParsed);//  Sleep(50);//(Dispatch message)

								if(bParsed)
								{
									char* pchCrit = m_ppRulesObj[nRuleIndex]->m_pszCriterion;
									int nTestPassed = 0;
									if((pchCrit)&&(strlen(pchCrit)>0))
									{
										if(bKey)
										{
											if(((*pchCrit)>47)&&((*pchCrit)<58)) // its all numerical
											{
												int nValue = nKeyer;
												int nCrit  = atoi(pchCrit);
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) 	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse04: %d, %d", nValue, nCrit); // Sleep(50);//(Dispatch message)

					
												if(
														( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_EQUALS)
														&&(nValue==nCrit) )
													||( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_NOTEQUAL)
														&&(nValue!=nCrit) )
													||( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_GTOE)
														&&(nValue>=nCrit) )
													||( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_LTOE)
														&&(nValue<=nCrit) )
													) 
												{
													nTestPassed=1;
												}
												//	nTestPassed=0;
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) 	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse05: %s, %d%s%d", nTestPassed>0?"passed":"NOT passed",nValue, m_ppRulesObj[nRuleIndex]->m_usComparisonType==DIRECT_RULE_COMPARE_GTOE?">=":"<=",nCrit); // Sleep(50);//(Dispatch message)
											} // if numerical
										} // if keyer
										else // must be a text compare
										{
											if(
													( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_EQUALS)
													&&(stricmp(pszFieldContents, pchCrit)==0) )
												||( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_NOTEQUAL)
													&&(stricmp(pszFieldContents, pchCrit)!=0) )
												||( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_GTOE)
													&&(stricmp(pszFieldContents, pchCrit)<=0) )
												||( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_LTOE)
													&&(stricmp(pszFieldContents, pchCrit)>=0) )
												) 
											{
												nTestPassed=1;
											}

										}
									}  // if criterion has a length
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse05a"); // Sleep(50);//(Dispatch message)
			
									if(nTestPassed>0)
									{
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse06a: Test passed!"); // Sleep(50);//(Dispatch message)
										if((ppszEventName==NULL)&&(ppszExplicitExtension==NULL))
										{
											return DIRECT_SUCCESS;
										}
									
										// if were here, we have to parse.
										char* pchFileName = (char*)malloc(strlen(pszSafeEvent)+1);
										if(pchFileName)
										{
											sprintf(pchFileName, "%s", pszSafeEvent);
										// pchFileName should now hold the event name
											if(m_ppRulesObj[nRuleIndex]->m_usSearchType != DIRECT_RULE_SEARCH_EXPLICIT)
											{
											// now check for an extension, strip it if not explicit.
												bool bDot = true;
												char* pch = strrchr(pchFileName, '.');
												if(pch) *pch = 0; // null term it!
												else bDot= false;

												if(strlen(pchFileName)>0)
												{
													if(ppszEventName)
													{
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse08: ppszEventName %d", ppszEventName); // Sleep(50);//(Dispatch message)
														*ppszEventName = pchFileName;
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse08: %s final", *ppszEventName); // Sleep(50);//(Dispatch message)
													}
													// else free(pchReturn);
															
													if(ppszExplicitExtension)
													{
														if(bDot)
														{
															pch++;
															if(strlen(pch)>0)
															{																
																char* pchReturnExt = (char*)malloc(strlen(pch)+1);
																if(pchReturnExt)
																{
																	strcpy(pchReturnExt, pch);
																	*ppszExplicitExtension = pchReturnExt;
																}	else *ppszExplicitExtension = NULL;
															}	else *ppszExplicitExtension = NULL;
														} else *ppszExplicitExtension = NULL;
													}

													if(ppszEventName == NULL) free(pchFileName);
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse08: %s final", *ppszEventName);//  Sleep(50);//(Dispatch message)

													return DIRECT_SUCCESS;
												}
												else // strlen pchFileName
												{
													free(pchFileName);
													if(ppszEventName)
													{
														*ppszEventName = NULL;
													}
													
													if(ppszExplicitExtension)
													{
														if(bDot)
														{
															*ppszExplicitExtension = NULL;  // no filename, so just fail out.
															/*
															pch++;
															if(strlen(pch)>0)
															{
																char* pchExtReturn = (char*)malloc(strlen(pch)+1);
																if(pchExtReturn)
																{
																	strcpy(pchExtReturn, pch);
																	*ppszExplicitExtension = pchExtReturn;
																} else *ppszExplicitExtension = NULL;
															} else *ppszExplicitExtension = NULL;
															*/
														} else *ppszExplicitExtension = NULL;
													}
												}  // strlen return was 0
											}
											else  //DIRECT_RULE_SEARCH_EXPLICIT
											{
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES)
{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse31: file %s", pchReturn); // Sleep(50);//(Dispatch message)
	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse32: ppszEventName %d", ppszEventName);//  Sleep(50);//(Dispatch message)
}
												if(ppszEventName)
												{
													*ppszEventName = pchFileName;
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse33: final %s", *ppszEventName); // Sleep(50);//(Dispatch message)
												}

												if(ppszExplicitExtension)
												{
												// now check for an extension, strip it if not explicit.
													bool bDot = true;
													char* pch = strrchr(pchFileName, '.');
													if(pch) *pch = 0; // null term it!
													else bDot= false;
													if(bDot)
													{
														pch++;
														if(strlen(pch)>0)
														{																
															char* pchReturnExt = (char*)malloc(strlen(pch)+1);
															if(pchReturnExt)
															{
																strcpy(pchReturnExt, pch);
																*ppszExplicitExtension = pchReturnExt;
															}	else *ppszExplicitExtension = NULL;
														}	else *ppszExplicitExtension = NULL;
													} else *ppszExplicitExtension = NULL;
												}
													
												if(ppszEventName==NULL) free(pchFileName);

												return DIRECT_SUCCESS;
											}//DIRECT_RULE_SEARCH_EXPLICIT
										}
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse47: exiting"); // Sleep(50);//(Dispatch message)
									}//if(nTestPassed>0)
								}//parsed
							}// else no end tag, was malformed data
							pchBegin = strstr(pchBegin, pszFieldName);  // try again.
						}// while begin tag, (else non valid data)
					}// else no valid param
				} break; 
			case DIRECT_RULE_TYPE_XML://							0x00000002  // for Helios, with logo data in <logos> XML sub-fields .. free form tho
				{
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "xml rule");  Sleep(50);//(Dispatch message)

					if(
						  (m_ppRulesObj[nRuleIndex]->m_pszParamName)
						&&(strlen(m_ppRulesObj[nRuleIndex]->m_pszParamName)>0)
						&&(m_ppRulesObj[nRuleIndex]->m_pszEventLocation)
						&&(strlen(m_ppRulesObj[nRuleIndex]->m_pszEventLocation)>0)
						&&(m_ppRulesObj[nRuleIndex]->m_pszFieldName)
						&&(strlen(m_ppRulesObj[nRuleIndex]->m_pszFieldName)>0)
						)
					{
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "xml rule really");  Sleep(50);//(Dispatch message)
						char pszFieldName[MAX_PATH];
						char pszEndFieldName[MAX_PATH];
						sprintf(pszFieldName,"<%s>", m_ppRulesObj[nRuleIndex]->m_pszFieldName);
						sprintf(pszEndFieldName,"</%s>", m_ppRulesObj[nRuleIndex]->m_pszFieldName);
						char* pchBegin = strstr(pszData, pszFieldName);
						bool bParsed=false;
						while((pchBegin)&&(!bParsed))
						{
							pchBegin += strlen(pszFieldName);
							char* pchEnd = strstr(pchBegin, pszEndFieldName);
							if(pchEnd)
							{
								// search within the data to find the param name.

								// looking for <logo6><field>1</field><data>EventName</data></logo6>
								// if field = 0,  no logo
								char pszParamName[MAX_PATH];
								char pszEndParamName[MAX_PATH];
								sprintf(pszParamName,"<%s>", m_ppRulesObj[nRuleIndex]->m_pszParamName);
								sprintf(pszEndParamName,"</%s>", m_ppRulesObj[nRuleIndex]->m_pszParamName);
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "xml rule %s and %s", pszParamName,pszEndParamName);  Sleep(50);//(Dispatch message)

								char* pchBeginParam = strstr(pchBegin, pszParamName);
								if(pchBeginParam) pchBeginParam += strlen(pszParamName);
								char* pchEndParam = strstr(pchBeginParam, pszEndParamName);

								if((pchBeginParam)&&(pchEndParam)&&(pchBeginParam<=pchEndParam)&&(pchEndParam<pchEnd)) // <= because can be blank
								{
									bParsed = true;
									char pszFieldContents[MAX_PATH];
									memset(pszFieldContents,0,MAX_PATH);
									memcpy(pszFieldContents, pchBeginParam, pchEndParam-pchBeginParam);
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "xml rule %s and %s contents = %s", pszParamName,pszEndParamName, pszFieldContents);  Sleep(50);//(Dispatch message)
									char* pchCrit = m_ppRulesObj[nRuleIndex]->m_pszCriterion;
									int nTestPassed = 0;
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "xml rule %s and %s contents: %s ?= %s", pszParamName,pszEndParamName, pszFieldContents,pchCrit);  Sleep(50);//(Dispatch message)

									if((pchCrit)&&(strlen(pchCrit)>0))
									{
										if(
												( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_EQUALS)
												&&(strcmp(pchCrit, pszFieldContents)==0) )
											||( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_NOTEQUAL)
												&&(strcmp(pchCrit, pszFieldContents)!=0) )
											||( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_GTOE)
												&&(strcmp(pchCrit, pszFieldContents)>=0) )
											||( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_LTOE)
												&&(strcmp(pchCrit, pszFieldContents)<=0) )
											) 
										{
											nTestPassed=1;
										}
									}  // if criterion has a length
									else
									{
										//zero length;
										if(strlen(pszFieldContents)<=0)
										{
											if(m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_EQUALS)
												nTestPassed=1;
										}
										else
										{
											if(m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_NOTEQUAL)
												nTestPassed=1;
										}
									}
									if(nTestPassed>0)
									{
										// have to check valid event name
										if((ppszEventName==NULL)&&(ppszExplicitExtension==NULL))
										{
											return DIRECT_SUCCESS;
										}

										sprintf(pszParamName,"<%s>", m_ppRulesObj[nRuleIndex]->m_pszEventLocation);
										sprintf(pszEndParamName,"</%s>", m_ppRulesObj[nRuleIndex]->m_pszEventLocation);

										char* pchBeginParam = strstr(pchBegin, pszParamName);
										if(pchBeginParam) pchBeginParam += strlen(pszParamName);
										char* pchEndParam = strstr(pchBeginParam, pszEndParamName);

										char* pchFileName = NULL;

										if((pchBeginParam)&&(pchEndParam)&&(pchBeginParam<pchEndParam)&&(pchEndParam<pchEnd))  // only < because need a valid event name
										{
											char pszFieldContents[MAX_PATH];
											memset(pszFieldContents,0,MAX_PATH);
											memcpy(pszFieldContents, pchBeginParam, pchEndParam-pchBeginParam);
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "xml rule %s and %s contents: %s ?= %s", pszParamName,pszEndParamName, pszFieldContents,pchCrit);  Sleep(50);//(Dispatch message)
											if(strlen(pszFieldContents)>0)
											{
												pchFileName = (char*)malloc(strlen(pszFieldContents)+1);
												if(pchFileName)
												{
													sprintf(pchFileName, "%s", pszFieldContents);
												}
											}
										}

										// if were here, we have to parse.
										if(pchFileName)
										{
											if(strlen(pchFileName)>0)
											{
											// pchFileName should now hold the event name
												if(m_ppRulesObj[nRuleIndex]->m_usSearchType != DIRECT_RULE_SEARCH_EXPLICIT)
												{
												// now check for an extension, strip it if not explicit.
													bool bDot = true;
													char* pch = strrchr(pchFileName, '.');
													if(pch) *pch = 0; // null term it!
													else bDot= false;

													if(strlen(pchFileName)>0)
													{
														if(ppszEventName)
														{
		//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse08: ppszEventName %d", ppszEventName);  Sleep(50);//(Dispatch message)
															*ppszEventName = pchFileName;
		//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse08: %s final", *ppszEventName);  Sleep(50);//(Dispatch message)
														}
														// else free(pchReturn);
														
														if(ppszExplicitExtension)
														{
															if(bDot)
															{
																pch++;
																if(strlen(pch)>0)
																{																
																	char* pchReturnExt = (char*)malloc(strlen(pch)+1);
																	if(pchReturnExt)
																	{
																		strcpy(pchReturnExt, pch);
																		*ppszExplicitExtension = pchReturnExt;
																	}	else *ppszExplicitExtension = NULL;
																}	else *ppszExplicitExtension = NULL;
															} else *ppszExplicitExtension = NULL;
														}

														if(ppszEventName == NULL) free(pchFileName);
		//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse08: %s final", *ppszEventName);  Sleep(50);//(Dispatch message)

														return DIRECT_SUCCESS;
													}
													else // strlen pchFileName
													{
														free(pchFileName);
														if(ppszEventName)
														{
															*ppszEventName = NULL;
														}
														
														if(ppszExplicitExtension)
														{
															*ppszExplicitExtension = NULL;  // no filename, so just fail out.
															/*
															if(bDot)
															{
																pch++;
																if(strlen(pch)>0)
																{
																	char* pchExtReturn = (char*)malloc(strlen(pch)+1);
																	if(pchExtReturn)
																	{
																		strcpy(pchExtReturn, pch);
																		*ppszExplicitExtension = pchExtReturn;
																	} else *ppszExplicitExtension = NULL;
																} else *ppszExplicitExtension = NULL;
															} else *ppszExplicitExtension = NULL;
															*/
														}
													}  // strlen return was 0
												}
												else  //DIRECT_RULE_SEARCH_EXPLICIT
												{
		//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse31: file %s", pchReturn);  Sleep(50);//(Dispatch message)
		//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse32: ppszEventName %d", ppszEventName);  Sleep(50);//(Dispatch message)
													if(ppszEventName)
													{
														*ppszEventName = pchFileName;
		//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse33: final %s", *ppszEventName);  Sleep(50);//(Dispatch message)
													}

													if(ppszExplicitExtension)
													{
													// now check for an extension, strip it if not explicit.
														bool bDot = true;
														char* pch = strrchr(pchFileName, '.');
														if(pch) *pch = 0; // null term it!
														else bDot= false;
														if(bDot)
														{
															pch++;
															if(strlen(pch)>0)
															{																
																char* pchReturnExt = (char*)malloc(strlen(pch)+1);
																if(pchReturnExt)
																{
																	strcpy(pchReturnExt, pch);
																	*ppszExplicitExtension = pchReturnExt;
																}	else *ppszExplicitExtension = NULL;
															}	else *ppszExplicitExtension = NULL;
														} else *ppszExplicitExtension = NULL;
													}
														
													if(ppszEventName==NULL) free(pchFileName);

													return DIRECT_SUCCESS;
												}//DIRECT_RULE_SEARCH_EXPLICIT
											}
										}
									}
								}
							}// else no end tag, was malformed data
							pchBegin = strstr(pchBegin, pszFieldName);  // try again.
						}// while begin tag, (else non valid data)
					}// else no valid param				
				} break;
			case DIRECT_RULE_TYPE_IMAGESTORE://				0x00000003  // for Sentinel, without presmaster, data in title field
				{
					// Field name is in Title, so assume that is what is passed in
					if(
						  (m_ppRulesObj[nRuleIndex]->m_pszParamName)
						&&(strlen(m_ppRulesObj[nRuleIndex]->m_pszParamName)>0)
						)
					{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "applying rule %d to %s", nRuleIndex, pszData);  Sleep(50);//(Dispatch message)
						// the protocol is: we are looking for, in the string:
						// LOAD:n filename
						// LOADI:n filename
						// where n is a keyer layer number, followed by whitespace, followed by event name which may or may not have an extension

						char* pch = NULL;
						char* pchParam = (char*) malloc(strlen(m_ppRulesObj[nRuleIndex]->m_pszParamName)+2);
						if(pchParam)
						{
							sprintf(pchParam, "%s:",m_ppRulesObj[nRuleIndex]->m_pszParamName );
							pch = strstr(pszData, pchParam);
							free(pchParam);
						}
						 
						char* pchCrit = m_ppRulesObj[nRuleIndex]->m_pszCriterion;
						int nTestPassed = 0;
						if((pch)&&(pch==pszData)) // must be at beginning
						{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse00: %s, param %s, criterion = %s", pch, m_ppRulesObj[nRuleIndex]->m_pszParamName, pchCrit);  Sleep(50);//(Dispatch message)

							pch += (strlen(m_ppRulesObj[nRuleIndex]->m_pszParamName)+1); 
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse01: %s", pch);  Sleep(50);//(Dispatch message)

//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse02a: %s", pch);  Sleep(50);//(Dispatch message)

							if((pchCrit)&&(strlen(pchCrit)>0))
							{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse03: %s", pchCrit);  Sleep(50);//(Dispatch message)
								if(((*pch)>47)&&((*pch)<58)&&((*pchCrit)>47)&&((*pchCrit)<58)) // its all numerical
								{
									int nValue = atoi(pch);
									int nCrit  = atoi(pchCrit);
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse04: %d, %d", nValue, nCrit);  Sleep(50);//(Dispatch message)

	
									if(
										  ( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_EQUALS)
											&&(nValue==nCrit) )
										||( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_NOTEQUAL)
											&&(nValue!=nCrit) )
										||( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_GTOE)
											&&(nValue>=nCrit) )
										||( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_LTOE)
											&&(nValue<=nCrit) )
										) 
									{
										nTestPassed=1;
									}
									//	nTestPassed=0;

/*
									switch(m_ppRulesObj[nRuleIndex]->m_usComparisonType)
									{
									case DIRECT_RULE_COMPARE_EQUALS://				0x0001  // == equals
										{
											if(nValue==nCrit) bTestPassed=true;
										} break;
									case DIRECT_RULE_COMPARE_NOTEQUAL://			0x0002  // != equals
										{
											if(nValue!=nCrit) bTestPassed=true;
										} break;
									case DIRECT_RULE_COMPARE_GTOE://					0x0003  // >= greater than or equal to
										{
											if(nValue>=nCrit) bTestPassed=true;
										} break;
									case DIRECT_RULE_COMPARE_LTOE://					0x0004  // <= less than or equal to
										{
											if(nValue<=nCrit) bTestPassed=true;
										} break;
									default:
									case DIRECT_RULE_COMPARE_UNKNOWN://				0xffff  // type unknown
										{
										}	break;
									}  // switch
*/
//									g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse05: %s, %d%s%d", nTestPassed>0?"passed":"NOT passed",nValue, m_ppRulesObj[nRuleIndex]->m_usComparisonType==DIRECT_RULE_COMPARE_GTOE?">=":"<=",nCrit);  Sleep(50);//(Dispatch message)
								} // if numerical
							}  // if criterion has a length
							else
							{
								//zero length;
								if(((*pch)==0)||(isspace(*pch)))
								{
									if(m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_EQUALS)
										nTestPassed=1;
								}
								else
								{
									if(m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_NOTEQUAL)
										nTestPassed=1;
								}
							}

//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse05a: %s", pch);  Sleep(50);//(Dispatch message)

							if(nTestPassed>0)
							{
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse06a: %s", pch);  Sleep(50);//(Dispatch message)
								if((ppszEventName==NULL)&&(ppszExplicitExtension==NULL))
								{
									return DIRECT_SUCCESS;
								}

								// if were here, we have to parse.
								char* pchFileName = (char*)malloc(strlen(pch)+1);
								if(pchFileName)
								{
									sprintf(pchFileName, "%s", pch+1);

									while(( pchFileName[0]!=0)&&(isspace(pchFileName[0])) )
									{
										int nLen = strlen(pchFileName);
										int t=0;
										while(t<nLen)
										{
											pchFileName[t]=pchFileName[t+1]; // should get the term 0 too!
											t++;
										}
									}
									if(strlen(pchFileName)>0)
									{
									// pchFileName should now hold the event name
										if(m_ppRulesObj[nRuleIndex]->m_usSearchType != DIRECT_RULE_SEARCH_EXPLICIT)
										{
										// now check for an extension, strip it if not explicit.
											bool bDot = true;
											pch = strrchr(pchFileName, '.');
											if(pch) *pch = 0; // null term it!
											else bDot= false;

											if(strlen(pchFileName)>0)
											{
												if(ppszEventName)
												{
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse08: ppszEventName %d", ppszEventName);  Sleep(50);//(Dispatch message)
													*ppszEventName = pchFileName;
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse08: %s final", *ppszEventName);  Sleep(50);//(Dispatch message)
												}
												// else free(pchReturn);
												
												if(ppszExplicitExtension)
												{
													if(bDot)
													{
														pch++;
														if(strlen(pch)>0)
														{																
															char* pchReturnExt = (char*)malloc(strlen(pch)+1);
															if(pchReturnExt)
															{
																strcpy(pchReturnExt, pch);
																*ppszExplicitExtension = pchReturnExt;
															}	else *ppszExplicitExtension = NULL;
														}	else *ppszExplicitExtension = NULL;
													} else *ppszExplicitExtension = NULL;
												}

												if(ppszEventName == NULL) free(pchFileName);
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse08: %s final", *ppszEventName);  Sleep(50);//(Dispatch message)

												return DIRECT_SUCCESS;
											}
											else // strlen pchFileName
											{
												free(pchFileName);
												if(ppszEventName)
												{
													*ppszEventName = NULL;
												}
												
												if(ppszExplicitExtension)
												{
													*ppszExplicitExtension = NULL;  // no filename, so just fail out.
													/*
													if(bDot)
													{
														pch++;
														if(strlen(pch)>0)
														{
															char* pchExtReturn = (char*)malloc(strlen(pch)+1);
															if(pchExtReturn)
															{
																strcpy(pchExtReturn, pch);
																*ppszExplicitExtension = pchExtReturn;
															} else *ppszExplicitExtension = NULL;
														} else *ppszExplicitExtension = NULL;
													} else *ppszExplicitExtension = NULL;
													*/
												}
											}  // strlen return was 0
										}
										else  //DIRECT_RULE_SEARCH_EXPLICIT
										{
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse31: file %s", pchReturn);  Sleep(50);//(Dispatch message)
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse32: ppszEventName %d", ppszEventName);  Sleep(50);//(Dispatch message)
											if(ppszEventName)
											{
												*ppszEventName = pchFileName;
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse33: final %s", *ppszEventName);  Sleep(50);//(Dispatch message)
											}

											if(ppszExplicitExtension)
											{
											// now check for an extension, strip it if not explicit.
												bool bDot = true;
												pch = strrchr(pchFileName, '.');
												if(pch) *pch = 0; // null term it!
												else bDot= false;
												if(bDot)
												{
													pch++;
													if(strlen(pch)>0)
													{																
														char* pchReturnExt = (char*)malloc(strlen(pch)+1);
														if(pchReturnExt)
														{
															strcpy(pchReturnExt, pch);
															*ppszExplicitExtension = pchReturnExt;
														}	else *ppszExplicitExtension = NULL;
													}	else *ppszExplicitExtension = NULL;
												} else *ppszExplicitExtension = NULL;
											}
												
											if(ppszEventName==NULL) free(pchFileName);

											return DIRECT_SUCCESS;
										}//DIRECT_RULE_SEARCH_EXPLICIT
									}
								}
							}
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse47: exiting with %s", pch);  Sleep(50);//(Dispatch message)

						}
					}
				} break;
			case DIRECT_RULE_TYPE_FTP://							0x00000004  // for Sentinel and Evertz
				{
					//data consists of ID|Title

//					Field Param comparison value location
//					Device ID = SS26 Title

//					if (ID == SS26) then test passed, return title
					if(
						  (m_ppRulesObj[nRuleIndex]->m_pszCriterion)
						&&(strlen(m_ppRulesObj[nRuleIndex]->m_pszCriterion)>0)
						)
					{
						char* pch = strchr(pszData, '|');
						 
					//	char* pchCrit = m_ppRulesObj[nRuleIndex]->m_pszCriterion;
						int nTestPassed = 0;
						if((pch)&&(pszData)) 
						{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse00: %s, param %s, criterion = %s", pch, m_ppRulesObj[nRuleIndex]->m_pszParamName, pchCrit);  Sleep(50);//(Dispatch message)

						//	pch += (strlen(m_ppRulesObj[nRuleIndex]->m_pszParamName)+1); 
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse01: %s", pch);  Sleep(50);//(Dispatch message)

//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse02a: %s", pch);  Sleep(50);//(Dispatch message)

							if(1)//(pchCrit)&&(strlen(pchCrit)>0))
							{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse03: %s", pchCrit);  Sleep(50);//(Dispatch message)
								if(1)//((*pch)>47)&&((*pch)<58)&&((*pchCrit)>47)&&((*pchCrit)<58)) // its all numerical
								{
							//		int nValue = atoi(pch);
							//		int nCrit  = atoi(pchCrit);
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse04: %d, %d", nValue, nCrit);  Sleep(50);//(Dispatch message)

/*
	
									if(
										  ( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_EQUALS)
											&&(nValue==nCrit) )
										||( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_NOTEQUAL)
											&&(nValue!=nCrit) )
										||( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_GTOE)
											&&(nValue>=nCrit) )
										||( (m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_LTOE)
											&&(nValue<=nCrit) )
										) 
*/
									*pch = 0; // null term.
									pch++;


									if(strcmp(pszData, m_ppRulesObj[nRuleIndex]->m_pszCriterion))
									{
										nTestPassed=1;
									}
									//	nTestPassed=0;

/*
									switch(m_ppRulesObj[nRuleIndex]->m_usComparisonType)
									{
									case DIRECT_RULE_COMPARE_EQUALS://				0x0001  // == equals
										{
											if(nValue==nCrit) bTestPassed=true;
										} break;
									case DIRECT_RULE_COMPARE_NOTEQUAL://			0x0002  // != equals
										{
											if(nValue!=nCrit) bTestPassed=true;
										} break;
									case DIRECT_RULE_COMPARE_GTOE://					0x0003  // >= greater than or equal to
										{
											if(nValue>=nCrit) bTestPassed=true;
										} break;
									case DIRECT_RULE_COMPARE_LTOE://					0x0004  // <= less than or equal to
										{
											if(nValue<=nCrit) bTestPassed=true;
										} break;
									default:
									case DIRECT_RULE_COMPARE_UNKNOWN://				0xffff  // type unknown
										{
										}	break;
									}  // switch
*/
//									g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse05: %s, %d%s%d", nTestPassed>0?"passed":"NOT passed",nValue, m_ppRulesObj[nRuleIndex]->m_usComparisonType==DIRECT_RULE_COMPARE_GTOE?">=":"<=",nCrit);  Sleep(50);//(Dispatch message)
								} // if numerical
							}  // if criterion has a length
/*
							else
							{
								//zero length;
								if(((*pch)==0)||(isspace(*pch)))
								{
									if(m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_EQUALS)
										nTestPassed=1;
								}
								else
								{
									if(m_ppRulesObj[nRuleIndex]->m_usComparisonType == DIRECT_RULE_COMPARE_NOTEQUAL)
										nTestPassed=1;
								}
							}
*/
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse05a: %s", pch);  Sleep(50);//(Dispatch message)

							if(nTestPassed>0)
							{
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse06a: %s", pch);  Sleep(50);//(Dispatch message)
								if((ppszEventName==NULL)&&(ppszExplicitExtension==NULL))
								{
									return DIRECT_SUCCESS;
								}

								// if were here, we have to parse.
								char* pchFileName = (char*)malloc(strlen(pch)+1);
								if(pchFileName)
								{
									sprintf(pchFileName, "%s", pch+1);

									while(( pchFileName[0]!=0)&&(isspace(pchFileName[0])) )
									{
										int nLen = strlen(pchFileName);
										int t=0;
										while(t<nLen)
										{
											pchFileName[t]=pchFileName[t+1]; // should get the term 0 too!
											t++;
										}
									}
									if(strlen(pchFileName)>0)
									{
									// pchFileName should now hold the event name
										if(m_ppRulesObj[nRuleIndex]->m_usSearchType != DIRECT_RULE_SEARCH_EXPLICIT)
										{
										// now check for an extension, strip it if not explicit.
											bool bDot = true;
											pch = strrchr(pchFileName, '.');
											if(pch) *pch = 0; // null term it!
											else bDot= false;

											if(strlen(pchFileName)>0)
											{
												if(ppszEventName)
												{
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse08: ppszEventName %d", ppszEventName);  Sleep(50);//(Dispatch message)
													*ppszEventName = pchFileName;
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse08: %s final", *ppszEventName);  Sleep(50);//(Dispatch message)
												}
												// else free(pchReturn);
												
												if(ppszExplicitExtension)
												{
													if(bDot)
													{
														pch++;
														if(strlen(pch)>0)
														{																
															char* pchReturnExt = (char*)malloc(strlen(pch)+1);
															if(pchReturnExt)
															{
																strcpy(pchReturnExt, pch);
																*ppszExplicitExtension = pchReturnExt;
															}	else *ppszExplicitExtension = NULL;
														}	else *ppszExplicitExtension = NULL;
													} else *ppszExplicitExtension = NULL;
												}

												if(ppszEventName == NULL) free(pchFileName);
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse08: %s final", *ppszEventName);  Sleep(50);//(Dispatch message)

												return DIRECT_SUCCESS;
											}
											else // strlen pchFileName
											{
												free(pchFileName);
												if(ppszEventName)
												{
													*ppszEventName = NULL;
												}
												
												if(ppszExplicitExtension)
												{
													*ppszExplicitExtension = NULL;  // no filename, so just fail out.
													/*
													if(bDot)
													{
														pch++;
														if(strlen(pch)>0)
														{
															char* pchExtReturn = (char*)malloc(strlen(pch)+1);
															if(pchExtReturn)
															{
																strcpy(pchExtReturn, pch);
																*ppszExplicitExtension = pchExtReturn;
															} else *ppszExplicitExtension = NULL;
														} else *ppszExplicitExtension = NULL;
													} else *ppszExplicitExtension = NULL;
													*/
												}
											}  // strlen return was 0
										}
										else  //DIRECT_RULE_SEARCH_EXPLICIT
										{
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse31: file %s", pchReturn);  Sleep(50);//(Dispatch message)
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse32: ppszEventName %d", ppszEventName);  Sleep(50);//(Dispatch message)
											if(ppszEventName)
											{
												*ppszEventName = pchFileName;
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse33: final %s", *ppszEventName);  Sleep(50);//(Dispatch message)
											}

											if(ppszExplicitExtension)
											{
											// now check for an extension, strip it if not explicit.
												bool bDot = true;
												pch = strrchr(pchFileName, '.');
												if(pch) *pch = 0; // null term it!
												else bDot= false;
												if(bDot)
												{
													pch++;
													if(strlen(pch)>0)
													{																
														char* pchReturnExt = (char*)malloc(strlen(pch)+1);
														if(pchReturnExt)
														{
															strcpy(pchReturnExt, pch);
															*ppszExplicitExtension = pchReturnExt;
														}	else *ppszExplicitExtension = NULL;
													}	else *ppszExplicitExtension = NULL;
												} else *ppszExplicitExtension = NULL;
											}
												
											if(ppszEventName==NULL) free(pchFileName);

											return DIRECT_SUCCESS;
										}//DIRECT_RULE_SEARCH_EXPLICIT
									}
								}
							}
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse47: exiting with %s", pch);  Sleep(50);//(Dispatch message)

						}
					}
				} break;
			}
		}
	}
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) 	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "parse48: returning"); // Sleep(50);//(Dispatch message)
	return DIRECT_ERROR;
}

int CDirectData::CheckRules(char* pszData, unsigned short usAutomationType)  // just makes sure SOME rule applies.
{
	if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "checking %d rules for %s", m_nNumRulesObjects, pszData?pszData:"null");  //Sleep(50);//(Dispatch message)
	if((pszData)&&(strlen(pszData)>0)&&(m_ppRulesObj)&&(m_nNumRulesObjects>0))
	{
		int nRuleIndex = 0;
		while(nRuleIndex < m_nNumRulesObjects)
		{
			if(m_ppRulesObj[nRuleIndex])
			{
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "usAutomationType = %d, rule type = %d", usAutomationType, m_ppRulesObj[nRuleIndex]->m_ulType);  //Sleep(50);//(Dispatch message)
				if(
						(
							(usAutomationType == DIRECT_DEP_AUTO_SENTINEL)
						&&
							(
							  (m_ppRulesObj[nRuleIndex]->m_ulType == DIRECT_RULE_TYPE_IMAGESTORE)
							||(m_ppRulesObj[nRuleIndex]->m_ulType == DIRECT_RULE_TYPE_FTP)
							)
						)
					||(
							(usAutomationType == DIRECT_DEP_AUTO_HELIOS)
						&&(
								(m_ppRulesObj[nRuleIndex]->m_ulType == DIRECT_RULE_TYPE_PRESMASTER)
							||(m_ppRulesObj[nRuleIndex]->m_ulType == DIRECT_RULE_TYPE_XML)
							)
						)
					)
				{
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_RULES) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "Rule %d checking", nRuleIndex);  //Sleep(50);//(Dispatch message)

					if(ApplyRule(nRuleIndex, pszData) >= DIRECT_SUCCESS) return DIRECT_SUCCESS;
				}
			}
			nRuleIndex++;
		}
	}
	return DIRECT_ERROR;
}

int	CDirectData::GetRuleIndex(int nRuleID)
{
	if((m_ppRulesObj)&&(m_nNumRulesObjects>0))
	{
		int nRuleIndex = 0;
		while(nRuleIndex<m_nNumRulesObjects)
		{
			if(m_ppRulesObj[nRuleIndex])
			{
				if(m_ppRulesObj[nRuleIndex]->m_nRuleID == nRuleID) return nRuleIndex;
			}
			nRuleIndex++;
		}
	}
	return DIRECT_ERROR;
}

int CDirectData::ReleaseRecordSet(bool bResetIncrementor)
{
	if(m_prsEvents)  // it may already be finished out and closed
	{
		m_prsEvents->Close();
		delete m_prsEvents;
		m_prsEvents = NULL;
	}
	if (bResetIncrementor) m_nEventCheckIndex = -1;
	return DIRECT_SUCCESS;
}

int CDirectData::ReturnNumberOfRecords(char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.
	if((m_pdb)&&(m_pdbConn))
	{
		char errorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];

		// TODO have to get the count of stuff we are going to analyze... so let's make it have the same queries as the analysis select.

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT COUNT(1) AS totalcount FROM %s.dbo.%s",
				g_pdirect->m_settings.m_pszDefaultDB?g_pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
				g_pdirect->m_settings.m_pszLiveEvents?g_pdirect->m_settings.m_pszLiveEvents:"Events"   // the LiveEvents table name
			);
		
//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

		int nNumFound = 0;
		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
					CString szTemp;
					int nTemp=0;
					try
					{
						// get id!
						prs->GetFieldValue("totalcount", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							nTemp = atoi(szTemp);
						}
					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
							if(m_pmsgr)
							{
								char errorstring[DB_ERRORSTRING_LEN];
								_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
								Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
							}
*/
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							// The error code is in e->m_nRetCode
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
						}
						else 
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
						}
						e->Delete();
					} 
					catch( ... )
					{
					}

					prs->Close();
					delete prs;
					prs = NULL;

					if(nTemp>=0)
					{
						LeaveCriticalSection(&m_critSQL);
						return nTemp;
					}

				// prs->MoveNext();
			} //if(!prs->IsEOF())
			if(prs)
			{
				prs->Close();
				delete prs;
				prs = NULL;
			}	
		}
		else  //else prs null
		{
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
		}
		LeaveCriticalSection(&m_critSQL);

	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}

	return -1;
}



int CDirectData::ReturnModuleIndex(CDirectEventObject* pObj, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.

	if(pObj)
	{
		int nDestModule=0;

		while(nDestModule<g_pdirect->m_settings.m_nNumEndpointsInstalled)
		{
			if((g_pdirect->m_settings.m_ppEndpointObject)&&(g_pdirect->m_settings.m_ppEndpointObject[nDestModule]))
			{
				if(g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType&DIRECT_DEP_EDGE_MASK)
				{
					if(pObj->m_sz_vc64_module_dbname.CompareNoCase(g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName)==0)
					{
						// this is it.
						return nDestModule;
						break;
					}
				}
			}
			nDestModule++;
		}
	}
	return -1;
}

DiskSpaceObject_t*  CDirectData::ReturnDestinationDiskSpace(CDirectEventObject* pObj, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.

	int nDestModule=ReturnModuleIndex(pObj, pszInfo);

	if(
		  (pObj)&&(m_pdb)&&(m_pdbConn)
			&&(nDestModule>=0)
			&&(g_pdirect->m_settings.m_nNumEndpointsInstalled>0)
			&&(nDestModule<g_pdirect->m_settings.m_nNumEndpointsInstalled)
			&&(g_pdirect->m_settings.m_ppEndpointObject)
			&&(g_pdirect->m_settings.m_ppEndpointObject[nDestModule])
		)
	{
//		ReleaseRecordSet(); // have to release so we can get new results.
		char errorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		CString szTemp = "Destinations";  // we didnt't find it. just use the default name
		if(nDestModule>=0) szTemp = g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination?g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination:"Destinations";

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT diskspace_free, diskspace_total, diskspace_threshold, (1.00 - cast((diskspace_free/case when diskspace_total = 0 then 1 else diskspace_total end) AS decimal(3,2))) * 100 AS percentutil FROM %s.dbo.%s WHERE destinationid = %s",
				pObj->m_sz_vc64_module_dbname,			// the edge device DB name
				szTemp,
				pObj->m_sz_n_destinationid
			);
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "ReturnDestinationDiskSpace SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

		int nNumFound = 0;
		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				DiskSpaceObject_t* pDiskSpaceReturn = new DiskSpaceObject_t;
				if(pDiskSpaceReturn)
				{
					pDiskSpaceReturn->dblDiskFree=-1;
					pDiskSpaceReturn->dblDiskTotal=-1;
					pDiskSpaceReturn->dblDiskThreshold=-1;
					pDiskSpaceReturn->dblDiskPercentUtilized=-1;

					double dblTemp;
					try
					{
						// get id!
						prs->GetFieldValue("diskspace_free", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							dblTemp = atof(szTemp);
							if(dblTemp>=0) pDiskSpaceReturn->dblDiskFree=dblTemp;
						}

						prs->GetFieldValue("diskspace_total", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							dblTemp = atof(szTemp);
							if(dblTemp>=0) pDiskSpaceReturn->dblDiskTotal=dblTemp;
						}

						prs->GetFieldValue("diskspace_threshold", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							dblTemp = atof(szTemp);
							if(dblTemp>=0) pDiskSpaceReturn->dblDiskThreshold=dblTemp;
						}

						prs->GetFieldValue("percentutil", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							dblTemp = atof(szTemp);
							if(dblTemp>=0) pDiskSpaceReturn->dblDiskPercentUtilized=dblTemp;
						}
					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
							if(m_pmsgr)
							{
								char errorstring[DB_ERRORSTRING_LEN];
								_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
								Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
							}
*/
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							// The error code is in e->m_nRetCode
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
						}
						else 
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
						}
						e->Delete();
					} 
					catch( ... )
					{
					}

					prs->Close();
					delete prs;
					prs = NULL;
					LeaveCriticalSection(&m_critSQL);

					return pDiskSpaceReturn;
				} 
				else 
				{
					if(pszInfo) sprintf(pszInfo, "Error allocating disk space info object");
				}

				// prs->MoveNext();
			} //if(!prs->IsEOF())
		
			prs->Close();
			delete prs;
			prs = NULL;
		}
		else  //else prs null
		{
//g_pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "ReturnDestinationDiskSpace error: %s", errorstring);  Sleep(50);//(Dispatch message)
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
		}
		LeaveCriticalSection(&m_critSQL);

	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return NULL;

} 

CFileMetaDataObject*  CDirectData::ReturnFileMetaDataObject(char* pszFilename, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.

	if( (pszFilename)
			&&(strlen(pszFilename))
			&&(m_nIndexMetadataEndpoint>=0)
			&&(g_pdirect->m_settings.m_ppEndpointObject)
			&&(g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint])
			&&(m_pdb)&&(m_pdbConn)
		 )
	{
//		ReleaseRecordSet(); // have to release so we can get new results.
		char errorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];

		char* pszEncodedFilename = m_pdb->EncodeQuotes(pszFilename);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT sys_filename, sys_filepath, sys_description, \
sys_operator, sys_type, sys_duration, sys_valid_from, sys_expires_after, sys_ingest_date, sys_file_flags, \
sys_file_size, sys_file_timestamp, sys_created_on, sys_created_by, sys_last_modified_on, sys_last_modified_by, \
direct_last_used, direct_times_used FROM %s.dbo.%s WHERE sys_filename = '%s' \
AND sys_file_flags <> %d AND sys_file_flags <> %d order by sys_last_modified_on desc",
				g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
				g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the LiveEvents table name
				pszEncodedFilename?pszEncodedFilename:pszFilename,
				ARCHIVIST_FILEFLAG_ARCHIVED,
				ARCHIVIST_FILEFLAG_DELETED
			);
		if(pszEncodedFilename) free(pszEncodedFilename);
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "ReturnFileMetaDataObject SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

		int nNumFound = 0;
		if(prs != NULL) 
		{
			CFileMetaDataObject* pFileObjectReturn = NULL;
			if(!prs->IsEOF()) pFileObjectReturn = new CFileMetaDataObject;
			if(pFileObjectReturn)
			{
				while(!prs->IsEOF())
				{ 
					if(nNumFound>0)
					{
						// non unique!
						pFileObjectReturn->m_bUnique = false;
						break;
					}

					nNumFound++;
					CString szTemp;
					try
					{
						prs->GetFieldValue("sys_filename", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_vc256_sys_filename, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_filepath", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_vc256_sys_filepath, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

/*						prs->GetFieldValue("sys_linked_file", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_vc256_sys_linked_file, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}
*/
						prs->GetFieldValue("sys_description", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_vc256_sys_description, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_operator", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_vc64_sys_operator, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_type", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_type, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_duration", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_duration, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_valid_from", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_valid_from, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_expires_after", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_expires_after, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_ingest_date", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_ingest_date, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_file_flags", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_file_flags, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_file_size", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_dbl_sys_file_size, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_file_timestamp", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_dbl_sys_file_timestamp, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_created_on", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_created_on, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_created_by", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_vc32_sys_created_by, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_last_modified_on", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_last_modified_on, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_last_modified_by", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_vc32_sys_last_modified_by, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("direct_last_used", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_direct_last_used, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("direct_times_used", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_direct_times_used, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
g_pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);

/*
							if(m_pmsgr)
							{
								char errorstring[DB_ERRORSTRING_LEN];
								_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
								Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
							}
*/
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							// The error code is in e->m_nRetCode
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
g_pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "Retrieve: Caught exception: out of memory.\n%s", szSQL);
						}
						else 
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
g_pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "Retrieve: Caught other exception.\n%s", szSQL);
						}
						e->Delete();
					} 
					catch( ... )
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
g_pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "Retrieve: Caught exception.\n%s", szSQL);
					}

					prs->MoveNext();
				} //while not eof
				
				prs->Close();
				delete prs;
				prs = NULL;
				LeaveCriticalSection(&m_critSQL);
				return pFileObjectReturn;
			}
			else 
			{
				if(pszInfo) sprintf(pszInfo, "Error allocating file metadata object");
			}
			prs->Close();
			delete prs;
			prs = NULL;
		}
		else  //else prs null
		{
g_pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "ReturnFileMetaDataObject ERROR: %s", errorstring);  //(Dispatch message)
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
		}
		LeaveCriticalSection(&m_critSQL);
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return NULL;

} 

CDestinationMediaObject*  CDirectData::ReturnDestinationMediaObjectToDelete(CDirectEventObject* pObj, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.

	int nDestModule=ReturnModuleIndex(pObj, pszInfo);

	if(
		  (pObj)&&(m_pdb)&&(m_pdbConn)
			&&(nDestModule>=0)
			&&(g_pdirect->m_settings.m_nNumEndpointsInstalled>1)
			&&(nDestModule<g_pdirect->m_settings.m_nNumEndpointsInstalled)
			&&(g_pdirect->m_settings.m_ppEndpointObject)
			&&(g_pdirect->m_settings.m_ppEndpointObject[nDestModule])
			&&(m_nIndexMetadataEndpoint>=0)
			&&(m_nIndexMetadataEndpoint<g_pdirect->m_settings.m_nNumEndpointsInstalled)
			&&(g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint])
		)
	{
	// use db data to figure out which is the statistical best file to delete.
		char pszFilename[MAX_PATH];
		char pszPartition[MAX_PATH];
		strcpy(pszFilename, "");
		memset(pszPartition, 0, MAX_PATH);

		if(g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType == DIRECT_DEP_EDGE_PROSPERO)
		{
			if(pObj->m_nCurrentFileSearch < 0)
			{ // main file
				if((pObj->m_pszMainFile)&&(strlen(pObj->m_pszMainFile)))
				{
					char* pchPartition = strchr(pObj->m_pszMainFile, '/'); // have to remove partition.
					if(pchPartition)
					{
						strcpy(pszFilename, pchPartition+1);
						strncpy(pszPartition, pObj->m_pszMainFile, pchPartition-pObj->m_pszMainFile);
					}
				}
			}
			else
			if(pObj->m_nCurrentFileSearch < pObj->m_nNumChildren)
			{
				if((pObj->m_ppszChildren)&&(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch])&&(strlen(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch])))
				{
					char* pchPartition = strchr(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch], '/'); // have to remove partition.
					if(pchPartition)
					{
						strcpy(pszFilename, pchPartition+1);
						strncpy(pszPartition, pObj->m_ppszChildren[pObj->m_nCurrentFileSearch], pchPartition-(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch]));
					}
				}
			}

			if(strlen(pszFilename)) // we arent actually using filename, but we needed it to get the partition from which to delete...
			{
//				ReleaseRecordSet(); // have to release so we can get new results.

				// first go for the ones with an expiry date!
				_timeb timestamp;
				_ftime( &timestamp );

				char errorstring[DB_ERRORSTRING_LEN];
				char szSQL[DB_SQLSTRING_MAXLEN];
				CString szTemp = "Destinations_Media";  // we didn't find it. just use the default name
				if(nDestModule>=0) szTemp = g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestinationMedia?g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestinationMedia:"Destinations_Media";

				CString szMetadata = "File_Metadata";
				if(m_nIndexMetadataEndpoint>=0) 
					szMetadata = g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata";

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 destinations_media.file_name FROM %s.dbo.%s as file_metadata \
JOIN %s.dbo.%s as destinations_media ON file_metadata.sys_filename = destinations_media.file_name \
WHERE (destinations_media.host = '%s' and destinations_media.partition = '%s' \
AND (destinations_media.direct_local_last_used < %d \
AND destinations_media.direct_local_last_used IS NOT NULL) \
AND (file_metadata.sys_ingest_date > 0 AND file_metadata.sys_ingest_date IS NOT NULL)) \
AND (file_metadata.sys_expires_after < %d OR file_metadata.sys_file_flags = %d) \
ORDER BY destinations_media.file_size DESC, destinations_media.direct_local_last_used ASC",

						g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",
						szMetadata,
						pObj->m_sz_vc64_module_dbname,			// the edge device DB name
						szTemp,
						pObj->m_sz_vc64_dest_host,
						pszPartition,
						(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0) -(g_pdirect->m_settings.m_ulDeletionThreshold) ), // local time....//[2 days ago local unix time]
						(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0) -(g_pdirect->m_settings.m_ulDeletionThreshold) ), // local time....//[2 days ago local unix time]
	//					time(NULL)-(g_pdirect->m_settings.m_ulDeletionThreshold), //[2 days ago unix time]
	//					time(NULL)-(g_pdirect->m_settings.m_ulDeletionThreshold),  //[2 days ago unix time]
						ARCHIVIST_FILEFLAG_DELETED
					);
		
				CDestinationMediaObject* pDestMediaReturn = new CDestinationMediaObject;
				if(pDestMediaReturn)
				{
					EnterCriticalSection(&m_critSQL);
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "ReturnDestinationMediaObjectToDelete SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
					CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

					int nNumFound = 0;
					if(prs != NULL) 
					{
						if(!prs->IsEOF())
						{
							try
							{
								prs->GetFieldValue("file_name", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								if(szTemp.GetLength())
								{
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc256_file_name, szTemp.GetBuffer(1));
									szTemp.ReleaseBuffer();
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc16_partition, pszPartition);
									nNumFound++;
								}
							}
							catch(CException *e)// CDBException *e, CMemoryException *m)  
							{
								if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
		/*
									if(m_pmsgr)
									{
										char errorstring[DB_ERRORSTRING_LEN];
										_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
										Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
									}
		*/
								}
								else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
								{
									// The error code is in e->m_nRetCode
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
		//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
								}
								else 
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
		//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
								}
								e->Delete();
							} 
							catch( ... )
							{
							}

							if(nNumFound>0)
							{
								prs->Close();
								delete prs;
								prs = NULL;

								LeaveCriticalSection(&m_critSQL);
								return pDestMediaReturn;
							}

						// prs->MoveNext();
						} //if(!prs->IsEOF())
					
						prs->Close();
						delete prs;
						prs = NULL;


						// if we are here, we didnt find one to return...
// so, now try... 2.(least used by direct) order by direct_times_used asc, file_size desc where last used by direct last used is null or more than 2 days old and times_used is not null.

						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 destinations_media.file_name FROM %s.dbo.%s as file_metadata \
JOIN %s.dbo.%s as destinations_media ON file_metadata.sys_filename = destinations_media.file_name \
WHERE (destinations_media.host = '%s' and destinations_media.partition = '%s' \
AND (destinations_media.direct_local_last_used < %d \
AND destinations_media.direct_local_last_used IS NOT NULL) \
AND (file_metadata.sys_ingest_date > 0 AND file_metadata.sys_ingest_date IS NOT NULL)) \
ORDER BY destinations_media.direct_local_last_used ASC, destinations_media.file_size DESC",

								g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",
								szMetadata,
								pObj->m_sz_vc64_module_dbname,			// the edge device DB name
								szTemp,
								pObj->m_sz_vc64_dest_host,
								pszPartition,
								(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0) -(g_pdirect->m_settings.m_ulDeletionThreshold) ) // local time....//[2 days ago local unix time]
//								time(NULL)-(g_pdirect->m_settings.m_ulDeletionThreshold) //[2 days ago unix time]
							);
							
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "ReturnDestinationMediaObjectToDelete(2) SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
						prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
						if(prs != NULL) 
						{
							if(!prs->IsEOF())
							{
								try
								{
									prs->GetFieldValue("file_name", szTemp);//HARDCODE
									szTemp.TrimLeft(); szTemp.TrimRight();
									if(szTemp.GetLength())
									{
										pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc256_file_name, szTemp.GetBuffer(1));
										szTemp.ReleaseBuffer();
										pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc16_partition, pszPartition);
										nNumFound++;
									}
								}
								catch(CException *e)// CDBException *e, CMemoryException *m)  
								{
									if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
									{
										if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
			/*
										if(m_pmsgr)
										{
											char errorstring[DB_ERRORSTRING_LEN];
											_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
											Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
										}
			*/
									}
									else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
									{
										// The error code is in e->m_nRetCode
										if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
			//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
									}
									else 
									{
										if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
			//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
									}
									e->Delete();
								} 
								catch( ... )
								{
								}

								if(nNumFound>0)
								{
									prs->Close();
									delete prs;
									prs = NULL;
									LeaveCriticalSection(&m_critSQL);
									return pDestMediaReturn;
								}

							// prs->MoveNext();
							} //if(!prs->IsEOF())
						
							prs->Close();
							delete prs;
							prs = NULL;
						// if we are here, we STILL didnt find one to return...
// so, now try... 3. oldest transfer_date  

							if(g_pdirect->m_settings.m_bAutoDeleteOldest)
							{
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 destinations_media.file_name FROM %s.dbo.%s as file_metadata \
JOIN %s.dbo.%s as destinations_media ON file_metadata.sys_filename = destinations_media.file_name \
WHERE (destinations_media.host = '%s' and destinations_media.partition = '%s' \
AND (destinations_media.direct_local_last_used < %d \
AND destinations_media.direct_local_last_used IS NOT NULL) \
AND (file_metadata.sys_ingest_date > 0 AND file_metadata.sys_ingest_date IS NOT NULL)) \
ORDER BY destinations_media.transfer_date DESC",
									g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",
									szMetadata,
									pObj->m_sz_vc64_module_dbname,			// the edge device DB name
									szTemp,
									pObj->m_sz_vc64_dest_host,
									pszPartition,
									(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0) -(g_pdirect->m_settings.m_ulDeletionThreshold) ) // local time....//[2 days ago local unix time]
//									time(NULL)-(g_pdirect->m_settings.m_ulDeletionThreshold) //[2 days ago unix time]
									);
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "ReturnDestinationMediaObjectToDelete(3) SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
								prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

								if(prs != NULL) 
								{
									if(!prs->IsEOF())
									{
										try
										{
											prs->GetFieldValue("file_name", szTemp);//HARDCODE
											szTemp.TrimLeft(); szTemp.TrimRight();
											if(szTemp.GetLength())
											{
												pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc256_file_name, szTemp.GetBuffer(1));
												szTemp.ReleaseBuffer();
												pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc16_partition, pszPartition);
												nNumFound++;
											}
										}
										catch(CException *e)// CDBException *e, CMemoryException *m)  
										{
											if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
											{
												if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
					/*
												if(m_pmsgr)
												{
													char errorstring[DB_ERRORSTRING_LEN];
													_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
													Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
												}
					*/
											}
											else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
											{
												// The error code is in e->m_nRetCode
												if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
					//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
											}
											else 
											{
												if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
					//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
											}
											e->Delete();
										} 
										catch( ... )
										{
										}

										if(nNumFound>0)
										{
											prs->Close();
											delete prs;
											prs = NULL;

											LeaveCriticalSection(&m_critSQL);
											return pDestMediaReturn;
										}

									// prs->MoveNext();
									} //if(!prs->IsEOF())
								
									prs->Close();
									delete prs;
									prs = NULL;
								}
								else  //else prs null
								{
//g_pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "ReturnDestinationMediaObjectToDelete(2) ERROR: %s", errorstring);  Sleep(50);//(Dispatch message)
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
								}
							}
						}
						else  //else prs null
						{
//g_pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "ReturnDestinationMediaObjectToDelete(1) ERROR: %s", errorstring);  Sleep(50);//(Dispatch message)
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
						}

					}
					else  //else prs null
					{
//g_pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "ReturnDestinationMediaObjectToDelete ERROR: %s", errorstring);  Sleep(50);//(Dispatch message)
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
					}
					LeaveCriticalSection(&m_critSQL);
				} // allocated the object.
				else 
				{
					if(pszInfo) sprintf(pszInfo, "Error allocating destination media object");
				}

			}
			else
			{
				if(pszInfo) sprintf(pszInfo, "Invalid filename");
			}
		}
		else
		{
			if(pszInfo) sprintf(pszInfo, "Unsupported device.");
		}
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return NULL;

}

CDestinationMediaObject*  CDirectData::ReturnDestinationMediaObject(CDirectEventObject* pObj, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.

	int nDestModule=ReturnModuleIndex(pObj, pszInfo);

	if(
		  (pObj)&&(m_pdb)&&(m_pdbConn)
			&&(nDestModule>=0)
			&&(g_pdirect->m_settings.m_nNumEndpointsInstalled>0)
			&&(nDestModule<g_pdirect->m_settings.m_nNumEndpointsInstalled)
			&&(g_pdirect->m_settings.m_ppEndpointObject)
			&&(g_pdirect->m_settings.m_ppEndpointObject[nDestModule])
		)
	{
		char pszFilename[MAX_PATH];
		strcpy(pszFilename, "");
		if(g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_usType == DIRECT_DEP_EDGE_PROSPERO)
		{
			if(pObj->m_nCurrentFileSearch < 0)
			{ // main file
				if((pObj->m_pszMainFile)&&(strlen(pObj->m_pszMainFile)))
				{
					char* pchPartition = strchr(pObj->m_pszMainFile, '/'); // have to remove partition.
					if(pchPartition) strcpy(pszFilename, pchPartition+1);
				}
			}
			else
			if(pObj->m_nCurrentFileSearch < pObj->m_nNumChildren)
			{
				if((pObj->m_ppszChildren)&&(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch])&&(strlen(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch])))
				{
					char* pchPartition = strchr(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch], '/'); // have to remove partition.
					if(pchPartition) strcpy(pszFilename, pchPartition+1);
				}
			}

			if(strlen(pszFilename))
			{
//				ReleaseRecordSet(); // have to release so we can get new results.
				char errorstring[DB_ERRORSTRING_LEN];
				char szSQL[DB_SQLSTRING_MAXLEN];
				CString szTemp = "Destinations_Media";  // we didn't find it. just use the default name
				if(nDestModule>=0) szTemp = g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestinationMedia?g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestinationMedia:"Destinations_Media";
				char* pszEncodedFilename = m_pdb->EncodeQuotes(pszFilename);
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s.dbo.%s WHERE filename = %s AND host = '%s'",
						pObj->m_sz_vc64_module_dbname,			// the edge device DB name
						szTemp,
						pszEncodedFilename?pszEncodedFilename:pszFilename,
						pObj->m_sz_vc64_dest_host
					);
				if(pszEncodedFilename) free(pszEncodedFilename);
			
				EnterCriticalSection(&m_critSQL);
				CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

				int nNumFound = 0;
				if(prs != NULL) 
				{
					if(!prs->IsEOF())
					{
						CDestinationMediaObject* pDestMediaReturn = new CDestinationMediaObject;
						if(pDestMediaReturn)
						{
							try
							{
								prs->GetFieldValue("file_name", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								if(szTemp.GetLength())
								{
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc256_file_name, szTemp.GetBuffer(1));
									szTemp.ReleaseBuffer();
								}

								prs->GetFieldValue("transfer_date", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								if(szTemp.GetLength())
								{
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_n_transfer_date, szTemp.GetBuffer(1));
									szTemp.ReleaseBuffer();
								}

								prs->GetFieldValue("partition", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								if(szTemp.GetLength())
								{
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc16_partition, szTemp.GetBuffer(1));
									szTemp.ReleaseBuffer();
								}

								prs->GetFieldValue("file_size", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								if(szTemp.GetLength())
								{
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_dbl_file_size, szTemp.GetBuffer(1));
									szTemp.ReleaseBuffer();
								}

								prs->GetFieldValue("Direct_local_last_used", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								if(szTemp.GetLength())
								{
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_n_Direct_local_last_used, szTemp.GetBuffer(1));
									szTemp.ReleaseBuffer();
								}

								prs->GetFieldValue("Direct_local_times_used", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								if(szTemp.GetLength())
								{
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_n_Direct_local_times_used, szTemp.GetBuffer(1));
									szTemp.ReleaseBuffer();
								}

							}
							catch(CException *e)// CDBException *e, CMemoryException *m)  
							{
								if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
		/*
									if(m_pmsgr)
									{
										char errorstring[DB_ERRORSTRING_LEN];
										_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
										Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
									}
		*/
								}
								else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
								{
									// The error code is in e->m_nRetCode
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
		//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
								}
								else 
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
		//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
								}
								e->Delete();
							} 
							catch( ... )
							{
							}

							prs->Close();
							delete prs;
							prs = NULL;
							LeaveCriticalSection(&m_critSQL);

							return pDestMediaReturn;
						} 
						else 
						{
							if(pszInfo) sprintf(pszInfo, "Error allocating destination media object");
						}

						// prs->MoveNext();
					} //if(!prs->IsEOF())
				
					prs->Close();
					delete prs;
					prs = NULL;
				}
				else  //else prs null
				{
					if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
				}
				LeaveCriticalSection(&m_critSQL);

			}
			else
			{
				if(pszInfo) sprintf(pszInfo, "Invalid filename");
			}
		}
		else
		{
			if(pszInfo) sprintf(pszInfo, "Unsupported device.");
		}
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return NULL;
} 

unsigned long CDirectData::ReturnDestinationFlags(CDirectEventObject* pObj, char* pszInfo)
{
#ifdef USE_CHANNEL_DEST_VIEW
	_ftime( &m_timebTick );  // we're still alive.
	if(pObj)
	{
		return IsChannelDestDestinationIDActive(pObj->m_sz_n_destinationid);
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return DIRECT_FLAGS_INVALID;
#else

	_ftime( &m_timebTick );  // we're still alive.

	int nDestModule=ReturnModuleIndex(pObj, pszInfo);

	if(
		  (pObj)&&(m_pdb)&&(m_pdbConn)
			&&(nDestModule>=0)
			&&(g_pdirect->m_settings.m_nNumEndpointsInstalled>0)
			&&(nDestModule<g_pdirect->m_settings.m_nNumEndpointsInstalled)
			&&(g_pdirect->m_settings.m_ppEndpointObject)
			&&(g_pdirect->m_settings.m_ppEndpointObject[nDestModule])
		)
	{
//		ReleaseRecordSet(); // have to release so we can get new results.
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];

		CString szTemp = "Destinations";  // we didnt't find it. just use the default name
		if(nDestModule>=0) szTemp = g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination?g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination:"Destinations";
		
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT flags FROM %s.dbo.%s WHERE destinationid = %s",
			pObj->m_sz_vc64_module_dbname,			// the edge device DB name
			szTemp,
			pObj->m_sz_n_destinationid
			);
		
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				CString szTemp;
				unsigned long ulTemp = DIRECT_FLAGS_INVALID;
				try
				{
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						ulTemp = atol(szTemp);
					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				prs->Close();
				delete prs;
				prs = NULL;
				LeaveCriticalSection(&m_critSQL);

				return ulTemp;
				// prs->MoveNext();
			} //if(!prs->IsEOF())
		
			prs->Close();
			delete prs;
			prs = NULL;
		}
		else  //else prs null
		{
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
		}			
		LeaveCriticalSection(&m_critSQL);

	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return DIRECT_FLAGS_INVALID;
#endif USE_CHANNEL_DEST_VIEW

}


unsigned long CDirectData::ReturnChannelFlags(CDirectEventObject* pObj, char* pszInfo)
{
//create table Channels (ID int NOT NULL identity(1,1), flags int NOT NULL, 
//description varchar(256), server varchar(32), listid int);
#ifdef USE_CHANNEL_DEST_VIEW
	_ftime( &m_timebTick );  // we're still alive.
	if(pObj)
	{
		if(IsChannelDestChannelActive(pObj->m_sz_n_channelid))
			return DIRECT_FLAG_ENABLED;
		else return DIRECT_FLAG_DISABLED;
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return DIRECT_FLAGS_INVALID;
#else
	_ftime( &m_timebTick );  // we're still alive.
	if(
		  (pObj)&&(m_pdb)&&(m_pdbConn)
			&&(m_nIndexAutomationEndpoint>=0)
			&&(g_pdirect->m_settings.m_nNumEndpointsInstalled>0)
			&&(m_nIndexAutomationEndpoint<g_pdirect->m_settings.m_nNumEndpointsInstalled)
			&&(g_pdirect->m_settings.m_ppEndpointObject)
			&&(g_pdirect->m_settings.m_ppEndpointObject[m_nIndexAutomationEndpoint])
		)
	{
//		ReleaseRecordSet(); // have to release so we can get new results.
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT flags FROM %s.dbo.%s WHERE ID = %s",
			g_pdirect->m_settings.m_ppEndpointObject[m_nIndexAutomationEndpoint]->m_pszDBName?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexAutomationEndpoint]->m_pszDBName:"Helios",			// the edge device DB name could be Sentinel too. hmm on this default.
			g_pdirect->m_settings.m_ppEndpointObject[m_nIndexAutomationEndpoint]->m_pszChannel?g_pdirect->m_settings.m_ppEndpointObject[m_nIndexAutomationEndpoint]->m_pszChannel:"Channels",   // the channels table name
			pObj->m_sz_n_channelid
			);
		
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				CString szTemp;
				unsigned long ulTemp = DIRECT_FLAGS_INVALID;
				try
				{
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						ulTemp = atol(szTemp);
					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				prs->Close();
				delete prs;
				prs = NULL;
				LeaveCriticalSection(&m_critSQL);

				return ulTemp;
				// prs->MoveNext();
			} //if(!prs->IsEOF())
		
			prs->Close();
			delete prs;
			prs = NULL;
		}
		else  //else prs null
		{
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
		}
		LeaveCriticalSection(&m_critSQL);
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return DIRECT_FLAGS_INVALID;
#endif USE_CHANNEL_DEST_VIEW

}

char* CDirectData::ReturnPartition(CDirectEventObject* pObj, char* pszFilename, char* pszInfo)
{
#ifdef USE_FILE_MAPS_IN_MEM

	_ftime( &m_timebTick );  // we're still alive.

	char* pchExt = NULL;
	int nDestModule=-1;
	
	if((pszFilename)&&(strlen(pszFilename))) pchExt = strrchr(pszFilename, '.');
	if(pchExt)
	{ 
		pchExt++;
		nDestModule=ReturnModuleIndex(pObj, pszInfo);
	}

	if((pObj)&&(pchExt)&&(strlen(pchExt))&&(nDestModule>=0)&&(nDestModule<g_pdirect->m_settings.m_nNumEndpointsInstalled)
		&&(g_pdirect->m_settings.m_ppEndpointObject)&&(g_pdirect->m_settings.m_ppEndpointObject[nDestModule]))
	{
		if((g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_nNumMaps>0)&&(g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_ppMap))
		{
			int ix=0;
			while(ix<g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_nNumMaps)
			{
				if(
					  (g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_ppMap[ix])
					&&(g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_ppMap[ix]->m_pszExt)
					&&(stricmp(g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_ppMap[ix]->m_pszExt, pchExt)==0)
					)
				{
					if(g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_ppMap[ix]->m_pszDir)
					{
						pchExt = (char*)malloc(strlen(g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_ppMap[ix]->m_pszDir)+1);
						if(pchExt)
						{
							sprintf(pchExt, "%s", g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_ppMap[ix]->m_pszDir);
							return pchExt;
						}
						else
						{
							if(pszInfo) strcpy(pszInfo, "Error allocating buffer"); return NULL;
						}
					}
					else
					{
						if(pszInfo) strcpy(pszInfo, "Partition buffer was null"); return NULL;
					}
				}
				ix++;
			}
		}
		// if we are here, there was nothing!
		if(pszInfo) strcpy(pszInfo, "No partition information was returned");
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return NULL;


#else
	_ftime( &m_timebTick );  // we're still alive.

	char* pchExt = NULL;
	int nDestModule=-1;
	
	if((pszFilename)&&(strlen(pszFilename))) pchExt = strrchr(pszFilename, '.');
	if(pchExt)
	{ 
		pchExt++;
		nDestModule=ReturnModuleIndex(pObj, pszInfo);
	}
	if((pObj)&&(m_pdb)&&(m_pdbConn)&&(pchExt)&&(strlen(pchExt))&&(nDestModule>=0)&&(nDestModule<g_pdirect->m_settings.m_nNumEndpointsInstalled)
		&&(g_pdirect->m_settings.m_ppEndpointObject)&&(g_pdirect->m_settings.m_ppEndpointObject[nDestModule]))
	{
//		ReleaseRecordSet(); // have to release so we can get new results.
		char errorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];


		char* pchEncodedExt = m_pdb->EncodeQuotes(pchExt);
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT partition FROM %s.dbo.%s WHERE criterion = '%s'",
				g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName?g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName:"Propsero", 
				g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszFileTypes?g_pdirect->m_settings.m_ppEndpointObject[nDestModule]->m_pszFileTypes:"FileTypeMapping",  // edge device DB and table name;
				pchEncodedExt?pchEncodedExt:pchExt
			);
		if(pchEncodedExt) free(pchEncodedExt);
		
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

		int nNumFound = 0;
		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				CString szTemp="";
				try
				{
					prs->GetFieldValue("partition", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				if(szTemp.GetLength()>0)
				{
					pchExt = (char*)malloc(szTemp.GetLength()+1);
					if(pchExt)
					{
						sprintf(pchExt, "%s", szTemp);

						prs->Close();
						delete prs;
						prs = NULL;
						LeaveCriticalSection(&m_critSQL);

						return pchExt;
					}
					else
					{
						if(pszInfo) strcpy(pszInfo, "Error allocating buffer");
					}

				}
				else
				{
					if(pszInfo) strcpy(pszInfo, "No partition information was returned");
				}
				// prs->MoveNext();
			} //if(!prs->IsEOF())
		
			prs->Close();
			delete prs;
			prs = NULL;
		}
		else  //else prs null
		{
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
		}
		LeaveCriticalSection(&m_critSQL);

	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return NULL;
#endif // USE_FILE_MAPS_IN_MEM
} 

CDirectQueueObject* CDirectData::ReturnFromEndpointQueue(CDirectEventObject* pObj, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.
	if((pObj)&&(m_pdb)&&(m_pdbConn))
	{
//		ReleaseRecordSet(); // have to release so we can get new results.
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s.dbo.%s WHERE event_itemid = %s and username = '�%s�'",
				pObj->m_sz_vc64_module_dbname,			// the edge device DB name
				pObj->m_sz_vc64_module_dbqueue,   // the Queue table name
//				pObj->m_sz_n_event_itemid // is actually the automation item's id, so not necessarily unique if more than one rule applies
				pObj->m_sz_n_itemid, // is actually unique, the local uid of the direct analysis object.
			g_pdirect->m_settings.m_pszDefaultDB?g_pdirect->m_settings.m_pszDefaultDB:"DiReCT"

			);
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "--------  ReturnFromEndpointQueue SQL: %s", szSQL); // Sleep(50); //(Dispatch message)
		
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

		if(prs != NULL) 
		{
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "--------  ReturnFromEndpointQueue prs not null");  //Sleep(50); //(Dispatch message)
			if(!prs->IsEOF())
			{
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "--------  ReturnFromEndpointQueue prs not EOF");  //Sleep(50); //(Dispatch message)
				CDirectQueueObject* pQueueReturn = new CDirectQueueObject;
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "--------  ReturnFromEndpointQueue pQueueReturn");  //Sleep(50); //(Dispatch message)

				if(pQueueReturn)
				{
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "--------  ReturnFromEndpointQueue pQueueReturn not null");  //Sleep(50); //(Dispatch message)
					CString szTemp;
					int nTemp;
					try
					{
						// get id!
						prs->GetFieldValue("itemid", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "--------  ReturnFromEndpointQueue itemid %s", szTemp);  //Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							nTemp = atoi(szTemp);
							if(nTemp>=0) pQueueReturn->m_nItemID = nTemp;
						}

						prs->GetFieldValue("filename_local", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "--------  ReturnFromEndpointQueue filename_local %s", szTemp);  //Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							pQueueReturn->SetField(&pQueueReturn->m_pszFilenameLocal, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("filename_remote", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "--------  ReturnFromEndpointQueue filename_remote %s", szTemp);  //Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							pQueueReturn->SetField(&pQueueReturn->m_pszFilenameRemote, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("action", szTemp);//HARDCODE
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "--------  ReturnFromEndpointQueue action %s", szTemp);  //Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							nTemp = atoi(szTemp);
							if(nTemp>=0) pQueueReturn->m_nActionID = nTemp;
						}

						prs->GetFieldValue("host", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "--------  ReturnFromEndpointQueue host %s", szTemp);  //Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							pQueueReturn->SetField(&pQueueReturn->m_pszHost, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("timestamp", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "--------  ReturnFromEndpointQueue timestamp %s", szTemp); // Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							pQueueReturn->m_dblTimestamp = atof(szTemp);
						}

						prs->GetFieldValue("username", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "--------  ReturnFromEndpointQueue username %s", szTemp);  //Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							pQueueReturn->SetField(&pQueueReturn->m_pszUsername, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("event_itemid", szTemp);//HARDCODE
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "--------  ReturnFromEndpointQueue event_itemid %s", szTemp);  //Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							nTemp = atoi(szTemp);
							if(nTemp>=0) pQueueReturn->m_nEventItemID = nTemp;
						}
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE) g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "--------  ReturnFromEndpointQueue event item id %d", pQueueReturn->m_nEventItemID); // Sleep(50); //(Dispatch message)

						prs->GetFieldValue("message", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pQueueReturn->SetField(&pQueueReturn->m_pszMessage, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
							if(m_pmsgr)
							{
								char errorstring[DB_ERRORSTRING_LEN];
								_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
								Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
							}
*/
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							// The error code is in e->m_nRetCode
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
						}
						else 
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
						}
						e->Delete();
					} 
					catch( ... )
					{
					}

					prs->Close();
					delete prs;
					prs = NULL;
					LeaveCriticalSection(&m_critSQL);

					return pQueueReturn;
				} 
				else 
				{
					if(pszInfo) sprintf(pszInfo, "Error allocating queue object");
				}

				// prs->MoveNext();
			} //if(!prs->IsEOF())
		
			prs->Close();
			delete prs;
			prs = NULL;
		}
		else  //else prs null
		{
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
		}
		LeaveCriticalSection(&m_critSQL);

	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return NULL;
}

int CDirectData::RemoveFromEndpointQueue(CDirectEventObject* pObj, int nItemID, int nLine, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.
	if((pObj)&&(m_pdb)&&(m_pdbConn))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
			

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s WHERE itemid = %d",  //HARDCODE
					pObj->m_sz_vc64_module_dbname,
					pObj->m_sz_vc64_module_dbqueue,
					nItemID
				);

if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE)  g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "--- for %s -----  %d:RemoveFromEndpointQueue SQL: %s", pObj->m_sz_n_itemid, nLine, szSQL); // Sleep(50); //(Dispatch message)

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
		//**MSG
g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)
			LeaveCriticalSection(&m_critSQL);
			return DIRECT_ERROR;
		}
		LeaveCriticalSection(&m_critSQL);
		return DIRECT_SUCCESS;
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return DIRECT_ERROR;
}

int CDirectData::ScheduleEndpointQueue(CDirectEventObject* pObj, char* pszFilenameLocal, char* pszFilenameRemote, int nAction, bool bOnAirTime, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.
	if((pObj)&&(m_pdb)&&(m_pdbConn))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		char* pchEncodedFileLocal = m_pdb->EncodeQuotes(pszFilenameLocal);
		char* pchEncodedFileRemote = m_pdb->EncodeQuotes(pszFilenameRemote);

		if(bOnAirTime)
		{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(filename_local, filename_remote, action, host, timestamp, username, event_itemid) \
VALUES ('%s','%s', %d, '%s', %s, '�%s�', %s)", //HARDCODE
															pObj->m_sz_vc64_module_dbname,
															pObj->m_sz_vc64_module_dbqueue,
															pchEncodedFileLocal?pchEncodedFileLocal:"",
															pchEncodedFileRemote?pchEncodedFileRemote:"",
															nAction,
															pObj->m_sz_vc64_dest_host,
															pObj->m_sz_dbl_event_start,
															g_pdirect->m_settings.m_pszDefaultDB?g_pdirect->m_settings.m_pszDefaultDB:"DiReCT",
//															pObj->m_sz_n_event_itemid // is actually the automation item's id, so not necessarily unique if more than one rule applies
															pObj->m_sz_n_itemid // is actually unique, the local uid of the direct analysis object.
															);
		
		}
		else
		{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(filename_local, filename_remote, action, host, timestamp, username, event_itemid) \
VALUES ('%s','%s', %d, '%s', %d.%d, '�%s�', %s)", //HARDCODE
															pObj->m_sz_vc64_module_dbname,
															pObj->m_sz_vc64_module_dbqueue,
															pchEncodedFileLocal?pchEncodedFileLocal:"",
															pchEncodedFileRemote?pchEncodedFileRemote:"",
															nAction,
															pObj->m_sz_vc64_dest_host,
															m_timebTick.time,
															m_timebTick.millitm,
															g_pdirect->m_settings.m_pszDefaultDB?g_pdirect->m_settings.m_pszDefaultDB:"DiReCT",
//															pObj->m_sz_n_event_itemid // is actually the automation item's id, so not necessarily unique if more than one rule applies
															pObj->m_sz_n_itemid // is actually unique, the local uid of the direct analysis object.
															);
		}
		if(pchEncodedFileLocal) free(pchEncodedFileLocal);
		if(pchEncodedFileRemote) free(pchEncodedFileRemote);

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_QUEUE)  g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "ScheduleEndpointQueue SQL: %s", szSQL); // Sleep(50); //(Dispatch message)
		char errorstring[DB_ERRORSTRING_LEN]; 
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
//		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
		{
		//**MSG
g_pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "ERROR executing SQL: %s", errorstring); // Sleep(50); //(Dispatch message)
			LeaveCriticalSection(&m_critSQL);
			return DIRECT_ERROR;
		}
		LeaveCriticalSection(&m_critSQL);
		return DIRECT_SUCCESS;
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return DIRECT_ERROR;
}


int CDirectData::SimpleSetStatus(CDirectEventObject* pObj, int nStatus, bool bUpdateSearchFiles, bool bFileIndex, int nAppDataAux, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.
	if((pObj)&&(m_pdb)&&(m_pdbConn))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		if(nAppDataAux>0)
		{
			if(bUpdateSearchFiles)
			{
				char* pszEncodedSearchFile = m_pdb->EncodeQuotes(pObj->m_sz_vc1024_search_files);
				if(bFileIndex)
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d, search_files = '%s', file_index = %d, app_data_aux = %d WHERE itemid = %s",
							g_pdirect->m_settings.m_pszDefaultDB?g_pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
							g_pdirect->m_settings.m_pszLiveEvents?g_pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							pszEncodedSearchFile?pszEncodedSearchFile:pObj->m_sz_vc1024_search_files,
							pObj->m_nCurrentFileSearch,
							nAppDataAux,
							pObj->m_sz_n_itemid
						);
				}
				else
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d, search_files = '%s', app_data_aux = %d WHERE itemid = %s",
							g_pdirect->m_settings.m_pszDefaultDB?g_pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
							g_pdirect->m_settings.m_pszLiveEvents?g_pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							pszEncodedSearchFile?pszEncodedSearchFile:pObj->m_sz_vc1024_search_files,
							nAppDataAux,
							pObj->m_sz_n_itemid
						);
				}
				if(pszEncodedSearchFile) free(pszEncodedSearchFile);
			}
			else
			{
				if(bFileIndex)
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d, file_index = %d, app_data_aux = %d WHERE itemid = %s",
							g_pdirect->m_settings.m_pszDefaultDB?g_pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
							g_pdirect->m_settings.m_pszLiveEvents?g_pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							pObj->m_nCurrentFileSearch,
							nAppDataAux,
							pObj->m_sz_n_itemid
						);
				}
				else
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d, app_data_aux = %d WHERE itemid = %s",
							g_pdirect->m_settings.m_pszDefaultDB?g_pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
							g_pdirect->m_settings.m_pszLiveEvents?g_pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							nAppDataAux,
							pObj->m_sz_n_itemid
						);
				}
			}
		}
		else  // dont update app data
		{
			if(bUpdateSearchFiles)
			{
				char* pszEncodedSearchFile = m_pdb->EncodeQuotes(pObj->m_sz_vc1024_search_files);
				if(bFileIndex)
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d, search_files = '%s', file_index = %d WHERE itemid = %s",
							g_pdirect->m_settings.m_pszDefaultDB?g_pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
							g_pdirect->m_settings.m_pszLiveEvents?g_pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							pszEncodedSearchFile?pszEncodedSearchFile:pObj->m_sz_vc1024_search_files,
							pObj->m_nCurrentFileSearch,
							pObj->m_sz_n_itemid
						);
				}
				else
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d, search_files = '%s' WHERE itemid = %s",
							g_pdirect->m_settings.m_pszDefaultDB?g_pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
							g_pdirect->m_settings.m_pszLiveEvents?g_pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							pszEncodedSearchFile?pszEncodedSearchFile:pObj->m_sz_vc1024_search_files,
							pObj->m_sz_n_itemid
						);
				}
				if(pszEncodedSearchFile) free(pszEncodedSearchFile);
			}
			else
			{
				if(bFileIndex)
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d, file_index = %d WHERE itemid = %s",
							g_pdirect->m_settings.m_pszDefaultDB?g_pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
							g_pdirect->m_settings.m_pszLiveEvents?g_pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							pObj->m_nCurrentFileSearch,
							pObj->m_sz_n_itemid
						);
				}
				else
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d WHERE itemid = %s",
							g_pdirect->m_settings.m_pszDefaultDB?g_pdirect->m_settings.m_pszDefaultDB:"Direct",			// the Default DB name
							g_pdirect->m_settings.m_pszLiveEvents?g_pdirect->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							pObj->m_sz_n_itemid
						);
				}
			}
		}

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "SimpleSetStatus SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
		char errorstring[DB_ERRORSTRING_LEN];		
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
//		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
		{
		//**MSG
g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "ERROR executing SQL: %s", errorstring); // Sleep(50); //(Dispatch message)
			LeaveCriticalSection(&m_critSQL);
			return DIRECT_ERROR;
		}
		LeaveCriticalSection(&m_critSQL);
		return DIRECT_SUCCESS;
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return DIRECT_ERROR;
}

int CDirectData::UpdateSearchFiles(CDirectEventObject* pObj, int nStatus)
{
	if((pObj)&&(nStatus>=0))
	{
		pObj->m_sz_vc1024_search_files.Format("%d%s",
				pObj->m_nCurrentFileSearch<0?nStatus:pObj->m_nMainFileStatus, 
				pObj->m_pszMainFile
			);

		int c=0;
		CString szTemp;
		while(c<pObj->m_nNumChildren)
		{
			szTemp = pObj->m_sz_vc1024_search_files;
			pObj->m_sz_vc1024_search_files.Format("%s|%d%s",
				szTemp,
				c==pObj->m_nCurrentFileSearch?nStatus:pObj->m_pnChildrenStatus[c], 
				pObj->m_ppszChildren[c]
				);
			c++;
		}
/*
// file status codes:
0 not yet checked
1 checking
2 checked and present
3 transferring
4 transferred
5 error
6 skipped
*/

		return DIRECT_SUCCESS;
	}
	return DIRECT_ERROR;
}


int CDirectData::ClearTempFolder(int nMode)
{
	if((g_pdirect)&&(g_pdirect->m_settings.m_pszTempFolderPath)&&(strlen(g_pdirect->m_settings.m_pszTempFolderPath)))
	{

		char pszSearchPath[MAX_PATH+1];
		char pszDelPath[MAX_PATH+1];
		// make the dir if not exists!

		strcpy(pszSearchPath, g_pdirect->m_settings.m_pszTempFolderPath);

if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_PURGE)	
	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:purge", "Clearing %s files in %s", 
	 (nMode == DIRECT_PURGE_ALL)?"all":"old", pszSearchPath ); 
		for (int i=0; i<(int)strlen(g_pdirect->m_settings.m_pszTempFolderPath); i++)
		{
			if((g_pdirect->m_settings.m_pszTempFolderPath[i]=='/')||(g_pdirect->m_settings.m_pszTempFolderPath[i]=='\\'))
			{
				pszSearchPath[i] = 0;
				if(strlen(pszSearchPath)>0)
				{
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "mkdir with %s", dir_buffer);  //(Dispatch message)

					_mkdir(pszSearchPath);
				}
				pszSearchPath[i] = '\\';
			}
			else
			if(i==(int)strlen(g_pdirect->m_settings.m_pszTempFolderPath)-1)
			{
				if(strlen(pszSearchPath)>0)
				{
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "mkdir with %s", dir_buffer);  //(Dispatch message)

					_mkdir(pszSearchPath);
				}
			}
			else
				pszSearchPath[i] = g_pdirect->m_settings.m_pszTempFolderPath[i];
		}




		WIN32_FIND_DATA wfd;
		HANDLE hfile = NULL;
		sprintf(pszSearchPath, "%s*.*", g_pdirect->m_settings.m_pszTempFolderPath); // has trailing backslash


		int nFound=0;
		int nDel=0;
		int nErr=0;
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_PURGE)	
	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:purge", "Searching %s", pszSearchPath ); 

		hfile = FindFirstFile( pszSearchPath,  // pointer to name of file to search for 
			&wfd  // pointer to returned information 
		); 

		// note: have to recurse dirs if set in pdu->m_bUseSubDirs
		if(hfile != INVALID_HANDLE_VALUE)
		{

			if((!(wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY))&&(strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a real file
			{
				nFound++;
			//	 - timestamp.timezone

				if( nMode == DIRECT_PURGE_ALL)
				{

					sprintf(pszDelPath, "%s%s", g_pdirect->m_settings.m_pszTempFolderPath, wfd.cFileName); // has trailing backslash

					if(DeleteFile(pszDelPath))
					{
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_PURGE)	
	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:purge", "Deleted %s", wfd.cFileName ); 
						nDel++;
					}
					else
					{
	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:purge", "Error %d deleting %s", GetLastError(), pszDelPath ); 
						nErr++;
					}
				}
				else // check if old, then delete if it is
				{
					SYSTEMTIME SystemTime;
					FileTimeToSystemTime(&wfd.ftLastWriteTime, &SystemTime);

					CTime time(SystemTime);
					CTime now = CTime::GetCurrentTime();
					if(time.GetTime()<(now.GetTime()-g_pdirect->m_settings.m_nTempFilePurgeInterval))
					{
						sprintf(pszDelPath, "%s%s", g_pdirect->m_settings.m_pszTempFolderPath, wfd.cFileName); // has trailing backslash
						if(DeleteFile(pszDelPath))
						{
	if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_PURGE)	
		g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:purge", "Deleted %s", wfd.cFileName ); 
							nDel++;
						}
						else
						{
		g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:purge", "Error %d deleting %s", GetLastError(), pszDelPath ); 
							nErr++;
						}

					}//else do not delete

				}
			}
		
			while(FindNextFile( hfile,  // handle to search 
				&wfd  // pointer to structure for data on found file 
				) //If the function fails, the return value is zero. To get extended error information, call GetLastError. If no matching files can be found, the GetLastError function returns ERROR_NO_MORE_FILES
			)
			{
				if((!(wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY))&&(strcmp(wfd.cFileName, "."))&&(strcmp(wfd.cFileName, ".."))) // only if its a real file
				{
					nFound++;
					if( nMode == DIRECT_PURGE_ALL)
					{
						sprintf(pszDelPath, "%s%s", g_pdirect->m_settings.m_pszTempFolderPath, wfd.cFileName); // has trailing backslash
						if(DeleteFile(pszDelPath))
						{
	if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_PURGE)	
		g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:purge", "Deleted %s", wfd.cFileName ); 
							nDel++;
						}
						else
						{
		g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:purge", "Error %d deleting %s", GetLastError(), pszDelPath ); 
							nErr++;
						}
					}
					else // check if old, then delete if it is
					{
						SYSTEMTIME SystemTime;
						FileTimeToSystemTime(&wfd.ftLastWriteTime, &SystemTime);

						CTime time(SystemTime);
						CTime now = CTime::GetCurrentTime();
						if(time.GetTime()<(now.GetTime()-g_pdirect->m_settings.m_nTempFilePurgeInterval))
						{
							sprintf(pszDelPath, "%s%s", g_pdirect->m_settings.m_pszTempFolderPath, wfd.cFileName); // has trailing backslash
							if(DeleteFile(pszDelPath))
							{
		if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_PURGE)	
			g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:purge", "Deleted %s", wfd.cFileName ); 
								nDel++;
							}
							else
							{
			g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:purge", "Error %d deleting %s", GetLastError(), pszDelPath ); 
								nErr++;
							}

						}//else do not delete

					}
				}
			}
			FindClose(hfile);

	if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_PURGE)	
		g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:purge", "%d files found, %d deleted, %d errors", nFound, nDel, nErr ); 

		}
		else
		{
if(g_pdirect->m_settings.m_ulDebug&DIRECT_DEBUG_PURGE)	
	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:purge", "No files found for %s", pszSearchPath ); 
		}

		return DIRECT_SUCCESS;
	}
	return DIRECT_ERROR;
}

int	CDirectData::ParseFiles(char* pszSourceData, char** pszFile, int* pnStatus, char*** pppszChildren, int** ppnChildrenStatus, int* pnNumChildren)
{
	if((pszSourceData)&&(strlen(pszSourceData)>0)&&(pszFile)&&(pnStatus)&&(pppszChildren)&&(ppnChildrenStatus)&&(pnNumChildren))
	{
//		*pszFile = NULL; *pnStatus=0; *pppszChildren=NULL; *ppnChildrenStatus=NULL; *pnNumChildren=NULL; return 1;

		CSafeBufferUtil sbu;
		char* pchReturn;
		char* pch = sbu.Token(pszSourceData, strlen(pszSourceData), "|");
		if(pch)
		{
			*pnStatus = (int)((*pch)-48); // ascii code. translated to numerical
			pch++;

			pchReturn = (char*)malloc(strlen(pch)+1);
			if(pchReturn)
			{
				strcpy(pchReturn, pch);
				*pszFile = pchReturn;
			}
			else return DIRECT_ERROR;
		}
		pch = sbu.Token(NULL, NULL, "|");
		int nNumChildren=0;
		int* pnChildrenStatus=NULL;
		char** ppszChildren= NULL;
		while(pch)
		{
			int* pnChStatus= new int[nNumChildren+1];
			char** ppszCh = new char*[nNumChildren+1];

			if((pnChStatus)&&(ppszCh))
			{
				if(pnChildrenStatus)
				{
					int i=0;
					while(i<nNumChildren)
					{
						pnChStatus[i] = pnChildrenStatus[i];
						i++;
					}
					delete [] pnChildrenStatus;
				}

				if(ppszChildren)
				{
					int i=0;
					while(i<nNumChildren)
					{
						ppszCh[i] = ppszChildren[i];
						i++;
					}
					delete [] ppszChildren;
				}

				pnChildrenStatus = pnChStatus;
				ppszChildren = ppszCh;


				pnChildrenStatus[nNumChildren] = (int)(*pch) - 48; // 48 is char code for 0.
				if((pnChildrenStatus[nNumChildren]<0)||(pnChildrenStatus[nNumChildren]>9)) // has to be single digit
					pnChildrenStatus[nNumChildren] = 0;
				pch++;

				pchReturn = (char*)malloc(strlen(pch)+1);
				if(pchReturn)
				{
					strcpy(pchReturn, pch);
					ppszChildren[nNumChildren] = pchReturn;
				}
				nNumChildren++;
			}
			pch = sbu.Token(NULL, NULL, "|");
		}
		*pppszChildren = ppszChildren;
		*ppnChildrenStatus = pnChildrenStatus;
		*pnNumChildren = nNumChildren;
	}
	return DIRECT_ERROR;
}

/*
int CDirectData::GetConnections(char* pszInfo)
{
	if((g_pdirect)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY server",  //HARDCODE
			((g_pdirect->m_settings.m_pszConnections)&&(strlen(g_pdirect->m_settings.m_pszConnections)))?g_pdirect->m_settings.m_pszConnections:"Connections");
//		g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "GetConnections");  Sleep(250); //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = DIRECT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szHost="";
				CString szClient="";
				CString szDesc="";
				CString szTemp;
				unsigned long ulFlags;   // various flags
			//	unsigned short usType;
				int nTemp = -1;
				bool bFlagsFound = false;
				bool bFound = false;
				try
				{
					prs->GetFieldValue("server", szHost);//HARDCODE
					szHost.TrimLeft(); szHost.TrimRight();
					prs->GetFieldValue("description", szDesc);//HARDCODE
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulFlags = atol(szTemp);
					}
					prs->GetFieldValue("client", szClient);//HARDCODE
					szClient.TrimLeft(); szClient.TrimRight();
				}
				catch( ... )
				{
				}

				if((m_ppConnObj)&&(m_nNumConnectionObjects))
				{
					nTemp=0;
					while(nTemp<m_nNumConnectionObjects)
					{
//			g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "c2");  Sleep(250); //(Dispatch message)
						if(m_ppConnObj[nTemp])
						{
							if((szHost.GetLength()>0)&&(szHost.CompareNoCase(m_ppConnObj[nTemp]->m_pszServerName)==0))
							{
								bFound = true;
//			g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "c3");  Sleep(250); //(Dispatch message)

								if(
										((bFlagsFound)&&(!(ulFlags&DIRECT_FLAG_ENABLED))&&((m_ppConnObj[nTemp]->m_ulFlags)&DIRECT_FLAG_ENABLED))
									||(
												(szClient.GetLength()>0)&&(szClient.CompareNoCase(m_ppConnObj[nTemp]->m_pszClientName)) // different client name
											&&(
													((bFlagsFound)&&(ulFlags&DIRECT_FLAG_ENABLED))
												||((!bFlagsFound)&&((m_ppConnObj[nTemp]->m_ulFlags)&DIRECT_FLAG_ENABLED))
												)
										)
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", 
										m_ppConnObj[nTemp]->m_pszDesc?m_ppConnObj[nTemp]->m_pszDesc:m_ppConnObj[nTemp]->m_pszServerName);  
									g_pdirect->m_msgr.DM(MSG_ICONINFO, NULL, "DiReCT:destination_change", errorstring);    //(Dispatch message)
//									m_ppConnObj[nTemp]->m_pDlg->OnDisconnect();
									//**** disconnect

//									g_adc.DisconnectServer(m_ppConnObj[nTemp]->m_pszServerName);
									m_ppConnObj[nTemp]->m_bKillConnThread = true;
									//**** should check return value....

									m_ppConnObj[nTemp]->m_ulStatus = DIRECT_STATUS_NOTCON;
								}

								if(
										((bFlagsFound)&&(ulFlags&DIRECT_FLAG_ENABLED)&&(!((m_ppConnObj[nTemp]->m_ulFlags)&DIRECT_FLAG_ENABLED)))
									||(
												(szClient.GetLength()>0)&&(szClient.CompareNoCase(m_ppConnObj[nTemp]->m_pszClientName)) // different client name
											&&(
													((bFlagsFound)&&(ulFlags&DIRECT_FLAG_ENABLED))
												||((!bFlagsFound)&&((m_ppConnObj[nTemp]->m_ulFlags)&DIRECT_FLAG_ENABLED))
												)
										)
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s to %s...", 
										m_ppConnObj[nTemp]->m_pszClientName?m_ppConnObj[nTemp]->m_pszClientName:"Direct",  
										m_ppConnObj[nTemp]->m_pszDesc?m_ppConnObj[nTemp]->m_pszDesc:m_ppConnObj[nTemp]->m_pszServerName);  
									g_pdirect->m_msgr.DM(MSG_ICONINFO, NULL, "DiReCT:destination_change", errorstring);    //(Dispatch message)
//									m_ppConnObj[nTemp]->m_pDlg->OnButtonConnect();
									//**** if connect set following status

									m_ppConnObj[nTemp]->m_bKillConnThread = false;
									m_ppConnObj[nTemp]->m_pAPIConn = NULL;
									if(_beginthread(DirectConnectionThread, 0, (void*)m_ppConnObj[nTemp])==-1)
									{
										//error.

										//**MSG
							
										
									}
									//**** should check return value....

									m_ppConnObj[nTemp]->m_ulStatus = DIRECT_STATUS_CONN;
								}

								if(bFlagsFound) m_ppConnObj[nTemp]->m_ulFlags = ulFlags|DIRECT_FLAG_FOUND;
								else m_ppConnObj[nTemp]->m_ulFlags |= DIRECT_FLAG_FOUND;

							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szHost.GetLength()>0)) // have to add.
				{
//			g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "adding %s", szHost);  Sleep(250); //(Dispatch message)
					CDirectConnectionObject* pscno = new CDirectConnectionObject;
					if(pscno)
					{
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CDirectConnectionObject** ppObj = new CDirectConnectionObject*[m_nNumConnectionObjects+1];
						if(ppObj)
						{
//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((m_ppConnObj)&&(m_nNumConnectionObjects>0))
							{
								while(o<m_nNumConnectionObjects)
								{
									ppObj[o] = m_ppConnObj[o];
									o++;
								}
								delete [] m_ppConnObj;

							}
							ppObj[o] = pscno;
							m_ppConnObj = ppObj;
							m_nNumConnectionObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szHost.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, "%s", szHost);

							if(szClient.GetLength()<=0)
							{
								szClient = "Direct";
							}

							if(szClient.GetLength()>0)
							{
								ppObj[o]->m_pszClientName = (char*)malloc(szClient.GetLength()+1); 
								if(ppObj[o]->m_pszClientName) sprintf(ppObj[o]->m_pszClientName, "%s", szClient);
							}

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}
//							ppObj[o]->m_usType = not used;
							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|DIRECT_FLAG_FOUND;
							else ppObj[o]->m_ulFlags |= DIRECT_FLAG_FOUND;
								

							if((ppObj[o]->m_ulFlags)&DIRECT_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s to %s...", 
									((ppObj[o]->m_pszClientName)&&(strlen(ppObj[o]->m_pszClientName)))?ppObj[o]->m_pszClientName:"Direct",  
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName);  
								g_pdirect->m_msgr.DM(MSG_ICONINFO, NULL, "DiReCT:destination_change", errorstring);    //(Dispatch message)

								//**** if connect 

								// create a connection object in g_adc.
								// associate that with the new CDirectConnectionObject;
								// start the thread.
								ppObj[o]->m_bKillConnThread = false;
								ppObj[o]->m_pAPIConn = NULL;
									g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "beginning thread for %s", szHost);  Sleep(250); //(Dispatch message)
								if(_beginthread(DirectConnectionThread, 0, (void*)ppObj[o])==-1)
								{
									//error.

									//**MSG
									g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "problem adding %s", szHost);  Sleep(250); //(Dispatch message)

						
									
								}
							//**** should check return value....

								ppObj[o]->m_ulStatus = DIRECT_STATUS_CONN;
							}
						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//			g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<m_nNumConnectionObjects)
			{
				if((m_ppConnObj)&&(m_ppConnObj[nIndex]))
				{
					if((m_ppConnObj[nIndex]->m_ulFlags)&DIRECT_FLAG_FOUND)
					{
						(m_ppConnObj[nIndex]->m_ulFlags) &= ~DIRECT_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(m_ppConnObj[nIndex])
						{
							if((m_ppConnObj[nIndex]->m_ulFlags)&DIRECT_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", 
									m_ppConnObj[nIndex]->m_pszDesc?m_ppConnObj[nIndex]->m_pszDesc:m_ppConnObj[nIndex]->m_pszServerName);  
								g_pdirect->m_msgr.DM(MSG_ICONINFO, NULL, "DiReCT:destination_remove", errorstring);    //(Dispatch message)
//									m_ppConnObj[nTemp]->m_pDlg->OnDisconnect();

								//**** disconnect

								m_ppConnObj[nIndex]->m_bKillConnThread = false;
								while(m_ppConnObj[nIndex]->m_bConnThreadStarted) Sleep(1);

								//g_adc.DisconnectServer(m_ppConnObj[nIndex]->m_pszServerName);
								//**** should check return value....

								m_ppConnObj[nIndex]->m_ulStatus = DIRECT_STATUS_NOTCON;
							}

							delete m_ppConnObj[nIndex];
							m_nNumConnectionObjects--;

							int nTemp=nIndex;
							while(nTemp<m_nNumConnectionObjects)
							{
								m_ppConnObj[nTemp]=m_ppConnObj[nTemp+1];
								nTemp++;
							}
							m_ppConnObj[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

//			g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
	}
//			g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return DIRECT_ERROR;
}
*/

