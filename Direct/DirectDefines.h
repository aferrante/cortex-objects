// DirectDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(DIRECTDEFINES_H_INCLUDED)
#define DIRECTDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define DIRECT_CURRENT_VERSION		"2.2.2.7"


// modes
#define DIRECT_MODE_DEFAULT			0x00000000  // exclusive
#define DIRECT_MODE_LISTENER		0x00000001  // exclusive
#define DIRECT_MODE_CLONE				0x00000002  // exclusive
#define DIRECT_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define DIRECT_MODE_VOLATILE		0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define DIRECT_MODE_MASK				0x0000000f  // 

// default port values.
//#define DIRECT_PORT_FILE				80		
#define DIRECT_PORT_CMD					20670		
#define DIRECT_PORT_STATUS			20671		

#define DIRECT_PORT_INVALID			0	

#define DIRECT_FLAGS_INVALID		0xffffffff	 
#define DIRECT_FLAG_DISABLED		0x0000	 // default
#define DIRECT_FLAG_ENABLED			0x0001	
#define DIRECT_FLAG_FOUND				0x1000

#define DIRECT_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded

// installed dependencies
#define DIRECT_DEP_UNKNOWN								0x0000  // type unknown
#define DIRECT_DEP_DATA_ARCHIVIST					0x0002  // Archivist is installed
#define DIRECT_DEP_DATA_MASK							0x0002  // mask for data bit  // there is only one choice at the moment!
#define DIRECT_DEP_AUTO_HELIOS						0x0010  // Omnibus is installed (Helios)
#define DIRECT_DEP_AUTO_SENTINEL					0x0020  // Harris is installed (Sentinel)
#define DIRECT_DEP_AUTO_MASK							0x00f0  // mask for automation bit
#define DIRECT_DEP_EDGE_PROSPERO					0x0100  // Miranda is installed (Prospero)
#define DIRECT_DEP_EDGE_LUMINARY					0x0200  // Chyron (CAL) is installed (Luminary)
#define DIRECT_DEP_EDGE_RADIANCE					0x0300  // Orad is installed (Radiance)
#define DIRECT_DEP_EDGE_LIBRETTO					0x0400  // Intelligent interface box is installed (Libretto)
#define DIRECT_DEP_EDGE_BARBERO						0x0500  // Evertz inerface FTP
#define DIRECT_DEP_EDGE_MASK							0x0f00  // mask for edge device bit (device to have stuff transferred to)

// rule types
#define DIRECT_RULE_TYPE_UNKNOWN					0x00000000  // type unknown
#define DIRECT_RULE_TYPE_PRESMASTER				0x00000001  // for Helios, with presmaster data in <data> XML field
#define DIRECT_RULE_TYPE_XML							0x00000002  // for Helios, with logo data in <logos> XML sub-fields .. free form tho
#define DIRECT_RULE_TYPE_IMAGESTORE				0x00000003  // for Sentinel, Miranda driver
#define DIRECT_RULE_TYPE_FTP							0x00000004  // for Sentinel and Evertz

//comparison types
///// previously only supported ==, >=, <=, unk.  mst update code everywher to support newer comparators.
#define DIRECT_RULE_COMPARE_UNKNOWN				0xffff  // type unknown
#define DIRECT_RULE_COMPARE_EQUALS				0x0001  // == equals
#define DIRECT_RULE_COMPARE_GT						0x0002  // >  greater than
#define DIRECT_RULE_COMPARE_LT						0x0003  // <  less than
#define DIRECT_RULE_COMPARE_GTOE					0x0004  // >= greater than or equal to
#define DIRECT_RULE_COMPARE_LTOE					0x0005  // <= less than or equal to
#define DIRECT_RULE_COMPARE_NOTEQUAL			0x0006  // != equals
#define DIRECT_RULE_COMPARE_PARTIAL				0x0007  // ~= partial comparison
#define DIRECT_RULE_COMPARE_AND						0x0008  // &  contains any of the bitflags
#define DIRECT_RULE_COMPARE_ALL						0x0009  // =& contains all the bitflags
#define DIRECT_RULE_COMPARE_NOT						0x000a  // !& does not contain any the bitflag



/*
//destination types (decimal)
#define DIRECT_RULE_DESTTYPE_UNKNOWN				0000  // type unknown
#define DIRECT_RULE_DESTTYPE_ORAD_DVG				1000  // Orad DVG
#define DIRECT_RULE_DESTTYPE_ORAD_HDVG			1001  // Orad HDVG

#define DIRECT_RULE_DESTTYPE_MIRANDA_IS2		2001  // Imagestore 2
#define DIRECT_RULE_DESTTYPE_MIRANDA_INT		2002  // Intuition
#define DIRECT_RULE_DESTTYPE_MIRANDA_IS300	2003  // Imagestore 300
#define DIRECT_RULE_DESTTYPE_MIRANDA_ISHD		2004  // Imagestore HD
#define DIRECT_RULE_DESTTYPE_MIRANDA_INTHD	2005  // Intuition HD

#define DIRECT_RULE_DESTTYPE_EVERTZ_9625LG			4000 
#define DIRECT_RULE_DESTTYPE_EVERTZ_9625LGA			4001
#define DIRECT_RULE_DESTTYPE_EVERTZ_9725LGA			4002
#define DIRECT_RULE_DESTTYPE_EVERTZ_9725LG			4003
#define DIRECT_RULE_DESTTYPE_EVERTZ_HD9625LG		4004
#define DIRECT_RULE_DESTTYPE_EVERTZ_HD9625LGA		4005
#define DIRECT_RULE_DESTTYPE_EVERTZ_HD9725LG		4006
#define DIRECT_RULE_DESTTYPE_EVERTZ_HD9725LGA		4007
*/

//search types
#define DIRECT_RULE_SEARCH_UNKNOWN				0xffff  // type unknown
#define DIRECT_RULE_SEARCH_EXPLICIT				0x0000  // search for the exact file only
#define DIRECT_RULE_SEARCH_AUDIO					0x0001  // search for corresponding audio file
#define DIRECT_RULE_SEARCH_VIDEO					0x0002  // search for corresponding video file
#define DIRECT_RULE_SEARCH_AV							0x0003  // search for corresponding audio and video files

//action types
#define DIRECT_RULE_ACTION_UNKNOWN				0xffff  // type unknown
#define DIRECT_RULE_ACTION_NORMAL					0x0000  // just error if nothing found...
#define DIRECT_RULE_ACTION_NULLOXT				0x0001  // if video file not found, create null OXT
#define DIRECT_RULE_ACTION_NULLOXTORNORM	0x0002  // if video file not found, create null OXT if audio file exists, otherwise just xfer video file.
#define DIRECT_RULE_ACTION_PATTERNRENAME	0x0003  // rename the file on the destination box.


#define DIRECT_EVENTSTATUS_NONE						0x00000000 //nothing found.
#define DIRECT_EVENTSTATUS_AUDIO					0x00001000 //audio found.
#define DIRECT_EVENTSTATUS_VIDEO					0x00002000 //video found.
#define DIRECT_EVENTSTATUS_FONT						0x00004000 //fonts found. 
#define DIRECT_EVENTSTATUS_TEM						0x00008000 //tem found
#define DIRECT_EVENTSTATUS_TEMLOC					0x00010000 //tem retrieved locally
#define DIRECT_EVENTSTATUS_SUBMASK				0x00000fff //mask - tem subelements enumerated.
#define DIRECT_EVENTSTATUS_ERRORSENT_XFER	0x00100000 //an error was sent out already indicating transfer failure
#define DIRECT_EVENTSTATUS_ERRORSENT_META	0x00200000 //an error was sent out already indicating no metadata
#define DIRECT_EVENTSTATUS_ERRORSENT_NOXT	0x00400000 //an error was sent out already indicating no null oxt file found


// status
#define DIRECT_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define DIRECT_STATUS_UNKNOWN							0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define DIRECT_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define DIRECT_STATUS_ERROR								0x00000020  // error (red icon)
#define DIRECT_STATUS_CONN								0x00000030  // ready (green icon)	
#define DIRECT_STATUS_OK									0x00000030  // ready (green icon)	
#define DIRECT_STATUS_RUN									0x00000040  // in progress, running, owned etc (blue icon);	
#define DIRECT_ICON_MASK									0x00000070  // mask	

#define DIRECT_STATUS_SUSPEND							0x00000080  // suspended	(yellow icon please)

#define DIRECT_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define DIRECT_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define DIRECT_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define DIRECT_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define DIRECT_STATUS_CMDSVR_MASK					0x00007000  // command server mask bits

#define DIRECT_STATUS_STATUSSVR_START			0x00010000  // starting the status server
#define DIRECT_STATUS_STATUSSVR_RUN				0x00020000  // status server running
#define DIRECT_STATUS_STATUSSVR_END				0x00030000  // status server shutting down
#define DIRECT_STATUS_STATUSSVR_ERROR			0x00040000  // status server error
#define DIRECT_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define DIRECT_STATUS_THREAD_START				0x00100000  // starting the main thread
#define DIRECT_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define DIRECT_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define DIRECT_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define DIRECT_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define DIRECT_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define DIRECT_STATUS_THREAD_MASK					0x00f00000  // main thread mask bits

// various failures...
#define DIRECT_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define DIRECT_STATUS_FAIL_DB							0x20000000  // could not get DB
#define DIRECT_STATUS_FAIL_MASK						0xf0000000  // failure code mask bits

//return values
#define DIRECT_SUCCESS   0
#define DIRECT_ERROR	   -1

// search on
#define DIRECT_SEARCH_DESTID	   0
#define DIRECT_SEARCH_HOST		   1

//purge tempfiles
#define DIRECT_PURGE_ALL	   0
#define DIRECT_PURGE_OLD		 1


// default filenames
#define DIRECT_SETTINGS_FILE_SETTINGS	  "direct.csr"		// csr = cortex settings redirect
#define DIRECT_SETTINGS_FILE_DEFAULT	  "direct.csf"		// csf = cortex settings file

// debug defines
#define DIRECT_DEBUG_TIMING			0x00000001
#define DIRECT_DEBUG_RULES			0x00000002
#define DIRECT_DEBUG_ANALYZE		0x00000004
#define DIRECT_DEBUG_RENAME			0x00000008
#define DIRECT_DEBUG_RETRIEVE		0x00000010
#define DIRECT_DEBUG_PARSE			0x00000020
#define DIRECT_DEBUG_IDPROCESS	0x00000040
#define DIRECT_DEBUG_QUEUE			0x00000080
#define DIRECT_DEBUG_PURGE			0x00000100
#define DIRECT_DEBUG_COMM				0x00000200


#endif // !defined(DIRECTDEFINES_H_INCLUDED)
