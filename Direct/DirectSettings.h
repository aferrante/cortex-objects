// DirectSettings.h: interface for the CDirectSettings class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIRECTSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
#define AFX_DIRECTSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

//#include "DirectData.h"  // for endpoints
#include "DirectDefines.h"
#include "../../Common/MFC/ODBC/DBUtil.h"  

class CDirectEndpointFileMap  
{
public:
	CDirectEndpointFileMap();
	virtual ~CDirectEndpointFileMap();

	char* m_pszExt;
	char* m_pszDir;
	unsigned long m_ulFlags;
};

class CDirectEndpointObject  
{
public:
	CDirectEndpointObject();
	virtual ~CDirectEndpointObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	int m_nModName;
	int m_nModDBName;
	int m_nModQueue;  // the Queue table name
	int m_nModExchange;  // the Exchange table name
	int m_nModLiveEvents;  // the LiveEvents table name if applicable
	int m_nModDestination;  // the Destination table name if applicable
	int m_nModDestinationMedia;  // the DestinationsMedia table name if applicable
	int m_nModChannel;  // the Channel table name if applicable
	int m_nModConnections;  // the Connections table name if applicable
	int m_nModMetadata;  // the Metadata table name if applicable
	int m_nModFileTypes;  // the File types table name if applicable

	int m_nLastModName;
	int m_nLastModDBName;
	int m_nLastModQueue;  // the Queue table name
	int m_nLastModExchange;  // the Exchange table name
	int m_nLastModLiveEvents;  // the LiveEvents table name if applicable
	int m_nLastModDestination;  // the Destination table name if applicable
	int m_nLastModDestinationMedia;  // the DestinationsMedia table name if applicable
	int m_nLastModChannel;  // the Channel table name if applicable
	int m_nLastModConnections;  // the Connections table name if applicable
	int m_nLastModMetadata;  // the Metadata table name if applicable
	int m_nLastModFileTypes;  // the File types table name if applicable

	
	char* m_pszName;
	char* m_pszDBName;
	char* m_pszQueue;  // the Queue table name
	char* m_pszExchange;  // the Exchange table name
	char* m_pszLiveEvents;  // the LiveEvents table name if applicable
	char* m_pszDestination;  // the Destination table name if applicable
	char* m_pszDestinationMedia;  // the DestinationsMedia table name if applicable
	char* m_pszChannel;  // the Channel table name if applicable
	char* m_pszConnections;  // the Connections table name if applicable
	char* m_pszMetadata;  // the Metadata table name if applicable
	char* m_pszFileTypes;  // the File types table name if applicable

	int CheckDatabaseMod(CDBUtil* pdb, CDBconn* pdbConn, char* pszTableName, char* pszInfo=NULL);
	int IncrementDatabaseMods(CDBUtil* pdb, CDBconn* pdbConn, char* pszTableName, char* pszInfo=NULL);

//	CAConnection* m_pAPIConn;  // pointer to the associated API connection.

	CDirectEndpointFileMap** m_ppMap;
	int m_nNumMaps;

	int GetFileMaps(CDBUtil* pdb, CDBconn* pdbConn);
// control
//	bool* m_pbKillConnThread;
//	bool m_bKillChannelThread;
//	bool m_bChannelThreadStarted;
};



class CDirectSettings  
{
public:
	CDirectSettings();
	virtual ~CDirectSettings();

	int GetFromDatabase(char* pszInfo=NULL);  //only get.  Interface publishes
	int Settings(bool bRead);
	char* ProcessString(char* pszString, bool bFreeIncomingString);

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;

	char* m_pszName;  // familiar name of this instance (and therefore the name of the Direct database)
	char* m_pszType;  // familiar name of the type.
	char* m_pszProject;  // familiar name of the project name.
	unsigned long m_ulMainMode;

	// ports
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;

	int m_nThreadDwellMS;
	bool m_bDebugLists;  // if true, dumps the events table
	int  m_nDebugListTopCount;  //if positive, just prints out the top n recored in the events tables.
	bool m_bDebugSQL;  // if true, prints out the SQL calls, all but event inserts
	bool m_bDebugInsertSQL;  // if true, prints out the event insert SQL calls
	unsigned long m_ulDebug;  // prints out debug statements that & with this.
	char* m_pszDebugOrder; // the order by clause for debug

	// The following flags determine what settings are loaded. 
	// The specifics do not need to be stored in cortex settings, they are pushed to the various objects.

	// messaging for Direct
	bool m_bUseLog;			// write a log file
	bool m_bUseEmail;		// send an email on failures (or commanded from remotes)
	bool m_bUseNetwork; // send a message to a remote host
	bool m_bLogNetworkErrors; // enables messaging into network object (logs socket errors, etc).
	char* m_pszFileSpec;
	char* m_pszMailSpec;
	char* m_pszProcessedFileSpec;
	char* m_pszProcessedMailSpec;
	bool m_bUseXMLClientLog;			// write a log file
	bool m_bMillisecondMessaging;			// use millisecond resolution for messages and asrun

	// Harris API
//	bool m_bUseListCount; // get all events up until the list count (otherwise just up to the lookahead)

	// automation
	int m_nFilterType;  // where event type = X, if>=0, etc.
	int m_nTypeComparator;  
	char* m_pszAdditionalWhereClause;  // to further restrict the event pull - must start with " AND "... to be added to the end of the thing


	// installed dependencies
	CDirectEndpointObject** m_ppEndpointObject; // which endpoint module(s) is (are) installed.
	int  m_nNumEndpointsInstalled; // number of endpoint module(s) is (are) installed.

	bool m_bReportSuccessfulOperation;
	bool m_bLogTransfers;
	bool m_bClearEventsOnStartup;  // clears events on startup

	int m_nAutoPurgeMessageDays;
//	int m_nAutoPurgeAsRunDays;
	int m_nAutoPurgeInterval;
	int m_nTempFilePurgeInterval;


	// database stuff
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;

	char* m_pszDefaultDB;			// the Default DB name

	char* m_pszSettings;  // the Settings table name
	char* m_pszExchange;  // the Exchange table name
	char* m_pszMessages;  // the Messages table name

	char* m_pszQueue;  // the Queue table name
//	char* m_pszChannels;  // the Channels table name
//	char* m_pszConnections;  // the Connections table name
	char* m_pszLiveEvents;  // the LiveEvents table name
	char* m_pszMapping;  // the Mapping table name
	char* m_pszChannelDests;  // the Channel dest view table name

	//system folder
	char* m_pszSystemFolderPath;			// the path of the folder used to store needed template files
	char* m_pszTempFolderPath;			// the path of the folder used for parse files etc. = gets cleared on startup!

	// file handling
	char* m_pszRenamePrefix;//   Filename conversion prefix, Prepended to filename for disk storage.   defval = ""  string max64, min0 (i.e. can be blank)
	char* m_pszRenameSuffix;//   Filename conversion suffix, Appended to filename for disk storage.  defval = "H"  string max64, min0 (i.e. can be blank)

	//imagestore specific 
	char* m_pszImagestoreNullFile;				// the filename of the nullfile - must be in the system folder
	char* m_pszIntuitionSearchExt;				// search extensions for Intuition
	char* m_pszImagestore2SearchExt;			// search extensions for IS2
	char* m_pszImagestore300SearchExt;		// search extensions for IS300
	char* m_pszImagestore300HDSearchExt;		// search extensions for IS300 HD
	char* m_pszImagestoreHDSearchExt;			// search extensions for ISHD
	char* m_pszIntuitionHDSearchExt;			// search extensions for INTHD
	char* m_pszIntuitionRenameExt;//  Intution File Conversion Extensions,  Comma separated list of file extensions to allow filename conversion on the Intuition. def val = "tem"
	char* m_pszImagestoreRenameExt;//  Imagestore File Conversion Extensions,  Comma separated list of file extensions to allow filename conversion on the Imagestore.   def val = oxa,oxt,oxe"
	char* m_pszIDPreprocessString;//  convert ID space to filename space

	// endpoint autodeletions
	bool m_bAutoDeleteOldest;
	bool m_bPropagateChangedFiles;

	unsigned long m_ulDeletionThreshold;  // number of seconds in past to seek for files to delete

	int m_nTransferFailureRetryTime;  // number of seconds after a transfer fails to retry it.
	int m_nMaxTopRecheckElapsedTime;  // number of seconds after the top item is checked before re-checking it - prevents us from checking the last stuff
	//in the list while something at the top is not being checked, no need to wait for an automation change

	int m_nMaxAutomationBufferMS;  // number of milliseconds after an automation change to wait for further changes.
	int m_nMaxAutomationForceMS;  // maximum number of milliseconds after an automation change to trigger changes. (prevents a situation where continual automation changes keep extending buffer time forever)
	bool m_bUseOnAirTimeForEndpointSchedule;  //prioritizes queue based on on air time of event, not first come first served.
	int m_nAnalysisDwellMS;  // number of milliseconds to wait after an analysis run to re-get the events.

	unsigned long m_ulModsIntervalMS;  // interval on which to check database mods

	char* m_pszLicense;  // the License Key
	char* m_pszIconPath;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

};

#endif // !defined(AFX_DIRECTSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
