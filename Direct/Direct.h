// Direct.h : main header file for the DIRECT application
//

#if !defined(AFX_DIRECT_H__82614209_3A12_4A52_8D88_49B5FD42C967__INCLUDED_)
#define AFX_DIRECT_H__82614209_3A12_4A52_8D88_49B5FD42C967__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif
#import "msxml3.dll" named_guids 
using namespace MSXML2;

// have to add rpcrt4.lib to the linker for UUID stuff


// These are damned useful:
#ifndef max
#define max(a,b)            ((a > b) ? a : b)
#endif

#ifndef min
#define min(a,b)            ((a < b) ? a : b)
#endif                    

// cortex window flags
#define CXWF_DEFAULT			0x00000000
#define CXWF_QUIET_MODE		0x00000001

#include "resource.h"		// main symbols

// various includes might be helpful.
#include <stdio.h>
#include "DirectDefines.h" 
#include "../../Cortex/3.0.4.5/CortexDefines.h" 
#include "../../Cortex/3.0.4.5/CortexShared.h" // included to have xml messaging objects and other shared things
#include "../../Common/IMG/BMP/CBmpUtil_MFC.h" 
#include "../../Common/MSG/Messager.h"
#include "../../Common/MFC/ListCtrlEx/ListCtrlEx.h"
#include "../../Common/TXT/BufferUtil.h" 

/////////////////////////////////////////////////////////////////////////////
// CDirectApp:
// See Direct.cpp for the implementation of this class
//

class CDirectApp : public CWinApp
{
public:
	CDirectApp();

//	unsigned long m_ulFlags;
	char* m_pszSettingsURL;
	char* GetSettingsFilename();
	int m_nPID;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDirectApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDirectApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
//	BOOL m_bAutostart;

};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

// some notes
/*
purge files etc.
insert into Settings (category, category_display_name, parameter, value, parameter_display_name, min_len, max_len, pattern, min_value, max_value, compare_type, sortorder, description, sys_only) values ('Purge', 'Purge Files', 'Directory', '', 'Directory', NULL, 256, 'FilePath', NULL, NULL, NULL, 20, 'Purge files are uploaded to this directory. Must have trailing \', 0);

	create table ImportStatus (statusid int, description varchar(256), action bit, buttondesc varchar(64), statuscolor varchar(8));

insert into ImportStatus (statusid, description, action, buttondesc, statuscolor) values (0, 'File not uploaded', 0, '', 'FFFFFF');

insert into ImportStatus (statusid, description, action, buttondesc, statuscolor) values  (1, 'File has been uploaded. ', 1, 'Continue', 'FFFF00');

insert into ImportStatus (statusid, description, action, buttondesc, statuscolor) values  (2, 'Processing', 0, '', '00FF00');

insert into ImportStatus (statusid, description, action, buttondesc, statuscolor) values  (3, 'Completed', 0, '', 'C0C0C0');

insert into ImportStatus (statusid, description, action, buttondesc, statuscolor) values  (-1, 'Error', 0, '', 'FF0000');
*/

#endif // !defined(AFX_DIRECT_H__82614209_3A12_4A52_8D88_49B5FD42C967__INCLUDED_)
