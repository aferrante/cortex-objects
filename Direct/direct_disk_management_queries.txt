1.

past expire date or flags = 8 (deleted) order by file size desc,  last modified asc where last used by direct last used is null or more than 2 days old. where ingest date is null or 0


select destinations_media.file_name from archivist.dbo.file_metadata as file_metadata 
join prospero.dbo.destinations_media as destinations_media on file_metadata.sys_filename = destinations_media.file_name 
where (destinations_media.host = '10.0.0.18' and destinations_media.partition = '$VIDEO' 
and 
(destinations_media.direct_local_last_used < 1150243200 [2 days ago unix time] and destinations_media.direct_local_last_used is NOT NULL)

and (file_metadata.sys_ingest_date > 0 and file_metadata.sys_ingest_date is not NULL))
and (file_metadata.sys_expires_after < 1150455935 [yesterday at midnight unixtime] or file_metadata.sys_file_flags = 8) 

order by destinations_media.file_size desc, destinations_media.direct_local_last_used asc




2.(least used by direct) order by direct_times_used asc, file_size desc where last used by direct last used is null or more than 2 days old and times_used is not null.

select destinations_media.file_name from archivist.dbo.file_metadata as file_metadata 
join prospero.dbo.destinations_media as destinations_media on file_metadata.sys_filename = destinations_media.file_name 
where (destinations_media.host = '10.0.0.18' and destinations_media.partition = '$VIDEO' 
and (destinations_media.direct_local_last_used < 1150243200 [2 days ago unixtime] and destinations_media.direct_local_last_used is NOT NULL)
and (file_metadata.sys_ingest_date > 0 and file_metadata.sys_ingest_date is NOT NULL))
order by destinations_media.direct_local_last_used asc, destinations_media.file_size desc


3. oldest transfer_date  **** REDO THIS ONE WITH THE SETTING

select destinations_media.file_name from archivist.dbo.file_metadata as file_metadata 
join prospero.dbo.destinations_media as destinations_media on file_metadata.sys_filename = destinations_media.file_name 
where (destinations_media.host = '10.0.0.18' and destinations_media.partition = '$VIDEO' 
and (destinations_media.direct_local_last_used < 1150243200 [2 days ago unixtime] and destinations_media.direct_local_last_used is NOT NULL)
and (file_metadata.sys_ingest_date > 0 and file_metadata.sys_ingest_date is NOT NULL))
order by destinations_media.transfer_date desc


**local direct times used

partition and host are supplied

