// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__2731050A_6B3A_4983_B4C8_2293D5FD598E__INCLUDED_)
#define AFX_STDAFX_H__2731050A_6B3A_4983_B4C8_2293D5FD598E__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC OLE automation classes
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#define __tostring(L)			#L
#define __makestring(M,L)		M(L)
#define __line	    __makestring( __tostring, __LINE__ )
#define alert		  __FILE__ "(" __line "): Alert: "

//#pragma message(alert "this is an alert")



//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__2731050A_6B3A_4983_B4C8_2293D5FD598E__INCLUDED_)
