// DirectSettings.cpp: implementation of the CDirectSettings.
//
//////////////////////////////////////////////////////////////////////
#include <stdafx.h>  // for direct debug
#include "DirectMain.h"   // for direct debug
#include "DirectDefines.h"
#include "DirectSettings.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CDirectMain* g_pdirect;
extern CDirectApp theApp;

//////////////////////////////////////////////////////////////////////
// CDirectEndpointFileMap Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirectEndpointFileMap::CDirectEndpointFileMap()
{
	m_pszExt=NULL;
	m_pszDir=NULL;
	m_ulFlags = DIRECT_FLAG_DISABLED;  // various states
}

CDirectEndpointFileMap::~CDirectEndpointFileMap()
{
	if(m_pszExt) free(m_pszExt);
	if(m_pszDir) free(m_pszDir);
}



//////////////////////////////////////////////////////////////////////
// CDirectEndpointObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirectEndpointObject::CDirectEndpointObject()
{
	m_ppMap = NULL;
	m_nNumMaps=0;

	m_ulStatus	= DIRECT_STATUS_UNINIT;
	m_ulFlags = DIRECT_FLAG_DISABLED;  // various states
	m_usType = DIRECT_DEP_UNKNOWN; // not used
	m_pszName = NULL;
	m_pszDBName = NULL;
	m_pszQueue = NULL;  // the Queue table name
	m_pszExchange= NULL;  // the Exchange table name
	m_pszLiveEvents= NULL;  // the LiveEvents table name if applicable
	m_pszDestination= NULL;  // the Destination table name if applicable
	m_pszDestinationMedia = NULL;  // the DestinationsMedia table name if applicable
	m_pszChannel= NULL;  // the Channel table name if applicable
	m_pszConnections= NULL;  // the Connections table name if applicable
	m_pszMetadata= NULL;  // the Metadata table name if applicable
	m_pszFileTypes = NULL;

	m_nModName = -1;
	m_nModDBName = -1;
	m_nModQueue = -1;  // the Queue table name
	m_nModExchange = -1;  // the Exchange table name
	m_nModLiveEvents = -1;  // the LiveEvents table name if applicable
	m_nModDestination = -1;  // the Destination table name if applicable
	m_nModDestinationMedia = -1;  // the DestinationsMedia table name if applicable
	m_nModChannel = -1;  // the Channel table name if applicable
	m_nModConnections = -1;  // the Connections table name if applicable
	m_nModMetadata = -1;  // the Metadata table name if applicable
	m_nModFileTypes = -1;  // the File types table name if applicable

	m_nLastModName = -100;
	m_nLastModDBName = -100;
	m_nLastModQueue = -100;  // the Queue table name
	m_nLastModExchange = -100;  // the Exchange table name
	m_nLastModLiveEvents = -100;  // the LiveEvents table name if applicable
	m_nLastModDestination = -100;  // the Destination table name if applicable
	m_nLastModDestinationMedia = -100;  // the DestinationsMedia table name if applicable
	m_nLastModChannel = -100;  // the Channel table name if applicable
	m_nLastModConnections = -100;  // the Connections table name if applicable
	m_nLastModMetadata = -100;  // the Metadata table name if applicable
	m_nLastModFileTypes = -100;  // the File types table name if applicable

}

CDirectEndpointObject::~CDirectEndpointObject()
{
	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_pszDBName) free(m_pszDBName); // must use malloc to allocate
	if(m_pszQueue) free(m_pszQueue); // must use malloc to allocate
	if(m_pszExchange) free(m_pszExchange); // must use malloc to allocate
	if(m_pszLiveEvents) free(m_pszLiveEvents); // must use malloc to allocate
	if(m_pszDestination) free(m_pszDestination); // must use malloc to allocate
	if(m_pszDestinationMedia) free(m_pszDestinationMedia); // must use malloc to allocate
	if(m_pszChannel) free(m_pszChannel); // must use malloc to allocate
	if(m_pszConnections) free(m_pszConnections); // must use malloc to allocate
	if(m_pszMetadata) free(m_pszMetadata); // must use malloc to allocate
	if(m_pszFileTypes) free(m_pszFileTypes); // must use malloc to allocate
	if((m_nNumMaps>0)&&(m_ppMap))
	{
		int i=0;
		while(i<m_nNumMaps)
		{
			if(m_ppMap[i]) free(m_ppMap[i]);
			i++;
		}
		delete [] m_ppMap;
	}
}

int CDirectEndpointObject::IncrementDatabaseMods(CDBUtil* pdb, CDBconn* pdbConn, char* pszTableName, char* pszInfo)
{
	if((g_pdirect)&&(pdbConn)&&(pdb)&&(pszTableName)&&(strlen(pszTableName)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );
	
	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s set mod = \
(SELECT case when max(mod) is null then 1 when max(mod) >= %d \
then 1 else max(mod) + 1 end from %s.dbo.%s WHERE criterion = '%s') WHERE criterion = '%s'",
		m_pszDBName?m_pszDBName:m_pszName, m_pszExchange?m_pszExchange:"Exchange",
		DIRECT_DB_MOD_MAX,
		m_pszDBName?m_pszDBName:m_pszName, m_pszExchange?m_pszExchange:"Exchange",
		szTemp, szTemp		
		);
		
		EnterCriticalSection(&g_pdirect->m_data.m_critSQL);
		if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);
			return DIRECT_SUCCESS;
		}
		LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);
	}
	return DIRECT_ERROR;
}

int CDirectEndpointObject::GetFileMaps(CDBUtil* pdb, CDBconn* pdbConn)
{
	if((g_pdirect)&&(pdbConn)&&(pdb)&&(m_pszDBName)&&(strlen(m_pszDBName)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT DISTINCT criterion, partition FROM %s.dbo.%s", 
			m_pszDBName?m_pszDBName:m_pszName,
			(((m_pszFileTypes)&&(strlen(m_pszFileTypes)))?m_pszFileTypes:"FileTypeMapping")
			);

//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "rule SQL: %s", szSQL);  Sleep(100); //(Dispatch message)

//		ReleaseRecordSet();
		EnterCriticalSection(&g_pdirect->m_data.m_critSQL);
		CRecordset* prs = pdb->Retrieve(pdbConn, szSQL, errorstring);
		if(prs)
		{
			strcpy(errorstring, "");
			int nReturn = DIRECT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szExt;
				CString szDir;
				bool bFound = false;
				try
				{
					prs->GetFieldValue("criterion", szExt);//HARDCODE
					szExt.TrimLeft(); szExt.TrimRight();

					prs->GetFieldValue("partition", szDir);//HARDCODE
					szDir.TrimLeft(); szDir.TrimRight();
					
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "rule obtained: %d", nRuleID);  Sleep(100); //(Dispatch message)

				}
				catch( ... )
				{
g_pdirect->m_msgr.DM(MSG_ICONERROR, NULL, "DiReCT:debug", "exception obtaining destination file type maps"); //(Dispatch message)
				}

				if((m_ppMap)&&(m_nNumMaps))
				{
					int nTemp=0;
					while(nTemp<m_nNumMaps)
					{
						if(m_ppMap[nTemp])
						{
							if((m_ppMap[nTemp]->m_pszExt)&&(szExt.CompareNoCase(m_ppMap[nTemp]->m_pszExt)==0)) 
							{
								bFound = true;
								// override with the new changes:
								if(
									  (m_ppMap[nTemp]->m_pszDir==NULL)
									||((m_ppMap[nTemp]->m_pszDir)&&(szDir.Compare(m_ppMap[nTemp]->m_pszDir)))
									)
								{
									if(m_ppMap[nTemp]->m_pszDir) free(m_ppMap[nTemp]->m_pszDir);
									m_ppMap[nTemp]->m_pszDir = (char*)malloc(szDir.GetLength()+1);
									if(m_ppMap[nTemp]->m_pszDir)
									{
										sprintf(m_ppMap[nTemp]->m_pszDir, "%s", szDir);
									}
								}	
								m_ppMap[nTemp]->m_ulFlags = DIRECT_FLAG_FOUND;

							}
						}
						nTemp++;
					}
				}
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "finished search for rule %d", nRuleID);  Sleep(100); //(Dispatch message)

				if((!bFound)&&(szExt.GetLength()>0)) // have to add.
				{
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "adding rule %d", nRuleID);  Sleep(100); //(Dispatch message)

					CDirectEndpointFileMap* pscho = new CDirectEndpointFileMap;
					if(pscho)
					{
						CDirectEndpointFileMap** ppObj = new CDirectEndpointFileMap*[m_nNumMaps+1];
						if(ppObj)
						{
							int o=0;
							if((m_ppMap)&&(m_nNumMaps>0))
							{
								while(o<m_nNumMaps)
								{
									ppObj[o] = m_ppMap[o];
									o++;
								}
								delete [] m_ppMap;

							}
							ppObj[m_nNumMaps] = pscho;
							m_ppMap = ppObj;
							m_nNumMaps++;

//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "adding rule %d, type is %d", nRuleID, nType);  Sleep(100); //(Dispatch message)
							// override with the new changes:
							pscho->m_pszExt = (char*)malloc(szExt.GetLength()+1);
							if(pscho->m_pszExt)
							{
								sprintf(pscho->m_pszExt, "%s", szExt);
							}
							pscho->m_pszDir = (char*)malloc(szDir.GetLength()+1);
							if(pscho->m_pszDir)
							{
								sprintf(pscho->m_pszDir, "%s", szDir);
							}
							
							pscho->m_ulFlags = DIRECT_FLAG_FOUND;

						}
						else
							delete pscho;
					}
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			prs = NULL;
//	g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "removing non found rules");  Sleep(100); //(Dispatch message)
			LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<m_nNumMaps)
			{
				if((m_ppMap)&&(m_ppMap[nIndex]))
				{
					if((m_ppMap[nIndex]->m_ulFlags)&DIRECT_FLAG_FOUND)
					{
						(m_ppMap[nIndex]->m_ulFlags) &= ~DIRECT_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(m_ppMap[nIndex])
						{
							delete m_ppMap[nIndex];
							m_nNumMaps--;

							int nTemp=nIndex;
							while(nTemp<m_nNumMaps)
							{
								m_ppMap[nTemp]=m_ppMap[nTemp+1];
								nTemp++;
							}
							m_ppMap[nTemp] = NULL;
						} else nIndex++;
					}
				}
				else
					nIndex++;
			}

			return nReturn;
		}
		LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);
	}
	return DIRECT_ERROR;
}


int CDirectEndpointObject::CheckDatabaseMod(CDBUtil* pdb, CDBconn* pdbConn, char* pszTableName, char* pszInfo)
{
	if((pdbConn)&&(pdb)&&(m_pszDBName)&&(strlen(m_pszDBName))&&(pszTableName)&&(strlen(pszTableName)))
	{		
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s.dbo.%s", 
			m_pszDBName,
			((m_pszExchange)&&(strlen(m_pszExchange)))?m_pszExchange:"Exchange");

//g_pdirect->m_msgr.DM(MSG_ICONHAND, NULL, "DiReCT:debug", "endpoint SQL: %s", szSQL);   Sleep(50);//(Dispatch message)

		EnterCriticalSection(&g_pdirect->m_data.m_critSQL);
		CRecordset* prs = pdb->Retrieve(pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = DIRECT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCriterion;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
				}

				szTemp.Format("DBT_%s", pszTableName);
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					nReturn = atoi(szMod);
					if(nReturn>0) 
					{
						prs->Close();

						delete prs;
						prs = NULL;
						LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);
						return nReturn;
					}
				}

				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			delete prs;
			prs = NULL;
			LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);
			return nReturn;
		}
		LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);
	}
	return DIRECT_ERROR;
}



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirectSettings::CDirectSettings()
{
	m_pdb = NULL;
	m_pdbConn = NULL;

	m_pszName = NULL;
	m_pszType = NULL;  // familiar name of the type.
	m_pszProject = NULL;  // familiar name of the project name.
	m_ulMainMode = DIRECT_MODE_DEFAULT;

	// ports
	m_usCommandPort	= DIRECT_PORT_CMD;
	m_usStatusPort	= DIRECT_PORT_STATUS;

	m_nThreadDwellMS = 1000;
	m_bDebugLists= false;  // if true, dumps the temp and events databases
	m_nDebugListTopCount=-1;  //if positive, just prints out the top n recored in the events tables.
	m_bDebugSQL=false;  // if true, prints out the SQL calls
	m_bDebugInsertSQL=false;  // if true, prints out the event insert SQL calls
	m_ulDebug = 0;
	m_pszDebugOrder = NULL;

	m_nAutoPurgeMessageDays = 30; // default
//	m_nAutoPurgeAsRunDays = 30; // default
	m_nAutoPurgeInterval = 300;  // 5 minutes should be enough.
	m_nTempFilePurgeInterval = 432000;  // days, in seconds

	// messaging for Direct
	m_bUseLog = true;			// write a log file
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host
	m_pszFileSpec = NULL;
	m_pszMailSpec = NULL;
	m_pszProcessedFileSpec = NULL;
	m_pszProcessedMailSpec = NULL;
	m_bUseXMLClientLog = false;
	m_bMillisecondMessaging=true;			// use millisecond resolution for messages and asrun

	// Harris API
//	m_bUseListCount = false; // get all events up until the list count (otherwise just up to the lookahead)
	m_nFilterType =-1;  // where event type = X, if>=0, etc.
	m_nTypeComparator =-1;  
	m_pszAdditionalWhereClause=NULL;  // to further restrict the event pull - must start with " AND "... to be added to the end of the thing


	// installed dependencies
	m_ppEndpointObject=NULL; // which endpoint module(s) is (are) installed.
	m_nNumEndpointsInstalled=0; // number of endpoint module(s) is (are) installed.

	m_bReportSuccessfulOperation=false;
	m_bLogTransfers=false;
	m_bClearEventsOnStartup = true;  // clears events on startup



	// DSN params
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;

	m_pszDefaultDB = NULL;  // the Default DB name

	// file handling
	m_pszRenamePrefix= NULL;//   Filename conversion prefix, Prepended to filename for disk storage.   defval = ""  string max64, min0 (i.e. can be blank)
	m_pszRenameSuffix= NULL;//   Filename conversion suffix, Appended to filename for disk storage.  defval = "H"  string max64, min0 (i.e. can be blank)

	m_pszSettings = NULL;  // the Settings table name
	m_pszExchange = NULL;  // the Exchange table name
	m_pszMessages = NULL;  // the Messages table name

	m_pszQueue = NULL;  // the Queue table name
//	m_pszChannels = NULL;  // the Channels table name
//	m_pszConnections = NULL;  // the Connections table name
	m_pszLiveEvents = NULL; // the LiveEvents table name
	m_pszMapping = NULL;  // the m_pszMapping table name
	m_pszChannelDests = NULL;

	m_pszSystemFolderPath = NULL;			// the path of the folder used for parse files etc.
	m_pszTempFolderPath = NULL;			// the path of the folder used for parse files etc. = gets cleared on startup!
	m_nMaxTopRecheckElapsedTime = 30;  // number of seconds after the top item is checked before re-checking it - prevents us from checking the last stuff
	//in the list while something at the top is not being checked, no need to wait for an automation change

	//imagestore specific 
	m_pszImagestoreNullFile = NULL;			// the filename of the nullfile - must be in the system folder
	m_pszIntuitionSearchExt = NULL;				// search extensions for Intuition
	m_pszImagestore2SearchExt = NULL;			// search extensions for IS2
	m_pszImagestore300SearchExt = NULL;		// search extensions for IS300
	m_pszImagestore300HDSearchExt = NULL;		// search extensions for IS300 HD
	m_pszImagestoreHDSearchExt = NULL;		// search extensions for ISHD
	m_pszIntuitionHDSearchExt = NULL;		// search extensions for INTHD
	m_pszIntuitionRenameExt = NULL;//  Intution File Conversion Extensions,  Comma separated list of file extensions to allow filename conversion on the Intuition. def val = "tem"
	m_pszImagestoreRenameExt = NULL;//  Imagestore File Conversion Extensions,  Comma separated list of file extensions to allow filename conversion on the Imagestore.   def val = oxa,oxt,oxe"

	m_pszIDPreprocessString = NULL;//  convert ID space to filename space

	// endpoint autodeletions
	m_bAutoDeleteOldest = true;
	m_ulDeletionThreshold = 172800; // two days in seconds

	m_bPropagateChangedFiles = true;

	m_nMaxAutomationBufferMS = 5000;  // number of milliseconds after an automation change to wait for further changes.
	m_nMaxAutomationForceMS = 10000;  // maximum number of milliseconds after an automation change to trigger changes. (prevents a situation where continual automation changes keep extending buffer time forever)
	m_bUseOnAirTimeForEndpointSchedule = true;  //prioritizes queue based on on air time of event, not first come first served.
	m_nAnalysisDwellMS = 2000;  // number of milliseconds to wait after an analysis run to re-get the events.



	m_ulModsIntervalMS = 6000;
	m_nTransferFailureRetryTime = 60;  // seconds - one minute as a default
	m_pszLicense=NULL;  // the License Key
	m_pszIconPath=NULL;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.
}

CDirectSettings::~CDirectSettings()
{
	if(m_pszTempFolderPath) free(m_pszTempFolderPath); // must use malloc to allocate
	if(m_pszSystemFolderPath) free(m_pszSystemFolderPath); // must use malloc to allocate
	if(m_pszImagestoreNullFile) free(m_pszImagestoreNullFile); // must use malloc to allocate

	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_pszType) free(m_pszType); // must use malloc to allocate
	if(m_pszProject) free(m_pszProject); // must use malloc to allocate
	if(m_pszDSN) free(m_pszDSN); // must use malloc to allocate
	if(m_pszUser) free(m_pszUser); // must use malloc to allocate
	if(m_pszPW) free(m_pszPW); // must use malloc to allocate
	if(m_pszDefaultDB) free(m_pszDefaultDB); // must use malloc to allocate

	if(m_pszSettings) free(m_pszSettings); // must use malloc to allocate
	if(m_pszExchange) free(m_pszExchange); // must use malloc to allocate
	if(m_pszMessages) free(m_pszMessages); // must use malloc to allocate

//	if(m_pszChannels) free(m_pszChannels); // must use malloc to allocate
//	if(m_pszConnections) free(m_pszConnections); // must use malloc to allocate
	if(m_pszLiveEvents) free(m_pszLiveEvents); // must use malloc to allocate
	if(m_pszQueue) free(m_pszQueue); // must use malloc to allocate
	if(m_pszMapping) free(m_pszMapping); // must use malloc to allocate
	if(m_pszLicense) free(m_pszLicense); // must use malloc to allocate
	if(m_pszIconPath) free(m_pszIconPath); // must use malloc to allocate	

	if(m_pszFileSpec) free(m_pszFileSpec); // must use malloc to allocate	
	if(m_pszMailSpec) free(m_pszMailSpec); // must use malloc to allocate	
	if(m_pszProcessedFileSpec) free(m_pszProcessedFileSpec); // must use malloc to allocate	
	if(m_pszProcessedMailSpec) free(m_pszProcessedMailSpec); // must use malloc to allocate	

	if(m_pszDebugOrder) free(m_pszDebugOrder); // must use malloc to allocate	
	if(m_pszChannelDests) free(m_pszChannelDests); // must use malloc to allocate	
	

	if(m_pszRenamePrefix) free(m_pszRenamePrefix); // must use malloc to allocate	
	if(m_pszRenameSuffix) free(m_pszRenameSuffix); // must use malloc to allocate	
	if(m_pszImagestoreNullFile) free(m_pszImagestoreNullFile); // must use malloc to allocate	
	if(m_pszIntuitionSearchExt) free(m_pszIntuitionSearchExt); // must use malloc to allocate	
	if(m_pszImagestore300SearchExt) free(m_pszImagestore300SearchExt); // must use malloc to allocate	
	if(m_pszImagestore300HDSearchExt) free(m_pszImagestore300HDSearchExt); // must use malloc to allocate	
	if(m_pszImagestoreHDSearchExt) free(m_pszImagestoreHDSearchExt); // must use malloc to allocate	
	if(m_pszIntuitionHDSearchExt) free(m_pszIntuitionHDSearchExt); // must use malloc to allocate	
	if(m_pszImagestoreRenameExt) free(m_pszImagestoreRenameExt); // must use malloc to allocate	
	if(m_pszIntuitionRenameExt) free(m_pszIntuitionRenameExt); // must use malloc to allocate	
	if(m_pszIDPreprocessString) free(m_pszIDPreprocessString); // must use malloc to allocate	
	

	if(m_ppEndpointObject)
	{
		int i=0;
		while(i<m_nNumEndpointsInstalled)
		{
			if(m_ppEndpointObject[i]) delete m_ppEndpointObject[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppEndpointObject; // delete array of pointers to objects, must use new to allocate
	}

}

int CDirectSettings::Settings(bool bRead)
{
	char pszFilename[MAX_PATH];
	strcpy(pszFilename, "");
//		AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, DIRECT_SETTINGS_FILE_DEFAULT);  // cortex settings file
	CFileUtil file;
	// get settings.
	file.GetSettings(pszFilename, false); 

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
		// load up the values on the settings object
			m_pszName = file.GetIniString("Main", "Name", "DiReCT", m_pszName);
			m_pszType = file.GetIniString("Main", "Type", "DiReCT", m_pszType);
			m_pszProject = file.GetIniString("Main", "Project", "VDS", m_pszProject);
			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);
			m_nThreadDwellMS = file.GetIniInt("Main", "ThreadDwellMS", 1000);

			m_bDebugLists = file.GetIniInt("Database", "DebugLists", 0)?true:false;  // if true, dumps the temp and events databases
			m_nDebugListTopCount = file.GetIniInt("Database", "DebugListTopCount", -1);  //if positive, just prints out the top n recored in the events tables.
			m_bDebugSQL = file.GetIniInt("Database", "DebugSQL", 0)?true:false;  // if true, prints out the SQL calls
			m_bDebugInsertSQL = file.GetIniInt("Database", "DebugInsertSQL", 0)?true:false;  // if true, prints out the event insert SQL calls
			m_ulDebug = file.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.
			m_pszDebugOrder = file.GetIniString("Database", "DebugOrder", "(case when parent_position < 0 then ((event_position)/1000.0) else (parent_calc_start + (event_position-parent_position)/1000.0) end)", m_pszDebugOrder); // the order by clause for debug


			// compile license key params
			if(g_pdirect->m_data.m_key.m_pszLicenseString) free(g_pdirect->m_data.m_key.m_pszLicenseString);
			g_pdirect->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(m_pszLicense)+1);
			if(g_pdirect->m_data.m_key.m_pszLicenseString)
			sprintf(g_pdirect->m_data.m_key.m_pszLicenseString, "%s", m_pszLicense);

			g_pdirect->m_data.m_key.InterpretKey();

			char errorstring[MAX_MESSAGE_LENGTH];
			if(g_pdirect->m_data.m_key.m_bValid)
			{
				unsigned long i=0;
				while(i<g_pdirect->m_data.m_key.m_ulNumParams)
				{
					if((g_pdirect->m_data.m_key.m_ppszParams)
						&&(g_pdirect->m_data.m_key.m_ppszValues)
						&&(g_pdirect->m_data.m_key.m_ppszParams[i])
						&&(g_pdirect->m_data.m_key.m_ppszValues[i]))
					{
						if(stricmp(g_pdirect->m_data.m_key.m_ppszParams[i], "max")==0)
						{
	//						g_pdirect->m_data.m_nMaxLicensedDevices = atoi(g_pdirect->m_data.m_key.m_ppszValues[i]);
						}
					}
					i++;
				}

				if(
						(
							(!g_pdirect->m_data.m_key.m_bExpires)
						||((g_pdirect->m_data.m_key.m_bExpires)&&(!g_pdirect->m_data.m_key.m_bExpired))
						||((g_pdirect->m_data.m_key.m_bExpires)&&(g_pdirect->m_data.m_key.m_bExpireForgiveness)&&(g_pdirect->m_data.m_key.m_ulExpiryDate+g_pdirect->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!g_pdirect->m_data.m_key.m_bMachineSpecific)
						||((g_pdirect->m_data.m_key.m_bMachineSpecific)&&(g_pdirect->m_data.m_key.m_bValidMAC))
						)
					)
				{
					// this overrides stuff so let's not let it do that.

										// let's just not override this in a green condition.  
										// comment it out.
//					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//					g_pdirect->m_data.SetStatusText(errorstring, DIRECT_STATUS_OK);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
					g_pdirect->m_data.SetStatusText(errorstring, DIRECT_STATUS_ERROR);
				}
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				g_pdirect->m_data.SetStatusText(errorstring, DIRECT_STATUS_ERROR);
			}

			m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", DIRECT_PORT_CMD);
			m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", DIRECT_PORT_STATUS);

			m_pszIconPath = file.GetIniString("FileServer", "IconPath", "C:\\Inetpub\\wwwroot\\Cortex\\direct\\images\\", m_pszIconPath);

			m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
			m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;
			m_bUseXMLClientLog = file.GetIniInt("Messager", "UseXMLClientLog", 0)?true:false;
			m_bMillisecondMessaging = file.GetIniInt("Messager", "MillisecondMessaging", 1)?true:false;			// use millisecond resolution for messages and asrun

			m_nAutoPurgeMessageDays = file.GetIniInt("AutoPurge", "AutoPurgeMessageDays", 30);
	//		m_nAutoPurgeAsRunDays = file.GetIniInt("AutoPurge", "AutoPurgeAsRunDays", 30);
			m_nAutoPurgeInterval  = file.GetIniInt("AutoPurge", "AutoPurgeInterval", 300);  // 5 minutes should be enough.

			m_pszLiveEvents = file.GetIniString("Database", "LiveEventsTableName", "Events",m_pszLiveEvents);  // the LiveEvents table name
			m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue",m_pszQueue);  // the Queue table name
			m_pszMapping = file.GetIniString("Database", "MappingTableName", "Mapping", m_pszMapping);  // the Mapping table name
			m_pszChannelDests = file.GetIniString("Database", "ChannelDestsInfoViewName", "ChannelInfo", m_pszChannelDests); 

			m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
			m_nTransferFailureRetryTime = file.GetIniInt("FileHandling", "TransferFailureRetryTime", 60);  // in seconds
			m_bAutoDeleteOldest = file.GetIniInt("FileHandling", "AutoDeleteOldest", 1)?true:false;
			m_ulDeletionThreshold = file.GetIniInt("FileHandling", "DeletionThreshold", 172800); // two days

			m_pszTempFolderPath = file.GetIniString("FileHandling", "TempFolder", "C:\\DiReCT_Temp\\", m_pszTempFolderPath);  // // the path of the folder used for parse files etc. = gets cleared on startup! must have trailing slash
			m_pszSystemFolderPath = file.GetIniString("FileHandling", "SystemFolder", "C:\\", m_pszSystemFolderPath);  // must have trailing slash
			m_bPropagateChangedFiles = file.GetIniInt("FileHandling", "PropagateChangedFiles", 1)?true:false;
			m_pszRenamePrefix = file.GetIniString("FileHandling", "RenamePrefix", "", m_pszRenamePrefix);//   Filename conversion prefix, Prepended to filename for disk storage.   defval = ""  string max64, min0 (i.e. can be blank)
			m_pszRenameSuffix = file.GetIniString("FileHandling", "RenameSuffix", "H", m_pszRenameSuffix);//   Filename conversion suffix, Appended to filename for disk storage.  defval = "H"  string max64, min0 (i.e. can be blank)
			m_bClearEventsOnStartup  = file.GetIniInt("FileHandling", "ClearEventsOnStartup", 1)?true:false;  // clears events on startup


			m_nMaxTopRecheckElapsedTime = file.GetIniInt("FileHandling", "MaxTopRecheckElapsedTime", 30);  // number of seconds after the top item is checked before re-checking it - prevents us from checking the last stuff
			//in the list while something at the top is not being checked, no need to wait for an automation change

			m_nMaxAutomationBufferMS = file.GetIniInt("FileHandling", "MaxAutomationBufferMS",5000);  // number of milliseconds after an automation change to wait for further changes.
			m_nMaxAutomationForceMS  = file.GetIniInt("FileHandling", "MaxAutomationForceMS",10000);  // maximum number of milliseconds after an automation change to trigger changes. (prevents a situation where continual automation changes keep extending buffer time forever)
			m_bUseOnAirTimeForEndpointSchedule  = file.GetIniInt("FileHandling", "UseOnAirTimeForEndpointSchedule", 1)?true:false;  //prioritizes queue based on on air time of event, not first come first served.
			m_nAnalysisDwellMS  = file.GetIniInt("FileHandling", "AnalysisDwellMS", 2000);  // number of milliseconds to wait after an analysis run to re-get the events.
			m_nTempFilePurgeInterval  = file.GetIniInt("FileHandling", "TempFilePurgeInterval", 432000);  // days, in seconds


			m_pszImagestoreNullFile = file.GetIniString("Imagestore", "NullOXTFile", "null.oxt", m_pszImagestoreNullFile);  // name of the null file, must be in system folder
			m_pszIntuitionSearchExt = file.GetIniString("Imagestore", "IntuitionSearchExt", "tem", m_pszIntuitionSearchExt); // search extensions for Intuition
			m_pszImagestore2SearchExt = file.GetIniString("Imagestore", "Imagestore2SearchExt", "oxa,oxt,oxw", m_pszImagestore2SearchExt);			// search extensions for IS2
			m_pszImagestore300SearchExt = file.GetIniString("Imagestore", "Imagestore300SearchExt", "oxa,oxt,oxe", m_pszImagestore300SearchExt);		// search extensions for IS300
			m_pszImagestore300HDSearchExt = file.GetIniString("Imagestore", "Imagestore300HDSearchExt", "oxa,oxt,oxe", m_pszImagestore300HDSearchExt);		// search extensions for IS300
			m_pszImagestoreHDSearchExt = file.GetIniString("Imagestore", "ImagestoreHDSearchExt", "oxa,oxt,oxe", m_pszImagestoreHDSearchExt);		// search extensions for ISHD
			m_pszIntuitionHDSearchExt = file.GetIniString("Imagestore", "IntuitionHDSearchExt", "tem", m_pszIntuitionHDSearchExt);			// search extensions for INTHD
			m_pszIntuitionRenameExt = file.GetIniString("Imagestore", "IntuitionRenameExt", "tem", m_pszIntuitionRenameExt);//  Intution File Conversion Extensions,  Comma separated list of file extensions to allow filename conversion on the Intuition. def val = "tem"
			m_pszImagestoreRenameExt = file.GetIniString("Imagestore", "ImagestoreRenameExt", "oxa,oxt,oxe", m_pszImagestoreRenameExt);//  Imagestore File Conversion Extensions,  Comma separated list of file extensions to allow filename conversion on the Imagestore.   def val = oxa,oxt,oxe"

			m_pszIDPreprocessString  = file.GetIniString("FileHandling", "IDPreprocessString", "", m_pszIDPreprocessString);//  convert ID space to filename space
			if(m_pszIDPreprocessString) 
			{
				if(strlen(m_pszIDPreprocessString)==0) 
				{
					try{ free(m_pszIDPreprocessString); } catch(...){}
					m_pszIDPreprocessString = NULL;
				}
			}

			m_nFilterType = file.GetIniInt("Automation", "FilterEventType", -1);
			m_nTypeComparator  = file.GetIniInt("Automation", "FilterEventTypeComparator", -1);  
			m_pszAdditionalWhereClause = file.GetIniString("Automation", "AdditionalWhereClause", "", m_pszAdditionalWhereClause);  // to further restrict the event pull - must start with " AND "... to be added to the end of the thing


			m_pszDSN = file.GetIniString("Database", "DSN", (m_pszName?m_pszName:"Direct"), m_pszDSN);
			m_pszUser = file.GetIniString("Database", "DBUser", "sa", m_pszUser);
			m_pszPW = file.GetIniString("Database", "DBPassword", "", m_pszPW);
			m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings", m_pszSettings);  // the Settings table name
			m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange", m_pszExchange);  // the Exchange table name
			m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages", m_pszMessages);  // the Messages table name

			m_pszFileSpec = file.GetIniString("Messager", "LogFileIni", "Logs\\DiReCT|YD||1|", m_pszFileSpec);
				// SMTP address[: override port (optional)] | distribution lists | from email |  subject spec | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
			m_pszMailSpec = file.GetIniString("Messager", "SMTPIni", "mail.server.com|f3:recipient@address.com|\"someone\"<from@here.com>|%P %T from %h|%h|C:\\DiReCTsmtp.txt|1|1|0", m_pszMailSpec);
			if(m_pszFileSpec) 
			{
				if(m_pszProcessedFileSpec)
				{
					try{ free(m_pszProcessedFileSpec); } catch(...) {}
				}
				m_pszProcessedFileSpec = ProcessString(m_pszFileSpec, false);
			}
			
			if(m_pszMailSpec) 
			{
				if(m_pszProcessedMailSpec)
				{
					try{ free(m_pszProcessedMailSpec); } catch(...) {}
				}
				m_pszProcessedMailSpec = ProcessString(m_pszMailSpec, false);
			}

		}
		else //write
		{

			file.SetIniString("Main", "Name", m_pszName);
			file.SetIniString("Main", "Type", m_pszType);
			file.SetIniString("Main", "Project", m_pszProject);
			file.SetIniInt("CommandServer", "ListenPort", m_usCommandPort);
			file.SetIniInt("StatusServer", "ListenPort", m_usStatusPort);
			file.SetIniString("License", "Key", m_pszLicense);
			file.SetIniInt("Main", "ThreadDwellMS", m_nThreadDwellMS);

			file.SetIniInt("Database", "DebugLists", m_bDebugLists?1:0);  // if true, dumps the temp and events databases
			file.SetIniInt("Database", "DebugListTopCount", m_nDebugListTopCount);  //if positive, just prints out the top n recored in the events tables.
			file.SetIniInt("Database", "DebugSQL", m_bDebugSQL?1:0);  // if true, prints out the SQL calls
			file.SetIniInt("Database", "DebugInsertSQL", m_bDebugInsertSQL?1:0);  // if true, prints out the event insert SQL calls
			file.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.
			file.SetIniString("Database", "DebugOrder", m_pszDebugOrder); // the order by clause for debug

			file.SetIniString("FileServer", "IconPath", m_pszIconPath);

			file.SetIniInt("Messager", "UseEmail", m_bUseEmail?1:0);
			file.SetIniInt("Messager", "UseNet", m_bUseNetwork?1:0);
			file.SetIniInt("Messager", "UseLog", m_bUseLog?1:0);
			file.SetIniInt("Messager", "ReportSuccessfulOperation", m_bReportSuccessfulOperation?1:0);
			file.SetIniInt("Messager", "LogTransfers", m_bLogTransfers?1:0);
			file.SetIniInt("Messager", "UseXMLClientLog", m_bUseXMLClientLog?true:false);
			file.SetIniInt("Messager", "MillisecondMessaging", m_bMillisecondMessaging?true:false);		// use millisecond resolution for messages and asrun

			file.SetIniString("Database", "DSN", m_pszDSN);
			file.SetIniString("Database", "DBUser", m_pszUser);
			file.SetIniString("Database", "DBPassword", m_pszPW);
			file.SetIniString("Database", "SettingsTableName", m_pszSettings);  // the Settings table name
			file.SetIniString("Database", "ExchangeTableName", m_pszExchange);  // the Exchange table name
			file.SetIniString("Database", "MessagesTableName", m_pszMessages);  // the Messages table name


			file.SetIniString("Database", "LiveEventsTableName", m_pszLiveEvents);  // the LiveEvents table name
			file.SetIniString("Database", "QueueTableName", m_pszQueue);  // the Queue table name
			file.SetIniString("Database", "MappingTableName", m_pszMapping);  // the Rules table name
			file.SetIniString("Database", "ChannelDestsInfoViewName", m_pszChannelDests); 

			file.SetIniInt("Database", "ModificationCheckInterval", m_ulModsIntervalMS);  // in milliseconds
			file.SetIniInt("FileHandling", "TransferFailureRetryTime", m_nTransferFailureRetryTime);  // in seconds
			file.SetIniInt("FileHandling", "AutoDeleteOldest", m_bAutoDeleteOldest?1:0);
			file.SetIniInt("FileHandling", "DeletionThreshold", m_ulDeletionThreshold ); // two days
			file.SetIniInt("FileHandling", "PropagateChangedFiles", m_bPropagateChangedFiles?1:0);
			file.SetIniString("FileHandling", "RenamePrefix", m_pszRenamePrefix);//   Filename conversion prefix, Prepended to filename for disk storage.   defval = ""  string max64, min0 (i.e. can be blank)
			file.SetIniString("FileHandling", "RenameSuffix", m_pszRenameSuffix);//   Filename conversion suffix, Appended to filename for disk storage.  defval = "H"  string max64, min0 (i.e. can be blank)
			file.SetIniInt("FileHandling", "MaxTopRecheckElapsedTime", m_nMaxTopRecheckElapsedTime);  // number of seconds after the top item is checked before re-checking it - prevents us from checking the last stuff
			//in the list while something at the top is not being checked, no need to wait for an automation change

			file.SetIniInt("FileHandling", "MaxAutomationBufferMS", m_nMaxAutomationBufferMS);  // number of milliseconds after an automation change to wait for further changes.
			file.SetIniInt("FileHandling", "MaxAutomationForceMS",m_nMaxAutomationForceMS );  // maximum number of milliseconds after an automation change to trigger changes. (prevents a situation where continual automation changes keep extending buffer time forever)
			file.SetIniInt("FileHandling", "UseOnAirTimeForEndpointSchedule", m_bUseOnAirTimeForEndpointSchedule?1:0);  //prioritizes queue based on on air time of event, not first come first served.
			file.SetIniInt("FileHandling", "AnalysisDwellMS", m_nAnalysisDwellMS);  // number of milliseconds to wait after an analysis run to re-get the events.
			file.SetIniInt("FileHandling", "ClearEventsOnStartup", m_bClearEventsOnStartup?1:0);  // clears events on startup


			file.SetIniInt("Automation", "FilterEventType", m_nFilterType);
			file.SetIniInt("Automation", "FilterEventTypeComparator", m_nTypeComparator );  
			file.SetIniString("Automation", "AdditionalWhereClause", m_pszAdditionalWhereClause);  // to further restrict the event pull - must start with " AND "... to be added to the end of the thing


			file.SetIniString("FileHandling", "SystemFolder", m_pszSystemFolderPath);  // must have trailing slash
			file.SetIniString("FileHandling", "TempFolder", m_pszTempFolderPath);  // // the path of the folder used for parse files etc. = gets cleared on startup! must have trailing slash

			file.SetIniString("FileHandling", "IDPreprocessString", m_pszIDPreprocessString);//  convert ID space to filename space
			file.SetIniInt("FileHandling", "TempFilePurgeInterval", m_nTempFilePurgeInterval);  // in seconds


			file.SetIniString("Imagestore", "NullOXTFile", m_pszImagestoreNullFile );  // name of the null file, must be in system folder
			file.SetIniString("Imagestore", "IntuitionSearchExt", m_pszIntuitionSearchExt); // search extensions for Intuition
			file.SetIniString("Imagestore", "Imagestore2SearchExt", m_pszImagestore2SearchExt);			// search extensions for IS2
			file.SetIniString("Imagestore", "Imagestore300SearchExt", m_pszImagestore300SearchExt);		// search extensions for IS300
			file.SetIniString("Imagestore", "Imagestore300HDSearchExt", m_pszImagestore300HDSearchExt);		// search extensions for IS300
			file.SetIniString("Imagestore", "ImagestoreHDSearchExt", m_pszImagestoreHDSearchExt);		// search extensions for ISHD
			file.SetIniString("Imagestore", "IntuitionHDSearchExt", m_pszIntuitionHDSearchExt);			// search extensions for INTHD
			file.SetIniString("Imagestore", "IntuitionRenameExt", m_pszIntuitionRenameExt);//  Intution File Conversion Extensions,  Comma separated list of file extensions to allow filename conversion on the Intuition. def val = "tem"
			file.SetIniString("Imagestore", "ImagestoreRenameExt", m_pszImagestoreRenameExt);//  Imagestore File Conversion Extensions,  Comma separated list of file extensions to allow filename conversion on the Imagestore.   def val = oxa,oxt,oxe"


			file.SetIniInt("AutoPurge", "AutoPurgeMessageDays", m_nAutoPurgeMessageDays);
//			file.SetIniInt("AutoPurge", "AutoPurgeAsRunDays", m_nAutoPurgeAsRunDays);
			file.SetIniInt("AutoPurge", "AutoPurgeInterval", m_nAutoPurgeInterval);  // 5 minutes should be enough.

			file.SetIniString("Messager", "LogFileIni", m_pszFileSpec);
			file.SetIniString("Messager", "SMTPIni", m_pszMailSpec);

			file.SetSettings(pszFilename, false);  // have to have correct filename

		}
		return DIRECT_SUCCESS;
	}
	return DIRECT_ERROR;
}

int CDirectSettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	if((m_pszSettings)&&(strlen(m_pszSettings)>0)&&(m_pdbConn)&&(m_pdb)&&(m_pdbConn->m_bConnected)&&(Settings(true)==DIRECT_SUCCESS))  //read has to succeed
	{
		// get settings.
/*
		char pszFilename[MAX_PATH];

		strcpy(pszFilename, DIRECT_SETTINGS_FILE_DEFAULT);  // direct settings file

		CFileUtil file;
		file.GetSettings(pszFilename, false); 

		// load up the values on the settings object
		if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
		{
			g_pdirect->m_settings.m_pszName = file.GetIniString("Main", "Name", "Direct");
			g_pdirect->m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");

			g_pdirect->m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");

			g_pdirect->m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", DIRECT_PORT_CMD);
			g_pdirect->m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", DIRECT_PORT_STATUS);

			g_pdirect->m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			g_pdirect->m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			g_pdirect->m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			g_pdirect->m_settings.m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
			g_pdirect->m_settings.m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;

	//		g_pdirect->m_settings.m_bUseListCount = file.GetIniInt("HarrisAPI", "UseListCount", 0)?true:false; // get all events up until the list count (otherwise just up to the lookahead)

			g_pdirect->m_settings.m_pszDSN = file.GetIniString("Database", "DSN", g_pdirect->m_settings.m_pszName?g_pdirect->m_settings.m_pszName:"Direct");
			g_pdirect->m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
			g_pdirect->m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
			g_pdirect->m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
			g_pdirect->m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
			g_pdirect->m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name

	//		g_pdirect->m_settings.m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels");  // the Channels table name
	//		g_pdirect->m_settings.m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Connections");  // the Connections table name
			g_pdirect->m_settings.m_pszLiveEvents = file.GetIniString("Database", "LiveEventsTableName", "Events");  // the LiveEvents table name
			g_pdirect->m_settings.m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue");  // the Queue table name
			g_pdirect->m_settings.m_pszMapping = file.GetIniString("Database", "RulesTableName", "Rules");  // the Rules table name

			g_pdirect->m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
			g_pdirect->m_settings.m_nTransferFailureRetryTime = file.GetIniInt("FileHandling", "TransferFailureRetryTime", 60);  // in seconds
			g_pdirect->m_settings.m_bAutoDeleteOldest = file.GetIniInt("FileHandling", "AutoDeleteOldest", 1)?true:false;
			g_pdirect->m_settings.m_ulDeletionThreshold = file.GetIniInt("FileHandling", "DeletionThreshold", 172800); // two days

			g_pdirect->m_settings.m_pszSystemFolderPath = file.GetIniString("FileHandling", "SystemFolder", "C:\\");  // must have trailing slash

			g_pdirect->m_settings.m_bPropagateChangedFiles = file.GetIniInt("FileHandling", "PropagateChangedFiles", 1)?true:false;


			g_pdirect->m_settings.m_pszImagestoreNullFile = file.GetIniString("Imagestore", "NullOXTFile", "null.oxt");  // name of the null file, must be in system folder
			g_pdirect->m_settings.m_pszIntuitionSearchExt = file.GetIniString("Imagestore", "IntuitionSearchExt", "tem"); // search extensions for Intuition
			g_pdirect->m_settings.m_pszImagestore2SearchExt = file.GetIniString("Imagestore", "Imagestore2SearchExt", "oxa,oxt,oxw");			// search extensions for IS2
			g_pdirect->m_settings.m_pszImagestore300SearchExt = file.GetIniString("Imagestore", "Imagestore300SearchExt", "oxa,oxt,oxe");		// search extensions for IS300
			g_pdirect->m_settings.m_pszImagestoreHDSearchExt = file.GetIniString("Imagestore", "ImagestoreHDSearchExt", "oxa,oxt,oxe");		// search extensions for ISHD

		}
		
		
	*/	
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
		EnterCriticalSection(&g_pdirect->m_data.m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = DIRECT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCategory="";
				CString szParameter="";
				CString szValue="";
				CString szTemp="";
				int min, max;
				bool bmin = false, bmax = false;
				try
				{
					prs->GetFieldValue("category", szCategory);  //HARDCODE
					prs->GetFieldValue("parameter", szParameter);  //HARDCODE
					prs->GetFieldValue("value", szValue);  //HARDCODE
					prs->GetFieldValue("min_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						min = atoi(szTemp);
						bmin = true;
					}
					prs->GetFieldValue("max_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						max = atoi(szTemp);
						bmax = true;
					}
				}
				catch( ... )
				{
				}

				int nLength = szValue.GetLength();
				if(szCategory.CompareNoCase("Main")==0)
				{
					if(szParameter.CompareNoCase("Name")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszName) free(m_pszName);
								m_pszName = pch;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("License")==0)
				{
					if(szParameter.CompareNoCase("Key")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLicense) free(m_pszLicense);
								m_pszLicense = pch;

								// recompile license key params
								if(g_pdirect->m_data.m_key.m_pszLicenseString) free(g_pdirect->m_data.m_key.m_pszLicenseString);
								g_pdirect->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
								if(g_pdirect->m_data.m_key.m_pszLicenseString)
								sprintf(g_pdirect->m_data.m_key.m_pszLicenseString, "%s", szValue);

								g_pdirect->m_data.m_key.InterpretKey();

								char errorstring[MAX_MESSAGE_LENGTH];
								if(g_pdirect->m_data.m_key.m_bValid)
								{
									unsigned long i=0;
									while(i<g_pdirect->m_data.m_key.m_ulNumParams)
									{
										if((g_pdirect->m_data.m_key.m_ppszParams)
											&&(g_pdirect->m_data.m_key.m_ppszValues)
											&&(g_pdirect->m_data.m_key.m_ppszParams[i])
											&&(g_pdirect->m_data.m_key.m_ppszValues[i]))
										{
											if(stricmp(g_pdirect->m_data.m_key.m_ppszParams[i], "max")==0)
											{
//												g_pdirect->m_data.m_nMaxLicensedDevices = atoi(g_pdirect->m_data.m_key.m_ppszValues[i]);
											}
										}
										i++;
									}
								
									if(
											(
												(!g_pdirect->m_data.m_key.m_bExpires)
											||((g_pdirect->m_data.m_key.m_bExpires)&&(!g_pdirect->m_data.m_key.m_bExpired))
											||((g_pdirect->m_data.m_key.m_bExpires)&&(g_pdirect->m_data.m_key.m_bExpireForgiveness)&&(g_pdirect->m_data.m_key.m_ulExpiryDate+g_pdirect->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
											)
										&&(
												(!g_pdirect->m_data.m_key.m_bMachineSpecific)
											||((g_pdirect->m_data.m_key.m_bMachineSpecific)&&(g_pdirect->m_data.m_key.m_bValidMAC))
											)
										)
									{
					// this overrides stuff so let's not let it do that.

										// let's just not override this in a green condition.  
										// comment it out.
//										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//										g_pdirect->m_data.SetStatusText(errorstring, DIRECT_STATUS_OK);
									}
									else
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
										g_pdirect->m_data.SetStatusText(errorstring, DIRECT_STATUS_ERROR);
									}
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
									g_pdirect->m_data.SetStatusText(errorstring, DIRECT_STATUS_ERROR);
								}

							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("CommandServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usCommandPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("StatusServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usStatusPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Messager")==0)
				{
					if(szParameter.CompareNoCase("UseEmail")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseEmail = true;
							else m_bUseEmail = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseNet")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseNetwork = true;
							else m_bUseNetwork = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseLog = true;
							else m_bUseLog = false;
						}
					}
					else
					if(szParameter.CompareNoCase("ReportSuccessfulOperation")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bReportSuccessfulOperation = true;
							else m_bReportSuccessfulOperation = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseXMLClientLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseXMLClientLog = true;
							else m_bUseXMLClientLog = false;
						}
					}
				}
				else
/*			
				if(szCategory.CompareNoCase("HarrisAPI")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("UseListCount")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseListCount = true;
							else m_bUseListCount = false;
						}
					}
				}
				else
	*/
				if(szCategory.CompareNoCase("FileHandling")==0)
				{
					if(szParameter.CompareNoCase("SystemFolder")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszSystemFolderPath) free(m_pszSystemFolderPath);
								m_pszSystemFolderPath = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("TempFolder")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszTempFolderPath) free(m_pszTempFolderPath);
								m_pszTempFolderPath = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("AutoDeleteOldest")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bAutoDeleteOldest = true;
							else m_bAutoDeleteOldest = false;
						}
					}
					else
					if(szParameter.CompareNoCase("ClearEventsOnStartup")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bClearEventsOnStartup = true;
							else m_bClearEventsOnStartup = false;
						}
					}
					else
					if(szParameter.CompareNoCase("PropagateChangedFiles")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bPropagateChangedFiles = true;
							else m_bPropagateChangedFiles = false;
						}
					}
					else
					if(szParameter.CompareNoCase("TransferFailureRetryTime")==0)
					{
						if(nLength>0)
						{
							m_nTransferFailureRetryTime = atoi(szValue);
						}
					}
					else
					if(szParameter.CompareNoCase("MaxTopRecheckElapsedTime")==0)
					{
						if(nLength>0)
						{
							m_nMaxTopRecheckElapsedTime = atoi(szValue);
						}
					}
					else
					if(szParameter.CompareNoCase("RenamePrefix")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszRenamePrefix) free(m_pszRenamePrefix);
								m_pszRenamePrefix = pch;
							}
						}
						else
						{
							if(m_pszRenamePrefix) free(m_pszRenamePrefix);
							m_pszRenamePrefix = NULL;
						}
					}
					else
					if(szParameter.CompareNoCase("RenameSuffix")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszRenameSuffix) free(m_pszRenameSuffix);
								m_pszRenameSuffix = pch;
							}
						}
						else
						{
							if(m_pszRenameSuffix) free(m_pszRenameSuffix);
							m_pszRenameSuffix = NULL;
						}

					}
					else
					if(szParameter.CompareNoCase("MaxAutomationBufferMS")==0)
					{
						if(nLength>0)
						{
							m_nMaxAutomationBufferMS = atoi(szValue);
						}
					}
					else
					if(szParameter.CompareNoCase("MaxAutomationForceMS")==0)
					{
						if(nLength>0)
						{
							m_nMaxAutomationForceMS = atoi(szValue);
						}
					}
					else
					if(szParameter.CompareNoCase("UseOnAirTimeForEndpointSchedule")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseOnAirTimeForEndpointSchedule = true;
							else m_bUseOnAirTimeForEndpointSchedule = false;
						}
					}
					else
					if(szParameter.CompareNoCase("AnalysisDwellMS")==0)
					{
						if(nLength>0)
						{
							m_nAnalysisDwellMS = atoi(szValue);
						}
					}
					else
					if(szParameter.CompareNoCase("IDPreprocessString")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszIDPreprocessString) free(m_pszIDPreprocessString);
								m_pszIDPreprocessString = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("TempFilePurgeInterval")==0)
					{
						if(nLength>0)
						{
							m_nTempFilePurgeInterval = atoi(szValue);
							if(m_nTempFilePurgeInterval<0) m_nTempFilePurgeInterval = 432000; //default.
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Imagestore")==0)
				{
					if(szParameter.CompareNoCase("NullOXTFile")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszImagestoreNullFile) free(m_pszImagestoreNullFile);
								m_pszImagestoreNullFile = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("IntuitionSearchExt")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszIntuitionSearchExt) free(m_pszIntuitionSearchExt);
								m_pszIntuitionSearchExt = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("Imagestore2SearchExt")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszImagestore2SearchExt) free(m_pszImagestore2SearchExt);
								m_pszImagestore2SearchExt = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("Imagestore300SearchExt")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszImagestore300SearchExt) free(m_pszImagestore300SearchExt);
								m_pszImagestore300SearchExt = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("Imagestore300HDSearchExt")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszImagestore300HDSearchExt) free(m_pszImagestore300HDSearchExt);
								m_pszImagestore300HDSearchExt = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ImagestoreHDSearchExt")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszImagestoreHDSearchExt) free(m_pszImagestoreHDSearchExt);
								m_pszImagestoreHDSearchExt = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("IntuitionRenameExt")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszIntuitionRenameExt) free(m_pszIntuitionRenameExt);
								m_pszIntuitionRenameExt = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ImagestoreRenameExt")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszImagestoreRenameExt) free(m_pszImagestoreRenameExt);
								m_pszImagestoreRenameExt = pch;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Database")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("SettingsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszSettings) free(m_pszSettings);
								m_pszSettings = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ExchangeTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszExchange) free(m_pszExchange);
								m_pszExchange = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("MessagesTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszMessages) free(m_pszMessages);
								m_pszMessages = pch;
							}
						}
					}
/*
					else
					if(szParameter.CompareNoCase("ChannelsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszChannels) free(m_pszChannels);
								m_pszChannels = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ConnectionsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszConnections) free(m_pszConnections);
								m_pszConnections = pch;
							}
						}
					}
*/
					else
					if(szParameter.CompareNoCase("LiveEventsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLiveEvents) free(m_pszLiveEvents);
								m_pszLiveEvents = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ModificationCheckInterval")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_ulModsIntervalMS = nLength;
						}
					}
				}
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
					if(szParameter.CompareNoCase("Messages")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeMessageDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
				}
/*
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
					if(szParameter.CompareNoCase("AsRun_Log")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeAsRunDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
				}
				
*/

				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			if(pszInfo)
			{
				_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
			}
			delete prs;
			prs = NULL;
			LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);

			Settings(false); //write
/*
			if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
			{
				// these explicts arent necessary - uncomment to write out a full file to edit...
				file.SetIniString("Main", "Name", g_pdirect->m_settings.m_pszName);
				file.SetIniString("License", "Key", g_pdirect->m_settings.m_pszLicense);

				file.SetIniString("FileServer", "IconPath", g_pdirect->m_settings.m_pszIconPath);
				file.SetIniInt("CommandServer", "ListenPort", g_pdirect->m_settings.m_usCommandPort);
				file.SetIniInt("StatusServer", "ListenPort", g_pdirect->m_settings.m_usStatusPort);

				file.SetIniInt("Messager", "UseEmail", g_pdirect->m_settings.m_bUseEmail?1:0);
				file.SetIniInt("Messager", "UseNet", g_pdirect->m_settings.m_bUseNetwork?1:0);
				file.SetIniInt("Messager", "UseLog", g_pdirect->m_settings.m_bUseLog?1:0);
				file.SetIniInt("Messager", "ReportSuccessfulOperation", g_pdirect->m_settings.m_bReportSuccessfulOperation?1:0);
				file.SetIniInt("Messager", "LogTransfers", g_pdirect->m_settings.m_bLogTransfers?1:0);

				file.SetIniString("Database", "DSN", g_pdirect->m_settings.m_pszDSN);
				file.SetIniString("Database", "DBUser", g_pdirect->m_settings.m_pszUser);
				file.SetIniString("Database", "DBPassword", g_pdirect->m_settings.m_pszPW);
				file.SetIniString("Database", "SettingsTableName", g_pdirect->m_settings.m_pszSettings);  // the Settings table name
				file.SetIniString("Database", "ExchangeTableName", g_pdirect->m_settings.m_pszExchange);  // the Exchange table name
				file.SetIniString("Database", "MessagesTableName", g_pdirect->m_settings.m_pszMessages);  // the Messages table name

		//		file.SetIniInt("HarrisAPI", "UseListCount", g_pdirect->m_settings.m_bUseListCount?1:0); // get all events up until the list count (otherwise just up to the lookahead)

		//		file.SetIniString("Database", "ChannelsTableName", g_pdirect->m_settings.m_pszChannels);  // the Channels table name
		//		file.SetIniString("Database", "ConnectionsTableName", g_pdirect->m_settings.m_pszConnections);  // the Connections table name
				file.SetIniString("Database", "LiveEventsTableName", g_pdirect->m_settings.m_pszLiveEvents);  // the LiveEvents table name
				file.SetIniString("Database", "QueueTableName", g_pdirect->m_settings.m_pszQueue);  // the Queue table name
				file.SetIniString("Database", "RulesTableName", g_pdirect->m_settings.m_pszMapping);  // the Rules table name

				file.SetIniInt("Database", "ModificationCheckInterval", g_pdirect->m_settings.m_ulModsIntervalMS);  // in milliseconds
				file.SetIniInt("FileHandling", "TransferFailureRetryTime", g_pdirect->m_settings.m_nTransferFailureRetryTime);  // in seconds
				file.SetIniInt("FileHandling", "AutoDeleteOldest", g_pdirect->m_settings.m_bAutoDeleteOldest?1:0);
				file.SetIniInt("FileHandling", "DeletionThreshold", g_pdirect->m_settings.m_ulDeletionThreshold ); // two days
				file.SetIniInt("FileHandling", "PropagateChangedFiles", g_pdirect->m_settings.m_bPropagateChangedFiles?1:0);

				file.SetIniString("FileHandling", "SystemFolder", g_pdirect->m_settings.m_pszSystemFolderPath);  // must have trailing slash
				file.SetIniString("Imagestore", "NullOXTFile", g_pdirect->m_settings.m_pszImagestoreNullFile );  // name of the null file, must be in system folder
				file.SetIniString("Imagestore", "IntuitionSearchExt", g_pdirect->m_settings.m_pszIntuitionSearchExt); // search extensions for Intuition
				file.SetIniString("Imagestore", "Imagestore2SearchExt", g_pdirect->m_settings.m_pszImagestore2SearchExt);			// search extensions for IS2
				file.SetIniString("Imagestore", "Imagestore300SearchExt", g_pdirect->m_settings.m_pszImagestore300SearchExt);		// search extensions for IS300
				file.SetIniString("Imagestore", "ImagestoreHDSearchExt", g_pdirect->m_settings.m_pszImagestoreHDSearchExt);		// search extensions for ISHD

				file.SetSettings(DIRECT_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

			}
*/


			return DIRECT_SUCCESS;
		}
		LeaveCriticalSection(&g_pdirect->m_data.m_critSQL);
	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s%s%s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. ",
				m_pdb?"":"Database pointer was NULL. ",
				m_pdbConn?"":"Connection pointer was NULL. ",
				((m_pdbConn)&&(!m_pdbConn->m_bConnected))?"Database not connected. ":""
				);
		}
	}

	return DIRECT_ERROR;
}

char* CDirectSettings::ProcessString(char* pszString, bool bFreeIncomingString)
{
	if(pszString)
	{
		char pszOutput[4096];
		int nInLen=strlen(pszString);
		int nOutLen=0;
		char* pch = pszString;
		char* pchEnd = min((pch+4096),(pszString+nInLen));
		while((*pch!=0)&&(pch<pchEnd))
		{
			if(*pch == '%')
			{
				// we are in a specifier.
				// here is a list of ours:
				// %h  local hostname
		
				pch++;
				if((*pch!=0)&&(pch<pchEnd))
				{
					if(*pch == 'h')
					{
						if((g_pdirect->m_data.m_pszHost)&&(strlen(g_pdirect->m_data.m_pszHost)))
						{
							char* pszTemp = (char*)malloc(strlen(g_pdirect->m_data.m_pszHost)+1);
							if(pszTemp)
							{
								strcpy(pszTemp, g_pdirect->m_data.m_pszHost);
							
								char* pchPlus = pszTemp;
								while((*pchPlus != 0)&&(nOutLen<4096))
								{
									pszOutput[nOutLen]= *pchPlus;
									nOutLen++;
									pchPlus++;
								}
								free(pszTemp);
							}
						}
						pch++;
					}
					else
					{
						// some other specifier, can be anything.
						pszOutput[nOutLen]= *(pch-1); // do add the %, this may get further processed
						nOutLen++;
						pszOutput[nOutLen]= *pch;
						nOutLen++;
						pch++;
					}
				}
				else
				{
					pszOutput[nOutLen]= *(pch-1);
					nOutLen++;
				}
				
			}
			else
			{
				pszOutput[nOutLen]= *pch;
				nOutLen++;
				pch++;
			}
		}

		pszOutput[nOutLen]=0;

		pch = (char*)malloc(strlen(pszOutput)+1);
		if(pch)
		{
			strcpy(pch,pszOutput);
			if(bFreeIncomingString) 
			{
				try {free(pszString);} catch(...){}
			}
		}
		return pch;
	}
	return NULL;
}


