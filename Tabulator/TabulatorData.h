// TabulatorData.h: interface for the CTabulatorData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TABULATORDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
#define AFX_TABULATORDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <winsock2.h>
#include <sys/timeb.h>
#include "../../Common/TXT/BufferUtil.h" 
#include "../../Common/LAN/NetUtil.h" 
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"
#include "../../Common/KEY/LicenseKey.h"
//#include "../../Common/FILE/DirUtil.h"


// these are data reference objects, pulled from the database.
// actual connection, list, and event objects are in the CADC object
/*

class CTabulatorConnectionObject  
{
public:
	CTabulatorConnectionObject();
	virtual ~CTabulatorConnectionObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	char* m_pszServerName;
	char* m_pszClientName;
	char* m_pszDesc;

//	CAConnection* m_pAPIConn;  // pointer to the associated API connection.

	// control
	bool m_bKillConnThread;
	bool m_bConnThreadStarted;
};

*/
class CTabulatorDownloadObject  
{
public:
	CTabulatorDownloadObject();
	virtual ~CTabulatorDownloadObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	int m_nID;  // the unique plugin ID within Tabulator setup. (assigned externally)

	char* m_pszName;
	char* m_pszDesc;
	char* m_pszModule;
	HINSTANCE				m_hinstDLL; // DLL load...
	LPFNDLLCTRL			m_lpfnDllCtrl; // pointer to function
	DLLdata_t*			m_pDLLdata;

//	CAConnection* m_pAPIConn;  // pointer to the associated API connection.

	int m_nSettingsMod;
	int m_nLastSettingsMod;

// control
//	bool* m_pbKillConnThread;
//	bool m_bKillDownloadThread;
//	bool m_bDownloadThreadStarted;
//	_timeb m_timebDownloadTick; // the last time check inside the thread
};


// same as CNucleusEvent definition, currently.

class CTabulatorEvent  
{
public:
	CTabulatorEvent();
	virtual ~CTabulatorEvent();

	unsigned long m_ulFlags;
	double m_dblTriggerTime; 
	bool m_bAnalyzed;
	bool m_bReAnalyzed;
	int  m_nAnalyzedTriggerID;
	int  m_nEventID;
	int  m_nDestType;
	
	int m_nType;
	CString m_szName;
	CString m_szHost;
	CString m_szSource;
	CString m_szScene;
	CString m_szIdentifier;
	CString m_szValue;
	CString m_szParamDependencies;
	CString m_szDestModule;
	CString m_szLayer;
};



class CTabulatorEventArray  
{
public:
	CTabulatorEventArray();
	virtual ~CTabulatorEventArray();

	void	DeleteEvents();

	int m_nEvents;
	CTabulatorEvent** m_ppEvents;
	CRITICAL_SECTION m_critEvents;
	int m_nEventMod;
	int m_nLastEventMod;

};





class CTabulatorData  
{
public:
	CTabulatorData();
	virtual ~CTabulatorData();

	// util object
	CBufferUtil m_bu;

  // hostname.  have to get it, store it here
	char*	m_pszHost;	// the name of the host
	char*	m_pszCompleteHost;	// the complete name of the host

	char*	m_pszCortexHost;	// the name of the cortex host
	unsigned short m_usCortexCommandPort;
	unsigned short m_usCortexStatusPort;

	bool m_bNetworkMessagingInitialized;
	bool m_bCheckModsWarningSent;
	bool m_bCheckMsgsWarningSent;
	bool m_bCheckAsRunWarningSent;

	_timeb m_timebAutoPurge; // the last time autopurge was run
/*
	CTabulatorConnectionObject** m_ppConnObj;
	int m_nNumConnectionObjects;
*/
	CTabulatorDownloadObject** m_ppDownloadObj;
	int m_nNumDownloadObjects;

	CTabulatorEventArray m_eventarray;

	_timeb m_timebTick; // the last time check inside the thread
	_timeb m_timebLastStatus; // the time of the last status given to cortex
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for cortex global status (each obj has their own as well)

	int m_nSettingsMod;
	int m_nDownloadMod;
//	int m_nConnectionsMod;
	int m_nLastSettingsMod;
	int m_nLastDownloadMod;
//	int m_nLastConnectionsMod;

	int m_nQueueMod;
	int m_nLastQueueMod;

	bool m_bProcessSuspended;
//	bool m_bAutomationThreadStarted;
//	double m_dblLastAutomationChange;  //global change
//	_timeb m_timebAutomationTick;

	char* GetStatusText(unsigned long* pulStatus);  // allocates mem, must free after use
	int		SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError = false);
	int		GetHost();

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;
	CDBUtil* m_pdb2;
	CDBconn* m_pdb2Conn;

	int CheckDatabaseMods(char* pszInfo=NULL);
	int CheckMessages(char* pszInfo=NULL);
	int CheckAsRun(char* pszInfo=NULL);
	int IncrementDatabaseMods(char* pszTableName, char* pszInfo=NULL);
//	int GetConnections(char* pszInfo=NULL);
	int GetDownloaders(char* pszInfo=NULL);
	int FindDownloader(char* pszName, int nStartIndex=-1);
	int m_nLastDownloader;

	int GetEvents(char* pszInfo=NULL);

	int GetQueue(char* pszInfo=NULL);

	CLicenseKey m_key;

	bool m_bQuietKill;
	CRITICAL_SECTION m_critSQL;

private:
	CRITICAL_SECTION m_critText;
	CRITICAL_SECTION m_critDownloader;

	char* m_pszStatus;	// parseable string

//	unsigned long m_ulReqCount;
//	unsigned long m_ulSvcCount;
};

#endif // !defined(AFX_TABULATORDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
