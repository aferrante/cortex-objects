// TabulatorDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(TABULATORDEFINES_H_INCLUDED)
#define TABULATORDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define TABULATOR_CURRENT_VERSION		"1.0.1.13"

// downloader module DLL format
#include "..\..\Applications\Generic\Client\ClientDefines.h"
#include <sys/timeb.h>


typedef struct
{
	bool bKillThread;
	bool bThreadStarted;
	int  nThreadControl;  //generic
	int  nThreadState;  //generic
	char pszThreadStateText[256];

	char*	pszThreadName;

	// URLs may use format specifiers
	char*	pszBaseURL;
	char*	pszInfoURL;
	char*	pszConfirmURL;

	char*	pszSeriesParamName;    //  %S
	char*	pszProgramParamName;   //  %P
	char*	pszEpisodeParamName;   //  %E

	char*	pszSeriesID;    //  %s
	char*	pszProgramID;   //  %p
	char*	pszEpisodeID;   //  %e

	// some generic parameters
	char*	pszToDateParamName;				//  %t
	char*	pszFromDateParamName;			//  %f
	char*	pszNowParamName;					//  %n
	char*	pszBatchParamName;				//  %b
	char*	pszFromIDParamName;				//  %m  // start id
	char*	pszGetIDParamName;				//  %i

	char*	pszLastID;  // last ID gotten   %l

	int nBatchParam;
	int nShortIntervalMS;
	int nLongIntervalMS;
	bool bDirParamStyle;  // true is baseurl/paramname/paramvalue/  false is baseurl?paramname=paramvalue

	_timeb timebTick; // the time check inside the thread
	_timeb timebLastPullTick; // the last time data pulled inside the thread
	int nIDrateAvg;
	int nIDrateMax;
	int nIDrateMin;
	int nNumFailures;
	int nNumSuccess;
	double m_dblLastUpdate;
	CRITICAL_SECTION m_crit;

	char*	pszHost;
	int nPort;
	int nAuxValue;
} DLLthread_t;


typedef struct
{
	DLLthread_t** thread;
	int nNumThreads;
	CRITICAL_SECTION m_crit;
	CDialog*				pDlg; // if there is a dialog....
} DLLdata_t;


// modes
#define TABULATOR_MODE_DEFAULT			0x00000000  // exclusive
#define TABULATOR_MODE_LISTENER			0x00000001  // exclusive
#define TABULATOR_MODE_CLONE				0x00000002  // exclusive
#define TABULATOR_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define TABULATOR_MODE_VOLATILE			0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define TABULATOR_MODE_MASK					0x0000000f  // 

// default port values.
//#define TABULATOR_PORT_FILE				80		
#define TABULATOR_PORT_CMD					10688		
#define TABULATOR_PORT_STATUS				10689		

#define TABULATOR_PORT_INVALID			0	

#define TABULATOR_FLAG_DISABLED			0x0000	 // default
#define TABULATOR_FLAG_ENABLED			0x0001	
#define TABULATOR_FLAG_FOUND				0x1000

#define TABULATOR_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded


#define TABULATOR_FILEFLAG_RECORDONLY			0x00000000
#define TABULATOR_FILEFLAG_FILEXFER				0x00000001
#define TABULATOR_FILEFLAG_FILECOPIED			0x00000002
#define TABULATOR_FILEFLAG_ARCHIVED				0x00000004
#define TABULATOR_FILEFLAG_DELETED				0x00000008


// status
#define TABULATOR_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define TABULATOR_STATUS_UNKNOWN						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define TABULATOR_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define TABULATOR_STATUS_ERROR							0x00000020  // error (red icon)
#define TABULATOR_STATUS_CONN								0x00000030  // ready (green icon)	
#define TABULATOR_STATUS_OK									0x00000030  // ready (green icon)	
#define TABULATOR_STATUS_RUN								0x00000040  // in progress, running, owned etc (blue icon);	
#define TABULATOR_ICON_MASK									0x00000070  // mask	

#define TABULATOR_STATUS_SUSPEND						0x00000080  // suspended	(yellow icon please)

#define TABULATOR_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define TABULATOR_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define TABULATOR_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define TABULATOR_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define TABULATOR_STATUS_CMDSVR_MASK				0x00007000  // command server mask bits

#define TABULATOR_STATUS_STATUSSVR_START		0x00010000  // starting the status server
#define TABULATOR_STATUS_STATUSSVR_RUN			0x00020000  // status server running
#define TABULATOR_STATUS_STATUSSVR_END			0x00030000  // status server shutting down
#define TABULATOR_STATUS_STATUSSVR_ERROR		0x00040000  // status server error
#define TABULATOR_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define TABULATOR_STATUS_THREAD_START				0x00100000  // starting the main thread
#define TABULATOR_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define TABULATOR_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define TABULATOR_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define TABULATOR_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define TABULATOR_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define TABULATOR_STATUS_THREAD_MASK				0x00f00000  // main thread mask bits

// various failures...
#define TABULATOR_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define TABULATOR_STATUS_FAIL_DB						0x20000000  // could not get DB
#define TABULATOR_STATUS_FAIL_MASK					0xf0000000  // failure code mask bits

//return values
#define TABULATOR_SUCCESS   0
#define TABULATOR_ERROR	   -1
#define TABULATOR_FILE_EXISTS  -2


// commands
#define TABULATOR_CMD_HELLO				   0xa0
#define TABULATOR_CMD_PLUGINCALL	   0xb0


#define TABULATOR_CMD_QUEUE				0xc0 // puts thing in queue
#define TABULATOR_CMD_CMDQUEUE		0xc1 // puts thing in command queue
#define TABULATOR_CMD_DIRECT			0xc2 // skips queues, commands DLL directly

#define TABULATOR_CMD_EXSET				0xea // increments exchange counter
#define TABULATOR_CMD_EXGET				0xeb // gets exchange mod value
#define TABULATOR_CMD_MODSET			0xec // gets exchange mod value

#define TABULATOR_CMD_GETSTATUS		0x99 // gets status info




// default filenames
#define TABULATOR_SETTINGS_FILE_SETTINGS	  "tabulator.csr"		// csr = cortex settings redirect
#define TABULATOR_SETTINGS_FILE_DEFAULT	  "tabulator.csf"		// csf = cortex settings file

// debug defines
#define TABULATOR_DEBUG_TIMING					0x00000001
#define TABULATOR_DEBUG_EXCHANGE				0x00000002
#define TABULATOR_DEBUG_CONNECT					0x00000004
#define TABULATOR_DEBUG_CONNECTSTATUS		0x00000008
#define TABULATOR_DEBUG_DATAFEEDS				0x00000010
#define TABULATOR_DEBUG_EVENTS					0x00000020
#define TABULATOR_DEBUG_COMM						0x00000200


//destination types (decimal)
#define TABULATOR_DESTTYPE_UNKNOWN							0000  // type unknown

#define TABULATOR_DESTTYPE_ORAD_DVG						1000  // Orad DVG
#define TABULATOR_DESTTYPE_ORAD_HDVG						1001  // Orad HDVG

#define TABULATOR_DESTTYPE_MIRANDA_IS2					2001  // Imagestore 2
#define TABULATOR_DESTTYPE_MIRANDA_INT					2002  // Intuition
#define TABULATOR_DESTTYPE_MIRANDA_IS300				2003  // Imagestore 300
#define TABULATOR_DESTTYPE_MIRANDA_ISHD				2004  // Imagestore HD

#define TABULATOR_DESTTYPE_CHYRON_LEX					3000	// Duet LEX
#define TABULATOR_DESTTYPE_CHYRON_HYPERX				3001  // Duet HyperX
#define TABULATOR_DESTTYPE_CHYRON_MICROX				3002  // Duet MicroX
#define TABULATOR_DESTTYPE_CHYRON_CHANNELBOX		3003  // Channel Box
#define TABULATOR_DESTTYPE_CHYRON_CALBOX				3004  // CAL Box

#define TABULATOR_DESTTYPE_EVERTZ_9625LG				4000 
#define TABULATOR_DESTTYPE_EVERTZ_9625LGA			4001
#define TABULATOR_DESTTYPE_EVERTZ_9725LGA			4002
#define TABULATOR_DESTTYPE_EVERTZ_9725LG				4003
#define TABULATOR_DESTTYPE_EVERTZ_HD9625LG			4004
#define TABULATOR_DESTTYPE_EVERTZ_HD9625LGA		4005
#define TABULATOR_DESTTYPE_EVERTZ_HD9725LG			4006
#define TABULATOR_DESTTYPE_EVERTZ_HD9725LGA		4007

#define TABULATOR_DESTTYPE_HARRIS_ICONII				5000


#define TABULATOR_HARRIS_ICONII_LSFORMAT_NUM4				0 // 4 digit numerical LayoutSalvoFormat
#define TABULATOR_HARRIS_ICONII_LSFORMAT_NUM8				1 // 8 digit numerical LayoutSalvoFormat
#define TABULATOR_HARRIS_ICONII_LSFORMAT_ALPHA			2 // alphanumerical string LayoutSalvoFormat




#endif // !defined(TABULATORDEFINES_H_INCLUDED)
