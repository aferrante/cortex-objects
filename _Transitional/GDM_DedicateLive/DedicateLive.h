// DedicateLive.h : main header file for the DEDICATELIVE DLL
//

#if !defined(AFX_DEDICATELIVE_H__5DB2AA9D_54E6_4CD1_A073_29B25FE34BB4__INCLUDED_)
#define AFX_DEDICATELIVE_H__5DB2AA9D_54E6_4CD1_A073_29B25FE34BB4__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "../../ISMdefines.h"		

// These are damned useful:
#ifndef max
#define max(a,b)            ((a > b) ? a : b)
#endif

#ifndef min
#define min(a,b)            ((a < b) ? a : b)
#endif                    


#define READ TRUE
#define WRITE FALSE


#include "resource.h"		// main symbols
#include "../../../Shared/imaging/bmp helpers/bmp.h" // needed for about box (at minimum).

// various includes might be helpful.
#include "../../../Shared/MessageDispatcher/MessageDispatcher.h"		// main symbols
#include "../../../Shared/ListCtrlEx/ListCtrlEx.h"
//#include "../../../Shared/EditEx/EditEx.h"
#include "../../../Shared/TextUtils/TextUtils.h"
//#include "../../../Shared/networking/networking.h"
//#include "../../../Shared/SmutFilter/smutfilter.h"
//#include "../../../Shared/VDIfile/VDIfile.h"
#include "../../../Shared/Duet/Duet.h"
#include "../../Devices/ISM_DatabaseT2/TextToAirDB2.h" // for data types
#include "../../Playout/ISM_TextToAir/TextToAirDefines.h" // for CMD defs


#include "DedicateLiveDefines.h"
#include "DedicateLiveDlg.h"
#include "DedicateLiveSettings.h"
#include "DedicateLiveData.h"
#include "DedicateLiveThreads.h"
#include "DedicateLiveCore.h"

/////////////////////////////////////////////////////////////////////////////
// CDedicateLiveApp
// See DedicateLive.cpp for the implementation of this class
//

class CDedicateLiveApp : public CWinApp
{
public:
	CDedicateLiveApp();

	int ShowSettings(void** ppSettings, UINT nType);
	int InitShow();
	int ExecuteCommand(char pszDataBuffer[], UINT nSocket);
	int EndShow();
	int SuspendShow(BOOL bSuspend);
	int AbortShow();
	int GetShowState();
	int SetDevice(int nIndex);
	int GetShowStatus(void* pszStatusText, void* pszToolTipText, int* pnLEDState);
// not exported
	void ServerCommandHandler();
	void SetShowState(UINT nState);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDedicateLiveApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CDedicateLiveApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEDICATELIVE_H__5DB2AA9D_54E6_4CD1_A073_29B25FE34BB4__INCLUDED_)
