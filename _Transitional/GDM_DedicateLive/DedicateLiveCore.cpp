// DedicateLiveCore.cpp: implementation of the CDedicateLiveCore class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DedicateLive.h"
#include "DedicateLiveCore.h"
#include <sys/timeb.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//  necessary global externs
extern CDedicateLiveSettings g_settings;
extern CDedicateLiveData g_data;
extern CDedicateLiveApp theApp;
extern CDedicateLiveCore g_core;  // engine class
extern CDedicateLiveThreadHelpers g_threadHelper;
extern CMessageDispatcher g_md;
extern ISMShowData_t g_showdata;
extern CNetworking g_net;
extern BOOL g_bUseDuet;

extern CDuetAPI *g_pduet;
extern void __cdecl OnUserEvent(long connId, long eventId, long data);
extern CString getRelativePath();


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDedicateLiveCore::CDedicateLiveCore()
{

}

CDedicateLiveCore::~CDedicateLiveCore()
{

}

CString CDedicateLiveCore::FixLocation(CString szLocation)
{
	if (szLocation.Left(1) == ",")
	{ szLocation = szLocation.Mid(1); szLocation.TrimLeft(); }
	if (szLocation.Right(1) == ",")
	{ szLocation = szLocation.Left(szLocation.GetLength() - 1); szLocation.TrimRight();}
	return szLocation;
}

//returns crawl text for dedication text (for both dedications and replies)
CString  CDedicateLiveCore::BuildCrawl(CString szDelimitedData)
{
	CString szCrawl = _T("");
	CTextUtils tu;
	CString szFields[DL_MAXFIELDS];
	int nItems = tu.breakStringOnSepChar('|', szDelimitedData, szFields, DL_MAXFIELDS);

	int nType = tu.ReturnHexByte(szFields[0]);
	for (int i=DFIELD_SENDER_NAME+2; i<RFIELD_REPLY+2; i++)
	{
		szFields[i].TrimLeft();
		szFields[i].TrimRight();
	}
	switch(nType)
	{
	case DB_TYPE_TEXT://          0x04 // messages
		{
			if (atoi(szFields[1]) == TYPE_DEDICATION)
			{
				CString szFormat=g_settings.m_szDedicationFormatString;
				for (i=0; i<szFormat.GetLength(); i++)
				{
					if (szFormat[i] == '%')
					{
						i++; if (i>=szFormat.GetLength()) break;
						if (szFormat[i] == '%') 
							szCrawl += "%";
						else if (szFormat[i] == 'N') // dedicatee name
							szCrawl += szFields[DFIELD_RECVR_NAME+2];
						else if (szFormat[i] == 'L') // dedicatee location
							szCrawl += FixLocation(szFields[DFIELD_RECVR_LOC+2]);
						else if (szFormat[i] == 'n') // dedicator name
							szCrawl += szFields[DFIELD_SENDER_NAME+2];
						else if (szFormat[i] == 'l') // dedicator location
							szCrawl += FixLocation(szFields[DFIELD_SENDER_LOC+2]);
						else if (szFormat[i] == 'D') // dedication text
							szCrawl += szFields[DFIELD_DEDICATION+2];
						else szCrawl += (char) szFormat[i];
					}
					else szCrawl += (char) szFormat[i];
				}
			}
			else if (atoi(szFields[1]) == TYPE_REPLY)
			{
				CString szFormat=g_settings.m_szReplyDedFormatString;
				for (i=0; i<szFormat.GetLength(); i++)
				{
					if (szFormat[i] == '%')
					{
						i++; if (i>=szFormat.GetLength()) break;
						if (szFormat[i] == '%') 
							szCrawl += "%";
						else if (szFormat[i] == 'N') // dedicatee name
							szCrawl += szFields[RFIELD_RECVR_NAME+2];
						else if (szFormat[i] == 'L') // dedicatee location
							szCrawl += FixLocation(szFields[RFIELD_RECVR_LOC+2]);
						else if (szFormat[i] == 'n') // dedicator name
							szCrawl += szFields[RFIELD_SENDER_NAME+2];
						else if (szFormat[i] == 'l') // dedicator location
							szCrawl += FixLocation(szFields[RFIELD_SENDER_LOC+2]);
						else if (szFormat[i] == 'D') // dedication text
							szCrawl += szFields[RFIELD_DEDICATION+2];
						else if (szFormat[i] == 'D') // reply text
							szCrawl += szFields[RFIELD_REPLY+2];
						else szCrawl += (char) szFormat[i];
					}
					else szCrawl += (char) szFormat[i];
				}
			}
		} break;
	}

	return szCrawl;
}

//returns crawl text for reply text (for replies only)
CString  CDedicateLiveCore::BuildReplyCrawl(CString szDelimitedData)
{
	CString szCrawl = _T("");
	CTextUtils tu;
	CString szFields[DL_MAXFIELDS];
	int nItems = tu.breakStringOnSepChar('|', szDelimitedData, szFields, DL_MAXFIELDS);

	int nType = tu.ReturnHexByte(szFields[0]);
	for (int i=DFIELD_SENDER_NAME+2; i<RFIELD_REPLY+2; i++)
	{
		szFields[i].TrimLeft();
		szFields[i].TrimRight();
	}
	switch(nType)
	{
	case DB_TYPE_TEXT://          0x04 // messages
		{
			if (atoi(szFields[1]) == TYPE_REPLY)
			{
				CString szFormat=g_settings.m_szReplyFormatString;
				for (int i=0; i<szFormat.GetLength(); i++)
				{
					if (szFormat[i] == '%')
					{
						i++; if (i>=szFormat.GetLength()) break;
						if (szFormat[i] == '%') 
							szCrawl += "%";
						else if (szFormat[i] == 'N') // dedicatee name
							szCrawl += szFields[RFIELD_RECVR_NAME+2];
						else if (szFormat[i] == 'L') // dedicatee location
							szCrawl += FixLocation(szFields[RFIELD_RECVR_LOC+2]);
						else if (szFormat[i] == 'n') // dedicator name
							szCrawl += szFields[RFIELD_SENDER_NAME+2];
						else if (szFormat[i] == 'l') // dedicator location
							szCrawl += FixLocation(szFields[RFIELD_SENDER_LOC+2]);
						else if (szFormat[i] == 'D') // dedication text
							szCrawl += szFields[RFIELD_DEDICATION+2];
						else if (szFormat[i] == 'R') // reply text
							szCrawl += szFields[RFIELD_REPLY+2];
						else szCrawl += (char) szFormat[i];
					}
					else szCrawl += (char) szFormat[i];
				}
			}
		} break;
	}

	return szCrawl;
}

BOOL CDedicateLiveCore::SendReadyPulse()
{
//  g_md.DM(0,0,0,"Attempting ready pulse with settings IP: %s:%d", g_data.m_szReadyIP, g_settings.m_nReadyPort);
  BYTE ReplyCmd; CString szReply;
	CTextUtils tu;
  int rv=-1;
	g_data.m_bInEndAnim = FALSE;
	g_data.m_bWorking = FALSE;
	g_data.m_nEventPlaying = -1;
  if 
//		((g_settings.m_szReadyIP == "") || (g_settings.m_szReadyIP == "0.0.0.0") || 
		((!tu.IsValidDottedQuad(g_settings.m_szReadyIP, FALSE))||
    (g_settings.m_nReadyPort <= 0))
    return FALSE;
  g_md.DM(0,0,0,"Sending ready pulse to IP: %s:%d|SendReadyPulse", g_settings.m_szReadyIP, g_settings.m_nReadyPort);

	int nAttempt=0;
	while((rv<0)&&(nAttempt<3))
	{
		rv = g_net.ServerSendCmd(CMD_READYPULSE, g_settings.m_szReadyIP, 
			g_settings.m_nReadyPort, &ReplyCmd, &szReply);
		g_md.DM(0,0,0,"Ready pulse sent (function returned %d, ReplyCmd was %02x - FYI: ACK is 0x0a, NAK is 0x0b)|SendReadyPulse", (int) rv, ReplyCmd);
		if(rv<0) Sleep(30);
		nAttempt++;
	}
  return (rv>=0);
}
// CAL functions
void CDedicateLiveCore::Setup()
{
	CTextUtils tu;
	CALRegisterUserEventHandler(OnUserEvent, 0);

	CheckGDM(CALSetOptions(0, "PREFERENCES\\MAXWAIT=60"), __LINE__);
//AfxMessageBox("debug01"); 

	int i=0;

	for(i=0; i<DL_MAXFONTS; i++)
	{
		// load fonts etc
		CheckGDM(CALLoadFontName(
			(LPCTSTR) g_settings.m_szFont[i], 
			g_settings.m_nFontSize[i], 
			g_settings.m_nFontStyle[i], 
			&g_data.m_hcalFont[i]), __LINE__);
	}
//AfxMessageBox("debug02"); 
		// load icon fonts etc
		CheckGDM(CALLoadFontName(
			(LPCTSTR) g_settings.m_szIconFont, 
			g_settings.m_nIconFontSize, 
			ecalNormal, 
			&g_data.m_hcalIconFont), __LINE__);
//AfxMessageBox("debug03"); 


	for(i=0; i<DL_MAXCOLORS; i++)
	{
		int r,g,b,a;
		r=(int)tu.ReturnHexByte(g_settings.m_szColors[i], 0);
		g=(int)tu.ReturnHexByte(g_settings.m_szColors[i], 2);
		b=(int)tu.ReturnHexByte(g_settings.m_szColors[i], 4);
		a=(int)tu.ReturnHexByte(g_settings.m_szColors[i], 6);
		
//		tu.AMB("CALLoadPalette %d, %s, %x,%x,%x,%x", i, g_settings.m_szColors[i],r,g,b,a);

		CheckGDM(CALLoadPalette(1, CALRGBA(r,g,b,a), &(g_data.m_hcalColor[i])), __LINE__);
	}
//AfxMessageBox("debug04"); 


	for(i=0; i<DL_MAXFONTS; i++)
	{
		// edge effects
		CString szFields[DL_MAXFIELDS];
		int nItems = tu.breakStringOnSepChar('|', g_settings.m_szFontEdge[i], szFields, DL_MAXFIELDS);

		if((nItems>5)&&(atoi(szFields[0])>0)) // fields zero is true false for edge effects
		{
//tu.AMB("%d CALAddEdge (%s)",i,g_settings.m_szFontEdge[i]);

			long priority, softness;
			ecalEdgeType type;
			Real32 angle, size;

			int iColor =atol(szFields[1]); // color index
			if((iColor>=DL_MAXCOLORS)||(iColor<0)) iColor=0;
			priority = 0; // priority is always 0
			type = (ecalEdgeType)atol(szFields[2]);
			angle = (Real32)atof(szFields[3]);
			size  = (Real32)atof(szFields[4]);
			softness = atol(szFields[5]);

//			tu.AMB("%d CALAddEdge (%s):\n\
//g_data.m_hcalFont[i]:%d, priority:%d, g_data.m_hcalColor[iColor]:%d, type:%d, angle:%f, size:%f, softness:%d", 
//				i,(g_settings.m_szFontEdge[i]),
//				(g_data.m_hcalFont[i]), priority, (g_data.m_hcalColor[iColor]), type, angle, size, softness);

			CheckGDM(CALAddEdge((g_data.m_hcalFont[i]), priority, (g_data.m_hcalColor[iColor]), type, angle, size, softness), __LINE__);
		}
	}
// AfxMessageBox("debug05"); 
		// edge effects
	CString szFields[DL_MAXFIELDS];
	int nItems = tu.breakStringOnSepChar('|', g_settings.m_szIconFontEdge, szFields, DL_MAXFIELDS);

	if((nItems>5)&&(atoi(szFields[0])>0)) // fields zero is true false for edge effects
	{

			long priority, softness;
			ecalEdgeType type;
			Real32 angle, size;

			int iColor =atol(szFields[1]); // color index
			if((iColor>=DL_MAXCOLORS)||(iColor<0)) iColor=0;
			priority = 0; // priority is always 0
			type = (ecalEdgeType)atol(szFields[2]);
			angle = (Real32)atof(szFields[3]);
			size  = (Real32)atof(szFields[4]);
			softness = atol(szFields[5]);

			CheckGDM(CALAddEdge((g_data.m_hcalIconFont), priority, (g_data.m_hcalColor[iColor]), type, angle, size, softness), __LINE__);
	}
// AfxMessageBox("debug06"); 
 
  CString szFontCache = ""; char ch;
  for (ch = ' '; ch <= '~'; ch++) szFontCache+=ch;
	// font 0 gets top 3 colors
	for(i=0; i<3; i++)
	  CheckGDM(CALCacheFontChars(g_data.m_hcalFont[0], g_data.m_hcalColor[i], (LPCTSTR) szFontCache), __LINE__);
 	// font 1 gets top 2 colors
	for(i=0; i<2; i++)
	  CheckGDM(CALCacheFontChars(g_data.m_hcalFont[1], g_data.m_hcalColor[i], (LPCTSTR) szFontCache), __LINE__);
 	// font 2 gets color 0
  CheckGDM(CALCacheFontChars(g_data.m_hcalFont[2], g_data.m_hcalColor[DL_COLOR_ORANGE], (LPCTSTR) szFontCache), __LINE__);
 	// font 3 gets color 1
  CheckGDM(CALCacheFontChars(g_data.m_hcalFont[3], g_data.m_hcalColor[DL_COLOR_WHITE], (LPCTSTR) szFontCache), __LINE__);

  g_data.m_szDingbatsCache=""; // must store this, for compares.
  for (i=0; i<g_settings.m_nNumCategories; i++) 
		g_data.m_szDingbatsCache += (char) g_settings.m_CatInfo[i].nIconChar;

	// all icon fonts get colors 1 and 3
	CheckGDM(CALCacheFontChars(g_data.m_hcalIconFont, g_data.m_hcalColor[DL_COLOR_WHITE], (LPCTSTR) g_data.m_szDingbatsCache), __LINE__);
	CheckGDM(CALCacheFontChars(g_data.m_hcalIconFont, g_data.m_hcalColor[DL_COLOR_PURPLE], (LPCTSTR) g_data.m_szDingbatsCache), __LINE__);

//  AfxMessageBox("debug06a"); 

	// Dedications
  // setup backplates:
  // Dedications:
  //g_md.DM(0,0,0,"backplate = %ld", m_DedicationBackplateID);

  CheckGDM(CALImageRgn(0, 0, 
		g_settings.m_sizeDedicationImgRgn.cx, g_settings.m_sizeDedicationImgRgn.cy, 
		ecalCenter,ecalScaleToFit, 
    &g_data.m_hcalDedicationBackplateID), __LINE__);

  CheckGDM(CALCacheImage(g_data.m_hcalDedicationBackplateID, "DedicationGeneric", 
    getRelativePath() + g_settings.m_szDedicationBackplateFile), __LINE__);
  
  int x=0, y=0, nCategoryOffset=10; // was 12
	int nCrawlRate=g_settings.m_nDedicationCrawlRate;
  // VGA preview mode speed compensation
  if (!g_bUseDuet) nCrawlRate*=3;

  //  Dedications use these:
  // g_data.m_hcalDedicationCategory, 
	// g_data.m_hcalDedicationIcon, g_data.m_hcalDedicationFrom, 
	// g_data.m_hcalDedicationTo, g_data.m_hcalDedicationCrawl 
//AfxMessageBox("debug07"); 

  // category
  CheckGDM(CALTextRgn(x+13+12+nCategoryOffset, y+3, 160-nCategoryOffset, 25,  
    g_data.m_hcalFont[1], g_data.m_hcalColor[DL_COLOR_ORANGE], ecalCenterLeft, &g_data.m_hcalDedicationCategory), __LINE__);
  CheckGDM(CALTextOptions(g_data.m_hcalDedicationCategory, 0, ecalScaleTextToFit, 0, 0), __LINE__);

  // icon
  CheckGDM(CALTextRgn(x+13, y-10, 50, 25,  
    g_data.m_hcalIconFont, g_data.m_hcalColor[DL_COLOR_PURPLE], ecalCenterLeft, &g_data.m_hcalDedicationIcon), __LINE__);
  CheckGDM(CALTextOptions(g_data.m_hcalDedicationIcon, 0, ecalScaleTextToFit, 0, 0), __LINE__);

	// source icon  //297, 72, 50, 65 (full size) * 424/494 scale factor = 255, 62
  CheckGDM(CALImageRgn(255, 62, 43, 56, 
		ecalCenter,ecalScaleToFit, 
    &g_data.m_hcalSourceIcon), __LINE__);

  CheckGDM(CALCacheImage(g_data.m_hcalSourceIcon, "SourceIcon", 
    getRelativePath() + g_settings.m_szSourceIconFile0), __LINE__);

  // name to
  CheckGDM(CALTextRgn(x+176+22, y+8, 182, 18,  g_data.m_hcalFont[2], g_data.m_hcalColor[DL_COLOR_ORANGE], ecalCenter, &g_data.m_hcalDedicationTo), __LINE__);
  CheckGDM(CALTextOptions(g_data.m_hcalDedicationTo, 0, ecalCompLine, 0, 0), __LINE__);

  // from
  CheckGDM(CALTextRgn(x+63+17, y+73, 175, 18,  g_data.m_hcalFont[2], g_data.m_hcalColor[DL_COLOR_ORANGE], ecalCenter, &g_data.m_hcalDedicationFrom), __LINE__);
  CheckGDM(CALTextOptions(g_data.m_hcalDedicationFrom, 0, ecalCompLine, 0, 0), __LINE__);

  // The crawl
  CheckGDM(CALTextRgn(x+42+1, y+35, g_settings.m_nDedicationWidth, 34,  g_data.m_hcalFont[0], g_data.m_hcalColor[DL_COLOR_WHITE], ecalUpperLeft, &g_data.m_hcalDedicationCrawl), __LINE__);
  CheckGDM(CALTextOptions(g_data.m_hcalDedicationCrawl, 0.0f, ecalNoClip, 0, 0), __LINE__);
  CheckGDM(CALCrawl(g_data.m_hcalDedicationCrawl, ecalRate, nCrawlRate, ecalNoEase), __LINE__);

  CheckGDM(CALGroup(6, 
		g_data.m_hcalDedicationBackplateID, 
		g_data.m_hcalDedicationCategory, 
		g_data.m_hcalDedicationTo, 
    g_data.m_hcalDedicationFrom, 
		g_data.m_hcalDedicationCrawl, 
		g_data.m_hcalDedicationIcon, 
		&g_data.m_hcalDedicationGroupID), __LINE__);
  CheckGDM(CALPoly3DView(g_data.m_hcalDedicationGroupID), __LINE__);
  CheckGDM(CALSetOptions(g_data.m_hcalDedicationGroupID, "Rendering\\View\\AutoFocus=500"), __LINE__);
//AfxMessageBox("debug08"); 

  
  //REPLIES
  // backplates
  CheckGDM(CALImageRgn(0, 0, g_settings.m_sizeReplyImgRgn.cx, g_settings.m_sizeReplyImgRgn.cy, ecalCenter, ecalScaleToFit, &g_data.m_hcalReplyBackplateID), __LINE__);
  CheckGDM(CALCacheImage(g_data.m_hcalReplyBackplateID, "ReplyNormal", 
    getRelativePath() + g_settings.m_szReplyDedBackplateFile), __LINE__);
  CheckGDM(CALCacheImage(g_data.m_hcalReplyBackplateID, "ReplyReverse", 
    getRelativePath() + g_settings.m_szReplyBackplateFile), __LINE__);

  // reply icons
  CheckGDM(CALTextRgn(x-2, y, 50, 25,  
    g_data.m_hcalIconFont, g_data.m_hcalColor[DL_COLOR_WHITE], ecalCenterLeft, &g_data.m_hcalReplyIcon1), __LINE__);
  CheckGDM(CALTextOptions(g_data.m_hcalReplyIcon1, 0, ecalScaleTextToFit, 0, 0), __LINE__);

  CheckGDM(CALTextRgn(x-5, y+40, 50, 25,  
    g_data.m_hcalIconFont, g_data.m_hcalColor[DL_COLOR_WHITE], ecalCenterLeft, &g_data.m_hcalReplyIcon2), __LINE__);
  CheckGDM(CALTextOptions(g_data.m_hcalReplyIcon2, 0, ecalScaleTextToFit, 0, 0), __LINE__);

	nCrawlRate=g_settings.m_nReplyDedCrawlRate;
  // VGA preview mode speed compensation
  if (!g_bUseDuet) nCrawlRate*=3;

  //crawl
  CheckGDM(CALTextRgn(x+75-11, y+40+8, g_settings.m_nReplyDedWidth, 30,  g_data.m_hcalFont[0], g_data.m_hcalColor[DL_COLOR_WHITE], ecalUpperLeft, &g_data.m_hcalReplyCrawl1), __LINE__);
  CheckGDM(CALTextOptions(g_data.m_hcalReplyCrawl1, 0.0f, ecalNoClip, 0.0f, 0.0f), __LINE__);
  CheckGDM(CALCrawl(g_data.m_hcalReplyCrawl1, ecalRate, nCrawlRate, ecalNoEase), __LINE__);

	nCrawlRate=g_settings.m_nReplyCrawlRate;
  // VGA preview mode speed compensation
  if (!g_bUseDuet) nCrawlRate*=3;
  // second (reply) crawl
  CheckGDM(CALTextRgn(x+76-12, y+15+4, g_settings.m_nReplyWidth, 30,  g_data.m_hcalFont[0], g_data.m_hcalColor[DL_COLOR_WHITE], ecalUpperLeft, &g_data.m_hcalReplyCrawl2), __LINE__);
  CheckGDM(CALTextOptions(g_data.m_hcalReplyCrawl2, 0.0f, ecalNoClip, 0.0f, 0.0f), __LINE__);
  CheckGDM(CALCrawl(g_data.m_hcalReplyCrawl2, ecalRate, nCrawlRate, ecalNoEase), __LINE__);

  // category1
//  CheckGDM(CALTextRgn((x+95-50)-23, y+6+5, 160, 25,  g_data.m_hcalFont[1], g_data.m_hcalColor[DL_COLOR_WHITE], ecalUpperLeft, &g_data.m_hcalReplyCategory), __LINE__);
  CheckGDM(CALTextRgn((x+17+nCategoryOffset), y+6+5, 190-nCategoryOffset, 25,  g_data.m_hcalFont[1], g_data.m_hcalColor[DL_COLOR_WHITE], ecalUpperLeft, &g_data.m_hcalReplyCategory), __LINE__);
  CheckGDM(CALTextOptions(g_data.m_hcalReplyCategory, 0, ecalScaleTextToFit, 0, 0), __LINE__);

  // Original Dedication
  CheckGDM(CALTextRgn(x+207, y+8+6, 220, 25,  g_data.m_hcalFont[3], g_data.m_hcalColor[DL_COLOR_WHITE], ecalCenter, &g_data.m_hcalReplyOrigLabel), __LINE__);
  CheckGDM(CALTextOptions(g_data.m_hcalReplyOrigLabel, 0, ecalNoClip, 0.5, 0), __LINE__);
  CheckGDM(CALUpdateW(g_data.m_hcalReplyOrigLabel, ecalUpdateSelect, 1, "Original Dedication"), __LINE__);

  // DEDICATION REPLY text
  CheckGDM(CALTextRgn(x+207, y+45+9, 220, 25, g_data.m_hcalFont[3], g_data.m_hcalColor[DL_COLOR_WHITE], ecalCenter, &g_data.m_hcalReplyReplyLabel), __LINE__);
  CheckGDM(CALTextOptions(g_data.m_hcalReplyReplyLabel, 0, ecalNoClip, 1, 0), __LINE__);
  CheckGDM(CALUpdateW(g_data.m_hcalReplyReplyLabel, ecalUpdateSelect, 1, "Dedication Reply"), __LINE__);

  // Category2 on reverse backplate
//  CheckGDM(CALTextRgn(x+97-72, y+42+8, 160, 25,  g_data.m_hcalFont[1], g_data.m_hcalColor[DL_COLOR_WHITE], ecalUpperLeft, &g_data.m_hcalReplyCategory2), __LINE__);
  CheckGDM(CALTextRgn(x+15+nCategoryOffset, y+42+8, 190-nCategoryOffset, 25,  g_data.m_hcalFont[1], g_data.m_hcalColor[DL_COLOR_WHITE], ecalUpperLeft, &g_data.m_hcalReplyCategory2), __LINE__);
  CheckGDM(CALTextOptions(g_data.m_hcalReplyCategory2, 0, ecalScaleTextToFit, 0, 0), __LINE__);

  // Group 1
  CheckGDM(CALGroup(3, 
		g_data.m_hcalReplyBackplateID, 
		g_data.m_hcalReplyOrigLabel, 
		g_data.m_hcalReplyCategory, 
		&g_data.m_hcalReplyGroupID), __LINE__);
  CheckGDM(CALPoly3DView(g_data.m_hcalReplyGroupID), __LINE__);
  CheckGDM(CALSetOptions(g_data.m_hcalReplyGroupID, "Rendering\\View\\AutoFocus=500"), __LINE__);
}

void CDedicateLiveCore::UnSetup()
{
  CALRegisterUserEventHandler(NULL, 0);
}

void CDedicateLiveCore::Remove(HCAL hcal)
{
	long priority;
	CheckGDM(CALGetPriority(hcal, &priority), __LINE__);
	CheckGDM(CALRemoveFromGroup(0, priority, 1), __LINE__);
}

// plays the dedication part of either dedication or reply
UINT CDedicateLiveCore::Play(CString szDelimitedData)
{
	if(!g_data.m_bGraphicsSetup) return DL_ERROR;
	UINT dwReturn = DL_SUCCESS;
	CTextUtils tu;
	CString szFields[DL_MAXFIELDS];
	szDelimitedData = StripNonCachedChars(szDelimitedData);
	g_data.m_szLastDataPlayed = szDelimitedData;  
	
	int nItems = tu.breakStringOnSepChar('|', szDelimitedData, szFields, DL_MAXFIELDS);

	int nType = tu.ReturnHexByte(szFields[0]);
	for (int i=DFIELD_SENDER_NAME+2; i<RFIELD_REPLY+2; i++)
	{
		szFields[i].TrimLeft();
		szFields[i].TrimRight();
	}

	g_data.m_dwStartTime = clock();
	g_data.m_dwExpireTime = EXPIRY_THRESHOLD_MS + g_data.m_dwStartTime;

	if(szDelimitedData.Compare(g_data.m_szLastDataEstimated)) // have to re-estimate timing, etc.
	{
		switch(nType)
		{
		case DB_TYPE_TEXT://          0x04 // messages
			{
				if (atoi(szFields[1]) == TYPE_DEDICATION)
				{
					g_data.m_szLastDataCrawl1 = BuildCrawl(szDelimitedData);
					g_data.m_szLastDataCrawl2 = _T("");
					g_data.m_nLengthMetric_D = g_data.m_szLastDataCrawl1.GetLength(); //may want to do a DC textlen to get kerning
	//				g_data.m_nLengthMetric_D = GetTextExtent(g_data.m_szLastDataCrawl1);
					g_data.m_dwLastDataEstimate = 
						(DWORD)(	(int)((double)g_data.m_nLengthMetric_D*g_data.m_dblTimingMultiplier_D)
										+ g_data.m_nTimingOffsetMS_D);
				}
				else 
				if (atoi(szFields[1]) == TYPE_REPLY)
				{
					g_data.m_szLastDataCrawl1 = BuildCrawl(szDelimitedData);
					g_data.m_szLastDataCrawl2 = BuildReplyCrawl(szDelimitedData);
					g_data.m_nLengthMetric_R1 = g_data.m_szLastDataCrawl1.GetLength(); //may want to do a DC textlen to get kerning
	//				g_data.m_nLengthMetric_R1 = GetTextExtent(g_data.m_szLastDataCrawl1);
					g_data.m_nLengthMetric_R2 = g_data.m_szLastDataCrawl2.GetLength(); //may want to do a DC textlen to get kerning
	//				g_data.m_nLengthMetric_R2 = GetTextExtent(g_data.m_szLastDataCrawl2);
					g_data.m_dwLastDataEstimate = 
						(DWORD)(	(int)((double)g_data.m_nLengthMetric_R1*g_data.m_dblTimingMultiplier_R1) 
										+ (int)((double)g_data.m_nLengthMetric_R2*g_data.m_dblTimingMultiplier_R2)
										+ g_data.m_nTimingOffsetMS_R1+g_data.m_nTimingOffsetMS_R2);
				}
				g_data.m_dwExpireTime+=g_data.m_dwLastDataEstimate;
				g_data.m_dwLastPlayedEstimate = g_data.m_dwLastDataEstimate;
			} break;
		}
	}
	else  // already did this, so save some processing by using stored values
	{
		g_data.m_dwExpireTime+=g_data.m_dwLastDataEstimate;
		g_data.m_dwLastPlayedEstimate = g_data.m_dwLastDataEstimate;
	}

	switch(nType)
	{
	case DB_TYPE_TEXT://          0x04 // messages
		{
			if (atoi(szFields[1]) == TYPE_DEDICATION)
			{
				g_data.m_nEventPlaying = 1;

				if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Caching dedication regions...|Play (Dedication)" ); //(Dispatch message)

				// zoom in and start crawl
				// *******
				int iw, ih; //imageWidth, imageHeight
				long attribs[20], keys[20];  // attribute to affect, keyframes
				Real32 values[20],  scale=2.0f; // values, initial scale factor
				int n=0;

				iw=g_settings.m_sizeDedicationImgRgn.cx; ih=g_settings.m_sizeDedicationImgRgn.cy;

				// Find the position
				g_data.m_nPositionIndex++;
				if (g_data.m_nPositionIndex>2) g_data.m_nPositionIndex=0;

			#ifdef BACKPLATE_IT
				HCAL bg;
				CString bgname = getRelativePath() + "DefaultData\\DedicateLive\\backdrop.bmp";
				CheckGDM(CALImageRgn(0, 0, 720, 486, ecalCenter, ecalScaleToFit, &bg), __LINE__);
				CheckGDM(CALCacheImage(bg, "bg", (LPCTSTR)(bgname)), __LINE__);
				CheckGDM(CALUpdateW(bg, ecalUpdateSelect, 1, "bg"), __LINE__);
				CheckGDM(CALPlayI(bg), __LINE__);
			#endif

				CheckGDM(CALUpdateW(g_data.m_hcalDedicationBackplateID, ecalUpdateSelect, 1, "DedicationGeneric"), __LINE__);

				CString szTemp;
				szTemp.Format("From: %s", szFields[DFIELD_SENDER_NAME+2]);
				CheckGDM(CALUpdateW(g_data.m_hcalDedicationFrom, ecalUpdateSelect, 1, (LPCTSTR)(szTemp)), __LINE__);
				szTemp.Format("To: %s", szFields[DFIELD_RECVR_NAME+2]);
				CheckGDM(CALUpdateW(g_data.m_hcalDedicationTo, ecalUpdateSelect, 1, (LPCTSTR)(szTemp)), __LINE__);
				szFields[DFIELD_CAT_NAME+2].MakeUpper();
				CheckGDM(CALUpdateW(g_data.m_hcalDedicationCategory, ecalUpdateSelect, 1, 
					(LPCTSTR)(szFields[DFIELD_CAT_NAME+2])), __LINE__);

				int nChar = LookupCategoryIcon(szFields[DFIELD_CAT_NAME+2]);

				if(g_settings.m_bVerboseLogging)
				{
					g_md.DM(0,MD_FMT_UI_ICONHAND,0,"category data: %s, %d|",szFields[DFIELD_CAT_NAME+2], nChar); //(Dispatch message)
					for (i=0; i<min(g_settings.m_nNumCategories,DL_MAXCATS); i++)
					{
					g_md.DM(0,MD_FMT_UI_ICONHAND,0,"category data %d: %s, %d|",i,
						g_settings.m_CatInfo[i].szName,
						g_settings.m_CatInfo[i].nIconChar);
					}
				}

				if((nChar>0)&&
					 (g_data.m_szDingbatsCache.Find((char)nChar)>=0))
				{
					CheckGDM(CALUpdateW(g_data.m_hcalDedicationIcon, ecalUpdateSelect, 1, 
						(LPCTSTR)((CString) nChar)), __LINE__);
				}
				else // blank icon....
				{
					CheckGDM(CALUpdateW(g_data.m_hcalDedicationIcon, ecalUpdateSelect, 1, 
						(LPCTSTR)("")), __LINE__);
				}

				g_data.m_nSourceIcon = atoi(szFields[DFIELD_SOURCE_ID+2]);
				if(g_data.m_nSourceIcon<0) g_data.m_nSourceIcon=0;
				switch(g_data.m_nSourceIcon)
				{
				case 0:
					{
					} break;
				case 1:
					{
						CheckGDM(CALInsertIntoGroup(g_data.m_hcalDedicationGroupID, -1, 1, g_data.m_hcalSourceIcon), __LINE__);
						CheckGDM(CALUpdateW(g_data.m_hcalSourceIcon, ecalUpdateSelect, 1, "SourceIcon"), __LINE__);
					} break;
				}

				szTemp=g_pduet->CALEncodeBrackets(g_data.m_szLastDataCrawl1);
				szTemp=g_pduet->CALAppendEvent(szTemp, "[e 1]");
				if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Zooming dedication in and playing...|Play (Dedication)" ); //(Dispatch message)
				if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Data: %s|Play (Dedication)", szTemp ); //(Dispatch message)

				// animation keyframes
				for (n=0, i=0; i<20; i++) {	attribs[i] = 0; keys[i] = 0; values[i] = 0.0; }
				n=0;
				//CheckGDM(CALAnimate(m_DedicationGroupID, attribs, 0, keys, values, ecalClearKeyFrames), __LINE__);

				// scale in
				attribs[n] = ecalXCenter;	keys[n] = 0;      values[n] = (iw/2);	n++;
				attribs[n] = ecalYCenter;	keys[n] = 0;      values[n] = (ih/2);	n++;
				attribs[n] = ecalZCenter;	keys[n] = 0;      values[n] = 0;	n++;
				attribs[n] = ecalXCenter;	keys[n] = g_settings.m_nDedicationZoomInFrames; values[n] = (iw/2);	n++;
				attribs[n] = ecalYCenter;	keys[n] = g_settings.m_nDedicationZoomInFrames; values[n] = (ih/2);	n++;
				attribs[n] = ecalZCenter;	keys[n] = g_settings.m_nDedicationZoomInFrames; values[n] = 0;	n++;

				attribs[n] = ecalXScale;		keys[n] = 0;	    values[n] = 0.0;	  n++;
				attribs[n] = ecalXScale;		keys[n] = g_settings.m_nDedicationZoomInFrames;	values[n] = 1.0;		  n++;
				attribs[n] = ecalYScale;		keys[n] = 0;	    values[n] = 0.0;	  n++;
				attribs[n] = ecalYScale;		keys[n] = g_settings.m_nDedicationZoomInFrames;	values[n] = 1.0;		  n++;

				attribs[n] = ecalXPos;		keys[n] = 0;	    values[n] = g_settings.m_sizePosOffsets[g_data.m_nPositionIndex].cx/*+(iw/2)*/;	  n++;
				attribs[n] = ecalXPos;		keys[n] = g_settings.m_nDedicationZoomInFrames;	values[n] = g_settings.m_sizePosOffsets[g_data.m_nPositionIndex].cx;		  n++;
				attribs[n] = ecalYPos;		keys[n] = 0;	    values[n] = g_settings.m_sizePosOffsets[g_data.m_nPositionIndex].cy/*+(ih/2)*/;	  n++;
				attribs[n] = ecalYPos;		keys[n] = g_settings.m_nDedicationZoomInFrames;	values[n] = g_settings.m_sizePosOffsets[g_data.m_nPositionIndex].cy;		  n++;

				//attribs[n] = ecalAlpha;	  keys[n] = 0;           values[n] = 0;   n++;
				//attribs[n] = ecalAlpha;	  keys[n] = 1;           values[n] = 255; n++;
				//attribs[n] = ecalAlpha;	  keys[n] = g_settings.m_nDedicationZoomInFrames; values[n] = 255;	n++;

				CheckGDM(CALAnimate(g_data.m_hcalDedicationGroupID, attribs, n, keys, values, (enum ecalInterpMode)(ecalLinear | ecalClearKeyFrames)), __LINE__);
				CheckGDM(CALUpdateW(g_data.m_hcalDedicationCrawl, ecalUpdateAndSpoolEnd, 1, 
					(LPCTSTR)(szTemp)), __LINE__);
				CheckGDM(CALPlayW(g_data.m_hcalDedicationGroupID), __LINE__);
//	g_md.DM(0,MD_FMT_UI_ICONEXCLAMATION,0,"p1|ExecuteCommand");
				if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Played dedication to right edge...|Play (Dedication)" ); //(Dispatch message)

				// *******
			}
			else 
			if (atoi(szFields[1]) == TYPE_REPLY)
			{
				g_data.m_nEventPlaying = 2;
				if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Caching dedication regions...|Play (Reply)" ); //(Dispatch message)

				// zoom in and start crawl
				// *******
				int iw, ih; //imageWidth, imageHeight
				long attribs[20], keys[20];  // attribute to affect, keyframes
				Real32 values[20],  scale=2.0f; // values, initial scale factor
				int n=0, i;

				iw=g_settings.m_sizeReplyImgRgn.cx; ih=g_settings.m_sizeReplyImgRgn.cy;

				// Find the position
				g_data.m_nPositionIndex++;
				if (g_data.m_nPositionIndex>2) g_data.m_nPositionIndex=0;

			#ifdef BACKPLATE_IT
				HCAL bg;
				CString bgname = getRelativePath() + "DefaultData\\DedicateLive\\backdrop.bmp";
				CheckGDM(CALImageRgn(0, 0, 720, 486, ecalCenter, ecalScaleToFit, &bg), __LINE__);
				CheckGDM(CALCacheImage(bg, "bg", (LPCTSTR)(bgname)), __LINE__);
				CheckGDM(CALUpdateW(bg, ecalUpdateSelect, 1, "bg"), __LINE__);
				CheckGDM(CALPlayI(bg), __LINE__);
			#endif

				CheckGDM(CALInsertIntoGroup(g_data.m_hcalReplyGroupID, -1, 2, g_data.m_hcalReplyCrawl1, g_data.m_hcalReplyIcon1), __LINE__);

				// Select the proper backplate
				CheckGDM(CALUpdateW(g_data.m_hcalReplyBackplateID, ecalUpdateSelect, 1, "ReplyNormal"), __LINE__);
				CString szTemp;

				szFields[RFIELD_CAT_NAME+2].MakeUpper();
				CheckGDM(CALUpdateW(g_data.m_hcalReplyCategory, ecalUpdateSelect, 1, 
					(LPCTSTR)(szFields[RFIELD_CAT_NAME+2])), __LINE__);
				
				g_data.m_szAfterFlipCategory = szFields[RFIELD_CAT_NAME+2];

				int nChar = LookupCategoryIcon(szFields[RFIELD_CAT_NAME+2]);
				if((nChar>0)&&
					 (g_data.m_szDingbatsCache.Find((char)nChar)>=0))
				{
					CheckGDM(CALUpdateW(g_data.m_hcalReplyIcon1, ecalUpdateSelect, 1, 
						(LPCTSTR)((CString) nChar)), __LINE__);
					CheckGDM(CALUpdateW(g_data.m_hcalReplyIcon2, ecalUpdateSelect, 1, 
						(LPCTSTR)((CString) nChar)), __LINE__);
				}
				else // blank icon....
				{
					CheckGDM(CALUpdateW(g_data.m_hcalReplyIcon1, ecalUpdateSelect, 1, 
						(LPCTSTR)("")), __LINE__);
					CheckGDM(CALUpdateW(g_data.m_hcalReplyIcon2, ecalUpdateSelect, 1, 
						(LPCTSTR)("")), __LINE__);
				}

				CheckGDM(CALUpdateW(g_data.m_hcalReplyOrigLabel, ecalUpdateSelect, 1, "Original Dedication"), __LINE__);

				
				szTemp=g_pduet->CALEncodeBrackets(g_data.m_szLastDataCrawl1);
				szTemp=g_pduet->CALAppendEvent(szTemp, "[e 2]");

				if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Zooming dedication in and playing...|Play (Reply)" ); //(Dispatch message)
				if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Data: %s|Play (Reply)", szTemp ); //(Dispatch message)
				// Fly In
				for (n=0, i = 0; i<20; i++) {	attribs[i] = 0; keys[i] = 0; values[i] = 0.0; }
				n = 0;
			//  CheckGDM(CALAnimate(m_ReplyGroupID, attribs, 0, keys, values, ecalClearKeyFrames), __LINE__);
  
				attribs[n] = ecalXCenter;	keys[n] = 0;      values[n] = /*g_settings.m_sizePosOffsets[g_data.m_nPositionIndex].cx+*/(iw/2);	n++;
				attribs[n] = ecalYCenter;	keys[n] = 0;      values[n] = /*g_settings.m_sizePosOffsets[g_data.m_nPositionIndex].cy+*/(ih/2);	n++;
				attribs[n] = ecalZCenter;	keys[n] = 0;      values[n] = 0;	n++;
				attribs[n] = ecalXCenter;	keys[n] = g_settings.m_nReplyDedZoomInFrames; values[n] = /*g_settings.m_sizePosOffsets[g_data.m_nPositionIndex].cx+*/(iw/2);	n++;
				attribs[n] = ecalYCenter;	keys[n] = g_settings.m_nReplyDedZoomInFrames; values[n] = /*g_settings.m_sizePosOffsets[g_data.m_nPositionIndex].cy+*/(ih/2);	n++;
				attribs[n] = ecalZCenter;	keys[n] = g_settings.m_nReplyDedZoomInFrames; values[n] = 0;	n++;

				attribs[n] = ecalXScale;  keys[n] = 0;	    values[n] = 0.0;	  n++;
				attribs[n] = ecalXScale;	keys[n] = g_settings.m_nReplyDedZoomInFrames;	values[n] = 1.0;		  n++;
				attribs[n] = ecalYScale;	keys[n] = 0;	    values[n] = 0.0;	  n++;
				attribs[n] = ecalYScale;	keys[n] = g_settings.m_nReplyDedZoomInFrames;	values[n] = 1.0;		  n++;

				attribs[n] = ecalXPos;		keys[n] = 0;	    values[n] = g_settings.m_sizeRPosOffsets[g_data.m_nPositionIndex].cx/*+(iw/2)*/;	  n++;
				attribs[n] = ecalXPos;		keys[n] = g_settings.m_nReplyDedZoomInFrames;	values[n] = g_settings.m_sizeRPosOffsets[g_data.m_nPositionIndex].cx;		  n++;
				attribs[n] = ecalYPos;		keys[n] = 0;	    values[n] = g_settings.m_sizeRPosOffsets[g_data.m_nPositionIndex].cy/*+(ih/2)*/;	  n++;
				attribs[n] = ecalYPos;		keys[n] = g_settings.m_nReplyDedZoomInFrames;	values[n] = g_settings.m_sizeRPosOffsets[g_data.m_nPositionIndex].cy;		  n++;

				CheckGDM(CALAnimate(g_data.m_hcalReplyGroupID, attribs, n, keys, values, (enum ecalInterpMode)((enum ecalInterpMode)(ecalLinear | ecalClearKeyFrames) | ecalClearKeyFrames)), __LINE__);
				CheckGDM(CALUpdateW(g_data.m_hcalReplyCrawl1, ecalUpdateAndSpoolEnd, 1, (LPCTSTR)(szTemp)), __LINE__);
				CheckGDM(CALPlayW(g_data.m_hcalReplyGroupID), __LINE__);
				if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Played dedication to right edge...|Play (Reply)" ); //(Dispatch message)
				
				// *******
			}
		} break;
	}


	return dwReturn;
}

// plays the reply part of the reply
UINT CDedicateLiveCore::PlayReply()
{
	if(!g_data.m_bGraphicsSetup) return DL_ERROR;
	UINT dwReturn = DL_SUCCESS;
	g_data.m_nEventPlaying = 3;

	int iw, ih; //imageWidth, imageHeight
	long attribs[20], keys[20];  // attribute to affect, keyframes
	Real32 values[20],  scale=2.0f; // values, initial scale factor
	int n=0, i;
  long priority;
  int nStart;
	CString szTemp;

	iw=g_settings.m_sizeReplyImgRgn.cx; ih=g_settings.m_sizeReplyImgRgn.cy;

	if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Removing dedication...|PlayReply" ); //(Dispatch message)

	// Get rid of dedication crawl, flip, and play out g_data.m_szLastDataCrawl2
  Remove(g_data.m_hcalReplyGroupID);

  CheckGDM(CALUpdateW(g_data.m_hcalReplyCrawl1, ecalUpdateSelect, 1, " "), __LINE__);
//  CheckGDM(CALUpdateW(g_data.m_hcalReplyCrawl1, ecalUpdateAndReplace, 1, " "), __LINE__);

  CheckGDM(CALGetPriority(g_data.m_hcalReplyCrawl1, &priority), __LINE__); 
  CheckGDM(CALRemoveFromGroup(g_data.m_hcalReplyGroupID, priority, 1), __LINE__);
  CheckGDM(CALGetPriority(g_data.m_hcalReplyIcon1, &priority), __LINE__); 
  CheckGDM(CALRemoveFromGroup(g_data.m_hcalReplyGroupID, priority, 1), __LINE__);
  

  //////////////////////////////////////////////////////////////////////////////
  //
  //  FLIP
  //

  //AfxMessageBox("About to flip");
	if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Flipping backplate...|PlayReply" ); //(Dispatch message)

  for (n=0, i = 0; i<20; i++) {	attribs[i] = 0; keys[i] = 0; values[i] = 0.0; }

  n=0;
  // CheckGDM(CALAnimate(m_ReplyGroupID, attribs, 0, keys, values, ecalClearKeyFrames), __LINE__);

  attribs[n] = ecalXPos;		keys[n] = 0;	          values[n] = g_settings.m_sizeRPosOffsets[g_data.m_nPositionIndex].cx; n++;
  attribs[n] = ecalXPos;		keys[n] = g_settings.m_nReplyFlipFrames;	values[n] = g_settings.m_sizeRPosOffsets[g_data.m_nPositionIndex].cx; n++;
  attribs[n] = ecalYPos;		keys[n] = 0;	          values[n] = g_settings.m_sizeRPosOffsets[g_data.m_nPositionIndex].cy; n++;
  attribs[n] = ecalYPos;		keys[n] = g_settings.m_nReplyFlipFrames;	values[n] = g_settings.m_sizeRPosOffsets[g_data.m_nPositionIndex].cy; n++;

  attribs[n] = ecalXCenter;	keys[n] = 0;            values[n] = (iw/2);	n++;
  attribs[n] = ecalYCenter;	keys[n] = 0;            values[n] = (ih/2);	n++;
  attribs[n] = ecalZCenter;	keys[n] = 0;            values[n] = 0;	n++;
  attribs[n] = ecalXCenter;	keys[n] = g_settings.m_nReplyFlipFrames;  values[n] = (iw/2);	n++;
  attribs[n] = ecalYCenter;	keys[n] = g_settings.m_nReplyFlipFrames;  values[n] = (ih/2);	n++;
  attribs[n] = ecalZCenter;	keys[n] = g_settings.m_nReplyFlipFrames;  values[n] = 0;	n++;

  nStart = n;
  attribs[n] = ecalAlpha;	  keys[n] = 0;            values[n] = 255; n++;
  attribs[n] = ecalAlpha;	  keys[n] = g_settings.m_nReplyFlipFrames/2 + 1; values[n] = 255; n++;
  attribs[n] = ecalAlpha;	  keys[n] = g_settings.m_nReplyFlipFrames;  values[n] = 10;	 n++;
  attribs[n] = ecalXRot;	  keys[n] = 0;	          values[n] = 0;	n++;
  attribs[n] = ecalXRot;	  keys[n] = g_settings.m_nReplyFlipFrames;	values[n] = 90;	n++;

  //g_md.DM("About to animate flip...");

  CheckGDM(CALAnimate(g_data.m_hcalReplyGroupID, attribs, n, keys, values, (enum ecalInterpMode)(ecalLinear | ecalClearKeyFrames)), __LINE__);
  CheckGDM(CALPlayW(g_data.m_hcalReplyGroupID), __LINE__);
	if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Switching to reply data...|PlayReply" ); //(Dispatch message)

  // at this point, we're 90 degrees into the rotation
  Remove(g_data.m_hcalReplyGroupID);

  //clear regions that are no longer used...
  //g_md.DM("clear regions that are no longer used...");
  CheckGDM(CALUpdateW(g_data.m_hcalReplyOrigLabel, ecalUpdateSelect, 1, " "), __LINE__);
  CheckGDM(CALUpdateW(g_data.m_hcalReplyCategory, ecalUpdateSelect, 1, " "), __LINE__);
  CheckGDM(CALUpdateW(g_data.m_hcalReplyIcon1, ecalUpdateSelect, 1, " "), __LINE__);

  // add the new category and crawl regions
  CheckGDM(CALUpdateW(g_data.m_hcalReplyCategory2, ecalUpdateSelect, 1, (LPCTSTR)(g_data.m_szAfterFlipCategory)), __LINE__);
	g_data.m_dwStartTimeR2 = clock();
	szTemp=g_pduet->CALEncodeBrackets(g_data.m_szLastDataCrawl2);
	szTemp=g_pduet->CALAppendEvent(szTemp, "[e 3]");
  //CheckGDM(CALUpdateW(g_data.m_hcalReplyReplyLabel, ecalUpdateSelect, 1, "Dedication Reply"), __LINE__);

  // change the image
  //g_md.DM("change the image...");
  CheckGDM(CALUpdateW(g_data.m_hcalReplyBackplateID, ecalUpdateSelect, 1, "ReplyReverse"), __LINE__);

  // add the new stuff to the group
  //g_md.DM("insert new crawl and label into group...");
  CheckGDM(CALInsertIntoGroup(g_data.m_hcalReplyGroupID, -1, 4, g_data.m_hcalReplyCrawl2, 
    g_data.m_hcalReplyReplyLabel, g_data.m_hcalReplyCategory2, g_data.m_hcalReplyIcon2), __LINE__);

	if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Flipping backplate to reply and playing...|PlayReply" ); //(Dispatch message)
	if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Data: %s|PlayReply", szTemp ); //(Dispatch message)

  // rotate again
  attribs[nStart  ] = ecalAlpha;  keys[nStart  ] = 0;           values[nStart] = 10;
  attribs[nStart+1] = ecalAlpha;  keys[nStart+1] = g_settings.m_nReplyFlipFrames/2; values[nStart+1] = 255;
  attribs[nStart+2] = ecalAlpha;  keys[nStart+2] = g_settings.m_nReplyFlipFrames; values[nStart+2] = 255;
  attribs[nStart+3] = ecalXRot;	  keys[nStart+3] = 0;	          values[nStart+3] = -90;
  attribs[nStart+4] = ecalXRot;	  keys[nStart+4] = g_settings.m_nReplyFlipFrames;	values[nStart+4] = 0;

  //g_md.DM("about to rotate again");
  
  CheckGDM(CALAnimate(g_data.m_hcalReplyGroupID, attribs, n, keys, values, (enum ecalInterpMode)(ecalLinear | ecalClearKeyFrames)), __LINE__);
  CheckGDM(CALUpdateW(g_data.m_hcalReplyCrawl2, ecalUpdateAndSpoolEnd, 1, (LPCTSTR)(szTemp)), __LINE__);
  CheckGDM(CALPlayW(g_data.m_hcalReplyGroupID), __LINE__);
	if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Played reply to right edge...|PlayReply" ); //(Dispatch message)

	return dwReturn;
}

UINT CDedicateLiveCore::Intro(CString szDelimitedData)
{
	if(!g_data.m_bGraphicsSetup) return DL_ERROR;
	return 0;
}

UINT CDedicateLiveCore::Outro(CString szDelimitedData)
{
	if(!g_data.m_bGraphicsSetup) return DL_ERROR;
	return 0;
}

DWORD CDedicateLiveCore::EstimateDuration(CString szDelimitedData)
{
	DWORD dwReturn=0;
	CTextUtils tu;
	CString szFields[DL_MAXFIELDS];
	szDelimitedData = StripNonCachedChars(szDelimitedData);
	g_data.m_szLastDataEstimated = szDelimitedData;           
	int nItems = tu.breakStringOnSepChar('|', szDelimitedData, szFields, DL_MAXFIELDS);

	int nType = tu.ReturnHexByte(szFields[0]);

	switch(nType)
	{
	case DB_TYPE_TEXT://          0x08 // messages
		{
			if (atoi(szFields[1]) == TYPE_DEDICATION)
			{
				g_data.m_szLastDataCrawl1 = BuildCrawl(szDelimitedData);
				g_data.m_szLastDataCrawl2 = _T("");
				g_data.m_nLengthMetric_D = g_data.m_szLastDataCrawl1.GetLength(); //may want to do a DC textlen to get kerning
//				g_data.m_nLengthMetric_D = GetTextExtent(g_data.m_szLastDataCrawl1);
				dwReturn = 
					(DWORD)(	(int)((double)g_data.m_nLengthMetric_D*g_data.m_dblTimingMultiplier_D)
									+ g_data.m_nTimingOffsetMS_D);
			}
			else 
			if (atoi(szFields[1]) == TYPE_REPLY)
			{
				g_data.m_szLastDataCrawl1 = BuildCrawl(szDelimitedData);
				g_data.m_szLastDataCrawl2 = BuildReplyCrawl(szDelimitedData);
				g_data.m_nLengthMetric_R1 = g_data.m_szLastDataCrawl1.GetLength(); //may want to do a DC textlen to get kerning
//				g_data.m_nLengthMetric_R1 = GetTextExtent(g_data.m_szLastDataCrawl1);
				g_data.m_nLengthMetric_R2 = g_data.m_szLastDataCrawl2.GetLength(); //may want to do a DC textlen to get kerning
//				g_data.m_nLengthMetric_R2 = GetTextExtent(g_data.m_szLastDataCrawl2);
				dwReturn = 
					(DWORD)(	(int)((double)g_data.m_nLengthMetric_R1*g_data.m_dblTimingMultiplier_R1 )
									+ (int)((double)g_data.m_nLengthMetric_R2*g_data.m_dblTimingMultiplier_R2 )
									+ g_data.m_nTimingOffsetMS_R1 + g_data.m_nTimingOffsetMS_R2);
			}
		} break;
	}

	g_data.m_dwLastDataEstimate = dwReturn;
	return dwReturn;
}

CString CDedicateLiveCore::StripNonCachedChars(CString szText)
{
	CString szReturn=""; 
	char ch;
	for (int i=0; i<szText.GetLength(); i++)
	{
		ch=szText.GetAt(i);
    if ((ch>=' ')&&(ch<='~'))
    {
      szReturn+=(char)ch;
		}
	}
	return szReturn;
}

int  CDedicateLiveCore::GetTextExtent(CString szText)
{
	CSize size; size.cx=-1;
	if(g_data.m_bDCOK)
		size = g_data.m_dc.GetTextExtent(szText);
	return size.cx;
}

void CDedicateLiveCore::AddCategoryRecord(CString szDelimitedData)
{
	int nArrayIdx=-1; 
	CString szFields[DL_MAXFIELDS];
	CTextUtils tu;
	int nItems = tu.breakStringOnSepChar('|', szDelimitedData, szFields, DL_MAXFIELDS);
	if(nItems<INFO_CAT_FIELD_ICON_CHAR) 
		return; // not good enough data.
	nArrayIdx = LookupCategoryIndex(szFields[INFO_CAT_FIELD_CATNAME]);
	
	if(nArrayIdx>=0)  // modify it
	{
//		g_settings.m_CatInfo[nArrayIdx].nIndex=atoi(szFields[INFO_CAT_FIELD_CATNUM]);
		g_settings.m_CatInfo[nArrayIdx].szName=szFields[INFO_CAT_FIELD_CATNAME];
		g_settings.m_CatInfo[nArrayIdx].szName.TrimLeft(); g_settings.m_CatInfo[nArrayIdx].szName.TrimRight();
//		g_settings.m_CatInfo[nArrayIdx].szFont=szFields[INFO_CAT_FIELD_FONT];
//		g_settings.m_CatInfo[nArrayIdx].szFont.TrimLeft(); g_settings.m_CatInfo[nArrayIdx].szFont.TrimRight();
		g_settings.m_CatInfo[nArrayIdx].nIconChar=atoi(szFields[INFO_CAT_FIELD_ICON_CHAR]);
//		g_settings.m_CatInfo[nArrayIdx].nIconSize=atoi(szFields[INFO_CAT_FIELD_ICON_SIZE]);
//		g_settings.m_CatInfo[nArrayIdx].nIconRelX=atoi(szFields[INFO_CAT_FIELD_ICON_RELX]);
//		g_settings.m_CatInfo[nArrayIdx].nIconRelY=atoi(szFields[INFO_CAT_FIELD_ICON_RELY]);
	}
	else if(g_settings.m_nNumCategories<DL_MAXCATS)// add it
	{
//		g_settings.m_CatInfo[g_settings.m_nNumCategories].nIndex=atoi(szFields[INFO_CAT_FIELD_CATNUM]);
		g_settings.m_CatInfo[g_settings.m_nNumCategories].szName=szFields[INFO_CAT_FIELD_CATNAME];
		g_settings.m_CatInfo[g_settings.m_nNumCategories].szName.TrimLeft(); g_settings.m_CatInfo[g_settings.m_nNumCategories].szName.TrimRight();
//		g_settings.m_CatInfo[g_settings.m_nNumCategories].szFont=szFields[INFO_CAT_FIELD_FONT];
//		g_settings.m_CatInfo[g_settings.m_nNumCategories].szFont.TrimLeft(); g_settings.m_CatInfo[g_settings.m_nNumCategories].szFont.TrimRight();
		g_settings.m_CatInfo[g_settings.m_nNumCategories].nIconChar=atoi(szFields[INFO_CAT_FIELD_ICON_CHAR]);
//		g_settings.m_CatInfo[g_settings.m_nNumCategories].nIconSize=atoi(szFields[INFO_CAT_FIELD_ICON_SIZE]);
//		g_settings.m_CatInfo[g_settings.m_nNumCategories].nIconRelX=atoi(szFields[INFO_CAT_FIELD_ICON_RELX]);
//		g_settings.m_CatInfo[g_settings.m_nNumCategories].nIconRelY=atoi(szFields[INFO_CAT_FIELD_ICON_RELY]);
		g_settings.m_nNumCategories++;
	}
	g_settings.Settings(WRITE);
}

void CDedicateLiveCore::DelCategoryRecord(CString szDelimitedData)
{
	if(g_settings.m_nNumCategories<=0) return;  // cant delete if theres nothing
	
	if(szDelimitedData.Compare("delete_all_categories"))
	{
		CString szFields[DL_MAXFIELDS];
		CTextUtils tu;
		int nItems = tu.breakStringOnSepChar('|', szDelimitedData, szFields, DL_MAXFIELDS);
		int nArrayIdx = LookupCategoryIndex(szFields[INFO_CAT_FIELD_CATNAME]);

		if(nArrayIdx>=0)
		{
			int nFrom=nArrayIdx;
			for(int i=nArrayIdx; i<(min(g_settings.m_nNumCategories,DL_MAXCATS)-1); i++)
			{
				nFrom++;
//				g_settings.m_CatInfo[i].nIndex=g_settings.m_CatInfo[nFrom].nIndex;
				g_settings.m_CatInfo[i].szName=g_settings.m_CatInfo[nFrom].szName;
//				g_settings.m_CatInfo[i].szFont=g_settings.m_CatInfo[nFrom].szFont;
				g_settings.m_CatInfo[i].nIconChar=g_settings.m_CatInfo[nFrom].nIconChar;
//				g_settings.m_CatInfo[i].nIconSize=g_settings.m_CatInfo[nFrom].nIconSize;
//				g_settings.m_CatInfo[i].nIconRelX=g_settings.m_CatInfo[nFrom].nIconRelX;
//				g_settings.m_CatInfo[i].nIconRelY=g_settings.m_CatInfo[nFrom].nIconRelY;
			}
			// clear last one
//			g_settings.m_CatInfo[nFrom].nIndex=0;
			g_settings.m_CatInfo[nFrom].szName=_T("");
//			g_settings.m_CatInfo[nFrom].szFont=_T("");
			g_settings.m_CatInfo[nFrom].nIconChar=0;
//			g_settings.m_CatInfo[nFrom].nIconSize=0;
//			g_settings.m_CatInfo[nFrom].nIconRelX=0;
//			g_settings.m_CatInfo[nFrom].nIconRelY=0;
			g_settings.m_nNumCategories--;
		}
	}
	else
	{
		g_settings.m_nNumCategories=0;
		for(int i=0; i<DL_MAXCATS; i++)
		{
//			g_settings.m_CatInfo[i].nIndex=0;
			g_settings.m_CatInfo[i].szName=_T("");
//			g_settings.m_CatInfo[i].szFont=_T("");
			g_settings.m_CatInfo[i].nIconChar=0;
//			g_settings.m_CatInfo[i].nIconSize=0;
//			g_settings.m_CatInfo[i].nIconRelX=0;
//			g_settings.m_CatInfo[i].nIconRelY=0;
		}
	}
	g_settings.Settings(WRITE);
}

int	 CDedicateLiveCore::LookupCategoryIcon(CString szCategory) // returns char based on category name 
{
	if (szCategory.GetLength()<=0) return -1;
	int nReturn = -1;
	int i=0; BOOL bFound = FALSE;
	while((i<g_settings.m_nNumCategories)&&(!bFound))
	{
		if((g_settings.m_CatInfo[i].szName.CompareNoCase(szCategory)==0)
			&&(g_settings.m_CatInfo[i].szName.GetLength()>0))
		{
			bFound=TRUE;
			nReturn = g_settings.m_CatInfo[i].nIconChar;
		}
		i++;
	}
	return nReturn;
}

int	 CDedicateLiveCore::LookupCategoryIndex(CString szCategory) // returns array index based on category name 
{
	if (szCategory.GetLength()<=0) return -1;
	int nReturn = -1;
	int i=0; BOOL bFound = FALSE;
	while((i<g_settings.m_nNumCategories)&&(!bFound))
	{
		if((g_settings.m_CatInfo[i].szName.CompareNoCase(szCategory)==0)
			&&(g_settings.m_CatInfo[i].szName.GetLength()>0))
		{
			bFound=TRUE;
			nReturn = i;
		}
		i++;
	}
	return nReturn;
}

CString CDedicateLiveCore::ReturnCategories()
{
	CString szReturn=_T("");
	for (int i=0; i<min(g_settings.m_nNumCategories,DL_MAXCATS); i++)
	{
//		g_settings.m_CatInfo[i].szFont.TrimLeft(); g_settings.m_CatInfo[i].szFont.TrimRight();
		g_settings.m_CatInfo[i].szName.TrimLeft(); g_settings.m_CatInfo[i].szName.TrimRight();
		szReturn.Format("%s|%d\n",
				szReturn,
//				g_settings.m_CatInfo[i].nIndex,
				g_settings.m_CatInfo[i].szName,
//				g_settings.m_CatInfo[i].szFont,
				g_settings.m_CatInfo[i].nIconChar//,
//				g_settings.m_CatInfo[i].nIconSize,
//				g_settings.m_CatInfo[i].nIconRelX,
//				g_settings.m_CatInfo[i].nIconRelY
			);
	}
	return szReturn;
}

