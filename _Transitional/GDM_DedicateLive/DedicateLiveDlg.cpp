// DedicateLiveDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DedicateLive.h"
#include "DedicateLiveDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CMessageDispatcher g_md;
extern CDedicateLiveDlg *g_pdlg;
extern CDedicateLiveSettings g_settings;
extern CDedicateLiveData g_data;
extern ISMShowData_t g_showdata;  // data to expose to ISM/GDM

#define DEDICATELIVE_KILL_TIMER_ID 37
/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CAboutDlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
  OnLogo(nFlags, point);

	CDialog::OnLButtonUp(nFlags, point);
}

BOOL CAboutDlg::OnInitDialog() 
{
  CDialog::OnInitDialog();

  
  CFont *pFont, newFont, *pSetFont;
  LOGFONT lfLogFont;
//  pFont = GetDlgItem(IDC_STATIC_URL)->GetFont();
  pFont = GetFont();
  COLORREF color;
  CDC *dc;

  // set font to underline
	pSetFont =pFont;

  if(pFont->GetLogFont(&lfLogFont))
	{
		lfLogFont.lfUnderline = (BYTE) TRUE;
		if(newFont.CreateFontIndirect(&lfLogFont))
			pSetFont = &newFont;
	}

  // set color to blue...
  dc = GetDlgItem(IDC_STATIC_URL)->GetDC( );
  color=RGB(0,0,255);
	if(dc!=NULL)
	{
		HDC hdc= dc->GetSafeHdc();
		((CStatic*)GetDlgItem(IDC_STATIC_URL))->SetBitmap(
			TextOutBitmap(
				hdc, 
				"http://www.VideoDesignInteractive.com", 
				pSetFont, 
				color, 
				GetSysColor(COLOR_3DFACE), 
				0
				)
			);
		//center window
		CRect rcCtrl, rcWnd;
		GetClientRect(&rcWnd);
		GetDlgItem(IDC_STATIC_URL)->GetWindowRect(&rcCtrl);
		ScreenToClient(&rcCtrl);
		GetDlgItem(IDC_STATIC_URL)->SetWindowPos(NULL, (rcWnd.Width()-rcCtrl.Width())/2, rcCtrl.top ,0,0,SWP_NOSIZE|SWP_NOZORDER);

	}
	else
	{
		GetDlgItem(IDC_STATICTEXT_URL)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATICTEXT_URL)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_URL)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_URL)->EnableWindow(FALSE);
	}

  CString szTemp;
  szTemp.Format("Build date: %s %s", __DATE__, __TIME__);
  GetDlgItem(IDC_STATIC_BUILD)->SetWindowText(szTemp);


  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void CAboutDlg::OnLogo(UINT nFlags, CPoint point)
{
	CRect rcLogo;
  GetDlgItem(IDC_STATIC_URL)->GetWindowRect(&rcLogo);
  ScreenToClient(&rcLogo);
	if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
		(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
	{
    // launch browser
    HINSTANCE hi;
    hi=ShellExecute((HWND) NULL, "open", "http://www.VideoDesignInteractive.com", NULL, NULL, SW_HIDE);
  }
  GetDlgItem(IDC_STATIC_LOGO)->GetWindowRect(&rcLogo);
  ScreenToClient(&rcLogo);
	if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
		(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
	{
    // launch browser
    HINSTANCE hi;
    hi=ShellExecute((HWND) NULL, "open", "http://www.VideoDesignInteractive.com", NULL, NULL, SW_HIDE);
  }

}




/////////////////////////////////////////////////////////////////////////////
// CDedicateLiveDlg dialog


CDedicateLiveDlg::CDedicateLiveDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDedicateLiveDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDedicateLiveDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_bVis=FALSE;
	m_bNewSizeInit=TRUE;
	m_bListPaused=FALSE;
}


void CDedicateLiveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDedicateLiveDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDedicateLiveDlg, CDialog)
	//{{AFX_MSG_MAP(CDedicateLiveDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_QUERYDRAGICON()
	ON_WM_PAINT()
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_BUTTON_PAUSE, OnButtonPause)
	ON_WM_TIMER()
	ON_WM_CTLCOLOR()
	ON_NOTIFY_EX( TTN_NEEDTEXT, 0, OnToolTipNotify )
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDedicateLiveDlg message handlers

void CDedicateLiveDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);

	}
}

BOOL CDedicateLiveDlg::Create(CWnd* pWnd) 
{
//					AfxMessageBox("Dlg creation");
	return CDialog::Create(CDedicateLiveDlg::IDD, pWnd);
}

void CDedicateLiveDlg::OnCancel() 
{
//	CDialog::OnCancel();
}


void CDedicateLiveDlg::OnOK() 
{
//	CDialog::OnOK();
}

void CDedicateLiveDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

void CDedicateLiveDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
//	AfxMessageBox("on show window");
//sizing stuff
	if(m_bNewSizeInit)
	{
		int nCtrlID;
//		AfxMessageBox("DLL show Wnd size init");
		GetClientRect(&m_rcDlg); 
		CRect rcDlg=m_rcDlg;
		for (int i=0;i<DEDICATELIVEDLG_NUM_MOVING_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_LIST: nCtrlID=IDC_LIST_STATUS;break;
			case ID_STATUS: nCtrlID=IDC_STATIC_STATUS;break;
			case ID_BNPAUSE: nCtrlID=IDC_BUTTON_PAUSE;break;
			case ID_LOGO: nCtrlID=IDC_STATIC_DEDICATELIVE_LOGO;break;
			}
			GetDlgItem(nCtrlID)->GetWindowRect(&m_rcCtrl[i]);
			ScreenToClient(&m_rcCtrl[i]);
		}
	}
	m_bVis=TRUE;
	m_bNewSizeInit=FALSE;
}

void CDedicateLiveDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	int nCtrlID;

	int dx,dy;
	CRect rcList;
	dx=m_rcDlg.right-cx;
	dy=m_rcDlg.bottom-cy;

	if(m_bVis)
	{
		CWnd* pWnd;

		//list: height variable, width variable, position constant
		pWnd=GetDlgItem(IDC_LIST_STATUS);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			0, 0, 
			(m_rcCtrl[ID_LIST].right-m_rcCtrl[ID_LIST].left)-dx, 
			(m_rcCtrl[ID_LIST].bottom-m_rcCtrl[ID_LIST].top)-dy,  
			SWP_NOZORDER|SWP_NOMOVE
			);

		pWnd=GetDlgItem(IDC_STATIC_STATUS);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_STATUS].left, 
			m_rcCtrl[ID_STATUS].top-dy,  
			m_rcCtrl[ID_STATUS].Width()-dx, 
			m_rcCtrl[ID_STATUS].Height(),  
			SWP_NOZORDER
			);
		pWnd=GetDlgItem(IDC_STATIC_DEDICATELIVE_LOGO);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_LOGO].left, 
			m_rcCtrl[ID_LOGO].top-dy,  
			m_rcCtrl[ID_LOGO].Width(), 
			m_rcCtrl[ID_LOGO].Height(),  
			SWP_NOZORDER|SWP_NOSIZE
			);
		pWnd=GetDlgItem(IDC_BUTTON_PAUSE);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_BNPAUSE].left-dx, 
			m_rcCtrl[ID_BNPAUSE].top-dy,  
			0,0,
			SWP_NOZORDER|SWP_NOSIZE
			);

		for (int i=0;i<DEDICATELIVEDLG_NUM_MOVING_CONTROLS;i++) 
		{
			switch(i)
			{
			case ID_LIST: nCtrlID=IDC_LIST_STATUS;break;
			case ID_STATUS: nCtrlID=IDC_STATIC_STATUS;break;
			case ID_BNPAUSE: nCtrlID=IDC_BUTTON_PAUSE;break;
			case ID_LOGO: nCtrlID=IDC_STATIC_DEDICATELIVE_LOGO;break;
			}
			GetDlgItem(nCtrlID)->Invalidate();
		}
	}	
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDedicateLiveDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

int  CDedicateLiveDlg::OnExit()
{

	CString szExit = _T("");
	szExit.Format("-------------- %s %s %s exit ---------------",g_showdata.szChannelName,g_settings.m_szShowName, DEDICATELIVE_VERSION );
	CString szLine = _T("");
	for(int l=0; l<szExit.GetLength(); l++) szLine+="-"; // make it pretty!
	
	g_md.DM(0,MD_FMT_UI_ICONINFO,0,
		"%s\n%s\n\n|OnExit",
		szExit,szLine
		); //(Dispatch message)

	SetTimer(DEDICATELIVE_KILL_TIMER_ID, 350, NULL);
	return 1;
}

void CDedicateLiveDlg::OnGetMinMaxInfo( MINMAXINFO FAR* lpMMI ) 
{
	lpMMI->ptMinTrackSize.x=DEDICATELIVEDLG_MINSIZEX;
	lpMMI->ptMinTrackSize.y=DEDICATELIVEDLG_MINSIZEY;
}

BOOL CDedicateLiveDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

// dont do this, the window is a child, has no titlebar
// this has been moved to the settings dialog.
/*
	
	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
*/
	// TODO: Add extra initialization here

	m_bUseSysDefaultColors = FALSE;  // if you want to specify dialog BG color (3d face color)
//	m_bUseSysDefaultColors = TRUE;  // if you want to use default sytsem dialog BG color (COLOR_3DFACE)

	// use the following to setup a nice background color for the dialog.
	// this is useful because in the GDM the background color can help to distinguish this show
	// status from others with different colors, at a glance.
//	m_crDefBG=RGB(235,128,108); //red
//	m_crDefBG=RGB(192,192,255); //blue
	m_crDefBG=RGB(156,190,255); //Dedicate Live blue
	
	m_brush.CreateSolidBrush(m_crDefBG);


	m_lce.SubclassDlgItem(IDC_LIST_STATUS, this);
	m_lce.SetHeight(16);
	m_lce.SetImages(IDB_BITMAP_STDICONS, 16, RGB(254,254,254)); 


	UINT nRegisterCode=MD_DESTINATION_REGISTERED;

	CString szParams=_T(""), szReport=_T("");

	// set up default MB title
	szParams.Format("%s", g_settings.m_szShowName);
	nRegisterCode = g_md.RegisterDestination(MD_DEST_UI_MB, MD_DESTTYPE_UI_MB,	szParams, (void*)(NULL));
	if (nRegisterCode != MD_DESTINATION_REGISTERED) {szReport.Format("failed to register MB Title\n code: 0x%02x", nRegisterCode); AfxMessageBox(szReport);}

	// set up logging list control
	szParams.Format("500|T-iM-F-S-D"); // if this changes, modify dest in OnButtonPause should match
	nRegisterCode = g_md.RegisterDestination(MD_DEST_UI_LIST1, MD_DESTTYPE_UI_LISTCTRLEX,	szParams, (void*)(&m_lce));
	if (nRegisterCode != MD_DESTINATION_REGISTERED) {szReport.Format("failed to register ListCtrlEx\n code: 0x%02x", nRegisterCode); AfxMessageBox(szReport);}

	// set up logfile
//	szParams.Format("%s-%s|YM",g_showdata.szChannelName, g_settings.m_szShowName );
	szParams.Format("Logfiles\\%s\\%s-%s|%s|%s",
		g_settings.m_szShowName, 
		g_showdata.szChannelName,
		g_settings.m_szShowName,	
		g_settings.m_szFileRotSpec,
		g_settings.m_szFileCustNameSpec
	 );

	nRegisterCode = g_md.RegisterDestination(MD_DEST_FILE_LOG1, MD_DESTTYPE_FILE_TEXT, szParams);
	if (nRegisterCode != MD_DESTINATION_REGISTERED) {szReport.Format("failed to register File\n code: 0x%02x", nRegisterCode); AfxMessageBox(szReport);}

	// set up e-mail group 1 -> make this general alert
	szParams.Format("%s|%s - %s|%s|%s - %s Alert",
		g_settings.m_szEmailTo,g_showdata.szChannelName,g_settings.m_szShowName , g_settings.m_szEmailIP,g_showdata.szChannelName,g_settings.m_szShowName );
	nRegisterCode = g_md.RegisterDestination(MD_DEST_EMAIL_GP1, MD_DESTTYPE_EMAIL, szParams);
//	if (nRegisterCode != MD_DESTINATION_REGISTERED) {szReport.Format("failed to register e-mail1\n code: 0x%02x", nRegisterCode); AfxMessageBox(szReport);}

	// set up e-mail group 2 -> make this just VDI
	szParams.Format("alerts@videodesigninteractive.com|%s - %s|%s|%s - %s Alert",
		g_showdata.szChannelName,g_settings.m_szShowName , g_settings.m_szEmailIP,g_showdata.szChannelName,g_settings.m_szShowName  );
	nRegisterCode = g_md.RegisterDestination(MD_DEST_EMAIL_GP2, MD_DESTTYPE_EMAIL, szParams);
//	if (nRegisterCode != MD_DESTINATION_REGISTERED) {szReport.Format("failed to register e-mail2\n code: 0x%02x", nRegisterCode); AfxMessageBox(szReport);}
	
	// set dlg box text!
	szParams = _T("");
	nRegisterCode = g_md.RegisterDestination(MD_DEST_UI_ITEM1, MD_DESTTYPE_UI_STATICTEXT, szParams, (void*)(GetDlgItem(IDC_STATIC_STATUS)));
//	if (nRegisterCode != MD_DESTINATION_REGISTERED) {szReport.Format("failed to register static text\n code: 0x%02x", nRegisterCode); AfxMessageBox(szReport);}
	
	szParams.Format("-------------- %s %s %s start --------------",g_showdata.szChannelName,g_settings.m_szShowName, DEDICATELIVE_VERSION );
	CString szLine = _T("");
	for(int l=0; l<szParams.GetLength(); l++) szLine+="-"; // make it pretty!
	
	g_md.DM(0,MD_FMT_UI_ICONINFO,0,
		"%s\n%s|OnInitDialog",
		szLine,szParams
		); //(Dispatch message)

	// Load all bitmaps, store handles
	CBitmap bmp;

  bmp.LoadBitmap(IDB_BITMAP_PAUSELIST);
	m_hbmp[BMP_PAUSE] = ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BITMAP_PLAYLIST);
	m_hbmp[BMP_PLAY] = ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));

	bmp.DeleteObject();

	// uncomment below to set up buttons
	((CButton*)GetDlgItem(IDC_BUTTON_PAUSE))->SetBitmap(m_hbmp[BMP_PAUSE]);

	// uncomment below for tooltips
	EnableToolTips(TRUE);

//	AfxMessageBox("oninit");

	// for text extents and estimation
	if(g_data.m_dc.CreateCompatibleDC(NULL)) g_data.m_bDCOK=TRUE;
	else g_data.m_bDCOK=FALSE;
		
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDedicateLiveDlg::OnButtonPause() 
{
	CString szParams = _T("");
	int nRegisterCode=0;
	if(m_bListPaused) 
	{
		szParams.Format("500|T-iM-F-S-D|0");  // if this changes, reg dest in OnInitDialog should match
		nRegisterCode = g_md.ModifyDestination(MD_DEST_UI_LIST1, MD_DESTTYPE_UI_LISTCTRLEX,	szParams, (void*)(&m_lce));
		if (nRegisterCode == MD_DESTINATION_REGISTERED) 
		{
			m_bListPaused=FALSE;
			((CButton*)GetDlgItem(IDC_BUTTON_PAUSE))->SetBitmap(m_hbmp[BMP_PAUSE]);
		}
	}
	else
	{
		szParams.Format("1000|T-iM-F-S-D|1");  // if this changes, reg dest in OnInitDialog should match
		nRegisterCode = g_md.ModifyDestination(MD_DEST_UI_LIST1, MD_DESTTYPE_UI_LISTCTRLEX,	szParams, (void*)(&m_lce));
		if (nRegisterCode == MD_DESTINATION_REGISTERED) 
		{
			m_bListPaused=TRUE;
			((CButton*)GetDlgItem(IDC_BUTTON_PAUSE))->SetBitmap(m_hbmp[BMP_PLAY]);
		}
	}
}

BOOL CDedicateLiveDlg::OnToolTipNotify( UINT id, NMHDR* pNMHDR, LRESULT* pResult )
{
//	AfxMessageBox("TTN");
	TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
	UINT nID =pNMHDR->idFrom;
	if (pTTT->uFlags & TTF_IDISHWND)
	{
		// idFrom is actually the HWND of the tool
		nID = ::GetDlgCtrlID((HWND)nID);
		if(nID)
		{
			switch(nID)
			{
			case IDC_BUTTON_PAUSE:
				{
					if(m_bListPaused) strcpy(pTTT->szText, "Resume automatic scrolling of status text.");
					else  strcpy(pTTT->szText, "Pause automatic scrolling of status text.");
				} break;
			case IDC_STATIC_STATUS:
				{
					sprintf(pTTT->szText, "Status of %s.", g_showdata.szShowName);
				} break;
			}
			return TRUE;
		}
	}
	return FALSE;
}

void CDedicateLiveDlg::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==DEDICATELIVE_KILL_TIMER_ID)
	{
		CDialog::OnCancel();
		KillTimer(DEDICATELIVE_KILL_TIMER_ID);
		g_pdlg=NULL;
	}
	else
	CDialog::OnTimer(nIDEvent);
}

HBRUSH CDedicateLiveDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	if(m_bUseSysDefaultColors)
	{
		HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
		return hbr;	
	}

// if using sepficed color, and one of these ctrls,
	if( 
			(nCtlColor==CTLCOLOR_DLG)
		||(nCtlColor==CTLCOLOR_STATIC)
//		||(nCtlColor==CTLCOLOR_BTN)
		)		
	{
		pDC->SetBkMode(TRANSPARENT);
		return (HBRUSH)m_brush.GetSafeHandle();
	}

// else just return the syscolor
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	return hbr;	
}
