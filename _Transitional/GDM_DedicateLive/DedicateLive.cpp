// DedicateLive.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "DedicateLive.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//CNetworking g_cnet;
CMessageDispatcher g_md;
CMessageDispatcher* g_pmd;

CDedicateLiveDlg *g_pdlg;
CDedicateLiveSettings g_settings;
CDedicateLiveData g_data;  // private data. DLL usage
CDedicateLiveThreadHelpers g_threadHelper; // thread data for server if needed (one per server)
CDedicateLiveCore g_core;  // engine class
ISMShowData_t g_showdata;  // data to expose to ISM/GDM
CNetworking g_net;

// proto for server thread.
extern UINT ServerThreadProcess(LPVOID pParam);
// proto for command thread.
extern UINT CommandThreadProcess(LPVOID pParam);

// ******************************************
// THINGS you have to do to get this project to compile correctly
// 1) Add to files: bmp.cpp, bmp.h, EmailHandler.cpp/.h, 
//    FileHandler.cpp/.h, ListCtrlEx.cpp/.h, MessageDispatcher.cpp/.h, MDdefines.h,
//    NAHandler.cpp/.h, networking.cpp/.h, sendmail.cpp/.h, TextUtils.cpp/.h,
//    UIHandler.cpp/.h
// 2) Modify Project Settings in the following manner:
//    A) in "Link" change the output filename extension to *.gpm (GDM Playout Module)

// ******************************************
// THINGS you have to do to get this project to have the right info!
// 1) Modify the versioning info in VS_VERSION_INFO in resource editor
// 2) Modify the AboutBox to have the right module name (do not change the format)
// 3) in CDedicateLiveApp::InitInstance(), you must set the default parameters for
//    what type of playout module it is, and what type of show it is.


// ******************************************
// There is a list of commented out includes in the "DedicateLive.h" file.  
// Generally, add includes to that file, as all files in the project include that file.


//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

/////////////////////////////////////////////////////////////////////////////
// CDedicateLiveApp

BEGIN_MESSAGE_MAP(CDedicateLiveApp, CWinApp)
	//{{AFX_MSG_MAP(CDedicateLiveApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDedicateLiveApp construction

CDedicateLiveApp::CDedicateLiveApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CDedicateLiveApp object

CDedicateLiveApp theApp;

BOOL CDedicateLiveApp::InitInstance() 
{
	SetRegistryKey("Video Design Interactive\\GDM Playout");
	g_pmd=&g_md;

  int MultAnte = GetProfileInt("Timing Settings", "MultiplierD", 120);
  int MultPost = GetProfileInt("Timing Settings", "MultiplierFractionalD", 0);
	g_data.m_dblTimingMultiplier_D=(double)MultAnte+((double)MultPost/1000000.0);

  MultAnte = GetProfileInt("Timing Settings", "MultiplierR1", 120);
  MultPost = GetProfileInt("Timing Settings", "MultiplierFractionalR1", 0);
	g_data.m_dblTimingMultiplier_R1=(double)MultAnte+((double)MultPost/1000000.0);

  MultAnte = GetProfileInt("Timing Settings", "MultiplierR2", 120);
  MultPost = GetProfileInt("Timing Settings", "MultiplierFractionalR2", 0);
	g_data.m_dblTimingMultiplier_R2=(double)MultAnte+((double)MultPost/1000000.0);
	
	g_settings.Settings(READ);

	for(int i=0; i<ISM_MAXDEVICEDLLS; i++)	g_showdata.pDevice[i]=NULL;
	// reasonable default for the ISM registered name.
	strcpy(g_showdata.szShowName, "DedicateLive"); // resettable in registry.
	// pick one.
//	g_showdata.nShowType = ISM_SHOWTYPE_ETERNAL; // ISM type: eternal or periodic.
	g_showdata.nShowType = ISM_SHOWTYPE_PERIODIC; // ISM type: eternal or periodic.

	g_data.SetShowState(ISM_SHOWDLL_STATE_DORMANT); // set the state before returning.

	return CWinApp::InitInstance();
}

int CDedicateLiveApp::ExitInstance() 
{
	
  WriteProfileInt("Timing Settings", "MultiplierD", (int)g_data.m_dblTimingMultiplier_D);
  WriteProfileInt("Timing Settings", "MultiplierFractionalD", 1000000*(g_data.m_dblTimingMultiplier_D-(double)((int)g_data.m_dblTimingMultiplier_D)) );
  WriteProfileInt("Timing Settings", "MultiplierR1", (int)g_data.m_dblTimingMultiplier_R1);
  WriteProfileInt("Timing Settings", "MultiplierFractionalR1", 1000000*(g_data.m_dblTimingMultiplier_R1-(double)((int)g_data.m_dblTimingMultiplier_R1)) );
  WriteProfileInt("Timing Settings", "MultiplierR2", (int)g_data.m_dblTimingMultiplier_R1);
  WriteProfileInt("Timing Settings", "MultiplierFractionalR2", 1000000*(g_data.m_dblTimingMultiplier_R2-(double)((int)g_data.m_dblTimingMultiplier_R2)) );

	if((g_data.m_bDialogStarted)&&(g_pdlg))	g_pdlg->OnExit();

//	Sleep(500);  // let msg threads finish
//			AfxMessageBox("done");
	return CWinApp::ExitInstance();
}

int CDedicateLiveApp::SetDevice(int nIndex)
{
	//GDM calls this after a comparison of supported device names
	// it sets the current index to the index of the single installed device.
	g_data.m_nCurrDeviceIndex = nIndex;
	CString szDevice=_T("[No device]");
	if((nIndex>GDM_IS_OWNER)&&(nIndex<ISM_MAXDEVICEDLLS)) // # of supported devices, not the single GDM device
		if(strlen(g_showdata.szDeviceName[nIndex])>0) szDevice.Format("%s", g_showdata.szDeviceName[nIndex]);

	g_md.DM(0,0,0, "The GDM device has been set to %s (device %d).|SetDevice",szDevice,nIndex);

//	AfxMessageBox(szDevice);
	return ISM_SHOWDLL_SETDEVICE_SUCCESS;
}

int  CDedicateLiveApp::InitShow()
{

	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nReturn = ISM_SHOWDLL_INIT_SUCCESS;

	g_settings.Settings(READ);

	// add any other initializations here
  int MultAnte = GetProfileInt("Timing Settings", "MultiplierD", 120);
  int MultPost = GetProfileInt("Timing Settings", "MultiplierFractionalD", 0);
	g_data.m_dblTimingMultiplier_D=(double)MultAnte+((double)MultPost/1000000.0);

  MultAnte = GetProfileInt("Timing Settings", "MultiplierR1", 120);
  MultPost = GetProfileInt("Timing Settings", "MultiplierFractionalR1", 0);
	g_data.m_dblTimingMultiplier_R1=(double)MultAnte+((double)MultPost/1000000.0);

  MultAnte = GetProfileInt("Timing Settings", "MultiplierR2", 120);
  MultPost = GetProfileInt("Timing Settings", "MultiplierFractionalR2", 0);
	g_data.m_dblTimingMultiplier_R2=(double)MultAnte+((double)MultPost/1000000.0);

/*
// uncomment for a server thread: if settings for the server change (listen port) you must shut down the thread and start a new one (done in CTextToAirSettings::OnOK() )
	if(!g_data.m_bServerStarted)
	{
		g_threadHelper.SetServerThreadCommand(ISMDLL_SHOWTHREAD_BEGIN);
		AfxBeginThread(ServerThreadProcess, &g_threadHelper);
	}
// uncomment for a command thread (that is not a CAL connection thread): 
//if settings for the server change (listen port) you must shut down the thread and start a new one (done in CTextToAirSettings::OnOK() )
	if(!g_data.m_bCommandThreadStarted)
	{
		g_threadHelper.SetCommandThreadCommand(ISMDLL_SHOWTHREAD_BEGIN);
		AfxBeginThread(CommandThreadProcess, &g_threadHelper);
	}

*/


	g_md.DM(0,0,0, "InitShow() called.|InitShow");


/*
	switch(g_data.m_nCurrDeviceIndex)
	{
	case -1: //no device
		{
			// deal with no device
			nReturn = ISM_SHOWDLL_INIT_ERROR;
		} break;
	case 0: //duet
		{
		  CDuet* pduet;       // is supported device 0 (in this sample)
			pduet = (CDuet*) g_showdata.pDevice[0];
			if(pduet) pduet->whatever()...

			...


		} break;
	case 1: //duetlex
		{
		  CDuetLex* pduetlex; // is supported device 1 (in this sample)
			pduetlex = (CDuetLex*) g_showdata.pDevice[1];
			if(pduetlex) pduetlex->whatever()...
	
			...


		} break;

	}

*/
	// if good
	{
		g_data.SetShowState(ISM_SHOWDLL_STATE_DORMANT); // set the state before returning.
		g_data.m_bShowInProgress = TRUE;
	}
	// else if bad
//  g_data.SetShowState(ISM_SHOWDLL_STATE_INIT|ISM_SHOWDLL_STATE_ERROR); // set the state before returning.

	return nReturn;
}

int  CDedicateLiveApp::EndShow()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nRV=ISM_SHOWDLL_END_SUCCESS;

	g_md.DM(0,0,0, "EndShow() called.|EndShow");

	
  WriteProfileInt("Timing Settings", "MultiplierD", (int)g_data.m_dblTimingMultiplier_D);
  WriteProfileInt("Timing Settings", "MultiplierFractionalD", 1000000*(g_data.m_dblTimingMultiplier_D-(double)((int)g_data.m_dblTimingMultiplier_D)) );
  WriteProfileInt("Timing Settings", "MultiplierR1", (int)g_data.m_dblTimingMultiplier_R1);
  WriteProfileInt("Timing Settings", "MultiplierFractionalR1", 1000000*(g_data.m_dblTimingMultiplier_R1-(double)((int)g_data.m_dblTimingMultiplier_R1)) );
  WriteProfileInt("Timing Settings", "MultiplierR2", (int)g_data.m_dblTimingMultiplier_R1);
  WriteProfileInt("Timing Settings", "MultiplierFractionalR2", 1000000*(g_data.m_dblTimingMultiplier_R2-(double)((int)g_data.m_dblTimingMultiplier_R2)) );


/*
	switch(g_data.m_nCurrDeviceIndex)
	{
	case -1: //no device
		{
			// deal with no device
			nReturn = ISM_SHOWDLL_INIT_ERROR;
		} break;
	case 0: //duet
		{
		  CDuet* pduet;       // is supported device 0 (in this sample)
			pduet = (CDuet*) g_showdata.pDevice[0];
			if(pduet) pduet->whatever()...

			...


		} break;
	case 1: //duetlex
		{
		  CDuetLex* pduetlex; // is supported device 1 (in this sample)
			pduetlex = (CDuetLex*) g_showdata.pDevice[1];
			if(pduetlex) pduetlex->whatever()...
	
			...


		} break;

	}

*/
	/*
	BOOL bServer=FALSE;
	BOOL bCommand=FALSE;

	if(g_data.m_bServerStarted)
	{
		g_data.m_pSvrExeThreadEvent->ResetEvent(); // sets it to non-signalled
		bServer=TRUE;
	}
	if(g_data.m_bCommandThreadStarted)
	{
		g_data.m_pCmdExeThreadEvent->ResetEvent(); // sets it to non-signalled
		bCommand=TRUE;
	}
	g_threadHelper.SetServerThreadCommand(ISMDLL_SHOWTHREAD_KILL);
	g_threadHelper.SetCommandThreadCommand(ISMDLL_SHOWTHREAD_KILL);

	// wait for threads to end so we end the shows right, no trailing threads.
	if(bServer) WaitForSingleObject(g_data.m_pSvrExeThreadEvent->m_hObject, ISM_THREAD_WAIT);
	if(bCommand) WaitForSingleObject(g_data.m_pCmdExeThreadEvent->m_hObject, ISM_THREAD_WAIT);
*/
	g_data.ClearData();  //reset for next show.

	// if good
  g_data.SetShowState(ISM_SHOWDLL_STATE_DORMANT); // set the state before returning.
	// else if bad
//  g_data.SetShowState(ISM_SHOWDLL_STATE_ENDSHOW|ISM_SHOWDLL_STATE_ERROR); // set the state before returning.
	return nRV;
}

int  CDedicateLiveApp::ShowSettings(void** ppSettings, UINT nType)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nRV = ISM_SHOWDLL_SETTINGS_SUCCESS; //success
	g_settings.Settings(READ); //read the settings from reg.
	switch(nType)
	{
	case ISM_SHOWDLL_SETTINGS_DOMODAL: nRV=g_settings.DoModal(); break;
	case ISM_SHOWDLL_SETTINGS_GETPTR:  
		{ 
			*ppSettings = &g_showdata; 
			if(*ppSettings==NULL) nRV = ISM_SHOWDLL_SETTINGS_ERROR;
		} break;
	case ISM_SHOWDLL_SETTINGS_RELEASEWND:
		// this is here because the window should not get created before the settings
		// have been exchanged, cross-set, and verified.  ISM will call after init settings gotten
		{ 
			if(!g_data.m_bDialogStarted)
			{
				g_pdlg = new CDedicateLiveDlg;
				if(g_pdlg->Create()) 
				{
					g_data.m_bDialogStarted = TRUE;
					if (ppSettings)
					{
						*ppSettings = &g_pdlg;
					}
					g_showdata.pWnd = g_pdlg;
					g_pdlg->ShowWindow(SW_SHOW);
					g_pdlg->ShowWindow(SW_HIDE);
					SetShowState(ISM_SHOWDLL_STATE_DORMANT); // set the state before returning.
					// uncomment for a CAL command thread: if settings for the server change (listen port) you must shut down the thread and start a new one (done in CTextToAirSettings::OnOK() )
					if(!g_data.m_bCommandThreadStarted)
					{
						g_threadHelper.SetCommandThreadCommand(ISMDLL_SHOWTHREAD_BEGIN);
						AfxBeginThread(CommandThreadProcess, &g_threadHelper);
					}
				}
				else
				{
					g_data.m_bDialogStarted = FALSE;
//					AfxMessageBox("Dlg creation failed");
				}
			}
		} break;
	case ISM_SHOWDLL_SETTINGS_DESTROYWND:
		// ISM will call destroy explicitly, to allow time for msg threads to finish before exiting
		{ 
			if(g_data.m_bDialogStarted)
			{

				BOOL bCommand=FALSE;

				if(g_data.m_bCommandThreadStarted)
				{
					g_data.m_pCmdExeThreadEvent->ResetEvent(); // sets it to non-signalled
					bCommand=TRUE;
				}
				g_threadHelper.SetCommandThreadCommand(ISMDLL_SHOWTHREAD_KILL);

				// wait for threads to end so we end the shows right, no trailing threads.
				if(bCommand) WaitForSingleObject(g_data.m_pCmdExeThreadEvent->m_hObject, ISM_THREAD_WAIT);

				g_data.m_bDialogStarted = FALSE;
				g_showdata.pWnd = NULL;
				if(g_pdlg) g_pdlg->OnExit();
			}
		} break;
	}
	return nRV;
}



int CDedicateLiveApp::ExecuteCommand(char pszDataBuffer[], UINT nSocket)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nReturn = GDM_SHOWDLL_CMDEXE_SUCCESS;
  CString szData; 
	szData.Format("%s",pszDataBuffer);
	szData.TrimLeft();szData.TrimRight(); 
	if((szData.Compare("ISM_SHOWDLL_STATE_DORMANT")==0)&&(nSocket==NULL))
	{
		// this is necessary to resume from aborts
	  g_data.SetShowState(ISM_SHOWDLL_STATE_DORMANT); // set it to ready
		return nReturn;
	}

	SOCKET socket = (SOCKET) nSocket;
	CString szReply=_T("");
	//responsibility for ACKing the request is now in this DLL, you must
	// ACK or NAK on the socket


//  ********* main command here. 
//  if OK, return GDM_SHOWDLL_CMDEXE_SUCCESS
//  if error, return GDM_SHOWDLL_CMDEXE_ERROR
//  if server commands expected, call ServerCommandHandler()

  CTextUtils tu;
  szData.Format("Command received.  Data: [%s]",pszDataBuffer);
  szData = tu.Encode(szData);
  szData+="|ExecuteCommand";
  g_md.DM(szData, 0,0,0 );


  if (g_data.m_nCurrDeviceIndex < 0)
  {
    // deal with no device
    g_md.DM(0,0,0,"No device! (device index=%d)", g_data.m_nCurrDeviceIndex);
    szReply.Format("%02x", GDM_REPLY_ERROR_SHOW_CMD_FAILED);
		g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, szReply, FALSE);
    return GDM_SHOWDLL_CMDEXE_ERROR;
  }
  
  //AfxBeginThread(ThreadProcess, (void *) pszDataBuffer);
  g_data.SetShowState(GDM_SHOWDLL_STATE_CMDEXE); // set it to executing now.

  szData.Format("%s",pszDataBuffer);
	g_threadHelper.QueueRequestForCommand(szData, socket);


/*
	switch(g_data.m_nCurrDeviceIndex)
	{
	case -1: //no device
		{
			// deal with no device
			nReturn = ISM_SHOWDLL_INIT_ERROR;
		} break;
	case 0: //duet
		{
		  CDuet* pduet;       // is supported device 0 (in this sample)
			pduet = (CDuet*) g_showdata.pDevice[0];
			if(pduet) pduet->whatever()...

			...


		} break;
	case 1: //duetlex
		{
		  CDuetLex* pduetlex; // is supported device 1 (in this sample)
			pduetlex = (CDuetLex*) g_showdata.pDevice[1];
			if(pduetlex) pduetlex->whatever()...
	
			...


		} break;

	}

*/
  //g_data.SetShowState(GDM_SHOWDLL_STATE_CMDEXE); // set the state before returning.
	return nReturn;
}



int CDedicateLiveApp::SuspendShow(BOOL bSuspend)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nReturn = ISM_SHOWDLL_SUSPEND_SUCCESS;
	if(bSuspend) // suspend the show
	{
		g_data.m_nShowStateBeforeSuspend=g_data.m_nShowState; // so you know what to resume to.

		// suspension body here
		// ********************
		
		nReturn = ISM_SHOWDLL_SUSPEND_SUCCESS;
	  g_data.SetShowState(ISM_SHOWDLL_STATE_SUSPENDED); // set the state before returning.
	}
	else  // un-suspend the show
	{

		// resume body here
		// ********************
		
		nReturn = ISM_SHOWDLL_SUSPEND_SUCCESS;  // same RV for resume, refers to func name, not action
	  g_data.SetShowState(g_data.m_nShowStateBeforeSuspend); // set the state back before returning.
	}


	return nReturn;
}
int CDedicateLiveApp::AbortShow()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nReturn = ISM_SHOWDLL_ABORT_SUCCESS;
	// abort may call EndShow to clean up and really should.

	g_md.DM(0,0,0, "AbortShow() called.|AbortShow");

// may want to call EndShow();
	EndShow();
/*
	switch(g_data.m_nCurrDeviceIndex)
	{
	case -1: //no device
		{
			// deal with no device
			nReturn = ISM_SHOWDLL_INIT_ERROR;
		} break;
	case 0: //duet
		{
		  CDuet* pduet;       // is supported device 0 (in this sample)
			pduet = (CDuet*) g_showdata.pDevice[0];
			if(pduet) pduet->whatever()...

			...


		} break;
	case 1: //duetlex
		{
		  CDuetLex* pduetlex; // is supported device 1 (in this sample)
			pduetlex = (CDuetLex*) g_showdata.pDevice[1];
			if(pduetlex) pduetlex->whatever()...
	
			...


		} break;

	}

*/

	// if good
  g_data.SetShowState(ISM_SHOWDLL_STATE_SUSPENDED); // set the state before returning.
	// else if bad
//  g_data.SetShowState(ISM_SHOWDLL_STATE_ABORTSHOW|ISM_SHOWDLL_STATE_ERROR); // set the state before returning.
	return nReturn;
}

int CDedicateLiveApp::GetShowState()
{
// this function is called by the ISM to determine what function the DLL
//	wants it to call. This is how the show can control the sequence of events
//  The state is set at the end of each exported control function.
//  The ISM then calls this function to understand what is the next control function to call.
	
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
/*
// show states (return values to GetState)
#define ISM_SHOWDLL_STATE_MISSINGTRIGDEV	0x10000000
#define ISM_SHOWDLL_STATE_MISSINGCTRLDEV	0x20000000
#define ISM_SHOWDLL_STATE_DORMANT     		0x00000000
#define ISM_SHOWDLL_STATE_INIT		     		0x00100000
#define ISM_SHOWDLL_STATE_CHECKING     		0x00000010
#define ISM_SHOWDLL_STATE_INITSHOW     		0x00000020
#define ISM_SHOWDLL_STATE_PRESHOW     		0x00000040
#define ISM_SHOWDLL_STATE_DURINGSHOW     	0x00000080
#define ISM_SHOWDLL_STATE_POSTSHOW     		0x00000100
#define ISM_SHOWDLL_STATE_SUSPENDEDSHOW   0x00000200
#define ISM_SHOWDLL_STATE_ENDSHOW     		0x00000400
#define ISM_SHOWDLL_STATE_ABORTSHOW     	0x00000800
#define ISM_SHOWDLL_STATE_SUSPENDED	     	0x00001000 // not calling suspended control
#define ISM_SHOWDLL_STATE_PREEVENT     		0x00002000 // transient state inside control 
#define ISM_SHOWDLL_STATE_POSTEVENT     	0x00004000 // transient state inside control 
*/

	return g_data.m_nShowState;
}

void CDedicateLiveApp::ServerCommandHandler() 
{
	// should be called by control functions to handle cmds coming in off the (optional) server
	// if data needs to be passed back to the control, simply set members of the global g_data object and return
	SOCKET socket;
	BYTE command;
	CString szData;

	// pull command keeps up the heartbeat.
  while (g_threadHelper.PullRequestFromServerQueue(&socket, &command, &szData))
	{
		CString szTemp,szReply;
		switch(command)
		{
		case 0x01:				//0x01
			{

			} break;
		case 0x02:			//0x02
			{

			} break;
		}
	}
}

// a function just for convenience
void CDedicateLiveApp::SetShowState(UINT nState)
{
	g_data.SetShowState(nState);
}

int	CDedicateLiveApp::GetShowStatus(void* pszStatusText, void* pszToolTipText, int* pnLEDState)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int nRV = ISM_DEVDLL_GETSTATUS_SUCCESS; //success
	g_data.m_criticalStatus.Lock();

	if(pszStatusText) strcpy((char*)pszStatusText, (LPCTSTR)g_data.m_szStatusText.Left(80));
	else nRV = ISM_DEVDLL_GETSTATUS_ERROR; //error
	if(pszToolTipText) strcpy((char*)pszToolTipText, (LPCTSTR)g_data.m_szToolTipText.Left(80));
	else nRV = ISM_DEVDLL_GETSTATUS_ERROR; //error
	if(pnLEDState) *pnLEDState = g_data.m_nLEDState;
	else nRV = ISM_DEVDLL_GETSTATUS_ERROR; //error
	
	g_data.m_criticalStatus.Unlock();

	return nRV;
}

