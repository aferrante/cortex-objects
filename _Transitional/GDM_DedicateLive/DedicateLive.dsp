# Microsoft Developer Studio Project File - Name="DedicateLive" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 5.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=DedicateLive - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "DedicateLive.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "DedicateLive.mak" CFG="DedicateLive - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "DedicateLive - Win32 Release" (based on\
 "Win32 (x86) Dynamic-Link Library")
!MESSAGE "DedicateLive - Win32 Debug" (based on\
 "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "DedicateLive - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_USRDLL" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 /nologo /subsystem:windows /dll /machine:I386 /out:"Release/DedicateLive.gpm"

!ELSEIF  "$(CFG)" == "DedicateLive - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_USRDLL" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /dll /debug /machine:I386 /out:"Debug/DedicateLive.gpm" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "DedicateLive - Win32 Release"
# Name "DedicateLive - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE="..\..\..\Shared\imaging\bmp helpers\bmp.cpp"
# End Source File
# Begin Source File

SOURCE=.\DedicateLive.cpp
# End Source File
# Begin Source File

SOURCE=.\DedicateLive.def
# End Source File
# Begin Source File

SOURCE=.\DedicateLive.rc

!IF  "$(CFG)" == "DedicateLive - Win32 Release"

!ELSEIF  "$(CFG)" == "DedicateLive - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\DedicateLiveCore.cpp
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveData.cpp
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveSettings.cpp
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveThreads.cpp
# End Source File
# Begin Source File

SOURCE=.\DLImportExportDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\Duet\duet.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\MessageDispatcher\EmailHandler\EmailHandler.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\MessageDispatcher\FileHandler\FileHandler.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\ListCtrlEx\ListCtrlEx.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\MessageDispatcher\MessageDispatcher.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\MessageDispatcher\NAHandler\NAHandler.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\Networking\networking.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\Email\SendMail\sendmail.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\TextUtils\TextUtils.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\MessageDispatcher\UIHandler\UIHandler.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE="..\..\..\Shared\imaging\bmp helpers\bmp.h"
# End Source File
# Begin Source File

SOURCE=.\DedicateLive.h
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveCore.h
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveData.h
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveDefines.h
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveDlg.h
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveSettings.h
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveThreads.h
# End Source File
# Begin Source File

SOURCE=.\DLImportExportDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\Duet\duet.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\MessageDispatcher\EmailHandler\EmailHandler.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\MessageDispatcher\FileHandler\FileHandler.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\ListCtrlEx\ListCtrlEx.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\MessageDispatcher\MDdefines.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\MessageDispatcher\MessageDispatcher.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\MessageDispatcher\NAHandler\NAHandler.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\Networking\networking.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\Email\SendMail\sendmail.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\TextUtils\TextUtils.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Shared\MessageDispatcher\UIHandler\UIHandler.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\DedicateLive.rc2
# End Source File
# Begin Source File

SOURCE=.\res\logo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\pauselist.bmp
# End Source File
# Begin Source File

SOURCE=.\res\playlist.bmp
# End Source File
# Begin Source File

SOURCE=.\res\settings.bmp
# End Source File
# Begin Source File

SOURCE=".\res\Status-Blu.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Status-Grn.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Status-Red.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Status-Yel.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\stdicons.bmp
# End Source File
# Begin Source File

SOURCE=.\res\vdi.bmp
# End Source File
# Begin Source File

SOURCE=.\res\vdi.ico
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
