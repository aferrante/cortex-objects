#include "stdafx.h"
#include "DedicateLive.h"
#include "DedicateLiveThreads.h"
#include "DedicateLiveDefines.h"

//  necessary global externs
extern CDedicateLiveSettings g_settings;
extern CDedicateLiveData g_data;
extern CDedicateLiveApp theApp;
extern CDedicateLiveThreadHelpers g_threadHelper;
extern CDedicateLiveCore g_core;  // engine class
extern CMessageDispatcher g_md;
extern ISMShowData_t g_showdata;
extern CNetworking g_net;

//non extern globals
CDuetAPI* g_pduet = NULL;
BOOL g_bUseDuet = TRUE, g_bConnected = FALSE;

// proto for server thread.
UINT ServerThreadProcess(LPVOID pParam);
// proto for command thread.
UINT CommandThreadProcess(LPVOID pParam);
//proto for wait thread
UINT WaitThreadProcess(LPVOID pParam);

// protos
void clearRegions();
void __cdecl OnUserEvent(long connId, long eventId, long data);
/*
void clearFontsAndColors();
void clearScreen();
*/
CString getRelativePath();

  // Imaging DLL function pointers
  typedef BOOL (CALLBACK* LPFNDLLFUNC_IMAGING_CVT_JPEG)(CString szFilename, CString szTempBMPName, int *width, int *height);
  typedef BOOL (CALLBACK* LPFNDLLFUNC_IMAGING_CVT_GIF)(CString localPath, CString localFile, CString tempPath, CString tempFile, int *frameCount, int *width, int *height);
  typedef BOOL (CALLBACK* LPFNDLLFUNC_IMAGING_WRT_BMP)(CString szFilename, unsigned char *where,int width, int height);

  HINSTANCE g_imagingDLL=NULL;
  LPFNDLLFUNC_IMAGING_CVT_JPEG g_fpImagingConvertJPEG;
  LPFNDLLFUNC_IMAGING_CVT_GIF g_fpImagingConvertGIF;
  LPFNDLLFUNC_IMAGING_WRT_BMP g_fpImagingWriteBMP;


/////////////////////////////////////////////////
//  ThreadHelpers Class

CDedicateLiveThreadHelpers::CDedicateLiveThreadHelpers()
{
	m_nServerThreadCommand=ISMDLL_SHOWTHREAD_DONOTKILL;
	m_nCommandThreadCommand=ISMDLL_SHOWTHREAD_DONOTKILL;

	for(int g=0; g<ISMDLL_MAX_QUEUED_COMMANDS; g++)
	{
		m_socket[g]=NULL;
		m_cmd[g]=0;
		m_szData[g]=_T("");
	
		m_socketIn[g]=0;
		m_cmdIn[g]=0;
		m_szDataIn[g]=_T("");

		m_cmdOut[g]=0;
		m_szDataOut[g]=_T("");
	}

	m_bCommandThreadStarted=FALSE;
	m_bServerStarted=FALSE;

	m_nNumQueuedCommandsIn=0;
	m_nNumQueuedCommandsOut=0;
	m_nNumQueuedCommands=0;
	m_nServerPort=10999;

	m_nNumRetries=3;
}

CDedicateLiveThreadHelpers::~CDedicateLiveThreadHelpers()
{
}

BOOL CDedicateLiveThreadHelpers::IsServerThreadToEnd()
{
	BOOL bRet = TRUE;
	UINT nTick= GetTickCount();
	m_criticalServerCmdData.Lock();
	m_nServerHeartbeat=GetTickCount(); // server's heart beat!

	if(nTick-m_nLastHeartbeat>ISMDLL_THREAD_KILL_TIMEOUT) 
	{
		m_nNumRetries++; // Show's heartbeat.
		m_nLastHeartbeat=nTick; // reset for retry
	}

	if(m_nNumRetries>=ISMDLL_THREAD_KILL_NUM_RETRIES)
	{
		m_criticalServerCmdData.Unlock();
		return TRUE; 
	}
	m_criticalServerCmdData.Unlock();

	m_criticalServerThreadCmd.Lock();
	bRet=(m_nServerThreadCommand&ISMDLL_SHOWTHREAD_KILL);
	m_criticalServerThreadCmd.Unlock();
	return bRet;
}

BOOL CDedicateLiveThreadHelpers::SetServerThreadCommand(UINT nCmd)
{
	m_criticalServerThreadCmd.Lock();
	m_nServerThreadCommand=nCmd;
	m_criticalServerThreadCmd.Unlock();
	return TRUE;
}


BOOL CDedicateLiveThreadHelpers::QueueRequestFromServer(SOCKET s, BYTE Request, CString szData)
{
	m_criticalServerCmdData.Lock();
  if (m_nNumQueuedCommands<ISMDLL_MAX_QUEUED_COMMANDS)
  {
    m_socket[m_nNumQueuedCommands] = s;
    m_cmd[m_nNumQueuedCommands] = Request;
    if(!szData.IsEmpty()) m_szData[m_nNumQueuedCommands] = szData;
		else  m_szData[m_nNumQueuedCommands] = _T("");
    m_nNumQueuedCommands++;
  }
	m_criticalServerCmdData.Unlock();
	return TRUE;
}

BOOL CDedicateLiveThreadHelpers::PullRequestFromServerQueue(SOCKET *s, BYTE *Request, CString *szData)
{
	BOOL bRet = FALSE;
	m_criticalServerCmdData.Lock();
	m_nLastHeartbeat=GetTickCount();
	m_nNumRetries=0;

  if (m_nNumQueuedCommands > 0)
  {
	  int i;
	  *s = m_socket[0];
    *Request = m_cmd[0];
		*szData=m_szData[0];
//		g_cLog.logErrors("command pulled from Q, about to reshuffle",  FALSE, FALSE);

    for (i=0; i<(m_nNumQueuedCommands - 1); i++)
    {
      m_socket[i] = m_socket[i+1];
      m_cmd[i]		=	m_cmd[i+1];
      m_szData[i] = m_szData[i+1];
    }
//		g_cLog.logErrors("reshuffled",  FALSE, FALSE);
    m_nNumQueuedCommands--;
    bRet = TRUE;
  }
	else
	{
	  *s = NULL;
    *Request = 0;
		*szData=_T("");
		bRet = FALSE;
	}
	m_criticalServerCmdData.Unlock();
	return bRet;
}


//command
BOOL CDedicateLiveThreadHelpers::IsCommandThreadToEnd()
{
	BOOL bRet = TRUE;
	m_criticalCommandThreadCmd.Lock();
	bRet=(m_nCommandThreadCommand&ISMDLL_SHOWTHREAD_KILL);
	m_criticalCommandThreadCmd.Unlock();
	return bRet;
}

BOOL CDedicateLiveThreadHelpers::SetCommandThreadCommand(UINT nCmd)
{
	m_criticalCommandThreadCmd.Lock();
	m_nCommandThreadCommand=nCmd;
	m_criticalCommandThreadCmd.Unlock();
	return TRUE;
}

// to command the command thread for CAL
BOOL CDedicateLiveThreadHelpers::QueueRequestForCommand(CString szData, SOCKET socket)
{
	m_criticalCommandCmdData.Lock();
  if (m_nNumQueuedCommandsIn<ISMDLL_MAX_QUEUED_COMMANDS)
  {
    if(!szData.IsEmpty()) m_szDataIn[m_nNumQueuedCommandsIn] = szData;
		else  m_szDataIn[m_nNumQueuedCommandsIn] = _T("");
		m_socketIn[m_nNumQueuedCommandsIn] = socket;
    m_nNumQueuedCommandsIn++;
  }
	m_criticalCommandCmdData.Unlock();
	return TRUE;
}

BOOL CDedicateLiveThreadHelpers::PullRequestFromCommandQueueIntoThread(CString* szData,  SOCKET* socket)
{
	BOOL bRet = FALSE;
	m_criticalCommandCmdData.Lock();

  if (m_nNumQueuedCommandsIn > 0)
  {
	  int i;
		*szData = m_szDataIn[0];
		*socket = m_socketIn[0];

    // g_cLog.logErrors("command pulled from Q, about to reshuffle",  FALSE, FALSE);

    for (i=0; i<(m_nNumQueuedCommandsIn - 1); i++)
    {
      m_szDataIn[i] = m_szDataIn[i+1];
      m_socketIn[i] = m_socketIn[i+1];
    }
    // g_cLog.logErrors("reshuffled",  FALSE, FALSE);
    m_nNumQueuedCommandsIn--;
    bRet = TRUE;
  }
	else
	{
		*szData=_T("");
		*socket=NULL;
		bRet = FALSE;
	}
	m_criticalCommandCmdData.Unlock();
	return bRet;
}

/*
// to command the command thread for generic
BOOL CDedicateLiveThreadHelpers::QueueRequestForCommand(BYTE Request, CString szData)
{
	m_criticalCommandCmdData.Lock();
  if (m_nNumQueuedCommandsIn<ISMDLL_MAX_QUEUED_COMMANDS)
  {
    m_cmdIn[m_nNumQueuedCommandsIn] = Request;
    if(!szData.IsEmpty()) m_szDataIn[m_nNumQueuedCommandsIn] = szData;
		else  m_szDataIn[m_nNumQueuedCommandsIn] = _T("");
    m_nNumQueuedCommandsIn++;
  }
	m_criticalCommandCmdData.Unlock();
	return TRUE;
}

BOOL CDedicateLiveThreadHelpers::PullRequestFromCommandQueueIntoThread(BYTE* Request, CString* szData)
{
	BOOL bRet = FALSE;
	m_criticalCommandCmdData.Lock();

  if (m_nNumQueuedCommandsIn > 0)
  {
	  int i;
    *Request = m_cmdIn[0];
		*szData=m_szDataIn[0];
//		g_cLog.logErrors("command pulled from Q, about to reshuffle",  FALSE, FALSE);

    for (i=0; i<(m_nNumQueuedCommandsIn - 1); i++)
    {
      m_cmdIn[i]		=	m_cmdIn[i+1];
      m_szDataIn[i] = m_szDataIn[i+1];
    }
//		g_cLog.logErrors("reshuffled",  FALSE, FALSE);
    m_nNumQueuedCommandsIn--;
    bRet = TRUE;
  }
	else
	{
    *Request = 0;
		*szData=_T("");
		bRet = FALSE;
	}
	m_criticalCommandCmdData.Unlock();
	return bRet;
}
*/

// to get commands from command thread
BOOL CDedicateLiveThreadHelpers::QueueRequestFromCommand(BYTE Request, CString szData)
{
	m_criticalCommandCmdData.Lock();
  if (m_nNumQueuedCommandsOut<ISMDLL_MAX_QUEUED_COMMANDS)
  {
    m_cmdOut[m_nNumQueuedCommandsOut] = Request;
    if(!szData.IsEmpty()) m_szDataOut[m_nNumQueuedCommandsOut] = szData;
		else  m_szDataOut[m_nNumQueuedCommandsOut] = _T("");
    m_nNumQueuedCommandsOut++;
  }
	m_criticalCommandCmdData.Unlock();
	return TRUE;
}

BOOL CDedicateLiveThreadHelpers::PullRequestFromCommandQueueOutOfThread(BYTE *Request, CString *szData)
{
	BOOL bRet = FALSE;
	m_criticalCommandCmdData.Lock();

  if (m_nNumQueuedCommandsOut > 0)
  {
	  int i;
    *Request = m_cmdOut[0];
		*szData=m_szDataOut[0];
//		g_cLog.logErrors("command pulled from Q, about to reshuffle",  FALSE, FALSE);

    for (i=0; i<(m_nNumQueuedCommandsOut - 1); i++)
    {
      m_cmdOut[i]		=	m_cmdOut[i+1];
      m_szDataOut[i] = m_szDataOut[i+1];
    }
//		g_cLog.logErrors("reshuffled",  FALSE, FALSE);
    m_nNumQueuedCommandsOut--;
    bRet = TRUE;
  }
	else
	{
    *Request = 0;
		*szData=_T("");
		bRet = FALSE;
	}
	m_criticalCommandCmdData.Unlock();
	return bRet;
}



/////////////////////////////////////////////////
//  Threads


UINT ServerThreadProcess(LPVOID pParam)
{
	CDedicateLiveThreadHelpers* pThread = (CDedicateLiveThreadHelpers*) pParam;

	CNetworking net;
	net.StartNetworking();
	SOCKET s;
	BYTE Request;
	CString szData=_T("");

	pThread->m_criticalServerCmdData.Lock();

	pThread->m_nLastHeartbeat=GetTickCount();
	pThread->m_nNumRetries=0;
	pThread->m_bServerStarted=FALSE;
	g_data.m_bServerStarted=FALSE;

	if(net.StartServer(pThread->m_nServerPort)!=RETURN_SUCCESS) 
	{
		// may want to add e-mail to this!
		//MD_DEST_EMAIL_GP1|MD_DEST_EMAIL_GP2  // gp 2 is private for VDI
		g_md.DM(MD_DEST_UI_MB,MD_FMT_UI_MB_ALWAYSONTOP|MD_FMT_UI_ICONERROR,MD_PRI_CRITICAL,
			"StartServer() failed! Cannot accept server commands...|ServerThreadProcess||ff0000"); //(Dispatch message)
		pThread->m_nServerHeartbeat=GetTickCount();  // will assert main GDM after timeout.
		pThread->m_bServerStarted=FALSE;
		return ISMDLL_SERVERINIT_FAILED;
	}
	else 		
	{
		g_md.DM(0,0,0, "Server and server thread started.|ServerThreadProcess"); //(Dispatch message)
		pThread->m_bServerStarted=TRUE;
		g_data.m_bServerStarted=TRUE;
	}

	pThread->m_criticalServerCmdData.Unlock();


	while(!pThread->IsServerThreadToEnd())
	{
		//listen for any commands:
		if (net.ServerListener(pThread->m_nServerPort, &s, &Request, &szData)>=RETURN_SUCCESS)
			pThread->QueueRequestFromServer(s, Request, szData);

		//  keep heartbeat going
		pThread->m_nLastHeartbeat=GetTickCount();
		Sleep(30);
	}// thread ending

	net.ShutdownServer(pThread->m_nServerPort);
	pThread->m_criticalServerCmdData.Lock();
	pThread->m_bServerStarted=FALSE;
	pThread->m_criticalServerCmdData.Unlock();

	g_data.m_bServerStarted=FALSE;

	net.ShutdownNetworking();
	g_md.DM(0,0,0, "Server thread ending.|ServerThreadProcess"); //(Dispatch message)

  g_data.m_pSvrExeThreadEvent->SetEvent(); // sets it to signalled

	return 0;

}

/*
// dont use this one for a CAL command thread - see below 
// (if you use the other one, comment this one out)
UINT CommandThreadProcess(LPVOID pParam)
{
	CDedicateLiveThreadHelpers* pThread = (CDedicateLiveThreadHelpers*) pParam;

//	CNetworking net;
//	net.startNetworking();
	BYTE Request;
	CString szData=_T("");
	pThread->m_criticalCommandCmdData.Lock();
	pThread->m_bCommandThreadStarted=TRUE;
	pThread->m_criticalCommandCmdData.Unlock();
	g_data.m_bCommandThreadStarted=TRUE;
	g_md.DM(0,0,0, "Command thread started.|CommandThreadProcess"); //(Dispatch message)

	while(!pThread->IsCommandThreadToEnd())
	{
		//listen for any commands:
		while (pThread->PullRequestFromCommandQueueIntoThread(&Request, &szData))
		{
			//do something with the data in Request and szData

			// possibly send a command somewhere (maybe to the server thread, or to the device -
			// wherever it is has to call g_threadHelper.PullRequestFromCommandQueueOutOfThread() to
			// get the commands out of the thread).
			// the following is (sample only)
			pThread->QueueRequestFromCommand(Request, szData);
		}

		Sleep(5);
	}// thread ending

	pThread->m_criticalCommandCmdData.Lock();
	pThread->m_bCommandThreadStarted=FALSE;
	pThread->m_criticalCommandCmdData.Unlock();

//	net.shutdownNetworking();

	g_data.m_bCommandThreadStarted=FALSE;
	g_md.DM(0,0,0, "Command thread ending.|CommandThreadProcess"); //(Dispatch message)

		// let the app object know it can go now.
  g_data.m_pCmdExeThreadEvent->SetEvent(); // sets it to signalled

	return 0;

}

*/

// use this one for a CAL command thread (plus helpers below)
UINT CommandThreadProcess(LPVOID pParam)
{
	CDedicateLiveThreadHelpers* pThread = (CDedicateLiveThreadHelpers*) pParam;

	CString szData=_T(""),szReply=_T("");
	SOCKET socket;
	CTextUtils tu;
  int nReturn;

  pThread->m_criticalCommandCmdData.Lock();
	pThread->m_bCommandThreadStarted=TRUE;
	pThread->m_criticalCommandCmdData.Unlock();
	g_data.m_bCommandThreadStarted=TRUE;
	g_md.DM(0,0,0, "Command thread started.|CommandThreadProcess"); //(Dispatch message)

	while(!pThread->IsCommandThreadToEnd())
	{
		//listen for any commands from ExecuteCommand:
		while (pThread->PullRequestFromCommandQueueIntoThread(&szData, &socket))
		{
			CString szOut[10];
      int nItems = tu.breakStringOnSepChar('|', szData, szOut, 10);
			if ((nItems <2) || (nItems >3))
			{
				// not enough args to command
				//AfxMessageBox("args != 3");
      	g_md.DM(0,0,0, "Error: Command received with %d args: %s|CommandThreadProcess", nItems, szData); //(Dispatch message)
		    szReply.Format("%02x", GDM_REPLY_ERROR_SHOW_CMD_FAILED);
				g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, szReply, FALSE);
        break;
      }

			// szOut[0] is the show name
			// szOut[1] is the command byte
			// szOut[2] is the (perhaps empty) additional data

			//CString szTemp;
			//szTemp.Format("Command: %d %s", tu.ReturnHexByte(szOut[1]), szOut[1]);
			//AfxMessageBox(szTemp); 

			nReturn = GDM_SHOWDLL_CMDEXE_SUCCESS;

			//CString szTemp;
			//szTemp.Format("in: '%s' byte = %x, params='%s'", 
		  //		szData, tu.ReturnHexByte(szOut[1]), szOut[2]);
			//AfxMessageBox(szTemp);
 
      szOut[2] = tu.Decode(szOut[2]);

			// the following ACK is provided so that you can just ACK for all commands
			// but if you need to return specific data with each command,
			// you have to out the ACK in each case statment below, and
			// build szReply as appropriate.
			// the reply string should be of the form:
	    //   szReply.Format("%02x|%ld", SHOWNAME_CMD_ACK, nSomeDataValue);
			// where the first two bytes of the reply string constitute the reply CMD 
			// for the individual command (the SHOWNAME_CMD_ACK can be = GDM_CMD_ACK.
			// This way, a call: g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, szReply, FALSE);
			// sends a GDM_CMD_ACK to indicate the GDM has recieved the command properly,
			// and may send a GDM_CMD_ACK or SHOWNAME_CMD_ACK if defined in the szReply
			// to indicate that the command itself processed fine.
			// Other values such as SHOWNAME_CMD_NAK or SHOWNAME_CMD_NOTABLE can be defined and used

//		  szReply.Format("%02x", GDM_CMD_ACK);
//			g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, szReply, FALSE);

      CDuetAPI* pduet = (CDuetAPI*) g_showdata.pDevice[0]; 
      if (pduet)  
      {
        g_pduet = pduet;
//	g_md.DM(0,MD_FMT_UI_ICONEXCLAMATION,0,"Q|ExecuteCommand");

				BYTE byteCommand = tu.ReturnHexByte(szOut[1]);
				switch (byteCommand)
				{

				////////////////////////////////////////////////////////////////////
				//  Common commands
      
				case CMD_CONNECT:

					switch(g_data.m_nCurrDeviceIndex)
					{
					case 0: //duet
						g_pduet = (CDuetAPI*) g_showdata.pDevice[0]; 
						if (g_pduet)
						{
							if(g_bConnected)
							{
								g_md.DM("Connected to CAL."); 
 								g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE); // ack immediately, then be busy
							}
							else
							{
								//g_md.DM("pduet != NULL.  About to call Connect()|ExecuteCommand",0,MD_FMT_UI_ICONINFO,0); //(Dispatch message)
								CString szInfo;
								if (g_pduet->connect("127.0.0.1", g_bUseDuet, ecalFormatNTSC, &szInfo))
								{
									g_bConnected = TRUE;
									g_md.DM("Connection to CAL successful."); 
 						g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE); // ack immediately, then be busy
								}
								else
								{
									g_bConnected = FALSE;
									g_md.DM(0,MD_FMT_UI_ICONERROR,0, "Connection to CAL unsuccessful: %s", szInfo); 
 						g_net.ServerSendCmdReply(socket, GDM_CMD_NAK, FALSE); // ack immediately, then be busy

									if(pduet->shutdown(&szInfo))
										g_md.DM("Shutdown CAL successful."); 
									else
										g_md.DM(0,MD_FMT_UI_ICONERROR,0, "Shutdown CAL unsuccessful: %s", szInfo); 

								}
							}
						}
						else 
						{
							g_md.DM("pduet was null!|ExecuteCommand",0,MD_FMT_UI_ICONERROR,0); //(Dispatch message)
							g_bConnected = FALSE;
						}
						break;

				//	case 1: //duetlex
				//		CDuetLex* pduetlex; // is supported device 1 (in this sample)
				//		pduetlex = (CDuetLex*) g_showdata.pDevice[1];
				//		if(pduetlex) pduetlex->whatever()...
				//			break; 
							
					}

					// grab the DLL
					if (g_imagingDLL == NULL) 
					{
						g_imagingDLL = AfxLoadLibrary("ImagingDLL.dll");
						if (!g_imagingDLL) 	g_md.DM("Could not load Imaging DLL");
						else
						{
							g_fpImagingConvertJPEG = (LPFNDLLFUNC_IMAGING_CVT_JPEG)GetProcAddress(g_imagingDLL, "ConvertJPEG");
							if (!g_fpImagingConvertJPEG) 	g_md.DM("Could not attach function: ConvertJPEG");
							else
							{
								g_fpImagingConvertGIF = (LPFNDLLFUNC_IMAGING_CVT_GIF)GetProcAddress(g_imagingDLL, "ConvertGIFAnimation");
								if (!g_fpImagingConvertGIF) 	g_md.DM("Could not attach function: ConvertGIFAnimation");
								else
								{
									g_fpImagingWriteBMP = (LPFNDLLFUNC_IMAGING_WRT_BMP)GetProcAddress(g_imagingDLL, "WriteBMP24");
									if (!g_fpImagingWriteBMP) 	g_md.DM("Could not attach function: WriteBMP24");
									else
									{
										// successful!
										;
									}
								}
							}
						}
					}
					break;

				case CMD_DISCONNECT:
 				  g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE);
					switch(g_data.m_nCurrDeviceIndex)
					{
					case 0: //duet
						if (g_bConnected)
						{
							CDuetAPI* pduet = (CDuetAPI*) g_showdata.pDevice[0]; 
							if (pduet)
							{
								clearRegions();
								CString szInfo;
								if(	pduet->disconnect(&szInfo))
									g_md.DM("Shutdown CAL successful."); 
								else
									g_md.DM(0,MD_FMT_UI_ICONERROR,0, "Disconnection from CAL unsuccessful: %s", szInfo); 

								g_bConnected = FALSE;
								g_data.m_bGraphicsSetup=FALSE;
							}
							break;
  						/* case 1: //duetlex
							CDuetLex* pduetlex; // is supported device 1 (in this sample)
							pduetlex = (CDuetLex*) g_showdata.pDevice[1];
							if(pduetlex) pduetlex->whatever()...
								break;
								*/
						}
						else g_md.DM("Not connected!");
					}

					break;

				case CMD_SHUTDOWN:
 				  g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE);
					switch(g_data.m_nCurrDeviceIndex)
					{
					case 0: //duet
						CDuetAPI* pduet = (CDuetAPI*) g_showdata.pDevice[0]; 
						if (pduet)
						{
							if (g_bConnected)
							{
								clearRegions();
								pduet->disconnect();
								g_data.m_bGraphicsSetup=FALSE;
								g_bConnected = FALSE;
							}
							CString szInfo;
							if(pduet->shutdown(&szInfo))
								g_md.DM("Shutdown CAL successful."); 
							else
								g_md.DM(0,MD_FMT_UI_ICONERROR,0, "Shutdown CAL unsuccessful: %s", szInfo); 
						}
						break;
					/* case 1: //duetlex
						CDuetLex* pduetlex; // is supported device 1 (in this sample)
						pduetlex = (CDuetLex*) g_showdata.pDevice[1];
						if(pduetlex) pduetlex->whatever()...
							break;
							*/
					}
					// get rid of the Imaging DLL
					if (g_imagingDLL) { AfxFreeLibrary(g_imagingDLL); g_imagingDLL=NULL; }
					break;
				case CMD_USEVGA:
 				  g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE);
					g_bUseDuet = FALSE;
    			g_md.DM("Using VGA Video|ExecuteCommand",0,MD_FMT_UI_ICONINFO,0); //(Dispatch message)
					break;
				case CMD_USEDUET:
 				  g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE);
    			g_md.DM("Using Duet Video|ExecuteCommand",0,MD_FMT_UI_ICONINFO,0); //(Dispatch message)
					g_bUseDuet = TRUE;
					break;
				case CMD_CLEAR:
 				  g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE);
					clearRegions();
					break;
      
				////////////////////////////////////////////////////////////////////
				//  show-specific commands
      
        case CMD_T2A_INIT:
					{
						if ((g_bConnected) &&(g_pduet))
						{
							clearRegions();
 							g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE);
						}
						else
						{
 							g_net.ServerSendCmdReply(socket, GDM_CMD_NAK, FALSE);
							continue;
						}

						g_data.m_bWorking = FALSE;

						if ((g_settings.m_bEnableSqueeze) && (g_bUseDuet) && (g_pduet))
						{
							g_pduet->SetupSqueezeback();
						}

						if (!g_data.m_bGraphicsSetup) 
						{
							g_core.Setup();
							g_data.m_bGraphicsSetup=TRUE;
						}
					}	break;
        case CMD_T2A_UNINIT:
					{
 						g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE);
						g_core.UnSetup();
//						g_data.m_bGraphicsSetup=FALSE; // safer to require shutdown of CAL...
					}	break;
        case CMD_T2A_INTRO:
					{
 						g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE);
						// fly in
						//g_core.Intro(""); //nothing in DL
					} break;
        case CMD_T2A_OUTRO:
					{
 						g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE);
						// fly out
						//g_core.Outro(); //nothing in DL
					} break;
        case CMD_T2A_DATA:
					{
						// data spec as follows: in szOut[2]
						//"TYPE|show dependent delimited data"
						// for DL the following:
						//"%02x|%d|delim data here", DB_TYPE_TEXT, TYPE_DEDICATION, (dedication fields = "dedicationID | categoryName | songID | source | sender name | sender location | receiver name | receiver location | Dedication")
						//"%02x|%d|delim data here", DB_TYPE_TEXT, TYPE_REPLY, (reply fields = "replyID | categoryID | songID | dedicationID | sender name | sender location | receiver name | reply location | Dedication | Reply")
						CString szFields[DL_MAXFIELDS];
						int nItems = tu.breakStringOnSepChar('|', szOut[2], szFields, DL_MAXFIELDS);

						if (nItems>1) // set mimimum req. fields here
						{
							//OK first we need to understand what type of thing it is.
							int nType = tu.ReturnHexByte(szFields[0]);
							switch(nType)
							{
							case DB_TYPE_TEXT://          0x08 // messages
								{
									if (g_data.m_bWorking) 
									{
										// send busy if busy playing already
										g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, 
										"28" /*GDM_REPLY_ERROR_SHOW_BUSY = 0x28*/,
										FALSE);
									}
									else
									{
										g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE);
										g_data.m_bWorking = TRUE;

										// handle fields in a show specific manner:
										if (atoi(szFields[1]) == TYPE_DEDICATION)
										{
											if(szFields[DFIELD_DEDICATION+2].GetLength()>0) // must have something to crawl!
												g_core.Play(szOut[2]);
											else 
											{
												g_data.m_bWorking = FALSE;
												g_core.SendReadyPulse(); // have to send to clear busy
											}
										}
										else if (atoi(szFields[1]) == TYPE_REPLY)
										{
											if( (szFields[RFIELD_DEDICATION+2].GetLength()>0)// must have something to crawl!
												&&(szFields[RFIELD_REPLY+2].GetLength()>0) )// must have something to crawl!
												g_core.Play(szOut[2]);
											else 
											{
												g_data.m_bWorking = FALSE;
												g_core.SendReadyPulse(); // have to send to clear busy
											}
										}
										else// unknown type!
										{
											g_md.DM(0,MD_FMT_UI_ICONERROR,0, "Unknown type.|ExecuteCommand",byteCommand ); //(Dispatch message)
											g_core.SendReadyPulse(); // have to send to clear busy
											g_data.m_bWorking = FALSE;
										}

									}
								} break;
							case DB_TYPE_AUXGFX://        0x10 // images, sponsorship, logos, icons, supplementary text, etc.
								{
									g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE);
									g_md.DM(0,MD_FMT_UI_ICONERROR,0, "DB_TYPE_AUXGFX not supported.|ExecuteCommand" ); //(Dispatch message)
									// not supported in DL
									g_core.SendReadyPulse(); // have to send to clear busy
								} break;
							case DB_TYPE_USER1://		      0x80 // assignable
								{
									g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE);
									g_md.DM(0,MD_FMT_UI_ICONERROR,0, "DB_TYPE_USER1 not supported.|ExecuteCommand" ); //(Dispatch message)
									// not supported in DL
									g_core.SendReadyPulse(); // have to send to clear busy
								} break;
							default://		    
								{
									g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE);
									g_md.DM(0,MD_FMT_UI_ICONERROR,0, "Default type not supported.|ExecuteCommand" ); //(Dispatch message)
									// not supported in DL
									g_core.SendReadyPulse(); // have to send to clear busy
								} break;
							}
						}
						else
						{
							g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, FALSE); // this returns OK for now... (ignores error)
							g_core.SendReadyPulse(); // have to send to clear busy
		    			g_md.DM(0,MD_FMT_UI_ICONERROR,0,"Incomplete data received: %s|ExecuteCommand::CMD_T2A_DATA",tu.Encode(szOut[2])); //(Dispatch message)
						}
					}	break;
        case CMD_T2A_DURATION: 
					{
	          szReply.Format("%02x|%ld", GDM_CMD_ACK, 0);
						CString szFields[DL_MAXFIELDS];
						int nItems = tu.breakStringOnSepChar('|', szOut[2], szFields, DL_MAXFIELDS);

						if (nItems>1) // set mimimum req. fields here
						{
							//OK first we need to understand what type of thing it is.
							g_md.DM(0,MD_FMT_UI_ICONINFO,0,"Timing estimate requested with [%s]|ExecuteCommand",tu.Encode(szOut[2])); //(Dispatch message)
							int nType = tu.ReturnHexByte(szFields[0]);
							switch(nType)
							{
							case DB_TYPE_TEXT://          0x08 // messages
								{
									int nDuration = 0;

									// handle fields in a show specific manner:
									if (atoi(szFields[1]) == TYPE_DEDICATION)
									{
										if(szFields[DFIELD_DEDICATION+2].GetLength()>0) // must have something to crawl!
											nDuration = g_core.EstimateDuration(szOut[2]);
										szReply.Format("%02x|%ld", GDM_CMD_ACK, nDuration);
										g_md.DM(0,MD_FMT_UI_ICONINFO,0,"Dedication timing estimate was [%s]. LengthMetric=%d, Multiplier=%f|ExecuteCommand",tu.Encode(szReply),g_data.m_nLengthMetric_D,g_data.m_dblTimingMultiplier_D); //(Dispatch message)
									}
									else if (atoi(szFields[1]) == TYPE_REPLY)
									{
										if( (szFields[RFIELD_DEDICATION+2].GetLength()>0)// must have something to crawl!
											&&(szFields[RFIELD_REPLY+2].GetLength()>0) )// must have something to crawl!
											nDuration = g_core.EstimateDuration(szOut[2]);
										szReply.Format("%02x|%ld", GDM_CMD_ACK, nDuration);
										g_md.DM(0,MD_FMT_UI_ICONINFO,0,"Reply timing estimate was [%s]. LengthMetricD=%d, MultiplierD=%f, LengthMetricR=%d, MultiplierR=%f|ExecuteCommand",
											tu.Encode(szReply),
											g_data.m_nLengthMetric_R1,g_data.m_dblTimingMultiplier_R1,
											g_data.m_nLengthMetric_R2,g_data.m_dblTimingMultiplier_R2
											); //(Dispatch message)
									}
									else// unknown type!
									{
										g_md.DM(0,MD_FMT_UI_ICONERROR,0, "Unknown type.|ExecuteCommand" ); //(Dispatch message)
									}
								} break;
							case DB_TYPE_AUXGFX://        0x10 // images, sponsorship, logos, icons, supplementary text, etc.
								{
									// not supported in DL
									g_md.DM(0,MD_FMT_UI_ICONERROR,0, "DB_TYPE_AUXGFX not supported.|ExecuteCommand" ); //(Dispatch message)
									szReply.Format("%02x|%ld", GDM_CMD_ACK, 0);
								} break;
							case DB_TYPE_USER1://		      0x80 // assignable
								{
									// not supported in DL
									g_md.DM(0,MD_FMT_UI_ICONERROR,0, "DB_TYPE_USER1 not supported.|ExecuteCommand" ); //(Dispatch message)
									szReply.Format("%02x|%ld", GDM_CMD_ACK, 0);
								} break;
							default:
								{
									// not supported in DL
									g_md.DM(0,MD_FMT_UI_ICONERROR,0, "Default type not supported.|ExecuteCommand" ); //(Dispatch message)
									szReply.Format("%02x|%ld", GDM_CMD_ACK, 0);
								} break;
							}
						}
						else
						{
		    			g_md.DM(0,MD_FMT_UI_ICONERROR,0,"Incomplete data received: %s|ExecuteCommand::CMD_T2A_DURATION",tu.Encode(szOut[2])); //(Dispatch message)
						}
 						g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, szReply, FALSE);
					} break; 
				case CMD_T2A_INFO://0x66 // supplemental info
					{
						// for DL, it is amend category and icon lookup
	          szReply.Format("%02x|%d", GDM_CMD_ACK,0);
						CString szRecords[DL_MAXCATS+1]; // add one, because first record is action|num records
						int nItems = tu.breakStringOnSepChar(10, szOut[2], szRecords, DL_MAXCATS+1);

						if (nItems>0) // set mimimum req. fields here
						{
							//OK first we need to understand what action it is.
							// first record MUST be "0d|23" first 2 digits are hex command, pipe, then decimal num items
							int nType = tu.ReturnHexByte(szRecords[0]);
							int nNum = 0;
							switch(nType)
							{
							case INFO_CMD_CAT_ADD://				0
								{
									nNum = atoi(szRecords[0].Mid(3));
									for(int i=1;i<min(nNum,nItems-1);i++)
									{
										g_core.AddCategoryRecord(szRecords[i]);
									}
								} break;
							case INFO_CMD_CAT_DEL://				1
								{
									nNum = atoi(szRecords[0].Mid(3));
									for(int i=1;i<min(nNum,nItems-1);i++)
									{
										g_core.DelCategoryRecord(szRecords[i]);
									}
								} break;
							case INFO_CMD_CAT_CLR://				2
								{
									g_core.DelCategoryRecord("delete_all_categories");
								} break;
							case INFO_CMD_CAT_GET://				3
								{
									szReply.Format("%02x|%s", GDM_CMD_ACK,tu.Encode(g_core.ReturnCategories()));
								} break;
							}
						}
 						g_net.ServerSendCmdReply(socket, GDM_CMD_ACK, szReply, FALSE);						
					} break; 
				case CMD_DL_USEREVENT1://0xa1 // zoom dedication out.
					{
						g_core.Remove(g_data.m_hcalDedicationGroupID);
//	g_md.DM(0,MD_FMT_UI_ICONEXCLAMATION,0,"x2|ExecuteCommand");
	if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Clearing dedication...|",CMD_DL_USEREVENT1 ); //(Dispatch message)

						// check this: if not this, it takes 12 seconds longer to complete... 
						// seems true!
						CheckGDM(CALUpdateW(g_data.m_hcalDedicationCrawl, ecalUpdateAndReplace, 1, " "), __LINE__);
//						CheckGDM(CALUpdateI(g_data.m_hcalDedicationCrawl, ecalUpdateAndReplace, 1, " "), __LINE__);
//	g_md.DM(0,MD_FMT_UI_ICONEXCLAMATION,0,"x3|ExecuteCommand");

						int iw, ih; //imageWidth, imageHeight
						long attribs[20], keys[20], priority;  // attribute to affect, keyframes
						Real32 values[20],  scale=2.0f; // values, initial scale factor
						int n=0, i;

						iw=g_settings.m_sizeDedicationImgRgn.cx; ih=g_settings.m_sizeDedicationImgRgn.cy;
						// animate out
						// animation keyframes
						for (n=0, i=0; i<20; i++) { attribs[i] = 0; keys[i] = 0; values[i] = 0.0; }
						n=0;
						//CheckGDM(CALAnimate(m_DedicationGroupID, attribs, 0, keys, values, ecalClearKeyFrames), __LINE__);
	if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Zooming dedication out...|",CMD_DL_USEREVENT1 ); //(Dispatch message)

						attribs[n] = ecalXCenter;	keys[n] = 0;      values[n] = (iw/2);	n++;
						attribs[n] = ecalYCenter;	keys[n] = 0;      values[n] = (ih/2);	n++;
						attribs[n] = ecalZCenter;	keys[n] = 0;      values[n] = 0;	n++;
						attribs[n] = ecalXCenter;	keys[n] = g_settings.m_nDedicationZoomOutFrames; values[n] = (iw/2);	n++;
						attribs[n] = ecalYCenter;	keys[n] = g_settings.m_nDedicationZoomOutFrames; values[n] = (ih/2);	n++;
						attribs[n] = ecalZCenter;	keys[n] = g_settings.m_nDedicationZoomOutFrames; values[n] = 0;	n++;

						attribs[n] = ecalXScale;		keys[n] = 0;	    values[n] = 1.0;	  n++;
						attribs[n] = ecalXScale;		keys[n] = g_settings.m_nDedicationZoomOutFrames;	values[n] = 0.0;		  n++;
						attribs[n] = ecalYScale;		keys[n] = 0;	    values[n] = 1.0;	  n++;
						attribs[n] = ecalYScale;		keys[n] = g_settings.m_nDedicationZoomOutFrames;	values[n] = 0.0;		  n++;

						attribs[n] = ecalXPos;		keys[n] = 0;	    values[n] = g_settings.m_sizePosOffsets[g_data.m_nPositionIndex].cx;	  n++;
						attribs[n] = ecalXPos;		keys[n] = g_settings.m_nDedicationZoomOutFrames;	values[n] = g_settings.m_sizePosOffsets[g_data.m_nPositionIndex].cx/*+(iw/2)*/;		  n++;
						attribs[n] = ecalYPos;		keys[n] = 0;	    values[n] = g_settings.m_sizePosOffsets[g_data.m_nPositionIndex].cy;	  n++;
						attribs[n] = ecalYPos;		keys[n] = g_settings.m_nDedicationZoomOutFrames;	values[n] = g_settings.m_sizePosOffsets[g_data.m_nPositionIndex].cy/*+(ih/2)*/;		  n++;

						//attribs[n] = ecalAlpha;	  keys[n] = 0;             values[n] = 255; n++;
						//attribs[n] = ecalAlpha;	  keys[n] = g_settings.m_nDedicationZoomOutFrames-1; values[n] = 255; n++;
						//attribs[n] = ecalAlpha;	  keys[n] = g_settings.m_nDedicationZoomOutFrames;   values[n] = 0;	 n++;

						CheckGDM(CALAnimate(g_data.m_hcalDedicationGroupID, attribs, n, keys, values, (enum ecalInterpMode)(ecalLinear | ecalClearKeyFrames)), __LINE__);
						CheckGDM(CALPlayW(g_data.m_hcalDedicationGroupID), __LINE__); //playW because we need to calc time

						switch(g_data.m_nSourceIcon)
						{
						case 0:
							{
							} break;
						case 1:
							{
								CheckGDM(CALGetPriority(g_data.m_hcalSourceIcon, &priority), __LINE__); 
								CheckGDM(CALRemoveFromGroup(g_data.m_hcalDedicationGroupID, priority, 1), __LINE__);
							} break;
						}

						g_core.Remove(g_data.m_hcalDedicationGroupID);

						g_data.m_nTimingEstMS_D = clock() - g_data.m_dwStartTime;
						g_md.DM(0,MD_FMT_UI_ICONEXCLAMATION,0,"Last dedication had a duration of %d milliseconds, estimate was %d milliseconds.|ExecuteCommand", 
							g_data.m_nTimingEstMS_D, g_data.m_dwLastPlayedEstimate);

						double newMult = (double)(g_data.m_nTimingEstMS_D - g_data.m_nTimingOffsetMS_D)/(double)g_data.m_nLengthMetric_D;
						g_data.m_dblTimingMultiplier_D = (4.0*g_data.m_dblTimingMultiplier_D+newMult)/5.0; //weighted....

						g_core.SendReadyPulse();

						g_md.DM(0,MD_FMT_UI_ICONINFO,0, "Dedication finished playing. Command 0x%02x returned.|ExecuteCommand",byteCommand ); //(Dispatch message)
					}	break;
				case CMD_DL_USEREVENT2://0xa2 // flip to reply.
					{
						g_data.m_bInEndAnim = FALSE;

						g_data.m_nTimingEstMS_R1 = clock() - g_data.m_dwStartTime;

						// first multiplier
						double newMult = (double)(g_data.m_nTimingEstMS_R1 - g_data.m_nTimingOffsetMS_R1)/(double)g_data.m_nLengthMetric_R1;
						g_data.m_dblTimingMultiplier_R1 = (4.0*g_data.m_dblTimingMultiplier_R1+newMult)/5.0; //weighted....


						g_core.PlayReply();
					}	break;
				case CMD_DL_USEREVENT3://0xa3 // zoom reply out.
					{
						long priority;

						g_core.Remove(g_data.m_hcalReplyGroupID);

	if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Clearing reply...|",CMD_DL_USEREVENT3 ); //(Dispatch message)
						// clean up, just in case crawl finished early...
						//g_md.DM("clean up, just in case crawl finished early...");
						CheckGDM(CALUpdateW(g_data.m_hcalReplyCrawl2, ecalUpdateSelect, 1, " "), __LINE__);
//						CheckGDM(CALUpdateI(g_data.m_hcalReplyCrawl2, ecalUpdateAndReplace, 1, " "), __LINE__);

						int iw, ih; //imageWidth, imageHeight
						long attribs[20], keys[20];  // attribute to affect, keyframes
						Real32 values[20],  scale=2.0f; // values, initial scale factor
						int n=0, i;

						iw=g_settings.m_sizeReplyImgRgn.cx; ih=g_settings.m_sizeReplyImgRgn.cy;

	if(g_settings.m_bVerboseLogging) g_md.DM(0,MD_FMT_UI_ICONHAND,0, "Zooming reply out...|",CMD_DL_USEREVENT3 ); //(Dispatch message)
						//g_md.DM("about to fly out");
						// Fly out
						for (i = 0; i<20; i++) {	attribs[i] = 0; keys[i] = 0; values[i] = 0.0; }
						n=0;
						// CheckGDM(CALAnimate(m_ReplyGroupID, attribs, 0, keys, values, ecalClearKeyFrames), __LINE__);

						attribs[n] = ecalXCenter;	keys[n] = 0;      values[n] = (iw/2);	n++;
						attribs[n] = ecalYCenter;	keys[n] = 0;      values[n] = (ih/2);	n++;
						attribs[n] = ecalZCenter;	keys[n] = 0;      values[n] = 0;	n++;
						attribs[n] = ecalXCenter;	keys[n] = g_settings.m_nReplyZoomOutFrames; values[n] = (iw/2);	n++;
						attribs[n] = ecalYCenter;	keys[n] = g_settings.m_nReplyZoomOutFrames; values[n] = (ih/2);	n++;
						attribs[n] = ecalZCenter;	keys[n] = g_settings.m_nReplyZoomOutFrames; values[n] = 0;	n++;

						attribs[n] = ecalXScale;	keys[n] = 0;	    values[n] = 1.0;	  n++;
						attribs[n] = ecalXScale;	keys[n] = g_settings.m_nReplyZoomOutFrames;	values[n] = 0.0;		  n++;
						attribs[n] = ecalYScale;	keys[n] = 0;	    values[n] = 1.0;	  n++;
						attribs[n] = ecalYScale;	keys[n] = g_settings.m_nReplyZoomOutFrames;	values[n] = 0.0;		  n++;

						attribs[n] = ecalXPos;		keys[n] = 0;	    values[n] = g_settings.m_sizeRPosOffsets[g_data.m_nPositionIndex].cx;	  n++;
						attribs[n] = ecalXPos;		keys[n] = g_settings.m_nReplyZoomOutFrames;	values[n] = g_settings.m_sizeRPosOffsets[g_data.m_nPositionIndex].cx/*+(iw/2)*/;		  n++;
						attribs[n] = ecalYPos;		keys[n] = 0;	    values[n] = g_settings.m_sizeRPosOffsets[g_data.m_nPositionIndex].cy;	  n++;
						attribs[n] = ecalYPos;		keys[n] = g_settings.m_nReplyZoomOutFrames;	values[n] = g_settings.m_sizeRPosOffsets[g_data.m_nPositionIndex].cy/*+(ih/2)*/;		  n++;

						//attribs[n] = ecalAlpha;	  keys[n] = 0;             values[n] = 255; n++;
						//attribs[n] = ecalAlpha;	  keys[n] = g_settings.m_nReplyZoomOutFrames-1; values[n] = 255; n++;
						//attribs[n] = ecalAlpha;	  keys[n] = g_settings.m_nReplyZoomOutFrames;   values[n] = 0;	 n++;
  
						CheckGDM(CALAnimate(g_data.m_hcalReplyGroupID, attribs, n, keys, values, (enum ecalInterpMode)(ecalLinear | ecalClearKeyFrames)), __LINE__);
						CheckGDM(CALPlayW(g_data.m_hcalReplyGroupID), __LINE__);

						//g_md.DM("about to remove crawl2 and reply label and category2 from group");

						// pull out master before removing children
						g_core.Remove(g_data.m_hcalReplyGroupID);
  

						// removing children
  
						CheckGDM(CALGetPriority(g_data.m_hcalReplyCrawl2, &priority), __LINE__); 
						CheckGDM(CALRemoveFromGroup(g_data.m_hcalReplyGroupID, priority, 1), __LINE__);
						CheckGDM(CALGetPriority(g_data.m_hcalReplyReplyLabel, &priority), __LINE__); 
						CheckGDM(CALRemoveFromGroup(g_data.m_hcalReplyGroupID, priority, 1), __LINE__);
						CheckGDM(CALGetPriority(g_data.m_hcalReplyCategory2, &priority), __LINE__); 
						CheckGDM(CALRemoveFromGroup(g_data.m_hcalReplyGroupID, priority, 1), __LINE__);
						CheckGDM(CALGetPriority(g_data.m_hcalReplyIcon2, &priority), __LINE__); 
						CheckGDM(CALRemoveFromGroup(g_data.m_hcalReplyGroupID, priority, 1), __LINE__);

						g_data.m_nTimingEstMS_R = clock() - g_data.m_dwStartTime;
						g_data.m_nTimingEstMS_R2 = clock() - g_data.m_dwStartTimeR2;
						g_md.DM(0,MD_FMT_UI_ICONEXCLAMATION,0,"Last reply (with original dedication) had a duration of %d milliseconds, estimate was %d milliseconds.|ExecuteCommand", 
							g_data.m_nTimingEstMS_R, g_data.m_dwLastPlayedEstimate);

						// second multiplier
						double newMult = (double)(g_data.m_nTimingEstMS_R2 - g_data.m_nTimingOffsetMS_R2)/(double)g_data.m_nLengthMetric_R2;
						g_data.m_dblTimingMultiplier_R2 = (4.0*g_data.m_dblTimingMultiplier_R2+newMult)/5.0; //weighted....

						g_core.SendReadyPulse();
						g_md.DM(0,MD_FMT_UI_ICONINFO,0, "Dedication and Reply finished playing. Command 0x%02x returned.|ExecuteCommand",byteCommand ); //(Dispatch message)
					}	break;

				}  //end of switch

  	    if(!g_data.m_bWorking) 
				{
					if(!(
							 (byteCommand==CMD_DL_USEREVENT1) 
						 ||(byteCommand==CMD_DL_USEREVENT2) 
						 ||(byteCommand==CMD_DL_USEREVENT3) 
						))
						g_md.DM(0,MD_FMT_UI_ICONINFO,0, "Command %s returned.|ExecuteCommand", szOut[1]); //(Dispatch message)
				}
        else
        {
          if (
							( (g_settings.m_bAllowExpiry)&&(clock() > g_data.m_dwExpireTime) )
						||( clock() > (g_data.m_dwExpireTime+30000)) // MUST expire after a 30 second wait.
						)
          { 
            // Expired this one because [e 1] never hit!
						if(!g_data.m_bInEndAnim) 
						{
							g_data.m_bInEndAnim = TRUE;
							g_md.DM(0,MD_FMT_UI_ICONSTOP,0,
								"Expired this one: %s|ExecuteCommand",
								tu.Encode(g_data.m_szLastDataPlayed)); //(Dispatch message)

							//do an out anim here (if called for)
							// nothing for DL
							// anim code ****

							g_core.SendReadyPulse();  //sets g_data.m_bInEndAnim = FALSE; g_data.m_bWorking = FALSE;

						}// else [e 1] has hit, just late, so dont do it again.
          }
				}
//  			g_md.DM("Command returned.|ExecuteCommand",0,MD_FMT_UI_ICONINFO,0); //(Dispatch message)
			}
      else
      {
       g_md.DM("pduet was null!|ExecuteCommand",0,MD_FMT_UI_ICONERROR,0); //(Dispatch message)
       // maybe do a ACK with failure here as well.
      }
		}

		if(theApp.GetShowState()!=ISM_SHOWDLL_STATE_DORMANT) // set the state back only if nec.
			g_data.SetShowState(ISM_SHOWDLL_STATE_DORMANT); // set the state back.

		Sleep(5);
	}// thread ending

	pThread->m_criticalCommandCmdData.Lock();
	pThread->m_bCommandThreadStarted=FALSE;
	pThread->m_criticalCommandCmdData.Unlock();

	g_data.m_bCommandThreadStarted=FALSE;
	g_md.DM(0,0,0, "Command thread ending.|CommandThreadProcess"); //(Dispatch message)

		// let the app object know it can go now.
  g_data.m_pCmdExeThreadEvent->SetEvent(); // sets it to signalled

	return 0;

}


// CAL specific helpers!
/*
void clearScreen()
{
  clearRegions();
  clearFontsAndColors();
}
*/
void clearRegions()
{
  CheckGDM(CALFree(0, ecalFreeNow), __LINE__);

  HCAL MyBogusTxtRgn;
  CheckGDM(CALTextRgn(0, 0, 10, 10, 0, ecalBlack, ecalNoJustify, &MyBogusTxtRgn), __LINE__);
  CheckGDM(CALPlayI(MyBogusTxtRgn), __LINE__);
  CheckGDM(CALFree(MyBogusTxtRgn, ecalFreeNow), __LINE__);
}

/*
void clearFontsAndColors()
{
  if (font1>=0) { CheckGDM(CALFreeFont(font1), __LINE__); font1 = -1; }
  if (font2>=0) { CheckGDM(CALFreeFont(font2), __LINE__); font2 = -1; }
  if (font3>=0) { CheckGDM(CALFreeFont(font3), __LINE__); font3 = -1; }
  if (font4>=0) { CheckGDM(CALFreeFont(font4), __LINE__); font4 = -1; }
  if (font5>=0) { CheckGDM(CALFreeFont(font5), __LINE__); font5 = -1; }

/*#ifndef MMUSA
  // Byrne says: do not free colors to fix the AddEdge 
  // space-removal problem...
  
  //Check(CALFreeColor(-1), __LINE__)
  //color_white = color_orange = color_brn = color_green = color_yellow = 
  //  color_gray = color_purple = color_green2 = color_ltblue = -1; 

  if (color_white>=0)  { Check(CALFreeColor(color_white), __LINE__);  color_white = -1; }
  if (color_orange>=0) { Check(CALFreeColor(color_orange), __LINE__); color_orange = -1; }
  if (color_brn>=0)    { Check(CALFreeColor(color_brn), __LINE__);    color_brn = -1; }
  if (color_green>=0)  { Check(CALFreeColor(color_green), __LINE__);  color_green = -1; }
  if (color_yellow>=0) { Check(CALFreeColor(color_yellow), __LINE__); color_yellow = -1; }
  if (color_gray>=0)   { Check(CALFreeColor(color_gray), __LINE__);   color_gray = -1; }
  if (color_purple>=0) { Check(CALFreeColor(color_purple), __LINE__); color_purple = -1; }
  if (color_green2>=0) { Check(CALFreeColor(color_green2), __LINE__); color_green2 = -1; }
  if (color_ltblue>=0) { Check(CALFreeColor(color_ltblue), __LINE__); color_ltblue = -1; }
#endif * /
}
*/


/*
 *  Returns the path to where DuetEngine Server is running,
 *  with a trailing backslash
 */
CString getRelativePath()
{
  CString szDir;
  char buffer[512];
  GetCurrentDirectory(510, buffer);
  szDir.Format("%s", buffer);
  if (szDir.Right(1) != "\\") szDir += "\\";
  return szDir;
}

void __cdecl OnUserEvent(long connId, long eventId, long data)
{
  CTextUtils tu;
  switch (eventId)
  {
  case 0: // not used
    break;
  case 1: // end of dedication, send ready pulse
  case 2: // end of dedication, flip to reply
  case 3: // end of reply, send ready pulse
		if(!g_data.m_bInEndAnim) //g_data.m_bInEndAnim = TRUE only if expiry has occurred.  we do this check in cae [e 1] was late.
		{
			g_data.m_bInEndAnim = TRUE;
			g_data.m_nEventID = eventId;
			AfxBeginThread(WaitThreadProcess, (LPVOID)&g_data.m_nEventID); 
		}
    break;
  }
}

UINT WaitThreadProcess(LPVOID pParam)
{
	int nID = g_data.m_nEventID;
//	g_md.DM(0,MD_FMT_UI_ICONEXCLAMATION,0,"before|ExecuteCommand");

	switch(nID)
	{
	case 0: break;
	case 1: 
		{
			Sleep((10 * g_settings.m_nDedicationFinishPercent * g_settings.m_nDedicationWidth)/g_settings.m_nDedicationCrawlRate); 
		} break;
	case 2:
		{
			Sleep((10 * g_settings.m_nReplyDedFinishPercent * g_settings.m_nReplyDedWidth)/g_settings.m_nReplyDedCrawlRate); 
		} break;
	case 3: 
		{
			Sleep((10 * g_settings.m_nReplyFinishPercent * g_settings.m_nReplyWidth)/g_settings.m_nReplyCrawlRate); 
		} break;
	}
//	g_md.DM(0,MD_FMT_UI_ICONEXCLAMATION,0,"w1|ExecuteCommand");
	CString szData;
	szData.Format("%02x|%02x|null",0xa0|nID,0xa0|nID);
	g_threadHelper.QueueRequestForCommand(szData, NULL); 
	return 0;
}
