// DEDICATELIVETHREADS DEFINES
#ifndef DEDICATELIVETHREADS_DEFINES_INCLUDED
#define DEDICATELIVETHREADS_DEFINES_INCLUDED

#include <afxmt.h>
#include "../../../Shared/networking/networking.h"

class CDedicateLiveThreadHelpers  // : public CObject
{
public:
	CDedicateLiveThreadHelpers();   // standard constructor
	~CDedicateLiveThreadHelpers();   // standard destructor

// for server

	CCriticalSection m_criticalServerThreadCmd;
	UINT		m_nServerThreadCommand;
	int			m_nServerPort;
	UINT		m_nLastHeartbeat;
	UINT		m_nServerHeartbeat;
	int			m_nNumRetries;
	BOOL		m_bServerStarted;


	CCriticalSection m_criticalServerCmdData;
	// comes in off the server
	SOCKET	m_socket[ISMDLL_MAX_QUEUED_COMMANDS];
	BYTE		m_cmd[ISMDLL_MAX_QUEUED_COMMANDS];
	CString m_szData[ISMDLL_MAX_QUEUED_COMMANDS];
	int			m_nNumQueuedCommands;
	
	BOOL QueueRequestFromServer(SOCKET s, BYTE Request, CString szData);
	BOOL PullRequestFromServerQueue(SOCKET *s, BYTE *Request, CString *szData);
	BOOL IsServerThreadToEnd();
	BOOL SetServerThreadCommand(UINT nCmd);

// for command

	CCriticalSection m_criticalCommandThreadCmd;
	UINT m_nCommandThreadCommand;
	BOOL	m_bCommandThreadStarted;


	CCriticalSection m_criticalCommandCmdData;
	// goes into the thread
	SOCKET	m_socketIn[ISMDLL_MAX_QUEUED_COMMANDS]; // for CAL
	BYTE		m_cmdIn[ISMDLL_MAX_QUEUED_COMMANDS];
	CString m_szDataIn[ISMDLL_MAX_QUEUED_COMMANDS];
	int			m_nNumQueuedCommandsIn;

	// comes out of the thread
	BYTE		m_cmdOut[ISMDLL_MAX_QUEUED_COMMANDS];
	CString m_szDataOut[ISMDLL_MAX_QUEUED_COMMANDS];
	int			m_nNumQueuedCommandsOut;

// to command the command thread
	BOOL QueueRequestForCommand(CString szData, SOCKET socket); // for CAL
	BOOL PullRequestFromCommandQueueIntoThread(CString *szData, SOCKET* socket); // for CAL
//	BOOL QueueRequestForCommand(BYTE Request, CString szData); //generic
//	BOOL PullRequestFromCommandQueueIntoThread(BYTE* Request, CString* szData); //generic
// to get commands from command thread
	BOOL QueueRequestFromCommand(BYTE Request, CString szData);
	BOOL PullRequestFromCommandQueueOutOfThread(BYTE* Request, CString* szData);
// thread maintenance
	BOOL IsCommandThreadToEnd();
	BOOL SetCommandThreadCommand(UINT nCmd);
};


#endif //DEDICATELIVETHREADS_DEFINES_INCLUDED
