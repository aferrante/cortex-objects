// DedicateLiveCore.h: interface for the CDedicateLiveCore class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DEDICATELIVECORE_H__8F3457DE_DD48_4725_8462_E6493B23C3FD__INCLUDED_)
#define AFX_DEDICATELIVECORE_H__8F3457DE_DD48_4725_8462_E6493B23C3FD__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CDedicateLiveCore  
{
public:
	CDedicateLiveCore();
	virtual ~CDedicateLiveCore();

// util functions
	CString BuildCrawl(CString szDelimitedData); 
	BOOL SendReadyPulse();
	//specific to DL
	CString BuildReplyCrawl(CString szDelimitedData);
	void AddCategoryRecord(CString szDelimitedData);
	void DelCategoryRecord(CString szDelimitedData);
	int	 LookupCategoryIcon(CString szCategory); // returns a char based on category name  
	int	 LookupCategoryIndex(CString szCategory); // returns array index based on category name (but accepts full record)
	CString ReturnCategories();
	CString FixLocation(CString szLocation);

// CAL functions
	void Setup();
	void UnSetup();
	void Remove(HCAL hcal);
	UINT Intro(CString szDelimitedData); 
	UINT Outro(CString szDelimitedData); 
	UINT Play(CString szDelimitedData); 
	int  GetTextExtent(CString szText); 
	DWORD EstimateDuration(CString szDelimitedData);
	CString StripNonCachedChars(CString szText);
	//specific to DL
	UINT PlayReply(); 

};

#endif // !defined(AFX_DEDICATELIVECORE_H__8F3457DE_DD48_4725_8462_E6493B23C3FD__INCLUDED_)
