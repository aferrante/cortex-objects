//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DedicateLive.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDM_IMPORT                      0x0020
#define IDM_EXPORT                      0x0030
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDR_MAINFRAME                   128
#define IDD_DEDICATELIVE_DIALOG         129
#define IDB_BITMAP_STDICONS             129
#define IDB_BITMAP_VDI                  131
#define IDD_DEDICATELIVE_SETTINGS_DIALOG 131
#define IDB_BITMAP_STATUSYEL            132
#define IDB_BITMAP_STATUSBLU            133
#define IDB_BITMAP_LOGO                 134
#define IDD_DEDICATELIVE_CAT_IMPEXP_DIALOG 134
#define IDB_SETTINGS                    135
#define IDB_BITMAP_STATUSRED            143
#define IDB_BITMAP_STATUSGRN            144
#define IDB_BITMAP_PAUSELIST            146
#define IDB_BITMAP_PLAYLIST             147
#define IDC_STATIC_LOGO                 1000
#define IDC_LIST_STATUS                 1000
#define IDC_STATIC_URL                  1003
#define IDC_URLFRAME                    1005
#define IDC_STATICTEXT_URL              1006
#define IDC_STATIC_SHOWNAME             1010
#define IDC_STATIC_LISTENPORT           1012
#define IDC_EDIT_EMAILTO                1013
#define IDC_EDIT_EMAILIP                1014
#define IDC_STATIC_READYPORT            1015
#define IDC_EDIT_DATA1URL               1016
#define IDC_STATIC_READYIP              1016
#define IDC_STATIC_DATA1                1017
#define IDC_STATIC_READYIP2             1017
#define IDC_STATIC_DATA2                1018
#define IDC_EDIT_DATA2URL               1019
#define IDC_STATIC_DATA3                1020
#define IDC_STATIC_READYIP5             1020
#define IDC_EDIT_DATA3URL               1021
#define IDC_STATIC_DATA4                1022
#define IDC_EDIT_DATA4URL               1023
#define IDC_STATIC_READYIP8             1023
#define IDC_STATIC_BUILD                1029
#define IDC_EDIT_NAIPS                  1029
#define IDC_EDIT_SHOWNAME               1031
#define IDC_EDIT_LISTENPORT             1032
#define IDC_EDIT_READYPORT              1033
#define IDC_EDIT_READYIP                1034
#define IDC_EDIT_D_CRAWLSPD             1035
#define IDC_EDIT_D_CRAWLPCNT            1036
#define IDC_EDIT_D_ZOOMIN               1037
#define IDC_EDIT_D_ZOOMOUT              1038
#define IDC_STATICTEXT_TITLEVERSION     1039
#define IDC_EDIT_D_CRAWLFMT             1039
#define IDC_STATICGROUP_PLAYOUTSETTINGS 1040
#define IDC_STATICGROUP_DATADOWNLOAD    1041
#define IDC_EDIT_RD_CRAWLSPD            1041
#define IDC_STATICGROUP_EMAIL           1042
#define IDC_BUTTON_PAUSE                1043
#define IDC_EDIT_RD_CRAWLPCNT           1043
#define IDC_EDIT_RD_ZOOMIN              1044
#define IDC_STATIC_STATUS               1045
#define IDC_STATICGROUP_NETALERT        1045
#define IDC_EDIT_RR_ZOOMOUT             1046
#define IDC_EDIT_RD_CRAWLFMT            1047
#define IDC_EDIT_RR_CRAWLSPD            1048
#define IDC_EDIT_RR_CRAWLPCNT           1049
#define IDC_STATIC_DEDICATELIVE_LOGO    1050
#define IDC_CHECK_ENABLE_SQUEEZE        1050
#define IDC_EDIT_CAL_LAYER_GFX          1051
#define IDC_EDIT_RR_CRAWLFMT            1052
#define IDC_EDIT_CAL_LAYER_SQUEEZE      1053
#define IDC_EDIT_RD_FLIP                1054
#define IDC_STATICGROUP_REQDEV          1055
#define IDC_EDIT_SUPPDEV_DUET           1056
#define IDC_EDIT_SUPPDEV_OTHER          1057
#define IDC_STATICTEXT_COPYRIGHT        1077
#define IDC_EDIT1                       1087
#define IDC_BUTTON_BROWSE               1088
#define IDC_CHECK_VERBOSE               1092
#define IDC_STATIC_SENDEMAILALERTSTO    3081
#define IDC_STATIC_MAILSVRIP            3083
#define IDC_STATIC_NETALERTIPS          3084
#define IDC_STATIC_SUPPDEV_DUET         3085
#define IDC_STATIC_SUPPDEV_OTHER        3086
#define IDC_STATIC_SUPPDEV_DUET2        3086
#define IDC_STATIC_SUPPDEV_DUET3        3087

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1050
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
