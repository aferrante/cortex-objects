#if !defined(AFX_DLIMPORTEXPORTDLG_H__1E11CF8A_4B7F_4E14_8B16_F97C05F55BFB__INCLUDED_)
#define AFX_DLIMPORTEXPORTDLG_H__1E11CF8A_4B7F_4E14_8B16_F97C05F55BFB__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DLImportExportDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDLCatImportExportDlg dialog

class CDLCatImportExportDlg : public CDialog
{
// Construction
public:
	CDLCatImportExportDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDLCatImportExportDlg)
	enum { IDD = IDD_DEDICATELIVE_CAT_IMPEXP_DIALOG };
	CString	m_szFileName;
	//}}AFX_DATA


	BOOL m_bImport; //cf export


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDLCatImportExportDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDLCatImportExportDlg)
	afx_msg void OnButtonBrowse();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLIMPORTEXPORTDLG_H__1E11CF8A_4B7F_4E14_8B16_F97C05F55BFB__INCLUDED_)
