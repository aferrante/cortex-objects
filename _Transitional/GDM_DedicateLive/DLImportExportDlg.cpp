// DLImportExportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DedicateLive.h"
#include "DLImportExportDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDLCatImportExportDlg dialog


CDLCatImportExportDlg::CDLCatImportExportDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDLCatImportExportDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDLCatImportExportDlg)
	m_szFileName = _T("categories.txt");
	//}}AFX_DATA_INIT
	m_bImport = TRUE;
}


void CDLCatImportExportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDLCatImportExportDlg)
	DDX_Text(pDX, IDC_EDIT1, m_szFileName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDLCatImportExportDlg, CDialog)
	//{{AFX_MSG_MAP(CDLCatImportExportDlg)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE, OnButtonBrowse)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDLCatImportExportDlg message handlers

void CDLCatImportExportDlg::OnButtonBrowse() 
{
	CFileDialog dlg(TRUE, "txt", NULL, OFN_LONGNAMES|OFN_NOCHANGEDIR|OFN_PATHMUSTEXIST|OFN_HIDEREADONLY);
	dlg.m_ofn.lpstrTitle="Select file";
	char buffer[80];
	sprintf(buffer, "TagMonitor Event file (.txt)%c*.txt%c%c",0,0,0);
	dlg.m_ofn.lpstrFilter=buffer; 
	if(dlg.DoModal()==IDOK)
	{
		m_szFileName=dlg.GetPathName();
		UpdateData(FALSE);
	}
}

void CDLCatImportExportDlg::OnOK() 
{
	if(m_bImport)
	{
		if(AfxMessageBox("This will overwrite all data currently in the event list!", MB_OKCANCEL)!=IDOK) return;
	}
	else
	{
		if(AfxMessageBox("This will overwrite all data currently in the export file!", MB_OKCANCEL)!=IDOK) return;
	}
	CDialog::OnOK();
}

BOOL CDLCatImportExportDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	if(m_bImport)
	{
		GetDlgItem(IDOK)->SetWindowText("Import");
		SetWindowText("Import Categories from file");
	}
	else
	{
		GetDlgItem(IDOK)->SetWindowText("Export");
		SetWindowText("Export Categories to file");
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
