#ifndef CDEDICATELIVEDATA_INCLUDED
#define CDEDICATELIVEDATA_INCLUDED

class CDedicateLiveData // : public CObject
{

public:
	CDedicateLiveData();   // standard constructor
	~CDedicateLiveData();   // standard destructor


public:
	BOOL m_bServerStarted;
	BOOL m_bDialogStarted;
	BOOL m_bCommandThreadStarted;
	BOOL m_bShowInProgress;

	int m_nCurrDeviceIndex;
	
	UINT m_nShowState;
	UINT m_nShowStateBeforeSuspend;

	CString m_szStatusText;
	CString m_szToolTipText;
	int m_nLEDState;
	CCriticalSection m_criticalStatus;

  CEvent* m_pCmdExeThreadEvent;
  CEvent* m_pSvrExeThreadEvent;

	CDC m_dc; // for text extent
	BOOL m_bDCOK;

	double m_dblTimingMultiplier_D;
	int m_nTimingOffsetMS_D;
	int m_nTimingEstMS_D;
	int m_nLengthMetric_D;

	double m_dblTimingMultiplier_R1;
	double m_dblTimingMultiplier_R2;
	int m_nLengthMetric_R1;
	int m_nLengthMetric_R2;
	int m_nTimingOffsetMS_R1;
	int m_nTimingOffsetMS_R2;
	int m_nTimingEstMS_R;
	int m_nTimingEstMS_R1;
	int m_nTimingEstMS_R2;

	BOOL m_bSqueezed;
	BOOL m_bGraphicsSetup;
  BOOL m_bWorking;
  BOOL m_bInEndAnim;

	DWORD m_dwStartTime;
	DWORD m_dwStartTimeR2;
	DWORD m_dwExpireTime;

	CString m_szLastDataPlayed;
	CString m_szLastDataEstimated;
	DWORD m_dwLastDataEstimate;
	DWORD m_dwLastPlayedEstimate;
	CString m_szLastDataCrawl1;
	CString m_szLastDataCrawl2;
	CString m_szAfterFlipCategory;

	CString m_szDingbatsCache;

	int m_nEventPlaying;

	int m_nEventID;

	int m_nPositionIndex;

	int m_nSourceIcon;

	//CAL
	HCAL m_hcalFont[DL_MAXFONTS];
	HCAL m_hcalIconFont; 

	HCAL m_hcalColor[DL_MAXCOLORS]; 

	HCAL m_hcalDedicationBackplateID; 
	HCAL m_hcalReplyBackplateID;
	HCAL m_hcalDedicationGroupID;
	HCAL m_hcalDedicationCategory;
	HCAL m_hcalDedicationFrom;
	HCAL m_hcalDedicationTo;
	HCAL m_hcalDedicationCrawl;
	HCAL m_hcalReplyCategory;
	HCAL m_hcalReplyCategory2;
	HCAL m_hcalReplyOrigLabel;
	HCAL m_hcalReplyReplyLabel;
	HCAL m_hcalReplyCrawl1; 
	HCAL m_hcalReplyCrawl2; 
	HCAL m_hcalDedicationIcon;
	HCAL m_hcalReplyIcon1;
	HCAL m_hcalReplyIcon2;
	HCAL m_hcalReplyGroupID;
	HCAL m_hcalSourceIcon;
	
// util functions
	void ClearData();
	void SetShowState(UINT nState);
	int	SetShowStatus(CString szStatusText, CString szToolTipText, int nLEDState);
};


#endif  //CDEDICATELIVEDATA_INCLUDED
