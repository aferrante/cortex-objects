// DL2GEData.h: interface for the CDL2GEData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DL2GEDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
#define AFX_DL2GEDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <winsock2.h>
#include <sys/timeb.h>
#include "DL2GEDefines.h"
#include "../../../Common/TXT/BufferUtil.h" 
#include "../../../Common/LAN/NetUtil.h" 


class CDL2GEObject  
{
public:
	CDL2GEObject();
	virtual ~CDL2GEObject();
	void Free();

	// util objects
	CBufferUtil m_bu;
	CNetUtil m_net;  // network client only.  to send commands and keep connections open (uses m_socketCmd and m_socketStatus below)

	char* m_pszName;
	unsigned short m_usType;

	// destinations within object
	char** m_ppszDestName;
	char** m_ppszDestParams;  // has to be generic for all types
	unsigned long m_ulNumDest; // number of destinations.

	// dependencies within object
	char** m_ppszResourceName;
	char** m_ppszResourceParams;  // has to be generic for all types
	unsigned long m_ulNumResources; // number of dependency resources.

	// IP and ports
	char* m_pszHost;
	unsigned short m_usFilePort;
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;
//	SOCKET m_socketFile; // current comm socket for retrieving files  // dont store, not persistent conn.
	SOCKET m_socketCmd; // current comm socket for commands
	SOCKET m_socketStatus; // current comm socket for status

	// cortex assigned status
	unsigned long m_ulStatus;
	unsigned long m_ulOwner;

	// there is a negotiation to set the following from default values.
	unsigned long m_ulStatusIntervalMS;		// the interval, in milliseconds, at which we expect to hear status.
	unsigned long m_ulFailureIntervalMS;  // the interval, in milliseconds, at which we declare failure of object.

	// status received from object
	_timeb m_timebLastStatus; // the time of the last status received (parseable)
	_timeb m_timebLastTime;		// the last time received (object's local time)
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for object status

	char* GetStatusText(unsigned long* pulStatus);  // allocates mem, must free after use
	int		SetStatusText(char* pszText, unsigned long ulStatus, unsigned long ulStatusCounter);  // the counter is assigned with a remote get over network

private:
	CRITICAL_SECTION m_critText;
	char* m_pszStatus;	// parseable string
};



class CDL2GEData  
{
public:
	CDL2GEData();
	virtual ~CDL2GEData();

	// util object
	CBufferUtil m_bu;

	// cortex object brokering
	unsigned short m_usResourcePortLast;
	unsigned short m_usProcessPortLast;

	CDL2GEObject** m_ppObj;
	int m_nNumObjects;
	int m_nNumResources;  // this is for convenience only
	int m_nNumProcesses;  // this is for convenience only

	_timeb m_timebTick; // the last time check inside the thread
	_timeb m_timebLastStatus; // the time of the last status given to another cortex
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for cortex global status (each obj has their own as well)

	char* GetStatusText(unsigned long* pulStatus);  // allocates mem, must free after use
	int		SetStatusText(char* pszText, unsigned long ulStatus);

private:
	CRITICAL_SECTION m_critText;
	char* m_pszStatus;	// parseable string

//	unsigned long m_ulReqCount;
//	unsigned long m_ulSvcCount;
};

#endif // !defined(AFX_DL2GEDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
