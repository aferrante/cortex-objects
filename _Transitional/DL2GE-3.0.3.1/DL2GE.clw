; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDL2GEDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "DL2GE.h"
LastPage=0

ClassCount=5
Class1=CAboutDlg
Class2=CDL2GEApp
Class3=CDL2GEHandler

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MENU1
Class4=CDL2GEDlg
Class5=CDL2GESettings
Resource3=IDD_CORTEX_DIALOG

[CLS:CAboutDlg]
Type=0
HeaderFile=cortexdlg.h
ImplementationFile=cortexdlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CAboutDlg

[CLS:CDL2GEApp]
Type=0
BaseClass=CWinApp
HeaderFile=DL2GE.h
ImplementationFile=DL2GE.cpp
Filter=N
VirtualFilter=AC
LastObject=CDL2GEApp

[CLS:CDL2GEHandler]
Type=0
BaseClass=CWnd
HeaderFile=DL2GEHandler.h
ImplementationFile=DL2GEHandler.cpp
Filter=W
VirtualFilter=WC
LastObject=CDL2GEHandler

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=8
Control1=IDC_STATICTEXT_TITLE,static,1342308481
Control2=IDC_STATICTEXT_URL,static,1208090625
Control3=IDOK,button,1342373889
Control4=IDC_STATIC_LOGO,static,1342177294
Control5=IDC_URLFRAME,static,1342177298
Control6=IDC_STATIC_BUILD,static,1342308353
Control7=IDC_STATIC_URL,static,1342177294
Control8=IDC_STATICTEXT_COPYRIGHT,static,1342308353

[MNU:IDR_MENU1]
Type=1
Class=?
Command1=ID_CMD_ABOUT
Command2=ID_CMD_SETTINGS
Command3=ID_CMD_SHOWWND
Command4=ID_CMD_EXIT
Command5=ID_CMD_ABOUT
Command6=ID_CMD_SETTINGS
Command7=ID_CMD_SHOWWND
Command8=ID_CMD_EXIT
CommandCount=8

[CLS:CDL2GEDlg]
Type=0
HeaderFile=DL2GEDlg.h
ImplementationFile=DL2GEDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_TREE1

[CLS:CDL2GESettings]
Type=0
HeaderFile=DL2GESettings.h
ImplementationFile=DL2GESettings.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CDL2GESettings

[DLG:IDD_CORTEX_DIALOG]
Type=1
Class=CDL2GEDlg
ControlCount=7
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_SETTINGS,button,1342242944
Control4=IDC_LIST1,SysListView32,1350681613
Control5=IDC_STATIC_STATUSTEXT,static,1342308352
Control6=IDC_STATIC_PROGBAR,static,1342177294
Control7=IDC_TREE1,SysTreeView32,1082130487

