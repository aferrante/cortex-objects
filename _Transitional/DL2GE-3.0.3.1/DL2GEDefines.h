// DL2GEDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(DL2GEDEFINES_H_INCLUDED)
#define DL2GEDEFINES_H_INCLUDED

#include <stdlib.h>


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define CX_CURRENT_VERSION		"3.0.3.1"
#define CX_AUTH_USER					"cortex"  // for cortex to tell an object that it is cortex that is sending commands
#define CX_AUTH_PWD						"medulla" // cortex's password


// modes
#define CX_MODE_DEFAULT			0x00000000  // exclusive
#define CX_MODE_LISTENER		0x00000001  // exclusive
#define CX_MODE_CLONE				0x00000002  // exclusive
#define CX_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define CX_MODE_VOLATILE		0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define CX_MODE_MASK				0x0000000f  // 

// default port values.
#define CX_PORT_FILE				80		
#define CX_PORT_CMD					10560		
#define CX_PORT_STATUS			10561		

#define CX_PORT_RESMIN			10660		
#define CX_PORT_RESMAX			20659		

#define CX_PORT_PRCMIN			20660		
#define CX_PORT_PRCMAX			30659		

#define CX_PORT_INVALID			0	

// types
#define CX_TYPE_UNDEF				0x0000  // undefined	
#define CX_TYPE_RESOURCE		0x8000  // resource	
#define CX_TYPE_PROCESS			0x4000  // process	

// resource types
#define CX_TYPE_DB					0x0001  // database	
#define CX_TYPE_AUTOMATION	0x0002  // automation	
#define CX_TYPE_CG					0x0003  // CG	
#define CX_TYPE_AUDIO				0x0004  // audio	
#define CX_TYPE_MIXER				0x0005  // mixer	
#define CX_TYPE_FEED				0x0006  // feed	
#define CX_TYPE_POLL				0x0007  // polling data manager
#define CX_TYPE_USER				0x000f  // user defined	

// process types
#define CX_TYPE_PERIODIC		0x0010  // periodic process, like a show	
#define CX_TYPE_ETERNAL			0x0020  // eternal process.	
#define CX_TYPE_INFO				0x0100  // process that acts as an information resource for other processes.	
#define CX_TYPE_META				0x0200  // process that keeps tabs on other processes and resources, without grabbing ownership.	


// status
#define CX_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define CX_STATUS_UNKNOWN							0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define CX_STATUS_ERROR								0x00000020  // error (red icon)
#define CX_STATUS_OK									0x00000030  // ready (green icon)	
#define CX_STATUS_RUN									0x00000040  // in progress, running, owned etc (blue icon);	
#define CX_ICON_MASK									0x00000070  // mask	

#define CX_STATUS_SUSPEND							0x00000080  // suspended	(yellow icon please)


#define CX_STATUS_FILESVR_START				0x00000100  // starting the file server
#define CX_STATUS_FILESVR_RUN					0x00000200  // file server running
#define CX_STATUS_FILESVR_END					0x00000300  // file server shutting down
#define CX_STATUS_FILESVR_ERROR				0x00000400  // file server error
#define CX_STATUS_FILESVR_MASK				0x00000700  // file server mask bits

#define CX_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define CX_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define CX_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define CX_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define CX_STATUS_CMDSVR_MASK					0x00007000  // command server mask bits

#define CX_STATUS_STATUSSVR_START			0x00010000  // starting the status server
#define CX_STATUS_STATUSSVR_RUN				0x00020000  // status server running
#define CX_STATUS_STATUSSVR_END				0x00030000  // status server shutting down
#define CX_STATUS_STATUSSVR_ERROR			0x00040000  // status server error
#define CX_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define CX_STATUS_THREAD_START				0x00100000  // starting the main thread
#define CX_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define CX_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define CX_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define CX_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define CX_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define CX_STATUS_THREAD_MASK					0x00f00000  // main thread mask bits

// various failures...
#define CX_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define CX_STATUS_FAIL_MASK						0xf0000000  // failure code mask bits

// owners are zero-based enumerated, but we reserve the following values
#define CX_OWNER_INVALID				0xffffffff  // not owned, or owner unknown
#define CX_OWNER_DL2GE					0xfffffffe  // owned by this instance of cortex (not another cortex somewhere)

// default intervals
#define CX_TIME_PING				5000		// get device status every 5 seconds
#define CX_TIME_FAIL				60000		// object failure after 1 minute timeout


//return values
#define CX_SUCCESS   0
#define CX_ERROR	   -1


// default filenames
#define CX_SETTINGS_FILE_DEFAULT	  "cortex.csf"		// csf = cortex settings file
#define CX_SETTINGS_FILE_LISTENER	  "listen.csf"		// csf = cortex settings file
#define CX_SETTINGS_FILE_CLONE		  "clone.csf"			// csf = cortex settings file
#define CX_SETTINGS_FILE_SPARK			"cortex.csl"		// csl = cortex spark list
#define CX_SETTINGS_FILE_ENCRYPT		"cortex.esf"		// esf = encrypted settings file
#define CX_SETTINGS_FILE_SECURE1		"cortex.epa"		// epa = ecrypted password archive
#define CX_SETTINGS_FILE_SECURE2		"cortex.epb"		// epb = ecrypted password backup


// common commands.  objects must be careful not to override these.
#define CX_CMD_NULL					0x00  // null or empty command
#define CX_CMD_ACK					0x06  // same as ascii, used as response only
#define CX_CMD_NAK					0x15  // same as ascii, used as response only

#define CX_CMD_HASAUTH			0x80  // usually a subcommand, ORable, menas, has auth info in data.

// commands used by cortex to command cortex listeners
#define CX_CMD_SPARK				0x31  // run the spark list. main cortex hostname:commandport in data.
#define CX_CMD_KILL					0x32  // send kill commands to the exes on the spark list.
#define CX_CMD_KILLSELF			0x33  // end process, possibly for upgrade. (upgrading process should shell execute after upgrade)

// communications params
// commands used by cortex to command resources and processes
#define CX_CMD_CHGCOMM			0x01  // change communication parameters.
// following are ORable subs.
#define CX_CMD_HOST					0x01  // change the host (can be used for switching to backup)
#define CX_CMD_PORT					0x02  // change the ports // has data "commandport:statusport"


// familiar name labels
#define CX_CMD_GETNAME			0x02  // get the familiar names associated with a process or resource.
#define CX_CMD_CHGNAME			0x03  // change the familiar name for a process or resource (if we have one thats identical, must append a suffix to make unique)  new name given in data
// following are exclusive subs.
#define CX_CMD_OBJ					0x01  // default, get the familiar name for the object
#define CX_CMD_DEST					0x02  // get the familiar name for all destinations in a resource.


// other object information
#define CX_CMD_GETINFO			0x04  // get the information associated with a cortex object
#define CX_CMD_ASSIGN				0x05  // set ownership and other info
// following are exclusive subs.
#define CX_CMD_TYPE					0x01  // get the enumerated type value of the object.
#define CX_CMD_STATUS				0x02  // get current status.
#define CX_CMD_OWNER				0x03  // get/set the name of the current owner.
#define CX_CMD_STATUSINT		0x04  // get/set status interval.
#define CX_CMD_FAILINT			0x05  // get/set failure interval.
#define CX_CMD_SETTING			0x06  // get/set setting value.
#define CX_CMD_AUTHCRED			0x07  // set cortex authorization credentials to identify cortex to a module

// files
#define CX_CMD_GETFILE			0x06	// get files associated with module.
// following are exclusive subs.
#define CX_CMD_LIST_ADMIN   0x01  // retrieves a list of files and templates necessary for admin
#define CX_CMD_LIST_AUTH	  0x02  // retrieves a list of files and templates necessary for auth
#define CX_CMD_LIST_STATUS  0x03  // retrieves a list of files and templates necessary for status
#define CX_CMD_LIST_LOGS	  0x04  // retrieves a list of files and templates necessary for logs
#define CX_CMD_LIST_HELP	  0x05  // retrieves a list of files and templates necessary for help
#define CX_CMD_LIST_QUERY	  0x06  // retrieves a list of files that match a query
#define CX_CMD_FILE					0x08  // retrieves a single file. (used after receiving lists, retrieves deficiencies)

#define CX_CMD_BYE				  0x0f  // command module to shut down.


// commands used by resources and processes to request things from cortex
#define CX_REQ_HELLO				0x01  // startup... has data "host:commandport:statusport"
#define CX_REQ_GIVESTATUS		0x02  // gives formatted status of module and destinations, etc
#define CX_REQ_REQOWN		    0x03  // requests ownership of a destination
#define CX_REQ_RELOWN		    0x04  // releases ownership of a destination
#define CX_REQ_AUTHCHK		  0x05  // check credentials against the cortex main security files

// commands used by cortex to answer resources and processes. (main cmd should be ACK or NAK plus flags)
#define CX_REPLY_OK					0x00  // was able to comply
#define CX_REPLY_NOT				0x01  // was not able to comply.



#endif // !defined(DL2GEDEFINES_H_INCLUDED)
