
#include "stdafx.h"
#include "DL2GE.h"
#include "DedicateLiveData.h"

extern CDedicateLiveSettings g_settings;

CDedicateLiveData::CDedicateLiveData()
{
	m_pSvrExeThreadEvent = new CEvent(FALSE, TRUE);
  m_pSvrExeThreadEvent->SetEvent(); // sets it to signalled
  m_pCmdExeThreadEvent = new CEvent(FALSE, TRUE);
  m_pCmdExeThreadEvent->SetEvent(); // sets it to signalled
	m_nCurrDeviceIndex=-1;
	m_bDialogStarted=FALSE;
	m_szStatusText=_T("Initializing");
	m_szToolTipText=_T("Initializing");
	m_nLEDState=2; //red = 0, green = 1, yellow = 2, blue =3
	m_nEventID =0;

	ClearData();
}

CDedicateLiveData::~CDedicateLiveData()
{
}

void CDedicateLiveData::ClearData()
{
	m_bServerStarted=FALSE;
	m_bCommandThreadStarted=FALSE;
	m_bShowInProgress=FALSE;

	m_nSourceIcon = 0;

	m_nShowState=ISM_SHOWDLL_STATE_DORMANT; // necessary to start off GDM shows
	m_nShowStateBeforeSuspend=ISM_SHOWDLL_STATE_DORMANT;

	m_bGraphicsSetup = FALSE;
	m_bSqueezed = FALSE;
  m_bWorking = FALSE;
	m_bInEndAnim = FALSE;
	m_nLengthMetric_R1 = 0;
	m_nLengthMetric_R2 = 0;
	m_nLengthMetric_D = 0;
	m_szLastDataPlayed = _T("");
	m_szLastDataEstimated = _T("");
	m_szLastDataCrawl1 = _T("");
	m_szLastDataCrawl2 = _T("");
	m_dwLastDataEstimate = 0;

	m_nEventPlaying = -1;
	m_nPositionIndex=0;
}


void CDedicateLiveData::SetShowState(UINT nState)
{
	m_nShowState = nState;
	CString szText=_T("");
	CString szStatusText=_T("");
	CString szToolTipText=_T("");
	int nLEDState = 0;

	switch(nState&0x0ffffff0)
	{
	case ISM_SHOWDLL_STATE_DORMANT: szText=_T("DORMANT (0x00000000)"); szStatusText = szToolTipText = _T("Waiting for Command");  if(m_bShowInProgress) nLEDState = 3; else nLEDState = 1; break;
	case ISM_SHOWDLL_STATE_INIT: szText=_T("INIT (0x00100000)");  szStatusText = szToolTipText = _T("Initialized");  nLEDState = 1; break;
	case ISM_SHOWDLL_STATE_CHECKING: szText=_T("CHECKING (0x00000010)"); 	szStatusText = szToolTipText = _T("Checking Triggers");  nLEDState = 1; break;
	case ISM_SHOWDLL_STATE_INITSHOW: szText=_T("INITSHOW (0x00000020)"); szStatusText = szToolTipText = _T("Initializing Show");  nLEDState = 1; break;
	case ISM_SHOWDLL_STATE_PRESHOW: szText=_T("PRESHOW (0x00000040)");	szStatusText = szToolTipText = _T("In Pre-Show");  nLEDState = 1; break;
	case GDM_SHOWDLL_STATE_CMDEXE: szText=_T("DURINGSHOW (0x00000080)"); szStatusText = szToolTipText = _T("Executing Command");  nLEDState = 3; break;
	case ISM_SHOWDLL_STATE_POSTSHOW: szText=_T("POSTSHOW (0x00000100)"); szStatusText = szToolTipText = _T("In Post-Show");  nLEDState = 1; break;
	case ISM_SHOWDLL_STATE_SUSPENDEDSHOW: szText=_T("SUSPENDEDSHOW (0x00000200)"); szStatusText = szToolTipText = _T("In Suspended Show Control");  nLEDState = 2; break;
	case ISM_SHOWDLL_STATE_ENDSHOW: szText=_T("ENDSHOW (0x00000400)"); szStatusText = szToolTipText = _T("Ending Show");  nLEDState = 1; break;
	case ISM_SHOWDLL_STATE_ABORTSHOW: szText=_T("ABORTSHOW (0x00000800)"); szStatusText = szToolTipText = _T("Aborting");  nLEDState = 2; break;
	case ISM_SHOWDLL_STATE_SUSPENDED: szText=_T("SUSPENDED (0x00001000)"); szStatusText = szToolTipText = _T("Suspended");  nLEDState = 2; break;
	default: szText.Format("State: 0x%08x",nState); break;
	}

	if ((nState&ISM_SHOWDLL_STATE_MISSINGTRIGDEV)||(nState&ISM_SHOWDLL_STATE_MISSINGTRIGDEV))
	{
		szText+=_T(". Missing devices."); szStatusText+=_T(". Missing devices."); szStatusText+=_T(". Missing devices."); nLEDState = 0; 
	}
	if (nState&ISM_SHOWDLL_STATE_ERROR)
	{
		szText+=_T(". Other errors exist."); szStatusText+=_T(". ERROR."); szStatusText+=_T(". ERROR."); nLEDState = 0; 
	}

	SetShowStatus(szStatusText, szToolTipText, nLEDState );
//	if(g_pdlg) g_pdlg->GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(szText);
//  g_md.DM(szText,MD_DEST_UI_ITEM1|MD_DEST_NODEFAULTS,0,0);

}

int CDedicateLiveData::SetShowStatus(CString szStatusText, CString szToolTipText, int nLEDState)
{
	m_criticalStatus.Lock();
	int nRV = m_nLEDState; //return last state

	m_szStatusText = szStatusText;
	m_szToolTipText = szToolTipText;
	m_nLEDState = nLEDState;

	m_criticalStatus.Unlock();
	return nRV;
}
