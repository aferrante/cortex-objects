#ifndef TEXTTOAIRDB2_INCLUDED
#define TEXTTOAIRDB2_INCLUDED


#include <afxdb.h>

//field names
// Show, ID, ItemID, Data, User1, User2, Type, Status, Attributes, SubmitTime, TimeOnAir, Duration
#define DB_FIELDNAME_SHOW      "Show"
#define DB_FIELDNAME_ID        "ID"
#define DB_FIELDNAME_ITEMID    "ItemID"
#define DB_FIELDNAME_DATA      "Data"
#define DB_FIELDNAME_USER1     "User1"
#define DB_FIELDNAME_USER2     "User2"
#define DB_FIELDNAME_TYPE      "Type"
#define DB_FIELDNAME_STATUS    "Status"
#define DB_FIELDNAME_ATTRIBS	 "Attributes"
#define DB_FIELDNAME_TIMESTAMP "SubmitTime"
#define DB_FIELDNAME_TIMEONAIR "TimeOnAir"
#define DB_FIELDNAME_DURATION  "Duration"

// field defs
#define DB_FIELD_SHOW      0
#define DB_FIELD_ID        1
#define DB_FIELD_ITEMID    2
#define DB_FIELD_DATA      3
#define DB_FIELD_USER1     4
#define DB_FIELD_USER2     5
#define DB_FIELD_TYPE      6
#define DB_FIELD_STATUS    7
#define DB_FIELD_ATTRIBS	 8
#define DB_FIELD_TIMESTAMP 9
#define DB_FIELD_TIMEONAIR 10
#define DB_FIELD_DURATION  11

// text field sizes
#define DB_FIELDSIZE_ID        16   // normal use is for automationID, usually limited to 8 chars, but extra given for user uses
#define DB_FIELDSIZE_ITEMID    8    // 8 chars allows for 100 million unique IDs if using decimal, 4294967296 if using hex, and over 281 trillion if using base 64.  that ought to be enough.
#define DB_FIELDSIZE_DATA      1024 // this is main data, its 1024 because if this is text data, it usually exceeds the number of characters that can be scrolled on the screen in a reasonable fashion during the length of an avg music video.  If the field is used for other things and more space needed, it can be linked to additional fields with the use of a user field to identify another record...
#define DB_FIELDSIZE_USER1     64   // extra data, not main data.
#define DB_FIELDSIZE_USER2     64   // more extra data, in case necessary.

// retrieve by fields...
#define DB_RB_SHOW					0x00000001
#define DB_RB_ID						0x00000002
#define DB_RB_ITEMID				0x00000004
#define DB_RB_DATA					0x00000008
#define DB_RB_USER1					0x00000010
#define DB_RB_USER2					0x00000020
#define DB_RB_TYPE					0x00000040
#define DB_RB_STATUS				0x00000080
#define DB_RB_ATTRIBS				0x00000100
#define DB_RB_TIMESTAMP			0x00000200
#define DB_RB_TIMEONAIR			0x00000400
#define DB_RB_DURATION			0x00000800

// types (can only be one type but types ORable for retrieval of multitypes) 
#define DB_TYPE_SHOWOPEN      0x01 // show open ID
#define DB_TYPE_SHOWCLOSE     0x02 // show close ID
#define DB_TYPE_CLIPINFO	    0x04 // extra information about clips
#define DB_TYPE_TEXT          0x08 // messages
#define DB_TYPE_AUXGFX        0x10 // images, sponsorship, logos, icons, supplementary text, etc.
#define DB_TYPE_MARK		      0x20 // editiorial mark - if this type, ATTRIB byte is a mark.
#define DB_TYPE_ARCINFO	      0x40 // archive information - show information.
#define DB_TYPE_USER1		      0x80 // assignable
#define DB_TYPE_MAX 		      0x80 // largest single value, just for convenience
#define DB_TYPE_ALL			      0xff // for convenience
#define DB_TYPE_NONE		      0x00 // for convenience
// statuses
#define DB_STATUS_PLAYED			0x01 // played 
#define DB_STATUS_ARCHIVED		0x02 // archived
#define DB_STATUS_ERROR				0x04 // some error occurred
#define DB_STATUS_ONAIR				0x08 // on air now!
#define DB_STATUS_CUED				0x10 // next up
#define DB_STATUS_REINS				0x20 // reinsert
#define DB_STATUS_USER1				0x40 // assignable
#define DB_STATUS_USER2				0x80 // assignable
#define DB_STATUS_NONE				0x00 // for convenience
// following attributes can override default behavior.
#define DB_ATTRIB_NONE				0x00 // attribute - convenience, reset
#define DB_ATTRIB_MAX					0x10 // highest independent value
#define DB_ATTRIB_NOARC				0x01 // attribute - do not archive item after show - delete after show
#define DB_ATTRIB_REINSNOW		0x02 // attribute - reinsert into active playstack at end (for cycling within clip)
#define DB_ATTRIB_REINSAFTER	0x04 // attribute - reinsert into active playstack at end (for cycling within show, plays once per clip)
#define DB_ATTRIB_SCHED				0x08 // attribute - do not play until after unixdate kept in dwTimeOnAir
#define DB_ATTRIB_EXP					0x10 // attribute - expire item after unixdate kept in dwDuration 
// following is silly - removed
//#define DB_ATTRIB_NODEL				0x02 // attribute - do not delete item after play - leave in active playstack (cant OR with DB_ATTRIB_NOARC - if so, DB_ATTRIB_NOARC behavior ensues)
// editiorial marks - dont OR these.
#define DB_MARK_NONE					0x00 // editiorial mark - convenience, reset
#define DB_MARK_MAX						0x7f // highest independent value
#define DB_MARK_NO						0x01 // editiorial mark - not desireable/ignore
#define DB_MARK_OK						0x02 // editiorial mark - selected as good
#define DB_MARK_GREAT					0x03 // editiorial mark - really great
#define DB_MARK_QUEUED				0x80 // editiorial mark - queued - this can be ORed with other marks

//Return codes for functions
#define DB_SUCCESS					0x00000000
#define DB_ERROR						0x10000000 // generic
#define DB_ERROR_RETRIEVE		0x00000001 // retrieval error exists
#define DB_ERROR_INSERT			0x00000002 // insertion error exists
#define DB_ERROR_DELETE			0x00000004 // deletion error exists
#define DB_ERROR_PARAM			0x00000008 // invalid parameter passed in
#define DB_ERROR_NOTOPEN		0x00000010 // db not open
#define DB_ERROR_MEMEX			0x00000020 // memory exception
#define DB_ERROR_DBEX				0x00000040 // databse exception
#define DB_ERROR_EX 				0x00000080 // other exception


// the following structure is used to exchange data with the TextToAir database tables.
// the Field names as they must be in the actual Tables are:
// Show, ID, ItemID, Data, User1, User2, Type, Status, Attributes, SubmitTime, TimeOnAir, Duration
typedef struct dbT2_t // seenotes of fieldsizes above.
{
 // I changed int to byte to save memspace when declaring huge arrays for a retrieve. (was creating errors)	
	// it can still be BIGINT or whatever in the DB itself.
  BYTE nShow;     // for sort by show  // FieldName = Show
  char szID[DB_FIELDSIZE_ID];          // for use as Cart ID or other  // FieldName = ID
  char szItemID[DB_FIELDSIZE_ITEMID];  // for use as Item ID (char so alpha can be used, but usually numerical) // FieldName = ItemID
  char szData[DB_FIELDSIZE_DATA];      // FieldName = Data
  char szUser1[DB_FIELDSIZE_USER1];    // FieldName = User1
  char szUser2[DB_FIELDSIZE_USER2];    // FieldName = User2
  BYTE nType; // FieldName = Type
    /*We can make type=DB_TYPE_TEXT normal text for air, and
      make type=DB_TYPE_SHOWOPEN a show open, type DB_TYPE_SHOWCLOSE = close.
			So if we are not in a show, we can retrive by type = DB_TYPE_SHOWOPEN 
			to find any possible show opens, when it does open, then we can retrieve type
			= DB_TYPE_SHOWCLOSE to find what the associated show close is (all the type 
			DB_TYPE_SHOWCLOSE IDs with the Show field = show field of the open.)  Then
			until that ID is found, we retrieve type DB_TYPE_TEXTs and push text etc
      ...*/
  BYTE nStatus, nAttributes; // FieldNames = Status, Attributes, 
    /*We need status and property fields. Was trying to use boolean ORs
		  to the type field to save space, but found that this made a huge list of
			possibilities when trying to search for things with certain properties
			or statuses, or even all things of a single type with any status or
			property.  It is definitely worth having these fields separate since
			they are used all the time.
      ...*/
  DWORD dwTimestamp; // FieldName = SubmitTime
/*
  OK. I was going to do the following but no reason to, here's why.
	Since a show does not ever last 115 days or whatever the real number is, it's OK
	to sort within a show to 10 ms resolution, or even millisecond resolution (since
	shows dont last 11.5 days! - we could use 
	m_dbItem.dwTimestamp = (DWORD)((timebuffer.time*1000)+timebuffer.millitm);)
	Then, when the show gets archived, we can put in a single DB Item DB_TYPE_ARCINFO
	and put the show onair time and duration in the dwTimeOnAir and dwDuration fields.
	When a show is airing we can put the real onair times into each item's onair field
	and then retrieve in between dwTimeOnAir and dwTimeOnAir+dwDuration of the show.
	This gives us good sorting. It also allows us to have archived comments about the
	show in the data and attribs etc fields.

	BYTE nMilliTimeDivBy4; // FieldName = SubmitMS
		/* One second resolution was good enough for dwTimeOnAir and dwDuration, but
			not for submittime (since submits can happen very close, within one second,
			it becomes impossible to sort these things within the second).  To get around
			this before I did this: 
			m_dbItem.dwTimestamp = (DWORD)((timebuffer.time*100)+(timebuffer.millitm/10));
			which gives me 10 millisecond resolution, but at the sacrifice of being able to 
			sort back before 115 days or whatever is works out to, instead of back until
			1970.  By adding a single byte I can store full unixtime in dwTimestamp and 
			timebuffer.millitm/4 in nMilliTimeDivBy4 which gets me 4 ms resolution (if
			necessary) at the space expense of only one BYTE more.
			*/
  DWORD dwTimeOnAir, dwDuration;  // FieldNames = TimeOnAir, Duration
      /*
      I think we need another two time fields as well for Scheduled OnAir time 
			and Duration.  If we had these two,
      then we could have T2A where its not just pumping one after the other
      without respect to real time, but actually schedule stuff.  This might
      come in handy on putting text over 30 minute segments, over movies, or
      whatever.  The numbers can be relative to the start of the LouthID, or
      they can be absolute.  They can also be ignored altogether if we are
      doing one after the other type of push.  We can specificy all that 
			in the pipe delim data field for the show open ID (we can have a whole 
			description for what kind of show it will be in there!)*/

} dbT2_t;



class CTextToAirDB2
{
public:
	CTextToAirDB2();   // standard constructor
	~CTextToAirDB2();   // standard destructor

	CString m_szPrimaryTable;
	CString m_szSecondaryTable;
	CString m_szArchiveTable;

  CDatabase m_DB;
  BOOL m_bDatabaseOpen;
	BOOL m_bMSSQL; //TRUE if MSQSL, FALSE if MySQL.
	BOOL m_bUseNames; //if TRUE (default), uses field names.  if FALSE, uses record indices.

// some past usage equivalents  
  UINT RetrieveByShowIDType(int nShow, CString szID, int nType, dbT2_t dbArray[], int nMaxItems, int* pnItems, CString* pszReturnString, CString szTable="");
  UINT RetrieveByShow(int nShow, dbT2_t dbArray[], int nMaxItems, int* pnItems, CString* pszReturnString, CString szTable="");
// following is rewrite since forgot to backup this file to current version
  UINT RetrieveByShowAndOrder(int nShow, dbT2_t dbArray[], int nMaxItems, int* pnItems, CString* pszReturnString, CString szSortField, CString szTable="");
  UINT RetrieveAndDeleteOldestByShowIDType(int nShow, CString szID, int nType, dbT2_t* pdbItem, BOOL* pbNoData, CString* pszReturnString, CString szTable="");
  UINT DeleteAll(CString* pszReturnString, CString szTable="");
  UINT DeleteAllForShow(int nShow, CString* pszReturnString, CString szTable="");
  UINT DeleteAllCommentsForShow(int nShow, CString* pszReturnString, CString szTable="");
  UINT RetrieveOpensAndCloses(dbT2_t* pdbArray, int nMaxItems, int* pnItems, CString* pszReturnString, CString szTable="");

// core
  UINT ConnectDatabase(CString szDBname, CString szUser, CString szPass, CString* pszReturnString);
  UINT DisconnectDatabase(CString* pszReturnString);
  UINT Retrieve(dbT2_t dbArray[], int nMaxItems, int* pnItems, CString* pszReturnString, CString szSQL="", CString szTable="");
  UINT Insert(dbT2_t dbItem, CString* pszReturnString, CString szTable="");
  UINT Delete(dbT2_t dbItem, CString* pszReturnString, CString szTable="");
  UINT Delete(int nShow, CString szID, CString szData, DWORD dwSubmitTime, CString* pszReturnString, CString szTable="");
  UINT ExecuteSQL(CString szSQL, CString* pszReturnString, CString szCallingMethod);
  CString EncodeQuotes(CString szIn);

// generic
  UINT RetrieveRangeByFields(DWORD dwRetrieveFields, dbT2_t dbCriteria, dbT2_t dbArray[], int nMaxItems, int* pnItems, CString* pszReturnString, CString szTable="");
  UINT RetrieveByFields(DWORD dwRetrieveFields, dbT2_t dbCriteria, dbT2_t dbArray[], int nMaxItems, int* pnItems, CString* pszReturnString, CString szTable="");
  UINT RetrieveByFields(DWORD dwRetrieveFields, dbT2_t dbCriteria, DWORD dwOrderByFields[], int nNumOrderFields, dbT2_t dbArray[], int nMaxItems, int* pnItems, CString* pszReturnString, CString szTable="");
  UINT DeleteRangeByFields(DWORD dwRetrieveFields, dbT2_t dbCriteria, CString* pszReturnString, CString szTable="");
  UINT DeleteByFields(DWORD dwRetrieveFields, dbT2_t dbCriteria, CString* pszReturnString, CString szTable="");

};
#endif
