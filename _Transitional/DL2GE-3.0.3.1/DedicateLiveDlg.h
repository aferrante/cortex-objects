#if !defined(AFX_DEDICATELIVEDLG_H__18A5B959_5650_4EAC_9D72_676E85C8FF33__INCLUDED_)
#define AFX_DEDICATELIVEDLG_H__18A5B959_5650_4EAC_9D72_676E85C8FF33__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DedicateLiveDlg.h : header file
//

#define ID_LIST 0
#define ID_STATUS 1
#define ID_BNPAUSE 2
#define ID_LOGO 3
#define DEDICATELIVEDLG_NUM_MOVING_CONTROLS 4

#define BMP_PAUSE 0
#define BMP_PLAY 1
#define MAX_BMPS 2

#define DEDICATELIVEDLG_MINSIZEX 40
#define DEDICATELIVEDLG_MINSIZEY 40

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG


public:
  void OnLogo(UINT nFlags, CPoint point) ;

	DECLARE_MESSAGE_MAP()
};



/////////////////////////////////////////////////////////////////////////////
// CDedicateLiveDlg dialog

class CDedicateLiveDlg : public CDialog
{
// Construction
public:
	CDedicateLiveDlg(CWnd* pParent = NULL);   // standard constructor
	int OnExit();

		//sizing
	BOOL m_bVis;
	BOOL 	m_bNewSizeInit;
	CRect m_rcDlg;
	CRect m_rcCtrl[DEDICATELIVEDLG_NUM_MOVING_CONTROLS];

	CListCtrlEx m_lce;
	HBITMAP m_hbmp[MAX_BMPS];

	BOOL m_bListPaused;

	BOOL OnToolTipNotify( UINT id, NMHDR* pNMHDR, LRESULT* pResult );

	BOOL   m_bUseSysDefaultColors;  // or the colors specificed below
	CBrush m_brush; // def BG color for dialog 
	COLORREF m_crDefBG; //yep


// Dialog Data
	//{{AFX_DATA(CDedicateLiveDlg)
	enum { IDD = IDD_DEDICATELIVE_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDedicateLiveDlg)
	public:
	virtual BOOL Create(CWnd* pWnd=NULL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CDedicateLiveDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnPaint();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo( MINMAXINFO FAR* lpMMI );
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonPause();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEDICATELIVEDLG_H__18A5B959_5650_4EAC_9D72_676E85C8FF33__INCLUDED_)
