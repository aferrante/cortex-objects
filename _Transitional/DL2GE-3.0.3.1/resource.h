//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DL2GE.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDB_BITMAP_STDICONS             129
#define IDI_ICON_CXR                    129
#define IDB_BITMAP_STATUSYEL            132
#define IDD_DL2GE_DIALOG                132
#define IDB_BITMAP_STATUSBLU            133
#define IDB_SETTINGS                    135
#define IDC_CURSOR_LINK                 135
#define IDI_ICON_CLR                    136
#define IDI_ICON_CXG                    137
#define IDI_ICON_CXY                    138
#define IDI_ICON_CXB                    139
#define IDB_BITMAP_STATUSRED            143
#define IDB_BITMAP_STATUSGRN            144
#define IDB_BITMAP_VDS                  149
#define IDC_STATIC_LOGO                 1000
#define IDC_STATICTEXT_TITLE            1001
#define IDC_STATICTEXT_COPYRIGHT        1002
#define IDC_STATIC_URL                  1003
#define IDC_BUTTON_SETTINGS             1004
#define IDC_URLFRAME                    1005
#define IDC_LIST1                       1005
#define IDC_STATICTEXT_URL              1006
#define IDC_STATIC_STATUSTEXT           1006
#define IDC_STATIC_PROGBAR              1010
#define IDC_STATIC_PROGRESS             1012
#define IDC_TREE1                       1012
#define IDC_STATIC_BUILD                1029
#define ID_CMD_SETTINGS                 32771
#define ID_CMD_EXIT                     32772
#define ID_CMD_SHOWWND                  32774
#define ID_CMD_ABOUT                    32775

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
