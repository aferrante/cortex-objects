# Microsoft Developer Studio Project File - Name="DL2GE" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 5.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=DL2GE - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "DL2GE.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "DL2GE.mak" CFG="DL2GE - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "DL2GE - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "DL2GE - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "DL2GE - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "DL2GE - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o NUL /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "DL2GE - Win32 Release"
# Name "DL2GE - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\..\Common\TXT\BufferUtil.cpp

!IF  "$(CFG)" == "DL2GE - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "DL2GE - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\..\Common\IMG\BMP\CBmpUtil_MFC.cpp
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveCore.cpp
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveData.cpp
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveSettings.cpp
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveThreads.cpp
# End Source File
# Begin Source File

SOURCE=.\DL2GE.cpp
# End Source File
# Begin Source File

SOURCE=.\DL2GE.rc
# End Source File
# Begin Source File

SOURCE=.\DL2GEData.cpp

!IF  "$(CFG)" == "DL2GE - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "DL2GE - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\DL2GEDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DL2GEHandler.cpp
# End Source File
# Begin Source File

SOURCE=.\DL2GEMain.cpp
# End Source File
# Begin Source File

SOURCE=.\DL2GESettings.cpp

!IF  "$(CFG)" == "DL2GE - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "DL2GE - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\olddep\duet.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TXT\FileUtil.cpp

!IF  "$(CFG)" == "DL2GE - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "DL2GE - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\..\Common\HTTP\HTTP10.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MFC\ListCtrlEx\ListCtrlEx.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TXT\LogUtil.cpp

!IF  "$(CFG)" == "DL2GE - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "DL2GE - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MSG\Messager.cpp

!IF  "$(CFG)" == "DL2GE - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "DL2GE - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MSG\MessagingObject.cpp

!IF  "$(CFG)" == "DL2GE - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "DL2GE - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\..\Common\LAN\NetUtil.cpp
# End Source File
# Begin Source File

SOURCE=.\olddep\networking.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TXT\Security.cpp

!IF  "$(CFG)" == "DL2GE - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "DL2GE - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp

!IF  "$(CFG)" == "DL2GE - Win32 Release"

# ADD CPP /Yc

!ELSEIF  "$(CFG)" == "DL2GE - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\olddep\TextUtils.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\..\Common\TXT\BufferUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\IMG\BMP\CBmpUtil_MFC.h
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveCore.h
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveData.h
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveDefines.h
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveSettings.h
# End Source File
# Begin Source File

SOURCE=.\DedicateLiveThreads.h
# End Source File
# Begin Source File

SOURCE=.\DL2GE.h
# End Source File
# Begin Source File

SOURCE=.\DL2GEData.h
# End Source File
# Begin Source File

SOURCE=.\DL2GEDefines.h
# End Source File
# Begin Source File

SOURCE=.\DL2GEDlg.h
# End Source File
# Begin Source File

SOURCE=.\DL2GEHandler.h
# End Source File
# Begin Source File

SOURCE=.\DL2GEMain.h
# End Source File
# Begin Source File

SOURCE=.\DL2GESettings.h
# End Source File
# Begin Source File

SOURCE=.\olddep\duet.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TXT\FileUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\HTTP\HTTP10.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MFC\ListCtrlEx\ListCtrlEx.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TXT\LogUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MSG\Messager.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MSG\MessagingObject.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MSG\msg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\LAN\NetDefines.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\LAN\NetUtil.h
# End Source File
# Begin Source File

SOURCE=.\olddep\networking.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TXT\Security.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TXT\SecurityDefines.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\olddep\TextUtils.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\cursor1.cur
# End Source File
# Begin Source File

SOURCE=.\res\DL2GE.ico
# End Source File
# Begin Source File

SOURCE=.\res\DL2GE.rc2
# End Source File
# Begin Source File

SOURCE=.\res\favicon.ico
# End Source File
# Begin Source File

SOURCE=.\res\hpbvdsw.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00002.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00003.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00004.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00005.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00006.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00007.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00008.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00009.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00010.ico
# End Source File
# Begin Source File

SOURCE=".\res\icon-cxb.ico"
# End Source File
# Begin Source File

SOURCE=".\res\icon-cxg.ico"
# End Source File
# Begin Source File

SOURCE=".\res\icon-cxr.ico"
# End Source File
# Begin Source File

SOURCE=".\res\icon-cxy.ico"
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon2.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon_com.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon_qua.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_cxr.ico
# End Source File
# Begin Source File

SOURCE=.\res\jewelIcon.ico
# End Source File
# Begin Source File

SOURCE=.\res\pauselist.bmp
# End Source File
# Begin Source File

SOURCE=.\res\playlist.bmp
# End Source File
# Begin Source File

SOURCE=.\res\settings.bmp
# End Source File
# Begin Source File

SOURCE=".\res\Status-Blu.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Status-Grn.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Status-Red.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Status-Yel.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\stdicons.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Titling_Branding_Banner_Harris.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Titling_Branding_Banner_VDS.bmp
# End Source File
# Begin Source File

SOURCE=.\res\vdi.bmp
# End Source File
# Begin Source File

SOURCE=.\res\vdi.ico
# End Source File
# Begin Source File

SOURCE=.\res\VDS_logo.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\todo.txt
# End Source File
# End Target
# End Project
