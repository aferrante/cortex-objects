// DedicateLiveSettings.cpp : implementation file
//

#include "stdafx.h"
#include "DL2GE.h"
#include "DL2GE.h"  // for CAbout
#include "DedicateLiveSettings.h"
//#include "DLImportExportDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//extern ISMShowData_t g_showdata;  // data to expose to ISM/GDM
extern CDedicateLiveThreadHelpers g_threadHelper; // thread data for server if needed (one per server)
extern CDedicateLiveData g_data;
extern UINT ServerThreadProcess(LPVOID pParam); // server thread proto


/////////////////////////////////////////////////////////////////////////////
// CDedicateLiveSettings dialog


CDedicateLiveSettings::CDedicateLiveSettings(CWnd* pParent /*=NULL*/)
//	: CDialog(CDedicateLiveSettings::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDedicateLiveSettings)
	m_szEmailIP = _T("0.0.0.0");
	m_szEmailTo = _T("");
	m_nListenPort = ISMDLL_DEF_LISTEN_PORT;
	m_szShowName = _T("DedicateLive");
	m_szNetAlertIPs = _T("0.0.0.0:10777, 0.0.0.1:10777");
	m_szSupDevDuet = _T("Duet");
	m_bEnableSqueeze = FALSE;
	m_nLayerGfx = 0;
	m_nLayerSqueeze = 0;
  m_nReadyPort = 10561;
	m_szReadyIP = _T("");
	m_bVerboseLogging = FALSE;
	//}}AFX_DATA_INIT

	m_szFileRotSpec=_T("YM");
	m_szFileCustNameSpec=_T("");

	m_bVerboseLogging = FALSE;
	m_nNumCategories=0;
	int i;
	for(i=0; i<DL_MAXCATS; i++)
	{
//		m_CatInfo[i].nIndex=0;
		m_CatInfo[i].szName=_T("");
//		m_CatInfo[i].szFont=_T("");
		m_CatInfo[i].nIconChar=0;
//		m_CatInfo[i].nIconSize=0;
//		m_CatInfo[i].nIconRelX=0;
//		m_CatInfo[i].nIconRelY=0;
	}

	m_szColors[DL_COLOR_ORANGE] = _T("732b50ff");
	m_szColors[DL_COLOR_WHITE]	= _T("ebebebff");
	m_szColors[DL_COLOR_GREY]		= _T("f4a8c3ff");
	m_szColors[DL_COLOR_PURPLE] = _T("a83f75ff");

	m_szFont[0] = _T("HelveticaRounded Bold");
	m_szFont[1] = _T("Arial Black");
	m_szFont[2] = _T("HelveticaRounded Bold");
	m_szFont[3] = _T("HelveticaRounded Bold");

	// spec is: "BOOLAddedge|intcoloridx|intedgeType|real32angle|real32depth|longsoftness"
	m_szFontEdge[0].Format("1|0|%d|315.0|5.0|0",ecalDropShadow);
	m_szFontEdge[1] = _T("0");
	m_szFontEdge[2] = _T("0");
	m_szFontEdge[3].Format("1|0|%d|315.0|3.0|0",ecalDropShadow);

	m_nFontStyle[0] = ecalNormal;
	m_nFontStyle[1] = ecalItalic;
	m_nFontStyle[2] = ecalNormal;
	m_nFontStyle[3] = ecalNormal;

	m_nFontSize[0] = 24;
	m_nFontSize[1] = 28;
	m_nFontSize[2] = 20;
	m_nFontSize[3] = 22;

	m_szIconFont = _T("ZapfDingbats BT");
	m_szIconFontEdge.Format("1|0|%d|315.0|3.0|0",ecalDropShadow);
	m_nIconFontSize = 48;

	m_bAllowExpiry = FALSE;


	m_sizePosOffsets[0].cx=37;	//65
	m_sizePosOffsets[0].cy=40;	//100
	m_sizePosOffsets[1].cx=266; //266
	m_sizePosOffsets[1].cy=60;	//100
	m_sizePosOffsets[2].cx=266;	//266
	m_sizePosOffsets[2].cy=315;	//315

	m_sizeRPosOffsets[0].cx=52;		//65
	m_sizeRPosOffsets[0].cy=40;		//100
	m_sizeRPosOffsets[1].cx=222;	//266
	m_sizeRPosOffsets[1].cy=60;		//100
	m_sizeRPosOffsets[2].cx=222;	//266
	m_sizeRPosOffsets[2].cy=312;	//315

	m_sizeDedicationImgRgn.cx = 424;
	m_sizeDedicationImgRgn.cy = 97;

	m_nDedicationCrawlRate = 100;
	m_szDedicationFormatString = _T("To: %N, %L / From %n, %l / \"%D\"");
	m_nDedicationWidth = 290;
	m_nDedicationFinishPercent=100;
	m_szDedicationBackplateFile = _T("DefaultData\\DedicateLive\\dedframe-generic-v2.tga");
	m_nDedicationZoomInFrames=5;
	m_nDedicationZoomOutFrames=5;

	m_szSourceIconFile0  = _T("DefaultData\\DedicateLive\\BLUEcellPhone.tga");

	m_sizeReplyImgRgn.cx = 438;
	m_sizeReplyImgRgn.cy = 100;

	m_nReplyDedCrawlRate = 100;
	m_szReplyDedFormatString = _T("To: %N, %L / From %n, %l / \"%D\"");
	m_nReplyDedWidth = 298;
	m_nReplyDedFinishPercent=100;
	m_szReplyDedBackplateFile = _T("DefaultData\\DedicateLive\\replyframe-v2.tga");
	m_nReplyDedZoomInFrames=5;
	m_nReplyFlipFrames=5;

	m_nReplyCrawlRate = 100;
	m_szReplyFormatString = _T("To: %n, %l / From: %N, %L / \"%R\"");
	m_nReplyWidth = 298;
	m_nReplyFinishPercent=100;
	m_szReplyBackplateFile = _T("DefaultData\\DedicateLive\\replyframe-rev-v2.tga");
	m_nReplyZoomOutFrames=5;

}

/*
void CDedicateLiveSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDedicateLiveSettings)
	DDX_Text(pDX, IDC_EDIT_EMAILIP, m_szEmailIP);
	DDX_Text(pDX, IDC_EDIT_EMAILTO, m_szEmailTo);
	DDX_Text(pDX, IDC_EDIT_LISTENPORT, m_nListenPort);
	DDX_Text(pDX, IDC_EDIT_SHOWNAME, m_szShowName);
	DDX_Text(pDX, IDC_EDIT_NAIPS, m_szNetAlertIPs);
	DDX_Text(pDX, IDC_EDIT_SUPPDEV_DUET, m_szSupDevDuet);
	DDX_Check(pDX, IDC_CHECK_ENABLE_SQUEEZE, m_bEnableSqueeze);
	DDX_Text(pDX, IDC_EDIT_CAL_LAYER_GFX, m_nLayerGfx);
	DDX_Text(pDX, IDC_EDIT_CAL_LAYER_SQUEEZE, m_nLayerSqueeze);
	DDX_Text(pDX, IDC_EDIT_READYPORT, m_nReadyPort);
	DDX_Text(pDX, IDC_EDIT_READYIP, m_szReadyIP);
	DDV_MaxChars(pDX, m_szReadyIP, 15);
	DDX_Check(pDX, IDC_CHECK_VERBOSE, m_bVerboseLogging);
	DDX_Text(pDX, IDC_EDIT_D_CRAWLFMT, m_szDedicationFormatString);
	DDX_Text(pDX, IDC_EDIT_D_CRAWLSPD, m_nDedicationCrawlRate);
	DDV_MinMaxInt(pDX, m_nDedicationCrawlRate, 5, 200);
	DDX_Text(pDX, IDC_EDIT_D_CRAWLPCNT, m_nDedicationFinishPercent);
	DDV_MinMaxInt(pDX, m_nDedicationFinishPercent, 0, 200);
	DDX_Text(pDX, IDC_EDIT_D_ZOOMIN, m_nDedicationZoomInFrames);
	DDV_MinMaxInt(pDX, m_nDedicationZoomInFrames, 0, 350);
	DDX_Text(pDX, IDC_EDIT_D_ZOOMOUT, m_nDedicationZoomOutFrames);
	DDV_MinMaxInt(pDX, m_nDedicationZoomOutFrames, 0, 350);
	DDX_Text(pDX, IDC_EDIT_RD_CRAWLFMT, m_szReplyDedFormatString);
	DDX_Text(pDX, IDC_EDIT_RD_CRAWLSPD, m_nReplyDedCrawlRate);
	DDV_MinMaxInt(pDX, m_nReplyDedCrawlRate, 5, 200);
	DDX_Text(pDX, IDC_EDIT_RD_CRAWLPCNT, m_nReplyDedFinishPercent);
	DDV_MinMaxInt(pDX, m_nReplyDedFinishPercent, 0, 200);
	DDX_Text(pDX, IDC_EDIT_RR_CRAWLFMT, m_szReplyFormatString);
	DDX_Text(pDX, IDC_EDIT_RR_CRAWLSPD, m_nReplyCrawlRate);
	DDV_MinMaxInt(pDX, m_nReplyCrawlRate, 5, 200);
	DDX_Text(pDX, IDC_EDIT_RR_CRAWLPCNT, m_nReplyFinishPercent);
	DDV_MinMaxInt(pDX, m_nReplyFinishPercent, 0, 200);
	DDX_Text(pDX, IDC_EDIT_RD_ZOOMIN, m_nReplyDedZoomInFrames);
	DDV_MinMaxInt(pDX, m_nReplyDedZoomInFrames, 0, 350);
	DDX_Text(pDX, IDC_EDIT_RD_FLIP, m_nReplyFlipFrames);
	DDV_MinMaxInt(pDX, m_nReplyFlipFrames, 0, 350);
	DDX_Text(pDX, IDC_EDIT_RR_ZOOMOUT, m_nReplyZoomOutFrames);
	DDV_MinMaxInt(pDX, m_nReplyZoomOutFrames, 0, 350);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDedicateLiveSettings, CDialog)
	//{{AFX_MSG_MAP(CDedicateLiveSettings)
	ON_WM_SYSCOMMAND()
	ON_WM_QUERYDRAGICON()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
*/
/////////////////////////////////////////////////////////////////////////////
// CDedicateLiveSettings message handlers
/*
void CDedicateLiveSettings::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	if ((nID & 0xFFF0) == IDM_IMPORT)
	{
		CDLCatImportExportDlg dlg;
		dlg.m_bImport = TRUE;
		if(dlg.DoModal()==IDOK)
		{
			FILE* fp;
			fp = fopen(dlg.m_szFileName, "rt");
			if(fp)
			{
				char buffer[4096]; // maxlen of one line!
				BOOL bFoundAll=FALSE;
				CTextUtils tu;
				CString szLine;
				int nItemsSoFar = 0;
				while((!feof(fp))&&(!bFoundAll))
				{
					if(fgets((char*)buffer, 4096, fp))
					{
						if(buffer[0]!=';') // skip comments
						{
							CString szFields[8];// only need 5
							// we are expecting:
							szLine.Format("%s",buffer);
							szLine.TrimLeft(); szLine.TrimRight();
							int	nNumFields = tu.breakStringOnSepChar(',', szLine, szFields, 8);
							if(nNumFields>3) // else malformed
							{
//								m_CatInfo[nItemsSoFar].nIndex = atoi(szFields[0]);
								szFields[0].TrimLeft(); szFields[0].TrimRight();
								m_CatInfo[nItemsSoFar].szName = szFields[0];
								m_CatInfo[nItemsSoFar].nIconChar = atoi(szFields[1]);
//								m_CatInfo[nItemsSoFar].nIconRelX = atoi(szFields[3]);
//								m_CatInfo[nItemsSoFar].nIconRelY = atoi(szFields[4]);
								nItemsSoFar++;
								if(nItemsSoFar>=DL_MAXCATS) bFoundAll=TRUE;
							}
						}
					}
				}
				m_nNumCategories = nItemsSoFar;
				Settings(WRITE);
			}
			else
			{
				AfxMessageBox("Could not open file for reading.", MB_ICONERROR);
			}
			fclose(fp);
		}
	}
	else
	if ((nID & 0xFFF0) == IDM_EXPORT)
	{
		CDLCatImportExportDlg dlg;
		dlg.m_bImport = FALSE;
		if(dlg.DoModal()==IDOK)
		{
			FILE* fp;
			fp = fopen(dlg.m_szFileName, "wt");
			if(fp)
			{
				for(int i=0; i<m_nNumCategories;i++)
				{
					fprintf(fp, "%s,%d\n",
//						m_CatInfo[i].nIndex,
						m_CatInfo[i].szName,
//						m_CatInfo[i].szFont; // we only cache one font, so....
						m_CatInfo[i].nIconChar//,
//						m_CatInfo[i].nIconSize,
//						m_CatInfo[i].nIconRelX,
//						m_CatInfo[i].nIconRelY
						);
				}
			}
			else
			{
				AfxMessageBox("Could not open file for writing.", MB_ICONERROR);
			}
			fclose(fp);
		}
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

void CDedicateLiveSettings::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}


// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDedicateLiveSettings::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


void CDedicateLiveSettings::OnOK() 
{
	if(!UpdateData(TRUE)) 
		return;

	if(m_nInitListenPort!=m_nListenPort)
	{
		if(AfxMessageBox("Are you sure you want to change the listening port?",MB_YESNO|MB_ICONQUESTION)!=IDYES) return;
	}
	
	Settings(WRITE); //write to reg

	if(m_nInitListenPort!=m_nListenPort)
	{
// uncomment for a server thread: if settings for the server change (listen port) you must shut down the thread and start a new one
//		g_threadHelper.SetThreadCommand(ISMDLL_SHOWTHREAD_KILL);
//		Sleep(1000); // let the thread kill itself
//		g_threadHelper.SetThreadCommand(ISMDLL_SHOWTHREAD_BEGIN);
//		AfxBeginThread(ServerThreadProcess, &g_threadHelper);
	}

	CDialog::OnOK();
}

void CDedicateLiveSettings::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

BOOL CDedicateLiveSettings::OnInitDialog() 
{
//	AfxMessageBox("on init dlg");
	CDialog::OnInitDialog();
//	AfxMessageBox("after main init");
	Settings(READ);
	CString szTitle; szTitle.Format("%s Settings",m_szShowName);
	SetWindowText(szTitle);

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
// dl specific
		pSysMenu->AppendMenu(MF_SEPARATOR);
		pSysMenu->AppendMenu(MF_STRING, IDM_IMPORT, "Import categories...");
		pSysMenu->AppendMenu(MF_STRING, IDM_EXPORT, "Export categories...");

	
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon


	m_nInitListenPort=m_nListenPort; // check for changes in this!

	UpdateData(FALSE);

	return TRUE;  
}
*/
void CDedicateLiveSettings::Settings(BOOL bRead)
{

	CWinApp *pWinApp = AfxGetApp();
	CString foo, szTemp; 
	int i=0;
	CTextUtils tu;

	if (bRead) // if reading the registry
	{
		//public data for ISM, set it here
//		g_showdata.nShowType = ISM_SHOWTYPE_PERIODIC; // eternal or periodic
//		g_showdata.nShowType = ISM_SHOWTYPE_ETERNAL;

//		strcpy(g_showdata.szShowName,pWinApp->GetProfileString("Main Settings", "ShowName", "DedicateLive"));

//		m_szShowName = g_showdata.szShowName;

		
		m_szShowName =  pWinApp->GetProfileString("Main Settings", "ShowName", "DedicateLive");


/*
		for (i=0; i<GDM_MAXDEVICEDLLS ;i++)
		{
			foo.Format("SupportedDevice%1x",i);
			strcpy(g_showdata.szDeviceName[i], pWinApp->GetProfileString("Device Settings", foo,""));
		}
*/		// after a read, you must assign what devices are what:
		// abitrary pick device 0 as Duet and device 1 as other:
		m_szSupDevDuet = pWinApp->GetProfileString("Device Settings", foo,"");//g_showdata.szDeviceName[0];

		// in this sample there are only 2 devices.  No need to do any more assignment

    m_nListenPort = pWinApp->GetProfileInt("Main Settings", "ListenPort",10888);
    m_nReadyPort = pWinApp->GetProfileInt("Main Settings", "ReadyPort", 10561);
    m_szReadyIP = pWinApp->GetProfileString("Main Settings", "ReadyIP", "0.0.0.0");
    
    m_bEnableSqueeze = pWinApp->GetProfileInt("Main Settings", "Enable Squeeze", 0);
    m_nLayerGfx = pWinApp->GetProfileInt("Main Settings", "LayerGfx",2);
    m_nLayerSqueeze = pWinApp->GetProfileInt("Main Settings", "LayerSqueeze", 3);
		
		//http data
//		m_szURL = pWinApp->GetProfileString("HTTP Settings", "URL","http://");


		//email alerts
		m_szEmailTo = pWinApp->GetProfileString("Email Settings", "EmailTo","");
		m_szEmailIP = pWinApp->GetProfileString("Email Settings", "EmailIP","0.0.0.0");
//		m_nEmailPort= pWinApp->GetProfileInt("Email Settings", "EmailPort",ISMDLL_DEF_EMAIL_PORT);

		//net alerts
		m_szNetAlertIPs = pWinApp->GetProfileString("Net Alert Settings", "NetIPs","0.0.0.0:10777");
	
		// file rotation
		m_szFileRotSpec= pWinApp->GetProfileString("File Settings", "RotationSpec", "YM");
		m_szFileCustNameSpec= pWinApp->GetProfileString("File Settings", "CustomNameSpec", "");


		m_bVerboseLogging = pWinApp->GetProfileInt("Main Settings", "Verbose", FALSE);
		// category information
		m_nNumCategories = pWinApp->GetProfileInt("Category Settings", "NumCategories", 0);
		for (i=0; i<min(m_nNumCategories,DL_MAXCATS); i++)
		{
			foo.Format("CategoryInfo_%02d",i);
			szTemp = pWinApp->GetProfileString("Category Settings", foo, "");
			CString szFields[DL_MAXFIELDS];
			int nItems = tu.breakStringOnSepChar('|', szTemp, szFields, DL_MAXFIELDS);
//			m_CatInfo[i].nIndex=atoi(szFields[INFO_CAT_FIELD_CATNUM]);
			m_CatInfo[i].szName=szFields[INFO_CAT_FIELD_CATNAME];
			m_CatInfo[i].szName.TrimLeft(); m_CatInfo[i].szName.TrimRight();
//			m_CatInfo[i].szFont=szFields[INFO_CAT_FIELD_FONT];
//			m_CatInfo[i].szFont.TrimLeft(); m_CatInfo[i].szFont.TrimRight();
			m_CatInfo[i].nIconChar=atoi(szFields[INFO_CAT_FIELD_ICON_CHAR]);
//			m_CatInfo[i].nIconSize=atoi(szFields[INFO_CAT_FIELD_ICON_SIZE]);
//			m_CatInfo[i].nIconRelX=atoi(szFields[INFO_CAT_FIELD_ICON_RELX]);
//			m_CatInfo[i].nIconRelY=atoi(szFields[INFO_CAT_FIELD_ICON_RELY]);
		}

		m_szColors[DL_COLOR_ORANGE] = pWinApp->GetProfileString("Font and Color Settings", "Color0", "732b50ff");
		m_szColors[DL_COLOR_WHITE]	= pWinApp->GetProfileString("Font and Color Settings", "Color1", "ebebebff");
		m_szColors[DL_COLOR_GREY]		= pWinApp->GetProfileString("Font and Color Settings", "Color2", "f4a8c3ff");
		m_szColors[DL_COLOR_PURPLE] = pWinApp->GetProfileString("Font and Color Settings", "Color3", "a83f75ff");

		m_szFont[0] = pWinApp->GetProfileString("Font and Color Settings", "Font0", "HelveticaRounded Bold");
		m_szFont[1] = pWinApp->GetProfileString("Font and Color Settings", "Font1", "Arial Black");
		m_szFont[2] = pWinApp->GetProfileString("Font and Color Settings", "Font2", "HelveticaRounded Bold");
		m_szFont[3] = pWinApp->GetProfileString("Font and Color Settings", "Font3", "HelveticaRounded Bold");

		foo.Format("1|0|%d|315.0|5.0|0",ecalDropShadow);
		m_szFontEdge[0] = pWinApp->GetProfileString("Font and Color Settings", "FontEdge0", foo);
		m_szFontEdge[1] = pWinApp->GetProfileString("Font and Color Settings", "FontEdge1", "0");
		m_szFontEdge[2] = pWinApp->GetProfileString("Font and Color Settings", "FontEdge2", "0");
		foo.Format("1|0|%d|315.0|3.0|0",ecalDropShadow);
		m_szFontEdge[3] = pWinApp->GetProfileString("Font and Color Settings", "FontEdge3", foo);

		m_nFontSize[0] = pWinApp->GetProfileInt("Font and Color Settings", "FontSize0", 24);
		m_nFontSize[1] = pWinApp->GetProfileInt("Font and Color Settings", "FontSize1", 28);
		m_nFontSize[2] = pWinApp->GetProfileInt("Font and Color Settings", "FontSize2", 20);
		m_nFontSize[3] = pWinApp->GetProfileInt("Font and Color Settings", "FontSize3", 22);

		m_nFontStyle[0] = pWinApp->GetProfileInt("Font and Color Settings", "FontStyle0", ecalNormal);
		m_nFontStyle[1] = pWinApp->GetProfileInt("Font and Color Settings", "FontStyle1", ecalItalic);
		m_nFontStyle[2] = pWinApp->GetProfileInt("Font and Color Settings", "FontStyle2", ecalNormal);
		m_nFontStyle[3] = pWinApp->GetProfileInt("Font and Color Settings", "FontStyle3", ecalNormal);

		m_szIconFont = pWinApp->GetProfileString("Font and Color Settings", "IconFont", "ZapfDingbats BT");
		m_nIconFontSize = pWinApp->GetProfileInt("Font and Color Settings", "IconFontSize", 48);
		foo.Format("1|0|%d|315.0|3.0|0",ecalDropShadow);
		m_szIconFontEdge = pWinApp->GetProfileString("Font and Color Settings", "IconFontEdge", foo);



		//CAL
		m_bAllowExpiry= pWinApp->GetProfileInt("CAL Settings", "AllowExpiry", 0);

		//dedications
		m_sizeDedicationImgRgn.cx = pWinApp->GetProfileInt("CAL Settings", "DedicationImgRgnX",424);
		m_sizeDedicationImgRgn.cy = pWinApp->GetProfileInt("CAL Settings", "DedicationImgRgnY", 97);

    m_nDedicationCrawlRate = pWinApp->GetProfileInt("CAL Settings", "DedicationCrawlRate", 100);
		m_szDedicationFormatString = pWinApp->GetProfileString("CAL Settings", "DedicationFormatString", "To: %N, %L / From %n, %l / \"%D\"");
		m_nDedicationWidth = pWinApp->GetProfileInt("CAL Settings", "DedicationWidth", 290);
		m_nDedicationFinishPercent = pWinApp->GetProfileInt("CAL Settings", "DedicationFinishPercent", 100);
		m_szDedicationBackplateFile = pWinApp->GetProfileString("CAL Settings", "DedicationBackplateFile", "DefaultData\\DedicateLive\\dedframe-generic-v2.tga");
		m_nDedicationZoomInFrames = pWinApp->GetProfileInt("CAL Settings", "DedicationZoomInFrames", 5);
		m_nDedicationZoomOutFrames = pWinApp->GetProfileInt("CAL Settings", "DedicationZoomOutFrames", 5);
		m_szSourceIconFile0 = pWinApp->GetProfileString("CAL Settings", "DedicationSourceIconFile", "DefaultData\\DedicateLive\\cellPhone_1.tga");

		//replies
		m_sizeReplyImgRgn.cx = pWinApp->GetProfileInt("CAL Settings", "ReplyImgRgnX", 438);
		m_sizeReplyImgRgn.cy = pWinApp->GetProfileInt("CAL Settings", "ReplyImgRgnY", 100);

    m_nReplyDedCrawlRate = pWinApp->GetProfileInt("CAL Settings", "ReplyDedCrawlRate", 100);
		m_szReplyDedFormatString = pWinApp->GetProfileString("CAL Settings", "ReplyDedFormatString", "To: %N, %L / From %n, %l / \"%D\"");
		m_nReplyDedWidth = pWinApp->GetProfileInt("CAL Settings", "ReplyDedWidth", 298);
		m_nReplyDedFinishPercent = pWinApp->GetProfileInt("CAL Settings", "ReplyDedFinishPercent", 100);
		m_szReplyDedBackplateFile = pWinApp->GetProfileString("CAL Settings", "ReplyDedBackplateFile", "DefaultData\\DedicateLive\\replyframe-v2.tga");
		m_nReplyDedZoomInFrames = pWinApp->GetProfileInt("CAL Settings", "ReplyDedZoomInFrames", 5);
		m_nReplyFlipFrames = pWinApp->GetProfileInt("CAL Settings", "ReplyFlipFrames", 5);

    m_nReplyCrawlRate = pWinApp->GetProfileInt("CAL Settings", "ReplyCrawlRate", 100);
		m_szReplyFormatString = pWinApp->GetProfileString("CAL Settings", "ReplyFormatString", "To: %n, %l / From: %N, %L / \"%R\"");
		m_nReplyWidth = pWinApp->GetProfileInt("CAL Settings", "ReplyWidth", 298);
 		m_nReplyFinishPercent = pWinApp->GetProfileInt("CAL Settings", "ReplyFinishPercent", 100);
		m_szReplyBackplateFile = pWinApp->GetProfileString("CAL Settings", "ReplyBackplateFile", "DefaultData\\DedicateLive\\replyframe-rev-v2.tga");
		m_nReplyZoomOutFrames = pWinApp->GetProfileInt("CAL Settings", "ReplyZoomOutFrames", 5);
 
    g_data.m_nTimingOffsetMS_D  = (10 * m_nDedicationFinishPercent * m_nDedicationWidth)/m_nDedicationCrawlRate + ((m_nDedicationZoomInFrames+m_nDedicationZoomOutFrames)*1000)/30; 
    g_data.m_nTimingOffsetMS_R1 = (10 * m_nReplyDedFinishPercent * m_nReplyDedWidth)/m_nReplyDedCrawlRate + ((m_nReplyDedZoomInFrames)*1000)/30; 
    g_data.m_nTimingOffsetMS_R2 = (10 * m_nReplyFinishPercent * m_nReplyWidth)/m_nReplyCrawlRate + ((m_nReplyFlipFrames+m_nReplyZoomOutFrames)*1000)/30; 

		//positioning
		m_sizePosOffsets[0].cx = pWinApp->GetProfileInt("Positioning", "Position_1_X",37);	//65
		m_sizePosOffsets[0].cy = pWinApp->GetProfileInt("Positioning", "Position_1_Y",40);	//100
		m_sizePosOffsets[1].cx = pWinApp->GetProfileInt("Positioning", "Position_2_X",266); //266
		m_sizePosOffsets[1].cy = pWinApp->GetProfileInt("Positioning", "Position_2_Y",60);	//100
		m_sizePosOffsets[2].cx = pWinApp->GetProfileInt("Positioning", "Position_3_X",266);	//266
		m_sizePosOffsets[2].cy = pWinApp->GetProfileInt("Positioning", "Position_3_Y",315);	//315

		m_sizeRPosOffsets[0].cx = pWinApp->GetProfileInt("Positioning", "Position_R1_X",52);		//65
		m_sizeRPosOffsets[0].cy = pWinApp->GetProfileInt("Positioning", "Position_R1_Y",40);		//100
		m_sizeRPosOffsets[1].cx = pWinApp->GetProfileInt("Positioning", "Position_R2_X",222);	//266
		m_sizeRPosOffsets[1].cy = pWinApp->GetProfileInt("Positioning", "Position_R2_Y",60);		//100
		m_sizeRPosOffsets[2].cx = pWinApp->GetProfileInt("Positioning", "Position_R3_X",222);	//266
		m_sizeRPosOffsets[2].cy = pWinApp->GetProfileInt("Positioning", "Position_R3_Y",312);	//315
	}
	else //write
	{
		//public data for ISM, set it here
//		g_showdata.nShowType = ISM_SHOWTYPE_PERIODIC; //do not write, do not allow change
//		g_showdata.nShowType = ISM_SHOWTYPE_ETERNAL; //do not write, do not allow change

//		strcpy(g_showdata.szShowName,m_szShowName);


		// after any update of the UI, you must re-assign what devices are what:
		// abitrary pick device 0 as Duet and device 1 as Louth:
//		strcpy(g_showdata.szDeviceName[0],m_szSupDevDuet);

		for (i=0; i<1/*ISM_MAXDEVICEDLLS*/ ;i++)
		{
			foo.Format("SupportedDevice%1x",i);
			pWinApp->WriteProfileString("Device Settings",foo ,m_szSupDevDuet);//g_showdata.szDeviceName[i]);
		}

		

	//main
		pWinApp->WriteProfileString("Main Settings", "ShowName", m_szShowName);//g_showdata.szShowName);
		pWinApp->WriteProfileInt("Main Settings", "ListenPort",m_nListenPort);
    pWinApp->WriteProfileInt("Main Settings", "ReadyPort",m_nReadyPort);
    pWinApp->WriteProfileString("Main Settings", "ReadyIP", m_szReadyIP);
    
    pWinApp->WriteProfileInt("Main Settings", "Enable Squeeze", m_bEnableSqueeze);
    pWinApp->WriteProfileInt("Main Settings", "LayerGfx", m_nLayerGfx);
    pWinApp->WriteProfileInt("Main Settings", "LayerSqueeze", m_nLayerSqueeze);

		//http data
//		pWinApp->WriteProfileString("HTTP Settings", "URL",m_szURL);

		//email alerts
		pWinApp->WriteProfileString("Email Settings", "EmailTo",m_szEmailTo);
		pWinApp->WriteProfileString("Email Settings", "EmailIP",m_szEmailIP);
//		pWinApp->WriteProfileInt("Email Settings", "EmailPort",m_nEmailPort);

		// file rotation
		pWinApp->WriteProfileString("File Settings", "RotationSpec", m_szFileRotSpec);
		pWinApp->WriteProfileString("File Settings", "CustomNameSpec", m_szFileCustNameSpec);

				//net alerts
		pWinApp->WriteProfileString("Net Alert Settings", "NetIPs",m_szNetAlertIPs);

		pWinApp->WriteProfileInt("Main Settings", "Verbose", m_bVerboseLogging);

		// category information
		pWinApp->WriteProfileInt("Category Settings", "NumCategories", m_nNumCategories);
		for (i=0; i<min(m_nNumCategories,DL_MAXCATS); i++)
		{
			foo.Format("CategoryInfo_%02d",i);
//			m_CatInfo[i].szFont.TrimLeft(); m_CatInfo[i].szFont.TrimRight();
			m_CatInfo[i].szName.TrimLeft(); m_CatInfo[i].szName.TrimRight();
			szTemp.Format("%s|%d",
//					m_CatInfo[i].nIndex,
					m_CatInfo[i].szName,
//					m_CatInfo[i].szFont,
					m_CatInfo[i].nIconChar//,
//					m_CatInfo[i].nIconSize,
//					m_CatInfo[i].nIconRelX,
//					m_CatInfo[i].nIconRelY
				);
			pWinApp->WriteProfileString("Category Settings", foo, szTemp);
		}

		for (i=0; i<DL_MAXCOLORS; i++)
		{
			foo.Format("Color%1d",i);
			pWinApp->WriteProfileString("Font and Color Settings", foo, m_szColors[i]);
		}

		for (i=0; i<DL_MAXFONTS; i++)
		{
			foo.Format("Font%1d",i);
			pWinApp->WriteProfileString("Font and Color Settings", foo, m_szFont[i]);
			foo.Format("FontEdge%1d",i);
			pWinApp->WriteProfileString("Font and Color Settings", foo, m_szFontEdge[i]);
			foo.Format("FontSize%1d",i);
			pWinApp->WriteProfileInt("Font and Color Settings", foo, m_nFontSize[i]);
			foo.Format("FontStyle%1d",i);
			pWinApp->WriteProfileInt("Font and Color Settings", foo, m_nFontStyle[i]);
		}

		pWinApp->WriteProfileString("Font and Color Settings", "IconFont", m_szIconFont);
		pWinApp->WriteProfileString("Font and Color Settings", "IconFontEdge", m_szIconFontEdge);
		pWinApp->GetProfileInt("Font and Color Settings", "IconFontSize", m_nIconFontSize);



// CAL
		pWinApp->WriteProfileInt("CAL Settings", "AllowExpiry", m_bAllowExpiry);

		//dedications
		pWinApp->WriteProfileInt("CAL Settings", "DedicationImgRgnX", m_sizeDedicationImgRgn.cx);
		pWinApp->WriteProfileInt("CAL Settings", "DedicationImgRgnY", m_sizeDedicationImgRgn.cy);

    pWinApp->WriteProfileInt("CAL Settings", "DedicationCrawlRate", m_nDedicationCrawlRate);
		pWinApp->WriteProfileString("CAL Settings", "DedicationFormatString", m_szDedicationFormatString);
		pWinApp->WriteProfileInt("CAL Settings", "DedicationWidth", m_nDedicationWidth);
		pWinApp->WriteProfileInt("CAL Settings", "DedicationFinishPercent", m_nDedicationFinishPercent);
		pWinApp->WriteProfileString("CAL Settings", "DedicationBackplateFile", m_szDedicationBackplateFile );
		pWinApp->WriteProfileInt("CAL Settings", "DedicationZoomInFrames", m_nDedicationZoomInFrames );
		pWinApp->WriteProfileInt("CAL Settings", "DedicationZoomOutFrames", m_nDedicationZoomOutFrames );
		pWinApp->WriteProfileString("CAL Settings", "DedicationSourceIconFile", m_szSourceIconFile0);

		//replies
		pWinApp->WriteProfileInt("CAL Settings", "ReplyImgRgnX", m_sizeReplyImgRgn.cx );
		pWinApp->WriteProfileInt("CAL Settings", "ReplyImgRgnY", m_sizeReplyImgRgn.cy );

    pWinApp->WriteProfileInt("CAL Settings", "ReplyDedCrawlRate", m_nReplyDedCrawlRate );
		pWinApp->WriteProfileString("CAL Settings", "ReplyDedFormatString", m_szReplyDedFormatString );
		pWinApp->WriteProfileInt("CAL Settings", "ReplyDedWidth", m_nReplyDedWidth );
		pWinApp->WriteProfileInt("CAL Settings", "ReplyDedFinishPercent", m_nReplyDedFinishPercent );
		pWinApp->WriteProfileString("CAL Settings", "ReplyDedBackplateFile", m_szReplyDedBackplateFile);
		pWinApp->WriteProfileInt("CAL Settings", "ReplyDedZoomInFrames", m_nReplyDedZoomInFrames );
		pWinApp->WriteProfileInt("CAL Settings", "ReplyFlipFrames", m_nReplyFlipFrames);

    pWinApp->WriteProfileInt("CAL Settings", "ReplyCrawlRate", m_nReplyCrawlRate );
		pWinApp->WriteProfileString("CAL Settings", "ReplyFormatString", m_szReplyFormatString );
		pWinApp->WriteProfileInt("CAL Settings", "ReplyWidth", m_nReplyWidth );
 		pWinApp->WriteProfileInt("CAL Settings", "ReplyFinishPercent", m_nReplyFinishPercent);
		pWinApp->WriteProfileString("CAL Settings", "ReplyBackplateFile", m_szReplyBackplateFile );
		pWinApp->WriteProfileInt("CAL Settings", "ReplyZoomOutFrames", m_nReplyZoomOutFrames );

		//positioning
		CString foo;
		for (int i=0; i<3; i++)
		{
			foo.Format("Position_%1d_X",i+1);
			pWinApp->WriteProfileInt("Positioning", foo,	m_sizePosOffsets[i].cx);
			foo.Format("Position_%1d_Y",i+1);
			pWinApp->WriteProfileInt("Positioning", foo,	m_sizePosOffsets[i].cy);
			foo.Format("Position_R%1d_X",i+1);
			pWinApp->WriteProfileInt("Positioning", foo,	m_sizeRPosOffsets[i].cx);
			foo.Format("Position_R%1d_Y",i+1);
			pWinApp->WriteProfileInt("Positioning", foo,	m_sizeRPosOffsets[i].cy);
		}
	}
}

