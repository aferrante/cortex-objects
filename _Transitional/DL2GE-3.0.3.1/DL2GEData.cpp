// DL2GEData.cpp: implementation of the CDL2GEData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "DL2GEData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// CDL2GEObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDL2GEObject::CDL2GEObject()
{
	InitializeCriticalSection(&m_critText);

	m_pszName		= NULL;
	m_usType		= CX_TYPE_UNDEF;

	// destinations within object
	m_ppszDestName		= NULL;
	m_ppszDestParams	= NULL;  // has to be generic for all types
	m_ulNumDest				= 0; // number of destinations.

	// dependencies within object
	m_ppszResourceName		= NULL;
	m_ppszResourceParams	= NULL;  // has to be generic for all types
	m_ulNumResources			= NULL; // number of dependency resources.

	// IP and ports
	m_pszHost				= NULL;
	m_usFilePort		= CX_PORT_INVALID;
	m_usCommandPort	= CX_PORT_INVALID;
	m_usStatusPort	= CX_PORT_INVALID;

	// cortex assigned status
	m_ulStatus	= CX_STATUS_UNINIT;
	m_ulOwner		= CX_OWNER_INVALID;

	// there is a negotiation to set the following from default values.
	m_ulStatusIntervalMS	= CX_TIME_PING;		// the interval, in milliseconds, at which we expect to hear status.
	m_ulFailureIntervalMS	= CX_TIME_FAIL;  // the interval, in milliseconds, at which we declare failure of object.

	// status received from object
	//m_timebLastStatus; // the time of the last status received (parseable)
	//m_timebLastTime;		// the last time received (object's local time)
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for object status
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
}

CDL2GEObject::~CDL2GEObject()
{
	Free();
	DeleteCriticalSection(&m_critText);
}

void CDL2GEObject::Free()
{
	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_ppszDestName)
	{
		unsigned long i=0;
		while(i<m_ulNumDest)
		{
			if(m_ppszDestName[i]) free(m_ppszDestName[i]); // must use malloc to allocate
			if(m_ppszDestParams[i]) free(m_ppszDestParams[i]); // must use malloc to allocate
			i++;
		}
		free(m_ppszDestName);
	}
	if(m_ppszResourceName)
	{
		unsigned long i=0;
		while(i<m_ulNumResources)
		{
			if(m_ppszResourceName[i]) free(m_ppszResourceName[i]); // must use malloc to allocate
			if(m_ppszResourceParams[i]) free(m_ppszResourceParams[i]); // must use malloc to allocate
			i++;
		}
		free(m_ppszResourceName);
	}
	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
}

char* CDL2GEObject::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CDL2GEObject::SetStatusText(char* pszText, unsigned long ulStatus, unsigned long ulStatusCounter)
{
	int nRV = CX_ERROR;
	if(pszText)
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter=ulStatusCounter;
			}
		}
		else
		{
			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter=ulStatusCounter;
		}
		LeaveCriticalSection(&m_critText);
	}
	return nRV;
}



//////////////////////////////////////////////////////////////////////
// CDL2GEData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDL2GEData::CDL2GEData()
{
	InitializeCriticalSection(&m_critText);
	m_usResourcePortLast	= CX_PORT_INVALID;
	m_usProcessPortLast		= CX_PORT_INVALID;

	m_ppObj = NULL;
	m_nNumObjects = 0;
	m_nNumResources = 0;  // this is for convenience only
	m_nNumProcesses = 0;  // this is for convenience only

	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebTick );
}

CDL2GEData::~CDL2GEData()
{
	if(m_ppObj)
	{
		int i=0;
		while(i<m_nNumObjects)
		{
			if(m_ppObj[i]) delete m_ppObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppObj; // delete array of pointers to objects, must use new to allocate
	}

	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);
}

char* CDL2GEData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CDL2GEData::SetStatusText(char* pszText, unsigned long ulStatus)
{
	int nRV = CX_ERROR;
	if(pszText)
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	return nRV;
}

