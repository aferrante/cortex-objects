#ifndef DEDICATELIVE_CMD_DEFINES_INCLUDED
#define DEDICATELIVE_CMD_DEFINES_INCLUDED

#define DEDICATELIVE_VERSION	"1.01e"

// standard CAL commands
#ifndef CMD_CONNECT
#define CMD_CONNECT            0x40
#define CMD_DISCONNECT		     0x41
#define CMD_SHUTDOWN		       0x42
#define CMD_USEVGA		         0x43
#define CMD_USEDUET		         0x44
#define CMD_DEMOMODE		       0x45
#define CMD_NONDEMOMODE		     0x46
#define CMD_CLEAR	             0x47
#endif

#define EXPIRY_THRESHOLD_MS 5000  // 5 seconds longer than estimate....

// show specific commands
#define CMD_DL_USEREVENT1			0xa1
#define CMD_DL_USEREVENT2			0xa2
#define CMD_DL_USEREVENT3			0xa3

#define TYPE_DEDICATION		0
#define TYPE_REPLY				1

#define INFO_CMD_CAT_ADD				0
#define INFO_CMD_CAT_DEL				1
#define INFO_CMD_CAT_CLR				2
#define INFO_CMD_CAT_GET				3

//#define INFO_CAT_FIELD_CATNUM			0
#define INFO_CAT_FIELD_CATNAME		0
//#define INFO_CAT_FIELD_FONT				2
#define INFO_CAT_FIELD_ICON_CHAR	1
//#define INFO_CAT_FIELD_ICON_SIZE	3
//#define INFO_CAT_FIELD_ICON_RELX	3
//#define INFO_CAT_FIELD_ICON_RELY	4

// field ordinals
#define DFIELD_DED_ID						0
#define DFIELD_CAT_NAME					1
#define DFIELD_SONG_ID					2
#define DFIELD_SOURCE_ID				3
#define DFIELD_SENDER_NAME			4
#define DFIELD_SENDER_LOC				5
#define DFIELD_RECVR_NAME				6
#define DFIELD_RECVR_LOC				7
#define DFIELD_DEDICATION				8

#define RFIELD_REP_ID						0
#define RFIELD_CAT_NAME					1
#define RFIELD_SONG_ID					2
#define RFIELD_DED_ID						3
#define RFIELD_SENDER_NAME			4
#define RFIELD_SENDER_LOC				5
#define RFIELD_RECVR_NAME				6
#define RFIELD_RECVR_LOC				7
#define RFIELD_DEDICATION				8
#define RFIELD_REPLY						9

#define DL_MAXFIELDS			15 // need 12 only
#define DL_MAXCATS				32 // max num categories.
#define DL_MAXCOLORS			4
#define DL_MAXFONTS				4

#define DL_COLOR_ORANGE		0
#define DL_COLOR_WHITE		1
#define DL_COLOR_GREY			2
#define DL_COLOR_PURPLE		3

#define DL_SUCCESS		0x00
#define DL_ERROR			0x01


// using T2A defines in: 
//#include "../../Devices/ISM_DatabaseT2/TextToAirDB2.h" // for data types
//#include "../../Playout/ISM_TextToAir/TextToAirDefines.h" // for CMD defs

typedef struct CategoryInfo_t
{
//	int nIndex;
	CString szName;
//	CString szFont; // we only cache one font, so....
	int nIconChar;
//	int nIconSize;
//	int nIconRelX;
//	int nIconRelY;
} CategoryInfo_t;



#endif // DEDICATELIVE_CMD_DEFINES_INCLUDED
