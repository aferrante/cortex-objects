#if !defined(AFX_DEDICATELIVESETTINGS_H__1BFF215B_A245_4A8D_B00E_3A664B40B299__INCLUDED_)
#define AFX_DEDICATELIVESETTINGS_H__1BFF215B_A245_4A8D_B00E_3A664B40B299__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DedicateLiveSettings.h : header file
//
#define ISMDLL_DEF_EMAIL_PORT 25

/////////////////////////////////////////////////////////////////////////////
// CDedicateLiveSettings dialog

class CDedicateLiveSettings //: public CDialog
{
// Construction
public:
	CDedicateLiveSettings(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDedicateLiveSettings)
//	enum { IDD = IDD_DEDICATELIVE_SETTINGS_DIALOG };
	CString	m_szEmailIP;
	CString	m_szEmailTo;
	int		m_nListenPort;
	CString	m_szShowName;
	CString	m_szNetAlertIPs;
	CString	m_szSupDevDuet;
	BOOL	m_bEnableSqueeze;
	int		m_nLayerGfx;
	int		m_nLayerSqueeze;
  int		m_nReadyPort;
	CString	m_szReadyIP;
	BOOL	m_bVerboseLogging;
	int m_nDedicationCrawlRate;
	CString m_szDedicationFormatString;
	int m_nDedicationFinishPercent;
	int m_nDedicationZoomInFrames;
	int m_nDedicationZoomOutFrames;
	int m_nReplyDedCrawlRate;
	CString m_szReplyDedFormatString;
	int m_nReplyDedFinishPercent;
	int m_nReplyDedZoomInFrames;
	int m_nReplyFlipFrames;
	int m_nReplyCrawlRate;
	CString m_szReplyFormatString;
	int m_nReplyFinishPercent;
	int m_nReplyZoomOutFrames;
	//}}AFX_DATA

	CString m_szFileRotSpec;
	CString m_szFileCustNameSpec;

	int m_nInitListenPort;

	//CAL settings
	BOOL m_bAllowExpiry;
//	int m_nCrawlRate;
//	CString m_szCrawlFormatString; // generic

	//Categories
	int m_nNumCategories;
	CategoryInfo_t m_CatInfo[DL_MAXCATS];

	CString m_szColors[DL_MAXCOLORS];

	CString m_szFont[DL_MAXFONTS];
	CString m_szFontEdge[DL_MAXFONTS];
	int     m_nFontSize[DL_MAXFONTS];
	int     m_nFontStyle[DL_MAXFONTS];

	CString m_szIconFont;
	CString m_szIconFontEdge;
	int     m_nIconFontSize;

// position offsets:
	CSize m_sizePosOffsets[3];
	CSize m_sizeRPosOffsets[3];

// dedications
	CSize m_sizeDedicationImgRgn;

//	int m_nDedicationCrawlRate;
//	CString m_szDedicationFormatString;
	int m_nDedicationWidth;
//	int m_nDedicationFinishPercent;
	CString m_szDedicationBackplateFile;
//	int m_nDedicationZoomInFrames;
//	int m_nDedicationZoomOutFrames;

	CString m_szSourceIconFile0;
// replies	
	CSize m_sizeReplyImgRgn;

//	int m_nReplyDedCrawlRate;
//	CString m_szReplyDedFormatString;
	int m_nReplyDedWidth;
//	int m_nReplyDedFinishPercent;
	CString m_szReplyDedBackplateFile;
//	int m_nReplyDedZoomInFrames;
//	int m_nReplyFlipFrames;

//	int m_nReplyCrawlRate;
//	CString m_szReplyFormatString;
	int m_nReplyWidth;
//	int m_nReplyFinishPercent;
	CString m_szReplyBackplateFile;
//	int m_nReplyZoomOutFrames;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDedicateLiveSettings)
	protected:
//	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:	
	void Settings(BOOL bRead);


protected:
//	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CDedicateLiveSettings)
//	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
//	afx_msg HCURSOR OnQueryDragIcon();
//	afx_msg void OnPaint();
//	virtual void OnOK();
//	virtual void OnCancel();
//	virtual BOOL OnInitDialog();
	//}}AFX_MSG
//	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEDICATELIVESETTINGS_H__1BFF215B_A245_4A8D_B00E_3A664B40B299__INCLUDED_)
