// DL2GESettings.h: interface for the CDL2GESettings class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DL2GESETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
#define AFX_DL2GESETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "DL2GEDefines.h"

class CDL2GESettings  
{
public:
	CDL2GESettings();
	virtual ~CDL2GESettings();

	char* m_pszName;  // familiar name of this instance.
	unsigned long m_ulMainMode;

	// ports
	unsigned short m_usFilePort;
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;

	// port ranges
	unsigned short m_usResourcePortMin;
	unsigned short m_usResourcePortMax;

	unsigned short m_usProcessPortMin;
	unsigned short m_usProcessPortMax;

	// The following flags determine what settings are loaded. 
	// The specifics do not need to be stored in cortex settings, they are pushed to the various objects.

	// messaging for DL2GE
	bool m_bUseLog;			// write a log file
	bool m_bUseEmail;		// send an email on failures (or commanded from remotes)
	bool m_bUseNetwork; // send a message to a remote host

	// messaging for remote objects
	bool m_bUseLogRemote;	// write a separate log file for each object

	//admin
	bool m_bUseAuthentication;	// user/password verification, otherwise totally open to all clients
	char* m_pszUserName;  // cortex master username
	char* m_pszPassword;	// cortex master password
	char* m_pszSecurityFile1;
	char* m_pszSecurityFile2;

	// backup
	bool m_bUseClone;			// spark a clone (unless a clone itself)
};

#endif // !defined(AFX_DL2GESETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
