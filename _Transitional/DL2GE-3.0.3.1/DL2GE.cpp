// DL2GE.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "DL2GEMain.h"
#include "DL2GE.h"
#include "DL2GEHandler.h"
#include "DL2GEDlg.h"
#include <process.h>
//#include "DL2GESettings.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// global control vars for main thread
extern bool g_bKillThread;
extern bool g_bThreadStarted;


//  necessary globals
CDedicateLiveSettings g_settings;
CDedicateLiveData g_data;
//extern CDedicateLiveApp theApp;
CDedicateLiveCore g_core;  // engine class
CDedicateLiveThreadHelpers g_threadHelper;
//extern CMessageDispatcher g_md;
//extern ISMShowData_t g_showdata;
CNetworking g_net;


/////////////////////////////////////////////////////////////////////////////
// CDL2GEApp

BEGIN_MESSAGE_MAP(CDL2GEApp, CWinApp)
	//{{AFX_MSG_MAP(CDL2GEApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDL2GEApp construction

CDL2GEApp::CDL2GEApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
//	m_ulFlags = CXWF_DEFAULT;
	m_pszSettingsURL = NULL;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CDL2GEApp object

CDL2GEApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CDL2GEApp initialization

BOOL CDL2GEApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	// uncomment the following to initialize RichEdit Controls
//	AfxInitRichEdit();

	// prevent multiple instances

	bool bAllowMultiple = false;
  if (m_lpCmdLine[0] != '\0')
  {
		if( (strstr(m_lpCmdLine, "/c")) || (strstr(m_lpCmdLine, "/C")) );  // clone mode.  Allow another instance (until we check later for existence of a listener)
		bAllowMultiple = true;
  }

	
	::CreateMutex(NULL,TRUE,m_pszExeName);
	if((GetLastError()==ERROR_ALREADY_EXISTS)&&(!bAllowMultiple)) return FALSE;  

  
	// uncomment the following to write PID to file (change filename)
  FILE *fp; fp = fopen("cortex.pid", "wt");
  if (fp) { fprintf(fp, "%ld\n", _getpid()); fclose(fp); } 


#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

  //SetRegistryKey("Video Design Interactive");  // can use this for licensing later on, or use a hidden file somewhere

	CDL2GEDlg* pDlg = new CDL2GEDlg();
	VERIFY(pDlg->Create());
//	pDlg->DoModal();

// handler window for systray
	CDL2GEHandler* pWnd = new CDL2GEHandler();
	pWnd->m_pMainDlg = pDlg;
	VERIFY(pWnd->Create());
	m_pMainWnd = pWnd;

	// here start the main (almost non-MFC) thread.
	// we can feed a pointer to the app into it to give it access to the status windows.
	// all settings are in the thread.
	g_bThreadStarted = false;
	if(_beginthread(DL2GEMainThread, 0, (void*) this)<0)
	{
		AfxMessageBox("Could not begin main thread!\nMust exit.");
		return FALSE;
	}

	return TRUE;
}

int CDL2GEApp::ExitInstance() 
{
	g_bKillThread=true;
	while(g_bThreadStarted) Sleep(1);
	if(m_pszSettingsURL) free(m_pszSettingsURL);
	return CWinApp::ExitInstance();
}
