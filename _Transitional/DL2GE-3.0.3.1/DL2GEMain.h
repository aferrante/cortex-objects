// DL2GEMain.h: interface for the CDL2GEMain class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DL2GEMAIN_H__2FA150AF_62FD_474D_9F15_E4CA67BA3807__INCLUDED_)
#define AFX_DL2GEMAIN_H__2FA150AF_62FD_474D_9F15_E4CA67BA3807__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../../../Common/MSG/msg.h"
#include "../../../Common/MSG/Messager.h"
#include "../../../Common/TXT/FileUtil.h"
#include "../../../Common/HTTP/HTTP10.h"  // includes CMessagingObject
#include "DL2GESettings.h"
#include "DL2GEData.h"


// global message function so that dialogs etc (external objects) can write to the log etc.
//void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations);

// global main thread - this is the business right here!
void DL2GEMainThread(void* pvArgs);

// cortex http thread - we pass in the main cortex object, do all the cgi programming and security checking.
void DL2GEHTTPThread(void* pvArgs);

void DL2GECommandHandlerThread(void* pvArgs);
void DL2GEStatusHandlerThread(void* pvArgs);

class CDL2GEMain  
{
public:
	CDL2GEMain();
	virtual ~CDL2GEMain();

	CMessager					m_msgr;				// main messager // the one and only messager object
	CDL2GESettings		m_settings;		// main mem storage for settings.  settings file objects are local to the DL2GEMainThread.
	CDL2GEData				m_data;				// internal variables
	CHTTP10						m_http;				// the main http file server object -  has an internal CNetUtil object, but...
	CNetUtil					m_net;				// have a separate one for the status and command services.
	// the security object is declared locally in the DL2GEMainThread thread, and a pointer to it is fed 
	// to the CHTTP10 object.

	//core http functions
	char*	DL2GETranslate(CHTTPHeader* pHeader, char* pszBuffer);				// apply cortex scripting language
	int		InterpretDirective(CHTTPHeader* pHeader, void** ppResult, char* pszInfo=NULL);		// parse cgi

	// core net functions
	SOCKET*		SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char SubCmd=CX_CMD_NULL, unsigned char* pucData=NULL, unsigned long ulDataLen=0, char* pchUser=NULL, char* pchPw=NULL);  // cortex initiates a request to an object server
	int		SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd=CX_CMD_NULL, unsigned char* pucData=NULL, unsigned long ulDataLen=0, char* pchUser=NULL, char* pchPw=NULL);		// cortex replies to an object server after receiving data. (usually ack or nak)
	int		SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd=CX_CMD_NULL, unsigned char* pucData=NULL, unsigned long ulDataLen=0, char* pchUser=NULL, char* pchPw=NULL);		// cortex answers a request from an object client

};

#endif // !defined(AFX_DL2GEMAIN_H__2FA150AF_62FD_474D_9F15_E4CA67BA3807__INCLUDED_)
