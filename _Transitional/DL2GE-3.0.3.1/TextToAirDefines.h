// defines
#ifndef TEXTTOAIR_DEFINES_INCLUDED
#define TEXTTOAIR_DEFINES_INCLUDED

#define TEXTTOAIR_VERSION	"2.01c"

#define CMD_READYPULSE         0x0f
#define CMD_READYPULSE_ACK     0x0a
#define READY_PORT             10667

#ifndef CMD_CONNECT
#define CMD_CONNECT            0x40
#define CMD_DISCONNECT		     0x41
#define CMD_SHUTDOWN		       0x42
#define CMD_USEVGA  		       0x43
#define CMD_USEDUET            0x44
#define CMD_DEMOMODE		       0x45
#define CMD_NONDEMOMODE		     0x46
#define CMD_CLEAR	             0x47
#endif

#define CMD_T2A_INIT						0x60
#define CMD_T2A_INTRO						0x61
#define CMD_T2A_DATA						0x62
#define CMD_T2A_OUTRO						0x63
#define CMD_T2A_DURATION				0x64
#define CMD_T2A_UNINIT					0x65
#define CMD_T2A_INFO						0x66  // supplemental info (data format show dependent) to be passed to GDM module

#define T2A_DURATIONTIMEOUT			8000 // if an estimate is a certain number of ms, if we dont geta ready pulse within this estimate + this define # of ms, we make it "ready" anyway
#define T2A_DURATIONTHRESHOLD		6000 // there has to be the estimate + this define milliseconds of time left in the window for us to play an item.
#define T2A_GFX_INIT_TIMEOUT		60 // in seconds
#define T2A_GFX_UNINIT_TIMEOUT	60 // in seconds
#define T2A_MAXOPENSANDCLOSES		256  //total
#define T2A_MAXDATAITEMS				600  // per clip (active comments), and max archive items
#define T2A_MAXMARKS						6000  // total!!!! 
//#define T2A_MAXLOGOS						128  // 
#define T2A_LTRUNCTIME 0x00ffffff

#define T2A_SHOWSTATE_INIT					0x00
#define T2A_SHOWSTATE_WAITFORID			0x01
#define T2A_SHOWSTATE_WAITFORINTRO	0x02
#define T2A_SHOWSTATE_INTRO					0x03
#define T2A_SHOWSTATE_WAITFOROUTRO	0x04
#define T2A_SHOWSTATE_OUTRO					0x05
#define T2A_SHOWSTATE_ENDINGSHOW		0x06
#define T2A_SHOWSTATE_FAILED				0x07

#define T2A_ISMSHOWSTATE_DURING			0x80

#define T2A_HARRIS_MAXGETEVENTS 1200 // <1232 (28 packets) 
#define T2A_HARRIS_TIMEOUT 3000
#define T2A_HARRIS_MAXINDEX_DELTA 120 // <132 (3 packets)

#define T2A_TIME_NOTYET					0
#define T2A_TIME_TRIGGER				1
#define T2A_TIME_ID_NOTMATCHED	2

#define T2A_CONTINGENCY_CONTINUE	0
#define T2A_CONTINGENCY_USEFILE		1

#define CMD_T2A_OK					0x00 // final success
#define CMD_T2A_MORE				0x01 // cmd success, but more data to come (ask for more)
#define CMD_T2A_ERROR				0x10 // generic error
#define CMD_T2A_ERROR_DB		0x01 // db error

// server commmands
#define CMD_T2A_GETSTATETABLE				0xf0

#define CMD_T2A_SETMARK							0x80 // sets a mark for an item
#define CMD_T2A_GETMARKS						0x81 // gets all editorial marks for a show number
#define CMD_T2A_CLRMARKS						0x82 // clears all editorial marks for a show number (if blank, all marks are deleted)

#define CMD_T2A_GETARCHIVESHOWS			0x83 // gets all archived shows (list)
#define CMD_T2A_CLRARCHIVESHOW			0x84 // deletes an archived show (data = ID, blank = all)
#define CMD_T2A_UNARCHIVESHOW				0x85 // unarchives all text items for a particular show (moves it to archive and clears status, that's all)
#define CMD_T2A_GETARCHIVETEXT			0x86 // gets all archived text items for a show

#define CMD_T2A_GETACTIVETEXT				0x90 // gets all active text items for a show
#define CMD_T2A_SNDACTIVETEXT				0x91 // send text data to DB (insert before - if blank data, put at end)
#define CMD_T2A_REMOVEACTIVETEXT		0x92 // remove text data from DB with ID in data
#define CMD_T2A_MOVEACTIVETEXT			0x93 // move text data in DB to before item indicated in data...
#define CMD_T2A_CLRALLTEXT					0x94 // clears all text (active and played) items for a show
#define CMD_T2A_CLRACTIVETEXTFORCLIP	0x95 // clears all active text items for a particular clip ID within a show
#define CMD_T2A_GETPLAYEDTEXT				0x96 // gets all played text items for a show

#define CMD_T2A_GETIMAGES						0xb0 // gets images (aux gfx) (list)
#define CMD_T2A_SETIMAGE						0xb1 // sets image (aux gfx) info
#define CMD_T2A_DELIMAGE						0xb2 // deletes image (aux gfx)

#define CMD_T2A_GETOPENSCLOSES			0xb5 // gets opens and closes (list)
#define CMD_T2A_SETOPENSCLOSE				0xb6 // sets open or close info
#define CMD_T2A_DELOPENSCLOSE				0xb7 // deletes open or close

#define CMD_T2A_GETLOUTH						0xba // gets Louth IDs from now until end of list or show close if in a show, nothing if not in a show (list) 

#define CMD_T2A_DBCMD								0xc0 // gets/sets/del formatted DB info...  general purpose.
#define DBCMD_INSERT		0
#define DBCMD_DELETE    1
#define DBCMD_RETRIVE   2
#define DBCMD_PRITABLE		0
#define DBCMD_SECTABLE    1
#define DBCMD_ARCTABLE    2

#define CMD_T2A_MANUAL_STARTSHOW		0xd0 // starts a show manually
#define CMD_T2A_MANUAL_ENDSHOW			0xd1 // ends a show manually
#define CMD_T2A_MANUAL_MODE					0xd2 // with zero data, returns triggering to auto, else goes manual, enabling manual text triggering
#define CMD_T2A_MANUAL_TRIGGERTEXT	0xd3 // triggers text manually
#define CMD_T2A_MANUAL_REINITDUET		0xd4 // reinitializes Duet
#define CMD_T2A_MANUAL_GETCLIPFILTER	0xdf // force Clip filter reload


// types
#define TYPE_T2A_MARKQ    0
#define TYPE_T2A_PLAYQ    1
#define TYPE_T2A_PLAYEDQ  2
#define TYPE_T2A_ARCHIVEQ 3
#define TYPE_T2A_LOUTHQ   4 // not really a Q, but definitely a status

/*  
TODO
need to do the following:
create archive show info item at any show beginning.
finish show duration at the end of the show - modify show archive item at every close
at show end, wipe active DB (just to make sure)
write cases for the whole DB thing (commands above)
and the info transmit thing
add db init and show arc info id array thing BOOL InUse stuff.
change on air time to actual louth time for archive info. (instead of time(NULL))
reporting comments URL


add DB_TYPE_AUXGFX handler.

create set clip filter feed info dlg - has URL and enum field for LouthID
create lookup for song filter
create overrides for each show
create authoring!!!! ugh
all sorts of stuff for auth... spec later
*/

// max
#define DWNLD_CLIPS_MAXRECORDS 2000 // more than reasonable for filter....
#define DWNLD_CLIPS_MAXFIELDS 10 // more than reasonable for filter, should be in the first 2 or 3 max, really....

#define T2A_MAX_SUPPORTED_SHOWS 32


#define SHOWOPENINFO_MAXFIELDS 20 
#define T2A_SHOWOPEN_FIELDENUM_MODULE					0
#define T2A_SHOWOPEN_FIELDENUM_TIMINGMODE			1
#define T2A_SHOWOPEN_FIELDENUM_TIMINGIN				2
#define T2A_SHOWOPEN_FIELDENUM_TIMINGOUT			3
#define T2A_SHOWOPEN_FIELDENUM_KEYERID				4
#define T2A_SHOWOPEN_FIELDENUM_KEYERT					5
#define T2A_SHOWOPEN_FIELDENUM_ALLOWNODATAIN	6
#define T2A_SHOWOPEN_FIELDENUM_NODATAOUT			7
#define T2A_SHOWOPEN_FIELDENUM_PAUSEBETWN			8
#define T2A_SHOWOPEN_FIELDENUM_DELAY					9
#define T2A_SHOWOPEN_FIELDENUM_USETIMINGEXP		10
#define T2A_SHOWOPEN_FIELDENUM_EXPIRYEST			11
#define T2A_SHOWOPEN_FIELDENUM_USECLIP				12
#define T2A_SHOWOPEN_FIELDENUM_CLIPURL				13
#define T2A_SHOWOPEN_FIELDENUM_CLIPFIELD			14
#define T2A_SHOWOPEN_FIELDENUM_PRESERVEMARKS	15
#define T2A_SHOWOPEN_FIELDENUM_USEREPORTURL		16
#define T2A_SHOWOPEN_FIELDENUM_REPORTURL			17


typedef struct ShowOpenInfo_t
{
	CString	szShowDescription;
	CString	szShowModuleName;
	CString	szClipFilterURL;
	CString	szReportToWebURL;
	CString	szKeyerID;
	CString	szKeyerTitle;
	CString	szLouthID;
	BOOL	  bUseClipFilterFeed;
	BOOL	  bUseTimingExpiry;
	BOOL	  bAllowNoDataIntro;
	BOOL	  bDoNoDataLeftOutro;
	BOOL	  bPreserveMarks;
	BOOL	  bUseReportToWeb;
	int			nShowNumber;
	int			nClipFilterField;
	int		  nEventTimingMode;
	int		  nTimingInpoint;
	int		  nTimingOutpoint;
	int		  nPauseBetween;
	int		  nDelayBeforeData;
	int		  nEstimateTimeoutMS;
} ShowOpenInfo_t;


#endif //TEXTTOAIR_DEFINES_INCLUDED
