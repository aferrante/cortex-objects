// (c) 2000-Present Video Design Interactive.  All Rights Reserved.
// This file constitutes the proprietary, confidential intellectual 
// property of Video Design Interactive and may not be distributed.

/*
  Duet implementation.   Requires CAL\\calxxxx.lib

  Changelog:
      11/5/2002 - updated PrintCALError with 3.0 msgs
      11/5/2002 - Added DEBUG define in duet.h that allows you to
                  AfxMessageBox error codes
      11/1/2002 - Added getDuetInfo(int *hw, double *cal)
                  for determining build revisions and h/w platform
      1/29/2003 - Worked on squeezeback routines with hardware detect
      1/30/2003 - Worked on the getDuetInfo() hardware detect routines
                  Replaced hardcoded 720x486 with calls to getVideoDimensions
*/
  
#include "stdafx.h"
#include "networking.h"
#include "duet.h"

#define GSTR(X) (char *)((LPCTSTR) X)

CDuetAPI::CDuetAPI()
{
  m_nDuetVideoFormat = ecalFormatNTSC;
  m_connId = NULL;
  m_bConnected = m_bUseDuet = FALSE;
  m_szDuetAddress = "";
  m_nCodiSqueezeBoardNumber = 2;
  m_net.StartNetworking();
}

CDuetAPI::~CDuetAPI()
{
  //AfxMessageBox("Duet API in");
  if (m_bConnected) disconnect();
  m_net.ShutdownNetworking();
  //AfxMessageBox("Duet API out");
}


//////////////////////////////////////////////////////////////////////////
//
//   Public Methods
//


void CDuetAPI::Check2(HRESULT error, int line)
{
  // Use Windows supplied macro to test for failure
  if (FAILED(error) )	
  {
    //console(line, __FILE__, MESSAGE_CRITICAL, "CAL Error!\n");
    throw(error);
  }
}


int CDuetAPI::connect(CString szDuetAddress, BOOL bUseDuet, 
                      int nDuetVideoFormat, CString* pszInfo)
{
  m_szDuetAddress = szDuetAddress;
  m_bUseDuet = bUseDuet;
 
  try 
  {
    // Test for connection to CAL Server by checking for NULL 
    // connection handle. If not connected, connect now and 
    // create/setup needed Text Regions. Note that member
    // variable connId was initialized to NULL in constructor.
    
    if (m_connId == NULL)
    {
      // Connect to Local CAL Server and "Check" result code
      if (m_bUseDuet)
      {
        DuetCheck(CALConnect(
          NULL, // m_szDuetAddress, 
          "Duet",
          // "DUET:VGE1|VGE2",  // for VGE ganging
          (enum ecalVideoMode) m_nDuetVideoFormat,
          &(m_connId)), __LINE__);
      }
      else
      {
        DuetCheck(CALConnect(
          m_szDuetAddress, 
          _T(""), 
          (enum ecalVideoMode) m_nDuetVideoFormat, 
          &(m_connId)), __LINE__);
      }    
    }
  }
  catch( HRESULT error)
  {
    // Handle error exception
    CString str;
    str.Format(_T("Error connecting to Duet %s (Caught HRESULT error: 0x%x, %s)"), 
      m_szDuetAddress, error, printCALError(error));
    if(pszInfo!=NULL) 
		{
			pszInfo->Format("%s",str);
		}
		else AfxMessageBox(str);
    m_bConnected = FALSE;
    return FALSE;
  }
  m_bConnected = TRUE;

  // Get information about this machine's CAL version/build 
  // and type of video hardware (Duet, LEX, or VGA)
  getDuetInfo(&m_nVideoHardwareType, &m_fCALVersion, &m_dwCALBuild);

  return TRUE;
}


int CDuetAPI::disconnect(CString* pszInfo)
{
  //AfxMessageBox("Disconnecting Duet!!!!");
  try 
  {
    if (m_connId != NULL)
    {
      // Connect to Local CAL Server and "Check" result code
      DuetCheck(CALDisconnect(m_connId), __LINE__);
    }
  }
  catch( HRESULT error)
  {
    // Handle error exception
    CString str;
    str.Format(_T("Error disconnecting from Duet %s (Caught HRESULT error: 0x%x, %s)"), 
      m_szDuetAddress, error, printCALError(error));
    if(pszInfo!=NULL) 
		{
			pszInfo->Format("%s",str);
		}
		else 
		{
			//AfxMessageBox(str);
		}
    return FALSE;
  }
  m_connId = NULL;
  m_bConnected = FALSE;
  return TRUE;
}

int CDuetAPI::shutdown(CString* pszInfo)
{
  //AfxMessageBox("Disconnecting Duet!!!!");
  try 
  {
//    if (m_connId != NULL) // if disconneted, the ID WILL be null, so shutdown skips.
//    {
      // shutdown Local CAL Server and "Check" result code
      DuetCheck(CALShutdown(), __LINE__);
//    }
  }
  catch( HRESULT error)
  {
    // Handle error exception
    CString str;
    str.Format(_T("Error shutting down CAL (Caught HRESULT error: 0x%x, %s)"), 
      error, printCALError(error));
    if(pszInfo!=NULL) 
		{
			pszInfo->Format("%s",str);
		}
		else
		{
		//	AfxMessageBox(str);
		}
    return FALSE;
  }
  m_connId = NULL;
  m_bConnected = FALSE;
  return TRUE;
}



/*
 *  Font string mapping routines
 */
BOOL CDuetAPI::mapFontString(CString szFontString, CString *szFont,
                             int *PointSize, int *nStyle, COLORREF *crColor)
{
  //CEditEx e;
  //e.breakApartFontString(CString szFontString, szFont, PointSize, nStyle, crColor);
  
  
  // does this font exist on this machine, and has it been loaded with
  // these prefs?
  
  return TRUE;
}




BOOL CDuetAPI::getVideoDimensions(int *nWidth, int *nHeight)
{
  CALSERVERSYSINFO si;

  if (FAILED(CALGetSystemInfo(ecalFormatCurrent, &si)))
  {
    //m_bConnected = FALSE;
    //AfxMessageBox("A CAL error has occurred. Try clicking 'Reset Chyron' and retrying."); 
    // reasonable defaults:
    *nWidth = 720;
    *nHeight = 486;
    return FALSE; 
  }

/*
  printf("CAL Version: %s\n", si.InterfaceVersion);
  printf("Hardware rev level: %s\n", si.HardwareRevLevel);
  printf("IP address: %s\n", si.IPAddress);
  printf("Hostname: %s\n", si.HostName);
  printf("Video mode: %d\n", si.VideoMode);
  printf("Status: %d\n", si.Status);
  printf("Coord sys: xy offsets: (%d, %d)\n", si.CoordSys.xmin, si.CoordSys.ymin);
  printf("Coord sys: xy res: (%d, %d)\n", si.CoordSys.xres, si.CoordSys.yres);
  printf("Coord sys: pixel aspect: (%d, %d)\n", si.CoordSys.pixW, si.CoordSys.pixW);
  printf("Coord sys: frame aspect NTSC=4:3: (%d, %d)\n", si.CoordSys.frW, si.CoordSys.frH);
*/
  *nWidth = si.CoordSys.xres;
  *nHeight = si.CoordSys.yres;
  return TRUE;
}


// Gets info about the Duet hardware and CAL version
// *hw gets filled with one of the HW_xxx values.
BOOL CDuetAPI::getDuetInfo(int *nVideoHardwareType, double *fCALVersion, 
                           DWORD *dwCALBuild)
{
  CALSERVERSYSINFO si;
  CString szHardwareRev, szCALVersion, szTemp;
  
  *dwCALBuild=0;

  if (!FAILED(CALGetSystemInfo(ecalFormatCurrent, &si)))
  {
    szHardwareRev.Format("%s", si.HardwareRevLevel);
    // CODI 6A3 for LEX
    if (szHardwareRev.Left(4).CompareNoCase("CODI") == 0)
      *nVideoHardwareType = VIDEO_HW_DUET_LEX;
    else if (szHardwareRev.Left(4).CompareNoCase("DUET") == 0)
      *nVideoHardwareType = VIDEO_HW_DUET_SD;
    else if (szHardwareRev.Left(4).CompareNoCase("Not Available") == 0)
      *nVideoHardwareType = VIDEO_HW_VGA;
    else
      *nVideoHardwareType = VIDEO_HW_UNDEFINED;

    // examples: "2.5 Beta (SW only)", "3.0", "3.0 Build 107"
    szCALVersion.Format("%s", si.InterfaceVersion);
    int nPos = szCALVersion.Find(" Build ");
    if (nPos >= 0)
    {
      szTemp = szCALVersion.Mid(nPos + 7);
      *dwCALBuild = atol(szTemp);
      szCALVersion = szCALVersion.Left(nPos);
    }
    *fCALVersion = atof(szCALVersion);

    //szTemp.Format("Values returned: CAL Version: %s  Hardware: %s\nI think it's CAL %0.2lf, build %d",
    //  szCALVersion, szHardwareRev, *fCALVersion, *dwCALBuild);
    //AfxMessageBox(szTemp);

    return TRUE;
  }
  return FALSE;
}



int CDuetAPI::fileExists(char *fn)
{
  FILE *fp;
  fp = fopen(fn, "rb");
  if (fp == NULL) return FALSE;
  fclose(fp);
  return TRUE;
}




// This function will encode a line of text for display
// under CAL.  Call this BEFORE adding any escape 
// sequences (such as [f 1] to change the font to font 1)
CString CDuetAPI::CALEncodeBrackets(CString szString)
{
  int k;
  for (k=0; k<szString.GetLength(); k++)
  {
    if (szString.GetAt(k) == '[')
    {
      szString = szString.Left(k) + "\\" + 
        szString.Right(szString.GetLength() - k);
      k++;
    }
  }
  return szString;
}


// This function will insert a CAL escape sequence
// at the end of a string, minding the fact that it
// cannot be followed by whitespace, and has to be
// followed by SOMETHING...   You should probably call
// this like this:
//    newString = CALAppendEvent(CALEncodeBrackets(szString), "[e 1]");

CString CDuetAPI::CALAppendEvent(CString szString, CString szEventString)
{
  CString szPrefix, szSuffix;

  int nSplitPoint = szString.GetLength() - 2;
  if (nSplitPoint < 0) nSplitPoint = 0;

  while ( (nSplitPoint>0) && (iswspace((char) szString.GetAt(nSplitPoint))) )
  {
    nSplitPoint--;
  }

	szPrefix = szString.Left(nSplitPoint);
  szSuffix = szString.Mid(nSplitPoint);
  
  return szPrefix + szEventString + szSuffix;
}



/*
 *  h = 0-360   s, v, r, g, b 0-1
 */
void CDuetAPI::HSV2RGB(double h, double s, double v, double *r, double *g, double *b)
{
  double p1,p2,p3, f;
  int i;

  if (h==360) h=0;


  h=h/60.0;
  i = (int) h;
  f = h - i;
  p1 = v*(1-s);
  p2 = v*(1-(s*f));
  p3 = v*(1-(s*(1-f)));

  switch(i)
  {
  case 0: *r=v;  *g=p3; *b=p1; break;
  case 1: *r=p2; *g=v;  *b=p1; break;
  case 2: *r=p1; *g=v;  *b=p3; break;
  case 3: *r=p1; *g=p2; *b=v; break;
  case 4: *r=p3; *g=p1; *b=v; break;
  case 5: *r=v;  *g=p1; *b=p2; break;
  }

  //AMB("p1=%lf p2=%lf p3=%lf  i=%d  (%lf,%lf,%lf)", p1,p2,p3,i, *r, *g, *b);
}


/*
 *  rgb 0-1
 *  h=0-360  sv=0-1
 */
void CDuetAPI::RGB2HSV(double r, double g, double b, double *oh, double *os, double *ov)
{
  double min,h, s, v, delta, max;

  max=r;  if (g>max) max=g; if (b>max) max=b;  // v=max(r,g,b)
  min=r;  if (g<min) min=g; if (b<min) min=b;  // m=min(r,g,b)
  delta = max-min;
  v = max;

  if (v!=0) s = delta/max;
  else s=0;

  if (s != 0)
  {
    if (r == max)
      h = (g-b)/delta;
    else if (g == max)
      h = 2 + ((b-r)/delta);
    else if (g == max)
      h = 4 + ((r-g)/delta);
    h = h * 60;
    if (h < 0) h += 360;
  }
  else h=0;  // h is undefined
  *oh = h;
  *os = s;
  *ov = v;
}


/*Set bgcolor to something bright */
void CDuetAPI::setBGBright(double *hout, double *sout, double *vout)
{
  double h, dr, dg, db, dh,ds,dv;
  HCAL bgID;

  // choose background color -  hue 30-315 excludes reds
  h = ((((double)(rand()))/(double)RAND_MAX )*(315-30))+ 30;
  HSV2RGB(h, 1.0, 1.0, &dr, &dg, &db);
  RGB2HSV(dr, dg, db, &dh, &ds, &dv);
  CALCOLOR c1 = CALRGBA((int)(dr*255), (int)(dg*255), (int)(db*255), 255);

//  AMB("H=%d S=1.0 V=1.0   R=%d G=%d B=%d  H=%lf S=%lf V=%lf", (int) h, (int)(dr*255), (int)(dg*255), (int)(db*255),dh, ds, dv);

  DuetCheck(CALCanvasRgn( 0, 0, 720, 486, &bgID), __LINE__);
  DuetCheck(CALBackground(bgID, ecalLeftToRight, c1, c1), __LINE__);
  DuetCheck(CALPlayI(bgID), __LINE__);

  *hout = h;
  *sout = 1.0;
  *vout = 1.0;
}



CString CDuetAPI::printCALError(unsigned long errcode)
{
  CString err;
  switch (errcode)
  {

  case ecalErrConnectGroup: err = "ecalErrConnectGroup"; break;
  case ecalErrNoAvailBoard: err = "ecalErrNoAvailBoard"; break;
  case ecalErrNoAvailOutput: err = "ecalErrNoAvailOutput"; break;
  case ecalErrInvalidBoard: err = "ecalErrInvalidBoard"; break;
  case ecalErrInvalidOutput: err = "ecalErrInvalidOutput"; break;
  case ecalErrInvalidFormat: err = "ecalErrInvalidFormat"; break;
  case ecalErrInvalidConnection: err = "ecalErrInvalidConnection"; break;
  case ecalErrInvalidBuffer: err = "ecalErrInvalidBuffer"; break;
  case ecalErrConnectException: err = "ecalErrConnectException"; break;
  case ecalErrDisconnectException: err = "ecalErrDisconnectException"; break;
  case ecalErrInvalidClientVersion: err = "ecalErrInvalidClientVersion"; break;

  // VideoDefaults Error Group
  case ecalErrVideoDefGroup: err = "ecalErrVideoDefGroup"; break;
  case ecalErrInvalidOption: err = "ecalErrInvalidOption"; break;
  case ecalErrInvalidVideoMode: err = "ecalErrInvalidVideoMode"; break;
  case ecalErrInvalidRefSyncParam: err = "ecalErrInvalidRefSyncParam"; break;
  case ecalErrInvalidMixParam: err = "ecalErrInvalidMixParam"; break;
  case ecalErrInvalidAlphaShape: err = "ecalErrInvalidAlphaShape"; break;
  case ecalErrInvalidAncillaryData: err = "ecalErrInvalidAncillaryData"; break;
  case ecalErrInvalidTimeCodeParam: err = "ecalErrInvalidTimeCodeParam"; break;
  case ecalErrInvalidBP2_3Param: err = "ecalErrInvalidBP2_3Param"; break;
  case ecalErrInvalidBP4_5Param: err = "ecalErrInvalidBP4_5Param"; break;
  case ecalErrInvalidBP6_7Param: err = "ecalErrInvalidBP6_7Param"; break;
  case ecalErrInvalidKeyInputParam: err = "ecalErrInvalidKeyInputParam"; break;
  case ecalErrInvalidHDelayParam: err = "ecalErrInvalidHDelayParam"; break;
  case ecalErrInvalidVDelayParam: err = "ecalErrInvalidVDelayParam"; break;

  //---------- Status/Information Error Section ---------
  case ecalErrStatusInfoSection: err = "ecalErrStatusInfoSection"; break;

  //     Info Error Group
  case ecalErrInfoGroup: err = "ecalErrInfoGroup"; break;
  case ecalErrInvalidStruct: err = "ecalErrInvalidStruct"; break;
  case ecalErrInvalidInfoType: err = "ecalErrInvalidInfoType"; break;
  case ecalErrGetNetInfo: err = "ecalErrGetNetInfo"; break;
  case ecalErrCannotGetRgnInfo: err = "ecalErrCannotGetRgnInfo"; break;

  //   Name/ID Error Group
  case ecalErrNameGroup: err = "ecalErrNameGroup"; break;
  case ecalErrNameExists: err = "ecalErrNameExists"; break;
  case ecalErrUnrecogName: err = "ecalErrUnrecogName"; break;

  //---------- Primitives/Regions Error Section ----------
  case ecalErrPrimitivesSection: err = "ecalErrPrimitivesSection"; break;

  //  Background Error Group

  //    Object Error Group
  case ecalErrObjectGroup: err = "ecalErrObjectGroup"; break;
  case ecalErrInvalidRefPt: err = "ecalErrInvalidRefPt"; break;
  case ecalErrInvalidXExt: err = "ecalErrInvalidXExt"; break;
  case ecalErrInvalidYExt: err = "ecalErrInvalidYExt"; break;
  case ecalErrInvalidZExt: err = "ecalErrInvalidZExt"; break;
  case ecalErrNullObject: err = "ecalErrNullObject"; break;
  case ecalErrNullObjectData: err = "ecalErrNullObjectData"; break;
  case ecalErrInvalidObject: err = "ecalErrInvalidObject"; break;
  case ecalErrCannotSetObjectName: err = "ecalErrCannotSetObjectName"; break;
  case ecalErrCannotReplaceObject: err = "ecalErrCannotReplaceObject"; break;
  case ecalErrCannotInsertObject: err = "ecalErrCannotInsertObject"; break;

  //     Image Error Group
  case ecalErrImageGroup: err = "ecalErrImageGroup"; break;
  case ecalErrInvalidPixelDepth: err = "ecalErrInvalidPixelDepth"; break;
  case ecalErrImageDevice: err = "ecalErrImageDevice"; break;
  case ecalErrImageDeviceEx: err = "ecalErrImageDeviceEx"; break;
  case ecalErrImageMemory: err = "ecalErrImageMemory"; break;
  case ecalErrImageFlush: err = "ecalErrImageFlush"; break;

  //     Group Error Group
  case ecalErrGroupGroup: err = "ecalErrGroupGroup"; break;
  case ecalErrInvalidGroup: err = "ecalErrInvalidGroup"; break;
  case ecalErrInvalidChildren: err = "ecalErrInvalidChildren"; break;
  case ecalErrHasParent: err = "ecalErrHasParent"; break;
  case ecalErrInvalidSlot: err = "ecalErrInvalidSlot"; break;
  case ecalErrOutOfSlots: err = "ecalErrOutOfSlots"; break;
  case ecalErrSlotOutOfRange: err = "ecalErrSlotOutOfRange"; break;
  case ecalErrSlotInUse: err = "ecalErrSlotInUse"; break;
  case ecalErrHasNoParent: err = "ecalErrHasNoParent"; break;

  //   Attribute Error Group
  case ecalErrAttrGroup: err = "ecalErrAttrGroup"; break;
  case ecalErrRegionWrongType: err = "ecalErrRegionWrongType"; break;
  case ecalErrInvalidJustify: err = "ecalErrInvalidJustify"; break;
  case ecalErrInvalidHMSFormat: err = "ecalErrInvalidHMSFormat"; break;
  case ecalErrInvalidRegionSize: err = "ecalErrInvalidRegionSize"; break;
  case ecalErrInvalidScaleMode: err = "ecalErrInvalidScaleMode"; break;
  case ecalErrInvalidBGDirection: err = "ecalErrInvalidBGDirection"; break;
  case ecalErrInvalidWrapMode: err = "ecalErrInvalidWrapMode"; break;

  //------------- Font/Color Error Section ----------------
  case ecalErrFontColorSection: err = "ecalErrFontColorSection"; break;

  //     Font Error Group
  case ecalErrFontGroup: err = "ecalErrFontGroup"; break;
  case ecalErrFontCacheFull: err = "ecalErrFontCacheFull"; break;
  case ecalErrUnknownFont: err = "ecalErrUnknownFont"; break;
  case ecalErrNullFont: err = "ecalErrNullFont"; break;
  case ecalErrFontDeletion: err = "ecalErrFontDeletion"; break;
  case ecalErrInvalidFontInfo: err = "ecalErrInvalidFontInfo"; break;
  case ecalErrUnrecognizedFont: err = "ecalErrUnrecognizedFont"; break;
  case ecalErrInvalidFontHeight: err = "ecalErrInvalidFontHeight"; break;
  case ecalErrCannotCreateFont: err = "ecalErrCannotCreateFont"; break;
  case ecalErrCannotCacheChar: err = "ecalErrCannotCacheChar"; break;
  case ecalErrUncachedChar: err = "ecalErrUncachedChar"; break;

  //   Font Options / Text Options Group
  case ecalErrFontOptionsGroup: err = "ecalErrFontOptionsGroup"; break;
  case ecalErrInvalidFontIndex: err = "ecalErrInvalidFontIndex"; break;
  case ecalErrInvalidShear: err = "ecalErrInvalidShear"; break;
  case ecalErrInvalidKerning: err = "ecalErrInvalidKerning"; break;
  case ecalErrInvalidLeading: err = "ecalErrInvalidLeading"; break;
  case ecalErrInvalidCharWidth: err = "ecalErrInvalidCharWidth"; break;
  case ecalErrInvalidAngle: err = "ecalErrInvalidAngle"; break;

  // Font Edge Effects Group
  case ecalErrFontEffectsGroup: err = "ecalErrFontEffectsGroup"; break;
  case ecalErrInvalidEdgeType: err = "ecalErrInvalidEdgeType"; break;
  case ecalErrInvalidEdgeSize: err = "ecalErrInvalidEdgeSize"; break;
  case ecalErrSoftnessOutOfRange: err = "ecalErrSoftnessOutOfRange"; break;
  case ecalErrCannotAddEdge: err = "ecalErrCannotAddEdge"; break;

  //     Color Error Group
  case ecalErrColorGroup: err = "ecalErrColorGroup"; break;
  case ecalErrColorIndexInvalid: err = "ecalErrColorIndexInvalid"; break;
  case ecalErrColorIndexWrongType: err = "ecalErrColorIndexWrongType"; break;
  case ecalErrColorInvalidRGBA: err = "ecalErrColorInvalidRGBA"; break;
  case ecalErrNullColor: err = "ecalErrNullColor"; break;

  //---------------- Update Error Section ----------------
  case ecalErrUpdateSection: err = "ecalErrUpdateSection"; break;

  //    Update Error Group
  case ecalErrUpdateGroup: err = "ecalErrUpdateGroup"; break;
  case ecalErrInvalidRegionType: err = "ecalErrInvalidRegionType"; break;
  case ecalErrBadClusterName: err = "ecalErrBadClusterName"; break;
  case ecalErrUpdatesSuspended: err = "ecalErrUpdatesSuspended"; break;
  case ecalErrDataNotCached: err = "ecalErrDataNotCached"; break;

  //-------------- Transition Error Section --------------
  case ecalErrTransitionSection: err = "ecalErrTransitionSection"; break;

  //  Transition Error Group
  case ecalErrTransitionGroup: err = "ecalErrTransitionGroup"; break;
  case ecalErrInvalidTransition: err = "ecalErrInvalidTransition"; break;
  case ecalErrInvalidEaseMode: err = "ecalErrInvalidEaseMode"; break;
  case ecalErrInvalidPattern: err = "ecalErrInvalidPattern"; break;
  case ecalErrNotATransition: err = "ecalErrNotATransition"; break;
  case ecalErrInvalidDirection: err = "ecalErrInvalidDirection"; break;
  case ecalErrInvalidTransOption: err = "ecalErrInvalidTransOption"; break;

  //---------------- View/Animation Error Section ----------------
  case ecalErrViewingSection: err = "ecalErrViewingSection"; break;

  //--------------------- View Error Group ----------------------
  case ecalErrViewGroup: err = "ecalErrViewGroup"; break;
  case ecalErrInvalidCameraPos: err = "ecalErrInvalidCameraPos"; break;
  case ecalErrInvalidCameraAngle: err = "ecalErrInvalidCameraAngle"; break;
  case ecalErrInvalidKeyframes: err = "ecalErrInvalidKeyframes"; break;
  case ecalErrInvalidKeyValues: err = "ecalErrInvalidKeyValues"; break;
  case ecalErrTooFewKeyframes: err = "ecalErrTooFewKeyframes"; break;
  case ecalErrTooFewKeyValues: err = "ecalErrTooFewKeyValues"; break;
  case ecalErrKeyframeOutOfRange: err = "ecalErrKeyframeOutOfRange"; break;
  case ecalErrInvalidViewSpec: err = "ecalErrInvalidViewSpec"; break;

  //----------------- Attrimation Error Group ------------------
  case ecalErrAttrimationGroup: err = "ecalErrAttrimationGroup"; break;
  case ecalErrInvalidAttrimation: err = "ecalErrInvalidAttrimation"; break;
  case ecalErrCannotAssignCompiler: err = "ecalErrCannotAssignCompiler"; break;
  case ecalErrNullTransform: err = "ecalErrNullTransform"; break;
  case ecalErrNullAttrimation: err = "ecalErrNullAttrimation"; break;
  case ecalErrNullAnimationState: err = "ecalErrNullAnimationState"; break;
  case ecalErrPreAnimationState: err = "ecalErrPreAnimationState"; break;

  //----------------- Lighting Error Group ----------------
  case ecalErrLightingGroup: err = "ecalErrLightingGroup"; break;
  case ecalErrInvalidLightPos: err = "ecalErrInvalidLightPos"; break;
  case ecalErrInvalidLightDir: err = "ecalErrInvalidLightDir"; break;
  case ecalErrLightNotAdded: err = "ecalErrLightNotAdded"; break;
  case ecalErrInvalidLightIndex: err = "ecalErrInvalidLightIndex"; break;

  //--------------- Playback Error Section ----------------
  case ecalErrPlaybackSection: err = "ecalErrPlaybackSection"; break;

  //    Frame Error Group
  case ecalErrFrameGroup: err = "ecalErrFrameGroup"; break;
  case ecalErrFrameInvalid: err = "ecalErrFrameInvalid"; break;

  //    Play Error Group
  case ecalErrPlayGroup: err = "ecalErrPlayGroup"; break;
  case ecalErrObjectPlaying: err = "ecalErrObjectPlaying"; break;

  //--------------- Resource Error Section ----------------
  case ecalErrResourceSection: err = "ecalErrResourceSection"; break;

  //    Memory Error Group
  case ecalErrMemoryGroup: err = "ecalErrMemoryGroup"; break;
  case ecalErrOutOfMemory: err = "ecalErrOutOfMemory"; break;
  case ecalErrOverflow: err = "ecalErrOverflow"; break;

  //------------------ I/O Error Section ------------------
  case ecalErrIOSection: err = "ecalErrIOSection"; break;

  //     File Error Group
  case ecalErrFileGroup: err = "ecalErrFileGroup"; break;
  case ecalErrHostNotFound: err = "ecalErrHostNotFound"; break;
  case ecalErrDriveNotFound: err = "ecalErrDriveNotFound"; break;
  case ecalErrPathNotFound: err = "ecalErrPathNotFound"; break;
  case ecalErrFileNotFound: err = "ecalErrFileNotFound"; break;
  case ecalErrAccessDenied: err = "ecalErrAccessDenied"; break;
  case ecalErrInvalidFileFormat: err = "ecalErrInvalidFileFormat"; break;
  case ecalErrInvalidFileType: err = "ecalErrInvalidFileType"; break;
  case ecalErrLangFilename: err = "ecalErrLangFilename"; break;
  case ecalErrFilenameTooLong: err = "ecalErrFilenameTooLong"; break;

  //     Import Error Group
  case ecalErrImportGroup: err = "ecalErrImportGroup"; break;
  case ecalErrUnsupportedOLEHeader: err = "ecalErrUnsupportedOLEHeader"; break;

  //     Export Error Group
  case ecalErrExportGroup: err = "ecalErrExportGroup"; break;
  case ecalErrExportInit: err = "ecalErrExportInit"; break;
  case ecalErrExportCreate: err = "ecalErrExportCreate"; break;
  case ecalErrExportOpen: err = "ecalErrExportOpen"; break;
  case ecalErrExportRead: err = "ecalErrExportRead"; break;
  case ecalErrExportWrite: err = "ecalErrExportWrite"; break;
  case ecalErrExportActive: err = "ecalErrExportActive"; break;
  case ecalErrExportClose: err = "ecalErrExportClose"; break;
  case ecalErrExportFileFormat: err = "ecalErrExportFileFormat"; break;
  case ecalErrExportNameFormat: err = "ecalErrExportNameFormat"; break;
  case ecalErrExportUnknown: err = "ecalErrExportUnknown"; break;

  //     GPIO Error Group
  case ecalErrGPIOGroup: err = "ecalErrGPIOGroup"; break;
  case ecalErrGPIONoHardware: err = "ecalErrGPIONoHardware"; break;
  case ecalErrGPIONoLibrary: err = "ecalErrGPIONoLibrary"; break;
  case ecalErrGPIOInvalidLibrary: err = "ecalErrGPIOInvalidLibrary"; break;

  //    Socket Error Group
  case ecalErrSocketGroup: err = "ecalErrSocketGroup"; break;
  case ecalErrSocketWrite: err = "ecalErrSocketWrite"; break;
  case ecalErrSocketRead: err = "ecalErrSocketRead"; break;
  case ecalErrSocketUserTimeout: err = "ecalErrSocketUserTimeout"; break;
  case ecalErrSocketInvalidPort: err = "ecalErrSocketInvalidPort"; break;
  case ecalErrSocketEINTR: err = "ecalErrSocketEINTR"; break;
  case ecalErrSocketEBADF: err = "ecalErrSocketEBADF"; break;
  case ecalErrSocketEACCES: err = "ecalErrSocketEACCES"; break;
  case ecalErrSocketEFAULT: err = "ecalErrSocketEFAULT"; break;
  case ecalErrSocketEINVAL: err = "ecalErrSocketEINVAL"; break;
  case ecalErrSocketEMFILE: err = "ecalErrSocketEMFILE"; break;
  case ecalErrSocketEWOULDBLOCK: err = "ecalErrSocketEWOULDBLOCK"; break;
  case ecalErrSocketEINPROGRESS: err = "ecalErrSocketEINPROGRESS"; break;
  case ecalErrSocketEALREADY: err = "ecalErrSocketEALREADY"; break;
  case ecalErrSocketENOTSOCK: err = "ecalErrSocketENOTSOCK"; break;
  case ecalErrSocketEDESTADDRREQ: err = "ecalErrSocketEDESTADDRREQ"; break;
  case ecalErrSocketEMSGSIZE: err = "ecalErrSocketEMSGSIZE"; break;
  case ecalErrSocketEPROTOTYPE: err = "ecalErrSocketEPROTOTYPE"; break;
  case ecalErrSocketENOPROTOOPT: err = "ecalErrSocketENOPROTOOPT"; break;
  case ecalErrSocketEPROTONOSUPPORT: err = "ecalErrSocketEPROTONOSUPPORT"; break;
  case ecalErrSocketESOCKTNOSUPPORT: err = "ecalErrSocketESOCKTNOSUPPORT"; break;
  case ecalErrSocketEOPNOTSUPP: err = "ecalErrSocketEOPNOTSUPP"; break;
  case ecalErrSocketEPFNOSUPPORT: err = "ecalErrSocketEPFNOSUPPORT"; break;
  case ecalErrSocketEAFNOSUPPORT: err = "ecalErrSocketEAFNOSUPPORT"; break;
  case ecalErrSocketEADDRINUSE: err = "ecalErrSocketEADDRINUSE"; break;
  case ecalErrSocketEADDRNOTAVAIL: err = "ecalErrSocketEADDRNOTAVAIL"; break;
  case ecalErrSocketENETDOWN: err = "ecalErrSocketENETDOWN"; break;
  case ecalErrSocketENETUNREACH: err = "ecalErrSocketENETUNREACH"; break;
  case ecalErrSocketENETRESET: err = "ecalErrSocketENETRESET"; break;
  case ecalErrSocketECONNABORTED: err = "ecalErrSocketECONNABORTED"; break;
  case ecalErrSocketECONNRESET: err = "ecalErrSocketECONNRESET"; break;
  case ecalErrSocketENOBUFS: err = "ecalErrSocketENOBUFS"; break;
  case ecalErrSocketEISCONN: err = "ecalErrSocketEISCONN"; break;
  case ecalErrSocketENOTCONN: err = "ecalErrSocketENOTCONN"; break;
  case ecalErrSocketESHUTDOWN: err = "ecalErrSocketESHUTDOWN"; break;
  case ecalErrSocketETOOMANYREFS: err = "ecalErrSocketETOOMANYREFS"; break;
  case ecalErrSocketETIMEDOUT: err = "ecalErrSocketETIMEDOUT"; break;
  case ecalErrSocketECONNREFUSED: err = "ecalErrSocketECONNREFUSED"; break;
  case ecalErrSocketELOOP: err = "ecalErrSocketELOOP"; break;
  case ecalErrSocketENAMETOOLONG: err = "ecalErrSocketENAMETOOLONG"; break;
  case ecalErrSocketEHOSTDOWN: err = "ecalErrSocketEHOSTDOWN"; break;
  case ecalErrSocketEHOSTUNREACH: err = "ecalErrSocketEHOSTUNREACH"; break;
  case ecalErrSocketENOTEMPTY: err = "ecalErrSocketENOTEMPTY"; break;
  case ecalErrSocketEPROCLIM: err = "ecalErrSocketEPROCLIM"; break;
  case ecalErrSocketEUSERS: err = "ecalErrSocketEUSERS"; break;
  case ecalErrSocketEDQUOT: err = "ecalErrSocketEDQUOT"; break;
  case ecalErrSocketESTALE: err = "ecalErrSocketESTALE"; break;
  case ecalErrSocketEREMOTE: err = "ecalErrSocketEREMOTE"; break;
  case ecalErrSocketSYSNOTREADY: err = "ecalErrSocketSYSNOTREADY"; break;
  case ecalErrSocketVERNOTSUPPORTED: err = "ecalErrSocketVERNOTSUPPORTED"; break;
  case ecalErrSocketNOTINITIALISED: err = "ecalErrSocketNOTINITIALISED"; break;
  case ecalErrSocketEDISCON: err = "ecalErrSocketEDISCON"; break;
  case ecalErrSocketENOMORE: err = "ecalErrSocketENOMORE"; break;
  case ecalErrSocketECANCELLED: err = "ecalErrSocketECANCELLED"; break;
  case ecalErrSocketEINVALIDPROCTABLE: err = "ecalErrSocketEINVALIDPROCTABLE"; break;
  case ecalErrSocketEINVALIDPROVIDER: err = "ecalErrSocketEINVALIDPROVIDER"; break;
  case ecalErrSocketEPROVIDERFAILEDINIT: err = "ecalErrSocketEPROVIDERFAILEDINIT"; break;
  case ecalErrSocketSYSCALLFAILURE: err = "ecalErrSocketSYSCALLFAILURE"; break;
  case ecalErrSocketSERVICE_NOT_FOUND: err = "ecalErrSocketSERVICE_NOT_FOUND"; break;
  case ecalErrSocketTYPE_NOT_FOUND: err = "ecalErrSocketTYPE_NOT_FOUND"; break;
  case ecalErrSocket_E_NO_MORE: err = "ecalErrSocket_E_NO_MORE"; break;
  case ecalErrSocket_E_CANCELLED: err = "ecalErrSocket_E_CANCELLED"; break;
  case ecalErrSocketEREFUSED: err = "ecalErrSocketEREFUSED"; break;
  case ecalErrSocketHOST_NOT_FOUND: err = "ecalErrSocketHOST_NOT_FOUND"; break;
  case ecalErrSocketTRY_AGAIN: err = "ecalErrSocketTRY_AGAIN"; break;
  case ecalErrSocketNO_RECOVERY: err = "ecalErrSocketNO_RECOVERY"; break;
  case ecalErrSocketNO_DATA: err = "ecalErrSocketNO_DATA"; break;
  case ecalErrSocketNO_ADDRESS: err = "ecalErrSocketNO_ADDRESS"; break;


  //    SDMixer Error Group
  case ecalErrSDMixerGroup: err = "ecalErrSDMixerGroup"; break;
  case ecalErrSDMixerNoLibrary: err = "ecalErrSDMixerNoLibrary"; break;
  case ecalErrSDMixerInvalidLibrary: err = "ecalErrSDMixerInvalidLibrary"; break;
  case ecalErrSDMixerLocked: err = "ecalErrSDMixerLocked"; break;
  case ecalErrSDMixerNotLocked: err = "ecalErrSDMixerNotLocked"; break;
  case ecalErrSDMixerEffectsData: err = "ecalErrSDMixerEffectsData"; break;
  case ecalErrSDMixerRouterData: err = "ecalErrSDMixerRouterData"; break;
  case ecalErrSDMixerNoMemory: err = "ecalErrSDMixerNoMemory"; break;
  case ecalErrSDMixerEffectsRange: err = "ecalErrSDMixerEffectsRange"; break;
  case ecalErrSDMixerInvalidParameter: err = "ecalErrSDMixerInvalidParameter"; break;
  case ecalErrSDMixerUnknown: err = "ecalErrSDMixerUnknown"; break;


  //    Squeeze Board Error Group
  case ecalErrSqueezeGroup: err = "ecalErrSqueezeGroup"; break;
  case ecalErrSqueezeNoLibrary: err = "ecalErrSqueezeNoLibrary"; break;
  case ecalErrSqueezeInvalidLibrary: err = "ecalErrSqueezeInvalidLibrary"; break;
  case ecalErrSqueezeLocked: err = "ecalErrSqueezeLocked"; break;
  case ecalErrSqueezeNotLocked: err = "ecalErrSqueezeNotLocked"; break;
  case ecalErrSqueezeEffectsData: err = "ecalErrSqueezeEffectsData"; break;
  case ecalErrSqueezeRouterData: err = "ecalErrSqueezeRouterData"; break;
  case ecalErrSqueezeNoMemory: err = "ecalErrSqueezeNoMemory"; break;
  case ecalErrSqueezeEffectsRange: err = "ecalErrSqueezeEffectsRange"; break;
  case ecalErrSqueezeNoField: err = "ecalErrSqueezeNoField"; break;
  case ecalErrSqueezePendingRfx: err = "ecalErrSqueezePendingRfx"; break;
  case ecalErrSqueezeUnknown: err = "ecalErrSqueezeUnknown"; break;

  //    CODI Error Group
  case ecalErrCodiGroup: err = "ecalErrCodiGroup"; break;
  case ecalErrCodiNoLibrary: err = "ecalErrCodiNoLibrary"; break;
  case ecalErrCodiInvalidLibrary: err = "ecalErrCodiInvalidLibrary"; break;
  case ecalErrCodiInfoQuery: err = "ecalErrCodiInfoQuery"; break;
  case ecalErrCodiWriteMemory: err = "ecalErrCodiWriteMemory"; break;

  //     Matrox Clip Player Error Group
  case ecalErrMcpGroup: err = "ecalErrMcpGroup"; break;
  case ecalErrMcpNoLibrary: err = "ecalErrMcpNoLibrary"; break;
  case ecalErrMcpInvalidLibrary: err = "ecalErrMcpInvalidLibrary"; break;

  //----------------- Misc Error Section -----------------
  case ecalErrMiscSection: err = "ecalErrMiscSection"; break;

  //     Misc Error Group
  case ecalErrMiscGroup: err = "ecalErrMiscGroup"; break;
  case ecalErrUnknown: err = "ecalErrUnknown"; break;
  case ecalErrInvalidID: err = "ecalErrInvalidID"; break;
  case ecalErrInvalidArgument: err = "ecalErrInvalidArgument"; break;
  case ecalErrInvalidComponent: err = "ecalErrInvalidComponent"; break;
  case ecalErrNotImplemented: err = "ecalErrNotImplemented"; break;
  case ecalErrNullOutputArgument: err = "ecalErrNullOutputArgument"; break;
  case ecalErrContextGrab: err = "ecalErrContextGrab"; break;
  case ecalErrExcessiveDelays: err = "ecalErrExcessiveDelays"; break;
  case ecalErrOGL: err = "ecalErrOGL"; break;
  case ecalErrNullOGL: err = "ecalErrNullOGL"; break;
  case ecalErrNullNode: err = "ecalErrNullNode"; break;
  case ecalErrCannotCacheTexture: err = "ecalErrCannotCacheTexture"; break;
  case ecalErrUncachedTexture: err = "ecalErrUncachedTexture"; break;
  case ecalErrChyAnimation: err = "ecalErrChyAnimation"; break;
  case ecalErrDevice: err = "ecalErrDevice"; break;
  case ecalErrWaitTimedOut: err = "ecalErrWaitTimedOut"; break;
  case ecalErrWaitFailed: err = "ecalErrWaitFailed"; break;
  case ecalErrInternal: err = "ecalErrInternal"; break;

  //     Range Error Group
  case ecalErrRangeGroup: err = "ecalErrRangeGroup"; break;
  case ecalErrNotEnoughValues: err = "ecalErrNotEnoughValues"; break;
  case ecalErrTooManyValues: err = "ecalErrTooManyValues"; break;
  case ecalErrBoundsExceeded: err = "ecalErrBoundsExceeded"; break;
  case ecalErrDuplicateValue: err = "ecalErrDuplicateValue"; break;

  //    Options Error Group
  case ecalErrOptionsGroup: err = "ecalErrOptionsGroup"; break;
  case ecalErrOptFormat: err = "ecalErrOptFormat"; break;
  case ecalErrOptMissing: err = "ecalErrOptMissing"; break;
  case ecalErrOptNoValue: err = "ecalErrOptNoValue"; break;
  case ecalErrOptValueConversion: err = "ecalErrOptValueConversion"; break;
  case ecalErrOptBufferTooSmall: err = "ecalErrOptBufferTooSmall"; break;


  //---------------------- Success ------------------------
  case ecalErrSuccess: err = "ecalErrSuccess"; break;
  case ecalErrSuccessContinue: err = "ecalErrSuccessContinue"; break;
  
  
  default:  err.Format("Unknown Error 0x%08x)", errcode); break;

    // hex 8 - system problem (bad dcom?)
    // hex a - cal error
  }

  return err;
}



#ifdef OLD_SHIT

void setupDuetCrawl(passDuet_t *settings)
{
  int Swidth, Sheight;

  m_szBackplate.TrimLeft();  m_szBackplate.TrimRight();
  if (m_szBackplate.Compare("") != 0)
  {
    if (fileExists((char *)((LPCTSTR)m_szBackplate)))
    {
      getInfo(&Swidth, &Sheight);
      DuetCheck(CALImageRgn( 0.0f, 0.0f, (float)Swidth, (float)Sheight, ecalCenter, ecalScaleToFit, &myBackground), __LINE__);
      DuetCheck(CALSetName(myBackground, "Background"), __LINE__);
      DuetCheck(CALCacheImage(myBackground, "bg", m_szBackplate), __LINE__);
      DuetCheck(CALUpdateW(myBackground, ecalUpdateSelect, 1, "bg"), __LINE__);
    }
    else 
    {
      // AfxMessageBox("Backplate "+m_szBackplate+" does not exist!");
    }
  }
}


void doDuetCrawl(passDuet_t *settings)
{
  int Swidth, Sheight;
  if (!m_bConnected)
  { AfxMessageBox("You're not connected to a Duet."); return; }
  
  if (!getInfo(&Swidth, &Sheight)) 
  {
    AfxMessageBox("getInfo() failed");
    return;
  }

  try 
  { 
    int index;

    DuetCheck(CALLoadPalette(1, 
      CALRGBA((int)((m_nDuetColor >> 16)&255), 
      (int)((m_nDuetColor >> 8)&255), (int)(m_nDuetColor&255), 255), 
      &index), __LINE__);

    DuetCheck(CALLoadFontName((LPCTSTR)m_szDuetFont, m_nDuetFontSize, 
      m_nDuetFontStyle, &myFont), __LINE__);
    DuetCheck(CALTextRgn((float) m_nWinPosX, (float) m_nWinPosY, 
      (float) m_nWinPosW, (float) m_nWinPosH, myFont, 
      index, ecalCenterLeft, &myCrawl), __LINE__); //x y w h
    DuetCheck(CALTextOptions(myCrawl, 0.0f, ecalNoClip, 0.0f, 0.0f), __LINE__);

    DuetCheck(CALCacheFontChars(myFont, 
      index, (LPCTSTR) m_szDuetCrawlText), __LINE__);
    
    m_szBackplate.TrimLeft();  m_szBackplate.TrimRight();
    if (m_szBackplate.Compare("") != 0)
    {
      if (fileExists((char *)((LPCTSTR)m_szBackplate)))
      {
        //AfxMessageBox("yo");
        DuetCheck(CALPlayI(myBackground), __LINE__);
      }
    }

    //AfxMessageBox("yo 2");
    DuetCheck(CALUpdateW(myCrawl, ecalUpdateSelect, 1, (LPCTSTR) m_szDuetCrawlText), __LINE__);
    DuetCheck(CALCrawl(myCrawl, ecalRate, (Real32) m_nDuetCrawlSpeed, ecalNoEase), __LINE__);
    DuetCheck(CALPlayI(myCrawl), __LINE__);

    DuetCheck(CALFreeColor(index), __LINE__);
  } 
  catch( HRESULT error)
  {
    // Handle error exception
    CString str;
    str.Format(_T("Error executing crawl on Duet (error 0x%x, %s)"), error, printCALError(error));
    AfxMessageBox(str);
  }
}



void loadDuetCrawl(CString text, int first, passDuet_t *settings)
{	
  if (!gnConnectedDuet)
  { AfxMessageBox("You're not connected to a Duet."); return; }
  
  if (first) m_szDuetCrawlText = text;
  else m_szDuetCrawlText += text;
}


void doLowerThirdDS(passDuet_t settings, CString line1, CString line2, 
                    CString line3, int numLines)
{
  int bp = TRUE;

  if (settings.szBackplate == "")
    bp = FALSE;

  doLowerThird(line1, line2, line3, numLines,
            GSTR(settings.szDuetFont), settings.nDuetColor, settings.nDuetFontStyle, 
            settings.nDuetFontSize, settings.nWinPosX, settings.nWinPosY, 
            settings.nWinPosW, settings.nWinPosH, bp, 
            GSTR(settings.szBackplate));
}



HCAL lineRgn1, lineRgn2, lineRgn3;
//bool playing;

void doLowerThird(CString line1, CString line2, CString line3, int numLines,
                  char duetFontName[32], int duetFontColor, int duetFontStyle, 
                  int duetFontSize, int x, int y, int w, int h, int bp, 
                  char bpName[])
{
  long fontIndex;
  int err, Swidth, Sheight, colorIndex;
  HCAL myBG;


  if (!gnConnectedDuet)
  { 
    //x// console(-1, __FILE__, MESSAGE_INFO, "You're not connected to a Duet."); 
    AfxMessageBox("You're not connected to a Duet."); 
    return; 
  }

  if (!getInfo(&Swidth, &Sheight)) return;

  try
  {
    // get video mode parameters
    getInfo(&Swidth, &Sheight);
    //printf("Screen: %d x %d\n", Swidth, Sheight);

    // Specify desired coordinates
    Real32 left = (float) x, top = (float) y, width = (float) w, height = (float) h;

    if ((width + x) > Swidth)   width = Swidth-left;  
    if ((height + x) > Sheight) height = Sheight-top;

    err = CALLoadFontName(duetFontName, duetFontSize,  duetFontStyle, &fontIndex);

    if (bp)
    {
      if (fileExists((char *)((LPCTSTR)bpName)))
      {
        // backplate
        DuetCheck(CALImageRgn( 0.0f, 0.0f, (float) Swidth, (float) Sheight, ecalCenter, ecalScaleToFit, &myBG), __LINE__);
        DuetCheck(CALSetName(myBG, "Background"), __LINE__);
        //x//console(-1, __FILE__, MESSAGE_INFO, "Attempting to load backplate '%s'\n", bpName);
        DuetCheck(CALCacheImage(myBG, "bg", bpName), __LINE__);
        DuetCheck(CALUpdateW(myBG, ecalUpdateSelect, 1, "bg"), __LINE__);
      }
      else 
      {
        //x// console(-1, __FILE__, MESSAGE_INFO, "Error loading backplate: '%s'\n", (LPCTSTR)bpName);
        bp = FALSE;
      }
    }
    
    if (err) fontIndex = 0;

    DuetCheck(CALLoadPalette(1, 
      CALRGBA((int)((duetFontColor >> 16)&255), (int)((duetFontColor >> 8)&255), (int)(duetFontColor&255), 255), 
      &colorIndex ), __LINE__); 

    if (numLines >= 1)
    {
      // Create Line N Text Region, Set background to white
      DuetCheck(CALTextRgn(left, top, width, height, fontIndex, colorIndex, ecalUpperLeft, &lineRgn1), __LINE__);
      //DuetCheck(CALBackground(lineRgn1, ecalLeftToRight, white, white), __LINE__);
      // Shift coordinates for "affiliation" region down
      top += duetFontSize + 10;
    }
    
    if (numLines >= 2)
    {
      // Create Line N Text Region, Set background to white
      DuetCheck(CALTextRgn(left, top, width, height, fontIndex,  colorIndex, ecalUpperLeft, &lineRgn2), __LINE__);
      //DuetCheck(CALBackground(lineRgn2, ecalLeftToRight, white, white), __LINE__);
      // Shift coordinates for "affiliation" region down
      top += duetFontSize + 10;
    }
    
    if (numLines >= 3)
    {
      // Create Line N Text Region, Set background to white
      DuetCheck(CALTextRgn(left, top, width, height, fontIndex,  colorIndex, ecalUpperLeft, &lineRgn3), __LINE__);
      //DuetCheck(CALBackground(lineRgn3, ecalLeftToRight, white, white), __LINE__);
      // Shift coordinates for "affiliation" region down
      top += duetFontSize + 10;
    }
    
    // Update text regions with data stored in member name
    if (numLines >= 1) DuetCheck(CALUpdateI(lineRgn1, 0, 1, line1.GetLength() < 1 ? _T(" ") : line1), __LINE__);
    if (numLines >= 2) DuetCheck(CALUpdateI(lineRgn2, 0, 1, line2.GetLength() < 1 ? _T(" ") : line2), __LINE__);
    if (numLines >= 3) DuetCheck(CALUpdateI(lineRgn3, 0, 1, line3.GetLength() < 1 ? _T(" ") : line3), __LINE__);
    
    
    // Display updated Text Regions
    if (bp) DuetCheck(CALPlayI(myBG), __LINE__);
    if (numLines >= 1) DuetCheck(CALPlayI(lineRgn1), __LINE__);
    if (numLines >= 2) DuetCheck(CALPlayI(lineRgn2), __LINE__);
    if (numLines >= 3) DuetCheck(CALPlayI(lineRgn3), __LINE__);
  } 
  catch( HRESULT error)
  {
    // Handle error exception
    CString str;
    str.Format(_T("Caught HRESULT error: 0x%x, %s"), error, printCALError(error));
    //x//console(__LINE__, __FILE__, MESSAGE_CRITICAL, "%s", (LPCTSTR) str);
    AfxMessageBox(str);
	}
}

#endif


////////////////////////////////////////////////////////////////////
//
// Squeezeback
//
// call SetupSetupSqueezeback(), then DoSqueezeIn(), then DoSqueezeOut()


void CDuetAPI::SetupSqueezeback()
{
  switch(m_nVideoHardwareType)
  {
  case VIDEO_HW_DUET_LEX:
    SetupSqueezebackCodi();
    break;
  case VIDEO_HW_DUET_SD:
    SetupSqueezebackDuet();
    break;
  default:
    // VGA mode or undefined hardware
    break;
  }
}

  
void CDuetAPI::ShutdownSqueezeback()
{
  switch(m_nVideoHardwareType)
  {
  case VIDEO_HW_DUET_LEX:
    ShutdownSqueezebackCodi();
    break;
  case VIDEO_HW_DUET_SD:
    ShutdownSqueezebackDuet();
    break;
  default:
    // VGA mode or undefined hardware
    break;
  }
}


void CDuetAPI::DoSqueezeOut(int nSqueezeFrames,
                            int nSqueezeX, int nSqueezeY,
                            int nSqueezeW, int nSqueezeH,
                            int nXCorrection, int nYCorrection)
{
  switch(m_nVideoHardwareType)
  {
  case VIDEO_HW_DUET_LEX:
    DoSqueezeOutCodi(nSqueezeFrames,
                     nSqueezeX, nSqueezeY,
                     nSqueezeW, nSqueezeH,
                     nXCorrection, nYCorrection);
    break;
  case VIDEO_HW_DUET_SD:
    DoSqueezeOutDuet(nSqueezeFrames,
                     nSqueezeX, nSqueezeY,
                     nSqueezeW, nSqueezeH,
                     nXCorrection, nYCorrection);
    break;
  default:
    // VGA mode or undefined hardware
    break;
  }
}



void CDuetAPI::DoSqueezeIn(int nSqueezeFrames,
                           int nSqueezeX, int nSqueezeY,
                           int nSqueezeW, int nSqueezeH,
                           int nXCorrection, int nYCorrection,
                           int nLayerCAL, int nLayerSqueeze)
{
  switch(m_nVideoHardwareType)
  {
  case VIDEO_HW_DUET_LEX:
    DoSqueezeInCodi(nSqueezeFrames,
                    nSqueezeX, nSqueezeY,
                    nSqueezeW, nSqueezeH,
                    nXCorrection, nYCorrection,
                    nLayerCAL, nLayerSqueeze);
    break;
  case VIDEO_HW_DUET_SD:
    DoSqueezeInDuet(nSqueezeFrames,
                    nSqueezeX, nSqueezeY,
                    nSqueezeW, nSqueezeH,
                    nXCorrection, nYCorrection,
                    nLayerCAL, nLayerSqueeze);
    break;
  default:
    // VGA mode or undefined hardware
    break;
  }
}




////////////////////////////////////////////////////////////////////
//
// Duet Squeezeback
//
// 


void CDuetAPI::SetupSqueezebackDuet()
{
  CString opts, szCRLF;
  
  szCRLF.Format("%c%c", 13, 10);
  
  // really don't want to change the params here...
  //SetVideoSync(ecalFreeRun);  //  ecalAnalogSync,ecalSDIVideo,ecalFreeRun

  SetVideoMixing(FALSE);
  
  // useful for scheduling - sets internal bias to sync system time and hardware timecode
  DuetCheck(CALSetOptions(0, "Preferences\\Timecode\\Bias=Auto"), __LINE__);
  
  // set maximum wait time on PlayW
  DuetCheck(CALSetOptions(0, "Preferences\\MaxWait=10"), __LINE__);
  
  // get a handle to the mixer
  DuetCheck(CALReserveDevice(ecalDeviceSDMixer, &m_Mixer), __LINE__);
  
  // stupidsticious
  opts = "";
  
  // kill the fader controls
  opts += "Layer1\\Fader=False" + szCRLF;
  opts += "Layer2\\Fader=False" + szCRLF;
  opts += "Layer3\\Fader=False" + szCRLF;
  opts += "Layer4\\Fader=False" + szCRLF;
  
  // setup the color conversion. VGE"s need RGB to YUV, Squeezer is straight YUV
  opts += "RGB2YUV\\GFX1=TRUE" + szCRLF;
  opts += "RGB2YUV\\GFX2=TRUE" + szCRLF;
  opts += "RGB2YUV\\GFX3=FALSE" + szCRLF;
  
  // gwr
  //opts += "Video=SDMixer" + szCRLF; // =VideoIO or SDMixer

  // instruct the mixer node to automatically remove itself from the playlist once it's played. Avoids repitition.
  opts += "AutoRemove=True" + szCRLF;
  
  // assign the options to the mixer
  DuetCheck(CALSetOptions(m_Mixer, opts), __LINE__);
  
  // get a handle to the Squeeze board
  DuetCheck(CALReserveDevice(ecalDeviceSqueeze, &m_DuetSqueeze), __LINE__);
  
  // clear opts
  opts = "";
  
  // enable the first of two squeezer channels, no key, and no shaping
  opts += szCRLF + "Region1=True";
  opts += szCRLF + "Region1\\KeyInput=False";
  opts += szCRLF + "Region1\\Shaping=False";
  
  opts += szCRLF + "Region2=False";
  opts += szCRLF + "Region2\\KeyInput=False";
  opts += szCRLF + "Region2\\Shaping=False";
  
  // instruct the squeezer node to automatically remove itself from the playlist once it's played. Avoids repitition.
  opts += szCRLF + "AutoRemove=True";
  
  // set the options
  try 
  {
    DuetCheck(CALSetOptions(m_DuetSqueeze, opts), __LINE__);
  } 
  catch( HRESULT error)
  {
    // Handle error exception
    CString str;
    str.Format(_T("Error setting up squeezeback %s (Caught HRESULT error: 0x%x, %s)"), 
      m_szDuetAddress, error, printCALError(error));
    //AfxMessageBox(str);
    m_bConnected = FALSE;
  }
}


void CDuetAPI::ShutdownSqueezebackDuet()
{
  SetVideoMixing(TRUE);
}


void CDuetAPI::DoSqueezeOutDuet(int nSqueezeFrames,
                            int nSqueezeX, int nSqueezeY,
                            int nSqueezeW, int nSqueezeH,
                            int nXCorrection, int nYCorrection)
{
  int n=0;
  long attribs[25], keys[25];  // attribute to affect, keyframes
  Real32 values[25]; // values, initial scale factor

  int nWidth, nHeight;
  getVideoDimensions(&nWidth, &nHeight);

  DuetCheck(CALAnimate(m_DuetSqueeze+1, attribs, 0, keys, values, ecalClearKeyFrames), __LINE__);

  for (int i = 0; i<25; i++) {	attribs[i] = 0; keys[i] = 0; values[i] = 0.0; }

  // secret squeeze mode settings
  //attribs[n] = ecalFocus;	keys[n] = 0; values[n] =4;	n++;  //Add(ecalFocus, 0, 4);
  //attribs[n] = ecalShine;	keys[n] = 0; values[n] =4;	n++;  //Add(ecalShine, 0, 4);

		// Screen Origin

  attribs[n] = ecalXPos;	keys[n] = 0; values[n] =nSqueezeX + nXCorrection;	n++;
  attribs[n] = ecalYPos;	keys[n] = 0; values[n] =nSqueezeY + nYCorrection;	n++;

  attribs[n] = ecalXPos;	keys[n] = nSqueezeFrames; values[n] =nXCorrection;	n++;
  attribs[n] = ecalYPos;	keys[n] = nSqueezeFrames; values[n] =nYCorrection;	n++;
	
	// Screen Width & Height
  attribs[n] = ecalXScale;	keys[n] = 0; values[n] =nSqueezeW;	n++;
  attribs[n] = ecalYScale;	keys[n] = 0; values[n] =nSqueezeH;	n++;

  attribs[n] = ecalXScale;	keys[n] = nSqueezeFrames; values[n] =nWidth;	n++;
  attribs[n] = ecalYScale;	keys[n] = nSqueezeFrames; values[n] =nHeight;	n++;

  // Video Offset
  attribs[n] = ecalXCenter;	keys[n] = 0; values[n] =0;	n++;
  attribs[n] = ecalYCenter;	keys[n] = 0; values[n] =0;	n++;

	// Video Width & Height
  attribs[n] = ecalXRot;	keys[n] = 0; values[n] =100;	n++;
  attribs[n] = ecalYRot;	keys[n] = 0; values[n] =100;	n++;

  try 
  {
    // apply
    DuetCheck(CALAnimate(m_DuetSqueeze+1, attribs, n, keys, values, 
      (enum ecalInterpMode) ecalLinear /*| ecalClearKeyFrames */), __LINE__);
    
    // play it
    DuetCheck(CALPlayI(m_DuetSqueeze), __LINE__);
  }
  catch( HRESULT error)
  {
    CString str;
    str.Format(_T("Error executing squeeze in (Caught HRESULT error: 0x%x, %s)"), 
      error, printCALError(error));
  }
}



void CDuetAPI::DoSqueezeInDuet(int nSqueezeFrames,
                           int nSqueezeX, int nSqueezeY,
                           int nSqueezeW, int nSqueezeH,
                           int nXCorrection, int nYCorrection,
                           int nLayerCAL, int nLayerSqueeze)
{
  int n=0;
  long attribs[25], keys[25];  // attribute to affect, keyframes
  Real32 values[25]; // values, initial scale factor

  int nWidth, nHeight;
  getVideoDimensions(&nWidth, &nHeight);

  DuetCheck(CALAnimate(m_DuetSqueeze+1, attribs, 0, keys, values, ecalClearKeyFrames), __LINE__);

  for (int i = 0; i<25; i++) {	attribs[i] = 0; keys[i] = 0; values[i] = 0.0; }

  // secret squeeze mode settings
  //attribs[n] = ecalFocus;	keys[n] = 0; values[n] =4;	n++;  //Add(ecalFocus, 0, 4);
  //attribs[n] = ecalShine;	keys[n] = 0; values[n] =4;	n++;  //Add(ecalShine, 0, 4);

	// Screen Origin
  attribs[n] = ecalXPos;	keys[n] = 0; values[n] =nXCorrection;	n++;
  attribs[n] = ecalYPos;	keys[n] = 0; values[n] =nYCorrection;	n++;

  attribs[n] = ecalXPos;	keys[n] = nSqueezeFrames; values[n] =nSqueezeX + nXCorrection;	n++;
  attribs[n] = ecalYPos;	keys[n] = nSqueezeFrames; values[n] =nSqueezeY + nYCorrection;	n++;
  
	// Screen Width & Height
  attribs[n] = ecalXScale;	keys[n] = 0; values[n] =nWidth;	n++;
  attribs[n] = ecalYScale;	keys[n] = 0; values[n] =nHeight;	n++;

  attribs[n] = ecalXScale;	keys[n] = nSqueezeFrames; values[n] =nSqueezeW;	n++;
  attribs[n] = ecalYScale;	keys[n] = nSqueezeFrames; values[n] =nSqueezeH;	n++;

  // Video Offset
  attribs[n] = ecalXCenter;	keys[n] = 0; values[n] =0;	n++;
  attribs[n] = ecalYCenter;	keys[n] = 0; values[n] =0;	n++;

	// Video Width & Height
  attribs[n] = ecalXRot;	keys[n] = 0; values[n] =100;	n++;
  attribs[n] = ecalYRot;	keys[n] = 0; values[n] =100;	n++;

  try 
  {
    // apply
    DuetCheck(CALAnimate(m_DuetSqueeze+1, attribs, n, keys, values, 
      (enum ecalInterpMode) ecalLinear /*| ecalClearKeyFrames*/), __LINE__);

    // play it
    DuetCheck(CALPlayI(m_DuetSqueeze), __LINE__);

    // enable the mix
    Mix(TRUE, nLayerCAL, nLayerSqueeze);

    // kill video
    SetVideoMixing(FALSE);
  }
  catch( HRESULT error)
  {
    CString str;
    str.Format(_T("Error executing squeeze out (Caught HRESULT error: 0x%x, %s)"), 
      error, printCALError(error));
  }
}



// set the sync to one of:   ecalAnalogSync,ecalSDIVideo,ecalFreeRun
void CDuetAPI::SetVideoSync(long nSync)
{
  DuetCheck(CALVideoDefaults(ecalVIOSync, nSync), __LINE__);
}

//Set video mixing to true to enable mixing of video input with VGE
// set to false for squeezeback
void CDuetAPI::SetVideoMixing(BOOL bMix)
{
  DuetCheck(CALVideoDefaults(ecalVIOMix, (long) bMix), __LINE__);
}


// nLayerCAL, nLayerSqueeze: 0=VGE1, 1=VGE2, 2=VGE3 etc.
void CDuetAPI::Mix(BOOL bEnable, int nLayerCAL, int nLayerSqueeze)
{
  CString szCRLF, opts="", szLayerCAL, szLayerSqueeze;
  szCRLF.Format("%c%c", 13, 10);
  
  szLayerCAL.Format("VGE%d", nLayerCAL+1);
  szLayerSqueeze.Format("VGE%d", nLayerSqueeze+1);
  

  // should we assign things to layers
  if (bEnable)
  {
    opts += "LAYER1=" + szLayerCAL /*"VGE2"*/  + szCRLF; // put VGE1 on layer closest to viewer
    opts += "LAYER2=" + szLayerSqueeze /*"VGE3"*/  + szCRLF; // at time of writing, squeezer was in the VGE3 slot
    opts += "LAYER3=NONE"  + szCRLF;
    opts += "LAYER4=VGE2"  + szCRLF; // bypass layer (not mixed)
  }
  else // or not
  {
    opts += "LAYER1=NONE"  + szCRLF; // clear mixer layers
    opts += "LAYER2=NONE"  + szCRLF;
    opts += "LAYER3=NONE"  + szCRLF;
    opts += "LAYER4=VGE2"  + szCRLF; // leave bypass
  }
  
  opts += "CHANNEL1=MIXER"  + szCRLF; // route layers 1-3 to channel 1
  //opts += "CHANNEL2=BYPASS"  + szCRLF; // route bypass to channel 2
  opts += "CHANNEL2=MIXER"  + szCRLF; // route bypass to channel 2
  
  opts += "Layer1\\Fader=False" + szCRLF; // keep faders off
  opts += "Layer2\\Fader=False" + szCRLF;
  opts += "Layer3\\Fader=False" + szCRLF;
  opts += "Layer4\\Fader=False" + szCRLF;
  
  // apply settings
  DuetCheck(CALSetOptions(m_Mixer, opts), __LINE__)
}



void CDuetAPI::MixDefault(BOOL bEnable)
{
  CString szCRLF, opts=""; //, szLayerCAL, szLayerSqueeze;
  szCRLF.Format("%c%c", 13, 10);
  
//  szLayerCAL.Format("VGE%d", nLayerCAL+1);
//  szLayerSqueeze.Format("VGE%d", nLayerSqueeze+1);
  

  // should we assign things to layers
  if (bEnable)
  {
    opts += "LAYER1=VGE1" + szCRLF; // put VGE1 on layer closest to viewer
    opts += "LAYER2=VGE2" + szCRLF; // at time of writing, squeezer was in the VGE3 slot
    opts += "LAYER3=VGE3"  + szCRLF;
    opts += "LAYER4=VGE2"  + szCRLF; // bypass layer (not mixed)
  }
  else // or not
  {
    opts += "LAYER1=NONE"  + szCRLF; // clear mixer layers
    opts += "LAYER2=NONE"  + szCRLF;
    opts += "LAYER3=NONE"  + szCRLF;
    opts += "LAYER4=VGE2"  + szCRLF; // leave bypass
  }
  
  opts += "CHANNEL1=MIXER"  + szCRLF; // route layers 1-3 to channel 1
  //opts += "CHANNEL2=BYPASS"  + szCRLF; // route bypass to channel 2
  opts += "CHANNEL2=MIXER"  + szCRLF; // route bypass to channel 2
  
  opts += "Layer1\\Fader=False" + szCRLF; // keep faders off
  opts += "Layer2\\Fader=False" + szCRLF;
  opts += "Layer3\\Fader=False" + szCRLF;
  opts += "Layer4\\Fader=False" + szCRLF;
  
  // apply settings
  DuetCheck(CALSetOptions(m_Mixer, opts), __LINE__)
}

//////////////////////////////////////////////////////////////////////////////


//  This function will recurse through a group to find a named region.  
//  Inputs:  rgnScene, the handle to the group
//           szRgnName, the region name from Lyric that we're looking for
//  Outputs: returns TRUE if found, FALSE otherwise
//           *rgnHandle, the region handle if found.
//  Author:  Gregg Riedel, 8-12-2002
BOOL CDuetAPI::GetFieldHandle(HCAL rgnScene, CString szRgnName, 
                                  HCAL *rgnHandle)
{
  char rname[100];
  //CString szTemp;

  // Get region info to check .type==ecalText, not used anymore
  //CALRGNINFO info;
  //DuetCheck(CALGetRgnInfo(rgnScene, &info), __LINE__);
  
  // Are YOU my daddy?
  DuetCheck(CALGetName(rgnScene, rname, 99), __LINE__);
  if (szRgnName.CompareNoCase(rname) == 0)
  {
    // WHO'S yo daddy!?
    *rgnHandle = rgnScene;  // return this region handle!
    //szTemp.Format("handle = %ld", rgnScene); AfxMessageBox(szTemp);
    return TRUE;
  }
  
  // Get children 		
  long dwCount, dwSize, x;

  // pass NULL first time to get count only
  DuetCheck(CALGetRgnIds(rgnScene, (long *) NULL, 0, &dwCount), __LINE__);

  dwSize = dwCount+1;
  if (dwCount > 0)
  {
    // allocate the children array...
    HCAL *children = (HCAL *)malloc((dwSize) * sizeof(HCAL));
    if (children != NULL)
    {
      // ... and populate it.
      DuetCheck(CALGetRgnIds(rgnScene, children, dwSize, &dwCount), __LINE__);

      // recurse for each child
      for (x=0; x<dwCount; x++)
      {
        if (GetFieldHandle(children[x], szRgnName, rgnHandle))
          return TRUE;
      }
      free(children);
    }
  }
  return FALSE;
}


// This function will get the projected verticies from a
// region in a scene that has been imported from Lyric
BOOL CDuetAPI::GetProjectedVertices(const char *pSceneName, 
                  const char *pRgnName,
                  double *vrtX0, double *vrtY0, 
                  double *vrtX1, double *vrtY1)
{
  BOOL bRV = FALSE;
  // this pseudo code is free hand with no validation
  
  // decl script buffer
  wchar_t script[1 << 10];
  
  // ez sizeof
  const Size = sizeof(script)/sizeof(script[0]);
  
  // script template
  const wchar_t *Template =
    L"CAL::GetID %S%S id\n"                   // parks rgn id into 'id'
    L"set obj [expr $id + 1]\n"               // reference first obj in region
    L"CAL::ProjectVertices $obj 0 start\n"    // get vertices at frame 0
    L"CAL::ProjectVertices $obj 999 end\n"    // get vertices at last frame
    L"return \"fnarg $start\\nblarg $end\"";              // return first and last vertices
  
  // vertices will be packed in Tcl list fashion; e.g., {10.0 245.2342 400.0 500.0}
  // where the curly braces are include in the result string and tbe double values
  // are seperated by white space. { top-left-x top-left-y bottom-right-x bottom-right-y }
  
  // format expression
  _snwprintf(script, Size, Template, pSceneName, pRgnName);
  
  // safety
  script[Size  - 1] = L'\0';

  //MessageBoxW(0, script, L"Script", 0);


  // decl VARIANT and initialize
  VARIANT v;
  VariantInit(&v);
  
  // set to bstr
  v.vt = VT_BSTR;
  v.bstrVal = SysAllocString(script);
  
  // send
  //MessageBoxW(0, v.bstrVal, L"BSTR v before perform", 0);
  long retval = CALPerform(&v);
  //MessageBoxW(0, v.bstrVal, L"BSTR v before perform", 0);
  
  // test
  if (SUCCEEDED(retval))
  {
    
    // decl alt variant
    VARIANT v2;
    VariantInit(&v2);
    
    // convert (shouldn't be necessary)
    if (SUCCEEDED(VariantChangeType(&v2, &v, 0, VT_BSTR)))
    {
      
      // simplify
      const wchar_t *p = ((v2.bstrVal != NULL) ? (v2.bstrVal) : (L"ha?"));
      
      // ok
      //MessageBoxW(0, p, L"Projected Vertices", 0);
      //MessageBoxW(0, v2.bstrVal, L"BSTR v", 0);
      
      // this may work for parsing - remember, we have two sets of vertices
      // above. I'm only going to scan for one set.
      
      // decl doubles
      double x0, y0, x1, y1;
      
      // parse
      if (swscanf(p, L" %lf %lf %lf %lf ", &x0, &y0, &x1, &y1) == 4)
      {
        // ok
        bRV = TRUE;
        *vrtX0  = x0;
        *vrtY0  = y0;
        *vrtX1  = x1;
        *vrtY1  = y1;
      }
      // clear v2
      VariantClear(&v2);
    }
  }
  
  // clear out val
  VariantClear(&v);
  return bRV;
}


/////////////////////////////////////////////////////////////////////////
//
//  Duet LE(x) Squeezeback routines
//

void CDuetAPI::SetupSqueezebackCodi()
{
  ecalDeviceType sqz;

  switch(m_nCodiSqueezeBoardNumber)
  {
  case 1: sqz = ecalDeviceCodi0; break;
  case 2: sqz = ecalDeviceCodi1; break;
  case 3: sqz = ecalDeviceCodi2; break;
  case 4: sqz = ecalDeviceCodi3; break;
  }

  try
  {
    // Do I need to reserve the board?
	  //Check(CALReserveDevice(ecalDeviceCodi2, m_CodiSqueeze))
	  DuetCheck(CALReserveDevice(sqz, &m_CodiSqueeze), __LINE__);
    
    // video blank on
    DuetCheck(CALUpdateW(m_CodiSqueeze, ecalUpdateSelect, 1, "\\VB\\1\\\\"), __LINE__);

    // force video key in OFF
    DuetCheck(CALUpdateW(m_CodiSqueeze, ecalUpdateSelect, 1, "\\VY\\1\\\\"), __LINE__);

    // turn off both video planes
    // \SQ\V\Video Index(1\2,3=both)\{1=on/0=off}\
    // {0=Video Resizer 1 on top of Resizer 2/
    // {1=Video Resizer 2 on top of Resizer 1}\\
    // (V)ideo Adjust Mode: This command interactively turns the Video Plane On and Off and Changes the Priority of Video 2 and Video 1.
    DuetCheck(CALUpdateW(m_CodiSqueeze, ecalUpdateSelect, 1, "\\SQ\\V\\3\\0\\\\"), __LINE__);

    // Set graphics on top, graphics plane on.
    // \SQ\G\{1=Top/0=Bottom}\{1=on/0=off}
    DuetCheck(CALUpdateW(m_CodiSqueeze, ecalUpdateSelect, 1, "\\SQ\\G\\1\\1\\\\"), __LINE__);
  }
  catch (HRESULT error)
  {
    // Handle error exception
    CString str;
    str.Format(_T("Error setting up squeeze board %d - is board number in registry correct? (Caught HRESULT error: 0x%x, %s)"), 
      m_nCodiSqueezeBoardNumber, error, printCALError(error));
    AfxMessageBox(str);
    return;
  }
}

void CDuetAPI::ShutdownSqueezebackCodi()
{
}


CString CDuetAPI::BuildCodiCommand(int vEffectId, int vRegion, int StartX, int EndX,
                                   int StartY, int EndY, int StartWidth, 
                                   int EndWidth,int StartHeight, int EndHeight, int Frames,
                                   int Ease, int FadeOn, int ClipTop, 
                                   int ClipBottom, int ClipLeft, int ClipRight,
                                   int GraphicsPlaneOn, int BackgroundOn, 
                                   int ResizerOn, int GraphicsOnTop,
                                   int Resizer2Over1, int BackgroundFrameDelay, 
                                   int DissolveEffect)
{
  CString szBuildCommand;

  // verify args
  if ((vRegion < 1) || (vRegion > 2))
  {
    AfxMessageBox("BuildCodiCommand Error: Region must be 1 or 2");
    return "";
  }
  
  if ((vEffectId < 1) || (vEffectId > 10))
  {
    AfxMessageBox("BuildCodiCommand Error: Effect id must be in the range [1..10]");
    return "";
  }
 /* 
  // ' hardware horz unit measure is 8 for clip
  // mvarClipRight = (vData + 4) / 8 ' round to nearest
  // ' hardware vert unit measure is 2 for clip
  // mvarClipBottom = (vData + 1) / 2 ' round to nearest
  
  //StartX = (StartX + 4) / 8; // no change; though, odd value will clamp to neighbor
  //EndX = (EndX + 4) / 8; // no change; though, odd value will clamp to neighbor
  StartY = (StartY + 1) / 2;
  EndY = (EndY + 1) / 2;

  // unit measure is 1 (hardware granularity of 2)
  // e.g., 360 & 361 both evaluate to 360
  // e.g., 489 & 490 evaluate to 488 & 490 respectively
  // no change; though, odd value will clamp to neighbor
  //StartWidth, EndWidth ok
  StartHeight = (StartHeight + 1) / 2;
  EndHeight = (EndHeight + 1) / 2;

  // hardware horz unit measure is 8 for clip
  // mvarClipRight = (vData + 4) / 8 ' round to nearest
*/
  ClipLeft = (ClipLeft + 4) / 8;
  ClipRight = (ClipRight + 4) / 8;
  ClipBottom = (ClipBottom + 1) / 2;
  ClipTop = (ClipTop + 1) / 2;

  szBuildCommand.Format("\\SQ\\S\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\%d\\\\",
    vEffectId, vRegion, StartX, EndX, StartY, EndY, 
    StartWidth, EndWidth, StartHeight, EndHeight, Frames,
    Ease, FadeOn, ClipTop, ClipBottom, ClipLeft, ClipRight,
    GraphicsPlaneOn, BackgroundOn, ResizerOn, GraphicsOnTop,
    Resizer2Over1, BackgroundFrameDelay, DissolveEffect);

  return szBuildCommand;
}


void CDuetAPI::DoSqueezeOutCodi(int nSqueezeFrames,
                            int nSqueezeX, int nSqueezeY,
                            int nSqueezeW, int nSqueezeH,
                            int nXCorrection, int nYCorrection)
{
  int nWidth, nHeight;
  getVideoDimensions(&nWidth, &nHeight);
  CString szCodiCommand;

  szCodiCommand = BuildCodiCommand(
    1, //int vEffectId, 
    1, // int vRegion, 
    nSqueezeX, 0, // start, end X
    nSqueezeY, 0, // start, end Y
    nSqueezeW, nWidth, // start, end W
    nSqueezeH, nHeight, // start, end H
    nSqueezeFrames,
    0, 0, // no ease, no fade on
    0, 0, // Scanlines to Clip off the top, bottom of the Video.
    0, 0, // Pixels to Clip off the left, right of the Video.
    1, 1, 1, // int GraphicsPlaneOn, int BackgroundOn, resizer on
    1, // iGraphicsPriority=1:	Set/Resets Priority of Graphics over video.
    1, // iResizer2OnTop=1:	Sets Video Resizer 2 Over Video Resizer 1.
    1, // int BackgroundFrameDelay,  (1 frame delay; 1=ON, 0=OFF)
    0); // int DissolveEffect
    
  // send settings to CODI
  DuetCheck(CALUpdateW(m_CodiSqueeze, ecalUpdateSelect, 1, szCodiCommand), __LINE__);

  // trigger the effect
  // \SQ\P\SqueezeBack Index (1-10)\Video Index(1/2,3=Both)\
  // (I)=Interpolate\(R)=Reverse,(F)=Forward\
	// (D)=Display Non-Display Buffer\\
  // (P)lay Mode: This command plays the effect previously defined with the Setup command.
  // (I)nterpolate will cause sub scanline and sub pixel movement during an animation. Using this requires BOTH Video 1 and Video 2 In to be the same.
	// (R)-(F): Reverse and Forward flag plays the animation backwards and forwards.
	// (D): In the same field as the animation starts a switch will be made between the non-displaying graphic plane and the displaying graphic plane.
  DuetCheck(CALUpdateW(m_CodiSqueeze, ecalUpdateSelect, 1, 
    "\\SQ\\P\\1\\1\\F\\D\\\\"), __LINE__);
    // "\\SQ\\P\\1\\3\\F\\D\\\\"), __LINE__);
}



void CDuetAPI::DoSqueezeInCodi(int nSqueezeFrames,
                           int nSqueezeX, int nSqueezeY,
                           int nSqueezeW, int nSqueezeH,
                           int nXCorrection, int nYCorrection,
                           int nLayerCAL, int nLayerSqueeze)
{
  int nWidth, nHeight;
  getVideoDimensions(&nWidth, &nHeight);
  CString szCodiCommand;

  szCodiCommand = BuildCodiCommand(
    1, //int vEffectId, 
    1, // int vRegion, 
    0, nSqueezeX, // start, end X
    0, nSqueezeY, // start, end Y
    nWidth, nSqueezeW,  // start, end W
    nHeight, nSqueezeH, // start, end H
    nSqueezeFrames,
    0, 0, // no ease, no fade on
    0, 0, // Scanlines to Clip off the top, bottom of the Video.
    0, 0, // Pixels to Clip off the left, right of the Video.
    1, 1, 1, // int GraphicsPlaneOn, int BackgroundOn, resizer on
    1, // iGraphicsPriority=1:	Set/Resets Priority of Graphics over video.
    1, // iResizer2OnTop=1:	Sets Video Resizer 2 Over Video Resizer 1.
    1, // int BackgroundFrameDelay,  (1 frame delay; 1=ON, 0=OFF)
    0); // int DissolveEffect
    
  // send settings to CODI
  DuetCheck(CALUpdateW(m_CodiSqueeze, ecalUpdateSelect, 1, szCodiCommand), __LINE__);

  // trigger the effect
  // \SQ\P\SqueezeBack Index (1-10)\Video Index(1/2,3=Both)\
  // (I)=Interpolate\(R)=Reverse,(F)=Forward\
	// (D)=Display Non-Display Buffer\\
  // (P)lay Mode: This command plays the effect previously defined with the Setup command.
  // (I)nterpolate will cause sub scanline and sub pixel movement during an animation. Using this requires BOTH Video 1 and Video 2 In to be the same.
	// (R)-(F): Reverse and Forward flag plays the animation backwards and forwards.
	// (D): In the same field as the animation starts a switch will be made between the non-displaying graphic plane and the displaying graphic plane.
  DuetCheck(CALUpdateW(m_CodiSqueeze, ecalUpdateSelect, 1, 
    "\\SQ\\P\\1\\1\\F\\D\\\\"), __LINE__);
    // "\\SQ\\P\\1\\3\\F\\D\\\\"), __LINE__);
}

