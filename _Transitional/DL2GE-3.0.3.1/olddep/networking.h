//   Networking.h
// (c) 2000-Present Video Design Interactive.  All Rights Reserved.
// This file constitutes the proprietary, confidential intellectual 
// property of Video Design Interactive and may not be distributed.

#ifndef CNETWORKING_INCLUDED
#define CNETWORKING_INCLUDED


#ifndef TRY_PROTECTED
// called with this proto: TRY_PROTECTED(LPFUNC Function(arg), LPFUNCRSLT pnRV, LPSTSTR pszError, CString szFuncName, UINT MB_OK, LPUINT)
#define TRY_PROTECTED(FUNC, LPFUNCRSLT, LPERRORSTRING, FUNCNAME, MBTYPE, LPMBRSLT) \
  try  \
  { \
		if(LPFUNCRSLT != NULL)  *LPFUNCRSLT = FUNC; \
		else FUNC;\
  } \
  catch(CException *e) \
  { \
		if(LPFUNCRSLT != NULL)  *LPFUNCRSLT = UINT(-1); \
    if (LPERRORSTRING != NULL) \
		{ \
			char buffer[1024]; \
			if(e->GetErrorMessage(buffer, 1024)) \
				LPERRORSTRING->Format("%s exception: %s", FUNCNAME, buffer); \
			else \
				LPERRORSTRING->Format("Unknown exception in %s.", FUNCNAME); \
    }\
		if(MBTYPE>=0) \
		{\
			UINT r = e->ReportError(MBTYPE);\
			if(LPMBRSLT != NULL) *LPMBRSLT = r;\
		}\
  }
#endif


typedef char *caddr_t;        /* UNIX <core address> type */

// for reference
// From arpa/telnet.h: 
#define TELNET_IAC     255             // interpret as command: 
#define TELNET_DONT    254             // you are not to use option 
#define TELNET_DO      253             // please, you use option 
#define TELNET_WONT    252             // I won't use option 
#define TELNET_WILL    251             // I will use option 
#define TELNET_SB      250             // interpret as subnegotiation 
#define TELNET_GA      249             // you may reverse the line 
#define TELNET_EL      248             // erase the current line 
#define TELNET_EC      247             // erase the current character 
#define TELNET_AYT     246             // are you there 
#define TELNET_AO      245             // abort output--but let prog finish 
#define TELNET_IP      244             // interrupt process--permanently 
#define TELNET_BREAK   243             // break 
#define TELNET_DM      242             // data mark--for connect. cleaning 
#define TELNET_NOP     241             // nop 
#define TELNET_SE      240             // end sub negotiation 
#define TELNET_EOR     239             // end of record (transparent mode) 
#define TELNET_ABORT   238             // Abort process 
#define TELNET_SUSP    237             // Suspend process 
#define TELNET_xEOF    236             // End of file: EOF is already used... 

#define TELOPT_BINARY         0	  // 8-bit data path 
#define TELOPT_ECHO           1	  // echo 
#define	TELOPT_RCP            2	  // prepare to reconnect 
#define	TELOPT_SGA            3	  // suppress go ahead 
#define	TELOPT_NAMS           4	  // approximate message size 
#define	TELOPT_STATUS         5	  // give status 
#define	TELOPT_TM             6	  // timing mark 
#define	TELOPT_RCTE           7	  // remote controlled transmission and echo 
#define TELOPT_NAOL           8	  // negotiate about output line width 
#define TELOPT_NAOP           9	  // negotiate about output page size 
#define TELOPT_NAOCRD        10	  // negotiate about CR disposition 
#define TELOPT_NAOHTS        11	  // negotiate about horizontal tabstops 
#define TELOPT_NAOHTD        12	  // negotiate about horizontal tab disposition 
#define TELOPT_NAOFFD        13	  // negotiate about formfeed disposition 
#define TELOPT_NAOVTS        14	  // negotiate about vertical tab stops 
#define TELOPT_NAOVTD        15  	// negotiate about vertical tab disposition 
#define TELOPT_NAOLFD        16  	// negotiate about output LF disposition 
#define TELOPT_XASCII        17  	// extended ascic character set 
#define	TELOPT_LOGOUT        18  	// force logout 
#define	TELOPT_BM            19	  // byte macro 
#define	TELOPT_DET           20   // data entry terminal 
#define	TELOPT_SUPDUP        21	  // supdup protocol 
#define	TELOPT_SUPDUPOUTPUT  22   // supdup output 
#define	TELOPT_SNDLOC        23   // send location 
#define	TELOPT_TTYPE         24   // terminal type 
#define	TELOPT_EOR           25   // end or record 
#define TELOPT_EXOPL	       255  // extended-options-list 


#define MAXHOSTNAME 256
#define NET_MAX_CONNS 32
/*
#define EOLN_CR   0
#define EOLN_LF   1
#define EOLN_CRLF 2
#define EOLN_NONE 3
*/
// had to redef to:
#define EOLN1_CR   0
#define EOLN1_LF   1
#define EOLN1_CRLF 2
#define EOLN1_NONE 3


// error codes
#define RETURN_SUCCESS					  	  0
#define ERROR_NONE							  	  0
#define ERROR_NO_WINSOCK				  	  -1
#define ERROR_WINSOCK_VERSION		  	  -2
#define ERROR_HOST_INVALID				    -3
#define ERROR_SOCKET_CREATE				    -4
#define ERROR_SOCKET_NOT_IN_LIST	    -5
#define ERROR_SOCKET_MAXNUM_REACHED	  -6
#define ERROR_SOCKET_GETSOCKNAME	    -7
#define ERROR_SOCKET_SEND        	    -8
#define ERROR_SOCKET_RECV        	    -9
#define ERROR_SOCKET_BIND        	    -10
#define ERROR_SOCKET_LISTEN      	    -11
#define ERROR_MALLOC_FAILED      	    -12
#define ERROR_SERVER_MAXNUM_REACHED	  -13
#define ERROR_SERVER_NOT_IN_LIST	    -14
#define ERROR_CHECKSUM          	    -15
#define ERROR_BUFFER_MAXLEN_EXCEEDED  -16
#define ERROR_MSINET_CONNECT          -17
#define ERROR_MSINET_READ             -18
#define ERROR_FILE_NOT_OPEN           -19

#define SOFTERROR_SOCKET_SETSOCKOPTS     1
#define SOFTERROR_SOCKET_NOCHAR          2
#define SOFTERROR_MALLOC_FAILED				   3
#define SOFTERROR_BUFFER_MAXLEN_EXCEEDED 4

#define RETURN_NOTHING				  	  -1

#define URL_MODE_TEXT				  	  0
#define URL_MODE_FILE				  	  1

#define USE_LOGGING

// the following line forces a link with the winsock lib, so you don't have to!
#pragma comment(lib, "ws2_32.lib")

// include the winsock headers
#include <winsock2.h>
#include <afxinet.h>

class CNetworking // : public CObject
{
public:
	CNetworking();   // standard constructor
	~CNetworking();   // standard destructor

#ifdef USE_LOGGING
	BOOL m_bWriteFileOps;
	BOOL m_bWriteFileErrors;
	CString m_szFilename;
#endif

  struct { int nSocket, nMode; } m_TelnetFlags[NET_MAX_CONNS];
  BOOL  m_bNetworkingStarted;
  BOOL  m_bOverflow;
  int   m_nTelnetCount;
  char* m_pszOverflowBuffer;

  // Server
  fd_set m_fdsRead;
//	fd_set m_fdsWrite;
  BOOL   m_bServerListening;
  int    m_nNumServers;
  struct  { int nPort; SOCKET socket; } m_ServerInfo[NET_MAX_CONNS];


  // protos
#ifdef USE_LOGGING
private:
	CString GetTimestamp(); // for logging
	void LogToFile(CString szText);
#endif

public:
  int  StartNetworking(CString* pszInfo=NULL);
  int  ShutdownNetworking(CString* pszInfo=NULL);
  int  OpenConnection(CString szHost, int nPort, SOCKET* ps, int nTelnet, int nTimeoutMS=30000, CString* pszInfo=NULL);
  int  CloseConnection(SOCKET s, BOOL bDoUnset=TRUE, CString* pszInfo=NULL);
  int  SendCommand(char text[], SOCKET s, BOOL bUnicode=FALSE, int nEoln=EOLN1_NONE, CString* pszInfo=NULL);
  int  GetCharFromSocket(SOCKET s, char* pch, int nLine=-1, CString* pszInfo=NULL);
  int  GetLineFromSocket(char line[], SOCKET s, CString* pszInfo=NULL);
  int  GetReplyTelnet(char buffer[1024], SOCKET s, int* pnLen, CString* pszInfo=NULL);
  int  GetReply(char buffer[1024], SOCKET s, int* pnLen, CString* pszInfo=NULL);
  int  SendTelnetIACresponse(int yes, char command, char option, SOCKET s, CString* pszInfo=NULL);
  int  TelnetIACresponse(char command, SOCKET s, CString* pszInfo=NULL);
  int  SetTelnet(int nSocket, int nMode, CString* pszInfo=NULL);
  int  UnsetTelnet(int nSocket, CString* pszInfo=NULL);
  int  GetTelnet(int nSocket, int* pnMode, CString* pszInfo=NULL);
  void UnicodeToASCII(char* unicode, char* ascii, int nUnilen);
  void ASCIIToUnicode(char* ascii, char* unicode, int* pnUnilen);
  void PrintSocketError(int n, int nLine, CString* szError);

  // server
  SOCKET PollServer(int nPort, CString* pszIP, int* pnPort, int nTimeoutMS=10);
  int StartServer(int nPort, CString* pszName=NULL, CString* pszInfo=NULL);
  int ShutdownServer(int nPort, CString* pszInfo=NULL);
  int GetServerRequest(SOCKET s, BYTE* pRequest, CString* pszRequestText, CString* pszInfo=NULL);
  int ServerListener(int nPort, SOCKET* ps, BYTE* pRequest, CString* pszData, CString* pszIP, int* pnPort, int nTimeoutMS=10, CString* pszInfo=NULL);
  int ServerListener(int nPort, SOCKET* ps, BYTE* pRequest, CString* pszData, int nTimeoutMS=10, CString* pszInfo=NULL);
  int ServerSendCmd(int cmd, CString szServerIP, int nServerPort, BYTE* pReplyCmd, CString* pszReply, BOOL bRetry=TRUE, int nTimeoutMilliseconds=10000, CString* pszInfo=NULL);
  int ServerSendCmd(int cmd, CString szData, CString szServerIP, int nServerPort, BYTE* pReplyCmd, CString* pszReply, BOOL bRetry=TRUE, int nTimeoutMilliseconds=10000, CString* pszInfo=NULL);
  int ServerSendCmdReply(SOCKET s, int cmd, BOOL bRetry=TRUE, int nTimeoutMilliseconds=10000, CString* pszInfo=NULL);
  int ServerSendCmdReply(SOCKET s, int cmd, CString szData, BOOL bRetry=TRUE, int nTimeoutMilliseconds=10000, CString* pszInfo=NULL);
  int ServerSendCmdInternals( CString szCmdString, int nLen, BOOL bIsReply, SOCKET s, CString szServerIP, int nServerPort, BYTE* pReplyCmd, CString* pszReplyData, BOOL bRetry=TRUE, int nTimeoutMilliseconds=10000, CString* pszInfo=NULL);
  BYTE Checksum(BYTE *cmd, unsigned long len);
  void PrintIP(struct sockaddr_in i, char ip[], BOOL bPort=FALSE);
  CString ServerGetStringFromSize(unsigned long dwSize);
  int GetServerSocket(int port, int *index, SOCKET *s, CString* pszInfo=NULL);

  // inet
  int RetrieveURLToText(CString szURL, CString* pszTextData, BOOL bSecure=FALSE, BOOL bCache=FALSE, CString szHeaders="", CString* pszInfo=NULL);
  int RetrieveURLToFile(CString szURL, CString szTempfile, BOOL bSecure=FALSE, BOOL bCache=FALSE, CString szHeaders="", CString* pszInfo=NULL);
  int RetrieveURLInternals(int nMode, FILE* fp, CString szURL, CString* pszTextData, BOOL bSecure=FALSE, BOOL bCache=FALSE, CString szHeaders="", CString* pszInfo=NULL);
  int RetrieveURLInternalsAdvanced(int nMode, FILE* fp, 
    CString szServer, int nPort, CString szObject, CString* pszTextData, 
    BOOL bSecure, BOOL bCache, CString szHeaders, CString szData, 
    CString szUser, CString szPassword, CString* pszInfo=NULL);

//  BOOL retrieveURLInternalsVDI(int nMode, FILE *fp, CString URL, CString *textData, CString *error, BOOL bApacheMode);

};

#endif
