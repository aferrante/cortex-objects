// (c) 2000-Present Video Design Interactive.  All Rights Reserved.
// This file constitutes the proprietary, confidential intellectual 
// property of Video Design Interactive and may not be distributed.

// changelog
// 1-8-2003 - fixed a bug in breakStringOnSepChar that would only
//            return 2 items for "item1|item2|"

#include "StdAfx.h"
#include "TextUtils.h"
#include <io.h>


CTextUtils::CTextUtils()
{
}

CTextUtils::~CTextUtils()
{
}



// The following breaks a single CString (szIn) and
// breaks it into some number of CStrings (szOut[])
// on a separator character (cSepChar).  The separator
// characters are discarded, and the total number of
// CStrings created is returned.  Finally, you must 
// pre-allocate the static array szOut[] with enough
// space so that it will not be overrun.
//
// You can optionally pass in the array size, which will 
// be respected.  However, buffer overruns are silent - 
// no error is generated.
int CTextUtils::breakStringOnSepChar(unsigned char cSepChar, 
                                     CString szIn, CString szOut[], 
                                     int nArraySize, int *nTotalItemsFound)
{
  int nNumberOfItems=0, nPosition;
  CString szRestOfString;

  if (nTotalItemsFound != NULL) *nTotalItemsFound = 0;

  if (szIn == "") return 0;

  szRestOfString = szIn;
  nPosition = szIn.Find(cSepChar);
  if (nPosition < 0) { szOut[0] = szIn; return 1; }

  while (nPosition >= 0)
  {
    if ((nArraySize == -1) || (nNumberOfItems < nArraySize))
      szOut[nNumberOfItems++] = szRestOfString.Left(nPosition);
    if (nTotalItemsFound != NULL) { (*nTotalItemsFound)++; }

    szRestOfString = szRestOfString.Right(szRestOfString.GetLength() - (nPosition+1));
    nPosition = szRestOfString.Find(cSepChar);
  }
  
  if ((szRestOfString != "") || (szIn.Right(1) == cSepChar))
  {
    if ((nArraySize == -1) || (nNumberOfItems < nArraySize))
      szOut[nNumberOfItems++] = szRestOfString;
    if (nTotalItemsFound != NULL) { (*nTotalItemsFound)++; }
  }
  return nNumberOfItems;
}


CString CTextUtils::formatTicks(unsigned long tickCount)
{
  CString szTick;
  int h, m, s;
  s = tickCount / 1000;
  h = s / 3600; s = s - (h*3600); m = s / 60;  s = s - (m*60);
  szTick.Format("%02d:%02d:%02d.%03d", h, m, s, tickCount % 1000);
  return szTick;
}


// Ah, the venerable "AMB", a shortcut for AfxMessageBoxes...
void CTextUtils::AMB(int x)
{ CString temp; temp.Format("%d", x); AfxMessageBox(temp); }

void CTextUtils::AMB(double x)
{ CString temp; temp.Format("%lf", x); AfxMessageBox(temp); }

void CTextUtils::HAMB(int x)
{ CString temp; temp.Format("%02x", x); AfxMessageBox(temp); }

void CTextUtils::AMB(CString x)
{ AfxMessageBox(x); }

void CTextUtils::AMB(char fmt_string[], ... )
{
  char buffer[1024], c;
  va_list marker;
  int i, j, l;
  
  // create the formatted output string
  va_start(marker, fmt_string); // Initialize variable arguments.
  vsprintf(buffer, fmt_string, (va_list) marker);
  va_end( marker );             // Reset variable arguments.
  
  // Handle non-printing characters
  l = strlen(buffer);
  for (i=0; i<l; i++)
  {
    if ((buffer[i] < 27) && (buffer[i] != 10)&& (buffer[i] != 9))
    {
      c = buffer[i]; buffer[i]='^';
      for (j=l; j>i; j--) buffer[j+1] = buffer[j];
      buffer[i+1] = 'A' + (c-1); l++; i++;
    }
  }
  
  AfxMessageBox(buffer);
}


// A function for enforcing a single space, except for 2 after a period
// not optimal for large strings (should use buffer access)
CString CTextUtils::fixSpacing(CString szIn)
{
  CString szOut="";
  int i, nSpaceCount=0;
  char ch, lastNonSpaceChar='x';

  for (i=0; i<szIn.GetLength(); i++)
  {
    ch = szIn.GetAt(i);
    if ((ch == ' ') || (ch == '\t'))
    {
      nSpaceCount++;
    }
    else
    {
      if (nSpaceCount > 0)
      {
        if (lastNonSpaceChar == '.')
          szOut += "  ";
        else szOut += " ";
        nSpaceCount = 0;
      }
      szOut += (char) ch;
      lastNonSpaceChar = ch;
    }
  }
  return szOut;
}



// uses GetLastError to get the error string, which is returned as a CString
CString CTextUtils::formatLastError()
{
  return formatLastError((DWORD) GetLastError());
}

// creates an OS error string from an error code
// and returns it as a CString.
CString CTextUtils::formatLastError(DWORD dwLastError)
{
  LPVOID lpMsgBuf;
  CString szErrorString;
  
  FormatMessage( 
    FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
    NULL,
    dwLastError,
    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
    (LPTSTR) &lpMsgBuf,
    0,
    NULL 
    );
  szErrorString.Format("%s", (char *) lpMsgBuf);
  LocalFree( lpMsgBuf );
  return szErrorString;
}


// A function to strip out nasties from a string, usually
// for logging purposes.
CString CTextUtils::MakePrintableString(CString szString)
{
	CString szReturn="", szCh=""; char ch;
	for (int i=0; i<szString.GetLength(); i++)
	{
		ch=szString.GetAt(i);
		//if (((ch>31)&&(ch<127))|| // printable range, 
	  //	 ((ch>127)&&(ch<256)))
    //  szReturn+=ch;
		//else {
    if (ch < 32)
    {
      szReturn += "^";
      szReturn += (char)('A' + (ch-1));
    }
    else
		{
      switch(ch)
      {
      // various smart quotes, ellipses and em dashes
      case 0x84: szReturn += "\""; break;
      case 0x85: szReturn += "..."; break;
      case 0x91: szReturn += "'"; break;
      case 0x92: szReturn += "'"; break;
      case 0x93: szReturn += "\""; break;
      case 0x94: szReturn += "\""; break;
      case 0x96: szReturn += "-"; break;
      case 0x97: szReturn += "-"; break;

      //case 0x09: szReturn += "<tab>"; break; // tab
      //case 0x0a:  szReturn += "<LF>"; break; // LF
      //case 0x0d:  szReturn += "<CR>"; break; // CR
      default:
        // default here should be 'do nothing'
        // szCh.Format("[0x%02x]",ch); szReturn+=szCh;
         szReturn+=(char)ch;
        break;
      }
		}
	}
	return szReturn;
}


/*
 * This routine takes a string of text and removes duplicates.  
 * Very useful for CALCacheChars().  For example:
 *   Hello, world!
 * becomes something like:
 *    !,Hdelorw
 */
CString CTextUtils::Uniq(CString szString)
{
  CString szTemp="";
  int i;
  char ch;
  BOOL bUsed[256];

  for (i=0; i<256; i++) bUsed[i] = FALSE;

  for (i=0; i<szString.GetLength(); i++)
  {
    ch = szString[i];
    if (!bUsed[(int)ch])
    {
      szTemp += (char) ch;
      bUsed[(int)ch] = TRUE;
    }
  }
  return szTemp;
}


// define this to see if there are unnecessary encodes/decodes
//#define ENCODE_DEBUG 1

// remove pipes, percents and newlines
CString CTextUtils::Encode(CString szText)
{
  CString szOut = "";
  int i;
#ifdef ENCODE_DEBUG
  szOut = "E(";
#endif

  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '|')
      szOut += "%:";
    else if (szText[i] == '%')
      szOut += "%%";
    else if (szText[i] == '\n')
      szOut += "%^";
    else szOut += (char) szText[i];
  }

#ifdef ENCODE_DEBUG
  szOut += ")";
#endif
  
  return szOut;
}

// remove pipes
CString CTextUtils::Decode(CString szText)
{
  CString szOut = "";
  int i;

  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '%')
    {
      i++; if (i>=szText.GetLength()) break;
      if (szText[i] == '%')
      {
        szOut += "%";
      }
      else if (szText[i] == ':')
      {
        szOut += "|";
      }
      else if (szText[i] == '^')
      {
        szOut += "\n";
      }
    }
    else szOut += (char) szText[i];
  }

#ifdef ENCODE_DEBUG
  szOut = szOut.Mid(2, szOut.GetLength() - 3);
#endif
  
  return szOut;
}


// checks to see that all chars are in the range 0-9, a-f, A-F.
// a blank string (zero length) returns false
BOOL CTextUtils::IsHexString(CString szText) 
{
  int i=0, len = szText.GetLength() ;
	if (len<=0) return FALSE;
  char ch;
	BOOL bHex=TRUE;
  while ((i<len)&&(bHex))
  {
    ch = szText.GetAt(i);
		if(!(
				((ch>47)&&(ch<58))
			||((ch>64)&&(ch<71))
			||((ch>96)&&(ch<103))
			))
			bHex=FALSE;
		i++;
  }
  return bHex;
}

// checks to see that all chars are in the range 0-9, '-' and '.' for negative and decimal #s.
// a blank string (zero length) returns false
UINT CTextUtils::IsDecimalString(CString szText) 
{
  int i=0, len = szText.GetLength() ;
	if (len<=0) return 0;
  char ch;
	BOOL bDecimal=TRUE;
	int nNumNeg=0;
	int nNumPoint=0;
  while ((i<len)&&(bDecimal))
  {
    ch = szText.GetAt(i);
		if(!(
				((ch>47)&&(ch<58))
			||(ch=='-')
			||(ch=='.')
			))
		{
			bDecimal=FALSE;
		}
		else
		{
			if(ch=='-') 
			{
				if(i!=0) return 0; // if it's not in the first position, it;s not a valid minus sign
				else
				{
					if(len==1) return 0; // if it is only a minus sign with no numbers after it, it's not a number.
				}
				nNumNeg++; if (nNumNeg>1) return 0; // multiple negative signs
 			}
			if(ch=='.') 
			{
				nNumPoint++;if (nNumPoint>1) return 0; // multiple decimal points
			}
		}
		i++;
  }

	//return values:
  if (bDecimal) // if decimal number, returns flags for negative and non-integer, like "98.73")
	{
		UINT nReturn = 0x01;// decimal
		if (nNumNeg) nReturn|=0x02; // negative
		if (nNumPoint) nReturn|=0x04; // non-integer
		return nReturn;
		// so a negative integer would return 3, a positive integer returns 1, negative non-integer returns 7, etc.
		// return value may be BOOL cast
	}
  return 0;
}

//returns a numerical byte from an ascii string starting from an index within that string (def=0)
// ex: ReturnHexByte("0e2", 1) returns 0xe2 = 226.
// returns zero if failure...
BYTE CTextUtils::ReturnHexByte(CString szString, int nIndex)
{
	int nReturn=0, nNibble=0; 
	if ((szString.GetLength()-nIndex)<2) return 0;
	if (!IsHexString(szString.Mid(nIndex,2))) return 0;
	char ch;
	int i=0;
	while(i<2)
	{
		ch = szString.GetAt(nIndex+i);
		if ((ch>47)&&(ch<58))
			nNibble=(int) ch - 48;
		else
		if((ch>64)&&(ch<71))
			nNibble=(int) ch - 55;
		else
		if((ch>96)&&(ch<103))
			nNibble=(int) ch - 87;
		else return -1;
		
		nReturn|=nNibble;
		if(i==0) nReturn<<=4;
		i++;
	}
	return nReturn;
}


CString CTextUtils::MakeHexString(BYTE b)
{
  CString szTemp;
  szTemp.Format("%02x", (BYTE) b);
  return szTemp;
}


BOOL CTextUtils::ConvertDataBufferToHex(BYTE *src, CString *szDest, int nBufferLength)
{
  CString szBuffer, szTemp;
  int i; 

  LPTSTR p = szDest->GetBuffer(nBufferLength*2);
  for (i=0; i<nBufferLength; i++)
  {
    szTemp = MakeHexString((BYTE) *(src + i));
    *(p + i*2) = (BYTE) szTemp[0];
    *(p + (i*2) + 1) = (BYTE) szTemp[1];
  }
  szDest->ReleaseBuffer(nBufferLength*2);  // Surplus memory released, p is now invalid.
  return TRUE;
}


// dest must be pre-allocated to size of szSrc.GetLength()
BOOL CTextUtils::ConvertHexBufferToData(CString szSrc, BYTE *dest, int nBufferLength)
{ 
  int i; 
  if (nBufferLength !=  (szSrc.GetLength() / 2))
  {
    CString szTemp;
    szTemp.Format("ConvertHexBufferToData: output buffer size was %d, but inbound datalength was %d", nBufferLength, (szSrc.GetLength() / 2));
    //AfxMessageBox(szTemp);
    return FALSE;
  }

  for (i=0; i<szSrc.GetLength(); i+=2)
  {
    *(dest+(i/2)) = ReturnHexByte(szSrc.Mid(i,2));
  }
  return TRUE;
}


// checks to see that all chars are not any of /\:*?<">| (MS requirement)
// a blank string (zero length) returns false
BOOL CTextUtils::IsFileString(CString szText, BOOL bAllowDirPaths, BOOL bAllowDrive) 
{
  int i=0, len = szText.GetLength();
	if (len<=0) return FALSE;
  char ch;
	BOOL bFile=TRUE;
  while ((i<len)&&(bFile))
  {
    ch = szText.GetAt(i);
		if(
				 ((ch=='/')&&(!bAllowDirPaths))
			 ||((ch=='\\')&&(!bAllowDirPaths))
			 ||((ch==':')&&(!bAllowDrive))
			 ||(ch=='*')
			 ||(ch=='?')
			 ||(ch=='"')
			 ||(ch=='<')
			 ||(ch=='>')
			 ||(ch=='|')
			)
		{
			bFile=FALSE;
			break;
		}
		i++;
  }
  return bFile;
}

BOOL CTextUtils::BreakFileString(CString szText, CString* pszPath, CString* pszFile, CString* pszExt)
{
	if(!IsFileString(szText, TRUE)) return FALSE;
	int nExtSep=-1, nFileSep=-1, nFileSep2=-1;
	nExtSep = szText.ReverseFind('.');
	nFileSep = szText.ReverseFind('\\');
	nFileSep2 = szText.ReverseFind('/');

	if((nExtSep>0)&&(max(nFileSep,nFileSep2)<nExtSep)) // have to avoid c:\dir.ext\FilenameNoExt
	{ // there is an extention on the file
		if(pszExt) *pszExt = szText.Mid(nExtSep+1);
		szText= szText.Left(nExtSep);
	}

	if(max(nFileSep,nFileSep2)>0) 
	{ // there is a path
		if(pszFile) *pszFile = szText.Mid(max(nFileSep,nFileSep2)+1);
		if(pszPath) *pszPath = szText.Left(max(nFileSep,nFileSep2));
	}
	else
	{ // there is no path
		if(pszFile) *pszFile = szText;
		if(pszPath) *pszPath = "";
	}
	return TRUE;
}


// checks to see that all chars are not any of /\:*?<">| (MS requirement)
// a blank string (zero length) a single char of nReplaceChar, if also blank, returns "X"
CString CTextUtils::MakeFileString(CString szText, char chReplaceChar, BOOL bAllowDirPaths)
{
  char ch = chReplaceChar; // can;t use illegal chars!
	char prch;
	if(
			 ((ch=='/')&&(!bAllowDirPaths))
		 ||((ch=='\\')&&(!bAllowDirPaths))
		 ||(ch==':')
		 ||(ch=='*')
		 ||(ch=='?')
		 ||(ch=='"')
		 ||(ch=='<')
		 ||(ch=='>')
		 ||(ch=='|')
		)
		chReplaceChar=0; // strip offending chars.

	CString szOut = _T("");
  int i=0, len = szText.GetLength();
	if (len<=0)
	{
		if (chReplaceChar==0)
			szOut=_T("X");
		else
			szOut.Format("%c",chReplaceChar);
		return szOut;
	}

  while (i<len)
  {
    ch = szText.GetAt(i);
		if(
				 ((ch=='/')&&(!bAllowDirPaths))
			 ||((ch=='\\')&&(!bAllowDirPaths))
			 ||(ch==':')
			 ||(ch=='*')
			 ||(ch=='?')
			 ||(ch=='"')
			 ||(ch=='<')
			 ||(ch=='>')
			 ||(ch=='|')
			)
		{
			if((!bAllowDirPaths)||((bAllowDirPaths)&&(i==0)))
			{
				if(chReplaceChar!=0) szOut+=chReplaceChar;
			}
			else
			if((bAllowDirPaths)&&(i>0))
			{
				// prevents repeat of path delimiter
			  prch = szText.GetAt(i-1);
				if(!(
					  ((prch=='/')||(prch =='\\'))
					&&((chReplaceChar=='/')||(chReplaceChar =='\\'))
					))
					if(chReplaceChar!=0) szOut+=chReplaceChar;
			}
		}
		else
		{
			if((!bAllowDirPaths)||((bAllowDirPaths)&&(i==0)))
			{
				szOut+=ch;
			}
			else
			if((bAllowDirPaths)&&(i>0))
			{
				// prevents repeat of path delimiter
			  prch = szText.GetAt(i-1);
				if(!(
					  ((prch=='/')||(prch =='\\'))
					&&((ch=='/')||(ch =='\\'))
					))
					szOut+=ch;
			}
		}

		i++;
  }

	if (szOut.GetLength()<=0) szOut=_T("X");
	return szOut;
}


// returns a CString that is the original text with the desired char removed
CString CTextUtils::StripChar(CString szText, char chStripChar)
{
	CString szReturn=""; char ch;
	for (int i=0; i<szText.GetLength(); i++)
	{
		ch=szText.GetAt(i);
    if (ch != chStripChar)
    {
      szReturn += ch;
    }
	}
	return szReturn;
}

CString CTextUtils::ReplaceChar(CString szText, char chRemoveChar, char chReplaceChar)
{
	CString szReturn=""; char ch;
	for (int i=0; i<szText.GetLength(); i++)
	{
		ch=szText.GetAt(i);
    if (ch != chRemoveChar)
    {
      szReturn += ch;
    }
		else
		{
		  szReturn += chReplaceChar;
		}
	}
	return szReturn;
}

CString CTextUtils::XMLEncode(CString szText)
{
	CString szReturn=""; char ch;
	for (int i=0; i<szText.GetLength(); i++)
	{
		ch=szText.GetAt(i);
    if (ch == '<') { szReturn += ch; }
		else if (ch == '>') { szReturn += "&gt;"; }
		else if (ch == '<') { szReturn += "&lt;"; }
		else if (ch == '"') { szReturn += "&quot;"; }
		else if (ch == '&') { szReturn += "&amp;"; }
		else { szReturn += ch; }
	}
	return szReturn;
}


int CTextUtils::CountChar(CString szString, char chFind)
{
	int nReturn=0; 
	char ch;
	for (int i=0; i<szString.GetLength(); i++)
	{
		ch=szString.GetAt(i);
    if (ch == chFind)
    {
      nReturn++;
    }
	}
	return nReturn;
}




/*
 * The following function is used to wrap text at a specified boundary.
 * It returns TRUE until there is no more text to wrap.
 *  
 * Usage:
 *    Given some long string, szIn, to be wrapped at 13 chars per line:
 *    while (TextWrapper(13, szIn, &szDisplayMe, &szOut))
 *    {
 *      display szDisplayMe;
 *      szIn = szOut;
 *    }
 * Note that szIn gets destroyed!
 */

BOOL CTextUtils::TextWrapper(int nMaxChars, CString szTextIn, 
                             CString *szLineToDisplay, CString *szTextOut)
{
  BOOL bInsideBrackets = FALSE;
  if (szTextIn.GetLength() <= 0)
  {
    return FALSE;
  }
  else if (szTextIn.GetLength() < nMaxChars)
  {
    szLineToDisplay->Format("%s", szTextIn);
    szTextOut->Format("");
    return TRUE;
  }
  else
  {
    int i, n=-1;
    CString szTemp = szTextIn;
    for (i=nMaxChars-1; i>=0; i--)
    {
      if ((i>0) && (szTextIn[i] == ']') && (szTextIn[i-1] != '\\'))
        bInsideBrackets = TRUE;
      else if (szTextIn[i] == '[')
        bInsideBrackets = FALSE;
      else if ((isspace(szTextIn[i])) && (!bInsideBrackets))
      {
        n=i;
        break;
      }
    }
    if (n<0) n = nMaxChars-1;
    szLineToDisplay->Format("%s", szTextIn.Left(n));
    szTextOut->Format("%s", szTextIn.Right(szTextIn.GetLength() - (n+1)));
    return TRUE;
  }
}




////////////////////////////////////////////////////////////
//  Time conversion utils

// converts a string time HH:MM:SS to DWORD seconds
DWORD CTextUtils::ConvertTimeStringToSeconds(CString szTime)
{
  // convert 00:00:00 to DWORD
  DWORD dwTemp=0;
  int c1=-1, c2=-1, i, l;
  CString szHr,szMin,szSec;
  int h=0,m=0,s=0;

  l = szTime.GetLength();

  for (i=0; i<l; i++)
  {
    if (szTime[i] == ':')
    {
      if (c1 < 0)
        c1 = i;
      else c2 = i;
    }
  }

  if ((c1<0) && (c2<0))
  { // seconds only
    dwTemp = s = atoi((LPCTSTR)szTime);
  }
  else if (c2 < 0)
  { // min:sec
    szMin = szTime.Left(c1);
    szSec = szTime.Right((l-c1)-1);
    m=atoi((LPCTSTR)szMin);
    s=atoi((LPCTSTR)szSec);
    dwTemp = m*60 + s;
  }
  else
  {
    szHr = szTime.Left(c1);
    szMin = szTime.Mid(c1+1, (c2-c1)-1);
    szSec = szTime.Right((l-c2)-1);

    h=atoi((LPCTSTR)szHr);
    m=atoi((LPCTSTR)szMin);
    s=atoi((LPCTSTR)szSec);

    dwTemp = h*3600 + m*60 + s;
  }

  //foo.Format(" %d : %d : %d = %ld", h,m,s, temp);  AfxMessageBox(foo);
  return dwTemp;
}



// the second param can be "dmy" or "ymd" or whatever, or can
// be left blank.  I deal with '02 / 2002 conversion internally.
// returns 00:00:00 on the given date as a unix time number.
// returns 0 on error.
DWORD CTextUtils::ConvertDateStringToUnixtime(CString szDate, CString szFormat)
{
  CString szOut[10];
  char cSepChar='/';
  int m, d, y;

  if (szDate.Find(".") >= 0) cSepChar='.';
  else if (szDate.Find("-") >= 0) cSepChar='-';
  else if (szDate.Find("/") >= 0) cSepChar='/';
  else
  {
    // could not find reasonable separator character
    return 0;
  }
  int nItems = breakStringOnSepChar(cSepChar, szDate, szOut, 10);
  if (nItems != 3)
  {
    // could not find three items
    return 0;
  }

  if (szFormat.CompareNoCase("mdy") == 0)
  { m = atol(szOut[0]); d = atol(szOut[1]); y = atol(szOut[2]); }
  else if (szFormat.CompareNoCase("dmy") == 0)
  { d = atol(szOut[0]); m = atol(szOut[1]); y = atol(szOut[2]); }
  else if (szFormat.CompareNoCase("ymd") == 0)
  { y = atol(szOut[0]); m = atol(szOut[1]); d = atol(szOut[2]); }
  else if (szFormat.CompareNoCase("ydm") == 0)
  { y = atol(szOut[0]); d = atol(szOut[1]); m = atol(szOut[2]); }

  if (y<38) y+=2000;
  else if ((y>=38) && (y<100)) y+=1900;

  CTime t(y, m, d, 0, 0, 0);

  return t.GetTime();
}





/*
 *  Create a CString of the format hh:mm:ss.  If approx is true,
 *  prepend "~", else prepend " "
 */
CString CTextUtils::ConvertSecondsToString(DWORD dwTotalSeconds, BOOL bApproxTime)
{
  DWORD dwHours, dwMins, dwSecs;
  CString szTime;
  
  dwHours = dwTotalSeconds / 3600;
  dwMins =  (dwTotalSeconds % 3600) / 60;
  dwSecs = dwTotalSeconds - (dwHours*3600) - (dwMins*60);
  if (bApproxTime)
    szTime.Format("~%02d:%02d:%02d", dwHours, dwMins, dwSecs);
  else
    szTime.Format("%02d:%02d:%02d", dwHours, dwMins, dwSecs);
  return szTime;
}   

BOOL CTextUtils::IsValidDottedQuad(CString szText, BOOL bHasPort)
{
	szText.TrimLeft(); szText.TrimRight();
	int nLen = szText.GetLength();
	if(!bHasPort)
	{
		if((nLen<7)||(nLen>15)) // min case 0.0.0.0, max 255.255.255.255
			return FALSE;
	}
	else
	{
		if((nLen<9)||(nLen>21)) // min case 0.0.0.0:0, max 255.255.255.255:65535
			return FALSE;
	}
	
	nLen = CountChar(szText, '.');
	if (nLen!=3) return FALSE; //must have 3 dots

	char *quad;  	char ip[32];
	sprintf(ip,"%s",(LPCTSTR)szText);
	// first quad
	quad = strtok(ip,"." );  szText = _T(quad); szText.TrimLeft(); szText.TrimRight(); 
	if(!IsDecimalString(szText)) return FALSE; // must be a number!
	nLen = szText.GetLength();
	if((nLen<1)||(nLen>3)) // min case 0, max 255
		return FALSE;
	nLen = atoi(szText); if((nLen<0)||(nLen>255)) return FALSE;
	// second quad
	quad = strtok(NULL, "." );  szText = _T(quad); szText.TrimLeft(); szText.TrimRight(); 
	if(!IsDecimalString(szText)) return FALSE; // must be a number!
	nLen = szText.GetLength();
	if((nLen<1)||(nLen>3)) // min case 0, max 255
		return FALSE;
	nLen = atoi(szText); if((nLen<0)||(nLen>255)) return FALSE;
	// third quad
	quad = strtok(NULL, "." );  szText = _T(quad); szText.TrimLeft(); szText.TrimRight(); 
	if(!IsDecimalString(szText)) return FALSE; // must be a number!
	nLen = szText.GetLength();
	if((nLen<1)||(nLen>3)) // min case 0, max 255
		return FALSE;
	nLen = atoi(szText); if((nLen<0)||(nLen>255)) return FALSE;
	// fourth quad
	if(bHasPort)
		quad = strtok(NULL, ":" );  
	else
		quad = strtok(NULL, "." );  

	szText = _T(quad); szText.TrimLeft(); szText.TrimRight(); 

	if(!IsDecimalString(szText)) return FALSE; // must be a number!
	nLen = szText.GetLength();
	if((nLen<1)||(nLen>3)) // min case 0, max 255
		return FALSE;
	nLen = atoi(szText); if((nLen<0)||(nLen>255)) return FALSE;
	//port
	quad = strtok(NULL, ":" );  szText = _T(quad); szText.TrimLeft(); szText.TrimRight(); 

	if((quad!=NULL)&&(!bHasPort)) return FALSE;

	if(bHasPort)
	{
		if(!IsDecimalString(szText)) return FALSE; // must be a number!
		nLen = szText.GetLength();
		if((nLen<1)||(nLen>5)) // min case 0, max 65535
			return FALSE;
		nLen = atoi(szText); if((nLen<0)||(nLen>65535)) return FALSE;
	}
	return TRUE;
}

int CTextUtils::Base64Encode(CString szIn, CString* pszOut, CString szAlphabet, char chPad, CString* pszInfo)
{
	// takes in and returns a CString that may contain chars of value 0 that do not terminate the string.
	// CString::GetLength is valid, even if bytes of value 0 are inside the string.

	if(pszOut==NULL) return ERROR_B64_NULLPTR;

	int nReturnCode = RETURN_SUCCESS;

	if(pszInfo) *pszInfo="";
	szAlphabet = szAlphabet.Left(64); // turncates in case longer
	if(szAlphabet.GetLength()<64)
	{
		szAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		nReturnCode |= SOFTERROR_B64_ALPHA;
		if(pszInfo) pszInfo->Format("Using default base64 alphabet.\n");
	}
	if((chPad<=0)||(szAlphabet.Find(chPad)>=0))
	{
		if(szAlphabet.Find('=')>=0)
		{
			szAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
			nReturnCode |= SOFTERROR_B64_PAD_IN_ALPHA;
			if(pszInfo) pszInfo->Format("%sUsing default base64 alphabet.\n",*pszInfo);
		}
		chPad = '=';
		nReturnCode |= SOFTERROR_B64_PADCH;
		if(pszInfo) pszInfo->Format("%sUsing default base64 pad character.",*pszInfo);
	}

  // pad is = char
  BYTE igroup[3], ogroup[4];
  int i, nLen, n=0;

  nLen = szIn.GetLength();
  *pszOut = "";

  DWORD dwDataLength = (nLen/3+((nLen%3)?1:0))*4;
  LPTSTR p = pszOut->GetBuffer(dwDataLength);

  for (i=0; i<nLen; i+=3)
  {
    // take three bytes in
    if (i<nLen)     { igroup[0] = (int)szIn[i];   n=1; } else igroup[0]=0;
    if ((i+1)<nLen) { igroup[1] = (int)szIn[i+1]; n=2; } else igroup[1]=0;
    if ((i+2)<nLen) { igroup[2] = (int)szIn[i+2]; n=3; } else igroup[2]=0;

    ogroup[0] = szAlphabet[igroup[0] >> 2];
    ogroup[1] = szAlphabet[((igroup[0] & 0x03) << 4) | (igroup[1] >> 4)];
    ogroup[2] = szAlphabet[((igroup[1] & 0x0f) << 2) | (igroup[2] >> 6)];
    ogroup[3] = szAlphabet[igroup[2] & 0x3f];
    
    if (n < 3) 
    {
      ogroup[3] = chPad;
      if (n < 2) 
      {
        ogroup[2] = chPad;
      }
    }

	  *(p++)  = (BYTE) ogroup[0];
	  *(p++)  = (BYTE) ogroup[1];
	  *(p++)  = (BYTE) ogroup[2];
	  *(p++)  = (BYTE) ogroup[3];
/*
			// pointer to CString very slow for large files
    *pszOut += (char) ogroup[0];
    *pszOut += (char) ogroup[1];
    *pszOut += (char) ogroup[2];
    *pszOut += (char) ogroup[3];
*/
  }
  pszOut->ReleaseBuffer(dwDataLength);  // Surplus memory released, p is now invalid.

  return nReturnCode;
}

int CTextUtils::Base64Decode(CString szIn, CString* pszOut, CString szAlphabet, char chPad, CString* pszInfo)
{
	// takes in and returns a CString that may contain chars of value 0 that do not terminate the string.
	// CString::GetLength is valid, even if bytes of value 0 are inside the string.
	if(pszOut==NULL) return ERROR_B64_NULLPTR;

	int nReturnCode = RETURN_SUCCESS;

	if(pszInfo) *pszInfo="";
	szAlphabet = szAlphabet.Left(64); // turncates in case longer
	if(szAlphabet.GetLength()<64)
	{
		szAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		nReturnCode |= SOFTERROR_B64_ALPHA;
		if(pszInfo) pszInfo->Format("Using default base64 alphabet.\n");
	}
	if((chPad<=0)||(szAlphabet.Find(chPad)>=0))
	{
		if(szAlphabet.Find('=')>=0)
		{
			szAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
			nReturnCode |= SOFTERROR_B64_PAD_IN_ALPHA;
			if(pszInfo) pszInfo->Format("%sUsing default base64 alphabet.\n",*pszInfo);
		}
		chPad = '=';
		nReturnCode |= SOFTERROR_B64_PADCH;
		if(pszInfo) pszInfo->Format("%sUsing default base64 pad character.",*pszInfo);
	}

  // pad is = char
  BYTE igroup[4], ogroup[3];
  int i, nLen, n=0, nOut=0, index[2] = {-1,-1};

  nLen = szIn.GetLength();
  *pszOut = "";

	DWORD dwDataLength;
	if(szIn[nLen-2]==chPad) // two pad char
		dwDataLength = (nLen*3/4)-2;
	else
	if(szIn[nLen-1]==chPad) // one pad char
		dwDataLength = (nLen*3/4)-1;
	else		// no pad char
		dwDataLength = nLen*3/4;

  LPTSTR p = pszOut->GetBuffer(dwDataLength);

  for (i=0; i<nLen; i+=4)
  {
    // take 4 bytes in
    if (i<nLen)     { igroup[0] = szIn[i];   n=1; } else igroup[0]=0;
    if ((i+1)<nLen) { igroup[1] = szIn[i+1]; n=2; } else igroup[1]=0;
    if ((i+2)<nLen) { igroup[2] = szIn[i+2]; n=3; } else igroup[2]=0;
    if ((i+3)<nLen) { igroup[3] = szIn[i+3]; n=4; } else igroup[3]=0;

		nOut=0;
		if(n>1)
		{
			index[0] = szAlphabet.Find(igroup[0]);
			if(index[0]>=0)
			{
				index[1] = szAlphabet.Find(igroup[1]);
				if(index[1]>=0)
				{
					ogroup[0] = (((index[0]<<2) & 0xfc)|((index[1]>>4) & 0x03));
					nOut++;
				}
			}
		}
		if(n>2)
		{
			index[0] = szAlphabet.Find(igroup[1]);
			if(index[0]>=0)
			{
				index[1] = szAlphabet.Find(igroup[2]); // if this is a pad char, it won't be found
				if(index[1]>=0)
				{
					ogroup[1] = (((index[0]<<4) & 0xf0)|((index[1]>>2) & 0x0f));
					nOut++;
				}
			}
		}
		if(n>3)
		{
			index[0] = szAlphabet.Find(igroup[2]);// if this is a pad char, it won't be found
			if(index[0]>=0)
			{
				index[1] = szAlphabet.Find(igroup[3]);// if this is a pad char, it won't be found
				if(index[1]>=0)
				{
					ogroup[2] = (((index[0]<<6) & 0xc0)|((index[1]) & 0x3f));
					nOut++;
				}
			}
		}
    // even though we don't use the pad char, we need to take it in to make sure it
		// doesn't exist in the alphabet.

	  if(nOut>0) *(p++)  = (BYTE) ogroup[0];
	  if(nOut>1) *(p++)  = (BYTE) ogroup[1];
	  if(nOut>2) *(p++)  = (BYTE) ogroup[2];
		
/*
		// pointer to CString very slow for large files
    if(nOut>0) *pszOut += (char) ogroup[0];
    if(nOut>1) *pszOut += (char) ogroup[1];
    if(nOut>2) *pszOut += (char) ogroup[2];
*/
  }
  pszOut->ReleaseBuffer(dwDataLength);  // Surplus memory released, p is now invalid.
  return nReturnCode;
}

int CTextUtils::Base64EncodeFromFile(CString szFilename, CString* pszOut, CString szAlphabet, char chPad, CString* pszInfo)
{
	// takes in and returns a CString that may contain chars of value 0 that do not terminate the string.
	// CString::GetLength is valid, even if bytes of value 0 are inside the string.

	if(pszOut==NULL) return ERROR_B64_NULLPTR;
	if(!IsFileString(szFilename, TRUE, TRUE)) return ERROR_B64_INVALID_FILENAME;

	int nReturnCode = RETURN_SUCCESS;

	if(pszInfo) *pszInfo="";
	szAlphabet = szAlphabet.Left(64); // turncates in case longer
	if(szAlphabet.GetLength()<64)
	{
		szAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		nReturnCode |= SOFTERROR_B64_ALPHA;
		if(pszInfo) pszInfo->Format("Using default base64 alphabet.\n");
	}
	if((chPad<=0)||(szAlphabet.Find(chPad)>=0))
	{
		if(szAlphabet.Find('=')>=0)
		{
			szAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
			nReturnCode |= SOFTERROR_B64_PAD_IN_ALPHA;
			if(pszInfo) pszInfo->Format("%sUsing default base64 alphabet.\n",*pszInfo);
		}
		chPad = '=';
		nReturnCode |= SOFTERROR_B64_PADCH;
		if(pszInfo) pszInfo->Format("%sUsing default base64 pad character.",*pszInfo);
	}

	FILE* fp;
  *pszOut = "";

	CString szData="";
	char buffer[1025];

	fp = fopen(szFilename,"rb");
  if (fp != NULL)
  {
		
		DWORD dwTotalDataLength = _filelength( _fileno( fp));
		DWORD dwDataLength=0; // so far
		DWORD dwOffset = 1024;
		LPTSTR p = szData.GetBuffer(dwTotalDataLength);
    while ((!feof(fp))&&(dwOffset>0))
    {
			fread( buffer, sizeof(BYTE), dwOffset, fp );

			if ((dwTotalDataLength-dwDataLength)<dwOffset)
				dwOffset = dwTotalDataLength-dwDataLength;
			memcpy((void *) (p+dwDataLength), buffer, dwOffset);
			dwDataLength+=dwOffset;
    }
		szData.ReleaseBuffer(dwTotalDataLength);  // Surplus memory released, p is now invalid.
		fclose(fp);
	  nReturnCode = Base64Encode(szData, pszOut, szAlphabet, chPad, pszInfo);
  }
	else nReturnCode = ERROR_B64_FILEERROR;

  return nReturnCode;
}

int CTextUtils::Base64DecodeToFile(CString szFilename, CString szIn, CString szAlphabet, char chPad, CString* pszInfo)
{
	// takes in and returns a CString that may contain chars of value 0 that do not terminate the string.
	// CString::GetLength is valid, even if bytes of value 0 are inside the string.
	if(!IsFileString(szFilename, TRUE, TRUE)) return ERROR_B64_INVALID_FILENAME;

	int nReturnCode = RETURN_SUCCESS;

	if(pszInfo) *pszInfo="";
	szAlphabet = szAlphabet.Left(64); // turncates in case longer
	if(szAlphabet.GetLength()<64)
	{
		szAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		nReturnCode |= SOFTERROR_B64_ALPHA;
		if(pszInfo) pszInfo->Format("Using default base64 alphabet.\n");
	}
	if((chPad<=0)||(szAlphabet.Find(chPad)>=0))
	{
		if(szAlphabet.Find('=')>=0)
		{
			szAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
			nReturnCode |= SOFTERROR_B64_PAD_IN_ALPHA;
			if(pszInfo) pszInfo->Format("%sUsing default base64 alphabet.\n",*pszInfo);
		}
		chPad = '=';
		nReturnCode |= SOFTERROR_B64_PADCH;
		if(pszInfo) pszInfo->Format("%sUsing default base64 pad character.",*pszInfo);
	}


	CString szOut;
	nReturnCode = Base64Decode(szIn, &szOut, szAlphabet, chPad, pszInfo);
	if(nReturnCode>=RETURN_SUCCESS)
	{
		FILE* fp;
		fp = fopen(szFilename,"wb");
		if (fp != NULL)
		{

			DWORD dwTotalDataLength = szOut.GetLength();
			DWORD dwDataLength=0; // so far
			LPTSTR p = szOut.GetBuffer(dwTotalDataLength);
			BOOL bDone=FALSE;
			int i = 0;
			DWORD nLen=0;
			while (!bDone)
			{
				nLen = fwrite(p+dwDataLength, sizeof(BYTE), dwTotalDataLength-dwDataLength, fp);
				dwDataLength+=nLen;
				if(dwTotalDataLength==dwDataLength) bDone=TRUE;
				if(nLen=0)
				{
					i++; // errors
				}
				if(i>5) { bDone=TRUE; nReturnCode = ERROR_B64_FILEERROR; } // retry a few times.
			}
			szOut.ReleaseBuffer(dwTotalDataLength);  // Surplus memory released, p is now invalid.
			fclose(fp);

/*
		// pad is = char
		BYTE igroup[4], ogroup[3];
		int i, nLen, n=0, nOut=0, index[2] = {-1,-1};
		FILE* fp;

		fp = fopen(szFilename,"wb");
		if (fp != NULL)
		{
			nLen = szIn.GetLength();
			for (i=0; i<nLen; i+=4)
			{
				// take 4 bytes in
				if (i<nLen)     { igroup[0] = szIn[i];   n=1; } else igroup[0]=0;
				if ((i+1)<nLen) { igroup[1] = szIn[i+1]; n=2; } else igroup[1]=0;
				if ((i+2)<nLen) { igroup[2] = szIn[i+2]; n=3; } else igroup[2]=0;
				if ((i+3)<nLen) { igroup[3] = szIn[i+3]; n=4; } else igroup[3]=0;

				nOut=0;
				if(n>1)
				{
					index[0] = szAlphabet.Find(igroup[0]);
					if(index[0]>=0)
					{
						index[1] = szAlphabet.Find(igroup[1]);
						if(index[1]>=0)
						{
							ogroup[0] = (((index[0]<<2) & 0xfc)|((index[1]>>4) & 0x03));
							nOut++;
						}
					}
				}
				if(n>2)
				{
					index[0] = szAlphabet.Find(igroup[1]);
					if(index[0]>=0)
					{
						index[1] = szAlphabet.Find(igroup[2]); // if this is a pad char, it won't be found
						if(index[1]>=0)
						{
							ogroup[1] = (((index[0]<<4) & 0xf0)|((index[1]>>2) & 0x0f));
							nOut++;
						}
					}
				}
				if(n>3)
				{
					index[0] = szAlphabet.Find(igroup[2]);// if this is a pad char, it won't be found
					if(index[0]>=0)
					{
						index[1] = szAlphabet.Find(igroup[3]);// if this is a pad char, it won't be found
						if(index[1]>=0)
						{
							ogroup[2] = (((index[0]<<6) & 0xc0)|((index[1]) & 0x3f));
							nOut++;
						}
					}
				}
				// even though we don't use the pad char, we need to take it in to make sure it
				// doesn't exist in the alphabet.
				if(nOut>0) fprintf(fp, "%c", (char) ogroup[0]);
				if(nOut>1) fprintf(fp, "%c", (char) ogroup[1]);
				if(nOut>2) fprintf(fp, "%c", (char) ogroup[2]);
			}
			fclose(fp);
*/
		}
		else nReturnCode = ERROR_B64_FILEERROR;
	}
  return nReturnCode;
}

CString CTextUtils::Base64Boundary(CString szAlphabet, char chPad)
{
	DWORD dwTime = time(NULL);
	CString szTime, szReturn; 
	szTime.Format("%08x", time);

	dwTime = Base64Encode(szTime, &szReturn, szAlphabet, chPad);
	if (dwTime >= RETURN_SUCCESS)
    szTime = szReturn;

// RFC 2045
//		(A good strategy is to choose a boundary that includes a character sequence such as "=_" 
//		which can never appear in a quoted-printable body.  See the definition of multipart messages in RFC 2046.)
	szReturn.Format("-----part%c_%s_-----", chPad, szTime); // include the pad char not in the last position
	return szReturn;
}