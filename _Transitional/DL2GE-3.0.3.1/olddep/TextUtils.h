// (c) 2000-Present Video Design Interactive.  All Rights Reserved.
// This file constitutes the proprietary, confidential intellectual 
// property of Video Design Interactive and may not be distributed.

#ifndef CTEXTUTILS_INCLUDED
#define CTEXTUTILS_INCLUDED

// return codes
#define RETURN_SUCCESS 0
#define SOFTERROR_B64_ALPHA 1 // soft because uses default alphabet
#define SOFTERROR_B64_PADCH 2 // soft because uses default pad char
#define SOFTERROR_B64_PAD_IN_ALPHA   4 // pad character exists in alpha string, soft because uses default pad char and alpha
#define ERROR_B64_NULLPTR   -1 // null pointer to output string 
#define ERROR_B64_INVALID_FILENAME -2
#define ERROR_B64_FILEERROR -3

class CTextUtils // : public CObject
{
public:
	CTextUtils();   // standard constructor
	~CTextUtils();   // standard destructor

  int breakStringOnSepChar(unsigned char cSepChar, CString szIn, 
                           CString szOut[], int nArraySize=-1, 
                           int *nTotalItemsFound=NULL);
  CString formatTicks(unsigned long tickCount);
  void AMB(int x);
  void HAMB(int x);  // hex amb
  void AMB(double x);
  void AMB(CString x);
  void AMB(char fmt_string[], ... );
  CString formatLastError();
  CString formatLastError(DWORD dwLastError);
  CString MakePrintableString(CString szString);
  CString Uniq(CString szString);
  int CountChar(CString szString, char chFind);
  CString fixSpacing(CString szIn);

  CString Decode(CString szText);
  CString Encode(CString szText);

	BOOL IsHexString(CString szText);
	BOOL IsValidDottedQuad(CString szText, BOOL bHasPort=FALSE);
	UINT IsDecimalString(CString szText);
  BYTE ReturnHexByte(CString szString, int nIndex=0);
  CString MakeHexString(BYTE b);
  BOOL ConvertDataBufferToHex(BYTE *src, CString *szDest, int nBufferLength);
  BOOL ConvertHexBufferToData(CString szSrc, BYTE *dest, int nBufferLength);
  
  BOOL IsFileString(CString szText, BOOL bAllowDirPaths=TRUE, BOOL bAllowDrive=TRUE);
  BOOL BreakFileString(CString szText, CString* pszPath, CString* pszFile, CString* pszExt);
	CString MakeFileString(CString szText, char chReplaceChar=0, BOOL bAllowDirPaths=TRUE);
	CString StripChar(CString szText, char chStripChar=' ');
	CString ReplaceChar(CString szText, char chRemoveChar=' ', char chReplaceChar=' ');
	CString XMLEncode(CString szText);

  BOOL TextWrapper(int nMaxChars, CString szTextIn, CString *szLineToDisplay, CString *szTextOut);

  DWORD ConvertTimeStringToSeconds(CString szTime);
  CString ConvertSecondsToString(DWORD dwTotalSeconds, BOOL bApproxTime);
  DWORD ConvertDateStringToUnixtime(CString szDate, CString szFormat="mdy");

  int Base64Encode(CString szIn, CString* pszOut, CString szAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", char chPad= '=', CString* pszInfo=NULL);
  int Base64Decode(CString szIn, CString* pszOut, CString szAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", char chPad= '=', CString* pszInfo=NULL);
  int Base64EncodeFromFile(CString szFilename, CString* pszOut, CString szAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", char chPad= '=', CString* pszInfo=NULL);
  int Base64DecodeToFile(CString szFilename, CString szIn, CString szAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", char chPad= '=', CString* pszInfo=NULL);
  CString Base64Boundary(CString szAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", char chPad= '=');
};

#endif
