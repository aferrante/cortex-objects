//   Networking.cpp
// (c) 2000-Present Video Design Interactive.  All Rights Reserved.
// This file constitutes the proprietary, confidential intellectual 
// property of Video Design Interactive and may not be distributed.


#include "stdafx.h"
#include "networking.h"
#include <sys/timeb.h>
#include <time.h>
#include <wininet.h>


/*
// totally unnecessary
//#define	NTELOPTS	(1+TELOPT_EOR)

	char*	m_pszTelopts[NTELOPTS] = 
	{
		"BINARY", "ECHO", "RCP", "SUPPRESS GO AHEAD", "NAME",
		"STATUS", "TIMING MARK", "RCTE", "NAOL", "NAOP",
		"NAOCRD", "NAOHTS", "NAOHTD", "NAOFFD", "NAOVTS",
		"NAOVTD", "NAOLFD", "EXTEND ASCII", "LOGOUT", "BYTE MACRO",
		"DATA ENTRY TERMINAL", "SUPDUP", "SUPDUP OUTPUT",
		"SEND LOCATION", "TERMINAL TYPE", "END OF RECORD",
	};
*/


CNetworking::CNetworking()
{
  m_nTelnetCount = 0; // Telnet flags
  m_pszOverflowBuffer = NULL;
  m_bOverflow = FALSE;
  m_bServerListening = FALSE;
  m_nNumServers = 0;
	m_bNetworkingStarted = FALSE;
#ifdef USE_LOGGING
	m_bWriteFileOps = FALSE;
	m_bWriteFileErrors = FALSE;
	m_szFilename = _T("networking.log");
#endif
}

CNetworking::~CNetworking()
{
  if (m_pszOverflowBuffer) { free(m_pszOverflowBuffer); m_pszOverflowBuffer=NULL; }
}

#ifdef USE_LOGGING
CString CNetworking::GetTimestamp()
{
  CString szTimestamp=_T("");
  struct _timeb timebuffer;
  _ftime( &timebuffer );
  CTime thetime = timebuffer.time;
  szTimestamp.Format("%s.%03d", thetime.Format("%m-%d-%Y %H:%M:%S"), timebuffer.millitm);
	return szTimestamp;
}

void CNetworking::LogToFile(CString szText)
{
	FILE* pFile = fopen(m_szFilename, "at");
	if(pFile)
	{
		fprintf(pFile, "%s %s\n", (LPCTSTR) GetTimestamp(), (LPCTSTR) szText);
		fflush(pFile);
		fclose(pFile);
	}
}
#endif

int CNetworking::StartNetworking(CString* pszInfo)
{
	if(m_bNetworkingStarted) return RETURN_SUCCESS;
	WORD wVersionRequested;
	WSADATA wsaData;
	int err;

	m_bNetworkingStarted = FALSE;
		
	wVersionRequested = MAKEWORD( 2, 2 );
	err = WSAStartup( wVersionRequested, &wsaData );
	if ( err != 0 ) 
	{
		// Tell the user that we could not find a usable WinSock DLL.
		if(pszInfo)
		{
			pszInfo->Format("Unable to start networking (no winsock). (%s:%d)", __FILE__,__LINE__);
		}
    return ERROR_NO_WINSOCK;
	}
	
	// Confirm that the WinSock DLL supports 2.2.        /
	// Note that if the DLL supports versions greater    /
	// than 2.2 in addition to 2.2, it will still return /
	// 2.2 in wVersion since that is the version we      /
	// requested.                                        /
	
	if ( LOBYTE( wsaData.wVersion ) != 2 ||	HIBYTE( wsaData.wVersion ) != 2 ) 
	{
    // Tell the user that we could not find a usable WinSock DLL. 
		if(pszInfo)
		{
			pszInfo->Format("Unable to start networking (incompatible winsock version). (%s:%d)", __FILE__,__LINE__);
		}
    return ERROR_WINSOCK_VERSION;
	}
	
	// The WinSock DLL is acceptable. Proceed. /
  m_bNetworkingStarted = TRUE;
	return RETURN_SUCCESS;
}



int CNetworking::ShutdownNetworking(CString* pszInfo)
{
  // shutdown all servers
  for (int i=0; i<m_nNumServers; i++)
  {
    if (m_ServerInfo[i].socket != NULL)
    {
      //CString x; x.Format("Shutting down socket %d...", m_ServerInfo[i].socket); AfxMessageBox(x);
			UnsetTelnet((int) m_ServerInfo[i].socket);  
      shutdown(m_ServerInfo[i].socket, SD_BOTH);
      closesocket(m_ServerInfo[i].socket);
      //AfxMessageBox("done");
    }
  }
	m_bNetworkingStarted = FALSE;

  WSACleanup();
  return RETURN_SUCCESS;
}


// Open connection to server 'host' on port 'port'.  Returns 
// the open socket (*ps).  If telnet is TRUE, telnet protocol
// parsing is enabled for this socket; otherwise the connection
// does no parsing.  Timeout is the timeout of the connect
int CNetworking::OpenConnection(CString szHost, int nPort, SOCKET* ps, int nTelnet, int nTimeoutMS, CString* pszInfo)
{
  int err, len, nReturnCode = RETURN_SUCCESS;
	register struct hostent *hp = NULL;
	char hostnamebuf[80], hostname[100];
  SOCKET socketReturn;
	struct sockaddr_in my_sockaddr;
	struct sockaddr_in myctladdr;

  szHost.TrimLeft(); szHost.TrimRight();

  if ((szHost == "") || (szHost == "0.0.0.0")) 
	{
		if(pszInfo)
		{
			pszInfo->Format("Invalid host %s. (%s:%d)", szHost, __FILE__,__LINE__);
		}
		return ERROR_HOST_INVALID;
	}

	memset(((char *)&my_sockaddr), 0, (sizeof (my_sockaddr)));
	my_sockaddr.sin_addr.s_addr = inet_addr((LPCTSTR)szHost);
	if (my_sockaddr.sin_addr.s_addr != -1) 
	{
		my_sockaddr.sin_family = AF_INET;
		(void) strncpy(hostnamebuf, (LPCTSTR)szHost, sizeof(hostnamebuf));
	} 
	else 
	{
		hp = gethostbyname((LPCTSTR)szHost);

		if (hp == NULL) 
		{
#ifdef USE_LOGGING
			CString szError;
			PrintSocketError(WSAGetLastError(), __LINE__, &szError);
			if(m_bWriteFileErrors)	LogToFile(szError);
			if(pszInfo) pszInfo->Format("Hostname error. (%s:%d)\n%s", __FILE__,__LINE__,szError);
#else
			if(pszInfo)
			{
				CString szError;
				PrintSocketError(WSAGetLastError(), __LINE__, &szError);
				pszInfo->Format("Unknown host. Connection to '%s:%d' failed. (%s:%d)\nHostname error: %s",szHost, nPort, __FILE__,__LINE__,szError);
			}
#endif

/*
			if(pszInfo)
			{
				pszInfo->Format("Unknown host. Connection to '%s:%d' failed. (%s:%d)", szHost, nPort, __FILE__,__LINE__);
			}
*/
				*ps = NULL;
			return ERROR_HOST_INVALID;
		}
		my_sockaddr.sin_family = hp->h_addrtype;
		memcpy((caddr_t) &my_sockaddr.sin_addr, hp->h_addr_list[0], hp->h_length);
//		memcpy( &my_sockaddr.sin_addr, hp->h_addr_list[0], hp->h_length);
		(void) strncpy(hostnamebuf, hp->h_name, sizeof(hostnamebuf));
	}

	strcpy(hostname, hostnamebuf);
	
	socketReturn = (SOCKET) socket(my_sockaddr.sin_family, SOCK_STREAM, 0);
	if (socketReturn == INVALID_SOCKET) 
	{
#ifdef USE_LOGGING
		CString szError;
		PrintSocketError(WSAGetLastError(), __LINE__, &szError);
		if(m_bWriteFileErrors)	LogToFile(szError);
		if(pszInfo) pszInfo->Format("Error creating socket. (%s:%d)\n%s", __FILE__,__LINE__,szError);
#else
		if(pszInfo)
		{
			CString szError;
			PrintSocketError(WSAGetLastError(), __LINE__, &szError);
			pszInfo->Format("Error creating socket. Connection to '%s:%d' failed. (%s:%d)\nError: %s",szHost, nPort, __FILE__,__LINE__,szError);
		}
#endif
/*
		if(pszInfo)
		{
			pszInfo->Format("Error creating socket. Connection to '%s:%d' failed. (%s:%d)", szHost, nPort, __FILE__,__LINE__);
		}
*/
		//UnsetTelnet((int)socketReturn); // we never set invalid socket
		*ps = NULL;
		return ERROR_SOCKET_CREATE;
	}

  // set the telnet option. 
  nReturnCode = SetTelnet((int)socketReturn, nTelnet, pszInfo);
	if (nReturnCode != RETURN_SUCCESS) 
	{
		//UnsetTelnet((int)socketReturn);  
		// was unsuccessful for some reason
		if(nReturnCode==ERROR_SOCKET_MAXNUM_REACHED)
		{
			shutdown (socketReturn, SD_BOTH);
			closesocket(socketReturn);
		}
		*ps = NULL;
		return nReturnCode;
	}

//  Note the use of the htons function, which is responsible for translating the port number into a unique representation. 
//  Different processors store numeric values differently in memory, and htons ensures that the representation of 
//  numbers is the same when arriving on any machine on the network. From:Power Outlets in Action: Windows Sockets
  
	my_sockaddr.sin_port = htons((u_short) nPort);

//	CString f; f.Format("Contacting host %s on port %d (=%x)...socket %d", hostname, nPort, my_sockaddr.sin_port, socketReturn);	AfxMessageBox(f);
	while (connect((SOCKET) socketReturn, (struct sockaddr FAR *) &my_sockaddr, (int) sizeof (my_sockaddr)) != 0)
	{
		// Try alias
		if (hp && hp->h_addr_list[1]) 
		{
			hp->h_addr_list++;
			memcpy((caddr_t)&my_sockaddr.sin_addr, hp->h_addr_list[0], hp->h_length);
//			memcpy(&my_sockaddr.sin_addr, hp->h_addr_list[0], hp->h_length);
			// (void) close(s);
			UnsetTelnet((int)socketReturn);
			shutdown (socketReturn, SD_BOTH);
			closesocket(socketReturn);
			socketReturn = (SOCKET) socket(my_sockaddr.sin_family, SOCK_STREAM, 0);
			if (socketReturn == INVALID_SOCKET)
			{
				//UnsetTelnet((int)socketReturn); // we haven't set the new
				shutdown (socketReturn, SD_BOTH);
				closesocket(socketReturn);
				*ps = NULL;
#ifdef USE_LOGGING
				CString szError;
				PrintSocketError(WSAGetLastError(), __LINE__, &szError);
				if(m_bWriteFileErrors)	LogToFile(szError);
				if(pszInfo) pszInfo->Format("Error creating socket. (%s:%d)\n%s", __FILE__,__LINE__,szError);
#else
				if(pszInfo)
				{
					CString szError;
					PrintSocketError(WSAGetLastError(), __LINE__, &szError);
					pszInfo->Format("Error creating socket. Connection to '%s:%d' failed. (%s:%d)\nError: %s",szHost, nPort, __FILE__,__LINE__,szError);
				}
#endif
				/*
				if(pszInfo)
				{
					pszInfo->Format("Error creating socket. Connection to '%s:%d' failed. (%s:%d)", szHost, nPort, __FILE__,__LINE__);
				}
				*/
				return ERROR_SOCKET_CREATE;
			}
			// set the telnet option. 
			nReturnCode = SetTelnet((int)socketReturn, nTelnet, pszInfo);
			if (nReturnCode != RETURN_SUCCESS) 
			{
				//UnsetTelnet((int)socketReturn);  
				// was unsuccessful for some reason
				if(nReturnCode==ERROR_SOCKET_MAXNUM_REACHED)
				{
					shutdown (socketReturn, SD_BOTH);
					closesocket(socketReturn);
				}
				*ps = NULL;
				return nReturnCode;
			}
			continue;
		}
		
		UnsetTelnet((int)socketReturn); // must remove socket
		shutdown (socketReturn, SD_BOTH);
		closesocket(socketReturn);
		*ps = NULL;
#ifdef USE_LOGGING
		CString szError;
		PrintSocketError(WSAGetLastError(), __LINE__, &szError);
		if(m_bWriteFileErrors)	LogToFile(szError);
		if(pszInfo) pszInfo->Format("Error creating socket. (%s:%d)\n%s", __FILE__,__LINE__,szError);
#else
		if(pszInfo)
		{
			CString szError;
			PrintSocketError(WSAGetLastError(), __LINE__, &szError);
			pszInfo->Format("Error creating socket. Connection to '%s:%d' failed. (%s:%d)\nError: %s",szHost, nPort, __FILE__,__LINE__,szError);
		}
#endif
/*
		if(pszInfo)
		{
			pszInfo->Format("Error creating socket. Connection to '%s:%d' failed. (%s:%d)", szHost, nPort, __FILE__,__LINE__);
		}
*/
		return ERROR_SOCKET_CREATE;
	}

	len = sizeof (myctladdr);
	if (getsockname(socketReturn, (struct sockaddr *)&myctladdr, &len) != 0) 
	{ 
		UnsetTelnet((int)socketReturn);  // remove from list
		shutdown (socketReturn, SD_BOTH);
		closesocket(socketReturn);
		*ps = NULL;
#ifdef USE_LOGGING
		CString szError;
		PrintSocketError(WSAGetLastError(), __LINE__, &szError);
		if(m_bWriteFileErrors)	LogToFile(szError);
		if(pszInfo) pszInfo->Format("Error getting socket name. (%s:%d)\n%s", __FILE__,__LINE__,szError);
#else
		if(pszInfo)
		{
			CString szError;
			PrintSocketError(WSAGetLastError(), __LINE__, &szError);
			pszInfo->Format("Error getting socket name. Connection to '%s:%d' failed. (%s:%d)\nError: %s",szHost, nPort, __FILE__,__LINE__,szError);
		}
#endif
/*
		if(pszInfo)
		{
			pszInfo->Format("Error getting socket name. Connection to '%s:%d' failed. (%s:%d)", szHost, nPort, __FILE__,__LINE__);
		}
*/
		return ERROR_SOCKET_GETSOCKNAME;
	}

#if defined(IP_TOS) && defined(IPTOS_LOWDELAY)
	tos = IPTOS_LOWDELAY;
	setsockopt(socketReturn, IPPROTO_IP, IP_TOS, (char *)&tos, sizeof(int));
#endif

	if(pszInfo) pszInfo->Format("");

#ifdef SO_OOBINLINE
  int on;
  on = 1;
  
  if ((setsockopt(socketReturn, SOL_SOCKET, SO_OOBINLINE, (char *)&on, sizeof(on)) == INVALID_SOCKET )
    )
  {	
		if(pszInfo)
		{
			pszInfo->Format("setsockopt(SO_OOBINLINE) error. (%s:%d)\n",  __FILE__,__LINE__);
		}
		nReturnCode = SOFTERROR_SOCKET_SETSOCKOPTS;
  }
    
#endif // SO_OOBINLINE 
    
	*ps = socketReturn;

  // timeouts
  err = setsockopt(*ps, SOL_SOCKET, SO_SNDTIMEO, (char *)&nTimeoutMS, sizeof(nTimeoutMS));
  if (err != NO_ERROR) 
	{
		if(pszInfo)
		{
			CString szInfo;  szInfo = *pszInfo;
			pszInfo->Format("%ssetsockopt(SO_SNDTIMEO) error. (%s:%d)\n", szInfo, __FILE__,__LINE__);
		}
		nReturnCode = SOFTERROR_SOCKET_SETSOCKOPTS;
	}	
  err = setsockopt(*ps, SOL_SOCKET, SO_RCVTIMEO, (char *)&nTimeoutMS, sizeof(nTimeoutMS));
  if (err != NO_ERROR) 
	{
		if(pszInfo)
		{
			CString szInfo;  szInfo = *pszInfo;
			pszInfo->Format("%ssetsockopt(SO_RCVTIMEO) error. (%s:%d)\n", szInfo, __FILE__,__LINE__);
		}
		nReturnCode = SOFTERROR_SOCKET_SETSOCKOPTS;
	}	

  if (nReturnCode == RETURN_SUCCESS) 
	{
#ifdef USE_LOGGING
		CString szMsg;
		szMsg.Format("Connection to '%s:%d' succeeded. (%s:%d)", szHost, nPort,  __FILE__,__LINE__);
		if(m_bWriteFileOps)	LogToFile(szMsg);
		if(pszInfo) *pszInfo=szMsg;
#else
		if(pszInfo)
		{
			pszInfo->Format("Connection to '%s:%d' succeeded. (%s:%d)", szHost, nPort,  __FILE__,__LINE__);
		}
#endif
	}	

	return nReturnCode;
}



//  Close an open connection 
int CNetworking::CloseConnection(SOCKET s, BOOL bDoUnset, CString* pszInfo)
{
	int nReturnCode = RETURN_SUCCESS;
	if((s != INVALID_SOCKET )&&(s != SOCKET_ERROR))
	{
		if(bDoUnset) UnsetTelnet((int) s, pszInfo); // don't check return value - not set if this is a reply
		shutdown (s, SD_BOTH);
		closesocket(s);
	}
#ifdef USE_LOGGING
	CString szMsg;
	szMsg.Format("Connection on socket %d closed. (%s:%d)", s,  __FILE__,__LINE__);
	if(m_bWriteFileOps)	LogToFile(szMsg);
	if(pszInfo) *pszInfo=szMsg;
#else
	if(pszInfo)
	{
		CString szText; szText.Format("Connection on socket %d closed. (%s:%d)", s,  __FILE__,__LINE__);
		*pszInfo+=szText;
	}
#endif
  return nReturnCode;
}



//  Send a command to the server
int CNetworking::SendCommand(char text[], SOCKET s, BOOL bUnicode, int nEoln, CString* pszInfo)
{
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		LogToFile("SendCommand");
	}
#endif
  int i, l, newbuflen, rc;
  char *newbuffer=NULL, *buffer=NULL;

    
  buffer = (char *) malloc((strlen(text)+5) * 2);
  if (buffer==NULL) 
  { 
#ifdef USE_LOGGING
	if(m_bWriteFileErrors)	
	{
		LogToFile("Error allocating buffer");
	}
#endif
		if(pszInfo)
		{
			pszInfo->Format("Error allocating buffer. (%s:%d)",  __FILE__,__LINE__);
		}
    return ERROR_MALLOC_FAILED;
  }

  newbuffer = (char *) malloc((strlen(text)+5) * 2);
  if (newbuffer==NULL) 
  { 
		if(pszInfo)
		{
#ifdef USE_LOGGING
	if(m_bWriteFileErrors)	
	{
		LogToFile("Error allocating conversion buffer");
	}
#endif
			pszInfo->Format("Error allocating conversion buffer. (%s:%d)",  __FILE__,__LINE__);
		}
    free(buffer); buffer=NULL;
    return ERROR_MALLOC_FAILED;
  }

  switch(nEoln)
  {
    case EOLN1_CR:   sprintf(buffer, "%s%c",  text,13); break;
    case EOLN1_LF:   sprintf(buffer, "%s%c",  text,10); break;
    case EOLN1_CRLF: sprintf(buffer, "%s%c%c",text,13,10); break;
    case EOLN1_NONE: strcpy(buffer, text); break;
    default:        strcpy(buffer, text); break;
  }


	int nMode;
	rc = GetTelnet((int) s, &nMode);
  if (rc >= RETURN_SUCCESS)
	{
		if (nMode>0)  // have to escape IAC, other stuff.
		{
			//Deal with telnet semantics
			for (i=0,l=0; i<(int) strlen(buffer); i++)
			{
				switch (buffer[i])
				{
				case ((char) TELNET_IAC):
					newbuffer[l++] = (char) TELNET_IAC;
					newbuffer[l++] = (char) TELNET_IAC;
					//printf("sending 2 IACs\n");
					break;
      
				// We need to heed RFC 854. LF (\n) is 10, CR (\r) is 13
				// we assume that the Terminal sends \n for lf+cr and \r for just cr
				// linefeed+carriage return is CR LF

				// we must send CRLF explicitly, thus commented out code below

				/*
				case 10:	// \n
					newbuffer[l++]=13; 
      		newbuffer[l++]=10;
					break;
				// carriage return is CR NUL
				case 13:	// \r
      		newbuffer[l++]=13;
      		//newbuffer[l++]=0;
      		break;
				*/
				// all other characters are just copied
				default:
					//printf("text[i]=%d\n", text[i]);
					newbuffer[l++] = buffer[i];
					break;
				}
			}
			newbuffer[l] = 0;
			newbuflen = l;
		}
		else
		{
			strcpy(newbuffer, buffer);
			newbuflen = strlen(newbuffer);
		}
	}
	else // try this
	{
		strcpy(newbuffer, buffer);
		newbuflen = strlen(newbuffer);
	}

  if (buffer) { free(buffer); buffer = NULL; }


  if (bUnicode)  // Convert buffer to unicode.  See http://www.unicode.org/unicode/reports/tr20/ 
  {
    buffer = (char *) malloc((newbuflen + 5)*2);
    if (buffer != NULL)
    {
      ASCIIToUnicode(newbuffer, buffer, &l);

#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		CString szMsg;
		szMsg.Format("Sending unicode buffer (length=%d) on socket %d",l,s);
		LogToFile(szMsg);
	}
#endif
            
      rc = send(s, buffer, l, 0);
      if (rc == SOCKET_ERROR) 
      {

#ifdef USE_LOGGING
				CString szError;
				PrintSocketError(WSAGetLastError(), __LINE__, &szError);
				if(m_bWriteFileErrors)	LogToFile(szError);
				if(pszInfo) pszInfo->Format("Send error (%s:%d)\n%s", __FILE__,__LINE__,szError);
#else
				if(pszInfo)
				{
					CString szError;
					PrintSocketError(WSAGetLastError(), __LINE__, &szError);
					pszInfo->Format("Send error (%s:%d)\n%s", __FILE__,__LINE__,szError);
				}
#endif
				free(buffer); buffer=NULL;
				if (newbuffer) { free(newbuffer); newbuffer = NULL; }
				return ERROR_SOCKET_SEND;
      }
      if (buffer) { free(buffer); buffer = NULL; }
    }
    else 
    {
			if(pszInfo)
			{
				pszInfo->Format("Error allocating buffer. (%s:%d)",  __FILE__,__LINE__);
			}
			return ERROR_MALLOC_FAILED;
    }
  }
  else
  {
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		CString szMsg;
		szMsg.Format("Sending buffer (lenght=%d) on socket %d",newbuflen,s);
		LogToFile(szMsg);
	}
#endif
    rc = send(s, newbuffer, newbuflen, 0);
    if (rc == SOCKET_ERROR) 
    {
#ifdef USE_LOGGING
			CString szError;
			PrintSocketError(WSAGetLastError(), __LINE__, &szError);
			if(m_bWriteFileErrors)	LogToFile(szError);
			if(pszInfo) pszInfo->Format("Send error (%s:%d)\n%s", __FILE__,__LINE__,szError);
#else
			if(pszInfo)
			{
				CString szError;
				PrintSocketError(WSAGetLastError(), __LINE__, &szError);
				pszInfo->Format("Send error (%s:%d)\n%s", __FILE__,__LINE__,szError);
			}
#endif
			free(buffer); buffer=NULL;
			if (newbuffer) { free(newbuffer); newbuffer = NULL; }
			return ERROR_SOCKET_SEND;
    }
    //if (DEBUG) console(-1, __FILE__, MESSAGE_INFO,  "Sent: '%s'\n", newbuffer);
  }

  if (newbuffer) { free(newbuffer); newbuffer = NULL; }
  return RETURN_SUCCESS;
}



// Gets a single line from the server, buffering greater than 1024 chars
// for later retrieval.
int CNetworking::GetLineFromSocket(char line[], SOCKET s, CString* pszInfo)
{
	char buffer[4096], buffer2[4096]; 
  int len, i, l, nReturnCode=RETURN_SUCCESS;
	
	// gets a single line from the server, buffering greater than 1024 chars for later retrieval.
	if (m_bOverflow)
	{
		if (strlen(m_pszOverflowBuffer) > 1023)
		{
			i=1023;
			while ((i>=0) && (!isspace(m_pszOverflowBuffer[i]))) i--;
			if (i<0) 
			{
				i=1023;
			}
			strncpy(line, m_pszOverflowBuffer, i);
			line[i]=0;
			strcpy(m_pszOverflowBuffer, m_pszOverflowBuffer+i);
		}
		else
		{
			strcpy(line, m_pszOverflowBuffer);
			if (m_pszOverflowBuffer) 
      { free(m_pszOverflowBuffer); m_pszOverflowBuffer = NULL; }
		  m_bOverflow=FALSE;
		}
	}
	else
	{
	  // get a line from the server.  If it is longer than 1023 chars, return the first 
		// 1023 (or less - find a whitespace character) chars, create an overflow buffer
		int nMode;
		nReturnCode = GetTelnet((int) s, &nMode);
    if (nReturnCode >= RETURN_SUCCESS)
		{
			if (nMode>0)
			{
				nReturnCode = GetReplyTelnet(buffer, s, &len, pszInfo);
				if (nReturnCode<RETURN_SUCCESS) 
				{ strcpy(line, ""); return nReturnCode; }
			}
			else
			{
				nReturnCode = GetReply(buffer, s, &len, pszInfo);
				if (nReturnCode<RETURN_SUCCESS) 
				{ strcpy(line, ""); return nReturnCode; }
			}
		}
		else return nReturnCode; 


		// if Unicode
    if ((buffer[0] == 0) && (buffer[2] == 0) && (len > 0)) 
    {
      UnicodeToASCII(buffer, buffer2, len);
      strcpy(buffer, buffer2);
    }

    // trim eol
		while  ((buffer[strlen(buffer)-1] == 10) || (buffer[strlen(buffer)-1] == 13))
			buffer[strlen(buffer)-1] = 0;
		
		l = strlen(buffer);
		if (l > 1023)
		{
    	m_bOverflow=TRUE;
			m_pszOverflowBuffer = (char *) malloc(l - 1020);
			if (m_pszOverflowBuffer == NULL)
			{
				nReturnCode = SOFTERROR_MALLOC_FAILED;
				if(pszInfo)
				{
					pszInfo->Format("Error allocating memory for overflow buffer. (%s:%d)", __FILE__,__LINE__);
				}
  			m_bOverflow=FALSE;
			}
			
			i=1023;
			while ((i>=0) && (!isspace(buffer[i]))) i--;
			if (i<0)
			{
				i=1023;
			}
			strncpy(line, buffer, i);	line[i]=0;
			if (m_bOverflow == TRUE) strcpy(m_pszOverflowBuffer, buffer+i);  // else we lost it because of memory error
		}
		else strcpy(line, buffer);		
	}
	return nReturnCode;
}


int CNetworking::GetCharFromSocket(SOCKET s, char* pch, int nLine, CString* pszInfo)
{
  int rc, nReturnCode;
  char buffer[5];

  // recv() returns the number of bytes read (1), 0 if the
  // socket was closed gracefully, or SOCKET_ERROR otherwise

  rc = recv(s, (char *) buffer, 1, 0); if(nLine<0) nLine = __LINE__;
  
  switch(rc)
	{
	case 0:
		{
			if(pszInfo)
			{
				pszInfo->Format("Connection closed gracefully. (%s:%d)",  __FILE__,__LINE__);
			}
			if(pch)	*pch = NULL;
			nReturnCode = SOFTERROR_SOCKET_NOCHAR;
    } break;
  case 1:
		{
			if(pch)	*pch = buffer[0];
			nReturnCode = RETURN_SUCCESS;
    } break;
  default:
  case SOCKET_ERROR:
		{
#ifdef USE_LOGGING
			CString szError;
			PrintSocketError(WSAGetLastError(), __LINE__, &szError);
			if(m_bWriteFileErrors)	LogToFile(szError);
			if(pszInfo) pszInfo->Format("Connection error on socket %d (%s:%d)\n%s", s, __FILE__,__LINE__,szError);
#else
			if(pszInfo)
			{
				CString szError;
				PrintSocketError(WSAGetLastError(), __LINE__, &szError);
				pszInfo->Format("Connection error on socket %d (%s:%d)\n%s", s, __FILE__,__LINE__,szError);
			}
#endif
			if(pch)	*pch = NULL;
			nReturnCode = ERROR_SOCKET_RECV;
    } break;
  }
  return nReturnCode;
}

int CNetworking::GetReply(char buffer[1024], SOCKET s, int* pnLen, CString* pszInfo)
{
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		LogToFile("GetReply");
	}
#endif
	char c;
  char*  pch;
  char reply_string[1024];		// last line of previous reply 
	BOOL bDone=FALSE;
	int nReturnCode = RETURN_SUCCESS;
	int* pnReturnCode = &nReturnCode;

  strcpy(buffer, "");  *pnLen = 0;

  pch = &reply_string[0];
	bDone=FALSE;
  while (!bDone)
  {
		TRY_PROTECTED(GetCharFromSocket(s, &c, __LINE__, pszInfo),	pnReturnCode, pszInfo, ("GetReply:"+__LINE__), -1, pnReturnCode) // this last arg doesnt get hit because of MBtype being -1;
//		nReturnCode = GetCharFromSocket(s, &c, __LINE__, pszInfo);
		if(nReturnCode < RETURN_SUCCESS)
    {  
#ifdef USE_LOGGING
	if(m_bWriteFileErrors)	
	{
		CString szMsg;
		szMsg.Format("Error receiving data at offset %d",*pnLen);
		LogToFile(szMsg);
	}
#endif
			bDone = TRUE;
      return nReturnCode; 
    } 
		if(c == '\n')		bDone = TRUE;

    *pnLen = (*pnLen)+1; // walk the length
    if ((*pnLen) < 1023) *pch++ = c;
    // else we overran the buffer
    else break;
  }
  if ((*pnLen) <= 1023) *pch = 0;

  for (int i=0; i<=(*pnLen); i++) buffer[i] = reply_string[i];
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		CString szMsg;
		szMsg.Format("Received data (%d bytes)",*pnLen);
		LogToFile(szMsg);
	}
#endif
  return nReturnCode;
}



int CNetworking::GetReplyTelnet(char buffer[1024], SOCKET s, int* pnLen, CString* pszInfo)
{
  char c, command;
  char*  pch;
  char reply_string[1024];		/* last line of previous reply */
	BOOL bDone=FALSE;
	int nReturnCode = RETURN_SUCCESS;

  strcpy(buffer, "");  *pnLen = 0;

  pch = &reply_string[0];
  while (!bDone)
  {
		nReturnCode = GetCharFromSocket(s, &c, __LINE__, pszInfo);
		if(nReturnCode < RETURN_SUCCESS)
    {  
			bDone = TRUE;
      return nReturnCode; 
    } 

    if (c == ((char) TELNET_IAC))
    { 
      // handle telnet commands 

			nReturnCode = GetCharFromSocket(s, &command, __LINE__, pszInfo);

			if(nReturnCode < RETURN_SUCCESS)
			{  
				bDone = TRUE;
				return nReturnCode; 
			} 

      if (command == ((char) TELNET_IAC))
      {
        c = ((char) TELNET_IAC);  //escaped IAC twice...
      }
      else 
      {
        nReturnCode = TelnetIACresponse(command, s, pszInfo);
        goto bottom;
      }
    }

    *pnLen = (*pnLen)+1; // walk the length
    if ((*pnLen) < 1023) *pch++ = c;
    // else we overran the buffer
    else break;

bottom:
    ;
  }
  if ((*pnLen) <= 1023) *pch = 0;

	if(nReturnCode != RETURN_SUCCESS) return nReturnCode; 
  for (int i=0; i<=(*pnLen); i++) buffer[i] = reply_string[i];
	return nReturnCode; 
}



void CNetworking::PrintSocketError(int n, int nLine, CString* pszError)
{
	if(pszError==NULL) return;

  switch (n)
	{
	case WSAEACCES: pszError->Format("Line %d: Attempt to connect datagram socket to broadcast address failed because setsockopt option SO_BROADCAST is not enabled.", nLine); break;
	case WSAEADDRINUSE: pszError->Format("Line %d: The socket's local address is already in use and the socket was not marked to allow address reuse with SO_REUSEADDR. This error usually occurs when executing bind, but could be delayed until this function if the bind was to a partially wildcard address (involving ADDR_ANY) and if a specific address needs to be committed at the time of this function.", nLine); break;
	case WSAEADDRNOTAVAIL: pszError->Format("Line %d: The remote address is not a valid address (such as ADDR_ANY).", nLine); break;
	case WSAEAFNOSUPPORT: pszError->Format("Line %d: Addresses in the specified family cannot be used with this socket.", nLine); break;
	case WSAEALREADY: pszError->Format("Line %d: A nonblocking connect call is in progress on the specified socket. Note In order to preserve backward compatibility, this error is reported as WSAEINVAL to Windows Sockets 1.1 applications that link to either Winsock.dll or Wsock32.dll. ", nLine); break;
	case WSAECONNABORTED: pszError->Format("Line %d:  The virtual circuit was terminated due to a time-out or other failure. The application should close the socket as it is no longer usable. ", nLine); break;
	case WSAECONNREFUSED: pszError->Format("Line %d: The attempt to connect was forcefully rejected.", nLine); break;
	case WSAECONNRESET: pszError->Format("Line %d: The connection was reset.", nLine); break;
	case WSAEFAULT: pszError->Format("Line %d: The parameter is not a valid part of the user address space, the namelen parameter is too small, or the name parameter contains incorrect address format for the associated address family.", nLine); break;
	case WSAEINPROGRESS: pszError->Format("Line %d: A blocking Windows Sockets 1.1 call is in progress, or the service provider is still processing a callback function.", nLine); break;
	case WSAEINTR: pszError->Format("Line %d: The blocking Windows Socket 1.1 call was canceled through WSACancelBlockingCall.", nLine); break;
	case WSAEINVAL: pszError->Format("Line %d: The parameter s is a listening socket, or is not bound.", nLine); break;
	case WSAEISCONN: pszError->Format("Line %d: The socket is already connected (connection-oriented sockets only).", nLine); break;
	case WSAEMSGSIZE: pszError->Format("Line %d:  The message was too large to fit into the specified buffer and was truncated. ", nLine); break;
	case WSAENETDOWN: pszError->Format("Line %d: The network subsystem has failed.", nLine); break;
	case WSAENETRESET: pszError->Format("Line %d:  The connection has been broken due to the keep-alive activity detecting a failure while the operation was in progress. ", nLine); break;
	case WSAENETUNREACH: pszError->Format("Line %d: The network cannot be reached from this host at this time.", nLine); break;
	case WSAENOBUFS: pszError->Format("Line %d: No buffer space is available. The socket cannot be connected.", nLine); break;
	case WSAENOTCONN: pszError->Format("Line %d: The socket is not connected. ", nLine); break;
	case WSAENOTSOCK: pszError->Format("Line %d: The descriptor is not a socket.", nLine); break;
	case WSAEOPNOTSUPP: pszError->Format("Line %d:  MSG_OOB was specified, but the socket is not stream-style such as type SOCK_STREAM, OOB data is not supported in the communication domain associated with this socket, or the socket is unidirectional and supports only send operations. ", nLine); break;
	case WSAESHUTDOWN: pszError->Format("Line %d:  The socket has been shut down; it is not possible to receive on a socket after shutdown has been invoked with how set to SD_RECEIVE or SD_BOTH. ", nLine); break;
	case WSAETIMEDOUT: pszError->Format("Line %d: Attempt to connect timed out without establishing a connection.", nLine); break;
	case WSAEWOULDBLOCK : pszError->Format("Line %d: The socket is marked as nonblocking and the operation cannot be completed immediately. ", nLine); break;
	case WSAHOST_NOT_FOUND : pszError->Format("Line %d: Authoritative Answer Host not found.", nLine); break;
	case WSANO_DATA: pszError->Format("Line %d: Valid name, no data record of requested type.", nLine); break;
	case WSANO_RECOVERY: pszError->Format("Line %d: Nonrecoverable error occurred.", nLine); break;
	case WSANOTINITIALISED: pszError->Format("Line %d: A successful WSAStartup call must occur before using this function.", nLine); break;
	case WSATRY_AGAIN: pszError->Format("Line %d: Non-Authoritative Host not found, or server failure.", nLine); break;
	default:
		{
		  LPVOID lpMsgBuf;
			CString szMessage;

			FormatMessage( 
					FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					NULL, n,
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
					(LPTSTR) &lpMsgBuf,0,NULL);
			szMessage.Format("%s",lpMsgBuf);  szMessage.TrimLeft(); szMessage.TrimRight();
			pszError->Format("Line %ld: %ld: unknown error (%s).", nLine, n, szMessage); 
			LocalFree( lpMsgBuf );
		}  break;
	}

}


int CNetworking::SendTelnetIACresponse(int yes, char command, char option, SOCKET s, CString* pszInfo)
{
  int rc;
  char temp[4];
  char outcommand;
  char commands[10][5];
  strcpy(commands[0], "WILL");  strcpy(commands[1], "WONT");  strcpy(commands[2], "DO");  strcpy(commands[3], "DONT");

  if (yes)
  {
    switch (command)
    {
    case ((char) TELNET_WILL): outcommand = ((char) TELNET_DO); break;
    case ((char) TELNET_DO):   outcommand = ((char) TELNET_WILL); break;
    case ((char) TELNET_DONT): outcommand = ((char) TELNET_WONT); break;
    case ((char) TELNET_WONT): outcommand = ((char) TELNET_DONT); break;
    }
  }
  else
  {
    switch (command)
    {
    case ((char) TELNET_WILL):
    case ((char) TELNET_WONT):
      outcommand = ((char) TELNET_DONT); break;
    case ((char) TELNET_DO):  
    case ((char) TELNET_DONT): 
      outcommand = ((char) TELNET_WONT); break;
    }
  }

	if(pszInfo)
	{
		pszInfo->Format("Response (%s:%d):\n%s", __FILE__,__LINE__, commands[outcommand-((char)TELNET_WILL)]);
	}
  
  temp[0] = (char) TELNET_IAC;
  temp[1] = (char) outcommand;
  temp[2] = (char) option;
  temp[3] = 0;
  rc = send(s, temp, 3, 0);  // can't use sendCommand, it will escape the IAC!
  if (rc == SOCKET_ERROR) 
  {
#ifdef USE_LOGGING
		CString szError;
		PrintSocketError(WSAGetLastError(), __LINE__, &szError);
		if(m_bWriteFileErrors)	LogToFile(szError);
		if(pszInfo) pszInfo->Format("Error on socket %d (%s:%d):\n%s", s, __FILE__,__LINE__,szError);
#else
		if(pszInfo)
		{
			CString szError;
			PrintSocketError(WSAGetLastError(), __LINE__, &szError);
			pszInfo->Format("Error on socket %d (%s:%d):\n%s", s, __FILE__,__LINE__,szError);
		}
#endif
    return ERROR_SOCKET_SEND;
  }
  return RETURN_SUCCESS;
}


int CNetworking::TelnetIACresponse(char command, SOCKET s, CString* pszInfo)
{
  char commands[10][5];
  char option;
	int nReturnCode = RETURN_SUCCESS;
  strcpy(commands[0], "WILL");  strcpy(commands[1], "WONT");  strcpy(commands[2], "DO");  strcpy(commands[3], "DONT");

  switch(command)
  {
  case ((char) TELNET_WILL):
  case ((char) TELNET_WONT):
  case ((char) TELNET_DO):  
  case ((char) TELNET_DONT):
		{
			nReturnCode = GetCharFromSocket(s, &option, __LINE__, pszInfo);
			if(nReturnCode == RETURN_SUCCESS)
			{  
				switch (option)
				{
				case ((char) TELOPT_BINARY):  // 0-BINARY mode - RFC 856
				case ((char) TELOPT_SGA): // 3-SUPPRESS-GO-AHEAD
					nReturnCode = SendTelnetIACresponse(TRUE, command, option, s, pszInfo);
					break;
				case ((char) TELOPT_ECHO):  // 1-ECHO mode - RFC 857
				case ((char) TELOPT_TM): // TIMING MARK RFC 860
				default:
					nReturnCode = SendTelnetIACresponse(FALSE, command, option, s, pszInfo);
					break;
				}
			}
		}	break;
    
  case ((char) TELNET_SB):    /* interpret as subnegotiation */
		{	if(pszInfo)	pszInfo->Format("Telnet IAC: SB unimplemented. (%s:%d)", __FILE__,__LINE__);} break;
  case ((char) TELNET_GA):    /* you may reverse the line */
		{	if(pszInfo)	pszInfo->Format("Telnet IAC: GA unimplemented. (%s:%d)", __FILE__,__LINE__);} break;
  case ((char) TELNET_EL):    /* erase the current line */
		{	if(pszInfo)	pszInfo->Format("Telnet IAC: EL unimplemented. (%s:%d)", __FILE__,__LINE__);} break;
  case ((char) TELNET_EC):    /* erase the current character */
		{	if(pszInfo)	pszInfo->Format("Telnet IAC: EC unimplemented. (%s:%d)", __FILE__,__LINE__);} break;
  case ((char) TELNET_AYT):   /* are you there */
		{	if(pszInfo)	pszInfo->Format("Telnet IAC: AYT unimplemented. (%s:%d)", __FILE__,__LINE__);} break;
  case ((char) TELNET_AO):    /* abort output--but let prog finish */
		{	if(pszInfo)	pszInfo->Format("Telnet IAC: AO unimplemented. (%s:%d)", __FILE__,__LINE__);} break;
  case ((char) TELNET_IP):    /* interrupt process--permanently */
		{	if(pszInfo)	pszInfo->Format("Telnet IAC: IP unimplemented. (%s:%d)", __FILE__,__LINE__);} break;
  case ((char) TELNET_BREAK): /* break */
		{	if(pszInfo)	pszInfo->Format("Telnet IAC: BREAK unimplemented. (%s:%d)", __FILE__,__LINE__);} break;
  case ((char) TELNET_DM):    /* data mark--for connect. cleaning */
		{	if(pszInfo)	pszInfo->Format("Telnet IAC: DM unimplemented. (%s:%d)", __FILE__,__LINE__);} break;
  case ((char) TELNET_NOP):   /* nop */
		{	if(pszInfo)	pszInfo->Format("Telnet IAC: NOP unimplemented. (%s:%d)", __FILE__,__LINE__);} break;
  case ((char) TELNET_SE):    /* end sub negotiation */
		{	if(pszInfo)	pszInfo->Format("Telnet IAC: SE unimplemented. (%s:%d)", __FILE__,__LINE__);} break;
  case ((char) TELNET_EOR):   /* end of record (transparent mode) */
		{	if(pszInfo)	pszInfo->Format("Telnet IAC: EOR unimplemented. (%s:%d)", __FILE__,__LINE__);} break;
  case ((char) TELNET_ABORT): /* Abort process */
		{	if(pszInfo)	pszInfo->Format("Telnet IAC: ABORT unimplemented. (%s:%d)", __FILE__,__LINE__);} break;
  case ((char) TELNET_SUSP):  /* Suspend process */
		{	if(pszInfo)	pszInfo->Format("Telnet IAC: SUSP unimplemented. (%s:%d)", __FILE__,__LINE__);} break;
  case ((char) TELNET_xEOF):  /* EOF */
		{	if(pszInfo)	pszInfo->Format("Telnet IAC: xEOF unimplemented. (%s:%d)", __FILE__,__LINE__);} break;
  }
  return nReturnCode;
}




int CNetworking::SetTelnet(int nSocket, int nMode, CString* pszInfo)
{
	if(nSocket==INVALID_SOCKET)
	{
#ifdef USE_LOGGING
		if(m_bWriteFileErrors)	
		{
			LogToFile("Invalid socket.");
		}
#endif

		if(pszInfo)
		{
			pszInfo->Format("Invalid socket. (%s:%d)", __FILE__,__LINE__);
		}
		return ERROR_SOCKET_CREATE;
	}

  if ((m_nTelnetCount < NET_MAX_CONNS)&&(m_nTelnetCount>=0))
  {
    m_TelnetFlags[m_nTelnetCount].nSocket = nSocket;
    m_TelnetFlags[m_nTelnetCount].nMode = nMode;
 		if(pszInfo)
		{
			pszInfo->Format("Mode %d set for socket %d (index=%d). (%s:%d)",nMode, nSocket, m_nTelnetCount, __FILE__,__LINE__);
		}
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		CString szMsg;
		szMsg.Format("Mode %d set for socket %d (index=%d).",nMode, nSocket, m_nTelnetCount);
		LogToFile(szMsg);
	}
#endif
    m_nTelnetCount++;
    return RETURN_SUCCESS;

  }
#ifdef USE_LOGGING
	if(m_bWriteFileErrors)	
	{
		CString szMsg;
		szMsg.Format("Maximum number of sockets (%d) has been reached.", NET_MAX_CONNS);
		LogToFile(szMsg);
	}
#endif

	if(pszInfo)
	{
		pszInfo->Format("Maximum number of sockets (%d) has been reached. (%s:%d)", NET_MAX_CONNS, __FILE__,__LINE__);
	}
  return ERROR_SOCKET_MAXNUM_REACHED;
}


int CNetworking::UnsetTelnet(int nSocket, CString* pszInfo)
{
  int i=0, j;
  while (i<m_nTelnetCount)
  {
    if (m_TelnetFlags[i].nSocket == nSocket)
    {

#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		CString szMsg;
		szMsg.Format("Socket %d removed from connection list.", m_TelnetFlags[i].nSocket);
		LogToFile(szMsg);
	}
#endif

      // delete this one
      for (j=i; j<m_nTelnetCount-1; j++)
			{
        m_TelnetFlags[j].nSocket = m_TelnetFlags[j+1].nSocket;
        m_TelnetFlags[j].nMode = m_TelnetFlags[j+1].nMode;
			}
 			if(pszInfo)
			{
				pszInfo->Format("Socket %d (index=%d) removed from connection list. (%s:%d)", nSocket, i, __FILE__,__LINE__);
			}
      if(m_nTelnetCount>0) m_nTelnetCount--;
			return RETURN_SUCCESS;
    }
		i++;
  }

#ifdef USE_LOGGING
	if(m_bWriteFileErrors)	
	{
		CString szMsg;
		szMsg.Format("Socket %d not found in connection list.", nSocket);
		LogToFile(szMsg);
	}
#endif
	if(pszInfo)
	{
		pszInfo->Format("Socket %d not found in connection list. (%s:%d)", nSocket, __FILE__,__LINE__);
	}
  return ERROR_SOCKET_NOT_IN_LIST;
}


int CNetworking::GetTelnet(int nSocket, int* pnMode, CString* pszInfo)
{
  int i;
  for (i=0; i< m_nTelnetCount; i++)
  {
    if (m_TelnetFlags[i].nSocket == nSocket)
		{
			*pnMode = m_TelnetFlags[i].nMode;
 			if(pszInfo)
			{
				pszInfo->Format("Returned %d. (%s:%d)",m_TelnetFlags[i].nMode, __FILE__,__LINE__);
			}
      return RETURN_SUCCESS;
		}
  }

	if(pszInfo)
	{
		pszInfo->Format("Socket %d not found. (%s:%d)", nSocket, __FILE__,__LINE__);
	}
  return ERROR_SOCKET_NOT_IN_LIST;
}


/************************** UNICODE ******************************/
void CNetworking::UnicodeToASCII(char* unicode, char* ascii, int nUnilen)
{
  char c1, c2;
  unsigned short us;
  int i, j=0;

  for (i=0; i<nUnilen; i+=2)
  {
    c1 = unicode[i];
    c2 = unicode[i+1];
    us = (((unsigned short) c1)<<8) + (unsigned short) c2;
		//if (us > 255) it's out of range, we truncate it
    *(ascii+j) = us & 255; // truncate
    j++;
  }
}


void CNetworking::ASCIIToUnicode(char* ascii, char* unicode, int* pnUnilen)
{
  int i, j;

  for (i=0, j=0; i<(int)strlen(ascii); i++)
  {
    *(unicode + j) = 0; j++;
    *(unicode + j) = ascii[i]; j++;
  }
  *pnUnilen = j;
  *(unicode + j++) = 0;  *(unicode + j++) = 0;  j++; // zero out the string.
}



/////////////////////////////////////////////////////////////////////////////////
//  Server
//
//  To set up a server, do the following:
//
//  1.  Create a thread with a repeating construct such as a while loop
//      This can be the main thread, but is generally better left to a thread
//      that will not be stalled by UI or other processing.
//
//      Before the repeating construct, initialize the server with the following:
//      
//        if (g_net.StartNetworking(&szInfo) != RETURN_SUCCESS) // RETURN_SUCCESS = 0
//        { // handle errors, print out extra info now in szInfo }
//        if (g_net.StartServer(nPort, &szName, &szInfo) != RETURN_SUCCESS) // RETURN_SUCCESS = 0
//        { // handle errors, print out extra info now in szInfo }
//
//      Then proceed to the repeating contstruct
//        if (g_net.ServerListener(&s, &Request, &szData) == DATA_EXISTS) DATA_EXISTS = 0
//        {
//          // handle all possible commands received.
//          ...
//          // At the earliest opportunity after appropriate processing,
//          // one of:
//          //    g_net.ServerSendCmdReply(s, CMD_ACK, szReplyData); 
//          //    g_net.ServerSendCmdReply(s, CMD_NAK, szErrorData); 
//          // should be sent
//        }


//  Common byte-based checksum.  
BYTE CNetworking::Checksum(BYTE *cmd, unsigned long len)
{
  register unsigned long i, ck=0;
  for (i=0; i<len; i++) 
  { 
    ck += (unsigned int) cmd[i]; 
    if (ck > 255) ck -= 256; 
  }
  return ((BYTE) (ck&255));
}

int CNetworking::StartServer(int nPort, CString* pszName, CString* pszInfo)
{
  struct hostent* hostp; // ptr to host info 
  struct sockaddr_in name; // name to be bound to accept socket 
  int first=TRUE;   // length of from name 
  register struct hostent* hp = 0;
  unsigned long val=1;
  char mach[MAXHOSTNAME+1]; // name of the server machine 

  if (m_nNumServers < (NET_MAX_CONNS - 1))
  {
    m_ServerInfo[m_nNumServers].nPort = nPort;
  }
  else return ERROR_SERVER_MAXNUM_REACHED;

  // create a socket
  m_ServerInfo[m_nNumServers].socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  
  if (m_ServerInfo[m_nNumServers].socket == INVALID_SOCKET)
  {
 		if(pszInfo)
		{
			pszInfo->Format("Fatal error creating socket. (%s:%d)", __FILE__,__LINE__);
		}
    return ERROR_SOCKET_CREATE;  
  }

  SetTelnet((int) m_ServerInfo[m_nNumServers].socket, 0, pszInfo);

  // make it non-blocking
  // val=1; ioctlsocket(s, FIONBIO, &val);

  gethostname( mach, MAXHOSTNAME); // where we are 
  hostp=gethostbyname( mach );     // get hostent structure 

	if(hostp==NULL)
	{
 		if(pszInfo)
		{
			pszInfo->Format("Fatal error creating socket. (%s:%d)", __FILE__,__LINE__);
		}
		ShutdownServer((int)m_ServerInfo[m_nNumServers].socket);
    return ERROR_SOCKET_CREATE;  
  }

  name.sin_family = hostp->h_addrtype; // traditionally, AF_INET 

  name.sin_port = htons((u_short) m_ServerInfo[m_nNumServers].nPort); // user defined port # 
  memcpy((char *)&name.sin_addr, hostp->h_addr, hostp->h_length );  // our IP address 


	if(bind(m_ServerInfo[m_nNumServers].socket, (const struct sockaddr *) &name, sizeof(name)))
	{
		ShutdownServer((int)m_ServerInfo[m_nNumServers].socket);
#ifdef USE_LOGGING
		CString szError;
		PrintSocketError(WSAGetLastError(), __LINE__, &szError);
		if(m_bWriteFileErrors)	LogToFile(szError);
		if(pszInfo) pszInfo->Format("Fatal error binding socket %d. (%s:%d)\n%s", m_ServerInfo[m_nNumServers].socket, __FILE__,__LINE__,szError);
#else
		if(pszInfo)
		{
			CString szError;
			PrintSocketError(WSAGetLastError(), __LINE__, &szError);
			pszInfo->Format("Fatal error binding socket %d. (%s:%d)\n%s", m_ServerInfo[m_nNumServers].socket, __FILE__,__LINE__,szError);
		}
#endif
    return ERROR_SOCKET_BIND;  

	}

	if(listen( m_ServerInfo[m_nNumServers].socket, SOMAXCONN ))
	{
		ShutdownServer((int)m_ServerInfo[m_nNumServers].socket);
#ifdef USE_LOGGING
		CString szError;
		PrintSocketError(WSAGetLastError(), __LINE__, &szError);
		if(m_bWriteFileErrors)	LogToFile(szError);
		if(pszInfo) pszInfo->Format("Fatal error listening on socket %d. (%s:%d)\n%s", m_ServerInfo[m_nNumServers].socket, __FILE__,__LINE__,szError);
#else
		if(pszInfo)
		{
			CString szError;
			PrintSocketError(WSAGetLastError(), __LINE__, &szError);
			pszInfo->Format("Fatal error listening on socket %d. (%s:%d)\n%s", m_ServerInfo[m_nNumServers].socket, __FILE__,__LINE__,szError);
		}
#endif
    return ERROR_SOCKET_LISTEN;  

	}

  FD_ZERO(&m_fdsRead);
//  FD_ZERO(&m_fdsWrite);

 	if(pszInfo)
	{
		pszInfo->Format("Server added: port %d, socket %ld.  Total servers=%d. (%s:%d)",
			m_ServerInfo[m_nNumServers].nPort,m_ServerInfo[m_nNumServers].socket, m_nNumServers+1, __FILE__,__LINE__);
	}
	
//	CString foo; foo.Format("Server added: %s, port %d, socket %ld.  Total servers=%d. (%s:%d)", mach, m_ServerInfo[m_nNumServers].nPort,m_ServerInfo[m_nNumServers].socket, m_nNumServers+1, __FILE__,__LINE__);
//	AfxMessageBox(foo);

  m_nNumServers++;  // we're ok, add to list.

  if (pszName != NULL)
  {
    char ipstring[256];
    PrintIP(name, ipstring, 0);
    pszName->Format("%s", ipstring);
  }

  return RETURN_SUCCESS;
}





int CNetworking::ShutdownServer(int nPort, CString* pszInfo)
{
  int j=0, index;
  SOCKET s;
	int nReturnCode = RETURN_SUCCESS;

	nReturnCode = GetServerSocket(nPort, &index, &s, pszInfo);
  if (nReturnCode==RETURN_SUCCESS)
  {
		UnsetTelnet((int) s);  
    shutdown(s, SD_BOTH);
    closesocket(s);
 		if(pszInfo)
		{
			pszInfo->Format("Server %d on port %d (socket %d) has been shut down. (%s:%d)", index, m_ServerInfo[j].nPort, m_ServerInfo[j].socket, __FILE__,__LINE__);
		}
    for (j=index; j<m_nNumServers-1; j++)
    {
      m_ServerInfo[j].nPort = m_ServerInfo[j+1].nPort;
      m_ServerInfo[j].socket = m_ServerInfo[j+1].socket;
    }
    if(m_nNumServers>0) m_nNumServers--;
    return RETURN_SUCCESS;
  }
  return nReturnCode;
}


BOOL CNetworking::GetServerSocket(int nPort, int *index, SOCKET *s, CString* pszInfo)
{
  int i;
  for (i=0; i<m_nNumServers; i++)
  {
    if (m_ServerInfo[i].nPort == nPort)
    {
      *index = i;
      *s = m_ServerInfo[i].socket;
 			if(pszInfo)
			{
				pszInfo->Format("Server %d on port %d has socket %d. (%s:%d)", i, m_ServerInfo[i].nPort, m_ServerInfo[i].socket, __FILE__,__LINE__);
			}
      return RETURN_SUCCESS;
    }
  }
 	if(pszInfo)
	{
		pszInfo->Format("Server on port %d not found. (%s:%d)", m_ServerInfo[i].nPort, __FILE__,__LINE__);
	}
  return ERROR_SERVER_NOT_IN_LIST;
}


//This function is called to see of there's anything waiting. 
//There's a default timeout of 10ms here,
SOCKET CNetworking::PollServer(int nPort, CString* pszIP, int* pnPort, int nTimeoutMS)
{
  struct timeval tv;
  SOCKET socket, snew;   // socket for sending data 
  struct sockaddr_in from; // client name to send data to
  int i, index, rc, fromlen;   // length of from name 
  //char myIP[100]; // name of the server machine 

  if (GetServerSocket(nPort, &index, &socket)!=RETURN_SUCCESS) return NULL;
//	CString f; f.Format("polling server port %d on socket %d (%d)",nPort,socket,index); AfxMessageBox(f);

	if(nTimeoutMS<=0) nTimeoutMS = 10;
  tv.tv_sec = nTimeoutMS/1000; tv.tv_usec = nTimeoutMS%1000;  // timeout value

  FD_SET(socket, &m_fdsRead);

  rc = select(0, &m_fdsRead, NULL, NULL, &tv);
  switch(rc)
  {
  case 0:
    // timed out;
    return NULL;
    break;
  case SOCKET_ERROR:
    //PrintSocketError(WSAGetLastError(), __LINE__, pszError);
//		{CString f; f.Format("%d",rc);AfxMessageBox(f);}
   return NULL;
    break;
  default:
    {
//			CString f; f.Format("%d",rc);AfxMessageBox(f);
      while (rc > 0)
      {
        if (FD_ISSET(socket, &m_fdsRead)) { i=0; rc--; }
        else continue;

        fromlen = sizeof( from );
        snew = accept( socket, (struct sockaddr *) &from, &fromlen );
        if( snew == INVALID_SOCKET )
        {
//          errno = WSAGetLastError();
//          if (( errno != WSAEINTR )&&(errno != WSAEWOULDBLOCK)) // nothing
          return NULL;
        }
        else
        {
          //AfxMessageBox("accept() was successful");
          //console(-1, __FILE__, MESSAGE_INFO, "\n%s\nServicing connection on port %ld, socket %ld from: ", port, snew);
          //printIP(from, myIP, IP_PORT);
					if(pszIP)
						pszIP->Format("%d.%d.%d.%d", 
							from.sin_addr.s_net, 
							from.sin_addr.s_host, 
							from.sin_addr.s_lh, 
							from.sin_addr.s_impno);
					if(pnPort)
		       *pnPort = ntohs(from.sin_port);
          return snew;
        }
      }
    }
  }
  return NULL;
}


// Given a struct sockaddr_in, print the IP address and port 
void CNetworking::PrintIP(struct sockaddr_in i, char ip[], BOOL bPort)
{
  if (!bPort)  // IP only
  {
    sprintf(ip, "%d.%d.%d.%d", 
			i.sin_addr.s_net, 
			i.sin_addr.s_host, 
	    i.sin_addr.s_lh,
			i.sin_addr.s_impno);
  }
  else // IP and port
  {
    sprintf(ip, "%d.%d.%d.%d:%d", 
			i.sin_addr.s_net, 
			i.sin_addr.s_host, 
	    i.sin_addr.s_lh, 
			i.sin_addr.s_impno, 
			ntohs(i.sin_port));
  }
}




////////////////////////////////////////////////////////////////////
//
// The following implements a 2-way Client-Server communications protocol
// that does something like this:
//
//    client sends command (checksum byte / cmd byte / 2 bytes size / optional data stream
//    server receives command, sends reply (same fmt as above)
//    client receives reply, closes connection.
//    
//  Replies are usually CMD_ACK or CMD_NAK, but may also return application-
//  defined binary data
//


int CNetworking::ServerListener(int nPort, SOCKET* ps, BYTE* pRequest, CString* pszData, int nTimeoutMS, CString* pszInfo)
{
  CString szFromIP; int nFromPort;
  return ServerListener(nPort, ps, pRequest, pszData, &szFromIP, &nFromPort, nTimeoutMS, pszInfo);
}


int CNetworking::ServerListener(int nPort, SOCKET* ps, BYTE* pRequest, CString* pszData, CString* pszIP, int* pnPort, int nTimeoutMS, CString* pszInfo)
{
  if (m_bServerListening) return RETURN_NOTHING;
//	CString f; f.Format("polling server port %d",nPort); AfxMessageBox(f);

	SOCKET s = NULL;
	SOCKET* pSocket = &s;


	TRY_PROTECTED(PollServer(nPort, pszIP, pnPort, nTimeoutMS),	pSocket, pszInfo, ("ServerListener:"+__LINE__), -1, pszInfo) // this last arg doesnt get hit because of MBtype being -1;
//  *ps = PollServer(nPort, pszIP, pnPort, nTimeoutMS);  // returns an open connection with the client

	*ps = s;
  if (*ps == NULL) return RETURN_NOTHING;  // no requests waiting
  
  m_bServerListening =  TRUE;

	int nReturnCode = GetServerRequest(*ps, pRequest, pszData, pszInfo);
  if (nReturnCode < RETURN_SUCCESS)
  {
    m_bServerListening = FALSE;
///*    
// we don;t want to shut down the socket just because there was an error
// - the error could just be checksum or something of that nature, not necessarily a comm error.

		if((*ps!=NULL)&&(*ps!=SOCKET_ERROR)&&(*ps!=INVALID_SOCKET))
		{
			UnsetTelnet((int) *ps);  //was commented out
			shutdown (*ps, SD_BOTH);
			closesocket(*ps);
		}
//*/
    *ps = NULL;
    return nReturnCode;
  }

  m_bServerListening = FALSE;
  return nReturnCode;
}


// Warning! a maximum of UINT_MAX = 0xffffffff bytes of data in a single request
int CNetworking::GetServerRequest(SOCKET s, BYTE* pRequest, CString* pszRequestText, CString* pszInfo)
{
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	LogToFile("GetServerRequest");
#endif
	int nReturnCode = RETURN_SUCCESS;
	int* pnReturnCode = &nReturnCode;
	CString szInternalInfo;
  BYTE size; // we use 4 bytes to determine data size,max is UINT_MAX = 0xffffffff
	char ch;
  unsigned long nSize, i, nBytesWritten, nBufferIndex;
  BYTE CheckByte;
  char buffer[8]; 
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		LogToFile("Receiving checksum");
	}
#endif

	TRY_PROTECTED(GetCharFromSocket(s, &ch, __LINE__, &szInternalInfo),	pnReturnCode, pszInfo, ("GetServerRequest:"+__LINE__), -1, pnReturnCode) // this last arg doesnt get hit because of MBtype being -1;
//	nReturnCode = GetCharFromSocket(s, &ch, __LINE__, pszInfo); // error info returned to pszInfo
  if (nReturnCode!=RETURN_SUCCESS) 
	{
		CString szInfo; 
#ifdef USE_LOGGING
	if(m_bWriteFileErrors)	
	{
		CString szMsg;
		szMsg.Format("Error receiving checksum. %s",(szInternalInfo.GetLength()>0?szInternalInfo:szInfo));
		LogToFile(szMsg);
	}
#endif
		if(pszInfo)
		{
			szInfo = *pszInfo;
			if(szInternalInfo.GetLength())
				pszInfo->Format("Error receiving checksum. (%s:%d)\n%s", __FILE__,__LINE__, szInternalInfo);
			else
				pszInfo->Format("Error receiving checksum. (%s:%d)\n%s", __FILE__,__LINE__, szInfo);
		}
		return nReturnCode;
	}

  CheckByte = (BYTE) ch; 

//	CString f;	f.Format("Received checksum 0x%02x",CheckByte); AfxMessageBox(f);
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		CString szMsg;
		szMsg.Format("Received checksum %d on socket %d, receiving command byte.",CheckByte, s);
		LogToFile(szMsg);
	}
#endif

	TRY_PROTECTED(GetCharFromSocket(s, &ch, __LINE__, &szInternalInfo),	pnReturnCode, pszInfo, ("GetServerRequest:"+__LINE__), -1, pnReturnCode) // this last arg doesnt get hit because of MBtype being -1;
// 	nReturnCode = GetCharFromSocket(s, &ch, __LINE__, pszInfo); // error info returned to pszInfo
  if (nReturnCode!=RETURN_SUCCESS) 
	{
		CString szInfo;  
#ifdef USE_LOGGING
	if(m_bWriteFileErrors)	
	{
		CString szMsg;
		szMsg.Format("Error receiving command byte. %s",(szInternalInfo.GetLength()>0?szInternalInfo:szInfo));
		LogToFile(szMsg);
	}
#endif
		if(pszInfo)
		{
			szInfo = *pszInfo;
			if(szInternalInfo.GetLength())
				pszInfo->Format("Error receiving command byte. (%s:%d)\n%s", __FILE__,__LINE__, szInternalInfo);
			else
				pszInfo->Format("Error receiving command byte. (%s:%d)\n%s", __FILE__,__LINE__, szInfo);
		}
		return nReturnCode;
	}

  *pRequest = (BYTE) ch; 
 
  nBufferIndex=0;
  buffer[nBufferIndex++] = *pRequest; //buffer now 1 byte

  *pszRequestText = "";
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		CString szMsg;
		szMsg.Format("Received command byte 0x%02x, receiving message header.",(BYTE) ch);
		LogToFile(szMsg);
	}
#endif

	nSize=0;
	for(i=0; i<4; i++)
	{
		TRY_PROTECTED(GetCharFromSocket(s, &ch, __LINE__, &szInternalInfo),	pnReturnCode, pszInfo, ("GetServerRequest:"+__LINE__), -1, pnReturnCode) // this last arg doesnt get hit because of MBtype being -1;
 		//nReturnCode = GetCharFromSocket(s, &ch, __LINE__, pszInfo); // error info returned to pszInfo
		if (nReturnCode!=RETURN_SUCCESS) 
		{
			CString szInfo; 
#ifdef USE_LOGGING
	if(m_bWriteFileErrors)	
	{
		CString szMsg;
		szMsg.Format("Error receiving message header. %s",(szInternalInfo.GetLength()>0?szInternalInfo:szInfo));
		LogToFile(szMsg);
	}
#endif
			if(pszInfo)
			{
				szInfo = *pszInfo;
				if(szInternalInfo.GetLength())
					pszInfo->Format("Error receiving size byte[%d]. (%s:%d)\n%s", i, __FILE__,__LINE__, szInternalInfo);
				else
					pszInfo->Format("Error receiving size byte[%d]. (%s:%d)\n%s", i, __FILE__,__LINE__, szInfo);
			}
			return nReturnCode;
		}
//		f.Format("Received size %d = %d",i, ch); AfxMessageBox(f);
		size = (BYTE) ch;
		buffer[nBufferIndex++] = size;
		nSize = (nSize<<8) + (unsigned long) size;
	}
	//buffer now 5 bytes
//	f.Format("Received size %d",nSize); AfxMessageBox(f);
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		CString szMsg;
		szMsg.Format("Received message header, data length = %d",nSize);
		LogToFile(szMsg);
	}
#endif

	if(nSize > UINT_MAX-5)
	{
#ifdef USE_LOGGING
	if(m_bWriteFileErrors)	
	{
		CString szMsg;
		szMsg.Format("Buffer length %d exceeds max",nSize);
		LogToFile(szMsg);
	}
#endif
		nReturnCode = SOFTERROR_BUFFER_MAXLEN_EXCEEDED;
		if(pszInfo)	pszInfo->Format("Maximum buffer size 0xffffffff exceeded.  Data will be truncated, checksum is invalid. (%s:%d)", __FILE__,__LINE__);
		nSize = UINT_MAX-5;
	}

  LPTSTR p = pszRequestText->GetBuffer(nSize+10);
  nBytesWritten = 0;
	int nInternalReturnCode;
	int* pnInternalReturnCode = &nInternalReturnCode;
//	CString szInternalInfo;
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		CString szMsg;
		szMsg.Format("Receiving data (%d bytes)",nSize);
		LogToFile(szMsg);
	}
#endif
  for (i=0; i<nSize; i++)
  {
		TRY_PROTECTED(GetCharFromSocket(s, &ch, __LINE__, &szInternalInfo),	pnInternalReturnCode, pszInfo, ("GetServerRequest:"+__LINE__), -1, pnInternalReturnCode) // this last arg doesnt get hit because of MBtype being -1;

// 		nInternalReturnCode = GetCharFromSocket(s, &ch, __LINE__, &szInternalInfo); // error info returned to pszInfo
		if (nInternalReturnCode!=RETURN_SUCCESS) 
		{
			if(pszInfo)
			{
				if(szInternalInfo.GetLength())
					pszInfo->Format("Command %02x: error receiving data. (%s:%d)\n%s", *pRequest, __FILE__,__LINE__, szInternalInfo);
				else
					pszInfo->Format("Command %02x: error receiving data. (%s:%d)\n%s", *pRequest, __FILE__,__LINE__, *pszInfo);
			}
#ifdef USE_LOGGING
	if(m_bWriteFileErrors)	
	{
		CString szMsg;
		szMsg.Format("Error receiving data at offset %d. %s",i, (szInternalInfo.GetLength()>0?szInternalInfo:(pszInfo==NULL?"":*pszInfo)));
		LogToFile(szMsg);
	}
#endif
			return nInternalReturnCode;
		}

    *(p+i) = (BYTE) ch;
    nBytesWritten++;
  }
  pszRequestText->ReleaseBuffer(nBytesWritten);  // Surplus memory released, p is now invalid.

//	f.Format("Received data %s", *pszRequestText); AfxMessageBox(f);

  CString szChecksumThis;
  p = szChecksumThis.GetBuffer(nSize+10);
  for (i=0; i<nBufferIndex; i++) *(p+i) = buffer[i];
  for (i=0; i<nBytesWritten; i++) *(p+i+nBufferIndex) = pszRequestText->GetAt(i);
  szChecksumThis.ReleaseBuffer(nBufferIndex+nBytesWritten);
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		CString szMsg;
		szMsg.Format("Received data (%d bytes)",nSize);
		LogToFile(szMsg);
	}
#endif

	BYTE CheckByte2 = Checksum((unsigned char *)((LPCTSTR)szChecksumThis), nBytesWritten+nBufferIndex);
//	f.Format("Calculated checksum 0x%02x",CheckByte2); AfxMessageBox(f);
  if (CheckByte != CheckByte2) 
  { 
		if(nReturnCode != SOFTERROR_BUFFER_MAXLEN_EXCEEDED)
		{
#ifdef USE_LOGGING
	if(m_bWriteFileErrors)	
	{
		CString szMsg;
		szMsg.Format("Checksum error (0x%02x != 0x%02x)",CheckByte, CheckByte2);
		LogToFile(szMsg);
	}
#endif
			if(pszInfo)	pszInfo->Format("Command %02x: checksum error (0x%02x != 0x%02x). (%s:%d)", *pRequest, CheckByte, CheckByte2,__FILE__,__LINE__);
			return ERROR_CHECKSUM; 
		}
  }
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		LogToFile("exit GetServerRequest");
	}
#endif
  return nReturnCode;
}


int CNetworking::ServerSendCmd(int cmd, CString szServerIP, int nServerPort,
                                BYTE* pReplyCmd, CString* pszReply, BOOL bRetry, 
                                int nTimeoutMilliseconds, CString* pszInfo)
{
  CString szBuffer;
  szBuffer.Format("%c%c%c%c%c", (BYTE)cmd, 0,0,0,0);
  return ServerSendCmdInternals(szBuffer, 5, FALSE, NULL, szServerIP, nServerPort,
        pReplyCmd, pszReply, bRetry, nTimeoutMilliseconds);
}

int CNetworking::ServerSendCmd(int cmd, CString szData, CString szServerIP, int nServerPort, 
                                BYTE* pReplyCmd, CString* pszReply, BOOL bRetry, 
                                int nTimeoutMilliseconds, CString* pszInfo)
{
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	LogToFile("ServerSendCmd");
#endif
	int nReturnCode = RETURN_SUCCESS;
  CString szBuffer;
  DWORD dwDataLength = szData.GetLength() + 5; // 5 bytes is command+length

  if (dwDataLength > UINT_MAX)
  {
#ifdef USE_LOGGING
	if(m_bWriteFileErrors)	LogToFile("Maximum buffer size 0xffffffff exceeded");
#endif
		if(pszInfo)	pszInfo->Format("Maximum buffer size 0xffffffff exceeded. (%s:%d)", __FILE__,__LINE__);
    return ERROR_BUFFER_MAXLEN_EXCEEDED;
  }

  // get the size bytes
  CString szTemp = ServerGetStringFromSize((unsigned long) szData.GetLength());

  // szBuffer.Format("%c%c%c%s", cmd, szTemp[0],szTemp[1],(LPCTSTR)szData);
  //or
  // szBuffer.Format("%c%c%c", cmd, szTemp[0], szTemp[1]); szBuffer += szData;
  //or
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	LogToFile("Buffer header assignment");
#endif
  LPTSTR p = szBuffer.GetBuffer(dwDataLength);
  *(p)   = (BYTE) cmd;
  *(p+1) = (BYTE) szTemp[0];
  *(p+2) = (BYTE) szTemp[1];
  *(p+3) = (BYTE) szTemp[2];
  *(p+4) = (BYTE) szTemp[3];
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		CString szMsg;
		szMsg.Format("Memcpy buffer length %d",szData.GetLength());
		LogToFile(szMsg);
	}
#endif
  memcpy((void *) (p+5), (LPCTSTR) szData, szData.GetLength());
  szBuffer.ReleaseBuffer(dwDataLength);  // Surplus memory released, p is now invalid.
 
  return ServerSendCmdInternals(szBuffer, dwDataLength, FALSE, NULL, szServerIP, nServerPort,
    pReplyCmd, pszReply, bRetry, nTimeoutMilliseconds, pszInfo);
}


int CNetworking::ServerSendCmdReply(SOCKET s, int cmd, BOOL bRetry, int nTimeoutMilliseconds, CString* pszInfo)
{
  CString szBuffer, szReply;  
	BYTE ReplyCmd;
  szBuffer.Format("%c%c%c%c%c", (BYTE)cmd, 0,0,0,0);
  return ServerSendCmdInternals(szBuffer, 5, TRUE, s, "", 0, &ReplyCmd, 
    &szReply, bRetry, nTimeoutMilliseconds, pszInfo);
}


BOOL CNetworking::ServerSendCmdReply(SOCKET s, int cmd, CString szData, 
                                     BOOL bRetry, int nTimeoutMilliseconds, CString* pszInfo)
{
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	LogToFile("ServerSendCmdReply");
#endif
	int nReturnCode = RETURN_SUCCESS;
  CString szBuffer, szReply;  
	BYTE ReplyCmd;
  DWORD dwDataLength = szData.GetLength() + 5; // 5 bytes is command+length

  if (dwDataLength > UINT_MAX)
  {
#ifdef USE_LOGGING
	if(m_bWriteFileErrors)	LogToFile("Maximum buffer size 0xffffffff exceeded");
#endif
		if(pszInfo)	pszInfo->Format("Maximum buffer size 0xffffffff exceeded. (%s:%d)", __FILE__,__LINE__);
    return ERROR_BUFFER_MAXLEN_EXCEEDED;
  }

  // get the size bytes
  CString szTemp = ServerGetStringFromSize((unsigned long) szData.GetLength());

  // szBuffer.Format("%c%c%c%s", cmd, szTemp[0],szTemp[1],(LPCTSTR)szData);
  //or
  // szBuffer.Format("%c%c%c", cmd, szTemp[0], szTemp[1]); szBuffer += szData;
  //or
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	LogToFile("Buffer header assignment");
#endif
  LPTSTR p = szBuffer.GetBuffer(dwDataLength);
  *(p)   = (BYTE) cmd;
  *(p+1) = (BYTE) szTemp[0];
  *(p+2) = (BYTE) szTemp[1];
  *(p+3) = (BYTE) szTemp[2];
  *(p+4) = (BYTE) szTemp[3];
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		CString szMsg;
		szMsg.Format("Memcpy buffer length %d",szData.GetLength());
		LogToFile(szMsg);
	}
#endif
  memcpy((void *) (p+5), (LPCTSTR) szData, szData.GetLength());
  szBuffer.ReleaseBuffer(dwDataLength);  // Surplus memory released, p is now invalid.

  return ServerSendCmdInternals(szBuffer, dwDataLength, TRUE, s, "", 0,
    &ReplyCmd, &szReply, bRetry, nTimeoutMilliseconds, pszInfo);
}


//  The following is the engine that assembles the data stream
//  with checksum and sends it.  It closes the connection when done,
int CNetworking::ServerSendCmdInternals(CString szCmdString, int nLen,
                                         BOOL bIsReply, 
                                         SOCKET s,
                                         CString szServerIP, int nServerPort,
                                         BYTE* pReplyCmd, CString* pszReplyData,
                                         BOOL bRetry, 
                                         int nTimeoutMilliseconds,
																				 CString* pszInfo)
{
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	LogToFile("ServerSendCmdInternals");
#endif
	int nReturnCode = RETURN_SUCCESS;
  SOCKET socket;
  int nBytesSent, nTries = 3;
  int nTry, okay=FALSE;
  char buffer[10];

	if(nTimeoutMilliseconds<=0) nTimeoutMilliseconds=10000;

  if (nLen > UINT_MAX)
  {
		if(pszInfo)	pszInfo->Format("Maximum buffer size 0xffffffff exceeded. (%s:%d)", __FILE__,__LINE__);
    return ERROR_BUFFER_MAXLEN_EXCEEDED;
  }

  if (!bRetry) nTries = 1;
  for (nTry=0; nTry<nTries; nTry++)
  {
    if (bIsReply) 
    {
			if (s == NULL)
			{
				if(pszInfo)	pszInfo->Format("Socket was NULL. (%s:%d)", __FILE__,__LINE__);
				return ERROR_SOCKET_NOT_IN_LIST;
			}
      socket = s;
    }
    else
    {
#ifdef USE_LOGGING
	if(m_bWriteFileOps)
	{
		CString szMsg;
		szMsg.Format("Open connection on %s:%d",szServerIP,nServerPort);
		LogToFile(szMsg);
	}
#endif

			nReturnCode = OpenConnection(szServerIP, nServerPort, &socket, FALSE, nTimeoutMilliseconds, pszInfo);
      if (nReturnCode<RETURN_SUCCESS) // soft errors ok
      {
				if(pszInfo)
				{
					CString szInfo;  szInfo = *pszInfo;
				  pszInfo->Format("Connection error (%s:%d)\n%s", __FILE__,__LINE__, szInfo);
				}
        return nReturnCode;
      }
    }
  
    buffer[0] = (BYTE) Checksum((BYTE *) ((LPCTSTR)szCmdString), nLen);
#ifdef USE_LOGGING
	if(m_bWriteFileOps)
	{
		CString szMsg;
		szMsg.Format("Send checksum on socket %d",socket);
		LogToFile(szMsg);
	}
#endif

    nBytesSent = send(socket, buffer, 1, 0); // checksum
    if (nBytesSent == SOCKET_ERROR) 
    {
#ifdef USE_LOGGING
			CString szError;
			PrintSocketError(WSAGetLastError(), __LINE__, &szError);
			if(m_bWriteFileErrors)	LogToFile(szError);
			if(pszInfo)	pszInfo->Format("ServerSendCmdInternals error sending checksum. (%s:%d)\n%s",  __FILE__,__LINE__, szError);

#else
			if(pszInfo)
			{
				CString szError;
				PrintSocketError(WSAGetLastError(), __LINE__, &szError);
				pszInfo->Format("ServerSendCmdInternals error sending checksum. (%s:%d)\n%s",  __FILE__,__LINE__, szError);
			}
#endif
      break; 
    }
    else if (nBytesSent != 1)
    {
			if(pszInfo)
			{
				pszInfo->Format("ServerSendCmdInternals error sending checksum.  %d bytes sent.  (%s:%d)", nBytesSent, __FILE__,__LINE__);
			}
      break; 
    }

#ifdef USE_LOGGING
	if(m_bWriteFileOps)
	{
		CString szMsg;
		szMsg.Format("Send data (length=%d) on socket %d",nLen, socket);
		LogToFile(szMsg);
	}
#endif
    nBytesSent = send(socket, (LPCTSTR) szCmdString, nLen, 0);
    if (nBytesSent == SOCKET_ERROR)
    {
#ifdef USE_LOGGING
			CString szError;
			PrintSocketError(WSAGetLastError(), __LINE__, &szError);
			if(m_bWriteFileErrors)	LogToFile(szError);
			if(pszInfo)	pszInfo->Format("ServerSendCmdInternals error sending data. (%s:%d)\n%s",  __FILE__,__LINE__, szError);
#else
			if(pszInfo)
			{
				CString szError;
				PrintSocketError(WSAGetLastError(), __LINE__, &szError);
				pszInfo->Format("ServerSendCmdInternals error sending data. (%s:%d)\n%s",  __FILE__,__LINE__, szError);
			}
#endif
      break;
    }
    else if (nBytesSent != nLen)
    {
			if(pszInfo)
			{
				pszInfo->Format("ServerSendCmdInternals error sending data.  %d bytes sent. (%s:%d)", nBytesSent, __FILE__,__LINE__);
			}
      break; 
    }

    if (bIsReply) 
    {
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	LogToFile("Closing connection...");
#endif
      CloseConnection(socket, FALSE, pszInfo); // even if fails, return success (failure is only internal lookup list, not actual connection)
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	LogToFile("Closed connection; exiting ServerSendCmdInternals.");
#endif
      return RETURN_SUCCESS;
    }

    // A reply must be received for the command just sent.
    // It must be decoded and the result returned in pszReplyData.
		nReturnCode = GetServerRequest(socket, pReplyCmd, pszReplyData, pszInfo);
    if(nReturnCode == RETURN_SUCCESS) break;

#ifdef USE_LOGGING
	if(m_bWriteFileOps)	LogToFile("Closing connection....");
#endif
    CloseConnection(socket, TRUE, pszInfo);
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	LogToFile("Closed connection; exiting ServerSendCmdInternals.");
#endif
  }
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	LogToFile("Closing connection.....");
#endif
  CloseConnection(socket, TRUE, pszInfo);
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	LogToFile("Closed connection; exiting ServerSendCmdInternals.");
#endif
  return nReturnCode;
}



// For sending size as 4 bytes
CString CNetworking::ServerGetStringFromSize(unsigned long dwSize)
{
  CString szReturn="";

  if (dwSize > UINT_MAX) dwSize = UINT_MAX;
	szReturn.Format("%c%c%c%c",  
    (unsigned char) ((dwSize >> 24) & 255),
    (unsigned char) ((dwSize >> 16) & 255),
    (unsigned char) ((dwSize >>  8) & 255),
    (unsigned char) (dwSize & 255)
		);

//  CString szString="";
//	szString.Format("ServerGetStringFromSize %d = %d %d %d %d",dwSize, szReturn.GetAt(0), szReturn.GetAt(1), szReturn.GetAt(2), szReturn.GetAt(3));
//	AfxMessageBox(szString);
  return szReturn;
}





/////////////////////////////////////////////////////////////////////////////////
//
//  Internet components
//

// The following function will execute a URL (CGI or otherwise) and return the 
// resultant text as a CString.  If it returns FALSE, then the textData is 
// undefined (probably ""), but the error string will have some info about why 
// it failed.  If it returns TRUE, 'error' contains the success code (usually 
// 100, I think).  HTTP_RETURN_SUCCESS is defined as any error code <300.
// The URL could be of the form:
//    "http://www.a.com/cgi-bin/x.cgi?action=foo&var=bar"
// or perhaps just:
//    "http://www.a.com/foo.html",
//    "http://www.a.com:81/foo.html",
//
// You will notice that bAuthenticate is always FALSE.
// I *think* the HTTP authentication works, but I wouldn't
// bet the farm on it...   You won't need this, I gather.
// 
// This depends on globals g_cnet and g_cLog.  You can
// pass them, or whatever.
// 

typedef WORD INTERNET_PORT;
BOOL AFXAPI AfxParseURL(LPCTSTR pstrURL, DWORD& dwServiceType,
	CString& strServer, CString& strObject, INTERNET_PORT& nPort);


//  The following two methods are the main entry points:
int CNetworking::RetrieveURLToText(CString szURL, CString* pszTextData, BOOL bSecure, BOOL bCache, CString szHeaders, CString* pszInfo)
{
  return RetrieveURLInternals(URL_MODE_TEXT, NULL, szURL, pszTextData, bSecure, bCache, szHeaders, pszInfo);
}

int CNetworking::RetrieveURLToFile(CString szURL, CString szTempfile, BOOL bSecure, BOOL bCache, CString szHeaders, CString* pszInfo)
{
  FILE *fp;
  CString szTextData;
  fp=fopen((LPCTSTR) szTempfile, "wb");
  if (fp == NULL) return ERROR_FILE_NOT_OPEN;
  return RetrieveURLInternals(URL_MODE_FILE, fp, szURL, &szTextData, bSecure, bCache, szHeaders, pszInfo);
}



//handles advanced HTTP protocol requests such as secure (passworded) POST connections.  
int CNetworking::RetrieveURLInternalsAdvanced(int nMode, FILE* fp, 
                    CString szServer, int nPort, CString szObject, CString* pszTextData, 
                    BOOL bSecure, BOOL bCache, CString szHeaders, CString szData, 
                    CString szUser, CString szPassword, CString* pszInfo)
{
  DWORD dwFlags, dwFlagsInet;
  int i, nReturnCode=RETURN_SUCCESS;
  char buffer[1026];
  unsigned long nContentLength=0, nIndex=0;
  LPTSTR p;

  CInternetSession* pcisInet;

  if (bCache)
  {
    dwFlagsInet = 0;
    dwFlags = INTERNET_FLAG_TRANSFER_BINARY; //INTERNET_FLAG_TRANSFER_ASCII 
  }
  else
  {
    dwFlagsInet = INTERNET_FLAG_DONT_CACHE;
    dwFlags = INTERNET_FLAG_DONT_CACHE | INTERNET_FLAG_RELOAD | INTERNET_FLAG_TRANSFER_BINARY; //INTERNET_FLAG_TRANSFER_ASCII 
  }
  if (bSecure) dwFlags |= INTERNET_FLAG_SECURE;


  *pszTextData = "";
  
  //(LPCTSTR pstrAgent = NULL, DWORD dwContext = 1, DWORD dwAccessType = INTERNET_OPEN_TYPE_PRECONFIG, LPCTSTR pstrProxyName = NULL, LPCTSTR pstrProxyBypass = NULL, DWORD dwFlags = 0 );
  
  //CInternetSession cisInet(NULL, 1, INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, INTERNET_FLAG_DONT_CACHE);
  pcisInet = new CInternetSession(NULL, 1, INTERNET_OPEN_TYPE_PRECONFIG, 
    NULL, NULL, dwFlagsInet );
  
  CHttpFile* pfile=NULL;
  UINT count;


  CHttpConnection *hc;
  try
  {
    hc = pcisInet->GetHttpConnection(szServer, nPort, szUser, szPassword);
  }
  catch (CInternetException *e)
  {
    if(pszInfo) pszInfo->Format("MSInet: Error (%d) connecting to web server.", e->m_dwError);
    pcisInet->Close();
    delete pcisInet;
    delete pfile;
    return ERROR_MSINET_CONNECT;
  }

  try
  {
/*
CHttpFile* OpenRequest( int nVerb, LPCTSTR pstrObjectName, 
LPCTSTR pstrReferer = NULL, DWORD dwContext = 1, LPCTSTR* pstrAcceptTypes = NULL, 
LPCTSTR pstrVersion = NULL, DWORD dwFlags = INTERNET_FLAG_EXISTING_CONNECT );
*/
      
		pfile =  (CHttpFile *) hc->OpenRequest(0, //=HTTP_VERB_POST
			(LPCTSTR) szObject,	NULL, 1, NULL, NULL, dwFlags	);

		pfile->SendRequest((LPCTSTR) szHeaders, szHeaders.GetLength(), (void *)((LPCTSTR) szData), szData.GetLength() );

  }
  catch (CInternetException *e)
  {
    if(pszInfo) pszInfo->Format("MSInet: Error (%d) connecting to web server.", e->m_dwError);
    pcisInet->Close();
    delete pcisInet;
    delete pfile;
    return ERROR_MSINET_CONNECT;
  }


  count=1;
  while (count > 0)
  {
    try 
    {
      count = pfile->Read(buffer, (UINT) 1024);
    }
    catch (CInternetException *e)
    {
      if(pszInfo) pszInfo->Format("MSInet: Error (%d)  reading from web server.", e->m_dwError);
      nReturnCode=ERROR_MSINET_READ;
      count = -1;// escapes the while
    }

		if(count >= 0)
		{
			switch(nMode)
			{
			case 0:
				// add data to CString textData in a friendly way...
				nContentLength += count;
				p = pszTextData->GetBuffer( nContentLength );
				for (i=0; i<(int)count; i++) p[nIndex++] = buffer[i];
				pszTextData->ReleaseBuffer(nContentLength);  // Surplus memory released, p is now invalid.
				break;
			case 1:
				// write data to file
				fwrite(buffer,count,1,fp);
				break;
			}
		}
  }

  if (nMode == 1) fclose(fp);
  pfile->Close();
  pcisInet->Close();
  delete pcisInet;
  delete pfile;
  return nReturnCode;
}


int CNetworking::RetrieveURLInternals(int nMode, FILE* fp, 
                    CString URL, CString* pszTextData,  
                    BOOL bSecure, BOOL bCache, CString szHeaders, CString* pszInfo)
{
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		LogToFile("RetrieveURLInternals");
	}
#endif
  DWORD dwFlags, dwFlagsInet;
  int nReturnCode=RETURN_SUCCESS;
  char buffer[1026];
  unsigned long nContentLength=0, nIndex=0;
  LPTSTR p;

  CInternetSession* pcisInet;

  if (bCache)
  {
    dwFlagsInet = 0;
    dwFlags = INTERNET_FLAG_TRANSFER_BINARY; //INTERNET_FLAG_TRANSFER_ASCII 
  }
  else
  {
    dwFlagsInet = INTERNET_FLAG_DONT_CACHE;
    dwFlags = INTERNET_FLAG_DONT_CACHE | INTERNET_FLAG_RELOAD | INTERNET_FLAG_TRANSFER_BINARY; //INTERNET_FLAG_TRANSFER_ASCII 
  }
  if (bSecure) dwFlags |= INTERNET_FLAG_SECURE;


  *pszTextData = "";
  
  //(LPCTSTR pstrAgent = NULL, DWORD dwContext = 1, DWORD dwAccessType = INTERNET_OPEN_TYPE_PRECONFIG, LPCTSTR pstrProxyName = NULL, LPCTSTR pstrProxyBypass = NULL, DWORD dwFlags = 0 );
  
  //CInternetSession cisInet(NULL, 1, INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, INTERNET_FLAG_DONT_CACHE);
  pcisInet = new CInternetSession(NULL, 1, INTERNET_OPEN_TYPE_PRECONFIG, 
    NULL, NULL, dwFlagsInet );
  
  CHttpFile* pfile=NULL;
  UINT count, i;
	BOOL bError = FALSE;
  
  try
  {
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		CString szMsg;
		szMsg.Format("Opening URL %s",URL);
		LogToFile(szMsg);
	}
#endif
    if (szHeaders == "")
    {
      pfile =  (CHttpFile *) pcisInet->OpenURL( (LPCTSTR) URL, 1, dwFlags, NULL, 0);
    }
    else
    {
      pfile =  (CHttpFile *) pcisInet->OpenURL( (LPCTSTR) URL, 1, dwFlags, 
        (LPCTSTR) szHeaders, szHeaders.GetLength());
    }
  }
  catch (CInternetException *e)
  {
#ifdef USE_LOGGING
	if(m_bWriteFileErrors)	
	{
		CString szMsg;
		szMsg.Format("MSInet: Error (%d) connecting to web server.", e->m_dwError);
		LogToFile(szMsg);
	}
#endif
    if(pszInfo) pszInfo->Format("MSInet: Error (%d) connecting to web server.", e->m_dwError);
    pcisInet->Close();
    delete pcisInet;
    delete pfile;
    return ERROR_MSINET_CONNECT;
  }
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		LogToFile("Reading from web server...");
	}
#endif

  count=1;
  while ((count > 0)&&(!bError))
  {
    try 
    {
      count = pfile->Read(buffer, (UINT) 1024);
    }
    catch (CInternetException *e)
    {
#ifdef USE_LOGGING
	if(m_bWriteFileErrors)	
	{
		CString szMsg;
		szMsg.Format("MSInet: Error (%d)  reading from web server.", e->m_dwError);
		LogToFile(szMsg);
	}
#endif
      if(pszInfo) pszInfo->Format("MSInet: Error (%d)  reading from web server.", e->m_dwError);
      nReturnCode=ERROR_MSINET_READ;
      bError =	TRUE; //escapes the while
    }

		if((count >= 0) &&(!bError))
		{
			switch(nMode)
			{
			case 0:
				// add data to CString textData in a friendly way...
				nContentLength += count;
				p = pszTextData->GetBuffer( nContentLength );
				for (i=0; i<count; i++) p[nIndex++] = buffer[i];
				pszTextData->ReleaseBuffer(nContentLength);  // Surplus memory released, p is now invalid.
				break;
			case 1:
				// write data to file
				fwrite(buffer,count,1,fp);
				break;
			}
		}
  }
#ifdef USE_LOGGING
	if(m_bWriteFileOps)	
	{
		CString szMsg;
		szMsg.Format("Received %d bytes from web server.  Exit RetrieveURLInternals.", nContentLength);
		LogToFile(szMsg);
	}
#endif

  if (nMode == 1) fclose(fp);
  pfile->Close();
  pcisInet->Close();
  delete pcisInet;
  delete pfile;
  return nReturnCode;
}



/*
#ifdef OLDER_OBSOLETE
//  The following two methods are the main entry points:
BOOL CNetworking::retrieveURLToText(CString URL, CString *textData, 
                                    CString *error, BOOL bApache,
                                    BOOL bMicrosoft, BOOL bSecure)
{
  BOOL bRV;
  if (bApache)
  {
    return retrieveURLInternals(0, NULL, URL, textData, error, bApache, bMicrosoft, bSecure);
  }
  else
  {
    // try first time without Apache semantics
    bRV = retrieveURLInternals(0, NULL, URL, textData, error, FALSE, bMicrosoft, bSecure);
    if (error->Left(4) == "404:")
    {
      bRV = retrieveURLInternals(0, NULL, URL, textData, error, TRUE, bMicrosoft, bSecure);
    }
    return bRV;
  }
}

BOOL CNetworking::retrieveURLToFile(CString URL, CString tempfile, 
                                    CString *error, BOOL bApache,
                                    BOOL bMicrosoft, BOOL bSecure)
{
  FILE *fp;
  CString textData;

  fp=fopen((LPCTSTR) tempfile, "wb");
  if (fp == NULL) return FALSE;

//  return retrieveURLInternals(1, fp, URL, &textData, error, bApache);

  BOOL bRV;
  if (bApache)
    return retrieveURLInternals(1, fp, URL, &textData, error, bApache, bMicrosoft, bSecure);
  else
  {
    // try first time without Apache semantics
    bRV = retrieveURLInternals(1, fp, URL, &textData, error, FALSE, bMicrosoft, bSecure);
    if (error->Left(4) == "404:")
    {
      bRV = retrieveURLInternals(1, fp, URL, &textData, error, TRUE, bMicrosoft, bSecure);
    }
    return bRV;
  }
}
#endif
*/

/*
#ifdef OLDER_OBSOLETE
BOOL CNetworking::retrieveURLInternals(int nMode, FILE *fp, CString URL, 
                                       CString *textData, CString *error,
                                       BOOL bApacheMode, BOOL bMicrosoft,
                                       BOOL bSecure)
{
  if (!bMicrosoft)
    return retrieveURLInternalsVDI(nMode, fp, URL, textData, error, bApacheMode);
  else 
    return retrieveURLInternalsMicrosoft(nMode, fp, URL, textData, error, bSecure);
}
#endif



BOOL CNetworking::retrieveURLInternalsVDI(int nMode, FILE *fp, CString URL, 
                                          CString *textData, CString *error,
                                          BOOL bApacheMode)
{
  CString szTemp, szBuffer="", szCodebuf, err;
  DWORD dwServiceType;
  CString szServer, szObject, szCommand, szResponse = "";
  WORD nPort; // WORD, really
  SOCKET socket;
  int nCode=100, done=FALSE, okay=TRUE, rc, i;
  char ch, buffer[1026];
  BOOL bDoneWithHeaders, bFirstLine, bSocketError=FALSE;
  unsigned long tickCount = GetTickCount();

  *textData = "";

  AfxParseURL((LPCTSTR)URL, dwServiceType, szServer, szObject, nPort);
  // FYI:  dwServiceType should be one of:
  //#define INTERNET_SERVICE_FTP    1
  //#define INTERNET_SERVICE_GOPHER 2
  //#define INTERNET_SERVICE_HTTP   3

  if (!openConnection(GSTR(szServer), nPort, &socket, FALSE))
  {
    err.Format("Error connecting to server %s:%d (was looking for object %s using service type %d).",
      szServer, nPort, szObject, dwServiceType);
    *error = err;
    if (nMode == 1)  fclose(fp);
    return FALSE;
  }

  BOOL bAuthenticate=FALSE;
  // These should be passed in.
  CString user="username", pass="pass";

  if (!bAuthenticate)
  {
    if (!bApacheMode)
    {
      // send the last command with a terminating CRLF
      szCommand.Format("GET %s HTTP/1.0%c%c", szObject, 13,10);
      sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_CRLF);
    }
    else // extra headers
    {
      szCommand.Format("GET %s HTTP/1.0%c%c", szObject, 13,10);
      sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);
    
      szCommand.Format("Connection: Keep-Alive%c%c", 13,10);
      sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);
    

      // IE stuff
      //szCommand.Format("If-Modified-Since: Fri, 12 Apr 2002 20:48:35 GMT; length=1698%c%c", 13,10);
      //sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);
      // End IE stuff
      
      
      szCommand.Format("User-Agent: Mozilla/4.79 [en] (Windows NT 5.0; U)%c%c", 13,10);
      sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);
    
      szCommand.Format("Host: %s%c%c", szServer, 13,10);
      sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);
    
      szCommand.Format("Accept: *.*%c%c", 13,10);
      sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);
    
      szCommand.Format("Accept-Encoding: %c%c",  13,10); // gzip
      sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);
    
      szCommand.Format("Accept-Language: en%c%c", 13,10);
      sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);
    
      szCommand.Format("Accept-Charset: iso-8859-1,*,utf-8%c%c", 13,10);
      sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_CRLF);  
    }
  }
  else  // authentication
  {
    szCommand.Format("GET %s HTTP/1.1%c%c", szObject, 13,10);
    sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);
  
    // needed this for an Apache 1.3 server (MMUSA)
    szCommand.Format("Connection: Keep-Alive%c%c", 13,10);
    sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);
  
    szCommand.Format("User-Agent: Mozilla/4.79 [en] (Windows NT 5.0; U)%c%c", 13,10);
    sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);
  
    szCommand.Format("Host: %s%c%c", szServer, 13,10);
    sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);
  
    szCommand.Format("Accept: *.*%c%c", 13,10);
    sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);
  
    szCommand.Format("Accept-Encoding: %c%c",  13,10); // gzip
    sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);
  
    szCommand.Format("Accept-Language: en%c%c", 13,10);
    sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);
  
    szCommand.Format("Accept-Charset: iso-8859-1,*,utf-8%c%c", 13,10);
    sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_NONE);  
  
    // send the last command with a terminating CRLF
    szCommand.Format("Authorization: Basic %s%c%c", base64Encode(user+":"+pass), 13,10);
    sendCommand(GSTR(szCommand), socket, FALSE, EOLN1_CRLF);
  } 

  bDoneWithHeaders = FALSE;  bFirstLine = TRUE;

  while (!done)
  {
    ch = sgetc(socket, &okay, __LINE__);

    if (!okay)
    {
      bSocketError = TRUE;
      done = TRUE; break;
    }
    else
    {
      if (!bDoneWithHeaders)
      {
        szBuffer += ch;
        if (ch == '\n')
        {
          szTemp = szBuffer; szTemp.TrimRight(); szTemp.TrimLeft();
          if ((szTemp == "") && (!bDoneWithHeaders))
          {
            bDoneWithHeaders = TRUE;
            //szResponse = "";
            done = TRUE;
            break;
          }

          if ((!bDoneWithHeaders) && (szTemp.Left(5) == "HTTP/"))
          {
            // read code past HTTP/1.x -

            CString szCodebuf = szBuffer.Right(szBuffer.GetLength() - 8);
            nCode = atol((LPCTSTR) szCodebuf);

            //szTemp.Format("Response code: %d\ncodebuf=%s", nCode,szCodebuf); AfxMessageBox(szTemp);
          }
          szResponse += szBuffer;
          szBuffer = ""; // reset response buffer
        }
      }
    }
  }

  //AfxMessageBox(szResponse);
  szResponse = "";

  if (bSocketError)
  {
    closeConnection(GSTR(szServer), 80, socket);
    //g_cLog.logErrors("Got a socket error trying to get data.");
    err.Format("Socket error (1) connecting to server %s:%d (looking for object %s using service type %d).",
      szServer, nPort, szObject, dwServiceType);
    *error = err;    
    if (nMode == 1)  fclose(fp);
    return FALSE;
  }

  //AfxMessageBox(szResponse);

  // download it
  done = FALSE;
  unsigned long nContentLength=0, nIndex=0;
  
  while (!done)
  {
    // recv() returns the number of bytes read (1), 0 if the
    // socket was closed gracefully, or SOCKET_ERROR otherwise
    rc = recv(socket, (char *) buffer, 1024, 0);
    switch(rc)
    {
    case 0: 
      done = TRUE; 
      break;
    case SOCKET_ERROR:
      done = TRUE;
      //bSocketError = TRUE;
      break;
    default:
      if (nMode == 0)
      {
        // FUTURE:  this is very inefficient for large files!
        // See buffer allocation details from NOC file project.
#ifdef SLOW_BUT_WORKS
        for (i=0; i<rc; i++)
          if (buffer[i] != 0) 
            (*textData) += (char) buffer[i];
#else
        nContentLength += rc;
        LPTSTR p = textData->GetBuffer( nContentLength );
        for (i=0; i<rc; i++)
          p[nIndex++] = buffer[i];
        textData->ReleaseBuffer(nContentLength);  // Surplus memory released, p is now invalid.
#endif

        //AfxMessageBox(*textData);
      }
      else 
      {
        fwrite(buffer, rc, 1, fp);
        fflush(fp);
        //AfxMessageBox(buffer);
      }
      break;
    }
  }

  if (nMode == 1)  fclose(fp);

  if (bSocketError)
  {
    closeConnection(GSTR(szServer), 80, socket);
    //g_cLog.logErrors("Got a socket error trying to get data.");
    err.Format("Socket error (2) connecting to server %s:%d (looking for object %s using service type %d).",
      szServer, nPort, szObject, dwServiceType);
    *error = err;    
    return FALSE;
  }


  closeConnection(GSTR(szServer), 80, socket);

  //g_cLog.logErrors("Retrieval of data took "+ formatTicks(GetTickCount() - tickCount));
  //g_cLog.logErrors("Data was: "+ *textData);


  if (HTTP_RETURN_SUCCESS(nCode))
  {
    //err.Format("%d: Command sent successfully (Retrieval of data took %s)", nCode, formatTicks(GetTickCount() - tickCount));
    err.Format("%d: Command sent successfully (Retrieval of data took %s; connecting to server %s:%d, looking for object %s using service type %d).",
      nCode, cTU.formatTicks(GetTickCount() - tickCount), szServer, nPort, szObject, dwServiceType);
    *error = err;
    return TRUE;
  }

  //err.Format("%d: Command sent successfully, but server returned error code.", nCode);
  err.Format("%d: Command sent successfully, but server returned error code.  (Connecting to server %s:%d, looking for object %s using service type %d).",
    nCode, szServer, nPort, szObject, dwServiceType);
  *error = err;    
  return FALSE;
}
*/
