// (c) 2000-Present Video Design Interactive.  All Rights Reserved.
// This file constitutes the proprietary, confidential intellectual 
// property of Video Design Interactive and may not be distributed.

/* Duet.h */

#ifndef DUET_API_INCLUDED
#define DUET_API_INCLUDED

#ifndef _CALDIR_H
#include "C:\Source\Common\API\Chyron\CAL\LIB\caldir.h"
#include "C:\Source\Common\API\Chyron\CAL\LIB\callib.h"
#endif

#include "networking.h"

#define VIDEO_HW_UNDEFINED  0
#define VIDEO_HW_DUET_LEX   1
#define VIDEO_HW_DUET_SD    2
#define VIDEO_HW_VGA        3


//#define DEBUG
#undef DEBUG


// From Frank
typedef struct {
  int iThisStructSize, iEndHeight, iEndHeight2, iEndWidth, iEndWidth2, 
    iEndingX, iEndingX2, iEndingY, iEndingY2;
  int iFields, iFields2, iStartHeight, iStartHeight2, iStartWidth, 
    iStartWidth2, iStartX, iStartX2, iStartY, iStartY2, iDisplayOn, 
    iDisplayOn2;
  int iEase, iEase2, iFadeOn, iFadeOn2, iClipTop, iClipTop2, 
    iClipBottom, iClipBottom2, iClipLeft, iClipLeft2, iClipRight, 
    iClipRight2;
  int iInterpolate, iGraphicsOnTop, iResizer2Over1;
  int iBackGroundON, iGraphicPlaneOn, iResizer1On, iResizer2On;
  int iBkg_Frame_Delay_On, iMsg, iDissolveEffect;
  int iGPI1;    // Forward GPI Number
  int iGPI2;    // Reverse GPI Number
  int bSlotActive;// Squeeze Effect Active Flag.
  char szName[10][21];  // user names for effects
  int unused[100];
} SqueezeEffect;




#ifdef DEBUG
#define DuetCheck(FUNC, LNUM) \
	if(FAILED(FUNC)) {\
    CString szTemp666; szTemp666.Format("%s: CAL Error: %x, line %d", __FILE__, (HRESULT) FUNC, LNUM); AfxMessageBox(szTemp666); \
  throw(FUNC); }
    // unused, in if statement above
    //console(LNUM, __FILE__, MESSAGE_CRITICAL, "CAL Error!   "); 
    //CString szTemp666; szTemp666.Format("%s: CAL Error: %x, line %d", __FILE__, (HRESULT) FUNC, LNUM); AfxMessageBox(szTemp666); 
#else
#define DuetCheck(FUNC, LNUM) \
	if(FAILED(FUNC)) {\
  throw(FUNC); }
    // unused, in if statement above
    //console(LNUM, __FILE__, MESSAGE_CRITICAL, "CAL Error!   "); 
    //CString szTemp666; szTemp666.Format("%s: CAL Error: %x, line %d", __FILE__, (HRESULT) FUNC, LNUM); AfxMessageBox(szTemp666);  
#endif


// used by other apps
#define CheckCAL(FUNC, LNUM) \
  { \
    CString szTemp666; HRESULT hr = FUNC; \
    if(FAILED(hr))\
    { \
      szTemp666.Format("%s: CAL Error: %s, line %d", __FILE__, (LPCTSTR)g_duetAPI.printCALError(hr), LNUM);\
      g_cLog.logErrors(szTemp666, FALSE, FALSE); \
      throw(FUNC); \
    } \
  }

#ifdef USEMDRELAY

#define CheckGDM(FUNC, LNUM) \
  { \
    CString szTemp666; HRESULT hr = FUNC; \
    if(FAILED(hr))\
    { \
      if (g_pduet != NULL) { \
      szTemp666.Format("%s: CAL Error: %s, line %d", __FILE__, (LPCTSTR)g_pduet->printCALError(hr), LNUM);\
  /*    g_mdr.DM(0,0,0, "%s|CheckGDM", szTemp666); */  \
      }\
      throw(FUNC); \
    } \
  }
#else

#define CheckGDM(FUNC, LNUM) \
  { \
    CString szTemp666; HRESULT hr = FUNC; \
    if(FAILED(hr))\
    { \
      if (g_pduet != NULL) { \
      szTemp666.Format("%s: CAL Error: %s, line %d", __FILE__, (LPCTSTR)g_pduet->printCALError(hr), LNUM);\
      /*g_md.DM(0,0,0, "%s|CheckGDM", szTemp666);*/ \
      }\
      throw(FUNC); \
    } \
  }

#define CheckAndReturnGDM(FUNC, LNUM, FAIL) \
  { \
    CString szTemp666; HRESULT hr = FUNC; \
    if(FAILED(hr))\
    { \
      if (g_pduet != NULL) { \
      szTemp666.Format("%s: CAL Error: %s, line %d", __FILE__, (LPCTSTR)g_pduet->printCALError(hr), LNUM);\
      g_md.DM(0,0,0, "%s|CheckGDM", szTemp666); \
			if(FAIL!=NULL) *FAIL = TRUE;\
      }\
      throw(FUNC); \
    } \
		else if(FAIL!=NULL) *FAIL = FALSE;\
  }

#endif


class CDuetAPI // : public CObject
{
public:
	CDuetAPI();   // standard constructor
	~CDuetAPI();   // standard destructor

public:
  BOOL m_bUseDuet, m_bConnected;
  int m_nDuetVideoFormat; // ecalFormatNTSC, ecalFormatPAL, ecalFormat720P, ecalFormat1080I
  CString m_szDuetAddress;
  HCAL m_connId;
  CNetworking m_net;
  
  // Hardware info:
  int m_nVideoHardwareType;
  double m_fCALVersion;
  DWORD m_dwCALBuild;

  HCAL m_Mixer, m_DuetSqueeze, m_CodiSqueeze;
  int m_nCodiSqueezeBoardNumber;

  int     connect(CString szDuetAddress, BOOL bUseDuet, int nDuetVideoFormat=ecalFormatNTSC, CString* pszInfo=NULL);
  int     disconnect(CString* pszInfo=NULL);
  int     shutdown(CString* pszInfo=NULL);
  BOOL    getVideoDimensions(int *nWidth, int *nHeight);
  int     fileExists(char *fn);
  void    RGB2HSV(double r, double g, double b, double *h, double *s, double *v);
  void    HSV2RGB(double h, double s, double v, double *r, double *g, double *b);
  void    setBGBright(double *hout, double *sout, double *vout);
  CString printCALError(unsigned long errcode);
  void    Check2(HRESULT error, int line);
  BOOL    mapFontString(CString szFontString, CString *szFont, int *PointSize, int *nStyle, COLORREF *crColor);
  BOOL    getDuetInfo(int *nHardwareType, double *fCALVersion, DWORD *dwBuild);

  // CAL Utils
  CString CALAppendEvent(CString szString, CString szEventString);
  CString CALEncodeBrackets(CString szString);
  BOOL GetFieldHandle(HCAL rgnScene, CString szRgnName, HCAL *rgnHandle);
  BOOL GetProjectedVertices(const char *pSceneName, 
                  const char *pRgnName,
                  double *vrtX0, double *vrtY0, 
                  double *vrtX1, double *vrtY1);


  // Main squeezeback entry points
  void SetupSqueezeback();
  void ShutdownSqueezeback();
  void DoSqueezeIn(int nSqueezeFrames,
                   int nSqueezeX, int nSqueezeY,
                   int nSqueezeW, int nSqueezeH,
                   int nXCorrection, int nYCorrection,
                   int nLayerCAL, int nLayerSqueeze);
  void DoSqueezeOut(int nSqueezeFrames,
                    int nSqueezeX, int nSqueezeY,
                    int nSqueezeW, int nSqueezeH,
                    int nXCorrection, int nYCorrection);

  // Duet Squeezeback
  void SetVideoSync(long nSync);
  void SetVideoMixing(BOOL bMix);
  void SetupSqueezebackDuet();
  void ShutdownSqueezebackDuet();
  void Mix(BOOL bEnable, int nLayerCAL, int nLayerSqueeze);
  void MixDefault(BOOL bEnable);
  void DoSqueezeInDuet(int nSqueezeFrames, int nSqueezeX, int nSqueezeY,
                   int nSqueezeW, int nSqueezeH, int nXCorrection, int nYCorrection,
                   int nLayerCAL, int nLayerSqueeze);
  void DoSqueezeOutDuet(int nSqueezeFrames, int nSqueezeX, int nSqueezeY,
                    int nSqueezeW, int nSqueezeH, int nXCorrection, int nYCorrection);

  // Codi Squeezeback
  void SetupSqueezebackCodi();
  void ShutdownSqueezebackCodi();
  void DoSqueezeInCodi(int nSqueezeFrames, int nSqueezeX, int nSqueezeY,
                   int nSqueezeW, int nSqueezeH, int nXCorrection, int nYCorrection,
                   int nLayerCAL, int nLayerSqueeze);
  void DoSqueezeOutCodi(int nSqueezeFrames, int nSqueezeX, int nSqueezeY,
                    int nSqueezeW, int nSqueezeH, int nXCorrection, int nYCorrection);
  CString BuildCodiCommand(int vEffectId, 
                           int vRegion, int StartX, int EndX,
                           int StartY, int EndY, int StartWidth, 
                           int EndWidth,
                           int StartHeight, int EndHeight, int Frames,
                           int Ease, int FadeOn, int ClipTop, 
                           int ClipBottom, int ClipLeft, int ClipRight,
                           int GraphicsPlaneOn, int BackgroundOn, 
                           int ResizerOn, int GraphicsOnTop,
                           int Resizer2Over1, 
                           int BackgroundFrameDelay, 
                           int DissolveEffect);

/*
  void setupDuetCrawl(passDuet_t *settings);
  void doDuetCrawl(passDuet_t *settings);
  void loadDuetCrawl(CString text, int first, passDuet_t *settings);
  void doLowerThird(CString line1, CString line2, CString line3, int numLines,
                    char duetFontName[32], int duetFontColor, int duetFontStyle, 
                    int duetFontSize, int x, int y, int w, int h, int bp, 
                    char bpName[]);
  void initializeDuetSettings(passDuet_t *settings,
                              int useduet, int wx, int wy, int ww, int wh,
                              int fsize, int fstyle, int vfmt, int speed,
                              unsigned long color, CString address,
                              CString face);
  void doLowerThirdDS(passDuet_t settings, CString line1, CString line2, CString line3, int numLines);
  // need this too...
  void console(int lineNumber, char source[], int color, char fmt_string[], ... );
 */
};


#endif