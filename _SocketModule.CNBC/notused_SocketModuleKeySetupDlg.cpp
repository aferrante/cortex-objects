// SocketModuleKeySetupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SocketModule.h"
#include "SocketModuleMain.h"
#include "SocketModuleKeySetupDlg.h"

extern CSocketModuleMain* g_psocketmodule;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSocketModuleKeySetupDlg dialog


CSocketModuleKeySetupDlg::CSocketModuleKeySetupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSocketModuleKeySetupDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSocketModuleKeySetupDlg)
	m_bActive = TRUE;
	m_bAlt = FALSE;
	m_bAppFocus = FALSE;
	m_bCtrl = FALSE;
	m_bShift = FALSE;
	m_szKey = _T("");
	//}}AFX_DATA_INIT
}


void CSocketModuleKeySetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSocketModuleKeySetupDlg)
	DDX_Check(pDX, IDC_CHECK_ACTIVE, m_bActive);
	DDX_Check(pDX, IDC_CHECK_ALT, m_bAlt);
	DDX_Check(pDX, IDC_CHECK_APPFOCUS, m_bAppFocus);
	DDX_Check(pDX, IDC_CHECK_CTRL, m_bCtrl);
	DDX_Check(pDX, IDC_CHECK_SHIFT, m_bShift);
	DDX_CBString(pDX, IDC_COMBO_KEYS, m_szKey);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSocketModuleKeySetupDlg, CDialog)
	//{{AFX_MSG_MAP(CSocketModuleKeySetupDlg)
	ON_BN_CLICKED(IDC_BUTTON_HELP, OnButtonHelp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSocketModuleKeySetupDlg message handlers

void CSocketModuleKeySetupDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);

	CDialog::OnOK();
}

void CSocketModuleKeySetupDlg::OnButtonHelp() 
{
	// TODO: Add your control notification handler code here
/*
	if((g_psocketmodule->m_settings.m_pszKeySetupHelp)&&(strlen(g_psocketmodule->m_settings.m_pszKeySetupHelp)>0))
	{
		MessageBox(g_psocketmodule->m_settings.m_pszKeySetupHelp,"Key Setup Help");
	}
	else
	{
		MessageBox("help text","Key Setup Help");
	}
*/
}

BOOL CSocketModuleKeySetupDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
/*
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "F1");//#define VK_F1             0x70
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "F2");//#define VK_F2             0x71
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "F3");//#define VK_F3             0x72
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "F4");//#define VK_F4             0x73
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "F5");//#define VK_F5             0x74
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "F6");//#define VK_F6             0x75
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "F7");//#define VK_F7             0x76
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "F8");//#define VK_F8             0x77
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "F9");//#define VK_F9             0x78
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "F10");//#define VK_F10            0x79
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "F11");//#define VK_F11            0x7A
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "F12");//#define VK_F12            0x7B
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "0");  //0x30
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "1");  //0x31
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "2");  //0x32
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "3");  //0x33
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "4");  //0x34
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "5");  //0x35
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "6");  //0x36
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "7");  //0x37
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "8");  //0x38
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "9");  //0x39
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "A");  //0x41
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "B");  //0x42
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "C");  //0x43
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "D");  //0x44
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "E");  //0x45
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "F");  //0x46
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "G");  //0x47
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "H");  //0x48
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "I");  //0x49
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "J");  //0x4a
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "K");  //0x4b
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "L");  //0x4c
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "M");  //0x4d
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "N");  //0x4e
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "O");  //0x4f
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "P");  //0x50
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "Q");  //0x51
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "R");  //0x52
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "S");  //0x53
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "T");  //0x54
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "U");  //0x55
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "V");  //0x56
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "W");  //0x57
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "X");  //0x58
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "Y");  //0x59
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "Z");  //0x5a
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "`");  //0xc0
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "-");  //0xbd
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "=");  //0xbb
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "[");  //0xdb
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "]");  //0xdd
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "\\");  //0xdc
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "/");  //0xbf
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "'");  //0xde
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, ";");  //0xba
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, ".");  //0xbe
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, ",");  //0xbc
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "Space");  //VK_SPACE          0x20
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "PrtScrn");  //VK_SNAPSHOT       0x2C
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "ScrLock");  //VK_SCROLL         0x91
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumLock");  //VK_NUMLOCK        0x90
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "Pause");  //VK_PAUSE          0x13
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "Insert"); //VK_INSERT         0x2D
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "Delete"); //VK_DELETE         0x2E
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "Home");   //VK_HOME           0x24
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "End");    //VK_END            0x23
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "PageUp"); //VK_PRIOR          0x21
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "PageDn"); //VK_NEXT           0x22
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "Bksp");   //VK_BACK           0x08
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "Up");     //VK_UP             0x26
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "Down");   //VK_DOWN           0x28
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "Left");   //VK_LEFT           0x25
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "Right");  //VK_RIGHT          0x27
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "Tab");    //VK_TAB            0x09
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumPad 0");//VK_NUMPAD0        0x60
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumPad 1");//VK_NUMPAD1        0x61
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumPad 2");//VK_NUMPAD2        0x62
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumPad 3");//VK_NUMPAD3        0x63
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumPad 4");//VK_NUMPAD4        0x64
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumPad 5");//VK_NUMPAD5        0x65
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumPad 6");//VK_NUMPAD6        0x66
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumPad 7");//VK_NUMPAD7        0x67
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumPad 8");//VK_NUMPAD8        0x68
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumPad 9");//VK_NUMPAD9        0x69
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumPad .");//VK_DECIMAL        0x6E
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumPad +");//VK_ADD            0x6B
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumPad -");//VK_SUBTRACT       0x6D
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumPad *");//VK_MULTIPLY       0x6A
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "NumPad /");//VK_DIVIDE         0x6F
	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->InsertString(-1, "Enter");//VK_RETURN	0x0D


	((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->SetCurSel(((CComboBox*)(GetDlgItem(IDC_COMBO_KEYS)))->FindStringExact(-1, m_szKey));
*/
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
