// SocketModuleSettings.cpp: implementation of the CSocketModuleSettings.
//
//////////////////////////////////////////////////////////////////////
#include <stdafx.h>  // for socketmodule debug
#include "SocketModuleMain.h"   // for socketmodule debug
#include "SocketModuleDefines.h"
#include "SocketModuleSettings.h"
#include "..\Tabulator\TabulatorDefines.h"
//#include "..\Radiance\RadianceDefines.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CSocketModuleMain* g_psocketmodule;
extern CSocketModuleApp theApp;



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSocketModuleSettings::CSocketModuleSettings()
{
	m_pszName = NULL;
	m_pszProgName = NULL;
	m_ulMainMode = SOCKETMODULE_MODE_DEFAULT;

	m_ulDebug = 0;
	// ports
	m_usCommandPort	= SOCKETMODULE_PORT_CMD;
	m_usStatusPort	= SOCKETMODULE_PORT_STATUS;

//	m_pszTabulatorHost = NULL;	// the name of the cortex host
//	m_usTabulatorCommandPort = TABULATOR_PORT_CMD;
//	m_pszPluginName = NULL;
//	m_pszUserToken = NULL;
//	m_pszKeySetupHelp = NULL;

	m_nAsRunRecent = 10;
//	m_nCommRetryMS = 5000;


	// messaging for SocketModule
	m_bUseLog = true;			// write a log file
	m_bUseAsRunLog = true;
	m_bUseJSONLog = true;
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host
	m_pszFileSpec = NULL;
	m_pszMailSpec = NULL;
	m_pszProcessedFileSpec = NULL;
	m_pszProcessedMailSpec = NULL;
	m_pszAsRunFileSpec = NULL;
	m_pszProcessedAsRunFileSpec = NULL;
	m_pszJSONFileSpec = NULL;
	m_pszProcessedJSONFileSpec = NULL;

//	m_bUseXMLClientLog = false;

	m_bReportSuccessfulOperation=false;

//	m_nTransferFailureRetryTime = 60;  // seconds - one minute as a default
	m_pszLicense=NULL;  // the License Key
	m_pszOEMcodes=NULL;  // the possible OEM string
	m_pszIconPath=NULL;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

	m_nSocketInactivityTimeout = 30; 
	m_ulLastUID = 1; 
	m_bCleanShutdown = false;

//	m_nInterVANCdelayMS=0;
	m_nHistoryItems = 16;
	m_nAsyncMessageItems=10;  // how many to hold in the buffer.


  // inserter settings
	m_pszVANCIP=NULL;  // IP address of vanc inserter
	m_nVANCport=5167;  // port of vanc inserter
	m_nDefaultSendTimeoutMS=5000;
	m_nDefaultReceiveTimeoutMS=5000;
	m_nVANCCommandDelay=0;
//	m_ulMPUID = 0xfffffff0;  //CNBC special
	m_pszMPUID = NULL;
	m_nCommandRepetitions = 0;
	m_bDF = true; // not used, time type in this spec is not 2.


	// Logger settings
	m_nBaud = 57600;
	m_nCom = 1;
	m_nParity = 0;
	m_nStopBits = 0;
	m_nByteSize = 8;
	m_nConnRetryIntervalMS = 10000;
	m_nConnTimeoutMS = 5000;

}


CSocketModuleSettings::~CSocketModuleSettings()
{

	if(m_pszName) free(m_pszName); // must use malloc to allocate

//	if(m_pszTabulatorHost) free(m_pszTabulatorHost); // must use malloc to allocate
//	if(m_pszPluginName) free(m_pszPluginName); // must use malloc to allocate
//	if(m_pszUserToken) free(m_pszUserToken); // must use malloc to allocate
//	if(m_pszKeySetupHelp) free(m_pszKeySetupHelp); // must use malloc to allocate

	
	if(m_pszLicense) free(m_pszLicense); // must use malloc to allocate
	if(m_pszOEMcodes) free(m_pszOEMcodes); // must use malloc to allocate
	if(m_pszIconPath) free(m_pszIconPath); // must use malloc to allocate	
	if(m_pszFileSpec) free(m_pszFileSpec); // must use malloc to allocate	
	if(m_pszMailSpec) free(m_pszMailSpec); // must use malloc to allocate	
	if(m_pszProcessedFileSpec) free(m_pszProcessedFileSpec); // must use malloc to allocate	
	if(m_pszProcessedMailSpec) free(m_pszProcessedMailSpec); // must use malloc to allocate	
	if(m_pszJSONFileSpec) free(m_pszJSONFileSpec); // must use malloc to allocate	
	if(m_pszProcessedJSONFileSpec) free(m_pszProcessedJSONFileSpec); // must use malloc to allocate	


}
/*
int CSocketModuleSettings::ReadKeys()
{
	char pszFilename[MAX_PATH];
	CFileUtil file;
	// get settings.
	strcpy(pszFilename, "");
//		AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, SOCKETMODULE_SETTINGS_FILE_DEFAULT);  // cortex settings file

	file.GetSettings(pszFilename, false); 

if(0)//g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_TRIGGER) 	
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "ReadKeys() %d %d", (file.m_ulStatus&FILEUTIL_MALLOC_OK), g_psocketmodule);  //(Dispatch message)
}

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(g_psocketmodule)
		{
			EnterCriticalSection(&g_psocketmodule->m_data.m_critMaps);

			if(g_psocketmodule->m_data.m_ppMappingObj)
			{
				int i=0;
				while(i<g_psocketmodule->m_data.m_nNumMappingObjects)
				{
					if(g_psocketmodule->m_data.m_ppMappingObj[i]) delete g_psocketmodule->m_data.m_ppMappingObj[i]; // delete objects, must use new to allocate
					i++;
				}
				delete [] g_psocketmodule->m_data.m_ppMappingObj; // delete array of pointers to objects, must use new to allocate
			}
			g_psocketmodule->m_data.m_ppMappingObj = NULL;
			g_psocketmodule->m_data.m_nNumMappingObjects = file.GetIniInt("Keys", "NumKeys", 0);


if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_TRIGGER) 	
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "reading %d keys", g_psocketmodule->m_data.m_nNumMappingObjects);  //(Dispatch message)
}


			if(g_psocketmodule->m_data.m_nNumMappingObjects>0)
			{
				g_psocketmodule->m_data.m_ppMappingObj = new CSocketModuleMappingObject*[g_psocketmodule->m_data.m_nNumMappingObjects];

				if(g_psocketmodule->m_data.m_ppMappingObj)
				{
					int i=0;
					while(i<g_psocketmodule->m_data.m_nNumMappingObjects)
					{
						g_psocketmodule->m_data.m_ppMappingObj[i] = new CSocketModuleMappingObject;
						if(g_psocketmodule->m_data.m_ppMappingObj[i])
						{
							char szText[256];
							sprintf(szText, "Key_%03d_active", i);
							g_psocketmodule->m_data.m_ppMappingObj[i]->m_bActive = file.GetIniInt("Keys", szText, 1)?true:false;
							sprintf(szText, "Key_%03d_appfocus", i);
							g_psocketmodule->m_data.m_ppMappingObj[i]->m_bRequireAppFocus = file.GetIniInt("Keys", szText, 1)?true:false;
							sprintf(szText, "Key_%03d_code", i);
							g_psocketmodule->m_data.m_ppMappingObj[i]->m_nKeyCode = file.GetIniInt("Keys", szText, -1);
							sprintf(szText, "Key_%03d_syschars", i);
							g_psocketmodule->m_data.m_ppMappingObj[i]->m_nSysChars = file.GetIniInt("Keys", szText, -1);

if(0)//g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_TRIGGER) 	
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "read key %d", i);  //(Dispatch message)
}

							if(!g_psocketmodule->m_data.m_ppMappingObj[i]->m_bRequireAppFocus)
							{	
								if(RegisterHotKey(NULL, i, g_psocketmodule->m_data.m_ppMappingObj[i]->m_nSysChars, g_psocketmodule->m_data.m_ppMappingObj[i]->m_nKeyCode)==0)
								{	
									g_psocketmodule->m_data.m_ppMappingObj[i]->m_bFailed=true; 
	g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, "SocketModule", "Failure to register hotkey: identifier %d, code 0x%02x modifiers 0x%02x.", i, g_psocketmodule->m_data.m_ppMappingObj[i]->m_nKeyCode, g_psocketmodule->m_data.m_ppMappingObj[i]->m_nSysChars);  //(Dispatch message)
								}
								else
								{
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_TRIGGER) 	
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "Registered hotkey: identifier %d, code 0x%02x modifiers 0x%02x.", i, g_psocketmodule->m_data.m_ppMappingObj[i]->m_nKeyCode, g_psocketmodule->m_data.m_ppMappingObj[i]->m_nSysChars);  //(Dispatch message)
}

									g_psocketmodule->m_data.m_ppMappingObj[i]->m_bFailed=false; 
								}
							}
						}
						i++;
					}

				}
			}


			LeaveCriticalSection(&g_psocketmodule->m_data.m_critMaps);
		}


	
		return SOCKETMODULE_SUCCESS;
	}
	return SOCKETMODULE_ERROR;
}
*/


int CSocketModuleSettings::SaveLastUID()
{
	char pszFilename[MAX_PATH];
	CFileUtil file;
	// get settings.
	strcpy(pszFilename, "");
//		AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, SOCKETMODULE_SETTINGS_FILE_DEFAULT);  // cortex settings file

	file.GetSettings(pszFilename, false); 

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		file.SetIniInt("Main", "LastUID", m_ulLastUID);
		file.SetSettings(pszFilename, false);  // have to have correct filename

		return SOCKETMODULE_SUCCESS;
	}
	return SOCKETMODULE_ERROR;
}

int CSocketModuleSettings::Settings(bool bRead)
{
	char pszFilename[MAX_PATH];
	CFileUtil file;
	// get settings.
	strcpy(pszFilename, "");
//		AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, SOCKETMODULE_SETTINGS_FILE_DEFAULT);  // cortex settings file

	file.GetSettings(pszFilename, false); 

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
		// load up the values on the settings object
			m_pszName = file.GetIniString("Main", "Name", "SocketModule", m_pszName);

			// programmatic name.
			m_pszProgName = file.GetIniString("Main", "ProgName", "", m_pszProgName);
			
			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);
			m_pszOEMcodes = file.GetIniString("License", "COM", "C1JL-sPLisQ5VCQqCR2LJwJPdlTK", m_pszOEMcodes);
			m_ulDebug = file.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.

			// compile license key params
			if(g_psocketmodule->m_data.m_key.m_pszLicenseString) free(g_psocketmodule->m_data.m_key.m_pszLicenseString);
			g_psocketmodule->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(m_pszLicense)+1);
			if(g_psocketmodule->m_data.m_key.m_pszLicenseString)
			sprintf(g_psocketmodule->m_data.m_key.m_pszLicenseString, "%s", m_pszLicense);

			g_psocketmodule->m_data.m_key.InterpretKey();

			char errorstring[MAX_MESSAGE_LENGTH];
			if(g_psocketmodule->m_data.m_key.m_bValid)
			{
				unsigned long i=0;
				while(i<g_psocketmodule->m_data.m_key.m_ulNumParams)
				{
					if((g_psocketmodule->m_data.m_key.m_ppszParams)
						&&(g_psocketmodule->m_data.m_key.m_ppszValues)
						&&(g_psocketmodule->m_data.m_key.m_ppszParams[i])
						&&(g_psocketmodule->m_data.m_key.m_ppszValues[i]))
					{
						if(stricmp(g_psocketmodule->m_data.m_key.m_ppszParams[i], "max")==0)
						{
	//						g_psocketmodule->m_data.m_nMaxLicensedDevices = atoi(g_psocketmodule->m_data.m_key.m_ppszValues[i]);
						}
						else
						if(stricmp(g_psocketmodule->m_data.m_key.m_ppszParams[i], "oem")==0)
						{
							// if it exists, check OEM string.

						// for OEM partner check on license key, need oem=xxxr in the params'
//oem=xxxr where xxx is a client code string, and r is any random ASCII character in the range 33 to 126, excluding URL-sensitive characters (34, 38, 39, 46, 47, 60, 61, 62, 63).
//the oem parameter should be the last thing in the string, and the last random character allows us to keep the encoding more secure.
//the client codes for OEM suppliers is as follows (note these ARE case sensitive):

//Strategy and Technology (S&T): SnT
//Harris: HAS
//Softel: SFT
//Chyron: Chy
//Ensequence: Ens
//VDS: VDS (for test purposes)

//If an OEM code is specified in the license key, a supplier code table including these values must exist in the CSF file, for it to match on. 
//This will be stored in a CSF parameter under the [License] heading, called COM for Cortex OEM Management (haha).

//[License]
//COM=C1JL-sPLisQ5VCQqCR2LJwJPdlTK

//the above string includes all of the above client codes.  The applications will do a check on the license key against the allowed suppliers in this code, which by default will be included and created by the applications.  As the approved vendor list changes, new versions will create a new default code, but for interim installations, the code can be updated in the CSF file.

// AND the way the string is generated is as follows:
// take this string:
//							VDSHAS|ChySnT|SFTEns
// which is delimited pairs of 3 letter codes (do in pairs so we dont have delimited 4 char, or naked 3 char regular)
//							base 64 endcode using the license key alpha and padch
//#define LICENSE_B64ALPHA		"XhIWsP4RtOoSTQpuvJ-LBCnGrAyd8eVi7%Dz1wkl5xMj6NqZa2YHbc9f0+Eg3UFm"
//#define LICENSE_B64PADCH		'K'
// and there you go.

							if((m_pszOEMcodes)&&(strlen(m_pszOEMcodes)>3))
							{
								CBufferUtil bu;
								char* pszCodes = m_pszOEMcodes;
								unsigned long ulBufLen = strlen(m_pszOEMcodes);
								bu.Base64Decode(&pszCodes, &ulBufLen, false, LICENSE_B64ALPHA, LICENSE_B64PADCH);

								CSafeBufferUtil sbu;
								char* pchCodes = sbu.Token(pszCodes, strlen(pszCodes), "|");
								g_psocketmodule->m_data.m_key.m_bValid = false;
								while(pchCodes)
								{
									if(strncmp(g_psocketmodule->m_data.m_key.m_ppszValues[i], pchCodes, 3)==0)
									{
										g_psocketmodule->m_data.m_key.m_bValid = true;
										break;
									}
									else
									if((strlen(pchCodes)>3)&&(strncmp(g_psocketmodule->m_data.m_key.m_ppszValues[i], pchCodes+3, 3)==0))
									{
										g_psocketmodule->m_data.m_key.m_bValid = true;
										break;
									}
									pchCodes = sbu.Token(NULL, NULL, "|");
								}
								if(pszCodes)
								{
									try{free(pszCodes);} catch(...){}
								}
							}
							else g_psocketmodule->m_data.m_key.m_bValid = false;
						}
					}
					i++;
				}

				if(
					  (g_psocketmodule->m_data.m_key.m_bValid)
					&&(
							(!g_psocketmodule->m_data.m_key.m_bExpires)
						||((g_psocketmodule->m_data.m_key.m_bExpires)&&(!g_psocketmodule->m_data.m_key.m_bExpired))
						||((g_psocketmodule->m_data.m_key.m_bExpires)&&(g_psocketmodule->m_data.m_key.m_bExpireForgiveness)&&(g_psocketmodule->m_data.m_key.m_ulExpiryDate+g_psocketmodule->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!g_psocketmodule->m_data.m_key.m_bMachineSpecific)
						||((g_psocketmodule->m_data.m_key.m_bMachineSpecific)&&(g_psocketmodule->m_data.m_key.m_bValidMAC))
						)
					)
				{
//					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//					g_psocketmodule->m_data.SetStatusText(errorstring, SOCKETMODULE_STATUS_OK);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
					g_psocketmodule->m_data.SetStatusText(errorstring, SOCKETMODULE_STATUS_ERROR);
				}
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				g_psocketmodule->m_data.SetStatusText(errorstring, SOCKETMODULE_STATUS_ERROR);
			}


			m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", SOCKETMODULE_PORT_CMD);
			m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", SOCKETMODULE_PORT_STATUS);


			// override port based on programmatic name
			if((m_pszProgName)&&(strlen(m_pszProgName)>1))
			{
				unsigned short us = file.GetIniInt(m_pszProgName, "ListenPort", 0);
				if(us>0) m_usStatusPort = us;
				
				us = file.GetIniInt(m_pszProgName, "CmdListenPort", 0);
				if(us>0) m_usCommandPort = us;

				if(stricmp(m_pszProgName, "Inserter")==0)
				{
					g_psocketmodule->m_data.m_nRole = 1;
					// inserter settings only
					m_pszVANCIP=file.GetIniString(m_pszProgName, "VANC_Address", "0.0.0.0", m_pszVANCIP); // IP address of vanc inserter
					m_nVANCport=file.GetIniInt(m_pszProgName, "VANC_Port", 5167);  // port of vanc inserter
					m_nDefaultSendTimeoutMS=file.GetIniInt(m_pszProgName, "VANC_DefaultSendTimeoutMS", 5000);  // socket params
					m_nDefaultReceiveTimeoutMS=file.GetIniInt(m_pszProgName, "VANC_DefaultReceiveTimeoutMS", 5000);  // socket params
					m_nVANCCommandDelay=file.GetIniInt(m_pszProgName, "VANC_CommandDelay", 0);  // socket params
//					m_ulMPUID=file.GetIniInt(m_pszProgName, "VANC_Seg_upid_MPU_FormatID", 0xfffffff0);  //CNBC special
					m_pszMPUID=file.GetIniString(m_pszProgName, "VANC_Seg_upid_MPU_FormatID", "fffffff0", m_pszMPUID); // 0xfffffff0;  //CNBC special


				}
				else
				if(stricmp(m_pszProgName, "Logger")==0)
				{
					g_psocketmodule->m_data.m_nRole = 2;
					// logger settings only

//					If any of these have changed and the COM port monitor thread is running, shut it down and let it restart. this is the check changed

					int 	nBaud=m_nBaud;
					int		nCom=m_nCom;
					int		nParity=m_nParity;
					int		nStopBits=m_nStopBits;
					int		nByteSize=m_nByteSize;


					m_nCom =file.GetIniInt(m_pszProgName, "COM_Port", 1);  // COM port
					m_nBaud = file.GetIniInt(m_pszProgName, "BaudRate",  57600); 
					m_nParity = file.GetIniInt(m_pszProgName, "Parity",  0); ;
					m_nStopBits = file.GetIniInt(m_pszProgName, "StopBitParamID",  0); 
					m_nByteSize =  file.GetIniInt(m_pszProgName, "ByteSize",  8);

					// not this one though.  This is just a retry interval
					m_nConnRetryIntervalMS = file.GetIniInt(m_pszProgName, "ConnectionRetryMS", 10000);
					m_nConnTimeoutMS = file.GetIniInt(m_pszProgName, "ConnectionDataTimeoutMS", 5000);

					
					
//					If any of these have changed and the COM port monitor thread is running, shut it down and let it restart. this is the restart

					if(
							(nBaud!=m_nBaud)
						||(nCom!=m_nCom)
						||(nParity!=m_nParity)
						||(nStopBits!=m_nStopBits)
						||(nByteSize!=m_nByteSize)
						)
					{
						g_psocketmodule->m_bMonitorSocket = FALSE;
						_ftime(&g_psocketmodule->m_timebNextCOM);
						if(m_nConnRetryIntervalMS>0)
						{
							g_psocketmodule->m_timebNextCOM.millitm += m_nConnRetryIntervalMS%1000;
							while(g_psocketmodule->m_timebNextCOM.millitm>999)
							{
								g_psocketmodule->m_timebNextCOM.millitm -= 1000;
								g_psocketmodule->m_timebNextCOM.time++;
							}
							g_psocketmodule->m_timebNextCOM.time += m_nConnRetryIntervalMS/1000;
						}
					}



				}
				else
				{
					g_psocketmodule->m_data.m_nRole = -1;

				}
			}



			m_pszIconPath = file.GetIniString("FileServer", "IconPath", "C:\\Inetpub\\wwwroot\\Cortex\\socketmodule\\images\\", m_pszIconPath);
//			m_bUseXMLClientLog = file.GetIniInt("Messager", "UseXMLClientLog", 0)?true:false;

//			m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			m_bUseAsRunLog = file.GetIniInt("Messager", "UseAsRunLog", 1)?true:false;
			m_bUseJSONLog = file.GetIniInt("Messager", "UseJSONLog", 1)?true:false;
			m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
//			m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;

			m_nAsRunRecent = file.GetIniInt("Messager", "RecentAsRunCount", 10);


//			m_pszKeySetupHelp = file.GetIniString("Extra", "KeySetupHelp", "some help text", m_pszKeySetupHelp);


//			m_pszTabulatorHost = file.GetIniString("Transmission", "TabulatorHost", "N\\A", m_pszTabulatorHost);	// the name of the cortex host
//			m_usTabulatorCommandPort = file.GetIniInt("Transmission", "TabulatorCommandPort", TABULATOR_PORT_CMD);
//			m_pszPluginName = file.GetIniString("Transmission", "TabulatorPluginName", "Collector", m_pszPluginName);	// the name of the cortex host
//			m_pszUserToken = file.GetIniString("Transmission", "UserToken", "", m_pszUserToken);	// the name of the cortex host
//			m_nCommRetryMS = file.GetIniInt("Transmission", "CommRetryMS", 5000);


//	// file base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs

			if(g_psocketmodule->m_data.m_nRole ==1)
			{
				m_pszAsRunFileSpec =  file.GetIniString("Messager", "AsRunLogFileIni", "Logs\\InserterAsRun|YD||1|1|", m_pszAsRunFileSpec); // allow repeats!

				m_pszJSONFileSpec =  file.GetIniString("Messager", "JSONLogFileIni", "Logs\\InserterJSON|YD||1|1|", m_pszJSONFileSpec); // allow repeats!

				m_pszFileSpec = file.GetIniString("Messager", "LogFileIni", "Logs\\Inserter|YD||1|", m_pszFileSpec);
			}
			else
			if(g_psocketmodule->m_data.m_nRole ==2)
			{
				m_pszAsRunFileSpec =  file.GetIniString("Messager", "AsRunLogFileIni", "Logs\\LoggerAsRun|YD||1|1|", m_pszAsRunFileSpec); // allow repeats!

				m_pszJSONFileSpec =  file.GetIniString("Messager", "JSONLogFileIni", "Logs\\LoggerJSON|YD||1|1|", m_pszJSONFileSpec); // allow repeats!

				m_pszFileSpec = file.GetIniString("Messager", "LogFileIni", "Logs\\Logger|YD||1|", m_pszFileSpec);
			}
			else
			{
				m_pszAsRunFileSpec =  file.GetIniString("Messager", "AsRunLogFileIni", "Logs\\SocketModuleAsRun|YD||1|1|", m_pszAsRunFileSpec); // allow repeats!

				m_pszJSONFileSpec =  file.GetIniString("Messager", "JSONLogFileIni", "Logs\\SocketModuleJSON|YD||1|1|", m_pszJSONFileSpec); // allow repeats!

				m_pszFileSpec = file.GetIniString("Messager", "LogFileIni", "Logs\\SocketModule|YD||1|", m_pszFileSpec);
			}


				// SMTP address[: override port (optional)] | distribution lists | from email |  subject spec | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
			m_pszMailSpec = file.GetIniString("Messager", "SMTPIni", "mail.server.com|f3:recipient@address.com|\"someone\"<from@here.com>|%P %T from %h|%h|C:\\smtp.txt|1|1|0", m_pszMailSpec);

			
			
			if(m_pszFileSpec) 
			{
				if(m_pszProcessedFileSpec)
				{
					try{ free(m_pszProcessedFileSpec); } catch(...) {}
				}
				m_pszProcessedFileSpec = ProcessString(m_pszFileSpec, false);
			}
			
/*
			if(m_pszMailSpec) 
			{
				if(m_pszProcessedMailSpec)
				{
					try{ free(m_pszProcessedMailSpec); } catch(...) {}
				}
				m_pszProcessedMailSpec = ProcessString(m_pszMailSpec, false);
			}
*/
			if(m_pszAsRunFileSpec) 
			{
				if(m_pszProcessedAsRunFileSpec)
				{
					try{ free(m_pszProcessedAsRunFileSpec); } catch(...) {}
				}
				m_pszProcessedAsRunFileSpec = ProcessString(m_pszAsRunFileSpec, false);
			}

			if(m_pszJSONFileSpec) 
			{
				if(m_pszProcessedJSONFileSpec)
				{
					try{ free(m_pszProcessedJSONFileSpec); } catch(...) {}
				}
				m_pszProcessedJSONFileSpec = ProcessString(m_pszProcessedJSONFileSpec, false);
			}


			m_nSocketInactivityTimeout = file.GetIniInt("Communication", "InactivityTimeout", 30);
			m_ulLastUID = file.GetIniInt("Main", "LastUID", 1);
//			m_nInterVANCdelayMS = file.GetIniInt("Communication", "InsertionDelayMS", 30);
			m_nHistoryItems = file.GetIniInt("Display", "HistoryItems", 16);
			m_nAsyncMessageItems = file.GetIniInt("Main", "InternalMessageBufferItems", 10);// how many to hold in the buffer.

		}
		else //write
		{

			file.SetIniString("Main", "Name", m_pszName);
			// programmatic name.
			file.SetIniString("Main", "ProgName", m_pszProgName);

			//			file.SetIniInt("CommandServer", "ListenPort", m_usCommandPort);
//			file.SetIniInt("StatusServer", "ListenPort", m_usStatusPort);
			file.SetIniString("License", "Key", m_pszLicense);

			file.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.

			file.SetIniString("FileServer", "IconPath", m_pszIconPath);

//			file.SetIniInt("Messager", "UseEmail", m_bUseEmail?1:0);
			file.SetIniInt("Messager", "UseNet", m_bUseNetwork?1:0);
			file.SetIniInt("Messager", "UseLog", m_bUseLog?1:0);
			file.SetIniInt("Messager", "UseAsRunLog", m_bUseAsRunLog?1:0);
			file.SetIniInt("Messager", "UseJSONLog", m_bUseJSONLog?1:0);
			file.SetIniInt("Messager", "ReportSuccessfulOperation", m_bReportSuccessfulOperation?1:0);
//			file.SetIniInt("Messager", "UseXMLClientLog", m_bUseXMLClientLog?true:false);




	//		file.SetIniString("Transmission", "TabulatorHost", m_pszTabulatorHost);	// the name of the cortex host
	//		file.SetIniInt("Transmission", "TabulatorCommandPort", m_usTabulatorCommandPort);
	//		file.SetIniString("Transmission", "TabulatorPluginName", m_pszPluginName);	
	//		file.SetIniString("Transmission", "UserToken", m_pszUserToken);
	//		file.SetIniInt("Transmission", "CommRetryMS", m_nCommRetryMS);


	//		file.SetIniString("Extra", "KeySetupHelp", m_pszKeySetupHelp);


			file.SetIniString("Messager", "LogFileIni", m_pszFileSpec);
//			file.SetIniString("Messager", "SMTPIni", m_pszMailSpec);
			file.SetIniString("Messager", "AsRunLogFileIni", m_pszAsRunFileSpec);
			file.SetIniString("Messager", "JSONLogFileIni", m_pszJSONFileSpec);


			file.SetIniInt("Messager", "RecentAsRunCount", m_nAsRunRecent);


/*
			if(g_psocketmodule)
			{
				EnterCriticalSection(&g_psocketmodule->m_data.m_critMaps);
				file.SetIniInt("Keys", "NumKeys", g_psocketmodule->m_data.m_nNumMappingObjects);

				int i=0;
				while(i<g_psocketmodule->m_data.m_nNumMappingObjects)
				{
					if((g_psocketmodule->m_data.m_ppMappingObj)&&(g_psocketmodule->m_data.m_ppMappingObj[i]))
					{
						char szText[256];
						sprintf(szText, "Key_%03d_active", i);
						file.SetIniInt("Keys", szText, g_psocketmodule->m_data.m_ppMappingObj[i]->m_bActive?1:0);
						sprintf(szText, "Key_%03d_appfocus", i);
						file.SetIniInt("Keys", szText, g_psocketmodule->m_data.m_ppMappingObj[i]->m_bRequireAppFocus?1:0);
						sprintf(szText, "Key_%03d_code", i);
						file.SetIniInt("Keys", szText, g_psocketmodule->m_data.m_ppMappingObj[i]->m_nKeyCode);
						sprintf(szText, "Key_%03d_syschars", i);
						file.SetIniInt("Keys", szText, g_psocketmodule->m_data.m_ppMappingObj[i]->m_nSysChars);
					}
					i++;
				}

				LeaveCriticalSection(&g_psocketmodule->m_data.m_critMaps);
			}
*/

			file.SetIniInt("Communication", "InactivityTimeout", m_nSocketInactivityTimeout);
			file.SetIniInt("Main", "LastUID", m_ulLastUID);
//			file.SetIniInt("Communication", "InsertionDelayMS", m_nInterVANCdelayMS);
			file.SetIniInt("Display", "HistoryItems", m_nHistoryItems);
			file.SetIniInt("Main", "InternalMessageBufferItems", m_nAsyncMessageItems);// how many to hold in the buffer.


			// override port based on programmatic name
			if((m_pszProgName)&&(strlen(m_pszProgName)>1))
			{

				if(stricmp(m_pszProgName, "Inserter")==0)
				{

					// only write this if a valid name
				file.SetIniInt(m_pszProgName, "ListenPort", m_usStatusPort);
				file.SetIniInt(m_pszProgName, "CmdListenPort", m_usCommandPort);
					
					
					// inserter settings only
					file.SetIniString(m_pszProgName, "VANC_Address", m_pszVANCIP); // IP address of vanc inserter
					file.SetIniInt(m_pszProgName, "VANC_Port", m_nVANCport);  // port of vanc inserter
					file.SetIniInt(m_pszProgName, "VANC_DefaultSendTimeoutMS", m_nDefaultSendTimeoutMS);  // socket params
					file.SetIniInt(m_pszProgName, "VANC_DefaultReceiveTimeoutMS", m_nDefaultReceiveTimeoutMS);  // socket params
					file.SetIniInt(m_pszProgName, "VANC_CommandDelay", m_nVANCCommandDelay);  // socket params
//					file.SetIniInt(m_pszProgName, "VANC_Seg_upid_MPU_FormatID", m_ulMPUID);  //CNBC special
					file.SetIniString(m_pszProgName, "VANC_Seg_upid_MPU_FormatID", m_pszMPUID); // 0xfffffff0;  //CNBC special

				}
				else
				if(stricmp(m_pszProgName, "Logger")==0)
				{

					// only write this if a valid name
				file.SetIniInt(m_pszProgName, "ListenPort", m_usStatusPort);
				file.SetIniInt(m_pszProgName, "CmdListenPort", m_usCommandPort);
					
					
					// logger settings only
					file.SetIniInt(m_pszProgName, "COM_Port", m_nCom);  // COM port
					file.SetIniInt(m_pszProgName, "BaudRate",  m_nBaud); 
					file.SetIniInt(m_pszProgName, "Parity",  m_nParity); ;
					file.SetIniInt(m_pszProgName, "StopBitParamID",  m_nStopBits); 
					file.SetIniInt(m_pszProgName, "ByteSize",  m_nByteSize); 

					file.SetIniInt(m_pszProgName, "ConnectionRetryMS", m_nConnRetryIntervalMS);
					file.SetIniInt(m_pszProgName, "ConnectionDataTimeoutMS", m_nConnTimeoutMS);

				}

			}

			file.SetSettings(pszFilename, false);  // have to have correct filename

		}
		return SOCKETMODULE_SUCCESS;
	}
	return SOCKETMODULE_ERROR;
}


char* CSocketModuleSettings::ProcessString(char* pszString, bool bFreeIncomingString)
{
	if(pszString)
	{
		char pszOutput[4096];
		int nInLen=strlen(pszString);
		int nOutLen=0;
		char* pch = pszString;
		char* pchEnd = min((pch+4096),(pszString+nInLen));
		while((*pch!=0)&&(pch<pchEnd))
		{
			if(*pch == '%')
			{
				// we are in a specifier.
				// here is a list of ours:
				// %h  local hostname
		
				pch++;
				if((*pch!=0)&&(pch<pchEnd))
				{
					if(*pch == 'h')
					{
						if((g_psocketmodule->m_data.m_pszHost)&&(strlen(g_psocketmodule->m_data.m_pszHost)))
						{
							char* pszTemp = (char*)malloc(strlen(g_psocketmodule->m_data.m_pszHost)+1);
							if(pszTemp)
							{
								strcpy(pszTemp, g_psocketmodule->m_data.m_pszHost);
							
								char* pchPlus = pszTemp;
								while((*pchPlus != 0)&&(nOutLen<4096))
								{
									pszOutput[nOutLen]= *pchPlus;
									nOutLen++;
									pchPlus++;
								}
								free(pszTemp);
							}
						}
						pch++;
					}
					else
					{
						// some other specifier, can be anything.
						pszOutput[nOutLen]= *(pch-1); // do add the %, this may get further processed
						nOutLen++;
						pszOutput[nOutLen]= *pch;
						nOutLen++;
						pch++;
					}
				}
				else
				{
					pszOutput[nOutLen]= *(pch-1);
					nOutLen++;
				}
				
			}
			else
			{
				pszOutput[nOutLen]= *pch;
				nOutLen++;
				pch++;
			}
		}

		pszOutput[nOutLen]=0;

		pch = (char*)malloc(strlen(pszOutput)+1);
		if(pch)
		{
			strcpy(pch,pszOutput);
			if(bFreeIncomingString) 
			{
				try {free(pszString);} catch(...){}
			}
		}
		return pch;
	}
	return NULL;
}


