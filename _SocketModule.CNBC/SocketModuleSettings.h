// SocketModuleSettings.h: interface for the CSocketModuleSettings class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SOCKETMODULESETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
#define AFX_SOCKETMODULESETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "SocketModuleData.h"  // for endpoints
#include "SocketModuleDefines.h"
//#include "../Sentinel/SentinelData.h"  
//#include "../../Common/MFC/ODBC/DBUtil.h"  


class CSocketModuleSettings  
{
public:
	CSocketModuleSettings();
	virtual ~CSocketModuleSettings();

//	int ReadKeys();
	int Settings(bool bRead);
	int SaveLastUID();
	char* ProcessString(char* pszString, bool bFreeIncomingString);

	char* m_pszName;  // familiar name of this instance 
	char* m_pszProgName;  // programmatic name of this instance 
	unsigned long m_ulMainMode;

	unsigned long m_ulDebug;  // prints out debug statements that & with this.

	// ports
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;

//	char*	m_pszTabulatorHost;	// the name of the cortex host
//	unsigned short m_usTabulatorCommandPort;
//	char*	m_pszPluginName;
//	char*	m_pszUserToken;
//	char*	m_pszKeySetupHelp;


	// The following flags determine what settings are loaded. 
	// The specifics do not need to be stored in cortex settings, they are pushed to the various objects.

	// messaging for SocketModule
	bool m_bUseLog;			// write a log file
	bool m_bUseAsRunLog;			// write a log file
	bool m_bUseJSONLog;			// write a log file
	bool m_bUseEmail;		// send an email on failures (or commanded from remotes)
	bool m_bUseNetwork; // send a message to a remote host
	char* m_pszFileSpec;
	char* m_pszAsRunFileSpec;
	char* m_pszJSONFileSpec;
	char* m_pszMailSpec;
	char* m_pszProcessedAsRunFileSpec;
	char* m_pszProcessedJSONFileSpec;
	char* m_pszProcessedFileSpec;
	char* m_pszProcessedMailSpec;
//	bool m_bUseXMLClientLog;			// write a log file
	bool m_bLogNetworkErrors; // enables messaging into network object (logs socket errors, etc).

	bool m_bReportSuccessfulOperation;

	int m_nAsRunRecent;
//	int m_nCommRetryMS;

	
	char* m_pszLicense;  // the License Key
	char* m_pszOEMcodes;  // the possible OEM string
	char* m_pszIconPath;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.


	int m_nDeviceType; // refer to cortex device types.


	int m_nSocketInactivityTimeout; 
	unsigned long m_ulLastUID; 
	bool m_bCleanShutdown;

//	int m_nInterVANCdelayMS;
	int m_nHistoryItems;

	int m_nAsyncMessageItems;  // how many to hold in the buffer.




  // inserter settings
	char* m_pszVANCIP;  // IP address of vanc inserter
	int m_nVANCport;  // port of vanc inserter
	int m_nDefaultSendTimeoutMS;
	int m_nDefaultReceiveTimeoutMS;
	int m_nVANCCommandDelay;
//	int m_ulMPUID;
	char* m_pszMPUID;  
	int m_nCommandRepetitions;
	bool m_bDF;


	// Logger settings
  int 	m_nBaud;
	int		m_nCom;
	int		m_nParity;
	int		m_nStopBits;
	int		m_nByteSize;

	int		m_nConnRetryIntervalMS;
	int		m_nConnTimeoutMS;


};

#endif // !defined(AFX_SOCKETMODULESETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
