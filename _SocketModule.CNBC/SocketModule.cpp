// SocketModule.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "SocketModuleMain.h"
#include "SocketModule.h"
#include "SocketModuleHandler.h"
#include "SocketModuleDlg.h"
#include <process.h>
#include <direct.h>
//#include "SocketModuleSettings.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// global control vars for main thread
extern bool g_bKillThread;
extern bool g_bThreadStarted;

extern CString g_szModuleName;
extern CString g_szModuleSetName;


// ******************************************
// THINGS you have to do to get this project to compile correctly
// 1) Add to files: bmp.cpp, bmp.h, EmailHandler.cpp/.h, 
//    FileHandler.cpp/.h, ListCtrlEx.cpp/.h, MessageDispatcher.cpp/.h, MDdefines.h,
//    NAHandler.cpp/.h, networking.cpp/.h, sendmail.cpp/.h, TextUtils.cpp/.h,
//    UIHandler.cpp/.h
//    The inlcludes for all these are in "$$root$$.h", and you can remove them if
//    your project does not need them.  However, the default project is built with:
//    an App Class, a Dlg class, a Handler Class, and a Settings class.
//    The App Class is the App, the Dlg is the Dialog which is displayed when the
//    systray icon is hit.  This is the class that is the "main dialog" and has all the
//    Messge dispatching, etc.  Howevr, it is not the application's main window.  The
//    main window is not visible, it is the Handler class.  It is also persistent, and
//    basically handles creation, destruction, the icons and handles the mouse clicks
//    in the systray.  The Settings class handles all the Settings, the dialog is not
//    persistent although the data is.

// ******************************************
// There is a list of commented out includes in the "$$root$$.h" file.  
// Generally, add includes to that file, as all files in the project include that file.


/////////////////////////////////////////////////////////////////////////////
// CSocketModuleApp

BEGIN_MESSAGE_MAP(CSocketModuleApp, CWinApp)
	//{{AFX_MSG_MAP(CSocketModuleApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSocketModuleApp construction

CSocketModuleApp::CSocketModuleApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
//	m_ulFlags = CXWF_DEFAULT;
	m_pszSettingsURL = NULL;
	m_nPID = -1;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CSocketModuleApp object

CSocketModuleApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CSocketModuleApp initialization

char* CSocketModuleApp::GetSettingsFilename() 
{
	char* pch = NULL;
	FILE* fp = fopen(SOCKETMODULE_SETTINGS_FILE_SETTINGS, "rb");
	if(fp)
	{
//	AfxMessageBox("opened");
		// determine file size
		fseek(fp,0,SEEK_END);
		unsigned long ulLen = ftell(fp);

		if((ulLen>0)&&(ulLen<MAX_PATH))
		{	
			fseek(fp,0,SEEK_SET);
			// allocate buffer and read in file contents
			pch = (char*) malloc(ulLen+1); // for 0
			if(pch!=NULL)
			{
//			CString foo; foo.Format("len=%d", ulLen);
//	AfxMessageBox(foo);
				fread(pch,sizeof(char),ulLen,fp);
				memset(pch+ulLen, 0, 1);
//	AfxMessageBox(pch);
			}
		}
		fclose(fp);
	}



	if(pch==NULL)
	{
		pch = (char*) malloc(strlen(m_pszExeName)+5); // for .csf + 0
		if(pch!=NULL)
		{
			strcpy(pch, m_pszExeName);  // cortex settings file
			*pch = toupper(*m_pszExeName); // capitalize
			strcat(pch, ".csf");  // cortex settings file
//		AfxMessageBox(pch);
		}

	}

/*
	if(pch==NULL)
	{
		pch = (char*) malloc(strlen(SOCKETMODULE_SETTINGS_FILE_DEFAULT)+1); // for 0
		if(pch!=NULL)
		{
			strcpy(pch, SOCKETMODULE_SETTINGS_FILE_DEFAULT);  // cortex settings file
		}
	}
*/

	return pch;
}




BOOL CSocketModuleApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	// uncomment the following to initialize RichEdit Controls
//	AfxInitRichEdit();

	// prevent multiple instances
/*
	bool bAllowMultiple = false;
  if (m_lpCmdLine[0] != '\0')
  {
		if( (strstr(m_lpCmdLine, "/c")) || (strstr(m_lpCmdLine, "/C")) )  // clone mode.  Allow another instance (until we check later for existence of a listener)
		bAllowMultiple = true;
  }
*/
//	AfxMessageBox( m_lpCmdLine );

	char pchGappbuf[MAX_PATH];
	sprintf(pchGappbuf,"Global\\%s",m_pszExeName);

	g_szModuleName.Format("SocketModule:%s",m_pszExeName);


	int nExit = 0;

	if(::CreateMutex(NULL,TRUE,pchGappbuf))
	{
		if(GetLastError()==ERROR_ALREADY_EXISTS)
		{
//			if(!bAllowMultiple)
			{
//				AfxMessageBox("A previous instance was detected.\nMust exit.");
				nExit = 1; //return FALSE; 
			}
		}
	}
	else
	{
		if(GetLastError()==ERROR_ACCESS_DENIED)
		{ 
//			if(!bAllowMultiple)
			{
//				AfxMessageBox("A previous instance was detected.\nMust exit.");
				nExit = 2; //return FALSE; 
			}
		}
		else 
		{
//			AfxMessageBox("Failure to create mutex!\nMust exit.");
			nExit = 3; //return FALSE; 
		}
	}
	bool bAllowMultiple = false; //default
	bool bAlertMultiple = true; //default to verbose
	bool bLogPID = false; //default to no 
	bool bLogPIDSuccess = false; //default to no
	bool bLogPIDError = true; //default to yes
	FILE *fp = NULL;

	CFileUtil fu;
	char pszFilename[MAX_PATH];
	strcpy(pszFilename, "");

	char* pchF=GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, SOCKETMODULE_SETTINGS_FILE_DEFAULT);  // cortex settings file
//		AfxMessageBox("s");

	if(fu.GetSettings(pszFilename, false)&FILEUTIL_MALLOC_OK)
	{
		bAllowMultiple = fu.GetIniInt("Startup", "AllowMultipleInstances", 0)?true:false;   //default
		bAlertMultiple = fu.GetIniInt("Startup", "AlertOnMultipleInstances", 1)?true:false; //default to silent
		bLogPID = fu.GetIniInt("Startup", "LogPID", 0)?true:false; //default to no logging
		bLogPIDSuccess = fu.GetIniInt("Startup", "LogPIDSuccess", 0)?true:false; //default to no logging
		bLogPIDError = fu.GetIniInt("Startup", "LogPIDError", 1)?true:false; //default to logging of errors only

		g_szModuleSetName = fu.GetIniString("Main", "Name", "SocketModule");

		fu.SetIniInt("Startup", "AllowMultipleInstances", bAllowMultiple?1:0);  // write them, here only
		fu.SetIniInt("Startup", "AlertOnMultipleInstances", bAlertMultiple?1:0);// write them, here only
		fu.SetIniInt("Startup", "LogPID", bLogPID?1:0);  // write them, here only
		fu.SetIniInt("Startup", "LogPIDSuccess", bLogPIDSuccess?1:0);  // write them, here only
		fu.SetIniInt("Startup", "LogPIDError", bLogPIDError?1:0);  // write them, here only

		fu.SetSettings(pszFilename, false);  // have to have correct filename
	}


	if(nExit>0)  // have to check stuff out, from settings.
	{
		if(bAllowMultiple) // no problem, just allow it.
		{
			if(bAlertMultiple) 
			{
				sprintf(pchGappbuf,"Are you sure you want to start another instance of %s?\nCode %d",m_pszExeName, nExit);
				
				if(AfxMessageBox(pchGappbuf, MB_YESNO) == IDYES)
				{
					nExit = -1; // does the pid file thing normally, but appends pid to the pid file, rather than overwrites.
				}
				else
				{
					// just make an error message for the log file.
					switch(nExit)
					{
					case 3: {	sprintf(pchGappbuf,"User declined new instance after failure to create mutex for %s.  Code %d",m_pszExeName, nExit); } break;
					default: { sprintf(pchGappbuf,"User declined new instance after a previous instance of %s was detected.  Code %d",m_pszExeName, nExit); } break;
					}
				}
			}
			else
			{
				// just allow it
				nExit = -1; // does the pid file thing normally, but appends pid to the pid file, rather than overwrites.
			}
		}
		else
		{
			if(bAlertMultiple)
			{
				switch(nExit)
				{
				case 3: {	sprintf(pchGappbuf,"Failure to create mutex for %s!\nMust exit.\nCode %d",m_pszExeName, nExit); } break;
				default: { sprintf(pchGappbuf,"A previous instance of %s was detected.\nMust exit.\nCode %d",m_pszExeName, nExit); } break;
				}
				AfxMessageBox(pchGappbuf);
				
			}
			else
			{
				// just make an error message for the log file.
				switch(nExit)
				{
				case 3: {	sprintf(pchGappbuf,"Failure to create mutex for %s.  Must exit.  Code %d",m_pszExeName, nExit); } break;
				default: { sprintf(pchGappbuf,"A previous instance of %s was detected.  Must exit.  Code %d",m_pszExeName, nExit); } break;
				}
			}
		}
	}

	if(nExit>0) 
	{
		if(bLogPIDError)
		{
			// deal with the file and then exit.
			FILE *fp; fp = fopen("socketmodule.pid", "ab"); // append!
			if (fp)
			{ 
				m_nPID = _getpid();
				_timeb timestamp;
				_ftime( &timestamp );

				tm* theTime = localtime( &timestamp.time	);
				char timebuf[MAX_PATH];
				strftime(timebuf, MAX_PATH-1, "* %Y-%m-%d_%H.%M.%S.", theTime ); // asterisk is a key character.

				fprintf(fp, "%s%d: %s pid(%ld)\n", 
					timebuf,
					timestamp.millitm,
					pchGappbuf,
					m_nPID
					); 
				fclose(fp); 
			} 
		}
		// and must exist here!
		return FALSE;
	}
	else
	{
		if((bLogPIDError)||(bLogPIDSuccess))  // have to check for errors)
		{
			fp = fopen("socketmodule.pid", "rb");
			if (fp)
			{ 
				fseek(fp,0,SEEK_END);
				unsigned long ulLen = ftell(fp);
				if((ulLen>0)&&(ulLen<MAX_PATH))
				{	
					fseek(fp,0,SEEK_SET);
					// allocate buffer and read in file contents
					char* pch = (char*) malloc(ulLen+1); // for 0
					if(pch!=NULL)
					{
		//			CString foo; foo.Format("len=%d", ulLen);
		//	AfxMessageBox(foo);
						fread(pch,sizeof(char),ulLen,fp);
						memset(pch+ulLen, 0, 1);
		//	AfxMessageBox(pch);
					
						if((bLogPIDSuccess)||(strchr(pch, '*')!=NULL)) // only if there is problem info in there...., or if logging successes
						{
							char filename[MAX_PATH];
							strcpy(filename, "Logs");
							_mkdir(filename);  // if exists already np

							_timeb timestamp;
							_ftime( &timestamp );
							tm* theTime = localtime( &timestamp.time	);
							char timebuf[MAX_PATH];
							strftime(timebuf, MAX_PATH-1, "\\%_%Y-%m-%d_%H.%M.%S.", theTime );
							strcat(filename, timebuf);
							sprintf(timebuf, "%s%s%d_pid.log", m_pszExeName, filename, timestamp.millitm);
							FILE* fpn;
							fpn = fopen(timebuf, "wb");
							if(fpn)
							{
								try
								{
									fwrite(pch, sizeof(char), ulLen, fpn);
								}
								catch(...)
								{
								}
								fclose(fpn);
							}
						}
						try{ free(pch); } catch(...){}
					}
				}
				fclose(fp);
			}
		}
		// overwrite!
		// FILE *fp; 

		if(bLogPID)
		{
			if(nExit<0)  // append pid number, means multi was allowed (and acknowledged yes if alerted)
			{
				fp = fopen("socketmodule.pid", "ab");
			}
			else
			{
				fp = fopen("socketmodule.pid", "wb");  // blows away existing, and previous copies the thing over to logs, so each file only has a record of multiplly open pids
			}
			m_nPID = _getpid();
			if (fp) { fprintf(fp, "%ld\n", m_nPID); fclose(fp); } 
		}
		else
		{
			_unlink("socketmodule.pid");
		}
	}

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

  //SetRegistryKey("Video Design Interactive");  // can use this for licensing later on, or use a hidden file somewhere

	CSocketModuleDlg* pDlg = new CSocketModuleDlg();
	VERIFY(pDlg->Create());
//	pDlg->DoModal();

// handler window for systray
	CSocketModuleHandler* pWnd = new CSocketModuleHandler();
	pWnd->m_pMainDlg = pDlg;
	VERIFY(pWnd->Create());
	m_pMainWnd = pWnd;


/////////////////////////////////////////////////////////////////////////
// this was a test
/*
	CXDlg* prDlg = new CXDlg;
	if(prDlg)
	{
		if(!prDlg->Create()) AfxMessageBox("Could not create object");
		else
		{
			x
			prDlg->UpdateData(FALSE); 
			prDlg->SetWindowText("xxx"); 

			prDlg->ShowWindow(SW_SHOW);
		}
	}
	return TRUE;
*/
/////////////////////////////////////////////////////////////////////////

	// here start the main (almost non-MFC) thread.
	// we can feed a pointer to the app into it to give it access to the status windows.
	// all settings are in the thread.
	g_bThreadStarted = false;
	if(_beginthread(SocketModuleMainThread, 0, (void*) this)==-1)
	{
		AfxMessageBox("Could not begin main thread!\nMust exit.");
		return FALSE;
	}

	return TRUE;
}

int CSocketModuleApp::ExitInstance() 
{
	g_bKillThread=true;
//	while(g_bThreadStarted) Sleep(1);
	if(m_pszSettingsURL) free(m_pszSettingsURL);
	return CWinApp::ExitInstance();
}
