// SocketModuleLibrettoImports.cpp:
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "SocketModule.h"  // just included to have access to windowing environment
#include "SocketModuleDlg.h"  // just included to have access to windowing environment
#include "SocketModuleHandler.h"  // just included to have access to windowing environment
#include "SocketModuleMain.h"
#include "..\..\Common\API\SCTE\SCTE104Util.h"
#include "SocketModuleLibrettoImports.h"
#include <process.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif



extern bool g_bKillThread;
extern bool g_bThreadStart;
extern bool g_bKillStatus;
extern CSocketModuleMain* g_psocketmodule;



// imported from Libretto but modified.
//#ifdef REMOVED_FOR_NOW_20151028


CLibrettoDestinationObject::CLibrettoDestinationObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
//	m_pszClientName = NULL;
	m_ulStatus	= LIBRETTO_STATUS_UNINIT;
	m_ulFlags = LIBRETTO_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
//	m_pAPIConn = NULL;
	m_usPort = 5167; //default telnet port for VANC

	m_dblDiskKBFree = -1.0;  
	m_dblDiskKBTotal = -1.0; 
	m_dblDiskPercent = 90.0; 
	m_nChannelID = -1;
	m_nItemID = -1;

	m_socket = NULL;

	m_bKillCommandQueueThread = true;
	m_bCommandQueueThreadStarted = false;

	m_bKillConnThread = true;
	m_bConnThreadStarted = false;
	m_bKillMonThread = true;
	m_bMonThreadStarted = false;
	m_bSending = false;
	_ftime(&m_timebLastDisconnect);
	_ftime(&m_timebCommandQueueTick);
	_ftime(&m_timebLastPing);
	
	InitializeCriticalSection(&m_critComm);
//	InitializeCriticalSection(&m_critMemQueue);
//	m_ppMemQueue = NULL;
//	m_nNumMemQueueArray=0;
//	m_nPtrMemQueueIn=-1;
//	m_nPtrMemQueueOut=-1;

	m_pvExternal = NULL;

//	m_bFailed = false;
//	m_timebFailure;
}

CLibrettoDestinationObject::~CLibrettoDestinationObject()
{
	m_bKillConnThread = true;
//	while(	m_bConnThreadStarted ) Sleep(1);

	if(m_pvExternal) delete m_pvExternal; // must use new to create object

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
//	if(m_pszClientName) free(m_pszClientName); // must use malloc to allocate
	DeleteCriticalSection(&m_critComm);
//	DeleteCriticalSection(&m_critMemQueue);
}

int CLibrettoDestinationObject::SendTelnetCommand(char* pchBuffer, unsigned long ulBufferLen, char chEolnType, char* pSource)
{
	if((pchBuffer)&&(m_pszServerName))
	{
		if((m_socket == NULL)&&(!m_bConnThreadStarted))
		{
			m_ulStatus = LIBRETTO_STATUS_CONN;
			if(_beginthread(LibrettoAsynchConnThread, 0, (void*)this)==-1)
			{
			//error.

			//**MSG
			}
			else
			{
				int nWait = 0;
				while((m_ulStatus == LIBRETTO_STATUS_CONN)&&(nWait<1000))  // 0.5 seconds?   make a full second.
				{
					nWait++;
					Sleep(1);
				}
			}
//			m_net.OpenConnection(m_pszServerName, g_psocketmodule->m_settings.m_nPort, &m_socket);
		}
		if(m_socket != NULL)
		{

			// removing this logging because already reported outside, and hex values start w 0 potentially and just show up as nothing
//	if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC) 
//			g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, pSource, "sending: %s", pchBuffer?((char*)(pchBuffer)):"(null)"); // Sleep(250); //(Dispatch message)

//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "sending: %s", pchBuffer?((char*)(pchBuffer)):"(null)"); // Sleep(250); //(Dispatch message)

			int nReturn = m_net.SendLine((unsigned char*)pchBuffer, ulBufferLen, m_socket, chEolnType);
			if(nReturn>=NET_SUCCESS)
			{
				return LIBRETTO_SUCCESS;
			}
			else if(nReturn == NET_ERROR_CONN)
			{// conn was cut, lets re connect and try one more time.
				m_net.CloseConnection(m_socket);
				m_socket = NULL;
				if(!m_bConnThreadStarted)
				{
					m_ulStatus = LIBRETTO_STATUS_CONN;
					if(_beginthread(LibrettoAsynchConnThread, 0, (void*)this)==-1)
					{
					//error.

					//**MSG
					}
					else
					{
						int nWait = 0;
						while((m_ulStatus == LIBRETTO_STATUS_CONN)&&(nWait<1000))  // 0.5 seconds?  make a full second.
						{
							nWait++;
							Sleep(1);
						}
					}
					//m_net.OpenConnection(m_pszServerName, g_plibretto->m_settings.m_nPort, &m_socket);
				}
				if((m_socket != NULL)&&(!m_bConnThreadStarted))
				{
//	if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC) 	
//	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, pSource, "retry: sending: %s", pchBuffer?((char*)(pchBuffer)):"(null)"); // Sleep(250); //(Dispatch message)

//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "retry: sending: %s", pchBuffer?((char*)(pchBuffer)):"(null)"); // Sleep(250); //(Dispatch message)
					int nReturn = m_net.SendLine((unsigned char*)pchBuffer, ulBufferLen, m_socket);
					if(nReturn>=NET_SUCCESS)
					{
						return LIBRETTO_SUCCESS;
					} // else just return error.
				}
			}
		}
	}
	return LIBRETTO_ERROR;
}






void LibrettoAsynchConnThread(void* pvArgs)
{
	CLibrettoDestinationObject* pDestObj = (CLibrettoDestinationObject*) pvArgs;
//	CLibrettoDestinationObject* pDestObj = new CLibrettoDestinationObject;
	if(pDestObj==NULL) return;
	if(g_psocketmodule==NULL) return;

//	pDestObj->m_pvExternal = new CSCTE104Util;
	if(pDestObj->m_pvExternal==NULL) return;


	pDestObj->m_bConnThreadStarted = true;
	CBufferUtil bu;


	char szSocketModuleSource[MAX_PATH]; 
	strcpy(szSocketModuleSource, "SocketModuleVANCHandler");

// check pDestObj->m_pszServerName  for "COM"
	char neterrorstring[NET_ERRORSTRING_LEN];
	strcpy(neterrorstring,"");
	int nPort = g_psocketmodule->m_settings.m_nVANCport;


if((g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC)||(g_psocketmodule->m_settings.m_bUseAsRunLog))
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, "asrun,log", szSocketModuleSource, "Opening connection to %s:%d", pDestObj->m_pszServerName, nPort);  //(Dispatch message)
}


	/*
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Opening connection to %s:%d", pDestObj->m_pszServerName, nPort); // Sleep(250); //(Dispatch message)
*/

	int nHostReturn = pDestObj->m_net.OpenConnection(pDestObj->m_pszServerName, nPort, &pDestObj->m_socket,
		g_psocketmodule->m_settings.m_nDefaultSendTimeoutMS,
		g_psocketmodule->m_settings.m_nDefaultReceiveTimeoutMS,
		neterrorstring); // re-establish

	if((nHostReturn>=NET_SUCCESS)&&(pDestObj->m_socket!=NULL))
	{
/*
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Success opening connection to %s:%d", pDestObj->m_pszServerName, nPort); // Sleep(250); //(Dispatch message)
		g_plibretto->SendMsg(CX_SENDMSG_INFO, "Init connection", "Connection to %s:%d established", pDestObj->m_pszServerName, nPort);
*/
		char tnbuffer[1024]; // more than enough for all connection types known so far
		int nBuflen = 4;
		
// have to clear the queue on reconnect, if it has been longer than some period....

/*

		if(g_plibretto->m_settings.m_nClearQueueIfDisconnectedMS>=0)
		{
			_timeb now;
			_ftime(&now);

			if(
					((pDestObj->m_timebLastDisconnect.time + g_plibretto->m_settings.m_nClearQueueIfDisconnectedMS/1000)<now.time)
				||(((pDestObj->m_timebLastDisconnect.time + g_plibretto->m_settings.m_nClearQueueIfDisconnectedMS/1000)==now.time)&&((pDestObj->m_timebLastDisconnect.millitm + g_plibretto->m_settings.m_nClearQueueIfDisconnectedMS%1000)<now.millitm))
				)
			{
			//	clear the queue
				if((g_plibretto->m_data.m_pdb)&&(g_plibretto->m_data.m_pdbConn))
				{
					char szSQL[DB_SQLSTRING_MAXLEN];
					char dberrorstring[DB_ERRORSTRING_LEN];
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE host = '%s'", //HARDCODE
						((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
						pDestObj->m_pszServerName);

if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Clearing queue: %s", szSQL); // Sleep(250); //(Dispatch message)
					if(g_plibretto->m_data.m_pdb->ExecuteSQL(g_plibretto->m_data.m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
					{
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "SQL error %s",dberrorstring);
					}
				}

/*
				EnterCriticalSection(&pDestObj->m_critMemQueue);
				if(pDestObj->m_nPtrMemQueueOut >= 0 )
				{
					pDestObj->ClearMemQueue();
				}
				LeaveCriticalSection(&pDestObj->m_critMemQueue);
* /
			}
		}
*/
		switch(pDestObj->m_usType)
		{
			
		case CX_DESTTYPE_HARRIS_ICONII: // else all the other types will just respond to \\, an error or something, but just something to init the conn.
			{
				sprintf(tnbuffer, "*\\\\%c%c", 13,10);
				nBuflen = 5;
			}	break;
		case CX_DESTTYPE_EVERTZ_7847FSE:  // need to init conn and kick off the timer, which can be entirely conatined in this thread.
			{
				// assemble the init message buffer.
				sprintf(tnbuffer, "");

				CSCTE104Util* p104 = (CSCTE104Util*)pDestObj->m_pvExternal; // for convenience.

				CSCTE104SingleMessage* sm = p104->CreateSingleMessageObject();
				if(sm)
				{
					sm->OpId1High =0x00;
					sm->OpId1Low =0x01;
					sm->MessageNumber = p104->IncrementSecondaryMessageNumber();
					unsigned char* pucBuf = p104->ReturnMessageBuffer(sm);
					if(pucBuf)
					{
						unsigned long ulLen = *(pucBuf+2);
						ulLen <<= 8; ulLen += *(pucBuf+3);
						memcpy(tnbuffer, pucBuf,  min(ulLen, 1024)); // 1024 should be more than enough, but use min just for safety
						nBuflen = ulLen;
						delete [] pucBuf;


	if((g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC)||(g_psocketmodule->m_settings.m_bUseAsRunLog))
	{
//unsigned long ulSend = buflen;
//char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_DELIM32BIT);
//AfxMessageBox(pchSnd);

		unsigned long ulSend = nBuflen;
		char* pchSnd = bu.ReadableHex((char*)tnbuffer, &ulSend, MODE_FULLHEX);

		g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "init_conn", "sending: %s", pchSnd?pchSnd:"(null)"); // Sleep(250); //(Dispatch message)

		if(pchSnd) free(pchSnd);
	}


					}
					delete sm;
				}
				else
				{
					//log the error
				}

			}	break;
		default:
			{
				sprintf(tnbuffer, "\\\\%c%c", 13,10);
			}	break;
		}


	EnterCriticalSection(&pDestObj->m_critComm);
		pDestObj->m_bSending=true;



		nHostReturn = pDestObj->SendTelnetCommand(tnbuffer, nBuflen, EOLN_NONE, szSocketModuleSource); 
		if((nHostReturn>SOCKETMODULE_ERROR)&&(pDestObj->m_socket!=NULL))
		{
			unsigned char* pucBuffer = NULL;
			unsigned long ulBufferLen = 0;

			struct timeval tv;
			tv.tv_sec = 1; tv.tv_usec = 500000;  // timeout value, 1.5 second for first connection response
			fd_set fds;
			int nNumSockets;
			FD_ZERO(&fds);
			FD_SET(pDestObj->m_socket, &fds);

//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Selecting");

			nNumSockets = select(0, &fds, NULL, NULL, &tv);
//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Select returned %d", nNumSockets);

			if ( nNumSockets == INVALID_SOCKET )
			{
				int nErrorCode = WSAGetLastError();
				char* pchError = pDestObj->m_net.WinsockEnglish(nErrorCode);
				if(pchError)
				{


//if((g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC)||(g_psocketmodule->m_settings.m_bUseAsRunLog))
{
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "init_conn", "Invalid socket on connection to %s:%d\n%s", 
							pDestObj->m_pszServerName, nPort, pchError?pchError:""); // Sleep(250); //(Dispatch message)
}

/*
					if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Invalid socket on connection to %s:%d\n%s", 
							pDestObj->m_pszServerName, nPort, pchError?pchError:""); // Sleep(250); //(Dispatch message)
*/
//					g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Init connection", "Invalid socket on connection to %s:%d\n%s", pDestObj->m_pszServerName, nPort, pchError);
					LocalFree(pchError);
				}
				pDestObj->m_net.CloseConnection(pDestObj->m_socket);
				pDestObj->m_socket=NULL;
				pDestObj->m_bSending=false;

			}
			else
			if(
					(nNumSockets==0) // 0 = timed out, -1 = error
				||(!(FD_ISSET(pDestObj->m_socket, &fds)))
				) 
			{ 
				// no data, no problem, just return for now.
				pDestObj->m_bSending=false;
			} 
			else // there is recv data.
			{
//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "there is receive data");
				nHostReturn = pDestObj->m_net.GetLine(&pucBuffer, &ulBufferLen, pDestObj->m_socket);
				pDestObj->m_bSending=false;
				if((nHostReturn<=LIBRETTO_ERROR)||(pDestObj->m_socket==NULL))
				{
					_ftime(&pDestObj->m_timebLastDisconnect);

					int nErrorCode = WSAGetLastError();
					char* pchError = pDestObj->m_net.WinsockEnglish(nErrorCode);
					if(pchError)
					{
						if((ulBufferLen<=0)||(pucBuffer==NULL))
						{
//if((g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC)||(g_psocketmodule->m_settings.m_bUseAsRunLog))
{
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "init_conn", "Connection to %s:%d terminated\n%s", 
									pDestObj->m_pszServerName, nPort, pchError?pchError:""); // Sleep(250); //(Dispatch message)

}
/*

							if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
								g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Connection to %s:%d terminated\n%s", 
									pDestObj->m_pszServerName, nPort, pchError?pchError:""); // Sleep(250); //(Dispatch message)

							g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Init connection", "Connection to %s:%d terminated\n%s", pDestObj->m_pszServerName, nPort, pchError);
*/
						}
						else
						{
//if((g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC)||(g_psocketmodule->m_settings.m_bUseAsRunLog))
{
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "init_conn", "Communication error on %s:%d\n%s", 
									pDestObj->m_pszServerName, nPort, pchError); // Sleep(250); //(Dispatch message)

}
/*
							if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
								g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Communication error on %s:%d\n%s", 
									pDestObj->m_pszServerName, nPort, pchError); // Sleep(250); //(Dispatch message)

							g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Init connection", "Communication error on %s:%d\n%s", pDestObj->m_pszServerName, nPort, pchError);
*/
							if(pucBuffer) free(pucBuffer);
						}
						LocalFree(pchError);
					}
					pDestObj->m_net.CloseConnection(pDestObj->m_socket);
					pDestObj->m_socket=NULL;
//					pDestObj->m_bSending=false;
					
//		if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "init_conn", "Error response on connection to %s:%d", pDestObj->m_pszServerName, nPort); // Sleep(250); //(Dispatch message)
//		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Error response on connection to %s:%d", pDestObj->m_pszServerName, nPort); // Sleep(250); //(Dispatch message)
				}
				else
				{

					switch(pDestObj->m_usType)
					{
					case CX_DESTTYPE_EVERTZ_7847FSE:  // need to kick off the timer, which can be entirely contained in this thread.
						{
							_ftime(&pDestObj->m_timebLastPing);
							pDestObj->m_timebLastPing.time -= 60; // back it off one minute so the keep alive will go immediately.


if((g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC)||(g_psocketmodule->m_settings.m_bUseAsRunLog))
//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO)) 
	{
//unsigned long ulSend = buflen;
//char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_DELIM32BIT);
//AfxMessageBox(pchSnd);

		unsigned long ulSend = ulBufferLen;
		char* pchSnd = bu.ReadableHex((char*)pucBuffer, &ulSend, MODE_FULLHEX);

	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "init_conn", "received: %s", pchSnd?pchSnd:"(null)"); // Sleep(250); //(Dispatch message)
//		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "init_conn", "received: %s", pchSnd?pchSnd:"(null)"); // Sleep(250); //(Dispatch message)

		if(pchSnd) free(pchSnd);
	}

						} break;
					default: 
						{
if((g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC)||(g_psocketmodule->m_settings.m_bUseAsRunLog))
//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO)) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "init_conn", "received: %s", pucBuffer?((char*)(pucBuffer)):"(null)"); // Sleep(250); //(Dispatch message)
//		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "received: %s", pucBuffer?((char*)(pucBuffer)):"(null)"); // Sleep(250); //(Dispatch message)
						} break;
					}

				}
				if(pucBuffer) free(pucBuffer);
			}
		}
		else
		{
			pDestObj->m_bSending=false;

//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, "init_conn", "Error initializing connection to %s:%d", pDestObj->m_pszServerName, nPort); // Sleep(250); //(Dispatch message)
//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Error initializing connection to %s:%d", pDestObj->m_pszServerName, nPort); // Sleep(250); //(Dispatch message)
			pDestObj->m_net.CloseConnection(pDestObj->m_socket);
			pDestObj->m_socket=NULL;
		}
	LeaveCriticalSection(&pDestObj->m_critComm);

	}
	else
	{
//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
{
	if(strlen(neterrorstring)>0)
	{
//		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Error opening connection to %s:%d\n%s", pDestObj->m_pszServerName, nPort, neterrorstring); // Sleep(250); //(Dispatch message)
		g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "init_conn", "Error opening connection to %s:%d\n%s", pDestObj->m_pszServerName, nPort, neterrorstring); // Sleep(250); //(Dispatch message)
	}
	else
	{
//		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Error opening connection to %s:%d", pDestObj->m_pszServerName, nPort); // Sleep(250); //(Dispatch message)
		g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "init_conn", "Error opening connection to %s:%d", pDestObj->m_pszServerName, nPort); // Sleep(250); //(Dispatch message)
	}
}		
		pDestObj->m_net.CloseConnection(pDestObj->m_socket);
		pDestObj->m_socket=NULL;
	}
	pDestObj->m_ulStatus = LIBRETTO_STATUS_UNINIT;
	pDestObj->m_bConnThreadStarted = false;

	// added:
//	if (pDestObj) delete (pDestObj);


	_endthread();

}

void LibrettoConnMonitorThread(void* pvArgs)
{
	CLibrettoDestinationObject* pDestObj = (CLibrettoDestinationObject*) pvArgs;
	if(pDestObj==NULL) return;
	CBufferUtil bu;

	pDestObj->m_bMonThreadStarted = true;
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, "Connection Monitor", "Started connection monitor"); 
	int nLastCheck = clock();
	while((!g_bKillThread)&&(pDestObj)&&(!pDestObj->m_bKillMonThread))
	{
//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "sending: %d, sock %d  [%d]",pDestObj->m_bSending, pDestObj->m_socket, clock()); 
		if((pDestObj->m_socket)&&(!pDestObj->m_bSending))
		{
			// monitor socket for disconnection.
			struct timeval tv;
			tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
			fd_set fds;
			int nNumSockets;
			FD_ZERO(&fds);
			if(!pDestObj->m_bSending)
			{
//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:conn_mon", "selecting sock %d  [%d]", pDestObj->m_socket, clock()); 
				FD_SET(pDestObj->m_socket, &fds);
				nNumSockets = select(0, &fds, NULL, NULL, &tv);

//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:conn_mon", "selected sock %d, return=%d  [%d]", pDestObj->m_socket, nNumSockets, clock()); 
				if ( nNumSockets == INVALID_SOCKET )
				{
					int nErrorCode = WSAGetLastError();
					char* pchError = pDestObj->m_net.WinsockEnglish(nErrorCode);
					if(pchError)
					{
/*
						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
							g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Invalid socket on connection to %s:%d\n%s", 
								pDestObj->m_pszServerName, pDestObj->m_usPort, pchError?pchError:""); // Sleep(250); //(Dispatch message)

						g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Connection monitor", "Invalid socket on connection to %s:%d\n%s", pDestObj->m_pszServerName, pDestObj->m_usPort, pchError);
*/
						LocalFree(pchError);
					}
					else
					{
/*
						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
							g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Invalid socket on connection to %s:%d", 
								pDestObj->m_pszServerName, pDestObj->m_usPort); // Sleep(250); //(Dispatch message)

						g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Connection monitor", "Invalid socket on connection to %s:%d", pDestObj->m_pszServerName, pDestObj->m_usPort);
*/
					}
					pDestObj->m_net.CloseConnection(pDestObj->m_socket);
					pDestObj->m_socket=NULL;
				}
				else
				if(
						(nNumSockets==0) // 0 = timed out, -1 = error
					||(!(FD_ISSET(pDestObj->m_socket, &fds)))
					) 
				{ 
					// no data, no problem.
//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "no data on sock %d [%d]", pDestObj->m_socket, clock()); 

					// no data, nothing to receive, so let's just check the time.
					// if need be, send a keep alive.
					_timeb timestamp;
					_ftime(&timestamp);

					if(timestamp.time > pDestObj->m_timebLastPing.time + 50) // spec is 60 seconds, then disconnect.
					{

						switch(pDestObj->m_usType)
						{
						case CX_DESTTYPE_EVERTZ_7847FSE:  // need to init conn and kick off the timer, which can be entirely contained in this thread.
							{
								// assemble the init message buffer.
								char tnbuffer[1024]; // more than enough for all connection types known so far
								int nBuflen;
								sprintf(tnbuffer, "");

								CSCTE104Util* p104 = (CSCTE104Util*)pDestObj->m_pvExternal; // for convenience.

								CSCTE104SingleMessage* sm = p104->CreateSingleMessageObject();
								if(sm)
								{
									sm->OpId1High =0x00;
									sm->OpId1Low =0x03;  // keep alive!
									sm->MessageNumber = p104->IncrementSecondaryMessageNumber();
									unsigned char* pucBuf = p104->ReturnMessageBuffer(sm);
									if(pucBuf)
									{
										unsigned long ulLen = *(pucBuf+2);
										ulLen <<= 8; ulLen += *(pucBuf+3);
										memcpy(tnbuffer, pucBuf,  min(ulLen, 1024)); // 1024 should be more than enough, but use min just for safety
										nBuflen = ulLen;
										delete [] pucBuf;
									}
									delete sm;


if((g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC)||(g_psocketmodule->m_settings.m_bUseAsRunLog))
//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO)) 
	{
//unsigned long ulSend = buflen;
//char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_DELIM32BIT);
//AfxMessageBox(pchSnd);

		unsigned long ulSend = nBuflen;
		char* pchSnd = bu.ReadableHex((char*)tnbuffer, &ulSend, MODE_FULLHEX);

		g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "keep_alive", "sending: %s", pchSnd?pchSnd:"(null)"); // Sleep(250); //(Dispatch message)
//		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "keep_alive", "sending: %s", pchSnd?pchSnd:"(null)"); // Sleep(250); //(Dispatch message)

		if(pchSnd) free(pchSnd);
	}

		EnterCriticalSection(&pDestObj->m_critComm);
			pDestObj->m_bSending=true;

			int nHostReturn = pDestObj->SendTelnetCommand(tnbuffer, nBuflen, EOLN_NONE, "keep_alive"); 
			if((nHostReturn>LIBRETTO_ERROR)&&(pDestObj->m_socket!=NULL))
			{
				unsigned char* pucBuffer = NULL;
				unsigned long ulBufferLen = 0;

	//			struct timeval tv;
				tv.tv_sec = 5; tv.tv_usec = 0;  // timeout value, 0.5 second for keep alives
				fd_set fds;
		//		int nNumSockets;
				FD_ZERO(&fds);
				FD_SET(pDestObj->m_socket, &fds);
				nNumSockets = select(0, &fds, NULL, NULL, &tv);

				if ( nNumSockets == INVALID_SOCKET )
				{
					int nErrorCode = WSAGetLastError();
					char* pchError = pDestObj->m_net.WinsockEnglish(nErrorCode);
					if(pchError)
					{
//if((g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC)||(g_psocketmodule->m_settings.m_bUseAsRunLog))
						//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//							g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Connection monitor", "Invalid socket on connection to %s:%d\n%s", 
							g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "Connection monitor", "Invalid socket on connection to %s:%d\n%s", 
								pDestObj->m_pszServerName, pDestObj->m_usPort, pchError?pchError:""); // Sleep(250); //(Dispatch message)

//						g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Init connection", "Invalid socket on connection to %s:%d\n%s", pDestObj->m_pszServerName, pDestObj->m_usPort, pchError);
						LocalFree(pchError);
					}
					pDestObj->m_net.CloseConnection(pDestObj->m_socket);
					pDestObj->m_socket=NULL;
					pDestObj->m_bSending=false;

				}
				else
				if(
						(nNumSockets==0) // 0 = timed out, -1 = error
					||(!(FD_ISSET(pDestObj->m_socket, &fds)))
					) 
				{ 
					// no data, no problem, just return for now.
					pDestObj->m_bSending=false;
//		if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
			g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "keep_alive", "No data response on connection to %s:%d", pDestObj->m_pszServerName, pDestObj->m_usPort); // Sleep(250); //(Dispatch message)
//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "keep_alive", "No data response on connection to %s:%d", pDestObj->m_pszServerName, pDestObj->m_usPort); // Sleep(250); //(Dispatch message)
				} 
				else // there is recv data.
				{
					nHostReturn = pDestObj->m_net.GetLine(&pucBuffer, &ulBufferLen, pDestObj->m_socket);
					pDestObj->m_bSending=false;
					if((nHostReturn<=LIBRETTO_ERROR)||(pDestObj->m_socket==NULL))
					{
						_ftime(&pDestObj->m_timebLastDisconnect);

						int nErrorCode = WSAGetLastError();
						char* pchError = pDestObj->m_net.WinsockEnglish(nErrorCode);
						if(pchError)
						{
							if((ulBufferLen<=0)||(pucBuffer==NULL))
							{
//								if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC) 	
//								if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//									g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "keep_alive", "Connection to %s:%d terminated\n%s", 
									g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "keep_alive", "Connection to %s:%d terminated\n%s", 
										pDestObj->m_pszServerName, pDestObj->m_usPort, pchError?pchError:""); // Sleep(250); //(Dispatch message)

//								g_plibretto->SendMsg(CX_SENDMSG_ERROR, "keep_alive", "Connection to %s:%d terminated\n%s", pDestObj->m_pszServerName, pDestObj->m_usPort, pchError);
							}
							else
							{
//								if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC) 	
//								if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//									g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Communication error on %s:%d\n%s", 
									g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "Init connection", "Communication error on %s:%d\n%s", 
										pDestObj->m_pszServerName, pDestObj->m_usPort, pchError); // Sleep(250); //(Dispatch message)

//								g_plibretto->SendMsg(CX_SENDMSG_ERROR, "keep_alive", "Communication error on %s:%d\n%s", pDestObj->m_pszServerName, pDestObj->m_usPort, pchError);
								if(pucBuffer) free(pucBuffer);
							}
							LocalFree(pchError);
						}
						pDestObj->m_net.CloseConnection(pDestObj->m_socket);
						pDestObj->m_socket=NULL;
						pDestObj->m_bSending=false;
						
//		if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
			g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "keep_alive", "Error response on connection to %s:%d", pDestObj->m_pszServerName, pDestObj->m_usPort); // Sleep(250); //(Dispatch message)
	//		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "keep_alive", "Error response on connection to %s:%d", pDestObj->m_pszServerName, pDestObj->m_usPort); // Sleep(250); //(Dispatch message)
					}
					else
					{
if((g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC)||(g_psocketmodule->m_settings.m_bUseAsRunLog))
//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO)) 
	{
//unsigned long ulSend = buflen;
//char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_DELIM32BIT);
//AfxMessageBox(pchSnd);

		unsigned long ulSend = ulBufferLen;
		char* pchSnd = bu.ReadableHex((char*)pucBuffer, &ulSend, MODE_FULLHEX);

		g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "keep_alive", "received: %s", pchSnd?pchSnd:"(null)"); // Sleep(250); //(Dispatch message)
//		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "keep_alive", "received: %s", pchSnd?pchSnd:"(null)"); // Sleep(250); //(Dispatch message)

		if(pchSnd) free(pchSnd);
	}
							_ftime(&pDestObj->m_timebLastPing);

					}
					if(pucBuffer) free(pucBuffer);
				}
			}
			else
			{
				pDestObj->m_bSending=false;

//		if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "keep_alive", "Error %d on connection to %s:%d, socket %d", pDestObj->m_pszServerName, pDestObj->m_usPort, nHostReturn, pDestObj->m_socket); // Sleep(250); //(Dispatch message)
		g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "keep_alive", "Error %d on connection to %s:%d, socket %d", pDestObj->m_pszServerName, pDestObj->m_usPort, nHostReturn, pDestObj->m_socket); // Sleep(250); //(Dispatch message)
				pDestObj->m_net.CloseConnection(pDestObj->m_socket);
				pDestObj->m_socket=NULL;
			}
		LeaveCriticalSection(&pDestObj->m_critComm);





								}
								else
								{
									//log the error
								}

							}	break;
						default:
							{
							}	break;
						}



					}


				} 
				else // there is recv data.
				{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
					//But, let there be some exclusion so we don't accidentally recv bytes meant for actual replies, somehow
					char buffer[4];
					if(!pDestObj->m_bSending)
					{
//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "recv on sock %d [%d]", pDestObj->m_socket, clock()); 
						int nSize = recv(pDestObj->m_socket, buffer, 1, 0);
//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "recv returned %d on sock %d [%d]", nSize, pDestObj->m_socket, clock()); 
						if((nSize==0)||(nSize==SOCKET_ERROR)) // disconnection.  otherwise, we messed up.
						{
							_ftime(&pDestObj->m_timebLastDisconnect);
							int nErrorCode = WSAGetLastError();
							char* pchError = pDestObj->m_net.WinsockEnglish(nErrorCode);
							if(pchError)
							{
//if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC) 	
//								if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
									g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "Libretto:debug", "Connection to %s:%d terminated\n%s", 
									//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Connection to %s:%d terminated\n%s", 
										pDestObj->m_pszServerName, pDestObj->m_usPort, pchError?pchError:""); // Sleep(250); //(Dispatch message)
							
				//				g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:chicken", "chicken returns %d", // Sleep(250); //(Dispatch message)

//								g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Connection monitor", "Connection to %s:%d terminated\n%s", pDestObj->m_pszServerName, pDestObj->m_usPort, pchError);
								LocalFree(pchError);
							}
							else
							{
//if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC) 	
//								if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
									g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", "Libretto:debug", "Connection to %s:%d terminated", 
	//								g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Connection to %s:%d terminated", 
										pDestObj->m_pszServerName, pDestObj->m_usPort); // Sleep(250); //(Dispatch message)

			//					g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Connection monitor", "Connection to %s:%d terminated", pDestObj->m_pszServerName, pDestObj->m_usPort);
							}
							pDestObj->m_net.CloseConnection(pDestObj->m_socket);
							pDestObj->m_socket=NULL;

// nah, let the next command just do it
							// ah ok, let it happen.
							// immediately re-try the connection
							if(!pDestObj->m_bConnThreadStarted)
							{
								if(_beginthread(LibrettoAsynchConnThread, 0, (void*)(pDestObj))==-1)
								{
								//error.
//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Error starting connection thread for server %s", pDestObj->m_pszServerName);//   Sleep(250);//(Dispatch message)
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, "Connection monitor", "Error starting connection thread for server %s", pDestObj->m_pszServerName);//   Sleep(250);//(Dispatch message)
								//**MSG
								}
							}

						}
					}
				}
			}
			nLastCheck = clock()+1000;  // changed from 1.5 sec to 1 sec.

			while((!g_bKillThread)&&(pDestObj)&&(!pDestObj->m_bKillMonThread)&&(clock()<nLastCheck))
			{
				Sleep(1);
			}

		}
		else
		{// socket is null, dont monitor
			if((!g_bKillThread)&&(pDestObj)&&(!pDestObj->m_bKillMonThread)&&(pDestObj->m_socket==NULL)&&(!pDestObj->m_bSending)&&(!pDestObj->m_bConnThreadStarted))
			{
				if(_beginthread(LibrettoAsynchConnThread, 0, (void*)(pDestObj))==-1)
				{
				//error.
g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, "Connection monitor", "Error starting connection thread for server %s", pDestObj->m_pszServerName);//   Sleep(250);//(Dispatch message)
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Connection monitor", "Error starting connection thread for server %s", pDestObj->m_pszServerName);//   Sleep(250);//(Dispatch message)
				//**MSG
				}


				nLastCheck = clock()+1500;
				while((!g_bKillThread)&&(pDestObj)&&(!pDestObj->m_bKillMonThread)&&(clock()<nLastCheck))
				{
					Sleep(1);
				}

			}
		}


		Sleep(1);
	}
	pDestObj->m_bMonThreadStarted = false;
	_endthread();

}

void LibrettoCommandQueueThread(void* pvArgs)
{
	CLibrettoDestinationObject* pDestObj = (CLibrettoDestinationObject*) pvArgs;
//	CLibrettoDestinationObject* pDestObj = new CLibrettoDestinationObject;
	if(pDestObj==NULL) return;
	if(g_psocketmodule==NULL) return;

//	pDestObj->m_pvExternal = new CSCTE104Util;
	if(pDestObj->m_pvExternal==NULL) return;
	CSCTE104Util* p104 = (CSCTE104Util*)pDestObj->m_pvExternal;

	char szSocketModuleSource[MAX_PATH]; 
	strcpy(szSocketModuleSource, "SocketModuleVANCHandler");

	pDestObj->m_bCommandQueueThreadStarted = true;

	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, "Beginning command queue thread for %s", pDestObj->m_pszServerName?pDestObj->m_pszServerName:"(null)");    //(Dispatch message)

//	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	char dberrorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");
	strcpy(dberrorstring, "");
//	CDBUtil db;
	CBufferUtil bu;

	CString szPayload;
	CString szLastPayload;
	bool bEmbed = false;
	unsigned long ulEmbeddingThread = 0xffffffff;

	unsigned long ulLen = 0;
	char* buffer = NULL;

	void* pm = NULL;
	BOOL bSingle = FALSE;

	CSCTE104MultiMessage* mm = NULL;
//				pm = u.CreateMultiMessageObject(2, 0);  // Fox has splice request and dtmf descriptor, and time type 0
	pm = p104->CreateMultiMessageObject(2, 0);  // CNBC has time signal plus seg descriptor
	mm = ((CSCTE104MultiMessage*)pm);
	mm->m_scte104Hdr.NumberOfOps = 0x02;
	
	mm->m_ppscte104Ops[0]->OpId1High = 	0x01; //		opID [high order byte]						0x0101 splice_request_data
	mm->m_ppscte104Ops[0]->OpId1Low = 0x04;		//		opID [low order byte]             0x0104 time_signal_request_data
	mm->m_ppscte104Ops[0]->OpDataLengthHigh = 0x00;			// xx		data_length [high order byte]			
	mm->m_ppscte104Ops[0]->OpDataLengthLow  = 0x02;			// xx		data_length [low order byte]			

	mm->m_ppscte104Ops[0]->proprietary_data_ptr = new unsigned char[sizeof(SCTE104_time_signal_request_data_t)]; // space for the ID.  max 256
//AfxMessageBox("HERE 1");

//			SCTE104_splice_request_data_t* psrd = (SCTE104_splice_request_data_t*) mm->m_ppscte104Ops[0]->proprietary_data_ptr;
	SCTE104_time_signal_request_data_t* ptsrd = (SCTE104_time_signal_request_data_t*) mm->m_ppscte104Ops[0]->proprietary_data_ptr;
//AfxMessageBox("HERE 2");

	if(mm->m_ppscte104Ops[0]->proprietary_data_ptr)
	{
		memset(mm->m_ppscte104Ops[0]->proprietary_data_ptr, 0x00, 0x02); // nulls
	}


	mm->m_ppscte104Ops[1]->OpId1High = 	0x01; //		opID [high order byte]						0x0101 splice_request_data
	mm->m_ppscte104Ops[1]->OpId1Low = 0x0b;		//		opID [low order byte]             0x010b insert_segmentation_descriptor_request_data
	mm->m_ppscte104Ops[1]->OpDataLengthHigh = 0x00;			// xx		data_length [high order byte]			
	mm->m_ppscte104Ops[1]->OpDataLengthLow  = sizeof(SCTE104_insert_segmentation_descriptor_request_data_t);		// xx		data_length [low order byte]			

	mm->m_ppscte104Ops[1]->proprietary_data_ptr = new unsigned char[sizeof(SCTE104_insert_segmentation_descriptor_request_data_t)+256]; // space for the ID.  max 256
//AfxMessageBox("HERE 1");

//			SCTE104_splice_request_data_t* psrd = (SCTE104_splice_request_data_t*) mm->m_ppscte104Ops[0]->proprietary_data_ptr;
	SCTE104_insert_segmentation_descriptor_request_data_t* psdrd = (SCTE104_insert_segmentation_descriptor_request_data_t*) mm->m_ppscte104Ops[1]->proprietary_data_ptr;
//AfxMessageBox("HERE 2");

	if(mm->m_ppscte104Ops[1]->proprietary_data_ptr)
	{
		memset(mm->m_ppscte104Ops[1]->proprietary_data_ptr, 0x00, sizeof(SCTE104_insert_segmentation_descriptor_request_data_t)+256); // nulls
	}


	unsigned long ul_splice_event_id=1;
	unsigned short usCommand=0x0000;


/*
	unsigned long ul_splice_event_id=m_ul_splice_event_id;
	if(ul_splice_event_id<0xffffffff) ul_splice_event_id++;
	else ul_splice_event_id = 1; // cycle around.

	unsigned short us_unique_program_id=m_us_unique_program_id;
	if(us_unique_program_id<0xffff) us_unique_program_id++;
	else us_unique_program_id = 1; // cycle around.

	if(psdrd)
	{
			psdrd->segmentation_event_id[0] = (unsigned char)((ul_splice_event_id&0x0f000000)>>24) | 0x20;    // most significant bit first
			psdrd->segmentation_event_id[1] = (unsigned char)((ul_splice_event_id&0x00ff0000)>>16);    // most significant bit first
			psdrd->segmentation_event_id[2] = (unsigned char)((ul_splice_event_id&0x0000ff00)>>8);    // most significant bit first
			psdrd->segmentation_event_id[3] = (unsigned char)((ul_splice_event_id&0x000000ff));    // most significant bit first
	}
*/


	while((!g_bKillThread)&&(!pDestObj->m_bKillCommandQueueThread))
	{
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "%d %d %d %d %d.%03d", g_plibretto->m_data.m_nIndexAutomationEndpoint, g_plibretto->m_settings.m_nNumEndpointsInstalled, g_plibretto->m_settings.m_ppEndpointObject, g_plibretto->m_settings.m_ppEndpointObject[g_plibretto->m_data.m_nIndexAutomationEndpoint], g_plibretto->m_data.m_timebTick.time, g_plibretto->m_data.m_timebTick.millitm);   Sleep(50);//(Dispatch message)



		if(
//			  (!g_plibretto->m_data.m_bProcessSuspended)
//			&&(g_plibretto->m_settings.m_bEnableCommandQueue) // must be enabled
	//		&&
			(g_psocketmodule->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!g_psocketmodule->m_data.m_key.m_bExpires)
				||((g_psocketmodule->m_data.m_key.m_bExpires)&&(!g_psocketmodule->m_data.m_key.m_bExpired))
				||((g_psocketmodule->m_data.m_key.m_bExpires)&&(g_psocketmodule->m_data.m_key.m_bExpireForgiveness)&&(g_psocketmodule->m_data.m_key.m_ulExpiryDate+g_psocketmodule->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!g_psocketmodule->m_data.m_key.m_bMachineSpecific)
				||((g_psocketmodule->m_data.m_key.m_bMachineSpecific)&&(g_psocketmodule->m_data.m_key.m_bValidMAC))
				)

			)
		{



			// check embedding params to see what to do

			EnterCriticalSection(&g_psocketmodule->m_data.m_critEmbed);

			if(g_psocketmodule->m_data.m_ulEmbeddingThread != 0xffffffff)
			{
				bEmbed = true;
				ulEmbeddingThread = g_psocketmodule->m_data.m_ulEmbeddingThread;
				szPayload = g_psocketmodule->m_data.m_szLastPayload;  // global insertion payload
			}
			else
			{
				ulEmbeddingThread = 0xffffffff;
				bEmbed = false;
			}
			LeaveCriticalSection(&g_psocketmodule->m_data.m_critEmbed);


			if(bEmbed)
			{

				if((szLastPayload.Compare(szPayload))||(buffer==NULL))
				{
					// update message contents.

					if(mm->m_ppscte104Ops[1]->proprietary_data_ptr)
					{
						memset(mm->m_ppscte104Ops[1]->proprietary_data_ptr, 0x00, sizeof(SCTE104_insert_segmentation_descriptor_request_data_t)+256); // nulls
					}

					szLastPayload = szPayload;
/*
					if(ptsrd)
					{
						CString szText;
						int n;
						GetDlgItem(IDC_EDIT_START)->GetWindowText(szText);
						n = atol(szText);

						ptsrd->pre_roll_time[0] = (unsigned char)((n&0x0000ff00)>>8);
						ptsrd->pre_roll_time[1] = (unsigned char)((n&0x000000ff));
					}
*/
					
					if(psdrd)
					{
						psdrd->segmentation_upid_type = 0x0c;  //valid for everything preceding var len ID!

						psdrd->segmentation_upid_length = (unsigned char)szLastPayload.GetLength()+4;
						char pbuffer[256];

						unsigned long MPU = bu.xtol(g_psocketmodule->m_settings.m_pszMPUID, 8);// has to be 8 chars.

						sprintf(pbuffer, "%c%c%c%c%s", 
							(unsigned char)(MPU>>24),
							(unsigned char)(MPU>>16), 
							(unsigned char)(MPU>>8), 
							(unsigned char)(MPU), 
							szLastPayload);
						memcpy(&(psdrd->segmentation_upid_length)+1, pbuffer, psdrd->segmentation_upid_length);


	mm->m_ppscte104Ops[1]->OpDataLengthHigh = 0x00;			// xx		data_length [high order byte]			
	mm->m_ppscte104Ops[1]->OpDataLengthLow  = sizeof(SCTE104_insert_segmentation_descriptor_request_data_t);		// xx		data_length [low order byte]			

						usCommand = mm->m_ppscte104Ops[1]->OpDataLengthHigh & 0x00ff;
						usCommand <<= 8; usCommand += (mm->m_ppscte104Ops[1]->OpDataLengthLow & 0x00ff);
						usCommand += psdrd->segmentation_upid_length;

	mm->m_ppscte104Ops[1]->OpDataLengthLow  = (usCommand&0xff);		// xx		data_length [low order byte]			
	mm->m_ppscte104Ops[1]->OpDataLengthHigh = ((usCommand>>8)&0xff);			// xx		data_length [high order byte]			

					}

					mm->UpdateDataLengths();
					buffer = (char*) p104->ReturnMessageBuffer(mm);
				}



/// libretto send


				if(ul_splice_event_id<0xffffffff) ul_splice_event_id++;
				else ul_splice_event_id = 1; // cycle around.

				if(psdrd)
				{
						psdrd->segmentation_event_id[0] = (unsigned char)((ul_splice_event_id&0x0f000000)>>24);// | 0x20;    // most significant bit first
						psdrd->segmentation_event_id[1] = (unsigned char)((ul_splice_event_id&0x00ff0000)>>16);    // most significant bit first
						psdrd->segmentation_event_id[2] = (unsigned char)((ul_splice_event_id&0x0000ff00)>>8);    // most significant bit first
						psdrd->segmentation_event_id[3] = (unsigned char)((ul_splice_event_id&0x000000ff));    // most significant bit first
				}


									char chEolnType = EOLN_CRLF;// chyron type default
									

//									if(pDestObj->m_usType == CX_DESTTYPE_EVERTZ_7847FSE)
//									{
										chEolnType = EOLN_NONE; // do not send CRLF
										// convert the buffer.  use the destination specific  buffer util obj
//										pDestObj->m_bu.Base64Decode(&buffer, &ulLen, false); // true frees the incoming buffer.  buffer and length are updated
										// replace time code and message number
//											nCommand = 0x0000ffff&((((CSCTE104SingleMessage*)buffer)->OpId1High<<8)|(((CSCTE104SingleMessage*)buffer)->OpId1Low));
										usCommand = buffer[0] & 0x00ff;
										usCommand <<= 8; usCommand += (buffer[1] & 0x00ff);
//									}


										ulLen = mm->m_scte104Hdr.MsgSizeHigh & 0x00ff;
										ulLen <<= 8; ulLen += (mm->m_scte104Hdr.MsgSizeLow & 0x00ff);

	
		//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "*** usCommand calc = %04x, buf0=%02x  buf1=%02x", usCommand, buffer[0],buffer[1]);  //Sleep(250); //(Dispatch message)

										
//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "start send now"); // Sleep(250); //(Dispatch message)

// added repititions
	int nRepeats=0;
	do
	{

//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "%d %04x", pDestObj->m_usType, usCommand); // Sleep(250); //(Dispatch message)

									if(pDestObj->m_usType == CX_DESTTYPE_EVERTZ_7847FSE)
									{
					//					CSCTE104Util* p104 = (CSCTE104Util*)pDestObj->m_pvExternal;   already defined above
										if(usCommand == 0xffff) // multi message
										{
											buffer[6] = (nRepeats==0)?p104->IncrementMessageNumber():p104->m_ucLastMessageNumber;

							//				CString Q;
							//				Q.Format("MULTI %04x",  nCommand);
							//				AfxMessageBox(Q);

#ifdef NOT_USING_TIME_TYPE_2
											if(buffer[10] == 0x02) // only if time type 2, replace time code
											{
												// multi obj
												SCTE104HMSF_t HMSF;

	//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
	//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "*x* TC %d %d", (g_plibretto->m_settings.m_bUseTimeCode), (g_plibretto->m_data.m_bTimeCodeThreadStarted)); // Sleep(250); //(Dispatch message)

												// get timecode if installed.
												bool bSysClock = false;
/*
												if((g_plibretto->m_settings.m_bUseTimeCode)&&(g_plibretto->m_data.m_bTimeCodeThreadStarted))
												{

	EnterCriticalSection(&g_plibretto->m_data.m_critTimeCode);
													unsigned long ulTimeCode = g_plibretto->m_data.m_ulTimeCode;
													unsigned long ulTimeCodeElapsed = g_plibretto->m_data.m_ulTimeCodeElapsed;
	LeaveCriticalSection(&g_plibretto->m_data.m_critTimeCode);
	//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
	//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "*-* TC %08x", ulTimeCode); // Sleep(250); //(Dispatch message)
													if(ulTimeCodeElapsed>(unsigned long)g_plibretto->m_settings.m_nSyncThresholdMS) // worse than the threshold. could just use the sys clock, which is close....
													{
														// should do something if elapsed, but not going to... will chalk it up to disconnected TC card.
														bSysClock=true;
	//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
	//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "*** TC elapsed %d %d", ulTimeCodeElapsed, g_plibretto->m_settings.m_nSyncThresholdMS); // Sleep(250); //(Dispatch message)
													}
													else
													{
														if(ulTimeCode != 0xffffffff)
														{
															unsigned char ucTempTC = (unsigned char)((ulTimeCode&0xff000000)>>24); 
															HMSF.hours = (unsigned char)((ucTempTC/16)*10	+ (ucTempTC%16));
															
															ucTempTC = (unsigned char)((ulTimeCode&0x00ff0000)>>16); 
															HMSF.minutes = (unsigned char)((ucTempTC/16)*10	+ (ucTempTC%16));

															ucTempTC = (unsigned char)((ulTimeCode&0x0000ff00)>>8); 
															HMSF.seconds = (unsigned char)((ucTempTC/16)*10	+ (ucTempTC%16));
							
															ucTempTC = (unsigned char)(ulTimeCode&0x000000ff);
															HMSF.frames = (unsigned char)((ucTempTC/16)*10	+ (ucTempTC%16));

	//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
	//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "*** %02d%02d%02d%02d", HMSF.hours,HMSF.minutes,HMSF.seconds,HMSF.frames); // Sleep(250); //(Dispatch message)
														}	else bSysClock=true;
													}
												}
												else bSysClock=true;
*/
												if(1)//bSysClock)
												{
													_timeb timestamp;
													_ftime(&timestamp);

													if(g_psocketmodule->m_settings.m_bDF)
													{
														// fill the data with drop frame
														int rem = (timestamp.time- (timestamp.timezone*60)+(timestamp.dstflag?3600:0));
														while(rem<0) rem+=86400;
														while(rem>86399) rem-=86400;
														int ms = (rem%86400)*1000 + timestamp.millitm;
	/*
														double ms = (rem%86400)*1000 + timestamp.millitm;
														int fr = (int)(((ms * 30.0) / 1001.0) + 0.5);
														int deci = (int)(fr / 17982);
														rem = fr % 17982;
														if (rem >= 1800) rem += 2 + ((rem - 1800) / 1798) * 2; 

														HMSF.hours = deci / 6; 
														HMSF.minutes = ((deci % 6) * 10) + (rem / 1800); 
														HMSF.seconds = (rem % 1800) / 30; 
														HMSF.frames = rem % 30;
	*/
														int nHours;
														int nMinutes;
														int nSeconds;
														int nFrames;

														pDestObj->m_timeconv.MStoHMSF(ms, &nHours, &nMinutes, &nSeconds, &nFrames); // default to ntsc df
														HMSF.hours		= (unsigned char)(nHours&0x000000ff);
														HMSF.minutes	= (unsigned char)(nMinutes&0x000000ff);
														HMSF.seconds	= (unsigned char)(nSeconds&0x000000ff);
														HMSF.frames		= (unsigned char)(nFrames&0x000000ff);
													}
													else
													{
														timestamp.time %= 86400; //remains of the day!
														HMSF.hours   = (unsigned char)(((timestamp.time/3600)%24)&0x000000ff);
														HMSF.minutes = (unsigned char)(((timestamp.time/60)%60)&0x000000ff);
														HMSF.seconds = (unsigned char)(((timestamp.time)%60)&0x000000ff);
														HMSF.frames  = (unsigned char)((timestamp.millitm/g_plibretto->m_settings.m_nFrameRate)&0x000000ff);
													}
	//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
	//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "*** sys %02x%02x%02x%02x", HMSF.hours,HMSF.minutes,HMSF.seconds,HMSF.frames); // Sleep(250); //(Dispatch message)
												}

												buffer[10] = 0x02;
												buffer[11] = HMSF.hours;
												buffer[12] = HMSF.minutes;
												buffer[13] = HMSF.seconds;
												buffer[14] = HMSF.frames;
											}  // if time type == 0x02 only

#endif //#ifdef NOT_USING_TIME_TYPE_2

										}
										else
										{
											if(nRepeats==0)
											{
												buffer[10] = p104->IncrementSecondaryMessageNumber();
											}
											else
											{
											
												int rv = p104->m_ucLastMessageNumber + p104->m_ucLastSecondaryMessageNumberInc + 128 - (p104->m_ucSecondaryMessageNumberofValues/2); // small loop averaging 180 degree phase on primary message number
												while(rv>255)	rv-=256; // loop around.
												buffer[10] = (char)rv;
											}


											// need to create a message number that starts at p104->m_ucLastMessageNumber + 128 and increments from there with a secondary incrementer
							//				CString Q;
							//				Q.Format("SINGLE %04x", nCommand);
							//				AfxMessageBox(Q);
							/*
											if( nCommand == 0x0001) // init message
											{
																	AfxMessageBox("init msg");
											}
											else
											if( nCommand == 0x0003) // keep_alive message
											{
																AfxMessageBox("keep_alive msg");
											}
								*/		
											// single obj
										}

									}

									if(pDestObj->m_usType == CX_DESTTYPE_EVERTZ_7847FSE)
									{

		unsigned long ulSend = ulLen;
		char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_FULLHEX);


	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, "asrun,log", szSocketModuleSource, "sending %d bytes (r=%d): %s", ulLen, nRepeats, pchSnd);   //(Dispatch message)


//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO|LIBRETTO_DEBUG_DEVCOMM)) 
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "sending %d bytes (r=%d): %s", ulLen, nRepeats, pchSnd); // Sleep(250); //(Dispatch message)

		if(pchSnd) free(pchSnd);
									}
/*
									else
									{
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO|LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "sending: %s", buffer); // Sleep(250); //(Dispatch message)

	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, "asrun,log", szSocketModuleSource, "sending %d bytes (r=%d): %s", ulLen, nRepeats, pchSnd);   //(Dispatch message)
									
									
									}
*/

	EnterCriticalSection(&pDestObj->m_critComm);
									pDestObj->m_bSending=true;
									int nReturn = pDestObj->SendTelnetCommand(buffer, ulLen, chEolnType, szSocketModuleSource);
									bool bLastCycle = (
																			(g_psocketmodule->m_settings.m_nCommandRepetitions<=0) 
																		||((g_psocketmodule->m_settings.m_nCommandRepetitions>0)&&(nRepeats>=g_psocketmodule->m_settings.m_nCommandRepetitions)) //i.e. last one before increment.
																		);
/*
									if(bLastCycle)
									{
										szLocal.ReleaseBuffer();
									}

*/
									if(nReturn<LIBRETTO_SUCCESS)
									{
											pDestObj->m_bSending=false;
	LeaveCriticalSection(&pDestObj->m_critComm);
										//**MSG
//										g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:telnet", 
//											"telnet command not successful on %s.", pDestObj->m_pszServerName );//   Sleep(50);//(Dispatch message)
	g_psocketmodule->m_msgr.DM(MSG_ICONERROR, "asrun,log", szSocketModuleSource, 
											"Command not successful on %s.", pDestObj->m_pszServerName );//   Sleep(50);//(Dispatch message)

/*
										if((nEventItemID>0)&&(bLastCycle))
										{
											if(plqo)
											{
												plqo->szMessage.Format("E256:command not successful on %s.", pDestObj->m_pszServerName );
												plqo->nAction=128;
												plqo->dblTimestamp = 5000000000.0;

												EnterCriticalSection(&pDestObj->m_critMemQueue);
												nMemQueueIndex = pDestObj->AddMemQueue(plqo);
												LeaveCriticalSection(&pDestObj->m_critMemQueue);
												if(nMemQueueIndex<0)
												{
													try
													{
														delete plqo;
													}
													catch(...)
													{
													}
												}

											}
											else
											if(pdbConn)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E256:command not successful on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d;",  //HARDCODE
													((g_plibretto->m_settings.m_pszCommandQueue)&&(strlen(g_plibretto->m_settings.m_pszCommandQueue)))?g_plibretto->m_settings.m_pszCommandQueue:"Command_Queue",
													pDestObj->m_pszServerName, nItemID
													);

												if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "ERROR executing SQL: %s", errorstring);//  Sleep(250); //(Dispatch message)

												}
											}
											continue; //return LIBRETTO_ERROR;  // because we dont want to increment counter.  // there is no counter.
											// we also dont want to remove the item at the end of this function
										}
*/

									}
									else
									{
										//while(!g_miranda.m_bTransferring) Sleep(1);  //not good, in case error

//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "%s on %s succeeded", szLocal, pchServer);  //Sleep(250); //(Dispatch message)
										// we have queued the xfer, lets update the thing with a status, then exit without removing
										char pchFinalBuffer[256];
										unsigned char* pucBuffer = NULL;
										unsigned long ulBufferLen = 256;
										nReturn = pDestObj->m_net.GetLine(&pucBuffer, &ulBufferLen, pDestObj->m_socket, NET_RCV_ONCE, errorstring);
										
											pDestObj->m_bSending=false;
	LeaveCriticalSection(&pDestObj->m_critComm);


										if(nReturn>=NET_SUCCESS) _ftime(&pDestObj->m_timebLastPing);

/*
										bool bEventID = ( 
																			(nEventItemID>0)
																		&&(
																				(g_plibretto->m_settings.m_nCommandRepetitions<=0) 
																			||((g_plibretto->m_settings.m_nCommandRepetitions>0)&&(nRepeats>=g_plibretto->m_settings.m_nCommandRepetitions)) //i.e. last one before increment.
																			)
																		);

										
										if(
												(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO|LIBRETTO_DEBUG_DEVCOMM)) 
											||((nEventItemID>0)&&(bLastCycle))
											)
										{
*/
											if((nReturn<NET_SUCCESS)||(ulBufferLen==0))
											{
												_snprintf(pchFinalBuffer, 256, "%s: (%d) %s", ((nReturn<NET_SUCCESS)?"error":"no buffer"), nReturn, errorstring);
											}
											else
											{

												if(pDestObj->m_usType == CX_DESTTYPE_EVERTZ_7847FSE)
												{

					unsigned long ulRecv = ulBufferLen;
					char* pchRcv = bu.ReadableHex((char*)pucBuffer, &ulRecv, MODE_FULLHEX);


													_snprintf(pchFinalBuffer, 256, "%s", (pchRcv?((char*)pchRcv):"(null)"));

//			if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO|LIBRETTO_DEBUG_DEVCOMM)) 
			g_psocketmodule->m_msgr.DM(MSG_ICONHAND, "asrun,log", szSocketModuleSource, "received %d bytes (r=%d): %s", ulBufferLen, nRepeats, pchRcv); // Sleep(250); //(Dispatch message)

					if(pchRcv) free(pchRcv);



					// here we check if the buffer is a success code, if so we dispatch and async message.

													if(pucBuffer) 
													{
														if(*(pucBuffer+5) == 0x64)  //single message return code.
														{

															_ftime(&pDestObj->m_timebLastPing);  // reset the timer for pinginess, on success only

															CSocketModuleAsyncMessageObject* pObj =  new CSocketModuleAsyncMessageObject;
															if(pObj)
															{
																pObj->m_ulThreadID = ulEmbeddingThread; 
																pObj->m_bDefeat = false; //  acking here so defeat async ack for this thread

/*
?	Every VANC insertion 
?	Server sends the following which includes the Payload & the unique Tag/Count inserted in the VANC, this is sent to every client.
?	{"Uid": "0xffffffff", "Command": "EmbedDataVANC",  "Payload": {"SponTag": "1","GraphicTag": "34" }, "VANCTag": "Tag" }
*/


																sprintf(pObj->m_pchJSON, "{ \"Uid\":\"0x%08x\", \"Command\":\"EmbedDataVANC\", \"Payload\":%s, \"VANCTag\": \"0x%08x\" }", 
																	ulEmbeddingThread,
																	szLastPayload,
																	ul_splice_event_id
																	);	// hard limit on field size

																_ftime(&pObj->m_tmbQueueTime);
																g_psocketmodule->m_data.AddAsyncMessage(pObj);
															}


														}
														else
														{
			g_psocketmodule->m_msgr.DM(MSG_ICONERROR, "asrun,log", szSocketModuleSource, "Received error code from VANC"); // Sleep(250); //(Dispatch message)

														}
													}
												}
/*
												else
												{
													_snprintf(pchFinalBuffer, 256, "%s", (pucBuffer?((char*)pucBuffer):"(null)"));
//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO|LIBRETTO_DEBUG_DEVCOMM)) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "received %s", pchFinalBuffer); // Sleep(250); //(Dispatch message)
												}
*/
											}


										//}
										if(pucBuffer) free(pucBuffer);

									}
// dont need this anymore..
//									pDestObj->m_bSending=false;
//	LeaveCriticalSection(&pDestObj->m_critComm);
										nRepeats++;


									// add intervanc delay here

										if(g_psocketmodule->m_settings.m_nVANCCommandDelay>0)
										{
											int nWait =  clock()+g_psocketmodule->m_settings.m_nVANCCommandDelay;


											while((!g_bKillThread)&&(!pDestObj->m_bKillCommandQueueThread)&&(clock()<nWait))
											{
												Sleep(1);
											}
										}

		} while((g_psocketmodule->m_settings.m_nCommandRepetitions>0)&&(nRepeats<=g_psocketmodule->m_settings.m_nCommandRepetitions)&&(pDestObj->m_socket)); 
		// had to add this while for repetitions.

//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "end send"); // Sleep(250); //(Dispatch message)

			}
			else
				Sleep(1);
		}
		else
		{
			Sleep(100);
		}
	
//		Sleep(1); // dont peg processor
	} 

	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, "Ending command queue thread for %s", pDestObj->m_pszServerName?pDestObj->m_pszServerName:"(null)");    //(Dispatch message)

	Sleep(50);//(Dispatch message)

	pDestObj->m_bCommandQueueThreadStarted=false;

	_endthread();
	Sleep(150);

}

//#endif // #ifdef REMOVED_FOR_NOW_20151028




