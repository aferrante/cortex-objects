// SocketModuleDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(SOCKETMODULEDEFINES_H_INCLUDED)
#define SOCKETMODULEDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define SOCKETMODULE_CURRENT_VERSION		"1.0.1.2"


// modes
#define SOCKETMODULE_MODE_DEFAULT			0x00000000  // exclusive
#define SOCKETMODULE_MODE_LISTENER		0x00000001  // exclusive
#define SOCKETMODULE_MODE_CLONE				0x00000002  // exclusive
#define SOCKETMODULE_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define SOCKETMODULE_MODE_VOLATILE		0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define SOCKETMODULE_MODE_MASK				0x0000000f  // 

// default port values.
//#define SOCKETMODULE_PORT_FILE				80		
#define SOCKETMODULE_PORT_CMD					30000		
#define SOCKETMODULE_PORT_STATUS			10540 //default to MOS

#define SOCKETMODULE_PORT_INVALID			0	

#define SOCKETMODULE_FLAGS_INVALID			0xffffffff	 
#define SOCKETMODULE_FLAG_DISABLED			0x0000	 // default
#define SOCKETMODULE_FLAG_ENABLED			0x0001	
#define SOCKETMODULE_FLAG_FOUND				0x1000

#define SOCKETMODULE_FLAG_TRIGGERED			0x01000000
#define SOCKETMODULE_FLAG_ANALYZED				0x00000010
#define SOCKETMODULE_FLAG_REANALYZED			0x00000020

#define SOCKETMODULE_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded

// status
#define SOCKETMODULE_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define SOCKETMODULE_STATUS_UNKNOWN							0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define SOCKETMODULE_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define SOCKETMODULE_STATUS_ERROR								0x00000020  // error (red icon)
#define SOCKETMODULE_STATUS_CONN								0x00000030  // ready (green icon)	
#define SOCKETMODULE_STATUS_OK									0x00000030  // ready (green icon)	
#define SOCKETMODULE_STATUS_RUN									0x00000040  // in progress, running, owned etc (blue icon);	
#define SOCKETMODULE_ICON_MASK									0x00000070  // mask	

#define SOCKETMODULE_STATUS_SUSPEND							0x00000080  // suspended	(yellow icon please)

#define SOCKETMODULE_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define SOCKETMODULE_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define SOCKETMODULE_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define SOCKETMODULE_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define SOCKETMODULE_STATUS_CMDSVR_MASK					0x00007000  // command server mask bits

#define SOCKETMODULE_STATUS_STATUSSVR_START			0x00010000  // starting the status server
#define SOCKETMODULE_STATUS_STATUSSVR_RUN				0x00020000  // status server running
#define SOCKETMODULE_STATUS_STATUSSVR_END				0x00030000  // status server shutting down
#define SOCKETMODULE_STATUS_STATUSSVR_ERROR			0x00040000  // status server error
#define SOCKETMODULE_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define SOCKETMODULE_STATUS_THREAD_START				0x00100000  // starting the main thread
#define SOCKETMODULE_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define SOCKETMODULE_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define SOCKETMODULE_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define SOCKETMODULE_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define SOCKETMODULE_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define SOCKETMODULE_STATUS_THREAD_MASK					0x00f00000  // main thread mask bits

// various failures...
#define SOCKETMODULE_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define SOCKETMODULE_STATUS_FAIL_DB							0x20000000  // could not get DB
#define SOCKETMODULE_STATUS_FAIL_MASK						0xf0000000  // failure code mask bits

//return values
#define SOCKETMODULE_SUCCESS   0
#define SOCKETMODULE_ERROR	   -1


// commands
#define SOCKETMODULE_CMD_INSERT	   0xa0
#define SOCKETMODULE_CMD_DELETE	   0xa1
#define SOCKETMODULE_CMD_GET		   0xa2
#define SOCKETMODULE_CMD_LOCK		   0xa3
#define SOCKETMODULE_CMD_MOVE		   0xa4
#define SOCKETMODULE_CMD_MOD		   0xa5
#define SOCKETMODULE_CMD_SWAP		   0xa6
#define SOCKETMODULE_CMD_FIND		   0xa7  // just return postion in list
#define SOCKETMODULE_CMD_DEL_ID	   0xa8  // delete by list of IDs, not index

// subcommands
#define SOCKETMODULE_SCMD_TIESEC	 0x08 // tie secondaries to primaries in the command
#define SOCKETMODULE_SCMD_NOSEC	   0x00 // explicit command, do NOT tie secondaries to primaries in the command
#define SOCKETMODULE_SCMD_UNLOCK   0x08
#define SOCKETMODULE_SCMD_LOCK		 0x00

#define SOCKETMODULE_CMD_QUEUE		   0xc0 // puts thing in queue
#define SOCKETMODULE_CMD_CMDQUEUE		 0xc1 // puts thing in command queue
#define SOCKETMODULE_CMD_DIRECT			 0xc2 // skips queues, commands box directly

#define SOCKETMODULE_CMD_EXSET			 0xea // increments exchange counter
#define SOCKETMODULE_CMD_EXGET			 0xeb // gets exchange mod value
#define SOCKETMODULE_CMD_MODSET			 0xec // sets exchange mod value, skips exchange table





// default filenames
#define SOCKETMODULE_SETTINGS_FILE_SETTINGS	  "socketmodule.csr"		// csr = cortex settings redirect
#define SOCKETMODULE_SETTINGS_FILE_DEFAULT	  "socketmodule.csf"		// csf = cortex settings file

// debug defines
#define SOCKETMODULE_DEBUG_TRIGGER				0x00000001  // logs keycodes
#define SOCKETMODULE_DEBUG_ASRUN					0x00000002
#define SOCKETMODULE_DEBUG_RULES					0x00000004
#define SOCKETMODULE_DEBUG_PARAMS				0x00000008
#define SOCKETMODULE_DEBUG_PARAMPARSE		0x00000010
#define SOCKETMODULE_DEBUG_TIMING				0x00000020
#define SOCKETMODULE_DEBUG_EVENTRULE			0x00000040
#define SOCKETMODULE_DEBUG_PROCESS				0x00000080
#define SOCKETMODULE_DEBUG_CHANNELS			0x00000100
#define SOCKETMODULE_DEBUG_COMM					0x00000200
#define SOCKETMODULE_DEBUG_VANC					0x00000400


#endif // !defined(SOCKETMODULEDEFINES_H_INCLUDED)
