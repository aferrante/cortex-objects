// SocketModuleData.cpp: implementation of the CSocketModuleData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <process.h>
#include "SocketModule.h"
#include "SocketModuleHandler.h" 
#include "SocketModuleMain.h" 
#include "SocketModuleData.h"
//#include "..\Archivist\ArchivistDefines.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CSocketModuleMain* g_psocketmodule;
extern CSocketModuleApp theApp;

extern void SocketModuleAutomationThread(void* pvArgs);
//extern void SocketModuleFarAnalysisThread(void* pvArgs);
extern void SocketModuleNearAnalysisThread(void* pvArgs);
extern void SocketModuleTriggerThread(void* pvArgs);


//extern CADC g_adc; 	// the Harris ADC object

/*
//////////////////////////////////////////////////////////////////////
// CSocketModuleConnectionObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSocketModuleConnectionObject::CSocketModuleConnectionObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_pszClientName = NULL;
	m_ulStatus	= SOCKETMODULE_STATUS_UNINIT;
	m_ulFlags = SOCKETMODULE_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
//	m_pAPIConn = NULL;
	m_bKillConnThread = true;
	m_bConnThreadStarted = false;

}

CSocketModuleConnectionObject::~CSocketModuleConnectionObject()
{
	m_bKillConnThread = true;
	while(	m_bConnThreadStarted ) Sleep(1);

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
	if(m_pszClientName) free(m_pszClientName); // must use malloc to allocate
}

*/



//////////////////////////////////////////////////////////////////////
// CSocketModuleAsyncMessageObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSocketModuleAsyncMessageObject::CSocketModuleAsyncMessageObject()
{
	m_ulCmdID = 0xffffffff;
	m_ulThreadID = 0xffffffff;
	m_bDefeat = false;
//	m_pchJSON = NULL;
	strcpy(m_pchJSON,"");	
}

CSocketModuleAsyncMessageObject::~CSocketModuleAsyncMessageObject()
{
//	if(m_pchJSON) free(m_pchJSON); // must use malloc to allocate
}


//////////////////////////////////////////////////////////////////////
// CSocketModuleMappingObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSocketModuleMappingObject::CSocketModuleMappingObject()
{
	m_nKeyCode = -1;
	m_nSysChars = 0;
	m_bActive = false;
	m_bRequireAppFocus = false;
	m_bFailed = false;

	m_tmbTransmitTime.time = 0;
	m_tmbTransmitTime.millitm = 0;
}

CSocketModuleMappingObject::~CSocketModuleMappingObject()
{
//	if(m_pszFieldName) free(m_pszFieldName); // must use malloc to allocate
}

//////////////////////////////////////////////////////////////////////
// CSocketModuleData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSocketModuleData::CSocketModuleData()
{
	m_bNetClientConnected = false;
	m_socket = NULL;

	m_nRole=-1;

	InitializeCriticalSection(&m_critText);
	InitializeCriticalSection(&m_critMaps);
	InitializeCriticalSection(&m_critAsRun);
	InitializeCriticalSection(&m_critSocket);
	InitializeCriticalSection(&m_critLastCommand);
	InitializeCriticalSection(&m_critAsyncMessage);
	InitializeCriticalSection(&m_critHistory);
	InitializeCriticalSection(&m_critEmbed);
	

	m_szDisplay = "";

	m_pucLastCommand=NULL;	
	m_ulLastCommandLength=0;	
	m_ulLastCommandTime=0;	
	m_ulLastKeepAlive = 0;
	m_ulLastParsedCommandTime = 1;


	// messaging...
	m_bNetworkMessagingInitialized=false;

	m_pLastObj = NULL;// start out null; new CSocketModuleMappingObject;

/*
	m_ppEventsList=NULL;
	m_nNumEventsList=0;
	m_ppEventsNewList=NULL;
	m_nNumEventsNewList=0;
	m_ppEventsPlaying=NULL;
	m_nNumEventsPlaying=0;
*/

//	m_ppRulesObj = NULL;
//	m_nNumRulesObjects = 0;
	m_ppMappingObj = NULL;
	m_nNumMappingObjects = 0;
	m_ppAsRunObj = NULL;
	m_nNumAsRunObjects = 0;


	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebAutoPurge ); // the last time autopurge was run
	_ftime( &m_timebTick );
	m_pszHost = NULL;			// the name of the host
	m_pszCortexHost = NULL;			// the name of the cortex host
	m_pszCompleteHost = NULL;	// the complete name of the host
	m_usCortexCommandPort = SOCKETMODULE_PORT_CMD;
	m_usCortexStatusPort = SOCKETMODULE_PORT_STATUS;

	m_bQuietKill = false;

	m_bProcessSuspended = false;

	m_ppHistory=NULL;
	m_nNumHistoryObjects=0;

	m_ppAsyncMsgObj=NULL;
	m_nNumAsyncMessageObjects=0;
	m_ulLastAsyncMsgID = 0;

	m_ulEmbeddingThread = 0xffffffff;  // which client thread owns the embedding.

	m_pDestObj = NULL;
}

CSocketModuleData::~CSocketModuleData()
{
	EnterCriticalSection(&m_critAsyncMessage);
	if(m_ppAsyncMsgObj)
	{
		int i=0;
		while(i<m_nNumAsyncMessageObjects)
		{
			if(m_ppAsyncMsgObj[i]) delete m_ppAsyncMsgObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppAsyncMsgObj; // delete array of pointers to objects, must use new to allocate
	}
	m_nNumAsyncMessageObjects = 0;
	LeaveCriticalSection(&m_critAsyncMessage);


	EnterCriticalSection(&m_critHistory);
	if(m_ppHistory)
	{
		int i=0;
		while(i<m_nNumHistoryObjects)
		{
			if(m_ppHistory[i]) delete m_ppHistory[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppHistory; // delete array of pointers to objects, must use new to allocate
		m_nNumHistoryObjects=0;
	}
	LeaveCriticalSection(&m_critHistory);


/*
	if(m_ppConnObj)
	{
		int i=0;
		while(i<m_nNumConnectionObjects)
		{
			if(m_ppConnObj[i]) delete m_ppConnObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppConnObj; // delete array of pointers to objects, must use new to allocate
	}
*/

	if(m_pucLastCommand) free(m_pucLastCommand);	
	m_pucLastCommand = NULL;
	m_ulLastCommandLength=0;	
	m_ulLastCommandTime=0;	


	EnterCriticalSection(&m_critMaps);
	if(m_ppMappingObj)
	{
		int i=0;
		while(i<m_nNumMappingObjects)
		{
			if(m_ppMappingObj[i])
			{
				if(!m_ppMappingObj[i]->m_bRequireAppFocus)
				{	UnregisterHotKey(NULL, i);}

				delete m_ppMappingObj[i]; // delete objects, must use new to allocate
			}
			i++;
		}
		delete [] m_ppMappingObj; // delete array of pointers to objects, must use new to allocate
	}
	m_ppMappingObj = NULL;
	m_nNumMappingObjects = 0;
	LeaveCriticalSection(&m_critMaps);

	EnterCriticalSection(&m_critAsRun);
	if(m_ppAsRunObj)
	{
		int i=0;
		while(i<m_nNumAsRunObjects)
		{
			if(m_ppAsRunObj[i]) delete m_ppAsRunObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppAsRunObj; // delete array of pointers to objects, must use new to allocate
	}
	m_ppAsRunObj = NULL;
	m_nNumAsRunObjects = 0;

	if(m_pLastObj) delete m_pLastObj; m_pLastObj=NULL;
	LeaveCriticalSection(&m_critAsRun);


	if(m_pszHost) free(m_pszHost);	// must use malloc to allocate
	if(m_pszCompleteHost) free(m_pszCompleteHost);	// must use malloc to allocate
	if(m_pszCortexHost) free(m_pszCortexHost);	// must use malloc to allocate

	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);

	DeleteCriticalSection(&m_critMaps);
	DeleteCriticalSection(&m_critAsRun);
	DeleteCriticalSection(&m_critSocket);
	DeleteCriticalSection(&m_critLastCommand);
	DeleteCriticalSection(&m_critAsyncMessage);
	DeleteCriticalSection(&m_critHistory);
	DeleteCriticalSection(&m_critEmbed);

/*

	if(	m_pDestObj )
	{
							int nWait =  clock()+1500;
							if((m_pDestObj->m_ulFlags)&LIBRETTO_FLAG_ENABLED)
							{
								m_pDestObj->m_bKillCommandQueueThread = true;
								m_pDestObj->m_bKillMonThread = true;
								m_pDestObj->m_bKillConnThread = true;
			char errorstring[MAX_MESSAGE_LENGTH];

								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
									m_pDestObj->m_pszDesc?m_pDestObj->m_pszDesc:m_pDestObj->m_pszServerName);  


if((g_psocketmodule)&&(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_VANC) )
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "destination_remove", errorstring);  //(Dispatch message)
}

//								g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto:destination_remove", errorstring);  // Sleep(20);  //(Dispatch message)
//									g_plibretto->m_data.m_ppDestObj[nTemp]->m_pDlg->OnDisconnect();


								//**** disconnect
//								while(m_pDestObj->m_bConnThreadStarted) Sleep(1);

								while(
											 (
										     (m_pDestObj->m_bMonThreadStarted)
											 ||(m_pDestObj->m_bConnThreadStarted)
											 ||(m_pDestObj->m_bCommandQueueThreadStarted)
											 )
											&&
											 (clock()<nWait)
										 )
								{
									Sleep(1);
								}


								//g_adc.DisconnectServer(m_pDestObj->m_pszServerName);
								//**** should check return value....
								if(m_pDestObj->m_socket != NULL)
								{
									m_pDestObj->m_net.CloseConnection(m_pDestObj->m_socket); // re-establish
								}
								m_pDestObj->m_socket = NULL;

								m_pDestObj->m_ulStatus = LIBRETTO_STATUS_NOTCON;
							}
					

							while((m_pDestObj->m_bMonThreadStarted)&&(clock()<nWait))
							{
								Sleep(1);
							}

							if(!(m_pDestObj->m_bMonThreadStarted)) delete m_pDestObj;
	}
	*/
}

char* CSocketModuleData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CSocketModuleData::SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError)
{
	int nRV = CX_ERROR;
	bool bError=false;

	if(!bOverwriteExistingError)
	{
		if(
			  ((m_ulFlags&SOCKETMODULE_ICON_MASK) == SOCKETMODULE_STATUS_ERROR)
			||((m_ulFlags&SOCKETMODULE_STATUS_CMDSVR_MASK) == SOCKETMODULE_STATUS_CMDSVR_ERROR)
			||((m_ulFlags&SOCKETMODULE_STATUS_STATUSSVR_MASK) ==  SOCKETMODULE_STATUS_STATUSSVR_ERROR)
			||((m_ulFlags&SOCKETMODULE_STATUS_THREAD_MASK) == SOCKETMODULE_STATUS_THREAD_ERROR)
			||((m_ulFlags&SOCKETMODULE_STATUS_FAIL_MASK) == SOCKETMODULE_STATUS_FAIL_DB)
			)	bError=true;
	}

	if (ulStatus&SOCKETMODULE_ICON_MASK)
	{
		m_ulFlags &= ~SOCKETMODULE_ICON_MASK;
		m_ulFlags |= (ulStatus&SOCKETMODULE_ICON_MASK);
	}
	if (ulStatus&SOCKETMODULE_STATUS_CMDSVR_MASK)
	{
		m_ulFlags &= ~SOCKETMODULE_STATUS_CMDSVR_MASK;
		m_ulFlags |= (ulStatus&SOCKETMODULE_STATUS_CMDSVR_MASK);
	}
	if (ulStatus&SOCKETMODULE_STATUS_STATUSSVR_MASK)
	{
		m_ulFlags &= ~SOCKETMODULE_STATUS_STATUSSVR_MASK;
		m_ulFlags |= (ulStatus&SOCKETMODULE_STATUS_STATUSSVR_MASK);
	}
	if (ulStatus&SOCKETMODULE_STATUS_THREAD_MASK)
	{
		m_ulFlags &= ~SOCKETMODULE_STATUS_THREAD_MASK;
		m_ulFlags |= (ulStatus&SOCKETMODULE_STATUS_THREAD_MASK);
	}
	if (ulStatus&SOCKETMODULE_STATUS_FAIL_MASK)
	{
		m_ulFlags &= ~SOCKETMODULE_STATUS_FAIL_MASK;
		m_ulFlags |= (ulStatus&SOCKETMODULE_STATUS_FAIL_MASK);
	}

	if(bError)
	{
		m_ulFlags &= ~SOCKETMODULE_ICON_MASK;
		m_ulFlags |= SOCKETMODULE_STATUS_ERROR;
	}

	if((pszText)&&(!bError))
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
//				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
//			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_psocketmodule->m_settings.m_pszIconPath)&&(strlen(g_psocketmodule->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_psocketmodule->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_psocketmodule->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_psocketmodule->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_psocketmodule->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_psocketmodule->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}

/*
int	CSocketModuleData::SetStatusText(char* pszText, unsigned long ulStatus)
{
	int nRV = CX_ERROR;
	if(pszText)
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_psocketmodule->m_settings.m_pszIconPath)&&(strlen(g_psocketmodule->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_psocketmodule->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_psocketmodule->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_psocketmodule->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_psocketmodule->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_psocketmodule->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}
	return nRV;
}
*/
// utility
int	CSocketModuleData::GetHost()
{
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&m_inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&m_inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszCompleteHost) free(m_pszCompleteHost);
						m_pszCompleteHost=pch;

						pch = (char*) malloc(nValue+1);
						if(pch!=NULL)
						{
							if(m_pszHost) free(m_pszHost);
							m_pszHost=pch;

							strcpy(m_pszHost, m_pszCompleteHost);
							pch = strchr(m_pszHost, '.');
							if(pch)
							{
								*pch = 0;
							}

							return SOCKETMODULE_SUCCESS;
						}
					}
				}
			}
		}
	}
	return SOCKETMODULE_ERROR;
}


/*
int CSocketModuleData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_psocketmodule)&&(g_psocketmodule->m_settings.m_pszExchange)&&(strlen(g_psocketmodule->m_settings.m_pszExchange)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];

		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT MAX(mod) AS MAX FROM %s WHERE criterion = '%s'", 
			g_psocketmodule->m_settings.m_pszExchange, szTemp		);

		//Sleep(500);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, dberrorstring);
		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				CString szValue;
				prs->GetFieldValue("MAX", szValue);
				if(szValue.GetLength()>0)
				{
					unsigned long ulLastMod = atol(szValue);
					if(ulLastMod>0) ulMod = ulLastMod+1;
				}
			}
			prs->Close();
			delete prs;
		}

		if(ulMod>0)
		{
			if(ulMod > SOCKETMODULE_DB_MOD_MAX) ulMod = 1;
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET mod = %d WHERE criterion = '%s'", // HARDCODE
				g_psocketmodule->m_settings.m_pszExchange, ulMod, szTemp		);
			if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
			{
				return SOCKETMODULE_SUCCESS;
			}
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (criterion, flag, mod) VALUES ('%s', '', 1)", // HARDCODE
				g_psocketmodule->m_settings.m_pszExchange, szTemp		);
			if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
			{
				return SOCKETMODULE_SUCCESS;
			}
		}
	}
	return SOCKETMODULE_ERROR;
}
*/



CString CSocketModuleData::DoText(CSocketModuleMappingObject* pObj)
{
	CString szNew="";
	if(pObj)
	{
		CString szText;

		bool bFirst=true;

		if(pObj->m_nSysChars&MOD_ALT)
		{
			szNew.Format("Alt");
			bFirst=false;
		}
		if(pObj->m_nSysChars&MOD_CONTROL)
		{
			if(bFirst)
				szText.Format("Ctrl");
			else
				szText.Format("+Ctrl");
			szNew+=szText;
			bFirst=false;
		}

		if(pObj->m_nSysChars&MOD_SHIFT)
		{
			if(bFirst)
				szText.Format("Shift");
			else
				szText.Format("+Shift");
			szNew+=szText;
			bFirst=false;
		}


		szText = CodeToText(pObj->m_nKeyCode);
		if(szText.GetLength())
		{
			if(!bFirst)
			{
				szNew += "+";
			}
			szNew+=szText;
		}

		if(!pObj->m_bRequireAppFocus)
		{
			szNew+= " (global)" ;
		}

		if(!pObj->m_bActive)
		{
			szNew+= " (inactive)" ;
		}

		if(pObj->m_bFailed)
		{
			szNew+= " (FAILED)" ;
		}
/*
	int m_nKeyCode;
	int m_nSysChars;
	bool m_bActive;
	bool m_bRequireAppFocus;
	int m_nMappingID;

#define VK_SHIFT          0x10
#define VK_CONTROL        0x11
#define VK_MENU           0x12


#define MOD_ALT         0x0001
#define MOD_CONTROL     0x0002
#define MOD_SHIFT       0x0004
*/
	}
	return szNew;
}

int	CSocketModuleData::AddHistory(CString* pObj,bool bError, char* pszError)
{
	if(pObj)
	{
	EnterCriticalSection(&m_critHistory);
		if((m_ppHistory)&&(m_nNumHistoryObjects>=g_psocketmodule->m_settings.m_nHistoryItems))
		{
/*
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_ASRUN) 	
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "Modifying as-run buffer: num: %d, buffer %d",m_nNumAsRunObjects, g_psocketmodule->m_settings.m_nAsRunRecent);  //(Dispatch message)
}
*/
			// just shift them around
			int i=m_nNumHistoryObjects-1;
			while(i>0)
			{
				m_ppHistory[i] = m_ppHistory[i-1];
				i--;
			}
			m_ppHistory[i] = pObj;

			// now, in case the setting was decreased, delete off the bottom
			while(m_nNumHistoryObjects>g_psocketmodule->m_settings.m_nHistoryItems)
			{
				try{ delete m_ppHistory[m_nNumHistoryObjects-1]; m_ppHistory[m_nNumHistoryObjects-1]=NULL; } 
				catch(...){}
				m_nNumHistoryObjects--;
			}
		}
		else
		{
/*
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_ASRUN) 	
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "Growing as-run buffer: num: %d, buffer %d",m_nNumAsRunObjects, g_psocketmodule->m_settings.m_nAsRunRecent);  //(Dispatch message)
}
*/
			// need to grow buffa
			CString** ppObj = new CString*[m_nNumHistoryObjects+1];
			if(ppObj)
			{
				m_nNumHistoryObjects++;
				ppObj[0] = pObj;

				if(m_ppHistory)
				{
					int i=1;
					while(i<m_nNumHistoryObjects)
					{
						ppObj[i] = m_ppHistory[i-1];
						i++;
					}
					try{ delete [] m_ppHistory; } catch(...){}
				}
				m_ppHistory = ppObj;
			}
		}
	LeaveCriticalSection(&m_critHistory);
	}
	return SOCKETMODULE_ERROR;

}


int	CSocketModuleData::AddAsyncMessage(CSocketModuleAsyncMessageObject* pObj, bool bError, char* pszError)
{
	if(pObj)
	{

	EnterCriticalSection(&m_critAsyncMessage);
		pObj->m_ulCmdID = ++m_ulLastAsyncMsgID;  if(m_ulLastAsyncMsgID>=UINT_MAX) m_ulLastAsyncMsgID=1;
		if((m_ppAsyncMsgObj)&&(m_nNumAsyncMessageObjects>=g_psocketmodule->m_settings.m_nAsyncMessageItems))
		{

if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 	
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "Modifying async buffer: num: %d, buffer %d", m_nNumAsyncMessageObjects, g_psocketmodule->m_settings.m_nAsyncMessageItems);  //(Dispatch message)
}

			// just shift them around
			int i=m_nNumAsyncMessageObjects-1;
			while(i>0)
			{
				m_ppAsyncMsgObj[i] = m_ppAsyncMsgObj[i-1];
				i--;
			}
			m_ppAsyncMsgObj[i] = pObj;

			// now, in case the recent setting was decreased, delete off the bottom
			while(m_nNumAsyncMessageObjects>g_psocketmodule->m_settings.m_nAsyncMessageItems)
			{
				try{ delete m_ppAsyncMsgObj[m_nNumAsyncMessageObjects-1]; m_ppAsyncMsgObj[m_nNumAsyncMessageObjects-1]=NULL; } 
				catch(...){}
				m_nNumAsyncMessageObjects--;
			}
		}
		else
		{

if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 	
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "Growing async buffer: num: %d, buffer %d",m_nNumAsyncMessageObjects, g_psocketmodule->m_settings.m_nAsyncMessageItems);  //(Dispatch message)
}

			// need to grow buffa
			CSocketModuleAsyncMessageObject** ppObj = new CSocketModuleAsyncMessageObject*[m_nNumAsyncMessageObjects+1];
			if(ppObj)
			{
				m_nNumAsyncMessageObjects++;
				ppObj[0] = pObj;

				if(m_ppAsyncMsgObj)
				{
					int i=1;
					while(i<m_nNumAsyncMessageObjects)
					{
						ppObj[i] = m_ppAsyncMsgObj[i-1];
						i++;
					}
					try{ delete [] m_ppAsyncMsgObj; } catch(...){}
				}
				m_ppAsyncMsgObj = ppObj;
			
			}

		}

	LeaveCriticalSection(&m_critAsyncMessage);


	}
	return SOCKETMODULE_ERROR;

}



int	CSocketModuleData::AddAsRun(CSocketModuleMappingObject* pObj, bool bError, char* pszError)
{
	if(pObj)
	{
/*		// write the log.
		if(g_psocketmodule->m_settings.m_bUseAsRunLog)
		{
			if(bError)
			{
				g_psocketmodule->m_msgr.DM(MSG_ICONNONE, "asrun,log", "SocketModule", 
					"ERROR sending Code 0x%02x, modifiers 0x%02x with user token %s sent to %s on %s:%d%s%s",
						pObj->m_nKeyCode, 
						pObj->m_nSysChars,
						g_psocketmodule->m_settings.m_pszUserToken,
						g_psocketmodule->m_settings.m_pszPluginName,
						g_psocketmodule->m_settings.m_pszTabulatorHost,
						g_psocketmodule->m_settings.m_usTabulatorCommandPort,
						((pszError)&&(strlen(pszError)))?": ":"",
						((pszError)&&(strlen(pszError)))?pszError:""
					);  //(Dispatch message)
			}
			else
			{
				g_psocketmodule->m_msgr.DM(MSG_ICONNONE, "asrun,log", "SocketModule", 
					"Code 0x%02x, modifiers 0x%02x with user token %s sent to %s on %s:%d",
						pObj->m_nKeyCode, 
						pObj->m_nSysChars,
						g_psocketmodule->m_settings.m_pszUserToken,
						g_psocketmodule->m_settings.m_pszPluginName,
						g_psocketmodule->m_settings.m_pszTabulatorHost,
						g_psocketmodule->m_settings.m_usTabulatorCommandPort
					);  //(Dispatch message)
			}
		}

		if((m_ppAsRunObj)&&(m_nNumAsRunObjects>=g_psocketmodule->m_settings.m_nAsRunRecent))
		{
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_ASRUN) 	
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "Modifying as-run buffer: num: %d, buffer %d",m_nNumAsRunObjects, g_psocketmodule->m_settings.m_nAsRunRecent);  //(Dispatch message)
}

			// just shift them around
			int i=m_nNumAsRunObjects-1;
			while(i>0)
			{
				m_ppAsRunObj[i]->m_nKeyCode = m_ppAsRunObj[i-1]->m_nKeyCode;
				m_ppAsRunObj[i]->m_nSysChars = m_ppAsRunObj[i-1]->m_nSysChars;
				m_ppAsRunObj[i]->m_bRequireAppFocus = m_ppAsRunObj[i-1]->m_bRequireAppFocus;
				m_ppAsRunObj[i]->m_tmbTransmitTime = m_ppAsRunObj[i-1]->m_tmbTransmitTime;

				i--;
			}
			m_ppAsRunObj[i]->m_nKeyCode = pObj->m_nKeyCode;
			m_ppAsRunObj[i]->m_nSysChars = pObj->m_nSysChars;
			m_ppAsRunObj[i]->m_bRequireAppFocus = pObj->m_bRequireAppFocus;
			m_ppAsRunObj[i]->m_tmbTransmitTime = pObj->m_tmbTransmitTime;
			m_ppAsRunObj[i]->m_bFailed = bError;
			m_ppAsRunObj[i]->m_bActive = true;

			// now, in case the recent setting was decreased, delete off the bottom
			while(m_nNumAsRunObjects>g_psocketmodule->m_settings.m_nAsRunRecent)
			{
				try{ delete m_ppAsRunObj[m_nNumAsRunObjects-1]; m_ppAsRunObj[m_nNumAsRunObjects-1]=NULL; } 
				catch(...){}
				m_nNumAsRunObjects--;
			}
		}
		else
		{
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_ASRUN) 	
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "Growing as-run buffer: num: %d, buffer %d",m_nNumAsRunObjects, g_psocketmodule->m_settings.m_nAsRunRecent);  //(Dispatch message)
}

			// need to grow buffa
			CSocketModuleMappingObject** ppObj = new CSocketModuleMappingObject*[m_nNumAsRunObjects+1];
			if(ppObj)
			{
				CSocketModuleMappingObject* pNewObj = new CSocketModuleMappingObject;
				if(pNewObj)
				{
					m_nNumAsRunObjects++;

if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_ASRUN) 	
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "Created new object");  //(Dispatch message)
}
					ppObj[0] = pNewObj;
					ppObj[0]->m_nKeyCode = pObj->m_nKeyCode;
					ppObj[0]->m_nSysChars = pObj->m_nSysChars;
					ppObj[0]->m_bRequireAppFocus = pObj->m_bRequireAppFocus;
					ppObj[0]->m_tmbTransmitTime = pObj->m_tmbTransmitTime;
					ppObj[0]->m_bFailed = bError;
					ppObj[0]->m_bActive = true;


if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_ASRUN) 	
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "Assigned new object");  //(Dispatch message)
}

					if(m_ppAsRunObj)
					{
						int i=1;
						while(i<m_nNumAsRunObjects)
						{
							ppObj[i] = m_ppAsRunObj[i-1];
							i++;
						}
						try{ delete [] m_ppAsRunObj; } catch(...){}
					}
					m_ppAsRunObj = ppObj;
				}
				else
				{
					try{ delete [] ppObj; } catch(...){}
				}
			}

		}
*/
	}
	return SOCKETMODULE_ERROR;
}


CString CSocketModuleData::CodeToText(int nCode)
{
	CString szText;
	switch(nCode)
	{
	case VK_F1: szText.Format("F1"); break;//VK_F1             0x70
	case VK_F2: szText.Format("F2"); break;//VK_F2             0x71
	case VK_F3: szText.Format("F3"); break;//VK_F3             0x72
	case VK_F4: szText.Format("F4"); break;//VK_F4             0x73
	case VK_F5: szText.Format("F5"); break;//VK_F5             0x74
	case VK_F6: szText.Format("F6"); break;//VK_F6             0x75
	case VK_F7: szText.Format("F7"); break;//VK_F7             0x76
	case VK_F8: szText.Format("F8"); break;//VK_F8             0x77
	case VK_F9: szText.Format("F9"); break;//VK_F9             0x78
	case VK_F10: szText.Format("F10"); break;//VK_F10            0x79
	case VK_F11: szText.Format("F11"); break;//VK_F11            0x7A
	case VK_F12: szText.Format("F12"); break;//VK_F12            0x7B
	case 0x30: szText.Format("0"); break;  //0x30
	case 0x31: szText.Format("1"); break;  //0x31
	case 0x32: szText.Format("2"); break;  //0x32
	case 0x33: szText.Format("3"); break;  //0x33
	case 0x34: szText.Format("4"); break;  //0x34
	case 0x35: szText.Format("5"); break;  //0x35
	case 0x36: szText.Format("6"); break;  //0x36
	case 0x37: szText.Format("7"); break;  //0x37
	case 0x38: szText.Format("8"); break;  //0x38
	case 0x39: szText.Format("9"); break;  //0x39
	case 0x41: szText.Format("A"); break;  //0x41
	case 0x42: szText.Format("B"); break;  //0x42
	case 0x43: szText.Format("C"); break;  //0x43
	case 0x44: szText.Format("D"); break;  //0x44
	case 0x45: szText.Format("E"); break;  //0x45
	case 0x46: szText.Format("F"); break;  //0x46
	case 0x47: szText.Format("G"); break;  //0x47
	case 0x48: szText.Format("H"); break;  //0x48
	case 0x49: szText.Format("I"); break;  //0x49
	case 0x4a: szText.Format("J"); break;  //0x4a
	case 0x4b: szText.Format("K"); break;  //0x4b
	case 0x4c: szText.Format("L"); break;  //0x4c
	case 0x4d: szText.Format("M"); break;  //0x4d
	case 0x4e: szText.Format("N"); break;  //0x4e
	case 0x4f: szText.Format("O"); break;  //0x4f
	case 0x50: szText.Format("P"); break;  //0x50
	case 0x51: szText.Format("Q"); break;  //0x51
	case 0x52: szText.Format("R"); break;  //0x52
	case 0x53: szText.Format("S"); break;  //0x53
	case 0x54: szText.Format("T"); break;  //0x54
	case 0x55: szText.Format("U"); break;  //0x55
	case 0x56: szText.Format("V"); break;  //0x56
	case 0x57: szText.Format("W"); break;  //0x57
	case 0x58: szText.Format("X"); break;  //0x58
	case 0x59: szText.Format("Y"); break;  //0x59
	case 0x5a: szText.Format("Z"); break;  //0x5a
	case 0xc0: szText.Format("`"); break;  //0xc0
	case 0xbd: szText.Format("-"); break;  //0xbd
	case 0xbb: szText.Format("="); break;  //0xbb
	case 0xdb: szText.Format("["); break;  //0xdb
	case 0xdd: szText.Format("]"); break;  //0xdd
	case 0xdc: szText.Format("\\"); break;  //0xdc
	case 0xbf: szText.Format("/"); break;  //0xbf
	case 0xde: szText.Format("'"); break;  //0xde
	case 0xba: szText.Format(";"); break;  //0xba
	case 0xbe: szText.Format("."); break;  //0xbe
	case 0xbc: szText.Format(","); break;  //0xbc
	case VK_SPACE: szText.Format("Space"); break;  //VK_SPACE          0x20
	case VK_SNAPSHOT: szText.Format("PrtScrn"); break;  //VK_SNAPSHOT       0x2C
	case VK_SCROLL: szText.Format("ScrLock"); break;  //VK_SCROLL         0x91
	case VK_NUMLOCK: szText.Format("NumLock"); break;  //VK_NUMLOCK        0x90
	case VK_PAUSE: szText.Format("Pause"); break;  //VK_PAUSE          0x13
	case VK_INSERT: szText.Format("Insert"); break; //VK_INSERT         0x2D
	case VK_DELETE: szText.Format("Delete"); break; //VK_DELETE         0x2E
	case VK_HOME: szText.Format("Home"); break;   //VK_HOME           0x24
	case VK_END: szText.Format("End"); break;    //VK_END            0x23
	case VK_PRIOR: szText.Format("PageUp"); break; //VK_PRIOR          0x21
	case VK_NEXT: szText.Format("PageDn"); break; //VK_NEXT           0x22
	case VK_BACK: szText.Format("Bksp"); break;   //VK_BACK           0x08
	case VK_UP: szText.Format("Up"); break;     //VK_UP             0x26
	case VK_DOWN: szText.Format("Down"); break;   //VK_DOWN           0x28
	case VK_LEFT: szText.Format("Left"); break;   //VK_LEFT           0x25
	case VK_RIGHT: szText.Format("Right"); break;  //VK_RIGHT          0x27
	case VK_TAB: szText.Format("Tab"); break;    //VK_TAB            0x09
	case VK_NUMPAD0: szText.Format("NumPad 0"); break;//VK_NUMPAD0        0x60
	case VK_NUMPAD1: szText.Format("NumPad 1"); break;//VK_NUMPAD1        0x61
	case VK_NUMPAD2: szText.Format("NumPad 2"); break;//VK_NUMPAD2        0x62
	case VK_NUMPAD3: szText.Format("NumPad 3"); break;//VK_NUMPAD3        0x63
	case VK_NUMPAD4: szText.Format("NumPad 4"); break;//VK_NUMPAD4        0x64
	case VK_NUMPAD5: szText.Format("NumPad 5"); break;//VK_NUMPAD5        0x65
	case VK_NUMPAD6: szText.Format("NumPad 6"); break;//VK_NUMPAD6        0x66
	case VK_NUMPAD7: szText.Format("NumPad 7"); break;//VK_NUMPAD7        0x67
	case VK_NUMPAD8: szText.Format("NumPad 8"); break;//VK_NUMPAD8        0x68
	case VK_NUMPAD9: szText.Format("NumPad 9"); break;//VK_NUMPAD9        0x69
	case VK_DECIMAL: szText.Format("NumPad ."); break;//VK_DECIMAL        0x6E
	case VK_ADD: szText.Format("NumPad +"); break;//VK_ADD            0x6B
	case VK_SUBTRACT: szText.Format("NumPad -"); break;//VK_SUBTRACT       0x6D
	case VK_MULTIPLY: szText.Format("NumPad *"); break;//VK_MULTIPLY       0x6A
	case VK_DIVIDE: szText.Format("NumPad /"); break;//VK_DIVIDE         0x6F
	case VK_RETURN: szText.Format("Enter"); break;//VK_RETURN	0x0D			
	default:
		{
			if((nCode == VK_SHIFT)||(nCode == VK_CONTROL)||(nCode == VK_MENU))
			{
				szText = "";
			}
			else if(nCode == VK_CAPITAL)
			{
				szText = "CapsLock";
			}
			else if(nCode == -1)
			{
				szText = "";
			}
			else 
			{
				szText.Format("code 0x%02x", nCode); 
			}
		}break;
	}
	return szText;
}

int CSocketModuleData::TextToCode(CString szText) 
{
	if(szText.CompareNoCase("F1")==0) { return VK_F1; }
	else if(szText.CompareNoCase("F2")==0) { return VK_F2; }
	else if(szText.CompareNoCase("F3")==0) { return VK_F3; }
	else if(szText.CompareNoCase("F4")==0) { return VK_F4; }
	else if(szText.CompareNoCase("F5")==0) { return VK_F5; }
	else if(szText.CompareNoCase("F6")==0) { return VK_F6; }
	else if(szText.CompareNoCase("F7")==0) { return VK_F7; }
	else if(szText.CompareNoCase("F8")==0) { return VK_F8; }
	else if(szText.CompareNoCase("F9")==0) { return VK_F9; }
	else if(szText.CompareNoCase("F10")==0) { return VK_F10; }
	else if(szText.CompareNoCase("F11")==0) { return VK_F11; }
	else if(szText.CompareNoCase("F12")==0) { return VK_F12; }
	else if(szText.CompareNoCase("0")==0) { return 0x30; } 
	else if(szText.CompareNoCase("1")==0) { return 0x31; } 
	else if(szText.CompareNoCase("2")==0) { return 0x32; } 
	else if(szText.CompareNoCase("3")==0) { return 0x33; }
	else if(szText.CompareNoCase("4")==0) { return 0x34; } 
	else if(szText.CompareNoCase("5")==0) { return 0x35; } 
	else if(szText.CompareNoCase("6")==0) { return 0x36; } 
	else if(szText.CompareNoCase("7")==0) { return 0x37; } 
	else if(szText.CompareNoCase("8")==0) { return 0x38; } 
	else if(szText.CompareNoCase("9")==0) { return 0x39; } 
	else if(szText.CompareNoCase("A")==0) { return 0x41; } 
	else if(szText.CompareNoCase("B")==0) { return 0x42; } 
	else if(szText.CompareNoCase("C")==0) { return 0x43; }
	else if(szText.CompareNoCase("D")==0) { return 0x44; }
	else if(szText.CompareNoCase("E")==0) { return 0x45; } 
	else if(szText.CompareNoCase("F")==0) { return 0x46; } 
	else if(szText.CompareNoCase("G")==0) { return 0x47; } 
	else if(szText.CompareNoCase("H")==0) { return 0x48; } 
	else if(szText.CompareNoCase("I")==0) { return 0x49; } 
	else if(szText.CompareNoCase("J")==0) { return 0x4a; } 
	else if(szText.CompareNoCase("K")==0) { return 0x4b; } 
	else if(szText.CompareNoCase("L")==0) { return 0x4c; } 
	else if(szText.CompareNoCase("M")==0) { return 0x4d; } 
	else if(szText.CompareNoCase("N")==0) { return 0x4e; } 
	else if(szText.CompareNoCase("O")==0) { return 0x4f; } 
	else if(szText.CompareNoCase("P")==0) { return 0x50; } 
	else if(szText.CompareNoCase("Q")==0) { return 0x51; } 
	else if(szText.CompareNoCase("R")==0) { return 0x52; } 
	else if(szText.CompareNoCase("S")==0) { return 0x53; } 
	else if(szText.CompareNoCase("T")==0) { return 0x54; } 
	else if(szText.CompareNoCase("U")==0) { return 0x55; } 
	else if(szText.CompareNoCase("V")==0) { return 0x56; } 
	else if(szText.CompareNoCase("W")==0) { return 0x57; } 
	else if(szText.CompareNoCase("X")==0) { return 0x58; } 
	else if(szText.CompareNoCase("Y")==0) { return 0x59; } 
	else if(szText.CompareNoCase("Z")==0) { return 0x5a; } 
	else if(szText.CompareNoCase("`")==0) { return 0xc0; } 
	else if(szText.CompareNoCase("-")==0) { return 0xbd; } 
	else if(szText.CompareNoCase("=")==0) { return 0xbb; } 
	else if(szText.CompareNoCase("[")==0) { return 0xdb; }
	else if(szText.CompareNoCase("]")==0) { return 0xdd; }
	else if(szText.CompareNoCase("\\")==0) { return 0xdc; } 
	else if(szText.CompareNoCase("/")==0) { return 0xbf; } 
	else if(szText.CompareNoCase("'")==0) { return 0xde; } 
	else if(szText.CompareNoCase(";")==0) { return 0xba; } 
	else if(szText.CompareNoCase(".")==0) { return 0xbe; } 
	else if(szText.CompareNoCase(",")==0) { return 0xbc; } 
	else if(szText.CompareNoCase("Space")==0) { return VK_SPACE; } 
	else if(szText.CompareNoCase("PrtScrn")==0) { return VK_SNAPSHOT; }
	else if(szText.CompareNoCase("ScrLock")==0) { return VK_SCROLL; } 
	else if(szText.CompareNoCase("NumLock")==0) { return VK_NUMLOCK; } 
	else if(szText.CompareNoCase("Pause")==0) { return VK_PAUSE; } 
	else if(szText.CompareNoCase("Insert")==0) { return VK_INSERT; } 
	else if(szText.CompareNoCase("Delete")==0) { return VK_DELETE; } 
	else if(szText.CompareNoCase("Home")==0) { return VK_HOME; } 
	else if(szText.CompareNoCase("End")==0) { return VK_END; } 
	else if(szText.CompareNoCase("PageUp")==0) { return VK_PRIOR; } 
	else if(szText.CompareNoCase("PageDn")==0) { return VK_NEXT; } 
	else if(szText.CompareNoCase("Bksp")==0) { return VK_BACK; } 
	else if(szText.CompareNoCase("Up")==0) { return VK_UP; } 
	else if(szText.CompareNoCase("Down")==0) { return VK_DOWN; } 
	else if(szText.CompareNoCase("Left")==0) { return VK_LEFT; } 
	else if(szText.CompareNoCase("Right")==0) { return VK_RIGHT; } 
	else if(szText.CompareNoCase("Tab")==0) { return VK_TAB; } 
	else if(szText.CompareNoCase("NumPad 0")==0) { return VK_NUMPAD0; } 
	else if(szText.CompareNoCase("NumPad 1")==0) { return VK_NUMPAD1; } 
	else if(szText.CompareNoCase("NumPad 2")==0) { return VK_NUMPAD2; } 
	else if(szText.CompareNoCase("NumPad 3")==0) { return VK_NUMPAD3; } 
	else if(szText.CompareNoCase("NumPad 4")==0) { return VK_NUMPAD4; } 
	else if(szText.CompareNoCase("NumPad 5")==0) { return VK_NUMPAD5; } 
	else if(szText.CompareNoCase("NumPad 6")==0) { return VK_NUMPAD6; } 
	else if(szText.CompareNoCase("NumPad 7")==0) { return VK_NUMPAD7; } 
	else if(szText.CompareNoCase("NumPad 8")==0) { return VK_NUMPAD8; } 
	else if(szText.CompareNoCase("NumPad 9")==0) { return VK_NUMPAD9; } 
	else if(szText.CompareNoCase("NumPad .")==0) { return VK_DECIMAL; } 
	else if(szText.CompareNoCase("NumPad +")==0) { return VK_ADD; } 
	else if(szText.CompareNoCase("NumPad -")==0) { return VK_SUBTRACT; } 
	else if(szText.CompareNoCase("NumPad *")==0) { return VK_MULTIPLY; } 
	else if(szText.CompareNoCase("NumPad /")==0) { return VK_DIVIDE; } 
	else if(szText.CompareNoCase("Enter")==0) { return VK_RETURN;} 
	return -1;
}

