// SocketModuleLibrettoImports.h: 
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SMLI_H__4E2B6B5A_D551_49FC_8122_7E942EFFA48F__INCLUDED_)
#define AFX_SMLI_H__4E2B6B5A_D551_49FC_8122_7E942EFFA48F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <winsock2.h>

#include <sys/timeb.h>
#include "../../Common/TXT/BufferUtil.h" 
#include "../../Common/LAN/NetUtil.h" 

//#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"
//#include "../../Common/API/Miranda/IS2Comm.h"
//#include "../../Common/KEY/LicenseKey.h"
#include "../../Common/TTY/Serial.h"
//#include "../../Common/TIME/TimeConvert.h"

// status
#define LIBRETTO_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define LIBRETTO_STATUS_UNKNOWN							0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define LIBRETTO_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define LIBRETTO_STATUS_ERROR								0x00000020  // error (red icon)
#define LIBRETTO_STATUS_CONN								0x00000030  // ready (green icon)	
#define LIBRETTO_STATUS_OK									0x00000030  // ready (green icon)	
#define LIBRETTO_STATUS_RUN									0x00000040  // in progress, running, owned etc (blue icon);	
#define LIBRETTO_ICON_MASK									0x00000070  // mask	

#define LIBRETTO_PORT_INVALID			0	

#define LIBRETTO_FLAG_DISABLED		0x0000	 // default
#define LIBRETTO_FLAG_ENABLED			0x0001	
#define LIBRETTO_FLAG_FOUND				0x1000
#define LIBRETTO_FLAG_NEW					0x0100
#define LIBRETTO_FLAG_REFRESH			0x0200

//return values
#define LIBRETTO_SUCCESS   0
#define LIBRETTO_ERROR	   -1



void LibrettoAsynchConnThread(void* pvArgs);
void LibrettoConnMonitorThread(void* pvArgs);
void LibrettoCommandQueueThread(void* pvArgs);

class CLibrettoDestinationObject  
{
public:
	CLibrettoDestinationObject();
	virtual ~CLibrettoDestinationObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	char* m_pszServerName;
//	char* m_pszClientName;
	char* m_pszDesc;
	unsigned short m_usPort;

//	CAConnection* m_pAPIConn;  // pointer to the associated API connection.
	SOCKET m_socket;
	CNetUtil m_net;
//	CSerial m_serial;
	CBufferUtil m_bu;
//	CTimeConvert  m_timeconv;


	double m_dblDiskKBFree;  
	double m_dblDiskKBTotal; 
	double m_dblDiskPercent; 
	int m_nChannelID;

	int m_nItemID;

	_timeb m_timebLastDisconnect;
	_timeb m_timebCommandQueueTick;
	_timeb m_timebLastPing;  // for keep alives

	// control
	bool m_bKillCommandQueueThread;
	bool m_bCommandQueueThreadStarted;
	bool m_bKillConnThread;
	bool m_bConnThreadStarted;
	bool m_bKillMonThread;
	bool m_bMonThreadStarted;
	bool m_bSending;

// external support
	void* m_pvExternal;

//	bool m_bFailed;
//	_timeb m_timebFailure;
	CRITICAL_SECTION m_critComm;
//	CRITICAL_SECTION m_critMemQueue;
//	CLibrettoQueueObject** m_ppMemQueue;
//	int m_nNumMemQueueArray;
//	int m_nPtrMemQueueIn;
//	int m_nPtrMemQueueOut;

	int SendTelnetCommand(char* pchBuffer, unsigned long ulBufferLen, char chEolnType, char* pSource);//=NULL);
//	int AddMemQueue(CLibrettoQueueObject* pMemQueue);
//	int SetMemQueue(int nMemIndex, CLibrettoQueueObject* pMemQueue);
//	int GetMemQueue(int nID=0); //returns index of next
//	int RemoveMemQueue(int nIndex); 
//	int ClearMemQueue();
};




#endif // !defined(AFX_SMLI_H__4E2B6B5A_D551_49FC_8122_7E942EFFA48F__INCLUDED_)
