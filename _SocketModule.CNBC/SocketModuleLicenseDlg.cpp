// SocketModuleLicenseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SocketModule.h"
#include "SocketModuleLicenseDlg.h"
#include "../../Common/KEY/LicenseKey.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSocketModuleLicenseDlg dialog


CSocketModuleLicenseDlg::CSocketModuleLicenseDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSocketModuleLicenseDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSocketModuleLicenseDlg)
	m_szKey = _T("");
	//}}AFX_DATA_INIT
}


void CSocketModuleLicenseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSocketModuleLicenseDlg)
	DDX_Text(pDX, IDC_EDIT1, m_szKey);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSocketModuleLicenseDlg, CDialog)
	//{{AFX_MSG_MAP(CSocketModuleLicenseDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSocketModuleLicenseDlg message handlers

void CSocketModuleLicenseDlg::OnOK() 
{
	// TODO: Add extra validation here
	CLicenseKey key;
	key.m_pszLicenseString = (char*)malloc(strlen(m_szKey)+1);
	if(key.m_pszLicenseString)
	sprintf(key.m_pszLicenseString, "%s", m_szKey);

	key.InterpretKey();

	if(!key.m_bValid)
	{
		AfxMessageBox("This is not a valid license code."); 
		return;
	}

	CDialog::OnOK();
}
