#if !defined(AFX_SOCKETMODULEKEYSETUPDLG_H__07632578_24BF_4068_B59E_D20146C01D90__INCLUDED_)
#define AFX_SOCKETMODULEKEYSETUPDLG_H__07632578_24BF_4068_B59E_D20146C01D90__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SocketModuleKeySetupDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSocketModuleKeySetupDlg dialog

class CSocketModuleKeySetupDlg : public CDialog
{
// Construction
public:
	CSocketModuleKeySetupDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSocketModuleKeySetupDlg)
	enum { IDD = IDD_KEYDIALOG };
	BOOL	m_bActive;
	BOOL	m_bAlt;
	BOOL	m_bAppFocus;
	BOOL	m_bCtrl;
	BOOL	m_bShift;
	CString	m_szKey;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSocketModuleKeySetupDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSocketModuleKeySetupDlg)
	virtual void OnOK();
	afx_msg void OnButtonHelp();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SOCKETMODULEKEYSETUPDLG_H__07632578_24BF_4068_B59E_D20146C01D90__INCLUDED_)
