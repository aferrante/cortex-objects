// SocketModuleMain.h: interface for the CSocketModuleMain class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SOCKETMODULEMAIN_H__2FA150AF_62FD_474D_9F15_E4CA67BA3807__INCLUDED_)
#define AFX_SOCKETMODULEMAIN_H__2FA150AF_62FD_474D_9F15_E4CA67BA3807__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "SocketModule.h" 
#include "../../Common/MSG/msg.h"
#include "../../Common/MSG/Messager.h"
#include "../../Common/TXT/FileUtil.h"
//#include "../../Common/HTTP/HTTP10.h"  // includes CMessagingObject
#include "../../Common/JSON/JSON.h"
#include "../../Common/TTY/Serial.h"

#include "SocketModuleSettings.h"
#include "SocketModuleData.h"
//#include "SocketModuleLibrettoImports.h"

#define SENDMSG_SENDER_MAXLEN 64
#define SENDMSG_MESSAGE_MAXLEN 512

// global message function so that dialogs etc (external objects) can write to the log etc.
//void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations);

// global main thread - this is the business right here!
void SocketModuleMainThread(void* pvArgs);

// cortex http thread - we pass in the main cortex object, do all the cgi programming and security checking.
//void SocketModuleHTTPThread(void* pvArgs);

void SocketModuleCommandHandlerThread(void* pvArgs);
//void SocketModuleStatusHandlerThread(void* pvArgs);
//void SocketModuleXMLHandlerThread(void* pvArgs);
void SocketModuleEmulationHandlerThread(void* pvArgs);
void SocketModuleJSONHandlerThread(void* pvArgs);




class CSocketModuleMain  
{
public:
	CSocketModuleMain();
	virtual ~CSocketModuleMain();

	CMessager						m_msgr;				// main messager // the one and only messager object
	CSocketModuleSettings		m_settings;		// main mem storage for settings.  settings file objects are local to the SocketModuleMainThread.
	CSocketModuleData				m_data;				// internal variables
	CNetUtil						m_net;				// have a separate one for the status and command services.
	
////////////////////////////////////////////////////
//if inserter
//	CLibrettoDestinationObject m_vanc; //vanc inserter destination.
//if inserter
////////////////////////////////////////////////////

	
////////////////////////////////////////////////////
//if logger
	CSerial m_serial;
	BOOL m_bSocketMonitorStarted;
	BOOL m_bMonitorSocket;
	_timeb m_timebNextCOM; // the next time a COM port connection should be attempted

//if logger
////////////////////////////////////////////////////


	// core net functions
	SOCKET*		SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char SubCmd=CX_CMD_NULL, unsigned char* pucData=NULL, unsigned long ulDataLen=0, char* pchUser=NULL, char* pchPw=NULL);  // cortex initiates a request to an object server
	int		SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd=CX_CMD_NULL, unsigned char* pucData=NULL, unsigned long ulDataLen=0, char* pchUser=NULL, char* pchPw=NULL);		// cortex replies to an object server after receiving data. (usually ack or nak)
	int		SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd=CX_CMD_NULL, unsigned char* pucData=NULL, unsigned long ulDataLen=0, char* pchUser=NULL, char* pchPw=NULL);		// cortex answers a request from an object client

	// move following to a Messager destination object...
//	int SendMsg(int nType, char* pszSender, char* pszError, ...);
//	int SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...);

};

#endif // !defined(AFX_SOCKETMODULEMAIN_H__2FA150AF_62FD_474D_9F15_E4CA67BA3807__INCLUDED_)
