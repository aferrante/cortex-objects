// SocketModuleData.h: interface for the CSocketModuleData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SOCKETMODULEDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
#define AFX_SOCKETMODULEDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <winsock2.h>
#include <sys/timeb.h>
#include "../../Common/TXT/BufferUtil.h" 
#include "../../Common/LAN/NetUtil.h" 
//#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"
#include "../../Common/KEY/LicenseKey.h"
#include "SocketModuleLibrettoImports.h"


// these are data reference objects, pulled from the database.
/*
class CSocketModuleConnectionObject  
{
public:
	CSocketModuleConnectionObject();
	virtual ~CSocketModuleConnectionObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	char* m_pszServerName;
	char* m_pszClientName;
	char* m_pszDesc;

//	CAConnection* m_pAPIConn;  // pointer to the associated API connection.

	// control
	bool m_bKillConnThread;
	bool m_bConnThreadStarted;
};
*/

class CSocketModuleMappingObject  
{
public:
	CSocketModuleMappingObject();
	virtual ~CSocketModuleMappingObject();

	int m_nKeyCode;
	int m_nSysChars;
	bool m_bActive;
	bool m_bRequireAppFocus;
	bool m_bFailed;

	// for as-run
	_timeb m_tmbTransmitTime;
};


class CSocketModuleAsyncMessageObject  
{
public:
	CSocketModuleAsyncMessageObject();
	virtual ~CSocketModuleAsyncMessageObject();

	unsigned long m_ulCmdID;  // this ID will be an incrementing queue id,
	unsigned long m_ulThreadID;  // thread ID (the assigned UID) of the originating thread
	bool m_bDefeat;  // if true, defeats the originating thread from sending anything in its own async stack
//	char* m_pchJSON; // the command to send
	char m_pchJSON[404]; //hard limit
	_timeb m_tmbQueueTime;
};




class CSocketModuleData  
{
public:
	CSocketModuleData();
	virtual ~CSocketModuleData();

/*
	CSocketModuleEvent** m_ppEventsList;
	int m_nNumEventsList;
	CSocketModuleEvent** m_ppEventsNewList;
	int m_nNumEventsNewList;
	CSocketModuleEvent** m_ppEventsPlaying;
	int m_nNumEventsPlaying;
*/
	// util objects
	CBufferUtil m_bu;
	CNetUtil m_net;
	bool m_bNetClientConnected;
	SOCKET m_socket;
	CNetData m_netdata;
	CRITICAL_SECTION m_critSocket;
	IN_ADDR m_inaddr;


	int m_nRole; //just an id number to tell it what it is, set in settings.

  // hostname.  have to get it, store it here
	char*	m_pszHost;	// the name of the host
	char*	m_pszCompleteHost;	// the complete name of the host

	char*	m_pszCortexHost;	// the name of the cortex host
	unsigned short m_usCortexCommandPort;
	unsigned short m_usCortexStatusPort;

	CSocketModuleMappingObject** m_ppMappingObj;
	int m_nNumMappingObjects;

	CSocketModuleMappingObject** m_ppAsRunObj;
	CSocketModuleMappingObject* m_pLastObj;
	int m_nNumAsRunObjects;

	CRITICAL_SECTION m_critMaps;
	CRITICAL_SECTION m_critAsRun;


	bool m_bNetworkMessagingInitialized;
	bool m_bCheckModsWarningSent;
	bool m_bCheckMsgsWarningSent;
	bool m_bCheckAsRunWarningSent;

	_timeb m_timebAutoPurge; // the last time autopurge was run

	CRITICAL_SECTION m_critLastCommand;

	unsigned char*	m_pucLastCommand;	
	unsigned long	m_ulLastCommandLength;	
	unsigned long	m_ulLastCommandTime;	
	unsigned long	m_ulLastKeepAlive;	
	unsigned long	m_ulLastParsedCommandTime;	

	CString m_szDisplay;



//	_timeb m_timebAutomationTick; // the last time check inside the thread
//	_timeb m_timebNearTick; // the last time check inside the thread
//	_timeb m_timebFarTick; // the last time check inside the thread
//	_timeb m_timebTriggerTick; // the last time check inside the thread


	_timeb m_timebTick; // the last time check inside the thread
//	_timeb m_timebLastStatus; // the time of the last status given to cortex
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for cortex global status (each obj has their own as well)

	bool m_bProcessSuspended;

	char* GetStatusText(unsigned long* pulStatus);  // allocates mem, must free after use
	int		SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError = false);
	int		GetHost();
	CString DoText(CSocketModuleMappingObject* pObj);
	int TextToCode(CString szText);
	CString CodeToText(int nCode);
	int		AddAsRun(CSocketModuleMappingObject* pObj,bool bError=false, char* pszError=NULL);
	int		AddHistory(CString* pObj,bool bError=false, char* pszError=NULL);

	CSocketModuleAsyncMessageObject** m_ppAsyncMsgObj;
	int m_nNumAsyncMessageObjects;
	unsigned long m_ulLastAsyncMsgID;
	CRITICAL_SECTION m_critAsyncMessage;

	int		AddAsyncMessage(CSocketModuleAsyncMessageObject* pObj,bool bError=false, char* pszError=NULL);


	CLicenseKey m_key;

	bool m_bQuietKill;


	CString** m_ppHistory;
	int m_nNumHistoryObjects;
	CRITICAL_SECTION m_critHistory;


	CRITICAL_SECTION m_critEmbed;
	unsigned long  m_ulEmbeddingThread;
	CString m_szLastPayload;  // global insertion payload

	CLibrettoDestinationObject* m_pDestObj;



private:
	CRITICAL_SECTION m_critText;
	char* m_pszStatus;	// parseable string

//	unsigned long m_ulReqCount;
//	unsigned long m_ulSvcCount;
};

#endif // !defined(AFX_SOCKETMODULEDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
