// HeliosMain.cpp: implementation of the CHeliosMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "Helios.h"  // just included to have access to windowing environment
#include "HeliosDlg.h"  // just included to have access to windowing environment
#include "HeliosHandler.h"  // just included to have access to windowing environment

#include "HeliosMain.h"
#include <process.h>
#include "../../Common/TXT/BufferUtil.h"
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"
//#include "../../Common/API/Omnibus/OmniComm.h"


#include <sys/timeb.h>
#include <time.h>
#include <direct.h>
#include <objsafe.h>
#include <atlbase.h>

// have to add rpcrt4.lib to the linker for UUID stuff


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;
bool g_bKillStatus=false;

CHeliosMain* g_phelios=NULL;
//CADC g_adc;
COmniComm g_omni;

extern CMessager* g_pmsgr;  // from Messager.cpp
extern CHeliosApp theApp;
extern CCAActives g_actives;


void HeliosConnectionThread(void* pvArgs);
void HeliosListThread(void* pvArgs);



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHeliosMain::CHeliosMain()
{
	InitializeCriticalSection(&m_critSendMsg);
}

CHeliosMain::~CHeliosMain()
{
	DeleteCriticalSection(&m_critSendMsg);
}

/*

char*	CHeliosMain::HeliosTranslate(CHTTPHeader* pHeader, char* pszBuffer)			// apply helios scripting language
{
	return pszBuffer;
}

int		CHeliosMain::InterpretDirective(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the directive, such as a char buffer.
	return HELIOS_SUCCESS;
}
*/

SOCKET*	CHeliosMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // helios initiates a request to an object server
{
	CNetData* pdata = new CNetData;

	pdata->m_ucType = ucType;      // defined type - indicates which protocol to use, structure of data

	pdata->m_ucCmd = ucCmd;       // the command byte
	pdata->m_ucSubCmd = ucSubCmd;       // the subcommand byte

	pdata->m_pucData = pucData;
	pdata->m_ulDataLen = ulDataLen;

	SOCKET* ps = NULL; 
	m_net.SendData(pdata, pchHost, usPort, 5000, 0, NET_SND_CMDTOSVR, ps);
//	m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);

	return ps;
}

int		CHeliosMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// helios replies to an object server after receiving data. (usually ack or nak)
{
	return HELIOS_SUCCESS;
}

int		CHeliosMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// helios answers a request from an object client
{
	return HELIOS_SUCCESS;
}


void HeliosMainThread(void* pvArgs)
{
	CHeliosApp* pApp = (CHeliosApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment
	//startup.
	g_bThreadStarted = true;

	//create the main objects.

	CHeliosMain helios;
	CDBUtil db;
	CDBUtil db2;

	helios.m_data.m_ulFlags &= ~HELIOS_STATUS_THREAD_MASK;
	helios.m_data.m_ulFlags |= HELIOS_STATUS_THREAD_START;
	helios.m_data.m_ulFlags &= ~HELIOS_ICON_MASK;
	helios.m_data.m_ulFlags |= HELIOS_STATUS_UNINIT;

	helios.m_data.GetHost();

	g_phelios = &helios;


	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
//	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\Helios\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}



//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

//		AfxMessageBox( pApp->m_lpCmdLine );

	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// check here for Cortex IP.
			if(strlen(pch))
			{
//		AfxMessageBox( pch );
				if(helios.m_data.m_bu.CountChar(pch, strlen(pch), ':')>=1)  // must be of form "hostname:cmdport" where hostname can be name or ip
				{
					helios.m_data.m_pszCortexHost = (char*)malloc(strlen(pch)+1);
					if(helios.m_data.m_pszCortexHost)
					{
						strcpy(helios.m_data.m_pszCortexHost, pch);
//		AfxMessageBox( helios.m_data.m_pszCortexHost );

						char* pchd = strchr(helios.m_data.m_pszCortexHost, ':');
						if(pchd)
						{
							*pchd = 0;  // null term the host name;
							pchd++;
//		AfxMessageBox( helios.m_data.m_pszCortexHost );

							char* pchd2 = strchr(pchd, ':');
							if(pchd2)
							{
								*pchd2 = 0;  // null term the cmd port;
								pchd2++;
								if(strlen(pchd2)) helios.m_data.m_usCortexStatusPort = atoi(pchd2);
							}

							if(strlen(pchd)) helios.m_data.m_usCortexCommandPort = atoi(pchd);
						}
					}

				}
			}
			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }

	// get settings.
	helios.m_settings.Settings(true); //read
/*
	char pszFilename[MAX_PATH];

	strcpy(pszFilename, HELIOS_SETTINGS_FILE_DEFAULT);  // helios settings file

	CFileUtil file;
	file.GetSettings(pszFilename, false); 
	char* pszParams = NULL;
// load up the values on the settings object
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		helios.m_settings.m_pszName = file.GetIniString("Main", "Name", "Helios");
		helios.m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");
*/
/*
		// compile license key params
		if(g_phelios->m_data.m_key.m_pszLicenseString) free(g_phelios->m_data.m_key.m_pszLicenseString);
		g_phelios->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(helios.m_settings.m_pszLicense)+1);
		if(g_phelios->m_data.m_key.m_pszLicenseString)
		sprintf(g_phelios->m_data.m_key.m_pszLicenseString, "%s", helios.m_settings.m_pszLicense);

		g_phelios->m_data.m_key.InterpretKey();

		if(g_phelios->m_data.m_key.m_bValid)
		{
			unsigned long i=0;
			while(i<g_phelios->m_data.m_key.m_ulNumParams)
			{
				if((g_phelios->m_data.m_key.m_ppszParams)
					&&(g_phelios->m_data.m_key.m_ppszValues)
					&&(g_phelios->m_data.m_key.m_ppszParams[i])
					&&(g_phelios->m_data.m_key.m_ppszValues[i]))
				{
					if(stricmp(g_phelios->m_data.m_key.m_ppszParams[i], "max")==0)
					{
						g_phelios->m_data.m_nMaxLicensedChannels = atoi(g_phelios->m_data.m_key.m_ppszValues[i]);
					}
					else
					if(stricmp(g_phelios->m_data.m_key.m_ppszParams[i], "oem")==0)
					{
						// if it exists, check OEM string.

					}
				}
				i++;
			}

			if(
			    (g_phelios->m_data.m_key.m_bValid)
				&&(
				    (!helios.m_data.m_key.m_bExpires)
					||((helios.m_data.m_key.m_bExpires)&&(!helios.m_data.m_key.m_bExpired))
					||((helios.m_data.m_key.m_bExpires)&&(helios.m_data.m_key.m_bExpireForgiveness)&&(helios.m_data.m_key.m_ulExpiryDate+helios.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
				    (!helios.m_data.m_key.m_bMachineSpecific)
					||((helios.m_data.m_key.m_bMachineSpecific)&&(helios.m_data.m_key.m_bValidMAC))
					)
				)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
				helios.m_data.SetStatusText(errorstring, HELIOS_STATUS_OK);
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				helios.m_data.SetStatusText(errorstring, HELIOS_STATUS_ERROR);
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
			helios.m_data.SetStatusText(errorstring, HELIOS_STATUS_ERROR);
		}
/*
		helios.m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");

		helios.m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", HELIOS_PORT_CMD);
		helios.m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", HELIOS_PORT_STATUS);

		helios.m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
		helios.m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
		helios.m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;

		helios.m_settings.m_pszOmnibusDataFields = file.GetIniString("OmnibusXML", "DataFields", "data,logos");  // the data fields we care about
		helios.m_settings.m_pszDebugLog = file.GetIniString("OmnibusXML", "DebugLog", NULL); 
		helios.m_settings.m_pszCommLog = file.GetIniString("OmnibusXML", "CommLog", NULL); 
		helios.m_settings.m_bWriteXML = file.SetIniInt("OmnibusXML", "WriteXML", 0)?true:false;  
		helios.m_settings.m_ulCheckInterval=file.GetIniInt("OmnibusXML", "PurgeCheckInterval", 0);	// number of seconds between checks (0 turns off)
		helios.m_settings.m_ulExpiryPeriod=file.GetIniInt("OmnibusXML", "PurgeExpiryPeriod", 10800);	// (default value 3 hours) when the event's end time (start plus duration) is more than this number of seconds older than the current server time, it is purged.
		helios.m_settings.m_ulConnTimeout=file.GetIniInt("OmnibusXML", "ConnectionTimeout", 300);	// (default value 5 mins) 


		helios.m_settings.m_pszDSN = file.GetIniString("Database", "DSN", helios.m_settings.m_pszName?helios.m_settings.m_pszName:"Helios");
		helios.m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
		helios.m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
		helios.m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
		helios.m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
		helios.m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name

		helios.m_settings.m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels");  // the Channels table name
		helios.m_settings.m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Adaptors");  // the Connections table name
		helios.m_settings.m_pszLiveEvents = file.GetIniString("Database", "LiveEventsTableName", "Events");  // the LiveEvents table name

		helios.m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds

		if(pszParams) free(pszParams); pszParams=NULL;
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
	}
*/
	bool bUseLog = false;
	bool bUseEmail = false;
	bool bLogNetworkErrors = false;
	if(helios.m_settings.m_bUseLog)
	{
		bUseLog = helios.m_settings.m_bUseLog;

		// for logfiles, we need params, and they must be in this format:
		//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
//		pszParams = file.GetIniString("Messager", "LogFileIni", "Helios|YD||1|");
		int nRegisterCode=0;

		nRegisterCode = helios.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
			"log", 
			helios.m_settings.m_pszProcessedFileSpec?helios.m_settings.m_pszProcessedFileSpec:helios.m_settings.m_pszFileSpec, 
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
			helios.m_data.SetStatusText(errorstring, (HELIOS_STATUS_FAIL_LOG|HELIOS_STATUS_ERROR));
		}

//		if(pszParams) free(pszParams); pszParams=NULL;
	}

	helios.m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "--------------------------------------------------\n\
-------------- Helios %s start --------------", HELIOS_CURRENT_VERSION);  //(Dispatch message)


	if(helios.m_settings.m_bUseEmail)
	{
		bUseEmail = helios.m_settings.m_bUseEmail;
				// for smtp, we need params, and they must be in this format:
				// SMTP address[: override port (optional)] | distribution lists | from email | subject spec | localhostname | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
				// note on distribution lists:
				// we want to give the option to map message type and severity to different address lists.
				// the distribution lists field format is this:
				// ff:x@x.com,q@q.com;0x:B@b.com
				// the : delimits the type and severity flag specifier from the email address list for that set. 
				// the email addresses are delimited by comma
				// the sets are delimited by ;
				// the delimiters are not allowed in addresses, as described here: http://en.wikipedia.org/wiki/E-mail_address
				// on the type and severity specifiers, it is like this:
				// the subfield is a 2 character hex code, sort of.
				// severity is the first char, possible values are 0,1,2,3,4, for normal, low, medium, high, critical
				// additionally, f means all
				// type is the second char, possible values are 0-8, for the follwing:
				// #define MSG_ICONNONE								0x00000000  // default, nothing
				// #define MSG_ICONQUESTION						0x00000001  // ? icon
				// #define MSG_ICONEXCLAMATION				0x00000002  // ! icon
				// #define MSG_ICONERROR							0x00000003  // X icon
				// #define MSG_ICONSTOP								0x00000004  // stop sign icon
				// #define MSG_ICONHAND								0x00000005  // hand icon
				// #define MSG_ICONINFO								0x00000006  // (i) icon 
				// #define MSG_ICONUSER1							0x00000007  
				// #define MSG_ICONUSER2							0x00000008  
				// ff means send all messages to the address list
				// 00 means send all messages with severity normal and no "icon" to the address list
				// 03 means send all messages with severity normal and error type to the address list
				// 43 means send all messages with severity critical and error type to the address list
				// you can set up different address lists with the same addresses - one email per dispatched message will be sent in the case of overlap.

		int nRegisterCode=0;

		// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
		nRegisterCode = helios.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
			"email", 
			helios.m_settings.m_pszProcessedMailSpec?helios.m_settings.m_pszProcessedMailSpec:helios.m_settings.m_pszMailSpec,
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
			helios.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
			helios.m_msgr.DM(MSG_ICONERROR, NULL, "Helios:smtp_init", errorstring);  //(Dispatch message)
		}

//		if(pszParams) free(pszParams); pszParams=NULL;
	}



//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
//	helios.m_http.InitializeMessaging(&helios.m_msgr);
//	helios.m_net.InitializeMessaging(&helios.m_msgr);
	if(helios.m_settings.m_bLogNetworkErrors) 
	{
		bLogNetworkErrors = helios.m_settings.m_bLogNetworkErrors;
		if(helios.m_net.InitializeMessaging(&helios.m_msgr)==0)
		{
			helios.m_data.m_bNetworkMessagingInitialized=true;
		}
	}
	// no setting for omni msging
	g_omni.InitializeMessaging(&helios.m_msgr);


// TODO: here, get the DB and pull any override settings.
	//****
	CDBconn* pdbConn = db.CreateNewConnection(helios.m_settings.m_pszDSN, helios.m_settings.m_pszUser, helios.m_settings.m_pszPW);
	helios.m_settings.m_pdbConn = pdbConn;
	helios.m_settings.m_pdb = &db;
	helios.m_data.m_pdbConn = pdbConn;
	helios.m_data.m_pdb = &db;
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			helios.m_data.SetStatusText(errorstring, (HELIOS_STATUS_FAIL_DB|HELIOS_STATUS_ERROR));
			helios.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Helios:database_connect", errorstring);  //(Dispatch message)
		}
		else
		{
			if(helios.m_settings.GetFromDatabase(errorstring)<HELIOS_SUCCESS)
			{
				helios.m_data.SetStatusText(errorstring, (HELIOS_STATUS_FAIL_DB|HELIOS_STATUS_ERROR));
				helios.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Helios:database_get", errorstring);  //(Dispatch message)
			}
			else
			{
				helios.m_data.m_nLastSettingsMod = helios.m_data.m_nSettingsMod;
				if(
						(helios.m_settings.m_pszLiveEvents)
					&&(strlen(helios.m_settings.m_pszLiveEvents)>0)
					)
				{

					char szSQL[DB_SQLSTRING_MAXLEN];
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
														helios.m_settings.m_pszLiveEvents  // table name
													);
//			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "HeliosChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

					EnterCriticalSection(&g_phelios->m_data.m_critSQL);
					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						//**MSG
					}
					LeaveCriticalSection(&g_phelios->m_data.m_critSQL);

					EnterCriticalSection(&g_phelios->m_data.m_critIncrement);
					if (helios.m_data.IncrementDatabaseMods(helios.m_settings.m_pszLiveEvents, errorstring)<HELIOS_SUCCESS)
					{
						//**MSG
					}
					LeaveCriticalSection(&g_phelios->m_data.m_critIncrement);

				}
				if((!helios.m_settings.m_bUseEmail)&&(bUseEmail))
				{
					bUseEmail = false;
					// reset it
					helios.m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "Shutting down email functions.");  //(Dispatch message)

//						Sleep(250); // let the message get there.
					helios.m_msgr.RemoveDestination("email");

				}
				if((!helios.m_settings.m_bLogNetworkErrors)&&(bLogNetworkErrors))
				{
					// reset it
					helios.m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "Shutting down network logging.");  //(Dispatch message)
					if(helios.m_data.m_bNetworkMessagingInitialized)
					{
						helios.m_net.UninitializeMessaging();  // void return
						helios.m_data.m_bNetworkMessagingInitialized=false;
					}

				}

				if((!helios.m_settings.m_bUseLog)&&(bUseLog))
				{
					// reset it
					helios.m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

					Sleep(250); // let the message get there.
					helios.m_msgr.RemoveDestination("log");

				}
			}
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", helios.m_settings.m_pszDSN, helios.m_settings.m_pszUser, helios.m_settings.m_pszPW); 
		helios.m_data.SetStatusText(errorstring, (HELIOS_STATUS_FAIL_DB|HELIOS_STATUS_ERROR));
		helios.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Helios:database_init", errorstring);  //(Dispatch message)

		//**MSG
	}

	// create a secondary connection
	CDBconn* pdb2Conn = db2.CreateNewConnection(helios.m_settings.m_pszDSN, helios.m_settings.m_pszUser, helios.m_settings.m_pszPW);
	helios.m_data.m_pdb2Conn = pdb2Conn;
	helios.m_data.m_pdb2 = &db2;
	if(pdb2Conn)
	{
		if(db2.ConnectDatabase(pdb2Conn, errorstring)<DB_SUCCESS)
		{
			helios.m_data.SetStatusText(errorstring, (HELIOS_STATUS_FAIL_DB|HELIOS_STATUS_ERROR));
			helios.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Helios:database2_connect", errorstring);  //(Dispatch message)
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create secondary connection for %s:%s:%s", helios.m_settings.m_pszDSN, helios.m_settings.m_pszUser, helios.m_settings.m_pszPW); 

		helios.m_data.SetStatusText(errorstring, (HELIOS_STATUS_FAIL_DB|HELIOS_STATUS_ERROR));
		helios.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Helios:database2_init", errorstring);  //(Dispatch message)

		//**MSG
	}



//init command and status listeners.

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing command server on %d", helios.m_settings.m_usCommandPort); 
	helios.m_data.SetStatusText(errorstring, HELIOS_STATUS_CMDSVR_START);
	helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:command_server_init", errorstring);  Sleep(100); //(Dispatch message)

	if(helios.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = helios.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "HeliosCommandServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = HeliosCommandHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &helios;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &helios.m_net;					// pointer to the object with the Message function.


		if(helios.m_net.StartServer(pServer, &helios.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			helios.m_data.SetStatusText(errorstring, HELIOS_STATUS_CMDSVR_ERROR);
			helios.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Helios:command_server_init", errorstring);  //(Dispatch message)
EnterCriticalSection(&(helios.m_critSendMsg));  // do these outside
			helios.SendMsg(CX_SENDMSG_ERROR, "Helios:command_server_init", errorstring);
LeaveCriticalSection(&(helios.m_critSendMsg));  // do these outside
		}
		else
		{
//AfxMessageBox("Q1");
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Command server listening on %d", helios.m_settings.m_usCommandPort);
			helios.m_data.SetStatusText(errorstring, HELIOS_STATUS_CMDSVR_RUN);
			helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:command_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing XML server on %d", helios.m_settings.m_usStatusPort); 
	helios.m_data.SetStatusText(errorstring, HELIOS_STATUS_STATUSSVR_START);
	helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:xml_server_init", errorstring);  Sleep(100);//(Dispatch message)

	if(helios.m_settings.m_usStatusPort>0)
	{
//AfxMessageBox("Q1");
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = helios.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "HeliosXMLServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = HeliosXMLHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &helios;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &helios.m_net;					// pointer to the object with the Message function.

		if(helios.m_net.StartServer(pServer, &helios.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			helios.m_data.SetStatusText(errorstring, HELIOS_STATUS_STATUSSVR_ERROR);
			helios.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Helios:xml_server_init", errorstring);  //(Dispatch message)
EnterCriticalSection(&(helios.m_critSendMsg));  // do these outside
			helios.SendMsg(CX_SENDMSG_ERROR, "Helios:xml_server_init", errorstring);
LeaveCriticalSection(&(helios.m_critSendMsg));  // do these outside
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML server listening on %d", helios.m_settings.m_usStatusPort);
			helios.m_data.SetStatusText(errorstring, HELIOS_STATUS_STATUSSVR_RUN);
			helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:xml_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

//AfxMessageBox("QQ");

	
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Helios is connecting to registered Colossus Adaptors...");
	helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:automation_conn_init", errorstring);  Sleep(100);//(Dispatch message)
	helios.m_data.SetStatusText(errorstring, HELIOS_STATUS_THREAD_SPARK);

	// now connect all the registered servers
	// this is the list of servers in the db that get connected

//	g_omni.m_pcritActive = &helios.m_data.m_critChannels;  // this line turns the whole use of active channels on inside the adaptor comm thread
	g_actives.m_bUseActive = helios.m_settings.m_bUseActive;  //changed to this
	g_actives.m_bClearEventsInit = helios.m_settings.m_bClearEventsInit;

	if(!(helios.m_settings.m_ulMainMode&HELIOS_MODE_CLONE))
	{
		helios.m_data.CheckDatabaseMods();

		EnterCriticalSection(&helios.m_data.m_critChannels);
		if(helios.m_data.GetConnections()>=HELIOS_SUCCESS)
		{
			helios.m_data.m_nLastConnectionsMod = helios.m_data.m_nConnectionsMod;
		}

		if(helios.m_data.GetChannels()>=HELIOS_SUCCESS)
		{
			helios.m_data.m_nLastChannelsMod = helios.m_data.m_nChannelsMod;
		}
		LeaveCriticalSection(&helios.m_data.m_critChannels);
	}
//	helios.m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "X4");  Sleep(250);//(Dispatch message)

	
	if((helios.m_data.m_ulFlags&HELIOS_ICON_MASK) != HELIOS_STATUS_ERROR)
	{
		helios.m_data.m_ulFlags &= ~HELIOS_ICON_MASK;
		helios.m_data.m_ulFlags |= HELIOS_STATUS_OK;  // green - we want run to be blue when something in progress
	}


// initialize cortex (make this not persistent for now)
	CNetData* pdata = new CNetData;
	if(pdata)
	{

		pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA;      // defined type - indicates which protocol to use, structure of data

		pdata->m_ucCmd = CX_REQ_HELLO;       // the command byte
		pdata->m_ucSubCmd = 0;       // the subcommand byte

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
		if(pch)
		{
			sprintf(pch, "%s:%d:%d:%d:%d:%s", 
				helios.m_data.m_pszHost,
				helios.m_settings.m_usCommandPort,
				helios.m_settings.m_usStatusPort,
				CX_TYPE_RESOURCE,
				theApp.m_nPID,
				helios.m_settings.m_pszName?helios.m_settings.m_pszName:"Helios"
				);

			pdata->m_pucData =  (unsigned char*) pch;
			pdata->m_ulDataLen = strlen(pch);
		}

		SOCKET s = NULL; 
//AfxMessageBox("sending");
//AfxMessageBox( helios.m_data.m_pszCortexHost );
//AfxMessageBox( (char*)pdata->m_pucData );
		if(helios.m_net.SendData(pdata, helios.m_data.m_pszCortexHost, helios.m_data.m_usCortexCommandPort, 5000, 0, NET_SND_CMDTOSVR, &s)>=NET_SUCCESS)
		{
			//send ack
//			AfxMessageBox("ack");
			helios.m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);
		}
		else
		{
//			AfxMessageBox("could not send");
		}
		helios.m_net.CloseConnection(s);

		try
		{
			delete pdata;
		}	catch(...) {}

	}


	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Helios main thread running.");  
	helios.m_data.SetStatusText(errorstring, HELIOS_STATUS_THREAD_RUN);
//	helios.m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", errorstring);  Sleep(250);//(Dispatch message)
EnterCriticalSection(&(helios.m_critSendMsg));  // do these outside
	helios.SendMsg(CX_SENDMSG_INFO, "Helios:init", "Helios %s main thread running.", HELIOS_CURRENT_VERSION);
LeaveCriticalSection(&(helios.m_critSendMsg));  // do these outside

	_timeb timebCheckMods;
	_ftime( &timebCheckMods );
///AfxMessageBox("xxxxx");
	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &helios.m_data.m_timebTick );
//	helios.m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "%d.%03d", helios.m_data.m_timebTick.time, helios.m_data.m_timebTick.millitm);   Sleep(250);//(Dispatch message)

// monitor changes in exchange db and react.
		if(
					(helios.m_data.m_timebTick.time > timebCheckMods.time )
				||((helios.m_data.m_timebTick.time == timebCheckMods.time)&&(helios.m_data.m_timebTick.millitm >= timebCheckMods.millitm))
				&&(!g_bKillThread)
			)
		{
//	helios.m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "time to check");  Sleep(250);//(Dispatch message)
			timebCheckMods.time = helios.m_data.m_timebTick.time + helios.m_settings.m_ulModsIntervalMS/1000; 
			timebCheckMods.millitm = helios.m_data.m_timebTick.millitm + (unsigned short)(helios.m_settings.m_ulModsIntervalMS%1000); // fractional second updates
			if(timebCheckMods.millitm>999)
			{
				timebCheckMods.time++;
				timebCheckMods.millitm%=1000;
			}
//	helios.m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "checking db connected");   Sleep(250);//(Dispatch message)
			if(pdbConn)//&&(pdbConn->m_bConnected))
			{
//AfxMessageBox("x");
if(helios.m_settings.m_ulDebug&(HELIOS_DEBUG_TIMING|HELIOS_DEBUG_PROCESS)) helios.m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "checkmods");  //(Dispatch message)
				strcpy(errorstring, "");

				if(helios.m_data.CheckDatabaseMods(errorstring)==HELIOS_ERROR)
				{
					if(!helios.m_data.m_bCheckModsWarningSent)
					{
						helios.m_msgr.DM(MSG_ICONERROR, NULL, "CheckDatabaseMods", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
						helios.m_data.m_bCheckModsWarningSent=true;
					}
				}
				else
				{
					if(helios.m_data.m_bCheckModsWarningSent)
					{
						helios.m_msgr.DM(MSG_ICONINFO, NULL, "CheckDatabaseMods", "Resumed checking."); // Sleep(50); //(Dispatch message)
					}
					helios.m_data.m_bCheckModsWarningSent=false;
				}

				if(helios.m_data.m_timebTick.time > helios.m_data.m_timebAutoPurge.time + helios.m_settings.m_nAutoPurgeInterval)
				{
					_ftime(&helios.m_data.m_timebAutoPurge);
					if(helios.m_settings.m_nAutoPurgeMessageDays>0)
					{
						if(helios.m_data.CheckMessages(errorstring)==HELIOS_ERROR)
						{
							if(!helios.m_data.m_bCheckMsgsWarningSent)
							{
								helios.m_msgr.DM(MSG_ICONERROR, NULL, "CheckMessages", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								helios.m_data.m_bCheckMsgsWarningSent=true;
							}
						}
						else
						if(helios.m_data.m_bCheckMsgsWarningSent)
						{
							helios.m_msgr.DM(MSG_ICONINFO, NULL, "CheckMessages", "Resumed checking messages for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						helios.m_data.m_bCheckMsgsWarningSent=false;
					}

	/*
					if(helios.m_settings.m_nAutoPurgeAsRunDays>0)
					{
						if(helios.m_data.CheckAsRun(errorstring)==HELIOS_ERROR)
						{
							if(!helios.m_data.m_bCheckAsRunWarningSent)
							{
								helios.m_msgr.DM(MSG_ICONERROR, NULL, "CheckAsRun", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								helios.m_data.m_bCheckAsRunWarningSent=true;
							}
						}
						else
						if(helios.m_data.m_bCheckMsgsWarningSent)
						{
							helios.m_msgr.DM(MSG_ICONINFO, NULL, "CheckAsRun", "Resumed checking as-run for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						helios.m_data.m_bCheckAsRunWarningSent=false;
					}

	*/
				}

				if(helios.m_data.m_nSettingsMod != helios.m_data.m_nLastSettingsMod)
				{
if(helios.m_settings.m_ulDebug&(HELIOS_DEBUG_PROCESS)) helios.m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "GetFromDatabase (%d, %d)", helios.m_data.m_nSettingsMod, helios.m_data.m_nLastSettingsMod);  //(Dispatch message)
					if(helios.m_settings.GetFromDatabase()>=HELIOS_SUCCESS)
					{
						helios.m_data.m_nLastSettingsMod = helios.m_data.m_nSettingsMod;

						// check for stuff to change

						// network messaging
						if(helios.m_settings.m_bLogNetworkErrors) 
						{
							if(!helios.m_data.m_bNetworkMessagingInitialized)
							{
								if(helios.m_net.InitializeMessaging(&helios.m_msgr)==0)
								{
									helios.m_data.m_bNetworkMessagingInitialized=true;
								}
							}
						}
						else
						{
							if(helios.m_data.m_bNetworkMessagingInitialized)
							{
								helios.m_net.UninitializeMessaging();  // void return
								helios.m_data.m_bNetworkMessagingInitialized=false;
							}
						}

						// logging and email messaging:

						if(!helios.m_settings.m_bUseEmail)
						{
							if(bUseEmail)
							{
								bUseEmail = false;
								// reset it
								helios.m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "Shutting down email functions.");  //(Dispatch message)

		//						Sleep(250); // let the message get there.
								helios.m_msgr.RemoveDestination("email");
							}
						}
						else
						{
							if(!bUseEmail)
							{
								bUseEmail = true;
								int nRegisterCode=0;

								// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
								nRegisterCode = helios.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
									"email", 
									helios.m_settings.m_pszProcessedMailSpec?helios.m_settings.m_pszProcessedMailSpec:helios.m_settings.m_pszMailSpec,
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
									helios.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
									helios.m_msgr.DM(MSG_ICONERROR, NULL, "Helios:smtp_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=helios.m_msgr.GetDestIndex("email");
								if(nIndex>=0)
								{
									if((helios.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(helios.m_settings.m_pszProcessedMailSpec?helios.m_settings.m_pszProcessedMailSpec:helios.m_settings.m_pszMailSpec))
									{
										if(strcmp(helios.m_msgr.m_ppDest[nIndex]->m_pszParams, (helios.m_settings.m_pszProcessedMailSpec?helios.m_settings.m_pszProcessedMailSpec:helios.m_settings.m_pszMailSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = helios.m_msgr.ModifyDestination(
												"email", 
												helios.m_settings.m_pszProcessedMailSpec?helios.m_settings.m_pszProcessedMailSpec:helios.m_settings.m_pszMailSpec,
												MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify SMTP object!\n code: %d", nRegisterCode); 
												//helios.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												helios.m_msgr.DM(MSG_ICONERROR, NULL, "Helios:smtp_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}

						if(!helios.m_settings.m_bUseLog)
						{
							if(bUseLog)
							{
								bUseLog = false;
								// reset it
								helios.m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

								Sleep(250); // let the message get there.
								helios.m_msgr.RemoveDestination("log");
							}
						}
						else
						{
							if(!bUseLog)
							{
								bUseLog = true;
								int nRegisterCode=0;

								nRegisterCode = helios.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
									"log", 
									helios.m_settings.m_pszProcessedFileSpec?helios.m_settings.m_pszProcessedFileSpec:helios.m_settings.m_pszFileSpec, 
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
									helios.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_LOG|CX_STATUS_ERROR));
									helios.m_msgr.DM(MSG_ICONERROR, NULL, "Helios:log_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=helios.m_msgr.GetDestIndex("log");
								if(nIndex>=0)
								{
									if((helios.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(helios.m_settings.m_pszProcessedFileSpec?helios.m_settings.m_pszProcessedFileSpec:helios.m_settings.m_pszFileSpec))
									{
										if(strcmp(helios.m_msgr.m_ppDest[nIndex]->m_pszParams, (helios.m_settings.m_pszProcessedFileSpec?helios.m_settings.m_pszProcessedFileSpec:helios.m_settings.m_pszFileSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = helios.m_msgr.ModifyDestination(
												"log", 
												helios.m_settings.m_pszProcessedFileSpec?helios.m_settings.m_pszProcessedFileSpec:helios.m_settings.m_pszFileSpec,
												MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify log object!\n code: %d", nRegisterCode); 
												//helios.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												helios.m_msgr.DM(MSG_ICONERROR, NULL, "Helios:log_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}








					}
				}

				if(			 
						(!helios.m_data.m_bProcessSuspended)
					&&(helios.m_data.m_key.m_bValid)  // must have a valid license
					&&(
							(!helios.m_data.m_key.m_bExpires)
						||((helios.m_data.m_key.m_bExpires)&&(!helios.m_data.m_key.m_bExpired))
						||((helios.m_data.m_key.m_bExpires)&&(helios.m_data.m_key.m_bExpireForgiveness)&&(helios.m_data.m_key.m_ulExpiryDate+helios.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!helios.m_data.m_key.m_bMachineSpecific)
						||((helios.m_data.m_key.m_bMachineSpecific)&&(helios.m_data.m_key.m_bValidMAC))
						)
					)
				{
				

					if(helios.m_data.m_nConnectionsMod != helios.m_data.m_nLastConnectionsMod)
					{
	//			helios.m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "getting connections again");   Sleep(250);//(Dispatch message)

		EnterCriticalSection(&helios.m_data.m_critChannels);
if(helios.m_settings.m_ulDebug&(HELIOS_DEBUG_PROCESS)) helios.m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "GetConnections (%d, %d)", helios.m_data.m_nConnectionsMod, helios.m_data.m_nLastConnectionsMod);  //(Dispatch message)
						if(helios.m_data.GetConnections()>=HELIOS_SUCCESS)
						{
							helios.m_data.m_nLastConnectionsMod = helios.m_data.m_nConnectionsMod;
						}
		LeaveCriticalSection(&helios.m_data.m_critChannels);
					}

					if(helios.m_data.m_nChannelsMod != helios.m_data.m_nLastChannelsMod)
					{
	//			helios.m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "getting channels again");   Sleep(250);//(Dispatch message)
		EnterCriticalSection(&helios.m_data.m_critChannels);
if(helios.m_settings.m_ulDebug&(HELIOS_DEBUG_PROCESS)) helios.m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "GetChannels (%d, %d)", helios.m_data.m_nChannelsMod, helios.m_data.m_nLastChannelsMod);  //(Dispatch message)
						if(helios.m_data.GetChannels()>=HELIOS_SUCCESS)
						{
							helios.m_data.m_nLastChannelsMod = helios.m_data.m_nChannelsMod;
						}
		LeaveCriticalSection(&helios.m_data.m_critChannels);
					}

				}
			}
		}

		if(	
			  (!helios.m_data.m_bProcessSuspended)
			&&(helios.m_data.m_key.m_bValid)  // must have a valid license
			&&(
					(!helios.m_data.m_key.m_bExpires)
				||((helios.m_data.m_key.m_bExpires)&&(!helios.m_data.m_key.m_bExpired))
				||((helios.m_data.m_key.m_bExpires)&&(helios.m_data.m_key.m_bExpireForgiveness)&&(helios.m_data.m_key.m_ulExpiryDate+helios.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
					(!helios.m_data.m_key.m_bMachineSpecific)
				||((helios.m_data.m_key.m_bMachineSpecific)&&(helios.m_data.m_key.m_bValidMAC))
				)
			)
		{
			int incr=0;
	EnterCriticalSection (&g_omni.m_crit);
			while((g_omni.m_ppConn)&&(incr<g_omni.m_usNumConn))
			{
				if(g_omni.m_ppConn[incr])
				{
		EnterCriticalSection (&(g_omni.m_ppConn[incr]->m_critCounter));
					if(g_omni.m_ppConn[incr]->m_ulRefTick != g_omni.m_ppConn[incr]->m_ulCounter)
					{
if(helios.m_settings.m_ulDebug&(HELIOS_DEBUG_PROCESS)) helios.m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "IncrementDatabaseMods on conn %d (%d, %d)", incr, g_omni.m_ppConn[incr]->m_ulRefTick, g_omni.m_ppConn[incr]->m_ulCounter);  //(Dispatch message)

						g_omni.m_ppConn[incr]->m_ulRefTick = g_omni.m_ppConn[incr]->m_ulCounter;
						EnterCriticalSection(&helios.m_data.m_critIncrement);
						if(helios.m_data.IncrementDatabaseMods(helios.m_settings.m_pszLiveEvents, errorstring)<HELIOS_SUCCESS)
						{
							//**MSG
						}
						LeaveCriticalSection(&helios.m_data.m_critIncrement);
					}
		LeaveCriticalSection (&(g_omni.m_ppConn[incr]->m_critCounter));
				}
				incr++;
			}
	LeaveCriticalSection (&g_omni.m_crit);

		}

//AfxMessageBox("zoinks");
		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();

		Sleep(1); 
//		Sleep(1000);  // stalls window procedure
	}

	helios.m_data.m_ulFlags &= ~HELIOS_STATUS_THREAD_MASK;
	helios.m_data.m_ulFlags |= HELIOS_STATUS_THREAD_END;

	helios.m_msgr.DM(MSG_ICONINFO, NULL, "Helios:uninit", "Helios is shutting down.");  //(Dispatch message)
EnterCriticalSection(&(helios.m_critSendMsg));  // do these outside
	helios.SendMsg(CX_SENDMSG_INFO, "Helios:uninit", "Helios %s is shutting down.", HELIOS_CURRENT_VERSION);
LeaveCriticalSection(&(helios.m_critSendMsg));  // do these outside

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Helios is shutting down."); 
	
	// ok to reset errors to yellow...
	helios.m_data.m_ulFlags &= ~CX_ICON_MASK;
	helios.m_data.m_ulFlags |= CX_STATUS_UNKNOWN;
	helios.m_data.SetStatusText(errorstring, helios.m_data.m_ulFlags);

	

// shut down all the running objects;
	EnterCriticalSection(&helios.m_data.m_critChannels);

	if(helios.m_data.m_ppChannelObj)
	{
		while(helios.m_data.m_nNumChannelObjects>0)
		{
			int i = helios.m_data.m_nNumChannelObjects-1;
			if(helios.m_data.m_ppChannelObj[i])
			{
				helios.m_data.m_ppChannelObj[i]->m_ulFlags = HELIOS_FLAG_DISABLED; // so killing thread wont start it up again
//				helios.m_data.m_ppChannelObj[i]->m_bKillChannelThread = true;
				
//				while(helios.m_data.m_ppChannelObj[i]->m_bChannelThreadStarted) Sleep(10);//(may be dangerous stall here)


				try
				{
					delete helios.m_data.m_ppChannelObj[i];
				}	catch(...) {}
				helios.m_data.m_ppChannelObj[i] = NULL;
			}
			helios.m_data.m_nNumChannelObjects--;
		}

		try
		{
			delete [] helios.m_data.m_ppChannelObj;
		}	catch(...) {}
	}
	helios.m_data.m_ppChannelObj = NULL;

	if(helios.m_data.m_ppConnObj)
	{
		while(helios.m_data.m_nNumConnectionObjects>0)
		{
			int i = helios.m_data.m_nNumConnectionObjects-1;
			if(helios.m_data.m_ppConnObj[i])
			{
				// **** disconnect servers
				helios.m_data.m_ppConnObj[i]->m_ulFlags = HELIOS_FLAG_DISABLED; // so killing thread wont start it up again
//				helios.m_data.m_ppConnObj[i]->m_bKillConnThread = true;

				if((helios.m_data.m_ppConnObj[i]->m_ulStatus&HELIOS_ICON_MASK) == HELIOS_STATUS_CONN)
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s%s%s...", 
						helios.m_data.m_ppConnObj[i]->m_pszDesc?helios.m_data.m_ppConnObj[i]->m_pszDesc:helios.m_data.m_ppConnObj[i]->m_pszServerName,
						helios.m_data.m_ppConnObj[i]->m_pszDesc?" on ":"",
						helios.m_data.m_ppConnObj[i]->m_pszDesc?helios.m_data.m_ppConnObj[i]->m_pszServerName:""
						);  
					helios.m_msgr.DM(MSG_ICONINFO, NULL, "Helios:connection_uninit", errorstring);    //(Dispatch message)
					helios.m_data.SetStatusText(errorstring, helios.m_data.m_ulFlags);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Removing %s...", helios.m_data.m_ppConnObj[i]->m_pszDesc?helios.m_data.m_ppConnObj[i]->m_pszDesc:helios.m_data.m_ppConnObj[i]->m_pszServerName);  
					helios.m_data.SetStatusText(errorstring, helios.m_data.m_ulFlags);
				}


				if((helios.m_data.m_ppConnObj[i])&&(helios.m_data.m_ppConnObj[i]->m_pCAConn)&&(helios.m_data.m_ppConnObj[i]->m_pCAConn->m_bThreadStarted))
				{
					int nDisTime=g_omni.DisconnectServer(g_phelios->m_data.m_ppConnObj[i]->m_pCAConn, 10000);
					if( nDisTime >= OMNI_SUCCESS)
						g_phelios->m_data.m_ppConnObj[i]->m_pCAConn = NULL;
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_TIMING)
{
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnected %s%s%s in %d milliseconds (5000ms timeout)", 
		g_phelios->m_data.m_ppConnObj[i]->m_pszDesc?g_phelios->m_data.m_ppConnObj[i]->m_pszDesc:g_phelios->m_data.m_ppConnObj[i]->m_pszServerName,
		g_phelios->m_data.m_ppConnObj[i]->m_pszDesc?" on ":"",
		g_phelios->m_data.m_ppConnObj[i]->m_pszDesc?g_phelios->m_data.m_ppConnObj[i]->m_pszServerName:"",
		nDisTime
		);  
	g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:debug", errorstring); // Sleep(100);  //(Dispatch message)
}


/*
// cant do the following.  the connection pointer is deleted in DisconnectServer
					int nClock = clock()+10000;  // give it ten seconds to close out.
					while((helios.m_data.m_ppConnObj[i])&&(helios.m_data.m_ppConnObj[i]->m_pCAConn)&&(helios.m_data.m_ppConnObj[i]->m_pCAConn->m_bThreadStarted)&&(clock()<nClock))
					{
						Sleep(10);
					}
*/
				
				}
				
				Sleep(1000); // sleep one second before deleting the object itself

				try
				{
					delete helios.m_data.m_ppConnObj[i];
				}	catch(...) {}
				helios.m_data.m_ppConnObj[i] = NULL;
			}
			helios.m_data.m_nNumConnectionObjects--;
		}
		try
		{
			delete [] helios.m_data.m_ppConnObj;
		}	catch(...) {}

	}
	helios.m_data.m_ppConnObj = NULL;
	LeaveCriticalSection(&helios.m_data.m_critChannels);


/*
	// here's a hard coded one for now
	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no need to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = helios.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		helios.m_net.OpenConnection(helios.m_http.m_pszHost, 10888, &s); // branding hardcode
		helios.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		helios.m_net.CloseConnection(s); // branding hardcode
	}
*/

//	m_pDlg->SetProgress(HELIOSDLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
//	helios.m_data.m_ulFlags &= ~HELIOS_STATUS_FILESVR_MASK;
//	helios.m_data.m_ulFlags |= HELIOS_STATUS_FILESVR_END;
//	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down file server....");  
//	helios.m_data.SetStatusText(errorstring, helios.m_data.m_ulFlags);
//	_ftime( &helios.m_data.m_timebTick );
//	helios.m_http.EndServer();
	_ftime( &helios.m_data.m_timebTick );
//	AfxMessageBox("shutting down command server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down command server....");
	helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:command_server_uninit", errorstring);   Sleep(100);  //(Dispatch message)
	helios.m_data.SetStatusText(errorstring, HELIOS_STATUS_CMDSVR_END);
	_ftime( &helios.m_data.m_timebTick );
	helios.m_net.StopServer(helios.m_settings.m_usCommandPort, 5000, errorstring);
	_ftime( &helios.m_data.m_timebTick );
//	AfxMessageBox("shutting down status server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down xml server....");  
	helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:xml_server_uninit", errorstring);   Sleep(100);  //(Dispatch message)
	helios.m_data.SetStatusText(errorstring, HELIOS_STATUS_STATUSSVR_END);
	_ftime( &helios.m_data.m_timebTick );
	helios.m_net.StopServer(helios.m_settings.m_usStatusPort, 5000, errorstring);
	_ftime( &helios.m_data.m_timebTick );
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Helios is exiting.");  
	helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", errorstring);   Sleep(100);  //(Dispatch message)
	helios.m_data.SetStatusText(errorstring, helios.m_data.m_ulFlags);

//helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "about to kill status");Sleep(100);
	g_bKillStatus = true;

	Sleep(250);


	// save settings.  // dont save them here.  save them on any changes in the main command loop.
//helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "settings");Sleep(100);
	helios.m_settings.Settings(false); //write
//	helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "settings done");Sleep(100);
/*
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// these explicts arent necessary - uncomment to write out a full file to edit...
		file.SetIniString("Main", "Name", helios.m_settings.m_pszName);
		file.SetIniInt("CommandServer", "ListenPort", helios.m_settings.m_usCommandPort);
		file.SetIniInt("StatusServer", "ListenPort", helios.m_settings.m_usStatusPort);
		file.SetIniString("License", "Key", helios.m_settings.m_pszLicense);

		file.SetIniString("FileServer", "IconPath", helios.m_settings.m_pszIconPath);

		file.SetIniInt("Messager", "UseEmail", helios.m_settings.m_bUseEmail?1:0);
		file.SetIniInt("Messager", "UseNet", helios.m_settings.m_bUseNetwork?1:0);
		file.SetIniInt("Messager", "UseLog", helios.m_settings.m_bUseLog?1:0);

		file.SetIniString("Database", "DSN", helios.m_settings.m_pszDSN);
		file.SetIniString("Database", "DBUser", helios.m_settings.m_pszUser);
		file.SetIniString("Database", "DBPassword", helios.m_settings.m_pszPW);
		file.SetIniString("Database", "SettingsTableName", helios.m_settings.m_pszSettings);  // the Settings table name
		file.SetIniString("Database", "ExchangeTableName", helios.m_settings.m_pszExchange);  // the Exchange table name
		file.SetIniString("Database", "MessagesTableName", helios.m_settings.m_pszMessages);  // the Messages table name

		file.SetIniString("OmnibusXML", "DataFields", helios.m_settings.m_pszOmnibusDataFields);  // the data fields we care about
		file.SetIniString("OmnibusXML", "DebugLog", helios.m_settings.m_pszDebugLog);  
		file.SetIniString("OmnibusXML", "CommLog", helios.m_settings.m_pszCommLog);  
		file.SetIniInt("OmnibusXML", "WriteXML", helios.m_settings.m_bWriteXML?1:0);  
		file.SetIniInt("OmnibusXML", "PurgeCheckInterval", helios.m_settings.m_ulCheckInterval);	// number of seconds between checks (0 turns off)
		file.SetIniInt("OmnibusXML", "PurgeExpiryPeriod", helios.m_settings.m_ulExpiryPeriod);	// (default value 3 hours) when the event's end time (start plus duration) is more than this number of seconds older than the current server time, it is purged.
		file.SetIniInt("OmnibusXML", "ConnectionTimeout", helios.m_settings.m_ulConnTimeout);	// (default value 2 mins) 

		file.SetIniString("Database", "ChannelsTableName", helios.m_settings.m_pszChannels);  // the Channels table name
		file.SetIniString("Database", "ConnectionsTableName", helios.m_settings.m_pszConnections);  // the Connections table name
		file.SetIniString("Database", "LiveEventsTableName", helios.m_settings.m_pszLiveEvents);  // the LiveEvents table name

		file.SetIniInt("Database", "ModificationCheckInterval", helios.m_settings.m_ulModsIntervalMS);  // in milliseconds

		file.SetSettings(HELIOS_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

	}
*/

//helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "id-001");Sleep(100);
	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Helios is exiting");
	helios.m_data.m_ulFlags &= ~CX_ICON_MASK;
	helios.m_data.m_ulFlags |= CX_STATUS_UNINIT;
	helios.m_data.SetStatusText(errorstring, helios.m_data.m_ulFlags);
//helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "id-002");Sleep(100);


	//exiting
	helios.m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "-------------- Helios %s exit ---------------\n\
--------------------------------------------------\n", HELIOS_CURRENT_VERSION);  //(Dispatch message)
///	m_pDlg->SetProgress(HELIOSDLG_CLEAR); // no point

//helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "id-003");Sleep(100);

	_ftime( &helios.m_data.m_timebTick );
	helios.m_data.m_ulFlags &= ~HELIOS_STATUS_THREAD_MASK;
	helios.m_data.m_ulFlags |= HELIOS_STATUS_THREAD_ENDED;

//helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "id-004");Sleep(100);

	Sleep(250); // let the message get there.
//	helios.m_msgr.m_bKillThread = true;
//helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "id-005");Sleep(100);

	EnterCriticalSection(&helios.m_data.m_critSQL);
//helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "id-006");Sleep(100);
	helios.m_settings.m_pdbConn = NULL;
	helios.m_settings.m_pdb = NULL;
	helios.m_data.m_pdbConn = NULL;
	helios.m_data.m_pdb = NULL;
//helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "id-007");Sleep(100);

	db.RemoveConnection(pdbConn);
	db2.RemoveConnection(pdb2Conn);
//helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "id-008");Sleep(100);
	LeaveCriticalSection(&helios.m_data.m_critSQL);

	Sleep(250); // need 1 second total after killstatus, before g_phelios = NULL;.  so far, 750.
	helios.m_msgr.RemoveDestination("log");
	helios.m_msgr.RemoveDestination("email");

//helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "id-009");Sleep(100);
// moved these to end.
//	helios.m_msgr.m_bKillThread = true;
//	helios.m_msgr.RemoveDestination("log");

	int nClock = clock() + 300; // small delay at end
	while(clock()<nClock)	{_ftime( &helios.m_data.m_timebTick );}
//helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "id-010");Sleep(100);
	g_bThreadStarted = false;

	nClock = clock() + helios.m_settings.m_nThreadDwellMS; // another small delay at end
	while(clock()<nClock)	{_ftime( &helios.m_data.m_timebTick );}
//helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "id-011");Sleep(100);
	g_phelios = NULL;
//helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "id-012");Sleep(100);
	Sleep(200); //one more small delay at end for the kill timer to be able to hit.
//helios.m_msgr.DM(MSG_ICONNONE, NULL, "Helios:uninit", "id-013");Sleep(100);
	_endthread();
}

/*
void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((g_pmsgr)&&(g_pmsgr->m_lpfnDM))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		CMessagingObject msg;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = msg.EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = msg.EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = msg.EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch;
		pch = (char*) malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
* /
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if(pszEncodedMsg) 
			{
				ulBufferLen = strlen(pszEncodedMsg);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedCaller)
			{
				ulBufferLen = strlen(pszEncodedCaller);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedDest)
			{
				ulBufferLen = strlen(pszEncodedDest);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			g_pmsgr->m_lpfnDM(pch, ulFlags);
			free(pch);
		}
	}
}
*/


/*
// this thread is based on void HTTP10HandlerThread(void* pvArgs) from
// HTTP10.cpp.  It makes a special webserver with security, cgi parsing, and dynamic results
void HeliosHTTPThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return;}

	CHeliosMain* pHelios = (CHeliosMain*)(pClient->m_lpObject) ;  // pointer to the global object.

// HTTP service by def is a non-persistent service, so even if the Client type is
// set to persist, we ignore that directive in this handler thread

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;


		CHTTP10* phttp = (CHTTP10*)(&(pHelios->m_http)) ;  // pointer to the global object.
		if(phttp == NULL)// cant do anything!
		{
			shutdown(pClient->m_socket, SD_BOTH);
			closesocket(pClient->m_socket);
			(*(pClient->m_pulConnections))--;
			_endthread(); return;
		}

		CHTTP10 http(phttp->m_pszRoot);  // local object for utility functions ONLY.
		// this allows the local to clone the root setting of the global, 
		// in order to do correct parsing in its own thread

		// the following are just pointers to the global obj.  
		// must NULL these out before object gets destroyed, 
		// so that global resources not freed!   !!!!!!!  important !!!!!
		http.m_pszHost = phttp->m_pszHost;
		http.m_ppszErrors = phttp->m_ppszMime;
		http.m_ppszMime = phttp->m_ppszMime;   
		http.m_ulStatus = phttp->m_ulStatus;   // important for redirect and error
		
		unsigned char* pch;// for use with GetLine().
		unsigned long ulBufLen;// for use with GetLine().
		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		unsigned short usCode = 0;

		// get the raw buffer and process it yourself
		nReturn = http.m_net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_EOLN|EOLN_HTTP, pszStatus);
		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). 
			//was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
		{
			//error.
			if(pClient->m_lpMsgObj)
			{
				_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error receiving data.  %s", pszStatus);
				((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
			}

			usCode = 400;  //  we TRY to send this....
			if(pch!=NULL)
			{
				free(pch);  // must free the incoming data buffer 
				pch = NULL;
			}
			// create a return message buffer.
			pch = (unsigned char*)http.ErrorBuffer(NULL, usCode);   // 400 bad request
			ulBufLen = strlen((char*)pch); // valid from an error page.

			// send malformed request answer if possible  - have to tell the client that the request was not received in good order.
			nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
			if(nReturn<NET_SUCCESS)
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
				}
			}
		}
		else  // successful reception of data.
		{
			// process the data here...
			// in this sample the buffer is loaded into
			// pch, with ulBufLen as length

/*
			FILE* fp = fopen("rec.txt", "ab");
			if (fp)
			{
				fwrite(pch, 1, ulBufLen, fp );
				fflush(fp);
				fclose(fp);
			}
* /
			// parse buffer, format reply.
			CHTTPHeader header;

			if (http.ParseHeader(pch, ulBufLen, &header) < HTTP_SUCCESS)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("header not parsed\r\n\r\n", 1, strlen("header not parsed\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}
* /
				
				if(pch!=NULL) 
				{
					free(pch);  // must free the incoming data buffer 
					pch = NULL;
				}
				// create a return message buffer.
				usCode = 400;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}
			else
			{
				// here we have enough info to check security if desired, and if not, return a 401;
				// else continue....
/*
					FILE* fp = fopen("rec.txt", "ab");
					if (fp)
					{// lets write out the whole header so we can examine its full contents. including args.
						char bufferin[10000];
						sprintf(bufferin,
"header successfully parsed\r\n\
method: %d\r\n\
version: %s\r\n\
URL: %s\r\n\
Path: %s\r\n\
mimetype: %s\r\n\
user: %s\r\n\
password: %s\r\n\
num args: %ld\r\n",
	header.m_ucMethod,  // enumerated values
	header.m_pszVersion==NULL?"(null)":header.m_pszVersion,
	header.m_pszURL==NULL?"(null)":header.m_pszURL,
	header.m_pszPath==NULL?"(null)":header.m_pszPath,
	header.m_pszMimeType==NULL?"(null)":header.m_pszMimeType,
	header.m_pszUser==NULL?"(null)":header.m_pszUser,
	header.m_pszPassword==NULL?"(null)":header.m_pszPassword,
	header.m_ulNumArgs

);
		
	unsigned long k=0;
	while(k<header.m_ulNumArgs)
	{
		if((header.m_ppszArgNames)&&(header.m_ppszArgValues))
		{
			strcat(bufferin, "  ");
			strcat(bufferin, header.m_ppszArgNames[k]==NULL?"(null)":header.m_ppszArgNames[k]);
			strcat(bufferin, "=");
			strcat(bufferin, header.m_ppszArgValues[k]==NULL?"(null)":header.m_ppszArgValues[k]);
			strcat(bufferin, "\r\n");
		}
	k++;
	}

	strcat(bufferin, "headers:\r\n");
	strcat(bufferin, header.m_pszHeaders);
	strcat(bufferin, "\r\n");


			//		fwrite("header successfully parsed\r\n", 1, strlen("header successfully parsed\r\n"), fp );
						fwrite(bufferin, 1, strlen(bufferin), fp );
						fflush(fp);
						fclose(fp);
					}

* /


				if((phttp->m_pSecure)&&((header.m_pszUser==NULL)||(header.m_pszPassword==NULL)))
				{
					usCode = 401;
					pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
					ulBufLen = strlen((char*)pch); // valid from an error page.
					// buffer successfully parsed.
				}
				else 
				{
					if(pch!=NULL)
					{
						free(pch);  // must free the incoming data buffer 
						pch = NULL;
					}

					// create a return message buffer.
					switch(header.m_ucMethod)
					{
					case HTTP_GET://			1  // HTTP/1.0
					case HTTP_HEAD://			2  // HTTP/1.0
						{
							if(
									(strnicmp(header.m_pszPath, http.m_pszRoot, strlen(http.m_pszRoot)) == 0 )  // else not a part of the served file tree
								)
							{
								if(phttp->m_pSecure)
								{
									// have to check the main object for permission.
									// we are going to go by URL (asset level)
									// Loci are going to just be convenient names for the admins to set up.

									if((phttp->m_pSecure->CheckSecure(header.m_pszUser, header.m_pszPassword, NULL, header.m_pszURL))!=SECURE_SUCCESS)
									{
										usCode = 401;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
										ulBufLen = strlen((char*)pch); // valid from an error page.
										break; // get out of the switch from here.
									}
									// else it flows thru....
								}

								DWORD dwAttrib;

								dwAttrib = GetFileAttributes(header.m_pszPath);

								// check for directory (redirect nec.)
								if ( (dwAttrib!=0xffffffff)&&(dwAttrib&FILE_ATTRIBUTE_DIRECTORY) )  // if error, it may be because no file exists, so check in the next part.
								{
									usCode = 301;  // not necessary but why not.
									pch = (unsigned char*)http.RedirectBuffer(&header);
									ulBufLen = strlen((char*)pch); // valid from an redirect page.
								}
								else
								{
									// before we try to open the file, lets spend the time now (not while the file is open)
									// to figure out how long the static text respone buffer will be.
									CBufferUtil bu;
									// notice the sample "Server: HeliosServer/3.0.3.1" header included.  Pick the appname and ver, probably best
									unsigned long ulBufferLen = strlen("HTTP/1.0 200 OK\r\nServer: HeliosServer/\r\nContent-Length: \r\n\r\n");
									ulBufferLen += strlen(HELIOS_CURRENT_VERSION);

									if((header.m_pszMimeType)&&(strlen(header.m_pszMimeType)>0))
									{
										ulBufferLen += (strlen("Content-Type: \r\n") + strlen(header.m_pszMimeType)); // only add if known!
									}
									else // empty!
									{
										// dont free the "permanent resource", just null the pointer
										header.m_pszMimeType=NULL;
									}

									FILE* pFile = NULL;

									pFile = fopen(header.m_pszPath, "rb");
									if (pFile == NULL)  // file not openable, or not found.
									{
										usCode = 404;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 404 not found
										ulBufLen = strlen((char*)pch); // valid from an error page.
									}
									else
									{
										// this part just assembles the file into a buffer.
										// this may be modified to do dynamic html things by using the 
										// pClient->m_lpObject that refers back to some parser....

										usCode = 200; // OK  (!)
										// determine file size
										fseek(pFile, 0, SEEK_END);
										unsigned long ulFileLen = ftell(pFile);


										// here we have to parse out any directives.  if this is a template file,
										// we may have a variable length.
									//	x


										if(header.m_ucMethod == HTTP_GET)
										{
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += (strlen(length)+ulFileLen);

											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you truncations at zeroed bytes

											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: HeliosServer/%s", HELIOS_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
											
											fclose(pFile);
										}
										else
										if(header.m_ucMethod == HTTP_HEAD)
										{
											fclose(pFile);  // we have all we need already.
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += strlen(length);
											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											//  create the header
											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: HeliosServer/%s", HELIOS_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												// its identical except for the body.  next line must stay commented out, its just there for ref.
												//fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
										}
									}
								}
							}
							else  // not part of the server service.
							{
								usCode = 403;
								pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 403 forbidden ...
								ulBufLen = strlen((char*)pch); // valid from an error page.
							}

						} break;
					case HTTP_POST://			3  // HTTP/1.0
						{
							usCode = 501;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 501 not implemented ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_PUT://			4  // HTTP/1.1
					case HTTP_DELETE://		5  // HTTP/1.1
					case HTTP_OPTIONS://  6  // HTTP/1.1
					case HTTP_TRACE://	  7  // HTTP/1.1
						{
							usCode = 505;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 505 http version not supported ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_UNDEF://		0  // internal
					default:
						{
							usCode = 400;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					}
				}
			}

			// send a reply, in pch., with strlen pch as length

			if(pch == NULL)  // had a problem forming the request, try to send a last ditch error code.
			{
				usCode = 500;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 500 internal server error
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}

			if(pch != NULL)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("sending:\r\n", 1, strlen("sending:\r\n"), fp );
					fwrite(pch, 1, ulBufLen, fp );
					fwrite("\r\n\r\n", 1, strlen("\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}

* /
				nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
				if(nReturn<NET_SUCCESS)
				{
					//error.
					if(pClient->m_lpMsgObj)
					{
						_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
						((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
					}
				}
			}
		}

//		char foo[34]; sprintf(foo, "code: %d", usCode);	AfxMessageBox(foo);

		http.m_pszHost = NULL;		// but do NOT free resource
		http.m_ppszErrors = NULL; // but do NOT free resource
		http.m_ppszMime = NULL;   // but do NOT free resource

		if(pch!=NULL) free(pch);  // must free the data buffer 

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}
*/


void HeliosCommandHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure in Protocol 1  //SECURE with PROTOCOL2 later
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HeliosCommandHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Helios:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HeliosCommandHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Helios:CommandHandlerThread");
						}
						// we had an error receiving data and then anotehr trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{
//AfxMessageBox("receiving");

					// parse the command:

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					switch(data.m_ucCmd)
					{
					case CX_CMD_GETINFO: // not actually supported yet.  everything thru csf files for now.
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case CX_CMD_GETSTATUS: //	0x99 // gets status info
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							unsigned long ulStatus;
							// assemble if there is 

							char* pch = g_phelios->m_data.GetStatusText(&ulStatus);  // allocates mem, must free after use
							if(pch)
							{
								data.m_ucSubCmd = (unsigned char)(HELIOS_ICON_MASK&ulStatus);
								data.m_pucData=(unsigned char*)pch;
								data.m_ulDataLen = strlen(pch);
							}
							else
							{
								data.m_ucCmd = NET_CMD_NAK;
								data.m_ucSubCmd = NET_CMD_NAK;
							}

							data.m_ucType = NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case CX_CMD_BYE:
						{
	g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "HeliosCommandHandlerThread", "The Global Kill command has been received.");  //(Dispatch message)

							bSetGlobalKill = true;
							g_phelios->m_data.m_bQuietKill = true;
							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					default:
						{
							//just ack
							data.m_ucCmd = NET_CMD_ACK;

		//					char repbuf[64];
							if(data.m_pucData!=NULL)
							{
		/*
								_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

								for(unsigned long q=0; q<data.m_ulDataLen; q++)
								{
									if(strlen(repbuf)<63)
										strncat(repbuf, (char*)(data.m_pucData+q), 1);
								}
								if(strlen(repbuf)<63)
									strcat(repbuf, "]");
								data.m_ulDataLen = strlen(repbuf);
								repbuf[data.m_ulDataLen] = 0;  // just in case
		*/
								free(data.m_pucData);  //destroy the buffer;
							}
		/*
							else
							{
								_snprintf(repbuf, 63, "data was NULL");
								data.m_ulDataLen = strlen(repbuf); 
							}

							data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
							if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
							else 
							{
								data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
								data.m_ulDataLen = 0;
							}
		*/
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					}

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HeliosCommandHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Helios:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HeliosCommandHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Helios:CommandHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	try
	{
		delete pClient; // was created with new in the thread that spawned this one.
	}	catch(...) {}
	if (bSetGlobalKill)	//g_bKillThread = true;
	((CHeliosHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}

void HeliosXMLHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;
	
	char szHeliosSource[MAX_PATH]; 
	strcpy(szHeliosSource, "HeliosXMLHandler");

	CBufferUtil bu;

	HRESULT hRes;
	try
	{  

#if _WIN32_WINNT >= 0x0400
		hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		hRes = CoInitialize(NULL);
#endif
	}
	catch(...)
	{
		if(g_phelios) g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, szHeliosSource, "Exception in CoInitialize");  //(Dispatch message)
	}


	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		char dberrorstring[DB_ERRORSTRING_LEN];
//		char xmlerrorstring[MAX_MESSAGE_LENGTH];
		char errorstring[MAX_MESSAGE_LENGTH];
//		char szSQL[DB_SQLSTRING_MAXLEN];
		strcpy(dberrorstring, "");
		strcpy(errorstring, "");

//		char element[MAX_MESSAGE_LENGTH];

//		AfxMessageBox(szHeliosSource);

		CNetUtil net(false); // local object for utility functions.
		CCortexUtil util;  // cortex utility functions.

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		bool bCloseCommand = false;  // make these not persistent unless a persist command comes in.
		bool bPersist = false;  // make these not persistent unless a persist command comes in.
		int nPeriodic = -1;  
		int nTimed = -1;  

		char* pchBuffer = NULL;
		unsigned long ulBufferLen = 0;
		char* pch = NULL;
		char* pchXML = NULL;
		char* pchXMLStream = NULL;
		unsigned long ulAccumulatedBufferLen = 0;
		unsigned long ulRetry = 0;

		unsigned long ulConnLastMessage = 0;
		unsigned long ulConnTimeout = 0;
		
		char filename[MAX_PATH];
		char lastrxfilename[MAX_PATH];
		char lasttxfilename[MAX_PATH];
		int nRxDupes=0;
		int nTxDupes=0;

		FILE* fp = NULL;

		_timeb timestamp;
		_timeb timeactive;
		_timeb timeperiodic;

		_ftime(&timeactive);
		_ftime(&timeperiodic);


	// 	(*(pClient->m_pulConnections))++;//already exists

		IXMLDOMDocumentPtr pDoc(__uuidof(DOMDocument));  // let's have only one object per connection
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread D"); Sleep(100); //(Dispatch message)
		HRESULT hr;//= pDoc.CreateInstance(__uuidof(DOMDocument));
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread E");  //(Dispatch message)
		hr = pDoc->put_async(VARIANT_FALSE);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread F");  //(Dispatch message)
										
		// Remove document safety options
		IObjectSafety* pSafety=NULL;
		DWORD dwSupported, dwEnabled;
			

		if ( SUCCEEDED(pDoc->QueryInterface(IID_IObjectSafety, (void**)&pSafety)))
		{
			pSafety->GetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, &dwSupported, &dwEnabled );
			pSafety->SetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, dwSupported, 0 );
		}

		// initialize random seed
		srand ( time(NULL) );

		CCortexMessage msg;

		// allocate all the static ones
//		char* m_pchResponse[CX_XML_BUFFER_COUNT] = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};

//		AfxMessageBox("formatting");

		msg.FormatContent(CX_XML_BUFFER_CORTEXBEGIN, MAX_MESSAGE_LENGTH, "<cortex src=\"%s\" version=\"%s\">", 
			(g_phelios->m_settings.m_pszName?g_phelios->m_settings.m_pszName:"Helios"), 
			CX_XML_VERSION
			);


		msg.SetContent(CX_XML_BUFFER_CORTEXEND, "</cortex>");

		msg.SetContent(CX_XML_BUFFER_FLAGSEND, "</flags>");

		msg.SetContent(CX_XML_BUFFER_DATAEND, "</data>");

		//  ack and nak are the same length so we can allocate it, just replace contents later
		msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

//		AfxMessageBox("set");



		if (FAILED(hRes))
		{
			sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established, but CoInitialize failed.  Unable to process XML.", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,
				pClient->m_socket);

			if(g_phelios)
			{
				g_phelios->m_msgr.DM(MSG_ICONERROR, NULL, szHeliosSource, errorstring);  //(Dispatch message)
				g_phelios->SendMsg(CX_SENDMSG_ERROR, szHeliosSource, errorstring);
			}

			goto cleanup;
		}

		
		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);
		if(g_phelios)
		{
			g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, szHeliosSource, errorstring);  //(Dispatch message)
			g_phelios->SendMsg(CX_SENDMSG_INFO, szHeliosSource, errorstring);
		}



		while ( 
									(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
//								&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
								&&(pClient->m_socket!=NULL)
								&&(!bCloseCommand)
					)
		{


			pchBuffer = NULL;
			ulBufferLen = 0;

			
			int nReturnCode = net.GetLine((unsigned char**)&pchBuffer, &ulBufferLen, pClient->m_socket, NET_RCV_ONCE, errorstring);
			if(nReturnCode == NET_SUCCESS)
			{
				ulRetry = 0;
				//process any received XML.
				// have to keep accumulating until we find a </cortex> tag.
				if((pchBuffer)&&(ulBufferLen)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
				{
					int nLen = 0;
					if(pchXMLStream) nLen = ulAccumulatedBufferLen;
					pch = (char*) malloc(nLen+ulBufferLen+1);  //term 0
					if(pch)
					{
						char* pchEnd = NULL;
						char* pchNext = NULL;
						if(pchXMLStream)  // we have an old buffer.
						{
							memcpy(pch, pchXMLStream, ulAccumulatedBufferLen);
							memcpy(pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen);
							free(pchXMLStream); 
							pchXMLStream = pch;  // reassign!
							ulAccumulatedBufferLen += ulBufferLen;
							*(pch+ulAccumulatedBufferLen) = 0;  // null term
						}
						else
						{
							// this is new.
							// first we have to skip all chars that are not a '<'
							// pchEnd = strchr(pchBuffer, '<');  // have to find first tag, we are only interested in XML.
							// used to use strchr but if there are leading zeros in the buffer, we never get past them

							pchEnd = pchBuffer;
							while((*pchEnd!='<')&&(pchEnd<pchBuffer+ulBufferLen)) pchEnd++;

							if(pchEnd<pchBuffer+ulBufferLen)
							{
								strcpy(pch, pchEnd);
								ulAccumulatedBufferLen += (ulBufferLen-(pchEnd-pchBuffer));
								pchXMLStream = pch;  // reassign!
								*(pch+ulAccumulatedBufferLen) = 0;  // null term
							}
							else  // not found!
							{
								free(pch);
								pchXMLStream = NULL;
							}
						}

						if(pchBuffer) free(pchBuffer);
						pchBuffer = NULL;
						if(pchXMLStream)
						{

							pchEnd = strstr(pchXMLStream, "</cortex>");
							while((pchEnd)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
							{
							// found a token.
								pchEnd+=strlen("</cortex>");

								pchNext = pchEnd;
								while((*pchNext!='<')&&(pchNext<pchEnd+strlen(pchEnd))) pchNext++;

								if(pchNext<pchEnd+strlen(pchEnd))
								{
									// we found a remainder.
									nLen = strlen(pchNext);
									pch = (char*) malloc(nLen+1);  //term 0
									if(pch)
									{
										memcpy(pch, pchNext, nLen);
										*(pch+nLen) = 0;
									}
								}
								else pch = NULL;


								pchXML = pchXMLStream; // just use it.
								*pchEnd = 0; //null terminate it

								pchXMLStream = pch;  // take the rest of the stream.
								if(pchXMLStream) ulAccumulatedBufferLen = strlen(pchXMLStream);
								else ulAccumulatedBufferLen=0;

								// now have to deal with XML found in pchXML.
								//debug file write
								if((g_phelios)&&(g_phelios->m_settings.m_bUseXMLClientLog))
								{
//				AfxMessageBox("logging");
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_COMM) 
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, szHeliosSource, "begin log receive");  //(Dispatch message)

									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_phelios->m_settings.m_pszName?g_phelios->m_settings.m_pszName:"Helios"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lastrxfilename, filename)==0)
									{
										nRxDupes++;
									}
									else
									{
										nRxDupes=0;
										strcpy(lastrxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_RX.xml",nRxDupes);

//	AfxMessageBox(filename);

									fp = fopen(filename, "wb");
									if(fp)
									{
//											fwrite(filename, 1, strlen(filename),fp);
										fwrite(pchXML, 1, strlen(pchXML),fp);
										fflush(fp);
										fclose(fp);
										fp = NULL;
									}
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_COMM) 
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, szHeliosSource, "end log receive");  //(Dispatch message)

								}

								// do stuff here.

								_ftime(&timeactive);  // reset the inactivity timer.


								// first attempt to extract the rxid.
								char* pchRXID = NULL;
								int nIDlen = 0;

								char* pchFind = strstr(pchXML, "<rx rxid=\"");

								if(pchFind)
								{
									pchFind+=strlen("<rx rxid=\"");
									char* pchTerminate = pchFind;
									while(pchTerminate<pchEnd)
									{
										if(
											  (*pchTerminate == '\"')
											||(*pchTerminate == '>')
											||((pchTerminate<pchEnd)&&(*pchTerminate == '/')&&(*(pchTerminate+1) == '>'))
											)
										{
											break;
										}
										else
										{
											pchTerminate+=1;
										}
									}

									nIDlen = pchTerminate-pchFind;
									if(nIDlen>0)
									{
										pchRXID = (char*)malloc(nIDlen+1);
									}
									if(pchRXID)
									{
										memset(pchRXID, 0, nIDlen+1);
										memcpy(pchRXID, pchFind, nIDlen);

										pchTerminate = bu.XMLEncode(pchRXID); // just in case
										if(pchTerminate)
										{
											try{free(pchRXID);} catch(...){}
											pchRXID = pchTerminate;
										}
									}
								}


							//create a response UUID
								UUID uuid;
								UuidCreate(&uuid); // could check response code to be RPC_S_OK ?

					//			CoCreateGuid(&uuid);

								unsigned char* pucUUID = NULL;
								UuidToString(&uuid, &pucUUID);
								// call this later: RpcStringFree(&pucUUID);

								
								bool bResponseSent = false;  // means, sent a response already.  Otherwise have to look at the failure code and deal at the end

								msg.m_nTxStep = CX_XML_BUFFER_CORTEXBEGIN;

								//OK, let's assemble the tx element.

								if(pchRXID)
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUID);
								}


								// then, parse the XML
								msg.m_nFailure=CX_XML_ERR_SUCCESS;
								pDoc->put_async(VARIANT_FALSE);
			//				AfxMessageBox((char*)pchXML);
								hr = pDoc->loadXML((char*)pchXML);
								if(hr!=VARIANT_TRUE)
								{
								//					AfxMessageBox("bad load");
									bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML parse error: [%s]", pchXML );
									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, szHeliosSource, errorstring);
									//let's try to send a nak.

									msg.m_nFailure=CX_XML_ERR_XMLPARSE;
									msg.m_nType = CX_XML_TYPE_UNK;
									msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
									msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML parse error %d</msg>", msg.m_nFailure);
									msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");
								}
								else
								{
	//								AfxMessageBox("yirgacheff");

if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_COMM) 
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, szHeliosSource, "begin xml parse");  //(Dispatch message)
									IXMLDOMNodeListPtr pChildNodes = pDoc->GetchildNodes();

									IXMLDOMNodePtr pNodes[CX_XML_NODECOUNT];
									int n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }

									msg.m_nType = CX_XML_TYPE_UNK;
									msg.SetType("unknown");

									bool bCommandHandled = false;
									if (pChildNodes != NULL)
									{
										IXMLDOMNodePtr pChild;
										while(pChild = pChildNodes->nextNode())
										{
											DOMNodeType nodeType;
											pChild->get_nodeType(&nodeType);

											if(nodeType == NODE_ELEMENT)
											{
												char tag[MAX_PATH]; 
//												strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
												util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

											//	AfxMessageBox(tag);

												if(strcmp("cortex", tag)==0)
												{
													pNodes[CX_XML_NODE_CORTEX] = pChild;
													// now get subs to cortex

													pChildNodes = pChild->GetchildNodes();
													if (pChildNodes != NULL)
													{
														while(pChild = pChildNodes->nextNode())
														{
															pChild->get_nodeType(&nodeType);

															if(nodeType == NODE_ELEMENT)
															{
//																strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
																util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

														//		AfxMessageBox(tag);

																if(strcmp("rx", tag)==0)
																{
																	pNodes[CX_XML_NODE_RX] = pChild;
																	// now get the "real" rxid;

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
																	//	AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
//																			strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));

								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("rxid", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
//																				strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));

																		//		AfxMessageBox("replace rxid.");

																				if(pchRXID)
																				{
																					try{free(pchRXID);} catch(...){}
																					pchRXID = bu.XMLEncode(tag); // just in case
																					if(pchRXID)
																					{
// reassemble tx element
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
																					}
																				}

																			}
																			else
																			if(strcmp("part", tag)==0)
																			{
																				//  multi part message
																			}
																		}
																	}

																}
																else
																if(strcmp("cmd", tag)==0)
																{
																	pNodes[CX_XML_NODE_CMD] = pChild;
																	pNodes[CX_XML_NODE_OPTIONS] = NULL;
																	pNodes[CX_XML_NODE_DATA] = NULL;

																	// now get attribs and then get subs, data and options

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
								//										AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
//																			strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));
								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("type", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
//																				strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));
																				msg.SetType(tag);

//																																		AfxMessageBox(tag);

																				if(strcmp("ack", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_ACK;
																				}
																				else
																				if(strcmp("nak", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_NAK;
																				}
																				else
																				if(strcmp("persist", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PERSIST;
																					bPersist = true;
																				}
																				else
																				if(strcmp("quit", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_QUIT;
																					bPersist = false;
																				}
																				else
																				if(strcmp("status", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_STATUS;
																				}
																				else
																				if(strcmp("bye", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_BYE;
																				}
																				else
																				if(strcmp("get_version", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_GETVERSION;
																				}
																				else
																				if(strcmp("ping", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PING;
																				}
/*
																				else

																					

/////////////////////////////////////////////////////////////////////////////////////////////////
////                                    begin Helios specific commands
//#define HELIOS_XML_TYPE_GETCONN				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
//#define HELIOS_XML_TYPE_GETLIST				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
//#define HELIOS_XML_TYPE_GETEVENT			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
																				if(strcmp("get_conn", tag)==0)
																				{
																					msg.m_nType = HELIOS_XML_TYPE_GETCONN;
																				}
																				else
																				if(strcmp("get_list", tag)==0)
																				{
																					msg.m_nType = HELIOS_XML_TYPE_GETLIST;
																				}
																				else
																				if(strcmp("get_event", tag)==0)
																				{
																					msg.m_nType = HELIOS_XML_TYPE_GETEVENT;
																				}
																					
////                                    end Helios specific commands
/////////////////////////////////////////////////////////////////////////////////////////////////
*/

																				
																			}
																			else
																			if(strcmp("user", tag)==0)
																			{
																			}
																			else
																			if(strcmp("password", tag)==0)
																			{
																			}
																		}
																	}


																	IXMLDOMNodeListPtr pCmdChildNodes = pChild->GetchildNodes();
																	if (pCmdChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pCmdChild;
																		while(pCmdChild = pCmdChildNodes->nextNode())
																		{
																			pCmdChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
//																				strncpy(tag, W2T(pCmdChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pCmdChild->GetnodeName()));
																				if(strcmp("options", tag)==0)
																				{
																					pNodes[CX_XML_NODE_OPTIONS] = pCmdChild;
																				}
																				else
																				if(strcmp("data", tag)==0)
																				{
																					pNodes[CX_XML_NODE_DATA] = pCmdChild;
																				}
																			}
																		}
																	}
																}
															}
														}
														// by here, all the necessary stuff should be filled out.

//														AfxMessageBox("here");
												

														switch(msg.m_nType)
														{
														case CX_XML_TYPE_ACK://						1	// 4.1 ack Acknowledges a message with success
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the ack

																
															} break; //case CX_XML_TYPE_ACK
														case CX_XML_TYPE_NAK://						2	// 4.2 nak Acknowledges a message with failure, or cancels
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the nak

															} break; //case CX_XML_TYPE_NAK
														case CX_XML_TYPE_PERSIST://				3	// 4.3 persist Requests a persistent connection
															{
																//persistence already done, above
																// set timeout and hearbeat intervals
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"persist\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																if(pNodes[CX_XML_NODE_OPTIONS])
																{

																	// have to return flags
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																	IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																	if (pOptChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pOptChild;
																		while(pOptChild = pOptChildNodes->nextNode())
																		{
																			pOptChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
//																				strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pOptChild->GetnodeName()));
																				if(strcmp("timeout", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						nTimed = atoi(pchTmp);

																						_snprintf(tag, MAX_PATH, "<timeout>%d</timeout>", nTimed);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;
																					}
																				}
																				else
																				if(strcmp("heartbeat", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						strncpy(tag, pchTmp, MAX_PATH);

																						if(strcmp("yes", tag)==0)
																						{
																							nPeriodic = 30;
																						}
																						else
																						if(strcmp("no", tag)==0)
																						{
																							nPeriodic = -1;
																						}
																						else
																						{
																							nPeriodic = atoi(tag);
																						}

																						_snprintf(tag, MAX_PATH, "<heartbeat>%d</heartbeat>", nPeriodic);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );


																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;

																					}
																				}
																			}
																		}
																	}
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																}
																
															} break; //case CX_XML_TYPE_PERSIST
														case CX_XML_TYPE_QUIT://					4	// 4.4 quit Disconnects a persistent connection
															{
																//already done, above
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"quit\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																
															} break; //case CX_XML_TYPE_QUIT
														case CX_XML_TYPE_STATUS://				5	// 4.5 status Obtains status information regarding the module
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"status\" code=\"0\">");

																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																char* pchStatus = g_phelios->m_data.GetStatusText(&ulDataLen);
																if(pchStatus)
																{
																	msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<status><code>0x%08x</code><text>%s</text></status>",
																		ulDataLen,
																		pchStatus	
																		);
																}
																else
																{
																	msg.SetContent(CX_XML_BUFFER_DATA, 
																		"<status><code>0xffffffff</code><text>unavailable</text></status>"
																		);

																}

																if(pchStatus){try { free(pchStatus); } catch(...){}  pchStatus=NULL;}

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


															} break; //case CX_XML_TYPE_STATUS
														case CX_XML_TYPE_BYE://						6	// 4.6 bye Causes the module to shut down
															{

g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "The Global Kill XML command has been received.");  //(Dispatch message)

																bSetGlobalKill = true;
																g_phelios->m_data.m_bQuietKill = true;
																bCloseCommand = true;

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"bye\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_GETVERSION://		7	// 4.7 get_version gets the version of the module as well as the build date
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_version\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<version>%s</version><build>%s %s</build>",
																		CX_CURRENT_VERSION,
																		 __DATE__,
																		 __TIME__	
																		);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_PING://					8	// 4.8 ping just sends an ack to indicate it is alive
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"ping\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE


////////////////////////////////////////////////////////
// begin Helios specific XML commands
/*
														case HELIOS_XML_TYPE_GETCONN://				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_conn\" code=\"%d\">", g_phelios->m_data.m_nNumConnectionObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_phelios->m_data.m_critConns);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_phelios->m_data.m_ppConnObj)&&(g_phelios->m_data.m_nNumConnectionObjects))
																{

																	//let's see if we have any options.

																	char* pchServer = NULL;  //server name search
																	int nActive =-1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{

																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("active", tag)==0)
																					{
																				//		AfxMessageBox("active");
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}
																					//		AfxMessageBox("active 2");

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																				//			AfxMessageBox(tag);

																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_phelios->m_data.m_nNumConnectionObjects)
																	{

																		if(g_phelios->m_data.m_ppConnObj[m])
																		{
																			bool bInclude = true;
																			CHeliosConnectionObject* pObj = g_phelios->m_data.m_ppConnObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																	//		AfxMessageBox("here");
																			if((nActive==0)&&(pObj->m_ulFlags&HELIOS_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&HELIOS_FLAG_ENABLED))) bInclude = false;

																	//			AfxMessageBox("here 2");
																			if(bInclude)
																			{
																				if(pObj->m_pAPIConn)
																				{
																					if((pObj->m_pAPIConn->m_SysData.systemfrx==0x29)
																						&&(pObj->m_pAPIConn->m_SysData.systemdf))
																					{
																						strcpy(tag, "29.97");
																					}
																					else
																					{
																						sprintf(tag, "%02x",pObj->m_pAPIConn->m_SysData.systemfrx);
																					}
																				}

																				_ftime(&timestamp);
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<aconn>\
<config active=\"%d\">\
<server>%s</server>\
<client>%s</client>\
<port>N/A</port>\
<basis>%s</basis>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<server_time>%.03f</server_time>\
<server_status>%d</server_status>\
<server_lists>%d</server_lists>\
<server_changed>%d</server_changed>\
<server_last_update>%.03f</server_last_update>\
<message>%s</message>\
</status>\
</aconn>",
																					((pObj->m_ulFlags&HELIOS_FLAG_ENABLED)?1:0),
																					pObj->m_pszServerName,
																					pObj->m_pszClientName,
																					(pObj->m_pAPIConn?tag:"N/A"),
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(pObj->m_dblLastServerTimeMS/1000.0),
																					(((pObj->m_ulStatus&HELIOS_ICON_MASK) != HELIOS_STATUS_CONN)? -1 : (pObj->m_pAPIConn?pObj->m_pAPIConn->m_Status:-1)),
																					(pObj->m_pAPIConn?pObj->m_pAPIConn->m_SysData.systemlistcount:-1),
																					(pObj->m_pAPIConn?pObj->m_pAPIConn->m_SysData.syschanged:-1),
																					pObj->m_dblUpdateTime,
																					(((pObj->m_ulStatus&HELIOS_ICON_MASK) != HELIOS_STATUS_CONN)? (((pObj->m_ulStatus&HELIOS_ICON_MASK) == HELIOS_STATUS_ERROR)?"connection error":"not connected"):"")

																																										
																				);
																					
																			

																				msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}

																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;

																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_phelios->m_data.m_critConns);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);


															} break; //case HELIOS_XML_TYPE_GETCONN

														case HELIOS_XML_TYPE_GETLIST://				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_list\" code=\"%d\">", g_phelios->m_data.m_nNumChannelObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																
EnterCriticalSection(&g_phelios->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_phelios->m_data.m_ppChannelObj)&&(g_phelios->m_data.m_nNumChannelObjects))
																{

																	//let's see if we have any options.

																	char* pchServer = NULL;  //server name search
																	int nActive =-1;
																	int nID=-1;
																	int nListNum = -1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{

																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("id", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nID = atoi(pchTmp);
																								if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("list_number", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nListNum = atoi(pchTmp);
																								if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("active", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_phelios->m_data.m_nNumChannelObjects)
																	{

																		if(g_phelios->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CHeliosChannelObject* pObj = g_phelios->m_data.m_ppChannelObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&HELIOS_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&HELIOS_FLAG_ENABLED))) bInclude = false;

																			if((nID>0)&&(pObj->m_nChannelID != nID))  bInclude = false;
																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				_ftime(&timestamp);

																				tlistdata* ptlist = NULL;
																				if(pObj->m_pAPIConn)
																				{
																					ptlist = &(pObj->m_pAPIConn->m_ListData[pObj->m_nHarrisListID-1]);
																				}
																				
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<alist>\
<config active=\"%d\" id=\"%d\">\
<server>%s</server>\
<list_number>%d</list_number>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<list_state>%d</list_state>\
<list_changed>%d</list_changed>\
<list_display>%d</list_display>\
<list_syschange>%d</list_syschange>\
<list_count>%d</list_count>\
<list_lookahead>%d</list_lookahead>\
<list_last_update>%.3f</list_last_update>\
</status>\
</alist>",
																					((pObj->m_ulFlags&HELIOS_FLAG_ENABLED)?1:0),
																					pObj->m_nChannelID,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(ptlist?ptlist->liststate:-1),
																					(ptlist?ptlist->listchanged:-1),
																					(ptlist?ptlist->listdisplay:-1),
																					(ptlist?ptlist->listsyschange:-1),
																					(ptlist?ptlist->listcount:-1),
																					(ptlist?ptlist->lookahead:-1),
																					pObj->m_dblUpdateTime
																																										
																				);
																					
																			

																				msg.FormatContent(CX_XML_BUFFER_MSG, HELIOS_XML_LIST_MAXPAYLOAD, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}

																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;

																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_phelios->m_data.m_critChannels);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case HELIOS_XML_TYPE_GETLIST
														case HELIOS_XML_TYPE_GETEVENT://			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
															{

																msg.ClearContent(CX_XML_BUFFER_MSG);
																int nGlobalCount =0;
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_phelios->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_phelios->m_data.m_ppChannelObj)&&(g_phelios->m_data.m_nNumChannelObjects))
																{

																	//let's see if we have any options.

//																	char* pchServer = NULL;  //server name search
//																	int nActive =-1;
																	int nID=-1;
																	int nLimitNum = -1;
																	int nDone = -1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{
																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("id", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nID = atoi(pchTmp);
																								if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("limit", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nLimitNum = atoi(pchTmp);
																								if(nLimitNum <=0) nLimitNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<limit>%d</limit>", nLimitNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("done", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nDone = 0;
																								else if(strcmp("1", pchTmp)==0) nDone = 1;
																								else if(strcmp("yes", pchTmp)==0) nDone = 1;
																								else if(strcmp("no", pchTmp)==0) nDone = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<done>%d</done>", nDone);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}

																					/*
																					else
																					if(strcmp("list_number", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nListNum = atoi(pchTmp);
																								if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("active", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																					* /
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_phelios->m_data.m_nNumChannelObjects)
																	{

																		if(g_phelios->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CHeliosChannelObject* pObj = g_phelios->m_data.m_ppChannelObj[m];

/*
																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&HELIOS_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&HELIOS_FLAG_ENABLED))) bInclude = false;

* /																		if(
																				//	(nID>0)&&  //commenting this out makes it mandatory!
																					(pObj->m_nChannelID != nID)
																				)  
																				bInclude = false;

//																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				// found a channel that works, now must loop thru the events, filtering if nec.

																				// need to allocate a big buffer and deal with directly, rather than allocating new for every event.
																			
/* // was this, which worked but slowly.  below is new
																				if(pObj->m_ppevents)
																				{
																					 
																					int q = 0;
																					int nCount = 0;
																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;
																						unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));

																						if(pObj->m_ppevents[q])
																						{
																							CHeliosEventObject* pEObj = pObj->m_ppevents[q];

																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;
																							if(bInclude)
																							{

																								_ftime(&timestamp);

																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
																								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																									"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_usControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CHeliosEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																								);
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																								msg.FormatContent(CX_XML_BUFFER_MSG, HELIOS_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );

																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
* /




																				char* pchPayload = (char*)malloc(HELIOS_XML_EVENT_MAXPAYLOAD);
																				char* pchAppend = pchPayload;

																				// re-use errorstring, why not.
																		//		int nTimes[10] = {0,0,0,0,0,0,0,0,0,0};
																				int nClock = clock();
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_COMM) 
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, szHeliosSource, "start event payload assembly");  //(Dispatch message)

																				if((pObj->m_ppevents)&&(pchPayload))
																				{
																					unsigned long ulBufferLen = HELIOS_XML_EVENT_MAXPAYLOAD;
																					char* pchBufferEnd = (pchPayload+HELIOS_XML_EVENT_MAXPAYLOAD);
																					int nEventLen = 0;
																					unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));
																					int q = 0;
																					int nCount = 0;
																					int nBuffers = 1;
																					int nTempOffset;
																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;

																						if(pObj->m_ppevents[q])
																						{
																					//		nTimes[0] += (clock() - nClock); nClock = clock(); 

																							CHeliosEventObject* pEObj = pObj->m_ppevents[q];
																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;

																				//			nTimes[1] += (clock() - nClock); nClock = clock(); 
																							if(bInclude)
																							{

																							//	_ftime(&timestamp);
																					//		nTimes[2] += (clock() - nClock); nClock = clock(); 

																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
																					//		nTimes[3] += (clock() - nClock); nClock = clock(); 
																								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, 
																									"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_usControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CHeliosEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																								);
																					//		nTimes[4] += (clock() - nClock); nClock = clock(); 
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																						//	nTimes[5] += (clock() - nClock); nClock = clock(); 
																								nEventLen = strlen(errorstring)+1;//+1 for term zero

																								if(pchAppend+nEventLen>pchBufferEnd)  // if must be changed to while if HELIOS_XML_EVENT_MAXPAYLOAD defined < MAX_MESSAGE_LENGTH (buffer vs errorstring)
																								{
																									// need to realloc
																									nBuffers++;
																									ulBufferLen = HELIOS_XML_EVENT_MAXPAYLOAD*nBuffers;
																									char* pchBufferNew = (char*)malloc(ulBufferLen);
																									if(pchBufferNew)
																									{
																										nTempOffset = pchAppend+1 - pchPayload;
																										memcpy(pchBufferNew, pchPayload, nTempOffset);
																										try {free(pchPayload);} catch(...){}
																										pchPayload = pchBufferNew;
																										pchBufferEnd = (pchPayload+ulBufferLen);
																										pchAppend = pchPayload+nTempOffset-1;																									

																									}
																								}
																								

																								// append
																								memcpy(pchAppend, errorstring, nEventLen);
																								pchAppend += nEventLen-1; // append over the term 0 next time.


																						//	nTimes[6] += (clock() - nClock); nClock = clock(); 


/*
																								msg.FormatContent(CX_XML_BUFFER_MSG, HELIOS_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
* /
																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
																				msg.ClearContent(CX_XML_BUFFER_DATA);
																				if(pchPayload)
																				{
																					msg.m_pchResponse[CX_XML_BUFFER_DATA] = pchPayload;		
																					ulDataLen = pchAppend - pchPayload;
																				}
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_COMM) 
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, szHeliosSource, "end event payload assembly");  //(Dispatch message)

																			}

																		}
																		m++;
																	}

/*																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;
* /
																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_phelios->m_data.m_critChannels);

/* // not needed now, we tally the buffer as we go
																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																* /
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_event\" code=\"%d\">", nGlobalCount);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case HELIOS_XML_TYPE_GETEVENT

// end Helios specific XML commands
////////////////////////////////////////////////////////
*/
														case CX_XML_TYPE_UNK://						0 // unknown
														default:
															{
																sprintf(errorstring, "Unsupported XML command: [%s]", (msg.m_pchType?msg.m_pchType:"") );
																g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, szHeliosSource, errorstring);
																//let's try to send a nak.

																msg.m_nType = CX_XML_TYPE_UNK;
																msg.m_nFailure=CX_XML_ERR_BADCMD;
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"%s\" code=\"%d\">", (msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Unsupported XML command [%s] (error %d)</msg>",(msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

															} break; //case CX_XML_TYPE_UNK://						0 // unknown
														}
															
														
														if(!bPersist) bCloseCommand = true;

													}
													else // no child nodes to cortex - can't identify command.
													{
														bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
														sprintf(errorstring, "Cortex XML format error: [%s]", pchXML );
														g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, szHeliosSource, errorstring);
														//let's try to send a nak.

														msg.m_nType = CX_XML_TYPE_UNK;
														msg.m_nFailure=CX_XML_ERR_NOCMD;
														msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
														msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Cortex XML format error %d</msg>", msg.m_nFailure);
														msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

													}
												}
											}
										}
									}
									else
									{
										// no child nodes, can't do nuttin.
										bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
										sprintf(errorstring, "XML format error: [%s]", pchXML );
										g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, szHeliosSource, errorstring);
										//let's try to send a nak.

										msg.m_nType = CX_XML_TYPE_UNK;
										msg.m_nFailure=CX_XML_ERR_NOCHILD;
										msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
										msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML format error %d</msg>", msg.m_nFailure);
										msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

									}
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_COMM) 
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, szHeliosSource, "end xml parse");  //(Dispatch message)

									n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }
								}

								if(!bResponseSent)
								{
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_COMM) 
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, szHeliosSource, "begin send response");  //(Dispatch message)

								// send it
		_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_phelios)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending response message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_phelios->m_msgr.DM(MSG_ICONERROR, NULL, szHeliosSource, errorstring);  //(Dispatch message)
				g_phelios->SendMsg(CX_SENDMSG_ERROR, szHeliosSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_COMM) 
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, szHeliosSource, "end send response");  //(Dispatch message)

								}

								// log it

								//debug file write
								if((g_phelios)&&(g_phelios->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
								{
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_COMM) 
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, szHeliosSource, "begin log response");  //(Dispatch message)
									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_phelios->m_settings.m_pszName?g_phelios->m_settings.m_pszName:"Helios"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lasttxfilename, filename)==0)
									{
										nTxDupes++;
									}
									else
									{
										nTxDupes=0;
										strcpy(lasttxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

									fp = fopen(filename, "wb");
									if(fp)
									{
										int b=CX_XML_BUFFER_CORTEXBEGIN;
										while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
										{
//											fwrite(filename, 1, strlen(filename),fp);
											if(msg.m_pchResponse[b])
											{
												if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

												fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
												fflush(fp);
											}
											b++;

										}
										fclose(fp);
										fp = NULL;
									}
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_COMM) 
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, szHeliosSource, "end log response");  //(Dispatch message)
								}

//AfxMessageBox("001");
								int b=CX_XML_BUFFER_TX;
								while(b<CX_XML_BUFFER_DATAEND)
								{
									if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
									{
										try { free(msg.m_pchResponse[b]); } catch(...){}
										msg.m_pchResponse[b] = NULL;
									}
									b++;
								}

//AfxMessageBox("002");
								if(pucUUID)
								{
									try { RpcStringFree(&pucUUID); } catch(...){}
								}
								pucUUID = NULL;

//AfxMessageBox("003");
								if(pchXML) 
								{
									try { free(pchXML); } catch(...){}
								}
								pchXML = NULL;
//AfxMessageBox("004");

								if(pchRXID) 
								{
									try { free(pchRXID); } catch(...){}
								}
								pchRXID = NULL;

//AfxMessageBox("005");
								if (pchXMLStream) 
									pchEnd = strstr(pchXMLStream, "</cortex>");
								else pchEnd = NULL;
//AfxMessageBox("006");
							}  // while </cortex> exists.
						}//if(pchXMLStream)
						// dont do a free(pch);  
					}  // else out of mem, so just skip
					else
					{
						// out of mem.. .log error?
					}

				} //if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))

			} // if getline succeeds.
			else
			{
				// here, we either have timed out because theres no data, or, the connection has been lost.
				// if we lost the conn, no need to re-establish, we are a client handler, let the client reconnect if they want.
				if(nReturnCode == NET_ERROR_CONN)		// connection lost
				{
					ulRetry++;

					if(g_phelios)
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH, "Lost connection from %d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno );

						g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, szHeliosSource, errorstring);  //(Dispatch message)
						g_phelios->SendMsg(CX_SENDMSG_INFO, szHeliosSource, errorstring);
					}

					break;  // break out of while loop, closes connection, starts again
				}
				else
				{
//									g_phelios->m_msgr.DM(MSG_ICONERROR, NULL, szHeliosSource, "check %d", clock());  //(Dispatch message)

					// we prob just havent gotten a message because no data.  
					// so, check to see if its time for publication service
									// no, do this in the stall loop


				}

			} // end of else from if(nReturnCode == NET_SUCCESS)

			// and lets check if we've timed out on the connection, no matter what the success was
			_ftime( &timestamp );
			if(
				  (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				&&(ulConnTimeout>0)
				&&(ulConnLastMessage>0)
				&&((ulConnLastMessage + ulConnTimeout) < (unsigned long)timestamp.time )
				)
			{
				// we timed out, need to disconnect
				ulRetry++;


				sprintf(errorstring, "Connection from %d.%d.%d.%d timed out.", 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno);

				if(g_phelios)
				{
					g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, szHeliosSource, errorstring);  //(Dispatch message)
					g_phelios->SendMsg(CX_SENDMSG_INFO, szHeliosSource, errorstring);
				}

				if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;} //free this memory before breaking out of the while.

				break;  // break out of while loop, closes connection, starts again
			}

			if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;}
			
			if(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) Sleep(1);  // dont peg processor




/*
			// following line fills out the data structure
//			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);
			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Helios:XMLHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Helios:XMLHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Helios:XMLHandlerThread");
						}
					}
				}
			}
*/
			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Helios:XMLHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
//						g_phelios->m_msgr.DM(MSG_ICONERROR, NULL, szHeliosSource, "check %d", clock());  //(Dispatch message)


						if(nPeriodic>0)
						{
							_ftime( &timestamp );
							if((timeperiodic.time + nPeriodic)< timestamp.time)
							{
								// send asynchronous message

								//create a response UUID
								UUID uuida;
								UuidCreate(&uuida); // could check response code to be RPC_S_OK ?

								unsigned char* pucUUIDa = NULL;
								UuidToString(&uuida, &pucUUIDa);

								msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUIDa);

								if(pucUUIDa)
								{
									try { RpcStringFree(&pucUUIDa); } catch(...){}
								}
								pucUUIDa = NULL;


								msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"system\" code=\"0\">");

								msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
								msg.ClearContent(CX_XML_BUFFER_FLAGS);

								unsigned long ulDataLen = 0;
								char* pchStatus = g_phelios->m_data.GetStatusText(&ulDataLen);
								if(pchStatus)
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0x%08x</code><text>%s</text></status><time>%d.%03d</time></system>",
										ulDataLen,
										pchStatus,
										timestamp.time, timestamp.millitm
										);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0xffffffff</code><text>unavailable</text></status><time>%d.%03d</time></system>",
										timestamp.time, timestamp.millitm
										);

								}

								if(pchStatus){try { free(pchStatus); } catch(...){} pchStatus=NULL;}

								ulDataLen = 0;
								if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
								{
									ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
								}
								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
									ulDataLen,
									net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
									);

								msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>%s status message</msg>", 
									(g_phelios->m_settings.m_pszName?g_phelios->m_settings.m_pszName:"Helios"));

								msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


								// now, send and log.
	////////////////////////////////////////////// code copied from above.  if modified, must be 2 places.  sorry...
									// send it
			_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_phelios)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending system message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_phelios->m_msgr.DM(MSG_ICONERROR, NULL, szHeliosSource, errorstring);  //(Dispatch message)
				g_phelios->SendMsg(CX_SENDMSG_ERROR, szHeliosSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
								
	//AfxMessageBox("oop");
									// log it

									//debug file write
									if((g_phelios)&&(g_phelios->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
									{
										strcpy(filename, "Logs");
										_mkdir(filename);  // if exists already np
										strcat(filename, "\\");
										sprintf(errorstring, "%d.%d.%d.%d", 
											pClient->m_si.sin_addr.s_net, 
											pClient->m_si.sin_addr.s_host, 
											pClient->m_si.sin_addr.s_lh, 
											pClient->m_si.sin_addr.s_impno);

										strcat(filename, errorstring);

										_mkdir(filename);  // if exists already np

		//								_ftime( &timestamp );  // use timestamp we just had..

										tm* theTime = localtime( &timestamp.time	);

										sprintf(errorstring, "\\%s_", 
											(g_phelios->m_settings.m_pszName?g_phelios->m_settings.m_pszName:"Helios"));

										strcat(filename, errorstring);

										char filenametemp[MAX_PATH];
										strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

										strcat(filename, filenametemp);

										int nOffset = strlen(filename);
										sprintf(filename+nOffset,"%03d",timestamp.millitm);

										if (strcmp(lasttxfilename, filename)==0)
										{
											nTxDupes++;
										}
										else
										{
											nTxDupes=0;
											strcpy(lasttxfilename, filename);
										}
										nOffset = strlen(filename);
										sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

										fp = fopen(filename, "wb");
										if(fp)
										{
											int b=CX_XML_BUFFER_CORTEXBEGIN;
											while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
											{
	//											fwrite(filename, 1, strlen(filename),fp);
												if(msg.m_pchResponse[b])
												{
													if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

													fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
													fflush(fp);
												}
												b++;

											}
											fclose(fp);
											fp = NULL;
										}
									}


									b=CX_XML_BUFFER_TX;
									while(b<CX_XML_BUFFER_DATAEND)
									{
										if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
										{
											try { free(msg.m_pchResponse[b]); } catch(...){}
											msg.m_pchResponse[b] = NULL;
										}
										b++;
									}
	/////////////////////////////////////////////
							}

						}

						//and check timeout
						if(nTimed>0)
						{
							_ftime( &timestamp );
							if((timeactive.time + nTimed) < timestamp.time)
							{
								//kill it

								bCloseCommand = true;
							}
							
						}


						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} 

cleanup:
//		AfxMessageBox("cleanup");
//AfxMessageBox("010");

//		pDoc->Release();  // destructor calls this so don't do it here. 
		if(pSafety) pSafety->Release();

//AfxMessageBox("011");

// cleanup:  //was here but want to uninit com etc
//AfxMessageBox("012");

		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d closed.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);

//AfxMessageBox("013");
		if(g_phelios)
		{
			g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, szHeliosSource, errorstring);  //(Dispatch message)
			g_phelios->SendMsg(CX_SENDMSG_INFO, szHeliosSource, errorstring);
		}

//AfxMessageBox("014");

		if(pch!=NULL) { try{free(pch);} catch(...) {} } // must free the data buffer 

//AfxMessageBox("015");

		shutdown(pClient->m_socket, SD_BOTH);
//AfxMessageBox("016");
		closesocket(pClient->m_socket);
//AfxMessageBox("017");
		(*(pClient->m_pulConnections))--;
//AfxMessageBox("018");

	}
//		AfxMessageBox("end cleanup");
//AfxMessageBox("019");

	try
	{  
		CoUninitialize(); //XML
	}
	catch(...)
	{
		if(g_phelios) g_phelios->m_msgr.DM(MSG_ICONERROR, NULL, szHeliosSource, "Exception in CoUninitialize");  //(Dispatch message)
	}

//AfxMessageBox("020");

	try
	{
		delete pClient; // was created with new in the thread that spawned this one.
	}	catch(...) {}

//AfxMessageBox("021");
	if (bSetGlobalKill)	//g_bKillThread = true;
		((CHeliosHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();


//		AfxMessageBox("end thread");
}



int CHeliosMain::SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...)
{
 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszAsRun)&&(strlen(m_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		if((pszEventID)&&(strlen(pszEventID)>SENDMSG_EVENT_MAXLEN)) pszEventID[SENDMSG_EVENT_MAXLEN-1]=0;
		
		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);
		char* pchEvent = m_data.m_pdb->EncodeQuotes(pszEventID);
		if(m_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, channelid, eventid) \
VALUES ('%s', '%s', %d, %d.%03d, %d, '%s')", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm,
				nChannelID,
				pchEvent?pchEvent:""
				);
		}
		else
		{

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				(m_settings.m_pszAsRun?m_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				(m_settings.m_pszAsRun?m_settings.m_pszAsRun:"AsRun_Log")
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
		if(pchEvent) free(pchEvent);

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_data.m_critSQL);
		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_data.m_critSQL);
			m_data.IncrementDatabaseMods(m_settings.m_pszAsRun, dberrorstring);
			return HELIOS_SUCCESS;
		}
		LeaveCriticalSection(&m_data.m_critSQL);

	}
	return HELIOS_ERROR;
}








int CHeliosMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)&&(strlen(m_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

		if(m_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime) \
VALUES ('%s', '%s', %d, %d.%03d)", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				m_settings.m_pszMessages
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		EnterCriticalSection(&g_phelios->m_data.m_critSQL);
		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&g_phelios->m_data.m_critSQL);
			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return HELIOS_SUCCESS;
		}
		LeaveCriticalSection(&g_phelios->m_data.m_critSQL);
	}
	return HELIOS_ERROR;
}

/*
void HeliosConnectionThread(void* pvArgs)
{
//			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "HeliosConnectionThread");  Sleep(250); //(Dispatch message)
	CHeliosConnectionObject* pConn = (CHeliosConnectionObject*) pvArgs;
	if(pConn==NULL) 
	{
		//**MSG
		return;
	}
	if(pConn->m_bKillConnThread) return;
	pConn->m_bConnThreadStarted = true;

	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:connection", "Server management thread for %s (%s) has begun.",
											pConn->m_pszServerName, //server_name
											pConn->m_pszDesc
		);  Sleep(100); //(Dispatch message)

	unsigned long ulTick = GetTickCount();
	unsigned long ulElapsedTick = GetTickCount();


//			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "HeliosConnectionThread2");  Sleep(250); //(Dispatch message)
	while(!pConn->m_bKillConnThread)
	{
		if(	pConn->m_pAPIConn )
		{
//			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "getting status");  Sleep(250); //(Dispatch message)
			// we can monitor the connection and get status
//			EnterCriticalSection(&g_omni.m_crit);
			g_adc.GetStatusData(pConn->m_pAPIConn);
//			LeaveCriticalSection(&g_adc.m_crit);
			ulTick = GetTickCount();

			if(pConn->m_pAPIConn->m_Status > 0)  // connected
			{
				// here we need to monitor list states and threads
				int nChannels = 0;

				while((nChannels<g_phelios->m_data.m_nNumChannelObjects)&&(!pConn->m_bKillConnThread))
				{
					if((g_phelios->m_data.m_ppChannelObj)&&(g_phelios->m_data.m_ppChannelObj[nChannels]))
					{
						if(strcmp(g_phelios->m_data.m_ppChannelObj[nChannels]->m_pszServerName, pConn->m_pszServerName)==0) // we are on the right adc conn.
						{
							//if the list is active, check to see if there is a thread going.
							if(g_phelios->m_data.m_ppChannelObj[nChannels]->m_ulFlags&HELIOS_FLAG_ENABLED)
							{
								if(
									  (g_phelios->m_data.m_ppChannelObj[nChannels]->m_nHarrisListID<0)
									||(g_phelios->m_data.m_ppChannelObj[nChannels]->m_nHarrisListID>=MAXSYSLISTS)
									)
								{
									// error!
									g_phelios->m_data.m_ppChannelObj[nChannels]->m_ulFlags &= ~HELIOS_FLAG_ENABLED;  //disable

								}
								else
								if(( !g_phelios->m_data.m_ppChannelObj[nChannels]->m_bChannelThreadStarted)&&(!pConn->m_bKillConnThread))
								{ // start the thread!
									g_phelios->m_data.m_ppChannelObj[nChannels]->m_bKillChannelThread=false;
									g_phelios->m_data.m_ppChannelObj[nChannels]->m_pAPIConn = pConn->m_pAPIConn;
//									g_phelios->m_data.m_ppChannelObj[nChannels]->m_pbKillConnThread=&(pConn->m_bKillConnThread);

//			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "beginning list thread");  Sleep(250); //(Dispatch message)
									if(_beginthread(HeliosListThread, 0, (void*)g_phelios->m_data.m_ppChannelObj[nChannels])==-1)
									{
										//error.
							
//			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "problem beginning list thread");  Sleep(250); //(Dispatch message)
										//**MSG
									}
									else
									{
										Sleep(200);
										// while(!g_phelios->m_data.m_ppChannelObj[nChannels]->m_bChannelThreadStarted) Sleep(1);
									}
								}
							}
							else  // disabled
							{
								if( g_phelios->m_data.m_ppChannelObj[nChannels]->m_bChannelThreadStarted)
								{
									// end the thread.
//			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "ending list thread");  Sleep(250); //(Dispatch message)
									g_phelios->m_data.m_ppChannelObj[nChannels]->m_bKillChannelThread=true;
									Sleep(200); // give it a  rest in between
								}
							}
						}
					}
					nChannels++;
				}
			}
			else
			{
	EnterCriticalSection(&g_adc.m_crit);
				g_adc.DisconnectServer(pConn->m_pAPIConn);  // just removes from adc list.
	LeaveCriticalSection(&g_adc.m_crit);
				pConn->m_ulStatus = HELIOS_STATUS_ERROR;  // due to disconnection.  resets on connect
				pConn->m_pAPIConn = NULL; // break out.
			}

			if((pConn->m_pAPIConn)&&(!pConn->m_bKillConnThread))
			{
				ulElapsedTick = GetTickCount();
				if(ulElapsedTick<=ulTick) //wrapped or instantaneous
				{
					Sleep(((pConn->m_pAPIConn->m_SysData.systemfrx) == 0x24)?40:33);
				}
				else
				{
					ulElapsedTick -= ulTick;
					if(ulElapsedTick<(unsigned long)(((pConn->m_pAPIConn->m_SysData.systemfrx) == 0x24)?40:33))
					{
			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "sleeping on status %d ms", (((pConn->m_pAPIConn->m_SysData.systemfrx) == 0x24)?40:33) - ulElapsedTick);  Sleep(250); //(Dispatch message)
						Sleep((((pConn->m_pAPIConn->m_SysData.systemfrx) == 0x24)?40:33) - ulElapsedTick);
					}  // else we already waited long.
				}
			}
			//else break out and try to reconnect.
		}
		else
		{
			if(pConn->m_ulFlags&HELIOS_FLAG_ENABLED) // active.
			{
				if((pConn->m_pszClientName)&&(strlen(pConn->m_pszClientName)<=0)) // zero length client name
				{
					free(pConn->m_pszClientName);
					pConn->m_pszClientName = NULL;
				}
				if(!pConn->m_pszClientName)
				{
					pConn->m_pszClientName = (char*)malloc(strlen("Helios")+1);
					if(pConn->m_pszClientName) strcpy(pConn->m_pszClientName, "Helios");
				}
					
				char server[32];
				_snprintf(server, 32, "%s", pConn->m_pszServerName);
				char client[32];
				_snprintf(client, 32, "%s", pConn->m_pszClientName);
//			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "connecting %s with %s ", server, client);  Sleep(250); //(Dispatch message)
				EnterCriticalSection(&g_adc.m_crit);
				CAConnection* pAPIConn = g_adc.ConnectServer(server, client);
				LeaveCriticalSection(&g_adc.m_crit);
				if(pAPIConn!=NULL)
				{
					pConn->m_pAPIConn = pAPIConn; // connected.
					pConn->m_ulStatus &= ~HELIOS_ICON_MASK;
					pConn->m_ulStatus |= HELIOS_STATUS_CONN;
//			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "connected %s with %s ", pConn->m_pszServerName, pConn->m_pszClientName);  Sleep(250); //(Dispatch message)
				}
				else
				{
					//**MSG
					pConn->m_pAPIConn = NULL;
					pConn->m_ulStatus &= ~HELIOS_ICON_MASK;
					pConn->m_ulStatus |= HELIOS_STATUS_ERROR;
//					g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "NULL connecting %s with %s ", pConn->m_pszServerName, pConn->m_pszClientName);  Sleep(250); //(Dispatch message)
				}
			} // else don't connect, just 
			else
			{
				pConn->m_pAPIConn = NULL;
				pConn->m_ulStatus &= ~HELIOS_ICON_MASK;
				pConn->m_ulStatus |= HELIOS_STATUS_NOTCON;
			}

			Sleep(2000);// wait two seconds after a connection attempt;
		}
			
	}


	if((g_phelios->m_data.m_ppChannelObj)&&(g_phelios->m_data.m_nNumChannelObjects))
	{
		int nThreads=0;
		while(nThreads < g_phelios->m_data.m_nNumChannelObjects)
		{
			if(
					(g_phelios->m_data.m_ppChannelObj[nThreads])
				&&(g_phelios->m_data.m_ppChannelObj[nThreads]->m_pszServerName)
				&&(pConn->m_pszServerName)
				&&(strcmp(g_phelios->m_data.m_ppChannelObj[nThreads]->m_pszServerName, pConn->m_pszServerName)==0)
				)
			{


/*	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:connection", "Shutting down list management thread for %s:%d (%s).",
											g_phelios->m_data.m_ppChannelObj[nThreads]->m_pszServerName, //server_name
											g_phelios->m_data.m_ppChannelObj[nThreads]->m_nHarrisListID,
											g_phelios->m_data.m_ppChannelObj[nThreads]->m_pszDesc
		);  Sleep(20); //(Dispatch message)
* /
				g_phelios->m_data.m_ppChannelObj[nThreads]->m_bKillChannelThread = true;			
				if(g_phelios->m_data.m_ppChannelObj[nThreads]->m_bChannelThreadStarted)
				{
/////////////
					
/*
					g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:connection", "Shutting down list management thread for %s:%d (%s).",
											g_phelios->m_data.m_ppChannelObj[nThreads]->m_pszServerName, //server_name
											g_phelios->m_data.m_ppChannelObj[nThreads]->m_nHarrisListID,
											g_phelios->m_data.m_ppChannelObj[nThreads]->m_pszDesc
		);  //Sleep(50); //(Dispatch message)
//////////////
	g_phelios->m_msgr.DM(MSG_ICONERROR, NULL, "Helios:connection", "2 - Shutting down list management thread for %s:%d (%s).   %d\n\n%d %d %d",
											g_phelios->m_data.m_ppChannelObj[nThreads]->m_pszServerName, //server_name
											g_phelios->m_data.m_ppChannelObj[nThreads]->m_nHarrisListID,
											g_phelios->m_data.m_ppChannelObj[nThreads]->m_pszDesc, clock(),
											(g_phelios->m_data.m_ppChannelObj),(g_phelios->m_data.m_ppChannelObj[nThreads]),(g_phelios->m_data.m_ppChannelObj[nThreads]->m_bChannelThreadStarted)
		); // Sleep(50); //(Dispatch message)
* /
//	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:connection", "entering while");
					while((g_phelios->m_data.m_ppChannelObj)&&(g_phelios->m_data.m_ppChannelObj[nThreads])&&(g_phelios->m_data.m_ppChannelObj[nThreads]->m_bChannelThreadStarted))
					{
//						g_phelios->m_data.m_ppChannelObj[nThreads]->m_bKillChannelThread = true;			
/*	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:connection", "Shutting down list management thread for %s:%d (%s).   %d\n\n%d %d %d",
											g_phelios->m_data.m_ppChannelObj[nThreads]->m_pszServerName, //server_name
											g_phelios->m_data.m_ppChannelObj[nThreads]->m_nHarrisListID,
											g_phelios->m_data.m_ppChannelObj[nThreads]->m_pszDesc, clock(),
											(g_phelios->m_data.m_ppChannelObj),(g_phelios->m_data.m_ppChannelObj[nThreads]),(g_phelios->m_data.m_ppChannelObj[nThreads]->m_bChannelThreadStarted)
		); // Sleep(50); //(Dispatch message)
	* /
						Sleep(10);
					}
//	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:connection", "leaving while");
				}
			}
			nThreads++;
		}
	}
				


	EnterCriticalSection(&g_adc.m_crit);
	g_adc.DisconnectServer(pConn->m_pszServerName);
	LeaveCriticalSection(&g_adc.m_crit);
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:connection", "Server management thread for %s (%s) is ending.",
											pConn->m_pszServerName, //server_name
											pConn->m_pszDesc
		);  Sleep(100); //(Dispatch message)

	pConn->m_pAPIConn = NULL;
	pConn->m_bConnThreadStarted = false;
	_endthread();
}

void HeliosListThread(void* pvArgs)
{
//	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "HeliosChannelThread");  Sleep(250); //(Dispatch message)
	CHeliosChannelObject* pChannel = (CHeliosChannelObject*) pvArgs;
	if(pChannel==NULL)
	{
//	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "pChannel NULL, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}
	if(pChannel->m_bKillChannelThread) return;
/*
	bool* pbKillConnThread = &(pChannel->m_bKillChannelThread);//pChannel->m_pbKillConnThread;
	if(pbKillConnThread==NULL)  // the address, not what it points to
	{
//	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "g_phelios NULL, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}
* /
	if(	pChannel->m_pAPIConn == NULL) // need this to do anything.  // could get it by looping through g_adc.... later.
	{
//	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "m_pAPIConn NULL, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}

	// lets set up some convenience vars;
	int nZeroList = pChannel->m_nHarrisListID-1;


	if((nZeroList<0)||(nZeroList>=MAXSYSLISTS)) // error on list number.
	{
//	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "nZeroList out of range, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}
	pChannel->m_bChannelThreadStarted = true;
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:list", "List management thread for %s:%d (%s) has begun.",
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
											pChannel->m_pszDesc
		);  Sleep(100); //(Dispatch message)



	tlistdata* plistdata = &(pChannel->m_pAPIConn->m_ListData[nZeroList]);
//	TConnectionStatus* pstatus =  &(pChannel->m_pAPIConn->m_Status);
	CAList** ppList = &(pChannel->m_pAPIConn->m_pList[nZeroList]);
	CAList*  pList = pChannel->m_pAPIConn->m_pList[nZeroList];  // volatile, always reassign
	CDBUtil* pdb = g_phelios->m_data.m_pdb;
	CDBconn* pdbConn = g_phelios->m_data.m_pdbConn;

	pChannel->m_pAPIConn->m_bListActive[nZeroList] = true;
//	unsigned long ulTick = GetTickCount();
//	unsigned long ulElapsedTick = GetTickCount();
//			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "HeliosChannelThread2");  Sleep(250); //(Dispatch message)

	tlistdata listdata;
	tlistdata lastlistdata;  // for comparison

  lastlistdata.listchanged=0;				// incremented whenever list size changes
  lastlistdata.listdisplay=0;				// incremented whenever an event changes
  lastlistdata.listsyschange=0;			// incremented when list related values are changed. 
  lastlistdata.listcount=0;					// number of events in list 
  lastlistdata.liststate=0;
	char dberrorstring[DB_ERRORSTRING_LEN];
	char szSQL[DB_SQLSTRING_MAXLEN];
			
	while(!pChannel->m_bKillChannelThread)//&&((*pbKillConnThread)!=true))
	{
//		EnterCriticalSection(&g_adc.m_crit);
//		g_adc.GetStatusData(pConn->m_pAPIConn);
//		LeaveCriticalSection(&g_adc.m_crit);
		if((pChannel->m_pAPIConn)&&(pChannel->m_pAPIConn->m_Status > 0))  // have to be connected.
		{
			// snapshot the live list data
			if((!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true))
			{
				EnterCriticalSection(&g_adc.m_crit);
				memcpy(&listdata, plistdata, sizeof(tlistdata));
				LeaveCriticalSection(&g_adc.m_crit);
			}

//	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "%s HeliosChannelThread checking list change", pChannel->m_pszDesc);  Sleep(50); //(Dispatch message)
			if(((listdata.listchanged!=lastlistdata.listchanged)||(listdata.listdisplay!=lastlistdata.listdisplay))&&(pChannel->m_pAPIConn)&&(!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true)) // just do the event chage one as well for now.
			{
//	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "%s HeliosChannelThread list change, getting events", pChannel->m_pszDesc);  Sleep(50); //(Dispatch message)
				// the list has changed, need to get all the events again  (will take care of any event changes as well)
				EnterCriticalSection(&g_adc.m_crit);
				int nNumEvents = g_adc.GetEvents(pChannel->m_pAPIConn,
					pChannel->m_nHarrisListID, 
					0, //start at the top.
					g_phelios->m_settings.m_bUseListCount?listdata.listcount:listdata.lookahead,
					g_phelios->m_settings.m_bUseListCount?(ADC_GETEVENTS_LIST|ADC_GETEVENTS_USEOFFSET|ADC_GETEVENTS_FULLLIST):(ADC_GETEVENTS_LIST|ADC_GETEVENTS_USEOFFSET)
					);
				LeaveCriticalSection(&g_adc.m_crit);

				if((!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn)) pList = pChannel->m_pAPIConn->m_pList[nZeroList];  // volatile, always reassign
				else pList=NULL;
				if((pList)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))//&&((*pbKillConnThread)!=true))
				{
//	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "%s HeliosChannelThread list change, got %d events, entering critical", pChannel->m_pszDesc, nNumEvents);  Sleep(50); //(Dispatch message)
					EnterCriticalSection(&(pList->m_crit));  // lock the list out
//	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "%s HeliosChannelThread list change, in critical", pChannel->m_pszDesc);  Sleep(50); //(Dispatch message)
					if((nNumEvents>=ADC_SUCCESS)&&(pList)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))//&&((*pbKillConnThread)!=true))
					{
						// deal with database.
						if((pdb!=NULL)&&(pdbConn!=NULL)&&(g_phelios->m_settings.m_pszLiveEvents)&&(strlen(g_phelios->m_settings.m_pszLiveEvents)>0)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))
						{
//		g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "%s HeliosChannelThread dealing with db", pChannel->m_pszDesc);  Sleep(50); //(Dispatch message)

							double dblLastPrimaryEventTime = -1.0;
							unsigned short usLastPrimaryOnAirJulianDate = 0xffff;
							unsigned long ulLastPrimaryOnAirTimeMS = 0xffffffff;
							double dblServerTime = ((double)pChannel->m_pAPIConn->m_usRefJulianDate - 25567.0)*86400.0   // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
											+ (double)pChannel->m_pAPIConn->m_ulRefTimeMS/1000.0;

							int n=0;
							while((!pChannel->m_bKillChannelThread)&&(n<nNumEvents)&&(pChannel->m_pAPIConn))//&&((*pbKillConnThread)!=true))
							{

//		g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "%s HeliosChannelThread event: %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
								if((pList->m_ppEvents)&&(pList->m_ppEvents[n])&&(pList->m_ppEvents[n]->m_pszID)&&(strlen(pList->m_ppEvents[n]->m_pszID)))
								{

									double dblEventTime = 0.0; 
									unsigned short usOnAirJulianDate = pList->m_ppEvents[n]->m_usOnAirJulianDate;

							//		if(usOnAirJulianDate == 0xffff)  // can't trust event based julian date, have to use server date
//									{
//										_timeb timestamp;
//										_ftime(&timestamp);

//										usOnAirJulianDate = pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.

// move time calc to below julian recalc
//										dblEventTime = ((double)usOnAirJulianDate - 25567.0)*86400  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
//											+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
//									}
/*
									else
									{
										dblEventTime = ((double)pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567.0)*86400    // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
											+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
									}
* /											
//		g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "%s HeliosChannelThread event: %d [%s] again", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)



									if(usLastPrimaryOnAirJulianDate != 0xffff)
									{
//		g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "%s HeliosChannelThread event: %d [%s] again1a", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
										usOnAirJulianDate = usLastPrimaryOnAirJulianDate;
									}
									else
									{
//		g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "%s HeliosChannelThread event: %d [%s] again1b", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
										usOnAirJulianDate = pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.
									}

//		g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "%s HeliosChannelThread event: %d [%s] again2", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)

									unsigned short usType = pList->m_ppEvents[n]->m_usType;

									if(usType&SECONDARYEVENT)
									{
										if(ulLastPrimaryOnAirTimeMS!=0xffffffff)
										{
											dblEventTime = ((dblLastPrimaryEventTime == -1.0)?0:dblLastPrimaryEventTime)
												+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;

											usOnAirJulianDate = (usLastPrimaryOnAirJulianDate!=0xffff)?usLastPrimaryOnAirJulianDate:pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.

											if(pList->m_ppEvents[n]->m_ulOnAirTimeMS + ulLastPrimaryOnAirTimeMS >= 86400000 )
											{
												//wrapped days.
												usOnAirJulianDate++;
											}
										}
										else
										{
											// no last primary
											dblEventTime = (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
										}
									}
									else
									{
										if(ulLastPrimaryOnAirTimeMS!=0xffffffff)
										{
											if(ulLastPrimaryOnAirTimeMS > pList->m_ppEvents[n]->m_ulOnAirTimeMS)
											{
												// we wrapped days.
												usOnAirJulianDate++;
											}
										}

										dblEventTime = ((double)usOnAirJulianDate - 25567.0)*86400.0  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
											+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;

										ulLastPrimaryOnAirTimeMS = pList->m_ppEvents[n]->m_ulOnAirTimeMS;
										dblLastPrimaryEventTime = dblEventTime;
										usLastPrimaryOnAirJulianDate = usOnAirJulianDate;
									}
//		g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "%s HeliosChannelThread event: %d [%s] again3", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)

									char* pchID = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszID);
									char* pchTitle = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszTitle);
									char* pchRecKey = NULL;
									char* pchData = NULL;
									
									if(pList->m_ppEvents[n]->m_pszData) 
										pchData = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszData);
									if(pList->m_ppEvents[n]->m_pszReconcileKey) 
										pchRecKey = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszReconcileKey);


	//						select event with server, list, start time +/- tolerance, and clip ID
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE \
server_name = '%s' AND \
server_list = %d AND \
event_clip = '%s' AND \
event_title = '%s' AND \
event_start >= %f AND \
event_start <= %f",
											g_phelios->m_settings.m_pszLiveEvents,
											pChannel->m_pszServerName,
											pChannel->m_nHarrisListID,
											pchID?pchID:pList->m_ppEvents[n]->m_pszID,
											pchTitle?pchTitle:pList->m_ppEvents[n]->m_pszTitle,
											dblEventTime-0.3, dblEventTime+0.3
										);

//		g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "HeliosChannelThread event: %s", szSQL);  Sleep(50); //(Dispatch message)
									// get
									CRecordset* prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
									int nNumFound = 0;
									int nID = -1;
									if(prs != NULL) 
									{
										if(!prs->IsEOF())
										{
											CString szValue;
											try
											{
												// get id!
												prs->GetFieldValue("itemid", szValue);
												nID = atoi(szValue);
											}
											catch( ... )
											{
											}

											nNumFound++;
											// prs->MoveNext();
										}
										prs->Close();
										delete prs;
									}

									if((!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true))
									{
									if((nNumFound>0)&&(nID>0))
									{
										// update it.
										// if found update it with new values, and a found flag (app_data_aux int will be the found flag 1=found)
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
server_name = '%s', \
server_list = %d, \
list_id = %d, \
event_id = '%s', \
event_clip = '%s', \
event_title = '%s', \
%s%s%s\
event_type = %d, \
event_status = %d, \
event_time_mode = %d, \
event_start = %f, \
event_duration = %d, \
event_last_update = %f, \
app_data_aux = 1 WHERE itemid = %d;",  // 1 is the found flag.
											g_phelios->m_settings.m_pszLiveEvents,  // table name
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
											pChannel->m_nChannelID, // list_id - internal lookup channel number
											pList->m_ppEvents[n]->m_pszReconcileKey?pList->m_ppEvents[n]->m_pszReconcileKey:"", //event_id
											pchID?pchID:pList->m_ppEvents[n]->m_pszID, //event_clip  (must not be null)
											pchTitle?pchTitle:"", //event_title
											pchData?"event_data = '":"", //event_data (exists)
											pchData?pchData:"", //event_data  
											pchData?"', ":"", //event_data (exists)
											pList->m_ppEvents[n]->m_usType, //event_type
											pList->m_ppEvents[n]->m_usStatus, //event_status
											pList->m_ppEvents[n]->m_usControl, //event_time_mode
											dblEventTime, //event_start
											pList->m_ppEvents[n]->m_ulDurationMS, //event_duration
											dblServerTime, //event_last_update
											nID // the unique ID of the thing
										);
	// app_data = , // no app data for the above, let it be NULL or whatever it is.




									}
									else
									{
										// add it.
										// if not found, insert one with a found flag
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
server_name, \
server_list, \
list_id, \
event_id, \
event_clip, \
event_title, \
%s\
event_type, \
event_status, \
event_time_mode, \
event_start, \
event_duration, \
event_last_update, \
app_data_aux) VALUES ('%s', %d, %d, '%s', '%s', '%s', %s%s%s%d, %d, %d, %f, %d, %f, 1)",  // 1 is the found flag.
											g_phelios->m_settings.m_pszLiveEvents,  // table name
											pList->m_ppEvents[n]->m_pszData?"event_data, ":"", //event_data (exists)
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
											pChannel->m_nChannelID, // list_id - internal lookup channel number
											pList->m_ppEvents[n]->m_pszReconcileKey?pList->m_ppEvents[n]->m_pszReconcileKey:"", //event_id
											pchID?pchID:pList->m_ppEvents[n]->m_pszID, //event_clip  (must not be null)
											pchTitle?pchTitle:"", //event_title
											pchData?"event_data = '":"", //event_data (exists)
											pchData?pchData:"", //event_data  
											pchData?"', ":"", //event_data (exists)
											pList->m_ppEvents[n]->m_usType, //event_type
											pList->m_ppEvents[n]->m_usStatus, //event_status
											pList->m_ppEvents[n]->m_usControl, //event_time_mode
											dblEventTime, //event_start
											pList->m_ppEvents[n]->m_ulDurationMS, //event_duration
											dblServerTime //event_last_update
											);

									}
									}
									

									if(pchID) free(pchID);
									if(pchTitle) free(pchTitle);
									if(pchRecKey) free(pchRecKey);
									if(pchData) free(pchData);


//		g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "HeliosChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

									if((!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true))
									{
									if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
									{
										//**MSG
									}
									}
								}
								n++;
							}

							///////////////////////
							// remove found flags.

	//					remove all with no found flag.

							if((!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true))
							{

							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d AND \
app_data_aux < 1",
											g_phelios->m_settings.m_pszLiveEvents,  // table name
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
										);
//		g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "HeliosChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

							if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
							{
								//**MSG
							}
	//					remove all found flags for next go-around.
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
app_data_aux = 0 WHERE \
server_name = '%s' AND \
server_list = %d",
											g_phelios->m_settings.m_pszLiveEvents,  // table name
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
										);
//		g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "HeliosChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

							if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
							{
								//**MSG
							}

						
							// increment exchange mod counter. for events table.
							EnterCriticalSection(&g_phelios->m_data.m_critIncrement);
							if (g_phelios->m_data.IncrementDatabaseMods(g_phelios->m_settings.m_pszLiveEvents, dberrorstring)<HELIOS_SUCCESS)
							{
								//**MSG
							}
							LeaveCriticalSection(&g_phelios->m_data.m_critIncrement);
							}
						}
						// record last values if get was successful.
						memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
					}
					else //if((pList)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))  // plist is null if no events in list.
					if(nNumEvents == 0)  // but this is correct
					{
						// record last values if get was successful.
						memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
					}
//		g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "HeliosChannelThread leaving critical");  Sleep(20); //(Dispatch message)
					LeaveCriticalSection(&(pList->m_crit));
//		g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "%s HeliosChannelThread left critical", pChannel->m_pszDesc);  Sleep(20); //(Dispatch message)
				}//if((pList)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))
			} //			if(((listdata.listchanged!=lastlistdata.listchanged)||(listdata.listdisplay!=lastlistdata.listdisplay))&&(pChannel->m_pAPIConn)&&(!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true)) // just do the event chage one as well for now.

/*			else
			if(listdata.listdisplay!=lastlistdata.listdisplay)
			{
				// an event has changed, need to find it and just deal with that one.
				// record last values
				memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
			}
* /
		}

	}

	//		remove all, since not active, want to purge list.
	if(
			(pdb!=NULL)&&(pdbConn!=NULL)
			&&(g_phelios->m_settings.m_pszLiveEvents)
			&&(strlen(g_phelios->m_settings.m_pszLiveEvents)>0)
			&&(pChannel)
			&&(pChannel->m_pszServerName)
			&&(strlen(pChannel->m_pszServerName)>0)
		)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d",
											g_phelios->m_settings.m_pszLiveEvents,  // table name
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
										);
//g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "HeliosChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

		if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
		{
			//**MSG
		}
		// increment exchange mod counter. for events table.
		EnterCriticalSection(&g_phelios->m_data.m_critIncrement);
		if (g_phelios->m_data.IncrementDatabaseMods(g_phelios->m_settings.m_pszLiveEvents, dberrorstring)<HELIOS_SUCCESS)
		{
//g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "Increment Error: %s", dberrorstring);  Sleep(50); //(Dispatch message)
			//**MSG
		}
		LeaveCriticalSection(&g_phelios->m_data.m_critIncrement);

	}
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:list", "List management thread for %s:%d (%s) is ending.",
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
											pChannel->m_pszDesc
		);  Sleep(100); //(Dispatch message)

	if(pChannel->m_pAPIConn)	pChannel->m_pAPIConn->m_bListActive[nZeroList] = false;
	pChannel->m_bChannelThreadStarted = false;
	_endthread();
}
*/



void HeliosControlModuleServiceThread(void* pvArgs)
{
	EnterCriticalSection(&g_phelios->m_data.m_critControlModuleService);
	g_phelios->m_data.m_bControlModuleServiceThreadStarted=true;
	LeaveCriticalSection(&g_phelios->m_data.m_critControlModuleService);
	int nLastMod=-27;
	_timeb timestamp;

	g_phelios->m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:debug", "Control module service thread started."); //(Dispatch message)


	CBufferUtil bu;
	CCortexMessage msg;
	char errorstring[DB_ERRORSTRING_LEN];
	int nCode=0;

	// initialize a nice big array
	EnterCriticalSection(&g_phelios->m_data.m_critControlModuleService);

	g_phelios->m_data.m_nNumChanges=0;
	g_phelios->m_data.m_nNumChangesArray=0;
	g_phelios->m_data.m_nChangesArrayIndex=0;
	g_phelios->m_data.m_ppChanges = new CSentinelChange*[HELIOS_CHANGELIST_ARRAYSIZE_INC];

	if(g_phelios->m_data.m_ppChanges)
	{
		g_phelios->m_data.m_nNumChangesArray = HELIOS_CHANGELIST_ARRAYSIZE_INC;
	}

	
	LeaveCriticalSection(&g_phelios->m_data.m_critControlModuleService);

	while((!g_bKillThread)&&(!g_phelios->m_data.m_bKillControlModuleServiceThread))
	{
		_ftime(&timestamp);

		if(g_phelios->m_data.m_nLastControllersMod!=nLastMod)
		{
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONTROL)	
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "checking control module mods diff %d != %d, array %d (num %d)", g_phelios->m_data.m_nLastControllersMod, nLastMod, g_phelios->m_data.m_ppControlObj, g_phelios->m_data.m_nNumControlObjects); //  Sleep(250);//(Dispatch message)
			// check for new modules to initialize.
//g_phelios->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "D entering extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
			EnterCriticalSection(&g_phelios->m_data.m_critExternalModules);
//g_phelios->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "D entered extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)

			if(g_phelios->m_data.m_ppControlObj)
			{
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONTROL)	
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "checking control module init"); //  Sleep(250);//(Dispatch message)
				int i=0;
				while(i<g_phelios->m_data.m_nNumControlObjects)
				{
if((g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONTROL)&&(g_phelios->m_data.m_ppControlObj[i]))	
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "checking control module %d: %d %d %d 0x%08x", 
	i, 
	(g_phelios->m_data.m_ppControlObj[i]),
	g_phelios->m_data.m_ppControlObj[i]->m_hinstDLL,
	g_phelios->m_data.m_ppControlObj[i]->m_lpfnDllCtrl,
	g_phelios->m_data.m_ppControlObj[i]->m_ulFlags); //  Sleep(250);//(Dispatch message)
					if(
						  (g_phelios->m_data.m_ppControlObj[i])
						&&(g_phelios->m_data.m_ppControlObj[i]->m_hinstDLL)
						&&(g_phelios->m_data.m_ppControlObj[i]->m_lpfnDllCtrl)
						&&(g_phelios->m_data.m_ppControlObj[i]->m_ulFlags&HELIOS_FLAG_INIT)
						&&(g_phelios->m_data.m_ppControlObj[i]->m_ulFlags&HELIOS_FLAG_ENABLED)  // only enabled ones
						)
					{
						//initialize channels and events
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONTROL)	
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "initializing control module"); //  Sleep(250);//(Dispatch message)
						int ch = 0;
						EnterCriticalSection(&g_phelios->m_data.m_critChannels);
						while(ch<g_phelios->m_data.m_nNumChannelObjects)
						{
						if(
									(g_phelios->m_data.m_ppChannelObj[ch])
								&&(g_phelios->m_data.m_ppControlObj[i]->IsChannelAssociated(g_phelios->m_data.m_ppChannelObj[ch]->m_nChannelID)==CX_SUCCESS)
								)
							{
								// have an associated channel
								// first send connection info
								//initialize connections
								int c=0;
								EnterCriticalSection(&g_phelios->m_data.m_critConns);
								while(c<g_phelios->m_data.m_nNumConnectionObjects)
								{
									// need to find out if this thing is associated with a new channel
									if((g_phelios->m_data.m_ppConnObj[c])&&(g_phelios->m_data.m_ppConnObj[c]->m_pszServerName)&&g_phelios->m_data.m_ppChannelObj[ch]->m_pszServerName)
									{
										if(strcmp(g_phelios->m_data.m_ppConnObj[c]->m_pszServerName, g_phelios->m_data.m_ppChannelObj[ch]->m_pszServerName)==0)
										{
											//great!  this is the connection

											CSentinelConnectionObject* pObj = g_phelios->m_data.m_ppConnObj[c];

											char tag[128];
											systemstatus				sys; 
											TConnectionStatus   status;

											sys.systemlistcount=-1;
											sys.syschanged=-1;
											sys.systemfrx=0x29;
											sys.systemdf = 1;
											
											status = netNoConnection;

											EnterCriticalSection(&pObj->m_critAPI);

											if(pObj->m_pAPIConn)
											{
												memcpy(&sys, &pObj->m_pAPIConn->m_SysData, sizeof(systemstatus));
												memcpy(&status, &pObj->m_pAPIConn->m_Status, sizeof(TConnectionStatus));
												LeaveCriticalSection(&pObj->m_critAPI);
												if((sys.systemfrx==0x29)
													&&(sys.systemdf))
												{
													strcpy(tag, "29.97");
												}
												else
												{
													sprintf(tag, "%02x",sys.systemfrx);
												}
											}
											else
											{
												LeaveCriticalSection(&pObj->m_critAPI);
												sprintf(tag, "N/A");
											}
											

											msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																					"<aconn>\
<config active=\"%d\">\
<server>%s</server>\
<client>%s</client>\
<port>N/A</port>\
<basis>%s</basis>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<server_time>%.03f</server_time>\
<server_status>%d</server_status>\
<server_lists>%d</server_lists>\
<server_changed>%d</server_changed>\
<server_last_update>%.03f</server_last_update>\
<message>%s</message>\
</status>\
</aconn>",
																					((pObj->m_ulFlags&HELIOS_FLAG_ENABLED)?1:0),
																					pObj->m_pszServerName,
																					pObj->m_pszClientName,
																					tag,
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(pObj->m_dblLastServerTimeMS/1000.0),
																					(((pObj->m_ulStatus&HELIOS_ICON_MASK) != HELIOS_STATUS_CONN)? -1 : (status&0xff)),
																					(sys.systemlistcount==0xff)?-1:(sys.systemlistcount&0xff),
																					(sys.syschanged&0xff),
																					pObj->m_dblUpdateTime,
																					(((pObj->m_ulStatus&HELIOS_ICON_MASK) != HELIOS_STATUS_CONN)? (((pObj->m_ulStatus&HELIOS_ICON_MASK) == HELIOS_STATUS_ERROR)?"connection error":"not connected"):"")

																																										
																				);

											

								nCode = g_phelios->m_data.m_ppControlObj[i]->m_lpfnDllCtrl((void**)(&(msg.m_pchResponse[CX_XML_BUFFER_DATA])), DLLCMD_ACONN);

								if(nCode<HELIOS_SUCCESS)
								{
									/// do something, report?
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending connection info for %s to module %s: %s (%s)", nCode,
														pObj->m_pszServerName, g_phelios->m_data.m_ppControlObj[i]->m_pszModule, g_phelios->m_data.m_ppControlObj[i]->m_pszName,g_phelios->m_data.m_ppControlObj[i]->m_pszDesc );
									g_phelios->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:control_module_service", errorstring);    //(Dispatch message)
								}


											break; // just do it once, in case more channels on one connection.
										}
									}
									c++;
								}

								LeaveCriticalSection(&g_phelios->m_data.m_critConns);


								//then send channel info

								CSentinelChannelObject* pObj = g_phelios->m_data.m_ppChannelObj[ch];

								_ftime(&timestamp);

								tlistdata tlist;
								tlist.liststate = -1;
								tlist.listchanged = -1;
								tlist.listdisplay = -1;
								tlist.listsyschange = -1;
								tlist.listcount = -1;
								tlist.lookahead = -1;
								if(pObj->m_pcritAPI)  // otherwise, prob not conected
								{
									EnterCriticalSection(pObj->m_pcritAPI);

									if((pObj->m_ppAPIConn)&&(*(pObj->m_ppAPIConn)))
									{
										memcpy(&tlist, &((*(pObj->m_ppAPIConn))->m_ListData[pObj->m_nHarrisListID-1]), sizeof(tlistdata));
									}
									LeaveCriticalSection(pObj->m_pcritAPI);
								}

								msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
									"<alist>\
<config active=\"%d\" id=\"%d\">\
<server>%s</server>\
<list_number>%d</list_number>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<list_state>%d</list_state>\
<list_changed>%d</list_changed>\
<list_display>%d</list_display>\
<list_syschange>%d</list_syschange>\
<list_count>%d</list_count>\
<list_lookahead>%d</list_lookahead>\
<list_last_update>%.3f</list_last_update>\
</status>\
</alist>",
									((pObj->m_ulFlags&HELIOS_FLAG_ENABLED)?1:0),
									pObj->m_nChannelID,
									pObj->m_pszServerName,
									pObj->m_nHarrisListID,
									pObj->m_pszDesc,
									timestamp.time, timestamp.millitm,
									tlist.liststate,
									tlist.listchanged,
									tlist.listdisplay,
									tlist.listsyschange,
									tlist.listcount,
									tlist.lookahead,
									pObj->m_dblUpdateTime
																														
								);

								nCode = g_phelios->m_data.m_ppControlObj[i]->m_lpfnDllCtrl((void**)(&(msg.m_pchResponse[CX_XML_BUFFER_DATA])), DLLCMD_ALIST);
								if(nCode<HELIOS_SUCCESS)
								{
									/// do something, report?
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending channel info for %s:%d to module %s: %s (%s)", nCode,
														pObj->m_pszServerName, pObj->m_nHarrisListID, g_phelios->m_data.m_ppControlObj[i]->m_pszModule, g_phelios->m_data.m_ppControlObj[i]->m_pszName,g_phelios->m_data.m_ppControlObj[i]->m_pszDesc );
									g_phelios->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:control_module_service", errorstring);    //(Dispatch message)
								}



								// then send event info

								int e=0;
								while((pObj->m_ppevents)&&(e<pObj->m_nNumEvents))
								{
									if(pObj->m_ppevents[e])
									{
										CSentinelEventObject* pEObj = pObj->m_ppevents[e];

										char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
										char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
										char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
										char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
										char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
										
										msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH,  
												"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_ulControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CSentinelEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																									);
											

								nCode = g_phelios->m_data.m_ppControlObj[i]->m_lpfnDllCtrl((void**)(&(msg.m_pchResponse[CX_XML_BUFFER_DATA])), DLLCMD_AITEM);
								if(nCode<HELIOS_SUCCESS)
								{
									/// do something, report?
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending event info for %s to module %s: %s (%s)", nCode,
														(pchXMLclip?pchXMLclip:""), g_phelios->m_data.m_ppControlObj[i]->m_pszModule, g_phelios->m_data.m_ppControlObj[i]->m_pszName,g_phelios->m_data.m_ppControlObj[i]->m_pszDesc );
									g_phelios->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:control_module_service", errorstring);    //(Dispatch message)
								}

										if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
										if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
										if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
										if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
										if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

									}
									e++;
								}

								// send traffic info  - later
#pragma message(alert "add traffic info to this later")

							}
							ch++;
						}
						LeaveCriticalSection(&g_phelios->m_data.m_critChannels);


						g_phelios->m_data.m_ppControlObj[i]->m_ulFlags &= ~HELIOS_FLAG_INIT;
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONTROL)	
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "removing control module init 0x%08x", g_phelios->m_data.m_ppControlObj[i]->m_ulFlags); //  Sleep(250);//(Dispatch message)

					}
					i++;
				}
			}
//g_phelios->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "D leaving extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
			LeaveCriticalSection(&g_phelios->m_data.m_critExternalModules);
//g_phelios->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "D left extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)

			nLastMod = g_phelios->m_data.m_nLastControllersMod;
		}

		// do any requests.
		if(g_phelios->m_data.m_ppChanges)
		{
			EnterCriticalSection(&g_phelios->m_data.m_critControlModuleService);
			if(g_phelios->m_data.m_nNumChanges>0)
			{

				CSentinelChange* pChg = g_phelios->m_data.m_ppChanges[g_phelios->m_data.m_nChangesArrayIndex];
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONTROL)	
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "changes are now %d in the service loop %d %d %d", 
	g_phelios->m_data.m_nNumChanges, pChg, pChg?((int)(pChg->m_pObj)):-1, pChg?pChg->m_nChannelID:-1); //  Sleep(250);//(Dispatch message)
				int i=g_phelios->m_data.m_nChangesArrayIndex;
				if((pChg)&&(pChg->m_pObj)&&((pChg->m_nChannelID>0)||(pChg->m_nType==DLLCMD_ACONN)))  // DLLCMD_ACONN doesnt have a channel association.
				{
					// do the magic here.
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONTROL)	
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "change is valid"); //  Sleep(250);//(Dispatch message)


					bool bBufferMade = false;

//g_phelios->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "E entering extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
					EnterCriticalSection(&g_phelios->m_data.m_critExternalModules);
//g_phelios->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "E entered extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)

					if(g_phelios->m_data.m_ppControlObj)
					{
						int i=0;
						while(i<g_phelios->m_data.m_nNumControlObjects)
						{
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONTROL)	
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "checking obj %d of %d 0x%08x, %d=>[%s] ?%d",
	i+1, g_phelios->m_data.m_nNumControlObjects, g_phelios->m_data.m_ppControlObj[i]->m_ulFlags, 
	pChg->m_nChannelID, g_phelios->m_data.m_ppControlObj[i]->m_pszChannelIds, g_phelios->m_data.m_ppControlObj[i]->IsChannelAssociated(pChg->m_nChannelID)); //  Sleep(250);//(Dispatch message)

							if(
									(g_phelios->m_data.m_ppControlObj[i])
								&&(g_phelios->m_data.m_ppControlObj[i]->m_hinstDLL)
								&&(g_phelios->m_data.m_ppControlObj[i]->m_lpfnDllCtrl)
								&&(g_phelios->m_data.m_ppControlObj[i]->m_ulFlags&HELIOS_FLAG_ENABLED)
								&&(!(g_phelios->m_data.m_ppControlObj[i]->m_ulFlags&HELIOS_FLAG_INIT)) // must be initialized first.
								&&((g_phelios->m_data.m_ppControlObj[i]->IsChannelAssociated(pChg->m_nChannelID)==CX_SUCCESS)||(pChg->m_nType==DLLCMD_ACONN))// DLLCMD_ACONN doesnt have a channel association. have to search it here.
								)
							{

								switch(pChg->m_nType)
								{
									
								case DLLCMD_ACONN://	0x0100: //	Sent to the DLL when a connection to an automation system is established or disconnected. Disconnection is indicated by the aconn.status.server_status element.�
									{

										// have to check the channel association on connection.
										CSentinelConnectionObject* pObj = (CSentinelConnectionObject*)pChg->m_pObj;
										if(pObj)
										{

//if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONTROL)	
//	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "checking plugin on conn for DLLCMD_ACONN"); //  Sleep(250);//(Dispatch message)

											if(g_phelios->m_data.CheckPluginOnConn(g_phelios->m_data.m_ppControlObj[i]->m_pszChannelIds, pChg->m_pszServerName)==HELIOS_SUCCESS)
											{


//if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONTROL)	
//	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "sending DLLCMD_ACONN (%d)", bBufferMade); //  Sleep(250);//(Dispatch message)

										if(!bBufferMade)
										{

											char tag[128];

											if(pChg->m_sys.systemfrx==-1)
											{
												sprintf(tag, "N/A");
											}
											else
											{
												if((pChg->m_sys.systemfrx==0x29)
													&&(pChg->m_sys.systemdf))
												{
													strcpy(tag, "29.97");
												}
												else
												{
													sprintf(tag, "%02x",pChg->m_sys.systemfrx);
												}
											}
												

											msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																				"<aconn>\
<config active=\"%d\">\
<server>%s</server>\
<client>%s</client>\
<port>N/A</port>\
<basis>%s</basis>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<server_time>%.03f</server_time>\
<server_status>%d</server_status>\
<server_lists>%d</server_lists>\
<server_changed>%d</server_changed>\
<server_last_update>%.03f</server_last_update>\
<message>%s</message>\
</status>\
</aconn>",
																					((pObj->m_ulFlags&HELIOS_FLAG_ENABLED)?1:0),
																					pChg->m_pszServerName?pChg->m_pszServerName:"",
																					pChg->m_pszClientName?pChg->m_pszClientName:"",
																					tag,
																					pChg->m_pszDesc?pChg->m_pszDesc:"",
																					pChg->m_timestamp.time, pChg->m_timestamp.millitm,
																					(pObj->m_dblLastServerTimeMS/1000.0),
																					(((pObj->m_ulStatus&HELIOS_ICON_MASK) != HELIOS_STATUS_CONN)? -1 : (pChg->m_status&0xff)),
																					(pChg->m_sys.systemlistcount==0xff)?-1:(pChg->m_sys.systemlistcount&0xff),
																					(pChg->m_sys.syschanged&0xff),
																					pObj->m_dblUpdateTime,
																					(((pObj->m_ulStatus&HELIOS_ICON_MASK) != HELIOS_STATUS_CONN)? (((pObj->m_ulStatus&HELIOS_ICON_MASK) == HELIOS_STATUS_ERROR)?"connection error":"not connected"):"")

																																										
																				);
											bBufferMade = true;

										}

										nCode = g_phelios->m_data.m_ppControlObj[i]->m_lpfnDllCtrl((void**)(&(msg.m_pchResponse[CX_XML_BUFFER_DATA])), DLLCMD_ACONN);
										if(nCode<HELIOS_SUCCESS)
										{
											/// do something, report?
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending connection info for %s to module %s: %s (%s)", nCode,
																pChg->m_pszServerName, g_phelios->m_data.m_ppControlObj[i]->m_pszModule, g_phelios->m_data.m_ppControlObj[i]->m_pszName,g_phelios->m_data.m_ppControlObj[i]->m_pszDesc );
											g_phelios->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:control_module_service", errorstring);    //(Dispatch message)
										}
									



											}
										}


									} break;
									case DLLCMD_ALIST://	0x0101: //	Sent to the DLL when monitoring for a list on an active connection is begun or discontinued.  If the connection on which the list information is coming has terminated, then a separate DLLCMD_ALIST command is not sent, as the monitoring may still be active, and will resume when the connection is restored.  Discontinuation of list monitoring is indicated by the active attribute of the alist.config element.�
									{
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONTROL)	
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "sending DLLCMD_ALIST (%d)", bBufferMade); //  Sleep(250);//(Dispatch message)

										if(!bBufferMade)
										{
											CSentinelChannelObject* pObj = (CSentinelChannelObject*)pChg->m_pObj;

											msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
												"<alist>\
<config active=\"%d\" id=\"%d\">\
<server>%s</server>\
<list_number>%d</list_number>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<list_state>%d</list_state>\
<list_changed>%d</list_changed>\
<list_display>%d</list_display>\
<list_syschange>%d</list_syschange>\
<list_count>%d</list_count>\
<list_lookahead>%d</list_lookahead>\
<list_last_update>%.3f</list_last_update>\
</status>\
</alist>",
												((pObj->m_ulFlags&HELIOS_FLAG_ENABLED)?1:0),
												pObj->m_nChannelID,
												pChg->m_pszServerName,
												pObj->m_nHarrisListID,
												pChg->m_pszDesc,
												pChg->m_timestamp.time, pChg->m_timestamp.millitm,
												pChg->m_list.liststate,
												pChg->m_list.listchanged,
												pChg->m_list.listdisplay,
												pChg->m_list.listsyschange,
												pChg->m_list.listcount,
												pChg->m_list.lookahead,
												pObj->m_dblUpdateTime
																																	
											);
											
											bBufferMade = true;

										}

										nCode = g_phelios->m_data.m_ppControlObj[i]->m_lpfnDllCtrl((void**)(&(msg.m_pchResponse[CX_XML_BUFFER_DATA])), DLLCMD_ALIST);
										if(nCode<HELIOS_SUCCESS)
										{
											/// do something, report?
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending channel info for %s:%d to module %s: %s (%s)", nCode,
																pChg->m_pszServerName, ((CSentinelChannelObject*)(pChg->m_pObj))->m_nHarrisListID, g_phelios->m_data.m_ppControlObj[i]->m_pszModule, g_phelios->m_data.m_ppControlObj[i]->m_pszName,g_phelios->m_data.m_ppControlObj[i]->m_pszDesc );
											g_phelios->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:control_module_service", errorstring);    //(Dispatch message)
										}



									} break;
									case DLLCMD_AITEM://	0x0102: //	Sent to the DLL when an event on a monitored list is new, has changed, or is removed.  In the case that the event is removed, the aitem.info.position element will contain a value of -1, and no other aitem.info elements are included.�
									{

if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONTROL)	
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "sending DLLCMD_AITEM (%d)", bBufferMade); //  Sleep(250);//(Dispatch message)

										if(!bBufferMade)
										{
											CSentinelEventObject* pObj = (CSentinelEventObject*)pChg->m_pObj;

											if(pObj->m_nPosition<0)  //deletion!
											{

												msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
													"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<position>%d</position>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pObj->m_uid,
																					pChg->m_pszServerName?pChg->m_pszServerName:"",
																					pChg->m_nHarrisListID,
																					pChg->m_nChannelID,
																					pObj->m_nPosition,
																					pObj->m_dblUpdateTime
																																										
																									);
											}
											else
											{

												msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
													"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pObj->m_uid,
																					pChg->m_pszServerName?pChg->m_pszServerName:"",
																					pChg->m_nHarrisListID,
																					pChg->m_nChannelID,
																					pChg->m_pchXMLkey?pChg->m_pchXMLkey:"",
																					pChg->m_pchXMLrec?pChg->m_pchXMLrec:"",
																					pChg->m_pchXMLclip?pChg->m_pchXMLclip:"",
																					((pObj->m_event.m_ucSegment==0xff)?-1:pObj->m_event.m_ucSegment),
																					pChg->m_pchXMLtitle?pChg->m_pchXMLtitle:"",
																					pChg->m_pchXMLdata?pChg->m_pchXMLdata:"",
																					pObj->m_event.m_usType,
																					pObj->m_event.m_usStatus,
																					pObj->m_event.m_ulControl,
																					pObj->m_dblTime,
																					pObj->m_event.m_ulDurationMS,
																					pObj->m_dblCalcTime,
																					pObj->m_nPosition,
																					pChg->m_nParentUID,
																					pObj->m_dblUpdateTime
																																										
																									);
											}
											bBufferMade = true;


										}

										nCode = g_phelios->m_data.m_ppControlObj[i]->m_lpfnDllCtrl((void**)(&(msg.m_pchResponse[CX_XML_BUFFER_DATA])), DLLCMD_AITEM);
										if(nCode<HELIOS_SUCCESS)
										{
											/// do something, report?
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending event info for %s to module %s: %s (%s)", nCode,
																pChg->m_pchXMLclip, g_phelios->m_data.m_ppControlObj[i]->m_pszModule, g_phelios->m_data.m_ppControlObj[i]->m_pszName,g_phelios->m_data.m_ppControlObj[i]->m_pszDesc );
											g_phelios->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:control_module_service", errorstring);    //(Dispatch message)
										}

									} break;
								}
									
							}
							i++;
						}
					}
//g_phelios->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "E leaving extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
					LeaveCriticalSection(&g_phelios->m_data.m_critExternalModules);
//g_phelios->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "E left extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)


					try{ delete pChg; } catch(...){}
					pChg=NULL;
					g_phelios->m_data.m_nChangesArrayIndex++;
					g_phelios->m_data.m_nNumChanges--;
					if(g_phelios->m_data.m_nChangesArrayIndex>=g_phelios->m_data.m_nNumChangesArray)
						g_phelios->m_data.m_nChangesArrayIndex = 0; // ring buffer
				}
				LeaveCriticalSection(&g_phelios->m_data.m_critControlModuleService);
			}
			else
			{
				LeaveCriticalSection(&g_phelios->m_data.m_critControlModuleService);
				Sleep(1);  // fast while when changes.
			}
		}
	
	}  // Big while, but want it to be fast if there are changes to process.
	EnterCriticalSection(&g_phelios->m_data.m_critControlModuleService);

	if(g_phelios->m_data.m_ppChanges)
	{	
		int i=g_phelios->m_data.m_nChangesArrayIndex;
		int j=0;
		while((j<g_phelios->m_data.m_nNumChanges)&&(i<g_phelios->m_data.m_nNumChangesArray))
		{
			if(g_phelios->m_data.m_ppChanges[i]){ try{ delete g_phelios->m_data.m_ppChanges[i]; } catch(...){}} // delete objects, must use new to allocate
			i++;
			j++;
		}
		try{ delete [] g_phelios->m_data.m_ppChanges;  } catch(...){}// delete array of pointers to objects, must use new to allocate
	}

	g_phelios->m_data.m_ppChanges = NULL;
	g_phelios->m_data.m_nNumChanges=0;
	g_phelios->m_data.m_nNumChangesArray=0;
	g_phelios->m_data.m_nChangesArrayIndex=0;

	g_phelios->m_data.m_bControlModuleServiceThreadStarted=false;
	LeaveCriticalSection(&g_phelios->m_data.m_critControlModuleService);
	g_phelios->m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:debug", "Control module service thread ended."); //(Dispatch message)

}

void HeliosPublishChangeThread(void* pvArgs)
{
	if((pvArgs==NULL)||(!g_phelios->m_data.m_bControlModuleServiceThreadStarted)||(!g_phelios->m_settings.m_bUseControlModules)) return;
	CSentinelChange* pChg = (CSentinelChange*)pvArgs;

	_ftime(&(pChg->m_timestamp));
	EnterCriticalSection(&g_phelios->m_data.m_critControlModuleService);

	if(g_phelios->m_data.m_nNumChanges >= g_phelios->m_data.m_nNumChangesArray) // have to grow the buffa
	{
		
		CSentinelChange** ppChanges = new CSentinelChange*[(g_phelios->m_data.m_nNumChangesArray+HELIOS_CHANGELIST_ARRAYSIZE_INC)];

		if(ppChanges)
		{
			int j=0;
			int i=g_phelios->m_data.m_nChangesArrayIndex;
			
			while(j<g_phelios->m_data.m_nNumChanges)
			{
				ppChanges[j] = g_phelios->m_data.m_ppChanges[i];

				if(i>=g_phelios->m_data.m_nNumChangesArray) i=0; // go around the ring.

				j++;
			}

			g_phelios->m_data.m_nChangesArrayIndex=0;
			g_phelios->m_data.m_nNumChangesArray += HELIOS_CHANGELIST_ARRAYSIZE_INC;

			try{ delete [] g_phelios->m_data.m_ppChanges; } catch(...){}

			g_phelios->m_data.m_ppChanges = ppChanges;

		}
	}

	if(g_phelios->m_data.m_nNumChanges < g_phelios->m_data.m_nNumChangesArray) // if there is buffa room
	{
		int i = g_phelios->m_data.m_nChangesArrayIndex + g_phelios->m_data.m_nNumChanges;
		if(i>=g_phelios->m_data.m_nNumChangesArray) i-= g_phelios->m_data.m_nNumChangesArray; // ring!
		g_phelios->m_data.m_ppChanges[i] = pChg;
		g_phelios->m_data.m_nNumChanges++;
	}
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONTROL)	
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "added change, changes now %d", g_phelios->m_data.m_nNumChanges); //  Sleep(250);//(Dispatch message)


	LeaveCriticalSection(&g_phelios->m_data.m_critControlModuleService);
}


// [0]
// [1]
// [2]  index 
// [3]
// [4]
// array = 5


void SentinelWorkingSetMaintainThread(void* pvArgs)
{
	if((g_phelios)&&(g_phelios->m_data.m_bWorkingSetThreadStarted)) return; // allow only one working set thread.
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

	g_phelios->m_data.m_bWorkingSetThreadSuppress = true;

	CDBUtil db;
	CDBconn* pdbConn = db.CreateNewConnection(g_phelios->m_settings.m_pszDSN, g_phelios->m_settings.m_pszUser, g_phelios->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			g_phelios->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Sentinel:working_set_database_connect", errorstring);  //(Dispatch message)
		}
		else
		{
			g_phelios->m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:working_set_database_connect", "database connected");  //(Dispatch message)
		}
	}
//	else	?

	EnterCriticalSection(&g_phelios->m_data.m_critWorkingSet);
	if(g_phelios->m_data.m_ppWorkingSet)
	{
		int i=0;
		while(i<g_phelios->m_data.m_nNumSetIDs)
		{
			if(g_phelios->m_data.m_ppWorkingSet[i])
			{
				if(g_phelios->m_data.m_ppWorkingSet[i]->pszKey) free(g_phelios->m_data.m_ppWorkingSet[i]->pszKey);
				try{delete g_phelios->m_data.m_ppWorkingSet[i];} // delete objects, must use new to allocate
				catch(...){}
			}
			i++;
		}
		try{delete [] g_phelios->m_data.m_ppWorkingSet;} // delete array of pointers to objects, must use new to allocate
		catch(...){}
	}
	if(g_phelios->m_data.m_ppWorkingSetChanges)
	{
		int i=0;
		while(i<g_phelios->m_data.m_nNumSetChanges)
		{
			if(g_phelios->m_data.m_ppWorkingSetChanges[i])
			{
				if(g_phelios->m_data.m_ppWorkingSetChanges[i]->pszKey) free(g_phelios->m_data.m_ppWorkingSetChanges[i]->pszKey);
				try{delete g_phelios->m_data.m_ppWorkingSetChanges[i];} // delete objects, must use new to allocate
				catch(...){}
			}
			i++;
		}
		try{delete [] g_phelios->m_data.m_ppWorkingSetChanges;} // delete array of pointers to objects, must use new to allocate
		catch(...){}
	}

	g_phelios->m_data.m_ppWorkingSetChanges=NULL;
	g_phelios->m_data.m_nNumSetChanges=0;

	g_phelios->m_data.m_ppWorkingSet=NULL;
	g_phelios->m_data.m_nNumSetIDs=0;

	LeaveCriticalSection(&g_phelios->m_data.m_critWorkingSet);

	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
													g_phelios->m_settings.m_pszWorkingSet  // table name
												);


if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_WORKINGSET)
{
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set init SQL: %s", szSQL);
}

	if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
	{
			//**MSG
g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set init SQL exception: %s", errorstring);
	}

	g_phelios->m_data.m_bWorkingSetThreadStarted = true;
	g_phelios->m_data.m_bWorkingSetThreadSuppress = false;

	while(((!g_bKillThread)||(g_phelios->m_data.m_nNumSetIDs>0))&&(g_phelios->m_settings.m_bUseWorkingSet)) // exit when killed, but only after all have been managed.
	{
		EnterCriticalSection(&g_phelios->m_data.m_critWorkingSet);

		workingset* pItem = NULL;

		if((g_phelios->m_data.m_ppWorkingSetChanges)&&(g_phelios->m_data.m_nNumSetChanges))
		{
			pItem = g_phelios->m_data.m_ppWorkingSetChanges[0];

			int i=0;
			g_phelios->m_data.m_nNumSetChanges--;
			while(i<g_phelios->m_data.m_nNumSetChanges)
			{
				g_phelios->m_data.m_ppWorkingSetChanges[i] = g_phelios->m_data.m_ppWorkingSetChanges[i+1];
				i++;
			}
			g_phelios->m_data.m_ppWorkingSetChanges[i]=NULL;
		}

		if((pItem)&&(pItem->pszKey)&&(strlen(pItem->pszKey)>0))
		{ 
			int i=0, r=-1;
			if(pItem->bAdd)
			{
				if(g_phelios->m_data.m_ppWorkingSet)
				{
					while(i<g_phelios->m_data.m_nNumSetIDs)
					{
						if(
								(g_phelios->m_data.m_ppWorkingSet[i])
							&&(g_phelios->m_data.m_ppWorkingSet[i]->pszKey)
							)
						{
							if(strcmp(g_phelios->m_data.m_ppWorkingSet[i]->pszKey, pItem->pszKey)==0)
							{
								r=i;
								g_phelios->m_data.m_ppWorkingSet[i]->nRefCount++;
								free(pItem->pszKey);
								try{delete pItem;} catch(...){}
								break;
							}
						}
						
						i++;
					}
				}
				if(r<0) // not found have to add.
				{
					workingset** ppItems = new workingset*[g_phelios->m_data.m_nNumSetIDs+1];
					if(ppItems)
					{
						i=0;
						while(i<g_phelios->m_data.m_nNumSetIDs)
						{
							if(g_phelios->m_data.m_ppWorkingSet)
							{
								ppItems[i] = g_phelios->m_data.m_ppWorkingSet[i];
							}
							i++;
						}
						ppItems[i] = pItem;
						ppItems[i]->nRefCount=1;
						workingset** ppItemsTemp = g_phelios->m_data.m_ppWorkingSet;
						g_phelios->m_data.m_ppWorkingSet = ppItems;
						g_phelios->m_data.m_nNumSetIDs++;
						try{delete [] ppItemsTemp;} catch(...){}

						// do database add
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
							"INSERT INTO %s SELECT getDate(), %s.* FROM %s WHERE key_field = '%s'",
															g_phelios->m_settings.m_pszWorkingSet, // table name
															g_phelios->m_settings.m_pszClipKeyMetadataView, // view name
															g_phelios->m_settings.m_pszClipKeyMetadataView, // view name
															pItem->pszKey
														);
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_WORKINGSET)
{
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set insert SQL: %s", szSQL);
}

						if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
								//**MSG
g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set insert SQL exception: %s", errorstring);
						}

					}
					else
					{ // error, just have to ignore.
						free(pItem->pszKey);
						try{delete pItem;} catch(...){}
					}
				}
			}
			else  //delete
			{
				if(g_phelios->m_data.m_ppWorkingSet)
				{
					while(i<g_phelios->m_data.m_nNumSetIDs)
					{
						if(
								(g_phelios->m_data.m_ppWorkingSet[i])
							&&(g_phelios->m_data.m_ppWorkingSet[i]->pszKey)
							)
						{
							if(strcmp(g_phelios->m_data.m_ppWorkingSet[i]->pszKey, pItem->pszKey)==0)
							{
								r=i;
								g_phelios->m_data.m_ppWorkingSet[i]->nRefCount--;
								free(pItem->pszKey);
								try{delete pItem;} catch(...){}
								if(g_phelios->m_data.m_ppWorkingSet[i]->nRefCount<=0)
								{
									// do database delete
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
										"DELETE FROM %s WHERE key_field = '%s'",
																		g_phelios->m_settings.m_pszWorkingSet, // table name
																		g_phelios->m_data.m_ppWorkingSet[i]->pszKey
																	);
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_WORKINGSET)
{
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set delete SQL: %s", szSQL);
}

									if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
									{
											//**MSG
g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set delete SQL exception: %s", errorstring);
									}

									try{free(g_phelios->m_data.m_ppWorkingSet[i]->pszKey);} catch(...){}
									try{delete g_phelios->m_data.m_ppWorkingSet[i];} catch(...){}
									g_phelios->m_data.m_nNumSetIDs--;
									while(i<g_phelios->m_data.m_nNumSetIDs)
									{
										g_phelios->m_data.m_ppWorkingSet[i] = g_phelios->m_data.m_ppWorkingSet[i+1];
										i++;
									}
									g_phelios->m_data.m_ppWorkingSet[i] = NULL;
								}
								break;
							}
						}
						
						i++;
					}
				}

			}
		}

		LeaveCriticalSection(&g_phelios->m_data.m_critWorkingSet);
		Sleep(10);
	}

	EnterCriticalSection(&g_phelios->m_data.m_critWorkingSet);
	g_phelios->m_data.m_bWorkingSetThreadSuppress = true;
	LeaveCriticalSection(&g_phelios->m_data.m_critWorkingSet);

	// exiting, remove working set from db.
	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
													g_phelios->m_settings.m_pszWorkingSet  // table name
												);


if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_WORKINGSET)
{
	g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set init SQL: %s", szSQL);
}

	if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
	{
			//**MSG
g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set init SQL exception: %s", errorstring);
	}
	db.RemoveConnection(pdbConn);

	// clear mem
	EnterCriticalSection(&g_phelios->m_data.m_critWorkingSet);
	if(g_phelios->m_data.m_ppWorkingSet)
	{
		int i=0;
		while(i<g_phelios->m_data.m_nNumSetIDs)
		{
			if(g_phelios->m_data.m_ppWorkingSet[i])
			{
				if(g_phelios->m_data.m_ppWorkingSet[i]->pszKey) free(g_phelios->m_data.m_ppWorkingSet[i]->pszKey);
				try{delete g_phelios->m_data.m_ppWorkingSet[i];} // delete objects, must use new to allocate
				catch(...){}
			}
			i++;
		}
		try{delete [] g_phelios->m_data.m_ppWorkingSet;} // delete array of pointers to objects, must use new to allocate
		catch(...){}
	}
	if(g_phelios->m_data.m_ppWorkingSetChanges)
	{
		int i=0;
		while(i<g_phelios->m_data.m_nNumSetChanges)
		{
			if(g_phelios->m_data.m_ppWorkingSetChanges[i])
			{
				if(g_phelios->m_data.m_ppWorkingSetChanges[i]->pszKey) free(g_phelios->m_data.m_ppWorkingSetChanges[i]->pszKey);
				try{delete g_phelios->m_data.m_ppWorkingSetChanges[i];} // delete objects, must use new to allocate
				catch(...){}
			}
			i++;
		}
		try{delete [] g_phelios->m_data.m_ppWorkingSetChanges;} // delete array of pointers to objects, must use new to allocate
		catch(...){}
	}

	g_phelios->m_data.m_ppWorkingSetChanges=NULL;
	g_phelios->m_data.m_nNumSetChanges=0;

	g_phelios->m_data.m_ppWorkingSet=NULL;
	g_phelios->m_data.m_nNumSetIDs=0;

	LeaveCriticalSection(&g_phelios->m_data.m_critWorkingSet);
	g_phelios->m_data.m_bWorkingSetThreadStarted = false;
}


void SentinelWorkingSetAdjustThread(void* pvArgs)
{
	workingset* pItem = (workingset*)pvArgs;
	if((pItem)&&(pItem->pszKey)&&(strlen(pItem->pszKey)>0))
	{ 
		EnterCriticalSection(&g_phelios->m_data.m_critWorkingSet);
		if(!g_phelios->m_data.m_bWorkingSetThreadSuppress)
		{
			workingset** ppItems = new workingset*[g_phelios->m_data.m_nNumSetChanges+1];
			if(ppItems)
			{
				int i=0;
				while(i<g_phelios->m_data.m_nNumSetChanges)
				{
					if(g_phelios->m_data.m_ppWorkingSetChanges)
					{
						ppItems[i] = g_phelios->m_data.m_ppWorkingSetChanges[i];
					}
					i++;
				}
				ppItems[i] = pItem;
				workingset** ppItemsTemp = g_phelios->m_data.m_ppWorkingSetChanges;
				g_phelios->m_data.m_ppWorkingSetChanges = ppItems;
				g_phelios->m_data.m_nNumSetChanges++;
				try{delete [] ppItemsTemp;}catch(...){}
			}
			else
			{ // error, just have to ignore.
				free(pItem->pszKey);
				try{delete pItem;}catch(...){}
			}
		}

		LeaveCriticalSection(&g_phelios->m_data.m_critWorkingSet);
	}
}


