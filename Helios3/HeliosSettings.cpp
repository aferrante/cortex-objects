// HeliosSettings.cpp: implementation of the CHeliosSettings.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "HeliosDefines.h"
#include "HeliosSettings.h"
//#include "../../Common/API/Omnibus/OmniComm.h"
#include "HeliosMain.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern COmniComm g_omni;
extern CHeliosMain* g_phelios;
extern CHeliosApp theApp;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHeliosSettings::CHeliosSettings()
{
	m_pdb = NULL;
	m_pdbConn = NULL;

	m_pszName = NULL;
	m_pszType = NULL;  // familiar name of the type.
	m_pszProject = NULL;  // familiar name of the project name.
	m_ulMainMode = HELIOS_MODE_DEFAULT;

	
	m_nAutoPurgeMessageDays = 30; // default
//	m_nAutoPurgeAsRunDays = 30; // default
	m_nAutoPurgeInterval = 300;  // 5 minutes should be enough.


	// ports
	m_usCommandPort	= HELIOS_PORT_CMD;
	m_usStatusPort	= HELIOS_PORT_STATUS;

	// messaging for Helios
	m_bUseLog = true;			// write a log file
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host
	m_pszFileSpec = NULL;
	m_pszMailSpec = NULL;
	m_pszProcessedFileSpec = NULL;
	m_pszProcessedMailSpec = NULL;
	m_bSendMsg= true; // send a message to the UI
	m_bListStatusInExchange=false; // old way, in exchange table.
	m_bStatusInTables=false; // new way, in Connections (server status) and Channels (list status) table.

	// Harris API
//	m_bUseListCount = false; // get all events up until the list count (otherwise just up to the lookahead)
	m_nThreadDwellMS = 1000;
	m_bDebugLists= false;  // if true, dumps the temp and events databases
	m_nDebugListTopCount=-1;  //if positive, just prints out the top n recored in the events tables.
	m_bDebugSQL=false;  // if true, prints out the SQL calls
	m_bDebugInsertSQL=false;  // if true, prints out the event insert SQL calls
	m_ulDebug = 0;
	m_pszDebugOrder = NULL;

	// DSN params
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;

	m_pszSettings = NULL;  // the Settings table name
	m_pszExchange = NULL;  // the Exchange table name
	m_pszMessages = NULL;  // the Messages table name
	m_pszAsRun = NULL;
	m_pszControlModules=NULL;  // the control modules table name - DLLs that get commanded by changes in Sentinel
	m_pszInterpreterModules=NULL;  // the interpreter modules table name - DLLs that accept automation data from an external source

	m_pszChannels = NULL;  // the Channels table name
	m_pszConnections = NULL;  // the Connections table name
	m_pszLiveEvents = NULL; // the LiveEvents table name

	m_bClearEventsInit = false;  // if true, clears channel based event table on init
	m_bClearEventsExit = false;  // if true, clears channel based event table on exit

	m_bUseActive = false;

	m_pszOmnibusDataFields = NULL;
	m_pszDebugLog=NULL;
	m_pszCommLog=NULL;
	m_bWriteXML=false;
	// Omnibus old event purge
	m_ulCheckInterval=0;	// number of seconds between checks (0 turns off)
	m_ulExpiryPeriod=10800;	// (default value 3 hours) when the event's end time (start plus duration) is more than this number of seconds older than the current server time, it is purged.
	m_ulConnTimeout=300;	// when the connection has received no update for number of seconds, the connection is restarted 
	m_ulConnectionRetries = LONG_MAX-1;		// how many times to try to re-establish ULONG_MAX-1 is 4085 years with 30 second - so half that interval!
	m_ulConnectionInterval = 30;	// how long to wait in seconds before retrying

	m_ulModsIntervalMS = 6000;

	m_bUseXMLClientLog = false;
	m_bMillisecondMessaging=true;			// use millisecond resolution for messages and asrun

	m_pszLicense=NULL;  // the License Key
	m_pszOEMcodes=NULL; // the possible OEM string
	m_pszIconPath=NULL;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.
}

CHeliosSettings::~CHeliosSettings()
{
	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_pszType) free(m_pszType); // must use malloc to allocate
	if(m_pszProject) free(m_pszProject); // must use malloc to allocate
	if(m_pszDSN) free(m_pszDSN); // must use malloc to allocate
	if(m_pszUser) free(m_pszUser); // must use malloc to allocate
	if(m_pszPW) free(m_pszPW); // must use malloc to allocate
	if(m_pszSettings) free(m_pszSettings); // must use malloc to allocate
	if(m_pszExchange) free(m_pszExchange); // must use malloc to allocate
	if(m_pszMessages) free(m_pszMessages); // must use malloc to allocate
	if(m_pszAsRun) free(m_pszAsRun); // must use malloc to allocate

	if(m_pszChannels) free(m_pszChannels); // must use malloc to allocate
	if(m_pszConnections) free(m_pszConnections); // must use malloc to allocate
	if(m_pszLiveEvents) free(m_pszLiveEvents); // must use malloc to allocate
	if(m_pszDebugLog) free(m_pszDebugLog); // must use malloc to allocate
	if(m_pszCommLog) free(m_pszCommLog); // must use malloc to allocate
	if(m_pszLicense) free(m_pszLicense); // must use malloc to allocate
	if(m_pszOEMcodes) free(m_pszOEMcodes); // must use malloc to allocate
	if(m_pszIconPath) free(m_pszIconPath); // must use malloc to allocate	
	if(m_pszFileSpec) free(m_pszFileSpec); // must use malloc to allocate	
	if(m_pszMailSpec) free(m_pszMailSpec); // must use malloc to allocate	
	if(m_pszProcessedFileSpec) free(m_pszProcessedFileSpec); // must use malloc to allocate	
	if(m_pszProcessedMailSpec) free(m_pszProcessedMailSpec); // must use malloc to allocate	

	if(m_pszDebugOrder) free(m_pszDebugOrder); // must use malloc to allocate
}


int CHeliosSettings::Settings(bool bRead)
{
	char pszFilename[MAX_PATH];
	strcpy(pszFilename, "");
//		AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, HELIOS_SETTINGS_FILE_DEFAULT);  // cortex settings file

	CFileUtil file;
	// get settings.
	file.GetSettings(pszFilename, false); 

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
		// load up the values on the settings object
			m_pszName = file.GetIniString("Main", "Name", "Helios", m_pszName);
			m_pszType = file.GetIniString("Main", "Type", "Helios", m_pszType);
			m_pszProject = file.GetIniString("Main", "Project", "VDS", m_pszProject);
			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);
			m_pszOEMcodes = file.GetIniString("License", "COM", "C1JL-sPLisQ5VCQqCR2LJwJPdlTK", m_pszOEMcodes);
			m_nThreadDwellMS = file.GetIniInt("Main", "ThreadDwellMS", 1000);


			m_nAutoPurgeMessageDays = file.GetIniInt("AutoPurge", "AutoPurgeMessageDays", 30);
	//		m_nAutoPurgeAsRunDays = file.GetIniInt("AutoPurge", "AutoPurgeAsRunDays", 30);
			m_nAutoPurgeInterval  = file.GetIniInt("AutoPurge", "AutoPurgeInterval", 300);  // 5 minutes should be enough.

			m_bDebugLists = file.GetIniInt("Database", "DebugLists", 0)?true:false;  // if true, dumps the temp and events databases
			m_nDebugListTopCount = file.GetIniInt("Database", "DebugListTopCount", -1);  //if positive, just prints out the top n recored in the events tables.
			m_bDebugSQL = file.GetIniInt("Database", "DebugSQL", 0)?true:false;  // if true, prints out the SQL calls
			m_bDebugInsertSQL = file.GetIniInt("Database", "DebugInsertSQL", 0)?true:false;  // if true, prints out the event insert SQL calls
			m_ulDebug = file.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.
			m_pszDebugOrder = file.GetIniString("Database", "DebugOrder", "(case when parent_position < 0 then ((event_position)/1000.0) else (parent_calc_start + (event_position-parent_position)/1000.0) end)", m_pszDebugOrder); // the order by clause for debug

			// recompile license key params

			if(g_phelios)
			{
				if(g_phelios->m_data.m_key.m_pszLicenseString) free(g_phelios->m_data.m_key.m_pszLicenseString);
				g_phelios->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(m_pszLicense)+1);
				if(g_phelios->m_data.m_key.m_pszLicenseString)
				sprintf(g_phelios->m_data.m_key.m_pszLicenseString, "%s", m_pszLicense);

				g_phelios->m_data.m_key.InterpretKey();

				char errorstring[MAX_MESSAGE_LENGTH];
				if(g_phelios->m_data.m_key.m_bValid)
				{
					unsigned long i=0;
					while(i<g_phelios->m_data.m_key.m_ulNumParams)
					{
						if((g_phelios->m_data.m_key.m_ppszParams)
							&&(g_phelios->m_data.m_key.m_ppszValues)
							&&(g_phelios->m_data.m_key.m_ppszParams[i])
							&&(g_phelios->m_data.m_key.m_ppszValues[i]))
						{
							if(stricmp(g_phelios->m_data.m_key.m_ppszParams[i], "max")==0)
							{
								g_phelios->m_data.m_nMaxLicensedChannels = atoi(g_phelios->m_data.m_key.m_ppszValues[i]);
							}
							else
							if(stricmp(g_phelios->m_data.m_key.m_ppszParams[i], "oem")==0)
							{
								// if it exists, check OEM string.

							// for OEM partner check on license key, need oem=xxxr in the params'
//oem=xxxr where xxx is a client code string, and r is any random ASCII character in the range 33 to 126, excluding URL-sensitive characters (34, 38, 39, 46, 47, 60, 61, 62, 63).
//the oem parameter should be the last thing in the string, and the last random character allows us to keep the encoding more secure.
//the client codes for OEM suppliers is as follows (note these ARE case sensitive):

//Strategy and Technology (S&T): SnT
//Harris: HAS
//Softel: SFT
//Chyron: Chy
//Ensequence: Ens
//VDS: VDS (for test purposes)

//If an OEM code is specified in the license key, a supplier code table including these values must exist in the CSF file, for it to match on. 
//This will be stored in a CSF parameter under the [License] heading, called COM for Cortex OEM Management (haha).

//[License]
//COM=C1JL-sPLisQ5VCQqCR2LJwJPdlTK

//the above string includes all of the above client codes.  The applications will do a check on the license key against the allowed suppliers in this code, which by default will be included and created by the applications.  As the approved vendor list changes, new versions will create a new default code, but for interim installations, the code can be updated in the CSF file.

// AND the way the string is generated is as follows:
// take this string:
//							VDSHAS|ChySnT|SFTEns
// which is delimited pairs of 3 letter codes (do in pairs so we dont have delimited 4 char, or naked 3 char regular)
//							base 64 endcode using the license key alpha and padch
//#define LICENSE_B64ALPHA		"XhIWsP4RtOoSTQpuvJ-LBCnGrAyd8eVi7%Dz1wkl5xMj6NqZa2YHbc9f0+Eg3UFm"
//#define LICENSE_B64PADCH		'K'
// and there you go.

								if((m_pszOEMcodes)&&(strlen(m_pszOEMcodes)>3))
								{
									CBufferUtil bu;
									char* pszCodes = m_pszOEMcodes;
									unsigned long ulBufLen = strlen(m_pszOEMcodes);
									bu.Base64Decode(&pszCodes, &ulBufLen, false, LICENSE_B64ALPHA, LICENSE_B64PADCH);

									CSafeBufferUtil sbu;
									char* pchCodes = sbu.Token(pszCodes, strlen(pszCodes), "|");
									g_phelios->m_data.m_key.m_bValid = false;
									while(pchCodes)
									{
										if(strncmp(g_phelios->m_data.m_key.m_ppszValues[i], pchCodes, 3)==0)
										{
											g_phelios->m_data.m_key.m_bValid = true;
											break;
										}
										else
										if((strlen(pchCodes)>3)&&(strncmp(g_phelios->m_data.m_key.m_ppszValues[i], pchCodes+3, 3)==0))
										{
											g_phelios->m_data.m_key.m_bValid = true;
											break;
										}
										pchCodes = sbu.Token(NULL, NULL, "|");
									}
									if(pszCodes)
									{
										try{free(pszCodes);} catch(...){}
									}
								}
								else g_phelios->m_data.m_key.m_bValid = false;

							}
						}
						i++;
					}
				
					if(
							(g_phelios->m_data.m_key.m_bValid)
						&&(
								(!g_phelios->m_data.m_key.m_bExpires)
							||((g_phelios->m_data.m_key.m_bExpires)&&(!g_phelios->m_data.m_key.m_bExpired))
							||((g_phelios->m_data.m_key.m_bExpires)&&(g_phelios->m_data.m_key.m_bExpireForgiveness)&&(g_phelios->m_data.m_key.m_ulExpiryDate+g_phelios->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
							)
						&&(
								(!g_phelios->m_data.m_key.m_bMachineSpecific)
							||((g_phelios->m_data.m_key.m_bMachineSpecific)&&(g_phelios->m_data.m_key.m_bValidMAC))
							)
						)
					{
					// this overrides stuff so let's not let it do that.

										// let's just not override this in a green condition.  
										// comment it out.
//						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//						g_phelios->m_data.SetStatusText(errorstring, HELIOS_STATUS_OK);
					}
					else
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
						g_phelios->m_data.SetStatusText(errorstring, HELIOS_STATUS_ERROR);
					}
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
					g_phelios->m_data.SetStatusText(errorstring, HELIOS_STATUS_ERROR);
				}
			}

			m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", HELIOS_PORT_CMD);
			m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", HELIOS_PORT_STATUS);

			m_pszIconPath = file.GetIniString("FileServer", "IconPath", "C:\\Inetpub\\wwwroot\\Cortex\\helios\\images\\", m_pszIconPath);

			m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			m_bUseXMLClientLog = file.GetIniInt("Messager", "UseXMLClientLog", 0)?true:false;
			m_bMillisecondMessaging = file.GetIniInt("Messager", "MillisecondMessaging", 1)?true:false;			// use millisecond resolution for messages and asrun
			m_pszAsRun = file.GetIniString("Database", "AsRunTableName", "AsRun_Log", m_pszAsRun);  // the AsRun table name
			
			m_bSendMsg = file.GetIniInt("Messager", "SendMsg", 1)?true:false;  // send a message to the UI from the Helios connection thread
			m_bListStatusInExchange = file.GetIniInt("Database", "ListStatusInExchange", 0)?true:false;  // old way, in exchange table.
			m_bStatusInTables = file.GetIniInt("Database", "StatusInTables", 1)?true:false;// new way, in Connections (server status) and Channels (list status) table.

			m_pszOmnibusDataFields = file.GetIniString("OmnibusXML", "DataFields", "data,logos", m_pszOmnibusDataFields);  // the data fields we care about
			m_pszDebugLog = file.GetIniString("OmnibusXML", "DebugLog", NULL, m_pszDebugLog); 
			m_pszCommLog = file.GetIniString("OmnibusXML", "CommLog", NULL, m_pszCommLog); 
			m_bWriteXML = file.GetIniInt("OmnibusXML", "WriteXML", 0)?true:false;  

			m_ulCheckInterval=file.GetIniInt("OmnibusXML", "PurgeCheckInterval", 0);	// number of seconds between checks (0 turns off)
			m_ulExpiryPeriod=file.GetIniInt("OmnibusXML", "PurgeExpiryPeriod", 10800);	// (default value 3 hours) when the event's end time (start plus duration) is more than this number of seconds older than the current server time, it is purged.
			m_ulConnTimeout=file.GetIniInt("OmnibusXML", "ConnectionTimeout", 300);	// (default value 5 mins) 
			m_ulConnectionRetries = file.GetIniInt("OmnibusXML", "ConnectionRetries", LONG_MAX-1);		// how many times to try to re-establish ULONG_MAX-1 is 4085 years with 30 second interval!
			m_ulConnectionInterval = file.GetIniInt("OmnibusXML", "ConnectionInterval", 30);	// how long to wait in seconds before retrying

			m_bUseActive = file.GetIniInt("Database", "UseActive", 0)?true:false;

			m_pszDSN = file.GetIniString("Database", "DSN", (m_pszName?m_pszName:"Helios"), m_pszDSN);
			m_pszUser = file.GetIniString("Database", "DBUser", "sa", m_pszUser);
			m_pszPW = file.GetIniString("Database", "DBPassword", "", m_pszPW);
			m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings", m_pszSettings);  // the Settings table name
			m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange", m_pszExchange);  // the Exchange table name
			m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages", m_pszMessages);  // the Messages table name
			m_pszControlModules = file.GetIniString("Database", "ControlModulesTableName", "Control_Modules", m_pszControlModules);  // the control modules table name - DLLs that get commanded by changes in Sentinel
			m_pszInterpreterModules = file.GetIniString("Database", "InterpreterModulesTableName", "Interpreter_Modules", m_pszInterpreterModules);  // the interpreter modules table name - DLLs that accept automation data from an external source

			m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels", m_pszChannels);  // the Channels table name
			m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Adaptors", m_pszConnections);  // the Connections table name
			m_pszLiveEvents = file.GetIniString("Database", "LiveEventsTableName", "Events", m_pszLiveEvents);  // the LiveEvents table name

			m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
			m_bClearEventsInit = file.GetIniInt("Database", "ClearEventsOnChannelInit", 0)?true:false; // if true, clears channel based event table on init
			m_bClearEventsExit = file.GetIniInt("Database", "ClearEventsOnChannelExit", 0)?true:false; // if true, clears channel based event table on exit
	
			m_pszFileSpec = file.GetIniString("Messager", "LogFileIni", "Logs\\Helios|YD||1|", m_pszFileSpec);
				// SMTP address[: override port (optional)] | distribution lists | from email |  subject spec | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
			m_pszMailSpec = file.GetIniString("Messager", "SMTPIni", "mail.server.com|f3:recipient@address.com|\"someone\"<from@here.com>|%P %T from %h|%h|C:\\heliossmtp.txt|1|1|0", m_pszMailSpec);
			if(m_pszFileSpec) 
			{
				if(m_pszProcessedFileSpec)
				{
					try{ free(m_pszProcessedFileSpec); } catch(...) {}
				}
				m_pszProcessedFileSpec = ProcessString(m_pszFileSpec, false);
			}
			
			if(m_pszMailSpec) 
			{
				if(m_pszProcessedMailSpec)
				{
					try{ free(m_pszProcessedMailSpec); } catch(...) {}
				}
				m_pszProcessedMailSpec = ProcessString(m_pszMailSpec, false);
			}
		}
		else //write
		{

			file.SetIniString("Main", "Name", m_pszName);
			file.SetIniString("Main", "Type", m_pszType);
			file.SetIniString("Main", "Project", m_pszProject);
			file.SetIniInt("CommandServer", "ListenPort", m_usCommandPort);
			file.SetIniInt("StatusServer", "ListenPort", m_usStatusPort);
			file.SetIniString("License", "Key", m_pszLicense);
			file.SetIniInt("Main", "ThreadDwellMS", m_nThreadDwellMS);

			file.SetIniInt("Database", "DebugLists", m_bDebugLists?1:0);  // if true, dumps the temp and events databases
			file.SetIniInt("Database", "DebugListTopCount", m_nDebugListTopCount);  //if positive, just prints out the top n recored in the events tables.
			file.SetIniInt("Database", "DebugSQL", m_bDebugSQL?1:0);  // if true, prints out the SQL calls
			file.SetIniInt("Database", "DebugInsertSQL", m_bDebugInsertSQL?1:0);  // if true, prints out the event insert SQL calls
			file.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.
			file.SetIniString("Database", "DebugOrder", m_pszDebugOrder); // the order by clause for debug

			file.SetIniString("FileServer", "IconPath", m_pszIconPath);

			file.SetIniInt("Messager", "UseEmail", m_bUseEmail?1:0);
			file.SetIniInt("Messager", "UseNet", m_bUseNetwork?1:0);
			file.SetIniInt("Messager", "UseLog", m_bUseLog?1:0);
			file.SetIniInt("Messager", "SendMsg", m_bSendMsg?1:0);  // send a message to the UI from the Helios connection thread
			file.SetIniInt("Messager", "UseXMLClientLog", m_bUseXMLClientLog?true:false);
			file.SetIniInt("Messager", "MillisecondMessaging", m_bMillisecondMessaging?true:false);		// use millisecond resolution for messages and asrun
			file.SetIniString("Database", "AsRunTableName", m_pszAsRun);  // the AsRun table name

			file.SetIniString("Database", "DSN", m_pszDSN);
			file.SetIniString("Database", "DBUser", m_pszUser);
			file.SetIniString("Database", "DBPassword", m_pszPW);
			file.SetIniString("Database", "SettingsTableName", m_pszSettings);  // the Settings table name
			file.SetIniString("Database", "ExchangeTableName", m_pszExchange);  // the Exchange table name
			file.SetIniString("Database", "MessagesTableName", m_pszMessages);  // the Messages table name
			file.SetIniString("Database", "ControlModulesTableName", m_pszControlModules);  // the control modules table name - DLLs that get commanded by changes in Sentinel
			file.SetIniString("Database", "InterpreterModulesTableName", m_pszInterpreterModules);  // the interpreter modules table name - DLLs that accept automation data from an external source
			file.SetIniInt("Database", "ListStatusInExchange", m_bListStatusInExchange?1:0);  // old way, in exchange table.
			file.SetIniInt("Database", "StatusInTables", m_bStatusInTables?1:0);// new way, in Connections (server status) and Channels (list status) table.
			file.SetIniInt("Database", "UseActive", m_bUseActive?1:0);

			file.SetIniString("OmnibusXML", "DataFields", m_pszOmnibusDataFields);  // the data fields we care about
			file.SetIniString("OmnibusXML", "DebugLog", m_pszDebugLog);  
			file.SetIniString("OmnibusXML", "CommLog", m_pszCommLog);  
			file.SetIniInt("OmnibusXML", "WriteXML", m_bWriteXML?1:0);  
			file.SetIniInt("OmnibusXML", "PurgeCheckInterval", m_ulCheckInterval);	// number of seconds between checks (0 turns off)
			file.SetIniInt("OmnibusXML", "PurgeExpiryPeriod", m_ulExpiryPeriod);	// (default value 3 hours) when the event's end time (start plus duration) is more than this number of seconds older than the current server time, it is purged.
			file.SetIniInt("OmnibusXML", "ConnectionTimeout", m_ulConnTimeout);	// (default value 2 mins) 
			file.SetIniInt("OmnibusXML", "ConnectionRetries", m_ulConnectionRetries);		// how many times to try to re-establish ULONG_MAX-1 is 4085 years with 30 second interval!
			file.SetIniInt("OmnibusXML", "ConnectionInterval", m_ulConnectionInterval);	// how long to wait in seconds before retrying

			file.SetIniString("Database", "ChannelsTableName", m_pszChannels);  // the Channels table name
			file.SetIniString("Database", "ConnectionsTableName", m_pszConnections);  // the Connections table name
			file.SetIniString("Database", "LiveEventsTableName", m_pszLiveEvents);  // the LiveEvents table name

			file.SetIniInt("Database", "ModificationCheckInterval", m_ulModsIntervalMS);  // in milliseconds
			file.SetIniInt("Database", "ClearEventsOnChannelInit", m_bClearEventsInit?1:0); // if true, clears channel based event table on init
			file.SetIniInt("Database", "ClearEventsOnChannelExit", m_bClearEventsExit?1:0); // if true, clears channel based event table on exit

			file.SetIniInt("AutoPurge", "AutoPurgeMessageDays", m_nAutoPurgeMessageDays);
//			file.SetIniInt("AutoPurge", "AutoPurgeAsRunDays", m_nAutoPurgeAsRunDays);
			file.SetIniInt("AutoPurge", "AutoPurgeInterval", m_nAutoPurgeInterval);  // 5 minutes should be enough.


			file.SetIniString("Messager", "LogFileIni", m_pszFileSpec);
			file.SetIniString("Messager", "SMTPIni", m_pszMailSpec);

			file.SetSettings(pszFilename, false);  // have to have correct filename

		}
		return HELIOS_SUCCESS;
	}
	return HELIOS_ERROR;
}



int CHeliosSettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	if((m_pszSettings)&&(strlen(m_pszSettings)>0)&&(m_pdbConn)&&(m_pdb)&&(m_pdbConn->m_bConnected)&&(Settings(true)==HELIOS_SUCCESS))  //read has to succeed
	{
		// get settings.
/*
		char pszFilename[MAX_PATH];

		strcpy(pszFilename, HELIOS_SETTINGS_FILE_DEFAULT);  // helios settings file

		CFileUtil file;
		file.GetSettings(pszFilename, false); 

		// load up the values on the settings object
		if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
		{
			g_phelios->m_settings.m_pszName = file.GetIniString("Main", "Name", "Helios");
			g_phelios->m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");
			g_phelios->m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");

			g_phelios->m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", HELIOS_PORT_CMD);
			g_phelios->m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", HELIOS_PORT_STATUS);

			g_phelios->m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			g_phelios->m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			g_phelios->m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;

			g_phelios->m_settings.m_pszOmnibusDataFields = file.GetIniString("OmnibusXML", "DataFields", "data,logos");  // the data fields we care about
			g_phelios->m_settings.m_pszDebugLog = file.GetIniString("OmnibusXML", "DebugLog", NULL); 
			g_phelios->m_settings.m_pszCommLog = file.GetIniString("OmnibusXML", "CommLog", NULL); 
			g_phelios->m_settings.m_bWriteXML = file.GetIniInt("OmnibusXML", "WriteXML", 0)?true:false;  

			g_phelios->m_settings.m_ulCheckInterval=file.GetIniInt("OmnibusXML", "PurgeCheckInterval", 0);	// number of seconds between checks (0 turns off)
			g_phelios->m_settings.m_ulExpiryPeriod=file.GetIniInt("OmnibusXML", "PurgeExpiryPeriod", 10800);	// (default value 3 hours) when the event's end time (start plus duration) is more than this number of seconds older than the current server time, it is purged.
			g_phelios->m_settings.m_ulConnTimeout=file.GetIniInt("OmnibusXML", "ConnectionTimeout", 300);	// (default value 5 mins) 

			g_phelios->m_settings.m_pszDSN = file.GetIniString("Database", "DSN", g_phelios->m_settings.m_pszName?g_phelios->m_settings.m_pszName:"Helios");
			g_phelios->m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
			g_phelios->m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
			g_phelios->m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
			g_phelios->m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
			g_phelios->m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name

			g_phelios->m_settings.m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels");  // the Channels table name
			g_phelios->m_settings.m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Adaptors");  // the Connections table name
			g_phelios->m_settings.m_pszLiveEvents = file.GetIniString("Database", "LiveEventsTableName", "Events");  // the LiveEvents table name

			g_phelios->m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
		}
*/
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
		EnterCriticalSection(&g_phelios->m_data.m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = HELIOS_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCategory="";
				CString szParameter="";
				CString szValue="";
				CString szTemp="";
				int min, max;
				bool bmin = false, bmax = false;
				try
				{
					prs->GetFieldValue("category", szCategory);  //HARDCODE
					prs->GetFieldValue("parameter", szParameter);  //HARDCODE
					prs->GetFieldValue("value", szValue);  //HARDCODE
					prs->GetFieldValue("min_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						min = atoi(szTemp);
						bmin = true;
					}
					prs->GetFieldValue("max_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						max = atoi(szTemp);
						bmax = true;
					}
				}
				catch( ... )
				{
				}

				int nLength = szValue.GetLength();
				if(szCategory.CompareNoCase("Main")==0)
				{
					if(szParameter.CompareNoCase("Name")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszName) free(m_pszName);
								m_pszName = pch;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("License")==0)
				{
					if(szParameter.CompareNoCase("Key")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLicense) free(m_pszLicense);
								m_pszLicense = pch;

								// recompile license key params
								if(g_phelios->m_data.m_key.m_pszLicenseString) free(g_phelios->m_data.m_key.m_pszLicenseString);
								g_phelios->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
								if(g_phelios->m_data.m_key.m_pszLicenseString)
								sprintf(g_phelios->m_data.m_key.m_pszLicenseString, "%s", szValue);

								g_phelios->m_data.m_key.InterpretKey();

								char errorstring[MAX_MESSAGE_LENGTH];
								if(g_phelios->m_data.m_key.m_bValid)
								{
									unsigned long i=0;
									while(i<g_phelios->m_data.m_key.m_ulNumParams)
									{
										if((g_phelios->m_data.m_key.m_ppszParams)
											&&(g_phelios->m_data.m_key.m_ppszValues)
											&&(g_phelios->m_data.m_key.m_ppszParams[i])
											&&(g_phelios->m_data.m_key.m_ppszValues[i]))
										{
											if(stricmp(g_phelios->m_data.m_key.m_ppszParams[i], "max")==0)
											{
												g_phelios->m_data.m_nMaxLicensedChannels = atoi(g_phelios->m_data.m_key.m_ppszValues[i]);
											}
											else
											if(stricmp(g_phelios->m_data.m_key.m_ppszParams[i], "oem")==0)
											{
												// if it exists, check OEM string.

											// for OEM partner check on license key, need oem=xxxr in the params'
				//oem=xxxr where xxx is a client code string, and r is any random ASCII character in the range 33 to 126, excluding URL-sensitive characters (34, 38, 39, 46, 47, 60, 61, 62, 63).
				//the oem parameter should be the last thing in the string, and the last random character allows us to keep the encoding more secure.
				//the client codes for OEM suppliers is as follows (note these ARE case sensitive):

				//Strategy and Technology (S&T): SnT
				//Harris: HAS
				//Softel: SFT
				//Chyron: Chy
				//Ensequence: Ens
				//VDS: VDS (for test purposes)

				//If an OEM code is specified in the license key, a supplier code table including these values must exist in the CSF file, for it to match on. 
				//This will be stored in a CSF parameter under the [License] heading, called COM for Cortex OEM Management (haha).

				//[License]
				//COM=C1JL-sPLisQ5VCQqCR2LJwJPdlTK

				//the above string includes all of the above client codes.  The applications will do a check on the license key against the allowed suppliers in this code, which by default will be included and created by the applications.  As the approved vendor list changes, new versions will create a new default code, but for interim installations, the code can be updated in the CSF file.

				// AND the way the string is generated is as follows:
				// take this string:
				//							VDSHAS|ChySnT|SFTEns
				// which is delimited pairs of 3 letter codes (do in pairs so we dont have delimited 4 char, or naked 3 char regular)
				//							base 64 endcode using the license key alpha and padch
				//#define LICENSE_B64ALPHA		"XhIWsP4RtOoSTQpuvJ-LBCnGrAyd8eVi7%Dz1wkl5xMj6NqZa2YHbc9f0+Eg3UFm"
				//#define LICENSE_B64PADCH		'K'
				// and there you go.

												if((m_pszOEMcodes)&&(strlen(m_pszOEMcodes)>3))
												{
													CBufferUtil bu;
													char* pszCodes = m_pszOEMcodes;
													unsigned long ulBufLen = strlen(m_pszOEMcodes);
													bu.Base64Decode(&pszCodes, &ulBufLen, false, LICENSE_B64ALPHA, LICENSE_B64PADCH);

													CSafeBufferUtil sbu;
													char* pchCodes = sbu.Token(pszCodes, strlen(pszCodes), "|");
													g_phelios->m_data.m_key.m_bValid = false;
													while(pchCodes)
													{
														if(strncmp(g_phelios->m_data.m_key.m_ppszValues[i], pchCodes, 3)==0)
														{
															g_phelios->m_data.m_key.m_bValid = true;
															break;
														}
														else
														if((strlen(pchCodes)>3)&&(strncmp(g_phelios->m_data.m_key.m_ppszValues[i], pchCodes+3, 3)==0))
														{
															g_phelios->m_data.m_key.m_bValid = true;
															break;
														}
														pchCodes = sbu.Token(NULL, NULL, "|");
													}
													if(pszCodes)
													{
														try{free(pszCodes);} catch(...){}
													}
												}
												else g_phelios->m_data.m_key.m_bValid = false;

											}
										}
										i++;
									}
								
									if(
											(
												(!g_phelios->m_data.m_key.m_bExpires)
											||((g_phelios->m_data.m_key.m_bExpires)&&(!g_phelios->m_data.m_key.m_bExpired))
											||((g_phelios->m_data.m_key.m_bExpires)&&(g_phelios->m_data.m_key.m_bExpireForgiveness)&&(g_phelios->m_data.m_key.m_ulExpiryDate+g_phelios->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
											)
										&&(
												(!g_phelios->m_data.m_key.m_bMachineSpecific)
											||((g_phelios->m_data.m_key.m_bMachineSpecific)&&(g_phelios->m_data.m_key.m_bValidMAC))
											)
										)
									{
					// this overrides stuff so let's not let it do that.

										// let's just not override this in a green condition.  
										// comment it out.
//										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//										g_phelios->m_data.SetStatusText(errorstring, HELIOS_STATUS_OK);
									}
									else
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
										g_phelios->m_data.SetStatusText(errorstring, HELIOS_STATUS_ERROR);
									}
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
									g_phelios->m_data.SetStatusText(errorstring, HELIOS_STATUS_ERROR);
								}

							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("CommandServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usCommandPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("StatusServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usStatusPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Messager")==0)
				{
					if(szParameter.CompareNoCase("UseEmail")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseEmail = true;
							else m_bUseEmail = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseNet")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseNetwork = true;
							else m_bUseNetwork = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseLog = true;
							else m_bUseLog = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseXMLClientLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseXMLClientLog = true;
							else m_bUseXMLClientLog = false;
						}
					}
					else
					if(szParameter.CompareNoCase("SendMsg")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bSendMsg = true;
							else m_bSendMsg = false;
						}
					}



				}
				else
				if(szCategory.CompareNoCase("OmnibusXML")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("DataFields")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszOmnibusDataFields) free(m_pszOmnibusDataFields);
								m_pszOmnibusDataFields = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ConnectionTimeout")==0)
					{
						if(nLength>0)
						{
							m_ulConnTimeout = atol(szValue);
							int i=0;
	EnterCriticalSection (&g_omni.m_crit);
							while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
							{
								if(g_omni.m_ppConn[i]) g_omni.m_ppConn[i]->m_ulConnTimeout = m_ulConnTimeout;
								i++;
							}
	LeaveCriticalSection (&g_omni.m_crit);
						}
					}
					else
					if(szParameter.CompareNoCase("ConnectionRetries")==0)
					{
						if(nLength>0)
						{
							m_ulConnectionRetries = atol(szValue);
							int i=0;
	EnterCriticalSection (&g_omni.m_crit);
							while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
							{
								if(g_omni.m_ppConn[i]) g_omni.m_ppConn[i]->m_ulConnectionRetries = m_ulConnectionRetries;
								i++;
							}
	LeaveCriticalSection (&g_omni.m_crit);
						}
					}
					else
					if(szParameter.CompareNoCase("ConnectionInterval")==0)
					{
						if(nLength>0)
						{
							m_ulConnectionInterval = atol(szValue);
							int i=0;
	EnterCriticalSection (&g_omni.m_crit);
							while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
							{
								if(g_omni.m_ppConn[i]) g_omni.m_ppConn[i]->m_ulConnectionInterval = m_ulConnectionInterval;
								i++;
							}
	LeaveCriticalSection (&g_omni.m_crit);
						}
					}
					else
					if(szParameter.CompareNoCase("DebugLog")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszDebugLog) free(m_pszDebugLog);
								m_pszDebugLog = pch;

								int i=0;
	EnterCriticalSection (&g_omni.m_crit);
								while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
								{
									if(g_omni.m_ppConn[i]) g_omni.m_ppConn[i]->SetDebugName(m_pszDebugLog);
									i++;
								}
	LeaveCriticalSection (&g_omni.m_crit);
							}
						}
						else
						{
							if(m_pszDebugLog) free(m_pszDebugLog);
							m_pszDebugLog =NULL;
							int i=0;
	EnterCriticalSection (&g_omni.m_crit);
							while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
							{
								if(g_omni.m_ppConn[i])
								{
									if(g_omni.m_ppConn[i]->m_pszDebugFile) free(g_omni.m_ppConn[i]->m_pszDebugFile);
									g_omni.m_ppConn[i]->m_pszDebugFile =NULL;
								}
								i++;
							}
	LeaveCriticalSection (&g_omni.m_crit);

						}
					}
					else
					if(szParameter.CompareNoCase("CommLog")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszCommLog) free(m_pszCommLog);
								m_pszCommLog = pch;

								int i=0;
	EnterCriticalSection (&g_omni.m_crit);
								while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
								{
									if(g_omni.m_ppConn[i]) g_omni.m_ppConn[i]->SetCommLogName(m_pszCommLog);
									i++;
								}
	LeaveCriticalSection (&g_omni.m_crit);
							}
						}
						else
						{
							if(m_pszCommLog) free(m_pszCommLog);
							m_pszCommLog =NULL;
							int i=0;
	EnterCriticalSection (&g_omni.m_crit);
							while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
							{
								if(g_omni.m_ppConn[i])
								{
									if(g_omni.m_ppConn[i]->m_pszCommFile) free(g_omni.m_ppConn[i]->m_pszCommFile);
									g_omni.m_ppConn[i]->m_pszCommFile =NULL;
								}
								i++;
							}
	LeaveCriticalSection (&g_omni.m_crit);
						}
					}
					else
					if(szParameter.CompareNoCase("WriteXML")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bWriteXML = true;
							else m_bWriteXML = false;

							int i=0;
	EnterCriticalSection (&g_omni.m_crit);
							while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
							{
								if(g_omni.m_ppConn[i]) g_omni.m_ppConn[i]->m_bWriteXMLtoFile = m_bWriteXML;
								i++;
							}
	LeaveCriticalSection (&g_omni.m_crit);
						}
					}
					else
					if(szParameter.CompareNoCase("PurgeCheckInterval")==0)
					{
						if(nLength>0)
						{
							m_ulCheckInterval = atol(szValue);
							int i=0;
	EnterCriticalSection (&g_omni.m_crit);
							while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
							{
								if(g_omni.m_ppConn[i]) g_omni.m_ppConn[i]->m_ulCheckInterval = m_ulCheckInterval;
								i++;
							}
	LeaveCriticalSection (&g_omni.m_crit);
						}
					}
					else
					if(szParameter.CompareNoCase("PurgeExpiryPeriod")==0)
					{
						if(nLength>0)
						{
							m_ulExpiryPeriod = atol(szValue);
							int i=0;
	EnterCriticalSection (&g_omni.m_crit);
							while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
							{
								if(g_omni.m_ppConn[i]) g_omni.m_ppConn[i]->m_ulExpiryPeriod = m_ulExpiryPeriod;
								i++;
							}
	LeaveCriticalSection (&g_omni.m_crit);
						}
					}
				}
				else 
				if(szCategory.CompareNoCase("Database")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("SettingsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszSettings) free(m_pszSettings);
								m_pszSettings = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ExchangeTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszExchange) free(m_pszExchange);
								m_pszExchange = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("MessagesTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszMessages) free(m_pszMessages);
								m_pszMessages = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ChannelsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszChannels) free(m_pszChannels);
								m_pszChannels = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ConnectionsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszConnections) free(m_pszConnections);
								m_pszConnections = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("LiveEventsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLiveEvents) free(m_pszLiveEvents);
								m_pszLiveEvents = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ControlModulesTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszControlModules)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszControlModules) free(m_pszControlModules);
								m_pszControlModules = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("InterpreterModulesTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszInterpreterModules)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszInterpreterModules) free(m_pszInterpreterModules);
								m_pszInterpreterModules = pch;
							}
						}
					}

					else
					if(szParameter.CompareNoCase("ModificationCheckInterval")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_ulModsIntervalMS = nLength;
						}
					}
					else
					if(szParameter.CompareNoCase("ListStatusInExchange")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bListStatusInExchange = true;
							else m_bListStatusInExchange = false;
						}
					}
					else
					if(szParameter.CompareNoCase("StatusInTables")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bStatusInTables = true;
							else m_bStatusInTables = false;
						}
					}
					else
					if(szParameter.CompareNoCase("ClearEventsOnChannelInit")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bClearEventsInit = true;
							else m_bClearEventsInit = false;
						}
					}
					else
					if(szParameter.CompareNoCase("ClearEventsOnChannelExit")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bClearEventsExit = true;
							else m_bClearEventsExit = false;
						}
					}
				}
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
					if(szParameter.CompareNoCase("Messages")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeMessageDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
				}
/*
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
					if(szParameter.CompareNoCase("AsRun_Log")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeAsRunDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
				}
*/				


				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			if(pszInfo)
			{
				_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
			}
			try
			{
				delete prs;
			}	catch(...) {}
			LeaveCriticalSection(&g_phelios->m_data.m_critSQL);

			Settings(false); //write
/*
			if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
			{
				// these explicts arent necessary - uncomment to write out a full file to edit...
				file.SetIniString("Main", "Name", g_phelios->m_settings.m_pszName);
				file.SetIniInt("CommandServer", "ListenPort", g_phelios->m_settings.m_usCommandPort);
				file.SetIniInt("StatusServer", "ListenPort", g_phelios->m_settings.m_usStatusPort);
				file.SetIniString("License", "Key", g_phelios->m_settings.m_pszLicense);

				file.SetIniString("FileServer", "IconPath", g_phelios->m_settings.m_pszIconPath);

				file.SetIniInt("Messager", "UseEmail", g_phelios->m_settings.m_bUseEmail?1:0);
				file.SetIniInt("Messager", "UseNet", g_phelios->m_settings.m_bUseNetwork?1:0);
				file.SetIniInt("Messager", "UseLog", g_phelios->m_settings.m_bUseLog?1:0);

				file.SetIniString("Database", "DSN", g_phelios->m_settings.m_pszDSN);
				file.SetIniString("Database", "DBUser", g_phelios->m_settings.m_pszUser);
				file.SetIniString("Database", "DBPassword", g_phelios->m_settings.m_pszPW);
				file.SetIniString("Database", "SettingsTableName", g_phelios->m_settings.m_pszSettings);  // the Settings table name
				file.SetIniString("Database", "ExchangeTableName", g_phelios->m_settings.m_pszExchange);  // the Exchange table name
				file.SetIniString("Database", "MessagesTableName", g_phelios->m_settings.m_pszMessages);  // the Messages table name

				file.SetIniString("OmnibusXML", "DataFields", g_phelios->m_settings.m_pszOmnibusDataFields);  // the data fields we care about
				file.SetIniString("OmnibusXML", "DebugLog", g_phelios->m_settings.m_pszDebugLog);  
				file.SetIniString("OmnibusXML", "CommLog", g_phelios->m_settings.m_pszCommLog);  
				file.SetIniInt("OmnibusXML", "WriteXML", g_phelios->m_settings.m_bWriteXML?1:0);  
				file.SetIniInt("OmnibusXML", "PurgeCheckInterval", g_phelios->m_settings.m_ulCheckInterval);	// number of seconds between checks (0 turns off)
				file.SetIniInt("OmnibusXML", "PurgeExpiryPeriod", g_phelios->m_settings.m_ulExpiryPeriod);	// (default value 3 hours) when the event's end time (start plus duration) is more than this number of seconds older than the current server time, it is purged.
				file.SetIniInt("OmnibusXML", "ConnectionTimeout", g_phelios->m_settings.m_ulConnTimeout);	// (default value 2 mins) 

				file.SetIniString("Database", "ChannelsTableName", g_phelios->m_settings.m_pszChannels);  // the Channels table name
				file.SetIniString("Database", "ConnectionsTableName", g_phelios->m_settings.m_pszConnections);  // the Connections table name
				file.SetIniString("Database", "LiveEventsTableName", g_phelios->m_settings.m_pszLiveEvents);  // the LiveEvents table name

				file.SetIniInt("Database", "ModificationCheckInterval", g_phelios->m_settings.m_ulModsIntervalMS);  // in milliseconds

				file.SetSettings(HELIOS_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

			}
*/


			return HELIOS_SUCCESS;
		}
		LeaveCriticalSection(&g_phelios->m_data.m_critSQL);

	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s%s%s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. ",
				m_pdb?"":"Database pointer was NULL. ",
				m_pdbConn?"":"Connection pointer was NULL. ",
				((m_pdbConn)&&(!m_pdbConn->m_bConnected))?"Database not connected. ":""
				);
		}
	}
	return HELIOS_ERROR;
}



char* CHeliosSettings::ProcessString(char* pszString, bool bFreeIncomingString)
{
	if(pszString)
	{
		char pszOutput[4096];
		int nInLen=strlen(pszString);
		int nOutLen=0;
		char* pch = pszString;
		char* pchEnd = min((pch+4096),(pszString+nInLen));
		while((*pch!=0)&&(pch<pchEnd))
		{
			if(*pch == '%')
			{
				// we are in a specifier.
				// here is a list of ours:
				// %h  local hostname
		
				pch++;
				if((*pch!=0)&&(pch<pchEnd))
				{
					if(*pch == 'h')
					{
						if((g_phelios->m_data.m_pszHost)&&(strlen(g_phelios->m_data.m_pszHost)))
						{
							char* pszTemp = (char*)malloc(strlen(g_phelios->m_data.m_pszHost)+1);
							if(pszTemp)
							{
								strcpy(pszTemp, g_phelios->m_data.m_pszHost);
							
								char* pchPlus = pszTemp;
								while((*pchPlus != 0)&&(nOutLen<4096))
								{
									pszOutput[nOutLen]= *pchPlus;
									nOutLen++;
									pchPlus++;
								}
								free(pszTemp);
							}
						}
						pch++;
					}
					else
					{
						// some other specifier, can be anything.
						pszOutput[nOutLen]= *(pch-1); // do add the %, this may get further processed
						nOutLen++;
						pszOutput[nOutLen]= *pch;
						nOutLen++;
						pch++;
					}
				}
				else
				{
					pszOutput[nOutLen]= *(pch-1);
					nOutLen++;
				}
				
			}
			else
			{
				pszOutput[nOutLen]= *pch;
				nOutLen++;
				pch++;
			}
		}

		pszOutput[nOutLen]=0;

		pch = (char*)malloc(strlen(pszOutput)+1);
		if(pch)
		{
			strcpy(pch,pszOutput);
			if(bFreeIncomingString) 
			{
				try {free(pszString);} catch(...){}
			}
		}
		return pch;
	}
	return NULL;
}


