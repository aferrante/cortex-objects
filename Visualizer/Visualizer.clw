; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CVisualizerDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Visualizer.h"
LastPage=0

ClassCount=8
Class1=CAboutDlg
Class2=CVisualizerApp
Class3=CVisualizerHandler
Class4=CVisualizerDlg
Class5=CVisualizerSettings

ResourceCount=5
Resource1=IDD_LICENSE_DIALOG
Resource2=IDD_ABOUTBOX
Resource3=IDD_VISUALIZER_DIALOG
Class6=CVisualizerSettingsDlg
Resource4=IDD_SETTINGS_DIALOG
Class7=CVisualizerLicenseDlg
Class8=CVisualizerKeySetupDlg
Resource5=IDR_MENU1

[CLS:CAboutDlg]
Type=0
HeaderFile=visualizerdlg.h
ImplementationFile=visualizerdlg.cpp
BaseClass=CDialog
LastObject=CAboutDlg

[CLS:CVisualizerApp]
Type=0
BaseClass=CWinApp
HeaderFile=Visualizer.h
ImplementationFile=Visualizer.cpp
Filter=N
VirtualFilter=AC
LastObject=CVisualizerApp

[CLS:CVisualizerHandler]
Type=0
BaseClass=CWnd
HeaderFile=VisualizerHandler.h
ImplementationFile=VisualizerHandler.cpp
Filter=W
VirtualFilter=WC
LastObject=CVisualizerHandler

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=8
Control1=IDC_STATICTEXT_TITLE,static,1342308481
Control2=IDC_STATICTEXT_URL,static,1208090625
Control3=IDOK,button,1342373889
Control4=IDC_STATIC_LOGO,static,1342177294
Control5=IDC_URLFRAME,static,1342177298
Control6=IDC_STATIC_BUILD,static,1342308353
Control7=IDC_STATIC_URL,static,1342177294
Control8=IDC_STATICTEXT_COPYRIGHT,static,1342308353

[MNU:IDR_MENU1]
Type=1
Class=?
Command1=ID_CMD_ABOUT
Command2=ID_CMD_SHOWWND
Command3=ID_CMD_EXIT
Command4=ID_CMD_ABOUT
Command5=ID_CMD_SHOWWND
Command6=ID_CMD_EXIT
CommandCount=6

[CLS:CVisualizerDlg]
Type=0
HeaderFile=VisualizerDlg.h
ImplementationFile=VisualizerDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CVisualizerDlg

[CLS:CVisualizerSettings]
Type=0
HeaderFile=VisualizerSettings.h
ImplementationFile=VisualizerSettings.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CVisualizerSettings

[DLG:IDD_VISUALIZER_DIALOG]
Type=1
Class=CVisualizerDlg
ControlCount=9
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_SETTINGS,button,1207959680
Control4=IDC_LIST1,SysListView32,1350681613
Control5=IDC_STATIC_STATUSTEXT,static,1342308352
Control6=IDC_STATIC_PROGBAR,static,1342177294
Control7=IDC_TREE1,SysTreeView32,1082130487
Control8=IDC_CHECK_PUSHPIN,button,1342247043
Control9=IDC_CHECK_KEEPFOCUS,button,1208029315

[DLG:IDD_SETTINGS_DIALOG]
Type=1
Class=CVisualizerSettingsDlg
ControlCount=17
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308354
Control4=IDC_STATIC,static,1342308354
Control5=IDC_LIST1,SysListView32,1350631429
Control6=IDC_BUTTON_ADD,button,1342242816
Control7=IDC_BUTTON_EDIT,button,1342242816
Control8=IDC_BUTTON_DELETE,button,1342242816
Control9=IDC_EDIT_PLUGIN,edit,1350631552
Control10=IDC_EDIT_TOKEN,edit,1350631552
Control11=IDC_STATIC,static,1342308352
Control12=IDC_CHECK_SUSPEND,button,1342251011
Control13=IDC_STATIC,static,1342308354
Control14=IDC_EDIT_IP,edit,1350631552
Control15=IDC_STATIC,static,1342308354
Control16=IDC_EDIT_PORT,edit,1350631552
Control17=IDC_CHECK_ASRUN,button,1342242819

[CLS:CVisualizerSettingsDlg]
Type=0
HeaderFile=VisualizerSettingsDlg.h
ImplementationFile=VisualizerSettingsDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CVisualizerSettingsDlg

[DLG:IDD_LICENSE_DIALOG]
Type=1
Class=CVisualizerLicenseDlg
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1073807360
Control3=IDC_EDIT1,edit,1350631552

[CLS:CVisualizerLicenseDlg]
Type=0
HeaderFile=VisualizerLicenseDlg.h
ImplementationFile=VisualizerLicenseDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDOK

[CLS:CVisualizerKeySetupDlg]
Type=0
HeaderFile=VisualizerKeySetupDlg.h
ImplementationFile=VisualizerKeySetupDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CVisualizerKeySetupDlg

