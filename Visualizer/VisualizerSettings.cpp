// VisualizerSettings.cpp: implementation of the CVisualizerSettings.
//
//////////////////////////////////////////////////////////////////////
#include <stdafx.h>  // for visualizer debug
#include "VisualizerMain.h"   // for visualizer debug
#include "VisualizerDefines.h"
#include "VisualizerSettings.h"
#include "..\Tabulator\TabulatorDefines.h"
//#include "..\Radiance\RadianceDefines.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CVisualizerMain* g_pvisualizer;
extern CVisualizerApp theApp;



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVisualizerSettings::CVisualizerSettings()
{
	m_pszName = NULL;
	m_ulMainMode = VISUALIZER_MODE_DEFAULT;

	m_ulDebug = 0;
	// ports
	m_usCommandPort	= VISUALIZER_PORT_CMD;
	m_usStatusPort	= VISUALIZER_PORT_STATUS;

//	m_pszTabulatorHost = NULL;	// the name of the cortex host
//	m_usTabulatorCommandPort = TABULATOR_PORT_CMD;
//	m_pszPluginName = NULL;
//	m_pszUserToken = NULL;
//	m_pszKeySetupHelp = NULL;

	m_nAsRunRecent = 10;
//	m_nCommRetryMS = 5000;


	// messaging for Visualizer
	m_bUseLog = true;			// write a log file
	m_bUseAsRunLog = true;
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host
	m_pszFileSpec = NULL;
	m_pszMailSpec = NULL;
	m_pszProcessedFileSpec = NULL;
	m_pszProcessedMailSpec = NULL;
	m_pszAsRunFileSpec = NULL;
	m_pszProcessedAsRunFileSpec = NULL;

//	m_bUseXMLClientLog = false;

	m_bReportSuccessfulOperation=false;

//	m_nTransferFailureRetryTime = 60;  // seconds - one minute as a default
	m_pszLicense=NULL;  // the License Key
	m_pszOEMcodes=NULL;  // the possible OEM string
	m_pszIconPath=NULL;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.
}

CVisualizerSettings::~CVisualizerSettings()
{

	if(m_pszName) free(m_pszName); // must use malloc to allocate

//	if(m_pszTabulatorHost) free(m_pszTabulatorHost); // must use malloc to allocate
//	if(m_pszPluginName) free(m_pszPluginName); // must use malloc to allocate
//	if(m_pszUserToken) free(m_pszUserToken); // must use malloc to allocate
//	if(m_pszKeySetupHelp) free(m_pszKeySetupHelp); // must use malloc to allocate

	
	if(m_pszLicense) free(m_pszLicense); // must use malloc to allocate
	if(m_pszOEMcodes) free(m_pszOEMcodes); // must use malloc to allocate
	if(m_pszIconPath) free(m_pszIconPath); // must use malloc to allocate	
	if(m_pszFileSpec) free(m_pszFileSpec); // must use malloc to allocate	
	if(m_pszMailSpec) free(m_pszMailSpec); // must use malloc to allocate	
	if(m_pszProcessedFileSpec) free(m_pszProcessedFileSpec); // must use malloc to allocate	
	if(m_pszProcessedMailSpec) free(m_pszProcessedMailSpec); // must use malloc to allocate	

}
/*
int CVisualizerSettings::ReadKeys()
{
	char pszFilename[MAX_PATH];
	CFileUtil file;
	// get settings.
	strcpy(pszFilename, "");
//		AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, VISUALIZER_SETTINGS_FILE_DEFAULT);  // cortex settings file

	file.GetSettings(pszFilename, false); 

if(0)//g_pvisualizer->m_settings.m_ulDebug&VISUALIZER_DEBUG_TRIGGER) 	
{
	g_pvisualizer->m_msgr.DM(MSG_ICONINFO, NULL, "Visualizer", "ReadKeys() %d %d", (file.m_ulStatus&FILEUTIL_MALLOC_OK), g_pvisualizer);  //(Dispatch message)
}

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(g_pvisualizer)
		{
			EnterCriticalSection(&g_pvisualizer->m_data.m_critMaps);

			if(g_pvisualizer->m_data.m_ppMappingObj)
			{
				int i=0;
				while(i<g_pvisualizer->m_data.m_nNumMappingObjects)
				{
					if(g_pvisualizer->m_data.m_ppMappingObj[i]) delete g_pvisualizer->m_data.m_ppMappingObj[i]; // delete objects, must use new to allocate
					i++;
				}
				delete [] g_pvisualizer->m_data.m_ppMappingObj; // delete array of pointers to objects, must use new to allocate
			}
			g_pvisualizer->m_data.m_ppMappingObj = NULL;
			g_pvisualizer->m_data.m_nNumMappingObjects = file.GetIniInt("Keys", "NumKeys", 0);


if(g_pvisualizer->m_settings.m_ulDebug&VISUALIZER_DEBUG_TRIGGER) 	
{
	g_pvisualizer->m_msgr.DM(MSG_ICONINFO, NULL, "Visualizer", "reading %d keys", g_pvisualizer->m_data.m_nNumMappingObjects);  //(Dispatch message)
}


			if(g_pvisualizer->m_data.m_nNumMappingObjects>0)
			{
				g_pvisualizer->m_data.m_ppMappingObj = new CVisualizerMappingObject*[g_pvisualizer->m_data.m_nNumMappingObjects];

				if(g_pvisualizer->m_data.m_ppMappingObj)
				{
					int i=0;
					while(i<g_pvisualizer->m_data.m_nNumMappingObjects)
					{
						g_pvisualizer->m_data.m_ppMappingObj[i] = new CVisualizerMappingObject;
						if(g_pvisualizer->m_data.m_ppMappingObj[i])
						{
							char szText[256];
							sprintf(szText, "Key_%03d_active", i);
							g_pvisualizer->m_data.m_ppMappingObj[i]->m_bActive = file.GetIniInt("Keys", szText, 1)?true:false;
							sprintf(szText, "Key_%03d_appfocus", i);
							g_pvisualizer->m_data.m_ppMappingObj[i]->m_bRequireAppFocus = file.GetIniInt("Keys", szText, 1)?true:false;
							sprintf(szText, "Key_%03d_code", i);
							g_pvisualizer->m_data.m_ppMappingObj[i]->m_nKeyCode = file.GetIniInt("Keys", szText, -1);
							sprintf(szText, "Key_%03d_syschars", i);
							g_pvisualizer->m_data.m_ppMappingObj[i]->m_nSysChars = file.GetIniInt("Keys", szText, -1);

if(0)//g_pvisualizer->m_settings.m_ulDebug&VISUALIZER_DEBUG_TRIGGER) 	
{
	g_pvisualizer->m_msgr.DM(MSG_ICONINFO, NULL, "Visualizer", "read key %d", i);  //(Dispatch message)
}

							if(!g_pvisualizer->m_data.m_ppMappingObj[i]->m_bRequireAppFocus)
							{	
								if(RegisterHotKey(NULL, i, g_pvisualizer->m_data.m_ppMappingObj[i]->m_nSysChars, g_pvisualizer->m_data.m_ppMappingObj[i]->m_nKeyCode)==0)
								{	
									g_pvisualizer->m_data.m_ppMappingObj[i]->m_bFailed=true; 
	g_pvisualizer->m_msgr.DM(MSG_ICONERROR, NULL, "Visualizer", "Failure to register hotkey: identifier %d, code 0x%02x modifiers 0x%02x.", i, g_pvisualizer->m_data.m_ppMappingObj[i]->m_nKeyCode, g_pvisualizer->m_data.m_ppMappingObj[i]->m_nSysChars);  //(Dispatch message)
								}
								else
								{
if(g_pvisualizer->m_settings.m_ulDebug&VISUALIZER_DEBUG_TRIGGER) 	
{
	g_pvisualizer->m_msgr.DM(MSG_ICONINFO, NULL, "Visualizer", "Registered hotkey: identifier %d, code 0x%02x modifiers 0x%02x.", i, g_pvisualizer->m_data.m_ppMappingObj[i]->m_nKeyCode, g_pvisualizer->m_data.m_ppMappingObj[i]->m_nSysChars);  //(Dispatch message)
}

									g_pvisualizer->m_data.m_ppMappingObj[i]->m_bFailed=false; 
								}
							}
						}
						i++;
					}

				}
			}


			LeaveCriticalSection(&g_pvisualizer->m_data.m_critMaps);
		}


	
		return VISUALIZER_SUCCESS;
	}
	return VISUALIZER_ERROR;
}
*/

int CVisualizerSettings::Settings(bool bRead)
{
	char pszFilename[MAX_PATH];
	CFileUtil file;
	// get settings.
	strcpy(pszFilename, "");
//		AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, VISUALIZER_SETTINGS_FILE_DEFAULT);  // cortex settings file

	file.GetSettings(pszFilename, false); 

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
		// load up the values on the settings object
			m_pszName = file.GetIniString("Main", "Name", "Visualizer", m_pszName);
			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);
			m_pszOEMcodes = file.GetIniString("License", "COM", "C1JL-sPLisQ5VCQqCR2LJwJPdlTK", m_pszOEMcodes);
			m_ulDebug = file.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.

			// compile license key params
			if(g_pvisualizer->m_data.m_key.m_pszLicenseString) free(g_pvisualizer->m_data.m_key.m_pszLicenseString);
			g_pvisualizer->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(m_pszLicense)+1);
			if(g_pvisualizer->m_data.m_key.m_pszLicenseString)
			sprintf(g_pvisualizer->m_data.m_key.m_pszLicenseString, "%s", m_pszLicense);

			g_pvisualizer->m_data.m_key.InterpretKey();

			char errorstring[MAX_MESSAGE_LENGTH];
			if(g_pvisualizer->m_data.m_key.m_bValid)
			{
				unsigned long i=0;
				while(i<g_pvisualizer->m_data.m_key.m_ulNumParams)
				{
					if((g_pvisualizer->m_data.m_key.m_ppszParams)
						&&(g_pvisualizer->m_data.m_key.m_ppszValues)
						&&(g_pvisualizer->m_data.m_key.m_ppszParams[i])
						&&(g_pvisualizer->m_data.m_key.m_ppszValues[i]))
					{
						if(stricmp(g_pvisualizer->m_data.m_key.m_ppszParams[i], "max")==0)
						{
	//						g_pvisualizer->m_data.m_nMaxLicensedDevices = atoi(g_pvisualizer->m_data.m_key.m_ppszValues[i]);
						}
						else
						if(stricmp(g_pvisualizer->m_data.m_key.m_ppszParams[i], "oem")==0)
						{
							// if it exists, check OEM string.

						// for OEM partner check on license key, need oem=xxxr in the params'
//oem=xxxr where xxx is a client code string, and r is any random ASCII character in the range 33 to 126, excluding URL-sensitive characters (34, 38, 39, 46, 47, 60, 61, 62, 63).
//the oem parameter should be the last thing in the string, and the last random character allows us to keep the encoding more secure.
//the client codes for OEM suppliers is as follows (note these ARE case sensitive):

//Strategy and Technology (S&T): SnT
//Harris: HAS
//Softel: SFT
//Chyron: Chy
//Ensequence: Ens
//VDS: VDS (for test purposes)

//If an OEM code is specified in the license key, a supplier code table including these values must exist in the CSF file, for it to match on. 
//This will be stored in a CSF parameter under the [License] heading, called COM for Cortex OEM Management (haha).

//[License]
//COM=C1JL-sPLisQ5VCQqCR2LJwJPdlTK

//the above string includes all of the above client codes.  The applications will do a check on the license key against the allowed suppliers in this code, which by default will be included and created by the applications.  As the approved vendor list changes, new versions will create a new default code, but for interim installations, the code can be updated in the CSF file.

// AND the way the string is generated is as follows:
// take this string:
//							VDSHAS|ChySnT|SFTEns
// which is delimited pairs of 3 letter codes (do in pairs so we dont have delimited 4 char, or naked 3 char regular)
//							base 64 endcode using the license key alpha and padch
//#define LICENSE_B64ALPHA		"XhIWsP4RtOoSTQpuvJ-LBCnGrAyd8eVi7%Dz1wkl5xMj6NqZa2YHbc9f0+Eg3UFm"
//#define LICENSE_B64PADCH		'K'
// and there you go.

							if((m_pszOEMcodes)&&(strlen(m_pszOEMcodes)>3))
							{
								CBufferUtil bu;
								char* pszCodes = m_pszOEMcodes;
								unsigned long ulBufLen = strlen(m_pszOEMcodes);
								bu.Base64Decode(&pszCodes, &ulBufLen, false, LICENSE_B64ALPHA, LICENSE_B64PADCH);

								CSafeBufferUtil sbu;
								char* pchCodes = sbu.Token(pszCodes, strlen(pszCodes), "|");
								g_pvisualizer->m_data.m_key.m_bValid = false;
								while(pchCodes)
								{
									if(strncmp(g_pvisualizer->m_data.m_key.m_ppszValues[i], pchCodes, 3)==0)
									{
										g_pvisualizer->m_data.m_key.m_bValid = true;
										break;
									}
									else
									if((strlen(pchCodes)>3)&&(strncmp(g_pvisualizer->m_data.m_key.m_ppszValues[i], pchCodes+3, 3)==0))
									{
										g_pvisualizer->m_data.m_key.m_bValid = true;
										break;
									}
									pchCodes = sbu.Token(NULL, NULL, "|");
								}
								if(pszCodes)
								{
									try{free(pszCodes);} catch(...){}
								}
							}
							else g_pvisualizer->m_data.m_key.m_bValid = false;
						}
					}
					i++;
				}

				if(
					  (g_pvisualizer->m_data.m_key.m_bValid)
					&&(
							(!g_pvisualizer->m_data.m_key.m_bExpires)
						||((g_pvisualizer->m_data.m_key.m_bExpires)&&(!g_pvisualizer->m_data.m_key.m_bExpired))
						||((g_pvisualizer->m_data.m_key.m_bExpires)&&(g_pvisualizer->m_data.m_key.m_bExpireForgiveness)&&(g_pvisualizer->m_data.m_key.m_ulExpiryDate+g_pvisualizer->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!g_pvisualizer->m_data.m_key.m_bMachineSpecific)
						||((g_pvisualizer->m_data.m_key.m_bMachineSpecific)&&(g_pvisualizer->m_data.m_key.m_bValidMAC))
						)
					)
				{
//					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//					g_pvisualizer->m_data.SetStatusText(errorstring, VISUALIZER_STATUS_OK);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
					g_pvisualizer->m_data.SetStatusText(errorstring, VISUALIZER_STATUS_ERROR);
				}
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				g_pvisualizer->m_data.SetStatusText(errorstring, VISUALIZER_STATUS_ERROR);
			}


			m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", VISUALIZER_PORT_CMD);
			m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", VISUALIZER_PORT_STATUS);

			m_pszIconPath = file.GetIniString("FileServer", "IconPath", "C:\\Inetpub\\wwwroot\\Cortex\\visualizer\\images\\", m_pszIconPath);
//			m_bUseXMLClientLog = file.GetIniInt("Messager", "UseXMLClientLog", 0)?true:false;

			m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			m_bUseAsRunLog = file.GetIniInt("Messager", "UseAsRunLog", 1)?true:false;
			m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
//			m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;

			m_nAsRunRecent = file.GetIniInt("Messager", "RecentAsRunCount", 10);


//			m_pszKeySetupHelp = file.GetIniString("Extra", "KeySetupHelp", "some help text", m_pszKeySetupHelp);


//			m_pszTabulatorHost = file.GetIniString("Transmission", "TabulatorHost", "N\\A", m_pszTabulatorHost);	// the name of the cortex host
//			m_usTabulatorCommandPort = file.GetIniInt("Transmission", "TabulatorCommandPort", TABULATOR_PORT_CMD);
//			m_pszPluginName = file.GetIniString("Transmission", "TabulatorPluginName", "Collector", m_pszPluginName);	// the name of the cortex host
//			m_pszUserToken = file.GetIniString("Transmission", "UserToken", "", m_pszUserToken);	// the name of the cortex host
//			m_nCommRetryMS = file.GetIniInt("Transmission", "CommRetryMS", 5000);


//	// file base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
			m_pszAsRunFileSpec =  file.GetIniString("Messager", "AsRunLogFileIni", "Logs\\VisualizerAsRun|YD||1|1|", m_pszAsRunFileSpec); // allow repeats!

			m_pszFileSpec = file.GetIniString("Messager", "LogFileIni", "Logs\\Visualizer|YD||1|", m_pszFileSpec);
				// SMTP address[: override port (optional)] | distribution lists | from email |  subject spec | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
			m_pszMailSpec = file.GetIniString("Messager", "SMTPIni", "mail.server.com|f3:recipient@address.com|\"someone\"<from@here.com>|%P %T from %h|%h|C:\\visualizersmtp.txt|1|1|0", m_pszMailSpec);
			if(m_pszFileSpec) 
			{
				if(m_pszProcessedFileSpec)
				{
					try{ free(m_pszProcessedFileSpec); } catch(...) {}
				}
				m_pszProcessedFileSpec = ProcessString(m_pszFileSpec, false);
			}
			
			if(m_pszMailSpec) 
			{
				if(m_pszProcessedMailSpec)
				{
					try{ free(m_pszProcessedMailSpec); } catch(...) {}
				}
				m_pszProcessedMailSpec = ProcessString(m_pszMailSpec, false);
			}

			if(m_pszAsRunFileSpec) 
			{
				if(m_pszProcessedAsRunFileSpec)
				{
					try{ free(m_pszProcessedAsRunFileSpec); } catch(...) {}
				}
				m_pszProcessedAsRunFileSpec = ProcessString(m_pszAsRunFileSpec, false);
			}

		}
		else //write
		{

			file.SetIniString("Main", "Name", m_pszName);
//			file.SetIniInt("CommandServer", "ListenPort", m_usCommandPort);
//			file.SetIniInt("StatusServer", "ListenPort", m_usStatusPort);
			file.SetIniString("License", "Key", m_pszLicense);

			file.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.

			file.SetIniString("FileServer", "IconPath", m_pszIconPath);

			file.SetIniInt("Messager", "UseEmail", m_bUseEmail?1:0);
			file.SetIniInt("Messager", "UseNet", m_bUseNetwork?1:0);
			file.SetIniInt("Messager", "UseLog", m_bUseLog?1:0);
			file.SetIniInt("Messager", "UseAsRunLog", m_bUseAsRunLog?1:0);
			file.SetIniInt("Messager", "ReportSuccessfulOperation", m_bReportSuccessfulOperation?1:0);
//			file.SetIniInt("Messager", "UseXMLClientLog", m_bUseXMLClientLog?true:false);


	//		file.SetIniString("Transmission", "TabulatorHost", m_pszTabulatorHost);	// the name of the cortex host
	//		file.SetIniInt("Transmission", "TabulatorCommandPort", m_usTabulatorCommandPort);
	//		file.SetIniString("Transmission", "TabulatorPluginName", m_pszPluginName);	
	//		file.SetIniString("Transmission", "UserToken", m_pszUserToken);
	//		file.SetIniInt("Transmission", "CommRetryMS", m_nCommRetryMS);


	//		file.SetIniString("Extra", "KeySetupHelp", m_pszKeySetupHelp);


			file.SetIniString("Messager", "LogFileIni", m_pszFileSpec);
			file.SetIniString("Messager", "SMTPIni", m_pszMailSpec);
			file.SetIniString("Messager", "AsRunLogFileIni", m_pszAsRunFileSpec);




			file.SetIniInt("Messager", "RecentAsRunCount", m_nAsRunRecent);


/*
			if(g_pvisualizer)
			{
				EnterCriticalSection(&g_pvisualizer->m_data.m_critMaps);
				file.SetIniInt("Keys", "NumKeys", g_pvisualizer->m_data.m_nNumMappingObjects);

				int i=0;
				while(i<g_pvisualizer->m_data.m_nNumMappingObjects)
				{
					if((g_pvisualizer->m_data.m_ppMappingObj)&&(g_pvisualizer->m_data.m_ppMappingObj[i]))
					{
						char szText[256];
						sprintf(szText, "Key_%03d_active", i);
						file.SetIniInt("Keys", szText, g_pvisualizer->m_data.m_ppMappingObj[i]->m_bActive?1:0);
						sprintf(szText, "Key_%03d_appfocus", i);
						file.SetIniInt("Keys", szText, g_pvisualizer->m_data.m_ppMappingObj[i]->m_bRequireAppFocus?1:0);
						sprintf(szText, "Key_%03d_code", i);
						file.SetIniInt("Keys", szText, g_pvisualizer->m_data.m_ppMappingObj[i]->m_nKeyCode);
						sprintf(szText, "Key_%03d_syschars", i);
						file.SetIniInt("Keys", szText, g_pvisualizer->m_data.m_ppMappingObj[i]->m_nSysChars);
					}
					i++;
				}

				LeaveCriticalSection(&g_pvisualizer->m_data.m_critMaps);
			}
*/

			file.SetSettings(pszFilename, false);  // have to have correct filename

		}
		return VISUALIZER_SUCCESS;
	}
	return VISUALIZER_ERROR;
}


char* CVisualizerSettings::ProcessString(char* pszString, bool bFreeIncomingString)
{
	if(pszString)
	{
		char pszOutput[4096];
		int nInLen=strlen(pszString);
		int nOutLen=0;
		char* pch = pszString;
		char* pchEnd = min((pch+4096),(pszString+nInLen));
		while((*pch!=0)&&(pch<pchEnd))
		{
			if(*pch == '%')
			{
				// we are in a specifier.
				// here is a list of ours:
				// %h  local hostname
		
				pch++;
				if((*pch!=0)&&(pch<pchEnd))
				{
					if(*pch == 'h')
					{
						if((g_pvisualizer->m_data.m_pszHost)&&(strlen(g_pvisualizer->m_data.m_pszHost)))
						{
							char* pszTemp = (char*)malloc(strlen(g_pvisualizer->m_data.m_pszHost)+1);
							if(pszTemp)
							{
								strcpy(pszTemp, g_pvisualizer->m_data.m_pszHost);
							
								char* pchPlus = pszTemp;
								while((*pchPlus != 0)&&(nOutLen<4096))
								{
									pszOutput[nOutLen]= *pchPlus;
									nOutLen++;
									pchPlus++;
								}
								free(pszTemp);
							}
						}
						pch++;
					}
					else
					{
						// some other specifier, can be anything.
						pszOutput[nOutLen]= *(pch-1); // do add the %, this may get further processed
						nOutLen++;
						pszOutput[nOutLen]= *pch;
						nOutLen++;
						pch++;
					}
				}
				else
				{
					pszOutput[nOutLen]= *(pch-1);
					nOutLen++;
				}
				
			}
			else
			{
				pszOutput[nOutLen]= *pch;
				nOutLen++;
				pch++;
			}
		}

		pszOutput[nOutLen]=0;

		pch = (char*)malloc(strlen(pszOutput)+1);
		if(pch)
		{
			strcpy(pch,pszOutput);
			if(bFreeIncomingString) 
			{
				try {free(pszString);} catch(...){}
			}
		}
		return pch;
	}
	return NULL;
}


