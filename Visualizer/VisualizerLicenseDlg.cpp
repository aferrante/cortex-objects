// VisualizerLicenseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Visualizer.h"
#include "VisualizerLicenseDlg.h"
#include "../../Common/KEY/LicenseKey.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CVisualizerLicenseDlg dialog


CVisualizerLicenseDlg::CVisualizerLicenseDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CVisualizerLicenseDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CVisualizerLicenseDlg)
	m_szKey = _T("");
	//}}AFX_DATA_INIT
}


void CVisualizerLicenseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CVisualizerLicenseDlg)
	DDX_Text(pDX, IDC_EDIT1, m_szKey);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CVisualizerLicenseDlg, CDialog)
	//{{AFX_MSG_MAP(CVisualizerLicenseDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVisualizerLicenseDlg message handlers

void CVisualizerLicenseDlg::OnOK() 
{
	// TODO: Add extra validation here
	CLicenseKey key;
	key.m_pszLicenseString = (char*)malloc(strlen(m_szKey)+1);
	if(key.m_pszLicenseString)
	sprintf(key.m_pszLicenseString, "%s", m_szKey);

	key.InterpretKey();

	if(!key.m_bValid)
	{
		AfxMessageBox("This is not a valid license code."); 
		return;
	}

	CDialog::OnOK();
}
