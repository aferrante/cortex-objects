#if !defined(AFX_VISUALIZERKEYSETUPDLG_H__07632578_24BF_4068_B59E_D20146C01D90__INCLUDED_)
#define AFX_VISUALIZERKEYSETUPDLG_H__07632578_24BF_4068_B59E_D20146C01D90__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// VisualizerKeySetupDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CVisualizerKeySetupDlg dialog

class CVisualizerKeySetupDlg : public CDialog
{
// Construction
public:
	CVisualizerKeySetupDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CVisualizerKeySetupDlg)
	enum { IDD = IDD_KEYDIALOG };
	BOOL	m_bActive;
	BOOL	m_bAlt;
	BOOL	m_bAppFocus;
	BOOL	m_bCtrl;
	BOOL	m_bShift;
	CString	m_szKey;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVisualizerKeySetupDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CVisualizerKeySetupDlg)
	virtual void OnOK();
	afx_msg void OnButtonHelp();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VISUALIZERKEYSETUPDLG_H__07632578_24BF_4068_B59E_D20146C01D90__INCLUDED_)
