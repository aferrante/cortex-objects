// VisualizerSettings.h: interface for the CVisualizerSettings class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VISUALIZERSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
#define AFX_VISUALIZERSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "VisualizerData.h"  // for endpoints
#include "VisualizerDefines.h"
//#include "../Sentinel/SentinelData.h"  
//#include "../../Common/MFC/ODBC/DBUtil.h"  


class CVisualizerSettings  
{
public:
	CVisualizerSettings();
	virtual ~CVisualizerSettings();

//	int ReadKeys();
	int Settings(bool bRead);
	char* ProcessString(char* pszString, bool bFreeIncomingString);

	char* m_pszName;  // familiar name of this instance 
	unsigned long m_ulMainMode;

	unsigned long m_ulDebug;  // prints out debug statements that & with this.

	// ports
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;

//	char*	m_pszTabulatorHost;	// the name of the cortex host
//	unsigned short m_usTabulatorCommandPort;
//	char*	m_pszPluginName;
//	char*	m_pszUserToken;
//	char*	m_pszKeySetupHelp;


	// The following flags determine what settings are loaded. 
	// The specifics do not need to be stored in cortex settings, they are pushed to the various objects.

	// messaging for Visualizer
	bool m_bUseLog;			// write a log file
	bool m_bUseAsRunLog;			// write a log file
	bool m_bUseEmail;		// send an email on failures (or commanded from remotes)
	bool m_bUseNetwork; // send a message to a remote host
	char* m_pszFileSpec;
	char* m_pszAsRunFileSpec;
	char* m_pszMailSpec;
	char* m_pszProcessedAsRunFileSpec;
	char* m_pszProcessedFileSpec;
	char* m_pszProcessedMailSpec;
//	bool m_bUseXMLClientLog;			// write a log file
	bool m_bLogNetworkErrors; // enables messaging into network object (logs socket errors, etc).

	bool m_bReportSuccessfulOperation;

	int m_nAsRunRecent;
//	int m_nCommRetryMS;

	
	char* m_pszLicense;  // the License Key
	char* m_pszOEMcodes;  // the possible OEM string
	char* m_pszIconPath;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.


	int m_nDeviceType; // refer to cortex device types.

};

#endif // !defined(AFX_VISUALIZERSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
