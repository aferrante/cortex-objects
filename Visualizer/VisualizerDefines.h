// VisualizerDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(VISUALIZERDEFINES_H_INCLUDED)
#define VISUALIZERDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define VISUALIZER_CURRENT_VERSION		"1.0.0.1"


// modes
#define VISUALIZER_MODE_DEFAULT			0x00000000  // exclusive
#define VISUALIZER_MODE_LISTENER		0x00000001  // exclusive
#define VISUALIZER_MODE_CLONE				0x00000002  // exclusive
#define VISUALIZER_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define VISUALIZER_MODE_VOLATILE		0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define VISUALIZER_MODE_MASK				0x0000000f  // 

// default port values.
//#define VISUALIZER_PORT_FILE				80		
#define VISUALIZER_PORT_CMD					30000		
#define VISUALIZER_PORT_STATUS			10540 //default to MOS

#define VISUALIZER_PORT_INVALID			0	

#define VISUALIZER_FLAGS_INVALID			0xffffffff	 
#define VISUALIZER_FLAG_DISABLED			0x0000	 // default
#define VISUALIZER_FLAG_ENABLED			0x0001	
#define VISUALIZER_FLAG_FOUND				0x1000

#define VISUALIZER_FLAG_TRIGGERED			0x01000000
#define VISUALIZER_FLAG_ANALYZED				0x00000010
#define VISUALIZER_FLAG_REANALYZED			0x00000020

#define VISUALIZER_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded

// status
#define VISUALIZER_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define VISUALIZER_STATUS_UNKNOWN							0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define VISUALIZER_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define VISUALIZER_STATUS_ERROR								0x00000020  // error (red icon)
#define VISUALIZER_STATUS_CONN								0x00000030  // ready (green icon)	
#define VISUALIZER_STATUS_OK									0x00000030  // ready (green icon)	
#define VISUALIZER_STATUS_RUN									0x00000040  // in progress, running, owned etc (blue icon);	
#define VISUALIZER_ICON_MASK									0x00000070  // mask	

#define VISUALIZER_STATUS_SUSPEND							0x00000080  // suspended	(yellow icon please)

#define VISUALIZER_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define VISUALIZER_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define VISUALIZER_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define VISUALIZER_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define VISUALIZER_STATUS_CMDSVR_MASK					0x00007000  // command server mask bits

#define VISUALIZER_STATUS_STATUSSVR_START			0x00010000  // starting the status server
#define VISUALIZER_STATUS_STATUSSVR_RUN				0x00020000  // status server running
#define VISUALIZER_STATUS_STATUSSVR_END				0x00030000  // status server shutting down
#define VISUALIZER_STATUS_STATUSSVR_ERROR			0x00040000  // status server error
#define VISUALIZER_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define VISUALIZER_STATUS_THREAD_START				0x00100000  // starting the main thread
#define VISUALIZER_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define VISUALIZER_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define VISUALIZER_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define VISUALIZER_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define VISUALIZER_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define VISUALIZER_STATUS_THREAD_MASK					0x00f00000  // main thread mask bits

// various failures...
#define VISUALIZER_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define VISUALIZER_STATUS_FAIL_DB							0x20000000  // could not get DB
#define VISUALIZER_STATUS_FAIL_MASK						0xf0000000  // failure code mask bits

//return values
#define VISUALIZER_SUCCESS   0
#define VISUALIZER_ERROR	   -1


// commands
#define VISUALIZER_CMD_INSERT	   0xa0
#define VISUALIZER_CMD_DELETE	   0xa1
#define VISUALIZER_CMD_GET		   0xa2
#define VISUALIZER_CMD_LOCK		   0xa3
#define VISUALIZER_CMD_MOVE		   0xa4
#define VISUALIZER_CMD_MOD		   0xa5
#define VISUALIZER_CMD_SWAP		   0xa6
#define VISUALIZER_CMD_FIND		   0xa7  // just return postion in list
#define VISUALIZER_CMD_DEL_ID	   0xa8  // delete by list of IDs, not index

// subcommands
#define VISUALIZER_SCMD_TIESEC	 0x08 // tie secondaries to primaries in the command
#define VISUALIZER_SCMD_NOSEC	   0x00 // explicit command, do NOT tie secondaries to primaries in the command
#define VISUALIZER_SCMD_UNLOCK   0x08
#define VISUALIZER_SCMD_LOCK		 0x00

#define VISUALIZER_CMD_QUEUE		   0xc0 // puts thing in queue
#define VISUALIZER_CMD_CMDQUEUE		 0xc1 // puts thing in command queue
#define VISUALIZER_CMD_DIRECT			 0xc2 // skips queues, commands box directly

#define VISUALIZER_CMD_EXSET			 0xea // increments exchange counter
#define VISUALIZER_CMD_EXGET			 0xeb // gets exchange mod value
#define VISUALIZER_CMD_MODSET			 0xec // sets exchange mod value, skips exchange table





// default filenames
#define VISUALIZER_SETTINGS_FILE_SETTINGS	  "visualizer.csr"		// csr = cortex settings redirect
#define VISUALIZER_SETTINGS_FILE_DEFAULT	  "visualizer.csf"		// csf = cortex settings file

// debug defines
#define VISUALIZER_DEBUG_TRIGGER				0x00000001  // logs keycodes
#define VISUALIZER_DEBUG_ASRUN					0x00000002
#define VISUALIZER_DEBUG_RULES					0x00000004
#define VISUALIZER_DEBUG_PARAMS				0x00000008
#define VISUALIZER_DEBUG_PARAMPARSE		0x00000010
#define VISUALIZER_DEBUG_TIMING				0x00000020
#define VISUALIZER_DEBUG_EVENTRULE			0x00000040
#define VISUALIZER_DEBUG_PROCESS				0x00000080
#define VISUALIZER_DEBUG_CHANNELS			0x00000100
#define VISUALIZER_DEBUG_COMM					0x00000200


#endif // !defined(VISUALIZERDEFINES_H_INCLUDED)
