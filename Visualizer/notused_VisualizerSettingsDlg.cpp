// VisualizerSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Visualizer.h"
#include "VisualizerMain.h"
#include "VisualizerSettingsDlg.h"
#include "VisualizerLicenseDlg.h"
#include "VisualizerKeySetupDlg.h"

extern CVisualizerMain* g_pvisualizer;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CVisualizerSettingsDlg dialog


CVisualizerSettingsDlg::CVisualizerSettingsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CVisualizerSettingsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CVisualizerSettingsDlg)
	m_bSuspend = FALSE;
	m_szIP = _T("");
	m_szPlugin = _T("");
	m_nPort = 0;
	m_szToken = _T("");
	m_bAsRun = FALSE;
	//}}AFX_DATA_INIT

	m_ppMappingObj=NULL;
	m_nNumMappingObjects=0;

	m_hIcon = (HICON)LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(IDR_MAINFRAME), IMAGE_ICON, 16, 16, LR_LOADMAP3DCOLORS );
}

CVisualizerSettingsDlg::~CVisualizerSettingsDlg()
{
//	AfxMessageBox("destructo");
	if(m_ppMappingObj)
	{
		int i=0;
		while(i<m_nNumMappingObjects)
		{
			if(m_ppMappingObj[i]) delete m_ppMappingObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppMappingObj; // delete array of pointers to objects, must use new to allocate
	}
	m_ppMappingObj = NULL;
	m_nNumMappingObjects = 0;

}



void CVisualizerSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CVisualizerSettingsDlg)
	DDX_Control(pDX, IDC_LIST1, m_lce);
	DDX_Check(pDX, IDC_CHECK_SUSPEND, m_bSuspend);
	DDX_Text(pDX, IDC_EDIT_IP, m_szIP);
	DDX_Text(pDX, IDC_EDIT_PLUGIN, m_szPlugin);
	DDX_Text(pDX, IDC_EDIT_PORT, m_nPort);
	DDX_Text(pDX, IDC_EDIT_TOKEN, m_szToken);
	DDX_Check(pDX, IDC_CHECK_ASRUN, m_bAsRun);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CVisualizerSettingsDlg, CDialog)
	//{{AFX_MSG_MAP(CVisualizerSettingsDlg)
	ON_WM_SYSCOMMAND()
	ON_BN_CLICKED(IDC_BUTTON_ADD, OnButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, OnButtonDelete)
	ON_BN_CLICKED(IDC_BUTTON_EDIT, OnButtonEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVisualizerSettingsDlg message handlers

BOOL CVisualizerSettingsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		pSysMenu->AppendMenu(MF_SEPARATOR);
		pSysMenu->AppendMenu(MF_STRING, IDM_UPDATELICENSE, "Update license code...");
	}

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon


	CRect rc; 
	m_lce.GetWindowRect(&rc);
	m_lce.InsertColumn(0, "", LVCFMT_LEFT, rc.Width()-16, 0 );

	// seed things from g_pvisualizer->m_settings;

	/*
	if(g_pvisualizer)
	{
//		AfxMessageBox("X");
		m_bSuspend = g_pvisualizer->m_data.m_bProcessSuspended?TRUE:FALSE;
		m_szIP = g_pvisualizer->m_settings.m_pszTabulatorHost?g_pvisualizer->m_settings.m_pszTabulatorHost:"";
		m_szPlugin = g_pvisualizer->m_settings.m_pszPluginName?g_pvisualizer->m_settings.m_pszPluginName:"";
		m_nPort = g_pvisualizer->m_settings.m_usTabulatorCommandPort;
		m_szToken = g_pvisualizer->m_settings.m_pszUserToken?g_pvisualizer->m_settings.m_pszUserToken:"";

		m_bAsRun = g_pvisualizer->m_settings.m_bUseAsRunLog?TRUE:FALSE;
		
		if(m_szToken.GetLength()==0)
		{
			m_szToken = g_pvisualizer->m_data.m_pszHost?g_pvisualizer->m_data.m_pszHost:"";
		}

//		AfxMessageBox("X1");

		if((g_pvisualizer->m_data.m_ppMappingObj)&&(g_pvisualizer->m_data.m_nNumMappingObjects))
		{
			m_ppMappingObj = new CVisualizerMappingObject*[g_pvisualizer->m_data.m_nNumMappingObjects];
			if(m_ppMappingObj)
			{
				int i=0;
				while(i<g_pvisualizer->m_data.m_nNumMappingObjects)
				{
					m_ppMappingObj[i] = new CVisualizerMappingObject;
					if((m_ppMappingObj[i])&&(g_pvisualizer->m_data.m_ppMappingObj[i]))
					{
						m_ppMappingObj[i]->m_bActive = g_pvisualizer->m_data.m_ppMappingObj[i]->m_bActive;
						m_ppMappingObj[i]->m_bRequireAppFocus = g_pvisualizer->m_data.m_ppMappingObj[i]->m_bRequireAppFocus;
						m_ppMappingObj[i]->m_nKeyCode = g_pvisualizer->m_data.m_ppMappingObj[i]->m_nKeyCode;
						m_ppMappingObj[i]->m_nSysChars = g_pvisualizer->m_data.m_ppMappingObj[i]->m_nSysChars;
					}
					i++;
				}

				m_nNumMappingObjects = i;

			}
		}

	}
//		AfxMessageBox("X2");


	DoList();
//		AfxMessageBox("X3");
*/
	UpdateData(FALSE);
//		AfxMessageBox("X4");
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CVisualizerSettingsDlg::DoList()
{
	int nRows = m_lce.GetItemCount();
	int i=0;
	while((i<m_nNumMappingObjects)&&(m_ppMappingObj))
	{
		CString szText;
		CString szNew=g_pvisualizer->m_data.DoText(m_ppMappingObj[i]);

		if(i<nRows)
		{
			szText = m_lce.GetItemText(i,0);
			if(szText.Compare(szNew))	m_lce.SetItemText(i, 0, szNew);		
		}
		else
		{
			m_lce.InsertItem( i, szNew ); 
		}
//		m_lce.SetItemData( i, (DWORD)(&m_ppMappingObj[i]) ); 
		
		i++;
	}

	while(i<nRows)
	{
		m_lce.DeleteItem(i);
		nRows--;
	}


}

void CVisualizerSettingsDlg::OnOK() 
{
	// TODO: Add extra validation here
	UpdateData(TRUE);
/*
	if(g_pvisualizer)
	{
		g_pvisualizer->m_data.m_bProcessSuspended = m_bSuspend?true:false;

		if(g_pvisualizer->m_settings.m_pszTabulatorHost)
		{	try{delete g_pvisualizer->m_settings.m_pszTabulatorHost;} catch(...){}	}
		g_pvisualizer->m_settings.m_pszTabulatorHost = (char*)malloc(m_szIP.GetLength()+1);
		if(g_pvisualizer->m_settings.m_pszTabulatorHost)
		{	sprintf(g_pvisualizer->m_settings.m_pszTabulatorHost, "%s", m_szIP);	}

		if(g_pvisualizer->m_settings.m_pszPluginName)
		{	try{delete g_pvisualizer->m_settings.m_pszPluginName;} catch(...){}	}
		g_pvisualizer->m_settings.m_pszPluginName = (char*)malloc(m_szPlugin.GetLength()+1);
		if(g_pvisualizer->m_settings.m_pszPluginName)
		{	sprintf(g_pvisualizer->m_settings.m_pszPluginName, "%s", m_szPlugin);	}

		if(g_pvisualizer->m_settings.m_pszUserToken)
		{	try{delete g_pvisualizer->m_settings.m_pszUserToken;} catch(...){}	}
		g_pvisualizer->m_settings.m_pszUserToken = (char*)malloc(m_szToken.GetLength()+1);
		if(g_pvisualizer->m_settings.m_pszUserToken)
		{	sprintf(g_pvisualizer->m_settings.m_pszUserToken, "%s", m_szToken);	}


		g_pvisualizer->m_settings.m_usTabulatorCommandPort = (unsigned short) m_nPort;


		bool bAsRun = g_pvisualizer->m_settings.m_bUseAsRunLog;


		if(!m_bAsRun)
		{
			if(bAsRun)
			{
				bAsRun = false;
				g_pvisualizer->m_msgr.RemoveDestination("asrun");
			}
		}
		else
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");

			if(!bAsRun)
			{
				bAsRun = true;
				int nRegisterCode=0;

				nRegisterCode = g_pvisualizer->m_msgr.AddDestination(MSG_DESTTYPE_LOG,//|MSG_DESTTYPE_DEFAULT, 
					"asrun", 
					g_pvisualizer->m_settings.m_pszProcessedAsRunFileSpec?g_pvisualizer->m_settings.m_pszProcessedAsRunFileSpec:g_pvisualizer->m_settings.m_pszAsRunFileSpec, 
					errorstring);
				if (nRegisterCode != MSG_SUCCESS) 
				{
					// inform the windowing environment
		//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register as-run log file!\n code: %d", nRegisterCode); 
					g_pvisualizer->m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_LOG|CX_STATUS_ERROR));
					g_pvisualizer->m_msgr.DM(MSG_ICONERROR, NULL, "Visualizer:asrun_log_init", errorstring);  //(Dispatch message)
				}
			}
			else
			{ // check for change
				int nIndex=g_pvisualizer->m_msgr.GetDestIndex("asrun");
				if(nIndex>=0)
				{
					if((g_pvisualizer->m_msgr.m_ppDest[nIndex]->m_pszParams)&&(g_pvisualizer->m_settings.m_pszProcessedAsRunFileSpec?g_pvisualizer->m_settings.m_pszProcessedAsRunFileSpec:g_pvisualizer->m_settings.m_pszAsRunFileSpec))
					{
						if(strcmp(g_pvisualizer->m_msgr.m_ppDest[nIndex]->m_pszParams, (g_pvisualizer->m_settings.m_pszProcessedAsRunFileSpec?g_pvisualizer->m_settings.m_pszProcessedAsRunFileSpec:g_pvisualizer->m_settings.m_pszAsRunFileSpec)))
						{
							int nRegisterCode=0;

							// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
							nRegisterCode = g_pvisualizer->m_msgr.ModifyDestination(
								"asrun", 
								g_pvisualizer->m_settings.m_pszProcessedAsRunFileSpec?g_pvisualizer->m_settings.m_pszProcessedAsRunFileSpec:g_pvisualizer->m_settings.m_pszAsRunFileSpec,
								MSG_DESTTYPE_LOG,//|MSG_DESTTYPE_DEFAULT, 
								errorstring);
							if (nRegisterCode != MSG_SUCCESS) 
							{
								// inform the windowing environment
					//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify as-run log object!\n code: %d", nRegisterCode); 
								//g_pvisualizer->m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
								g_pvisualizer->m_msgr.DM(MSG_ICONERROR, NULL, "Visualizer:asrun_log_change", errorstring);  //(Dispatch message)
							}
						}
					}
				}
			}
		}

		g_pvisualizer->m_settings.m_bUseAsRunLog = m_bAsRun?true:false;



		EnterCriticalSection(&g_pvisualizer->m_data.m_critMaps);
		if(g_pvisualizer->m_data.m_ppMappingObj)
		{
			int i=0;
			while(i<g_pvisualizer->m_data.m_nNumMappingObjects)
			{
				if(g_pvisualizer->m_data.m_ppMappingObj[i])
				{
					if(!g_pvisualizer->m_data.m_ppMappingObj[i]->m_bRequireAppFocus)
					{	UnregisterHotKey(NULL, i);}
					try{delete g_pvisualizer->m_data.m_ppMappingObj[i];} catch(...){} // delete objects, must use new to allocate
				}
				i++;
			}
			try {delete [] g_pvisualizer->m_data.m_ppMappingObj;} catch(...){}// delete array of pointers to objects, must use new to allocate
		}
		g_pvisualizer->m_data.m_ppMappingObj = m_ppMappingObj;
		g_pvisualizer->m_data.m_nNumMappingObjects = m_nNumMappingObjects;

		
		// null these two out once they have been swapped, so they don't get deleted on dlg destruct
		m_ppMappingObj = NULL;
		m_nNumMappingObjects = 0;

		if(g_pvisualizer->m_data.m_ppMappingObj)
		{
			int i=0;
			while(i<g_pvisualizer->m_data.m_nNumMappingObjects)
			{
				if(g_pvisualizer->m_data.m_ppMappingObj[i])
				{
if(0)//g_pvisualizer->m_settings.m_ulDebug&VISUALIZER_DEBUG_TRIGGER) 	
{
	g_pvisualizer->m_msgr.DM(MSG_ICONINFO, NULL, "Visualizer", "checking key %d", i);  //(Dispatch message)
}

					if(!g_pvisualizer->m_data.m_ppMappingObj[i]->m_bRequireAppFocus)
					{	
						if(RegisterHotKey(NULL, i, g_pvisualizer->m_data.m_ppMappingObj[i]->m_nSysChars, g_pvisualizer->m_data.m_ppMappingObj[i]->m_nKeyCode)==0)
						{
							g_pvisualizer->m_data.m_ppMappingObj[i]->m_bFailed=true; 
	g_pvisualizer->m_msgr.DM(MSG_ICONERROR, NULL, "Visualizer", "Failure to register hotkey: identifier %d, code 0x%02x modifiers 0x%02x.", i, g_pvisualizer->m_data.m_ppMappingObj[i]->m_nKeyCode, g_pvisualizer->m_data.m_ppMappingObj[i]->m_nSysChars);  //(Dispatch message)
						}
						else
						{
if(g_pvisualizer->m_settings.m_ulDebug&VISUALIZER_DEBUG_TRIGGER) 	
{
	g_pvisualizer->m_msgr.DM(MSG_ICONINFO, NULL, "Visualizer", "Registered hotkey: identifier %d, code 0x%02x modifiers 0x%02x.", i, g_pvisualizer->m_data.m_ppMappingObj[i]->m_nKeyCode, g_pvisualizer->m_data.m_ppMappingObj[i]->m_nSysChars);  //(Dispatch message)
}
							g_pvisualizer->m_data.m_ppMappingObj[i]->m_bFailed=false; 

						}
					}
				}
				i++;
			}
		}

		LeaveCriticalSection(&g_pvisualizer->m_data.m_critMaps);

		g_pvisualizer->m_settings.Settings(false);
	}

	//	

*/
	CDialog::OnOK();
}

void CVisualizerSettingsDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_UPDATELICENSE)
	{
		CVisualizerLicenseDlg dlg;
		if(dlg.DoModal()==IDOK)
		{
			if(g_pvisualizer)
			{
				try
				{
					if(g_pvisualizer->m_settings.m_pszLicense) free(g_pvisualizer->m_settings.m_pszLicense);
//					if(g_pvisualizer->m_data.m_key.m_pszLicenseString) free(g_pvisualizer->m_data.m_key.m_pszLicenseString);
				}
				catch(...)
				{
				}

				g_pvisualizer->m_settings.m_pszLicense = (char*)malloc(strlen(dlg.m_szKey)+1);
				if(g_pvisualizer->m_settings.m_pszLicense)
				sprintf(g_pvisualizer->m_settings.m_pszLicense, "%s", dlg.m_szKey);

				g_pvisualizer->m_settings.Settings(false);  // saves setting to csf file
				g_pvisualizer->m_settings.Settings(true);  // reads it back in and interprets the key and params

			}
		}
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

int CVisualizerSettingsDlg::FindKey(int nCode, int nSysChars) 
{
	if((m_nNumMappingObjects)&&(m_ppMappingObj))
	{
		int i=0;
		while(i<m_nNumMappingObjects)
		{
			if(m_ppMappingObj[i])
			{
				if((m_ppMappingObj[i]->m_nKeyCode == nCode)&&(m_ppMappingObj[i]->m_nSysChars == nSysChars))
				{
					return i;
				}
			}
			i++;
		}
	}

	return -1;
}

void CVisualizerSettingsDlg::OnButtonAdd() 
{
	// TODO: Add your control notification handler code here
	CVisualizerKeySetupDlg dlg;
	if(dlg.DoModal()==IDOK)
	{
	// add it
		CVisualizerMappingObject* pObj = new CVisualizerMappingObject;
		if(pObj)
		{
			if(g_pvisualizer)
			{
				pObj->m_nKeyCode = g_pvisualizer->m_data.TextToCode(dlg.m_szKey);
			}
			pObj->m_nSysChars = ((dlg.m_bAlt?MOD_ALT:0)|(dlg.m_bCtrl?MOD_CONTROL:0)|(dlg.m_bShift?MOD_SHIFT:0));
			pObj->m_bActive = dlg.m_bActive?true:false;
			pObj->m_bRequireAppFocus = dlg.m_bAppFocus?true:false;

			int i = FindKey(pObj->m_nKeyCode, pObj->m_nSysChars);

			if(i>=0)
			{
				AfxMessageBox("Could not add item.\r\nKey combination already exists!");
				try{ delete pObj;} catch(...){}
				return;
			}

/*
#define MOD_ALT         0x0001
#define MOD_CONTROL     0x0002
#define MOD_SHIFT       0x0004
*/
			CVisualizerMappingObject** ppObj = new CVisualizerMappingObject*[m_nNumMappingObjects+1];
			i=0;
			if(ppObj)
			{
				while(i<m_nNumMappingObjects)
				{
					ppObj[i] = m_ppMappingObj[i];
					i++;
				}
				delete [] m_ppMappingObj;
				m_ppMappingObj = ppObj;
				ppObj[i] = pObj;

				m_nNumMappingObjects++;
			}
		}
		else
		{
			AfxMessageBox("Could not add item!\r\nError allocating memory.");
		}

		DoList();
	}
}

void CVisualizerSettingsDlg::OnButtonDelete() 
{
	// TODO: Add your control notification handler code here
	int n = m_lce.GetNextItem(-1, LVNI_SELECTED);
	if(n<0)
	{
		AfxMessageBox("No item selected.");
	}
	else
	{
		// delete it!

		if(m_ppMappingObj)
		{
			int i=n;
			if(m_ppMappingObj[n]) 
			{
				try{ delete m_ppMappingObj[n]; } catch(...){}
			}
			while(i<m_nNumMappingObjects-1)
			{
				m_ppMappingObj[i] = m_ppMappingObj[i+1];
				i++;
			}
			m_ppMappingObj[i] = NULL;
			m_nNumMappingObjects--;
		}

		DoList();
	}
}

void CVisualizerSettingsDlg::OnButtonEdit() 
{
	// TODO: Add your control notification handler code here
	int n = m_lce.GetNextItem(-1, LVNI_SELECTED);
	if(n<0)
	{
		AfxMessageBox("No item selected.");
	}
	else
	{
		CVisualizerKeySetupDlg dlg;
		// PRESET THE STUFF.
//		CVisualizerMappingObject* pObj = (CVisualizerMappingObject*)m_lce.GetItemData( n ); 
		CVisualizerMappingObject* pObj = m_ppMappingObj[n];

		if(g_pvisualizer)
		{
			dlg.m_szKey = g_pvisualizer->m_data.CodeToText(pObj->m_nKeyCode);
		}
		dlg.m_bAlt = (pObj->m_nSysChars&MOD_ALT)?true:false;
		dlg.m_bCtrl = (pObj->m_nSysChars&MOD_CONTROL)?true:false;
		dlg.m_bShift = (pObj->m_nSysChars&MOD_SHIFT)?true:false;

		dlg.m_bAppFocus = pObj->m_bRequireAppFocus;
		dlg.m_bActive = pObj->m_bActive;

		if(dlg.DoModal()==IDOK)
		{
			if(g_pvisualizer)
			{
				pObj->m_nKeyCode = g_pvisualizer->m_data.TextToCode(dlg.m_szKey);
			}
			pObj->m_nSysChars = ((dlg.m_bAlt?MOD_ALT:0)|(dlg.m_bCtrl?MOD_CONTROL:0)|(dlg.m_bShift?MOD_SHIFT:0));
			pObj->m_bActive = dlg.m_bActive?true:false;
			pObj->m_bRequireAppFocus = dlg.m_bAppFocus?true:false;


	/*
	#define MOD_ALT         0x0001
	#define MOD_CONTROL     0x0002
	#define MOD_SHIFT       0x0004
	*/

			DoList();
		}
	}
	
}

