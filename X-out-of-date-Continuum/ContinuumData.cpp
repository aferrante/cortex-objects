// ContinuumData.cpp: implementation of the CContinuumData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <process.h>
#include "Continuum.h"
#include "ContinuumHandler.h" 
#include "ContinuumMain.h" 
#include "ContinuumData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CContinuumMain* g_pcontinuum;
extern CContinuumApp theApp;

//extern void ContinuumConnectionThread(void* pvArgs);


//////////////////////////////////////////////////////////////////////
// CContinuumObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CContinuumObject::CContinuumObject()
{
	m_pszProcessName = NULL;
	m_pszStatus = NULL;
	m_pszIconPath = NULL;
	m_ulStatus	= CONTINUUM_STATUS_UNINIT;
	m_ulFlags = CONTINUUM_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
}

CContinuumObject::~CContinuumObject()
{
	if(m_pszProcessName) free(m_pszProcessName); // must use malloc to allocate
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
	if(m_pszIconPath) free(m_pszIconPath); // must use malloc to allocate
}


//////////////////////////////////////////////////////////////////////
// CContinuumData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CContinuumData::CContinuumData()
{
	InitializeCriticalSection(&m_critText);
//	InitializeCriticalSection(&m_critSQL);
	

	m_ppObj = NULL;
	m_nNumObjects = 0;

	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebTick );
	m_pszHost = NULL;			// the name of the host
	m_pszCortexHost = NULL;			// the name of the cortex host
	m_usCortexCommandPort = CONTINUUM_PORT_CMD;
	m_usCortexStatusPort = CONTINUUM_PORT_STATUS;
/*
	m_nSettingsMod = -1;
	m_nChannelsMod = -1;
	m_nConnectionsMod = -1;
	m_nLastSettingsMod = -1;
	m_nLastChannelsMod = -1;
	m_nLastConnectionsMod = -1;
	m_pdb = NULL;
	m_pdbConn = NULL;
*/
	m_bQuietKill = false;

	InitializeCriticalSection(&m_critIncrement);
}

CContinuumData::~CContinuumData()
{
	if(m_ppObj)
	{
		int i=0;
		while(i<m_nNumObjects)
		{
			if(m_ppObj[i]) delete m_ppObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppObj; // delete array of pointers to objects, must use new to allocate
	}

	if(m_pszHost) free(m_pszHost);	// must use malloc to allocate
	if(m_pszCortexHost) free(m_pszCortexHost);	// must use malloc to allocate

	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critIncrement);
//	DeleteCriticalSection(&m_critSQL);
}


char* CContinuumData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CContinuumData::SetStatusText(char* pszText, unsigned long ulStatus)
{
	int nRV = CX_ERROR;
	if(pszText)
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
/*
	if((g_pcontinuum->m_settings.m_pszIconPath)&&(strlen(g_pcontinuum->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_pcontinuum->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_pcontinuum->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_pcontinuum->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_pcontinuum->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_pcontinuum->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}
*/
	return nRV;
}

// utility
int	CContinuumData::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszHost) free(m_pszHost);
						m_pszHost=pch;
						return CONTINUUM_SUCCESS;
					}
				}
			}
		}
	}
	return CONTINUUM_ERROR;
}

/*
int CContinuumData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_pcontinuum)&&(g_pcontinuum->m_settings.m_pszExchange)&&(strlen(g_pcontinuum->m_settings.m_pszExchange)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s set mod = \
(SELECT case when max(mod) is null then 1 when max(mod) >= %d \
then 1 else max(mod) + 1 end from %s WHERE criterion = '%s') WHERE criterion = '%s'",
			g_pcontinuum->m_settings.m_pszExchange,
			CONTINUUM_DB_MOD_MAX,
			g_pcontinuum->m_settings.m_pszExchange,
			szTemp, szTemp		
			);
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return CONTINUUM_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return CONTINUUM_ERROR;
}

int CContinuumData::CheckDatabaseMods(char* pszInfo)
{
	if((g_pcontinuum)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_pcontinuum->m_settings.m_pszExchange)&&(strlen(g_pcontinuum->m_settings.m_pszExchange)))?g_pcontinuum->m_settings.m_pszExchange:"Exchange");

		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = CONTINUUM_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCriterion;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
				}

				if((g_pcontinuum->m_settings.m_pszSettings)&&(strlen(g_pcontinuum->m_settings.m_pszSettings)))
				{
					szTemp.Format("DBT_%s",g_pcontinuum->m_settings.m_pszSettings);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nSettingsMod = nReturn;
					}
				}

				if((g_pcontinuum->m_settings.m_pszChannels)&&(strlen(g_pcontinuum->m_settings.m_pszChannels)))
				{
					szTemp.Format("DBT_%s",g_pcontinuum->m_settings.m_pszChannels);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nChannelsMod = nReturn;
					}
				}

				if((g_pcontinuum->m_settings.m_pszConnections)&&(strlen(g_pcontinuum->m_settings.m_pszConnections)))
				{
					szTemp.Format("DBT_%s",g_pcontinuum->m_settings.m_pszConnections);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nConnectionsMod = nReturn;
					}
				}

				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			delete prs;
			LeaveCriticalSection(&m_critSQL);
			return nReturn;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return CONTINUUM_ERROR;
}

int CContinuumData::GetChannels(char* pszInfo)
{
	if((g_pcontinuum)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_pcontinuum->m_settings.m_pszChannels)&&(strlen(g_pcontinuum->m_settings.m_pszChannels)))?g_pcontinuum->m_settings.m_pszChannels:"Channels");

		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = CONTINUUM_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
//				CString szServer = "";
				CString szDesc = "";
				CString szTemp = "";
				unsigned long ulFlags;   // various flags
//				int nListID = -1;
				int nChannelID = -1;
				int nTemp;
				bool bFound = false;
				bool bFlagsFound = false;
				try
				{
					prs->GetFieldValue("ID", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nChannelID = nTemp;
					}
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bFlagsFound = true;
						ulFlags = atol(szTemp);
					}
					prs->GetFieldValue("description", szDesc);//HARDCODE
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
* /
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				if((g_pcontinuum->m_data.m_ppChannelObj)&&(g_pcontinuum->m_data.m_nNumChannelObjects))
				{
					nTemp=0;
					while(nTemp<g_pcontinuum->m_data.m_nNumChannelObjects)
					{
						if((g_pcontinuum->m_data.m_ppChannelObj[nTemp]))//&&(g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_pszServerName))
						{
							if(
								  (nChannelID==g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_nChannelID)
								)
							{
								bFound = true;
								// override with the new changes:
								if(
										(szDesc.GetLength()>0)
									&&((g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_pszDesc==NULL)||(szDesc.CompareNoCase(g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_pszDesc)))
									)
								{
									if(g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_pszDesc) free(g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_pszDesc);

									g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_pszDesc) sprintf(g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_pszDesc, szDesc);
								}

								if((bFlagsFound)&&(ulFlags&CONTINUUM_FLAG_ENABLED)&&(!((g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_ulFlags)&CONTINUUM_FLAG_ENABLED)))
								{

// ****  do something here to check out if the list is able to be activated, that is, if it's on a connected connection

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating list %d%s%s...", 
										g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_nChannelID,  
										g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_pszDesc?": ":"", 
										g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_pszDesc?g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_pszDesc:"");  
									g_pcontinuum->m_msgr.DM(MSG_ICONINFO, NULL, "Continuum:destination_change", errorstring);  Sleep(20);  //(Dispatch message)

// **** if it's able to be activated, set the status to connected.
									g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_ulStatus = CONTINUUM_STATUS_CONN;
								}
								

								if((bFlagsFound)&&(!(ulFlags&CONTINUUM_FLAG_ENABLED))&&((g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_ulFlags)&CONTINUUM_FLAG_ENABLED))
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating list %d%s%s...", 
										g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_nChannelID,  
										g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_pszDesc?": ":"", 
										g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_pszDesc?g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_pszDesc:"");  
									g_pcontinuum->m_msgr.DM(MSG_ICONINFO, NULL, "Continuum:destination_change", errorstring);  Sleep(20);     //(Dispatch message)

									g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_ulStatus = CONTINUUM_STATUS_NOTCON;
								}

								// and set up the flags
								if(bFlagsFound) g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_ulFlags = ulFlags|CONTINUUM_FLAG_FOUND;
								else g_pcontinuum->m_data.m_ppChannelObj[nTemp]->m_ulFlags |= CONTINUUM_FLAG_FOUND;
							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(nChannelID>0)) // have to add.
				{

					CContinuumObject* pscho = new CContinuumObject;
					if(pscho)
					{
						CContinuumObject** ppObj = new CContinuumObject*[g_pcontinuum->m_data.m_nNumChannelObjects+1];
						if(ppObj)
						{
							int o=0;
							if((g_pcontinuum->m_data.m_ppChannelObj)&&(g_pcontinuum->m_data.m_nNumChannelObjects>0))
							{
								while(o<g_pcontinuum->m_data.m_nNumChannelObjects)
								{
									ppObj[o] = g_pcontinuum->m_data.m_ppChannelObj[o];
									o++;
								}
								delete [] g_pcontinuum->m_data.m_ppChannelObj;

							}
							ppObj[g_pcontinuum->m_data.m_nNumChannelObjects] = pscho;
							g_pcontinuum->m_data.m_ppChannelObj = ppObj;
							g_pcontinuum->m_data.m_nNumChannelObjects++;

//							ppObj[o]->m_pszServerName = (char*)malloc(szServer.GetLength()+1); 
//							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, szServer);

//							ppObj[o]->m_nHarrisListID = nListID;

//							if(nChannelID>=0) 
							ppObj[o]->m_nChannelID = nChannelID;

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, szDesc);
							}
//							ppObj[o]->m_usType = not used;
							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|CONTINUUM_FLAG_FOUND;
							else ppObj[o]->m_ulFlags |= CONTINUUM_FLAG_FOUND;
								

// ****  do something here to check out if the list is able to be activated, that is, if it's on a connected connection
							if((ppObj[o]->m_ulFlags)&CONTINUUM_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating list %d%s%s...", 
									ppObj[o]->m_nChannelID,  
									ppObj[o]->m_pszDesc?": ":"", 
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:"");  
								g_pcontinuum->m_msgr.DM(MSG_ICONINFO, NULL, "Continuum:destination_change", errorstring);    Sleep(20);   //(Dispatch message)

// **** if it's able to be activated, set the status to connected.
								ppObj[o]->m_ulStatus = CONTINUUM_STATUS_CONN;
							}

						}
						else
							delete pscho;
					}
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			LeaveCriticalSection(&m_critSQL);

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<g_pcontinuum->m_data.m_nNumChannelObjects)
			{
				if((g_pcontinuum->m_data.m_ppChannelObj)&&(g_pcontinuum->m_data.m_ppChannelObj[nIndex]))
				{
					if((g_pcontinuum->m_data.m_ppChannelObj[nIndex]->m_ulFlags)&CONTINUUM_FLAG_FOUND)
					{
						(g_pcontinuum->m_data.m_ppChannelObj[nIndex]->m_ulFlags) &= ~CONTINUUM_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(g_pcontinuum->m_data.m_ppChannelObj[nIndex])
						{
							if((g_pcontinuum->m_data.m_ppChannelObj[nIndex]->m_ulFlags)&CONTINUUM_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating list %d%s%s...", 
									g_pcontinuum->m_data.m_ppChannelObj[nIndex]->m_nChannelID,  
									g_pcontinuum->m_data.m_ppChannelObj[nIndex]->m_pszDesc?": ":"", 
									g_pcontinuum->m_data.m_ppChannelObj[nIndex]->m_pszDesc?g_pcontinuum->m_data.m_ppChannelObj[nIndex]->m_pszDesc:""); 
								g_pcontinuum->m_msgr.DM(MSG_ICONINFO, NULL, "Continuum:destination_remove", errorstring);     Sleep(20);  //(Dispatch message)
								g_pcontinuum->m_data.m_ppChannelObj[nIndex]->m_ulStatus = CONTINUUM_STATUS_NOTCON;
							}

//							g_pcontinuum->m_data.m_ppChannelObj[nIndex]->m_bKillChannelThread = true;
//							while(g_pcontinuum->m_data.m_ppChannelObj[nIndex]->m_bChannelThreadStarted) Sleep(1);

							delete g_pcontinuum->m_data.m_ppChannelObj[nIndex];
							g_pcontinuum->m_data.m_nNumChannelObjects--;

							int nTemp=nIndex;
							while(nTemp<g_pcontinuum->m_data.m_nNumChannelObjects)
							{
								g_pcontinuum->m_data.m_ppChannelObj[nTemp]=g_pcontinuum->m_data.m_ppChannelObj[nTemp+1];
								nTemp++;
							}
							g_pcontinuum->m_data.m_ppChannelObj[nTemp] = NULL;
						} else nIndex++;
					}
				}
				else
					nIndex++;
			}

//		g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "GetChannels returning");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
		LeaveCriticalSection(&m_critSQL);

	}
	return CONTINUUM_ERROR;
}

int CContinuumData::GetConnections(char* pszInfo)
{
	if((g_pcontinuum)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY ip",  //HARDCODE
			((g_pcontinuum->m_settings.m_pszConnections)&&(strlen(g_pcontinuum->m_settings.m_pszConnections)))?g_pcontinuum->m_settings.m_pszConnections:"Connections");
//g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "GetConnections");  Sleep(250); //(Dispatch message)

		char error[DB_SQLSTRING_MAXLEN];
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, error);
//g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "GetConnections %s", error);  Sleep(250); //(Dispatch message)

		
//		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = CONTINUUM_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
//g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "GetConnections %d", nIndex);  Sleep(250); //(Dispatch message)
				CString szHost="";
//				CString szClient="";
				CString szDesc="";
				CString szTemp;
				unsigned long ulFlags = 0;   // various flags
				unsigned short usPort=10540;
				int nTemp = -1;
				bool bFlagsFound = false;
				bool bFound = false;
				try
				{
					prs->GetFieldValue("ip", szHost);//HARDCODE
					szHost.TrimLeft(); szHost.TrimRight();
//g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "GetConnections %s", szHost);  Sleep(250); //(Dispatch message)
					prs->GetFieldValue("name", szDesc);//HARDCODE
//g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "GetConnections %s", szDesc);  Sleep(250); //(Dispatch message)
					prs->GetFieldValue("enable", szTemp);//HARDCODE
//g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "GetConnections e %s", szTemp);  Sleep(250); //(Dispatch message)
					if(szTemp.GetLength())
					{
						ulFlags = atol(szTemp);
						bFlagsFound  =true;
					}
					prs->GetFieldValue("ports", szTemp);//HARDCODE
//g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "GetConnections p %s", szTemp);  Sleep(250); //(Dispatch message)
					if(szTemp.GetLength())
					{
						nTemp = atol(szTemp);
						if (nTemp!=0) usPort =nTemp;
					}
					prs->GetFieldValue("video_standard", szTemp);//HARDCODE
//g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "GetConnections vs %s", szTemp);  Sleep(250); //(Dispatch message)
					if(szTemp.GetLength())
					{
						nTemp = atol(szTemp);
						if (nTemp!=0) ulFlags |= nTemp;
					}

//					prs->GetFieldValue("client", szClient);//HARDCODE
//					szClient.TrimLeft(); szClient.TrimRight();
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
* /
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

//			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "c1");  Sleep(250); //(Dispatch message)
				if((g_pcontinuum->m_data.m_ppConnObj)&&(g_pcontinuum->m_data.m_nNumConnectionObjects))
				{
					nTemp=0;
					while(nTemp<g_pcontinuum->m_data.m_nNumConnectionObjects)
					{
//			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "c2");  Sleep(250); //(Dispatch message)
						if((g_pcontinuum->m_data.m_ppConnObj[nTemp])&&(g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszServerName))
						{
							if((szHost.GetLength()>0)&&(szHost.CompareNoCase(g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszServerName)==0))
							{
								bFound = true;
//			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "c3");  Sleep(250); //(Dispatch message)

								if(
										((bFlagsFound)&&(!(ulFlags&CONTINUUM_FLAG_ENABLED))&&((g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_ulFlags)&CONTINUUM_FLAG_ENABLED))
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s%s%s...", 
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszServerName,
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc?" on ":"",
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszServerName:""
										);  
									g_pcontinuum->m_msgr.DM(MSG_ICONINFO, NULL, "Continuum:destination_change", errorstring);  Sleep(100);  //(Dispatch message)
//									g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pDlg->OnDisconnect();
									//**** disconnect

//									g_adc.DisconnectServer(g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszServerName);
//									g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_bKillConnThread = true;  // let it finish on its own..

									g_omni.DisconnectServer(g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pCAConn);
									//**** should check return value....
	//								while((g_pcontinuum->m_data.m_ppConnObj[nTemp])&&(g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_bConnThreadStarted)) Sleep(100);
						//			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s is disconnected.", 
						//				g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszServerName);  
						//			g_pcontinuum->m_msgr.DM(MSG_ICONINFO, NULL, "Continuum:destination_change", errorstring);  Sleep(100);  //(Dispatch message)

									g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_ulStatus = CONTINUUM_STATUS_NOTCON;
								}

								if(
										(szDesc.GetLength()>0)
									&&((g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc==NULL)||(szDesc.CompareNoCase(g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc)))
									)
								{
									if(g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc) free(g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc);

									g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc) sprintf(g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc, "%s", szDesc);
								}


								if(
										((bFlagsFound)&&(ulFlags&CONTINUUM_FLAG_ENABLED)&&(!((g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_ulFlags)&CONTINUUM_FLAG_ENABLED)))
									||( (usPort!=g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_usPort)
											&&(
													((bFlagsFound)&&(ulFlags&CONTINUUM_FLAG_ENABLED))
												||((!bFlagsFound)&&((g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_ulFlags)&CONTINUUM_FLAG_ENABLED))
												)
										)
									)
								{

// cant connect again until the last thread has finished
									g_omni.DisconnectServer(g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pCAConn);
									g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_ulStatus = CONTINUUM_STATUS_NOTCON;

									g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_usPort = usPort;

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s%s%s...", 
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszServerName,
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc?" on ":"",
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszServerName:""
										);  
									g_pcontinuum->m_msgr.DM(MSG_ICONINFO, NULL, "Continuum:destination_change", errorstring);   Sleep(100); //(Dispatch message)

/*
g_pcontinuum->m_msgr.DM(MSG_ICONINFO, NULL, "Continuum:debug", "Connecting with %s, %d, %s, %s, %d, %d, %s, %s" ,
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszServerName, 
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_usPort, 
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc, 
										g_pcontinuum->m_settings.m_pszOmnibusDataFields, 
										g_pcontinuum->m_data.m_pdb, 
										g_pcontinuum->m_data.m_pdbConn, 
										g_pcontinuum->m_settings.m_pszLiveEvents,
										g_pcontinuum->m_settings.m_pszExchange
										 );   Sleep(100); //(Dispatch message)
* /

// with SQL
									CCAConn* pConn = g_omni.ConnectServer(
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszServerName, 
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_usPort, 
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc, 
										g_pcontinuum->m_settings.m_pszOmnibusDataFields, 
										g_pcontinuum->m_data.m_pdb, 
										g_pcontinuum->m_data.m_pdbConn, 
										g_pcontinuum->m_settings.m_pszLiveEvents,
										g_pcontinuum->m_settings.m_pszExchange,
										&(g_pcontinuum->m_data.m_critSQL)
										);
/*
// without SQL
									CCAConn* pConn = g_omni.ConnectServer(
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszServerName, 
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_usPort, 
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pszDesc, 
										g_pcontinuum->m_settings.m_pszOmnibusDataFields, 
										NULL, 
										NULL, 
										NULL,
										NULL,
										NULL
										);
* /
									if(pConn)
									{
//									g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pDlg->OnButtonConnect();
									//**** if connect set following status
										g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_ulStatus = CONTINUUM_STATUS_CONN; 

										pConn->SetFrameBasis(ulFlags);
										if((g_pcontinuum->m_settings.m_pszDebugLog)&&(strlen(g_pcontinuum->m_settings.m_pszDebugLog)>0))
											pConn->SetDebugName(g_pcontinuum->m_settings.m_pszDebugLog);
										if((g_pcontinuum->m_settings.m_pszCommLog)&&(strlen(g_pcontinuum->m_settings.m_pszCommLog)>0))
											pConn->SetCommLogName(g_pcontinuum->m_settings.m_pszCommLog);
										pConn->m_bWriteXMLtoFile = g_pcontinuum->m_settings.m_bWriteXML;
										pConn->m_ulCheckInterval = g_pcontinuum->m_settings.m_ulCheckInterval;
										pConn->m_ulExpiryPeriod = g_pcontinuum->m_settings.m_ulExpiryPeriod;
									}
									g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_pCAConn = pConn;

								}

								g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_usPort = usPort;

								if(bFlagsFound) g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_ulFlags = ulFlags|CONTINUUM_FLAG_FOUND;
								else g_pcontinuum->m_data.m_ppConnObj[nTemp]->m_ulFlags |= CONTINUUM_FLAG_FOUND;

							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szHost.GetLength()>0)) // have to add.
				{
//g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "adding %s", szHost);  Sleep(250); //(Dispatch message)
					CContinuumObject* pscno = new CContinuumObject;
					if(pscno)
					{
//g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CContinuumObject** ppObj = new CContinuumObject*[g_pcontinuum->m_data.m_nNumConnectionObjects+1];
						if(ppObj)
						{
//g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((g_pcontinuum->m_data.m_ppConnObj)&&(g_pcontinuum->m_data.m_nNumConnectionObjects>0))
							{
								while(o<g_pcontinuum->m_data.m_nNumConnectionObjects)
								{
									ppObj[o] = g_pcontinuum->m_data.m_ppConnObj[o];
									o++;
								}
								delete [] g_pcontinuum->m_data.m_ppConnObj;

							}
							ppObj[o] = pscno;
							g_pcontinuum->m_data.m_ppConnObj = ppObj;
							g_pcontinuum->m_data.m_nNumConnectionObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szHost.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, "%s", szHost);

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}
//							ppObj[o]->m_usType = not used;
							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|CONTINUUM_FLAG_FOUND;
							else ppObj[o]->m_ulFlags |= CONTINUUM_FLAG_FOUND;
								
							ppObj[o]->m_usPort = usPort;

							if((ppObj[o]->m_ulFlags)&CONTINUUM_FLAG_ENABLED)
							{


								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s%s%s...", 
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName,
									ppObj[o]->m_pszDesc?" on ":"",
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszServerName:""
									);  
								g_pcontinuum->m_msgr.DM(MSG_ICONINFO, NULL, "Continuum:destination_init", errorstring);   Sleep(100); //(Dispatch message)


// with SQL
								CCAConn* pConn = g_omni.ConnectServer(
									ppObj[o]->m_pszServerName, 
									ppObj[o]->m_usPort, 
									ppObj[o]->m_pszDesc, 
									g_pcontinuum->m_settings.m_pszOmnibusDataFields, 
									g_pcontinuum->m_data.m_pdb, 
									g_pcontinuum->m_data.m_pdbConn, 
									g_pcontinuum->m_settings.m_pszLiveEvents,
									g_pcontinuum->m_settings.m_pszExchange,
									&(g_pcontinuum->m_data.m_critSQL)
									);
/*
// without SQL
								CCAConn* pConn = g_omni.ConnectServer(
									ppObj[o]->m_pszServerName, 
									ppObj[o]->m_usPort, 
									ppObj[o]->m_pszDesc, 
									g_pcontinuum->m_settings.m_pszOmnibusDataFields, 
									NULL, 
									NULL, 
									NULL,
									NULL,
									NULL
									);
* /
								if(pConn)
								{
//									ppObj[o]->m_pDlg->OnButtonConnect();
								//**** if connect set following status
									ppObj[o]->m_ulStatus = CONTINUUM_STATUS_CONN; 
									pConn->SetFrameBasis(ppObj[o]->m_ulFlags);

									if((g_pcontinuum->m_settings.m_pszDebugLog)&&(strlen(g_pcontinuum->m_settings.m_pszDebugLog)>0))
										pConn->SetDebugName(g_pcontinuum->m_settings.m_pszDebugLog);
									if((g_pcontinuum->m_settings.m_pszCommLog)&&(strlen(g_pcontinuum->m_settings.m_pszCommLog)>0))
										pConn->SetCommLogName(g_pcontinuum->m_settings.m_pszCommLog);
									pConn->m_bWriteXMLtoFile = g_pcontinuum->m_settings.m_bWriteXML;
									pConn->m_ulCheckInterval = g_pcontinuum->m_settings.m_ulCheckInterval;
									pConn->m_ulExpiryPeriod = g_pcontinuum->m_settings.m_ulExpiryPeriod;
								}
								ppObj[o]->m_pCAConn = pConn;
								
							}
						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
 			LeaveCriticalSection(&m_critSQL);

//			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<g_pcontinuum->m_data.m_nNumConnectionObjects)
			{
				if((g_pcontinuum->m_data.m_ppConnObj)&&(g_pcontinuum->m_data.m_ppConnObj[nIndex]))
				{
					if((g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_ulFlags)&CONTINUUM_FLAG_FOUND)
					{
						(g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_ulFlags) &= ~CONTINUUM_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(g_pcontinuum->m_data.m_ppConnObj[nIndex])
						{
							if((g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_ulFlags)&CONTINUUM_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s%s%s...", 
									g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_pszDesc?g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_pszDesc:g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_pszServerName,
									g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_pszDesc?" on ":"",
									g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_pszDesc?g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_pszServerName:""
									);  
							}

//							g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_bKillConnThread = true;
							// this has to wait because we are deleting.
//							while((g_pcontinuum->m_data.m_ppConnObj[nIndex])&&(g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_bConnThreadStarted)) Sleep(100);

							if((g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_ulFlags)&CONTINUUM_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s is disconnected.", 
									g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_pszDesc?g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_pszDesc:g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_pszServerName);  
								g_pcontinuum->m_msgr.DM(MSG_ICONINFO, NULL, "Continuum:destination_remove", errorstring);   Sleep(100);  //(Dispatch message)

								//g_adc.DisconnectServer(g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_pszServerName);
								//**** should check return value....

//								g_pcontinuum->m_data.m_ppConnObj[nIndex]->m_ulStatus = CONTINUUM_STATUS_NOTCON;
							}

							delete g_pcontinuum->m_data.m_ppConnObj[nIndex];
							g_pcontinuum->m_data.m_nNumConnectionObjects--;

							int nTemp=nIndex;
							while(nTemp<g_pcontinuum->m_data.m_nNumConnectionObjects)
							{
								g_pcontinuum->m_data.m_ppConnObj[nTemp]=g_pcontinuum->m_data.m_ppConnObj[nTemp+1];
								nTemp++;
							}
							g_pcontinuum->m_data.m_ppConnObj[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

//g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "leaving GetConnections with %d", nIndex);  Sleep(250); //(Dispatch message)
			return nReturn;
		}
//	else g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "GetConnections was NULL");  Sleep(250); //(Dispatch message)
		LeaveCriticalSection(&m_critSQL);

	}
//			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return CONTINUUM_ERROR;
}

*/

