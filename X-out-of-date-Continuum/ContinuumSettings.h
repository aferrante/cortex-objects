// ContinuumSettings.h: interface for the CContinuumSettings class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CONTINUUMSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
#define AFX_CONTINUUMSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ContinuumDefines.h"
#include "../../Common/MFC/ODBC/DBUtil.h"

class CContinuumSettings  
{
public:
	CContinuumSettings();
	virtual ~CContinuumSettings();

//	int GetFromDatabase(char* pszInfo=NULL);  //only get.  Interface publishes

//	CDBUtil* m_pdb;
//	CDBconn* m_pdbConn;

	char* m_pszName;  // familiar name of this instance (and therefore the name of the Continuum database)
	unsigned long m_ulMainMode;

	// ports
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;

	// The following flags determine what settings are loaded. 
	// The specifics do not need to be stored in cortex settings, they are pushed to the various objects.

	// messaging for Continuum
	bool m_bUseLog;			 // write a log file
	bool m_bUseEmail;		 // send an email on failures (or commanded from remotes)
	bool m_bUseNetwork;  // send a message to a remote host
// if both of the following are false, the only ways to exit are via network command or task manager
	bool m_bUseTrayIcon; // has its own tray icon
	bool m_bUseDialog;   // has a dialog window

	// database stuff
//	char* m_pszDSN;
//	char* m_pszUser;
//	char* m_pszPW;

//	char* m_pszSettings;  // the Settings table name
//	char* m_pszExchange;  // the Exchange table name
//	char* m_pszMessages;  // the Messages table name

	unsigned long m_ulModsIntervalMS;  // interval on which to check database mods
	unsigned long m_ulSysTrayIntervalMS;  // interval on which to check database mods

	char* m_pszLicense;  // the License Key
//	char* m_pszIconPath;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

	//

};

#endif // !defined(AFX_CONTINUUMSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
