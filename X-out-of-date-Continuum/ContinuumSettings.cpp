// ContinuumSettings.cpp: implementation of the CContinuumSettings.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "ContinuumDefines.h"
#include "ContinuumSettings.h"
#include "ContinuumMain.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CContinuumMain* g_pcontinuum;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CContinuumSettings::CContinuumSettings()
{
//	m_pdb = NULL;
//	m_pdbConn = NULL;

	m_pszName = NULL;
	m_ulMainMode = CONTINUUM_MODE_DEFAULT;

	// ports
	m_usCommandPort	= CONTINUUM_PORT_CMD;
	m_usStatusPort	= CONTINUUM_PORT_STATUS;

	// messaging for Continuum
	m_bUseLog = true;			// write a log file
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host
// if both of the following are false, the only ways to exit are via network command or task manager
	m_bUseTrayIcon = true; // has its own tray icon
	m_bUseDialog = false;   // has a dialog window

	// Harris API
//	m_bUseListCount = false; // get all events up until the list count (otherwise just up to the lookahead)

	// DSN params
//	m_pszDSN = NULL;
//	m_pszUser = NULL;
//	m_pszPW = NULL;

//	m_pszSettings = NULL;  // the Settings table name
//	m_pszExchange = NULL;  // the Exchange table name
//	m_pszMessages = NULL;  // the Messages table name

	m_ulModsIntervalMS = 6000;
	m_ulSysTrayIntervalMS = 120000; // two minutes!

	m_pszLicense=NULL;  // the License Key
}

CContinuumSettings::~CContinuumSettings()
{
	if(m_pszName) free(m_pszName); // must use malloc to allocate
//	if(m_pszDSN) free(m_pszDSN); // must use malloc to allocate
//	if(m_pszUser) free(m_pszUser); // must use malloc to allocate
//	if(m_pszPW) free(m_pszPW); // must use malloc to allocate
//	if(m_pszSettings) free(m_pszSettings); // must use malloc to allocate
//	if(m_pszExchange) free(m_pszExchange); // must use malloc to allocate
//	if(m_pszMessages) free(m_pszMessages); // must use malloc to allocate

	if(m_pszLicense) free(m_pszLicense); // must use malloc to allocate
}

/*
int CContinuumSettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	if((m_pszSettings)&&(strlen(m_pszSettings)>0)&&(m_pdbConn)&&(m_pdb)&&(m_pdbConn->m_bConnected))
	{
		// get settings.
		char pszFilename[MAX_PATH];

		strcpy(pszFilename, CONTINUUM_SETTINGS_FILE_DEFAULT);  // continuum settings file

		CFileUtil file;
		file.GetSettings(pszFilename, false); 

		// load up the values on the settings object
		if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
		{
			g_pcontinuum->m_settings.m_pszName = file.GetIniString("Main", "Name", "Continuum");
			g_pcontinuum->m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");
			g_pcontinuum->m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");

			g_pcontinuum->m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", CONTINUUM_PORT_CMD);
			g_pcontinuum->m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", CONTINUUM_PORT_STATUS);

			g_pcontinuum->m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			g_pcontinuum->m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			g_pcontinuum->m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;

			g_pcontinuum->m_settings.m_pszOmnibusDataFields = file.GetIniString("OmnibusXML", "DataFields", "data,logos");  // the data fields we care about
			g_pcontinuum->m_settings.m_pszDebugLog = file.GetIniString("OmnibusXML", "DebugLog", NULL); 
			g_pcontinuum->m_settings.m_pszCommLog = file.GetIniString("OmnibusXML", "CommLog", NULL); 
			g_pcontinuum->m_settings.m_bWriteXML = file.GetIniInt("OmnibusXML", "WriteXML", 0)?true:false;  

			g_pcontinuum->m_settings.m_ulCheckInterval=file.GetIniInt("OmnibusXML", "PurgeCheckInterval", 0);	// number of seconds between checks (0 turns off)
			g_pcontinuum->m_settings.m_ulExpiryPeriod=file.GetIniInt("OmnibusXML", "PurgeExpiryPeriod", 10800);	// (default value 3 hours) when the event's end time (start plus duration) is more than this number of seconds older than the current server time, it is purged.

			g_pcontinuum->m_settings.m_pszDSN = file.GetIniString("Database", "DSN", g_pcontinuum->m_settings.m_pszName?g_pcontinuum->m_settings.m_pszName:"Continuum");
			g_pcontinuum->m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
			g_pcontinuum->m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
			g_pcontinuum->m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
			g_pcontinuum->m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
			g_pcontinuum->m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name

			g_pcontinuum->m_settings.m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels");  // the Channels table name
			g_pcontinuum->m_settings.m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Adaptors");  // the Connections table name
			g_pcontinuum->m_settings.m_pszLiveEvents = file.GetIniString("Database", "LiveEventsTableName", "Events");  // the LiveEvents table name

			g_pcontinuum->m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
		}
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
		EnterCriticalSection(&g_pcontinuum->m_data.m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = CONTINUUM_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCategory="";
				CString szParameter="";
				CString szValue="";
				CString szTemp="";
				int min, max;
				bool bmin = false, bmax = false;
				try
				{
					prs->GetFieldValue("category", szCategory);  //HARDCODE
					prs->GetFieldValue("parameter", szParameter);  //HARDCODE
					prs->GetFieldValue("value", szValue);  //HARDCODE
					prs->GetFieldValue("min_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						min = atoi(szTemp);
						bmin = true;
					}
					prs->GetFieldValue("max_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						max = atoi(szTemp);
						bmax = true;
					}
				}
				catch( ... )
				{
				}

				int nLength = szValue.GetLength();
				if(szCategory.CompareNoCase("Main")==0)
				{
					if(szParameter.CompareNoCase("Name")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszName) free(m_pszName);
								m_pszName = pch;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("License")==0)
				{
					if(szParameter.CompareNoCase("Key")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLicense) free(m_pszLicense);
								m_pszLicense = pch;

								// recompile license key params
								if(g_pcontinuum->m_data.m_key.m_pszLicenseString) free(g_pcontinuum->m_data.m_key.m_pszLicenseString);
								g_pcontinuum->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
								if(g_pcontinuum->m_data.m_key.m_pszLicenseString)
								sprintf(g_pcontinuum->m_data.m_key.m_pszLicenseString, "%s", szValue);

								g_pcontinuum->m_data.m_key.InterpretKey();

								char errorstring[MAX_MESSAGE_LENGTH];
								if(g_pcontinuum->m_data.m_key.m_bValid)
								{
									unsigned long i=0;
									while(i<g_pcontinuum->m_data.m_key.m_ulNumParams)
									{
										if((g_pcontinuum->m_data.m_key.m_ppszParams)
											&&(g_pcontinuum->m_data.m_key.m_ppszValues)
											&&(g_pcontinuum->m_data.m_key.m_ppszParams[i])
											&&(g_pcontinuum->m_data.m_key.m_ppszValues[i]))
										{
											if(stricmp(g_pcontinuum->m_data.m_key.m_ppszParams[i], "max")==0)
											{
//												g_pcontinuum->m_data.m_nMaxLicensedDevices = atoi(g_pcontinuum->m_data.m_key.m_ppszValues[i]);
											}
										}
										i++;
									}
								
									if(
											(
												(!g_pcontinuum->m_data.m_key.m_bExpires)
											||((g_pcontinuum->m_data.m_key.m_bExpires)&&(!g_pcontinuum->m_data.m_key.m_bExpired))
											||((g_pcontinuum->m_data.m_key.m_bExpires)&&(g_pcontinuum->m_data.m_key.m_bExpireForgiveness)&&(g_pcontinuum->m_data.m_key.m_ulExpiryDate+g_pcontinuum->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
											)
										&&(
												(!g_pcontinuum->m_data.m_key.m_bMachineSpecific)
											||((g_pcontinuum->m_data.m_key.m_bMachineSpecific)&&(g_pcontinuum->m_data.m_key.m_bValidMAC))
											)
										)
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
										g_pcontinuum->m_data.m_ulFlags &= ~CONTINUUM_ICON_MASK;
										g_pcontinuum->m_data.m_ulFlags |= CONTINUUM_STATUS_OK;
										g_pcontinuum->m_data.SetStatusText(errorstring, g_pcontinuum->m_data.m_ulFlags);
									}
									else
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
										g_pcontinuum->m_data.m_ulFlags &= ~CONTINUUM_ICON_MASK;
										g_pcontinuum->m_data.m_ulFlags |= CONTINUUM_STATUS_ERROR;
										g_pcontinuum->m_data.SetStatusText(errorstring, g_pcontinuum->m_data.m_ulFlags);
									}
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
									g_pcontinuum->m_data.m_ulFlags &= ~CONTINUUM_ICON_MASK;
									g_pcontinuum->m_data.m_ulFlags |= CONTINUUM_STATUS_ERROR;
									g_pcontinuum->m_data.SetStatusText(errorstring, g_pcontinuum->m_data.m_ulFlags);
								}

							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("CommandServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usCommandPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("StatusServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usStatusPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Messager")==0)
				{
					if(szParameter.CompareNoCase("UseEmail")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseEmail = true;
							else m_bUseEmail = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseNet")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseNetwork = true;
							else m_bUseNetwork = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseLog = true;
							else m_bUseLog = false;
						}
					}
				}
				else
				if(szCategory.CompareNoCase("OmnibusXML")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("DataFields")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszOmnibusDataFields) free(m_pszOmnibusDataFields);
								m_pszOmnibusDataFields = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("DebugLog")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszDebugLog) free(m_pszDebugLog);
								m_pszDebugLog = pch;

								int i=0;
								while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
								{
									if(g_omni.m_ppConn[i]) g_omni.m_ppConn[i]->SetDebugName(m_pszDebugLog);
									i++;
								}
							}
						}
						else
						{
							int i=0;
							while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
							{
								if(g_omni.m_ppConn[i])
								{
									if(g_omni.m_ppConn[i]->m_pszDebugFile) free(g_omni.m_ppConn[i]->m_pszDebugFile);
									g_omni.m_ppConn[i]->m_pszDebugFile =NULL;
								}
								i++;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("CommLog")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszCommLog) free(m_pszCommLog);
								m_pszCommLog = pch;

								int i=0;
								while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
								{
									if(g_omni.m_ppConn[i]) g_omni.m_ppConn[i]->SetCommLogName(m_pszCommLog);
									i++;
								}
							}
						}
						else
						{
							int i=0;
							while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
							{
								if(g_omni.m_ppConn[i])
								{
									if(g_omni.m_ppConn[i]->m_pszCommFile) free(g_omni.m_ppConn[i]->m_pszCommFile);
									g_omni.m_ppConn[i]->m_pszCommFile =NULL;
								}
								i++;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("WriteXML")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bWriteXML = true;
							else m_bWriteXML = false;

							int i=0;
							while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
							{
								if(g_omni.m_ppConn[i]) g_omni.m_ppConn[i]->m_bWriteXMLtoFile = m_bWriteXML;
								i++;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("PurgeCheckInterval")==0)
					{
						if(nLength>0)
						{
							m_ulCheckInterval = atol(szValue);
							int i=0;
							while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
							{
								if(g_omni.m_ppConn[i]) g_omni.m_ppConn[i]->m_ulCheckInterval = m_ulCheckInterval;
								i++;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("PurgeExpiryPeriod")==0)
					{
						if(nLength>0)
						{
							m_ulExpiryPeriod = atol(szValue);
							int i=0;
							while((g_omni.m_ppConn)&&(i<g_omni.m_usNumConn))
							{
								if(g_omni.m_ppConn[i]) g_omni.m_ppConn[i]->m_ulExpiryPeriod = m_ulExpiryPeriod;
								i++;
							}
						}
					}
				}
				else 
				if(szCategory.CompareNoCase("Database")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("SettingsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszSettings) free(m_pszSettings);
								m_pszSettings = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ExchangeTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszExchange) free(m_pszExchange);
								m_pszExchange = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("MessagesTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszMessages) free(m_pszMessages);
								m_pszMessages = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ChannelsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszChannels) free(m_pszChannels);
								m_pszChannels = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ConnectionsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszConnections) free(m_pszConnections);
								m_pszConnections = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("LiveEventsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLiveEvents) free(m_pszLiveEvents);
								m_pszLiveEvents = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ModificationCheckInterval")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_ulModsIntervalMS = nLength;
						}
					}
				}


				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			if(pszInfo)
			{
				_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
			}
			delete prs;
			LeaveCriticalSection(&g_pcontinuum->m_data.m_critSQL);

			if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
			{
				// these explicts arent necessary - uncomment to write out a full file to edit...
				file.SetIniString("Main", "Name", g_pcontinuum->m_settings.m_pszName);
				file.SetIniInt("CommandServer", "ListenPort", g_pcontinuum->m_settings.m_usCommandPort);
				file.SetIniInt("StatusServer", "ListenPort", g_pcontinuum->m_settings.m_usStatusPort);
				file.SetIniString("License", "Key", g_pcontinuum->m_settings.m_pszLicense);

				file.SetIniString("FileServer", "IconPath", g_pcontinuum->m_settings.m_pszIconPath);

				file.SetIniInt("Messager", "UseEmail", g_pcontinuum->m_settings.m_bUseEmail?1:0);
				file.SetIniInt("Messager", "UseNet", g_pcontinuum->m_settings.m_bUseNetwork?1:0);
				file.SetIniInt("Messager", "UseLog", g_pcontinuum->m_settings.m_bUseLog?1:0);

				file.SetIniString("Database", "DSN", g_pcontinuum->m_settings.m_pszDSN);
				file.SetIniString("Database", "DBUser", g_pcontinuum->m_settings.m_pszUser);
				file.SetIniString("Database", "DBPassword", g_pcontinuum->m_settings.m_pszPW);
				file.SetIniString("Database", "SettingsTableName", g_pcontinuum->m_settings.m_pszSettings);  // the Settings table name
				file.SetIniString("Database", "ExchangeTableName", g_pcontinuum->m_settings.m_pszExchange);  // the Exchange table name
				file.SetIniString("Database", "MessagesTableName", g_pcontinuum->m_settings.m_pszMessages);  // the Messages table name

				file.SetIniString("OmnibusXML", "DataFields", g_pcontinuum->m_settings.m_pszOmnibusDataFields);  // the data fields we care about
				file.SetIniString("OmnibusXML", "DebugLog", g_pcontinuum->m_settings.m_pszDebugLog);  
				file.SetIniString("OmnibusXML", "CommLog", g_pcontinuum->m_settings.m_pszCommLog);  
				file.SetIniInt("OmnibusXML", "WriteXML", g_pcontinuum->m_settings.m_bWriteXML?1:0);  
				file.SetIniInt("OmnibusXML", "PurgeCheckInterval", g_pcontinuum->m_settings.m_ulCheckInterval);	// number of seconds between checks (0 turns off)
				file.SetIniInt("OmnibusXML", "PurgeExpiryPeriod", g_pcontinuum->m_settings.m_ulExpiryPeriod);	// (default value 3 hours) when the event's end time (start plus duration) is more than this number of seconds older than the current server time, it is purged.

				file.SetIniString("Database", "ChannelsTableName", g_pcontinuum->m_settings.m_pszChannels);  // the Channels table name
				file.SetIniString("Database", "ConnectionsTableName", g_pcontinuum->m_settings.m_pszConnections);  // the Connections table name
				file.SetIniString("Database", "LiveEventsTableName", g_pcontinuum->m_settings.m_pszLiveEvents);  // the LiveEvents table name

				file.SetIniInt("Database", "ModificationCheckInterval", g_pcontinuum->m_settings.m_ulModsIntervalMS);  // in milliseconds

				file.SetSettings(CONTINUUM_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

			}



			return CONTINUUM_SUCCESS;
		}
		LeaveCriticalSection(&g_pcontinuum->m_data.m_critSQL);

	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s%s%s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. ",
				m_pdb?"":"Database pointer was NULL. ",
				m_pdbConn?"":"Connection pointer was NULL. ",
				((m_pdbConn)&&(!m_pdbConn->m_bConnected))?"Database not connected. ":""
				);
		}
	}
	return CONTINUUM_ERROR;
}

*/
