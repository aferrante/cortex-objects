// ContinuumDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(CONTINUUMDEFINES_H_INCLUDED)
#define CONTINUUMDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define CONTINUUM_CURRENT_VERSION		"1.0.0.1"


// modes
#define CONTINUUM_MODE_DEFAULT			0x00000000  // exclusive
#define CONTINUUM_MODE_LISTENER			0x00000001  // exclusive
#define CONTINUUM_MODE_CLONE				0x00000002  // exclusive
#define CONTINUUM_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define CONTINUUM_MODE_VOLATILE			0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define CONTINUUM_MODE_MASK					0x0000000f  // 

// default port values.
//#define CONTINUUM_PORT_FILE				80		
#define CONTINUUM_PORT_CMD					10562		
#define CONTINUUM_PORT_STATUS				10563		

#define CONTINUUM_PORT_INVALID			0	

#define CONTINUUM_FLAG_DISABLED			0x0000	 // default
#define CONTINUUM_FLAG_ENABLED			0x0001	
#define CONTINUUM_FLAG_FOUND				0x1000

#define CONTINUUM_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded


// status
#define CONTINUUM_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define CONTINUUM_STATUS_UNKNOWN						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define CONTINUUM_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define CONTINUUM_STATUS_ERROR							0x00000020  // error (red icon)
#define CONTINUUM_STATUS_CONN								0x00000030  // ready (green icon)	
#define CONTINUUM_STATUS_OK									0x00000030  // ready (green icon)	
#define CONTINUUM_STATUS_RUN								0x00000040  // in progress, running, owned etc (blue icon);	
#define CONTINUUM_ICON_MASK									0x00000070  // mask	

#define CONTINUUM_STATUS_SUSPEND						0x00000080  // suspended	(yellow icon please)

#define CONTINUUM_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define CONTINUUM_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define CONTINUUM_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define CONTINUUM_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define CONTINUUM_STATUS_CMDSVR_MASK				0x00007000  // command server mask bits

#define CONTINUUM_STATUS_STATUSSVR_START		0x00010000  // starting the status server
#define CONTINUUM_STATUS_STATUSSVR_RUN			0x00020000  // status server running
#define CONTINUUM_STATUS_STATUSSVR_END			0x00030000  // status server shutting down
#define CONTINUUM_STATUS_STATUSSVR_ERROR		0x00040000  // status server error
#define CONTINUUM_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define CONTINUUM_STATUS_THREAD_START				0x00100000  // starting the main thread
#define CONTINUUM_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define CONTINUUM_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define CONTINUUM_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define CONTINUUM_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define CONTINUUM_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define CONTINUUM_STATUS_THREAD_MASK					0x00f00000  // main thread mask bits

// various failures...
#define CONTINUUM_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define CONTINUUM_STATUS_FAIL_DB							0x20000000  // could not get DB
#define CONTINUUM_STATUS_FAIL_MASK						0xf0000000  // failure code mask bits

//return values
#define CONTINUUM_SUCCESS   0
#define CONTINUUM_ERROR	   -1

// default filenames
#define CONTINUUM_SETTINGS_FILE_DEFAULT	  "continuum.csf"		// csf = cortex settings file



#endif // !defined(CONTINUUMDEFINES_H_INCLUDED)
