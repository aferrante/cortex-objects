; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CContinuumDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Continuum.h"
LastPage=0

ClassCount=6
Class1=CAboutDlg
Class2=CContinuumApp
Class3=CContinuumHandler
Class4=CContinuumDlg
Class5=CContinuumSettings

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDD_CONTINUUM_DIALOG
Resource3=IDR_MENU1

[CLS:CAboutDlg]
Type=0
HeaderFile=continuumdlg.h
ImplementationFile=continuumdlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CAboutDlg

[CLS:CContinuumApp]
Type=0
BaseClass=CWinApp
HeaderFile=Continuum.h
ImplementationFile=Continuum.cpp
Filter=N
VirtualFilter=AC
LastObject=CContinuumApp

[CLS:CContinuumHandler]
Type=0
BaseClass=CWnd
HeaderFile=ContinuumHandler.h
ImplementationFile=ContinuumHandler.cpp
Filter=W
VirtualFilter=WC
LastObject=CContinuumHandler

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=8
Control1=IDC_STATICTEXT_TITLE,static,1342308481
Control2=IDC_STATICTEXT_URL,static,1208090625
Control3=IDOK,button,1342373889
Control4=IDC_STATIC_LOGO,static,1342177294
Control5=IDC_URLFRAME,static,1342177298
Control6=IDC_STATIC_BUILD,static,1342308353
Control7=IDC_STATIC_URL,static,1342177294
Control8=IDC_STATICTEXT_COPYRIGHT,static,1342308353

[MNU:IDR_MENU1]
Type=1
Class=?
Command1=ID_CMD_ABOUT
Command2=ID_CMD_SHOWWND
Command3=ID_CMD_EXIT
Command4=ID_CMD_ABOUT
Command5=ID_CMD_SHOWWND
Command6=ID_CMD_EXIT
Command7=ID_CMD_ABOUT
Command8=ID_CMD_EXIT
CommandCount=8

[CLS:CContinuumDlg]
Type=0
HeaderFile=ContinuumDlg.h
ImplementationFile=ContinuumDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDCANCEL

[CLS:CContinuumSettings]
Type=0
HeaderFile=ContinuumSettings.h
ImplementationFile=ContinuumSettings.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CContinuumSettings

[DLG:IDD_CONTINUUM_DIALOG]
Type=1
Class=CContinuumDlg
ControlCount=7
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_SETTINGS,button,1208025216
Control4=IDC_LIST1,SysListView32,1350681613
Control5=IDC_STATIC_STATUSTEXT,static,1342308352
Control6=IDC_STATIC_PROGBAR,static,1342177294
Control7=IDC_TREE1,SysTreeView32,1082130487

