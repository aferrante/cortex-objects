// ContinuumMain.cpp: implementation of the CContinuumMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "Continuum.h"  // just included to have access to windowing environment
#include "ContinuumDlg.h"  // just included to have access to windowing environment
#include "ContinuumHandler.h"  // just included to have access to windowing environment

#include "ContinuumMain.h"
#include <process.h>
#include "../../Common/TXT/BufferUtil.h"
#include "../../Common/MFC/ODBC/DBUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;
CContinuumMain* g_pcontinuum=NULL;

extern CMessager* g_pmsgr;  // from Messager.cpp
extern CContinuumApp theApp;

void ContinuumConnectionThread(void* pvArgs);
void ContinuumListThread(void* pvArgs);


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CContinuumMain::CContinuumMain()
{
}

CContinuumMain::~CContinuumMain()
{
}

/*

char*	CContinuumMain::ContinuumTranslate(CHTTPHeader* pHeader, char* pszBuffer)			// apply continuum scripting language
{
	return pszBuffer;
}

int		CContinuumMain::InterpretDirective(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the directive, such as a char buffer.
	return CONTINUUM_SUCCESS;
}
*/

SOCKET*	CContinuumMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // continuum initiates a request to an object server
{
	CNetData* pdata = new CNetData;

	pdata->m_ucType = ucType;      // defined type - indicates which protocol to use, structure of data

	pdata->m_ucCmd = ucCmd;       // the command byte
	pdata->m_ucSubCmd = ucSubCmd;       // the subcommand byte

	pdata->m_pucData = pucData;
	pdata->m_ulDataLen = ulDataLen;

	SOCKET* ps = NULL; 
	m_net.SendData(pdata, pchHost, usPort, 5000, 0, NET_SND_CMDTOSVR, ps);
//	m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);

	return ps;
}

int		CContinuumMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// continuum replies to an object server after receiving data. (usually ack or nak)
{
	return CONTINUUM_SUCCESS;
}

int		CContinuumMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// continuum answers a request from an object client
{
	return CONTINUUM_SUCCESS;
}


void ContinuumMainThread(void* pvArgs)
{
	CContinuumApp* pApp = (CContinuumApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment
	//startup.
	g_bThreadStarted = true;

	//create the main objects.

	CContinuumMain continuum;
//	CDBUtil db;

	continuum.m_data.m_ulFlags &= ~CONTINUUM_STATUS_THREAD_MASK;
	continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_THREAD_START;
	continuum.m_data.m_ulFlags &= ~CONTINUUM_ICON_MASK;
	continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_UNINIT;

	continuum.m_data.GetHost();

	g_pcontinuum = &continuum;


	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
//	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\Continuum\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}



//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

//		AfxMessageBox( pApp->m_lpCmdLine );

	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// check here for Cortex IP.
			if(strlen(pch))
			{
//		AfxMessageBox( pch );
				if(continuum.m_data.m_bu.CountChar(pch, strlen(pch), ':')>=1)  // must be of form "hostname:cmdport" where hostname can be name or ip
				{
					continuum.m_data.m_pszCortexHost = (char*)malloc(strlen(pch)+1);
					if(continuum.m_data.m_pszCortexHost)
					{
						strcpy(continuum.m_data.m_pszCortexHost, pch);
//		AfxMessageBox( continuum.m_data.m_pszCortexHost );

						char* pchd = strchr(continuum.m_data.m_pszCortexHost, ':');
						if(pchd)
						{
							*pchd = 0;  // null term the host name;
							pchd++;
//		AfxMessageBox( continuum.m_data.m_pszCortexHost );

							char* pchd2 = strchr(pchd, ':');
							if(pchd2)
							{
								*pchd2 = 0;  // null term the cmd port;
								pchd2++;
								if(strlen(pchd2)) continuum.m_data.m_usCortexStatusPort = atoi(pchd2);
							}

							if(strlen(pchd)) continuum.m_data.m_usCortexCommandPort = atoi(pchd);
						}
					}

				}
			}
			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }

	// get settings.
	char pszFilename[MAX_PATH];

	strcpy(pszFilename, CONTINUUM_SETTINGS_FILE_DEFAULT);  // continuum settings file

	CFileUtil file;
	file.GetSettings(pszFilename, false); 
	char* pszParams = NULL;
// load up the values on the settings object
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		continuum.m_settings.m_pszName = file.GetIniString("Main", "Name", "Continuum");
		continuum.m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");

		// compile license key params
		if(g_pcontinuum->m_data.m_key.m_pszLicenseString) free(g_pcontinuum->m_data.m_key.m_pszLicenseString);
		g_pcontinuum->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(continuum.m_settings.m_pszLicense)+1);
		if(g_pcontinuum->m_data.m_key.m_pszLicenseString)
		sprintf(g_pcontinuum->m_data.m_key.m_pszLicenseString, "%s", continuum.m_settings.m_pszLicense);

		g_pcontinuum->m_data.m_key.InterpretKey();

		if(g_pcontinuum->m_data.m_key.m_bValid)
		{
			unsigned long i=0;
			while(i<g_pcontinuum->m_data.m_key.m_ulNumParams)
			{
				if((g_pcontinuum->m_data.m_key.m_ppszParams)
					&&(g_pcontinuum->m_data.m_key.m_ppszValues)
					&&(g_pcontinuum->m_data.m_key.m_ppszParams[i])
					&&(g_pcontinuum->m_data.m_key.m_ppszValues[i]))
				{
					if(stricmp(g_pcontinuum->m_data.m_key.m_ppszParams[i], "max")==0)
					{
//						g_pcontinuum->m_data.m_nMaxLicensedDevices = atoi(g_pcontinuum->m_data.m_key.m_ppszValues[i]);
					}
				}
				i++;
			}

			if(
					(
				    (!continuum.m_data.m_key.m_bExpires)
					||((continuum.m_data.m_key.m_bExpires)&&(!continuum.m_data.m_key.m_bExpired))
					||((continuum.m_data.m_key.m_bExpires)&&(continuum.m_data.m_key.m_bExpireForgiveness)&&(continuum.m_data.m_key.m_ulExpiryDate+continuum.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
				    (!continuum.m_data.m_key.m_bMachineSpecific)
					||((continuum.m_data.m_key.m_bMachineSpecific)&&(continuum.m_data.m_key.m_bValidMAC))
					)
				)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
				continuum.m_data.m_ulFlags &= ~CONTINUUM_ICON_MASK;
				continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_OK;
				continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				continuum.m_data.m_ulFlags &= ~CONTINUUM_ICON_MASK;
				continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_ERROR;
				continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
			continuum.m_data.m_ulFlags &= ~CONTINUUM_ICON_MASK;
			continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_ERROR;
			continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
		}

		continuum.m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", CONTINUUM_PORT_CMD);
		continuum.m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", CONTINUUM_PORT_STATUS);

		continuum.m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
		continuum.m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
		continuum.m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
		continuum.m_settings.m_bUseTrayIcon = file.GetIniInt("Messager", "UseTrayIcon", 1)?true:false;
		continuum.m_settings.m_bUseDialog = file.GetIniInt("Messager", "UseDialog", 0)?true:false;


/*
		continuum.m_settings.m_pszDSN = file.GetIniString("Database", "DSN", continuum.m_settings.m_pszName?continuum.m_settings.m_pszName:"Continuum");
		continuum.m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
		continuum.m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
		continuum.m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
		continuum.m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
		continuum.m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name
*/

		continuum.m_settings.m_ulModsIntervalMS = file.GetIniInt("Application", "ModificationCheckInterval", 5000);  // in milliseconds
		continuum.m_settings.m_ulSysTrayIntervalMS = file.GetIniInt("Application", "SysTrayCheckInterval", 120000);  // in milliseconds

		if(pszParams) free(pszParams); pszParams=NULL;
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
	}

	bool bUseLog = false;
	if(continuum.m_settings.m_bUseLog)
	{
		bUseLog = continuum.m_settings.m_bUseLog;

		// for logfiles, we need params, and they must be in this format:
		//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
		pszParams = file.GetIniString("Messager", "LogFileIni", "Continuum|YD||1|");
		int nRegisterCode=0;

		nRegisterCode = continuum.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, "log", pszParams, errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
			continuum.m_data.m_ulFlags |= (CONTINUUM_STATUS_FAIL_LOG|CONTINUUM_STATUS_ERROR);
			continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
		}

		if(pszParams) free(pszParams); pszParams=NULL;
	}

	continuum.m_msgr.DM(MSG_ICONINFO, NULL, "Continuum", "--------------------------------------------------\n\
-------------- Continuum %s start --------------", CONTINUUM_CURRENT_VERSION);  //(Dispatch message)


//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
//	continuum.m_http.InitializeMessaging(&continuum.m_msgr);
	continuum.m_net.InitializeMessaging(&continuum.m_msgr);
//	g_omni.InitializeMessaging(&continuum.m_msgr);


// TODO: here, get the DB and pull any override settings.
	//****
/*
	CDBconn* pdbConn = db.CreateNewConnection(continuum.m_settings.m_pszDSN, continuum.m_settings.m_pszUser, continuum.m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			continuum.m_data.m_ulFlags |= (CONTINUUM_STATUS_FAIL_DB|CONTINUUM_STATUS_ERROR);
			continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
			continuum.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Continuum:database_connect", errorstring);  //(Dispatch message)
		}
		else
		{
			continuum.m_settings.m_pdbConn = pdbConn;
			continuum.m_settings.m_pdb = &db;
			continuum.m_data.m_pdbConn = pdbConn;
			continuum.m_data.m_pdb = &db;
			if(continuum.m_settings.GetFromDatabase(errorstring)<CONTINUUM_SUCCESS)
			{
				continuum.m_data.m_ulFlags |= (CONTINUUM_STATUS_FAIL_DB|CONTINUUM_STATUS_ERROR);
				continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
				continuum.m_msgr.DM(MSG_ICONERROR, NULL, "Continuum:database_get", errorstring);  //(Dispatch message)
			}
			else
			{

				if(
						(continuum.m_settings.m_pszLiveEvents)
					&&(strlen(continuum.m_settings.m_pszLiveEvents)>0)
					)
				{

					char szSQL[DB_SQLSTRING_MAXLEN];
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
														continuum.m_settings.m_pszLiveEvents  // table name
													);
//			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "ContinuumChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

					EnterCriticalSection(&g_pcontinuum->m_data.m_critSQL);
					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						//**MSG
					}
					LeaveCriticalSection(&g_pcontinuum->m_data.m_critSQL);

					EnterCriticalSection(&g_pcontinuum->m_data.m_critIncrement);
					if (continuum.m_data.IncrementDatabaseMods(continuum.m_settings.m_pszLiveEvents, errorstring)<CONTINUUM_SUCCESS)
					{
						//**MSG
					}
					LeaveCriticalSection(&g_pcontinuum->m_data.m_critIncrement);

				}

				if((!continuum.m_settings.m_bUseLog)&&(bUseLog))
				{
					// reset it
					continuum.m_msgr.DM(MSG_ICONINFO, NULL, "Continuum", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

					Sleep(250); // let the message get there.
					continuum.m_msgr.RemoveDestination("log");

				}
			}
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", continuum.m_settings.m_pszDSN, continuum.m_settings.m_pszUser, continuum.m_settings.m_pszPW); 
		continuum.m_data.m_ulFlags |= (CONTINUUM_STATUS_FAIL_DB|CONTINUUM_STATUS_ERROR);
		continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
		continuum.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Continuum:database_init", errorstring);  //(Dispatch message)

		//**MSG
	}

*/

//init command and status listeners.
	continuum.m_data.m_ulFlags &= ~CONTINUUM_STATUS_CMDSVR_MASK;
	continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_CMDSVR_START;

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing command server on %d", continuum.m_settings.m_usCommandPort); 
	continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
	continuum.m_msgr.DM(MSG_ICONNONE, NULL, "Continuum:command_server_init", errorstring);  Sleep(100); //(Dispatch message)

	if(continuum.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = continuum.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "ContinuumCommandServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = ContinuumCommandHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &continuum;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &continuum.m_net;					// pointer to the object with the Message function.


		if(continuum.m_net.StartServer(pServer, &continuum.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			continuum.m_data.m_ulFlags &= ~CONTINUUM_STATUS_CMDSVR_MASK;
			continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_CMDSVR_ERROR;
			continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
			continuum.m_msgr.DM(MSG_ICONERROR, NULL, "Continuum:command_server_init", errorstring);  //(Dispatch message)
//			continuum.SendMsg(CX_SENDMSG_ERROR, "Continuum:command_server_init", errorstring);
		}
		else
		{
//AfxMessageBox("Q1");
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Command server listening on %d", continuum.m_settings.m_usCommandPort);
			continuum.m_data.m_ulFlags &= ~CONTINUUM_STATUS_CMDSVR_MASK;
			continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_CMDSVR_RUN;
			continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
			continuum.m_msgr.DM(MSG_ICONNONE, NULL, "Continuum:command_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}
	continuum.m_data.m_ulFlags &= ~CONTINUUM_STATUS_STATUSSVR_MASK;
	continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_STATUSSVR_START;

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing status server on %d", continuum.m_settings.m_usStatusPort); 
	continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
	continuum.m_msgr.DM(MSG_ICONNONE, NULL, "Continuum:status_server_init", errorstring);  Sleep(100);//(Dispatch message)

	if(continuum.m_settings.m_usStatusPort>0)
	{
//AfxMessageBox("Q1");
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = continuum.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "ContinuumStatusServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = ContinuumStatusHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &continuum;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &continuum.m_net;					// pointer to the object with the Message function.

		if(continuum.m_net.StartServer(pServer, &continuum.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			continuum.m_data.m_ulFlags &= ~CONTINUUM_STATUS_STATUSSVR_MASK;
			continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_STATUSSVR_ERROR;
			continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
			continuum.m_msgr.DM(MSG_ICONERROR, NULL, "Continuum:status_server_init", errorstring);  //(Dispatch message)
//			continuum.SendMsg(CX_SENDMSG_ERROR, "Continuum:status_server_init", errorstring);
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Status server listening on %d", continuum.m_settings.m_usStatusPort);
			continuum.m_data.m_ulFlags &= ~CONTINUUM_STATUS_STATUSSVR_MASK;
			continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_STATUSSVR_RUN;
			continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
			continuum.m_msgr.DM(MSG_ICONNONE, NULL, "Continuum:status_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

//AfxMessageBox("QQ");

	if((continuum.m_data.m_ulFlags&CONTINUUM_ICON_MASK) != CONTINUUM_STATUS_ERROR)
	{
		continuum.m_data.m_ulFlags &= ~CONTINUUM_ICON_MASK;
		continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_OK;  // green - we want run to be blue when something in progress
	}

/*
// initialize cortex (make this not persistent for now)
	CNetData* pdata = new CNetData;
	if(pdata)
	{

		pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA;      // defined type - indicates which protocol to use, structure of data

		pdata->m_ucCmd = CX_REQ_HELLO;       // the command byte
		pdata->m_ucSubCmd = 0;       // the subcommand byte

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
		if(pch)
		{
			sprintf(pch, "%s:%d:%d:%d:%s", 
				continuum.m_data.m_pszHost,
				continuum.m_settings.m_usCommandPort,
				continuum.m_settings.m_usStatusPort,
				CX_TYPE_RESOURCE,
				continuum.m_settings.m_pszName?continuum.m_settings.m_pszName:"Continuum"
				);

			pdata->m_pucData =  (unsigned char*) pch;
			pdata->m_ulDataLen = strlen(pch);
		}

		SOCKET s = NULL; 
//AfxMessageBox("sending");
//AfxMessageBox( continuum.m_data.m_pszCortexHost );
//AfxMessageBox( (char*)pdata->m_pucData );
		if(continuum.m_net.SendData(pdata, continuum.m_data.m_pszCortexHost, continuum.m_data.m_usCortexCommandPort, 5000, 0, NET_SND_CMDTOSVR, &s)>=NET_SUCCESS)
		{
			//send ack
//			AfxMessageBox("ack");
			continuum.m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);
		}
		else
		{
//			AfxMessageBox("could not send");
		}
		continuum.m_net.CloseConnection(s);

		delete pdata;

	}
*/

	continuum.m_data.m_ulFlags &= ~CONTINUUM_STATUS_THREAD_MASK;
	continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_THREAD_RUN;

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Continuum main thread running.");  
	continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
//	continuum.m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", errorstring);  Sleep(250);//(Dispatch message)
//	continuum.SendMsg(CX_SENDMSG_INFO, "Continuum:init", "Continuum %s main thread running.", CONTINUUM_CURRENT_VERSION);

	_timeb timebCheckMods;
	_ftime( &timebCheckMods );
	_timeb timebCheckSysTray;
	_ftime( &timebCheckSysTray );
///AfxMessageBox("xxxxx");
//	CWnd* pWnd = new CWnd();
	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &continuum.m_data.m_timebTick );
//	continuum.m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "%d.%03d", continuum.m_data.m_timebTick.time, continuum.m_data.m_timebTick.millitm);   Sleep(250);//(Dispatch message)

// monitor changes and react.
		if(
					(continuum.m_data.m_timebTick.time > timebCheckMods.time )
				||((continuum.m_data.m_timebTick.time == timebCheckMods.time)&&(continuum.m_data.m_timebTick.millitm >= timebCheckMods.millitm))
				&&(!g_bKillThread)
			)
		{
//	continuum.m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "time to check");  Sleep(250);//(Dispatch message)
			timebCheckMods.time = continuum.m_data.m_timebTick.time + continuum.m_settings.m_ulModsIntervalMS/1000; 
			timebCheckMods.millitm = continuum.m_data.m_timebTick.millitm + (unsigned short)(continuum.m_settings.m_ulModsIntervalMS%1000); // fractional second updates
			if(timebCheckMods.millitm>999)
			{
				timebCheckMods.time++;
				timebCheckMods.millitm%=1000;
			}


//	continuum.m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "checking db connected");   Sleep(250);//(Dispatch message)
/*
			if((pdbConn)&&(pdbConn->m_bConnected))
			{
//AfxMessageBox("x");
//	continuum.m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "checkmods");  //(Dispatch message)
				continuum.m_data.CheckDatabaseMods();
				if(continuum.m_data.m_nSettingsMod != continuum.m_data.m_nLastSettingsMod)
				{
					if(continuum.m_settings.GetFromDatabase()>=CONTINUUM_SUCCESS)
					{
						continuum.m_data.m_nLastSettingsMod = continuum.m_data.m_nSettingsMod;
					}
				}

				if(
						(continuum.m_data.m_key.m_bValid)  // must have a valid license
					&&(
							(!continuum.m_data.m_key.m_bExpires)
						||((continuum.m_data.m_key.m_bExpires)&&(!continuum.m_data.m_key.m_bExpired))
						||((continuum.m_data.m_key.m_bExpires)&&(continuum.m_data.m_key.m_bExpireForgiveness)&&(continuum.m_data.m_key.m_ulExpiryDate+continuum.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!continuum.m_data.m_key.m_bMachineSpecific)
						||((continuum.m_data.m_key.m_bMachineSpecific)&&(continuum.m_data.m_key.m_bValidMAC))
						)
					)
				{
				
					if(continuum.m_data.m_nConnectionsMod != continuum.m_data.m_nLastConnectionsMod)
					{
	//			continuum.m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "getting connections again");   Sleep(250);//(Dispatch message)
						if(continuum.m_data.GetConnections()>=CONTINUUM_SUCCESS)
						{
							continuum.m_data.m_nLastConnectionsMod = continuum.m_data.m_nConnectionsMod;
						}
					}
					if(continuum.m_data.m_nChannelsMod != continuum.m_data.m_nLastChannelsMod)
					{
	//			continuum.m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "getting channels again");   Sleep(250);//(Dispatch message)
						if(continuum.m_data.GetChannels()>=CONTINUUM_SUCCESS)
						{
							continuum.m_data.m_nLastChannelsMod = continuum.m_data.m_nChannelsMod;
						}
					}
				}
			}
			*/
		}

		if(
				(continuum.m_data.m_key.m_bValid)  // must have a valid license
			&&(
					(!continuum.m_data.m_key.m_bExpires)
				||((continuum.m_data.m_key.m_bExpires)&&(!continuum.m_data.m_key.m_bExpired))
				||((continuum.m_data.m_key.m_bExpires)&&(continuum.m_data.m_key.m_bExpireForgiveness)&&(continuum.m_data.m_key.m_ulExpiryDate+continuum.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
					(!continuum.m_data.m_key.m_bMachineSpecific)
				||((continuum.m_data.m_key.m_bMachineSpecific)&&(continuum.m_data.m_key.m_bValidMAC))
				)
			)
		{
			if(
						(continuum.m_data.m_timebTick.time > timebCheckSysTray.time )
					||((continuum.m_data.m_timebTick.time == timebCheckSysTray.time)&&(continuum.m_data.m_timebTick.millitm >= timebCheckSysTray.millitm))
					&&(!g_bKillThread)
					&&(continuum.m_settings.m_ulSysTrayIntervalMS>0)
				)
			{
	//	continuum.m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "time to check");  Sleep(250);//(Dispatch message)
				timebCheckSysTray.time = continuum.m_data.m_timebTick.time + continuum.m_settings.m_ulSysTrayIntervalMS/1000; 
				timebCheckSysTray.millitm = continuum.m_data.m_timebTick.millitm + (unsigned short)(continuum.m_settings.m_ulSysTrayIntervalMS%1000); // fractional second updates
				if(timebCheckSysTray.millitm>999)
				{
					timebCheckSysTray.time++;
					timebCheckSysTray.millitm%=1000;
				}

	/*
				if(pWnd)
				{
					// AfxMessageBox("checking tray");
					CWnd* pTrayWnd = pWnd->FindWindow("Shell_TrayWnd", NULL);
					if(pTrayWnd) pTrayWnd->ShowWindow(FALSE);
				}
	*/ 
				HWND hWnd = ::FindWindow(_T("Shell_TrayWnd"), NULL);
				if(hWnd)
				{
					hWnd = ::FindWindowEx(hWnd,NULL,_T("TrayNotifyWnd"), NULL);
					if(hWnd)
					{
          
						hWnd = ::FindWindowEx(hWnd, NULL,_T("ToolbarWindow32"), NULL);
						if(hWnd)
						{
							RECT rcTray;
							GetWindowRect(hWnd, &rcTray);

							int count = (int)::SendMessage(hWnd, TB_BUTTONCOUNT, 0, 0);

							// this is ridiculous but it works.  
							// We steal the mouse for a very short time, run it over all the icons to get
							// the inactive ones to disappear, then return it.
							POINT ptRestore;
							GetCursorPos(&ptRestore);
							POINT pt; 
							pt.x = rcTray.left + 8; 
							pt.y = rcTray.top+8;
							for(int i=0; i<count; i++)
							{
								pt.x+=16;
								SetCursorPos(pt.x, pt.y);
							}
							SetCursorPos(ptRestore.x, ptRestore.y);
						}
	//						hWnd = ::FindWindowEx(hWnd,NULL,_T("SysPager"), NULL);
					}
				}
			}

			// do more stuff here...
		}

//AfxMessageBox("zoinks");
		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();

		Sleep(1); 
	}
//	if(pWnd) delete pWnd;

	continuum.m_data.m_ulFlags &= ~CONTINUUM_STATUS_THREAD_MASK;
	continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_THREAD_END;

	continuum.m_msgr.DM(MSG_ICONINFO, NULL, "Continuum:uninit", "Continuum is shutting down.");  //(Dispatch message)
//	continuum.SendMsg(CX_SENDMSG_INFO, "Continuum:uninit", "Continuum %s is shutting down.", CONTINUUM_CURRENT_VERSION);

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Continuum is shutting down.");  
	continuum.m_data.m_ulFlags &= ~CX_ICON_MASK;
	continuum.m_data.m_ulFlags |= CX_STATUS_UNKNOWN;
	continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);

/*
// shut down all the running objects;
	if(continuum.m_data.m_ppChannelObj)
	{
		while(continuum.m_data.m_nNumChannelObjects>0)
		{
			int i = continuum.m_data.m_nNumChannelObjects-1;
			if(continuum.m_data.m_ppChannelObj[i])
			{
				continuum.m_data.m_ppChannelObj[i]->m_ulFlags = CONTINUUM_FLAG_DISABLED; // so killing thread wont start it up again
//				continuum.m_data.m_ppChannelObj[i]->m_bKillChannelThread = true;
				
//				while(continuum.m_data.m_ppChannelObj[i]->m_bChannelThreadStarted) Sleep(10);//(may be dangerous stall here)

				continuum.m_data.m_nNumChannelObjects--;

				delete continuum.m_data.m_ppChannelObj[i];
				continuum.m_data.m_ppChannelObj[i] = NULL;
			}
		}
		delete [] continuum.m_data.m_ppChannelObj;
	}
	if(continuum.m_data.m_ppConnObj)
	{
		while(continuum.m_data.m_nNumConnectionObjects>0)
		{
			int i = continuum.m_data.m_nNumConnectionObjects-1;
			if(continuum.m_data.m_ppConnObj[i])
			{
				// **** disconnect servers
				continuum.m_data.m_ppConnObj[i]->m_ulFlags = CONTINUUM_FLAG_DISABLED; // so killing thread wont start it up again
//				continuum.m_data.m_ppConnObj[i]->m_bKillConnThread = true;

				if((continuum.m_data.m_ppConnObj[i]->m_ulStatus&CONTINUUM_ICON_MASK) == CONTINUUM_STATUS_CONN)
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s%s%s...", 
						continuum.m_data.m_ppConnObj[i]->m_pszDesc?continuum.m_data.m_ppConnObj[i]->m_pszDesc:continuum.m_data.m_ppConnObj[i]->m_pszServerName,
						continuum.m_data.m_ppConnObj[i]->m_pszDesc?" on ":"",
						continuum.m_data.m_ppConnObj[i]->m_pszDesc?continuum.m_data.m_ppConnObj[i]->m_pszServerName:""
						);  
					continuum.m_msgr.DM(MSG_ICONINFO, NULL, "Continuum:connection_uninit", errorstring);    //(Dispatch message)
					continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
					g_omni.DisconnectServer(continuum.m_data.m_ppConnObj[i]->m_pCAConn);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Removing %s...", continuum.m_data.m_ppConnObj[i]->m_pszDesc?continuum.m_data.m_ppConnObj[i]->m_pszDesc:continuum.m_data.m_ppConnObj[i]->m_pszServerName);  
					continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
				}


				while((continuum.m_data.m_ppConnObj[i]->m_pCAConn)&&(continuum.m_data.m_ppConnObj[i]->m_pCAConn->m_bThreadStarted)) Sleep(50);  //(may be dangerous stall here)

				continuum.m_data.m_nNumConnectionObjects--;

				delete continuum.m_data.m_ppConnObj[i];
				continuum.m_data.m_ppConnObj[i] = NULL;
			}
		}
		delete [] continuum.m_data.m_ppConnObj;
	}
*/

/*
	// here's a hard coded one for now
	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no need to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = continuum.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		continuum.m_net.OpenConnection(continuum.m_http.m_pszHost, 10888, &s); // branding hardcode
		continuum.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		continuum.m_net.CloseConnection(s); // branding hardcode
	}
*/

//	m_pDlg->SetProgress(CONTINUUMDLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
//	continuum.m_data.m_ulFlags &= ~CONTINUUM_STATUS_FILESVR_MASK;
//	continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_FILESVR_END;
//	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down file server....");  
//	continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
//	_ftime( &continuum.m_data.m_timebTick );
//	continuum.m_http.EndServer();
	_ftime( &continuum.m_data.m_timebTick );
//	AfxMessageBox("shutting down command server.");
	continuum.m_data.m_ulFlags &= ~CONTINUUM_STATUS_CMDSVR_MASK;
	continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_CMDSVR_END;
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down command server....");
	continuum.m_msgr.DM(MSG_ICONNONE, NULL, "Continuum:command_server_uninit", errorstring);   Sleep(100);  //(Dispatch message)
	continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
	_ftime( &continuum.m_data.m_timebTick );
	continuum.m_net.StopServer(continuum.m_settings.m_usCommandPort, 5000, errorstring);
	_ftime( &continuum.m_data.m_timebTick );
//	AfxMessageBox("shutting down status server.");
	continuum.m_data.m_ulFlags &= ~CONTINUUM_STATUS_STATUSSVR_MASK;
	continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_STATUSSVR_END;
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down status server....");  
	continuum.m_msgr.DM(MSG_ICONNONE, NULL, "Continuum:status_server_uninit", errorstring);   Sleep(100);  //(Dispatch message)
	continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);
	_ftime( &continuum.m_data.m_timebTick );
	continuum.m_net.StopServer(continuum.m_settings.m_usStatusPort, 5000, errorstring);
	_ftime( &continuum.m_data.m_timebTick );
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Continuum is exiting.");  
	continuum.m_msgr.DM(MSG_ICONNONE, NULL, "Continuum:uninit", errorstring);   Sleep(100);  //(Dispatch message)
	continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);



	// save settings.  // dont save them here.  save them on any changes in the main command loop.

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// these explicts arent necessary - uncomment to write out a full file to edit...
		file.SetIniString("Main", "Name", continuum.m_settings.m_pszName);
		file.SetIniInt("CommandServer", "ListenPort", continuum.m_settings.m_usCommandPort);
		file.SetIniInt("StatusServer", "ListenPort", continuum.m_settings.m_usStatusPort);
		file.SetIniString("License", "Key", continuum.m_settings.m_pszLicense);

//		file.SetIniString("FileServer", "IconPath", continuum.m_settings.m_pszIconPath);

		file.SetIniInt("Messager", "UseEmail", continuum.m_settings.m_bUseEmail?1:0);
		file.SetIniInt("Messager", "UseNet", continuum.m_settings.m_bUseNetwork?1:0);
		file.SetIniInt("Messager", "UseLog", continuum.m_settings.m_bUseLog?1:0);
		file.SetIniInt("Messager", "UseTrayIcon", continuum.m_settings.m_bUseTrayIcon?1:0);
		file.SetIniInt("Messager", "UseDialog", continuum.m_settings.m_bUseDialog?1:0);

/*
		file.SetIniString("Database", "DSN", continuum.m_settings.m_pszDSN);
		file.SetIniString("Database", "DBUser", continuum.m_settings.m_pszUser);
		file.SetIniString("Database", "DBPassword", continuum.m_settings.m_pszPW);
		file.SetIniString("Database", "SettingsTableName", continuum.m_settings.m_pszSettings);  // the Settings table name
		file.SetIniString("Database", "ExchangeTableName", continuum.m_settings.m_pszExchange);  // the Exchange table name
		file.SetIniString("Database", "MessagesTableName", continuum.m_settings.m_pszMessages);  // the Messages table name

*/
		file.SetIniInt("Application", "ModificationCheckInterval", continuum.m_settings.m_ulModsIntervalMS);  // in milliseconds
		file.GetIniInt("Application", "SysTrayCheckInterval", continuum.m_settings.m_ulSysTrayIntervalMS);  // in milliseconds

		file.SetSettings(CONTINUUM_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

	}

	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Continuum is exiting");
	continuum.m_data.m_ulFlags &= ~CX_ICON_MASK;
	continuum.m_data.m_ulFlags |= CX_STATUS_UNINIT;
	continuum.m_data.SetStatusText(errorstring, continuum.m_data.m_ulFlags);


	//exiting
	continuum.m_msgr.DM(MSG_ICONINFO, NULL, "Continuum", "-------------- Continuum %s exit ---------------\n\
--------------------------------------------------\n", CONTINUUM_CURRENT_VERSION);  //(Dispatch message)
///	m_pDlg->SetProgress(CONTINUUMDLG_CLEAR); // no point

	_ftime( &continuum.m_data.m_timebTick );
	continuum.m_data.m_ulFlags &= ~CONTINUUM_STATUS_THREAD_MASK;
	continuum.m_data.m_ulFlags |= CONTINUUM_STATUS_THREAD_ENDED;

	g_pcontinuum = NULL;
//	db.RemoveConnection(pdbConn);


	Sleep(500); // small delay at end
	g_bThreadStarted = false;
	Sleep(300); // another small delay at end
	_endthread();
}

/*
void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((g_pmsgr)&&(g_pmsgr->m_lpfnDM))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		CMessagingObject msg;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = msg.EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = msg.EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = msg.EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch;
		pch = (char*) malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
* /
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if(pszEncodedMsg) 
			{
				ulBufferLen = strlen(pszEncodedMsg);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedCaller)
			{
				ulBufferLen = strlen(pszEncodedCaller);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedDest)
			{
				ulBufferLen = strlen(pszEncodedDest);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			g_pmsgr->m_lpfnDM(pch, ulFlags);
			free(pch);
		}
	}
}
*/


/*
// this thread is based on void HTTP10HandlerThread(void* pvArgs) from
// HTTP10.cpp.  It makes a special webserver with security, cgi parsing, and dynamic results
void ContinuumHTTPThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return;}

	CContinuumMain* pContinuum = (CContinuumMain*)(pClient->m_lpObject) ;  // pointer to the global object.

// HTTP service by def is a non-persistent service, so even if the Client type is
// set to persist, we ignore that directive in this handler thread

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;


		CHTTP10* phttp = (CHTTP10*)(&(pContinuum->m_http)) ;  // pointer to the global object.
		if(phttp == NULL)// cant do anything!
		{
			shutdown(pClient->m_socket, SD_BOTH);
			closesocket(pClient->m_socket);
			(*(pClient->m_pulConnections))--;
			_endthread(); return;
		}

		CHTTP10 http(phttp->m_pszRoot);  // local object for utility functions ONLY.
		// this allows the local to clone the root setting of the global, 
		// in order to do correct parsing in its own thread

		// the following are just pointers to the global obj.  
		// must NULL these out before object gets destroyed, 
		// so that global resources not freed!   !!!!!!!  important !!!!!
		http.m_pszHost = phttp->m_pszHost;
		http.m_ppszErrors = phttp->m_ppszMime;
		http.m_ppszMime = phttp->m_ppszMime;   
		http.m_ulStatus = phttp->m_ulStatus;   // important for redirect and error
		
		unsigned char* pch;// for use with GetLine().
		unsigned long ulBufLen;// for use with GetLine().
		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		unsigned short usCode = 0;

		// get the raw buffer and process it yourself
		nReturn = http.m_net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_EOLN|EOLN_HTTP, pszStatus);
		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). 
			//was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
		{
			//error.
			if(pClient->m_lpMsgObj)
			{
				_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error receiving data.  %s", pszStatus);
				((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
			}

			usCode = 400;  //  we TRY to send this....
			if(pch!=NULL)
			{
				free(pch);  // must free the incoming data buffer 
				pch = NULL;
			}
			// create a return message buffer.
			pch = (unsigned char*)http.ErrorBuffer(NULL, usCode);   // 400 bad request
			ulBufLen = strlen((char*)pch); // valid from an error page.

			// send malformed request answer if possible  - have to tell the client that the request was not received in good order.
			nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
			if(nReturn<NET_SUCCESS)
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
				}
			}
		}
		else  // successful reception of data.
		{
			// process the data here...
			// in this sample the buffer is loaded into
			// pch, with ulBufLen as length

/*
			FILE* fp = fopen("rec.txt", "ab");
			if (fp)
			{
				fwrite(pch, 1, ulBufLen, fp );
				fflush(fp);
				fclose(fp);
			}
* /
			// parse buffer, format reply.
			CHTTPHeader header;

			if (http.ParseHeader(pch, ulBufLen, &header) < HTTP_SUCCESS)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("header not parsed\r\n\r\n", 1, strlen("header not parsed\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}
* /
				
				if(pch!=NULL) 
				{
					free(pch);  // must free the incoming data buffer 
					pch = NULL;
				}
				// create a return message buffer.
				usCode = 400;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}
			else
			{
				// here we have enough info to check security if desired, and if not, return a 401;
				// else continue....
/*
					FILE* fp = fopen("rec.txt", "ab");
					if (fp)
					{// lets write out the whole header so we can examine its full contents. including args.
						char bufferin[10000];
						sprintf(bufferin,
"header successfully parsed\r\n\
method: %d\r\n\
version: %s\r\n\
URL: %s\r\n\
Path: %s\r\n\
mimetype: %s\r\n\
user: %s\r\n\
password: %s\r\n\
num args: %ld\r\n",
	header.m_ucMethod,  // enumerated values
	header.m_pszVersion==NULL?"(null)":header.m_pszVersion,
	header.m_pszURL==NULL?"(null)":header.m_pszURL,
	header.m_pszPath==NULL?"(null)":header.m_pszPath,
	header.m_pszMimeType==NULL?"(null)":header.m_pszMimeType,
	header.m_pszUser==NULL?"(null)":header.m_pszUser,
	header.m_pszPassword==NULL?"(null)":header.m_pszPassword,
	header.m_ulNumArgs

);
		
	unsigned long k=0;
	while(k<header.m_ulNumArgs)
	{
		if((header.m_ppszArgNames)&&(header.m_ppszArgValues))
		{
			strcat(bufferin, "  ");
			strcat(bufferin, header.m_ppszArgNames[k]==NULL?"(null)":header.m_ppszArgNames[k]);
			strcat(bufferin, "=");
			strcat(bufferin, header.m_ppszArgValues[k]==NULL?"(null)":header.m_ppszArgValues[k]);
			strcat(bufferin, "\r\n");
		}
	k++;
	}

	strcat(bufferin, "headers:\r\n");
	strcat(bufferin, header.m_pszHeaders);
	strcat(bufferin, "\r\n");


			//		fwrite("header successfully parsed\r\n", 1, strlen("header successfully parsed\r\n"), fp );
						fwrite(bufferin, 1, strlen(bufferin), fp );
						fflush(fp);
						fclose(fp);
					}

* /


				if((phttp->m_pSecure)&&((header.m_pszUser==NULL)||(header.m_pszPassword==NULL)))
				{
					usCode = 401;
					pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
					ulBufLen = strlen((char*)pch); // valid from an error page.
					// buffer successfully parsed.
				}
				else 
				{
					if(pch!=NULL)
					{
						free(pch);  // must free the incoming data buffer 
						pch = NULL;
					}

					// create a return message buffer.
					switch(header.m_ucMethod)
					{
					case HTTP_GET://			1  // HTTP/1.0
					case HTTP_HEAD://			2  // HTTP/1.0
						{
							if(
									(strnicmp(header.m_pszPath, http.m_pszRoot, strlen(http.m_pszRoot)) == 0 )  // else not a part of the served file tree
								)
							{
								if(phttp->m_pSecure)
								{
									// have to check the main object for permission.
									// we are going to go by URL (asset level)
									// Loci are going to just be convenient names for the admins to set up.

									if((phttp->m_pSecure->CheckSecure(header.m_pszUser, header.m_pszPassword, NULL, header.m_pszURL))!=SECURE_SUCCESS)
									{
										usCode = 401;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
										ulBufLen = strlen((char*)pch); // valid from an error page.
										break; // get out of the switch from here.
									}
									// else it flows thru....
								}

								DWORD dwAttrib;

								dwAttrib = GetFileAttributes(header.m_pszPath);

								// check for directory (redirect nec.)
								if ( (dwAttrib!=0xffffffff)&&(dwAttrib&FILE_ATTRIBUTE_DIRECTORY) )  // if error, it may be because no file exists, so check in the next part.
								{
									usCode = 301;  // not necessary but why not.
									pch = (unsigned char*)http.RedirectBuffer(&header);
									ulBufLen = strlen((char*)pch); // valid from an redirect page.
								}
								else
								{
									// before we try to open the file, lets spend the time now (not while the file is open)
									// to figure out how long the static text respone buffer will be.
									CBufferUtil bu;
									// notice the sample "Server: ContinuumServer/3.0.3.1" header included.  Pick the appname and ver, probably best
									unsigned long ulBufferLen = strlen("HTTP/1.0 200 OK\r\nServer: ContinuumServer/\r\nContent-Length: \r\n\r\n");
									ulBufferLen += strlen(CONTINUUM_CURRENT_VERSION);

									if((header.m_pszMimeType)&&(strlen(header.m_pszMimeType)>0))
									{
										ulBufferLen += (strlen("Content-Type: \r\n") + strlen(header.m_pszMimeType)); // only add if known!
									}
									else // empty!
									{
										// dont free the "permanent resource", just null the pointer
										header.m_pszMimeType=NULL;
									}

									FILE* pFile = NULL;

									pFile = fopen(header.m_pszPath, "rb");
									if (pFile == NULL)  // file not openable, or not found.
									{
										usCode = 404;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 404 not found
										ulBufLen = strlen((char*)pch); // valid from an error page.
									}
									else
									{
										// this part just assembles the file into a buffer.
										// this may be modified to do dynamic html things by using the 
										// pClient->m_lpObject that refers back to some parser....

										usCode = 200; // OK  (!)
										// determine file size
										fseek(pFile, 0, SEEK_END);
										unsigned long ulFileLen = ftell(pFile);


										// here we have to parse out any directives.  if this is a template file,
										// we may have a variable length.
									//	x


										if(header.m_ucMethod == HTTP_GET)
										{
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += (strlen(length)+ulFileLen);

											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you truncations at zeroed bytes

											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: ContinuumServer/%s", CONTINUUM_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
											
											fclose(pFile);
										}
										else
										if(header.m_ucMethod == HTTP_HEAD)
										{
											fclose(pFile);  // we have all we need already.
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += strlen(length);
											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											//  create the header
											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: ContinuumServer/%s", CONTINUUM_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												// its identical except for the body.  next line must stay commented out, its just there for ref.
												//fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
										}
									}
								}
							}
							else  // not part of the server service.
							{
								usCode = 403;
								pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 403 forbidden ...
								ulBufLen = strlen((char*)pch); // valid from an error page.
							}

						} break;
					case HTTP_POST://			3  // HTTP/1.0
						{
							usCode = 501;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 501 not implemented ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_PUT://			4  // HTTP/1.1
					case HTTP_DELETE://		5  // HTTP/1.1
					case HTTP_OPTIONS://  6  // HTTP/1.1
					case HTTP_TRACE://	  7  // HTTP/1.1
						{
							usCode = 505;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 505 http version not supported ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_UNDEF://		0  // internal
					default:
						{
							usCode = 400;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					}
				}
			}

			// send a reply, in pch., with strlen pch as length

			if(pch == NULL)  // had a problem forming the request, try to send a last ditch error code.
			{
				usCode = 500;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 500 internal server error
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}

			if(pch != NULL)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("sending:\r\n", 1, strlen("sending:\r\n"), fp );
					fwrite(pch, 1, ulBufLen, fp );
					fwrite("\r\n\r\n", 1, strlen("\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}

* /
				nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
				if(nReturn<NET_SUCCESS)
				{
					//error.
					if(pClient->m_lpMsgObj)
					{
						_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
						((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
					}
				}
			}
		}

//		char foo[34]; sprintf(foo, "code: %d", usCode);	AfxMessageBox(foo);

		http.m_pszHost = NULL;		// but do NOT free resource
		http.m_ppszErrors = NULL; // but do NOT free resource
		http.m_ppszMime = NULL;   // but do NOT free resource

		if(pch!=NULL) free(pch);  // must free the data buffer 

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}
*/


void ContinuumCommandHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure in Protocol 1  //SECURE with PROTOCOL2 later
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszStatus);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Continuum:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Continuum:CommandHandlerThread");
						}
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{
//AfxMessageBox("receiving");

					// parse the command:

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					switch(data.m_ucCmd)
					{
					case CX_CMD_GETINFO: // not actually supported yet.  everything thru csf files for now.
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case CX_CMD_BYE:
						{
	g_pcontinuum->m_msgr.DM(MSG_ICONINFO, NULL, "Continuum", "The Global Kill command has been received.");  //(Dispatch message)

							bSetGlobalKill = true;
							g_pcontinuum->m_data.m_bQuietKill = true;
							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					default:
						{
							//just ack
							data.m_ucCmd = NET_CMD_ACK;

		//					char repbuf[64];
							if(data.m_pucData!=NULL)
							{
		/*
								_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

								for(unsigned long q=0; q<data.m_ulDataLen; q++)
								{
									if(strlen(repbuf)<63)
										strncat(repbuf, (char*)(data.m_pucData+q), 1);
								}
								if(strlen(repbuf)<63)
									strcat(repbuf, "]");
								data.m_ulDataLen = strlen(repbuf);
								repbuf[data.m_ulDataLen] = 0;  // just in case
		*/
								free(data.m_pucData);  //destroy the buffer;
							}
		/*
							else
							{
								_snprintf(repbuf, 63, "data was NULL");
								data.m_ulDataLen = strlen(repbuf); 
							}

							data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
							if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
							else 
							{
								data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
								data.m_ulDataLen = 0;
							}
		*/
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					}

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Continuum:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Continuum:CommandHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.
	if (bSetGlobalKill)	//g_bKillThread = true;
	((CContinuumHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}

void ContinuumStatusHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszStatus);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Continuum:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Continuum:CommandHandlerThread");
						}
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Continuum:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Continuum:CommandHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}

/*
int CContinuumMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)&&(strlen(m_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			m_settings.m_pszMessages,
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			m_settings.m_pszMessages
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		EnterCriticalSection(&g_pcontinuum->m_data.m_critSQL);
		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&g_pcontinuum->m_data.m_critSQL);
			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return CONTINUUM_SUCCESS;
		}
		LeaveCriticalSection(&g_pcontinuum->m_data.m_critSQL);
	}
	return CONTINUUM_ERROR;
}
*/

/*
void ContinuumConnectionThread(void* pvArgs)
{
//			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "ContinuumConnectionThread");  Sleep(250); //(Dispatch message)
	CContinuumConnectionObject* pConn = (CContinuumConnectionObject*) pvArgs;
	if(pConn==NULL) 
	{
		//**MSG
		return;
	}
	if(pConn->m_bKillConnThread) return;
	pConn->m_bConnThreadStarted = true;

	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:connection", "Server management thread for %s (%s) has begun.",
											pConn->m_pszServerName, //server_name
											pConn->m_pszDesc
		);  Sleep(100); //(Dispatch message)

	unsigned long ulTick = GetTickCount();
	unsigned long ulElapsedTick = GetTickCount();


//			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "ContinuumConnectionThread2");  Sleep(250); //(Dispatch message)
	while(!pConn->m_bKillConnThread)
	{
		if(	pConn->m_pAPIConn )
		{
//			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "getting status");  Sleep(250); //(Dispatch message)
			// we can monitor the connection and get status
//			EnterCriticalSection(&g_omni.m_crit);
			g_adc.GetStatusData(pConn->m_pAPIConn);
//			LeaveCriticalSection(&g_adc.m_crit);
			ulTick = GetTickCount();

			if(pConn->m_pAPIConn->m_Status > 0)  // connected
			{
				// here we need to monitor list states and threads
				int nChannels = 0;

				while((nChannels<g_pcontinuum->m_data.m_nNumChannelObjects)&&(!pConn->m_bKillConnThread))
				{
					if((g_pcontinuum->m_data.m_ppChannelObj)&&(g_pcontinuum->m_data.m_ppChannelObj[nChannels]))
					{
						if(strcmp(g_pcontinuum->m_data.m_ppChannelObj[nChannels]->m_pszServerName, pConn->m_pszServerName)==0) // we are on the right adc conn.
						{
							//if the list is active, check to see if there is a thread going.
							if(g_pcontinuum->m_data.m_ppChannelObj[nChannels]->m_ulFlags&CONTINUUM_FLAG_ENABLED)
							{
								if(
									  (g_pcontinuum->m_data.m_ppChannelObj[nChannels]->m_nHarrisListID<0)
									||(g_pcontinuum->m_data.m_ppChannelObj[nChannels]->m_nHarrisListID>=MAXSYSLISTS)
									)
								{
									// error!
									g_pcontinuum->m_data.m_ppChannelObj[nChannels]->m_ulFlags &= ~CONTINUUM_FLAG_ENABLED;  //disable

								}
								else
								if(( !g_pcontinuum->m_data.m_ppChannelObj[nChannels]->m_bChannelThreadStarted)&&(!pConn->m_bKillConnThread))
								{ // start the thread!
									g_pcontinuum->m_data.m_ppChannelObj[nChannels]->m_bKillChannelThread=false;
									g_pcontinuum->m_data.m_ppChannelObj[nChannels]->m_pAPIConn = pConn->m_pAPIConn;
//									g_pcontinuum->m_data.m_ppChannelObj[nChannels]->m_pbKillConnThread=&(pConn->m_bKillConnThread);

//			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "beginning list thread");  Sleep(250); //(Dispatch message)
									if(_beginthread(ContinuumListThread, 0, (void*)g_pcontinuum->m_data.m_ppChannelObj[nChannels])<0)
									{
										//error.
							
//			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "problem beginning list thread");  Sleep(250); //(Dispatch message)
										//**MSG
									}
									else
									{
										Sleep(200);
										// while(!g_pcontinuum->m_data.m_ppChannelObj[nChannels]->m_bChannelThreadStarted) Sleep(1);
									}
								}
							}
							else  // disabled
							{
								if( g_pcontinuum->m_data.m_ppChannelObj[nChannels]->m_bChannelThreadStarted)
								{
									// end the thread.
//			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "ending list thread");  Sleep(250); //(Dispatch message)
									g_pcontinuum->m_data.m_ppChannelObj[nChannels]->m_bKillChannelThread=true;
									Sleep(200); // give it a  rest in between
								}
							}
						}
					}
					nChannels++;
				}
			}
			else
			{
	EnterCriticalSection(&g_adc.m_crit);
				g_adc.DisconnectServer(pConn->m_pAPIConn);  // just removes from adc list.
	LeaveCriticalSection(&g_adc.m_crit);
				pConn->m_ulStatus = CONTINUUM_STATUS_ERROR;  // due to disconnection.  resets on connect
				pConn->m_pAPIConn = NULL; // break out.
			}

			if((pConn->m_pAPIConn)&&(!pConn->m_bKillConnThread))
			{
				ulElapsedTick = GetTickCount();
				if(ulElapsedTick<=ulTick) //wrapped or instantaneous
				{
					Sleep(((pConn->m_pAPIConn->m_SysData.systemfrx) == 0x24)?40:33);
				}
				else
				{
					ulElapsedTick -= ulTick;
					if(ulElapsedTick<(unsigned long)(((pConn->m_pAPIConn->m_SysData.systemfrx) == 0x24)?40:33))
					{
			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "sleeping on status %d ms", (((pConn->m_pAPIConn->m_SysData.systemfrx) == 0x24)?40:33) - ulElapsedTick);  Sleep(250); //(Dispatch message)
						Sleep((((pConn->m_pAPIConn->m_SysData.systemfrx) == 0x24)?40:33) - ulElapsedTick);
					}  // else we already waited long.
				}
			}
			//else break out and try to reconnect.
		}
		else
		{
			if(pConn->m_ulFlags&CONTINUUM_FLAG_ENABLED) // active.
			{
				if((pConn->m_pszClientName)&&(strlen(pConn->m_pszClientName)<=0)) // zero length client name
				{
					free(pConn->m_pszClientName);
					pConn->m_pszClientName = NULL;
				}
				if(!pConn->m_pszClientName)
				{
					pConn->m_pszClientName = (char*)malloc(strlen("Continuum")+1);
					if(pConn->m_pszClientName) strcpy(pConn->m_pszClientName, "Continuum");
				}
					
				char server[32];
				_snprintf(server, 32, "%s", pConn->m_pszServerName);
				char client[32];
				_snprintf(client, 32, "%s", pConn->m_pszClientName);
//			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "connecting %s with %s ", server, client);  Sleep(250); //(Dispatch message)
				EnterCriticalSection(&g_adc.m_crit);
				CAConnection* pAPIConn = g_adc.ConnectServer(server, client);
				LeaveCriticalSection(&g_adc.m_crit);
				if(pAPIConn!=NULL)
				{
					pConn->m_pAPIConn = pAPIConn; // connected.
					pConn->m_ulStatus &= ~CONTINUUM_ICON_MASK;
					pConn->m_ulStatus |= CONTINUUM_STATUS_CONN;
//			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "connected %s with %s ", pConn->m_pszServerName, pConn->m_pszClientName);  Sleep(250); //(Dispatch message)
				}
				else
				{
					//**MSG
					pConn->m_pAPIConn = NULL;
					pConn->m_ulStatus &= ~CONTINUUM_ICON_MASK;
					pConn->m_ulStatus |= CONTINUUM_STATUS_ERROR;
//					g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "NULL connecting %s with %s ", pConn->m_pszServerName, pConn->m_pszClientName);  Sleep(250); //(Dispatch message)
				}
			} // else don't connect, just 
			else
			{
				pConn->m_pAPIConn = NULL;
				pConn->m_ulStatus &= ~CONTINUUM_ICON_MASK;
				pConn->m_ulStatus |= CONTINUUM_STATUS_NOTCON;
			}

			Sleep(2000);// wait two seconds after a connection attempt;
		}
			
	}


	if((g_pcontinuum->m_data.m_ppChannelObj)&&(g_pcontinuum->m_data.m_nNumChannelObjects))
	{
		int nThreads=0;
		while(nThreads < g_pcontinuum->m_data.m_nNumChannelObjects)
		{
			if(
					(g_pcontinuum->m_data.m_ppChannelObj[nThreads])
				&&(g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_pszServerName)
				&&(pConn->m_pszServerName)
				&&(strcmp(g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_pszServerName, pConn->m_pszServerName)==0)
				)
			{


/*	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:connection", "Shutting down list management thread for %s:%d (%s).",
											g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_pszServerName, //server_name
											g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_nHarrisListID,
											g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_pszDesc
		);  Sleep(20); //(Dispatch message)
* /
				g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_bKillChannelThread = true;			
				if(g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_bChannelThreadStarted)
				{
/////////////
					
/*
					g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:connection", "Shutting down list management thread for %s:%d (%s).",
											g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_pszServerName, //server_name
											g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_nHarrisListID,
											g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_pszDesc
		);  //Sleep(50); //(Dispatch message)
//////////////
	g_pcontinuum->m_msgr.DM(MSG_ICONERROR, NULL, "Continuum:connection", "2 - Shutting down list management thread for %s:%d (%s).   %d\n\n%d %d %d",
											g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_pszServerName, //server_name
											g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_nHarrisListID,
											g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_pszDesc, clock(),
											(g_pcontinuum->m_data.m_ppChannelObj),(g_pcontinuum->m_data.m_ppChannelObj[nThreads]),(g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_bChannelThreadStarted)
		); // Sleep(50); //(Dispatch message)
* /
//	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:connection", "entering while");
					while((g_pcontinuum->m_data.m_ppChannelObj)&&(g_pcontinuum->m_data.m_ppChannelObj[nThreads])&&(g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_bChannelThreadStarted))
					{
//						g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_bKillChannelThread = true;			
/*	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:connection", "Shutting down list management thread for %s:%d (%s).   %d\n\n%d %d %d",
											g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_pszServerName, //server_name
											g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_nHarrisListID,
											g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_pszDesc, clock(),
											(g_pcontinuum->m_data.m_ppChannelObj),(g_pcontinuum->m_data.m_ppChannelObj[nThreads]),(g_pcontinuum->m_data.m_ppChannelObj[nThreads]->m_bChannelThreadStarted)
		); // Sleep(50); //(Dispatch message)
	* /
						Sleep(10);
					}
//	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:connection", "leaving while");
				}
			}
			nThreads++;
		}
	}
				


	EnterCriticalSection(&g_adc.m_crit);
	g_adc.DisconnectServer(pConn->m_pszServerName);
	LeaveCriticalSection(&g_adc.m_crit);
	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:connection", "Server management thread for %s (%s) is ending.",
											pConn->m_pszServerName, //server_name
											pConn->m_pszDesc
		);  Sleep(100); //(Dispatch message)

	pConn->m_pAPIConn = NULL;
	pConn->m_bConnThreadStarted = false;
	_endthread();
}

void ContinuumListThread(void* pvArgs)
{
//	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "ContinuumChannelThread");  Sleep(250); //(Dispatch message)
	CContinuumChannelObject* pChannel = (CContinuumChannelObject*) pvArgs;
	if(pChannel==NULL)
	{
//	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "pChannel NULL, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}
	if(pChannel->m_bKillChannelThread) return;
/*
	bool* pbKillConnThread = &(pChannel->m_bKillChannelThread);//pChannel->m_pbKillConnThread;
	if(pbKillConnThread==NULL)  // the address, not what it points to
	{
//	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "g_pcontinuum NULL, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}
* /
	if(	pChannel->m_pAPIConn == NULL) // need this to do anything.  // could get it by looping through g_adc.... later.
	{
//	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "m_pAPIConn NULL, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}

	// lets set up some convenience vars;
	int nZeroList = pChannel->m_nHarrisListID-1;


	if((nZeroList<0)||(nZeroList>=MAXSYSLISTS)) // error on list number.
	{
//	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "nZeroList out of range, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}
	pChannel->m_bChannelThreadStarted = true;
	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:list", "List management thread for %s:%d (%s) has begun.",
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
											pChannel->m_pszDesc
		);  Sleep(100); //(Dispatch message)



	tlistdata* plistdata = &(pChannel->m_pAPIConn->m_ListData[nZeroList]);
//	TConnectionStatus* pstatus =  &(pChannel->m_pAPIConn->m_Status);
	CAList** ppList = &(pChannel->m_pAPIConn->m_pList[nZeroList]);
	CAList*  pList = pChannel->m_pAPIConn->m_pList[nZeroList];  // volatile, always reassign
	CDBUtil* pdb = g_pcontinuum->m_data.m_pdb;
	CDBconn* pdbConn = g_pcontinuum->m_data.m_pdbConn;

	pChannel->m_pAPIConn->m_bListActive[nZeroList] = true;
//	unsigned long ulTick = GetTickCount();
//	unsigned long ulElapsedTick = GetTickCount();
//			g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "ContinuumChannelThread2");  Sleep(250); //(Dispatch message)

	tlistdata listdata;
	tlistdata lastlistdata;  // for comparison

  lastlistdata.listchanged=0;				// incremented whenever list size changes
  lastlistdata.listdisplay=0;				// incremented whenever an event changes
  lastlistdata.listsyschange=0;			// incremented when list related values are changed. 
  lastlistdata.listcount=0;					// number of events in list 
  lastlistdata.liststate=0;
	char dberrorstring[DB_ERRORSTRING_LEN];
	char szSQL[DB_SQLSTRING_MAXLEN];
			
	while(!pChannel->m_bKillChannelThread)//&&((*pbKillConnThread)!=true))
	{
//		EnterCriticalSection(&g_adc.m_crit);
//		g_adc.GetStatusData(pConn->m_pAPIConn);
//		LeaveCriticalSection(&g_adc.m_crit);
		if((pChannel->m_pAPIConn)&&(pChannel->m_pAPIConn->m_Status > 0))  // have to be connected.
		{
			// snapshot the live list data
			if((!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true))
			{
				EnterCriticalSection(&g_adc.m_crit);
				memcpy(&listdata, plistdata, sizeof(tlistdata));
				LeaveCriticalSection(&g_adc.m_crit);
			}

//	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "%s ContinuumChannelThread checking list change", pChannel->m_pszDesc);  Sleep(50); //(Dispatch message)
			if(((listdata.listchanged!=lastlistdata.listchanged)||(listdata.listdisplay!=lastlistdata.listdisplay))&&(pChannel->m_pAPIConn)&&(!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true)) // just do the event chage one as well for now.
			{
//	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "%s ContinuumChannelThread list change, getting events", pChannel->m_pszDesc);  Sleep(50); //(Dispatch message)
				// the list has changed, need to get all the events again  (will take care of any event changes as well)
				EnterCriticalSection(&g_adc.m_crit);
				int nNumEvents = g_adc.GetEvents(pChannel->m_pAPIConn,
					pChannel->m_nHarrisListID, 
					0, //start at the top.
					g_pcontinuum->m_settings.m_bUseListCount?listdata.listcount:listdata.lookahead,
					g_pcontinuum->m_settings.m_bUseListCount?(ADC_GETEVENTS_LIST|ADC_GETEVENTS_USEOFFSET|ADC_GETEVENTS_FULLLIST):(ADC_GETEVENTS_LIST|ADC_GETEVENTS_USEOFFSET)
					);
				LeaveCriticalSection(&g_adc.m_crit);

				if((!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn)) pList = pChannel->m_pAPIConn->m_pList[nZeroList];  // volatile, always reassign
				else pList=NULL;
				if((pList)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))//&&((*pbKillConnThread)!=true))
				{
//	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "%s ContinuumChannelThread list change, got %d events, entering critical", pChannel->m_pszDesc, nNumEvents);  Sleep(50); //(Dispatch message)
					EnterCriticalSection(&(pList->m_crit));  // lock the list out
//	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "%s ContinuumChannelThread list change, in critical", pChannel->m_pszDesc);  Sleep(50); //(Dispatch message)
					if((nNumEvents>=ADC_SUCCESS)&&(pList)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))//&&((*pbKillConnThread)!=true))
					{
						// deal with database.
						if((pdb!=NULL)&&(pdbConn!=NULL)&&(g_pcontinuum->m_settings.m_pszLiveEvents)&&(strlen(g_pcontinuum->m_settings.m_pszLiveEvents)>0)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))
						{
//		g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "%s ContinuumChannelThread dealing with db", pChannel->m_pszDesc);  Sleep(50); //(Dispatch message)

							double dblLastPrimaryEventTime = -1.0;
							unsigned short usLastPrimaryOnAirJulianDate = 0xffff;
							unsigned long ulLastPrimaryOnAirTimeMS = 0xffffffff;
							double dblServerTime = ((double)pChannel->m_pAPIConn->m_usRefJulianDate - 25567.0)*86400.0   // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
											+ (double)pChannel->m_pAPIConn->m_ulRefTimeMS/1000.0;

							int n=0;
							while((!pChannel->m_bKillChannelThread)&&(n<nNumEvents)&&(pChannel->m_pAPIConn))//&&((*pbKillConnThread)!=true))
							{

//		g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "%s ContinuumChannelThread event: %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
								if((pList->m_ppEvents)&&(pList->m_ppEvents[n])&&(pList->m_ppEvents[n]->m_pszID)&&(strlen(pList->m_ppEvents[n]->m_pszID)))
								{

									double dblEventTime = 0.0; 
									unsigned short usOnAirJulianDate = pList->m_ppEvents[n]->m_usOnAirJulianDate;

							//		if(usOnAirJulianDate == 0xffff)  // can't trust event based julian date, have to use server date
//									{
//										_timeb timestamp;
//										_ftime(&timestamp);

//										usOnAirJulianDate = pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.

// move time calc to below julian recalc
//										dblEventTime = ((double)usOnAirJulianDate - 25567.0)*86400  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
//											+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
//									}
/*
									else
									{
										dblEventTime = ((double)pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567.0)*86400    // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
											+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
									}
* /											
//		g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "%s ContinuumChannelThread event: %d [%s] again", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)



									if(usLastPrimaryOnAirJulianDate != 0xffff)
									{
//		g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "%s ContinuumChannelThread event: %d [%s] again1a", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
										usOnAirJulianDate = usLastPrimaryOnAirJulianDate;
									}
									else
									{
//		g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "%s ContinuumChannelThread event: %d [%s] again1b", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
										usOnAirJulianDate = pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.
									}

//		g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "%s ContinuumChannelThread event: %d [%s] again2", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)

									unsigned short usType = pList->m_ppEvents[n]->m_usType;

									if(usType&SECONDARYEVENT)
									{
										if(ulLastPrimaryOnAirTimeMS!=0xffffffff)
										{
											dblEventTime = ((dblLastPrimaryEventTime == -1.0)?0:dblLastPrimaryEventTime)
												+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;

											usOnAirJulianDate = (usLastPrimaryOnAirJulianDate!=0xffff)?usLastPrimaryOnAirJulianDate:pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.

											if(pList->m_ppEvents[n]->m_ulOnAirTimeMS + ulLastPrimaryOnAirTimeMS >= 86400000 )
											{
												//wrapped days.
												usOnAirJulianDate++;
											}
										}
										else
										{
											// no last primary
											dblEventTime = (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
										}
									}
									else
									{
										if(ulLastPrimaryOnAirTimeMS!=0xffffffff)
										{
											if(ulLastPrimaryOnAirTimeMS > pList->m_ppEvents[n]->m_ulOnAirTimeMS)
											{
												// we wrapped days.
												usOnAirJulianDate++;
											}
										}

										dblEventTime = ((double)usOnAirJulianDate - 25567.0)*86400.0  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
											+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;

										ulLastPrimaryOnAirTimeMS = pList->m_ppEvents[n]->m_ulOnAirTimeMS;
										dblLastPrimaryEventTime = dblEventTime;
										usLastPrimaryOnAirJulianDate = usOnAirJulianDate;
									}
//		g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "%s ContinuumChannelThread event: %d [%s] again3", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)

									char* pchID = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszID);
									char* pchTitle = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszTitle);
									char* pchRecKey = NULL;
									char* pchData = NULL;
									
									if(pList->m_ppEvents[n]->m_pszData) 
										pchData = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszData);
									if(pList->m_ppEvents[n]->m_pszReconcileKey) 
										pchRecKey = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszReconcileKey);


	//						select event with server, list, start time +/- tolerance, and clip ID
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE \
server_name = '%s' AND \
server_list = %d AND \
event_clip = '%s' AND \
event_title = '%s' AND \
event_start >= %f AND \
event_start <= %f",
											g_pcontinuum->m_settings.m_pszLiveEvents,
											pChannel->m_pszServerName,
											pChannel->m_nHarrisListID,
											pchID?pchID:pList->m_ppEvents[n]->m_pszID,
											pchTitle?pchTitle:pList->m_ppEvents[n]->m_pszTitle,
											dblEventTime-0.3, dblEventTime+0.3
										);

//		g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "ContinuumChannelThread event: %s", szSQL);  Sleep(50); //(Dispatch message)
									// get
									CRecordset* prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
									int nNumFound = 0;
									int nID = -1;
									if(prs != NULL) 
									{
										if(!prs->IsEOF())
										{
											CString szValue;
											try
											{
												// get id!
												prs->GetFieldValue("itemid", szValue);
												nID = atoi(szValue);
											}
											catch( ... )
											{
											}

											nNumFound++;
											// prs->MoveNext();
										}
										prs->Close();
										delete prs;
									}

									if((!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true))
									{
									if((nNumFound>0)&&(nID>0))
									{
										// update it.
										// if found update it with new values, and a found flag (app_data_aux int will be the found flag 1=found)
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
server_name = '%s', \
server_list = %d, \
list_id = %d, \
event_id = '%s', \
event_clip = '%s', \
event_title = '%s', \
%s%s%s\
event_type = %d, \
event_status = %d, \
event_time_mode = %d, \
event_start = %f, \
event_duration = %d, \
event_last_update = %f, \
app_data_aux = 1 WHERE itemid = %d;",  // 1 is the found flag.
											g_pcontinuum->m_settings.m_pszLiveEvents,  // table name
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
											pChannel->m_nChannelID, // list_id - internal lookup channel number
											pList->m_ppEvents[n]->m_pszReconcileKey?pList->m_ppEvents[n]->m_pszReconcileKey:"", //event_id
											pchID?pchID:pList->m_ppEvents[n]->m_pszID, //event_clip  (must not be null)
											pchTitle?pchTitle:"", //event_title
											pchData?"event_data = '":"", //event_data (exists)
											pchData?pchData:"", //event_data  
											pchData?"', ":"", //event_data (exists)
											pList->m_ppEvents[n]->m_usType, //event_type
											pList->m_ppEvents[n]->m_usStatus, //event_status
											pList->m_ppEvents[n]->m_usControl, //event_time_mode
											dblEventTime, //event_start
											pList->m_ppEvents[n]->m_ulDurationMS, //event_duration
											dblServerTime, //event_last_update
											nID // the unique ID of the thing
										);
	// app_data = , // no app data for the above, let it be NULL or whatever it is.




									}
									else
									{
										// add it.
										// if not found, insert one with a found flag
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
server_name, \
server_list, \
list_id, \
event_id, \
event_clip, \
event_title, \
%s\
event_type, \
event_status, \
event_time_mode, \
event_start, \
event_duration, \
event_last_update, \
app_data_aux) VALUES ('%s', %d, %d, '%s', '%s', '%s', %s%s%s%d, %d, %d, %f, %d, %f, 1)",  // 1 is the found flag.
											g_pcontinuum->m_settings.m_pszLiveEvents,  // table name
											pList->m_ppEvents[n]->m_pszData?"event_data, ":"", //event_data (exists)
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
											pChannel->m_nChannelID, // list_id - internal lookup channel number
											pList->m_ppEvents[n]->m_pszReconcileKey?pList->m_ppEvents[n]->m_pszReconcileKey:"", //event_id
											pchID?pchID:pList->m_ppEvents[n]->m_pszID, //event_clip  (must not be null)
											pchTitle?pchTitle:"", //event_title
											pchData?"event_data = '":"", //event_data (exists)
											pchData?pchData:"", //event_data  
											pchData?"', ":"", //event_data (exists)
											pList->m_ppEvents[n]->m_usType, //event_type
											pList->m_ppEvents[n]->m_usStatus, //event_status
											pList->m_ppEvents[n]->m_usControl, //event_time_mode
											dblEventTime, //event_start
											pList->m_ppEvents[n]->m_ulDurationMS, //event_duration
											dblServerTime //event_last_update
											);

									}
									}
									

									if(pchID) free(pchID);
									if(pchTitle) free(pchTitle);
									if(pchRecKey) free(pchRecKey);
									if(pchData) free(pchData);


//		g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "ContinuumChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

									if((!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true))
									{
									if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
									{
										//**MSG
									}
									}
								}
								n++;
							}

							///////////////////////
							// remove found flags.

	//					remove all with no found flag.

							if((!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true))
							{

							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d AND \
app_data_aux < 1",
											g_pcontinuum->m_settings.m_pszLiveEvents,  // table name
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
										);
//		g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "ContinuumChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

							if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
							{
								//**MSG
							}
	//					remove all found flags for next go-around.
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
app_data_aux = 0 WHERE \
server_name = '%s' AND \
server_list = %d",
											g_pcontinuum->m_settings.m_pszLiveEvents,  // table name
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
										);
//		g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "ContinuumChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

							if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
							{
								//**MSG
							}

						
							// increment exchange mod counter. for events table.
							EnterCriticalSection(&g_pcontinuum->m_data.m_critIncrement);
							if (g_pcontinuum->m_data.IncrementDatabaseMods(g_pcontinuum->m_settings.m_pszLiveEvents, dberrorstring)<CONTINUUM_SUCCESS)
							{
								//**MSG
							}
							LeaveCriticalSection(&g_pcontinuum->m_data.m_critIncrement);
							}
						}
						// record last values if get was successful.
						memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
					}
					else //if((pList)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))  // plist is null if no events in list.
					if(nNumEvents == 0)  // but this is correct
					{
						// record last values if get was successful.
						memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
					}
//		g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "ContinuumChannelThread leaving critical");  Sleep(20); //(Dispatch message)
					LeaveCriticalSection(&(pList->m_crit));
//		g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "%s ContinuumChannelThread left critical", pChannel->m_pszDesc);  Sleep(20); //(Dispatch message)
				}//if((pList)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))
			} //			if(((listdata.listchanged!=lastlistdata.listchanged)||(listdata.listdisplay!=lastlistdata.listdisplay))&&(pChannel->m_pAPIConn)&&(!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true)) // just do the event chage one as well for now.

/*			else
			if(listdata.listdisplay!=lastlistdata.listdisplay)
			{
				// an event has changed, need to find it and just deal with that one.
				// record last values
				memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
			}
* /
		}

	}

	//		remove all, since not active, want to purge list.
	if(
			(pdb!=NULL)&&(pdbConn!=NULL)
			&&(g_pcontinuum->m_settings.m_pszLiveEvents)
			&&(strlen(g_pcontinuum->m_settings.m_pszLiveEvents)>0)
			&&(pChannel)
			&&(pChannel->m_pszServerName)
			&&(strlen(pChannel->m_pszServerName)>0)
		)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d",
											g_pcontinuum->m_settings.m_pszLiveEvents,  // table name
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
										);
//g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "ContinuumChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

		if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
		{
			//**MSG
		}
		// increment exchange mod counter. for events table.
		EnterCriticalSection(&g_pcontinuum->m_data.m_critIncrement);
		if (g_pcontinuum->m_data.IncrementDatabaseMods(g_pcontinuum->m_settings.m_pszLiveEvents, dberrorstring)<CONTINUUM_SUCCESS)
		{
//g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:debug", "Increment Error: %s", dberrorstring);  Sleep(50); //(Dispatch message)
			//**MSG
		}
		LeaveCriticalSection(&g_pcontinuum->m_data.m_critIncrement);

	}
	g_pcontinuum->m_msgr.DM(MSG_ICONHAND, NULL, "Continuum:list", "List management thread for %s:%d (%s) is ending.",
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
											pChannel->m_pszDesc
		);  Sleep(100); //(Dispatch message)

	if(pChannel->m_pAPIConn)	pChannel->m_pAPIConn->m_bListActive[nZeroList] = false;
	pChannel->m_bChannelThreadStarted = false;
	_endthread();
}
*/
