// ContinuumData.h: interface for the CContinuumData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CONTINUUMDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
#define AFX_CONTINUUMDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <winsock2.h>
#include <sys/timeb.h>
#include "../../Common/TXT/BufferUtil.h" 
#include "../../Common/LAN/NetUtil.h" 
//#include "../../Common/MFC/ODBC/DBUtil.h"
#include "../../Common/KEY/LicenseKey.h"


class CContinuumObject  
{
public:
	CContinuumObject();
	virtual ~CContinuumObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	char* m_pszProcessName;
	char* m_pszStatus;
	char* m_pszIconPath;
};


class CContinuumData  
{
public:
	CContinuumData();
	virtual ~CContinuumData();

	// util object
	CBufferUtil m_bu;

  // hostname.  have to get it, store it here
	char*	m_pszHost;	// the name of the host

	char*	m_pszCortexHost;	// the name of the cortex host
	unsigned short m_usCortexCommandPort;
	unsigned short m_usCortexStatusPort;

	CContinuumObject** m_ppObj;
	int m_nNumObjects;

	_timeb m_timebTick; // the last time check inside the thread
	_timeb m_timebLastStatus; // the time of the last status given to cortex
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for cortex global status (each obj has their own as well)

//	int m_nSettingsMod;
//	int m_nChannelsMod;
//	int m_nConnectionsMod;
//	int m_nLastSettingsMod;
//	int m_nLastChannelsMod;
//	int m_nLastConnectionsMod;

	char* GetStatusText(unsigned long* pulStatus);  // allocates mem, must free after use
	int		SetStatusText(char* pszText, unsigned long ulStatus);
	int		GetHost();

//	CDBUtil* m_pdb;
//	CDBconn* m_pdbConn;
//	int CheckDatabaseMods(char* pszInfo=NULL);
//	int IncrementDatabaseMods(char* pszTableName, char* pszInfo=NULL);
//	int GetConnections(char* pszInfo=NULL);
//	int GetChannels(char* pszInfo=NULL);

	CLicenseKey m_key;

	bool m_bQuietKill;
	CRITICAL_SECTION m_critIncrement; 
//	CRITICAL_SECTION m_critSQL;

private:
	CRITICAL_SECTION m_critText;
	char* m_pszStatus;	// parseable string

//	unsigned long m_ulReqCount;
//	unsigned long m_ulSvcCount;
};

#endif // !defined(AFX_CONTINUUMDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
