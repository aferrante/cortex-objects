// PromoLiteSettings.cpp: implementation of the CPromoLiteSettings.
//
//////////////////////////////////////////////////////////////////////
#include <stdafx.h>  // for promolite debug
#include "PromoLiteMain.h"   // for promolite debug
#include "PromoLiteDefines.h"
#include "PromoLiteSettings.h"
//#include "..\Radiance\RadianceDefines.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CPromoLiteMain* g_ppromolite;
extern CPromoLiteApp theApp;



//////////////////////////////////////////////////////////////////////
// CPromoLiteAutomationChannelObject Construction/Destruction
//////////////////////////////////////////////////////////////////////
CPromoLiteAutomationChannelObject::CPromoLiteAutomationChannelObject()
{
	m_pszServerName = NULL;
	m_pszDesc = NULL;
	m_ulStatus = 0;  // various states
	m_ulFlags =0;   // various flags
	m_usType=0;

	m_nChannelID=-1;  // the unique Channel ID within Sentinel setup. (assigned externally)
	m_nHarrisListNum=-1;  // the 1-based List # on the associated ADC-100 server
	m_nOmnibusPort=10540;

	m_pPromoLite = NULL;
	m_pEndpoint = NULL;
	

	m_dblServertime=-1.0;
	m_dblServerLastUpdate=-1.0;
	m_nServerStatus=-1;
	m_nServerBasis=-1;
	m_nServerChanged=-1;

	m_dblListLastUpdate=-1.0;
	m_nListState=-1;
	m_nListChanged=-1;
	m_nListDisplay=-1;
	m_nListSysChange=-1;
	m_nListCount=-1;
	m_nListLookahead=-1;

// control
	m_bKillAutomationThread = true;
	m_bAutomationThreadStarted = false;

	m_bTriggerEventsChanged = false;
	m_bTriggerThreadStarted = false;
//	m_bFarAnalysisThreadStarted=false;
//	m_bNearAnalysisThreadStarted=false;
//	m_bFarEventsChanged= false;
//	m_bNearEventsChanged= false;
//	m_bNearEventsChangedDelayed = false;

	m_dblAutomationTimeEstimate = -1.0; // the harris server time estimate
	m_dblAutomationTimeDiffMS=0.0; // difference in milliseconds between the time harris was set and the time it was read

	m_nAnalysisCounter=0;

	m_ppEvents = NULL;
	m_nEvents = 0;


	//  Lyric specific - currently assuming one Chyron box per  - need to change at some point
	m_nLastLyricAlias = -1;


	m_ppLyricAlias = NULL;
	m_nLastAlias=-1;
	m_nNumAliasArray=0;


//	m_nCurrentMessage = -1;
//	m_bMessageLoaded = false;
//	m_bMessageRead = false;

	m_nMinLyricAlias=-1;
	m_nMaxLyricAlias=-1;

}





CPromoLiteAutomationChannelObject::~CPromoLiteAutomationChannelObject()
{
	m_bKillAutomationThread = true;
	while((m_bTriggerThreadStarted)||(m_bAutomationThreadStarted)) Sleep(1);
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	
}


				//NUCLEUS->PROMOLITE_CHANGE

double CPromoLiteAutomationChannelObject::ReturnTriggerTickTime()
{
	_ftime(&m_timebTriggerTick);
	return (((double)(m_timebTriggerTick.time - (m_timebTriggerTick.timezone*60)+ (m_timebTriggerTick.dstflag?3600:0)))+((double)(m_timebTriggerTick.millitm + (g_ppromolite?g_ppromolite->m_settings.m_nTriggerAdvanceMS:0)))/1000.0);
}


int CPromoLiteAutomationChannelObject::ReturnAliasIndex(CString szMessage)
{
	if((m_ppLyricAlias)&&(m_nNumAliasArray))
	{
		int i=0;
		while(i<m_nNumAliasArray)
		{
			if(m_ppLyricAlias[i]->szMessage.Compare(szMessage)==0) return i;
			i++;
		}
	}
	return PROMOLITE_ERROR;
}

int CPromoLiteAutomationChannelObject::AddAlias(CString szMessage)
{
	// add means just find the place in the ring buffer and deal.
	if((m_ppLyricAlias)&&(m_nNumAliasArray))
	{
		int i=m_nLastAlias+1;
		if((i<0)||(i>=m_nNumAliasArray))
		{
			i=0;
		}
		
		if(m_ppLyricAlias[i]==NULL)
		{
			m_ppLyricAlias[i] = new LyricAlias_t;
		}

		if(m_ppLyricAlias[i])
		{
			if(m_nLastLyricAlias<0)
			{
				m_nLastLyricAlias = m_nMinLyricAlias;
			}
			else
			{
				m_nLastLyricAlias++;  
				if(m_nLastLyricAlias>m_nMaxLyricAlias)
					m_nLastLyricAlias = m_nMinLyricAlias;
			}

			m_ppLyricAlias[i]->szMessage = szMessage;
			m_ppLyricAlias[i]->nAlias = m_nLastLyricAlias;
			m_ppLyricAlias[i]->bMessageLoaded = false;
			m_ppLyricAlias[i]->bMessageRead = false;

			m_nLastAlias = i;

			return i;
		}
		
	}
	return PROMOLITE_ERROR;
}



//////////////////////////////////////////////////////////////////////
// CPromoLiteEndpointObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPromoLiteEndpointObject::CPromoLiteEndpointObject()
{
	m_ulStatus	= PROMOLITE_STATUS_UNINIT;
	m_ulFlags = PROMOLITE_FLAG_DISABLED;  // various states
	m_usType = PROMOLITE_DEP_UNKNOWN; // not used
	m_pszName = NULL;
	m_pszDBName = NULL;
	m_pszQueue = NULL;  // the Queue table name
	m_pszExchange= NULL;  // the Exchange table name
	m_pszLiveEvents= NULL;  // the LiveEvents table name if applicable
	m_pszDestination= NULL;  // the Destination table name if applicable
	m_pszDestinationMedia = NULL;  // the DestinationsMedia table name if applicable
	m_pszChannel= NULL;  // the Channel table name if applicable
	m_pszMetadata= NULL;  // the Metadata table name if applicable
	m_pszFileTypes = NULL;
	m_pszConnections = NULL;  // the Connections table name if applicable
	m_pszCommandQueue = NULL;

	m_nModName = -1;
	m_nModDBName = -1;
	m_nModQueue = -1;  // the Queue table name
	m_nModExchange = -1;  // the Exchange table name
	m_nModLiveEvents = -1;  // the LiveEvents table name if applicable
	m_nModDestination = -1;  // the Destination table name if applicable
	m_nModDestinationMedia = -1;  // the DestinationsMedia table name if applicable
	m_nModChannel = -1;  // the Channel table name if applicable
	m_nModMetadata = -1;  // the Metadata table name if applicable
	m_nModFileTypes = -1;  // the File types table name if applicable
	m_nModConnections = -1;  // the Connections table name if applicable
	m_nModChannels = -1;  // the Channels table name if applicable

	m_nLastModName = -100;
	m_nLastModDBName = -100;
	m_nLastModQueue = -100;  // the Queue table name
	m_nLastModExchange = -100;  // the Exchange table name
	m_nLastModLiveEvents = -100;  // the LiveEvents table name if applicable
	m_nLastModDestination = -100;  // the Destination table name if applicable
	m_nLastModDestinationMedia = -100;  // the DestinationsMedia table name if applicable
	m_nLastModChannel = -100;  // the Channel table name if applicable
	m_nLastModMetadata = -100;  // the Metadata table name if applicable
	m_nLastModFileTypes = -100;  // the File types table name if applicable
	m_nLastModConnections = -100;  // the Connections table name if applicable
	m_nLastModChannels = -100;  // the Channels table name if applicable

//	m_dblLastAutomationChange = -1.0;

	m_ppChannelObj = NULL;
	m_nNumChannelObjects=0;


}

CPromoLiteEndpointObject::~CPromoLiteEndpointObject()
{

	if(m_ppChannelObj)
	{
		int i=0;
		while(i<m_nNumChannelObjects)
		{
			if(m_ppChannelObj[i])
			{
				m_ppChannelObj[i]->m_bKillAutomationThread = true;
				delete m_ppChannelObj[i]; // delete objects, must use new to allocate
			}
			i++;
		}
		delete [] m_ppChannelObj; // delete array of pointers to objects, must use new to allocate
	}

	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_pszDBName) free(m_pszDBName); // must use malloc to allocate
	if(m_pszQueue) free(m_pszQueue); // must use malloc to allocate
	if(m_pszExchange) free(m_pszExchange); // must use malloc to allocate
	if(m_pszLiveEvents) free(m_pszLiveEvents); // must use malloc to allocate
	if(m_pszDestination) free(m_pszDestination); // must use malloc to allocate
	if(m_pszDestinationMedia) free(m_pszDestinationMedia); // must use malloc to allocate
	if(m_pszChannel) free(m_pszChannel); // must use malloc to allocate
	if(m_pszConnections) free(m_pszConnections); // must use malloc to allocate
	if(m_pszMetadata) free(m_pszMetadata); // must use malloc to allocate
	if(m_pszFileTypes) free(m_pszFileTypes); // must use malloc to allocate
	if(m_pszCommandQueue) free(m_pszCommandQueue); // must use malloc to allocate
}

int CPromoLiteEndpointObject::IncrementDatabaseMods(CDBUtil* pdb, CDBconn* pdbConn, char* pszTableName, char* pszInfo)
{
	if((pdbConn)&&(pdb)&&(pszTableName)&&(strlen(pszTableName)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );
	
	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s set mod = \
(SELECT case when max(mod) is null then 1 when max(mod) >= %d \
then 1 else max(mod) + 1 end from %s.dbo.%s WHERE criterion = '%s') WHERE criterion = '%s'",
		m_pszDBName?m_pszDBName:m_pszName, m_pszExchange?m_pszExchange:"Exchange",
		PROMOLITE_DB_MOD_MAX,
		m_pszDBName?m_pszDBName:m_pszName, m_pszExchange?m_pszExchange:"Exchange",
		szTemp, szTemp		
		);
		
		EnterCriticalSection(&g_ppromolite->m_data.m_critSQL);
		if(pdb->ExecuteSQL(pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);
			return PROMOLITE_SUCCESS;
		}
		LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);
	}
	return PROMOLITE_ERROR;
}


int CPromoLiteEndpointObject::CheckDatabaseMod(CDBUtil* pdb, CDBconn* pdbConn, char* pszTableName, char* pszInfo)
{
	if((pdbConn)&&(pdb)&&(m_pszDBName)&&(strlen(m_pszDBName))&&(pszTableName)&&(strlen(pszTableName)))
	{		
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s.dbo.%s", 
			m_pszDBName,
			((m_pszExchange)&&(strlen(m_pszExchange)))?m_pszExchange:"Exchange");

//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "endpoint SQL: %s", szSQL);   Sleep(50);//(Dispatch message)

		EnterCriticalSection(&g_ppromolite->m_data.m_critSQL);
		CRecordset* prs = pdb->Retrieve(pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = PROMOLITE_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCriterion;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
				}

				szTemp.Format("DBT_%s", pszTableName);
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					nReturn = atoi(szMod);
					if(nReturn>0) 
					{
						prs->Close();

						delete prs;
						LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);
						return nReturn;
					}
				}

				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			delete prs;
			LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);
			return nReturn;
		}
	}
	LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);
	return PROMOLITE_ERROR;
}



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPromoLiteSettings::CPromoLiteSettings()
{
	m_pdb = NULL;
	m_pdbConn = NULL;

	m_pszName = NULL;
	m_pszType = NULL;  // familiar name of the type.
	m_pszProject = NULL;  // familiar name of the project name.
	m_ulMainMode = PROMOLITE_MODE_DEFAULT;

	m_nThreadDwellMS = 1000;
	m_bDebugLists= false;  // if true, dumps the temp and events databases
	m_nDebugListTopCount=-1;  //if positive, just prints out the top n recored in the events tables.
	m_bDebugSQL=false;  // if true, prints out the SQL calls
	m_bDebugInsertSQL=false;  // if true, prints out the event insert SQL calls
	m_ulDebug = 0;
	m_pszDebugOrder = NULL;
	m_bUseUTF8 = false;

	m_nAutoPurgeMessageDays = 30; // default
	m_nAutoPurgeAsRunDays = 30; // default
	m_nAutoPurgeInterval = 300;  // 5 minutes should be enough.

	// ports
	m_usCommandPort	= PROMOLITE_PORT_CMD;
	m_usStatusPort	= PROMOLITE_PORT_STATUS;

	// messaging for PromoLite
	m_bUseLog = true;			// write a log file
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host
	m_pszFileSpec = NULL;
	m_pszMailSpec = NULL;
	m_pszProcessedFileSpec = NULL;
	m_pszProcessedMailSpec = NULL;
	m_bUseXMLClientLog = false;
	m_bMillisecondMessaging=true;			// use millisecond resolution for messages and asrun

	// Harris API
//	m_bUseListCount = false; // get all events up until the list count (otherwise just up to the lookahead)
	m_nHarrisStatusTriggerExclude = 1048593;  // post roll, playing, and post-upcounting events.


	// installed dependencies
	m_ppEndpointObject=NULL; // which endpoint module(s) is (are) installed.
	m_nNumEndpointsInstalled=0; // number of endpoint module(s) is (are) installed.

	m_bReportSuccessfulOperation=false;
	m_bLogTransfers=false;

	// DSN params
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;

	m_pszDefaultDB = NULL;  // the Default DB name

	//NUCLEUS->PROMOLITE_CHANGE
	m_pszDestinationHost = NULL;
	m_pszDestinationModule = NULL;
	m_nDestinationType=PROMOLITE_RULE_DESTTYPE_UNKNOWN;
	m_nEventBufferIdleRefreshMS = 2000;  // the amount of time between triggers, where if we have time, we'll refresh da buffa
	m_nDoneCount=3; // number of done events to keep.
	m_nRecycleSeconds=0;  // number of seconds to keep recycling done events.


	m_pszSettings = NULL;  // the Settings table name
	m_pszExchange = NULL;  // the Exchange table name
	m_pszMessages = NULL;  // the Messages table name
	m_pszAsRun = NULL;

	m_pszQueue = NULL;  // the Queue table name
//	m_pszChannels = NULL;  // the Channels table name
//	m_pszConnections = NULL;  // the Connections table name
	m_pszEvents = NULL; // the Events table name
	m_pszMappings = NULL;  // the Mapping table name
	m_pszEventRules = NULL;  // the EventRules view name
	m_pszTriggerAnalysisView = NULL;  // the TriggerAnalysisView name
	m_pszAnalysis = NULL;  // the Analysis table name
	m_pszAnalyzedTriggerData = NULL;  // the AnalyzedTriggerData table name
	m_pszAnalyzeRulesProc = NULL;  // the spAnalyzeRules stored procedure
	m_pszGetParameterValueProc = NULL;  // the spGetParameterValue stored procedure
	m_pszParameterRuleView = NULL;  // the  ParameterRuleView name
	m_pszMetaConfigUnionALLView = NULL;  // the  ParameterRuleView name
	m_pszChannelInfo = NULL;
	m_pszTriggerInfoView = NULL;

	m_pszCortexDB = NULL;			// the Cortex DB name
	m_pszLiveEventData = NULL;  // the LiveEventData table name

/*
	m_pszDefaultProject = NULL;
	m_pszDefaultScene = NULL; 
	m_pszDefaultHost = NULL; 
*/

//	m_pszHarrisAutomationServer = NULL;
//	m_nHarrisAutomationList = 1;

	m_nAutomationIntervalMS = 3000;  // just more than preroll amount seems good.
	m_nAnalyzeAutomationDwellMS=250; // number of milliseconds trigger buffer refresh is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once.
	m_nAnalyzeAutomationForceMS=2000; // number of milliseconds after a change, that if there are changes pending, we MUST be force trigger buffer refresh 
	m_nAnalyzeRulesDwellMS = 2000; // number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once.
	m_nAnalyzeParameterDwellMS = 2500; // number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once.
	m_nAnalyzeTimingDwellMS = 1000;  //after rules are analyzed, how much time to wait before analyzing timing columns
	m_nAnalyzeHarrisStatusExclude = 19;  // (not play, post, done)

	m_nAnalyzeRulesIntervalMS=1000; // number of milliseconds between automation checks that cause rules analysis
	m_nAnalyzeRulesForceMS=5000; // number of milliseconds after a change, that if there are changes pending, we MUST be force rules analysis 
	m_nAnalyzeParameterForceMS = 5000; // number of milliseconds to wait before forcing this to run.
	m_nTriggerNotificationDwellMS = 1000; // number of milliseconds trigger notification is delayed after a change 

	m_bEnableAnalysisTriggerNotification=true;
	m_bEnableEventTriggerNotification=false;
	m_bChannelInfoViewHasDesc = true;


	m_bUseLocalClock = false;
	m_bUseUTC = false;

	m_bFarAnalysisUsesCalc = true;
	m_bNearAnalysisUsesCalc = true;
	m_nTriggerBuffer  =12;
	m_nTriggerExclusionDelayMS = 10000;  

	m_nInterTriggerDelayMS = 50; // number of milliseconds to delay between SQL queries for triggers within an event
	m_nInterEventDelayMS = 3000; // number of milliseconds to delay between processing each event.


	m_pszSystemFolderPath = NULL;			// the path of the folder used for parse files etc.

/*
	// gfx module
	m_pszGfxHost = NULL; 
	m_nGfxPort = 10682; 
*/


	//imagestore specific 
//	m_pszImagestoreNullFile = NULL;			// the filename of the nullfile - must be in the system folder
//	m_pszIntuitionSearchExt = NULL;				// search extensions for Intuition
//	m_pszImagestore2SearchExt = NULL;			// search extensions for IS2
//	m_pszImagestore300SearchExt = NULL;		// search extensions for IS300
//	m_pszImagestoreHDSearchExt = NULL;		// search extensions for ISHD

	// endpoint autodeletions
//	m_bAutoDeleteOldest = true;
//	m_ulDeletionThreshold = 172800; // two days in seconds

	m_nMinLyricAlias = 9900;
	m_nMaxLyricAlias = 9999;
	m_bReleaseIsV_5_15 = true;

	m_nLayoutSalvoFormat =PROMOLITE_HARRIS_ICONII_LSFORMAT_NUM4;

	m_ulModsIntervalMS = 6000;
//	m_nTransferFailureRetryTime = 60;  // seconds - one minute as a default
	m_pszLicense=NULL;  // the License Key
	m_pszIconPath=NULL;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

	m_pszCloseAll=NULL; // the close all token
	m_pszOEMcodes=NULL;  // the possible OEM string
	m_pszStopAll=NULL;  // the stop (but not close) all token.


	m_ulAutomationLookahead = 0;  // number of events to analyze (per channel) 0 = analyze all.
	m_ulTriggerReanalyzeThreshold = 60; // number of seconds before previously analyzed on air time, to re-check event.
	m_ulTriggerStandbyThreshold   = 25; // number of seconds before re-analyzed on air time, to set standby, which means download stuff and search harris automation for info to assemble.
	m_ulTriggerCueThreshold       = 20; // number of seconds before re-analyzed on air time, to set cued, which means check scene loaded.
	m_nTriggerAdvanceMS          = 0; // number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)

}

CPromoLiteSettings::~CPromoLiteSettings()
{
	if(m_pszSystemFolderPath) free(m_pszSystemFolderPath); // must use malloc to allocate
//	if(m_pszImagestoreNullFile) free(m_pszImagestoreNullFile); // must use malloc to allocate

	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_pszType) free(m_pszType); // must use malloc to allocate
	if(m_pszProject) free(m_pszProject); // must use malloc to allocate
	if(m_pszDSN) free(m_pszDSN); // must use malloc to allocate
	if(m_pszUser) free(m_pszUser); // must use malloc to allocate
	if(m_pszPW) free(m_pszPW); // must use malloc to allocate
	if(m_pszDefaultDB) free(m_pszDefaultDB); // must use malloc to allocate

	if(m_pszSettings) free(m_pszSettings); // must use malloc to allocate
	if(m_pszExchange) free(m_pszExchange); // must use malloc to allocate
	if(m_pszMessages) free(m_pszMessages); // must use malloc to allocate
	if(m_pszAsRun) free(m_pszAsRun); // must use malloc to allocate

	if(m_pszCortexDB) free(m_pszCortexDB); // must use malloc to allocate
	if(m_pszLiveEventData) free(m_pszLiveEventData); // must use malloc to allocate
	if(m_pszDebugOrder) free(m_pszDebugOrder); // must use malloc to allocate	


//	if(m_pszChannels) free(m_pszChannels); // must use malloc to allocate
//	if(m_pszConnections) free(m_pszConnections); // must use malloc to allocate
	if(m_pszEvents) free(m_pszEvents); // must use malloc to allocate
	if(m_pszQueue) free(m_pszQueue); // must use malloc to allocate
	if(m_pszMappings) free(m_pszMappings); // must use malloc to allocate
	if(m_pszEventRules) free(m_pszEventRules); // must use malloc to allocate
	if(m_pszLicense) free(m_pszLicense); // must use malloc to allocate
	if(m_pszOEMcodes) free(m_pszOEMcodes); // must use malloc to allocate
	if(m_pszIconPath) free(m_pszIconPath); // must use malloc to allocate	
//	if(m_pszLiveEvents) free(m_pszLiveEvents); // must use malloc to allocate
	if(m_pszAnalysis) free(m_pszAnalysis); // must use malloc to allocate
	if(m_pszTriggerAnalysisView) free(m_pszTriggerAnalysisView); // must use malloc to allocate
	if(m_pszAnalyzedTriggerData) free(m_pszAnalyzedTriggerData); // must use malloc to allocate
	if(m_pszAnalyzeRulesProc) free(m_pszAnalyzeRulesProc); // must use malloc to allocate
	if(m_pszGetParameterValueProc) free(m_pszGetParameterValueProc); // must use malloc to allocate
	if(m_pszParameterRuleView) free(m_pszParameterRuleView); // must use malloc to allocate
	if(m_pszMetaConfigUnionALLView) free(m_pszMetaConfigUnionALLView); // must use malloc to allocate
	if(m_pszChannelInfo) free(m_pszChannelInfo); // must use malloc to allocate
	if(m_pszTriggerInfoView) free(m_pszTriggerInfoView); // must use malloc to allocate

	
	
//	if(m_pszHarrisAutomationServer) free(m_pszHarrisAutomationServer); // must use malloc to allocate

	/*
	if(m_pszDefaultProject) free(m_pszDefaultProject); // must use malloc to allocate
	if(m_pszDefaultScene) free(m_pszDefaultScene); // must use malloc to allocate
	if(m_pszDefaultHost) free(m_pszDefaultHost); // must use malloc to allocate
*/

	if(m_pszFileSpec) free(m_pszFileSpec); // must use malloc to allocate	
	if(m_pszMailSpec) free(m_pszMailSpec); // must use malloc to allocate	
	if(m_pszProcessedFileSpec) free(m_pszProcessedFileSpec); // must use malloc to allocate	
	if(m_pszProcessedMailSpec) free(m_pszProcessedMailSpec); // must use malloc to allocate	

//	if(m_pszGfxHost) free(m_pszGfxHost); // must use malloc to allocate

	if(m_ppEndpointObject)
	{
		int i=0;
		while(i<m_nNumEndpointsInstalled)
		{
			if(m_ppEndpointObject[i]) delete m_ppEndpointObject[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppEndpointObject; // delete array of pointers to objects, must use new to allocate
	}

}

int CPromoLiteSettings::Settings(bool bRead)
{
	char pszFilename[MAX_PATH];
	CFileUtil file;
	// get settings.
	strcpy(pszFilename, "");
//		AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, PROMOLITE_SETTINGS_FILE_DEFAULT);  // cortex settings file

	file.GetSettings(pszFilename, false); 

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
		// load up the values on the settings object
			m_pszName = file.GetIniString("Main", "Name", "PromoLite", m_pszName);
			m_pszType = file.GetIniString("Main", "Type", "PromoLite", m_pszType);
			m_pszProject = file.GetIniString("Main", "Project", "VDS", m_pszProject);
			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);
			m_pszOEMcodes = file.GetIniString("License", "COM", "C1JL-sPLisQ5VCQqCR2LJwJPdlTK", m_pszOEMcodes);
			m_nThreadDwellMS = file.GetIniInt("Main", "ThreadDwellMS", 1000);

			m_bDebugLists = file.GetIniInt("Database", "DebugLists", 0)?true:false;  // if true, dumps the temp and events databases
			m_nDebugListTopCount = file.GetIniInt("Database", "DebugListTopCount", -1);  //if positive, just prints out the top n recored in the events tables.
			m_bDebugSQL = file.GetIniInt("Database", "DebugSQL", 0)?true:false;  // if true, prints out the SQL calls
			m_bDebugInsertSQL = file.GetIniInt("Database", "DebugInsertSQL", 0)?true:false;  // if true, prints out the event insert SQL calls
			m_ulDebug = file.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.
			m_pszDebugOrder = file.GetIniString("Database", "DebugOrder", "(case when parent_position < 0 then ((event_position)/1000.0) else (parent_calc_start + (event_position-parent_position)/1000.0) end)", m_pszDebugOrder); // the order by clause for debug

			// compile license key params
			if(g_ppromolite->m_data.m_key.m_pszLicenseString) free(g_ppromolite->m_data.m_key.m_pszLicenseString);
			g_ppromolite->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(m_pszLicense)+1);
			if(g_ppromolite->m_data.m_key.m_pszLicenseString)
			sprintf(g_ppromolite->m_data.m_key.m_pszLicenseString, "%s", m_pszLicense);

			g_ppromolite->m_data.m_key.InterpretKey();

			char errorstring[MAX_MESSAGE_LENGTH];
			if(g_ppromolite->m_data.m_key.m_bValid)
			{
				unsigned long i=0;
				while(i<g_ppromolite->m_data.m_key.m_ulNumParams)
				{
					if((g_ppromolite->m_data.m_key.m_ppszParams)
						&&(g_ppromolite->m_data.m_key.m_ppszValues)
						&&(g_ppromolite->m_data.m_key.m_ppszParams[i])
						&&(g_ppromolite->m_data.m_key.m_ppszValues[i]))
					{
						if(stricmp(g_ppromolite->m_data.m_key.m_ppszParams[i], "max")==0)
						{
	//						g_ppromolite->m_data.m_nMaxLicensedDevices = atoi(g_ppromolite->m_data.m_key.m_ppszValues[i]);
						}
						else
						if(stricmp(g_ppromolite->m_data.m_key.m_ppszParams[i], "oem")==0)
						{
							// if it exists, check OEM string.

						// for OEM partner check on license key, need oem=xxxr in the params'
//oem=xxxr where xxx is a client code string, and r is any random ASCII character in the range 33 to 126, excluding URL-sensitive characters (34, 38, 39, 46, 47, 60, 61, 62, 63).
//the oem parameter should be the last thing in the string, and the last random character allows us to keep the encoding more secure.
//the client codes for OEM suppliers is as follows (note these ARE case sensitive):

//Strategy and Technology (S&T): SnT
//Harris: HAS
//Softel: SFT
//Chyron: Chy
//Ensequence: Ens
//VDS: VDS (for test purposes)

//If an OEM code is specified in the license key, a supplier code table including these values must exist in the CSF file, for it to match on. 
//This will be stored in a CSF parameter under the [License] heading, called COM for Cortex OEM Management (haha).

//[License]
//COM=C1JL-sPLisQ5VCQqCR2LJwJPdlTK

//the above string includes all of the above client codes.  The applications will do a check on the license key against the allowed suppliers in this code, which by default will be included and created by the applications.  As the approved vendor list changes, new versions will create a new default code, but for interim installations, the code can be updated in the CSF file.

// AND the way the string is generated is as follows:
// take this string:
//							VDSHAS|ChySnT|SFTEns
// which is delimited pairs of 3 letter codes (do in pairs so we dont have delimited 4 char, or naked 3 char regular)
//							base 64 endcode using the license key alpha and padch
//#define LICENSE_B64ALPHA		"XhIWsP4RtOoSTQpuvJ-LBCnGrAyd8eVi7%Dz1wkl5xMj6NqZa2YHbc9f0+Eg3UFm"
//#define LICENSE_B64PADCH		'K'
// and there you go.

							if((m_pszOEMcodes)&&(strlen(m_pszOEMcodes)>3))
							{
								CBufferUtil bu;
								char* pszCodes = m_pszOEMcodes;
								unsigned long ulBufLen = strlen(m_pszOEMcodes);
								bu.Base64Decode(&pszCodes, &ulBufLen, false, LICENSE_B64ALPHA, LICENSE_B64PADCH);

								CSafeBufferUtil sbu;
								char* pchCodes = sbu.Token(pszCodes, strlen(pszCodes), "|");
								g_ppromolite->m_data.m_key.m_bValid = false;
								while(pchCodes)
								{
									if(strncmp(g_ppromolite->m_data.m_key.m_ppszValues[i], pchCodes, 3)==0)
									{
										g_ppromolite->m_data.m_key.m_bValid = true;
										break;
									}
									else
									if((strlen(pchCodes)>3)&&(strncmp(g_ppromolite->m_data.m_key.m_ppszValues[i], pchCodes+3, 3)==0))
									{
										g_ppromolite->m_data.m_key.m_bValid = true;
										break;
									}
									pchCodes = sbu.Token(NULL, NULL, "|");
								}
								if(pszCodes)
								{
									try{free(pszCodes);} catch(...){}
								}
							}
							else g_ppromolite->m_data.m_key.m_bValid = false;
						}
					}
					i++;
				}

				if(
						(g_ppromolite->m_data.m_key.m_bValid)
					&&(
							(!g_ppromolite->m_data.m_key.m_bExpires)
						||((g_ppromolite->m_data.m_key.m_bExpires)&&(!g_ppromolite->m_data.m_key.m_bExpired))
						||((g_ppromolite->m_data.m_key.m_bExpires)&&(g_ppromolite->m_data.m_key.m_bExpireForgiveness)&&(g_ppromolite->m_data.m_key.m_ulExpiryDate+g_ppromolite->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!g_ppromolite->m_data.m_key.m_bMachineSpecific)
						||((g_ppromolite->m_data.m_key.m_bMachineSpecific)&&(g_ppromolite->m_data.m_key.m_bValidMAC))
						)
					)
				{
//					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//					g_ppromolite->m_data.SetStatusText(errorstring, PROMOLITE_STATUS_OK);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
					g_ppromolite->m_data.SetStatusText(errorstring, PROMOLITE_STATUS_ERROR);
				}
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				g_ppromolite->m_data.SetStatusText(errorstring, PROMOLITE_STATUS_ERROR);
			}


			m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", PROMOLITE_PORT_CMD);
			m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", PROMOLITE_PORT_STATUS);

			m_pszIconPath = file.GetIniString("FileServer", "IconPath", "C:\\Inetpub\\wwwroot\\Cortex\\promolite\\images\\", m_pszIconPath);
			m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
			m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;
			m_bUseXMLClientLog = file.GetIniInt("Messager", "UseXMLClientLog", 0)?true:false;
			m_bMillisecondMessaging = file.GetIniInt("Messager", "MillisecondMessaging", 1)?true:false;			// use millisecond resolution for messages and asrun


			m_pszDSN = file.GetIniString("Database", "DSN", (m_pszName?m_pszName:"PromoLite"), m_pszDSN);
			m_pszUser = file.GetIniString("Database", "DBUser", "sa", m_pszUser);
			m_pszPW = file.GetIniString("Database", "DBPassword", "", m_pszPW);
			m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings", m_pszSettings);  // the Settings table name
			m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange", m_pszExchange);  // the Exchange table name
			m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages", m_pszMessages);  // the Messages table name
			m_pszAsRun = file.GetIniString("Database", "AsRunTableName", "AsRun_Log", m_pszAsRun);  // the AsRun table name

			
			m_nAutoPurgeMessageDays = file.GetIniInt("AutoPurge", "AutoPurgeMessageDays", 30);
			m_nAutoPurgeAsRunDays = file.GetIniInt("AutoPurge", "AutoPurgeAsRunDays", 30);
			m_nAutoPurgeInterval  = file.GetIniInt("AutoPurge", "AutoPurgeInterval", 300);  // 5 minutes should be enough.

			m_pszEventRules = file.GetIniString("Database", "EventRulesViewName", "EventRuleView", m_pszEventRules);  // the EventRuleView name

			m_pszDefaultDB = file.GetIniString("Database", "DBDefault", "PromoLite", m_pszDefaultDB);  // the default db name
			m_pszCortexDB = file.GetIniString("Database", "CortexDB", "Cortex", m_pszCortexDB);  // the cortex db name
			m_pszLiveEventData = file.GetIniString("Database", "LiveEventData", "LiveEventData", m_pszLiveEventData);  // the LiveEventData name
			m_pszParameterRuleView = file.GetIniString("Database", "ParameterRuleView", "ParameterRuleView", m_pszParameterRuleView);  // the  ParameterRuleView name
			m_pszMetaConfigUnionALLView = file.GetIniString("Database", "MetaConfigUnionALLView", "MetaConfigUnionALLView", m_pszMetaConfigUnionALLView);  // the  MetaConfigUnionALLView name
			m_pszChannelInfo = file.GetIniString("Database", "ChannelInfoView", "ChannelInfo", m_pszChannelInfo);  // the  m_pszChannelInfo name
			m_pszTriggerInfoView = file.GetIniString("Database", "TriggerInfoView", "TriggerInfo", m_pszTriggerInfoView);  // the  m_pszChannelInfo name
			m_bUseUTF8 = file.GetIniInt("Database", "UseUTF8", 0)?true:false;

			m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue", m_pszQueue);  // the Queue table name
			m_pszMappings = file.GetIniString("Database", "RulesTableName", "Mapping", m_pszMappings);  // the Mapping table name
#ifdef NUCLEUS_PROMOLITE_CHANGE
			m_pszEvents = file.GetIniString("Database", "EventsTableName", "Schedule", m_pszEvents);  // the LiveEvents table name
			m_pszTriggerAnalysisView = file.GetIniString("Database", "AnalysisViewName", "Triggers", m_pszTriggerAnalysisView);  // the TriggerAnalysisView name
			m_pszAnalyzedTriggerData = file.GetIniString("Database", "AnalyzedTriggerDataTableName", "Schedule_Actions", m_pszAnalyzedTriggerData);  // the AnalyzedTriggerData table name
#else // #ifndef NUCLEUS_PROMOLITE_CHANGE
			m_pszEvents = file.GetIniString("Database", "EventsTableName", "Events", m_pszEvents);  // the LiveEvents table name
			m_pszTriggerAnalysisView = file.GetIniString("Database", "AnalysisViewName", "TriggerAnalysis", m_pszTriggerAnalysisView);  // the TriggerAnalysisView name
			m_pszAnalyzedTriggerData = file.GetIniString("Database", "AnalyzedTriggerDataTableName", "AnalyzedTriggerData", m_pszAnalyzedTriggerData);  // the AnalyzedTriggerData table name
#endif // #ifndef NUCLEUS_PROMOLITE_CHANGE
			m_pszAnalyzeRulesProc = file.GetIniString("Database", "AnalyzeRulesProcedureName", "spAnalyzeRules", m_pszAnalyzeRulesProc);  // the spAnalyzeRules stored procedure
			m_pszGetParameterValueProc = file.GetIniString("Database", "GetParameterValueProcedureName", "spGetParameterValue", m_pszGetParameterValueProc);  // the spGetParameterValue stored procedure
			m_pszAnalysis = file.GetIniString("Database", "Analysis", "Analysis", m_pszAnalysis);  // the Analysis table name
			
			m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds

			m_pszSystemFolderPath = file.GetIniString("FileHandling", "SystemFolder", "C:\\", m_pszSystemFolderPath);  // must have trailing slash

			// gfx module
//			m_pszGfxHost = file.GetIniString("Graphics", "Host", "127.0.0.1", m_pszGfxHost);  // must have trailing slash; 
//			m_nGfxPort = file.GetIniInt("Graphics", "Port", 10682); 

//			m_pszHarrisAutomationServer = file.GetIniString("Demo", "HarrisAutomationServer", "MAIN", m_pszHarrisAutomationServer);
//			m_nHarrisAutomationList = file.GetIniInt("Demo", "HarrisAutomationList", 1);
			m_nHarrisStatusTriggerExclude = file.GetIniInt("Automation", "HarrisStatusTriggerExclude", 1048593);  // post roll, playing, and post-upcounting events.

			m_pszCloseAll=file.GetIniString("Graphics", "CloseAllToken", "CloseAll", m_pszCloseAll); // the close all token
			m_pszStopAll=file.GetIniString("Graphics", "StopAllToken", "StopAll", m_pszStopAll); // the close all token
		
			
				//NUCLEUS->PROMOLITE_CHANGE
			m_pszDestinationHost=file.GetIniString("Graphics", "DestinationHost", "127.0.0.1", m_pszDestinationHost); // the single promolite graphics destination IP or hostname
			m_pszDestinationModule=file.GetIniString("Graphics", "DestinationModule", "Libretto", m_pszDestinationModule); // the single promolite graphics destination IP or hostname
			m_nDestinationType = file.GetIniInt("Graphics", "DestinationType", PROMOLITE_RULE_DESTTYPE_UNKNOWN);
			m_nEventBufferIdleRefreshMS = file.GetIniInt("Database", "EventBufferIdleRefreshMS", 2000) ;  // the amount of time between triggers, where if we have time, we'll refresh da buffa
			m_nDoneCount = file.GetIniInt("Timeline", "DoneCount", 3); // number of done events to keep.
			if(m_nDoneCount<0) m_nDoneCount=0;
			m_nRecycleSeconds = file.GetIniInt("Timeline", "RecycleSeconds", 0);  // number of seconds to keep recycling done events.



			m_nMinLyricAlias = file.GetIniInt("Lyric", "MinLyricAlias", 9900);
			m_nMaxLyricAlias = file.GetIniInt("Lyric", "MaxLyricAlias", 9999);
			m_bReleaseIsV_5_15 = file.GetIniInt("Lyric", "ReleaseIsV_5_15", 1)?true:false;

			
			m_nLayoutSalvoFormat = file.GetIniInt("Icon", "LayoutSalvoFormat", PROMOLITE_HARRIS_ICONII_LSFORMAT_NUM4);

			m_nTriggerBuffer  = file.GetIniInt("FileHandling", "TriggerBufferItems", 12);
			if(m_nTriggerBuffer>127) m_nTriggerBuffer=127;
			if(m_nTriggerBuffer<1) m_nTriggerBuffer=12; //default

			m_nTriggerExclusionDelayMS  = file.GetIniInt("FileHandling", "TriggerExclusionDelayMS", 10000);
			if(m_nTriggerExclusionDelayMS<0) m_nTriggerExclusionDelayMS=0;

			m_nInterTriggerDelayMS  = file.GetIniInt("FileHandling", "InterTriggerDelayMS", 50); // number of milliseconds to delay between SQL queries for triggers within an event
			if(m_nInterTriggerDelayMS<0) m_nInterTriggerDelayMS=0;
			m_nInterEventDelayMS = file.GetIniInt("FileHandling", "InterEventDelayMS", 3000); // number of milliseconds to delay between processing each event.
			if(m_nInterEventDelayMS<0) m_nInterEventDelayMS=0;


			m_nTriggerAdvanceMS = file.GetIniInt("Automation", "TriggerAdvanceMS", 0);// number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)
			m_nAnalyzeRulesDwellMS = file.GetIniInt("Automation", "AnalyzeRulesDwellMS", 2000);
			m_nAnalyzeParameterDwellMS = file.GetIniInt("Automation", "AnalyzeParameterDwellMS", 2500);
			m_nAnalyzeTimingDwellMS = file.GetIniInt("Automation", "AnalyzeTimingDwellMS", 1000);
			m_nAnalyzeHarrisStatusExclude = file.GetIniInt("Automation", "AnalyzeHarrisStatusExclude", 19);  // (not play, post, done)
			m_nAnalyzeAutomationDwellMS = file.GetIniInt("Automation", "AnalyzeAutomationDwellMS", 250); // number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once.
			m_nAnalyzeAutomationForceMS = file.GetIniInt("Automation", "AnalyzeAutomationForceMS", 2000); // number of milliseconds after a change, that if there are changes pending, we MUST be force analysis 
			m_nAutomationIntervalMS = file.GetIniInt("Automation", "AutomationIntervalMS", 3000); // number of milliseconds between channel time checks
			m_nAnalyzeRulesIntervalMS = file.GetIniInt("Automation", "AnalyzeRulesIntervalMS", 1000); // number of milliseconds between automation checks that cause rules analysis
			m_nAnalyzeRulesForceMS = file.GetIniInt("Automation", "AnalyzeRulesForceMS", 5000); // number of milliseconds after a change, that if there are changes pending, we MUST be force rules analysis 
			m_nAnalyzeParameterForceMS = file.GetIniInt("Automation", "AnalyzeParameterForceMS", 5000); // number of milliseconds to wait before forcing this to run.
			m_nTriggerNotificationDwellMS = file.GetIniInt("Automation", "TriggerNotificationDwellMS", 1000); // number of milliseconds trigger notification is delayed after a change 
			m_bEnableAnalysisTriggerNotification = file.GetIniInt("Automation", "EnableAnalysisTriggerNotification", 1)?true:false;
			m_bEnableEventTriggerNotification = file.GetIniInt("Automation", "EnableEventTriggerNotification", 1)?true:false;
			m_bChannelInfoViewHasDesc = file.GetIniInt("Automation", "ChannelInfoViewHasDesc", 1)?true:false;

			m_bUseLocalClock = file.GetIniInt("Automation", "UseLocalClock", 0)?true:false;
			m_bUseUTC = file.GetIniInt("Automation", "UseUTC", 0)?true:false;
			m_bFarAnalysisUsesCalc =  file.GetIniInt("Automation", "FarAnalysisUsesCalc", 1)?true:false;
			m_bNearAnalysisUsesCalc = file.GetIniInt("Automation", "NearAnalysisUsesCalc", 1)?true:false;


/*
			char* pDP = file.GetIniString("Demo", "DefaultProject", "Promo_B");
			char* pDS = file.GetIniString("Demo", "DefaultScene", "Promo_B");
			char* pDH = file.GetIniString("Demo", "DefaultHost", "10.0.0.22");

			if(  (strcmp(pDP, m_pszDefaultProject))
				 ||(strcmp(pDS, m_pszDefaultScene))
				 ||(strcmp(pDH, m_pszDefaultHost))
				)
			{
				free(m_pszDefaultProject);
				free(m_pszDefaultScene);
				free(m_pszDefaultHost);
				m_pszDefaultProject = pDP;
				m_pszDefaultScene = pDS;
				m_pszDefaultHost = pDH;

				char proj[256]; sprintf(proj, "%s%c%s%c%s", 
					m_pszDefaultHost, 28,
					m_pszDefaultProject, 28,
					m_pszDefaultScene);

				g_ppromolite->m_data.SendGraphicsCommand(RADIANCE_CMD_LOADSCENE, proj); // dont check errors

/*
				if((m_pdb)&&(m_pdbConn))
				{
					char proj[256]; sprintf("0|%s|%s", m_pszDefaultProject, m_pszDefaultScene);
					char* pchEncodedLocal = m_pdb->EncodeQuotes(proj);
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(local, remote, action, host, timestamp, username) \
VALUES ('%s','', %d, '%s', %d.%d, 'sys')", //HARDCODE
																		"Radiance",
																		"Command_Queue",
																		pchEncodedLocal?pchEncodedLocal:"0|Promo_B|Promo_B",
																		256,
																		m_pszDefaultHost,
																		g_ppromolite->m_data.m_timebTick.time,
																		g_ppromolite->m_data.m_timebTick.millitm
																		);
					if(pchEncodedLocal) free(pchEncodedLocal);

				//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ScheduleEndpointQueue SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
				//		char errorstring[DB_ERRORSTRING_LEN]; if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
					if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
					{
					//**MSG
				//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
					}
				}
* /
			}
			else
			{
				free(pDP);
				free(pDS);
				free(pDH);
			}
*/

			m_pszFileSpec = file.GetIniString("Messager", "LogFileIni", "Logs\\PromoLite|YD||1|", m_pszFileSpec);
				// SMTP address[: override port (optional)] | distribution lists | from email |  subject spec | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
			m_pszMailSpec = file.GetIniString("Messager", "SMTPIni", "mail.server.com|f3:recipient@address.com|\"someone\"<from@here.com>|%P %T from %h|%h|C:\\promolitesmtp.txt|1|1|0", m_pszMailSpec);
			if(m_pszFileSpec) 
			{
				if(m_pszProcessedFileSpec)
				{
					try{ free(m_pszProcessedFileSpec); } catch(...) {}
				}
				m_pszProcessedFileSpec = ProcessString(m_pszFileSpec, false);
			}
			
			if(m_pszMailSpec) 
			{
				if(m_pszProcessedMailSpec)
				{
					try{ free(m_pszProcessedMailSpec); } catch(...) {}
				}
				m_pszProcessedMailSpec = ProcessString(m_pszMailSpec, false);
			}


		}
		else //write
		{

			file.SetIniString("Main", "Name", m_pszName);
			file.SetIniString("Main", "Type", m_pszType);
			file.SetIniString("Main", "Project", m_pszProject);
			file.SetIniInt("CommandServer", "ListenPort", m_usCommandPort);
			file.SetIniInt("StatusServer", "ListenPort", m_usStatusPort);
			file.SetIniString("License", "Key", m_pszLicense);
			file.SetIniInt("Main", "ThreadDwellMS", m_nThreadDwellMS);

			file.SetIniInt("Database", "DebugLists", m_bDebugLists?1:0);  // if true, dumps the temp and events databases
			file.SetIniInt("Database", "DebugListTopCount", m_nDebugListTopCount);  //if positive, just prints out the top n recored in the events tables.
			file.SetIniInt("Database", "DebugSQL", m_bDebugSQL?1:0);  // if true, prints out the SQL calls
			file.SetIniInt("Database", "DebugInsertSQL", m_bDebugInsertSQL?1:0);  // if true, prints out the event insert SQL calls
			file.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.
			file.SetIniString("Database", "DebugOrder", m_pszDebugOrder); // the order by clause for debug
			file.SetIniInt("Database", "UseUTF8", m_bUseUTF8?1:0);

			file.SetIniString("FileServer", "IconPath", m_pszIconPath);

			file.SetIniInt("Messager", "UseEmail", m_bUseEmail?1:0);
			file.SetIniInt("Messager", "UseNet", m_bUseNetwork?1:0);
			file.SetIniInt("Messager", "UseLog", m_bUseLog?1:0);
			file.SetIniInt("Messager", "ReportSuccessfulOperation", m_bReportSuccessfulOperation?1:0);
			file.SetIniInt("Messager", "LogTransfers", m_bLogTransfers?1:0);
			file.SetIniInt("Messager", "UseXMLClientLog", m_bUseXMLClientLog?true:false);
			file.SetIniInt("Messager", "MillisecondMessaging", m_bMillisecondMessaging?true:false);		// use millisecond resolution for messages and asrun

			file.SetIniInt("AutoPurge", "AutoPurgeMessageDays", m_nAutoPurgeMessageDays);
			file.SetIniInt("AutoPurge", "AutoPurgeAsRunDays", m_nAutoPurgeAsRunDays);
			file.SetIniInt("AutoPurge", "AutoPurgeInterval", m_nAutoPurgeInterval);  // 5 minutes should be enough.


			file.SetIniString("Database", "DSN", m_pszDSN);
			file.SetIniString("Database", "DBUser", m_pszUser);
			file.SetIniString("Database", "DBPassword", m_pszPW);
			file.SetIniString("Database", "SettingsTableName", m_pszSettings);  // the Settings table name
			file.SetIniString("Database", "ExchangeTableName", m_pszExchange);  // the Exchange table name
			file.SetIniString("Database", "MessagesTableName", m_pszMessages);  // the Messages table name
			file.SetIniString("Database", "AsRunTableName", m_pszAsRun);  // the AsRun table name
			file.SetIniString("Database", "EventRulesViewName", m_pszEventRules);  // the EventRuleView name

			file.SetIniString("Database", "QueueTableName", m_pszQueue);  // the Queue table name
			file.SetIniString("Database", "RulesTableName", m_pszMappings);  // the Rules table name
			file.SetIniString("Database", "EventsTableName", m_pszEvents);  // the Events table name
			file.SetIniString("Database", "AnalysisViewName", m_pszTriggerAnalysisView);  // the TriggerAnalysisView name
			file.SetIniString("Database", "AnalyzedTriggerDataTableName", m_pszAnalyzedTriggerData);  // the AnalyzedTriggerData table name
			file.SetIniString("Database", "AnalyzeRulesProcedureName", m_pszAnalyzeRulesProc);  // the spAnalyzeRules stored procedure

			file.SetIniString("Database", "ParameterRuleView", m_pszParameterRuleView);  // the  ParameterRuleView name
			file.SetIniString("Database", "MetaConfigUnionALLView", m_pszMetaConfigUnionALLView);  // the  MetaConfigUnionALLView name
			file.SetIniString("Database", "ChannelInfoView", m_pszChannelInfo);  // the  m_pszChannelInfo name
			file.SetIniString("Database", "TriggerInfoView", m_pszTriggerInfoView);  // the  m_pszTriggerInfoView name
			
			file.SetIniString("Database", "GetParameterValueProcedureName", m_pszGetParameterValueProc);  // the spGetParameterValue stored procedure
			file.SetIniString("Database", "Analysis", m_pszAnalysis);  // the Analysis table name

			file.SetIniString("Database", "DBDefault", m_pszDefaultDB);  // the default db name
			file.SetIniString("Database", "CortexDB", m_pszCortexDB);  // the cortex db name
			file.SetIniString("Database", "LiveEventData", m_pszLiveEventData);  // the LiveEventData name


			file.SetIniInt("Automation", "HarrisStatusTriggerExclude", m_nHarrisStatusTriggerExclude);  // post roll, playing, and post-upcounting events.

/*
			file.SetIniString("Demo", "DefaultProject", m_pszDefaultProject);
			file.SetIniString("Demo", "DefaultScene", m_pszDefaultScene);
			file.SetIniString("Demo", "DefaultHost", m_pszDefaultHost);
*/
//			file.SetIniString("Demo", "HarrisAutomationServer", m_pszHarrisAutomationServer);
//			file.SetIniInt("Demo", "HarrisAutomationList", m_nHarrisAutomationList);


			file.SetIniInt("Database", "ModificationCheckInterval", m_ulModsIntervalMS);  // in milliseconds
//			file.SetIniInt("FileHandling", "TriggerAdvanceMS", m_nTriggerAdvanceMS);// number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)

			file.SetIniInt("Lyric", "MinLyricAlias", m_nMinLyricAlias);
			file.SetIniInt("Lyric", "MaxLyricAlias", m_nMaxLyricAlias);
			file.SetIniInt("Lyric", "ReleaseIsV_5_15", m_bReleaseIsV_5_15?1:0);

			file.SetIniInt("Icon", "LayoutSalvoFormat", m_nLayoutSalvoFormat);

			file.SetIniString("FileHandling", "SystemFolder", m_pszSystemFolderPath);  // must have trailing slash
//			file.SetIniInt("FileHandling", "FarAnalysisUsesCalc", m_bFarAnalysisUsesCalc?1:0);
//			file.SetIniInt("FileHandling", "NearAnalysisUsesCalc", m_bNearAnalysisUsesCalc?1:0);
			file.SetIniInt("FileHandling", "TriggerBufferItems", m_nTriggerBuffer);
			file.SetIniInt("FileHandling", "TriggerExclusionDelayMS", m_nTriggerExclusionDelayMS );
			file.SetIniInt("FileHandling", "InterTriggerDelayMS", m_nInterTriggerDelayMS); // number of milliseconds to delay between SQL queries for triggers within an event
			file.SetIniInt("FileHandling", "InterEventDelayMS", m_nInterEventDelayMS); // number of milliseconds to delay between processing each event.

			file.SetIniInt("Automation", "AnalyzeRulesDwellMS",  m_nAnalyzeRulesDwellMS);
			file.SetIniInt("Automation", "AnalyzeParameterDwellMS",  m_nAnalyzeParameterDwellMS);
			file.SetIniInt("Automation", "AnalyzeTimingDwellMS",  m_nAnalyzeTimingDwellMS);

			file.SetIniInt("Automation", "TriggerAdvanceMS", m_nTriggerAdvanceMS);// number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)
			file.SetIniInt("Automation", "UseLocalClock", m_bUseLocalClock?1:0);
			file.SetIniInt("Automation", "UseUTC", m_bUseUTC?1:0);  
			file.SetIniInt("Automation", "FarAnalysisUsesCalc", m_bFarAnalysisUsesCalc?1:0);
			file.SetIniInt("Automation", "NearAnalysisUsesCalc", m_bNearAnalysisUsesCalc?1:0);
			file.SetIniInt("Automation", "AnalyzeHarrisStatusExclude", m_nAnalyzeHarrisStatusExclude);  // (not play, post, done)

			file.SetIniInt("Automation", "AnalyzeAutomationDwellMS", m_nAnalyzeAutomationDwellMS); // number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once.
			file.SetIniInt("Automation", "AnalyzeAutomationForceMS", m_nAnalyzeAutomationForceMS); // number of milliseconds after a change, that if there are changes pending, we MUST be force analysis 
			file.SetIniInt("Automation", "AutomationIntervalMS", m_nAutomationIntervalMS); // number of milliseconds between channel time checks
			file.SetIniInt("Automation", "AnalyzeRulesIntervalMS", m_nAnalyzeRulesIntervalMS); // number of milliseconds between automation checks that cause rules analysis
			file.SetIniInt("Automation", "AnalyzeRulesForceMS", m_nAnalyzeRulesForceMS); // number of milliseconds after a change, that if there are changes pending, we MUST be force rules analysis 
			file.SetIniInt("Automation", "AnalyzeParameterForceMS", m_nAnalyzeParameterForceMS); // number of milliseconds to wait before forcing this to run.
			file.SetIniInt("Automation", "TriggerNotificationDwellMS", m_nTriggerNotificationDwellMS); // number of milliseconds trigger notification is delayed after a change 
			file.SetIniInt("Automation", "EnableAnalysisTriggerNotification", m_bEnableAnalysisTriggerNotification?1:0);
			file.SetIniInt("Automation", "EnableEventTriggerNotification", m_bEnableEventTriggerNotification?1:0);
			file.SetIniInt("Automation", "ChannelInfoViewHasDesc", m_bChannelInfoViewHasDesc?1:0);

			// gfx module
//			file.SetIniString("Graphics", "Host", m_pszGfxHost);  // must have trailing slash; 
//			file.SetIniInt("Graphics", "Port", m_nGfxPort); 
			file.SetIniString("Graphics", "CloseAllToken", m_pszCloseAll); // the close all token
			file.SetIniString("Graphics", "StopAllToken", m_pszStopAll); // the close all token

				//NUCLEUS->PROMOLITE_CHANGE
			file.SetIniString("Graphics", "DestinationHost", m_pszDestinationHost); // the single promolite graphics destination IP or hostname
			file.SetIniString("Graphics", "DestinationModule", m_pszDestinationModule); // the single promolite graphics destination IP or hostname
			file.SetIniInt("Graphics", "DestinationType", m_nDestinationType );
			file.SetIniInt("Database", "EventBufferIdleRefreshMS", m_nEventBufferIdleRefreshMS);  // the amount of time between triggers, where if we have time, we'll refresh da buffa
			file.SetIniInt("Timeline", "DoneCount", m_nDoneCount); // number of done events to keep.
			file.SetIniInt("Timeline", "RecycleSeconds", m_nRecycleSeconds);  // number of seconds to keep recycling done events.


			file.SetIniString("Messager", "LogFileIni", m_pszFileSpec);
			file.SetIniString("Messager", "SMTPIni", m_pszMailSpec);

			file.SetSettings(pszFilename, false);  // have to have correct filename

		}
		return PROMOLITE_SUCCESS;
	}
	return PROMOLITE_ERROR;
}


int CPromoLiteSettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	if((m_pszSettings)&&(strlen(m_pszSettings)>0)&&(m_pdbConn)&&(m_pdb)&&(m_pdbConn->m_bConnected)&&(Settings(true)==PROMOLITE_SUCCESS))  //read has to succeed
	{
		// get settings.
/*
		char pszFilename[MAX_PATH];

		strcpy(pszFilename, PROMOLITE_SETTINGS_FILE_DEFAULT);  // promolite settings file

		CFileUtil file;
		file.GetSettings(pszFilename, false); 

		// load up the values on the settings object
		if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
		{
			g_ppromolite->m_settings.m_pszName = file.GetIniString("Main", "Name", "PromoLite");
			g_ppromolite->m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");

			g_ppromolite->m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");

			g_ppromolite->m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", PROMOLITE_PORT_CMD);
			g_ppromolite->m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", PROMOLITE_PORT_STATUS);

			g_ppromolite->m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			g_ppromolite->m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			g_ppromolite->m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			g_ppromolite->m_settings.m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
			g_ppromolite->m_settings.m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;

	//		g_ppromolite->m_settings.m_bUseListCount = file.GetIniInt("HarrisAPI", "UseListCount", 0)?true:false; // get all events up until the list count (otherwise just up to the lookahead)

			g_ppromolite->m_settings.m_pszDSN = file.GetIniString("Database", "DSN", g_ppromolite->m_settings.m_pszName?g_ppromolite->m_settings.m_pszName:"PromoLite");
			g_ppromolite->m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
			g_ppromolite->m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
			g_ppromolite->m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
			g_ppromolite->m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
			g_ppromolite->m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name

	//		g_ppromolite->m_settings.m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels");  // the Channels table name
	//		g_ppromolite->m_settings.m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Connections");  // the Connections table name
			g_ppromolite->m_settings.m_pszEvents = file.GetIniString("Database", "EventsTableName", "Events");  // the LiveEvents table name
			g_ppromolite->m_settings.m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue");  // the Queue table name
			g_ppromolite->m_settings.m_pszMappings = file.GetIniString("Database", "RulesTableName", "Rules");  // the Rules table name
			g_ppromolite->m_settings.m_pszTriggerAnalysisView = file.GetIniString("Database", "AnalysisViewName", "TriggerAnalysisView");  // the TriggerAnalysisView name
			g_ppromolite->m_settings.m_pszAnalyzedTriggerData = file.GetIniString("Database", "AnalyzedTriggerDataTableName", "AnalyzedTriggerData");  // the AnalyzedTriggerData table name
			g_ppromolite->m_settings.m_pszAnalyzeRulesProc = file.GetIniString("Database", "AnalyzeRulesProcedureName", "spAnalyzeRules");  // the spAnalyzeRules stored procedure
			g_ppromolite->m_settings.m_pszGetParameterValueProc = file.GetIniString("Database", "GetParameterValueProcedureName", "spGetParameterValue");  // the spGetParameterValue stored procedure

			g_ppromolite->m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
//			g_ppromolite->m_settings.m_nTransferFailureRetryTime = file.GetIniInt("FileHandling", "TransferFailureRetryTime", 60);  // in seconds
//			g_ppromolite->m_settings.m_bAutoDeleteOldest = file.GetIniInt("FileHandling", "AutoDeleteOldest", 1)?true:false;
//			g_ppromolite->m_settings.m_ulDeletionThreshold = file.GetIniInt("FileHandling", "DeletionThreshold", 172800); // two days

			g_ppromolite->m_settings.m_pszSystemFolderPath = file.GetIniString("FileHandling", "SystemFolder", "C:\\");  // must have trailing slash
			g_ppromolite->m_settings.m_nTriggerAdvanceMS = file.GetIniInt("FileHandling", "TriggerAdvanceMS", 0);// number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)

			// gfx module
			g_ppromolite->m_settings.m_pszGfxHost = file.GetIniString("Graphics", "Host", "127.0.0.1");  // must have trailing slash; 
			g_ppromolite->m_settings.m_nGfxPort = file.GetIniInt("Graphics", "Port", 10682); 


			char* pDP = file.GetIniString("Demo", "DefaultProject", "Promo_B");
			char* pDS = file.GetIniString("Demo", "DefaultScene", "Promo_B");
			char* pDH = file.GetIniString("Demo", "DefaultHost", "10.0.0.22");

			if(  (strcmp(pDP, g_ppromolite->m_settings.m_pszDefaultProject))
				 ||(strcmp(pDS, g_ppromolite->m_settings.m_pszDefaultScene))
				 ||(strcmp(pDH, g_ppromolite->m_settings.m_pszDefaultHost))
				)
			{
				free(g_ppromolite->m_settings.m_pszDefaultProject);
				free(g_ppromolite->m_settings.m_pszDefaultScene);
				free(g_ppromolite->m_settings.m_pszDefaultHost);
				g_ppromolite->m_settings.m_pszDefaultProject = pDP;
				g_ppromolite->m_settings.m_pszDefaultScene = pDS;
				g_ppromolite->m_settings.m_pszDefaultHost = pDH;

				char proj[256]; sprintf(proj, "%s%c%s%c%s", 
					g_ppromolite->m_settings.m_pszDefaultHost, 28,
					g_ppromolite->m_settings.m_pszDefaultProject, 28,
					g_ppromolite->m_settings.m_pszDefaultScene);

				g_ppromolite->m_data.SendGraphicsCommand(RADIANCE_CMD_LOADSCENE, proj); // dont check errors

/*
				if((m_pdb)&&(m_pdbConn))
				{
					char proj[256]; sprintf("0|%s|%s", g_ppromolite->m_settings.m_pszDefaultProject, g_ppromolite->m_settings.m_pszDefaultScene);
					char* pchEncodedLocal = m_pdb->EncodeQuotes(proj);
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(local, remote, action, host, timestamp, username) \
VALUES ('%s','', %d, '%s', %d.%d, 'sys')", //HARDCODE
																		"Radiance",
																		"Command_Queue",
																		pchEncodedLocal?pchEncodedLocal:"0|Promo_B|Promo_B",
																		256,
																		g_ppromolite->m_settings.m_pszDefaultHost,
																		g_ppromolite->m_data.m_timebTick.time,
																		g_ppromolite->m_data.m_timebTick.millitm
																		);
					if(pchEncodedLocal) free(pchEncodedLocal);

				//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ScheduleEndpointQueue SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
				//		char errorstring[DB_ERRORSTRING_LEN]; if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
					if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
					{
					//**MSG
				//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
					}
				}
* /
			}
			else
			{
				free(pDP);
				free(pDS);
				free(pDH);
			}

//			g_ppromolite->m_settings.m_pszImagestoreNullFile = file.GetIniString("Imagestore", "NullOXTFile", "null.oxt");  // name of the null file, must be in system folder
//			g_ppromolite->m_settings.m_pszIntuitionSearchExt = file.GetIniString("Imagestore", "IntuitionSearchExt", "tem"); // search extensions for Intuition
//			g_ppromolite->m_settings.m_pszImagestore2SearchExt = file.GetIniString("Imagestore", "Imagestore2SearchExt", "oxa,oxt,oxw");			// search extensions for IS2
//			g_ppromolite->m_settings.m_pszImagestore300SearchExt = file.GetIniString("Imagestore", "Imagestore300SearchExt", "oxa,oxt,oxe");		// search extensions for IS300
//			g_ppromolite->m_settings.m_pszImagestoreHDSearchExt = file.GetIniString("Imagestore", "ImagestoreHDSearchExt", "oxa,oxt,oxe");		// search extensions for ISHD

		}
		
	*/	
		
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
		EnterCriticalSection(&g_ppromolite->m_data.m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = PROMOLITE_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCategory="";
				CString szParameter="";
				CString szValue="";
				CString szTemp="";
				int min, max;
				bool bmin = false, bmax = false;
				try
				{
					prs->GetFieldValue("category", szCategory);  //HARDCODE
					prs->GetFieldValue("parameter", szParameter);  //HARDCODE
					prs->GetFieldValue("value", szValue);  //HARDCODE
					prs->GetFieldValue("min_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						min = atoi(szTemp);
						bmin = true;
					}
					prs->GetFieldValue("max_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						max = atoi(szTemp);
						bmax = true;
					}
				}
				catch( ... )
				{
				}

				int nLength = szValue.GetLength();
				if(szCategory.CompareNoCase("Main")==0)
				{
					if(szParameter.CompareNoCase("Name")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszName)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszName) free(m_pszName);
								m_pszName = pch;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("License")==0)
				{
					if(szParameter.CompareNoCase("Key")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszLicense)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLicense) free(m_pszLicense);
								m_pszLicense = pch;

								// recompile license key params
								if(g_ppromolite->m_data.m_key.m_pszLicenseString) free(g_ppromolite->m_data.m_key.m_pszLicenseString);
								g_ppromolite->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
								if(g_ppromolite->m_data.m_key.m_pszLicenseString)
								sprintf(g_ppromolite->m_data.m_key.m_pszLicenseString, "%s", szValue);

								g_ppromolite->m_data.m_key.InterpretKey();

								char errorstring[MAX_MESSAGE_LENGTH];
								if(g_ppromolite->m_data.m_key.m_bValid)
								{
									unsigned long i=0;
									while(i<g_ppromolite->m_data.m_key.m_ulNumParams)
									{
										if((g_ppromolite->m_data.m_key.m_ppszParams)
											&&(g_ppromolite->m_data.m_key.m_ppszValues)
											&&(g_ppromolite->m_data.m_key.m_ppszParams[i])
											&&(g_ppromolite->m_data.m_key.m_ppszValues[i]))
										{
											if(stricmp(g_ppromolite->m_data.m_key.m_ppszParams[i], "max")==0)
											{
//												g_ppromolite->m_data.m_nMaxLicensedDevices = atoi(g_ppromolite->m_data.m_key.m_ppszValues[i]);
											}
											else
											if(stricmp(g_ppromolite->m_data.m_key.m_ppszParams[i], "oem")==0)
											{
												// if it exists, check OEM string.

											// for OEM partner check on license key, need oem=xxxr in the params'
					//oem=xxxr where xxx is a client code string, and r is any random ASCII character in the range 33 to 126, excluding URL-sensitive characters (34, 38, 39, 46, 47, 60, 61, 62, 63).
					//the oem parameter should be the last thing in the string, and the last random character allows us to keep the encoding more secure.
					//the client codes for OEM suppliers is as follows (note these ARE case sensitive):

					//Strategy and Technology (S&T): SnT
					//Harris: HAS
					//Softel: SFT
					//Chyron: Chy
					//Ensequence: Ens
					//VDS: VDS (for test purposes)

					//If an OEM code is specified in the license key, a supplier code table including these values must exist in the CSF file, for it to match on. 
					//This will be stored in a CSF parameter under the [License] heading, called COM for Cortex OEM Management (haha).

					//[License]
					//COM=C1JL-sPLisQ5VCQqCR2LJwJPdlTK

					//the above string includes all of the above client codes.  The applications will do a check on the license key against the allowed suppliers in this code, which by default will be included and created by the applications.  As the approved vendor list changes, new versions will create a new default code, but for interim installations, the code can be updated in the CSF file.

					// AND the way the string is generated is as follows:
					// take this string:
					//							VDSHAS|ChySnT|SFTEns
					// which is delimited pairs of 3 letter codes (do in pairs so we dont have delimited 4 char, or naked 3 char regular)
					//							base 64 endcode using the license key alpha and padch
					//#define LICENSE_B64ALPHA		"XhIWsP4RtOoSTQpuvJ-LBCnGrAyd8eVi7%Dz1wkl5xMj6NqZa2YHbc9f0+Eg3UFm"
					//#define LICENSE_B64PADCH		'K'
					// and there you go.

												if((m_pszOEMcodes)&&(strlen(m_pszOEMcodes)>3))
												{
													CBufferUtil bu;
													char* pszCodes = m_pszOEMcodes;
													unsigned long ulBufLen = strlen(m_pszOEMcodes);
													bu.Base64Decode(&pszCodes, &ulBufLen, false, LICENSE_B64ALPHA, LICENSE_B64PADCH);

													CSafeBufferUtil sbu;
													char* pchCodes = sbu.Token(pszCodes, strlen(pszCodes), "|");
													g_ppromolite->m_data.m_key.m_bValid = false;
													while(pchCodes)
													{
														if(strncmp(g_ppromolite->m_data.m_key.m_ppszValues[i], pchCodes, 3)==0)
														{
															g_ppromolite->m_data.m_key.m_bValid = true;
															break;
														}
														else
														if((strlen(pchCodes)>3)&&(strncmp(g_ppromolite->m_data.m_key.m_ppszValues[i], pchCodes+3, 3)==0))
														{
															g_ppromolite->m_data.m_key.m_bValid = true;
															break;
														}
														pchCodes = sbu.Token(NULL, NULL, "|");
													}
													if(pszCodes)
													{
														try{free(pszCodes);} catch(...){}
													}
												}
												else g_ppromolite->m_data.m_key.m_bValid = false;
											}
										}
										i++;
									}
								
									if(
											(g_ppromolite->m_data.m_key.m_bValid)
										&&(
												(!g_ppromolite->m_data.m_key.m_bExpires)
											||((g_ppromolite->m_data.m_key.m_bExpires)&&(!g_ppromolite->m_data.m_key.m_bExpired))
											||((g_ppromolite->m_data.m_key.m_bExpires)&&(g_ppromolite->m_data.m_key.m_bExpireForgiveness)&&(g_ppromolite->m_data.m_key.m_ulExpiryDate+g_ppromolite->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
											)
										&&(
												(!g_ppromolite->m_data.m_key.m_bMachineSpecific)
											||((g_ppromolite->m_data.m_key.m_bMachineSpecific)&&(g_ppromolite->m_data.m_key.m_bValidMAC))
											)
										)
									{
//										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//										g_ppromolite->m_data.SetStatusText(errorstring, PROMOLITE_STATUS_OK);
									}
									else
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
										g_ppromolite->m_data.SetStatusText(errorstring, PROMOLITE_STATUS_ERROR);
									}
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
									g_ppromolite->m_data.SetStatusText(errorstring, PROMOLITE_STATUS_ERROR);
								}

							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("CommandServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usCommandPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("StatusServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usStatusPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Messager")==0)
				{
					if(szParameter.CompareNoCase("UseEmail")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseEmail = true;
							else m_bUseEmail = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseNet")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseNetwork = true;
							else m_bUseNetwork = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseLog = true;
							else m_bUseLog = false;
						}
					}
					else
					if(szParameter.CompareNoCase("ReportSuccessfulOperation")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bReportSuccessfulOperation = true;
							else m_bReportSuccessfulOperation = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseXMLClientLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseXMLClientLog = true;
							else m_bUseXMLClientLog = false;
						}
					}
				}
				else
/*			
				if(szCategory.CompareNoCase("HarrisAPI")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("UseListCount")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseListCount = true;
							else m_bUseListCount = false;
						}
					}
				}
				else
	*/
				if(szCategory.CompareNoCase("FileHandling")==0)
				{
					if(szParameter.CompareNoCase("SystemFolder")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszSystemFolderPath)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszSystemFolderPath) free(m_pszSystemFolderPath);
								m_pszSystemFolderPath = pch;
							}
						}
					}
/*					
					else
					if(szParameter.CompareNoCase("AutoDeleteOldest")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bAutoDeleteOldest = true;
							else m_bAutoDeleteOldest = false;
						}
					}
					else
					if(szParameter.CompareNoCase("TransferFailureRetryTime")==0)
					{
						if(nLength>0)
						{
							m_nTransferFailureRetryTime = atoi(szValue);
						}
					}
*/

				}
				else
/*
				if(szCategory.CompareNoCase("Imagestore")==0)
				{
					if(szParameter.CompareNoCase("NullOXTFile")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszImagestoreNullFile) free(m_pszImagestoreNullFile);
								m_pszImagestoreNullFile = pch;
							}
						}
					}
				}
				else
*/

				if(szCategory.CompareNoCase("Lyric")==0)
				{
					if(szParameter.CompareNoCase("MinLyricAlias")==0)
					{
						if(nLength>0)
						{
							m_nMinLyricAlias = atoi(szValue);
						}
					}
					else
					if(szParameter.CompareNoCase("MaxLyricAlias")==0)
					{
						if(nLength>0)
						{
							m_nMaxLyricAlias = atoi(szValue);
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Timeline")==0)
				{
					if(szParameter.CompareNoCase("done_count")==0)
					{
						if(nLength>0)
						{
							m_nDoneCount = atoi(szValue);
							if(m_nDoneCount<0) m_nDoneCount=0;
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Debug")==0)
				{
					if(szParameter.CompareNoCase("RecycleEvents")==0)
					{
						if(nLength>0)
						{
							m_nRecycleSeconds = atoi(szValue);
						}
					}
				}
				else



				if(szCategory.CompareNoCase("Automation")==0)
				{
					if(szParameter.CompareNoCase("AnalyzeRulesDwellMS")==0)
					{
						if(nLength>0)
						{
							m_nAnalyzeRulesDwellMS = atoi(szValue);
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Automation")==0)
				{
					if(szParameter.CompareNoCase("AnalyzeParameterDwellMS")==0)
					{
						if(nLength>0)
						{
							m_nAnalyzeParameterDwellMS = atoi(szValue);
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Automation")==0)
				{
					if(szParameter.CompareNoCase("AnalyzeTimingDwellMS")==0)
					{
						if(nLength>0)
						{
							m_nAnalyzeTimingDwellMS = atoi(szValue);
						}
					}
				}
				else

				if(szCategory.CompareNoCase("Database")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("SettingsTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszSettings)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszSettings) free(m_pszSettings);
								m_pszSettings = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ExchangeTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszExchange)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszExchange) free(m_pszExchange);
								m_pszExchange = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("MessagesTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszMessages)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszMessages) free(m_pszMessages);
								m_pszMessages = pch;
							}
						}
					}
/*
					else
					if(szParameter.CompareNoCase("ChannelsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszChannels) free(m_pszChannels);
								m_pszChannels = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ConnectionsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszConnections) free(m_pszConnections);
								m_pszConnections = pch;
							}
						}
					}
* /
					else
					if(szParameter.CompareNoCase("LiveEventsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLiveEvents) free(m_pszLiveEvents);
								m_pszLiveEvents = pch;
							}
						}
					}
*/
					else
					if(szParameter.CompareNoCase("ModificationCheckInterval")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_ulModsIntervalMS = nLength;
						}
					}
				}
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
					if(szParameter.CompareNoCase("Messages")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeMessageDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
					else
					if(szParameter.CompareNoCase("AsRun_Log")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeAsRunDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
				}
/*
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
				}
*/				

				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			if(pszInfo)
			{
				_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
			}
			delete prs;
			LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);

			Settings(false); //write
/*
			if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
			{
				// these explicts arent necessary - uncomment to write out a full file to edit...
				file.SetIniString("Main", "Name", g_ppromolite->m_settings.m_pszName);
				file.SetIniString("License", "Key", g_ppromolite->m_settings.m_pszLicense);

				file.SetIniString("FileServer", "IconPath", g_ppromolite->m_settings.m_pszIconPath);
				file.SetIniInt("CommandServer", "ListenPort", g_ppromolite->m_settings.m_usCommandPort);
				file.SetIniInt("StatusServer", "ListenPort", g_ppromolite->m_settings.m_usStatusPort);

				file.SetIniInt("Messager", "UseEmail", g_ppromolite->m_settings.m_bUseEmail?1:0);
				file.SetIniInt("Messager", "UseNet", g_ppromolite->m_settings.m_bUseNetwork?1:0);
				file.SetIniInt("Messager", "UseLog", g_ppromolite->m_settings.m_bUseLog?1:0);
				file.SetIniInt("Messager", "ReportSuccessfulOperation", g_ppromolite->m_settings.m_bReportSuccessfulOperation?1:0);
				file.SetIniInt("Messager", "LogTransfers", g_ppromolite->m_settings.m_bLogTransfers?1:0);

				file.SetIniString("Database", "DSN", g_ppromolite->m_settings.m_pszDSN);
				file.SetIniString("Database", "DBUser", g_ppromolite->m_settings.m_pszUser);
				file.SetIniString("Database", "DBPassword", g_ppromolite->m_settings.m_pszPW);
				file.SetIniString("Database", "SettingsTableName", g_ppromolite->m_settings.m_pszSettings);  // the Settings table name
				file.SetIniString("Database", "ExchangeTableName", g_ppromolite->m_settings.m_pszExchange);  // the Exchange table name
				file.SetIniString("Database", "MessagesTableName", g_ppromolite->m_settings.m_pszMessages);  // the Messages table name

		//		file.SetIniInt("HarrisAPI", "UseListCount", g_ppromolite->m_settings.m_bUseListCount?1:0); // get all events up until the list count (otherwise just up to the lookahead)

		//		file.SetIniString("Database", "ChannelsTableName", g_ppromolite->m_settings.m_pszChannels);  // the Channels table name
		//		file.SetIniString("Database", "ConnectionsTableName", g_ppromolite->m_settings.m_pszConnections);  // the Connections table name
				file.SetIniString("Database", "QueueTableName", g_ppromolite->m_settings.m_pszQueue);  // the Queue table name
				file.SetIniString("Database", "RulesTableName", g_ppromolite->m_settings.m_pszMappings);  // the Rules table name
				file.SetIniString("Database", "EventsTableName", g_ppromolite->m_settings.m_pszEvents);  // the Events table name
				file.SetIniString("Database", "AnalysisViewName", g_ppromolite->m_settings.m_pszTriggerAnalysisView);  // the TriggerAnalysisView name
				file.SetIniString("Database", "AnalyzedTriggerDataTableName", g_ppromolite->m_settings.m_pszAnalyzedTriggerData);  // the AnalyzedTriggerData table name
				file.SetIniString("Database", "AnalyzeRulesProcedureName", g_ppromolite->m_settings.m_pszAnalyzeRulesProc);  // the spAnalyzeRules stored procedure
				file.SetIniString("Database", "GetParameterValueProcedureName", g_ppromolite->m_settings.m_pszGetParameterValueProc);  // the spGetParameterValue stored procedure

				file.SetIniString("Demo", "DefaultProject", g_ppromolite->m_settings.m_pszDefaultProject);
				file.SetIniString("Demo", "DefaultScene", g_ppromolite->m_settings.m_pszDefaultScene);
				file.SetIniString("Demo", "DefaultHost", g_ppromolite->m_settings.m_pszDefaultHost);


				file.SetIniInt("Database", "ModificationCheckInterval", g_ppromolite->m_settings.m_ulModsIntervalMS);  // in milliseconds
//				file.SetIniInt("FileHandling", "TransferFailureRetryTime", g_ppromolite->m_settings.m_nTransferFailureRetryTime);  // in seconds
//				file.SetIniInt("FileHandling", "AutoDeleteOldest", g_ppromolite->m_settings.m_bAutoDeleteOldest?1:0);
//				file.SetIniInt("FileHandling", "DeletionThreshold", g_ppromolite->m_settings.m_ulDeletionThreshold ); // two days
				file.SetIniInt("FileHandling", "TriggerAdvanceMS", g_ppromolite->m_settings.m_nTriggerAdvanceMS);// number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)

				file.SetIniString("FileHandling", "SystemFolder", g_ppromolite->m_settings.m_pszSystemFolderPath);  // must have trailing slash
//				file.SetIniString("Imagestore", "NullOXTFile", g_ppromolite->m_settings.m_pszImagestoreNullFile );  // name of the null file, must be in system folder
//				file.SetIniString("Imagestore", "IntuitionSearchExt", g_ppromolite->m_settings.m_pszIntuitionSearchExt); // search extensions for Intuition
//				file.SetIniString("Imagestore", "Imagestore2SearchExt", g_ppromolite->m_settings.m_pszImagestore2SearchExt);			// search extensions for IS2
//				file.SetIniString("Imagestore", "Imagestore300SearchExt", g_ppromolite->m_settings.m_pszImagestore300SearchExt);		// search extensions for IS300
//				file.SetIniString("Imagestore", "ImagestoreHDSearchExt", g_ppromolite->m_settings.m_pszImagestoreHDSearchExt);		// search extensions for ISHD


				// gfx module
				file.SetIniString("Graphics", "Host", g_ppromolite->m_settings.m_pszGfxHost);  // must have trailing slash; 
				file.SetIniInt("Graphics", "Port", g_ppromolite->m_settings.m_nGfxPort); 
				
				file.SetSettings(PROMOLITE_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

			}

*/

			return PROMOLITE_SUCCESS;
		}
		LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);
	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s%s%s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. ",
				m_pdb?"":"Database pointer was NULL. ",
				m_pdbConn?"":"Connection pointer was NULL. ",
				((m_pdbConn)&&(!m_pdbConn->m_bConnected))?"Database not connected. ":""
				);
		}
	}

	return PROMOLITE_ERROR;
}

char* CPromoLiteSettings::ProcessString(char* pszString, bool bFreeIncomingString)
{
	if(pszString)
	{
		char pszOutput[4096];
		int nInLen=strlen(pszString);
		int nOutLen=0;
		char* pch = pszString;
		char* pchEnd = min((pch+4096),(pszString+nInLen));
		while((*pch!=0)&&(pch<pchEnd))
		{
			if(*pch == '%')
			{
				// we are in a specifier.
				// here is a list of ours:
				// %h  local hostname
		
				pch++;
				if((*pch!=0)&&(pch<pchEnd))
				{
					if(*pch == 'h')
					{
						if((g_ppromolite->m_data.m_pszHost)&&(strlen(g_ppromolite->m_data.m_pszHost)))
						{
							char* pszTemp = (char*)malloc(strlen(g_ppromolite->m_data.m_pszHost)+1);
							if(pszTemp)
							{
								strcpy(pszTemp, g_ppromolite->m_data.m_pszHost);
							
								char* pchPlus = pszTemp;
								while((*pchPlus != 0)&&(nOutLen<4096))
								{
									pszOutput[nOutLen]= *pchPlus;
									nOutLen++;
									pchPlus++;
								}
								free(pszTemp);
							}
						}
						pch++;
					}
					else
					{
						// some other specifier, can be anything.
						pszOutput[nOutLen]= *(pch-1); // do add the %, this may get further processed
						nOutLen++;
						pszOutput[nOutLen]= *pch;
						nOutLen++;
						pch++;
					}
				}
				else
				{
					pszOutput[nOutLen]= *(pch-1);
					nOutLen++;
				}
				
			}
			else
			{
				pszOutput[nOutLen]= *pch;
				nOutLen++;
				pch++;
			}
		}

		pszOutput[nOutLen]=0;

		pch = (char*)malloc(strlen(pszOutput)+1);
		if(pch)
		{
			strcpy(pch,pszOutput);
			if(bFreeIncomingString) 
			{
				try {free(pszString);} catch(...){}
			}
		}
		return pch;
	}
	return NULL;
}


