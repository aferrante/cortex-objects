// PromoLiteMain.cpp: implementation of the CPromoLiteMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "PromoLite.h"  // just included to have access to windowing environment
#include "PromoLiteDlg.h"  // just included to have access to windowing environment
#include "PromoLiteHandler.h"  // just included to have access to windowing environment

#include "PromoLiteMain.h"
#include <process.h>
#include "../../Common/TXT/BufferUtil.h"
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"

#include "..\Sentinel\SentinelDefines.h"
#include "..\..\Common\API\Harris\ADCDefs.h"
//#include "..\..\Common\API\Miranda\IS2Comm.h"
#include "..\..\Common\API\Miranda\IS2Core.h"
//#include "..\Radiance\RadianceDefines.h"
#include "..\Libretto\LibrettoDefines.h"


#include <sys/timeb.h>
#include <time.h>
#include <direct.h>
#include <objsafe.h>
#include <atlbase.h>

// have to add rpcrt4.lib to the linker for UUID stuff


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;
bool g_bKillStatus = false;
CPromoLiteMain* g_ppromolite=NULL;
//CADC g_adc;
//CIS2Comm g_is2;  // inlined code - problems with CIS2Comm::UtilParseTem memory management . never figured this one out.

extern CMessager* g_pmsgr;  // from Messager.cpp
extern CPromoLiteApp theApp;

CPromoLiteAutomationChannelObject* g_pChannelObj=NULL;


//void PromoLiteConnectionThread(void* pvArgs);
//void PromoLiteListThread(void* pvArgs);

void PromoLiteGlobalAutomationThread(void* pvArgs);
void PromoLiteGlobalAnalysisThread(void* pvArgs);
void PromoLiteAutomationThread(void* pvArgs);
//void PromoLiteFarAnalysisThread(void* pvArgs);
void PromoLiteTriggerThread(void* pvArgs);
void PromoLiteTriggerDelayNotificationThread(void* pvArgs);
void PromoLiteDonePurgeThread(void* pvArgs);


#define NOT_USING_NEAR_ANALYSIS

#ifndef NOT_USING_NEAR_ANALYSIS
void PromoLiteNearAnalysisThread(void* pvArgs);
#endif //#ifndef NOT_USING_NEAR_ANALYSIS


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPromoLiteMain::CPromoLiteMain()
{
}

CPromoLiteMain::~CPromoLiteMain()
{
}

/*

char*	CPromoLiteMain::PromoLiteTranslate(CHTTPHeader* pHeader, char* pszBuffer)			// apply promolite scripting language
{
	return pszBuffer;
}

int		CPromoLiteMain::InterpretPromoLiteive(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the promoliteive, such as a char buffer.
	return PROMOLITE_SUCCESS;
}
*/

SOCKET*	CPromoLiteMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // promolite initiates a request to an object server
{
	CNetData* pdata = new CNetData;

	pdata->m_ucType = ucType;      // defined type - indicates which protocol to use, structure of data

	pdata->m_ucCmd = ucCmd;       // the command byte
	pdata->m_ucSubCmd = ucSubCmd;       // the subcommand byte

	pdata->m_pucData = pucData;
	pdata->m_ulDataLen = ulDataLen;

	SOCKET* ps = NULL; 
	m_net.SendData(pdata, pchHost, usPort, 5000, 0, NET_SND_CMDTOSVR, ps);
//	m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);

	return ps;
}

int		CPromoLiteMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// promolite replies to an object server after receiving data. (usually ack or nak)
{
	return PROMOLITE_SUCCESS;
}

int		CPromoLiteMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// promolite answers a request from an object client
{
	return PROMOLITE_SUCCESS;
}


void PromoLiteMainThread(void* pvArgs)
{
	CPromoLiteApp* pApp = (CPromoLiteApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment
	//startup.
	g_bThreadStarted = true;

	//create the main objects.

	CPromoLiteMain promolite;
	CDBUtil db;

	promolite.m_data.m_ulFlags &= ~PROMOLITE_STATUS_THREAD_MASK;
	promolite.m_data.m_ulFlags |= PROMOLITE_STATUS_THREAD_START;
	promolite.m_data.m_ulFlags &= ~PROMOLITE_ICON_MASK;
	promolite.m_data.m_ulFlags |= PROMOLITE_STATUS_UNINIT;

	promolite.m_data.GetHost();

	g_ppromolite = &promolite;


	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
//	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\PromoLite\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}



//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

//		AfxMessageBox( pApp->m_lpCmdLine );

	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// check here for Cortex IP.
			if(strlen(pch))
			{
//		AfxMessageBox( pch );
				if(promolite.m_data.m_bu.CountChar(pch, strlen(pch), ':')>=1)  // must be of form "hostname:cmdport" where hostname can be name or ip
				{
					promolite.m_data.m_pszCortexHost = (char*)malloc(strlen(pch)+1);
					if(promolite.m_data.m_pszCortexHost)
					{
						strcpy(promolite.m_data.m_pszCortexHost, pch);
//		AfxMessageBox( promolite.m_data.m_pszCortexHost );

						char* pchd = strchr(promolite.m_data.m_pszCortexHost, ':');
						if(pchd)
						{
							*pchd = 0;  // null term the host name;
							pchd++;
//		AfxMessageBox( promolite.m_data.m_pszCortexHost );

							char* pchd2 = strchr(pchd, ':');
							if(pchd2)
							{
								*pchd2 = 0;  // null term the cmd port;
								pchd2++;
								if(strlen(pchd2)) promolite.m_data.m_usCortexStatusPort = atoi(pchd2);
							}

							if(strlen(pchd)) promolite.m_data.m_usCortexCommandPort = atoi(pchd);
						}
					}

				}
			}
			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }

	promolite.m_settings.Settings(true); //read
/////////////////////////////////////////////////
// would comment this part out, but need to get dependencies here... once.
	char pszFilename[MAX_PATH];

	//	AfxMessageBox("3");

	strcpy(pszFilename, "");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, PROMOLITE_SETTINGS_FILE_DEFAULT);  // cortex settings file

	CFileUtil file;
	file.GetSettings(pszFilename, false); 
	char* pszParams = NULL;
// load up the values on the settings object
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
/*
		promolite.m_settings.m_pszName = file.GetIniString("Main", "Name", "PromoLite");
		promolite.m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");

		// compile license key params
		if(g_ppromolite->m_data.m_key.m_pszLicenseString) free(g_ppromolite->m_data.m_key.m_pszLicenseString);
		g_ppromolite->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(promolite.m_settings.m_pszLicense)+1);
		if(g_ppromolite->m_data.m_key.m_pszLicenseString)
		sprintf(g_ppromolite->m_data.m_key.m_pszLicenseString, "%s", promolite.m_settings.m_pszLicense);

		g_ppromolite->m_data.m_key.InterpretKey();

		if(g_ppromolite->m_data.m_key.m_bValid)
		{
			unsigned long i=0;
			while(i<g_ppromolite->m_data.m_key.m_ulNumParams)
			{
				if((g_ppromolite->m_data.m_key.m_ppszParams)
					&&(g_ppromolite->m_data.m_key.m_ppszValues)
					&&(g_ppromolite->m_data.m_key.m_ppszParams[i])
					&&(g_ppromolite->m_data.m_key.m_ppszValues[i]))
				{
					if(stricmp(g_ppromolite->m_data.m_key.m_ppszParams[i], "max")==0)
					{
//						g_ppromolite->m_data.m_nMaxLicensedDevices = atoi(g_ppromolite->m_data.m_key.m_ppszValues[i]);
					}
				}
				i++;
			}

			if(
					(
				    (!promolite.m_data.m_key.m_bExpires)
					||((promolite.m_data.m_key.m_bExpires)&&(!promolite.m_data.m_key.m_bExpired))
					||((promolite.m_data.m_key.m_bExpires)&&(promolite.m_data.m_key.m_bExpireForgiveness)&&(promolite.m_data.m_key.m_ulExpiryDate+promolite.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
				    (!promolite.m_data.m_key.m_bMachineSpecific)
					||((promolite.m_data.m_key.m_bMachineSpecific)&&(promolite.m_data.m_key.m_bValidMAC))
					)
				)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
				promolite.m_data.SetStatusText(errorstring, PROMOLITE_STATUS_OK);
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				promolite.m_data.SetStatusText(errorstring, PROMOLITE_STATUS_ERROR);
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
			promolite.m_data.SetStatusText(errorstring, PROMOLITE_STATUS_ERROR);
		}

		promolite.m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");

		promolite.m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", PROMOLITE_PORT_CMD);
		promolite.m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", PROMOLITE_PORT_STATUS);

		promolite.m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
		promolite.m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
		promolite.m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
		promolite.m_settings.m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
		promolite.m_settings.m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;

//		promolite.m_settings.m_bUseListCount = file.GetIniInt("HarrisAPI", "UseListCount", 0)?true:false; // get all events up until the list count (otherwise just up to the lookahead)

		promolite.m_settings.m_pszDSN = file.GetIniString("Database", "DSN", promolite.m_settings.m_pszName?promolite.m_settings.m_pszName:"PromoLite");
		promolite.m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
		promolite.m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
		promolite.m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
		promolite.m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
		promolite.m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name

//		promolite.m_settings.m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels");  // the Channels table name
//		promolite.m_settings.m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Connections");  // the Connections table name
		promolite.m_settings.m_pszEvents = file.GetIniString("Database", "EventsTableName", "Events");  // the Events table name
		promolite.m_settings.m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue");  // the Queue table name
		promolite.m_settings.m_pszRules = file.GetIniString("Database", "RulesTableName", "Rules");  // the Rules table name
			g_ppromolite->m_settings.m_pszTriggerAnalysisView = file.GetIniString("Database", "AnalysisViewName", "TriggerAnalysisView");  // the TriggerAnalysisView name
			g_ppromolite->m_settings.m_pszAnalyzedTriggerData = file.GetIniString("Database", "AnalyzedTriggerDataTableName", "AnalyzedTriggerData");  // the AnalyzedTriggerData table name
			g_ppromolite->m_settings.m_pszAnalyzeRulesProc = file.GetIniString("Database", "AnalyzeRulesProcedureName", "spAnalyzeRules");  // the spAnalyzeRules stored procedure
			g_ppromolite->m_settings.m_pszGetParameterValueProc = file.GetIniString("Database", "GetParameterValueProcedureName", "spGetParameterValue");  // the spGetParameterValue stored procedure

		promolite.m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
//		promolite.m_settings.m_nTransferFailureRetryTime = file.GetIniInt("FileHandling", "TransferFailureRetryTime", 60);  // in seconds
//		promolite.m_settings.m_bAutoDeleteOldest = file.GetIniInt("FileHandling", "AutoDeleteOldest", 1)?true:false;
//		promolite.m_settings.m_ulDeletionThreshold = file.GetIniInt("FileHandling", "DeletionThreshold", 172800); // two days
		promolite.m_settings.m_pszDefaultProject = file.GetIniString("Demo", "DefaultProject", "Promo_B");
		promolite.m_settings.m_pszDefaultScene = file.GetIniString("Demo", "DefaultScene", "Promo_B");
		promolite.m_settings.m_pszDefaultHost = file.GetIniString("Demo", "DefaultHost", "10.0.0.22");

		promolite.m_settings.m_pszSystemFolderPath = file.GetIniString("FileHandling", "SystemFolder", "C:\\");  // must have trailing slash
		promolite.m_settings.m_nTriggerAdvanceMS = file.GetIniInt("FileHandling", "TriggerAdvanceMS", 0);// number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)

//		promolite.m_settings.m_pszImagestoreNullFile = file.GetIniString("Imagestore", "NullOXTFile", "null.oxt");  // name of the null file, must be in system folder
//		promolite.m_settings.m_pszIntuitionSearchExt = file.GetIniString("Imagestore", "IntuitionSearchExt", "tem"); // search extensions for Intuition
//		promolite.m_settings.m_pszImagestore2SearchExt = file.GetIniString("Imagestore", "Imagestore2SearchExt", "oxa,oxt,oxw");			// search extensions for IS2
//		promolite.m_settings.m_pszImagestore300SearchExt = file.GetIniString("Imagestore", "Imagestore300SearchExt", "oxa,oxt,oxe");		// search extensions for IS300
//		promolite.m_settings.m_pszImagestoreHDSearchExt = file.GetIniString("Imagestore", "ImagestoreHDSearchExt", "oxa,oxt,oxe");		// search extensions for ISHD

			// gfx module
		promolite.m_settings.m_pszGfxHost = file.GetIniString("Graphics", "Host", "127.0.0.1");  // must have trailing slash; 
		promolite.m_settings.m_nGfxPort = file.GetIniInt("Graphics", "Port", 10682); 

*/
		// now, get the dependencies.... (get only, not set at end).
		promolite.m_settings.m_nNumEndpointsInstalled = 0;
		int nNumEndpoints = file.GetIniInt("Dependencies", "Number", 0); 
		int nDep=0;
		char szKey[64];
		while(nDep<nNumEndpoints)
		{
			sprintf(szKey, "Dependency%03d", nDep);
			pszParams = file.GetIniString("Dependencies", szKey, ""); 
			if(pszParams)
			{
				if(strlen(pszParams))
				{
					CPromoLiteEndpointObject* pdeo = new CPromoLiteEndpointObject;
					if(pdeo)
					{
						pdeo->m_pszDBName = file.GetIniString(pszParams, "DBDefault", pszParams); // db name!  real name
						pdeo->m_pszName = file.GetIniString(pszParams, "Label", pszParams); // familiar name, just a label
						pdeo->m_pszExchange = file.GetIniString(pszParams, "DBExchangeTable", "Exchange"); // Exchange table name
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBQueueTable", "Queue");// Queue table name
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszQueue = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBLiveEventsTable", "Events");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszLiveEvents = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBDestinationTable", "Destinations");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszDestination = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBDestinationMediaTable", "Destinations_Media");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszDestinationMedia = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBChannelTable", "");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszChannel = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBMetadataTable", "");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszMetadata = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBFileTypesTable", "");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszFileTypes = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}

						pszParams = file.GetIniString(pdeo->m_pszDBName, "Type", ""); 
						if(pszParams)
						{
							if(stricmp(pszParams, "Archivist")==0) { pdeo->m_usType = PROMOLITE_DEP_DATA_ARCHIVIST; promolite.m_data.m_nIndexMetadataEndpoint = promolite.m_settings.m_nNumEndpointsInstalled; }
							else if(stricmp(pszParams, "Helios")==0)  { pdeo->m_usType = PROMOLITE_DEP_AUTO_HELIOS; promolite.m_data.m_nTypeAutomationInstalled=PROMOLITE_DEP_AUTO_HELIOS; promolite.m_data.m_nIndexAutomationEndpoint=promolite.m_settings.m_nNumEndpointsInstalled;}
							else if(stricmp(pszParams, "Sentinel")==0){ pdeo->m_usType = PROMOLITE_DEP_AUTO_SENTINEL; promolite.m_data.m_nTypeAutomationInstalled=PROMOLITE_DEP_AUTO_SENTINEL; promolite.m_data.m_nIndexAutomationEndpoint=promolite.m_settings.m_nNumEndpointsInstalled;}
							else if(stricmp(pszParams, "Prospero")==0) pdeo->m_usType = PROMOLITE_DEP_EDGE_PROSPERO;
							else if(stricmp(pszParams, "Luminary")==0) pdeo->m_usType = PROMOLITE_DEP_EDGE_LUMINARY;
							else if(stricmp(pszParams, "Libretto")==0) pdeo->m_usType = PROMOLITE_DEP_EDGE_LIBRETTO;
							else if(stricmp(pszParams, "Radiance")==0) pdeo->m_usType = PROMOLITE_DEP_EDGE_RADIANCE;
							else if(stricmp(pszParams, "Barbero")==0) pdeo->m_usType = PROMOLITE_DEP_EDGE_BARBERO;
							free(pszParams); pszParams=NULL;
						}

						CPromoLiteEndpointObject** ppObj = new CPromoLiteEndpointObject*[promolite.m_settings.m_nNumEndpointsInstalled+1];
						if(ppObj)
						{
							int o=0;
							if((promolite.m_settings.m_ppEndpointObject)&&(promolite.m_settings.m_nNumEndpointsInstalled>0))
							{
								while(o<promolite.m_settings.m_nNumEndpointsInstalled)
								{
									ppObj[o] = promolite.m_settings.m_ppEndpointObject[o];
									o++;
								}
								delete [] promolite.m_settings.m_ppEndpointObject;

							}
							ppObj[promolite.m_settings.m_nNumEndpointsInstalled] = pdeo;
							promolite.m_settings.m_ppEndpointObject = ppObj;
							promolite.m_settings.m_nNumEndpointsInstalled++;
						}
						else
							delete pdeo;
					}
				}

				free(pszParams); pszParams=NULL;
			}
			nDep++;
		}

		if(promolite.m_settings.m_nNumEndpointsInstalled<nNumEndpoints)
		{
			//**MSG hmmmm..cant do till later on...
		}

		if(pszParams) free(pszParams); pszParams=NULL;
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
	}

	bool bUseLog = false;
	bool bUseEmail = false;
	bool bLogNetworkErrors = false;
	if(promolite.m_settings.m_bUseLog)
	{
		bUseLog = promolite.m_settings.m_bUseLog;

		// for logfiles, we need params, and they must be in this format:
		//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
//		pszParams = file.GetIniString("Messager", "LogFileIni", "PromoLite|YD||1|");
//		AfxMessageBox(pszParams);
		int nRegisterCode=0;

		nRegisterCode = promolite.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
			"log", 
			promolite.m_settings.m_pszProcessedFileSpec?promolite.m_settings.m_pszProcessedFileSpec:promolite.m_settings.m_pszFileSpec, 
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
			promolite.m_data.SetStatusText(errorstring, (PROMOLITE_STATUS_FAIL_LOG|PROMOLITE_STATUS_ERROR));
		}

		if(pszParams) free(pszParams); pszParams=NULL;
	}

	promolite.m_msgr.DM(MSG_ICONINFO, NULL, "PromoLite", "--------------------------------------------------\n\
-------------- PromoLite %s start --------------", PROMOLITE_CURRENT_VERSION);  //(Dispatch message)

	if(promolite.m_settings.m_bUseEmail)
	{
		bUseEmail = promolite.m_settings.m_bUseEmail;
				// for smtp, we need params, and they must be in this format:
				// SMTP address[: override port (optional)] | distribution lists | from email | subject spec | localhostname | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
				// note on distribution lists:
				// we want to give the option to map message type and severity to different address lists.
				// the distribution lists field format is this:
				// ff:x@x.com,q@q.com;0x:B@b.com
				// the : delimits the type and severity flag specifier from the email address list for that set. 
				// the email addresses are delimited by comma
				// the sets are delimited by ;
				// the delimiters are not allowed in addresses, as described here: http://en.wikipedia.org/wiki/E-mail_address
				// on the type and severity specifiers, it is like this:
				// the subfield is a 2 character hex code, sort of.
				// severity is the first char, possible values are 0,1,2,3,4, for normal, low, medium, high, critical
				// additionally, f means all
				// type is the second char, possible values are 0-8, for the follwing:
				// #define MSG_ICONNONE								0x00000000  // default, nothing
				// #define MSG_ICONQUESTION						0x00000001  // ? icon
				// #define MSG_ICONEXCLAMATION				0x00000002  // ! icon
				// #define MSG_ICONERROR							0x00000003  // X icon
				// #define MSG_ICONSTOP								0x00000004  // stop sign icon
				// #define MSG_ICONHAND								0x00000005  // hand icon
				// #define MSG_ICONINFO								0x00000006  // (i) icon 
				// #define MSG_ICONUSER1							0x00000007  
				// #define MSG_ICONUSER2							0x00000008  
				// ff means send all messages to the address list
				// 00 means send all messages with severity normal and no "icon" to the address list
				// 03 means send all messages with severity normal and error type to the address list
				// 43 means send all messages with severity critical and error type to the address list
				// you can set up different address lists with the same addresses - one email per dispatched message will be sent in the case of overlap.

		int nRegisterCode=0;

		// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
		nRegisterCode = promolite.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
			"email", 
			promolite.m_settings.m_pszProcessedMailSpec?promolite.m_settings.m_pszProcessedMailSpec:promolite.m_settings.m_pszMailSpec,
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
			promolite.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
			promolite.m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:smtp_init", errorstring);  //(Dispatch message)
		}

//		if(pszParams) free(pszParams); pszParams=NULL;
	}


//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
//	promolite.m_http.InitializeMessaging(&promolite.m_msgr);
//	promolite.m_net.InitializeMessaging(&promolite.m_msgr);
	if(promolite.m_settings.m_bLogNetworkErrors) 
	{
		bLogNetworkErrors = promolite.m_settings.m_bLogNetworkErrors;
		if(promolite.m_net.InitializeMessaging(&promolite.m_msgr)==0)
		{
			promolite.m_data.m_bNetworkMessagingInitialized=true;
		}
	}
//	g_is2.InitializeMessaging(&promolite.m_msgr);

// TODO: here, get the DB and pull any override settings.
	//****
	CDBconn* pdbConn = db.CreateNewConnection(promolite.m_settings.m_pszDSN, promolite.m_settings.m_pszUser, promolite.m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			promolite.m_data.SetStatusText(errorstring, (PROMOLITE_STATUS_FAIL_DB|PROMOLITE_STATUS_ERROR));
			promolite.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:database_connect", errorstring);  //(Dispatch message)
		}
		else
		{
			promolite.m_settings.m_pdbConn = pdbConn;
			promolite.m_settings.m_pdb = &db;
			promolite.m_data.m_pdbConn = pdbConn;
			promolite.m_data.m_pdb = &db;
			if(promolite.m_settings.GetFromDatabase(errorstring)<PROMOLITE_SUCCESS)
			{
				promolite.m_data.SetStatusText(errorstring, (PROMOLITE_STATUS_FAIL_DB|PROMOLITE_STATUS_ERROR));
				promolite.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:database_get", errorstring);  //(Dispatch message)
			}
			else
			{
				promolite.m_data.m_nLastSettingsMod = promolite.m_data.m_nSettingsMod;
				if((!promolite.m_settings.m_bUseEmail)&&(bUseEmail))
				{
					bUseEmail = false;
					// reset it
					promolite.m_msgr.DM(MSG_ICONINFO, NULL, "PromoLite", "Shutting down email functions.");  //(Dispatch message)

//						Sleep(250); // let the message get there.
					promolite.m_msgr.RemoveDestination("email");

				}
				if((!promolite.m_settings.m_bLogNetworkErrors)&&(bLogNetworkErrors))
				{
					// reset it
					promolite.m_msgr.DM(MSG_ICONINFO, NULL, "PromoLite", "Shutting down network logging.");  //(Dispatch message)
					if(promolite.m_data.m_bNetworkMessagingInitialized)
					{
						promolite.m_net.UninitializeMessaging();  // void return
						promolite.m_data.m_bNetworkMessagingInitialized=false;
					}

				}
				if((!promolite.m_settings.m_bUseLog)&&(bUseLog))
				{
					// reset it
					promolite.m_msgr.DM(MSG_ICONINFO, NULL, "PromoLite", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

					Sleep(250); // let the message get there.
					promolite.m_msgr.RemoveDestination("log");

				}
			}
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", promolite.m_settings.m_pszDSN, promolite.m_settings.m_pszUser, promolite.m_settings.m_pszPW); 
		promolite.m_data.SetStatusText(errorstring, (PROMOLITE_STATUS_FAIL_DB|PROMOLITE_STATUS_ERROR));
		promolite.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:database_init", errorstring);  //(Dispatch message)

		//**MSG
	}


//init command and status listeners.
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing command server on %d", promolite.m_settings.m_usCommandPort); 
	promolite.m_data.SetStatusText(errorstring, PROMOLITE_STATUS_CMDSVR_START);
	promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:command_server_init", errorstring);  //(Dispatch message)

	if(promolite.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = promolite.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "PromoLiteCommandServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = PromoLiteCommandHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &promolite;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &promolite.m_net;					// pointer to the object with the Message function.


		if(promolite.m_net.StartServer(pServer, &promolite.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			promolite.m_data.SetStatusText(errorstring, PROMOLITE_STATUS_CMDSVR_ERROR);
			promolite.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:command_server_init", errorstring);  //(Dispatch message)
			promolite.SendMsg(CX_SENDMSG_ERROR, "PromoLite:command_server_init", errorstring);
		}
		else
		{
//AfxMessageBox("Q1");
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Command server listening on %d", promolite.m_settings.m_usCommandPort);
			promolite.m_data.SetStatusText(errorstring, PROMOLITE_STATUS_CMDSVR_RUN);
			promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:command_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing XML server on %d", promolite.m_settings.m_usStatusPort); 
	promolite.m_data.SetStatusText(errorstring, PROMOLITE_STATUS_STATUSSVR_START);
	promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:xml_server_init", errorstring);  //(Dispatch message)

	if(promolite.m_settings.m_usStatusPort>0)
	{
//AfxMessageBox("Q1");
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = promolite.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "PromoLiteXMLServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = PromoLiteXMLHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &promolite;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &promolite.m_net;					// pointer to the object with the Message function.

		if(promolite.m_net.StartServer(pServer, &promolite.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			promolite.m_data.SetStatusText(errorstring, PROMOLITE_STATUS_STATUSSVR_ERROR);
			promolite.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:xml_server_init", errorstring);  //(Dispatch message)
			promolite.SendMsg(CX_SENDMSG_ERROR, "PromoLite:xml_server_init", errorstring);
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML server listening on %d", promolite.m_settings.m_usStatusPort);
			promolite.m_data.SetStatusText(errorstring, PROMOLITE_STATUS_STATUSSVR_RUN);
			promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:xml_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

//AfxMessageBox("QQ");

	
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "PromoLite is initializing...");
	promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:init", errorstring);  //(Dispatch message)
	promolite.m_data.SetStatusText(errorstring, PROMOLITE_STATUS_THREAD_SPARK);

	// now connect all the registered servers
	// this is the list of servers in the db that get connected


	if(
		  (!(promolite.m_settings.m_ulMainMode&PROMOLITE_MODE_CLONE))
		&&(promolite.m_data.m_key.m_bValid)  // must have a valid license
		&&(
				(!promolite.m_data.m_key.m_bExpires)
			||((promolite.m_data.m_key.m_bExpires)&&(!promolite.m_data.m_key.m_bExpired))
			||((promolite.m_data.m_key.m_bExpires)&&(promolite.m_data.m_key.m_bExpireForgiveness)&&(promolite.m_data.m_key.m_ulExpiryDate+promolite.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
			)
		&&(
				(!promolite.m_data.m_key.m_bMachineSpecific)
			||((promolite.m_data.m_key.m_bMachineSpecific)&&(promolite.m_data.m_key.m_bValidMAC))
			)
		)
	{
//	promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "here");  Sleep(250);//(Dispatch message)

//		promolite.m_data.GetMappings(); // event rules and such are all removed NUCLEUS->PROMOLITE_CHANGE
//		promolite.m_data.GetEventRules(); // event rules and such are all removed NUCLEUS->PROMOLITE_CHANGE
//		promolite.m_data.GetTimingColumns(); // event rules and such are all removed NUCLEUS->PROMOLITE_CHANGE
//		promolite.m_data.GetParameterRules(); // event rules and such are all removed NUCLEUS->PROMOLITE_CHANGE

//	promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "X2");  Sleep(250);//(Dispatch message)
//		promolite.m_data.GetChannels();
//	promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "X3");  Sleep(250);//(Dispatch message)
	}
//	promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "X4");  Sleep(250);//(Dispatch message)
	
	if((promolite.m_data.m_ulFlags&PROMOLITE_ICON_MASK) != PROMOLITE_STATUS_ERROR)
	{
		promolite.m_data.m_ulFlags &= ~PROMOLITE_ICON_MASK;
		promolite.m_data.m_ulFlags |= PROMOLITE_STATUS_OK;  // green - we want run to be blue when something in progress
	}

//	promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "here2");  Sleep(250);//(Dispatch message)

// initialize promolite (make this not persistent for now)
	CNetData* pdata = new CNetData;
	if(pdata)
	{

		pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA;      // defined type - indicates which protocol to use, structure of data

		pdata->m_ucCmd = CX_REQ_HELLO;       // the command byte
		pdata->m_ucSubCmd = 0;       // the subcommand byte

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
		if(pch)
		{
			sprintf(pch, "%s:%d:%d:%d:%d:%s", 
				promolite.m_data.m_pszHost,
				promolite.m_settings.m_usCommandPort,
				promolite.m_settings.m_usStatusPort,
				CX_TYPE_PROCESS,
				theApp.m_nPID,
				promolite.m_settings.m_pszName?promolite.m_settings.m_pszName:"PromoLite"
				);

			pdata->m_pucData =  (unsigned char*) pch;
			pdata->m_ulDataLen = strlen(pch);
		}
//	promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "here3");  Sleep(250);//(Dispatch message)

		SOCKET s = NULL; 
//AfxMessageBox("sending");
//AfxMessageBox( promolite.m_data.m_pszCortexHost );
//AfxMessageBox( (char*)pdata->m_pucData );
		if(promolite.m_net.SendData(pdata, promolite.m_data.m_pszCortexHost, promolite.m_data.m_usCortexCommandPort, 5000, 0, NET_SND_CMDTOSVR, &s)>=NET_SUCCESS)
		{
			//send ack
//			AfxMessageBox("ack");
			promolite.m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);
		}
		else
		{
//			AfxMessageBox("could not send");
		}
		promolite.m_net.CloseConnection(s);

		delete pdata;

	}

//	promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "here4");  Sleep(250);//(Dispatch message)
	_ftime(&promolite.m_data.m_timebAutomationTick); // the last time check inside the thread
//	_ftime(&promolite.m_data.m_timebNearTick); // the last time check inside the thread
//	_ftime(&promolite.m_data.m_timebFarTick); // the last time check inside the thread
//	_ftime(&promolite.m_data.m_timebTriggerTick); // the last time check inside the thread



	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "PromoLite main thread running.");  
	promolite.m_data.SetStatusText(errorstring, PROMOLITE_STATUS_THREAD_RUN);
	promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:init", "PromoLite is initialized, main thread is running.");  Sleep(50); //(Dispatch message)
	promolite.SendMsg(CX_SENDMSG_INFO, "PromoLite:init", "PromoLite %s main thread running.", PROMOLITE_CURRENT_VERSION);
//	promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", errorstring);  Sleep(250);//(Dispatch message)


	//have to start the automation and analysis threads
	promolite.m_data.m_bAutomationThreadStarted=false;
//	promolite.m_data.m_bFarAnalysisThreadStarted=false;
	promolite.m_data.m_bGlobalAnalysisThreadStarted=false;

/* // removed this for Promolite NUCLEUS->PROMOLITE_CHANGE
	if(_beginthread(PromoLiteGlobalAutomationThread, 0, (void*)&promolite)==-1)
	{
		//error.  try again once
		Sleep(1000);
		if(_beginthread(PromoLiteGlobalAutomationThread, 0, (void*)&promolite)==-1)
		{
			//report failure
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "PromoLite automation thread failure.");  
			promolite.m_data.SetStatusText(errorstring, (PROMOLITE_STATUS_ERROR|PROMOLITE_STATUS_THREAD_ERROR));
			promolite.m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:automation_thread_init", errorstring);  //(Dispatch message)
			promolite.SendMsg(CX_SENDMSG_ERROR, "PromoLite:automation_thread_init", errorstring);
		}
		else
		{
			promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:automation_thread_init", "PromoLite automation thread initialized.");  //(Dispatch message)
		}
	}
	else
	{
		promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:automation_thread_init", "PromoLite automation thread initialized.");  //(Dispatch message)
	}
	Sleep(250);
/*

	if(_beginthread(PromoLiteFarAnalysisThread, 0, (void*)&promolite)==-1)
	{
		//error.  try again once
		Sleep(1000);
		if(_beginthread(PromoLiteFarAnalysisThread, 0, (void*)&promolite)==-1)
		{
			//report failure
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "PromoLite analysis thread failure.");  
			promolite.m_data.SetStatusText(errorstring, (PROMOLITE_STATUS_THREAD_ERROR|PROMOLITE_STATUS_ERROR));
			promolite.m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:analysis_thread_init", errorstring);  //(Dispatch message)
			promolite.SendMsg(CX_SENDMSG_ERROR, "PromoLite:analysis_thread_init", errorstring);
		}
		else
		{
			promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:analysis_thread_init", "PromoLite analysis thread initialized.");  //(Dispatch message)
		}
	}
	else
	{
		promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:analysis_thread_init", "PromoLite analysis thread initialized.");  //(Dispatch message)
	}
	Sleep(250);

*/

/* //removed this for promolite NUCLEUS->PROMOLITE_CHANGE
	if(_beginthread(PromoLiteGlobalAnalysisThread, 0, (void*)&promolite)==-1)
	{
		//error.  try again once
		Sleep(1000);
		if(_beginthread(PromoLiteGlobalAnalysisThread, 0, (void*)&promolite)==-1)
		{
			//report failure
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "PromoLite global analysis thread failure.");  
			promolite.m_data.SetStatusText(errorstring, (PROMOLITE_STATUS_THREAD_ERROR|PROMOLITE_STATUS_ERROR));
			promolite.m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:near_analysis_thread_init", errorstring);  //(Dispatch message)
			promolite.SendMsg(CX_SENDMSG_ERROR, "PromoLite:near_analysis_thread_init", errorstring);
		}
		else
		{
			promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:global_analysis_thread_init", "PromoLite global analysis thread initialized.");  //(Dispatch message)
		}
	}
	else
	{
		promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:global_analysis_thread_init", "PromoLite global analysis thread initialized.");  //(Dispatch message)
	}
	Sleep(250);
/*

	// ******************************************************************
	// We really need to spawn one trigger thread per automation channel.
	// FOR DEMO, just one thread.
	// ******************************************************************
*/
// uncomment out the following for Promolite NUCLEUS->PROMOLITE_CHANGE
// since there is no automation in Promolite, don't need any channel info.  
// only destination based info, but that is in the trigger view.

#ifdef NUCLEUS_PROMOLITE_CHANGE

	// need an automation object just because that's where the trigger buffer is allocated.
	g_pChannelObj = new CPromoLiteAutomationChannelObject;
	if(g_pChannelObj==NULL)
	{
		//error
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "PromoLite object creation failure.");  
		promolite.m_data.SetStatusText(errorstring, (PROMOLITE_STATUS_ERROR));
		promolite.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:trigger_thread_init", errorstring);  //(Dispatch message)
		promolite.SendMsg(CX_SENDMSG_ERROR, "PromoLite:trigger_thread_init", errorstring);
	}
	else
	{
		g_pChannelObj->m_pPromoLite = &promolite;
		g_pChannelObj->m_bKillAutomationThread = false;


		if(_beginthread(PromoLiteTriggerThread, 0, (void*)g_pChannelObj)==-1)
		{
			//error.  try again once
			Sleep(1000);
			if(_beginthread(PromoLiteTriggerThread, 0, (void*)g_pChannelObj)==-1)
			{
				//report failure
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "PromoLite trigger thread failure.");  
				promolite.m_data.SetStatusText(errorstring, (PROMOLITE_STATUS_THREAD_ERROR|PROMOLITE_STATUS_ERROR));
				promolite.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:trigger_thread_init", errorstring);  //(Dispatch message)
				promolite.SendMsg(CX_SENDMSG_ERROR, "PromoLite:trigger_thread_init", errorstring);
			}
			else
			{
				promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:trigger_thread_init", "PromoLite trigger thread initialized.");  //(Dispatch message)
			}
		}
		else
		{
			promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:trigger_thread_init", "PromoLite trigger thread initialized.");  //(Dispatch message)
		}
		Sleep(250);
	}

				promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:trigger_thread_init", "PromoLite first check trigger %d %d %d.",
					(!g_bKillThread),(g_pChannelObj),(g_pChannelObj->m_bTriggerThreadStarted == false));  //(Dispatch message)


/// end of previously commented out section NUCLEUS->PROMOLITE_CHANGE
#endif //#ifdef NUCLEUS_PROMOLITE_CHANGE



	_timeb timebCheckMods;
	_ftime( &timebCheckMods );
///AfxMessageBox("xxxxx");

//	char szSQL[DB_SQLSTRING_MAXLEN];
	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &promolite.m_data.m_timebTick );
//	promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "%d.%03d", promolite.m_data.m_timebTick.time, promolite.m_data.m_timebTick.millitm);   Sleep(250);//(Dispatch message)

// monitor changes in exchange db and react.
		if(
					(promolite.m_data.m_timebTick.time > timebCheckMods.time )
				||((promolite.m_data.m_timebTick.time == timebCheckMods.time)&&(promolite.m_data.m_timebTick.millitm >= timebCheckMods.millitm))
				&&(!g_bKillThread)
			)
		{
//	promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "time to check");  Sleep(250);//(Dispatch message)
			timebCheckMods.time = promolite.m_data.m_timebTick.time + promolite.m_settings.m_ulModsIntervalMS/1000; 
			timebCheckMods.millitm = promolite.m_data.m_timebTick.millitm + (unsigned short)(promolite.m_settings.m_ulModsIntervalMS%1000); // fractional second updates
			if(timebCheckMods.millitm>999)
			{
				timebCheckMods.time++;
				timebCheckMods.millitm%=1000;
			}
//	promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "checking db connected");   Sleep(250);//(Dispatch message)
			if(pdbConn)//&&(pdbConn->m_bConnected))
			{
//AfxMessageBox("x");
//	promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "checkmods");  //(Dispatch message)
//				promolite.m_data.ReleaseRecordSet();
///				promolite.m_data.CheckDatabaseMods();  this was there twice!  2x the db hits, oh no!

				strcpy(errorstring, "");

				if(promolite.m_data.CheckDatabaseMods(errorstring)==PROMOLITE_ERROR)
				{
					if(!promolite.m_data.m_bCheckModsWarningSent)
					{
						promolite.m_msgr.DM(MSG_ICONERROR, NULL, "CheckDatabaseMods", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
						promolite.m_data.m_bCheckModsWarningSent=true;
					}
				}
				else
				{
					if(promolite.m_data.m_bCheckModsWarningSent)
					{
						promolite.m_msgr.DM(MSG_ICONINFO, NULL, "CheckDatabaseMods", "Resumed checking."); // Sleep(50); //(Dispatch message)
					}
					promolite.m_data.m_bCheckModsWarningSent=false;
				}

				if(promolite.m_data.m_timebTick.time > promolite.m_data.m_timebAutoPurge.time + promolite.m_settings.m_nAutoPurgeInterval)
				{
					_ftime(&promolite.m_data.m_timebAutoPurge);

					if(promolite.m_settings.m_nAutoPurgeMessageDays>0)
					{
						if(promolite.m_data.CheckMessages(errorstring)==PROMOLITE_ERROR)
						{
							if(!promolite.m_data.m_bCheckMsgsWarningSent)
							{
								promolite.m_msgr.DM(MSG_ICONERROR, NULL, "CheckMessages", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								promolite.m_data.m_bCheckMsgsWarningSent=true;
							}
						}
						else
						if(promolite.m_data.m_bCheckMsgsWarningSent)
						{
							promolite.m_msgr.DM(MSG_ICONINFO, NULL, "CheckMessages", "Resumed checking messages for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						promolite.m_data.m_bCheckMsgsWarningSent=false;
					}

					if(promolite.m_settings.m_nAutoPurgeAsRunDays>0)
					{
						if(promolite.m_data.CheckAsRun(errorstring)==PROMOLITE_ERROR)
						{
							if(!promolite.m_data.m_bCheckAsRunWarningSent)
							{
								promolite.m_msgr.DM(MSG_ICONERROR, NULL, "CheckAsRun", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								promolite.m_data.m_bCheckAsRunWarningSent=true;
							}
						}
						else
						if(promolite.m_data.m_bCheckMsgsWarningSent)
						{
							promolite.m_msgr.DM(MSG_ICONINFO, NULL, "CheckAsRun", "Resumed checking as-run for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						promolite.m_data.m_bCheckAsRunWarningSent=false;
					}

				}

				if(promolite.m_data.m_nSettingsMod != promolite.m_data.m_nLastSettingsMod)
				{
					if(promolite.m_settings.GetFromDatabase()>=PROMOLITE_SUCCESS)
					{
						promolite.m_data.m_nLastSettingsMod = promolite.m_data.m_nSettingsMod;


						// check for stuff to change

						// network messaging
						if(promolite.m_settings.m_bLogNetworkErrors) 
						{
							if(!promolite.m_data.m_bNetworkMessagingInitialized)
							{
								if(promolite.m_net.InitializeMessaging(&promolite.m_msgr)==0)
								{
									promolite.m_data.m_bNetworkMessagingInitialized=true;
								}
							}
						}
						else
						{
							if(promolite.m_data.m_bNetworkMessagingInitialized)
							{
								promolite.m_net.UninitializeMessaging();  // void return
								promolite.m_data.m_bNetworkMessagingInitialized=false;
							}
						}

						// logging and email messaging:

						if(!promolite.m_settings.m_bUseEmail)
						{
							if(bUseEmail)
							{
								bUseEmail = false;
								// reset it
								promolite.m_msgr.DM(MSG_ICONINFO, NULL, "Promolite", "Shutting down email functions.");  //(Dispatch message)

		//						Sleep(250); // let the message get there.
								promolite.m_msgr.RemoveDestination("email");
							}
						}
						else
						{
							if(!bUseEmail)
							{
								bUseEmail = true;
								int nRegisterCode=0;

								// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
								nRegisterCode = promolite.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
									"email", 
									promolite.m_settings.m_pszProcessedMailSpec?promolite.m_settings.m_pszProcessedMailSpec:promolite.m_settings.m_pszMailSpec,
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
									promolite.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
									promolite.m_msgr.DM(MSG_ICONERROR, NULL, "Promolite:smtp_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=promolite.m_msgr.GetDestIndex("email");
								if(nIndex>=0)
								{
									if((promolite.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(promolite.m_settings.m_pszProcessedMailSpec?promolite.m_settings.m_pszProcessedMailSpec:promolite.m_settings.m_pszMailSpec))
									{
										if(strcmp(promolite.m_msgr.m_ppDest[nIndex]->m_pszParams, (promolite.m_settings.m_pszProcessedMailSpec?promolite.m_settings.m_pszProcessedMailSpec:promolite.m_settings.m_pszMailSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = promolite.m_msgr.ModifyDestination(
												"email", 
												promolite.m_settings.m_pszProcessedMailSpec?promolite.m_settings.m_pszProcessedMailSpec:promolite.m_settings.m_pszMailSpec,
												MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify SMTP object!\n code: %d", nRegisterCode); 
												//promolite.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												promolite.m_msgr.DM(MSG_ICONERROR, NULL, "Promolite:smtp_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}

						if(!promolite.m_settings.m_bUseLog)
						{
							if(bUseLog)
							{
								bUseLog = false;
								// reset it
								promolite.m_msgr.DM(MSG_ICONINFO, NULL, "Promolite", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

								Sleep(250); // let the message get there.
								promolite.m_msgr.RemoveDestination("log");
							}
						}
						else
						{
							if(!bUseLog)
							{
								bUseLog = true;
								int nRegisterCode=0;

								nRegisterCode = promolite.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
									"log", 
									promolite.m_settings.m_pszProcessedFileSpec?promolite.m_settings.m_pszProcessedFileSpec:promolite.m_settings.m_pszFileSpec, 
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
									promolite.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_LOG|CX_STATUS_ERROR));
									promolite.m_msgr.DM(MSG_ICONERROR, NULL, "Promolite:log_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=promolite.m_msgr.GetDestIndex("log");
								if(nIndex>=0)
								{
									if((promolite.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(promolite.m_settings.m_pszProcessedFileSpec?promolite.m_settings.m_pszProcessedFileSpec:promolite.m_settings.m_pszFileSpec))
									{
										if(strcmp(promolite.m_msgr.m_ppDest[nIndex]->m_pszParams, (promolite.m_settings.m_pszProcessedFileSpec?promolite.m_settings.m_pszProcessedFileSpec:promolite.m_settings.m_pszFileSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = promolite.m_msgr.ModifyDestination(
												"log", 
												promolite.m_settings.m_pszProcessedFileSpec?promolite.m_settings.m_pszProcessedFileSpec:promolite.m_settings.m_pszFileSpec,
												MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify log object!\n code: %d", nRegisterCode); 
												//promolite.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												promolite.m_msgr.DM(MSG_ICONERROR, NULL, "Promolite:log_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}







					}
				}

				if(
					  (!promolite.m_data.m_bProcessSuspended)

					&&(promolite.m_data.m_key.m_bValid)  // must have a valid license
					&&(
							(!promolite.m_data.m_key.m_bExpires)
						||((promolite.m_data.m_key.m_bExpires)&&(!promolite.m_data.m_key.m_bExpired))
						||((promolite.m_data.m_key.m_bExpires)&&(promolite.m_data.m_key.m_bExpireForgiveness)&&(promolite.m_data.m_key.m_ulExpiryDate+promolite.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!promolite.m_data.m_key.m_bMachineSpecific)
						||((promolite.m_data.m_key.m_bMachineSpecific)&&(promolite.m_data.m_key.m_bValidMAC))
						)
					)
				{
/*  // event rules and such are all removed NUCLEUS->PROMOLITE_CHANGE
					bool bAnalysisChanges = false;
					if(promolite.m_data.m_nMappingMod != promolite.m_data.m_nLastMappingMod)
					{
						if(promolite.m_data.GetMappings()>=PROMOLITE_SUCCESS)
						{
							promolite.m_data.m_nLastMappingMod = promolite.m_data.m_nMappingMod;
						}
					}
					if(promolite.m_data.m_nEventRulesMod != promolite.m_data.m_nLastEventRulesMod)
					{
						// was this:
//						if(promolite.m_data.GetEventRules()>=PROMOLITE_SUCCESS)
						// now this, and below timing cols removed
						if((promolite.m_data.GetEventRules()>=PROMOLITE_SUCCESS)&&(promolite.m_data.GetTimingColumns()>=PROMOLITE_SUCCESS))
						{
							// this was originally not forced reanalyzed... but dont know why?  did I omit accidentally?
if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_PROCESS) 	
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Analysis change from EventRulesMod %d", promolite.m_data.m_nEventRulesMod );  // Sleep(50);//(Dispatch message)
							bAnalysisChanges = true;
							promolite.m_data.m_nLastEventRulesMod = promolite.m_data.m_nEventRulesMod;
						}
					}
					if(promolite.m_data.m_nEventsMod != promolite.m_data.m_nLastEventsMod)
					{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "getting Timing Column names"); // Sleep(100); //(Dispatch message)

/*
						if(promolite.m_data.GetTimingColumns()>=PROMOLITE_SUCCESS)
						{
							bAnalysisChanges = true;
*/ // just reset it, we do timing cols above now.
							promolite.m_data.m_nLastEventsMod = promolite.m_data.m_nEventsMod;
/*
						}
* /
					}
					if(promolite.m_data.m_nParameterRulesMod != promolite.m_data.m_nLastParameterRulesMod)
					{
						if(promolite.m_data.GetParameterRules()>=PROMOLITE_SUCCESS)
						{
if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_PROCESS) 	
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Analysis change from ParameterRulesMod %d", promolite.m_data.m_nParameterRulesMod );  // Sleep(50);//(Dispatch message)
							bAnalysisChanges = true;
							promolite.m_data.m_nLastParameterRulesMod = promolite.m_data.m_nParameterRulesMod;
						}
					}
					if(promolite.m_data.m_nConnectionsMod != promolite.m_data.m_nLastConnectionsMod)
					{

//			promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Channel info view has changed, calling GetConnections");   Sleep(10);//(Dispatch message)
						if(promolite.m_data.GetConnections()>=PROMOLITE_SUCCESS)
						{
			promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "GetConnections returned successfully"); //  Sleep(10);//(Dispatch message)
							promolite.m_data.m_nLastConnectionsMod = promolite.m_data.m_nConnectionsMod;
						}
						else
						{
			promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "GetConnections returned an error"); //  Sleep(10);//(Dispatch message)

						}
					}

					if(0)//bAnalysisChanges)  // Promolite does not do any analysis NUCLEUS->PROMOLITE_CHANGE
					{
						// go thru and indicate changes to each channel
						promolite.m_data.m_bForceAnalysis = true;


/*
// moved to a delay thread.
						if(
							  (promolite.m_settings.m_ppEndpointObject)
							&&(promolite.m_data.m_nIndexAutomationEndpoint>=0)
							&&(promolite.m_data.m_nIndexAutomationEndpoint<promolite.m_settings.m_nNumEndpointsInstalled)
							&&(promolite.m_settings.m_ppEndpointObject[promolite.m_data.m_nIndexAutomationEndpoint])
							)
						{
							CPromoLiteEndpointObject* pAutoObj = promolite.m_settings.m_ppEndpointObject[promolite.m_data.m_nIndexAutomationEndpoint];

							if(pAutoObj) pAutoObj->m_nLastModLiveEvents = -1;  // triggers global analysis rules.
// above line now taken care of above by promolite.m_data.m_bForceAnalysis = true;

							if((pAutoObj->m_ppChannelObj)&&(pAutoObj->m_nNumChannelObjects))
							{
								int cho=0;
								while(cho<pAutoObj->m_nNumChannelObjects)
								{
									if(pAutoObj->m_ppChannelObj[cho])
									{
										pAutoObj->m_ppChannelObj[cho]->m_bTriggerEventsChanged = true;
//										pAutoObj->m_ppChannelObj[cho]->m_bNearEventsChanged = true;

									}
									cho++;
								}
							}
						}
* /

						if((g_ppromolite->m_settings.m_bEnableEventTriggerNotification)&&(!g_ppromolite->m_data.m_bDelayingTriggerNotification))
						{
if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_PROCESS) 	
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "  >>* Spawning PromoLiteTriggerDelayNotificationThread from main thread");  // Sleep(50);//(Dispatch message)

							if(_beginthread(PromoLiteTriggerDelayNotificationThread, 0, (void*)NULL)==-1)
							{
								//error.
				promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Error starting trigger delay notification thread");  // Sleep(10);//(Dispatch message)

								//**MSG
							}
						}

					}
*/  // event rules and such are all removed NUCLEUS->PROMOLITE_CHANGE
				}
/*
				if(promolite.m_data.m_nConnectionsMod != promolite.m_data.m_nLastConnectionsMod)
				{
//			promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "getting connections again");   Sleep(250);//(Dispatch message)
					if(promolite.m_data.GetConnections()>=PROMOLITE_SUCCESS)
					{
						promolite.m_data.m_nLastConnectionsMod = promolite.m_data.m_nConnectionsMod;
					}
				}
				if(promolite.m_data.m_nChannelsMod != promolite.m_data.m_nLastChannelsMod)
				{
//			promolite.m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "getting channels again");   Sleep(250);//(Dispatch message)
					if(promolite.m_data.GetChannels()>=PROMOLITE_SUCCESS)
					{
						promolite.m_data.m_nLastChannelsMod = promolite.m_data.m_nChannelsMod;
					}
				}
*/


#ifdef NUCLEUS_PROMOLITE_CHANGE

				// need an automation object just because that's where the trigger buffer is allocated.
				if(g_pChannelObj==NULL)
				{
					promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:trigger_thread_init", "Creating new Channel object.");  //(Dispatch message)
					g_pChannelObj = new CPromoLiteAutomationChannelObject;

					if(g_pChannelObj==NULL)
					{

						//error
						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "PromoLite object creation failure.");  
						promolite.m_data.SetStatusText(errorstring, (PROMOLITE_STATUS_ERROR));
						promolite.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:trigger_thread_init", errorstring);  //(Dispatch message)
						promolite.SendMsg(CX_SENDMSG_ERROR, "PromoLite:trigger_thread_init", errorstring);
					}
				}

				promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:trigger_thread_init", "PromoLite trigger %d %d %d.",
					(!g_bKillThread),(g_pChannelObj),(g_pChannelObj->m_bTriggerThreadStarted == false));  //(Dispatch message)
				if((!g_bKillThread)&&(g_pChannelObj)&&(g_pChannelObj->m_bTriggerThreadStarted == false))
				{
					g_pChannelObj->m_pPromoLite = &promolite;
					g_pChannelObj->m_bKillAutomationThread = false;

					if(!g_bKillThread)
					{
						if(_beginthread(PromoLiteTriggerThread, 0, (void*)g_pChannelObj)==-1)
						{
							//error.  try again once
							Sleep(1000);
							if(!g_bKillThread)
							{
								if(_beginthread(PromoLiteTriggerThread, 0, (void*)g_pChannelObj)==-1)
								{
									//report failure
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "PromoLite trigger thread failure.");  
									promolite.m_data.SetStatusText(errorstring, (PROMOLITE_STATUS_THREAD_ERROR|PROMOLITE_STATUS_ERROR));
									promolite.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:trigger_thread_init", errorstring);  //(Dispatch message)
									promolite.SendMsg(CX_SENDMSG_ERROR, "PromoLite:trigger_thread_init", errorstring);
								}
								else
								{
									promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:trigger_thread_init", "PromoLite trigger thread initialized.");  //(Dispatch message)
								}
							}
						}
						else
						{
							promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:trigger_thread_init", "PromoLite trigger thread initialized.");  //(Dispatch message)
						}
					}
					Sleep(250); // let the thread start
				}
			/// end of previously commented out section NUCLEUS->PROMOLITE_CHANGE
#endif //#ifdef NUCLEUS_PROMOLITE_CHANGE



			}
		}  // check mod interval

		MSG msg;

//		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
//			AfxGetApp()->PumpMessage();


//AfxMessageBox("zoinks");
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();

		Sleep(1); 
//		Sleep(1000);  // stalls window procedure
	}

	promolite.m_data.m_ulFlags &= ~PROMOLITE_STATUS_THREAD_MASK;
	promolite.m_data.m_ulFlags |= PROMOLITE_STATUS_THREAD_END;

	promolite.m_msgr.DM(MSG_ICONINFO, NULL, "PromoLite:uninit", "PromoLite is shutting down.");  //(Dispatch message)
	promolite.SendMsg(CX_SENDMSG_INFO, "PromoLite:uninit", "PromoLite %s is shutting down.", PROMOLITE_CURRENT_VERSION);

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "PromoLite is shutting down.");  
	promolite.m_data.m_ulFlags &= ~CX_ICON_MASK;
	promolite.m_data.m_ulFlags |= CX_STATUS_UNKNOWN;
	promolite.m_data.SetStatusText(errorstring, promolite.m_data.m_ulFlags);



#ifdef NUCLEUS_PROMOLITE_CHANGE
	if(g_pChannelObj)
	{
		g_pChannelObj->m_bKillAutomationThread = true;
		while(g_pChannelObj->m_bTriggerThreadStarted)
		{
			Sleep(10);  // expire this?
		}

		try
		{
			delete g_pChannelObj;
		}
		catch(...)
		{
		}
		g_pChannelObj = NULL;
	}
#endif // #ifdef NUCLEUS_PROMOLITE_CHANGE



	int kt=0;
	BOOL bAllDone = FALSE;
	while ((kt<100)&&(!bAllDone))
/*
					(promolite.m_data.m_bAutomationThreadStarted)
				||(promolite.m_data.m_bNearAnalysisThreadStarted)
				||(promolite.m_data.m_bFarAnalysisThreadStarted)
				||(promolite.m_data.m_bTriggerThreadStarted)
				))
*/

	{

		/*

g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ending %d %d %d %d @ %d", 
											promolite.m_data.m_bAutomationThreadStarted,
											promolite.m_data.m_bNearAnalysisThreadStarted,
											promolite.m_data.m_bFarAnalysisThreadStarted,
											promolite.m_data.m_bTriggerThreadStarted, clock()
											); 
*/
		
		// have to go thru all endpoints until the threads are down.
		
		
		Sleep(50);
		_ftime( &promolite.m_data.m_timebTick );  // we're still alive.
		kt++;
	}



// shut down all the running objects;
/*
	if(promolite.m_data.m_ppConnObj)
	{
		int i=0;
		while(i<promolite.m_data.m_nNumConnectionObjects)
		{
			if(promolite.m_data.m_ppConnObj[i])
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", promolite.m_data.m_ppConnObj[i]->m_pszDesc?promolite.m_data.m_ppConnObj[i]->m_pszDesc:promolite.m_data.m_ppConnObj[i]->m_pszServerName);  

// **** disconnect servers
				promolite.m_data.SetStatusText(errorstring, promolite.m_data.m_ulFlags);
				promolite.m_msgr.DM(MSG_ICONINFO, NULL, "PromoLite:connection_uninit", errorstring);    //(Dispatch message)

				delete promolite.m_data.m_ppConnObj[i];
				promolite.m_data.m_ppConnObj[i] = NULL;
			}
			i++;
		}
		delete [] promolite.m_data.m_ppConnObj;
	}
	if(promolite.m_data.m_ppChannelObj)
	{
		int i=0;
		while(i<promolite.m_data.m_nNumChannelObjects)
		{
			if(promolite.m_data.m_ppChannelObj[i])
			{
				delete promolite.m_data.m_ppChannelObj[i];
				promolite.m_data.m_ppChannelObj[i] = NULL;
			}
			i++;
		}
		delete [] promolite.m_data.m_ppChannelObj;
	}
*/

/*
	// here's a hard coded one for now
	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no need to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = promolite.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		promolite.m_net.OpenConnection(promolite.m_http.m_pszHost, 10888, &s); // branding hardcode
		promolite.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		promolite.m_net.CloseConnection(s); // branding hardcode
	}
*/

//	m_pDlg->SetProgress(PROMOLITEDLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
//	promolite.m_data.m_ulFlags &= ~PROMOLITE_STATUS_FILESVR_MASK;
//	promolite.m_data.m_ulFlags |= PROMOLITE_STATUS_FILESVR_END;
//	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down file server....");  
//	promolite.m_data.SetStatusText(errorstring, promolite.m_data.m_ulFlags);
//	_ftime( &promolite.m_data.m_timebTick );
//	promolite.m_http.EndServer();
	_ftime( &promolite.m_data.m_timebTick );
//	AfxMessageBox("shutting down command server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down command server....");  
	promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:command_server_uninit", errorstring);  //(Dispatch message)
	promolite.m_data.SetStatusText(errorstring, PROMOLITE_STATUS_CMDSVR_END);
	_ftime( &promolite.m_data.m_timebTick );
	promolite.m_net.StopServer(promolite.m_settings.m_usCommandPort, 5000, errorstring);
	_ftime( &promolite.m_data.m_timebTick );
//	AfxMessageBox("shutting down status server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down XML server....");  
	promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:xml_server_uninit", errorstring);  //(Dispatch message)
	promolite.m_data.SetStatusText(errorstring, PROMOLITE_STATUS_STATUSSVR_END);
	_ftime( &promolite.m_data.m_timebTick );
	promolite.m_net.StopServer(promolite.m_settings.m_usStatusPort, 5000, errorstring);
	_ftime( &promolite.m_data.m_timebTick );
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "PromoLite is exiting.");  
	promolite.m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:uninit", errorstring);  //(Dispatch message)
	promolite.m_data.SetStatusText(errorstring, promolite.m_data.m_ulFlags);



	// save settings.  // dont save them here.  save them on any changes in the main command loop.
	promolite.m_settings.Settings(false); //write
/*

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// these explicts arent necessary - uncomment to write out a full file to edit...
		file.SetIniString("Main", "Name", promolite.m_settings.m_pszName);
		file.SetIniString("License", "Key", promolite.m_settings.m_pszLicense);

		file.SetIniString("FileServer", "IconPath", promolite.m_settings.m_pszIconPath);
		file.SetIniInt("CommandServer", "ListenPort", promolite.m_settings.m_usCommandPort);
		file.SetIniInt("StatusServer", "ListenPort", promolite.m_settings.m_usStatusPort);

		file.SetIniInt("Messager", "UseEmail", promolite.m_settings.m_bUseEmail?1:0);
		file.SetIniInt("Messager", "UseNet", promolite.m_settings.m_bUseNetwork?1:0);
		file.SetIniInt("Messager", "UseLog", promolite.m_settings.m_bUseLog?1:0);
		file.SetIniInt("Messager", "ReportSuccessfulOperation", promolite.m_settings.m_bReportSuccessfulOperation?1:0);
		file.SetIniInt("Messager", "LogTransfers", promolite.m_settings.m_bLogTransfers?1:0);

		file.SetIniString("Database", "DSN", promolite.m_settings.m_pszDSN);
		file.SetIniString("Database", "DBUser", promolite.m_settings.m_pszUser);
		file.SetIniString("Database", "DBPassword", promolite.m_settings.m_pszPW);
		file.SetIniString("Database", "SettingsTableName", promolite.m_settings.m_pszSettings);  // the Settings table name
		file.SetIniString("Database", "ExchangeTableName", promolite.m_settings.m_pszExchange);  // the Exchange table name
		file.SetIniString("Database", "MessagesTableName", promolite.m_settings.m_pszMessages);  // the Messages table name

//		file.SetIniInt("HarrisAPI", "UseListCount", promolite.m_settings.m_bUseListCount?1:0); // get all events up until the list count (otherwise just up to the lookahead)

//		file.SetIniString("Database", "ChannelsTableName", promolite.m_settings.m_pszChannels);  // the Channels table name
//		file.SetIniString("Database", "ConnectionsTableName", promolite.m_settings.m_pszConnections);  // the Connections table name
		file.SetIniString("Database", "EventsTableName", promolite.m_settings.m_pszEvents);  // the LiveEvents table name
		file.SetIniString("Database", "QueueTableName", promolite.m_settings.m_pszQueue);  // the Queue table name
		file.SetIniString("Database", "RulesTableName", promolite.m_settings.m_pszRules);  // the Rules table name
				file.SetIniString("Database", "AnalysisViewName", g_ppromolite->m_settings.m_pszTriggerAnalysisView);  // the TriggerAnalysisView name
				file.SetIniString("Database", "AnalyzedTriggerDataTableName", g_ppromolite->m_settings.m_pszAnalyzedTriggerData);  // the AnalyzedTriggerData table name
				file.SetIniString("Database", "AnalyzeRulesProcedureName", g_ppromolite->m_settings.m_pszAnalyzeRulesProc);  // the spAnalyzeRules stored procedure
				file.SetIniString("Database", "GetParameterValueProcedureName", g_ppromolite->m_settings.m_pszGetParameterValueProc);  // the spGetParameterValue stored procedure

		file.SetIniInt("Database", "ModificationCheckInterval", promolite.m_settings.m_ulModsIntervalMS);  // in milliseconds
//		file.SetIniInt("FileHandling", "TransferFailureRetryTime", promolite.m_settings.m_nTransferFailureRetryTime);  // in seconds
//		file.SetIniInt("FileHandling", "AutoDeleteOldest", promolite.m_settings.m_bAutoDeleteOldest?1:0);
//		file.SetIniInt("FileHandling", "DeletionThreshold", promolite.m_settings.m_ulDeletionThreshold );
			file.SetIniInt("FileHandling", "TriggerAdvanceMS", promolite.m_settings.m_nTriggerAdvanceMS);// number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)

			file.SetIniString("Demo", "DefaultProject", promolite.m_settings.m_pszDefaultProject);
			file.SetIniString("Demo", "DefaultScene", promolite.m_settings.m_pszDefaultScene);
			file.SetIniString("Demo", "DefaultHost", promolite.m_settings.m_pszDefaultHost);

		file.SetIniString("FileHandling", "SystemFolder", promolite.m_settings.m_pszSystemFolderPath);  // must have trailing slash
//		file.SetIniString("Imagestore", "NullOXTFile", promolite.m_settings.m_pszImagestoreNullFile );  // name of the null file, must be in system folder
//		file.SetIniString("Imagestore", "IntuitionSearchExt", promolite.m_settings.m_pszIntuitionSearchExt); // search extensions for Intuition
//		file.SetIniString("Imagestore", "Imagestore2SearchExt", promolite.m_settings.m_pszImagestore2SearchExt);			// search extensions for IS2
//		file.SetIniString("Imagestore", "Imagestore300SearchExt", promolite.m_settings.m_pszImagestore300SearchExt);		// search extensions for IS300
//		file.SetIniString("Imagestore", "ImagestoreHDSearchExt", promolite.m_settings.m_pszImagestoreHDSearchExt);		// search extensions for ISHD

		// gfx module
		file.SetIniString("Graphics", "Host", promolite.m_settings.m_pszGfxHost);  // must have trailing slash; 
		file.SetIniInt("Graphics", "Port", promolite.m_settings.m_nGfxPort); 

		file.SetSettings(PROMOLITE_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

	}

*/
	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "PromoLite is exiting");
	promolite.m_data.m_ulFlags &= ~CX_ICON_MASK;
	promolite.m_data.m_ulFlags |= CX_STATUS_UNINIT;
	promolite.m_data.SetStatusText(errorstring, promolite.m_data.m_ulFlags);

	//exiting
	promolite.m_msgr.DM(MSG_ICONINFO, NULL, "PromoLite", "-------------- PromoLite %s exit ---------------\n\
--------------------------------------------------\n", PROMOLITE_CURRENT_VERSION);  //(Dispatch message)
///	m_pDlg->SetProgress(PROMOLITEDLG_CLEAR); // no point

	_ftime( &promolite.m_data.m_timebTick );
	promolite.m_data.m_ulFlags &= ~PROMOLITE_STATUS_THREAD_MASK;
	promolite.m_data.m_ulFlags |= PROMOLITE_STATUS_THREAD_ENDED;

	g_bKillStatus = true;

	Sleep(250); // let the message get there.
	promolite.m_msgr.RemoveDestination("log");
	promolite.m_msgr.RemoveDestination("email");

	g_ppromolite = NULL;
	db.RemoveConnection(pdbConn);

	
	int nClock = clock() + 300; // small delay at end
	while(clock()<nClock)	{_ftime( &promolite.m_data.m_timebTick );}
	g_bThreadStarted = false;
	g_ppromolite = NULL;
	nClock = clock() + promolite.m_settings.m_nThreadDwellMS; // another small delay at end
	while(clock()<nClock)	{_ftime( &promolite.m_data.m_timebTick );}
	Sleep(300); //one more small delay at end
	_endthread();
}

/*
void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((g_pmsgr)&&(g_pmsgr->m_lpfnDM))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		CMessagingObject msg;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = msg.EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = msg.EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = msg.EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch;
		pch = (char*) malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
* /
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if(pszEncodedMsg) 
			{
				ulBufferLen = strlen(pszEncodedMsg);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedCaller)
			{
				ulBufferLen = strlen(pszEncodedCaller);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedDest)
			{
				ulBufferLen = strlen(pszEncodedDest);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			g_pmsgr->m_lpfnDM(pch, ulFlags);
			free(pch);
		}
	}
}
*/


/*
// this thread is based on void HTTP10HandlerThread(void* pvArgs) from
// HTTP10.cpp.  It makes a special webserver with security, cgi parsing, and dynamic results
void PromoLiteHTTPThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return;}

	CPromoLiteMain* pPromoLite = (CPromoLiteMain*)(pClient->m_lpObject) ;  // pointer to the global object.

// HTTP service by def is a non-persistent service, so even if the Client type is
// set to persist, we ignore that promoliteive in this handler thread

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;


		CHTTP10* phttp = (CHTTP10*)(&(pPromoLite->m_http)) ;  // pointer to the global object.
		if(phttp == NULL)// cant do anything!
		{
			shutdown(pClient->m_socket, SD_BOTH);
			closesocket(pClient->m_socket);
			(*(pClient->m_pulConnections))--;
			_endthread(); return;
		}

		CHTTP10 http(phttp->m_pszRoot);  // local object for utility functions ONLY.
		// this allows the local to clone the root setting of the global, 
		// in order to do correct parsing in its own thread

		// the following are just pointers to the global obj.  
		// must NULL these out before object gets destroyed, 
		// so that global resources not freed!   !!!!!!!  important !!!!!
		http.m_pszHost = phttp->m_pszHost;
		http.m_ppszErrors = phttp->m_ppszMime;
		http.m_ppszMime = phttp->m_ppszMime;   
		http.m_ulStatus = phttp->m_ulStatus;   // important for repromolite and error
		
		unsigned char* pch;// for use with GetLine().
		unsigned long ulBufLen;// for use with GetLine().
		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		unsigned short usCode = 0;

		// get the raw buffer and process it yourself
		nReturn = http.m_net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_EOLN|EOLN_HTTP, pszStatus);
		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). 
			//was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
		{
			//error.
			if(pClient->m_lpMsgObj)
			{
				_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error receiving data.  %s", pszStatus);
				((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
			}

			usCode = 400;  //  we TRY to send this....
			if(pch!=NULL)
			{
				free(pch);  // must free the incoming data buffer 
				pch = NULL;
			}
			// create a return message buffer.
			pch = (unsigned char*)http.ErrorBuffer(NULL, usCode);   // 400 bad request
			ulBufLen = strlen((char*)pch); // valid from an error page.

			// send malformed request answer if possible  - have to tell the client that the request was not received in good order.
			nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
			if(nReturn<NET_SUCCESS)
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
				}
			}
		}
		else  // successful reception of data.
		{
			// process the data here...
			// in this sample the buffer is loaded into
			// pch, with ulBufLen as length

/*
			FILE* fp = fopen("rec.txt", "ab");
			if (fp)
			{
				fwrite(pch, 1, ulBufLen, fp );
				fflush(fp);
				fclose(fp);
			}
* /
			// parse buffer, format reply.
			CHTTPHeader header;

			if (http.ParseHeader(pch, ulBufLen, &header) < HTTP_SUCCESS)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("header not parsed\r\n\r\n", 1, strlen("header not parsed\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}
* /
				
				if(pch!=NULL) 
				{
					free(pch);  // must free the incoming data buffer 
					pch = NULL;
				}
				// create a return message buffer.
				usCode = 400;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}
			else
			{
				// here we have enough info to check security if desired, and if not, return a 401;
				// else continue....
/*
					FILE* fp = fopen("rec.txt", "ab");
					if (fp)
					{// lets write out the whole header so we can examine its full contents. including args.
						char bufferin[10000];
						sprintf(bufferin,
"header successfully parsed\r\n\
method: %d\r\n\
version: %s\r\n\
URL: %s\r\n\
Path: %s\r\n\
mimetype: %s\r\n\
user: %s\r\n\
password: %s\r\n\
num args: %ld\r\n",
	header.m_ucMethod,  // enumerated values
	header.m_pszVersion==NULL?"(null)":header.m_pszVersion,
	header.m_pszURL==NULL?"(null)":header.m_pszURL,
	header.m_pszPath==NULL?"(null)":header.m_pszPath,
	header.m_pszMimeType==NULL?"(null)":header.m_pszMimeType,
	header.m_pszUser==NULL?"(null)":header.m_pszUser,
	header.m_pszPassword==NULL?"(null)":header.m_pszPassword,
	header.m_ulNumArgs

);
		
	unsigned long k=0;
	while(k<header.m_ulNumArgs)
	{
		if((header.m_ppszArgNames)&&(header.m_ppszArgValues))
		{
			strcat(bufferin, "  ");
			strcat(bufferin, header.m_ppszArgNames[k]==NULL?"(null)":header.m_ppszArgNames[k]);
			strcat(bufferin, "=");
			strcat(bufferin, header.m_ppszArgValues[k]==NULL?"(null)":header.m_ppszArgValues[k]);
			strcat(bufferin, "\r\n");
		}
	k++;
	}

	strcat(bufferin, "headers:\r\n");
	strcat(bufferin, header.m_pszHeaders);
	strcat(bufferin, "\r\n");


			//		fwrite("header successfully parsed\r\n", 1, strlen("header successfully parsed\r\n"), fp );
						fwrite(bufferin, 1, strlen(bufferin), fp );
						fflush(fp);
						fclose(fp);
					}

* /


				if((phttp->m_pSecure)&&((header.m_pszUser==NULL)||(header.m_pszPassword==NULL)))
				{
					usCode = 401;
					pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
					ulBufLen = strlen((char*)pch); // valid from an error page.
					// buffer successfully parsed.
				}
				else 
				{
					if(pch!=NULL)
					{
						free(pch);  // must free the incoming data buffer 
						pch = NULL;
					}

					// create a return message buffer.
					switch(header.m_ucMethod)
					{
					case HTTP_GET://			1  // HTTP/1.0
					case HTTP_HEAD://			2  // HTTP/1.0
						{
							if(
									(strnicmp(header.m_pszPath, http.m_pszRoot, strlen(http.m_pszRoot)) == 0 )  // else not a part of the served file tree
								)
							{
								if(phttp->m_pSecure)
								{
									// have to check the main object for permission.
									// we are going to go by URL (asset level)
									// Loci are going to just be convenient names for the admins to set up.

									if((phttp->m_pSecure->CheckSecure(header.m_pszUser, header.m_pszPassword, NULL, header.m_pszURL))!=SECURE_SUCCESS)
									{
										usCode = 401;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
										ulBufLen = strlen((char*)pch); // valid from an error page.
										break; // get out of the switch from here.
									}
									// else it flows thru....
								}

								DWORD dwAttrib;

								dwAttrib = GetFileAttributes(header.m_pszPath);

								// check for promoliteory (repromolite nec.)
								if ( (dwAttrib!=0xffffffff)&&(dwAttrib&FILE_ATTRIBUTE_PROMOLITEORY) )  // if error, it may be because no file exists, so check in the next part.
								{
									usCode = 301;  // not necessary but why not.
									pch = (unsigned char*)http.RepromoliteBuffer(&header);
									ulBufLen = strlen((char*)pch); // valid from an repromolite page.
								}
								else
								{
									// before we try to open the file, lets spend the time now (not while the file is open)
									// to figure out how long the static text respone buffer will be.
									CBufferUtil bu;
									// notice the sample "Server: PromoLiteServer/3.0.3.1" header included.  Pick the appname and ver, probably best
									unsigned long ulBufferLen = strlen("HTTP/1.0 200 OK\r\nServer: PromoLiteServer/\r\nContent-Length: \r\n\r\n");
									ulBufferLen += strlen(PROMOLITE_CURRENT_VERSION);

									if((header.m_pszMimeType)&&(strlen(header.m_pszMimeType)>0))
									{
										ulBufferLen += (strlen("Content-Type: \r\n") + strlen(header.m_pszMimeType)); // only add if known!
									}
									else // empty!
									{
										// dont free the "permanent resource", just null the pointer
										header.m_pszMimeType=NULL;
									}

									FILE* pFile = NULL;

									pFile = fopen(header.m_pszPath, "rb");
									if (pFile == NULL)  // file not openable, or not found.
									{
										usCode = 404;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 404 not found
										ulBufLen = strlen((char*)pch); // valid from an error page.
									}
									else
									{
										// this part just assembles the file into a buffer.
										// this may be modified to do dynamic html things by using the 
										// pClient->m_lpObject that refers back to some parser....

										usCode = 200; // OK  (!)
										// determine file size
										fseek(pFile, 0, SEEK_END);
										unsigned long ulFileLen = ftell(pFile);


										// here we have to parse out any promoliteives.  if this is a template file,
										// we may have a variable length.
									//	x


										if(header.m_ucMethod == HTTP_GET)
										{
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += (strlen(length)+ulFileLen);

											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you truncations at zeroed bytes

											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: PromoLiteServer/%s", PROMOLITE_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
											
											fclose(pFile);
										}
										else
										if(header.m_ucMethod == HTTP_HEAD)
										{
											fclose(pFile);  // we have all we need already.
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += strlen(length);
											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											//  create the header
											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: PromoLiteServer/%s", PROMOLITE_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												// its identical except for the body.  next line must stay commented out, its just there for ref.
												//fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
										}
									}
								}
							}
							else  // not part of the server service.
							{
								usCode = 403;
								pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 403 forbidden ...
								ulBufLen = strlen((char*)pch); // valid from an error page.
							}

						} break;
					case HTTP_POST://			3  // HTTP/1.0
						{
							usCode = 501;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 501 not implemented ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_PUT://			4  // HTTP/1.1
					case HTTP_DELETE://		5  // HTTP/1.1
					case HTTP_OPTIONS://  6  // HTTP/1.1
					case HTTP_TRACE://	  7  // HTTP/1.1
						{
							usCode = 505;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 505 http version not supported ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_UNDEF://		0  // internal
					default:
						{
							usCode = 400;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					}
				}
			}

			// send a reply, in pch., with strlen pch as length

			if(pch == NULL)  // had a problem forming the request, try to send a last ditch error code.
			{
				usCode = 500;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 500 internal server error
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}

			if(pch != NULL)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("sending:\r\n", 1, strlen("sending:\r\n"), fp );
					fwrite(pch, 1, ulBufLen, fp );
					fwrite("\r\n\r\n", 1, strlen("\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}

* /
				nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
				if(nReturn<NET_SUCCESS)
				{
					//error.
					if(pClient->m_lpMsgObj)
					{
						_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
						((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
					}
				}
			}
		}

//		char foo[34]; sprintf(foo, "code: %d", usCode);	AfxMessageBox(foo);

		http.m_pszHost = NULL;		// but do NOT free resource
		http.m_ppszErrors = NULL; // but do NOT free resource
		http.m_ppszMime = NULL;   // but do NOT free resource

		if(pch!=NULL) free(pch);  // must free the data buffer 

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}
*/


void PromoLiteCommandHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure in Protocol 1  //SECURE with PROTOCOL2 later
			strcpy(pszInfo, "");  // clear it
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error %d receiving data.  %s", nReturn, pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					strcpy(pszInfo, "");  // clear it
					nReturn = net.SendData(&data, pClient->m_socket, 2000, 0, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error %d sending NAK reply.  %s", nReturn, pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
						// we had an error receiving data and then anotehr trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{
//AfxMessageBox("receiving");

					// parse the command:

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					switch(data.m_ucCmd)
					{
					case CX_CMD_GETINFO: // not actually supported yet.  everything thru csf files for now.
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case PROMOLITE_CMD_EXSET://							0xea // sets exchange counter to a specific value
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case PROMOLITE_CMD_EXINC://							0xeb // increments exchange counter
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)
							{	
								data.m_ucSubCmd = (unsigned char) g_ppromolite->m_data.IncrementDatabaseMods((char*)data.m_pucData);
								free(data.m_pucData);  //destroy the buffer;
							}
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.

							data.m_ucType = NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case PROMOLITE_CMD_EXGET://							0xec // gets exchange mod value
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case PROMOLITE_CMD_MODSET://							0xed // sets exchange mod value, skips exchange table
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case PROMOLITE_CMD_MODINC://							0xee // increments exchange mod value, skips exchange table
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)
							{
							// *****************
							// only supporting the one incrementer.
if(g_ppromolite->m_settings.m_ulDebug&(PROMOLITE_DEBUG_COMM)) 
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:command_handler", "received MODINC: %s", (char*)data.m_pucData); // Sleep(250); //(Dispatch message)
								if(stricmp((char*)data.m_pucData, g_ppromolite->m_settings.m_pszTriggerAnalysisView)==0) 
								{
									g_pChannelObj->m_bTriggerEventsChanged = true;
									// is there a mod for this?
									// there isnt.
if(g_ppromolite->m_settings.m_ulDebug&(PROMOLITE_DEBUG_COMM)) 
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:command_handler", "Trigger events have changed!"); // Sleep(250); //(Dispatch message)

									try
									{
										free(data.m_pucData);  //destroy the buffer;
									}
									catch(...)
									{
									}
									data.m_ucCmd = NET_CMD_NAK;
									data.m_pucData=(unsigned char*)malloc(64);
									strcpy((char*)data.m_pucData, "0");
									if(data.m_pucData)
									{
										data.m_ulDataLen = strlen((char*)data.m_pucData);
										data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.
									}
									else
									{
										data.m_ulDataLen = 0;
										data.m_ucType = NET_TYPE_PROTOCOL1; // has data but no subcommand.
									}

								}
								else
								{
									try
									{
										free(data.m_pucData);  //destroy the buffer;
									}
									catch(...)
									{
									}
									data.m_ucCmd = NET_CMD_NAK;
									data.m_pucData=NULL;
									data.m_ulDataLen = 0;
									data.m_ucType = NET_TYPE_PROTOCOL1; // has data but no subcommand.
								}
							}
							else
							{ //error
								data.m_ucCmd = NET_CMD_NAK;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								data.m_ucType = NET_TYPE_PROTOCOL1; // has data but no subcommand.
							}
							


						} break;


					case CX_CMD_GETSTATUS: //	0x99 // gets status info
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							unsigned long ulStatus;
							// assemble if there is 

							char* pch = g_ppromolite->m_data.GetStatusText(&ulStatus);  // allocates mem, must free after use
							if(pch)
							{
								data.m_ucSubCmd = (unsigned char)(PROMOLITE_ICON_MASK&ulStatus);
								data.m_pucData=(unsigned char*)pch;
								data.m_ulDataLen = strlen(pch);
							}
							else
							{
								data.m_ucCmd = NET_CMD_NAK;
								data.m_ucSubCmd = NET_CMD_NAK;
							}

							data.m_ucType = NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case CX_CMD_BYE:
						{
	g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLite", "The Global Kill command has been received.");  //(Dispatch message)

							bSetGlobalKill = true;
							g_ppromolite->m_data.m_bQuietKill = true;
							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					default:
						{
							//just ack
							data.m_ucCmd = NET_CMD_ACK;

		//					char repbuf[64];
							if(data.m_pucData!=NULL)
							{
		/*
								_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

								for(unsigned long q=0; q<data.m_ulDataLen; q++)
								{
									if(strlen(repbuf)<63)
										strncat(repbuf, (char*)(data.m_pucData+q), 1);
								}
								if(strlen(repbuf)<63)
									strcat(repbuf, "]");
								data.m_ulDataLen = strlen(repbuf);
								repbuf[data.m_ulDataLen] = 0;  // just in case
		*/
								free(data.m_pucData);  //destroy the buffer;
							}
		/*
							else
							{
								_snprintf(repbuf, 63, "data was NULL");
								data.m_ulDataLen = strlen(repbuf); 
							}

							data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
							if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
							else 
							{
								data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
								data.m_ulDataLen = 0;
							}
		*/
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					}

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "PromoLite:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "PromoLite:CommandHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.
	if (bSetGlobalKill)	//g_bKillThread = true;
	((CPromoLiteHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}

void PromoLiteStatusHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "PromoLite:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "PromoLite:CommandHandlerThread");
						}
						// we had an error receiving data and then anotehr trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "PromoLite:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "PromoLite:CommandHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}


int CPromoLiteMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)&&(strlen(m_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

		if(m_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime) \
VALUES ('%s', '%s', %d, %d.%03d)", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				m_settings.m_pszMessages
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_data.m_critSQL);
		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_data.m_critSQL);
			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return PROMOLITE_SUCCESS;
		}
		LeaveCriticalSection(&m_data.m_critSQL);

	}
	return PROMOLITE_ERROR;
}

int CPromoLiteMain::SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...)
{
 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszAsRun)&&(strlen(m_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		if((pszEventID)&&(strlen(pszEventID)>SENDMSG_EVENT_MAXLEN)) pszEventID[SENDMSG_EVENT_MAXLEN-1]=0;
		
		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);
		char* pchEvent = m_data.m_pdb->EncodeQuotes(pszEventID);
		if(m_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, channelid, eventid) \
VALUES ('%s', '%s', %d, %d.%03d, %d, '%s')", // HARDCODE
				(m_settings.m_pszAsRun?m_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm,
				nChannelID,
				pchEvent?pchEvent:""
				);
		}
		else
		{

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				(m_settings.m_pszAsRun?m_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				(m_settings.m_pszAsRun?m_settings.m_pszAsRun:"AsRun_Log")
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
		if(pchEvent) free(pchEvent);

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_data.m_critSQL);
		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_data.m_critSQL);
			m_data.IncrementDatabaseMods(m_settings.m_pszAsRun, dberrorstring);
			return PROMOLITE_SUCCESS;
		}
		LeaveCriticalSection(&m_data.m_critSQL);

	}
	return PROMOLITE_ERROR;
}



/*
int CPromoLiteMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)&&(strlen(m_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		unsigned long ulID = 0;
		// get the highest ID#;
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT MAX(id) AS MAX FROM %s", 
			m_settings.m_pszMessages			);

		CRecordset* prs = m_data.m_pdb->Retrieve(m_data.m_pdbConn, szSQL, dberrorstring);
		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				CString szValue;
				prs->GetFieldValue("MAX", szValue);
				unsigned long ulLastID = atol(szValue);
				ulID = ulLastID+1;
			}
			prs->Close();
			delete prs;
		}

		if(ulID>0)
		{
			va_list marker;
			// create the formatted output string
			va_start(marker, pszMessage); // Initialize variable arguments.
			_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
			va_end( marker );             // Reset variable arguments.
			_timeb timestamp;
			_ftime( &timestamp );

			char* pchMessage = m_data.m_pdb->EncodeQuotes(m_settings.m_pszMessages);
			char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) VALUES ('%s', '%s', %d, %d, %d)", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				ulID
				);

			if(pchMessage) free(pchMessage);
			if(pchSender) free(pchSender);

			if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
			{
				m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
				return PROMOLITE_SUCCESS;
			}
		}
	}
	return PROMOLITE_ERROR;
}

/*
void PromoLiteConnectionThread(void* pvArgs)
{
			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "PromoLiteConnectionThread");  Sleep(250); //(Dispatch message)
	CPromoLiteConnectionObject* pConn = (CPromoLiteConnectionObject*) pvArgs;
	if(pConn==NULL) 
	{
		//**MSG
		return;
	}
	pConn->m_bConnThreadStarted = true;
	unsigned long ulTick = GetTickCount();
	unsigned long ulElapsedTick = GetTickCount();
			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "PromoLiteConnectionThread2");  Sleep(250); //(Dispatch message)
	while(!pConn->m_bKillConnThread)
	{

		if(	pConn->m_pAPIConn )
		{
			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "getting status");  Sleep(250); //(Dispatch message)
			// we can monitor the connection and get status
			EnterCriticalSection(&g_adc.m_crit);
			g_adc.GetStatusData(pConn->m_pAPIConn);
			LeaveCriticalSection(&g_adc.m_crit);
			ulTick = GetTickCount();

			if(pConn->m_pAPIConn->m_Status > 0)  // connected
			{
				// here we need to monitor list states and threads
				int nChannels = 0;

				while(nChannels<g_ppromolite->m_data.m_nNumChannelObjects)
				{
					if((g_ppromolite->m_data.m_ppChannelObj)&&(g_ppromolite->m_data.m_ppChannelObj[nChannels]))
					{
						if(strcmp(g_ppromolite->m_data.m_ppChannelObj[nChannels]->m_pszServerName, pConn->m_pszServerName)==0) // we are on the right adc conn.
						{
							//if the list is active, check to see if there is a thread going.
							if(g_ppromolite->m_data.m_ppChannelObj[nChannels]->m_ulFlags&PROMOLITE_FLAG_ENABLED)
							{
								if(
									  (g_ppromolite->m_data.m_ppChannelObj[nChannels]->m_nHarrisListID<0)
									||(g_ppromolite->m_data.m_ppChannelObj[nChannels]->m_nHarrisListID>=MAXSYSLISTS)
									)
								{
									// error!
									g_ppromolite->m_data.m_ppChannelObj[nChannels]->m_ulFlags &= ~PROMOLITE_FLAG_ENABLED;  //disable

								}
								else
								if( !g_ppromolite->m_data.m_ppChannelObj[nChannels]->m_bChannelThreadStarted)
								{ // start the thread!
									g_ppromolite->m_data.m_ppChannelObj[nChannels]->m_bKillChannelThread=false;
									g_ppromolite->m_data.m_ppChannelObj[nChannels]->m_pAPIConn=pConn->m_pAPIConn;
									g_ppromolite->m_data.m_ppChannelObj[nChannels]->m_pbKillConnThread=&(pConn->m_bKillConnThread);

			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "beginning list thread");  Sleep(250); //(Dispatch message)
									if(_beginthread(PromoLiteListThread, 0, (void*)g_ppromolite->m_data.m_ppChannelObj[nChannels])==-1)
									{
										//error.
							
			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "problem beginning list thread");  Sleep(250); //(Dispatch message)
										//**MSG
									}
								}
							}
							else  // disabled
							{
								if( g_ppromolite->m_data.m_ppChannelObj[nChannels]->m_bChannelThreadStarted)
								{
									// end the thread.
			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ending list thread");  Sleep(250); //(Dispatch message)
									g_ppromolite->m_data.m_ppChannelObj[nChannels]->m_bKillChannelThread=true;
								}
							}
						}
					}
					nChannels++;
				}
			}
			else
			{
				g_adc.DisconnectServer(pConn->m_pAPIConn);  // just removes from adc list.
				pConn->m_ulStatus = PROMOLITE_STATUS_ERROR;  // due to disconnection.  resets on connect
				pConn->m_pAPIConn = NULL; // break out.
			}

			if(pConn->m_pAPIConn)
			{
				ulElapsedTick = GetTickCount();
				if(ulElapsedTick<ulTick) //wrapped
				{
					Sleep(((pConn->m_pAPIConn->m_SysData.systemfrx) == 0x24)?40:33);
				}
				else
				{
					ulElapsedTick -= ulTick;
					Sleep(((pConn->m_pAPIConn->m_SysData.systemfrx) == 0x24)?40:33 - ulElapsedTick);
				}
			}
			//else break out and try to reconnect.
		}
		else
		{
			if(pConn->m_ulFlags&PROMOLITE_FLAG_ENABLED) // active.
			{
				if((pConn->m_pszClientName)&&(strlen(pConn->m_pszClientName)<=0)) 
				{
					free(pConn->m_pszClientName);
					pConn->m_pszClientName = NULL;
				}
				if(!pConn->m_pszClientName)
				{
					pConn->m_pszClientName = (char*)malloc(strlen("PromoLite")+1);
					if(pConn->m_pszClientName) strcpy(pConn->m_pszClientName, "PromoLite");
				}
					
				CAConnection* pAPIConn = g_adc.ConnectServer(pConn->m_pszServerName, pConn->m_pszClientName);
				if(pAPIConn)
				{
					pConn->m_pAPIConn = pAPIConn; // connected.
					pConn->m_ulStatus = PROMOLITE_STATUS_CONN;
				}
			} // else don't connect, just 
			else
			{
				pConn->m_ulStatus = PROMOLITE_STATUS_NOTCON;
			}

			Sleep(2000);// wait two seconds after a connection attempt;
		}
			
	}

	g_adc.DisconnectServer(pConn->m_pszServerName);

	pConn->m_bConnThreadStarted = false;
	_endthread();
}

void PromoLiteListThread(void* pvArgs)
{
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "PromoLiteChannelThread");  Sleep(250); //(Dispatch message)
	CPromoLiteChannelObject* pChannel = (CPromoLiteChannelObject*) pvArgs;
	if(pChannel==NULL)
	{
		//**MSG
		return;
	}
	bool* pbKillConnThread = pChannel->m_pbKillConnThread;
	if(pbKillConnThread==NULL)  // the address, not what it points to
	{
		//**MSG
		return;
	}
	if(	pChannel->m_pAPIConn ) // need this to do anything.  // could get it by looping through g_adc.... later.
	{
		//**MSG
		return;
	}
	pChannel->m_bChannelThreadStarted = true;


	// lets set up some convenience vars;
	int nZeroList = pChannel->m_nHarrisListID-1;


	if((nZeroList<0)||(nZeroList>=MAXSYSLISTS)) // error on list number.
	{
		//**MSG
		return;
	}

	tlistdata* plistdata = &(pChannel->m_pAPIConn->m_ListData[nZeroList]);
	TConnectionStatus* pstatus =  &(pChannel->m_pAPIConn->m_Status);
	CAList** ppList = &(pChannel->m_pAPIConn->m_pList[nZeroList]);
	CAList*  pList = pChannel->m_pAPIConn->m_pList[nZeroList];  // volatile, always reassign
	CDBUtil* pdb = g_ppromolite->m_data.m_pdb;
	CDBconn* pdbConn = g_ppromolite->m_data.m_pdbConn;

	pChannel->m_pAPIConn->m_bListActive[nZeroList] = true;
//	unsigned long ulTick = GetTickCount();
//	unsigned long ulElapsedTick = GetTickCount();
			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "PromoLiteChannelThread2");  Sleep(250); //(Dispatch message)

	tlistdata listdata;
	tlistdata lastlistdata;  // for comparison

  lastlistdata.listchanged=0;				// incremented whenever list size changes
  lastlistdata.listdisplay=0;				// incremented whenever an event changes
  lastlistdata.listsyschange=0;			// incremented when list related values are changed. 
  lastlistdata.listcount=0;					// number of events in list 
  lastlistdata.liststate=0;
			
	while((!pChannel->m_bKillChannelThread)&&((*pbKillConnThread)!=true))
	{
//		EnterCriticalSection(&g_adc.m_crit);
//		g_adc.GetStatusData(pConn->m_pAPIConn);
//		LeaveCriticalSection(&g_adc.m_crit);
		if(*pstatus > 0)  // have to be connected.
		{
			// snapshot the live list data
			EnterCriticalSection(&g_adc.m_crit);
			memcpy(&listdata, plistdata, sizeof(tlistdata));
			LeaveCriticalSection(&g_adc.m_crit);

			if((listdata.listchanged!=lastlistdata.listchanged)||(listdata.listdisplay!=lastlistdata.listdisplay)) // just do the event chage one as well for now.
			{
				// the list has changed, need to get all the events again  (will take care of any event changes as well)
				int nNumEvents = g_adc.GetEvents(pChannel->m_pAPIConn,
					pChannel->m_nHarrisListID, 
					0, //start at the top.
					g_ppromolite->m_settings.m_bUseListCount?listdata.listcount:listdata.lookahead);

				EnterCriticalSection(&(pList->m_crit));  // lock the list out
				pList = (*ppList);  // volatile, must reassign
				if((nNumEvents>=ADC_SUCCESS)&&(pList))
				{
					// deal with database.
					if((pdb!=NULL)&&(pdbConn!=NULL)&&(g_ppromolite->m_settings.m_pszLiveEvents)&&(strlen(g_ppromolite->m_settings.m_pszLiveEvents)>0))
					{
						char dberrorstring[DB_ERRORSTRING_LEN];
						char szSQL[DB_SQLSTRING_MAXLEN];

						double dblLastPrimaryEventTime = -1.0;
						unsigned short usLastPrimaryOnAirJulianDate = 0xffff;
						unsigned long ulLastPrimaryOnAirTimeMS = 0xffffffff;
						double dblServerTime = (double)pChannel->m_pAPIConn->m_usRefJulianDate - 25567.0   // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
										+ (double)pChannel->m_pAPIConn->m_ulRefTimeMS/1000.0;

						int n=0;
						while(n<nNumEvents)
						{

							if((pList->m_ppEvents)&&(pList->m_ppEvents[n])&&(pList->m_ppEvents[n]->m_pszID)&&(strlen(pList->m_ppEvents[n]->m_pszID)))
							{

								double dblEventTime = 0.0; 
								unsigned short usOnAirJulianDate = pList->m_ppEvents[n]->m_usOnAirJulianDate;

								if(usOnAirJulianDate == 0xffff)
								{
									_timeb timestamp;
									_ftime(&timestamp);

									usOnAirJulianDate = pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.

									dblEventTime = (double)usOnAirJulianDate - 25567.0   // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
										+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
								}
								else
								{
									dblEventTime = (double)pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567.0   // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
										+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
								}
									  

								unsigned short usType = pList->m_ppEvents[n]->m_usType;

								if(usType&SECONDARYEVENT)
								{
									if(ulLastPrimaryOnAirTimeMS>0.0)
									{
										dblEventTime = dblLastPrimaryEventTime 
											+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
										usOnAirJulianDate = usLastPrimaryOnAirJulianDate;
										if(pList->m_ppEvents[n]->m_ulOnAirTimeMS + ulLastPrimaryOnAirTimeMS >= 86400000 )
										{
											//wrapped days.
											usOnAirJulianDate++;
										}
									}
								}
								else
								{
									ulLastPrimaryOnAirTimeMS = pList->m_ppEvents[n]->m_ulOnAirTimeMS;
									dblLastPrimaryEventTime = dblEventTime;
									usLastPrimaryOnAirJulianDate = usOnAirJulianDate;
								}

//						select event with server, list, start time +/- tolerance, and clip ID
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s WHERE \
server_name = '%s' AND \
server_list = %d AND \
event_clip = '%s' AND \
event_start >= %f AND \
event_start <= %f",
										g_ppromolite->m_settings.m_pszLiveEvents,
										pChannel->m_pAPIConn->m_pszServerName,
										pChannel->m_nHarrisListID,
										pList->m_ppEvents[n]->m_pszID,
										dblEventTime-0.3, dblEventTime+0.3
									);

								// get
								CRecordset* prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
								int nNumFound = 0;
								int nID = -1;
								if(prs != NULL) 
								{
									if(!prs->IsEOF())
									{
										// get id!
										CString szValue;
										prs->GetFieldValue("(itemid", szValue);
										nID = atoi(szValue);
										nNumFound++;
										prs->MoveNext();
									}
									prs->Close();
									delete prs;
								}

								if((nNumFound>0)&&(nID>0))
								{
									// update it.
									// if found update it with new values, and a found flag (app_data_aux int will be the found flag 1=found)
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
server_name = '%s', \
server_list = %d, \
list_id = %d, \
event_id = '%s', \
event_clip = '%s', \
event_title = '%s', \
%s%s%s\
event_type = %d, \
event_status = %d, \
event_time_mode = %d, \
event_start = %f, \
event_duration = %d, \
event_last_update = %f, \
app_data_aux = 1 WHERE itemid = %d;",  // 1 is the found flag.
										g_ppromolite->m_settings.m_pszLiveEvents,  // table name
										pChannel->m_pAPIConn->m_pszServerName, //server_name
										pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
										pChannel->m_nChannelID, // list_id - internal lookup channel number
										pList->m_ppEvents[n]->m_pszReconcileKey?pList->m_ppEvents[n]->m_pszReconcileKey:"", //event_id
										pList->m_ppEvents[n]->m_pszID, //event_clip  (must not be null)
										pList->m_ppEvents[n]->m_pszTitle?pList->m_ppEvents[n]->m_pszTitle:"", //event_title
										pList->m_ppEvents[n]->m_pszData?"event_data = '":"", //event_data (exists)
										pList->m_ppEvents[n]->m_pszData?pList->m_ppEvents[n]->m_pszData:"", //event_data  
										pList->m_ppEvents[n]->m_pszData?"', ":"", //event_data (exists)
										pList->m_ppEvents[n]->m_usType, //event_type
										pList->m_ppEvents[n]->m_usStatus, //event_status
										pList->m_ppEvents[n]->m_usControl, //event_time_mode
										dblEventTime, //event_start
										pList->m_ppEvents[n]->m_ulDurationMS, //event_duration
										dblServerTime, //event_last_update
										nID // the unique ID of the thing
									);
// app_data = , // no app data for the above, let it be NULL or whatever it is.


								}
								else
								{
									// add it.
									// if not found, insert one with a found flag
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
server_name, \
server_list, \
list_id, \
event_id, \
event_clip, \
event_title, \
%s\
event_type, \
event_status, \
event_time_mode, \
event_start, \
event_duration, \
event_last_update, \
app_data_aux) VALUES ('%s', %d, %d, '%s', '%s', '%s', %s%s%s%d, %d, %d, %f, %d, %f, 1)",  // 1 is the found flag.
										g_ppromolite->m_settings.m_pszLiveEvents,  // table name
										pList->m_ppEvents[n]->m_pszData?"event_data, ":"", //event_data (exists)
										pChannel->m_pAPIConn->m_pszServerName, //server_name
										pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
										pChannel->m_nChannelID, // list_id - internal lookup channel number
										pList->m_ppEvents[n]->m_pszReconcileKey?pList->m_ppEvents[n]->m_pszReconcileKey:"", //event_id
										pList->m_ppEvents[n]->m_pszID, //event_clip  (must not be null)
										pList->m_ppEvents[n]->m_pszTitle?pList->m_ppEvents[n]->m_pszTitle:"", //event_title
										pList->m_ppEvents[n]->m_pszData?"'":"", //event_data (exists)
										pList->m_ppEvents[n]->m_pszData?pList->m_ppEvents[n]->m_pszData:"", //event_data  
										pList->m_ppEvents[n]->m_pszData?"', ":"", //event_data (exists)
										pList->m_ppEvents[n]->m_usType, //event_type
										pList->m_ppEvents[n]->m_usStatus, //event_status
										pList->m_ppEvents[n]->m_usControl, //event_time_mode
										dblEventTime, //event_start
										pList->m_ppEvents[n]->m_ulDurationMS, //event_duration
										dblServerTime //event_last_update
										);

								}

								if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
								{
									//**MSG
								}
							}
							n++;
						}

						///////////////////////
						// remove found flags.

//					remove all with no found flag.

						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d AND \
app_data_aux < 1",
										g_ppromolite->m_settings.m_pszLiveEvents,  // table name
										pChannel->m_pAPIConn->m_pszServerName, //server_name
										pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
									);
						if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
							//**MSG
						}
//					remove all found flags for next go-around.
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
app_data_aux = 0 WHERE \
server_name = '%s' AND \
server_list = %d",
										g_ppromolite->m_settings.m_pszLiveEvents,  // table name
										pChannel->m_pAPIConn->m_pszServerName, //server_name
										pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
									);

						if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
							//**MSG
						}

					
						// increment exchange mod counter. for events table.
						if (g_ppromolite->m_data.IncrementDatabaseMods(g_ppromolite->m_settings.m_pszLiveEvents, dberrorstring)<PROMOLITE_SUCCESS)
						{
							//**MSG
						}

					}
					// record last values if get was successful.
					memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
				}
				LeaveCriticalSection(&(pList->m_crit));
			}
/ * 			else
			if(listdata.listdisplay!=lastlistdata.listdisplay)
			{
				// an event has changed, need to find it and just deal with that one.
				// record last values
				memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
			}
* /
		}

	}
	pChannel->m_pAPIConn->m_bListActive[nZeroList] = false;
	pChannel->m_bChannelThreadStarted = false;
	_endthread();
}

*/

// this is basically just a time reference thread now.
void PromoLiteAutomationThread(void* pvArgs)
{
	CPromoLiteAutomationChannelObject* pChannelObj = (CPromoLiteAutomationChannelObject*) pvArgs;
	if(pChannelObj==NULL) return;
	CPromoLiteEndpointObject* pEndObj = (CPromoLiteEndpointObject*) pChannelObj->m_pEndpoint;
	if(pEndObj==NULL) return;
	CPromoLiteMain* ppromolite = (CPromoLiteMain*) pChannelObj->m_pPromoLite;
	if(ppromolite==NULL) return;



	pChannelObj->m_bAutomationThreadStarted=true;
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];

	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_SENTINEL)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning automation thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	}
	else
	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_HELIOS)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning automation thread for Omnibus stream %d", pChannelObj->m_nChannelID);
	}

	g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLiteAutomationThread", errorstring);    //(Dispatch message)

	strcpy(errorstring, "");
	CDBUtil db;


	CDBconn* pdbConn = db.CreateNewConnection(ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:automation_database_connect", errorstring);  //(Dispatch message)
			pdbConn = ppromolite->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW); 
		ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:automation_database_init", errorstring);  //(Dispatch message)
		pdbConn = ppromolite->m_data.m_pdbConn;

		//**MSG
	}



	CPromoLiteAutomationChannelObject tempChannelObj;
	bool bChangesPending = false;
	_timeb timebPending;
	_timeb timebChange;
	_ftime(&timebChange);
	_timeb timebSQL;
	_ftime(&timebSQL);

	timebChange.time -= (ppromolite->m_settings.m_nAutomationIntervalMS/1000)+1; // set it back so it goes immediately the first time around.
	timebSQL.time = timebChange.time;


	while((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
	{
		// check automation list for changes (mod)
		bool bChanges = false;
		bool bForceChanges = false;
		bool bTimeChanges = false;

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "%d %d %d %d %d.%03d", ppromolite->m_data.m_nIndexAutomationEndpoint, ppromolite->m_settings.m_nNumEndpointsInstalled, ppromolite->m_settings.m_ppEndpointObject, ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint], ppromolite->m_data.m_timebTick.time, ppromolite->m_data.m_timebTick.millitm);   Sleep(50);//(Dispatch message)

		if(
			  (!ppromolite->m_data.m_bProcessSuspended)
			&&(ppromolite->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!ppromolite->m_data.m_key.m_bExpires)
				||((ppromolite->m_data.m_key.m_bExpires)&&(!ppromolite->m_data.m_key.m_bExpired))
				||((ppromolite->m_data.m_key.m_bExpires)&&(ppromolite->m_data.m_key.m_bExpireForgiveness)&&(ppromolite->m_data.m_key.m_ulExpiryDate+ppromolite->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!ppromolite->m_data.m_key.m_bMachineSpecific)
				||((ppromolite->m_data.m_key.m_bMachineSpecific)&&(ppromolite->m_data.m_key.m_bValidMAC))
				)

			)
		{
//			CPromoLiteEndpointObject* pAutoObj = ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint];

	_ftime(&pChannelObj->m_timebAutomationTick); // the last time check inside the thread
/*
			if((pEndObj->m_pszLiveEvents)&&(strlen(pEndObj->m_pszLiveEvents)))
			{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Checking %s", pAutoObj->m_pszLiveEvents);   Sleep(50);//(Dispatch message)
				pEndObj->m_nModLiveEvents = pEndObj->CheckDatabaseMod(&db, pdbConn, pEndObj->m_pszLiveEvents);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Checked mod %s = %d, %d last", pAutoObj->m_pszLiveEvents, pAutoObj->m_nModLiveEvents, pAutoObj->m_nLastModLiveEvents);   Sleep(50);//(Dispatch message)


				if((pAutoObj->m_nModLiveEvents>=0)&&(pAutoObj->m_nModLiveEvents!=pAutoObj->m_nLastModLiveEvents))
				{
					pAutoObj->m_dblLastAutomationChange = (double)ppromolite->m_data.m_timebAutomationTick.time + ((double)ppromolite->m_data.m_timebAutomationTick.millitm)/1000.0;
					bChanges = true;
					ppromolite->m_data.m_bFarEventsChanged = true;
					ppromolite->m_data.m_bNearEventsChanged = true;
					ppromolite->m_data.m_bTriggerEventsChanged = true;
					
				}
			}
*/
			// check the channel info view for time and changes.
			tempChannelObj.m_nListChanged = -1;
			tempChannelObj.m_dblListLastUpdate = -1.0;

			if(
				  (!ppromolite->m_settings.m_bUseLocalClock) // no need to retrieve from db if using local time.  we getting list change info elsewhere anyway
				&&(ppromolite->m_settings.m_pszChannelInfo)&&(strlen(ppromolite->m_settings.m_pszChannelInfo))
				&&(// only do on interval
				    (ppromolite->m_data.m_timebTick.time > (timebSQL.time+ppromolite->m_settings.m_nAutomationIntervalMS/1000))
					||(
					    (ppromolite->m_data.m_timebTick.time==(timebSQL.time+ppromolite->m_settings.m_nAutomationIntervalMS/1000))
						&&(ppromolite->m_data.m_timebTick.millitm>(timebSQL.millitm+ppromolite->m_settings.m_nAutomationIntervalMS%1000))
						)
					)
				)
			{

				if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_SENTINEL)
				{

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT server_time, server_status, server_basis, server_changed, server_last_update, \
list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update FROM %s WHERE server = '%s' AND listid = %d",
						(g_ppromolite->m_settings.m_pszChannelInfo?g_ppromolite->m_settings.m_pszChannelInfo:"ChannelInfo"),

						pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum
						);
				}
				else
				if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_HELIOS)
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT server_time, server_status, server_basis, server_changed, server_last_update, \
list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update FROM %s WHERE listid = %d",
						(g_ppromolite->m_settings.m_pszChannelInfo?g_ppromolite->m_settings.m_pszChannelInfo:"ChannelInfo"),

						pChannelObj->m_nChannelID
						);
				}



				CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
				if(prs != NULL)
				{
					if(prs->IsEOF()) // no record found!
					{
						pChannelObj->m_ulStatus = PROMOLITE_STATUS_NOTCON;
						// trigger a refresh.
						g_ppromolite->m_data.m_nLastConnectionsMod = -1; // causes GetConnections to be called.
					}
					else
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� not null");  Sleep(50);//(Dispatch message)
					if((!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
					{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� NEOF");  Sleep(50);//(Dispatch message)
						try
						{
							CString szTemp;
							prs->GetFieldValue("server_time", szTemp);
							tempChannelObj.m_dblServertime = atof(szTemp);

							prs->GetFieldValue("server_status", szTemp);
							tempChannelObj.m_nServerStatus = atoi(szTemp);

							prs->GetFieldValue("server_basis", szTemp);
							tempChannelObj.m_nServerBasis = atoi(szTemp);

							prs->GetFieldValue("server_changed", szTemp);
							tempChannelObj.m_nServerChanged = atoi(szTemp);

							prs->GetFieldValue("server_last_update", szTemp);
							tempChannelObj.m_dblServerLastUpdate = atof(szTemp);

							prs->GetFieldValue("list_state", szTemp);
							tempChannelObj.m_nListState = atoi(szTemp);

							prs->GetFieldValue("list_changed", szTemp);
							tempChannelObj.m_nListChanged = atoi(szTemp);

							prs->GetFieldValue("list_display", szTemp);
							tempChannelObj.m_nListDisplay = atoi(szTemp);

							prs->GetFieldValue("list_syschange", szTemp);
							tempChannelObj.m_nListSysChange = atoi(szTemp);

							prs->GetFieldValue("list_count", szTemp);
							tempChannelObj.m_nListCount = atoi(szTemp);

							prs->GetFieldValue("list_lookahead", szTemp);
							tempChannelObj.m_nListLookahead = atoi(szTemp);

							prs->GetFieldValue("list_last_update", szTemp);
							tempChannelObj.m_dblListLastUpdate = atof(szTemp);

							pChannelObj->m_ulStatus = PROMOLITE_STATUS_CONN;

							_ftime(&timebSQL);

						}
						catch(...)
						{
						}
					}
					delete prs;
				}
				else
				{
					// error
					pChannelObj->m_ulStatus = PROMOLITE_STATUS_ERROR;//							0x00000020  // error (red icon)
					// trigger a refresh.
					g_ppromolite->m_data.m_nLastConnectionsMod = -1; // causes GetChannels to be called. (may as well...)
				}

			}// if time to retrieve...
	
			_ftime( &pChannelObj->m_timebAutomationTick );  // we're still alive.

			if(( tempChannelObj.m_nListChanged > 0 )&&(tempChannelObj.m_nListDisplay > 0 ))
			{
				if(
					  (tempChannelObj.m_nListChanged != pChannelObj->m_nListChanged)
					||(tempChannelObj.m_nListDisplay != pChannelObj->m_nListDisplay)
					||(tempChannelObj.m_nServerChanged != pChannelObj->m_nServerChanged)
					||(tempChannelObj.m_nListState != pChannelObj->m_nListState)
					)
				{
					pChannelObj->m_dblServertime  = tempChannelObj.m_dblServertime;
					pChannelObj->m_dblServerLastUpdate  = tempChannelObj.m_dblServerLastUpdate;
					pChannelObj->m_nServerStatus  = tempChannelObj.m_nServerStatus;
					pChannelObj->m_nServerBasis  = tempChannelObj.m_nServerBasis;
					pChannelObj->m_nServerChanged  = tempChannelObj.m_nServerChanged;

					pChannelObj->m_dblListLastUpdate  = tempChannelObj.m_dblListLastUpdate;
					pChannelObj->m_nListState  = tempChannelObj.m_nListState;
					pChannelObj->m_nListChanged  = tempChannelObj.m_nListChanged;
					pChannelObj->m_nListDisplay  = tempChannelObj.m_nListDisplay;
					pChannelObj->m_nListSysChange  = tempChannelObj.m_nListSysChange;
					pChannelObj->m_nListCount  = tempChannelObj.m_nListCount;
					pChannelObj->m_nListLookahead  = tempChannelObj.m_nListLookahead;
					
// following commented out to remain false until pending and force are checked.
// NO,we want to update the time right away.  however, we want to delay the event analysis.
					bChanges = true;
// OK this is not related at all to event analysis anymore

					if(!bChangesPending)
					{
						_ftime(&timebChange); // initialize this

					}

					bChangesPending = true;
					_ftime(&timebPending);
					timebPending.time += ppromolite->m_settings.m_nAnalyzeAutomationDwellMS/1000;
					timebPending.millitm += ppromolite->m_settings.m_nAnalyzeAutomationDwellMS%1000;
					if(timebPending.millitm>999)
					{
						timebPending.time++;
						timebPending.millitm -= 1000;
					}
				}
				else
				if((tempChannelObj.m_dblServerLastUpdate>0)&&(tempChannelObj.m_dblServerLastUpdate != pChannelObj->m_dblServerLastUpdate)) // we have a new update of server time
				{
					_ftime(&timebChange); // initialize this for the interval
					pChannelObj->m_dblServertime  = tempChannelObj.m_dblServertime;
					pChannelObj->m_dblServerLastUpdate  = tempChannelObj.m_dblServerLastUpdate;
					bTimeChanges= true;
				}

			}
			else if(tempChannelObj.m_dblServerLastUpdate > 0.0)
			{
				if((tempChannelObj.m_dblServerLastUpdate>0)&&(tempChannelObj.m_dblServerLastUpdate != pChannelObj->m_dblServerLastUpdate)) // we have a new update of server time
				{
					_ftime(&timebChange); // initialize this for the interval
					pChannelObj->m_dblServertime  = tempChannelObj.m_dblServertime;
					pChannelObj->m_dblServerLastUpdate  = tempChannelObj.m_dblServerLastUpdate;
					bTimeChanges= true;
				}
			}


			if(bChangesPending)
			{
				double dblTime = ((double)(timebChange.time))*1000.0 + (double)timebChange.millitm;
				if((tempChannelObj.m_dblListLastUpdate>0.0)&&(tempChannelObj.m_dblListLastUpdate < (dblTime-ppromolite->m_settings.m_nAnalyzeAutomationForceMS)))
				{
					bForceChanges = true;  // becuase we changed a while ago.  want to go immediately.
				}
			}

			// dwell changes - another change will reset the time, suppressing the previous change

			if(
				  (bChangesPending)
				&&(
						(bForceChanges)
				  ||(ppromolite->m_data.m_timebTick.time>timebPending.time)
					||(
					    (ppromolite->m_data.m_timebTick.time==timebPending.time)
						&&(ppromolite->m_data.m_timebTick.millitm>timebPending.millitm)
						)
					||(ppromolite->m_data.m_timebTick.time > (timebChange.time+ppromolite->m_settings.m_nAnalyzeAutomationForceMS/1000))
					||(
					    (ppromolite->m_data.m_timebTick.time==(timebChange.time+ppromolite->m_settings.m_nAnalyzeAutomationForceMS/1000))
						&&(ppromolite->m_data.m_timebTick.millitm>(timebChange.millitm+ppromolite->m_settings.m_nAnalyzeAutomationForceMS%1000))
						)
					)
				)
			{
				bForceChanges = false;
				bChangesPending = false;
				pChannelObj->m_bTriggerEventsChanged = true;

//				if(pChannelObj->m_nAnalysisCounter >= INT_MAX) pChannelObj->m_nAnalysisCounter = 0;
//				else		pChannelObj->m_nAnalysisCounter++;

//following moved to immediate from delayed.... 
				if(pChannelObj->m_nAnalysisCounter >= INT_MAX) pChannelObj->m_nAnalysisCounter = 0;
				else	pChannelObj->m_nAnalysisCounter++;

//				pChannelObj->m_bNearEventsChanged = true;
				// do some delayed stuff here.
			}


			if((bChanges)||(bTimeChanges))
			{
//				pChannelObj->m_bTriggerEventsChanged = true;  // moved to dwell.
//				pChannelObj->m_bFarEventsChanged = true;


//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Automation changes exist %d", pAutoObj->m_nModLiveEvents);  // Sleep(50);//(Dispatch message)
				// ppromolite->m_data.m_nEventLastMax = 0; // start at the beginning....
// actually dont reset this.
				bChanges = false;
				bool bSQLError = false;
				// if changes
				// {
				//   Close the m_prsEvents recordset and delete it, set it back to NULL.

//				ppromolite->m_data.ReleaseRecordSet();

				_ftime( &pChannelObj->m_timebAutomationTick); 
				if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK)==PROMOLITE_DEP_AUTO_HELIOS)
				{
					// can only use local clock
					if(ppromolite->m_settings.m_bUseUTC)
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
							  ((double)(pChannelObj->m_timebAutomationTick.time))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + ppromolite->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}
					else
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
							  ((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
							+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + ppromolite->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}


				}
				else
				if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK)==PROMOLITE_DEP_AUTO_SENTINEL)
				{
					//update the servertime.

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Automation svr atof? %f", timeval);  Sleep(50);//(Dispatch message)
						
					if(ppromolite->m_settings.m_bUseLocalClock)
					{
						if((ppromolite->m_settings.m_bUseLocalClock)||(pChannelObj->m_dblServerLastUpdate<=0.0))
						{
							pChannelObj->m_dblAutomationTimeEstimate = 
									((double)(pChannelObj->m_timebAutomationTick.time))
								+ ((double)(pChannelObj->m_timebAutomationTick.millitm + ppromolite->m_settings.m_nTriggerAdvanceMS))/1000.0;
						}
						else
						{
							pChannelObj->m_dblAutomationTimeEstimate = 
									((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
								+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))
								+ ((double)(pChannelObj->m_timebAutomationTick.millitm + ppromolite->m_settings.m_nTriggerAdvanceMS))/1000.0;
						}
					}
					else
					{
						if(pChannelObj->m_dblServerLastUpdate>0.0)
						{
							/*
							pChannelObj->m_dblAutomationTimeDiffMS = 
								(((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
								+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm) 
								- pChannelObj->m_dblServerLastUpdate;
							*/
							pChannelObj->m_dblAutomationTimeDiffMS = 
								((double)pChannelObj->m_timebAutomationTick.time)*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm
								- pChannelObj->m_dblServerLastUpdate;
						// DEMO only, need to handle midnight.
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Automation svr time in unix? %f", timeval);  Sleep(50);//(Dispatch message)
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Automation svr time diffMS? %d", nDiff);  Sleep(50);//(Dispatch message)
							pChannelObj->m_dblAutomationTimeEstimate = (pChannelObj->m_dblServertime + pChannelObj->m_dblAutomationTimeDiffMS 
								+ (double)ppromolite->m_settings.m_nTriggerAdvanceMS)/1000.0;
//										ppromolite->m_data.m_dblAutomationTimeEstimate = (double)timebTempTime.time;
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Automation estimate time? %f", ppromolite->m_data.m_dblAutomationTimeEstimate);  Sleep(50);//(Dispatch message)
						}
					}
				}  // type was sentinel
				// else automation type unknown, so nothing
			} // automation changes exist

/////////////////////////////////////////////////////////
			else  // no automation changes
			{

				_ftime( &pChannelObj->m_timebAutomationTick); 
				if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK)==PROMOLITE_DEP_AUTO_HELIOS)
				{
					// can only use local clock

					if(ppromolite->m_settings.m_bUseUTC)
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
							  ((double)(pChannelObj->m_timebAutomationTick.time))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + ppromolite->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}
					else
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
							  ((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
							+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + ppromolite->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}


				}
				else
				if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK)==PROMOLITE_DEP_AUTO_SENTINEL)
				{
					// just update the time based on estimate
					if((ppromolite->m_settings.m_bUseLocalClock)||(pChannelObj->m_dblServerLastUpdate<=0.0))
					{
						if(ppromolite->m_settings.m_bUseUTC)
						{
							pChannelObj->m_dblAutomationTimeEstimate = 
									((double)(pChannelObj->m_timebAutomationTick.time))
								+ ((double)(pChannelObj->m_timebAutomationTick.millitm + ppromolite->m_settings.m_nTriggerAdvanceMS))/1000.0;
						}
						else
						{
							pChannelObj->m_dblAutomationTimeEstimate = 
									((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
								+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))
								+ ((double)(pChannelObj->m_timebAutomationTick.millitm + ppromolite->m_settings.m_nTriggerAdvanceMS))/1000.0;
						}
					}
					else
					{
						if(pChannelObj->m_dblServerLastUpdate>0.0)
						{
							/*
							pChannelObj->m_dblAutomationTimeDiffMS = (((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
								+(pChannelObj->m_timebAutomationTick.dstflag?3600:0)))*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm) 
								- pChannelObj->m_dblServerLastUpdate;
								*/

							pChannelObj->m_dblAutomationTimeDiffMS = 
								((double)pChannelObj->m_timebAutomationTick.time)*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm
								- pChannelObj->m_dblServerLastUpdate;

							pChannelObj->m_dblAutomationTimeEstimate = (pChannelObj->m_dblServertime + pChannelObj->m_dblAutomationTimeDiffMS 
								+ (double)ppromolite->m_settings.m_nTriggerAdvanceMS)/1000.0;
						}
					}
				}

/*
				ppromolite->m_data.m_dblAutomationTimeEstimate = ppromolite->m_data.m_dblHarrisTime + ((double)(ppromolite->m_data.m_nHarrisTimeDiffMS)/1000.0) + (double)ppromolite->m_settings.m_nTriggerAdvanceMS/1000.0
					+ ((double)timebTempTime.time - (double)ppromolite->m_data.m_timebHarrisCheckTimeLocal.time)
					+ ((double)timebTempTime.millitm/1000.0 - (double)ppromolite->m_data.m_timebHarrisCheckTimeLocal.millitm/1000.0); // the difference beween now and last check
//										ppromolite->m_data.m_dblAutomationTimeEstimate = (double)timebTempTime.time;
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Automation estimate time now %f", ppromolite->m_data.m_dblAutomationTimeEstimate);  Sleep(50);//(Dispatch message)
*/

			}


		} // if license etc


		Sleep(1); // dont peg processor
	} // while((!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))

	Sleep(30);
	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_SENTINEL)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending automation thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	}
	else
	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_HELIOS)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending automation thread for Omnibus stream %d", pChannelObj->m_nChannelID);
	}

	g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLiteAutomationThread", errorstring);    //(Dispatch message)
	db.RemoveConnection(pdbConn);
	pChannelObj->m_bAutomationThreadStarted=false;

  Sleep(50);//(Dispatch message)
	_endthread();
	Sleep(150);

}

#ifndef NOT_USING_NEAR_ANALYSIS
// NO LONGER USING NEAR ANALYSIS
void PromoLiteNearAnalysisThread(void* pvArgs)
{
	CPromoLiteAutomationChannelObject* pChannelObj = (CPromoLiteAutomationChannelObject*) pvArgs;
	if(pChannelObj==NULL) return;
	CPromoLiteEndpointObject* pEndObj = (CPromoLiteEndpointObject*) pChannelObj->m_pEndpoint;
	if(pEndObj==NULL) return;
	CPromoLiteMain* ppromolite = (CPromoLiteMain*) pChannelObj->m_pPromoLite;
	if(ppromolite==NULL) return;

	pChannelObj->m_bNearAnalysisThreadStarted=true;
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	unsigned long ulAutoType  = 0;

	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_SENTINEL)
	{
		ulAutoType = PROMOLITE_DEP_AUTO_SENTINEL;
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning analysis thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	}
	else
	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_HELIOS)
	{
		ulAutoType = PROMOLITE_DEP_AUTO_HELIOS;
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning analysis thread for Omnibus stream %d", pChannelObj->m_nChannelID);
	}
	g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLiteAnalysisThread", errorstring);    //(Dispatch message)
	
	strcpy(errorstring, "");
	CDBUtil db;
	CDBUtil dbSet;

	CDBconn* pdbConn = db.CreateNewConnection(ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:analysis_database_connect", errorstring);  //(Dispatch message)
			pdbConn = ppromolite->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW); 
		ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:analysis_database_init", errorstring);  //(Dispatch message)
		pdbConn = ppromolite->m_data.m_pdbConn;

		//**MSG
	}

	CDBconn* pdbConnSet = dbSet.CreateNewConnection(ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW);
	if(pdbConnSet)
	{
		if(dbSet.ConnectDatabase(pdbConnSet, errorstring)<DB_SUCCESS)
		{
			ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:analysis_database2_connect", errorstring);  //(Dispatch message)
			pdbConnSet = ppromolite->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW); 
		ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:analysis_database2_init", errorstring);  //(Dispatch message)
		pdbConnSet = ppromolite->m_data.m_pdbConn;

		//**MSG
	}

	while(!pChannelObj->m_bNearEventsChanged)
	{
		_ftime(&pChannelObj->m_timebNearTick); // the last time check inside the thread
		Sleep(30);
	}

//	Sleep(30000); // dont start thread analysis util after 30 sec


	double delta = 0.0;
	double wait = 0.0;
	while((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
	{
		if(
/*
			  (ppromolite->m_data.m_nIndexAutomationEndpoint>=0)
			&&(ppromolite->m_settings.m_nNumEndpointsInstalled>0)
			&&(ppromolite->m_data.m_nIndexAutomationEndpoint<ppromolite->m_settings.m_nNumEndpointsInstalled)
			&&(ppromolite->m_settings.m_ppEndpointObject)
			&&(ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint])
			&&*/
			  (!ppromolite->m_data.m_bProcessSuspended)

			&&(

					(
						(ulAutoType == PROMOLITE_DEP_AUTO_SENTINEL)
					&&(pChannelObj->m_nListState&((1<<LISTTHREADING)|(1<<LISTISPLAYING)))  // list must be threaded and playing to run analysis
//					&&(!(pChannelObj->m_nListState&((1<<LISTINFREEZE)|(1<<LISTINHOLD))))  // list can be in freeze or hold to run analysis
					)
				||(
						(ulAutoType == PROMOLITE_DEP_AUTO_HELIOS)
					&&(pChannelObj->m_nListState!=0)  // list must be connected
//					&&(pChannelObj->m_nServerStatus!=0)  // server must be connected!  // comment this out to surivive adaptor disconnection retries....
					)
				)
				
// old				&&(pChannelObj->m_nListState&((1<<LISTTHREADING)|(1<<LISTISPLAYING)))  // list must be threaded and playing to run analysis
			&&(ppromolite->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!ppromolite->m_data.m_key.m_bExpires)
				||((ppromolite->m_data.m_key.m_bExpires)&&(!ppromolite->m_data.m_key.m_bExpired))
				||((ppromolite->m_data.m_key.m_bExpires)&&(ppromolite->m_data.m_key.m_bExpireForgiveness)&&(ppromolite->m_data.m_key.m_ulExpiryDate+ppromolite->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!ppromolite->m_data.m_key.m_bMachineSpecific)
				||((ppromolite->m_data.m_key.m_bMachineSpecific)&&(ppromolite->m_data.m_key.m_bValidMAC))
				)

			)
		{
//			CPromoLiteEndpointObject* pAutoObj = ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint];

			//Here, we analyze things that have not yet played, that are not yet analyzed, unless its within m_ulTriggerCueThreshold seconds of play time, then it gets reanalyzed
	_ftime(&pChannelObj->m_timebNearTick); // the last time check inside the thread

			pChannelObj->m_bNearEventsChanged = false;

			while((!pChannelObj->m_bNearEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
			{
	_ftime(&pChannelObj->m_timebNearTick); // the last time check inside the thread
				// just get the exports with params to parse.
/*
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
			//		  "select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE TriggerTime > %lf and ((event_status & 19) = 0) and ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY TriggerTime ASC) ORDER BY TriggerTime ASC",
//						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_calc_start > %u) and ((event_status & 19) = 0)) ORDER BY event_calc_start ASC)  and ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY TriggerTime ASC",
						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_status & 19) = 0) ORDER BY event_calc_start ASC) and server = '%s' AND listid = %d AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY TriggerTime ASC",
//						"SELECT TOP 1 * FROM %s WHERE TriggerTime > %lf and event_status & 19 = 0 and flags & %d = 0 ORDER BY TriggerTime ASC",
						ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
					//	(unsigned long)ppromolite->m_data.m_dblAutomationTimeEstimate,
						pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListID,	PROMOLITE_FLAG_REANALYZED
					);
*/


/*
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_status & 19) = 0) and channelid = %d AND AnalyzedTriggerDataFlags <> %d  ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
						ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
						pChannelObj->m_nChannelID, pChannelObj->m_nAnalysisCounter
					);
*/
				if(ulAutoType == PROMOLITE_DEP_AUTO_SENTINEL)
				{
		
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
							"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_status & 19) = 0) and channelid = %d AND ((AnalyzedTriggerDataFlags & %d) = 0)  ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
							ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							pChannelObj->m_nChannelID, PROMOLITE_FLAG_REANALYZED
						);
				
				//			need top x where top event item id is all same where event status & 19 =0 (not play, post, done)
				}
				else
				if(ulAutoType == PROMOLITE_DEP_AUTO_HELIOS)
				{
		
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
							"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE event_status < 10 and event_status > 4 and channelid = %d AND ((AnalyzedTriggerDataFlags & %d) = 0)  ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
							ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							pChannelObj->m_nChannelID, PROMOLITE_FLAG_REANALYZED
						);
				
				//			need top x where top event item id is all same where event status < 10  and event_status > 4 (not done. no errors, and allocated or better in system)
				}
					
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Near analysis SQL: %s", szSQL); // Sleep(50);//(Dispatch message)
				int nNumRec = 0;
				CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
				if(prs != NULL)
				{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Near analysis resultset not null %d record", prs->GetRecordCount( )); // Sleep(50);//(Dispatch message)
					while((!pChannelObj->m_bNearEventsChanged)&&(!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
					{
	_ftime(&pChannelObj->m_timebNearTick); // the last time check inside the thread
						nNumRec++;
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Near analysis resultset Got %d items so far", nNumRec); // Sleep(50);//(Dispatch message)
						CString szName;
						CString szValue;
						CString szTemp;
						//double dblTime;
						double dblClipTime;
						double dblEventTime;
						double dblParentClipTime;

						int nTriggerItemID=-1;
						int nAutomationItemID=-1;
						int nPromoLiteEventItemID=-1;
						int nEventActionID=-1;
						int nType=-1;
						int nAnalysisID=-1;

						prs->GetFieldValue("itemid", szTemp);
						if(szTemp.GetLength()) nAutomationItemID = atoi(szTemp);

						// Host
						// ParsedValue
						//prs->GetFieldValue("TriggerTime", szTemp);
						//dblTime=atof(szTemp);
						prs->GetFieldValue("analyzed_trigger_id", szTemp);
						if(szTemp.GetLength()) nTriggerItemID = atoi(szTemp);

						prs->GetFieldValue("AnalyzedTriggerDataFlags", szTemp);
						unsigned long ulFlags = atol(szTemp);
						//AnalyzedTriggerDataStatus

						prs->GetFieldValue("promolite_EventID", szTemp);
						if(szTemp.GetLength()) nPromoLiteEventItemID = atoi(szTemp);
						//PromoLite_Timing_Col_Name
						//Source
						//Scene
						prs->GetFieldValue("Graphic_Action_Type", szTemp);
						nType = atoi(szTemp);
						//Action_Name


						prs->GetFieldValue("Event_ActionID", szTemp);
						if(szTemp.GetLength()) nEventActionID = atoi(szTemp);

						//Action_Value
						prs->GetFieldValue("Action_Value", szValue);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Near Analysis value is: %s", szValue);//  Sleep(50);//(Dispatch message)
					
						prs->GetFieldValue("Analysis_ID", szTemp);
						if(szTemp.GetLength()) nAnalysisID = atoi(szTemp);

						prs->GetFieldValue("parent_calc_start", szTemp);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got TriggerTime %s", szTemp); 
						dblParentClipTime=atof(szTemp);

						prs->GetFieldValue("event_calc_start", szTemp);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got TriggerTime %s", szTemp); 
						dblClipTime=atof(szTemp);

						prs->GetFieldValue("event_start", szTemp);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got TriggerTime %s", szTemp); 
						dblEventTime=atof(szTemp);

// if analysis changes an item within X seconds of now, lets refresh the trigger buffer by setting pChannelObj->m_bTriggerEventsChanged = true
								if(nType == 0) // anim
								{
									// just set to 
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "anim is retrieved");//  Sleep(50);//(Dispatch message)
									ulFlags |= PROMOLITE_FLAG_REANALYZED;
								//	ulFlags = pChannelObj->m_nAnalysisCounter;

									if(nTriggerItemID<=0)
									{
										if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
										{
											//insert
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, promolite_eventid, event_actionid, flags, analysis_id) values \
(%d, %d, %d, %d, %d)",
													ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nPromoLiteEventItemID, nEventActionID, ulFlags, nAnalysisID
												);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug1", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
											if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
											{
											//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing near SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
											}//else break;

										}
									}
									else
									{
										// update
										if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
										{
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, promolite_eventid = %d, Event_ActionID = %d, flags = %d, analysis_id = %d WHERE analyzed_trigger_id = %d",
													ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nPromoLiteEventItemID, nEventActionID, ulFlags, nAnalysisID, nTriggerItemID
												);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
											if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
											{
											//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing near SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
											}
										}
									}
								}
								else
								if(nType == 1) // export
								{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "export %s is retrieved", szValue);//  Sleep(50);//(Dispatch message)
									if(
											(szValue.GetLength())
										&&(
												((ppromolite->m_settings.m_bUseUTF8)&&(szValue.Find("§")>=0))
											||((!ppromolite->m_settings.m_bUseUTF8)&&(szValue.Find("�")>=0))
											)
										)
									{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "NEAR value has a �: %s", szValue);  Sleep(50);//(Dispatch message)
										// we have a least one param to parse
										// just set to renanalyzed
										ulFlags |= PROMOLITE_FLAG_REANALYZED;
							//			ulFlags = pChannelObj->m_nAnalysisCounter;

										// lets parse out szValue and set it up;

										CString szPreParse = szValue;
										szValue = "";

										int i=0;

										while((i<szPreParse.GetLength())&&(!pChannelObj->m_bNearEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
										{
											bool bDelimFound=false;
											char ch = szPreParse.GetAt(i);
											if(ppromolite->m_settings.m_bUseUTF8)
											{
												if((ch == '�')&&(szPreParse.GetAt(i+1) == '�'))	
												{
													bDelimFound=true; i+=2;
												}
											}
											else
											{
												if(ch == '�')
												{
													bDelimFound=true; i++;
												}
											}

											if(bDelimFound)
											{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "NEAR ch has a �");  Sleep(50);//(Dispatch message)

												CString szParam = "";
												CString szStyle = "";

												bDelimFound=false;												
												ch = szPreParse.GetAt(i);

												bool bStyle = false;
												if(ppromolite->m_settings.m_bUseUTF8)
												{
													if((ch == '�')&&(szPreParse.GetAt(i+1) == '�'))	
													{
														bDelimFound=true; i+=2;
													}
												}
												else
												{
													if(ch == '�')
													{
														bDelimFound=true; i++;
													}
												}

												
												while((!bDelimFound)&&(i<szPreParse.GetLength())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
												{
													if(!bStyle)
													{
														szParam += ch;
													}
													else
													{
														szStyle += ch;
													}
													i++;
													ch = szPreParse.GetAt(i);
													if(ch == ':')
													{
														i++;
														bStyle = true;
														ch = szPreParse.GetAt(i);
													}
													if(ppromolite->m_settings.m_bUseUTF8)
													{
														if((ch == '�')&&(szPreParse.GetAt(i+1) == '�'))	
														{
															bDelimFound=true; i+=2;
														}
													}
													else
													{
														if(ch == '�')
														{
															bDelimFound=true; i++;
														}
													}
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "near Param %s, Style %s", szParam,szStyle);  Sleep(50);//(Dispatch message)


												}
												

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "near FINAL Param %s, Style %s", szParam,szStyle);  Sleep(50);//(Dispatch message)
												// ok here, we should have the param name and possible style.
												// first lets get the param.

/*												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s '%s', %f",
														ppromolite->m_settings.m_pszGetParameterValueProc?ppromolite->m_settings.m_pszGetParameterValueProc:"spGetParameterValue",  
														szParam, dblTime
													);
*/

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
/*
												// this sets the param
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
											//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}

												// this gets it!
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM [Output]");
*/
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
			
// try 2
/*
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "{CALL %s('%s', %f)}",
														ppromolite->m_settings.m_pszGetParameterValueProc?ppromolite->m_settings.m_pszGetParameterValueProc:"spGetParameterValue",  
														szParam, dblTime
													);

*/												
				// try 3								
											
												if((!pChannelObj->m_bNearEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))												
												{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Near parameter retrieve index for [%s]", szParam); // Sleep(50);//(Dispatch message)
													EnterCriticalSection(&ppromolite->m_data.m_critParameterRules);
													int idx = ppromolite->m_data.GetParameterQueryIndex(szParam);


	//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Near parameter retrieved index %d", idx); // Sleep(50);//(Dispatch message)
												if(idx>=0)
													{
														CString timestr;  
														if(ppromolite->m_data.m_ppParameterRule[idx]->sz_target_valueid.Compare("0"))
														{
															if(ppromolite->m_settings.m_bNearAnalysisUsesCalc)
																timestr.Format("%.3f", dblClipTime);
															else
																timestr.Format("%.3f", dblEventTime);
														}
														else
														{
													//		if(ppromolite->m_settings.m_bNearAnalysisUsesCalc)
													//			timestr.Format("%.3f", dblClipTime-0.020);
													//		else
																timestr.Format("%.3f", dblParentClipTime-0.020);  // now should always be the real time
														}
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s%s%s",
																ppromolite->m_data.m_ppParameterRuleQuery[idx]->sz_query1,
																timestr,
																ppromolite->m_data.m_ppParameterRuleQuery[idx]->sz_query2
															);
//if(ppromolite->m_settings.m_bUseEmail) 	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Near � get param (%s) Query: %s",szParam, szSQL); // Sleep(50);//(Dispatch message)
													}

													LeaveCriticalSection(&ppromolite->m_data.m_critParameterRules);
												
													
													
													CRecordset* prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);






//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� SQL ERROR %s", errorstring);  Sleep(50);//(Dispatch message)


/*

												bool bGotit=false;
												CString target_valueid, target_criterion_col_name, criterion_value, returned_property_col_name, default_value;

_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select target_valueid, target_criterion_col_name, criterion_value, returned_property_col_name, [default_value] from [Parameters] where [name] = '%s'",szParam);
//_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from Analysis");

ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
CRecordset* prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);

												if(prsParam != NULL)
												{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� not null");  Sleep(50);//(Dispatch message)
													if((!prsParam->IsEOF())&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
													{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� NEOF");  Sleep(50);//(Dispatch message)
														try
														{
															prsParam->GetFieldValue("target_valueid", target_valueid);
															prsParam->GetFieldValue("target_criterion_col_name", target_criterion_col_name);
															prsParam->GetFieldValue("criterion_value", criterion_value);
															prsParam->GetFieldValue("returned_property_col_name", returned_property_col_name);
															prsParam->GetFieldValue("default_value", default_value);

															bGotit=true;
														}
														catch(CException *e)// CDBException *e, CMemoryException *m)  
														{
															if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
															{
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);  Sleep(50);//(Dispatch message)
															}
															else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
															{
																// The error code is in e->m_nRetCode
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: out of memory");
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
															}
															else 
															{
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught other exception.\n%s", szSQL);
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
															}
															e->Delete();
														} 
														catch( ... )
														{
														}
													}
													prsParam->Close();
													delete prsParam;
												}

if(bGotit)
{
_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top 1 val, datatype from (select top ' + %s + ' (case when len(' + %s + ') > 0 THEN ' + %s + ' ELSE ''' + %s + ''' END) as val, event_start, (select datatype from MetaConfigUnionALLView \
where col_name = ''' + %s + ''') as datatype from cortex.dbo.LiveEventData as LiveEventData where event_start > ' + %f + ' and \
(event_status is NULL or event_status = 0) and ' + %s + ' = ''' + %s + ''' order by event_start) as Temp order by event_start desc'",
					target_valueid,returned_property_col_name,returned_property_col_name,
					default_value,returned_property_col_name,
					dblTime,target_criterion_col_name,criterion_value
					);
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "2� SQL %s", szSQL);  Sleep(50);//(Dispatch message)

prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);




}
else
{
	prsParam = NULL;
}
*/

													if(prsParam != NULL)
													{
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "NEAR ************** 2� not null"); // Sleep(50);//(Dispatch message)
														if((!prsParam->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
														{
	//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "2� NEOF");  Sleep(50);//(Dispatch message)

															CString szDataType;
															try
															{
																prsParam->GetFieldValue((short)0, szTemp);  //"val" field name
																prsParam->GetFieldValue((short)1, szDataType);  // "datatype" field name
	//															prsParam->GetFieldValue("value", szTemp);  // for use with output table
	//															prsParam->GetFieldValue("datatype", szDataType);  // sp now returns datatype
//if(ppromolite->m_settings.m_bUseEmail) 		ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "NEAR � got %s (datatype = %s) for %s, %s",szTemp, szDataType, szSQL, szPreParse);  Sleep(50);//(Dispatch message)

															}
															catch(CException *e)// CDBException *e, CMemoryException *m)  
															{
																if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
																{
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);  Sleep(50);//(Dispatch message)
																}
																else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
																{
																	// The error code is in e->m_nRetCode
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: out of memory");
										//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
																}
																else 
																{
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught other exception.\n%s", szSQL);
										//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
																}
																e->Delete();
																szTemp=""; // blank to be safe
															} 
															catch( ... )
															{
																szTemp=""; // blank to be safe
															}



			//												szTemp = "Parameter!";

	//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� got %s",szTemp);  Sleep(50);//(Dispatch message)
															if(szTemp.GetLength())
															{
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� returned %s", szTemp);  Sleep(50);//(Dispatch message)
																double dblNumber = atof(szTemp);
																unsigned long ulTime = (unsigned long)(dblNumber);
	//															bool bTime = (ulTime>1)?true:false;
																bool bTime = (szDataType.CompareNoCase("Time")==0)?true:false;
																if(bTime)
																{
																	ulTime%=86400;
																	szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
	//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "szParam =  %s", szParam);  Sleep(50);//(Dispatch message)
																}
																bool bPM = true;
																if(ulTime<43200) bPM = false;
																
		
																if(szStyle.GetLength())
																{

																	// now have to get a list of styles
																	prsParam->Close();
																	delete prsParam;

																	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select actions from promolite.dbo.styles as styles where name = '%s'", szStyle);
	//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "action near SQL: %s", szSQL);  Sleep(50); //(Dispatch message)

																	prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);
	//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "action near SQL error?: %s", errorstring);  Sleep(50); //(Dispatch message)

																	int nNumStyles = 0;
																	
																	szParam = "";
																	bool bAdded = false;
																	//unsigned long ulTimeTemp = ulTime;
																	if(prsParam != NULL)
																	{

																		if((!prsParam->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
																		{
																			prsParam->GetFieldValue("actions", szStyle);
	//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "action got: %s", szStyle);  Sleep(50); //(Dispatch message)


																			CSafeBufferUtil sbu;
																			char actions[2048];
																			sprintf(actions, "%s", szStyle);
																			char* paction = sbu.Token(actions, strlen(actions), "|");
																			bool b24 = false;

																			while((paction)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
																			{
																				if((strnicmp(paction, "snapTo(", 7)==0)&&(strlen(paction)>7))
																				{
																					int nMinutes = atoi(paction+7)*60;
																					nNumStyles++;

																					unsigned long ulTimeTemp = ((ulTime + 150)/nMinutes)*nMinutes;
																					ulTime=ulTimeTemp;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00:%02d", ((ulTime)%3600)/60); // special case 00
																						else
																							szParam.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					}
																					else
																						szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
																				}
																				else 
/*
																				if(stricmp(paction, "snapTo(5)")==0)
																				{
																					nNumStyles++;

																					unsigned long ulTimeTemp = ((ulTime + 150)/300)*300;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(15)")==0)
																				{
																					nNumStyles++;
																					unsigned long ulTimeTemp = ((ulTime +450)/900)*900;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(30)")==0)
																				{
																					nNumStyles++;
																					unsigned long ulTimeTemp = ((ulTime+900)/1800)*1800;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(60)")==0)
																				{
																					unsigned long ulTimeTemp = ((ulTime+1800)/3600)*3600;
																					ulTime=ulTimeTemp;
																					nNumStyles++;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
*/
																				if(stricmp(paction, "clock(24)")==0)
																				{
																					szTemp.Format("%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																					nNumStyles++;
																					b24 = true;
																				}
																				else 
																				if(stricmp(paction, "clock(12)")==0)
																				{
																					nNumStyles++;
																					if(ulTime>=43200) ulTime-=43200;
																					szTemp.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60);
																					b24 = false;
																				}
																				else 
																				if(stricmp(paction, "formatTime('H:MM:SS')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00:%02d:%02d", ((ulTime)%3600)/60, ((ulTime)%3600)%60); // special case 00
																						else
																							szParam.Format("%d:%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					}		
																					else
																						szParam.Format("%d:%02d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH:MM:SS')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																						szParam.Format("%02d:%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					else
																						szParam.Format("%02d:%02d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH:MM')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																						szParam.Format("%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					else
																						szParam.Format("%02d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('H:MM')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00:%02d", ((ulTime)%3600)/60); // default
																						else
																							szParam.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					}
																					else
																						szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('MM')")==0)
																				{
																					nNumStyles++;
																					szParam.Format("%02d", ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('H')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00"); // default
																						else
																							szParam.Format("%d", (ulTime)/3600); // default
																					}
																					else
																						szParam.Format("%d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600))); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																						szParam.Format("%02d", (ulTime)/3600); // default
																					else
																						szParam.Format("%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600))); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoW')")==0)
																				{ //i.e. Monday
																					nNumStyles++;

																					char buffer[48]; // need fewer;
																					long timeofthing = (long)dblNumber; 
																					tm* theTime = gmtime( &timeofthing );
																					strftime( buffer, 40, "%A", theTime );
																					szParam.Format("%s", buffer); 
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_ppromolite->m_data.m_timebTick.time - (g_ppromolite->m_data.m_timebTick.timezone*60) +(g_ppromolite->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoWex')")==0)
																				{ //i.e. Monday, but:
																					//if later today
																					//{ if > 6pm "tonight" else "later" }
																					// else if tomorrow "tomorrow" 
																					// else if > 1 week from now "coming"
																					// else "Monday" style day of week

																					unsigned long ulNow = (unsigned long)(g_ppromolite->m_data.m_timebTick.time - (g_ppromolite->m_data.m_timebTick.timezone*60) +(g_ppromolite->m_data.m_timebTick.dstflag?3600:0)); // local time....

																					if((ulNow/86400) == (((unsigned long)dblNumber)/86400)) // today
																					{
																						if((ulTime/3600)>17) // tonight, 6pm or later
																						{
																							szParam = "Tonight";
																						}
																						else
																						{
																							szParam = "Later";
																						}
																					}
																					else
																					if((ulNow/86400) == ((((unsigned long)dblNumber)/86400)-1)) // tomorrow
																					{
																						szParam = "Tomorrow";
																					}
																					else
																					if((ulNow/86400) <= ((((unsigned long)dblNumber)/86400)-7)) // next week
																					{
																						szParam = "Coming";
																					}
																					else
																					{
																						char buffer[48]; // need fewer;
																						long timeofthing = (long)ulTime; 
																						tm* theTime = gmtime( &timeofthing );
																						strftime( buffer, 40, "%A", theTime );
																						szParam.Format("%s", buffer); 
																					}

																					nNumStyles++;
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_ppromolite->m_data.m_timebTick.time - (g_ppromolite->m_data.m_timebTick.timezone*60) +(g_ppromolite->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoWa')")==0)
																				{ //i.e. Monday
																					nNumStyles++;

																					char buffer[48]; // need fewer;
																					long timeofthing = (long)dblNumber; 
																					tm* theTime = gmtime( &timeofthing );
																					strftime( buffer, 40, "%a", theTime );
																					szParam.Format("%s", buffer); 
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_ppromolite->m_data.m_timebTick.time - (g_ppromolite->m_data.m_timebTick.timezone*60) +(g_ppromolite->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoWaex')")==0)
																				{ //i.e. Monday, but:
																					//if later today
																					//{ if > 6pm "tonight" else "later" }
																					// else if tomorrow "tomorrow" 
																					// else if > 1 week from now "coming"
																					// else "Monday" style day of week

																					unsigned long ulNow = (unsigned long)(g_ppromolite->m_data.m_timebTick.time - (g_ppromolite->m_data.m_timebTick.timezone*60) +(g_ppromolite->m_data.m_timebTick.dstflag?3600:0)); // local time....

																					if((ulNow/86400) == (((unsigned long)dblNumber)/86400)) // today
																					{
																						if((ulTime/3600)>17) // tonight, 6pm or later
																						{
																							szParam = "Tonight";
																						}
																						else
																						{
																							szParam = "Later";
																						}
																					}
																					else
																					if((ulNow/86400) == ((((unsigned long)dblNumber)/86400)-1)) // tomorrow
																					{
																						szParam = "Tomorrow";
																					}
																					else
																					if((ulNow/86400) <= ((((unsigned long)dblNumber)/86400)-7)) // next week
																					{
																						szParam = "Coming";
																					}
																					else
																					{
																						char buffer[48]; // need fewer;
																						long timeofthing = (long)dblNumber; 
																						tm* theTime = gmtime( &timeofthing );
																						strftime( buffer, 40, "%a", theTime );
																						szParam.Format("%s", buffer); 
																					}

																					nNumStyles++;
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_ppromolite->m_data.m_timebTick.time - (g_ppromolite->m_data.m_timebTick.timezone*60) +(g_ppromolite->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "appendAMPM('lcase')")==0)
																				{
																					szParam = szTemp;
																					nNumStyles++;
																					szTemp.Format("%s %s", szParam, bPM?"pm":"am");
																				}
																				else 
																				if(stricmp(paction, "appendAMPM('ucase')")==0)
																				{
																					szParam = szTemp;
																					nNumStyles++;
																					szTemp.Format("%s %s", szParam, bPM?"PM":"AM");
																				}
	//else not nNumStyles++; which is why i had to do it that annoying way.


	/*
	Round,roundNumber()
	Truncate,truncateNumber()
	Filter Smut,smutFilter()
	Truncate To 20 Characters,truncateText(20)
	Truncate To 50 Characters,truncateText(50)
	Truncate To 100 Characters,truncateText(100)
*/

																				else 
																				if(stricmp(paction, "roundNumber()")==0)
																				{
																					dblNumber+= 0.5;
																					nNumStyles++;
																					szTemp.Format("%d", (int)dblNumber);
																				}
																				else 
																				if(stricmp(paction, "truncateNumber()")==0)
																				{
																					nNumStyles++;
																					szTemp.Format("%d", (int)dblNumber);
																				}
																				else 
																				if((strnicmp(paction, "truncateText(", 13)==0)&&(strlen(paction)>13))
																				{
																					int nTruncLen = atoi(paction+13);
																					nNumStyles++;
																					szParam = szTemp.Left(nTruncLen);
																					szTemp = szParam;
																				}

/*
	Convert All Text To Upper Case,textCase('upper')
	Convert All Text To Lower Case,textCase('lower')
	Convert First Letter Of First Word To Upper Case,textCase('sentence')
	Convert First Letter Of Text String To Upper Case,textCase('title')
*/
																				else 
																				if((strnicmp(paction, "textCase(", 9)==0)&&(strlen(paction)>9))
																				{
																					int nL=0;
																					bool bSpace = true;
																					char ch;
																					szParam = "";

																					while(nL<szTemp.GetLength())
																					{
																						ch = szTemp.GetAt(nL);
																						switch(*(paction+9))
																						{
																						case 'u':
																							{
																								szParam += toupper(ch);
																							} break;
																						case 'l':
																							{
																								szParam += tolower(ch);
																							} break;
																						case 's':
																							{
																								if((!(isspace(ch)))&&(bSpace))
																								{
																									bSpace = false;
																									szParam += toupper(ch);
																								}
																								else
																								{
																									szParam += ch;
																								}
																							} break;
																						case 't':
																							{
																								if(isspace(ch))
																								{
																									bSpace = true;
																									szParam += ch;
																								}
																								else
																								{
																									if(bSpace)
																									{
																										szParam += toupper(ch);
																									}
																									else
																									{
																										szParam += ch;
																									}
																									bSpace = false;
																								}
																							} break;
																						}
																						nL++;
																					}
																					nNumStyles++;
																					szTemp = szParam;
																				}


	/*
	Long Date Lower Case,formatDate('longdate', 'lcase')
	Long Date Upper Case,formatDate('longdate', 'ucase')
	Short Date Lower Case,formatDate('shortdate', 'lcase')
	Short Date Upper Case,formatDate('shortdate', 'ucase')
	Celsius,formatTemp('celsius')
	Fahrenheit,formatTemp('fahrenheit')
	*/
																				else 
																				if(stricmp(paction, "formatTemp('celsius')")==0)
																				{
																					nNumStyles++;
																					szTemp.Format("%d�C", (int)dblNumber);
																				}
																				else 
																				if(stricmp(paction, "formatTemp('fahrenheit')")==0)
																				{
																					nNumStyles++;
																					szTemp.Format("%d�F", (int)dblNumber);
																				}
																			
																				paction = sbu.Token(NULL, NULL, "|");
																			}
																			if(bTime) szValue += szTemp;
																		}
																		prsParam->Close();
																		delete prsParam;
																		prsParam = NULL;

																	}

																	if(nNumStyles==0)
																	{
																		if(bTime)
																		{
																			// format it the default way H:MM 24 hour clock
																			szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																			szValue += szTemp;
																		}
																		else 
																		{
																			szValue += szTemp;
																		}
																	}
																}
																else
																{
																	// if no style, need to understand if this is a time.
																	if(bTime)
																	{
																		// format it the default way H:MM 24 hour clock
																		szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																		szValue += szTemp;
																	}
																	else 
																	{
																		szValue += szTemp;
																	}
																}
															}
														}
														if(prsParam)
														{
															prsParam->Close();
															delete prsParam;
														}
													}
													else
													{
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "NEAR � returned NULL");  Sleep(50);//(Dispatch message)

													}
												}
											}
											else
											{
												szValue += ch;
												i++;
											}
										}
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "styles done");  Sleep(50); //(Dispatch message)

										if((!pChannelObj->m_bNearEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
										{
										if(nTriggerItemID<=0)
										{
											if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
											{
											//insert
												char* chValue = dbSet.EncodeQuotes(szValue);
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, promolite_eventid, Event_ActionID, value, flags, analysis_id) values \
(%d, %d, %d, '%s', %d, %d)",
													ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nPromoLiteEventItemID, nEventActionID, 
													chValue?chValue:szValue, 
													ulFlags
												);
												if(chValue) free(chValue);
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_ANALYZE) 	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug2", "near SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing near SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}
												//else break;
											}
										}
										else
										{
											// update
											if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
											{
												char* chValue = dbSet.EncodeQuotes(szValue);
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, promolite_eventid = %d, Event_ActionID = %d, value = '%s', flags = %d, analysis_id = %d WHERE analyzed_trigger_id = %d",
														ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
														nAutomationItemID, nPromoLiteEventItemID, nEventActionID, 
														chValue?chValue:szValue, 
														ulFlags, nAnalysisID, nTriggerItemID
													);
												if(chValue) free(chValue);
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_ANALYZE) 	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "near SQL: %s", szSQL);  //Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing near SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}
											}
										}
										}
										else
										{
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_ANALYZE) 	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "if((!ppromolite->m_data.m_bNearEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)) = true");  Sleep(50);
										}
									}
									else
									{
										// just set to renanalyzed
										ulFlags |= PROMOLITE_FLAG_REANALYZED;
//										ulFlags = pChannelObj->m_nAnalysisCounter;

										if((!pChannelObj->m_bNearEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
										if(nTriggerItemID<=0)
										{
											if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
											{
											//insert
												char* chValue = dbSet.EncodeQuotes(szValue);
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, promolite_eventid, Event_ActionID, value, flags, analysis_id) values \
(%d, %d, %d, '%s', %d, %d)",
													ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nPromoLiteEventItemID, nEventActionID, 
													chValue?chValue:szValue,
													ulFlags, nAnalysisID
												);
												if(chValue) free(chValue);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing near SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}// else break;
											}
										}
										else
										{
											// update
											char* chValue = dbSet.EncodeQuotes(szValue);
											if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, promolite_eventid = %d, Event_ActionID = %d, value = '%s', flags = %d, analysis_id=%d WHERE analyzed_trigger_id = %d",
														ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
														nAutomationItemID, nPromoLiteEventItemID, nEventActionID, 
														chValue?chValue:szValue,
														ulFlags, nAnalysisID, nTriggerItemID
													);
												if(chValue) free(chValue);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing near SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}
											}
										}

									}
								}
						
						if((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
						{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Analysis moving next");  Sleep(50); //(Dispatch message)
							prs->MoveNext();
						}
					} //while(!prs->IsEOF())
					prs->Close();
					delete prs;
					prs = NULL;
					if(nNumRec==0)
					{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Analysis waiting");  Sleep(50); //(Dispatch message)
						// no records. just wait a few seconds before hitting the db again.
						int ixi=0;
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_ANALYZE) g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "No analysis items, waiting for changes"); //(Dispatch message)
						while((!pChannelObj->m_bNearEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)&&(ixi<5000))  // max of 5 second pause on no records.
						{
	_ftime(&pChannelObj->m_timebNearTick); // the last time check inside the thread
							Sleep(1);
							ixi++;
						}
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Analysis finished waiting");  Sleep(50); //(Dispatch message)
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_ANALYZE) g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "finished waiting for changes after no analyzed items found X %d %d %d", ppromolite->m_data.m_bNearEventsChanged, g_bKillThread, pChannelObj->m_bKillAutomationThread); //(Dispatch message)
					}

				}//if(prs != NULL)
				else
				{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Near analysis resultset was null!"); // Sleep(50);//(Dispatch message)
	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_SENTINEL)
	{
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Analysis ERROR for %s:%d:  %s",pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum, errorstring); // Sleep(50);//(Dispatch message)
	}
	else
	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_HELIOS)
	{
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Analysis ERROR for Omnibus stream %d:  %s",pChannelObj->m_nChannelID, errorstring); // Sleep(50);//(Dispatch message)
	}

				}

			}
		} //		if(
//			  (ppromolite->m_data.m_nIndexAutomationEndpoint>=0)
//			&&(ppromolite->m_settings.m_nNumEndpointsInstalled>0)
///			&&(ppromolite->m_settings.m_ppEndpointObject)
//			&&(ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint])
//			&&(!ppromolite->m_data.m_bProcessSuspended)
//			&&(ppromolite->m_data.m_key.m_bValid)  // must have a valid license
//			&&(
//					(!ppromolite->m_data.m_key.m_bExpires)
//				||((ppromolite->m_data.m_key.m_bExpires)&&(!ppromolite->m_data.m_key.m_bExpired))
//				||((ppromolite->m_data.m_key.m_bExpires)&&(ppromolite->m_data.m_key.m_bExpireForgiveness)&&(ppromolite->m_data.m_key.m_ulExpiryDate+ppromolite->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
//				)
//			&&(
//					(!ppromolite->m_data.m_key.m_bMachineSpecific)
//				||((ppromolite->m_data.m_key.m_bMachineSpecific)&&(ppromolite->m_data.m_key.m_bValidMAC))
//				)
//			)

		//Sleep(1); // dont peg processor
	} // while((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
	
	Sleep(60);
	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_SENTINEL)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending analysis thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	}
	else
	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_HELIOS)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending analysis thread for Omnibus stream %d", pChannelObj->m_nChannelID);
	}
	g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLiteAnalysisThread", errorstring);    //(Dispatch message)

	pChannelObj->m_bNearAnalysisThreadStarted=false;
	_endthread();
	Sleep(150);

}

#endif //#ifndef NOT_USING_NEAR_ANALYSIS




void PromoLiteGlobalAutomationThread(void* pvArgs)
{
	CPromoLiteMain* ppromolite = (CPromoLiteMain*) pvArgs;
	if(ppromolite==NULL) return;

	ppromolite->m_data.m_bAutomationThreadStarted=true;

	g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLiteGlobalAutomationThread", "Beginning global automation analysis thread");    //(Dispatch message)

	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");
	CDBUtil db;

	CDBconn* pdbConn = db.CreateNewConnection(ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:automation_database_connect", errorstring);  //(Dispatch message)
			pdbConn = ppromolite->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW); 
		ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:automation_database_init", errorstring);  //(Dispatch message)
		pdbConn = ppromolite->m_data.m_pdbConn;

		//**MSG
	}


	if((pdbConn)&&(pdbConn->m_bConnected))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", //HARDCODE
			((ppromolite->m_settings.m_pszAnalysis)&&(strlen(ppromolite->m_settings.m_pszAnalysis)))?ppromolite->m_settings.m_pszAnalysis:"Analysis");


		if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//**MSG
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

		}
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", //HARDCODE
			((ppromolite->m_settings.m_pszAnalyzedTriggerData)&&(strlen(ppromolite->m_settings.m_pszAnalyzedTriggerData)))?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData");


		if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//**MSG
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

		}
	}



	bool bChangesPending = false;
	_timeb timebPending;
	_timeb timebChange;
	_ftime(&timebChange);
	timebChange.time -= (ppromolite->m_settings.m_nAnalyzeRulesIntervalMS/1000)+1;
	bool bLastProcessSuspended = 	ppromolite->m_data.m_bProcessSuspended;
	bool bTiming = false;

	while(!g_bKillThread)
	{
		// check automation list for changes (mod)
		bool bChanges = false;
		bool bForceTiming = false;

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "%d %d %d %d %d.%03d", ppromolite->m_data.m_nIndexAutomationEndpoint, ppromolite->m_settings.m_nNumEndpointsInstalled, ppromolite->m_settings.m_ppEndpointObject, ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint], ppromolite->m_data.m_timebTick.time, ppromolite->m_data.m_timebTick.millitm);   Sleep(50);//(Dispatch message)

		if(
			  (!ppromolite->m_data.m_bProcessSuspended)
			&&(ppromolite->m_settings.m_ppEndpointObject)
			&&(ppromolite->m_data.m_nIndexAutomationEndpoint>=0)
			&&(ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint])
			&&(ppromolite->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!ppromolite->m_data.m_key.m_bExpires)
				||((ppromolite->m_data.m_key.m_bExpires)&&(!ppromolite->m_data.m_key.m_bExpired))
				||((ppromolite->m_data.m_key.m_bExpires)&&(ppromolite->m_data.m_key.m_bExpireForgiveness)&&(ppromolite->m_data.m_key.m_ulExpiryDate+ppromolite->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!ppromolite->m_data.m_key.m_bMachineSpecific)
				||((ppromolite->m_data.m_key.m_bMachineSpecific)&&(ppromolite->m_data.m_key.m_bValidMAC))
				)

			)
		{

			if(bLastProcessSuspended != ppromolite->m_data.m_bProcessSuspended)
			{

				bLastProcessSuspended = ppromolite->m_data.m_bProcessSuspended;
				if(!bLastProcessSuspended)
				{
					// we've resumed so clear the analysis table.
					if((pdbConn)&&(pdbConn->m_bConnected))
					{
						char szSQL[DB_SQLSTRING_MAXLEN];
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", //HARDCODE
							((ppromolite->m_settings.m_pszAnalysis)&&(strlen(ppromolite->m_settings.m_pszAnalysis)))?ppromolite->m_settings.m_pszAnalysis:"Analysis");


						if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
							//**MSG
	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

						}
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", //HARDCODE
							((ppromolite->m_settings.m_pszAnalyzedTriggerData)&&(strlen(ppromolite->m_settings.m_pszAnalyzedTriggerData)))?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData");


						if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
							//**MSG
	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

						}
					}

				}
			}


			CPromoLiteEndpointObject* pAutoObj = ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint];

	_ftime(&ppromolite->m_data.m_timebAutomationTick); // the last time check inside the thread

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "*** Timing check 01: %d", clock()); // Sleep(250); //(Dispatch message)


			if(
				  (pAutoObj->m_pszLiveEvents)&&(strlen(pAutoObj->m_pszLiveEvents))
				&&(// only do on interval
				    (ppromolite->m_data.m_timebTick.time > (timebChange.time+ppromolite->m_settings.m_nAnalyzeRulesIntervalMS/1000))
					||(
					    (ppromolite->m_data.m_timebTick.time==(timebChange.time+ppromolite->m_settings.m_nAnalyzeRulesIntervalMS/1000))
						&&(ppromolite->m_data.m_timebTick.millitm>(timebChange.millitm+ppromolite->m_settings.m_nAnalyzeRulesIntervalMS%1000))
						)
					)
				)
			{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Checking %s", pAutoObj->m_pszLiveEvents);   Sleep(50);//(Dispatch message)
				pAutoObj->m_nModLiveEvents = pAutoObj->CheckDatabaseMod(&db, pdbConn, pAutoObj->m_pszLiveEvents);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Checked mod %s = %d, %d last", pAutoObj->m_pszLiveEvents, pAutoObj->m_nModLiveEvents, pAutoObj->m_nLastModLiveEvents);   Sleep(50);//(Dispatch message)
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "*** Timing check 02: %d", clock()); // Sleep(250); //(Dispatch message)


				if((pAutoObj->m_nModLiveEvents>=0)&&(pAutoObj->m_nModLiveEvents!=pAutoObj->m_nLastModLiveEvents))
				{
					pAutoObj->m_nLastModLiveEvents = pAutoObj->m_nModLiveEvents;
					ppromolite->m_data.m_dblLastAutomationChange = (double)ppromolite->m_data.m_timebAutomationTick.time + ((double)ppromolite->m_data.m_timebAutomationTick.millitm)/1000.0;
					bChanges = true;

					if(!bChangesPending)
					{
						_ftime(&timebChange); // initialize this
					}

					bChangesPending = true;
					_ftime(&timebPending);
					timebPending.time += ppromolite->m_settings.m_nAnalyzeRulesDwellMS/1000;
					timebPending.millitm += ppromolite->m_settings.m_nAnalyzeRulesDwellMS%1000;
					if(timebPending.millitm>999)
					{
						timebPending.time++;
						timebPending.millitm -= 1000;
					}
					if(bTiming) bForceTiming=true;
				}
			}

	
			_ftime( &ppromolite->m_data.m_timebTick );  // we're still alive.

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "*** Timing check 03: %d", clock()); // Sleep(250); //(Dispatch message)
				// immediate changes.
			if(bChanges)
			{
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_PROCESS) 	
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Automation changes exist %d", pAutoObj->m_nModLiveEvents);  // Sleep(50);//(Dispatch message)
				bChanges = false;

			}
			else
			{
				// if no changes, and no changes pending, check elapsed time and if expired, run the timing col queries again.
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_PROCESS) 	
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "No changes exist %d, force:%d, pending:%d, timing:%d. now:%d.%03d, pending:%d.%03d", pAutoObj->m_nModLiveEvents, bForceTiming, bChangesPending, bTiming,ppromolite->m_data.m_timebTick.time,ppromolite->m_data.m_timebTick.millitm, timebPending.time, timebPending.millitm);  // Sleep(50);//(Dispatch message)
				if(
						(bForceTiming)
					||(
							(!bChangesPending)
						&&(bTiming)
						&&(
								(ppromolite->m_data.m_timebTick.time>timebPending.time)
							||(
									(ppromolite->m_data.m_timebTick.time==timebPending.time)
								&&(ppromolite->m_data.m_timebTick.millitm>timebPending.millitm)
								)
							)
						)
					)
				{
					bForceTiming=false;

					EnterCriticalSection(&ppromolite->m_data.m_critEventRules);
					int nEventRuleRecord = 0; \
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "executing %d event rule query sets: %d", ppromolite->m_data.m_nNumEventRuleQueries); // Sleep(250); //(Dispatch message)
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "*** Timing check 04: %d", clock()); // Sleep(250); //(Dispatch message)

if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TIMING) 	
{ g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "TIMING col array %d, num %d", ppromolite->m_data.m_ppszTimingColName, ppromolite->m_data.m_nNumTimingColNames); }

					while((ppromolite->m_data.m_ppszTimingColName)&&(nEventRuleRecord<ppromolite->m_data.m_nNumTimingColNames)&&(!g_bKillThread))
					{
_ftime(&ppromolite->m_data.m_timebAutomationTick); // the last time check inside the thread
						if(ppromolite->m_data.m_ppszTimingColName[nEventRuleRecord])
						{
//									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", ppromolite->m_data.m_ppszTimingColName[nEventRuleRecord]->sz_query1);
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "update %s.dbo.%s set trigger_time = cast(TriggerTimingView.%s as decimal(20,3)) \
+ cast(ActionTriggerTime/1000.00 as decimal(20,3)) \
from %s.dbo.%s as AnalyzedTriggerData, (select * from %s.dbo.TriggerTimingView where promolite_Timing_Col_Name = '%s') as TriggerTimingView \
where AnalyzedTriggerData.automation_event_itemid = TriggerTimingView.itemid and TriggerTimingView.event_actionid = AnalyzedTriggerData.event_actionid", 

									(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
									(g_ppromolite->m_settings.m_pszAnalyzedTriggerData?g_ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"),
									*(ppromolite->m_data.m_ppszTimingColName[nEventRuleRecord]),


									(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
									(g_ppromolite->m_settings.m_pszAnalyzedTriggerData?g_ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"),

									(g_ppromolite->m_settings.m_pszCortexDB?g_ppromolite->m_settings.m_pszCortexDB:"Cortex"),
			//						(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
			//						(g_ppromolite->m_settings.m_pszTriggerAnalysisView?g_ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView"),
									*(ppromolite->m_data.m_ppszTimingColName[nEventRuleRecord])//,


			//						(g_ppromolite->m_settings.m_pszCortexDB?g_ppromolite->m_settings.m_pszCortexDB:"Cortex"),
			//						(g_ppromolite->m_settings.m_pszLiveEventData?g_ppromolite->m_settings.m_pszLiveEventData:"LiveEventData")
									
									);

//, %s.dbo.%s as LiveEventData
//triggeranalysisview.analyzed_trigger_id = AnalyzedTriggerData.analyzed_trigger_id and \ 


/*   update promolite.dbo.AnalyzedTriggerData set trigger_time = cast(LiveEventData.[replace with col_name] as decimal(20,3))\
+ cast(ActionTriggerTime/1000.00 as decimal(20,3)) from \
(select * from promolite.dbo.triggeranalysisview where promolite_Timing_Col_Name = '[replace with col_name]') as triggeranalysisview, \
promolite.dbo.AnalyzedTriggerData as AnalyzedTriggerData, cortex.dbo.LiveEventData as LiveEventData \
where triggeranalysisview.analyzed_trigger_id = AnalyzedTriggerData.analyzed_trigger_id and AnalyzedTriggerData.automation_event_itemid = LiveEventData.itemid 
*/

if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TIMING) 	
{ g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "TIMING SQL [%d]: %s", nEventRuleRecord, szSQL); }

							EnterCriticalSection(&ppromolite->m_data.m_critSQL);
							if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
							{
			ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR, retrying SQL: %s", errorstring); // Sleep(250); //(Dispatch message)
								Sleep(50); // try again.

								if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
								{
											//**MSG
			ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

								}
							}
							LeaveCriticalSection(&ppromolite->m_data.m_critSQL);
							_ftime(&timebPending);
							timebPending.time += ppromolite->m_settings.m_nAnalyzeTimingDwellMS/1000;
							timebPending.millitm += ppromolite->m_settings.m_nAnalyzeTimingDwellMS%1000;


						}
						nEventRuleRecord++;
					}
 					LeaveCriticalSection(&ppromolite->m_data.m_critEventRules);
					// moved here from below.
					bTiming = false;
					ppromolite->m_data.m_bNearEventsChanged = true; // kicks off parameter analysis thread
				}

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "*** Timing check 05: %d", clock()); // Sleep(250); //(Dispatch message)

			}

if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_PROCESS) 	
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Mod %d, force:%d, pending:%d, timing:%d. now:%d.%03d, pending:%d.%03d, analyze:%d.%03d", pAutoObj->m_nModLiveEvents, bForceTiming, bChangesPending, bTiming,
	ppromolite->m_data.m_timebTick.time,ppromolite->m_data.m_timebTick.millitm, 
	timebPending.time, timebPending.millitm,
	(timebChange.time+ppromolite->m_settings.m_nAnalyzeRulesForceMS/1000),(timebChange.millitm+ppromolite->m_settings.m_nAnalyzeRulesForceMS%1000) );  // Sleep(50);//(Dispatch message)
			// dwell changes - another change will reset the time, suppressing the previous change
			if(
					(	ppromolite->m_data.m_bForceAnalysis)
				||(
						(bChangesPending)
					&&(
							(ppromolite->m_data.m_timebTick.time>timebPending.time)
						||(
								(ppromolite->m_data.m_timebTick.time==timebPending.time)
							&&(ppromolite->m_data.m_timebTick.millitm>timebPending.millitm)
							)
						||(ppromolite->m_data.m_timebTick.time > (timebChange.time+ppromolite->m_settings.m_nAnalyzeRulesForceMS/1000))
						||(
								(ppromolite->m_data.m_timebTick.time==(timebChange.time+ppromolite->m_settings.m_nAnalyzeRulesForceMS/1000))
							&&(ppromolite->m_data.m_timebTick.millitm>(timebChange.millitm+ppromolite->m_settings.m_nAnalyzeRulesForceMS%1000))
							)
						)
					)
				)
			{
				ppromolite->m_data.m_bForceAnalysis = false;
        // do this AFTER the queries have run
//				ppromolite->m_data.m_bNearEventsChanged = true; // kicks off parameter analysis thread
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Automation changes exist %d (BUT WERE PENDING)", pAutoObj->m_nModLiveEvents);  // Sleep(50);//(Dispatch message)
				// ppromolite->m_data.m_nEventLastMax = 0; // start at the beginning....
// actually dont reset this.
				bChangesPending = false;
				bool bSQLError = false;
				// if changes
				// {
				//   Close the m_prsEvents recordset and delete it, set it back to NULL.

//				ppromolite->m_data.ReleaseRecordSet();

				if(
					  ((pAutoObj->m_usType&PROMOLITE_DEP_AUTO_MASK)==PROMOLITE_DEP_AUTO_HELIOS)
					||((pAutoObj->m_usType&PROMOLITE_DEP_AUTO_MASK)==PROMOLITE_DEP_AUTO_SENTINEL)
					)
				{
				// run the rules procedure

//		EnterCriticalSection(&ppromolite->m_data.m_critSQL);
/*
					if(db.ExecuteSQL(pdbConn, 
						g_ppromolite->m_settings.m_pszAnalyzeRulesProc?g_ppromolite->m_settings.m_pszAnalyzeRulesProc:"spAnalyzeRules", 
						errorstring)<DB_SUCCESS)
					{
						bSQLError = true;
								//**MSG
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

					}
*/

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = 0",
						(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
						(g_ppromolite->m_settings.m_pszAnalysis?g_ppromolite->m_settings.m_pszAnalysis:"Analysis")
						);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "EVENT rule SQL: %s", szSQL); // Sleep(250); //(Dispatch message)

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "*** Timing check 10: %d", clock()); // Sleep(250); //(Dispatch message)
					EnterCriticalSection(&ppromolite->m_data.m_critSQL);
					if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						bSQLError = true;
								//**MSG
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

					}
					LeaveCriticalSection(&ppromolite->m_data.m_critSQL);

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "*** Timing check 11: %d", clock()); // Sleep(250); //(Dispatch message)

					if(!bSQLError)
					{
						EnterCriticalSection(&ppromolite->m_data.m_critEventRules);
						int nEventRuleRecord = 0;
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "executing %d event rule query sets: %d", ppromolite->m_data.m_nNumEventRuleQueries); // Sleep(250); //(Dispatch message)
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "*** Timing check 12: %d", clock()); // Sleep(250); //(Dispatch message)
						while((ppromolite->m_data.m_ppEventRuleQuery)&&(nEventRuleRecord<ppromolite->m_data.m_nNumEventRuleQueries)&&(!g_bKillThread))
						{
	_ftime(&ppromolite->m_data.m_timebAutomationTick); // the last time check inside the thread
							if(ppromolite->m_data.m_ppEventRuleQuery[nEventRuleRecord])
							{
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", ppromolite->m_data.m_ppEventRuleQuery[nEventRuleRecord]->sz_query1);

if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_EVENTRULE) 	
{ g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:eventrule", "EVENT rule SQL [%d]#1: %s", nEventRuleRecord, szSQL);}

								EnterCriticalSection(&ppromolite->m_data.m_critSQL);
								if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
								{
									bSQLError = true;
											//**MSG
			ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

								}
								else
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", ppromolite->m_data.m_ppEventRuleQuery[nEventRuleRecord]->sz_query2);
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_EVENTRULE) 	
{ g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:eventrule", "EVENT rule SQL [%d]#2: %s", nEventRuleRecord, szSQL);}

									if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
									{
										bSQLError = true;
												//**MSG
				ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  //Sleep(250); //(Dispatch message)

									}
								}
								LeaveCriticalSection(&ppromolite->m_data.m_critSQL);
							}
							nEventRuleRecord++;
						}
 						LeaveCriticalSection(&ppromolite->m_data.m_critEventRules);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "*** Timing check 13: %d", clock()); // Sleep(250); //(Dispatch message)

						if(!bSQLError)
						{

							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s where status = 0",
									(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
									(g_ppromolite->m_settings.m_pszAnalysis?g_ppromolite->m_settings.m_pszAnalysis:"Analysis")
								);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "EVENT rule SQL: %s", szSQL); // Sleep(250); //(Dispatch message)
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "*** Timing check 14: %d", clock()); // Sleep(250); //(Dispatch message)
							EnterCriticalSection(&ppromolite->m_data.m_critSQL);
							if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
							{
								bSQLError = true;
										//**MSG
		ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  //Sleep(250); //(Dispatch message)

							}
							LeaveCriticalSection(&ppromolite->m_data.m_critSQL);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "*** Timing check 15: %d", clock()); // Sleep(250); //(Dispatch message)

							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s where cast(promolite_eventid as varchar(32)) \
+ cast(automation_event_itemid as varchar(32)) not in (select cast(promolite_eventid as varchar(32)) + cast(automation_event_itemid as varchar(32)) \
from %s.dbo.%s)",
									(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
									(g_ppromolite->m_settings.m_pszAnalyzedTriggerData?g_ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"),
									(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
									(g_ppromolite->m_settings.m_pszAnalysis?g_ppromolite->m_settings.m_pszAnalysis:"Analysis")
								);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "EVENT rule SQL: %s", szSQL); // Sleep(250); //(Dispatch message)
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "*** Timing check 16: %d", clock()); // Sleep(250); //(Dispatch message)
							EnterCriticalSection(&ppromolite->m_data.m_critSQL);
							if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
							{
								bSQLError = true;
										//**MSG
		ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  //Sleep(250); //(Dispatch message)

							}
							LeaveCriticalSection(&ppromolite->m_data.m_critSQL);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "*** Timing check 17: %d", clock()); // Sleep(250); //(Dispatch message)


							EnterCriticalSection(&ppromolite->m_data.m_critEventRules);
							nEventRuleRecord = 0; //re-use
	//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "executing %d event rule query sets: %d", ppromolite->m_data.m_nNumEventRuleQueries); // Sleep(250); //(Dispatch message)
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "*** Timing check 18: %d", clock()); // Sleep(250); //(Dispatch message)
							while((ppromolite->m_data.m_ppszTimingColName)&&(nEventRuleRecord<ppromolite->m_data.m_nNumTimingColNames)&&(!g_bKillThread))
							{
		_ftime(&ppromolite->m_data.m_timebAutomationTick); // the last time check inside the thread
								if(ppromolite->m_data.m_ppszTimingColName[nEventRuleRecord])
								{


									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "update %s.dbo.%s set trigger_time = cast(TriggerTimingView.%s as decimal(20,3)) \
+ cast(ActionTriggerTime/1000.00 as decimal(20,3)) \
from %s.dbo.%s as AnalyzedTriggerData, (select * from %s.dbo.TriggerTimingView where promolite_Timing_Col_Name = '%s') as TriggerTimingView \
where AnalyzedTriggerData.automation_event_itemid = TriggerTimingView.itemid and TriggerTimingView.event_actionid = AnalyzedTriggerData.event_actionid", 

											(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
											(g_ppromolite->m_settings.m_pszAnalyzedTriggerData?g_ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"),
											*ppromolite->m_data.m_ppszTimingColName[nEventRuleRecord],


											(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
											(g_ppromolite->m_settings.m_pszAnalyzedTriggerData?g_ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"),

											(g_ppromolite->m_settings.m_pszCortexDB?g_ppromolite->m_settings.m_pszCortexDB:"Cortex"),
					//						(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
					//						(g_ppromolite->m_settings.m_pszTriggerAnalysisView?g_ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView"),
											*ppromolite->m_data.m_ppszTimingColName[nEventRuleRecord]//,


					//						(g_ppromolite->m_settings.m_pszCortexDB?g_ppromolite->m_settings.m_pszCortexDB:"Cortex"),
					//						(g_ppromolite->m_settings.m_pszLiveEventData?g_ppromolite->m_settings.m_pszLiveEventData:"LiveEventData")
											
											);


/*
//									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", ppromolite->m_data.m_ppszTimingColName[nEventRuleRecord]->sz_query1);
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "update %s.dbo.%s set trigger_time = cast(LiveEventData.%s as decimal(20,3)) \
+ cast(ActionTriggerTime/1000.00 as decimal(20,3)) from \
(select * from %s.dbo.%s where promolite_Timing_Col_Name = '%s') as triggeranalysisview, \
%s.dbo.%s as AnalyzedTriggerData, %s.dbo.%s as LiveEventData \
where triggeranalysisview.analyzed_trigger_id = AnalyzedTriggerData.analyzed_trigger_id and \
AnalyzedTriggerData.automation_event_itemid = LiveEventData.itemid", 

											(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
											(g_ppromolite->m_settings.m_pszAnalyzedTriggerData?g_ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"),
											*ppromolite->m_data.m_ppszTimingColName[nEventRuleRecord],

											(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
											(g_ppromolite->m_settings.m_pszTriggerAnalysisView?g_ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView"),
											*ppromolite->m_data.m_ppszTimingColName[nEventRuleRecord],

											(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
											(g_ppromolite->m_settings.m_pszAnalyzedTriggerData?g_ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"),

											(g_ppromolite->m_settings.m_pszCortexDB?g_ppromolite->m_settings.m_pszCortexDB:"Cortex"),
											(g_ppromolite->m_settings.m_pszLiveEventData?g_ppromolite->m_settings.m_pszLiveEventData:"LiveEventData")
											
											);
*/
									
/*   update promolite.dbo.AnalyzedTriggerData set trigger_time = cast(LiveEventData.[replace with col_name] as decimal(20,3))\
+ cast(ActionTriggerTime/1000.00 as decimal(20,3)) from \
(select * from promolite.dbo.triggeranalysisview where promolite_Timing_Col_Name = '[replace with col_name]') as triggeranalysisview, \
promolite.dbo.AnalyzedTriggerData as AnalyzedTriggerData, cortex.dbo.LiveEventData as LiveEventData \
where triggeranalysisview.analyzed_trigger_id = AnalyzedTriggerData.analyzed_trigger_id and AnalyzedTriggerData.automation_event_itemid = LiveEventData.itemid 
*/

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "TIMING SQL [%d]: %s", nEventRuleRecord, szSQL); // Sleep(250); //(Dispatch message)
									EnterCriticalSection(&ppromolite->m_data.m_critSQL);
									if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
									{
										Sleep(50); // try again.

										if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
										{
											bSQLError = true;
													//**MSG
					ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

										}
									}
									LeaveCriticalSection(&ppromolite->m_data.m_critSQL);
									_ftime(&timebPending);
									timebPending.time += ppromolite->m_settings.m_nAnalyzeTimingDwellMS/1000;
									timebPending.millitm += ppromolite->m_settings.m_nAnalyzeTimingDwellMS%1000;


								}
								nEventRuleRecord++;
							}
 							LeaveCriticalSection(&ppromolite->m_data.m_critEventRules);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "*** Timing check 19: %d", clock()); // Sleep(250); //(Dispatch message)
						}

					}


//		LeaveCriticalSection(&ppromolite->m_data.m_critSQL);

/*
					if(!bSQLError) 
					{
						pAutoObj->m_nLastModLiveEvents = pAutoObj->m_nModLiveEvents;
						ppromolite->m_data.m_nEventLastMax=ppromolite->m_data.m_nNumberOfEvents-1; // in case the automation changes reduce the number of events, we still want to analyze below.
					}
*/
					// Sleep(1); 
				}  // type was sentinel
				// else automation type unknown, so nothing

				// moved here from above, to trigger analysis thread AFTER the queries have run.
//				ppromolite->m_data.m_bNearEventsChanged = true; // kicks off parameter analysis thread
				// actually realized, want to move this to after the timing queries.

				bTiming = true; 

			} // automation changes exist
/*
/////////////////////////////////////////////////////////
			else  // no automation changes
			{

				//DEMO
				// just update the time based on estimate
				_timeb timebTempTime; // the harris server time
				_ftime( &timebTempTime); 
				
				ppromolite->m_data.m_dblAutomationTimeEstimate = ppromolite->m_data.m_dblHarrisTime + ((double)(ppromolite->m_data.m_nHarrisTimeDiffMS)/1000.0) + (double)ppromolite->m_settings.m_nTriggerAdvanceMS/1000.0
					+ ((double)timebTempTime.time - (double)ppromolite->m_data.m_timebHarrisCheckTimeLocal.time)
					+ ((double)timebTempTime.millitm/1000.0 - (double)ppromolite->m_data.m_timebHarrisCheckTimeLocal.millitm/1000.0); // the difference beween now and last check
//										ppromolite->m_data.m_dblAutomationTimeEstimate = (double)timebTempTime.time;
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Automation estimate time now %f", ppromolite->m_data.m_dblAutomationTimeEstimate);  Sleep(50);//(Dispatch message)

			}
*/
		} // if license etc


		Sleep(1); // dont peg processor
	} // while((!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
	ppromolite->m_data.m_bAutomationThreadStarted=false;


	if((pdbConn)&&(pdbConn->m_bConnected))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", //HARDCODE
			((ppromolite->m_settings.m_pszAnalysis)&&(strlen(ppromolite->m_settings.m_pszAnalysis)))?ppromolite->m_settings.m_pszAnalysis:"Analysis");


		if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//**MSG
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

		}
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", //HARDCODE
			((ppromolite->m_settings.m_pszAnalyzedTriggerData)&&(strlen(ppromolite->m_settings.m_pszAnalyzedTriggerData)))?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData");


		if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//**MSG
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

		}
	}


	g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLiteGlobalAutomationThread", "Ending global automation analysis thread");    //(Dispatch message)

	Sleep(50);//(Dispatch message)
	_endthread();
	Sleep(150);

}

/*
void PromoLiteFarAnalysisThread(void* pvArgs)
{
	CPromoLiteAutomationChannelObject* pChannelObj = (CPromoLiteAutomationChannelObject*) pvArgs;
	if(pChannelObj==NULL) return;
	CPromoLiteEndpointObject* pEndObj = (CPromoLiteEndpointObject*) pChannelObj->m_pEndpoint;
	if(pEndObj==NULL) return;
	CPromoLiteMain* ppromolite = (CPromoLiteMain*) pChannelObj->m_pPromoLite;
	if(ppromolite==NULL) return;

	pChannelObj->m_bFarAnalysisThreadStarted=true;
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");
	CDBUtil db;
	CDBUtil dbSet;

	CDBconn* pdbConn = db.CreateNewConnection(ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:analysis_database_connect", errorstring);  //(Dispatch message)
			pdbConn = ppromolite->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW); 
		ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:analysis_database_init", errorstring);  //(Dispatch message)
		pdbConn = ppromolite->m_data.m_pdbConn;

		//**MSG
	}

	CDBconn* pdbConnSet = dbSet.CreateNewConnection(ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW);
	if(pdbConnSet)
	{
		if(dbSet.ConnectDatabase(pdbConnSet, errorstring)<DB_SUCCESS)
		{
			ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:analysis_database2_connect", errorstring);  //(Dispatch message)
			pdbConnSet = ppromolite->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW); 
		ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:analysis_database2_init", errorstring);  //(Dispatch message)
		pdbConnSet = ppromolite->m_data.m_pdbConn;

		//**MSG
	}

	if(pdbConn)
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM Analysis");
		while((db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
		{
		//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)
		}
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
				ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"
			);
		while((db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
		{
		//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)
		}
	}

	double delta = 0.0;
	double wait = 0.0;
	while((!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
	{
		if(
			  (ppromolite->m_data.m_nIndexAutomationEndpoint>=0)
			&&(ppromolite->m_settings.m_nNumEndpointsInstalled>0)
			&&(ppromolite->m_data.m_nIndexAutomationEndpoint<ppromolite->m_settings.m_nNumEndpointsInstalled)
			&&(ppromolite->m_settings.m_ppEndpointObject)
			&&(ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint])
			&&(!ppromolite->m_data.m_bProcessSuspended)
			&&(ppromolite->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!ppromolite->m_data.m_key.m_bExpires)
				||((ppromolite->m_data.m_key.m_bExpires)&&(!ppromolite->m_data.m_key.m_bExpired))
				||((ppromolite->m_data.m_key.m_bExpires)&&(ppromolite->m_data.m_key.m_bExpireForgiveness)&&(ppromolite->m_data.m_key.m_ulExpiryDate+ppromolite->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!ppromolite->m_data.m_key.m_bMachineSpecific)
				||((ppromolite->m_data.m_key.m_bMachineSpecific)&&(ppromolite->m_data.m_key.m_bValidMAC))
				)

			)
		{
//			CPromoLiteEndpointObject* pAutoObj = ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint];

			//Here, we analyze things that have not yet played, that are not yet analyzed, unless its within m_ulTriggerCueThreshold seconds of play time, then it gets reanalyzed
	_ftime(&ppromolite->m_data.m_timebFarTick); // the last time check inside the thread

			ppromolite->m_data.m_bFarEventsChanged = false;
			while (
							(!ppromolite->m_data.m_bFarEventsChanged)
						&&(ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint]->m_dblLastAutomationChange + () <(ppromolite->m_data.m_timebFarTick.time+ ((double)(ppromolite->m_data.m_timebFarTick.time)) + (double)(ppromolite->m_data.m_timebFarTick.millitm)/1000.0) )
						&&(!g_bKillThread)
						&&(!pAobj->m_bKillAutomationThread)
						)
			{
				// just get the exports with params to parse.
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"SELECT * FROM %s WHERE TriggerTime > %lf AND event_status = 0 AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY TriggerTime ASC",
						ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
						ppromolite->m_data.m_dblAutomationTimeEstimate+(double)ppromolite->m_settings.m_ulTriggerCueThreshold,
						PROMOLITE_FLAG_ANALYZED
					);
	_ftime(&ppromolite->m_data.m_timebFarTick); // the last time check inside the thread

					
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis SQL: %s", szSQL); // Sleep(50);//(Dispatch message)
				CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
				int nNumRec = 0;
				if(prs != NULL)
				{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis resultset not null %d records", prs->GetRecordCount( )); // Sleep(50);//(Dispatch message)

					while((!ppromolite->m_data.m_bFarEventsChanged)&&(!prs->IsEOF())&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
					{
	_ftime(&ppromolite->m_data.m_timebFarTick); // the last time check inside the thread
						nNumRec++;
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis resultset Got %d items so far", nNumRec); // Sleep(50);//(Dispatch message)

						CString szName;
						CString szValue;
						CString szTemp;
						//double dblTime;
						double dblClipTime;
						double dblEventTime;
						int nTriggerItemID=-1;
						int nAutomationItemID=-1;
						int nPromoLiteEventItemID=-1;
						int nEventActionID=-1;
						int nType=-1;
						int nAnalysisID=-1;
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis getting itemid"); 

						prs->GetFieldValue("itemid", szTemp);
						if(szTemp.GetLength()) nAutomationItemID = atoi(szTemp);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got itemid %s", szTemp); 

						// Host
						// ParsedValue
					//	prs->GetFieldValue("TriggerTime", szTemp);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got TriggerTime %s", szTemp); 
						//dblTime=atof(szTemp);
						prs->GetFieldValue("analyzed_trigger_id", szTemp);
						if(szTemp.GetLength()) nTriggerItemID = atoi(szTemp);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got analyzed_trigger_id %s", szTemp); 

						prs->GetFieldValue("AnalyzedTriggerDataFlags", szTemp);
						unsigned long ulFlags = atol(szTemp);
						//AnalyzedTriggerDataStatus
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got AnalyzedTriggerDataFlags %s", szTemp); 

						prs->GetFieldValue("promolite_EventID", szTemp);
						if(szTemp.GetLength()) nPromoLiteEventItemID = atoi(szTemp);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got PromoLite_EventID %s", szTemp); 
						//PromoLite_Timing_Col_Name
						//Source
						//Scene
						prs->GetFieldValue("Graphic_Action_Type", szTemp);
						nType = atoi(szTemp);
						//Action_Name

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got Graphic_Action_Type %s", szTemp); 

						prs->GetFieldValue("Event_ActionID", szTemp);
						if(szTemp.GetLength()) nEventActionID = atoi(szTemp);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got Event_ActionID %s", szTemp); 

						//Action_Value
						prs->GetFieldValue("Action_Value", szValue);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got Action_Value %s", szValue); 
					
						//Analysis_ID
						prs->GetFieldValue("Analysis_ID", szTemp);
						if(szTemp.GetLength()) nAnalysisID = atoi(szTemp);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got Analysis_ID %s", szTemp); 

						prs->GetFieldValue("event_calc_start", szTemp);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got TriggerTime %s", szTemp); 
						dblClipTime=atof(szTemp);

						prs->GetFieldValue("event_start", szTemp);
						dblEventTime=atof(szTemp);

						bool bCueTime = false;
						/*
						if(ppromolite->m_data.m_dblAutomationTimeEstimate+((double)ppromolite->m_settings.m_ulTriggerCueThreshold)>=dblTime)
						{
							bCueTime = true;
						}
						else
						{
							bCueTime = false;
						}
						* /
	//					if(
	//							((bCueTime)&&(!(ulFlags&PROMOLITE_FLAG_REANALYZED)))
	//						||((!bCueTime)&&(!(ulFlags&PROMOLITE_FLAG_ANALYZED)))
	//						)
						{
								if(nType == 0) // anim
								{
									// just set to 
				//					if(bCueTime)
				//						ulFlags |= PROMOLITE_FLAG_REANALYZED;
				//					else
									ulFlags |= PROMOLITE_FLAG_ANALYZED;
									if(nTriggerItemID<=0)
									{
										if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
										{
											//insert
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, promolite_eventid, event_actionid, flags, analysis_id) values \
(%d, %d, %d, %d, %d)",
													ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nPromoLiteEventItemID, nEventActionID, ulFlags, nAnalysisID
												);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug1", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
											if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
											{
											//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing far SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
											} //else break;

										}
									}
									else
									{
										// update
										if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
										{
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, promolite_eventid = %d, Event_ActionID = %d, flags = %d, analysis_id=%d WHERE analyzed_trigger_id = %d",
													ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nPromoLiteEventItemID, nEventActionID, ulFlags, nAnalysisID, nTriggerItemID
												);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
											if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
											{
											//**MSG
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing far SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
											}
										}
									}
								}
								else
								if(nType == 1) // export
								{
									if((szValue.GetLength())&&(szValue.Find("�")>=0))
									{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "FAR value has a �: %s", szValue);  Sleep(50);//(Dispatch message)
										// we have a least one param to parse
										// just set to renanalyzed
					//					if(bCueTime)
					///						ulFlags |= PROMOLITE_FLAG_REANALYZED;
					//					else
										ulFlags |= PROMOLITE_FLAG_ANALYZED;

										// lets parse out szValue and set it up;

										CString szPreParse = szValue;
										szValue = "";

										bool bInsBrk = false;

										int i=0;
										while((i<szPreParse.GetLength())&&(!ppromolite->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)) 
										{
											char ch = szPreParse.GetAt(i);
											if(ch == '�')
											{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "FAR ch has a �");  Sleep(50);//(Dispatch message)

												CString szParam = "";
												CString szStyle = "";
												i++;
												ch = szPreParse.GetAt(i);

												bool bStyle = false;
												while((ch != '�')&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
												{
													if(!bStyle)
													{
														szParam += ch;
													}
													else
													{
														szStyle += ch;
													}
													i++;
													ch = szPreParse.GetAt(i);
													if(ch == ':')
													{
														i++;
														bStyle = true;
														ch = szPreParse.GetAt(i);
													}
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "FAR Param %s, Style %s", szParam,szStyle);  Sleep(50);//(Dispatch message)


												}
												i++;

												// ok here, we should have the param name and possible style.
												// first lets get the param.
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "FAR FINAL Param %s, Style %s", szParam,szStyle);  Sleep(50);//(Dispatch message)

/*												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s '%s', %f",
														ppromolite->m_settings.m_pszGetParameterValueProc?ppromolite->m_settings.m_pszGetParameterValueProc:"spGetParameterValue",  
														szParam, dblTime
													);
* /
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
/*
												// this sets the param
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
											//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}

												// this gets it!
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM [Output]");
* /
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
			
// try 2 was like this.												
/*												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "{CALL %s('%s', %f)}",
														ppromolite->m_settings.m_pszGetParameterValueProc?ppromolite->m_settings.m_pszGetParameterValueProc:"spGetParameterValue",  
														szParam, dblTime
													);

* /
												
// this is try 3												
												if((!ppromolite->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)) 
												{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far parameter retrieve index for [%s]", szParam);  Sleep(50);//(Dispatch message)
													EnterCriticalSection(&ppromolite->m_data.m_critParameterRules);
													int idx = ppromolite->m_data.GetParameterQueryIndex(szParam);

//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far parameter retrieved index %d", idx);  Sleep(50);//(Dispatch message)
													if(idx>=0)
													{
														CString timestr;  
														if(ppromolite->m_data.m_ppParameterRule[idx]->sz_target_valueid.Compare("0"))
														{
															if(ppromolite->m_settings.m_bFarAnalysisUsesCalc)
																timestr.Format("%.3f", dblClipTime);
															else
																timestr.Format("%.3f", dblEventTime);
															
														}
														else
														{
															if(ppromolite->m_settings.m_bFarAnalysisUsesCalc)
																timestr.Format("%.3f", dblClipTime-0.020);
															else
																timestr.Format("%.3f", dblEventTime-0.020);
														}
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s%s%s",
																ppromolite->m_data.m_ppParameterRuleQuery[idx]->sz_query1,
																timestr,
																ppromolite->m_data.m_ppParameterRuleQuery[idx]->sz_query2
															);
														
//if(ppromolite->m_settings.m_bUseEmail) ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "**** FAR � get param (%s) Query: %s",szParam, szSQL);  Sleep(50);//(Dispatch message)
													}

													LeaveCriticalSection(&ppromolite->m_data.m_critParameterRules);
													
													CRecordset* prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);


//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� SQL ERROR %s", errorstring);  Sleep(50);//(Dispatch message)


/*

												bool bGotit=false;
												CString target_valueid, target_criterion_col_name, criterion_value, returned_property_col_name, default_value;

_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select target_valueid, target_criterion_col_name, criterion_value, returned_property_col_name, [default_value] from [Parameters] where [name] = '%s'",szParam);
//_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from Analysis");

ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
CRecordset* prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);

												if(prsParam != NULL)
												{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� not null");  Sleep(50);//(Dispatch message)
													if((!prsParam->IsEOF())&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
													{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� NEOF");  Sleep(50);//(Dispatch message)
														try
														{
															prsParam->GetFieldValue("target_valueid", target_valueid);
															prsParam->GetFieldValue("target_criterion_col_name", target_criterion_col_name);
															prsParam->GetFieldValue("criterion_value", criterion_value);
															prsParam->GetFieldValue("returned_property_col_name", returned_property_col_name);
															prsParam->GetFieldValue("default_value", default_value);

															bGotit=true;
														}
														catch(CException *e)// CDBException *e, CMemoryException *m)  
														{
															if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
															{
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);  Sleep(50);//(Dispatch message)
															}
															else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
															{
																// The error code is in e->m_nRetCode
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: out of memory");
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
															}
															else 
															{
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught other exception.\n%s", szSQL);
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
															}
															e->Delete();
														} 
														catch( ... )
														{
														}
													}
													prsParam->Close();
													delete prsParam;
												}

if(bGotit)
{
_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top 1 val, datatype from (select top ' + %s + ' (case when len(' + %s + ') > 0 THEN ' + %s + ' ELSE ''' + %s + ''' END) as val, event_start, (select datatype from MetaConfigUnionALLView \
where col_name = ''' + %s + ''') as datatype from cortex.dbo.LiveEventData as LiveEventData where event_start > ' + %f + ' and \
(event_status is NULL or event_status = 0) and ' + %s + ' = ''' + %s + ''' order by event_start) as Temp order by event_start desc'",
					target_valueid,returned_property_col_name,returned_property_col_name,
					default_value,returned_property_col_name,
					dblTime,target_criterion_col_name,criterion_value
					);
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "2� SQL %s", szSQL);  Sleep(50);//(Dispatch message)

prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);




}
else
{
	prsParam = NULL;
}
* /

													if(prsParam != NULL)
													{
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "FAR � parameter retrieve not null");  Sleep(50);//(Dispatch message)
														if((!prsParam->IsEOF())&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
														{
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "FAR � parameter retrieve NEOF");  Sleep(50);//(Dispatch message)
															CString szDataType;

															try
															{
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "FAR � about to: prsParam->GetFieldValue(\"val\", szTemp);");  Sleep(50);//(Dispatch message)
//																prsParam->GetFieldValue((short)0, szTemp);  //"val" field name
																prsParam->GetFieldValue("val", szTemp);  //"val" field name
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "FAR � parameter got %s for %s",szTemp, szSQL);  Sleep(50);//(Dispatch message)
//																prsParam->GetFieldValue((short)1, szDataType);  // "datatype" field name
																prsParam->GetFieldValue("datatype", szDataType);  // "datatype" field name
//if(ppromolite->m_settings.m_bUseEmail) 	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "FAR � got %s (datatype = %s) for %s, %s",szTemp, szDataType, szSQL, szPreParse);  Sleep(50);//(Dispatch message)
	//															prsParam->GetFieldValue("value", szTemp);  // for use with output table
	//															prsParam->GetFieldValue("datatype", szTemp);  // sp now returns datatype

															}
															catch(CException *e)// CDBException *e, CMemoryException *m)  
															{
																if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
																{
	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);  Sleep(50);//(Dispatch message)
																}
																else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
																{
																	// The error code is in e->m_nRetCode
	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: out of memory");
										//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
																}
																else 
																{
	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught other exception.\n%s", szSQL);
										//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
																}
																e->Delete();
																szTemp=""; // blank to be safe
															} 
															catch( ... )
															{
	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught other exception.\n%s", szSQL);
																szTemp=""; // blank to be safe
															}



			//												szTemp = "Parameter!";

//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "FAR � got %s",szTemp);  Sleep(50);//(Dispatch message)
															if(szTemp.GetLength())
															{
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "FAR � returned %s", szTemp);  Sleep(50);//(Dispatch message)
																unsigned long ulTime = (unsigned long)(atof(szTemp));
	//															bool bTime = (ulTime>1)?true:false;
																bool bTime = (szDataType.CompareNoCase("Time")==0)?true:false;
																if(bTime)
																{
																	ulTime%=86400;
																	szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "szParam =  %s", szParam);  Sleep(50);//(Dispatch message)
																}
																bool bPM = true;
																if(ulTime<43200) bPM = false;
																
		
																if(szStyle.GetLength())
																{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "szStyle =  %s", szStyle);  Sleep(50);//(Dispatch message)


																	// now have to get a list of styles
																	prsParam->Close();
																	delete prsParam;

																	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select actions from promolite.dbo.styles as styles where name = '%s'", szStyle);
	//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "action far SQL: %s", szSQL);  Sleep(50); //(Dispatch message)

																	prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "action far SQL error?: %s", errorstring);  Sleep(50); //(Dispatch message)

																	int nNumStyles = 0;
																	
																	szParam = "";
																	bool bAdded = false;
																	//unsigned long ulTimeTemp = ulTime;
																	if(prsParam != NULL)
																	{

																		if((!prsParam->IsEOF())&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
																		{
																			prsParam->GetFieldValue("actions", szStyle);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "action got: %s", szStyle);  Sleep(50); //(Dispatch message)


																			CSafeBufferUtil sbu;
																			char actions[2048];
																			sprintf(actions, "%s", szStyle);
																			char* paction = sbu.Token(actions, strlen(actions), "|");

																			while((paction)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
																			{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "action got: %s", paction);  Sleep(50); //(Dispatch message)
																				if(stricmp(paction, "snapTo(5)")==0)
																				{
																					nNumStyles++;

																					unsigned long ulTimeTemp = ((ulTime + 150)/300)*300;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(15)")==0)
																				{
																					nNumStyles++;
																					unsigned long ulTimeTemp = ((ulTime + 450)/900)*900;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(30)")==0)
																				{
																					nNumStyles++;
																					unsigned long ulTimeTemp = ((ulTime + 900)/1800)*1800;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(60)")==0)
																				{
																					unsigned long ulTimeTemp = ((ulTime +1800)/3600)*3600;
																					ulTime=ulTimeTemp;
																					nNumStyles++;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "clock(24)")==0)
																				{
																					szTemp.Format("%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																					nNumStyles++;
																				}
																				else 
																				if(stricmp(paction, "clock(12)")==0)
																				{
																					nNumStyles++;
																					if(ulTime>=43200) ulTime-=43200;
																					szTemp.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "clock(12): %s", szTemp);  Sleep(50); //(Dispatch message)
																				}
																				else 
																				if(stricmp(paction, "formatTime('H:MM:SS')")==0)
																				{
																					nNumStyles++;
																					szParam.Format("%d:%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH:MM:SS')")==0)
																				{
																					nNumStyles++;
																					szParam.Format("%02d:%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH:MM')")==0)
																				{
																					nNumStyles++;
																					szParam.Format("%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('MM')")==0)
																				{
																					nNumStyles++;
																					szParam.Format("%02d", ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('H')")==0)
																				{
																					nNumStyles++;
																					szParam.Format("%d", (ulTime)/3600); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "appendAMPM('lcase')")==0)
																				{
																					nNumStyles++;

																					szParam = szTemp;
																					szTemp.Format("%s %s", szParam, bPM?"pm":"am");
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "appendAMPM('lcase'): %s", szTemp);  Sleep(50); //(Dispatch message)
																				}
																				else 
																				if(stricmp(paction, "appendAMPM('ucase')")==0)
																				{
																					nNumStyles++;
																					szParam = szTemp;
																					szTemp.Format("%s %s", szParam, bPM?"PM":"AM");
																				}
	//else not nNumStyles++; which is why i had to do it that annoying way.


	/*
	Round,roundNumber()
	Truncate,truncateNumber()
	Filter Smut,smutFilter()
	Truncate To 20 Characters,truncateText(20)
	Truncate To 50 Characters,truncateText(50)
	Truncate To 100 Characters,truncateText(100)

	Convert All Text To Upper Case,textCase('upper')
	Convert All Text To Lower Case,textCase('lower')
	Convert First Letter Of First Word To Upper Case,textCase('sentence')
	Convert First Letter Of Text String To Upper Case,textCase('title')

	Long Date Lower Case,formatDate('longdate', 'lcase')
	Long Date Upper Case,formatDate('longdate', 'ucase')
	Short Date Lower Case,formatDate('shortdate', 'lcase')
	Short Date Upper Case,formatDate('shortdate', 'ucase')
	Celsius,formatTemp('celsius')
	Fahrenheit,formatTemp('fahrenheit')
	* /
																			
																				paction = sbu.Token(NULL, NULL, "|");
																			}
																			if(bTime) szValue += szTemp;
																		}
																		prsParam->Close();
																		delete prsParam;
																		prsParam = NULL;

																	}

																	if(nNumStyles==0)
																	{
																		if(bTime)
																		{
																			// format it the default way H:MM 24 hour clock
																			szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																			szValue += szTemp;
																		}
																		else 
																		{
																			szValue += szTemp;
																		}
																	}
																}
																else
																{
																	// if no style, need to understand if this is a time.
																	if(bTime)
																	{
																		// format it the default way H:MM 24 hour clock
																		szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																		szValue += szTemp;
																	}
																	else 
																	{
																		szValue += szTemp;
																	}
																}
															}
														}
														if(prsParam)
														{
															prsParam->Close();
															delete prsParam;
														}
													}
													else
													{
	//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� returned NULL");  Sleep(50);//(Dispatch message)

													}
												}
											}
											else
											{
												szValue += ch;
												i++;
											}
										}
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "styles done");  Sleep(50); //(Dispatch message)


										if((!ppromolite->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)) 
										{
										if(nTriggerItemID<=0)
										{
											if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
											{
											//insert

												char* chValue = dbSet.EncodeQuotes(szValue);
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, promolite_eventid, Event_ActionID, value, flags, analysis_id) values \
(%d, %d, %d, '%s', %d, %d)",
													ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nPromoLiteEventItemID, nEventActionID, 
													chValue?chValue:szValue,
													ulFlags, nAnalysisID
												);
												if(chValue) free(chValue);
//if(ppromolite->m_settings.m_bUseEmail) 	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug2", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing far SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}
												else
												{
													bInsBrk = true;
												}
											}
										}
										else
										{
											// update
											if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
											{
												char* chValue = dbSet.EncodeQuotes(szValue);
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, promolite_eventid = %d, Event_ActionID = %d, value = '%s', flags = %d, analysis_id = %d WHERE analyzed_trigger_id = %d",
														ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
														nAutomationItemID, nPromoLiteEventItemID, nEventActionID, 
													chValue?chValue:szValue,
														ulFlags, nAnalysisID, nTriggerItemID
													);
												if(chValue) free(chValue);
//if(ppromolite->m_settings.m_bUseEmail) 	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing far SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}
											}
										}
										}

/*
										int ixi=0;
										while((!ppromolite->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)&&(ixi<1000))  // 1 second pause if the SP is triggered
										{
											Sleep(1); ixi++;
										}
* /
										//if(bInsBrk) break;
									}
									else
									{
										// just set to renanalyzed
						//				if(bCueTime)
						//					ulFlags |= PROMOLITE_FLAG_REANALYZED;
						//				else
										ulFlags |= PROMOLITE_FLAG_ANALYZED;
										if((!ppromolite->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)) 
										if(nTriggerItemID<=0)
										{
											if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
											{
											//insert
												char* chValue = dbSet.EncodeQuotes(szValue);
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, promolite_eventid, Event_ActionID, value, flags, analysis_id) values \
(%d, %d, %d, '%s', %d, %d)",
													ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nPromoLiteEventItemID, nEventActionID, 
													chValue?chValue:szValue, 
													ulFlags, nAnalysisID
												);
												if(chValue) free(chValue);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing far SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												} //else break;
											}
										}
										else
										{
											// update
											if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
											{
												char* chValue = dbSet.EncodeQuotes(szValue);
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, promolite_eventid = %d, Event_ActionID = %d, value = '%s', flags = %d, analysis_id = %d WHERE analyzed_trigger_id = %d",
														ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
														nAutomationItemID, nPromoLiteEventItemID, nEventActionID, 
														chValue?chValue:szValue, 
														ulFlags, nAnalysisID, nTriggerItemID
													);
												if(chValue) free(chValue);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing far SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}
											}
										}
									}
								}
						}

						if(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread) prs->MoveNext();
						Sleep(1);
					} //while(!prs->IsEOF())
					prs->Close();
					delete prs;
					prs = NULL;

					if(nNumRec==0)
					{
						// no records. just wait.
					//	int ixi=0;
						while((!ppromolite->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))  // 2 second pause if the SP is triggered
						{
	_ftime(&ppromolite->m_data.m_timebFarTick); // the last time check inside the thread
							Sleep(1);// ixi++;
						}
					}
					else
					{
						Sleep(1);
					}
				}//if(prs != NULL)

				else
				{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis resultset was null!"); // Sleep(50);//(Dispatch message)
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis ERROR %s", errorstring); // Sleep(50);//(Dispatch message)

				}


/*
				if(delta>0.0)
				{
					wait = (double)clock();
					while((!ppromolite->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)&&(((double)clock())-wait<delta))  // wait here until there is a change
					{
						Sleep(1); // dont peg processor
					}
				}
		* /
/*
				delta = ((double)ppromolite->m_settings.m_ulTriggerCueThreshold)*500.0;
				wait = (double)clock();
				while((!ppromolite->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)&&(((double)clock())-wait<delta))  // wait here until there is a change
				{
					Sleep(1); // dont peg processor
				}
* /

				Sleep(500);
			}
		} //		if(
//			  (ppromolite->m_data.m_nIndexAutomationEndpoint>=0)
//			&&(ppromolite->m_settings.m_nNumEndpointsInstalled>0)
///			&&(ppromolite->m_settings.m_ppEndpointObject)
//			&&(ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint])
//			&&(!ppromolite->m_data.m_bProcessSuspended)
//			&&(ppromolite->m_data.m_key.m_bValid)  // must have a valid license
//			&&(
//					(!ppromolite->m_data.m_key.m_bExpires)
//				||((ppromolite->m_data.m_key.m_bExpires)&&(!ppromolite->m_data.m_key.m_bExpired))
//				||((ppromolite->m_data.m_key.m_bExpires)&&(ppromolite->m_data.m_key.m_bExpireForgiveness)&&(ppromolite->m_data.m_key.m_ulExpiryDate+ppromolite->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
//				)
//			&&(
//					(!ppromolite->m_data.m_key.m_bMachineSpecific)
//				||((ppromolite->m_data.m_key.m_bMachineSpecific)&&(ppromolite->m_data.m_key.m_bValidMAC))
//				)
//			)

		//Sleep(1); // dont peg processor
	} // while((!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))

	if(pdbConn)
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM Analysis");
		while((db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
		{
		//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)
		}
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
				ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"
			);
		while((db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
		{
		//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)
		}
	}


	ppromolite->m_data.m_bFarAnalysisThreadStarted=false;
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ending m_bFarAnalysisThreadStarted"); // Sleep(50);//(Dispatch message)
	_endthread();
	Sleep(150);

}
*/
void PromoLiteGlobalAnalysisThread(void* pvArgs)
{
	CPromoLiteMain* ppromolite = (CPromoLiteMain*) pvArgs;
	if(ppromolite==NULL) return;

	ppromolite->m_data.m_bGlobalAnalysisThreadStarted=true;

	g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLiteGlobalAnalysisThread", "Beginning global analysis thread");    //(Dispatch message)


	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];

	strcpy(errorstring, "");
	CDBUtil db;
	CDBUtil dbSet;
	CBufferUtil bu;

	CDBconn* pdbConn = db.CreateNewConnection(ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:analysis_database_connect", errorstring);  //(Dispatch message)
			pdbConn = ppromolite->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW); 
		ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:analysis_database_init", errorstring);  //(Dispatch message)
		pdbConn = ppromolite->m_data.m_pdbConn;

		//**MSG
	}

	CDBconn* pdbConnSet = dbSet.CreateNewConnection(ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW);
	if(pdbConnSet)
	{
		if(dbSet.ConnectDatabase(pdbConnSet, errorstring)<DB_SUCCESS)
		{
			ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:analysis_database2_connect", errorstring);  //(Dispatch message)
			pdbConnSet = ppromolite->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW); 
		ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:analysis_database2_init", errorstring);  //(Dispatch message)
		pdbConnSet = ppromolite->m_data.m_pdbConn;

		//**MSG
	}
/*
	while(!ppromolite->m_data.m_bNearEventsChanged)
	{
		_ftime(&ppromolite->m_data.m_timebNearTick); // the last time check inside the thread
		Sleep(30);
	}
*/
//	Sleep(30000); // dont start thread analysis util after 30 sec

	bool bChangesPending = false;
	_timeb timebPending;
	_timeb timebChange;

	double delta = 0.0;
	double wait = 0.0;

	CPromoLiteEndpointObject* pAutoObj = NULL;
	if(
			(g_ppromolite->m_settings.m_ppEndpointObject)
		&&(g_ppromolite->m_data.m_nIndexAutomationEndpoint>=0)
		&&(g_ppromolite->m_data.m_nIndexAutomationEndpoint<g_ppromolite->m_settings.m_nNumEndpointsInstalled)
		&&(g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexAutomationEndpoint])
		&&(!g_bKillThread)
		)
	{
		pAutoObj = ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint];
	}

	while((!g_bKillThread))//&&(!ppromolite->m_data.m_bKillAutomationThread))
	{
		if(
/*
			  (ppromolite->m_data.m_nIndexAutomationEndpoint>=0)
			&&(ppromolite->m_settings.m_nNumEndpointsInstalled>0)
			&&(ppromolite->m_data.m_nIndexAutomationEndpoint<ppromolite->m_settings.m_nNumEndpointsInstalled)
			&&(ppromolite->m_settings.m_ppEndpointObject)
			&&(ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint])
			&&*/
			  (!ppromolite->m_data.m_bProcessSuspended)
//			&&(pChannelObj->m_nListState&((1<<LISTTHREADING)|(1<<LISTISPLAYING)))  // list must be threaded and playing to run analysis
			&&(ppromolite->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!ppromolite->m_data.m_key.m_bExpires)
				||((ppromolite->m_data.m_key.m_bExpires)&&(!ppromolite->m_data.m_key.m_bExpired))
				||((ppromolite->m_data.m_key.m_bExpires)&&(ppromolite->m_data.m_key.m_bExpireForgiveness)&&(ppromolite->m_data.m_key.m_ulExpiryDate+ppromolite->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!ppromolite->m_data.m_key.m_bMachineSpecific)
				||((ppromolite->m_data.m_key.m_bMachineSpecific)&&(ppromolite->m_data.m_key.m_bValidMAC))
				)

			)
		{

// moved the following line upwards and out of the loop.  no need to keep rechecking it here, this gets set once.
//			CPromoLiteEndpointObject* pAutoObj = ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint];

			//Here, we analyze things that have not yet played, that are not yet analyzed, unless its within m_ulTriggerCueThreshold seconds of play time, then it gets reanalyzed
			_ftime(&ppromolite->m_data.m_timebNearTick); // the last time check inside the thread

			if(ppromolite->m_data.m_bNearEventsChanged)
			{
				ppromolite->m_data.m_bNearEventsChanged = false;
				if(!bChangesPending)
				{
					_ftime(&timebChange); // initialize this
				}
				bChangesPending = true;
				_ftime(&timebPending);
				timebPending.time += ppromolite->m_settings.m_nAnalyzeParameterDwellMS/1000;
				timebPending.millitm += ppromolite->m_settings.m_nAnalyzeParameterDwellMS%1000;
				if(timebPending.millitm>999)
				{
					timebPending.time++;
					timebPending.millitm -= 1000;
				}
			}
			// dwell changes - another change will reset the time, suppressing the previous change
			if(
				  (bChangesPending)
				&&(
				    (ppromolite->m_data.m_timebNearTick.time>timebPending.time)
					||(
					    (ppromolite->m_data.m_timebNearTick.time==timebPending.time)
						&&(ppromolite->m_data.m_timebNearTick.millitm>timebPending.millitm)
						)
					||(ppromolite->m_data.m_timebTick.time > (timebChange.time+ppromolite->m_settings.m_nAnalyzeParameterForceMS/1000))
					||(
					    (ppromolite->m_data.m_timebTick.time==(timebChange.time+ppromolite->m_settings.m_nAnalyzeParameterForceMS/1000))
						&&(ppromolite->m_data.m_timebTick.millitm>(timebChange.millitm+ppromolite->m_settings.m_nAnalyzeParameterForceMS%1000))
						)
					)
				)
			{
				bChangesPending = false;

				// have to reanalyze all

//				UPDATE AnalyzedTriggerData SET set flags = (flags & (~32));			

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET flags = (flags & (~%d)) WHERE status & %d = 0",
						ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the AnalyzedTriggerData table name
						PROMOLITE_FLAG_REANALYZED, 
						PROMOLITE_FLAG_TRIGGERED
					);
					if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
				//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
					}


			}

			while((!ppromolite->m_data.m_bNearEventsChanged)&&(!bChangesPending)&&(!g_bKillThread))//&&(!ppromolite->m_data.m_bKillAutomationThread))
			{
	_ftime(&ppromolite->m_data.m_timebNearTick); // the last time check inside the thread
				// just get the exports with params to parse.
/*
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
			//		  "select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE TriggerTime > %lf and ((event_status & 19) = 0) and ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY TriggerTime ASC) ORDER BY TriggerTime ASC",
//						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_calc_start > %u) and ((event_status & 19) = 0)) ORDER BY event_calc_start ASC)  and ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY TriggerTime ASC",
						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_status & 19) = 0) ORDER BY event_calc_start ASC) and server = '%s' AND listid = %d AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY TriggerTime ASC",
//						"SELECT TOP 1 * FROM %s WHERE TriggerTime > %lf and event_status & 19 = 0 and flags & %d = 0 ORDER BY TriggerTime ASC",
						ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
					//	(unsigned long)ppromolite->m_data.m_dblAutomationTimeEstimate,
						ppromolite->m_data.m_pszServerName, ppromolite->m_data.m_nHarrisListID,	PROMOLITE_FLAG_REANALYZED
					);
*/


/*
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_status & 19) = 0) and channelid = %d AND AnalyzedTriggerDataFlags <> %d  ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
						ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
						ppromolite->m_data.m_nChannelID, ppromolite->m_data.m_nAnalysisCounter
					);
*/
				if((pAutoObj==NULL)||((pAutoObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_SENTINEL)) // defaults to sentinel
				{
	
//itemid, TriggerTime, analyzed_trigger_id, AnalyzedTriggerDataFlags, promolite_EventID, Graphic_Action_Type, Event_ActionID, Action_Value, Analysis_ID, event_calc_start, event_start					
/*
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_status & 19) = 0) \
AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
							ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							PROMOLITE_FLAG_REANALYZED
						);
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select itemid, TriggerTime, analyzed_trigger_id, AnalyzedTriggerDataFlags, promolite_EventID, Graphic_Action_Type, Event_ActionID, Action_Value, Analysis_ID, event_calc_start, parent_calc_start, event_start from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_status & 19) = 0) \
AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
							ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							PROMOLITE_FLAG_REANALYZED
						);
*/
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select itemid, TriggerTime, analyzed_trigger_id, AnalyzedTriggerDataFlags, promolite_EventID, Graphic_Action_Type, Event_ActionID, Action_Value, Analysis_ID, event_calc_start, parent_calc_start, event_start \
from GlobalAnalysisView where itemid in (SELECT TOP 1 itemid FROM GlobalAnalysisView WHERE ((event_status & %d) = 0) \
AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
//							ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							ppromolite->m_settings.m_nAnalyzeHarrisStatusExclude,
							PROMOLITE_FLAG_REANALYZED
						);

				//			need top x where top event item id is all same where event status & 19 =0 (not play, post, done)
				}
				else
				if((pAutoObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_HELIOS)
				{
		

/*

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE event_status < 10 and event_status > 4 \
AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
							ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							PROMOLITE_FLAG_REANALYZED
						);

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select itemid, TriggerTime, analyzed_trigger_id, AnalyzedTriggerDataFlags, promolite_EventID, Graphic_Action_Type, Event_ActionID, Action_Value, Analysis_ID, event_calc_start, parent_calc_start, event_start from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE event_status < 10 and event_status > 4 \
AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
							ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							PROMOLITE_FLAG_REANALYZED
						);
*/

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select itemid, TriggerTime, analyzed_trigger_id, AnalyzedTriggerDataFlags, promolite_EventID, Graphic_Action_Type, Event_ActionID, Action_Value, Analysis_ID, event_calc_start, parent_calc_start, event_start \
from GlobalAnalysisView where itemid in (SELECT TOP 1 itemid FROM GlobalAnalysisView WHERE event_status < 10 and event_status > 4 \
AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
//							ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							PROMOLITE_FLAG_REANALYZED
						);

				//			need top x where top event item id is all same where event status < 10  and event_status > 4 (not done. no errors, and allocated or better in system)
				}


//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Global analysis SQL: %s", szSQL); // Sleep(50);//(Dispatch message)
				int nNumRec = 0;
				CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
				if(prs != NULL)
				{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Near analysis resultset not null %d record", prs->GetRecordCount( )); // Sleep(50);//(Dispatch message)
					while((!ppromolite->m_data.m_bNearEventsChanged)&&(!prs->IsEOF())&&(!g_bKillThread))//&&(!ppromolite->m_data.m_bKillAutomationThread))
					{
	_ftime(&ppromolite->m_data.m_timebNearTick); // the last time check inside the thread
						nNumRec++;
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Near analysis resultset Got %d items so far", nNumRec); // Sleep(50);//(Dispatch message)
						CString szName;
						CString szValue;
						CString szTemp;
						//double dblTime;
						double dblClipTime;
						double dblEventTime;
						double dblParentClipTime;
						int nTriggerItemID=-1;
						int nAutomationItemID=-1;
						int nPromoLiteEventItemID=-1;
						int nEventActionID=-1;
						int nType=-1;
						int nAnalysisID=-1;

						prs->GetFieldValue("itemid", szTemp);
						if(szTemp.GetLength()) nAutomationItemID = atoi(szTemp);

						// Host
						// ParsedValue
						//prs->GetFieldValue("TriggerTime", szTemp);
						//dblTime=atof(szTemp);
						prs->GetFieldValue("analyzed_trigger_id", szTemp);
						if(szTemp.GetLength()) nTriggerItemID = atoi(szTemp);

						prs->GetFieldValue("AnalyzedTriggerDataFlags", szTemp);
						unsigned long ulFlags = atol(szTemp);
						//AnalyzedTriggerDataStatus

						prs->GetFieldValue("promolite_EventID", szTemp);
						if(szTemp.GetLength()) nPromoLiteEventItemID = atoi(szTemp);
						//PromoLite_Timing_Col_Name
						//Source
						//Scene
						prs->GetFieldValue("Graphic_Action_Type", szTemp);
						nType = atoi(szTemp);
						//Action_Name


						prs->GetFieldValue("Event_ActionID", szTemp);
						if(szTemp.GetLength()) nEventActionID = atoi(szTemp);

						//Action_Value
						prs->GetFieldValue("Action_Value", szValue);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Near Analysis value is: %s", szValue);//  Sleep(50);//(Dispatch message)
					
						prs->GetFieldValue("Analysis_ID", szTemp);
						if(szTemp.GetLength()) nAnalysisID = atoi(szTemp);

						prs->GetFieldValue("event_calc_start", szTemp);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got TriggerTime %s", szTemp); 
						dblClipTime=atof(szTemp);


						prs->GetFieldValue("parent_calc_start", szTemp);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got parent_calc_start %s", szTemp); 
						dblParentClipTime=atof(szTemp);

						prs->GetFieldValue("event_start", szTemp);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Far analysis got TriggerTime %s", szTemp); 
						dblEventTime=atof(szTemp);

								if(nType == 0) // anim
								{
									// just set to 
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "anim is retrieved");//  Sleep(50);//(Dispatch message)
									ulFlags |= (PROMOLITE_FLAG_REANALYZED|PROMOLITE_FLAG_ANALYZED);
								//	ulFlags = ppromolite->m_data.m_nAnalysisCounter;

									if(nTriggerItemID<=0)
									{
										if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
										{
											//insert
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, promolite_eventid, event_actionid, flags, analysis_id) values \
(%d, %d, %d, %d, %d)",
													ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nPromoLiteEventItemID, nEventActionID, ulFlags, nAnalysisID
												);
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_ANALYZE) 	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug1", "Global Analysis SQL (anim): %s", szSQL);  Sleep(50); //(Dispatch message)
											if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
											{
											//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing near SQL: %s", errorstring); // Sleep(50); //(Dispatch message)
											}//else break;

										}
									}
									else
									{
										// update
										if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
										{
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, promolite_eventid = %d, Event_ActionID = %d, flags = %d, analysis_id = %d WHERE analyzed_trigger_id = %d",
													ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nPromoLiteEventItemID, nEventActionID, ulFlags, nAnalysisID, nTriggerItemID
												);
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_ANALYZE) 	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Global Analysis SQL (anim): %s", szSQL); // Sleep(50); //(Dispatch message)
											if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
											{
											//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing near SQL: %s", errorstring);//  Sleep(50); //(Dispatch message)
											}
										}
									}
								}
								else
								if(nType == 1) // export
								{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "export %s is retrieved", szValue);//  Sleep(50);//(Dispatch message)
									if(
											(szValue.GetLength())
										&&(
												((ppromolite->m_settings.m_bUseUTF8)&&(szValue.Find("§")>=0))
											||((!ppromolite->m_settings.m_bUseUTF8)&&(szValue.Find("�")>=0))
											)
										)
									{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "NEAR value has a �: %s", szValue);  Sleep(50);//(Dispatch message)
										// we have a least one param to parse
										// just set to renanalyzed
										ulFlags |= (PROMOLITE_FLAG_REANALYZED|PROMOLITE_FLAG_ANALYZED);
							//			ulFlags = ppromolite->m_data.m_nAnalysisCounter;

										// lets parse out szValue and set it up;

										CString szPreParse = szValue;
										szValue = "";

										int i=0;

/*
UTF-8 has can take anywhere from 1 to 4 bytes per character. One byte UTF-8 characters are just 7-bit ASCII, 
a subset of the ISO and Windows sets, so they are left alone. The rest of the characters match byte patterns 
in certain ranges which are unlikely to appear in regular text. They can be described in a compact and 
machine-readable way by a regular expression:

var pattern = /[\xC2-\xDF][\x80-\xBF]|[\xE0-\xEF][\x80-\xBF]{2}|[\xF0-\xF4][\x80-\xBF]{3}/g

Broken down, the regular expression matches one of:

   1. A byte between 0xC2 and 0xDF, followed by a byte between 0�80 and 0xBF, or
   2. A byte between 0xE0 and 0xEF, followed by 2 bytes between 0�80 and 0xBF, or
   3. A byte between 0xF0 and 0xF4, followed by 3 bytes between 0�80 and 0xBF.

These correspond to 2-, 3-, and 4-byte UTF-8 characters.


*/



										while((i<szPreParse.GetLength())&&(!ppromolite->m_data.m_bNearEventsChanged)&&(!g_bKillThread))//&&(!ppromolite->m_data.m_bKillAutomationThread))
										{
											bool bDelimFound=false;
											char ch = szPreParse.GetAt(i);
											if(ppromolite->m_settings.m_bUseUTF8)
											{
												if((ch == '�')&&(szPreParse.GetAt(i+1) == '�'))	
												{
													bDelimFound=true; i+=2;
												}
											}
											else
											{
												if(ch == '�')
												{
													bDelimFound=true; i++;
												}
											}

											if(bDelimFound)
											{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "NEAR ch has a �");  Sleep(50);//(Dispatch message)

												CString szParam = "";
												CString szStyle = "";

												bDelimFound=false;												
												ch = szPreParse.GetAt(i);

												bool bStyle = false;  

												if(ppromolite->m_settings.m_bUseUTF8)
												{
													if((ch == '�')&&(szPreParse.GetAt(i+1) == '�'))	
													{
														bDelimFound=true; i+=2;
													}
												}
												else
												{
													if(ch == '�')
													{
														bDelimFound=true; i++;
													}
												}

												
												while((!bDelimFound)&&(i<szPreParse.GetLength())&&(!g_bKillThread))//&&(!ppromolite->m_data.m_bKillAutomationThread))
												{
													if(!bStyle)
													{
														szParam += ch;
													}
													else
													{
														szStyle += ch;
													}
													i++;
													ch = szPreParse.GetAt(i);
													if(ch == ':')
													{
														i++;
														bStyle = true;
														ch = szPreParse.GetAt(i);
													}

													if(ppromolite->m_settings.m_bUseUTF8)
													{
														if((ch == '�')&&(szPreParse.GetAt(i+1) == '�'))	
														{
															bDelimFound=true; i+=2;
														}
													}
													else
													{
														if(ch == '�')
														{
															bDelimFound=true; i++;
														}
													}

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "near Param %s, Style %s", szParam,szStyle);  Sleep(50);//(Dispatch message)

												}

if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_PARAMPARSE) 	
	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "(global) FINAL Param %s, Style %s in value [%s]", szParam, szStyle, szValue);  //Sleep(50);//(Dispatch message)
												// ok here, we should have the param name and possible style.
												// first lets get the param.

/*												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s '%s', %f",
														ppromolite->m_settings.m_pszGetParameterValueProc?ppromolite->m_settings.m_pszGetParameterValueProc:"spGetParameterValue",  
														szParam, dblTime
													);
*/

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
/*
												// this sets the param
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
											//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}

												// this gets it!
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM [Output]");
*/
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
			
// try 2
/*
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "{CALL %s('%s', %f)}",
														ppromolite->m_settings.m_pszGetParameterValueProc?ppromolite->m_settings.m_pszGetParameterValueProc:"spGetParameterValue",  
														szParam, dblTime
													);

*/												
				// try 3								
											
												if((!ppromolite->m_data.m_bNearEventsChanged)&&(!g_bKillThread))//&&(!ppromolite->m_data.m_bKillAutomationThread))												
												{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Near parameter retrieve index for [%s]", szParam); // Sleep(50);//(Dispatch message)
													EnterCriticalSection(&ppromolite->m_data.m_critParameterRules);
													int idx = ppromolite->m_data.GetParameterQueryIndex(szParam);


	//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Near parameter retrieved index %d", idx); // Sleep(50);//(Dispatch message)
													if(idx>=0)
													{
														CString timestr;  
														if(ppromolite->m_data.m_ppParameterRule[idx]->sz_target_valueid.Compare("0"))
														{
															if(ppromolite->m_settings.m_bNearAnalysisUsesCalc)
																timestr.Format("%.3f", dblClipTime);
															else
																timestr.Format("%.3f", dblEventTime);
														}
														else
														{
													//		if(ppromolite->m_settings.m_bNearAnalysisUsesCalc)
													//			timestr.Format("%.3f", dblClipTime-0.020);
													//		else
																timestr.Format("%.3f", dblParentClipTime-0.020);  // now should always be the real time
														}
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s%s%s",
																ppromolite->m_data.m_ppParameterRuleQuery[idx]->sz_query1,
																timestr,
																ppromolite->m_data.m_ppParameterRuleQuery[idx]->sz_query2
															);
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_PARAMPARSE) 
 	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "(global) (time=%s) get param (%s) Query: %s", timestr, szParam, szSQL); // Sleep(50);//(Dispatch message)
													}

													LeaveCriticalSection(&ppromolite->m_data.m_critParameterRules);
												
													
													
													CRecordset* prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);






//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� SQL ERROR %s", errorstring);  Sleep(50);//(Dispatch message)


/*

												bool bGotit=false;
												CString target_valueid, target_criterion_col_name, criterion_value, returned_property_col_name, default_value;

_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select target_valueid, target_criterion_col_name, criterion_value, returned_property_col_name, [default_value] from [Parameters] where [name] = '%s'",szParam);
//_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from Analysis");

ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
CRecordset* prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);

												if(prsParam != NULL)
												{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� not null");  Sleep(50);//(Dispatch message)
													if((!prsParam->IsEOF())&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
													{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� NEOF");  Sleep(50);//(Dispatch message)
														try
														{
															prsParam->GetFieldValue("target_valueid", target_valueid);
															prsParam->GetFieldValue("target_criterion_col_name", target_criterion_col_name);
															prsParam->GetFieldValue("criterion_value", criterion_value);
															prsParam->GetFieldValue("returned_property_col_name", returned_property_col_name);
															prsParam->GetFieldValue("default_value", default_value);

															bGotit=true;
														}
														catch(CException *e)// CDBException *e, CMemoryException *m)  
														{
															if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
															{
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);  Sleep(50);//(Dispatch message)
															}
															else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
															{
																// The error code is in e->m_nRetCode
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: out of memory");
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
															}
															else 
															{
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught other exception.\n%s", szSQL);
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
															}
															e->Delete();
														} 
														catch( ... )
														{
														}
													}
													prsParam->Close();
													delete prsParam;
												}

if(bGotit)
{
_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top 1 val, datatype from (select top ' + %s + ' (case when len(' + %s + ') > 0 THEN ' + %s + ' ELSE ''' + %s + ''' END) as val, event_start, (select datatype from MetaConfigUnionALLView \
where col_name = ''' + %s + ''') as datatype from cortex.dbo.LiveEventData as LiveEventData where event_start > ' + %f + ' and \
(event_status is NULL or event_status = 0) and ' + %s + ' = ''' + %s + ''' order by event_start) as Temp order by event_start desc'",
					target_valueid,returned_property_col_name,returned_property_col_name,
					default_value,returned_property_col_name,
					dblTime,target_criterion_col_name,criterion_value
					);
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "2� SQL %s", szSQL);  Sleep(50);//(Dispatch message)

prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);




}
else
{
	prsParam = NULL;
}
*/

													if(prsParam != NULL)
													{
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "NEAR ************** 2� not null"); // Sleep(50);//(Dispatch message)
														if((!prsParam->IsEOF())&&(!g_bKillThread))//&&(!ppromolite->m_data.m_bKillAutomationThread))
														{
	//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "2� NEOF");  Sleep(50);//(Dispatch message)

															CString szDataType;
															try
															{
																prsParam->GetFieldValue((short)0, szTemp);  //"val" field name
																prsParam->GetFieldValue((short)1, szDataType);  // "datatype" field name
	//															prsParam->GetFieldValue("value", szTemp);  // for use with output table
	//															prsParam->GetFieldValue("datatype", szDataType);  // sp now returns datatype
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_PARAMPARSE)
	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "(global) param got %s (datatype = %s) for %s, %s",szTemp, szDataType, szSQL, szPreParse);  Sleep(50);//(Dispatch message)

															}
															catch(CException *e)// CDBException *e, CMemoryException *m)  
															{
																if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
																{
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);  Sleep(50);//(Dispatch message)
																}
																else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
																{
																	// The error code is in e->m_nRetCode
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: out of memory");
										//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
																}
																else 
																{
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught other exception.\n%s", szSQL);
										//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
																}
																e->Delete();
																szTemp=""; // blank to be safe
															} 
															catch( ... )
															{
																szTemp=""; // blank to be safe
															}



			//												szTemp = "Parameter!";

	//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� got %s",szTemp);  Sleep(50);//(Dispatch message)
															if(szTemp.GetLength())
															{
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� returned %s", szTemp);  Sleep(50);//(Dispatch message)
																double dblNumber = atof(szTemp);
																unsigned long ulTime = (unsigned long)(dblNumber);
	//															bool bTime = (ulTime>1)?true:false;
																bool bTime = (szDataType.CompareNoCase("Time")==0)?true:false;
																if(bTime)
																{
																	ulTime%=86400;
																	szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
	//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "szParam =  %s", szParam);  Sleep(50);//(Dispatch message)
																}
																bool bPM = true;
																if(ulTime<43200) bPM = false;
																
		
																if(szStyle.GetLength())
																{

																	// now have to get a list of styles
																	prsParam->Close();
																	delete prsParam;

																	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select actions from %s.dbo.styles as styles where name = '%s'",
																		(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
																		szStyle
																		);
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_PARAMPARSE) 	
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "(global) Applying style [%s] to param [%s] in event value [%s]", szStyle, szParam, szValue);

																	prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);

	//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "action near SQL error?: %s", errorstring);  Sleep(50); //(Dispatch message)

																	int nNumStyles = 0;
																	
																	szParam = "";
																	bool bAdded = false;
																	//unsigned long ulTimeTemp = ulTime;
																	if(prsParam != NULL)
																	{

																		if((!prsParam->IsEOF())&&(!g_bKillThread))//&&(!ppromolite->m_data.m_bKillAutomationThread))
																		{
																			prsParam->GetFieldValue("actions", szStyle);
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_PARAMPARSE) 
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "(global) style action retrieved: %s", szStyle); // Sleep(50); //(Dispatch message)


																			CSafeBufferUtil sbu;
																			char actions[2048];
																			sprintf(actions, "%s", szStyle);
																			char* paction = sbu.Token(actions, strlen(actions), "|");
																			bool b24 = false;

																			while((paction)&&(!g_bKillThread))//&&(!ppromolite->m_data.m_bKillAutomationThread))
																			{
																				if((strnicmp(paction, "snapTo(", 7)==0)&&(strlen(paction)>7))
																				{
																					int nMinutes = atoi(paction+7)*60;
																					nNumStyles++;

																					unsigned long ulTimeTemp = ((ulTime + 150)/nMinutes)*nMinutes;
																					ulTime=ulTimeTemp;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00:%02d", ((ulTime)%3600)/60); // special case 00
																						else
																							szParam.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					}
																					else
																						szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
																				}
																				else 
/*
																				if(stricmp(paction, "snapTo(5)")==0)
																				{
																					nNumStyles++;

																					unsigned long ulTimeTemp = ((ulTime + 150)/300)*300;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(15)")==0)
																				{
																					nNumStyles++;
																					unsigned long ulTimeTemp = ((ulTime +450)/900)*900;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(30)")==0)
																				{
																					nNumStyles++;
																					unsigned long ulTimeTemp = ((ulTime+900)/1800)*1800;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(60)")==0)
																				{
																					unsigned long ulTimeTemp = ((ulTime+1800)/3600)*3600;
																					ulTime=ulTimeTemp;
																					nNumStyles++;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
*/
																				if(stricmp(paction, "clock(24)")==0)
																				{
																					szTemp.Format("%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																					nNumStyles++;
																					b24 = true;
																				}
																				else 
																				if(stricmp(paction, "clock(12)")==0)
																				{
																					nNumStyles++;
																					if(ulTime>=43200) ulTime-=43200;
																					szTemp.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60);
																					b24 = false;
																				}
																				else 
																				if(stricmp(paction, "formatTime('H:MM:SS')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00:%02d:%02d", ((ulTime)%3600)/60, ((ulTime)%3600)%60); // special case 00
																						else
																							szParam.Format("%d:%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					}		
																					else
																						szParam.Format("%d:%02d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH:MM:SS')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																						szParam.Format("%02d:%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					else
																						szParam.Format("%02d:%02d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH:MM')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																						szParam.Format("%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					else
																						szParam.Format("%02d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('H:MM')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00:%02d", ((ulTime)%3600)/60); // default
																						else
																							szParam.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					}
																					else
																						szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('MM')")==0)
																				{
																					nNumStyles++;
																					szParam.Format("%02d", ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('H')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00"); // default
																						else
																							szParam.Format("%d", (ulTime)/3600); // default
																					}
																					else
																						szParam.Format("%d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600))); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																						szParam.Format("%02d", (ulTime)/3600); // default
																					else
																						szParam.Format("%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600))); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoW')")==0)
																				{ //i.e. Monday
																					nNumStyles++;

																					char buffer[48]; // need fewer;
																					long timeofthing = (long)(dblNumber);
																					tm* theTime = gmtime( &timeofthing );
																					strftime( buffer, 40, "%A", theTime );
																					szParam.Format("%s", buffer); 
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_ppromolite->m_data.m_timebTick.time - (g_ppromolite->m_data.m_timebTick.timezone*60) +(g_ppromolite->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoWex')")==0)
																				{ //i.e. Monday, but:
																					//if later today
																					//{ if > 6pm "tonight" else "later" }
																					// else if tomorrow "tomorrow" 
																					// else if > 1 week from now "coming"
																					// else "Monday" style day of week

																					unsigned long ulNow = (unsigned long)(g_ppromolite->m_data.m_timebTick.time - (g_ppromolite->m_data.m_timebTick.timezone*60) +(g_ppromolite->m_data.m_timebTick.dstflag?3600:0)); // local time....

																					if((ulNow/86400) == (((unsigned long)dblNumber)/86400)) // today
																					{
																						if((ulTime/3600)>17) // tonight, 6pm or later
																						{
																							szParam = "Tonight";
																						}
																						else
																						{
																							szParam = "Later";
																						}
																					}
																					else
																					if((ulNow/86400) == ((((unsigned long)dblNumber)/86400)-1)) // tomorrow
																					{
																						szParam = "Tomorrow";
																					}
																					else
																					if((ulNow/86400) <= ((((unsigned long)dblNumber)/86400)-7)) // next week
																					{
																						szParam = "Coming";
																					}
																					else
																					{
																						char buffer[48]; // need fewer;
																						long timeofthing = (long)dblNumber; 
																						tm* theTime = gmtime( &timeofthing );
																						strftime( buffer, 40, "%A", theTime );
																						szParam.Format("%s", buffer); 
																					}



																					nNumStyles++;
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_ppromolite->m_data.m_timebTick.time - (g_ppromolite->m_data.m_timebTick.timezone*60) +(g_ppromolite->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoWa')")==0)
																				{ //i.e. Monday
																					nNumStyles++;

																					char buffer[48]; // need fewer;
																					long timeofthing = (long)dblNumber; 
																					tm* theTime = gmtime( &timeofthing );
																					strftime( buffer, 40, "%a", theTime );
																					szParam.Format("%s", buffer); 
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_ppromolite->m_data.m_timebTick.time - (g_ppromolite->m_data.m_timebTick.timezone*60) +(g_ppromolite->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoWaex')")==0)
																				{ //i.e. Monday, but:
																					//if later today
																					//{ if > 6pm "tonight" else "later" }
																					// else if tomorrow "tomorrow" 
																					// else if > 1 week from now "coming"
																					// else "Monday" style day of week

																					unsigned long ulNow = (unsigned long)(g_ppromolite->m_data.m_timebTick.time - (g_ppromolite->m_data.m_timebTick.timezone*60) +(g_ppromolite->m_data.m_timebTick.dstflag?3600:0)); // local time....

																					if((ulNow/86400) == (((unsigned long)dblNumber)/86400)) // today
																					{
																						if((ulTime/3600)>17) // tonight, 6pm or later
																						{
																							szParam = "Tonight";
																						}
																						else
																						{
																							szParam = "Later";
																						}
																					}
																					else
																					if((ulNow/86400) == ((((unsigned long)dblNumber)/86400)-1)) // tomorrow
																					{
																						szParam = "Tomorrow";
																					}
																					else
																					if((ulNow/86400) <= ((((unsigned long)dblNumber)/86400)-7)) // next week
																					{
																						szParam = "Coming";
																					}
																					else
																					{
																						char buffer[48]; // need fewer;
																						long timeofthing = (long)dblNumber; 
																						tm* theTime = gmtime( &timeofthing );
																						strftime( buffer, 40, "%a", theTime );
																						szParam.Format("%s", buffer); 
																					}


																					nNumStyles++;
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_ppromolite->m_data.m_timebTick.time - (g_ppromolite->m_data.m_timebTick.timezone*60) +(g_ppromolite->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "appendAMPM('lcase')")==0)
																				{
																					szParam = szTemp;
																					nNumStyles++;
																					szTemp.Format("%s %s", szParam, bPM?"pm":"am");
																				}
																				else 
																				if(stricmp(paction, "appendAMPM('ucase')")==0)
																				{
																					szParam = szTemp;
																					nNumStyles++;
																					szTemp.Format("%s %s", szParam, bPM?"PM":"AM");
																				}
	//else not nNumStyles++; which is why i had to do it that annoying way.


	/*
	Round,roundNumber()
	Truncate,truncateNumber()
	Filter Smut,smutFilter()
	Truncate To 20 Characters,truncateText(20)
	Truncate To 50 Characters,truncateText(50)
	Truncate To 100 Characters,truncateText(100)
*/

																				else 
																				if(stricmp(paction, "roundNumber()")==0)
																				{
																					dblNumber+= 0.5;
																					nNumStyles++;
																					szTemp.Format("%d", (int)dblNumber);
																				}
																				else 
																				if(stricmp(paction, "truncateNumber()")==0)
																				{
																					nNumStyles++;
																					szTemp.Format("%d", (int)dblNumber);
																				}
																				else 
																				if((strnicmp(paction, "truncateText(", 13)==0)&&(strlen(paction)>13))
																				{
																					int nTruncLen = atoi(paction+13);
																					nNumStyles++;
																					szParam = szTemp.Left(nTruncLen);
																					szTemp = szParam;
																				}

/*
	Convert All Text To Upper Case,textCase('upper')
	Convert All Text To Lower Case,textCase('lower')
	Convert First Letter Of First Word To Upper Case,textCase('sentence')
	Convert First Letter Of Text String To Upper Case,textCase('title')
*/
																				else 
																				if((strnicmp(paction, "textCase(", 9)==0)&&(strlen(paction)>9))
																				{
																					int nL=0;
																					bool bSpace = true;
																					char ch;
																					szParam = "";

																					while(nL<szTemp.GetLength())
																					{
																						ch = szTemp.GetAt(nL);
																						switch(*(paction+10))
																						{
																						case 'u':
																							{
																								szParam += toupper(ch);
																							} break;
																						case 'l':
																							{
																								szParam += tolower(ch);
																							} break;
																						case 's':
																							{
																								if((!(isspace(ch)))&&(bSpace))
																								{
																									bSpace = false;
																									szParam += toupper(ch);
																								}
																								else
																								{
																									szParam += ch;
																								}
																							} break;
																						case 't':
																							{
																								if(isspace(ch))
																								{
																									bSpace = true;
																									szParam += ch;
																								}
																								else
																								{
																									if(bSpace)
																									{
																										szParam += toupper(ch);
																									}
																									else
																									{
																										szParam += ch;
																									}
																									bSpace = false;
																								}
																							} break;
																						}
																						nL++;
																					}
																					nNumStyles++;
																					szTemp = szParam;
																				}


	/*
	Long Date Lower Case,formatDate('longdate', 'lcase')
	Long Date Upper Case,formatDate('longdate', 'ucase')
	Short Date Lower Case,formatDate('shortdate', 'lcase')
	Short Date Upper Case,formatDate('shortdate', 'ucase')
	Celsius,formatTemp('celsius')
	Fahrenheit,formatTemp('fahrenheit')
	*/
																				else 
																				if(stricmp(paction, "formatTemp('celsius')")==0)
																				{
																					nNumStyles++;
																					szTemp.Format("%d�C", (int)dblNumber);
																				}
																				else 
																				if(stricmp(paction, "formatTemp('fahrenheit')")==0)
																				{
																					nNumStyles++;
																					szTemp.Format("%d�F", (int)dblNumber);
																				}
																			
																				paction = sbu.Token(NULL, NULL, "|");
																			}
																			if(bTime) szValue += szTemp;
																		}
																		prsParam->Close();
																		delete prsParam;
																		prsParam = NULL;

																	}
																	else
																	{
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "SQL returned NULL recordset: %s", errorstring); // Sleep(50); //(Dispatch message)
																	}

																	if(nNumStyles==0)
																	{
																		if(bTime)
																		{
																			// format it the default way H:MM 24 hour clock
																			szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																			szValue += szTemp;
																		}
																		else 
																		{
																			szValue += szTemp;
																		}
																	}
																}
																else
																{
																	// if no style, need to understand if this is a time.
																	if(bTime)
																	{
																		// format it the default way H:MM 24 hour clock
																		szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																		szValue += szTemp;
																	}
																	else 
																	{
																		szValue += szTemp;
																	}
																}
															}
														}
														if(prsParam)
														{
															prsParam->Close();
															delete prsParam;
														}
													}
													else
													{
//	ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "NEAR � returned NULL");  Sleep(50);//(Dispatch message)

													}
												}
											}
											else
											{
												szValue += ch;
												i++;
											}
										}
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "styles done");  Sleep(50); //(Dispatch message)

										if((!ppromolite->m_data.m_bNearEventsChanged)&&(!g_bKillThread))//&&(!ppromolite->m_data.m_bKillAutomationThread))
										{
										if(nTriggerItemID<=0)
										{
											if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
											{
											//insert
												char* chValue = dbSet.EncodeQuotes(szValue);
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, promolite_eventid, Event_ActionID, value, flags, analysis_id) values \
(%d, %d, %d, '%s', %d, %d)",
													ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nPromoLiteEventItemID, nEventActionID, 
													chValue?chValue:szValue, 
													ulFlags, nAnalysisID
												);
												if(chValue) free(chValue);
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_ANALYZE) 	
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug2", "Global Analysis SQL (export): %s", szSQL);  //Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing near SQL: %s", errorstring); // Sleep(50); //(Dispatch message)
												}
												//else break;
											}
										}
										else
										{
											// update
											if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
											{
												char* chValue = dbSet.EncodeQuotes(szValue);
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, promolite_eventid = %d, Event_ActionID = %d, value = '%s', flags = %d, analysis_id = %d WHERE analyzed_trigger_id = %d",
														ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
														nAutomationItemID, nPromoLiteEventItemID, nEventActionID, 
														chValue?chValue:szValue, 
														ulFlags, nAnalysisID, nTriggerItemID
													);
												if(chValue) free(chValue);
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_ANALYZE) 	
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug2", "Global Analysis SQL (export): %s", szSQL);  //Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing near SQL: %s", errorstring);  //Sleep(50); //(Dispatch message)
												}
											}
										}
										}
										else
										{
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_ANALYZE) 	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "if((!ppromolite->m_data.m_bNearEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)) = true"); // Sleep(50);
										}
									}
									else
									{
										// just set to renanalyzed
										ulFlags |= (PROMOLITE_FLAG_REANALYZED|PROMOLITE_FLAG_ANALYZED);
//										ulFlags = ppromolite->m_data.m_nAnalysisCounter;

										if((!ppromolite->m_data.m_bNearEventsChanged)&&(!g_bKillThread))//&&(!ppromolite->m_data.m_bKillAutomationThread))
										if(nTriggerItemID<=0)
										{
											if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
											{
											//insert
												char* chValue = dbSet.EncodeQuotes(szValue);
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, promolite_eventid, Event_ActionID, value, flags, analysis_id) values \
(%d, %d, %d, '%s', %d, %d)",
													ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nPromoLiteEventItemID, nEventActionID, 
													chValue?chValue:szValue,
													ulFlags, nAnalysisID
												);
												if(chValue) free(chValue);
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_ANALYZE) 	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "SQL: %s", szSQL);//  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing near SQL: %s", errorstring);//  Sleep(50); //(Dispatch message)
												}// else break;
											}
										}
										else
										{
											// update
											char* chValue = dbSet.EncodeQuotes(szValue);
											if((nAutomationItemID>0)&&(nPromoLiteEventItemID>0)&&(nEventActionID>0))
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, promolite_eventid = %d, Event_ActionID = %d, value = '%s', flags = %d, analysis_id=%d WHERE analyzed_trigger_id = %d",
														ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
														nAutomationItemID, nPromoLiteEventItemID, nEventActionID, 
														chValue?chValue:szValue,
														ulFlags, nAnalysisID, nTriggerItemID
													);
												if(chValue) free(chValue);
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_ANALYZE) 	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "SQL: %s", szSQL);//  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing near SQL: %s", errorstring); // Sleep(50); //(Dispatch message)
												}
											}
										}

									}
								}
						
						if((!g_bKillThread))//&&(!ppromolite->m_data.m_bKillAutomationThread))
						{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Analysis moving next");  Sleep(50); //(Dispatch message)
							prs->MoveNext();

							// make a short pause between records 
							_timeb timeCheck; 
							_ftime(&timeCheck); // the last time check inside the thread

							timeCheck.time += ppromolite->m_settings.m_nInterTriggerDelayMS/1000;
							timeCheck.millitm += ppromolite->m_settings.m_nInterTriggerDelayMS%1000;
							while(timeCheck.millitm>999)
							{
								timeCheck.millitm -= 1000;
								timeCheck.time++;
							}
							
							if(g_ppromolite->m_data.m_bRunningAnalyses) // means delay thread is waiting.
							{
								g_ppromolite->m_data.m_bRunningAnalyses = false;  // causes delay thread to trigger new trigger buffer, if active.
							}
							else // delay thread not waiting, but we want to spark it if we have a new analysis result in the trigger buffer.
							{

								if(
										(g_ppromolite->m_settings.m_bEnableAnalysisTriggerNotification)
									&&(
											((pAutoObj)&&(nNumRec<=pAutoObj->m_nNumChannelObjects*ppromolite->m_settings.m_nTriggerBuffer)) // aggressive but has to be, currently no way to go channel specific, so have to set them all to refresh.
										||(nNumRec<=ppromolite->m_settings.m_nTriggerBuffer)
										)
									)
								{

									g_ppromolite->m_data.m_bRunningAnalyses = false;

									if(!g_ppromolite->m_data.m_bDelayingTriggerNotification)
									{
										bool bGoNow = true;
	if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_PROCESS) 	
		g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "  >>* Spawning PromoLiteTriggerDelayNotificationThread from analysis thread");  // Sleep(50);//(Dispatch message)
										if(_beginthread(PromoLiteTriggerDelayNotificationThread, 0, (void*)(&bGoNow))==-1)
										{
											//error.
							g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Error starting trigger delay notification thread from analysis");  // Sleep(10);//(Dispatch message)

											//**MSG
										}
									}


								}
							}


							while (
											(!ppromolite->m_data.m_bNearEventsChanged)&&(!g_bKillThread)
										&&(
												(ppromolite->m_data.m_timebNearTick.time<timeCheck.time)
											||((ppromolite->m_data.m_timebNearTick.time<timeCheck.time)&&(ppromolite->m_data.m_timebNearTick.millitm<timeCheck.millitm))
											)
										)// max of 3 second pause in between events (not between records inside event).
							{
		_ftime(&ppromolite->m_data.m_timebNearTick); // the last time check inside the thread
								Sleep(1);
							}

						}
					} //					while((!ppromolite->m_data.m_bNearEventsChanged)&&(!prs->IsEOF())&&(!g_bKillThread))//&&(!ppromolite->m_data.m_bKillAutomationThread))

					prs->Close();
					delete prs;
					prs = NULL;
					if(nNumRec==0)
					{
						if(g_ppromolite->m_data.m_bRunningAnalyses) // means delay thread is waiting.
						{
							g_ppromolite->m_data.m_bRunningAnalyses = false;  // causes delay thread to trigger new trigger buffer, if active.
						}

//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Analysis waiting"); // Sleep(50); //(Dispatch message)
						// no records. just wait a few seconds before hitting the db again.
				//		int ixi=0;
						while((!ppromolite->m_data.m_bNearEventsChanged)&&(!g_bKillThread)) // &&(!ppromolite->m_data.m_bKillAutomationThread)&&(ixi<5000))  // max of 5 second pause on no records.
						{
	_ftime(&ppromolite->m_data.m_timebNearTick); // the last time check inside the thread
							Sleep(1);
				//			ixi++;
						}
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Analysis finished waiting"); // Sleep(50); //(Dispatch message)
					}
					else
					{
						_timeb timeCheck; 
						_ftime(&timeCheck); // the last time check inside the thread

						timeCheck.time += ppromolite->m_settings.m_nInterEventDelayMS/1000;
						timeCheck.millitm += ppromolite->m_settings.m_nInterEventDelayMS%1000;
						while(timeCheck.millitm>999)
						{
							timeCheck.millitm -= 1000;
							timeCheck.time++;
						}
						
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Analysis waiting in between events");  //Sleep(50); //(Dispatch message)
						while (
										(!ppromolite->m_data.m_bNearEventsChanged)&&(!g_bKillThread)
									&&(
											(ppromolite->m_data.m_timebNearTick.time<timeCheck.time)
										||((ppromolite->m_data.m_timebNearTick.time<timeCheck.time)&&(ppromolite->m_data.m_timebNearTick.millitm<timeCheck.millitm))
										)
									)// max of 3 second pause in between events (not between records inside event).
						{
	_ftime(&ppromolite->m_data.m_timebNearTick); // the last time check inside the thread
							Sleep(1);
						}
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Analysis finished waiting in between events"); // Sleep(50); //(Dispatch message)
					}

				}//if(prs != NULL)
				else
				{
					if(g_ppromolite->m_data.m_bRunningAnalyses) // means delay thread is waiting.
					{
						g_ppromolite->m_data.m_bRunningAnalyses = false;  // causes delay thread to trigger new trigger buffer, if active.
					}

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Near analysis resultset was null! %s", errorstring); // Sleep(50);//(Dispatch message)
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Global Analysis ERROR:  %s", errorstring); // Sleep(50);//(Dispatch message)
					Sleep(5000); // an exception occurred.
				}

			}//			while((!ppromolite->m_data.m_bNearEventsChanged)&&(!bChangesPending)&&(!g_bKillThread))//&&(!ppromolite->m_data.m_bKillAutomationThread))

		} //		if(
//			  (ppromolite->m_data.m_nIndexAutomationEndpoint>=0)
//			&&(ppromolite->m_settings.m_nNumEndpointsInstalled>0)
///			&&(ppromolite->m_settings.m_ppEndpointObject)
//			&&(ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint])
//			&&(!ppromolite->m_data.m_bProcessSuspended)
//			&&(ppromolite->m_data.m_key.m_bValid)  // must have a valid license
//			&&(
//					(!ppromolite->m_data.m_key.m_bExpires)
//				||((ppromolite->m_data.m_key.m_bExpires)&&(!ppromolite->m_data.m_key.m_bExpired))
//				||((ppromolite->m_data.m_key.m_bExpires)&&(ppromolite->m_data.m_key.m_bExpireForgiveness)&&(ppromolite->m_data.m_key.m_ulExpiryDate+ppromolite->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
//				)
//			&&(
//					(!ppromolite->m_data.m_key.m_bMachineSpecific)
//				||((ppromolite->m_data.m_key.m_bMachineSpecific)&&(ppromolite->m_data.m_key.m_bValidMAC))
//				)
//			)

		//Sleep(1); // dont peg processor
	} // while((!g_bKillThread)&&(!ppromolite->m_data.m_bKillAutomationThread))
	db.RemoveConnection(pdbConn);
	dbSet.RemoveConnection(pdbConnSet);
	ppromolite->m_data.m_bGlobalAnalysisThreadStarted=false;
	
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending global analysis thread");
	g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLiteGlobalAnalysisThread", errorstring);    //(Dispatch message)

	_endthread();
	Sleep(150);

}

void PromoLiteTriggerDelayNotificationThread(void* pvArgs)
{
	if(g_ppromolite->m_data.m_bDelayingTriggerNotification == true) return; // only 1 allowed
	g_ppromolite->m_data.m_bDelayingTriggerNotification = true;
	//delay notification here
	bool* pbGo = (bool*) pvArgs;
	if((pbGo!=NULL)&&((*pbGo)==true))
	{
		if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) 
			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Signalling trigger buffer refresh"); //(Dispatch message)
	}
	else
	{
		if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) 
			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Delaying trigger buffer refresh"); //(Dispatch message)
		while((!g_bKillThread)&&(g_ppromolite->m_data.m_bRunningAnalyses))
		{
			Sleep(10);// wait until done
		}

		g_ppromolite->m_data.m_bRunningAnalyses = true;
		if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) 
			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Signalling trigger buffer delay ready"); //(Dispatch message)

		while((!g_bKillThread)&&(g_ppromolite->m_data.m_bRunningAnalyses))
		{
			Sleep(10);
		}
		if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) 
			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Signalling trigger buffer refresh after delay"); //(Dispatch message)

	}

	// now, dwell!
	_timeb timebChange;
	_ftime(&timebChange);

	if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_PROCESS) 
		g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "  --* Dwell trigger buffer notification thread"); //(Dispatch message)
	while(!g_bKillThread)
	{
		if  (
					(g_ppromolite->m_data.m_timebTick.time > (timebChange.time+g_ppromolite->m_settings.m_nTriggerNotificationDwellMS/1000))
				||(
					  (g_ppromolite->m_data.m_timebTick.time==(timebChange.time+g_ppromolite->m_settings.m_nTriggerNotificationDwellMS/1000))
					&&(g_ppromolite->m_data.m_timebTick.millitm>(timebChange.millitm+g_ppromolite->m_settings.m_nTriggerNotificationDwellMS%1000))
					)
				)
		{
			 break;
		}

		Sleep(1);
		_ftime(&timebChange);

	}
	if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_PROCESS) 
		g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "  --* End dwell trigger buffer notification thread"); //(Dispatch message)

	if(
			(g_ppromolite->m_settings.m_ppEndpointObject)
		&&(g_ppromolite->m_data.m_nIndexAutomationEndpoint>=0)
		&&(g_ppromolite->m_data.m_nIndexAutomationEndpoint<g_ppromolite->m_settings.m_nNumEndpointsInstalled)
		&&(g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexAutomationEndpoint])
		&&(!g_bKillThread)
		)
	{
		CPromoLiteEndpointObject* pAutoObj = g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexAutomationEndpoint];

//		if(pAutoObj) pAutoObj->m_nLastModLiveEvents = -1;  // triggers global analysis rules.
		if((pAutoObj->m_ppChannelObj)&&(pAutoObj->m_nNumChannelObjects))
		{
			int cho=0;
			while((cho<pAutoObj->m_nNumChannelObjects)&&(!g_bKillThread))
			{
				if(pAutoObj->m_ppChannelObj[cho])
				{
					pAutoObj->m_ppChannelObj[cho]->m_bTriggerEventsChanged = true;
//										pAutoObj->m_ppChannelObj[cho]->m_bNearEventsChanged = true;

				}
				cho++;
			}
		}
	}
	if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_PROCESS) 
		g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "  <<* Ending trigger buffer notification thread"); //(Dispatch message)
	
	g_ppromolite->m_data.m_bDelayingTriggerNotification = false;

	_endthread();
}

void PromoLiteTriggerThread(void* pvArgs)
{

/*
create table Schedule (event_instance_id int identity(1,1) NOT NULL, event_template_name varchar(32), start_time decimal(20,3), layer int, last_modified int);  

create table Schedule_Actions (trigger_id int identity(1,1) NOT NULL, event_instance_id int, graphics_action_id int, trigger_offset int, trigger_value varchar(1024), trigger_status bigint NOT NULL DEFAULT(0));   
*/


	CPromoLiteAutomationChannelObject* pChannelObj = (CPromoLiteAutomationChannelObject*) pvArgs;
	if(pChannelObj==NULL)
	{
	g_ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLiteTriggerThread", "NULL Channel Object");    //(Dispatch message)

		return;  // this would be bad
	}

#ifndef NUCLEUS_PROMOLITE_CHANGE
	// there is no automation, the following is guaranteed to be NULL  NUCLEUS->PROMOLITE_CHANGE
	CPromoLiteEndpointObject* pEndObj = (CPromoLiteEndpointObject*) pChannelObj->m_pEndpoint;
	if(pEndObj==NULL)
	{
	g_ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLiteTriggerThread", "NULL Endpoint Object");    //(Dispatch message)

		return;  // this would be bad
	}

#endif

	CPromoLiteMain* ppromolite = (CPromoLiteMain*) pChannelObj->m_pPromoLite;
	if(ppromolite==NULL)
	{
	g_ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLiteTriggerThread", "NULL Main Object");    //(Dispatch message)

		return;  // this would be bad
	}


	pChannelObj->m_bTriggerThreadStarted=true;

	g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLiteTriggerThread", "started: %d",pChannelObj->m_bTriggerThreadStarted);    //(Dispatch message)


	char szCommandSQL[DB_SQLSTRING_MAXLEN];
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];


#ifndef NUCLEUS_PROMOLITE_CHANGE
	unsigned long ulAutoType  = 0;

	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_SENTINEL)
	{
		ulAutoType = PROMOLITE_DEP_AUTO_SENTINEL;
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning trigger thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	}
	else
	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_HELIOS)
	{
		ulAutoType = PROMOLITE_DEP_AUTO_HELIOS;
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning trigger thread for Omnibus stream %d", pChannelObj->m_nChannelID);
	}
#else //#ifdef NUCLEUS_PROMOLITE_CHANGE
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning Promolite trigger thread");
#endif // #ifdef NUCLEUS_PROMOLITE_CHANGE

	g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLiteTriggerThread", errorstring);    //(Dispatch message)

	strcpy(errorstring, "");
	CDBUtil db;
	CDBUtil dbSet;

	CDBconn* pdbConn = db.CreateNewConnection(ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:trigger_database_connect", errorstring);  //(Dispatch message)
			pdbConn = ppromolite->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW); 
		ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:trigger_database_init", errorstring);  //(Dispatch message)
		pdbConn = ppromolite->m_data.m_pdbConn;

		//**MSG
	}
	CDBconn* pdbConnSet = dbSet.CreateNewConnection(ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW);
	if(pdbConnSet)
	{
		if(dbSet.ConnectDatabase(pdbConnSet, errorstring)<DB_SUCCESS)
		{
			ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:analysis_database2_connect", errorstring);  //(Dispatch message)
			pdbConnSet = ppromolite->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW); 
		ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:analysis_database2_init", errorstring);  //(Dispatch message)
		pdbConnSet = ppromolite->m_data.m_pdbConn;

		//**MSG
	}


#ifdef NUCLEUS_PROMOLITE_CHANGE
	//  need to error out any items that are in the past, so that they arent just hanging out there.

	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
					"UPDATE %s.dbo.%s set %s.dbo.Schedule_Actions.trigger_status=%d \
from %s.dbo.%s as Schedule_Actions, %s.dbo.%s as Schedule \
where Schedule_Actions.event_instance_id = Schedule.event_instance_id and Schedule.start_time < %.3lf \
and Schedule_Actions.trigger_status = 0",

						(((g_ppromolite->m_settings.m_pszDefaultDB)&&(strlen(g_ppromolite->m_settings.m_pszDefaultDB)))?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
						(((g_ppromolite->m_settings.m_pszAnalyzedTriggerData)&&(strlen(g_ppromolite->m_settings.m_pszAnalyzedTriggerData)))?g_ppromolite->m_settings.m_pszAnalyzedTriggerData:"Schedule_Actions"),
						(((g_ppromolite->m_settings.m_pszDefaultDB)&&(strlen(g_ppromolite->m_settings.m_pszDefaultDB)))?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
						(PROMOLITE_FLAG_ERROR|PROMOLITE_FLAG_NOTRUN),
						(((g_ppromolite->m_settings.m_pszDefaultDB)&&(strlen(g_ppromolite->m_settings.m_pszDefaultDB)))?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
						(((g_ppromolite->m_settings.m_pszAnalyzedTriggerData)&&(strlen(g_ppromolite->m_settings.m_pszAnalyzedTriggerData)))?g_ppromolite->m_settings.m_pszAnalyzedTriggerData:"Schedule_Actions"),
						(((g_ppromolite->m_settings.m_pszDefaultDB)&&(strlen(g_ppromolite->m_settings.m_pszDefaultDB)))?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
						(((g_ppromolite->m_settings.m_pszEvents)&&(strlen(g_ppromolite->m_settings.m_pszEvents)))?g_ppromolite->m_settings.m_pszEvents:"Schedule"),
						pChannelObj->ReturnTriggerTickTime()
						);
//update promolite.dbo.Schedule_Actions   
//set Schedule_Actions.trigger_status=65535  
//from promolite.dbo.Schedule_Actions as Schedule_Actions, promolite.dbo.Schedule as Schedule  
//where Schedule_Actions.event_instance_id = Schedule.event_instance_id and Schedule.start_time < 1239990889 and Schedule_Actions.trigger_status = 0
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) 
{
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "initial SQL: %s", szSQL); 
}

	if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
	{
	//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing startup SQL: %s", errorstring);  //Sleep(50); //(Dispatch message)
	}



#endif



	// load the damn DEMO scene.



/*
	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 Host FROM %s WHERE Host IS NOT NULL",
			ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
			PROMOLITE_FLAG_TRIGGERED
		);

	CString szHostLoad = ppromolite->m_settings.m_pszDefaultHost;
	CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
	if(prs != NULL)
	{
		if(!prs->IsEOF())
		{
			prs->GetFieldValue("Host", szHostLoad);
		}
		prs->Close();
		delete prs; prs=NULL;
	}


	char proj[256]; sprintf(proj, "0|%s|%s", ppromolite->m_settings.m_pszDefaultProject, ppromolite->m_settings.m_pszDefaultScene);

	char* pchEncodedLocal = db.EncodeQuotes(proj);
	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(local, remote, action, host, timestamp, username) \
VALUES ('%s','', %d, '%s', %d.%d, 'sys')", //HARDCODE
															"Radiance",
															"Command_Queue",
															(pchEncodedLocal?pchEncodedLocal:"0|Promo_B|Promo_B"),
															256,
															szHostLoad,
															g_ppromolite->m_data.m_timebTick.time,
															g_ppromolite->m_data.m_timebTick.millitm
															);
	if(pchEncodedLocal) free(pchEncodedLocal);

g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ScheduleEndpointQueue SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
//		char errorstring[DB_ERRORSTRING_LEN]; if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
	if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
	{
	//**MSG
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
	}

*/
/*

	char proj[256]; sprintf(proj, "%s%c%s%c%s", 
		ppromolite->m_settings.m_pszDefaultHost, 28,
		ppromolite->m_settings.m_pszDefaultProject, 28,
		ppromolite->m_settings.m_pszDefaultScene);

	while((g_ppromolite->m_data.SendGraphicsCommand(RADIANCE_CMD_LOADSCENE, proj)<0)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
	{
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR RADIANCE_CMD_LOADSCENE: %s", proj);  Sleep(50); //(Dispatch message)
		int i=0;
		while ((i<100)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)) Sleep(100);
	}

	while(!ppromolite->m_data.m_bTriggerEventsChanged) Sleep(100);

	Sleep(60000); // dont start triggering util after 1 min
*/

/*
	// clear the old events.
	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
			ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"   // the AnalyzedTriggerData table name
	
		);
*/
/*
	while((db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
	{
	//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)
	}
*/

	CIS2Core is2; // for util functions only.
	CBufferUtil bu;

	pChannelObj->m_ppEvents = new CPromoLiteEvent*[128]; // this is fine, m_nTriggerBuffer is guaranteed to max out at 127.
	pChannelObj->m_nEvents = 0;

	if(pChannelObj->m_ppEvents)
	{
		while( pChannelObj->m_nEvents < 128)
		{
			pChannelObj->m_ppEvents[pChannelObj->m_nEvents] = NULL; // initialize
			CPromoLiteEvent* pEvent = new CPromoLiteEvent;
			if(pEvent)
			{
				pChannelObj->m_ppEvents[pChannelObj->m_nEvents] = pEvent;
				pChannelObj->m_nEvents++;
			}
			else break; // no infinite loop.
		}
	}


	pChannelObj->m_nMinLyricAlias=ppromolite->m_settings.m_nMinLyricAlias;
	pChannelObj->m_nMaxLyricAlias=ppromolite->m_settings.m_nMaxLyricAlias;

	int nAliases = pChannelObj->m_nMaxLyricAlias - pChannelObj->m_nMinLyricAlias + 1;
	if(nAliases<=0) nAliases = 10; //min set
	pChannelObj->m_ppLyricAlias = new LyricAlias_t*[nAliases];
	pChannelObj->m_nNumAliasArray = 0;

	if(pChannelObj->m_ppLyricAlias)
	{
		while( pChannelObj->m_nNumAliasArray < nAliases)
		{
			LyricAlias_t* pAlias = new LyricAlias_t;
			if(pAlias)
			{
				pChannelObj->m_ppLyricAlias[pChannelObj->m_nNumAliasArray] = pAlias;
				pAlias->szMessage = "";
				pAlias->nAlias = -1;
				pAlias->bMessageLoaded = false;
				pAlias->bMessageRead = false;

				pChannelObj->m_nNumAliasArray++;
			}
			else break; // no infinite loop.
		}
	}

	int nLastID=-1;
	double delta = 0.0;
	double wait = 0.0;

	while((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
	{
		if(
/*
			  (ppromolite->m_data.m_nIndexAutomationEndpoint>=0)
			&&(ppromolite->m_settings.m_nNumEndpointsInstalled>0)
			&&(ppromolite->m_data.m_nIndexAutomationEndpoint<ppromolite->m_settings.m_nNumEndpointsInstalled)
			&&(ppromolite->m_settings.m_ppEndpointObject)
			&&(ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint])
			&&*/
			  (!ppromolite->m_data.m_bProcessSuspended)
/*			&&(

// NUCLEUS->PROMOLITE_CHANGE // removed automation things
					(
						(ulAutoType == PROMOLITE_DEP_AUTO_SENTINEL)
					&&(pChannelObj->m_nListState&((1<<LISTTHREADING)|(1<<LISTISPLAYING)))  // list must be threaded and playing to trigger
					&&(!(pChannelObj->m_nListState&((1<<LISTINFREEZE)|(1<<LISTINHOLD))))  // list must not be in freeze or hold to trigger
					)
				||(
						(ulAutoType == PROMOLITE_DEP_AUTO_HELIOS)
					&&(pChannelObj->m_nListState!=0)  // list must be connected
//					&&(pChannelObj->m_nServerStatus!=0)  // server must be connected!  // comment this out to surivive adaptor disconnection retries....
					)
				)
*/
			&&(ppromolite->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!ppromolite->m_data.m_key.m_bExpires)
				||((ppromolite->m_data.m_key.m_bExpires)&&(!ppromolite->m_data.m_key.m_bExpired))
				||((ppromolite->m_data.m_key.m_bExpires)&&(ppromolite->m_data.m_key.m_bExpireForgiveness)&&(ppromolite->m_data.m_key.m_ulExpiryDate+ppromolite->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!ppromolite->m_data.m_key.m_bMachineSpecific)
				||((ppromolite->m_data.m_key.m_bMachineSpecific)&&(ppromolite->m_data.m_key.m_bValidMAC))
				)

			)
		{

		//			CPromoLiteEndpointObject* pAutoObj = ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint];



			///// DEMOTODO get status col name in view
/*
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d * FROM %s WHERE server = '%s' AND listid = %d AND \
AnalyzedTriggerDataStatus & %d = 0 \
AND analyzed_trigger_id IS NOT NULL ORDER BY TriggerTime ASC",  ppromolite->m_settings.m_nTriggerBuffer,
					ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
					pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListID, PROMOLITE_FLAG_TRIGGERED
				);
*/
#ifndef NUCLEUS_PROMOLITE_CHANGE
			if(ulAutoType == PROMOLITE_DEP_AUTO_SENTINEL)
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d * FROM %s WHERE channelid = %d AND \
AnalyzedTriggerDataStatus & %d = 0 and AnalyzedTriggerDataFlags & %d <> 0 and ((event_status & %d) = 0)\
AND TriggerTime > %.3f \
AND analyzed_trigger_id IS NOT NULL ORDER BY TriggerTime ASC",  ppromolite->m_settings.m_nTriggerBuffer,
						ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
						pChannelObj->m_nChannelID, PROMOLITE_FLAG_TRIGGERED, PROMOLITE_FLAG_ANALYZED,
						ppromolite->m_settings.m_nHarrisStatusTriggerExclude,
						pChannelObj->m_dblAutomationTimeEstimate - ((double)ppromolite->m_settings.m_nTriggerExclusionDelayMS)/1000.0
					);


///			((event_status & 17) = 0) // not done or postrolled.  // we want ones that are playing or yet to be played

// SENTINEL_EVENT_POSTUPCOUNT = 0x00100000 = 1048576 decimal.
// now to suppress post upcount events, we call ((event_status & 1048593) = 0) = not done or postrolled or post upcounter
			}
			else
			if(ulAutoType == PROMOLITE_DEP_AUTO_HELIOS)
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d * FROM %s WHERE channelid = %d AND \
AnalyzedTriggerDataStatus & %d = 0  and AnalyzedTriggerDataFlags & %d <> 0 and event_status < 10 and event_status > 4 \
AND TriggerTime > %.3f \
AND analyzed_trigger_id IS NOT NULL ORDER BY TriggerTime ASC",  ppromolite->m_settings.m_nTriggerBuffer,
						ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
						pChannelObj->m_nChannelID, PROMOLITE_FLAG_TRIGGERED, PROMOLITE_FLAG_ANALYZED,
						pChannelObj->m_dblAutomationTimeEstimate - ((double)ppromolite->m_settings.m_nTriggerExclusionDelayMS)/1000.0
					);

///			event_status < 10 and event_status >4 // not done or postrolledand no error  // we want ones that are playing or yet to be played and without errors
			}
#else //#ifdef NUCLEUS_PROMOLITE_CHANGE

	_ftime(&pChannelObj->m_timebTriggerTick); // the last time check inside the thread




			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d \
event_instance_id, trigger_value, trigger_time, trigger_id, scene, Graphic_Action_Type, Action_Name, layer, event_template_name FROM %s WHERE \
user_statusid = 1 \
AND trigger_status & %d = 0 \
AND trigger_time > %.3f \
AND trigger_id IS NOT NULL ORDER BY trigger_time ASC",  ppromolite->m_settings.m_nTriggerBuffer,
					ppromolite->m_settings.m_pszTriggerAnalysisView?ppromolite->m_settings.m_pszTriggerAnalysisView:"Triggers",   // the TriggerAnalysisView table name
					PROMOLITE_FLAG_TRIGGERED,
//					pChannelObj->m_dblAutomationTimeEstimate - ((double)ppromolite->m_settings.m_nTriggerExclusionDelayMS)/1000.0
//					pChannelObj->ReturnTriggerTickTime()

					((double)(pChannelObj->m_timebTriggerTick.time - (pChannelObj->m_timebTriggerTick.timezone*60)+ (pChannelObj->m_timebTriggerTick.dstflag?3600:0) ))
						+ (
								(
								((double)(pChannelObj->m_timebTriggerTick.millitm))
						-   ((double)(ppromolite->m_settings.m_nTriggerExclusionDelayMS))
								)/1000.0
							)
							
				);


/*
//TriggerInfo as select 
			ScheduleInfo.event_instance_id as event_instance_id, 
				ScheduleInfo.event_template_name, 
				ScheduleInfo.event_template_description, 
				ScheduleInfo.layer, 
				ScheduleInfo.user_statusid,  // ACTIVE=1 or INACTIVE =0 or other value = ?  just get active ones
				ScheduleInfo.trigger_id as trigger_id, 
				ScheduleInfo.graphics_action_id as graphics_action_id, 
				cast(ScheduleInfo.start_time as decimal(20,3)) as start_time, 
				cast(ScheduleInfo.start_time as decimal(20,3)) + cast(ScheduleInfo.trigger_offset as decimal(20,3)) as trigger_time, 
				ScheduleInfo.trigger_value as trigger_value, 
				case when ScheduleInfo.trigger_status is NULL then 0 else ScheduleInfo.trigger_status end as trigger_status, 
				GraphicsInfo.sys_scene as scene, 
				GraphicsInfo.graphic_action_typeid as Graphic_Action_Type, 
				GraphicsInfo.Name as Action_Name

*/


#endif //#ifdef NUCLEUS_PROMOLITE_CHANGE

	_ftime(&pChannelObj->m_timebTriggerTick); // the last time check inside the thread
//AND TriggerTime >= %lf \
//					ppromolite->m_data.m_dblAutomationTimeEstimate
//AND (event_status & 26  <> 0 ) // have to remove in order to get the backtimed ones
						
		//	&18<>0 = must be postrolled or playing
		//	&26<>0 = must be postrolled or playing - uses preroll because now we are running on calculated event trigger times
			// cant use pre-rol because time is not correct yet

if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) 
{
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "** trigger SQL: %s", szSQL); 
}
			EnterCriticalSection(&ppromolite->m_data.m_critSQL);
			CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
/*
			nEvents = 0;
			if(prs != NULL)
			{
				ppromolite->m_data.m_bTriggerEventsChanged = false;
				while((!ppromolite->m_data.m_bTriggerEventsChanged)&&(!prs->IsEOF())&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
				{
					CString szHost;
					CString szName;
					CString szValue;
					CString szTemp;
					double dblTime;
					int nType=-1;
					int nTriggerItemID=-1;
					int nAutomationItemID=-1;
					prs->GetFieldValue("itemid", szTemp);
					nAutomationItemID=atol(szTemp);
					prs->GetFieldValue("Host", szHost);
					prs->GetFieldValue("ParsedValue", szValue);
					prs->GetFieldValue("TriggerTime", szTemp);
					dblTime=atof(szTemp) - 0.333;  // ten frames anticipation.
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Got: %s, %f", szValue, dblTime);  Sleep(50);//(Dispatch message)
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "m_dblAutomationTimeEstimate>=dblTime?: %f %f", ppromolite->m_data.m_dblAutomationTimeEstimate, dblTime);  Sleep(50);//(Dispatch message)
					while((ppromolite->m_data.m_dblAutomationTimeEstimate<dblTime)&&(!ppromolite->m_data.m_bTriggerEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
					{
						Sleep(1);
					}

					if((!ppromolite->m_data.m_bTriggerEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
					{
						delta = 0.0;
						prs->GetFieldValue("analyzed_trigger_id", szTemp);
						if(szTemp.GetLength()) nTriggerItemID = atoi(szTemp);
						prs->GetFieldValue("Graphic_Action_Type", szTemp);
						nType = atoi(szTemp);
						prs->GetFieldValue("Action_Name", szName);

//						prs->MoveNext();
//						prs->Close();
//						delete prs;
//						prs = NULL;
						// do it.
/*
						szTemp.Format("%d|%s|%s", nType+1, szName, szValue);
						char* pchEncodedLocal = dbSet.EncodeQuotes(szTemp.GetBuffer(1));
						szTemp.ReleaseBuffer();
* /
						char proj[256]; sprintf(proj, "%s%c%s%c%s", 
							ppromolite->m_settings.m_pszDefaultHost, 28,
							szName, 28,
							szValue);

						if(g_ppromolite->m_data.SendGraphicsCommand(((nType==0)?RADIANCE_CMD_PLAYANIM:RADIANCE_CMD_SETEXPORT), proj)<0)
						{
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "error sending command: %s", proj); // Sleep(50);//(Dispatch message)
						}
						else
						{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "command sent: %s", proj); // Sleep(50);//(Dispatch message)
						}

/*
						if((nTriggerItemID>0)&&(szName.GetLength()))
						{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "pchEncodedLocal: %s", pchEncodedLocal);  Sleep(50);//(Dispatch message)
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "want to send %s", szTemp);  Sleep(50);//(Dispatch message)

							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(local, remote, action, host, timestamp, username) \
VALUES ('%s','', %d, '%s', %d.%d, 'sys')", //HARDCODE
																"Radiance",
																"Command_Queue",
																pchEncodedLocal?pchEncodedLocal:szTemp,
																256,
																szHost,
																g_ppromolite->m_data.m_timebTick.time,
																g_ppromolite->m_data.m_timebTick.millitm
																);
							if(pchEncodedLocal) free(pchEncodedLocal);
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "cq SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
							if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
							{
						//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
							}

						* /

							// now update status
							///// DEMOTODO get item id name in view
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d WHERE analyzed_trigger_id = %d",
									ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the AnalyzedTriggerData table name
									PROMOLITE_FLAG_TRIGGERED, nTriggerItemID
								);

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "AnalyzedTriggerDataStatus SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
							if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
							{
							//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing trigger	SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
							}

					//	}

						nEvents++;

						prs->MoveNext();
					}
				}
				*/
			pChannelObj->m_nEvents = 0;
			if(prs != NULL)
			{
				pChannelObj->m_bTriggerEventsChanged = false;
				bool bInvalidTimes = false;
				while((!pChannelObj->m_bTriggerEventsChanged)&&(!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
				{
	_ftime(&pChannelObj->m_timebTriggerTick); // the last time check inside the thread
					CString szTemp;
//					int nAutomationItemID=-1;



					try
					{
#ifdef NUCLEUS_PROMOLITE_CHANGE
	
/*
//TriggerInfo as select 
			ScheduleInfo.event_instance_id as event_instance_id, 
				ScheduleInfo.event_template_name, 
				ScheduleInfo.event_template_description, 
				ScheduleInfo.layer, 
				ScheduleInfo.user_statusid,  // ACTIVE=1 or INACTIVE =0 or other value = ?  just get active ones
				ScheduleInfo.trigger_id as trigger_id, 
				ScheduleInfo.graphics_action_id as graphics_action_id, 
				cast(ScheduleInfo.start_time as decimal(20,3)) as start_time, 
				cast(ScheduleInfo.start_time as decimal(20,3)) + cast(ScheduleInfo.trigger_offset as decimal(20,3)) as trigger_time, 
				ScheduleInfo.trigger_value as trigger_value, 
				case when ScheduleInfo.trigger_status is NULL then 0 else ScheduleInfo.trigger_status end as trigger_status, 
				GraphicsInfo.sys_scene as scene, 
				GraphicsInfo.graphic_action_typeid as Graphic_Action_Type, 
				GraphicsInfo.Name as Action_Name

*/

					// not really used, just for reporting.
					prs->GetFieldValue("event_instance_id", szTemp);
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_nEventID = atoi(szTemp);


					// first shot
//					if(nLastID<0) nLastID = pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_nEventID;
					// nah, let the done purge run at startup to clear out anything old.

					// Not used in view, just use loopback (but make a setting just in case)
//					prs->GetFieldValue("Host", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szHost);
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szHost 
						= (((g_ppromolite->m_settings.m_pszDestinationHost)&&(strlen(g_ppromolite->m_settings.m_pszDestinationHost)))?g_ppromolite->m_settings.m_pszDestinationHost:"127.0.0.1");

					prs->GetFieldValue("trigger_value", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szValue);
					prs->GetFieldValue("trigger_time", szTemp);

//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Trigger time received: %s", szTemp); // Sleep(50); //(Dispatch message)
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_dblTriggerTime = atof(szTemp);// - 0.333;  // ten frames anticipation.

					if((int)pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_dblTriggerTime <= 0) { bInvalidTimes = true; break;}

					prs->GetFieldValue("trigger_id", szTemp);
					if(szTemp.GetLength()) pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_nAnalyzedTriggerID = atoi(szTemp);

					//not used
//					prs->GetFieldValue("Source", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szSource);
					prs->GetFieldValue("scene", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szScene);

					prs->GetFieldValue("Graphic_Action_Type", szTemp);
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_nType = atoi(szTemp);

					prs->GetFieldValue("Action_Name", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szIdentifier);

					/// done with a setting
//					prs->GetFieldValue("dest_type", szTemp);
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_nDestType = g_ppromolite->m_settings.m_nDestinationType;

					/// done with a setting
//					prs->GetFieldValue("dest_module", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szDestModule);
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szDestModule
						= (((g_ppromolite->m_settings.m_pszDestinationModule)&&(strlen(g_ppromolite->m_settings.m_pszDestinationModule)))?g_ppromolite->m_settings.m_pszDestinationModule:"Libretto");

					prs->GetFieldValue("layer", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szLayer);

// want to add promotor event name here
					prs->GetFieldValue("event_template_name", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szEventName);

					// this is the order
//event_instance_id, trigger_value, trigger_time, trigger_id, scene, Graphic_Action_Type, Action_Name, layer, event_template_name

#else //#ifndef NUCLEUS_PROMOLITE_CHANGE

					prs->GetFieldValue("itemid", szTemp);
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_nEventID = atoi(szTemp);
					prs->GetFieldValue("Host", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szHost);
					prs->GetFieldValue("ParsedValue", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szValue);
					prs->GetFieldValue("TriggerTime", szTemp);

//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Trigger time received: %s", szTemp); // Sleep(50); //(Dispatch message)
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_dblTriggerTime = atof(szTemp);// - 0.333;  // ten frames anticipation.

					if((int)pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_dblTriggerTime <= 0) { bInvalidTimes = true; break;}
					prs->GetFieldValue("analyzed_trigger_id", szTemp);
					if(szTemp.GetLength()) pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_nAnalyzedTriggerID = atoi(szTemp);

					prs->GetFieldValue("Source", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szSource);
					prs->GetFieldValue("Scene", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szScene);

					prs->GetFieldValue("Graphic_Action_Type", szTemp);
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_nType = atoi(szTemp);
					prs->GetFieldValue("Action_Name", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szIdentifier);

					prs->GetFieldValue("dest_type", szTemp);
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_nDestType = atoi(szTemp);

					prs->GetFieldValue("dest_module", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szDestModule);
					prs->GetFieldValue("layer", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szLayer);

// want to add promotor event name here
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szEventName="";

#endif //#ifndef NUCLEUS_PROMOLITE_CHANGE
					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
              ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:debug", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							// The error code is in e->m_nRetCode
              ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:debug", "Retrieve: Caught exception: out of memory");
						}
						else 
						{
							ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:debug", "Retrieve: Caught other exception.\n%s", szSQL);
						}
						e->Delete();
					} 
					catch( ... )
					{
						ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:debug", "Retrieve: Caught unknown exception.\n%s", szSQL);
					}

					pChannelObj->m_nEvents++;

					prs->MoveNext();
				}
			


				if(prs)
				{
		//			prs->MoveNext();
					prs->Close();
					delete prs;
				}
				LeaveCriticalSection(&ppromolite->m_data.m_critSQL);
				

if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) 	
{
int dbglist=0;
#ifndef NUCLEUS_PROMOLITE_CHANGE
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "***** begin: %d events now in Trigger buffer for %s:%d", 
										pChannelObj->m_nEvents,	pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum ); // Sleep(50); //(Dispatch message)
#else //#ifdef NUCLEUS_PROMOLITE_CHANGE
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "***** begin: %d events now in Trigger buffer", pChannelObj->m_nEvents); // Sleep(50); //(Dispatch message)
#endif //#ifdef NUCLEUS_PROMOLITE_CHANGE
while(dbglist<pChannelObj->m_nEvents)
{
					int harris = (int)pChannelObj->m_ppEvents[dbglist]->m_dblTriggerTime;

g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "   Trigger[%02d] atid=%d eid=%d type=%d [@%.3f (%02d:%02d:%02d)] [host:%s][scene:%s][layer:%s][id:%s][value:%s][dest:%d][mod:%s]",
							dbglist,
							pChannelObj->m_ppEvents[dbglist]->m_nAnalyzedTriggerID,
							pChannelObj->m_ppEvents[dbglist]->m_nEventID,
							pChannelObj->m_ppEvents[dbglist]->m_nType,
							pChannelObj->m_ppEvents[dbglist]->m_dblTriggerTime,
							((harris%86400)/3600),
							((harris/60)%60),
							(((harris)%60)%60),
							pChannelObj->m_ppEvents[dbglist]->m_szHost,
							pChannelObj->m_ppEvents[dbglist]->m_szScene,
							pChannelObj->m_ppEvents[dbglist]->m_szLayer,
							pChannelObj->m_ppEvents[dbglist]->m_szIdentifier,
							pChannelObj->m_ppEvents[dbglist]->m_szValue,
							pChannelObj->m_ppEvents[dbglist]->m_nDestType,
							pChannelObj->m_ppEvents[dbglist]->m_szDestModule
							); // Sleep(20); //(Dispatch message)
dbglist++;

}

#ifndef NUCLEUS_PROMOLITE_CHANGE
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "***** end Trigger buffer for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum ); // Sleep(50); //(Dispatch message)
#else //#ifdef NUCLEUS_PROMOLITE_CHANGE
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "***** end Trigger buffer"); // Sleep(50); //(Dispatch message)
#endif //#ifdef NUCLEUS_PROMOLITE_CHANGE

}
				if((pChannelObj->m_nEvents>0)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)&&(!bInvalidTimes))
				{

					bool bFirst = true;
					// now update status
					///// DEMOTODO get item id name in view
#ifdef NUCLEUS_PROMOLITE_CHANGE
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET trigger_status = %d WHERE ",
							ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"Schedule_Actions",   // the AnalyzedTriggerData table name
							PROMOLITE_FLAG_TRIGGERED
						);
#else //#ifndef NUCLEUS_PROMOLITE_CHANGE
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d WHERE ",
							ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the AnalyzedTriggerData table name
							PROMOLITE_FLAG_TRIGGERED
						);
#endif //#ifndef NUCLEUS_PROMOLITE_CHANGE

					char proj[4096];
					int n = 0;

					bool bTriggered = false;
					while((n<pChannelObj->m_nEvents)&&(!pChannelObj->m_bTriggerEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
					{
/*
					sprintf(proj, "%s: scene[%s] %s:%s", 
							pChannelObj->m_ppEvents[n]->m_szHost,
							pChannelObj->m_ppEvents[n]->m_szScene,
							pChannelObj->m_ppEvents[n]->m_szIdentifier,
							pChannelObj->m_ppEvents[n]->m_szValue);
*/
#ifdef NUCLEUS_PROMOLITE_CHANGE
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "trigger delay: waiting until %.3f (now=%.03f) or until changes", pChannelObj->m_ppEvents[n]->m_dblTriggerTime, pChannelObj->ReturnTriggerTickTime()); //(Dispatch message)
						while((pChannelObj->ReturnTriggerTickTime()<pChannelObj->m_ppEvents[n]->m_dblTriggerTime)
							&&(!pChannelObj->m_bTriggerEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))

#else //#ifndef NUCLEUS_PROMOLITE_CHANGE

if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "trigger delay: waiting until %.3f (now=%.03f) or until changes", pChannelObj->m_ppEvents[n]->m_dblTriggerTime, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)
						while((pChannelObj->m_dblAutomationTimeEstimate<pChannelObj->m_ppEvents[n]->m_dblTriggerTime)
							&&(!pChannelObj->m_bTriggerEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
#endif //NUCLEUS_PROMOLITE_CHANGE
						{

#ifdef NUCLEUS_PROMOLITE_CHANGE
							if((pChannelObj->m_ppEvents[n]->m_dblTriggerTime-pChannelObj->ReturnTriggerTickTime())>0.5) // if we have half a second, start this otherwise do not.
#else //#ifndef NUCLEUS_PROMOLITE_CHANGE
							if((pChannelObj->m_ppEvents[n]->m_dblTriggerTime-pChannelObj->m_dblAutomationTimeEstimate)>0.5) // if we have half a second, start this otherwise do not.
#endif //NUCLEUS_PROMOLITE_CHANGE
							{
								if(	
									  ( pChannelObj->m_nMinLyricAlias!=ppromolite->m_settings.m_nMinLyricAlias) 
									||(	pChannelObj->m_nMaxLyricAlias!=ppromolite->m_settings.m_nMaxLyricAlias) 
									)
								{//settings have changed
									if((ppromolite->m_settings.m_nMaxLyricAlias-ppromolite->m_settings.m_nMinLyricAlias)>(pChannelObj->m_nMaxLyricAlias-pChannelObj->m_nMinLyricAlias))
									{
										// have to increase the buffer....

	int nAliases = ppromolite->m_settings.m_nMaxLyricAlias - ppromolite->m_settings.m_nMinLyricAlias + 1;
	if(nAliases<=0) nAliases = 10; //min set
	LyricAlias_t** pNew = new LyricAlias_t*[nAliases];

	if(pNew)//pChannelObj->m_ppLyricAlias)
	{
		int i=0;
		while( i < nAliases)
		{
			if(i<pChannelObj->m_nNumAliasArray)
			{
				pNew[i] = pChannelObj->m_ppLyricAlias[i];
			}
			else
			{
				LyricAlias_t* pAlias = new LyricAlias_t;
				if(pAlias)
				{
					pNew[i] = pAlias;
					pAlias->szMessage = "";
					pAlias->nAlias = -1;
					pAlias->bMessageLoaded = false;
					pAlias->bMessageRead = false;
				}
			}
			i++;
		}
		delete [] pChannelObj->m_ppLyricAlias;
		pChannelObj->m_ppLyricAlias = pNew;
		pChannelObj->m_nNumAliasArray = nAliases;

	}

									}
									pChannelObj->m_nMinLyricAlias=ppromolite->m_settings.m_nMinLyricAlias;
									pChannelObj->m_nMaxLyricAlias=ppromolite->m_settings.m_nMaxLyricAlias;
								}
							}

#ifdef NUCLEUS_PROMOLITE_CHANGE
	_ftime(&pChannelObj->m_timebTriggerTick); // the last time check inside the thread
							if(
								  (n>0)
								&&((pChannelObj->m_ppEvents[n]->m_dblTriggerTime-pChannelObj->ReturnTriggerTickTime())>(g_ppromolite?(g_ppromolite->m_settings.m_nEventBufferIdleRefreshMS/1000.0):2.0)) // if we have a full second and we are not a full buffer or at the start already, reload the buffer
								)
							{
								pChannelObj->m_bTriggerEventsChanged = true;
							}
#endif //NUCLEUS_PROMOLITE_CHANGE

	_ftime(&pChannelObj->m_timebTriggerTick); // the last time check inside the thread
							Sleep(1);
						}
#ifndef NUCLEUS_PROMOLITE_CHANGE
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "finished trigger delay: now=%.3f - %d %d %d", pChannelObj->m_dblAutomationTimeEstimate, pChannelObj->m_bTriggerEventsChanged, g_bKillThread, pChannelObj->m_bKillAutomationThread); //(Dispatch message)
#else //NUCLEUS_PROMOLITE_CHANGE
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "finished trigger delay: now=%.3f - %d %d %d", pChannelObj->ReturnTriggerTickTime(), pChannelObj->m_bTriggerEventsChanged, g_bKillThread, pChannelObj->m_bKillAutomationThread); //(Dispatch message)
#endif //NUCLEUS_PROMOLITE_CHANGE

						if((!pChannelObj->m_bTriggerEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
						{

// trigger graphics device

							if((pChannelObj->m_ppEvents[n]->m_nAnalyzedTriggerID>0)&&(pChannelObj->m_ppEvents[n]->m_szIdentifier.GetLength()))
							{
	//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "pchEncodedLocal: %s", pchEncodedLocal);  Sleep(50);//(Dispatch message)
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "want to send %s", proj);  Sleep(50);//(Dispatch message)

								CString szII;
								if(pChannelObj->m_ppEvents[n]->m_nType==0)  //anim
								{
	/*
	[18:44] rpbutterphly: Open
	[18:44] rpbutterphly: Load
	[18:44] rpbutterphly: Play
	[18:44] rpbutterphly: Stop
	[18:45] rpbutterphly: Close
	*/
/*
3000 Duet LEX
3001 Duet HyperX
3002 Duet MicroX
3003 Channel Box
3004 CAL Box
*/

//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Type is anim, with dest type: %d", pChannelObj->m_ppEvents[n]->m_nDestType);  Sleep(50);//(Dispatch message)

									switch(pChannelObj->m_ppEvents[n]->m_nDestType)
									{
									case PROMOLITE_RULE_DESTTYPE_MIRANDA_IS2://					2001  // Imagestore 2
									case PROMOLITE_RULE_DESTTYPE_MIRANDA_INT://					2002  // Intuition
									case PROMOLITE_RULE_DESTTYPE_MIRANDA_IS300://				2003  // Imagestore 300
									case PROMOLITE_RULE_DESTTYPE_MIRANDA_ISHD://				2004  // Imagestore HD
										{
											// no anims supported yet
										} break;

									case PROMOLITE_RULE_DESTTYPE_CHYRON_CHANNELBOX://		3003  // Channel Box
										{
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Open")==0)
											{
												szII.Format("B\\OP\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene);
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Load")==0)
											{
												szII.Format("B\\LO\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene);
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Play")==0)
											{
												szII.Format("B\\PL\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene);
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Stop")==0)
											{
												szII.Format("B\\ST\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene);
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Close")==0)
											{
												szII.Format("B\\CL\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene);
											}

											//system-wide commands
											else  
												if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase(ppromolite->m_settings.m_pszStopAll?ppromolite->m_settings.m_pszStopAll:"StopAll")==0)
											{
/*
Courtney Tompos
Software Engineer
Chyron Corporation
631-845-2141:

To stop all scenes (but leave them open), transmit:
Y\<1B>\\<CR><LF>

To close all scenes, transmit either:
Y\<CD>\\<CR><LF>
Or
Y\<FE>\\<CR><LF>
*/

												szII.Format("Y\\%c\\\\", 0x1b);
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase(ppromolite->m_settings.m_pszCloseAll?ppromolite->m_settings.m_pszCloseAll:"CloseAll")==0)
											{
												szII.Format("Y\\%c\\\\", 0xcd); // 0xcd seems to be a bit faster than 0xfe, maybe not appreciably so, but given the choice we use 0xcd
											}
											else  // prob a mistake so just use update.
											{

													//��#
												char* pch = bu.EscapeChar(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0), '\\');
												if(pch)
												{
													szII.Format("B\\UP\\%s\\%s%s�%s\\\\", 
														pChannelObj->m_ppEvents[n]->m_szScene, 
														pChannelObj->m_ppEvents[n]->m_szIdentifier, 
														(ppromolite->m_settings.m_bUseUTF8?"�":""),
														pch);
													free(pch);
												}
												else
												{
													szII.Format("B\\UP\\%s\\%s%s�%s\\\\", 
														pChannelObj->m_ppEvents[n]->m_szScene, 
														pChannelObj->m_ppEvents[n]->m_szIdentifier, 
														(ppromolite->m_settings.m_bUseUTF8?"�":""),
														pChannelObj->m_ppEvents[n]->m_szValue);
												}
											}
										} break;
										// following cases are "Lyric"
									case PROMOLITE_RULE_DESTTYPE_CHYRON_LEX://					3000	// Duet LEX
									case PROMOLITE_RULE_DESTTYPE_CHYRON_HYPERX://				3001  // Duet HyperX
									case PROMOLITE_RULE_DESTTYPE_CHYRON_MICROX://				3002  // Duet MicroX
									case PROMOLITE_RULE_DESTTYPE_CHYRON_CALBOX://				3004  // CAL Box
										{
/*
[15:33] rpbutterphly: load   = W command
[15:33] rpbutterphly: play   = not V\6   always use  Y\<D5><F3> for release, and immediate return
[15:33] rpbutterphly: release = V\5\15
[15:33] rpbutterphly: unload  //not impl
[15:33] rpbutterphly: clear    Y\<D5><FE><Frame Buffer>\\<CR><LF>

*/
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Load")==0)
											{
/*
												if(pChannelObj->m_nLastLyricAlias<0)
												{
													pChannelObj->m_nLastLyricAlias = ppromolite->m_settings.m_nMinLyricAlias;
												}
												else
												{
													pChannelObj->m_nLastLyricAlias++;  
													if (pChannelObj->m_nLastLyricAlias>ppromolite->m_settings.m_nMaxLyricAlias)
														pChannelObj->m_nLastLyricAlias = ppromolite->m_settings.m_nMinLyricAlias;
												}
												szII.Format("W\\%d\\%s\\", pChannelObj->m_nLastLyricAlias, pChannelObj->m_ppEvents[n]->m_szScene);
*/
///// new method
												int idxA = pChannelObj->ReturnAliasIndex(pChannelObj->m_ppEvents[n]->m_szScene);

												if(idxA<0)
												{
													idxA = pChannelObj->AddAlias(pChannelObj->m_ppEvents[n]->m_szScene);
												}

												if(idxA<0)
												{												
//													szII.Format("W\\%d\\%s\\", pChannelObj->m_nLastLyricAlias, pChannelObj->m_ppEvents[n]->m_szScene); // no choice
													szII.Format("W\\%s\\%s\\", pChannelObj->m_ppEvents[n]->m_szScene, pChannelObj->m_ppEvents[n]->m_szScene); // no choice
												}
												else
												{										
													szII.Format("W\\%d\\%s\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pChannelObj->m_ppEvents[n]->m_szScene);

													// prob need to set this after a success code .... at some point.
													pChannelObj->m_ppLyricAlias[idxA]->bMessageLoaded = true;
												}

////////////////
												for(int maxfields=0; maxfields<99; maxfields++)
												{
													szII += " \\";
												}
												szII += "\\";  // term II


//												pChannelObj->m_nCurrentMessage = atoi(pChannelObj->m_ppEvents[n]->m_szScene); // not sure why bothering to do this.
//												pChannelObj->m_bMessageLoaded = true;
//												pChannelObj->m_bMessageRead = false;  // reset this guy


											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Read")==0)
											{
												int idxA = pChannelObj->ReturnAliasIndex(pChannelObj->m_ppEvents[n]->m_szScene);
												if(idxA<0)
												{

													szII.Format("V\\5\\13\\1\\%s\\%s\\1\\\\", pChannelObj->m_ppEvents[n]->m_szLayer, pChannelObj->m_ppEvents[n]->m_szScene);
//													szII.Format("V\\5\\13\\1\\%s\\%d\\1\\\\", pChannelObj->m_ppEvents[n]->m_szLayer, pChannelObj->m_nLastLyricAlias);
												}
												else
												{
													szII.Format("V\\5\\13\\1\\%s\\%d\\1\\\\", pChannelObj->m_ppEvents[n]->m_szLayer, pChannelObj->m_ppLyricAlias[idxA]->nAlias);
													pChannelObj->m_ppLyricAlias[idxA]->bMessageRead = true; 
												}
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Play")==0)
											{
												szII.Format("Y\\%c%c%s\\\\", 0xd5, 0xf3, pChannelObj->m_ppEvents[n]->m_szLayer);  //we'll see if this works.
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Release")==0)
											{
												if(ppromolite->m_settings.m_bReleaseIsV_5_15)
												{
													szII.Format("V\\5\\15\\\\");
												}
												else
												{
/*

Andrea Guarini	aguarini@chyron.com
The only other way to release a pause is via a macro (E) command.  The
following will release a pause on Frame Buffer 2.

E\Lyric.FrameBuffer(2).ReleasePause\\

*/

													szII.Format("E\\Lyric.FrameBuffer(%s).ReleasePause\\\\", pChannelObj->m_ppEvents[n]->m_szLayer);  //we'll see if this works.

												}

											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Unload")==0)
											{
												// same as clear for now.
												szII.Format("Y\\%c%c%s\\\\", 0xd5, 0xfe, pChannelObj->m_ppEvents[n]->m_szLayer);  //we'll see if this works.
												int idxA = pChannelObj->ReturnAliasIndex(pChannelObj->m_ppEvents[n]->m_szScene);
												if(idxA>=0)
												{
													pChannelObj->m_ppLyricAlias[idxA]->bMessageLoaded = false;
												}

											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Clear")==0)
											{
												szII.Format("Y\\%c%c%s\\\\", 0xd5, 0xfe, pChannelObj->m_ppEvents[n]->m_szLayer);  //we'll see if this works.
												int idxA = pChannelObj->ReturnAliasIndex(pChannelObj->m_ppEvents[n]->m_szScene);
												if(idxA>=0)
												{
													pChannelObj->m_ppLyricAlias[idxA]->bMessageLoaded = false;
												}
											}
											else  // prob a mistake so just use update.
											{
												int idxA = pChannelObj->ReturnAliasIndex(pChannelObj->m_ppEvents[n]->m_szScene);
												if(idxA>=0)
												{
													char* pch = bu.EscapeChar(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0), '\\');
													if(pch)
													{
														if(pChannelObj->m_ppLyricAlias[idxA]->bMessageRead)
															szII.Format("U\\*\\%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pch);
														else
															szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pChannelObj->m_ppEvents[n]->m_szIdentifier, pch);
														free(pch);
													}
													else
													{
														if(pChannelObj->m_ppLyricAlias[idxA]->bMessageRead)
															szII.Format("U\\*\\%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szValue);
														else
															szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szValue);

													}
												}
											}
										} break;

									case PROMOLITE_RULE_DESTTYPE_HARRIS_ICONII://				5000
										{

/*
// IconStation II
IconStation supports four CII commands for greater output flexibility.
The first CII command allows you to load a layout and fire a Salvo.
The second CII command allows you to clear preview and clear output.
The third CII command allows you to poll the IconStation system to confirm IconStation is still running and responding to commands.
The fourth CII command allows you to update your item source text at any time.

Channel Entry
The Channel entry equals A or B.
	A = program channel
	B = preview channel

T\7\LayoutSalvo\Channel\\   - Load a Layout and Fire a Salvo
T\14\Channel\\   - Clear Preview and Clear Output
*\\   - Poll IconStation (returns *<CRLF>)
I\42\<LayoutTags><LayoutName>TheLayoutNameGoesHere</LayoutName><Region><Name>TheItemNameGoesHere</Name><Tag><Name>TheTagNameGoesHere</Name><Text>TheTextGoesHere</Text></Tag></Region></LayoutTags>\\    - Update Item Source Text
Example Command:
I\42\<LayoutTags><LayoutName>UpNext</LayoutName><Region><Name>Title1</Name><Tag> <Name>Field1</Name><Text>Economic Outlook</Text></Tag></Region></LayoutTags>\\

//stuff has this structure:
<LayoutTags>
	<LayoutName>InfoChannel</LayoutName>
	<Region> // region 1
		<Name>Crawl</Name> // the name of the region
		<Tag> // a "tag" within a region
			<Name>1-1</Name>  // predefined tag name such as Field1, Field2, 1-1, 2-1, etc, see documentation
			<Text>blarg</Text>  // text to put in the tag (field) in the region
		</Tag>
	</Region>
	<Region> // an additional region in the layout - one with multiple "tags"
		<Name>Title_area</Name>
		<Tag>
			<Name>Field1</Name>
			<Text>coming up next</Text>
		</Tag>
		<Tag>
			<Name>Field2</Name>
			<Text>more text</Text>
		</Tag>
	</Region>
</LayoutTags>


Enter the name of the data tag between the second set of <Name></Name> tags. The data
tag name depends on the item type

Item Type Examples
Title To update the text on the first line of your title item enter:
Field1
To update the text on the second line of your title item enter:
Field2
To update the text on the third line of your title item enter:
Field3
Continue entering the next field number for the following lines in
your title item.
Crawl To update the text for a crawl item enter:
1-1
Roll To update the text for a roll item enter:
1-1
Title Table To update the text in a title table item, use the row-column
naming conventation. See the following examples.
To update the text in the first row, first column enter:
1-1
To update the text in the second row, first column enter:
2-1
To update the text in the first row, second column enter:
1-2
To update the text in the second row, second column enter:
2-2

	To accomplish this, the identifier will have the following format:
	RegionName:TagName

*/


											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Load")==0)
											{
												switch(ppromolite->m_settings.m_nLayoutSalvoFormat)
												{
												default:
												case PROMOLITE_HARRIS_ICONII_LSFORMAT_NUM4://				0 // 4 digit numerical LayoutSalvoFormat
													{
														szII.Format("T\\7\\%s00\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene,pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												case PROMOLITE_HARRIS_ICONII_LSFORMAT_NUM8://				1 // 8 digit numerical LayoutSalvoFormat
													{
														szII.Format("T\\7\\%s0000\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene,pChannelObj->m_ppEvents[n]->m_szLayer);
													}	break;
												case PROMOLITE_HARRIS_ICONII_LSFORMAT_ALPHA://			2 // alphanumerical string LayoutSalvoFormat
													{
														szII.Format("T\\7\\%s/\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene,pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												}
												
											}
											else
/*
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("LoadFire")==0)
											{
												switch(ppromolite->m_settings.m_nLayoutSalvoFormat)
												{
												default:
												case PROMOLITE_HARRIS_ICONII_LSFORMAT_NUM4://				0 // 4 digit numerical LayoutSalvoFormat
												case PROMOLITE_HARRIS_ICONII_LSFORMAT_NUM8://				1 // 8 digit numerical LayoutSalvoFormat
													{
														szII.Format("T\\7\\%s%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene,pChannelObj->m_ppEvents[n]->m_szValue,pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												case PROMOLITE_HARRIS_ICONII_LSFORMAT_ALPHA://			2 // alphanumerical string LayoutSalvoFormat
													{
														szII.Format("T\\7\\%s/%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene,pChannelObj->m_ppEvents[n]->m_szValue,pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												}
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Fire")==0)
											{
												switch(ppromolite->m_settings.m_nLayoutSalvoFormat)
												{
												default:
												case PROMOLITE_HARRIS_ICONII_LSFORMAT_NUM4://				0 // 4 digit numerical LayoutSalvoFormat
													{
														szII.Format("T\\7\\00%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szValue,pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												case PROMOLITE_HARRIS_ICONII_LSFORMAT_NUM8://				1 // 8 digit numerical LayoutSalvoFormat
													{
														szII.Format("T\\7\\0000%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szValue,pChannelObj->m_ppEvents[n]->m_szLayer);
													}	break;
												case PROMOLITE_HARRIS_ICONII_LSFORMAT_ALPHA://			2 // alphanumerical string LayoutSalvoFormat
													{
														szII.Format("T\\7\\/%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szValue,pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												}
											}
											else
*/
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Clear")==0)
											{
												szII.Format("T\\14\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szLayer);
											}
											else  // its a salvo
											{
												switch(ppromolite->m_settings.m_nLayoutSalvoFormat)
												{
												default:
												case PROMOLITE_HARRIS_ICONII_LSFORMAT_NUM4://				0 // 4 digit numerical LayoutSalvoFormat
													{
														szII.Format("T\\7\\00%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												case PROMOLITE_HARRIS_ICONII_LSFORMAT_NUM8://				1 // 8 digit numerical LayoutSalvoFormat
													{
														szII.Format("T\\7\\0000%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szLayer);
													}	break;
												case PROMOLITE_HARRIS_ICONII_LSFORMAT_ALPHA://			2 // alphanumerical string LayoutSalvoFormat
													{
														szII.Format("T\\7\\/%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												}

/*
// update a region...?
												CString szRegion; 
												CString szTag;
												char* pszValue = bu.XMLEncode(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0)); // have to XML encode any values...I think.  this is a point to be clarified.

												int nColon = pChannelObj->m_ppEvents[n]->m_szIdentifier.ReverseFind(':');
												if(nColon>=0)
												{
													szRegion=pChannelObj->m_ppEvents[n]->m_szIdentifier.Left(nColon);
													szTag=pChannelObj->m_ppEvents[n]->m_szIdentifier.Right(nColon+1);
													if(szTag.IsEmpty()) szTag = "Field1"; // "safe" default
												}
												else
												{
													szRegion = pChannelObj->m_ppEvents[n]->m_szIdentifier;
													szTag = "Field1"; // "safe" default
												}

												szII.Format("I\\42\\<LayoutTags><LayoutName>%s</LayoutName><Region><Name>%s</Name><Tag><Name>%s</Name><Text>%s</Text></Tag></Region></LayoutTags>\\\\", 
													pChannelObj->m_ppEvents[n]->m_szScene,
													szRegion, szTag, (pszValue?pszValue:""));
												if(pszValue) free(pszValue);
*/

											}

										} break;

									}

								}
								else  // export
								{
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Type is export, with dest type: %d", pChannelObj->m_ppEvents[n]->m_nDestType);  Sleep(50);//(Dispatch message)
									switch(pChannelObj->m_ppEvents[n]->m_nDestType)
									{
									case PROMOLITE_RULE_DESTTYPE_MIRANDA_IS2://					2001  // Imagestore 2
									case PROMOLITE_RULE_DESTTYPE_MIRANDA_INT://					2002  // Intuition
									case PROMOLITE_RULE_DESTTYPE_MIRANDA_IS300://				2003  // Imagestore 300
									case PROMOLITE_RULE_DESTTYPE_MIRANDA_ISHD://				2004  // Imagestore HD
										{
											// setting the datasource
											char* pch = is2.EscapeSpecialChars(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0));
											if(pch)
											{
												szII.Format("m003%s|%s",pChannelObj->m_ppEvents[n]->m_szIdentifier, pch);
												free(pch);
											}
											else
											{
												szII.Format("m003%s|%s",pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szValue);
											}

										} break;
									case PROMOLITE_RULE_DESTTYPE_CHYRON_CHANNELBOX://		3003  // Channel Box
										{
													//��#
											char* pch = bu.EscapeChar(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0), '\\');
											if(pch)
											{
												szII.Format("B\\UP\\%s\\%s%s�%s\\\\", 
													pChannelObj->m_ppEvents[n]->m_szScene, 
													pChannelObj->m_ppEvents[n]->m_szIdentifier, 
													(ppromolite->m_settings.m_bUseUTF8?"�":""),
													pch);
												free(pch);
											}
											else
											{
												szII.Format("B\\UP\\%s\\%s%s�%s\\\\", 
													pChannelObj->m_ppEvents[n]->m_szScene, 
													pChannelObj->m_ppEvents[n]->m_szIdentifier, 
													(ppromolite->m_settings.m_bUseUTF8?"�":""),
													pChannelObj->m_ppEvents[n]->m_szValue);
											}
										} break;
										// following cases are "Lyric"
									case PROMOLITE_RULE_DESTTYPE_CHYRON_LEX://					3000	// Duet LEX
									case PROMOLITE_RULE_DESTTYPE_CHYRON_HYPERX://				3001  // Duet HyperX
									case PROMOLITE_RULE_DESTTYPE_CHYRON_MICROX://				3002  // Duet MicroX
									case PROMOLITE_RULE_DESTTYPE_CHYRON_CALBOX://				3004  // CAL Box
										{
											int idxA = pChannelObj->ReturnAliasIndex(pChannelObj->m_ppEvents[n]->m_szScene);
											if(idxA>=0)
											{
												char* pch = bu.EscapeChar(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0), '\\');
												if(pch)
												{
													if(pChannelObj->m_ppLyricAlias[idxA]->bMessageRead)
														szII.Format("U\\*\\%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pch);
													else
														szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pChannelObj->m_ppEvents[n]->m_szIdentifier, pch);
													free(pch);
												}
												else
												{
													if(pChannelObj->m_ppLyricAlias[idxA]->bMessageRead)
														szII.Format("U\\*\\%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szValue);
													else
														szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szValue);

												}
											}
											else
											{
// have to try....  but can't.. theres nothing to find.
/*
												char* pch = bu.EscapeChar(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0), '\\');
												if(pch)
												{
													if(pChannelObj->m_bMessageRead)
														szII.Format("U\\*\\%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pch);
													else
														szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_nLastLyricAlias, pChannelObj->m_ppEvents[n]->m_szIdentifier, pch);
													free(pch);
												}
												else
												{
													if(pChannelObj->m_bMessageRead)
														szII.Format("U\\*\\%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szValue);
													else
														szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_nLastLyricAlias, pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szValue);

												}
*/
											}
										} break;

									case PROMOLITE_RULE_DESTTYPE_HARRIS_ICONII://				5000
										{
											CString szRegion; 
											CString szTag;
											char* pszValue = bu.XMLEncode(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0)); // have to XML encode any values...I think.  this is a point to be clarified.

											int nColon = pChannelObj->m_ppEvents[n]->m_szIdentifier.ReverseFind(':');
											if(nColon>=0)
											{
												szRegion=pChannelObj->m_ppEvents[n]->m_szIdentifier.Left(nColon);
												szTag=pChannelObj->m_ppEvents[n]->m_szIdentifier.Mid(nColon+1);
												if(szTag.IsEmpty()) szTag = "Field1"; // "safe" default
											}
											else
											{
												szRegion = pChannelObj->m_ppEvents[n]->m_szIdentifier;
												szTag = "Field1"; // "safe" default
											}

											szII.Format("I\\42\\<LayoutTags><LayoutName>%s</LayoutName><Region><Name>%s</Name><Tag><Name>%s</Name><Text>%s</Text></Tag></Region></LayoutTags>\\\\", 
												pChannelObj->m_ppEvents[n]->m_szScene,
												szRegion, szTag, (pszValue?pszValue:""));
											if(pszValue) free(pszValue);

/*
// IconStation II
IconStation supports four CII commands for greater output flexibility.
The first CII command allows you to load a layout and fire a Salvo.
The second CII command allows you to clear preview and clear output.
The third CII command allows you to poll the IconStation system to confirm IconStation is still running and responding to commands.
The fourth CII command allows you to update your item source text at any time.

Channel Entry
The Channel entry equals A or B.
	A = program channel
	B = preview channel

T\7\LayoutSalvo\Channel\\   - Load a Layout and Fire a Salvo
T\14\Channel\\   - Clear Preview and Clear Output
*\\   - Poll IconStation (returns *<CRLF>)
I\42\<LayoutTags><LayoutName>TheLayoutNameGoesHere</LayoutName><Region><Name>TheItemNameGoesHere</Name><Tag><Name>TheTagNameGoesHere</Name><Text>TheTextGoesHere</Text></Tag></Region></LayoutTags>\\    - Update Item Source Text
Example Command:
I\42\<LayoutTags><LayoutName>UpNext</LayoutName><Region><Name>Title1</Name><Tag> <Name>Field1</Name><Text>Economic Outlook</Text></Tag></Region></LayoutTags>\\

//stuff has this structure:
<LayoutTags>
	<LayoutName>InfoChannel</LayoutName>
	<Region> // region 1
		<Name>Crawl</Name> // the name of the region
		<Tag> // a "tag" within a region
			<Name>1-1</Name>  // predefined tag name such as Field1, Field2, 1-1, 2-1, etc, see documentation
			<Text>blarg</Text>  // text to put in the tag (field) in the region
		</Tag>
	</Region>
	<Region> // an additional region in the layout - one with multiple "tags"
		<Name>Title_area</Name>
		<Tag>
			<Name>Field1</Name>
			<Text>coming up next</Text>
		</Tag>
		<Tag>
			<Name>Field2</Name>
			<Text>more text</Text>
		</Tag>
	</Region>
</LayoutTags>
*/




										} break;
									}
								}
//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "II is now: %s", szII);  Sleep(50);//(Dispatch message)
								char* pchEncodedLocal = dbSet.EncodeQuotes(szII);


	if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER)
	{
#ifndef NUCLEUS_PROMOLITE_CHANGE

								if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_SENTINEL)
								{
									sprintf(errorstring, "%s:%d Trigger", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
								}
								else
								if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_HELIOS)
								{
									sprintf(errorstring, "Omnibus %d Trigger", pChannelObj->m_nChannelID);
								}

#else //#ifdef NUCLEUS_PROMOLITE_CHANGE
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Promolite trigger");
#endif // #ifdef NUCLEUS_PROMOLITE_CHANGE

	}

//AS-RUN!
								g_ppromolite->SendAsRunMsg(CX_SENDMSG_INFO, pChannelObj->m_nChannelID, pChannelObj->m_ppEvents[n]->m_szEventName.GetBuffer(0), errorstring, "Triggered graphics device at %s with: %s", pChannelObj->m_ppEvents[n]->m_szHost, szII);

// want to change it to the following:
																//g_ppromolite->SendAsRunMsg(CX_SENDMSG_INFO, errorstring, "%s: Triggered graphics device at %s with: %s", pChannelObj->m_ppEvents[n]->m_szEventName, pChannelObj->m_ppEvents[n]->m_szHost, szII);

								if((pchEncodedLocal)&&(strlen(pchEncodedLocal)>0))
								{
									_snprintf(szCommandSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(local, remote, action, host, timestamp, username) \
VALUES ('%s','', %d, '%s', %d.%d, 'sys')", //HARDCODE
																	pChannelObj->m_ppEvents[n]->m_szDestModule,//"Libretto", //pEndObj->m_pszDBName?pEndObj->m_pszDBName:"Libretto",  // need to get this.
										//							pEndObj->m_pszQueue?pEndObj->m_pszQueue:"Queue",  // ok need to change this too!  argh! its not the endpoint object, that is the automation module....
																	"Command_Queue",  // ok need to change this too!  argh! its not the endpoint object, that is the automation module....
																	pchEncodedLocal?pchEncodedLocal:szII,
																	256,
																	pChannelObj->m_ppEvents[n]->m_szHost,
																	g_ppromolite->m_data.m_timebTick.time,
																	g_ppromolite->m_data.m_timebTick.millitm
																	);
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Trigger SQL: %s", szCommandSQL); // Sleep(50); //(Dispatch message)
									if(dbSet.ExecuteSQL(pdbConnSet, szCommandSQL, errorstring)<DB_SUCCESS)
									{
								//**MSG
		g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring); // Sleep(50); //(Dispatch message)

// need to add ERROR to the db for the trigger item status
									}
									else
									{

	if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER)
	{
		ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "command %d sent [H est time %f][trig %f]: %s", 
			pChannelObj->m_ppEvents[n]->m_nEventID,
#ifdef NUCLEUS_PROMOLITE_CHANGE
			pChannelObj->ReturnTriggerTickTime(),
#else //#ifndef NUCLEUS_PROMOLITE_CHANGE
			pChannelObj->m_dblAutomationTimeEstimate, 
#endif //NUCLEUS_PROMOLITE_CHANGE
			pChannelObj->m_ppEvents[n]->m_dblTriggerTime, szII); // Sleep(50);//(Dispatch message)
	}
										bTriggered = true;

#ifdef NUCLEUS_PROMOLITE_CHANGE

										if(
											  (
													(n<(pChannelObj->m_nEvents-1))
													&&(pChannelObj->m_ppEvents[n+1])
													&&(pChannelObj->m_ppEvents[n]->m_nEventID!=pChannelObj->m_ppEvents[n+1]->m_nEventID)
												)
											||(nLastID !=pChannelObj->m_ppEvents[n]->m_nEventID)
											)
//										if(nLastID !=pChannelObj->m_ppEvents[n]->m_nEventID)
										{

if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) 
{
g_ppromolite->m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "PromoLite:debug", "PromoLiteDonePurgeThread %d %d %d", 
												nLastID, 
												pChannelObj->m_ppEvents[n]->m_nEventID, 
												((
													(n<(pChannelObj->m_nEvents-1))
													&&(pChannelObj->m_ppEvents[n+1])
												 )?(pChannelObj->m_ppEvents[n+1]->m_nEventID):-1) 
												 );
}


											nLastID = pChannelObj->m_ppEvents[n]->m_nEventID;

											if(_beginthread(PromoLiteDonePurgeThread, 0, (void*)pChannelObj)==-1)
											{
												//error.

												//**MSG
											}
										}

										if(bFirst)
										{
											bFirst = false;
											sprintf(proj, "trigger_id = %d ", pChannelObj->m_ppEvents[n]->m_nAnalyzedTriggerID );
										}
										else
										{
											sprintf(proj, "OR trigger_id = %d ", pChannelObj->m_ppEvents[n]->m_nAnalyzedTriggerID );
										}
#else //#ifndef NUCLEUS_PROMOLITE_CHANGE
										if(bFirst)
										{
											bFirst = false;
											sprintf(proj, "analyzed_trigger_id = %d ", pChannelObj->m_ppEvents[n]->m_nAnalyzedTriggerID );
										}
										else
										{
											sprintf(proj, "OR analyzed_trigger_id = %d ", pChannelObj->m_ppEvents[n]->m_nAnalyzedTriggerID );
										}
#endif //#ifndef NUCLEUS_PROMOLITE_CHANGE

										strcat(szSQL, proj);

									}
								}
								if(pchEncodedLocal) free(pchEncodedLocal);

/*
								// now update status
								///// DEMOTODO get item id name in view
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d WHERE analyzed_trigger_id = %d",
										ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the AnalyzedTriggerData table name
										PROMOLITE_FLAG_TRIGGERED, pChannelObj->m_ppEvents[n]->m_nAnalyzedTriggerID
									);

	//ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "AnalyzedTriggerDataStatus SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
								if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
								{
								//**MSG
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing trigger SQL: %s", errorstring); // Sleep(50); //(Dispatch message)
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "error sending command: %s", szII); // Sleep(50);//(Dispatch message)
								}
*/
							}
						}

						if((bTriggered)&&((n>=pChannelObj->m_nEvents-1)||(pChannelObj->m_ppEvents[n]->m_dblTriggerTime<pChannelObj->m_ppEvents[n+1]->m_dblTriggerTime-0.666)||(pChannelObj->m_bTriggerEventsChanged)))  // have 2/3 a sec. or if thing have changed we need to set the ones we alredy did.
						{
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "AnalyzedTriggerDataStatus SQL: %s", szSQL); // Sleep(50);//(Dispatch message)
							if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
							{
							//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing trigger SQL: %s", errorstring);  //Sleep(50); //(Dispatch message)
							}
							else
							{
#ifdef NUCLEUS_PROMOLITE_CHANGE
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET trigger_status = %d WHERE ",
										ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"Schedule_Actions",   // the AnalyzedTriggerData table name
										PROMOLITE_FLAG_TRIGGERED
									);
#else //#ifndef NUCLEUS_PROMOLITE_CHANGE
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d WHERE ",
										ppromolite->m_settings.m_pszAnalyzedTriggerData?ppromolite->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the AnalyzedTriggerData table name
										PROMOLITE_FLAG_TRIGGERED
									);
#endif //#ifndef NUCLEUS_PROMOLITE_CHANGE
								bTriggered = false;
								bFirst = true;
							}
						}

						n++;
					}
				}


				if((pChannelObj->m_nEvents == 0)&&(!bInvalidTimes)) // got nothing in the buffer.
				{
#ifdef NUCLEUS_PROMOLITE_CHANGE
					nLastID = 0;
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) 
{
g_ppromolite->m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "PromoLite:debug", "PromoLiteDonePurgeThread no events in buffer");
}

					if(_beginthread(PromoLiteDonePurgeThread, 0, (void*)pChannelObj)==-1)
					{
						//error.
						//**MSG
					}
#endif //#ifdef NUCLEUS_PROMOLITE_CHANGE
					

if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Nothing in trigger buffer, waiting for changes"); //(Dispatch message)
					while((!pChannelObj->m_bTriggerEventsChanged)&&(!ppromolite->m_data.m_bNearEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
					{

							if(	
									( pChannelObj->m_nMinLyricAlias!=ppromolite->m_settings.m_nMinLyricAlias) 
								||(	pChannelObj->m_nMaxLyricAlias!=ppromolite->m_settings.m_nMaxLyricAlias) 
								)
							{//settings have changed
								if((ppromolite->m_settings.m_nMaxLyricAlias-ppromolite->m_settings.m_nMinLyricAlias)>(pChannelObj->m_nMaxLyricAlias-pChannelObj->m_nMinLyricAlias))
								{
									// have to increase the buffer....

int nAliases = ppromolite->m_settings.m_nMaxLyricAlias - ppromolite->m_settings.m_nMinLyricAlias + 1;
if(nAliases<=0) nAliases = 10; //min set
LyricAlias_t** pNew = new LyricAlias_t*[nAliases];

if(pNew)//pChannelObj->m_ppLyricAlias)
{
	int i=0;
	while( i < nAliases)
	{
		if(i<pChannelObj->m_nNumAliasArray)
		{
			pNew[i] = pChannelObj->m_ppLyricAlias[i];
		}
		else
		{
			LyricAlias_t* pAlias = new LyricAlias_t;
			if(pAlias)
			{
				pNew[i] = pAlias;
				pAlias->szMessage = "";
				pAlias->nAlias = -1;
				pAlias->bMessageLoaded = false;
				pAlias->bMessageRead = false;
			}
		}
		i++;
	}
	delete [] pChannelObj->m_ppLyricAlias;
	pChannelObj->m_ppLyricAlias = pNew;
	pChannelObj->m_nNumAliasArray = nAliases;

}

								}
								pChannelObj->m_nMinLyricAlias=ppromolite->m_settings.m_nMinLyricAlias;
								pChannelObj->m_nMaxLyricAlias=ppromolite->m_settings.m_nMaxLyricAlias;
							}
						

						
_ftime(&pChannelObj->m_timebTriggerTick); // the last time check inside the thread
						Sleep(1);
					}
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TRIGGER) g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "finished waiting for changes after empty trigger buffer %d %d %d %d", pChannelObj->m_bTriggerEventsChanged, ppromolite->m_data.m_bNearEventsChanged, g_bKillThread, pChannelObj->m_bKillAutomationThread); //(Dispatch message)
				} //	else  // finished or expired the buffer, go around again.
			}
			else
			{

#ifndef NUCLEUS_PROMOLITE_CHANGE
				
	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_SENTINEL)
	{
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Trigger ERROR for %s:%d:  %s",pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum, errorstring); // Sleep(50);//(Dispatch message)
	}
	else
	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_HELIOS)
	{
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Trigger ERROR for Omnibus stream %d:  %s",pChannelObj->m_nChannelID, errorstring); // Sleep(50);//(Dispatch message)
	}
#else //#ifdef NUCLEUS_PROMOLITE_CHANGE
ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Trigger ERROR %s", errorstring); // Sleep(50);//(Dispatch message)
#endif // #ifdef NUCLEUS_PROMOLITE_CHANGE

				LeaveCriticalSection(&ppromolite->m_data.m_critSQL);

			}

		} //		if(
//			  (ppromolite->m_data.m_nIndexAutomationEndpoint>=0)
//			&&(ppromolite->m_settings.m_nNumEndpointsInstalled>0)
///			&&(ppromolite->m_settings.m_ppEndpointObject)
//			&&(ppromolite->m_settings.m_ppEndpointObject[ppromolite->m_data.m_nIndexAutomationEndpoint])
//			&&(!ppromolite->m_data.m_bProcessSuspended)
//			&&(ppromolite->m_data.m_key.m_bValid)  // must have a valid license
//			&&(
//					(!ppromolite->m_data.m_key.m_bExpires)
//				||((ppromolite->m_data.m_key.m_bExpires)&&(!ppromolite->m_data.m_key.m_bExpired))
//				||((ppromolite->m_data.m_key.m_bExpires)&&(ppromolite->m_data.m_key.m_bExpireForgiveness)&&(ppromolite->m_data.m_key.m_ulExpiryDate+ppromolite->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
//				)
//			&&(
//					(!ppromolite->m_data.m_key.m_bMachineSpecific)
//				||((ppromolite->m_data.m_key.m_bMachineSpecific)&&(ppromolite->m_data.m_key.m_bValidMAC))
//				)
//			)
	_ftime(&pChannelObj->m_timebTriggerTick); // the last time check inside the thread

/*
		if(delta>0.0)
		{
			wait = (double)clock();
			while((!ppromolite->m_data.m_bTriggerEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)&&(((double)clock())-wait<delta))  // wait here until there is a change
			{
				Sleep(1); // dont peg processor
			}
		}
*/
	} // while((!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))

	pChannelObj->m_nEvents = 0;

	if(pChannelObj->m_ppEvents)
	{
		while( pChannelObj->m_nEvents < 128)
		{
			if(	pChannelObj->m_ppEvents[pChannelObj->m_nEvents] ) delete pChannelObj->m_ppEvents[pChannelObj->m_nEvents];
			pChannelObj->m_nEvents++;
		}
		delete [] pChannelObj->m_ppEvents;
		pChannelObj->m_ppEvents = NULL;
		pChannelObj->m_nEvents = 0;
	}

	if(pChannelObj->m_ppLyricAlias)
	{
		while( pChannelObj->m_nNumAliasArray > 0 )
		{
			if(	pChannelObj->m_ppLyricAlias[pChannelObj->m_nNumAliasArray-1] ) delete pChannelObj->m_ppLyricAlias[pChannelObj->m_nNumAliasArray-1];
			pChannelObj->m_nNumAliasArray--;
		}
		delete [] pChannelObj->m_ppLyricAlias;

		pChannelObj->m_ppLyricAlias = NULL;
	}


#ifndef NUCLEUS_PROMOLITE_CHANGE
	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_SENTINEL)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending trigger thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	}
	else
	if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_HELIOS)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending trigger thread for Omnibus stream %d", pChannelObj->m_nChannelID);
	}
#else //#ifdef NUCLEUS_PROMOLITE_CHANGE
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending Promolite trigger thread");
#endif // #ifdef NUCLEUS_PROMOLITE_CHANGE
	g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLiteTriggerThread", errorstring);    //(Dispatch message)

	db.RemoveConnection(pdbConn);
	dbSet.RemoveConnection(pdbConnSet);
	pChannelObj->m_bTriggerThreadStarted=false;

	Sleep(90);


	_endthread();
	Sleep(100);

}

// NUCLEUS->PROMOLITE_CHANGE
// added a done event purge thread... not used for Nucleus at all.
void PromoLiteDonePurgeThread(void* pvArgs)
{
	CPromoLiteAutomationChannelObject* pChannelObj = (CPromoLiteAutomationChannelObject*) pvArgs;
	if(pChannelObj==NULL)
	{
	g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLiteDonePurgeThread", "NULL Channel Object");    //(Dispatch message)

		return;  // this would be bad
	}

	CPromoLiteMain* ppromolite = (CPromoLiteMain*) pChannelObj->m_pPromoLite;
	if(ppromolite==NULL)
	{
	g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLiteDonePurgeThread", "NULL Main Object");    //(Dispatch message)

		return;  // this would be bad
	}


	pChannelObj->m_bAutomationThreadStarted=true;  // not really

	g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLiteDonePurgeThread", "started: %d",pChannelObj->m_bAutomationThreadStarted);    //(Dispatch message)


	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning Promolite done purge thread");

	g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLiteDonePurgeThread", errorstring);    //(Dispatch message)

	strcpy(errorstring, "");
	CDBUtil db;
	CDBUtil dbSet;

	CDBconn* pdbConn = db.CreateNewConnection(ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:done_purge_database_connect", errorstring);  //(Dispatch message)
			pdbConn = ppromolite->m_data.m_pdbConn;
		}
		
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", ppromolite->m_settings.m_pszDSN, ppromolite->m_settings.m_pszUser, ppromolite->m_settings.m_pszPW); 
		ppromolite->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "PromoLite:done_purge_database_connect", errorstring);  //(Dispatch message)
		pdbConn = ppromolite->m_data.m_pdbConn;

		//**MSG
	}
	if(pdbConn)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
			"SELECT Schedule.event_instance_id from %s.dbo.%s as Schedule left join \
(select event_instance_id, sum(trigger_status) as status, count(1) as actioncnt from %s.dbo.%s group by event_instance_id) \
as Schedule_Actions on Schedule.event_instance_id = Schedule_Actions.event_instance_id where status >= actioncnt order by start_time desc",

				(((g_ppromolite->m_settings.m_pszDefaultDB)&&(strlen(g_ppromolite->m_settings.m_pszDefaultDB)))?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
				(((g_ppromolite->m_settings.m_pszEvents)&&(strlen(g_ppromolite->m_settings.m_pszEvents)))?g_ppromolite->m_settings.m_pszEvents:"Schedule"),
				(((g_ppromolite->m_settings.m_pszDefaultDB)&&(strlen(g_ppromolite->m_settings.m_pszDefaultDB)))?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
				(((g_ppromolite->m_settings.m_pszAnalyzedTriggerData)&&(strlen(g_ppromolite->m_settings.m_pszAnalyzedTriggerData)))?g_ppromolite->m_settings.m_pszAnalyzedTriggerData:"Schedule_Actions")
			);

		//EnterCriticalSection(&ppromolite->m_data.m_critSQL); // dont need this, our connection is separate
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_DONE) 
{
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "done retrieve SQL: %s", szSQL); 
}

		CPromoLiteList idlist;
		CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
		if(prs != NULL)
		{
			while((!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
			{
				CString szTemp;

				try
				{
					prs->GetFieldValue("event_instance_id", szTemp);
					if(szTemp.GetLength())
					{
						idlist.Add( atoi(szTemp) );
					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
            ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:debug", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
            ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:debug", "Retrieve: Caught exception: out of memory");
					}
					else 
					{
						ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:debug", "Retrieve: Caught other exception.\n%s", szSQL);
					}
					e->Delete();
				} 
				catch( ... )
				{
					ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:debug", "Retrieve: Caught unknown exception.\n%s", szSQL);
				}

				pChannelObj->m_nEvents++;

				prs->MoveNext();
			}
			if(prs)
			{
	//			prs->MoveNext();
				prs->Close();
				delete prs;
			}

		} //if(prs != NULL)

		//now, have a list of IDs  (maybe a list of zero items though)

		if((idlist.m_nNumItems>0)&&(idlist.m_pnItems))
		{
			// else just let it go...

			if (ppromolite->m_settings.m_nRecycleSeconds > 0)
			{ 
//				int i=ppromolite->m_settings.m_nDoneCount;
//				while(i<idlist.m_nNumItems)
				int i=idlist.m_nNumItems-1;
				while((i>=0)&&(i>(idlist.m_nNumItems-ppromolite->m_settings.m_nDoneCount+1)))
				{
					double dblNow;
					_timeb now;
					_ftime(&now);
					dblNow = (((double)(now.time - (now.timezone*60)+ (now.dstflag?3600:0)))+((double)(now.millitm + (g_ppromolite?g_ppromolite->m_settings.m_nTriggerAdvanceMS:0)))/1000.0);

// m_nTriggerExclusionDelayMS  // we get stuff to trigger a bit back in time, but trigger them ahead m_nTriggerAdvanceMS

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"UPDATE %s.dbo.%s set start_time = case when start_time + %d < %.3lf then %.3lf + %d else start_time + %d end where event_instance_id = %d",

							(((g_ppromolite->m_settings.m_pszDefaultDB)&&(strlen(g_ppromolite->m_settings.m_pszDefaultDB)))?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
							(((g_ppromolite->m_settings.m_pszEvents)&&(strlen(g_ppromolite->m_settings.m_pszEvents)))?g_ppromolite->m_settings.m_pszEvents:"Schedule"),
							ppromolite->m_settings.m_nRecycleSeconds, dblNow, dblNow,
							ppromolite->m_settings.m_nRecycleSeconds,
							ppromolite->m_settings.m_nRecycleSeconds,
							idlist.m_pnItems[i]
							);

//update " & curMainDB & ".dbo.Schedule set start_time = case when start_time + " & recycle_events & " < datediff(s, '1/1/1970', getDate()) then datediff(s, '1/1/1970', getDate()) + " & recycle_events & " else start_time + " & recycle_events & " end where event_instance_id = " & filearray(0, i)   
/*

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"UPDATE %s.dbo.%s set start_time = start_time + %d where event_instance_id = %d",

							(((g_ppromolite->m_settings.m_pszDefaultDB)&&(strlen(g_ppromolite->m_settings.m_pszDefaultDB)))?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
							(((g_ppromolite->m_settings.m_pszEvents)&&(strlen(g_ppromolite->m_settings.m_pszEvents)))?g_ppromolite->m_settings.m_pszEvents:"Schedule"),
							ppromolite->m_settings.m_nRecycleSeconds,
							idlist.m_pnItems[i]
							);
*/
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_DONE) 
{
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "recycle 1 SQL: %s", szSQL); 
}

					if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
					//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "1) ERROR executing done purge SQL: %s", errorstring);  //Sleep(50); //(Dispatch message)
					}

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"UPDATE %s.dbo.%s set trigger_status = 0 where event_instance_id = %d",
							(((g_ppromolite->m_settings.m_pszDefaultDB)&&(strlen(g_ppromolite->m_settings.m_pszDefaultDB)))?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
							(((g_ppromolite->m_settings.m_pszAnalyzedTriggerData)&&(strlen(g_ppromolite->m_settings.m_pszAnalyzedTriggerData)))?g_ppromolite->m_settings.m_pszAnalyzedTriggerData:"Schedule_Actions"),
							idlist.m_pnItems[i]
							);

if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_DONE) 
{
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "recycle 2 SQL: %s", szSQL); 
}
					if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
					//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "2) ERROR executing done purge SQL: %s", errorstring);  //Sleep(50); //(Dispatch message)
					}


//					i++;
					i--;
				}
				pChannelObj->m_bTriggerEventsChanged = true; // signal that we have new events!
			}
			else
			{
//				int i=ppromolite->m_settings.m_nDoneCount;
//				while(i<idlist.m_nNumItems)
				int i=idlist.m_nNumItems-1;
				while((i>=0)&&(i>(idlist.m_nNumItems-ppromolite->m_settings.m_nDoneCount+1)))
				{



					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"DELETE %s.dbo.%s where event_instance_id = %d",

							(((g_ppromolite->m_settings.m_pszDefaultDB)&&(strlen(g_ppromolite->m_settings.m_pszDefaultDB)))?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
							(((g_ppromolite->m_settings.m_pszEvents)&&(strlen(g_ppromolite->m_settings.m_pszEvents)))?g_ppromolite->m_settings.m_pszEvents:"Schedule"),
							idlist.m_pnItems[i]
							);

if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_DONE) 
{
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "delete 1 SQL: %s", szSQL); 
}
					

					if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
					//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "1) ERROR executing done purge SQL: %s", errorstring);  //Sleep(50); //(Dispatch message)
					}

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"DELETE %s.dbo.%s where event_instance_id = %d",
							(((g_ppromolite->m_settings.m_pszDefaultDB)&&(strlen(g_ppromolite->m_settings.m_pszDefaultDB)))?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
							(((g_ppromolite->m_settings.m_pszAnalyzedTriggerData)&&(strlen(g_ppromolite->m_settings.m_pszAnalyzedTriggerData)))?g_ppromolite->m_settings.m_pszAnalyzedTriggerData:"Schedule_Actions"),
							idlist.m_pnItems[i]
							);
if(ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_DONE) 
{
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "delete 2 SQL: %s", szSQL); 
}

					if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
					//**MSG
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "2) ERROR executing done purge SQL: %s", errorstring);  //Sleep(50); //(Dispatch message)
					}


//					i++;
					i--;
				}

			}




		}


		try
		{
			db.DisconnectDatabase(pdbConn, errorstring);
			db.RemoveConnection(pdbConn);
		}
		catch(...)
		{
		}
	}


	db.RemoveConnection(pdbConn);
	pChannelObj->m_bAutomationThreadStarted=false;
	g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLiteDonePurgeThread", "ended: %d",pChannelObj->m_bAutomationThreadStarted);    //(Dispatch message)
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending Promolite done purge thread");

  Sleep(50);//(Dispatch message)
	_endthread();

}



void PromoLiteXMLHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;
	
	char szPromoLiteSource[MAX_PATH]; 
	strcpy(szPromoLiteSource, "PromoLiteXMLHandler");

	CBufferUtil bu;

	HRESULT hRes;
	try
	{  

#if _WIN32_WINNT >= 0x0400
		hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		hRes = CoInitialize(NULL);
#endif
	}
	catch(...)
	{
		if(g_ppromolite) g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, szPromoLiteSource, "Exception in CoInitialize");  //(Dispatch message)
	}


	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		char dberrorstring[DB_ERRORSTRING_LEN];
//		char xmlerrorstring[MAX_MESSAGE_LENGTH];
		char errorstring[MAX_MESSAGE_LENGTH];
//		char szSQL[DB_SQLSTRING_MAXLEN];
		strcpy(dberrorstring, "");
		strcpy(errorstring, "");

//		char element[MAX_MESSAGE_LENGTH];

//		AfxMessageBox(szPromoLiteSource);

		CNetUtil net(false); // local object for utility functions.
		CCortexUtil util;  // cortex utility functions.

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		bool bCloseCommand = false;  // make these not persistent unless a persist command comes in.
		bool bPersist = false;  // make these not persistent unless a persist command comes in.
		int nPeriodic = -1;  
		int nTimed = -1;  

		char* pchBuffer = NULL;
		unsigned long ulBufferLen = 0;
		char* pch = NULL;
		char* pchXML = NULL;
		char* pchXMLStream = NULL;
		unsigned long ulAccumulatedBufferLen = 0;
		unsigned long ulRetry = 0;

		unsigned long ulConnLastMessage = 0;
		unsigned long ulConnTimeout = 0;
		
		char filename[MAX_PATH];
		char lastrxfilename[MAX_PATH];
		char lasttxfilename[MAX_PATH];
		int nRxDupes=0;
		int nTxDupes=0;

		FILE* fp = NULL;

		_timeb timestamp;
		_timeb timeactive;
		_timeb timeperiodic;

		_ftime(&timeactive);
		_ftime(&timeperiodic);


	// 	(*(pClient->m_pulConnections))++;//already exists

		IXMLDOMDocumentPtr pDoc(__uuidof(DOMDocument));  // let's have only one object per connection
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread D"); Sleep(100); //(Dispatch message)
		HRESULT hr;//= pDoc.CreateInstance(__uuidof(DOMDocument));
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread E");  //(Dispatch message)
		hr = pDoc->put_async(VARIANT_FALSE);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread F");  //(Dispatch message)
										
		// Remove document safety options
		IObjectSafety* pSafety=NULL;
		DWORD dwSupported, dwEnabled;
			

		if ( SUCCEEDED(pDoc->QueryInterface(IID_IObjectSafety, (void**)&pSafety)))
		{
			pSafety->GetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, &dwSupported, &dwEnabled );
			pSafety->SetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, dwSupported, 0 );
		}

		// initialize random seed
		srand ( time(NULL) );

		CCortexMessage msg;

		// allocate all the static ones
//		char* m_pchResponse[CX_XML_BUFFER_COUNT] = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};

//		AfxMessageBox("formatting");

		msg.FormatContent(CX_XML_BUFFER_CORTEXBEGIN, MAX_MESSAGE_LENGTH, "<cortex src=\"%s\" version=\"%s\">", 
			(g_ppromolite->m_settings.m_pszName?g_ppromolite->m_settings.m_pszName:"PromoLite"), 
			CX_XML_VERSION
			);


		msg.SetContent(CX_XML_BUFFER_CORTEXEND, "</cortex>");

		msg.SetContent(CX_XML_BUFFER_FLAGSEND, "</flags>");

		msg.SetContent(CX_XML_BUFFER_DATAEND, "</data>");

		//  ack and nak are the same length so we can allocate it, just replace contents later
		msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

//		AfxMessageBox("set");



		if (FAILED(hRes))
		{
			sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established, but CoInitialize failed.  Unable to process XML.", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,
				pClient->m_socket);

			if(g_ppromolite)
			{
				g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, szPromoLiteSource, errorstring);  //(Dispatch message)
				g_ppromolite->SendMsg(CX_SENDMSG_ERROR, szPromoLiteSource, errorstring);
			}

			goto cleanup;
		}

		
		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);
		if(g_ppromolite)
		{
			g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, szPromoLiteSource, errorstring);  //(Dispatch message)
			g_ppromolite->SendMsg(CX_SENDMSG_INFO, szPromoLiteSource, errorstring);
		}



		while ( 
									(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
//								&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
								&&(pClient->m_socket!=NULL)
								&&(!bCloseCommand)
					)
		{


			pchBuffer = NULL;
			ulBufferLen = 0;

			
			int nReturnCode = net.GetLine((unsigned char**)&pchBuffer, &ulBufferLen, pClient->m_socket, NET_RCV_ONCE, errorstring);
			if(nReturnCode == NET_SUCCESS)
			{
				ulRetry = 0;
				//process any received XML.
				// have to keep accumulating until we find a </cortex> tag.
				if((pchBuffer)&&(ulBufferLen)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
				{
					int nLen = 0;
					if(pchXMLStream) nLen = ulAccumulatedBufferLen;
					pch = (char*) malloc(nLen+ulBufferLen+1);  //term 0
					if(pch)
					{
						char* pchEnd = NULL;
						char* pchNext = NULL;
						if(pchXMLStream)  // we have an old buffer.
						{
							memcpy(pch, pchXMLStream, ulAccumulatedBufferLen);
							memcpy(pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen);
							free(pchXMLStream); 
							pchXMLStream = pch;  // reassign!
							ulAccumulatedBufferLen += ulBufferLen;
							*(pch+ulAccumulatedBufferLen) = 0;  // null term
						}
						else
						{
							// this is new.
							// first we have to skip all chars that are not a '<'
							// pchEnd = strchr(pchBuffer, '<');  // have to find first tag, we are only interested in XML.
							// used to use strchr but if there are leading zeros in the buffer, we never get past them

							pchEnd = pchBuffer;
							while((*pchEnd!='<')&&(pchEnd<pchBuffer+ulBufferLen)) pchEnd++;

							if(pchEnd<pchBuffer+ulBufferLen)
							{
								strcpy(pch, pchEnd);
								ulAccumulatedBufferLen += (ulBufferLen-(pchEnd-pchBuffer));
								pchXMLStream = pch;  // reassign!
								*(pch+ulAccumulatedBufferLen) = 0;  // null term
							}
							else  // not found!
							{
								free(pch);
								pchXMLStream = NULL;
							}
						}

						if(pchBuffer) free(pchBuffer);
						pchBuffer = NULL;
						if(pchXMLStream)
						{

							pchEnd = strstr(pchXMLStream, "</cortex>");
							while((pchEnd)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
							{
							// found a token.
								pchEnd+=strlen("</cortex>");

								pchNext = pchEnd;
								while((*pchNext!='<')&&(pchNext<pchEnd+strlen(pchEnd))) pchNext++;

								if(pchNext<pchEnd+strlen(pchEnd))
								{
									// we found a remainder.
									nLen = strlen(pchNext);
									pch = (char*) malloc(nLen+1);  //term 0
									if(pch)
									{
										memcpy(pch, pchNext, nLen);
										*(pch+nLen) = 0;
									}
								}
								else pch = NULL;


								pchXML = pchXMLStream; // just use it.
								*pchEnd = 0; //null terminate it

								pchXMLStream = pch;  // take the rest of the stream.
								if(pchXMLStream) ulAccumulatedBufferLen = strlen(pchXMLStream);
								else ulAccumulatedBufferLen=0;

								// now have to deal with XML found in pchXML.
								//debug file write
								if((g_ppromolite)&&(g_ppromolite->m_settings.m_bUseXMLClientLog))
								{
//				AfxMessageBox("logging");
if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_COMM) 
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, szPromoLiteSource, "begin log receive");  //(Dispatch message)

									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_ppromolite->m_settings.m_pszName?g_ppromolite->m_settings.m_pszName:"PromoLite"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lastrxfilename, filename)==0)
									{
										nRxDupes++;
									}
									else
									{
										nRxDupes=0;
										strcpy(lastrxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_RX.xml",nRxDupes);

//	AfxMessageBox(filename);

									fp = fopen(filename, "wb");
									if(fp)
									{
//											fwrite(filename, 1, strlen(filename),fp);
										fwrite(pchXML, 1, strlen(pchXML),fp);
										fflush(fp);
										fclose(fp);
										fp = NULL;
									}
if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_COMM) 
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, szPromoLiteSource, "end log receive");  //(Dispatch message)

								}

								// do stuff here.

								_ftime(&timeactive);  // reset the inactivity timer.


								// first attempt to extract the rxid.
								char* pchRXID = NULL;
								int nIDlen = 0;

								char* pchFind = strstr(pchXML, "<rx rxid=\"");

								if(pchFind)
								{
									pchFind+=strlen("<rx rxid=\"");
									char* pchTerminate = pchFind;
									while(pchTerminate<pchEnd)
									{
										if(
											  (*pchTerminate == '\"')
											||(*pchTerminate == '>')
											||((pchTerminate<pchEnd)&&(*pchTerminate == '/')&&(*(pchTerminate+1) == '>'))
											)
										{
											break;
										}
										else
										{
											pchTerminate+=1;
										}
									}

									nIDlen = pchTerminate-pchFind;
									if(nIDlen>0)
									{
										pchRXID = (char*)malloc(nIDlen+1);
									}
									if(pchRXID)
									{
										memset(pchRXID, 0, nIDlen+1);
										memcpy(pchRXID, pchFind, nIDlen);

										pchTerminate = bu.XMLEncode(pchRXID); // just in case
										if(pchTerminate)
										{
											try{free(pchRXID);} catch(...){}
											pchRXID = pchTerminate;
										}
									}
								}


							//create a response UUID
								UUID uuid;
								UuidCreate(&uuid); // could check response code to be RPC_S_OK ?

					//			CoCreateGuid(&uuid);

								unsigned char* pucUUID = NULL;
								UuidToString(&uuid, &pucUUID);
								// call this later: RpcStringFree(&pucUUID);

								
								bool bResponseSent = false;  // means, sent a response already.  Otherwise have to look at the failure code and deal at the end

								msg.m_nTxStep = CX_XML_BUFFER_CORTEXBEGIN;

								//OK, let's assemble the tx element.

								if(pchRXID)
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUID);
								}


								// then, parse the XML
								msg.m_nFailure=CX_XML_ERR_SUCCESS;
								pDoc->put_async(VARIANT_FALSE);
			//				AfxMessageBox((char*)pchXML);
								hr = pDoc->loadXML((char*)pchXML);
								if(hr!=VARIANT_TRUE)
								{
								//					AfxMessageBox("bad load");
									bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML parse error: [%s]", pchXML );
									g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, szPromoLiteSource, errorstring);
									//let's try to send a nak.

									msg.m_nFailure=CX_XML_ERR_XMLPARSE;
									msg.m_nType = CX_XML_TYPE_UNK;
									msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
									msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML parse error %d</msg>", msg.m_nFailure);
									msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");
								}
								else
								{
	//								AfxMessageBox("yirgacheff");

if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_COMM) 
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, szPromoLiteSource, "begin xml parse");  //(Dispatch message)
									IXMLDOMNodeListPtr pChildNodes = pDoc->GetchildNodes();

									IXMLDOMNodePtr pNodes[CX_XML_NODECOUNT];
									int n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }

									msg.m_nType = CX_XML_TYPE_UNK;
									msg.SetType("unknown");

									bool bCommandHandled = false;
									if (pChildNodes != NULL)
									{
										IXMLDOMNodePtr pChild;
										while(pChild = pChildNodes->nextNode())
										{
											DOMNodeType nodeType;
											pChild->get_nodeType(&nodeType);

											if(nodeType == NODE_ELEMENT)
											{
												char tag[MAX_PATH]; 
												//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
												util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

											//	AfxMessageBox(tag);

												if(strcmp("cortex", tag)==0)
												{
													pNodes[CX_XML_NODE_CORTEX] = pChild;
													// now get subs to cortex

													pChildNodes = pChild->GetchildNodes();
													if (pChildNodes != NULL)
													{
														while(pChild = pChildNodes->nextNode())
														{
															pChild->get_nodeType(&nodeType);

															if(nodeType == NODE_ELEMENT)
															{
																//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
																util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

														//		AfxMessageBox(tag);

																if(strcmp("rx", tag)==0)
																{
																	pNodes[CX_XML_NODE_RX] = pChild;
																	// now get the "real" rxid;

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
																	//	AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
																			//strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));
								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("rxid", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
																				//strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));

																		//		AfxMessageBox("replace rxid.");

																				if(pchRXID)
																				{
																					try{free(pchRXID);} catch(...){}
																					pchRXID = bu.XMLEncode(tag); // just in case
																					if(pchRXID)
																					{
// reassemble tx element
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
																					}
																				}

																			}
																			else
																			if(strcmp("part", tag)==0)
																			{
																				//  multi part message
																			}
																		}
																	}

																}
																else
																if(strcmp("cmd", tag)==0)
																{
																	pNodes[CX_XML_NODE_CMD] = pChild;
																	pNodes[CX_XML_NODE_OPTIONS] = NULL;
																	pNodes[CX_XML_NODE_DATA] = NULL;

																	// now get attribs and then get subs, data and options

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
								//										AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
																			//strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));
								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("type", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
																				//strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));
																				msg.SetType(tag);

//																																		AfxMessageBox(tag);

																				if(strcmp("ack", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_ACK;
																				}
																				else
																				if(strcmp("nak", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_NAK;
																				}
																				else
																				if(strcmp("persist", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PERSIST;
																					bPersist = true;
																				}
																				else
																				if(strcmp("quit", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_QUIT;
																					bPersist = false;
																				}
																				else
																				if(strcmp("status", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_STATUS;
																				}
																				else
																				if(strcmp("bye", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_BYE;
																				}
																				else
																				if(strcmp("get_version", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_GETVERSION;
																				}
																				else
																				if(strcmp("ping", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PING;
																				}
/*
																				else

																					

/////////////////////////////////////////////////////////////////////////////////////////////////
////                                    begin PromoLite specific commands
//#define SENTINEL_XML_TYPE_GETCONN				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
//#define SENTINEL_XML_TYPE_GETLIST				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
//#define SENTINEL_XML_TYPE_GETEVENT			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
																				if(strcmp("get_conn", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETCONN;
																				}
																				else
																				if(strcmp("get_list", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETLIST;
																				}
																				else
																				if(strcmp("get_event", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETEVENT;
																				}
																					
////                                    end PromoLite specific commands
/////////////////////////////////////////////////////////////////////////////////////////////////
*/

																				
																			}
																			else
																			if(strcmp("user", tag)==0)
																			{
																			}
																			else
																			if(strcmp("password", tag)==0)
																			{
																			}
																		}
																	}


																	IXMLDOMNodeListPtr pCmdChildNodes = pChild->GetchildNodes();
																	if (pCmdChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pCmdChild;
																		while(pCmdChild = pCmdChildNodes->nextNode())
																		{
																			pCmdChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				//strncpy(tag, W2T(pCmdChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pCmdChild->GetnodeName()));
																				if(strcmp("options", tag)==0)
																				{
																					pNodes[CX_XML_NODE_OPTIONS] = pCmdChild;
																				}
																				else
																				if(strcmp("data", tag)==0)
																				{
																					pNodes[CX_XML_NODE_DATA] = pCmdChild;
																				}
																			}
																		}
																	}
																}
															}
														}
														// by here, all the necessary stuff should be filled out.

//														AfxMessageBox("here");
												

														switch(msg.m_nType)
														{
														case CX_XML_TYPE_ACK://						1	// 4.1 ack Acknowledges a message with success
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the ack

																
															} break; //case CX_XML_TYPE_ACK
														case CX_XML_TYPE_NAK://						2	// 4.2 nak Acknowledges a message with failure, or cancels
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the nak

															} break; //case CX_XML_TYPE_NAK
														case CX_XML_TYPE_PERSIST://				3	// 4.3 persist Requests a persistent connection
															{
																//persistence already done, above
																// set timeout and hearbeat intervals
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"persist\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																if(pNodes[CX_XML_NODE_OPTIONS])
																{

																	// have to return flags
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																	IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																	if (pOptChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pOptChild;
																		while(pOptChild = pOptChildNodes->nextNode())
																		{
																			pOptChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				//strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pOptChild->GetnodeName()));
																				if(strcmp("timeout", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						nTimed = atoi(pchTmp);

																						_snprintf(tag, MAX_PATH, "<timeout>%d</timeout>", nTimed);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;
																					}
																				}
																				else
																				if(strcmp("heartbeat", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						strncpy(tag, pchTmp, MAX_PATH);

																						if(strcmp("yes", tag)==0)
																						{
																							nPeriodic = 30;
																						}
																						else
																						if(strcmp("no", tag)==0)
																						{
																							nPeriodic = -1;
																						}
																						else
																						{
																							nPeriodic = atoi(tag);
																						}

																						_snprintf(tag, MAX_PATH, "<heartbeat>%d</heartbeat>", nPeriodic);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );


																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;

																					}
																				}
																			}
																		}
																	}
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																}
																
															} break; //case CX_XML_TYPE_PERSIST
														case CX_XML_TYPE_QUIT://					4	// 4.4 quit Disconnects a persistent connection
															{
																//already done, above
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"quit\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																
															} break; //case CX_XML_TYPE_QUIT
														case CX_XML_TYPE_STATUS://				5	// 4.5 status Obtains status information regarding the module
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"status\" code=\"0\">");

																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																char* pchStatus = g_ppromolite->m_data.GetStatusText(&ulDataLen);
																if(pchStatus)
																{
																	msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<status><code>0x%08x</code><text>%s</text></status>",
																		ulDataLen,
																		pchStatus	
																		);
																}
																else
																{
																	msg.SetContent(CX_XML_BUFFER_DATA, 
																		"<status><code>0xffffffff</code><text>unavailable</text></status>"
																		);

																}

																if(pchStatus){try { free(pchStatus); } catch(...){}  pchStatus=NULL;}

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


															} break; //case CX_XML_TYPE_STATUS
														case CX_XML_TYPE_BYE://						6	// 4.6 bye Causes the module to shut down
															{

g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLite", "The Global Kill XML command has been received.");  //(Dispatch message)

																bSetGlobalKill = true;
																g_ppromolite->m_data.m_bQuietKill = true;
																bCloseCommand = true;

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"bye\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_GETVERSION://		7	// 4.7 get_version gets the version of the module as well as the build date
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_version\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<version>%s</version><build>%s %s</build>",
																		CX_CURRENT_VERSION,
																		 __DATE__,
																		 __TIME__	
																		);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_PING://					8	// 4.8 ping just sends an ack to indicate it is alive
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"ping\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE


////////////////////////////////////////////////////////
// begin PromoLite specific XML commands
/*
														case SENTINEL_XML_TYPE_GETCONN://				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_conn\" code=\"%d\">", g_ppromolite->m_data.m_nNumConnectionObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_ppromolite->m_data.m_critConns);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_ppromolite->m_data.m_ppConnObj)&&(g_ppromolite->m_data.m_nNumConnectionObjects))
																{

																	//let's see if we have any options.

																	char* pchServer = NULL;  //server name search
																	int nActive =-1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{

																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("active", tag)==0)
																					{
																				//		AfxMessageBox("active");
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}
																					//		AfxMessageBox("active 2");

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																				//			AfxMessageBox(tag);

																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_ppromolite->m_data.m_nNumConnectionObjects)
																	{

																		if(g_ppromolite->m_data.m_ppConnObj[m])
																		{
																			bool bInclude = true;
																			CPromoLiteConnectionObject* pObj = g_ppromolite->m_data.m_ppConnObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																	//		AfxMessageBox("here");
																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																	//			AfxMessageBox("here 2");
																			if(bInclude)
																			{
																				if(pObj->m_pAPIConn)
																				{
																					if((pObj->m_pAPIConn->m_SysData.systemfrx==0x29)
																						&&(pObj->m_pAPIConn->m_SysData.systemdf))
																					{
																						strcpy(tag, "29.97");
																					}
																					else
																					{
																						sprintf(tag, "%02x",pObj->m_pAPIConn->m_SysData.systemfrx);
																					}
																				}

																				_ftime(&timestamp);
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<aconn>\
<config active=\"%d\">\
<server>%s</server>\
<client>%s</client>\
<port>N/A</port>\
<basis>%s</basis>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<server_time>%.03f</server_time>\
<server_status>%d</server_status>\
<server_lists>%d</server_lists>\
<server_changed>%d</server_changed>\
<server_last_update>%.03f</server_last_update>\
<message>%s</message>\
</status>\
</aconn>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pObj->m_pszServerName,
																					pObj->m_pszClientName,
																					(pObj->m_pAPIConn?tag:"N/A"),
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(pObj->m_dblLastServerTimeMS/1000.0),
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? -1 : (pObj->m_pAPIConn?pObj->m_pAPIConn->m_Status:-1)),
																					(pObj->m_pAPIConn?pObj->m_pAPIConn->m_SysData.systemlistcount:-1),
																					(pObj->m_pAPIConn?pObj->m_pAPIConn->m_SysData.syschanged:-1),
																					pObj->m_dblUpdateTime,
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? (((pObj->m_ulStatus&SENTINEL_ICON_MASK) == SENTINEL_STATUS_ERROR)?"connection error":"not connected"):"")

																																										
																				);
																					
																			

																				msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}

																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;

																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_ppromolite->m_data.m_critConns);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);


															} break; //case SENTINEL_XML_TYPE_GETCONN

														case SENTINEL_XML_TYPE_GETLIST://				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_list\" code=\"%d\">", g_ppromolite->m_data.m_nNumChannelObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																
EnterCriticalSection(&g_ppromolite->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_ppromolite->m_data.m_ppChannelObj)&&(g_ppromolite->m_data.m_nNumChannelObjects))
																{

																	//let's see if we have any options.

																	char* pchServer = NULL;  //server name search
																	int nActive =-1;
																	int nID=-1;
																	int nListNum = -1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{

																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("id", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nID = atoi(pchTmp);
																								if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("list_number", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nListNum = atoi(pchTmp);
																								if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("active", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_ppromolite->m_data.m_nNumChannelObjects)
																	{

																		if(g_ppromolite->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CPromoLiteChannelObject* pObj = g_ppromolite->m_data.m_ppChannelObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																			if((nID>0)&&(pObj->m_nChannelID != nID))  bInclude = false;
																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				_ftime(&timestamp);

																				tlistdata* ptlist = NULL;
																				if(pObj->m_pAPIConn)
																				{
																					ptlist = &(pObj->m_pAPIConn->m_ListData[pObj->m_nHarrisListID-1]);
																				}
																				
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<alist>\
<config active=\"%d\" id=\"%d\">\
<server>%s</server>\
<list_number>%d</list_number>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<list_state>%d</list_state>\
<list_changed>%d</list_changed>\
<list_display>%d</list_display>\
<list_syschange>%d</list_syschange>\
<list_count>%d</list_count>\
<list_lookahead>%d</list_lookahead>\
<list_last_update>%.3f</list_last_update>\
</status>\
</alist>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pObj->m_nChannelID,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(ptlist?ptlist->liststate:-1),
																					(ptlist?ptlist->listchanged:-1),
																					(ptlist?ptlist->listdisplay:-1),
																					(ptlist?ptlist->listsyschange:-1),
																					(ptlist?ptlist->listcount:-1),
																					(ptlist?ptlist->lookahead:-1),
																					pObj->m_dblUpdateTime
																																										
																				);
																					
																			

																				msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_LIST_MAXPAYLOAD, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}

																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;

																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_ppromolite->m_data.m_critChannels);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case SENTINEL_XML_TYPE_GETLIST
														case SENTINEL_XML_TYPE_GETEVENT://			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
															{

																msg.ClearContent(CX_XML_BUFFER_MSG);
																int nGlobalCount =0;
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_ppromolite->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_ppromolite->m_data.m_ppChannelObj)&&(g_ppromolite->m_data.m_nNumChannelObjects))
																{

																	//let's see if we have any options.

//																	char* pchServer = NULL;  //server name search
//																	int nActive =-1;
																	int nID=-1;
																	int nLimitNum = -1;
																	int nDone = -1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{
																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("id", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nID = atoi(pchTmp);
																								if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("limit", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nLimitNum = atoi(pchTmp);
																								if(nLimitNum <=0) nLimitNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<limit>%d</limit>", nLimitNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("done", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nDone = 0;
																								else if(strcmp("1", pchTmp)==0) nDone = 1;
																								else if(strcmp("yes", pchTmp)==0) nDone = 1;
																								else if(strcmp("no", pchTmp)==0) nDone = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<done>%d</done>", nDone);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}

																					/*
																					else
																					if(strcmp("list_number", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nListNum = atoi(pchTmp);
																								if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("active", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																					* /
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_ppromolite->m_data.m_nNumChannelObjects)
																	{

																		if(g_ppromolite->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CPromoLiteChannelObject* pObj = g_ppromolite->m_data.m_ppChannelObj[m];

/*
																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

* /																		if(
																				//	(nID>0)&&  //commenting this out makes it mandatory!
																					(pObj->m_nChannelID != nID)
																				)  
																				bInclude = false;

//																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				// found a channel that works, now must loop thru the events, filtering if nec.

																				// need to allocate a big buffer and deal with directly, rather than allocating new for every event.
																			
/* // was this, which worked but slowly.  below is new
																				if(pObj->m_ppevents)
																				{
																					 
																					int q = 0;
																					int nCount = 0;
																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;
																						unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));

																						if(pObj->m_ppevents[q])
																						{
																							CPromoLiteEventObject* pEObj = pObj->m_ppevents[q];

																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;
																							if(bInclude)
																							{

																								_ftime(&timestamp);

																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
																								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																									"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_usControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CPromoLiteEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																								);
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																								msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );

																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
* /




																				char* pchPayload = (char*)malloc(SENTINEL_XML_EVENT_MAXPAYLOAD);
																				char* pchAppend = pchPayload;

																				// re-use errorstring, why not.
																		//		int nTimes[10] = {0,0,0,0,0,0,0,0,0,0};
																				int nClock = clock();
if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_COMM) 
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, szPromoLiteSource, "start event payload assembly");  //(Dispatch message)

																				if((pObj->m_ppevents)&&(pchPayload))
																				{
																					unsigned long ulBufferLen = SENTINEL_XML_EVENT_MAXPAYLOAD;
																					char* pchBufferEnd = (pchPayload+SENTINEL_XML_EVENT_MAXPAYLOAD);
																					int nEventLen = 0;
																					unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));
																					int q = 0;
																					int nCount = 0;
																					int nBuffers = 1;
																					int nTempOffset;
																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;

																						if(pObj->m_ppevents[q])
																						{
																					//		nTimes[0] += (clock() - nClock); nClock = clock(); 

																							CPromoLiteEventObject* pEObj = pObj->m_ppevents[q];
																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;

																				//			nTimes[1] += (clock() - nClock); nClock = clock(); 
																							if(bInclude)
																							{

																							//	_ftime(&timestamp);
																					//		nTimes[2] += (clock() - nClock); nClock = clock(); 

																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
																					//		nTimes[3] += (clock() - nClock); nClock = clock(); 
																								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, 
																									"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_usControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CPromoLiteEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																								);
																					//		nTimes[4] += (clock() - nClock); nClock = clock(); 
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																						//	nTimes[5] += (clock() - nClock); nClock = clock(); 
																								nEventLen = strlen(errorstring)+1;//+1 for term zero

																								if(pchAppend+nEventLen>pchBufferEnd)  // if must be changed to while if SENTINEL_XML_EVENT_MAXPAYLOAD defined < MAX_MESSAGE_LENGTH (buffer vs errorstring)
																								{
																									// need to realloc
																									nBuffers++;
																									ulBufferLen = SENTINEL_XML_EVENT_MAXPAYLOAD*nBuffers;
																									char* pchBufferNew = (char*)malloc(ulBufferLen);
																									if(pchBufferNew)
																									{
																										nTempOffset = pchAppend+1 - pchPayload;
																										memcpy(pchBufferNew, pchPayload, nTempOffset);
																										try {free(pchPayload);} catch(...){}
																										pchPayload = pchBufferNew;
																										pchBufferEnd = (pchPayload+ulBufferLen);
																										pchAppend = pchPayload+nTempOffset-1;																									

																									}
																								}
																								

																								// append
																								memcpy(pchAppend, errorstring, nEventLen);
																								pchAppend += nEventLen-1; // append over the term 0 next time.


																						//	nTimes[6] += (clock() - nClock); nClock = clock(); 


/*
																								msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
* /
																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
																				msg.ClearContent(CX_XML_BUFFER_DATA);
																				if(pchPayload)
																				{
																					msg.m_pchResponse[CX_XML_BUFFER_DATA] = pchPayload;		
																					ulDataLen = pchAppend - pchPayload;
																				}
if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_COMM) 
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, szPromoLiteSource, "end event payload assembly");  //(Dispatch message)

																			}

																		}
																		m++;
																	}

/*																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;
* /
																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_ppromolite->m_data.m_critChannels);

/* // not needed now, we tally the buffer as we go
																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																* /
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_event\" code=\"%d\">", nGlobalCount);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case SENTINEL_XML_TYPE_GETEVENT

// end PromoLite specific XML commands
////////////////////////////////////////////////////////
*/
														case CX_XML_TYPE_UNK://						0 // unknown
														default:
															{
																sprintf(errorstring, "Unsupported XML command: [%s]", (msg.m_pchType?msg.m_pchType:"") );
																g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, szPromoLiteSource, errorstring);
																//let's try to send a nak.

																msg.m_nType = CX_XML_TYPE_UNK;
																msg.m_nFailure=CX_XML_ERR_BADCMD;
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"%s\" code=\"%d\">", (msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Unsupported XML command [%s] (error %d)</msg>",(msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

															} break; //case CX_XML_TYPE_UNK://						0 // unknown
														}
															
														
														if(!bPersist) bCloseCommand = true;

													}
													else // no child nodes to cortex - can't identify command.
													{
														bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
														sprintf(errorstring, "Cortex XML format error: [%s]", pchXML );
														g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, szPromoLiteSource, errorstring);
														//let's try to send a nak.

														msg.m_nType = CX_XML_TYPE_UNK;
														msg.m_nFailure=CX_XML_ERR_NOCMD;
														msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
														msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Cortex XML format error %d</msg>", msg.m_nFailure);
														msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

													}
												}
											}
										}
									}
									else
									{
										// no child nodes, can't do nuttin.
										bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
										sprintf(errorstring, "XML format error: [%s]", pchXML );
										g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, szPromoLiteSource, errorstring);
										//let's try to send a nak.

										msg.m_nType = CX_XML_TYPE_UNK;
										msg.m_nFailure=CX_XML_ERR_NOCHILD;
										msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
										msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML format error %d</msg>", msg.m_nFailure);
										msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

									}
if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_COMM) 
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, szPromoLiteSource, "end xml parse");  //(Dispatch message)

									n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }
								}

								if(!bResponseSent)
								{
if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_COMM) 
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, szPromoLiteSource, "begin send response");  //(Dispatch message)

								// send it
		_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_ppromolite)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending response message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, szPromoLiteSource, errorstring);  //(Dispatch message)
				g_ppromolite->SendMsg(CX_SENDMSG_ERROR, szPromoLiteSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_COMM) 
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, szPromoLiteSource, "end send response");  //(Dispatch message)

								}

								// log it

								//debug file write
								if((g_ppromolite)&&(g_ppromolite->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
								{
if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_COMM) 
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, szPromoLiteSource, "begin log response");  //(Dispatch message)
									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_ppromolite->m_settings.m_pszName?g_ppromolite->m_settings.m_pszName:"PromoLite"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lasttxfilename, filename)==0)
									{
										nTxDupes++;
									}
									else
									{
										nTxDupes=0;
										strcpy(lasttxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

									fp = fopen(filename, "wb");
									if(fp)
									{
										int b=CX_XML_BUFFER_CORTEXBEGIN;
										while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
										{
//											fwrite(filename, 1, strlen(filename),fp);
											if(msg.m_pchResponse[b])
											{
												if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

												fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
												fflush(fp);
											}
											b++;

										}
										fclose(fp);
										fp = NULL;
									}
if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_COMM) 
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, szPromoLiteSource, "end log response");  //(Dispatch message)
								}

//AfxMessageBox("001");
								int b=CX_XML_BUFFER_TX;
								while(b<CX_XML_BUFFER_DATAEND)
								{
									if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
									{
										try { free(msg.m_pchResponse[b]); } catch(...){}
										msg.m_pchResponse[b] = NULL;
									}
									b++;
								}

//AfxMessageBox("002");
								if(pucUUID)
								{
									try { RpcStringFree(&pucUUID); } catch(...){}
								}
								pucUUID = NULL;

//AfxMessageBox("003");
								if(pchXML) 
								{
									try { free(pchXML); } catch(...){}
								}
								pchXML = NULL;
//AfxMessageBox("004");

								if(pchRXID) 
								{
									try { free(pchRXID); } catch(...){}
								}
								pchRXID = NULL;

//AfxMessageBox("005");
								if (pchXMLStream) 
									pchEnd = strstr(pchXMLStream, "</cortex>");
								else pchEnd = NULL;
//AfxMessageBox("006");
							}  // while </cortex> exists.
						}//if(pchXMLStream)
						// dont do a free(pch);  
					}  // else out of mem, so just skip
					else
					{
						// out of mem.. .log error?
					}

				} //if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))

			} // if getline succeeds.
			else
			{
				// here, we either have timed out because theres no data, or, the connection has been lost.
				// if we lost the conn, no need to re-establish, we are a client handler, let the client reconnect if they want.
				if(nReturnCode == NET_ERROR_CONN)		// connection lost
				{
					ulRetry++;

					if(g_ppromolite)
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH, "Lost connection from %d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno );

						g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, szPromoLiteSource, errorstring);  //(Dispatch message)
						g_ppromolite->SendMsg(CX_SENDMSG_INFO, szPromoLiteSource, errorstring);
					}

					break;  // break out of while loop, closes connection, starts again
				}
				else
				{
//									g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, szPromoLiteSource, "check %d", clock());  //(Dispatch message)

					// we prob just havent gotten a message because no data.  
					// so, check to see if its time for publication service
									// no, do this in the stall loop


				}

			} // end of else from if(nReturnCode == NET_SUCCESS)

			// and lets check if we've timed out on the connection, no matter what the success was
			_ftime( &timestamp );
			if(
				  (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				&&(ulConnTimeout>0)
				&&(ulConnLastMessage>0)
				&&((ulConnLastMessage + ulConnTimeout) < (unsigned long)timestamp.time )
				)
			{
				// we timed out, need to disconnect
				ulRetry++;


				sprintf(errorstring, "Connection from %d.%d.%d.%d timed out.", 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno);

				if(g_ppromolite)
				{
					g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, szPromoLiteSource, errorstring);  //(Dispatch message)
					g_ppromolite->SendMsg(CX_SENDMSG_INFO, szPromoLiteSource, errorstring);
				}

				if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;} //free this memory before breaking out of the while.

				break;  // break out of while loop, closes connection, starts again
			}

			if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;}
			
			if(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) Sleep(1);  // dont peg processor




/*
			// following line fills out the data structure
//			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);
			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "PromoLite:XMLHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "PromoLite:XMLHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "PromoLite:XMLHandlerThread");
						}
					}
				}
			}
*/
			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "PromoLite:XMLHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
//						g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, szPromoLiteSource, "check %d", clock());  //(Dispatch message)


						if(nPeriodic>0)
						{
							_ftime( &timestamp );
							if((timeperiodic.time + nPeriodic)< timestamp.time)
							{
								// send asynchronous message

								//create a response UUID
								UUID uuida;
								UuidCreate(&uuida); // could check response code to be RPC_S_OK ?

								unsigned char* pucUUIDa = NULL;
								UuidToString(&uuida, &pucUUIDa);

								msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUIDa);

								if(pucUUIDa)
								{
									try { RpcStringFree(&pucUUIDa); } catch(...){}
								}
								pucUUIDa = NULL;


								msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"system\" code=\"0\">");

								msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
								msg.ClearContent(CX_XML_BUFFER_FLAGS);

								unsigned long ulDataLen = 0;
								char* pchStatus = g_ppromolite->m_data.GetStatusText(&ulDataLen);
								if(pchStatus)
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0x%08x</code><text>%s</text></status><time>%d.%03d</time></system>",
										ulDataLen,
										pchStatus,
										timestamp.time, timestamp.millitm
										);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0xffffffff</code><text>unavailable</text></status><time>%d.%03d</time></system>",
										timestamp.time, timestamp.millitm
										);

								}

								if(pchStatus){try { free(pchStatus); } catch(...){} pchStatus=NULL;}

								ulDataLen = 0;
								if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
								{
									ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
								}
								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
									ulDataLen,
									net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
									);

								msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>%s status message</msg>", 
									(g_ppromolite->m_settings.m_pszName?g_ppromolite->m_settings.m_pszName:"PromoLite"));

								msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


								// now, send and log.
	////////////////////////////////////////////// code copied from above.  if modified, must be 2 places.  sorry...
									// send it
			_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_ppromolite)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending system message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, szPromoLiteSource, errorstring);  //(Dispatch message)
				g_ppromolite->SendMsg(CX_SENDMSG_ERROR, szPromoLiteSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
								
	//AfxMessageBox("oop");
									// log it

									//debug file write
									if((g_ppromolite)&&(g_ppromolite->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
									{
										strcpy(filename, "Logs");
										_mkdir(filename);  // if exists already np
										strcat(filename, "\\");
										sprintf(errorstring, "%d.%d.%d.%d", 
											pClient->m_si.sin_addr.s_net, 
											pClient->m_si.sin_addr.s_host, 
											pClient->m_si.sin_addr.s_lh, 
											pClient->m_si.sin_addr.s_impno);

										strcat(filename, errorstring);

										_mkdir(filename);  // if exists already np

		//								_ftime( &timestamp );  // use timestamp we just had..

										tm* theTime = localtime( &timestamp.time	);

										sprintf(errorstring, "\\%s_", 
											(g_ppromolite->m_settings.m_pszName?g_ppromolite->m_settings.m_pszName:"PromoLite"));

										strcat(filename, errorstring);

										char filenametemp[MAX_PATH];
										strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

										strcat(filename, filenametemp);

										int nOffset = strlen(filename);
										sprintf(filename+nOffset,"%03d",timestamp.millitm);

										if (strcmp(lasttxfilename, filename)==0)
										{
											nTxDupes++;
										}
										else
										{
											nTxDupes=0;
											strcpy(lasttxfilename, filename);
										}
										nOffset = strlen(filename);
										sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

										fp = fopen(filename, "wb");
										if(fp)
										{
											int b=CX_XML_BUFFER_CORTEXBEGIN;
											while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
											{
	//											fwrite(filename, 1, strlen(filename),fp);
												if(msg.m_pchResponse[b])
												{
													if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

													fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
													fflush(fp);
												}
												b++;

											}
											fclose(fp);
											fp = NULL;
										}
									}


									b=CX_XML_BUFFER_TX;
									while(b<CX_XML_BUFFER_DATAEND)
									{
										if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
										{
											try { free(msg.m_pchResponse[b]); } catch(...){}
											msg.m_pchResponse[b] = NULL;
										}
										b++;
									}
	/////////////////////////////////////////////
							}

						}

						//and check timeout
						if(nTimed>0)
						{
							_ftime( &timestamp );
							if((timeactive.time + nTimed) < timestamp.time)
							{
								//kill it

								bCloseCommand = true;
							}
							
						}


						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} 

cleanup:
//		AfxMessageBox("cleanup");
//AfxMessageBox("010");

//		pDoc->Release();  // destructor calls this so don't do it here. 
		if(pSafety) pSafety->Release();

//AfxMessageBox("011");

// cleanup:  //was here but want to uninit com etc
//AfxMessageBox("012");

		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d closed.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);

//AfxMessageBox("013");
		if(g_ppromolite)
		{
			g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, szPromoLiteSource, errorstring);  //(Dispatch message)
			g_ppromolite->SendMsg(CX_SENDMSG_INFO, szPromoLiteSource, errorstring);
		}

//AfxMessageBox("014");

		if(pch!=NULL) { try{free(pch);} catch(...) {} } // must free the data buffer 

//AfxMessageBox("015");

		shutdown(pClient->m_socket, SD_BOTH);
//AfxMessageBox("016");
		closesocket(pClient->m_socket);
//AfxMessageBox("017");
		(*(pClient->m_pulConnections))--;
//AfxMessageBox("018");

	}
//		AfxMessageBox("end cleanup");
//AfxMessageBox("019");

	try
	{  
		CoUninitialize(); //XML
	}
	catch(...)
	{
		if(g_ppromolite) g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, szPromoLiteSource, "Exception in CoUninitialize");  //(Dispatch message)
	}

//AfxMessageBox("020");

	try
	{
		delete pClient; // was created with new in the thread that spawned this one.
	}	catch(...) {}

//AfxMessageBox("021");
	if (bSetGlobalKill)	//g_bKillThread = true;
		((CPromoLiteHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}




