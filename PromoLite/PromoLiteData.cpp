// PromoLiteData.cpp: implementation of the CPromoLiteData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <process.h>
#include "PromoLite.h"
#include "PromoLiteHandler.h" 
#include "PromoLiteMain.h" 
#include "PromoLiteData.h"
#include "..\Archivist\ArchivistDefines.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CPromoLiteMain* g_ppromolite;
extern CPromoLiteApp theApp;

extern void PromoLiteAutomationThread(void* pvArgs);
//extern void PromoLiteFarAnalysisThread(void* pvArgs);
extern void PromoLiteNearAnalysisThread(void* pvArgs);
extern void PromoLiteTriggerThread(void* pvArgs);


//extern CADC g_adc; 	// the Harris ADC object

/*
//////////////////////////////////////////////////////////////////////
// CPromoLiteConnectionObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPromoLiteConnectionObject::CPromoLiteConnectionObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_pszClientName = NULL;
	m_ulStatus	= PROMOLITE_STATUS_UNINIT;
	m_ulFlags = PROMOLITE_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
//	m_pAPIConn = NULL;
	m_bKillConnThread = true;
	m_bConnThreadStarted = false;

}

CPromoLiteConnectionObject::~CPromoLiteConnectionObject()
{
	m_bKillConnThread = true;
	while(	m_bConnThreadStarted ) Sleep(1);

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
	if(m_pszClientName) free(m_pszClientName); // must use malloc to allocate
}

*/

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPromoLiteEvent::CPromoLiteEvent()
{
	m_dblTriggerTime = -1.0; 
	m_bAnalyzed = false;
	m_bReAnalyzed = false;
	m_nEventID = -1;	
	m_nAnalyzedTriggerID = -1;
	m_nType = -1;
	m_nDestType = -1;
}

CPromoLiteEvent::~CPromoLiteEvent()
{
}



//////////////////////////////////////////////////////////////////////
// CDestinationMediaObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDestinationMediaObject::CDestinationMediaObject()
{
	m_sz_vc256_file_name;
	m_sz_n_transfer_date; // actually timestamp
	m_sz_vc16_partition;
	m_sz_dbl_file_size;
	m_sz_n_PromoLite_local_last_used;
	m_sz_n_PromoLite_local_times_used;
}

CDestinationMediaObject::~CDestinationMediaObject()
{
	if(m_sz_vc256_file_name) free(m_sz_vc256_file_name); // must use malloc to allocate
	if(m_sz_n_transfer_date) free(m_sz_n_transfer_date); // must use malloc to allocate
	if(m_sz_vc16_partition) free(m_sz_vc16_partition); // must use malloc to allocate
	if(m_sz_dbl_file_size) free(m_sz_dbl_file_size); // must use malloc to allocate
	if(m_sz_n_PromoLite_local_last_used) free(m_sz_n_PromoLite_local_last_used); // must use malloc to allocate
	if(m_sz_n_PromoLite_local_times_used) free(m_sz_n_PromoLite_local_times_used); // must use malloc to allocate
}

int CDestinationMediaObject::SetField(char** ppszField, char* pszData)
{
	if(ppszField)
	{
		if(*ppszField)
		{
			free(*ppszField);
			*ppszField=NULL;
		}
		if(pszData==NULL)
		{
			return PROMOLITE_SUCCESS;
		}
		char* pch = (char*) malloc(strlen(pszData)+1);
		if(pch)
		{
			strcpy(pch, pszData);
			*ppszField=pch;
			return PROMOLITE_SUCCESS;
		}
	}
	return PROMOLITE_ERROR;
}



//////////////////////////////////////////////////////////////////////
// CFileMetaDataObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFileMetaDataObject::CFileMetaDataObject()
{
	m_bUnique=true;  // false if more than one filename returned.
	m_sz_vc256_sys_filename = NULL;
	m_sz_vc256_sys_filepath = NULL;
//	m_sz_vc256_sys_linked_file = NULL;
	m_sz_vc256_sys_description = NULL;
	m_sz_vc64_sys_operator = NULL;
	m_sz_n_sys_type = NULL;
	m_sz_n_sys_duration = NULL;
	m_sz_n_sys_valid_from = NULL;
	m_sz_n_sys_expires_after = NULL;
	m_sz_n_sys_ingest_date = NULL;
	m_sz_n_sys_file_flags = NULL;
	m_sz_dbl_sys_file_size = NULL;
	m_sz_dbl_sys_file_timestamp=NULL;
	m_sz_n_sys_created_on = NULL;
	m_sz_vc32_sys_created_by = NULL;
	m_sz_n_sys_last_modified_on = NULL;
	m_sz_vc32_sys_last_modified_by = NULL;
	m_sz_n_promolite_last_used = NULL;
	m_sz_n_promolite_times_used = NULL;
}

CFileMetaDataObject::~CFileMetaDataObject()
{
	if(m_sz_vc256_sys_filename) free(m_sz_vc256_sys_filename); // must use malloc to allocate
	if(m_sz_vc256_sys_filepath) free(m_sz_vc256_sys_filepath); // must use malloc to allocate
//	if(m_sz_vc256_sys_linked_file) free(m_sz_vc256_sys_linked_file); // must use malloc to allocate
	if(m_sz_vc256_sys_description) free(m_sz_vc256_sys_description); // must use malloc to allocate
	if(m_sz_vc64_sys_operator) free(m_sz_vc64_sys_operator); // must use malloc to allocate
	if(m_sz_n_sys_type) free(m_sz_n_sys_type); // must use malloc to allocate
	if(m_sz_n_sys_duration) free(m_sz_n_sys_duration); // must use malloc to allocate
	if(m_sz_n_sys_valid_from) free(m_sz_n_sys_valid_from); // must use malloc to allocate
	if(m_sz_n_sys_expires_after) free(m_sz_n_sys_expires_after); // must use malloc to allocate
	if(m_sz_n_sys_ingest_date) free(m_sz_n_sys_ingest_date); // must use malloc to allocate
	if(m_sz_n_sys_file_flags) free(m_sz_n_sys_file_flags); // must use malloc to allocate
	if(m_sz_dbl_sys_file_size) free(m_sz_dbl_sys_file_size); // must use malloc to allocate
	if(m_sz_dbl_sys_file_timestamp) free(m_sz_dbl_sys_file_timestamp); // must use malloc to allocate
	if(m_sz_n_sys_created_on) free(m_sz_n_sys_created_on); // must use malloc to allocate
	if(m_sz_vc32_sys_created_by) free(m_sz_vc32_sys_created_by); // must use malloc to allocate
	if(m_sz_n_sys_last_modified_on) free(m_sz_n_sys_last_modified_on); // must use malloc to allocate
	if(m_sz_vc32_sys_last_modified_by) free(m_sz_vc32_sys_last_modified_by); // must use malloc to allocate
	if(m_sz_n_promolite_last_used) free(m_sz_n_promolite_last_used); // must use malloc to allocate
	if(m_sz_n_promolite_times_used) free(m_sz_n_promolite_times_used); // must use malloc to allocate
}

int CFileMetaDataObject::SetField(char** ppszField, char* pszData)
{
	if(ppszField)
	{
		if(*ppszField)
		{
			free(*ppszField);
			*ppszField=NULL;
		}
		if(pszData==NULL)
		{
			return PROMOLITE_SUCCESS;
		}
		char* pch = (char*) malloc(strlen(pszData)+1);
		if(pch)
		{
			strcpy(pch, pszData);
			*ppszField=pch;
			return PROMOLITE_SUCCESS;
		}
	}
	return PROMOLITE_ERROR;
}


int CFileMetaDataObject::UpdateMetadata(char* pszInfo) // increments promolite_last_used and promolite_times_used
{
	_ftime( &g_ppromolite->m_data.m_timebTick );  // we're still alive.
	if((g_ppromolite->m_data.m_nIndexMetadataEndpoint>=0)
		&&(g_ppromolite->m_settings.m_ppEndpointObject)
		&&(g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint])
		&&(m_sz_n_promolite_times_used)
		&&(m_sz_vc256_sys_filename)
		&&(m_sz_vc256_sys_filepath)  // used for uniqueness.
		&&(g_ppromolite->m_data.m_pdb)&&(g_ppromolite->m_data.m_pdbConn))
	{

		int nTimesUsed = atol(m_sz_n_promolite_times_used);

		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET promolite_last_used = %d, promolite_times_used = %d \
WHERE sys_filename = '%s' AND sys_filepath = '%s'",
			g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
			g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the LiveEvents table name
				g_ppromolite->m_data.m_timebTick.time,
				nTimesUsed,
				m_sz_vc256_sys_filename,
				m_sz_vc256_sys_filepath
			);

//		g_ppromolite->m_data.ReleaseRecordSet();
		EnterCriticalSection(&g_ppromolite->m_data.m_critSQL);
		if(g_ppromolite->m_data.m_pdb->ExecuteSQL(g_ppromolite->m_data.m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
		{
		//**MSG
//		g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)
			LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);
			return PROMOLITE_ERROR;
		}
		LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);
		return PROMOLITE_SUCCESS;
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return PROMOLITE_ERROR;

}

int CFileMetaDataObject::UpdateDestinationMetadata(int nEndpointIndex, char* pszHost, char* pszInfo)  // increments PromoLite_local_last_used and PromoLite_local_times_used in Endpoint Destinations_Media table
{
	_ftime( &g_ppromolite->m_data.m_timebTick );  // we're still alive.
	if((nEndpointIndex>=0)
		&&(g_ppromolite->m_settings.m_ppEndpointObject)
		&&(g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint])
		&&(pszHost)&&(strlen(pszHost))
		&&(m_sz_vc256_sys_filename)
		&&(g_ppromolite->m_data.m_pdb)&&(g_ppromolite->m_data.m_pdbConn))
	{
		int nTimesUsed = atol(m_sz_n_promolite_times_used);  ///check if there is a query we can autoincrement.

		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET PromoLite_local_last_used = %d, PromoLite_local_times_used = %d \
WHERE sys_filename = '%s' AND host = '%s'",
			g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
			g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the LiveEvents table name
				g_ppromolite->m_data.m_timebTick.time,
				nTimesUsed,
				m_sz_vc256_sys_filename,
				pszHost
			);

//		g_ppromolite->m_data.ReleaseRecordSet();
		EnterCriticalSection(&g_ppromolite->m_data.m_critSQL);
		if(g_ppromolite->m_data.m_pdb->ExecuteSQL(g_ppromolite->m_data.m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
		{
		//**MSG
//		g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)
			LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);
			return PROMOLITE_ERROR;
		}
		LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);
		return PROMOLITE_SUCCESS;
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return PROMOLITE_ERROR;

}


//////////////////////////////////////////////////////////////////////
// CPromoLiteQueueObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPromoLiteQueueObject::CPromoLiteQueueObject()
{
	m_nItemID=-1;
	m_pszFilenameLocal=NULL;
	m_pszFilenameRemote=NULL;
	m_nActionID = -1;
	m_pszHost = NULL;
	m_dblTimestamp=-1.0;   // timestamp
	m_pszUsername = NULL;
	m_nEventItemID=-1;
	m_pszMessage = NULL;
}

CPromoLiteQueueObject::~CPromoLiteQueueObject()
{
	if(m_pszFilenameLocal) free(m_pszFilenameLocal); // must use malloc to allocate
	if(m_pszFilenameRemote) free(m_pszFilenameRemote); // must use malloc to allocate
	if(m_pszHost) free(m_pszHost); // must use malloc to allocate
	if(m_pszUsername) free(m_pszUsername); // must use malloc to allocate
	if(m_pszMessage) free(m_pszMessage); // must use malloc to allocate
}

int CPromoLiteQueueObject::SetField(char** ppszField, char* pszData)
{
	if(ppszField)
	{
		if(*ppszField)
		{
			free(*ppszField);
			*ppszField=NULL;
		}
		if(pszData==NULL)
		{
			return PROMOLITE_SUCCESS;
		}
		char* pch = (char*) malloc(strlen(pszData)+1);
		if(pch)
		{
			strcpy(pch, pszData);
			*ppszField=pch;
			return PROMOLITE_SUCCESS;
		}
	}
	return PROMOLITE_ERROR;
}

//////////////////////////////////////////////////////////////////////
// CPromoLiteList Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPromoLiteList::CPromoLiteList()
{
	m_nNumItems = 0;
	m_pnItems = NULL;
}

CPromoLiteList::~CPromoLiteList()
{
	if((m_nNumItems)&&(m_pnItems))
	{
		try { delete [] m_pnItems; } catch (...) {} 
	}
}

int CPromoLiteList::Add(int nItem)
{
	int* ppn = new int[m_nNumItems+1];
	if(ppn)
	{
		if((m_nNumItems)&&(m_pnItems))
		{
			int i=0;
			while(i<m_nNumItems)
			{
				ppn[i] = m_pnItems[i];
				i++;
			}			 
		}

		ppn[m_nNumItems] = nItem;

		if(m_pnItems) try { delete [] m_pnItems; } catch (...) {}
		m_pnItems = ppn;
		m_nNumItems++;
		return m_nNumItems;

	}
	
	return PROMOLITE_ERROR;
}


//////////////////////////////////////////////////////////////////////
// CPromoLiteMappingObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPromoLiteMappingObject::CPromoLiteMappingObject()
{
	m_ulStatus	= PROMOLITE_STATUS_UNINIT;
	m_ulFlags = PROMOLITE_FLAG_DISABLED;  // various states
	m_ulType = PROMOLITE_RULE_TYPE_UNKNOWN;
	m_usComparisonType = PROMOLITE_RULE_COMPARE_UNKNOWN;
	m_ulDestinationType = PROMOLITE_RULE_DESTTYPE_UNKNOWN;
	m_usSearchType = PROMOLITE_RULE_SEARCH_UNKNOWN;
	m_usActionType = PROMOLITE_RULE_ACTION_UNKNOWN;
	m_nMappingID  =-1;
	m_pszFieldName = NULL;
	m_pszParamName = NULL;
	m_pszCriterion = NULL;
	m_pszEventLocation = NULL;
}

CPromoLiteMappingObject::~CPromoLiteMappingObject()
{
	if(m_pszFieldName) free(m_pszFieldName); // must use malloc to allocate
	if(m_pszParamName) free(m_pszParamName); // must use malloc to allocate
	if(m_pszCriterion) free(m_pszCriterion); // must use malloc to allocate
	if(m_pszEventLocation) free(m_pszEventLocation); // must use malloc to allocate
}

//////////////////////////////////////////////////////////////////////
// CPromoLiteEventObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPromoLiteEventObject::CPromoLiteEventObject()
{
	m_ppszChildren=NULL;
	m_pnChildrenStatus = NULL;
	m_nNumChildren = 0;
	m_nCurrentFileSearch = -1;
	m_nMainFileStatus = -1;
	m_pszMainFile = NULL;
}

CPromoLiteEventObject::~CPromoLiteEventObject()
{
	if(m_ppszChildren)
	{
		int i=0;
		while(i<m_nNumChildren)
		{
			if(m_ppszChildren[i]) free(m_ppszChildren[i]);
			i++;
		}
		delete [] m_ppszChildren;
	}
	if(m_pnChildrenStatus) delete [] m_pnChildrenStatus;
	if(m_pszMainFile) free(m_pszMainFile);
}

int CPromoLiteEventObject::AddChild(char* pszChild, int nStatus)
{
	if((pszChild)&&(strlen(pszChild)))
	{
		char** ppszChildren = new char*[m_nNumChildren+1];
		if(ppszChildren)
		{
			int*  pnChildrenStatus = new int[m_nNumChildren+1];
			if(pnChildrenStatus)
			{
				if(m_ppszChildren)
				{
					int n=0;
					while(n<m_nNumChildren)
					{
						ppszChildren[n] = m_ppszChildren[n];
						n++;
					}
					delete [] m_ppszChildren;
				}
				if(m_pnChildrenStatus)
				{
					int n=0;
					while(n<m_nNumChildren)
					{
						pnChildrenStatus[n] = m_pnChildrenStatus[n];
						n++;
					}
					delete [] m_pnChildrenStatus;
				}
				ppszChildren[m_nNumChildren] = pszChild;
				pnChildrenStatus[m_nNumChildren] = nStatus;
				m_nNumChildren++;
				m_ppszChildren=ppszChildren; 
				m_pnChildrenStatus=pnChildrenStatus; 
				return m_nNumChildren;
			}
			else delete [] ppszChildren;
		}
	}
	return -1;
}



//////////////////////////////////////////////////////////////////////
// CPromoLiteData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPromoLiteData::CPromoLiteData()
{
	m_bNetClientConnected = false;
	m_socket = NULL;

	InitializeCriticalSection(&m_critText);
	InitializeCriticalSection(&m_critSQL);
	InitializeCriticalSection(&m_critEventRules);
	InitializeCriticalSection(&m_critParameterRules);
	
	// messaging...
	m_bNetworkMessagingInitialized=false;

	m_bDelayingTriggerNotification = false;
	m_bCheckModsWarningSent = false;
	m_bCheckMsgsWarningSent = false;
	m_bCheckAsRunWarningSent = false;

//	m_dblHarrisTime = -1;
/*
	m_ppConnObj = NULL;
	m_nNumConnectionObjects = 0;
*/

/*
	m_ppEventsList=NULL;
	m_nNumEventsList=0;
	m_ppEventsNewList=NULL;
	m_nNumEventsNewList=0;
	m_ppEventsPlaying=NULL;
	m_nNumEventsPlaying=0;
*/
	m_nNumberOfEvents = 0;
//	m_ppRulesObj = NULL;
//	m_nNumRulesObjects = 0;
	m_ppMappingObj = NULL;
	m_nNumMappingObjects = 0;
	m_ppEventRule = NULL;
	m_nNumEventRules=0;
	m_ppParameterRule = NULL;
	m_nNumParameterRules=0;
	m_ppParameterRuleQuery=NULL;
	m_nNumParameterRuleQueries=0;
	m_ppEventRuleQuery=NULL;
	m_nNumEventRuleQueries=0;
	m_ppszTimingColName=NULL;
	m_nNumTimingColNames=0;


	m_nLastParameterRulesMod=-1;
	m_nParameterRulesMod=-1;

	m_nLastEventRulesMod=-1;
	m_nEventRulesMod=-1;

	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebTick );
	_ftime( &m_timebAutoPurge ); // the last time autopurge was run

	m_pszHost = NULL;			// the name of the host
	m_pszCortexHost = NULL;			// the name of the cortex host
	m_pszCompleteHost = NULL;	// the complete name of the host
	m_usCortexCommandPort = PROMOLITE_PORT_CMD;
	m_usCortexStatusPort = PROMOLITE_PORT_STATUS;
	m_nSettingsMod = -1;
//	m_nChannelsMod = -1;
	m_nConnectionsMod = -1;
//	m_nRulesMod = -1;
//	m_nLastRulesMod = -1;
	m_nLastSettingsMod = -1;
//	m_nLastChannelsMod = -1;
	m_nLastConnectionsMod = -100;  //foreces on init.
	m_nLastEventsMod=-100;
	m_nEventsMod=-1;



	m_nMappingMod = -1;
	m_nLastMappingMod = -1;

	m_pdb = NULL;
	m_pdbConn = NULL;
	m_bQuietKill = false;

	m_nEventCheckIndex = -1;
//	m_nEventLastMax = 1;
	m_prsEvents = NULL;

	m_nTypeAutomationInstalled = PROMOLITE_DEP_UNKNOWN;  // automation system unknown
	m_nIndexAutomationEndpoint=-1; // which automation module is installed.

//	m_pTransferEvent = NULL;
	m_pCheckEvent = NULL;

	m_bProcessSuspended = false;
	m_bAutomationThreadStarted=false;
	m_bGlobalAnalysisThreadStarted = false;
//	m_bFarAnalysisThreadStarted=false;
//	m_bNearAnalysisThreadStarted=false;
//	m_bFarEventsChanged= false;
	m_bNearEventsChanged= false;
//	m_bTriggerEventsChanged=false;
//	m_bTriggerThreadStarted = false;
	m_bForceAnalysis = false;
	m_bRunningAnalyses = false;
}

CPromoLiteData::~CPromoLiteData()
{
/*
	if(m_ppConnObj)
	{
		int i=0;
		while(i<m_nNumConnectionObjects)
		{
			if(m_ppConnObj[i]) delete m_ppConnObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppConnObj; // delete array of pointers to objects, must use new to allocate
	}
*/


	EnterCriticalSection(&m_critEventRules);
	if(m_ppEventRule)
	{
		int i=0;
		while(i<m_nNumEventRules)
		{
			if(m_ppEventRule[i]) delete m_ppEventRule[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppEventRule; // delete array of pointers to objects, must use new to allocate
	}

	if(m_ppEventRuleQuery)
	{
		int i=0;
		while(i<m_nNumEventRuleQueries)
		{
			if(m_ppEventRuleQuery[i]) delete m_ppEventRuleQuery[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppEventRuleQuery; // delete array of pointers to objects, must use new to allocate
	}
	if(m_ppszTimingColName)
	{
		int i=0;
		while(i<m_nNumTimingColNames)
		{
			if(m_ppszTimingColName[i]) delete m_ppszTimingColName[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppszTimingColName; // delete array of pointers to objects, must use new to allocate
	}
	m_ppszTimingColName=NULL;
	m_nNumTimingColNames=0;

	m_ppEventRuleQuery=NULL;
	m_nNumEventRuleQueries=0;
	m_ppEventRule = NULL;
	m_nNumEventRules=0;

	LeaveCriticalSection(&m_critEventRules);

	EnterCriticalSection(&m_critParameterRules);
	if(m_ppParameterRule)
	{
		int i=0;
		while(i<m_nNumParameterRules)
		{
			if(m_ppParameterRule[i]) delete m_ppParameterRule[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppParameterRule; // delete array of pointers to objects, must use new to allocate
	}
	if(m_ppParameterRuleQuery)
	{
		int i=0;
		while(i<m_nNumParameterRuleQueries)
		{
			if(m_ppParameterRuleQuery[i]) delete m_ppParameterRuleQuery[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppParameterRuleQuery; // delete array of pointers to objects, must use new to allocate
	}
	m_ppParameterRuleQuery=NULL;
	m_nNumParameterRuleQueries=0;
	m_ppParameterRule = NULL;
	m_nNumParameterRules=0;
	LeaveCriticalSection(&m_critParameterRules);


	if(m_ppMappingObj)
	{
		int i=0;
		while(i<m_nNumMappingObjects)
		{
			if(m_ppMappingObj[i]) delete m_ppMappingObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppMappingObj; // delete array of pointers to objects, must use new to allocate
	}
	if(m_pszHost) free(m_pszHost);	// must use malloc to allocate
	if(m_pszCompleteHost) free(m_pszCompleteHost);	// must use malloc to allocate
	if(m_pszCortexHost) free(m_pszCortexHost);	// must use malloc to allocate

	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critSQL);
	DeleteCriticalSection(&m_critEventRules);
	DeleteCriticalSection(&m_critParameterRules);

//	if(	m_pTransferEvent ) delete m_pTransferEvent;
	if(	m_pCheckEvent ) delete m_pCheckEvent;

}

char* CPromoLiteData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CPromoLiteData::SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError)
{
	int nRV = CX_ERROR;
	bool bError=false;

	if(!bOverwriteExistingError)
	{
		if(
			  ((m_ulFlags&PROMOLITE_ICON_MASK) == PROMOLITE_STATUS_ERROR)
			||((m_ulFlags&PROMOLITE_STATUS_CMDSVR_MASK) == PROMOLITE_STATUS_CMDSVR_ERROR)
			||((m_ulFlags&PROMOLITE_STATUS_STATUSSVR_MASK) ==  PROMOLITE_STATUS_STATUSSVR_ERROR)
			||((m_ulFlags&PROMOLITE_STATUS_THREAD_MASK) == PROMOLITE_STATUS_THREAD_ERROR)
			||((m_ulFlags&PROMOLITE_STATUS_FAIL_MASK) == PROMOLITE_STATUS_FAIL_DB)
			)	bError=true;
	}

	if (ulStatus&PROMOLITE_ICON_MASK)
	{
		m_ulFlags &= ~PROMOLITE_ICON_MASK;
		m_ulFlags |= (ulStatus&PROMOLITE_ICON_MASK);
	}
	if (ulStatus&PROMOLITE_STATUS_CMDSVR_MASK)
	{
		m_ulFlags &= ~PROMOLITE_STATUS_CMDSVR_MASK;
		m_ulFlags |= (ulStatus&PROMOLITE_STATUS_CMDSVR_MASK);
	}
	if (ulStatus&PROMOLITE_STATUS_STATUSSVR_MASK)
	{
		m_ulFlags &= ~PROMOLITE_STATUS_STATUSSVR_MASK;
		m_ulFlags |= (ulStatus&PROMOLITE_STATUS_STATUSSVR_MASK);
	}
	if (ulStatus&PROMOLITE_STATUS_THREAD_MASK)
	{
		m_ulFlags &= ~PROMOLITE_STATUS_THREAD_MASK;
		m_ulFlags |= (ulStatus&PROMOLITE_STATUS_THREAD_MASK);
	}
	if (ulStatus&PROMOLITE_STATUS_FAIL_MASK)
	{
		m_ulFlags &= ~PROMOLITE_STATUS_FAIL_MASK;
		m_ulFlags |= (ulStatus&PROMOLITE_STATUS_FAIL_MASK);
	}

	if(bError)
	{
		m_ulFlags &= ~PROMOLITE_ICON_MASK;
		m_ulFlags |= PROMOLITE_STATUS_ERROR;
	}

	if((pszText)&&(!bError))
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
//				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
//			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_ppromolite->m_settings.m_pszIconPath)&&(strlen(g_ppromolite->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_ppromolite->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_ppromolite->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_ppromolite->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_ppromolite->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_ppromolite->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}

/*
int	CPromoLiteData::SetStatusText(char* pszText, unsigned long ulStatus)
{
	int nRV = CX_ERROR;
	if(pszText)
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_ppromolite->m_settings.m_pszIconPath)&&(strlen(g_ppromolite->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_ppromolite->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_ppromolite->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_ppromolite->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_ppromolite->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_ppromolite->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}
	return nRV;
}
*/
// utility
int	CPromoLiteData::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszCompleteHost) free(m_pszCompleteHost);
						m_pszCompleteHost=pch;

						pch = (char*) malloc(nValue+1);
						if(pch!=NULL)
						{
							if(m_pszHost) free(m_pszHost);
							m_pszHost=pch;

							strcpy(m_pszHost, m_pszCompleteHost);
							pch = strchr(m_pszHost, '.');
							if(pch)
							{
								*pch = 0;
							}

							return PROMOLITE_SUCCESS;
						}
					}
				}
			}
		}
	}
	return PROMOLITE_ERROR;
}


/*
int CPromoLiteData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_ppromolite)&&(g_ppromolite->m_settings.m_pszExchange)&&(strlen(g_ppromolite->m_settings.m_pszExchange)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];

		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT MAX(mod) AS MAX FROM %s WHERE criterion = '%s'", 
			g_ppromolite->m_settings.m_pszExchange, szTemp		);

		//Sleep(500);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, dberrorstring);
		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				CString szValue;
				prs->GetFieldValue("MAX", szValue);
				if(szValue.GetLength()>0)
				{
					unsigned long ulLastMod = atol(szValue);
					if(ulLastMod>0) ulMod = ulLastMod+1;
				}
			}
			prs->Close();
			delete prs;
		}

		if(ulMod>0)
		{
			if(ulMod > PROMOLITE_DB_MOD_MAX) ulMod = 1;
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET mod = %d WHERE criterion = '%s'", // HARDCODE
				g_ppromolite->m_settings.m_pszExchange, ulMod, szTemp		);
			if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
			{
				return PROMOLITE_SUCCESS;
			}
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (criterion, flag, mod) VALUES ('%s', '', 1)", // HARDCODE
				g_ppromolite->m_settings.m_pszExchange, szTemp		);
			if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
			{
				return PROMOLITE_SUCCESS;
			}
		}
	}
	return PROMOLITE_ERROR;
}
*/
int CPromoLiteData::IncrementGlobalTimesUsed(CPromoLiteEventObject* pObj, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.
	if(   (g_ppromolite->m_data.m_nIndexMetadataEndpoint>=0)
			&&(g_ppromolite->m_settings.m_ppEndpointObject)
			&&(g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint])
			&&(m_pdb)&&(m_pdbConn)
		 )
	{
//		ReleaseRecordSet(); // have to release so we can get new results.
//		char errorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];

		CString szFilename;
		if(pObj->m_nCurrentFileSearch<0)
		{
			szFilename = pObj->m_pszMainFile;
			char* pchPartition = strchr(pObj->m_pszMainFile, '/'); // have to remove partition.
			if(pchPartition)
			{
				szFilename.Format("%s", pchPartition+1);
			}

		}
		else
		{
			szFilename = pObj->m_ppszChildren[pObj->m_nCurrentFileSearch];
			char* pchPartition = strchr(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch], '/'); // have to remove partition.
			if(pchPartition)
			{
				szFilename.Format("%s", pchPartition+1);
			}
		}

		char* pszEncodedFilename = m_pdb->EncodeQuotes(szFilename);
		char* pszEncodedFilepath = NULL;

		// need ot get filepath.
		CFileMetaDataObject* pfmdo =  ReturnFileMetaDataObject(szFilename.GetBuffer(1));
		szFilename.ReleaseBuffer();
		if(pfmdo)
		{
			pszEncodedFilepath = m_pdb->EncodeQuotes(pfmdo->m_sz_vc256_sys_filepath);
			delete pfmdo;
			pfmdo = NULL;
		}


		if(pszEncodedFilepath)
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET promolite_last_used = %d, promolite_times_used = \
(SELECT case when max(promolite_times_used) is null then 1 when max(promolite_times_used) >= %d \
then %d else max(promolite_times_used) + 1 end from %s.dbo.%s WHERE sys_filename = '%s' AND sys_filepath = '%s') \
WHERE sys_filename = '%s' AND sys_filepath = '%s'",
				g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
				g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the metadata table name
				(unsigned long)(m_timebTick.time - (m_timebTick.timezone*60) +(m_timebTick.dstflag?3600:0)),  // local time
				INT_MAX, INT_MAX,
				g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
				g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the metadata table name
				pszEncodedFilename?pszEncodedFilename:szFilename, pszEncodedFilepath,
				pszEncodedFilename?pszEncodedFilename:szFilename, pszEncodedFilepath
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET promolite_last_used = %d, promolite_times_used = \
(SELECT case when max(promolite_times_used) is null then 1 when max(promolite_times_used) >= %d \
then %d else max(promolite_times_used) + 1 end from %s.dbo.%s WHERE sys_filename = '%s') WHERE sys_filename = '%s'",
				g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
				g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the metadata table name
				(unsigned long)(m_timebTick.time - (m_timebTick.timezone*60) +(m_timebTick.dstflag?3600:0)),  // local time
				INT_MAX, INT_MAX,
				g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
				g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the metadata table name
				pszEncodedFilename?pszEncodedFilename:szFilename,
				pszEncodedFilename?pszEncodedFilename:szFilename		
				);
		}

		EnterCriticalSection(&m_critSQL);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "IncGlobal SQL: %s", szSQL);  Sleep(100); //(Dispatch message)
//		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)>=DB_SUCCESS)
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);

			
/*
// not using increment for archivist.
			g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->IncrementDatabaseMods(
				m_pdb, m_pdbConn, 
				g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata", 
				pszInfo);
*/
			//we need to tell archivist what file record we just modified, so we need to insert into its exchange table.
			if(pszEncodedFilepath)
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s (criterion, flag, mod) VALUES \
('DBT_%s', '%s|%s', %d )",  // the pipe is a delimiter,blank path is before filename
					g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
					g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszExchange?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszExchange:"Exchange",   // the Exchange table name
					g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the metadata table name
					pszEncodedFilepath, pszEncodedFilename?pszEncodedFilename:szFilename,
					(unsigned long)(m_timebTick.time - (m_timebTick.timezone*60) +(m_timebTick.dstflag?3600:0))  // local time
					);
				EnterCriticalSection(&m_critSQL);
				if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
				{
					// error
				}
				LeaveCriticalSection(&m_critSQL);
			}
			else
			{
				// dont insert it.  it means it was a null oxt transfer,  or there was not metadata for it.  so skip it.
/*
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s (criterion, flag, mod) VALUES \
('DBT_%s', '|%s', %d )",  // the pipe is a delimiter,blank path is before filename
					g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
					g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszExchange?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszExchange:"Exchange",   // the Exchange table name
					g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata?g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the metadata table name
					pszEncodedFilename?pszEncodedFilename:szFilename,	
					(unsigned long)(m_timebTick.time - (m_timebTick.timezone*60) +(m_timebTick.dstflag?3600:0))  // local time
					);

				EnterCriticalSection(&m_critSQL);
				if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
				{
					// error
				}
				LeaveCriticalSection(&m_critSQL);
*/
			}

			if(pszEncodedFilename) free(pszEncodedFilename);
			if(pszEncodedFilepath) free(pszEncodedFilepath);


			return PROMOLITE_SUCCESS;
		}
//		else g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "IncGlobal ERROR: %s", errorstring);  Sleep(100); //(Dispatch message)

		if(pszEncodedFilename) free(pszEncodedFilename);
		if(pszEncodedFilepath) free(pszEncodedFilepath);

		LeaveCriticalSection(&m_critSQL);

	}
	return PROMOLITE_ERROR;
}

int CPromoLiteData::IncrementLocalTimesUsed(CPromoLiteEventObject* pObj, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.
	int nDestModule=ReturnModuleIndex(pObj, pszInfo);

	if(
		  (pObj)&&(m_pdb)&&(m_pdbConn)
			&&(nDestModule>=0)
			&&(g_ppromolite->m_settings.m_nNumEndpointsInstalled>0)
			&&(nDestModule<g_ppromolite->m_settings.m_nNumEndpointsInstalled)
			&&(g_ppromolite->m_settings.m_ppEndpointObject)
			&&(g_ppromolite->m_settings.m_ppEndpointObject[nDestModule])
		)
	{
		char pszFilename[MAX_PATH];
		char pszPartition[MAX_PATH];
		strcpy(pszFilename, "");
		memset(pszPartition, 0, MAX_PATH);
		if(g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_usType == PROMOLITE_DEP_EDGE_PROSPERO)
		{
			if(pObj->m_nCurrentFileSearch < 0)
			{ // main file
				if((pObj->m_pszMainFile)&&(strlen(pObj->m_pszMainFile)))
				{
					char* pchPartition = strchr(pObj->m_pszMainFile, '/'); // have to remove partition.
					if(pchPartition)
					{
						strcpy(pszFilename, pchPartition+1);
						strncpy(pszPartition, pObj->m_pszMainFile, pchPartition-(pObj->m_pszMainFile));
					}
				}
			}
			else
			if(pObj->m_nCurrentFileSearch < pObj->m_nNumChildren)
			{
				if((pObj->m_ppszChildren)&&(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch])&&(strlen(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch])))
				{
					char* pchPartition = strchr(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch], '/'); // have to remove partition.
					if(pchPartition)
					{
						strcpy(pszFilename, pchPartition+1);
						strncpy(pszPartition, pObj->m_ppszChildren[pObj->m_nCurrentFileSearch], 
							pchPartition-(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch]));
					}
				}
			}

			if(strlen(pszFilename))
			{
//				ReleaseRecordSet(); // have to release so we can get new results.
//				char errorstring[DB_ERRORSTRING_LEN];
				char szSQL[DB_SQLSTRING_MAXLEN];
				CString szTemp = "Destinations_Media";  // we didn't find it. just use the default name
				if(nDestModule>=0) szTemp = g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestinationMedia?g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestinationMedia:"Destinations_Media";
				char* pszEncodedFilename = m_pdb->EncodeQuotes(pszFilename);


				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET PromoLite_local_last_used = %d, PromoLite_local_times_used = \
(SELECT case when max(PromoLite_local_times_used) is null then 1 when max(PromoLite_local_times_used) >= %d \
then %d else max(PromoLite_local_times_used) + 1 end from %s.dbo.%s WHERE file_name = '%s' AND host = '%s' AND partition = '%s') \
WHERE file_name = '%s' AND host = '%s' AND partition = '%s'",
					pObj->m_sz_vc64_module_dbname,			// the edge device DB name
					szTemp,
					(unsigned long)(m_timebTick.time - (m_timebTick.timezone*60) +(m_timebTick.dstflag?3600:0)), // local time....
					INT_MAX, INT_MAX,
					pObj->m_sz_vc64_module_dbname,			// the edge device DB name
					szTemp,
					pszEncodedFilename?pszEncodedFilename:pszFilename, 
					pObj->m_sz_vc64_dest_host, pszPartition,
					pszEncodedFilename?pszEncodedFilename:pszFilename,	
					pObj->m_sz_vc64_dest_host, pszPartition
					);
				
				if(pszEncodedFilename) free(pszEncodedFilename);
			
				EnterCriticalSection(&m_critSQL);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "IncLocal SQL: %s", szSQL);  Sleep(100); //(Dispatch message)
//		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)>=DB_SUCCESS)
				if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
				{
					LeaveCriticalSection(&m_critSQL);

					g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->IncrementDatabaseMods(
						m_pdb, m_pdbConn, 
						szTemp.GetBuffer(1),
						pszInfo);
					szTemp.ReleaseBuffer();

					return PROMOLITE_SUCCESS;
				}
//				else g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "IncLocal ERROR: %s", errorstring);  Sleep(100); //(Dispatch message)
				LeaveCriticalSection(&m_critSQL);
			}
		}  // no other types supported yet.
	}
	return PROMOLITE_ERROR;
}

int CPromoLiteData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_ppromolite)&&(g_ppromolite->m_settings.m_pszExchange)&&(strlen(g_ppromolite->m_settings.m_pszExchange)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s set mod = \
(SELECT case when max(mod) is null then 1 when max(mod) >= %d \
then 1 else max(mod) + 1 end from %s WHERE criterion = '%s') WHERE criterion = '%s'",
			g_ppromolite->m_settings.m_pszExchange,
			PROMOLITE_DB_MOD_MAX,
			g_ppromolite->m_settings.m_pszExchange,
			szTemp, szTemp		
			);
//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return PROMOLITE_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return PROMOLITE_ERROR;
}


int CPromoLiteData::CheckMessages(char* pszInfo)
{
	if((g_ppromolite)&&(m_pdbConn)&&(m_pdb)
		&&(g_ppromolite->m_settings.m_pszMessages)&&(strlen(g_ppromolite->m_settings.m_pszMessages)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_ppromolite->m_settings.m_pszMessages)&&(strlen(g_ppromolite->m_settings.m_pszMessages)))?g_ppromolite->m_settings.m_pszMessages:"Messages",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_ppromolite->m_settings.m_nAutoPurgeMessageDays*86400) // local time....
			);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return PROMOLITE_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return PROMOLITE_ERROR;
}

int CPromoLiteData::CheckAsRun(char* pszInfo)
{
	if((g_ppromolite)&&(m_pdbConn)&&(m_pdb)
		&&(g_ppromolite->m_settings.m_pszAsRun)&&(strlen(g_ppromolite->m_settings.m_pszAsRun)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_ppromolite->m_settings.m_pszAsRun)&&(strlen(g_ppromolite->m_settings.m_pszAsRun)))?g_ppromolite->m_settings.m_pszAsRun:"AsRun_Log",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_ppromolite->m_settings.m_nAutoPurgeAsRunDays*86400) // local time....
			);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return PROMOLITE_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return PROMOLITE_ERROR;
}


int CPromoLiteData::CheckDatabaseMods(char* pszInfo)
{
	if((g_ppromolite)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_ppromolite->m_settings.m_pszExchange)&&(strlen(g_ppromolite->m_settings.m_pszExchange)))?g_ppromolite->m_settings.m_pszExchange:"Exchange");

//		ReleaseRecordSet();
		EnterCriticalSection(&g_ppromolite->m_data.m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = PROMOLITE_SUCCESS;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCriterion;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
				}

				if((g_ppromolite->m_settings.m_pszSettings)&&(strlen(g_ppromolite->m_settings.m_pszSettings)))
				{
					szTemp.Format("DBT_%s",g_ppromolite->m_settings.m_pszSettings);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nSettingsMod = nReturn;
					}
				}

#ifndef NUCLEUS_PROMOLITE_CHANGE
				if((g_ppromolite->m_settings.m_pszMappings)&&(strlen(g_ppromolite->m_settings.m_pszMappings)))
				{
					szTemp.Format("DBT_%s",g_ppromolite->m_settings.m_pszMappings);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nMappingMod = nReturn;
					}
				}

				if((g_ppromolite->m_settings.m_pszParameterRuleView)&&(strlen(g_ppromolite->m_settings.m_pszParameterRuleView)))
				{
					szTemp.Format("DBT_%s",g_ppromolite->m_settings.m_pszParameterRuleView);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nParameterRulesMod = nReturn;
					}
				}

				if((g_ppromolite->m_settings.m_pszChannelInfo)&&(strlen(g_ppromolite->m_settings.m_pszChannelInfo)))
				{
					szTemp.Format("DBT_%s",g_ppromolite->m_settings.m_pszChannelInfo);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nConnectionsMod = nReturn;
					}
				}


				if((g_ppromolite->m_settings.m_pszEventRules)&&(strlen(g_ppromolite->m_settings.m_pszEventRules)))
				{
					szTemp.Format("DBT_%s",g_ppromolite->m_settings.m_pszEventRules);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nEventRulesMod = nReturn;
					}
				}

				if((g_ppromolite->m_settings.m_pszEvents)&&(strlen(g_ppromolite->m_settings.m_pszEvents)))
				{
					szTemp.Format("DBT_%s",g_ppromolite->m_settings.m_pszEvents);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nEventsMod = nReturn;
					}
				}

#endif // #ifndef NUCLEUS_PROMOLITE_CHANGE


				// get the suspend

				szTemp.Format("Suspend");
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					nReturn = atoi(szMod);
					if(nReturn>0)
					{
						if(!m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s is suspended.", g_ppromolite->m_settings.m_pszName?g_ppromolite->m_settings.m_pszName:"PromoLite");  
							g_ppromolite->m_data.SetStatusText(errorstring, CX_STATUS_UNKNOWN);
							g_ppromolite->m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:suspend", "*** %s has been suspended. ***", g_ppromolite->m_settings.m_pszName?g_ppromolite->m_settings.m_pszName:"PromoLite"); // Sleep(50); //(Dispatch message)
							g_ppromolite->SendMsg(CX_SENDMSG_INFO, "PromoLite:suspend", "%s has been suspended", g_ppromolite->m_settings.m_pszName?g_ppromolite->m_settings.m_pszName:"PromoLite");
							m_bProcessSuspended = true;
						}
					}
					else
					{
						if(m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s is running.", g_ppromolite->m_settings.m_pszName?g_ppromolite->m_settings.m_pszName:"PromoLite");  
							g_ppromolite->m_data.SetStatusText(errorstring, CX_STATUS_OK);
							g_ppromolite->m_msgr.DM(MSG_ICONNONE, NULL, "PromoLite:resume", "*** %s has been resumed. ***", g_ppromolite->m_settings.m_pszName?g_ppromolite->m_settings.m_pszName:"PromoLite");//  Sleep(50); //(Dispatch message)
							g_ppromolite->SendMsg(CX_SENDMSG_INFO, "PromoLite:resume", "%s has been resumed.", g_ppromolite->m_settings.m_pszName?g_ppromolite->m_settings.m_pszName:"PromoLite");
							m_bProcessSuspended = false;
						}
					}
				}
/*
				if((g_ppromolite->m_settings.m_pszChannels)&&(strlen(g_ppromolite->m_settings.m_pszChannels)))
				{
					szTemp.Format("DBT_%s",g_ppromolite->m_settings.m_pszChannels);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nChannelsMod = nReturn;
					}
				}

				if((g_ppromolite->m_settings.m_pszConnections)&&(strlen(g_ppromolite->m_settings.m_pszConnections)))
				{
					szTemp.Format("DBT_%s",g_ppromolite->m_settings.m_pszConnections);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nConnectionsMod = nReturn;
					}
				}
*/
				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			delete prs;
			LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);

			return nReturn;
		}
		LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);

	}
	return PROMOLITE_ERROR;
}



int CPromoLiteData::GetParameterRules(char* pszInfo)
{
	if((g_ppromolite)&&(m_pdbConn)&&(m_pdb))
	{

		CPromoLiteEndpointObject* pAutoObj = NULL;
		if((g_ppromolite->m_settings.m_ppEndpointObject)&&(g_ppromolite->m_data.m_nIndexAutomationEndpoint>=0)&&(g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexAutomationEndpoint]))
			pAutoObj = g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexAutomationEndpoint];

		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
			"SELECT parameterid, name, target_typeid, target_valueid, returned_property_module, returned_property_source, returned_property_type, returned_property_col_name,  default_value, parameterruleid, target_criterion_module, target_criterion_source, target_criterion_type, target_criterion_col_name, compare_type, criterion_value from %s",
			((g_ppromolite->m_settings.m_pszParameterRuleView)&&(strlen(g_ppromolite->m_settings.m_pszParameterRuleView)))?g_ppromolite->m_settings.m_pszParameterRuleView:"ParameterRuleView");

//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Parameter rule SQL: %s", szSQL); // Sleep(100); //(Dispatch message)

//		ReleaseRecordSet();
		EnterCriticalSection(&g_ppromolite->m_data.m_critSQL);
		char errorstring[DB_SQLSTRING_MAXLEN];
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
//		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = PROMOLITE_ERROR;
			int nIndex = 0;
			int nTemp = 0;
			while ((!prs->IsEOF()))
			{
				try
				{
					int num;

					ParameterRule_t* per = new ParameterRule_t;
					if(per)
					{
						if((m_ppParameterRule)&&(nIndex<m_nNumParameterRules))
						{
							if(m_ppParameterRule[nIndex]) delete m_ppParameterRule[nIndex];
							m_ppParameterRule[nIndex] = per;
							nIndex++;
						}
						else
						{
							// have to realloc
							ParameterRule_t** ppObj = new ParameterRule_t*[nIndex+1];
							if(ppObj)
							{
								num=0;
								if(m_ppParameterRule)
								{
									while(num<m_nNumParameterRules)
									{
										ppObj[num] = m_ppParameterRule[num];
										num++;
									}
									delete [] m_ppParameterRule;
								}
								ppObj[num] = per;
								nIndex = num+1;
	EnterCriticalSection(&m_critParameterRules);
								m_nNumParameterRules = nIndex;
								m_ppParameterRule = ppObj;
	LeaveCriticalSection(&m_critParameterRules);
							}
							else
							{
								delete per; per=NULL;
								continue;
							}
						}
						prs->GetFieldValue("parameterid", per->sz_parameterid);//HARDCODE
						per->sz_parameterid.TrimLeft(); per->sz_parameterid.TrimRight();
						prs->GetFieldValue("name", per->sz_name);//HARDCODE
						per->sz_name.TrimLeft(); per->sz_name.TrimRight();
						prs->GetFieldValue("target_typeid", per->sz_target_typeid);//HARDCODE
						per->sz_target_typeid.TrimLeft(); per->sz_target_typeid.TrimRight();
						prs->GetFieldValue("target_valueid", per->sz_target_valueid);//HARDCODE
						per->sz_target_valueid.TrimLeft(); per->sz_target_valueid.TrimRight();
						prs->GetFieldValue("returned_property_module", per->sz_returned_property_module);//HARDCODE
						per->sz_returned_property_module.TrimLeft(); per->sz_returned_property_module.TrimRight();
						prs->GetFieldValue("returned_property_source", per->sz_returned_property_source);//HARDCODE
						per->sz_returned_property_source.TrimLeft(); per->sz_returned_property_source.TrimRight();
						prs->GetFieldValue("returned_property_type", per->sz_returned_property_type);//HARDCODE
						per->sz_returned_property_type.TrimLeft(); per->sz_returned_property_type.TrimRight();
						prs->GetFieldValue("returned_property_col_name", per->sz_returned_property_col_name);//HARDCODE
						per->sz_returned_property_col_name.TrimLeft(); per->sz_returned_property_col_name.TrimRight();
						prs->GetFieldValue("default_value", per->sz_default_value);//HARDCODE
						per->sz_default_value.TrimLeft(); per->sz_default_value.TrimRight();
						prs->GetFieldValue("parameterruleid", per->sz_parameterruleid);//HARDCODE
						per->sz_parameterruleid.TrimLeft(); per->sz_parameterruleid.TrimRight();
						prs->GetFieldValue("target_criterion_module", per->sz_target_criterion_module);//HARDCODE
						per->sz_target_criterion_module.TrimLeft(); per->sz_target_criterion_module.TrimRight();

						prs->GetFieldValue("target_criterion_source", per->sz_target_criterion_source);//HARDCODE
						per->sz_target_criterion_source.TrimLeft(); per->sz_target_criterion_source.TrimRight();
						prs->GetFieldValue("target_criterion_type", per->sz_target_criterion_type);//HARDCODE
						per->sz_target_criterion_type.TrimLeft(); per->sz_target_criterion_type.TrimRight();
						prs->GetFieldValue("target_criterion_col_name", per->sz_target_criterion_col_name);//HARDCODE
						per->sz_target_criterion_col_name.TrimLeft(); per->sz_target_criterion_col_name.TrimRight();

						prs->GetFieldValue("compare_type", per->sz_compare_type);//HARDCODE
						per->sz_compare_type.TrimLeft(); per->sz_compare_type.TrimRight();

/*

create table LU_Comparison_Type (compare_type char(2) NOT NULL, name varchar(32), description varchar(256));
insert into LU_Comparison_Type (compare_type, name, description) values ('==', '<code>&#61;</code>', 'Equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('>', '<code>&#62;</code>', 'Greater than');
insert into LU_Comparison_Type (compare_type, name, description) values ('<', '<code>&#60;</code>', 'Less than');
insert into LU_Comparison_Type (compare_type, name, description) values ('>=', '<code>&#8805;</code>', 'Greater than or equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('<=', '<code>&#8804;</code>', 'Less than or equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('!=', '<code>&#8800;</code>', 'Not equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('~=', '<code>&#8776;</code>', 'Partial Comparison');
insert into LU_Comparison_Type (compare_type, name, description) values ('&', '<code>&#8715;</code>', 'Contains any bitflags');
insert into LU_Comparison_Type (compare_type, name, description) values ('=&', '<code>&#8839;</code>', 'Contains all bitflags');
insert into LU_Comparison_Type (compare_type, name, description) values ('!&', '<code>&#8713;</code>', 'Does not contain bitflags');
*/
	
					if(per->sz_compare_type.Compare("==")==0)				per->nComparisonType = PROMOLITE_RULE_COMPARE_EQUALS;
					else if(per->sz_compare_type.Compare(">")==0)		per->nComparisonType = PROMOLITE_RULE_COMPARE_GT;
					else if(per->sz_compare_type.Compare("<")==0)		per->nComparisonType = PROMOLITE_RULE_COMPARE_LT;
					else if(per->sz_compare_type.Compare(">=")==0)	per->nComparisonType = PROMOLITE_RULE_COMPARE_GTOE;
					else if(per->sz_compare_type.Compare("<=")==0)	per->nComparisonType = PROMOLITE_RULE_COMPARE_LTOE;
					else if(per->sz_compare_type.Compare("!=")==0)	per->nComparisonType = PROMOLITE_RULE_COMPARE_NOTEQUAL;
					else if(per->sz_compare_type.Compare("~=")==0)	per->nComparisonType = PROMOLITE_RULE_COMPARE_PARTIAL;
					else if(per->sz_compare_type.Compare("&")==0)		per->nComparisonType = PROMOLITE_RULE_COMPARE_AND;
					else if(per->sz_compare_type.Compare("=&")==0)	per->nComparisonType = PROMOLITE_RULE_COMPARE_ALL;
					else if(per->sz_compare_type.Compare("!&")==0)	per->nComparisonType = PROMOLITE_RULE_COMPARE_NOT;
					else per->nComparisonType=PROMOLITE_RULE_COMPARE_UNKNOWN;

					prs->GetFieldValue("criterion_value", per->sz_criterion_value);//HARDCODE
					per->sz_criterion_value.TrimLeft(); per->sz_criterion_value.TrimRight();

/*g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Parameter rule obtained: %s %d: [%s][%s][%s]", per->sz_name,
																				m_nNumParameterRules,
									per->sz_target_criterion_col_name,
									per->sz_compare_type,
									per->sz_criterion_value
		
											);//  Sleep(100); //(Dispatch message)
*/
					}
				}
				catch( ... )
				{
				}

				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "removing non found rules");  Sleep(100); //(Dispatch message)
			LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);

			// now, create the queries;

			EnterCriticalSection(&m_critParameterRules);

			// build all the queries.

			nIndex = 0;

			CString whereclause = "";
			CString wtemp = "";

			CString query1 = "";
			CString query2 = "";
			CString szType = "";

			int nParameterRuleRecord = 0;
			while((m_ppParameterRule)&&(nParameterRuleRecord<m_nNumParameterRules))
			{
				if(
					  (m_ppParameterRule[nParameterRuleRecord])
					&&(
						  (m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name.GetLength())
						||(m_ppParameterRule[nParameterRuleRecord]->sz_target_valueid.Compare("0")==0)
						)
					)
				{
					bool numerictype = true;
					if(
							(m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_type.CompareNoCase("Text")==0)
						||(m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_type.CompareNoCase("Selection")==0)
						)
						
						numerictype = false;

/*g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "query for %s: param record %d, [%s][%s][%s]", 
											m_ppParameterRule[nParameterRuleRecord]->sz_name,
											nParameterRuleRecord,
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_compare_type,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
											
											);//  Sleep(100); //(Dispatch message)
*/
					switch(m_ppParameterRule[nParameterRuleRecord]->nComparisonType)
					{
					case PROMOLITE_RULE_COMPARE_UNKNOWN://			0xffff  // type unknown
						{
							wtemp = whereclause;
						} break;
					case PROMOLITE_RULE_COMPARE_EQUALS://				0x0001  // == equals
						{
							if(numerictype)
								wtemp.Format("%s and %s = %s", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
							else
								wtemp.Format("%s and %s = '%s'", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);

						} break;
					case PROMOLITE_RULE_COMPARE_GT://						0x0002  // > greater than
						{
							if(numerictype)
								wtemp.Format("%s and %s > %s", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
							else
								wtemp.Format("%s and %s > '%s'", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
						} break;
					case PROMOLITE_RULE_COMPARE_LT://						0x0003  // < less than
						{
							if(numerictype)
								wtemp.Format("%s and %s < %s", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
							else
								wtemp.Format("%s and %s < '%s'", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
						} break;
					case PROMOLITE_RULE_COMPARE_GTOE://					0x0004  // >= greater than or equal to
						{
							if(numerictype)
								wtemp.Format("%s and (%s > %s or %s = %s)", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value,
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
							else
								wtemp.Format("%s and (%s > '%s' or %s = '%s')", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value,
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
						} break;
					case PROMOLITE_RULE_COMPARE_LTOE://					0x0005  // <= less than or equal to
						{
							if(numerictype)
								wtemp.Format("%s and (%s < %s or %s = %s)", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value,
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
							else
								wtemp.Format("%s and (%s < '%s' or %s = '%s')", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value,
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
						} break;
					case PROMOLITE_RULE_COMPARE_NOTEQUAL://			0x0006  // != equals
						{
							if(numerictype)
								wtemp.Format("%s and %s <> %s", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
							else
								wtemp.Format("%s and %s <> '%s'", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
						} break;
					case PROMOLITE_RULE_COMPARE_PARTIAL://			0x0007  // ~= partial comparison
						{
							// regardless of numerictype
							wtemp.Format("%s and %s LIKE '%s'", 
								whereclause, 
								m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
								m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
								);
						} break;
					case PROMOLITE_RULE_COMPARE_AND://			0x0008  //  &  contains any of the bitflags
						{
							if(numerictype)
								wtemp.Format("%s and (%s & %s) <> 0 ", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
							else
								wtemp.Format("%s and %s LIKE '%%%s%%'", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
						} break;
					case PROMOLITE_RULE_COMPARE_ALL://			0x0009  // =& contains all the bitflags
						{
							if(numerictype)
								wtemp.Format("%s and (%s & %s) = %s", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
							else
								wtemp.Format("%s and %s = '%s'", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
						} break;
					case PROMOLITE_RULE_COMPARE_NOT://			0x000a  // !& does not contain any the bitflag
						{
							if(numerictype)
								wtemp.Format("%s and (%s & %s) = 0 ",
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
							else
								wtemp.Format("%s and %s NOT LIKE '%%%s%%'", 
									whereclause, 
									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
									);
						} break;
					}
				

					whereclause = wtemp;
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "query for %s: whereclause so far [%s]", m_ppParameterRule[nParameterRuleRecord]->sz_name, whereclause);//  Sleep(100); //(Dispatch message)

					bool trigger = false;
					bool type0 = false;
					if(nParameterRuleRecord==m_nNumParameterRules-1) trigger = true;
					if(  
						  (nParameterRuleRecord<m_nNumParameterRules-1) 
						&&(m_ppParameterRule[nParameterRuleRecord]->sz_name.Compare(m_ppParameterRule[nParameterRuleRecord+1]->sz_name)) // different
						) trigger = true;
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "sz_target_valueid %s for %s", m_ppParameterRule[nParameterRuleRecord]->sz_target_valueid, m_ppParameterRule[nParameterRuleRecord]->sz_name);//  Sleep(100); //(Dispatch message)

					if(m_ppParameterRule[nParameterRuleRecord]->sz_target_valueid.Compare("0")==0) type0 = true;
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "query 1 for %s: trigger = %s", m_ppParameterRule[nParameterRuleRecord]->sz_name, trigger?"true":"false");//  Sleep(100); //(Dispatch message)

					if(
						  (trigger)
						&&(
							  ( (whereclause.GetLength()) && (!type0) )
							||(type0)
							)
						)
					{

						if(type0)
						{
							if(m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_type.CompareNoCase("Time")==0)
							{
								// have to cast this as decimal 15,3 so it doesnt come out as text in some format like 3.5E13 or whatever

								query1.Format("select top 1 cast(val as decimal(15,3)) as val, datatype from (select top 1 (case when len(%s) > 0 THEN %s ELSE '%s' END) as val, event_start, \
(select datatype from %s where col_name = '%s' and module = '%s' and source = '%s') as datatype from %s.dbo.%s as LiveEventData where \
parent_start > ",

//									m_ppParameterRule[nParameterRuleRecord]->sz_target_valueid,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_default_value,
									(g_ppromolite->m_settings.m_pszMetaConfigUnionALLView?g_ppromolite->m_settings.m_pszMetaConfigUnionALLView:"MetaConfigUnionALLView"),
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_module,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_source,
									(g_ppromolite->m_settings.m_pszCortexDB?g_ppromolite->m_settings.m_pszCortexDB:"Cortex"),
									(g_ppromolite->m_settings.m_pszLiveEventData?g_ppromolite->m_settings.m_pszLiveEventData:"LiveEventData")
									);
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Parameter query 1 for NOW %s (%s): %s ", m_ppParameterRule[nParameterRuleRecord]->sz_name, m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_type, query1);//  Sleep(100); //(Dispatch message)

								query2.Format("%s order by event_start) as Temp order by event_start desc",
									whereclause
									);
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Parameter query 2 for NOW %s (%s): %s", m_ppParameterRule[nParameterRuleRecord]->sz_name,m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_type, query2);//  Sleep(100); //(Dispatch message)

							}
							else
							{

								query1.Format("select top 1 val, datatype from (select top 1 (case when len(%s) > 0 THEN %s ELSE '%s' END) as val, event_start, \
(select datatype from %s where col_name = '%s' and module = '%s' and source = '%s') as datatype from %s.dbo.%s as LiveEventData where \
parent_start > ",

//									m_ppParameterRule[nParameterRuleRecord]->sz_target_valueid,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_default_value,
									(g_ppromolite->m_settings.m_pszMetaConfigUnionALLView?g_ppromolite->m_settings.m_pszMetaConfigUnionALLView:"MetaConfigUnionALLView"),
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_module,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_source,
									(g_ppromolite->m_settings.m_pszCortexDB?g_ppromolite->m_settings.m_pszCortexDB:"Cortex"),
									(g_ppromolite->m_settings.m_pszLiveEventData?g_ppromolite->m_settings.m_pszLiveEventData:"LiveEventData")
									);
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Parameter query 1 for NOW %s (%s): %s ", m_ppParameterRule[nParameterRuleRecord]->sz_name, m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_type, query1);//  Sleep(100); //(Dispatch message)
								query2.Format("%s order by event_start) as Temp order by event_start desc",
									whereclause
									);
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Parameter query 2 for NOW %s (%s): %s", m_ppParameterRule[nParameterRuleRecord]->sz_name,m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_type, query2);//  Sleep(100); //(Dispatch message)
							}						
						}
						else
						{

							if(m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_type.CompareNoCase("Time")==0)
							{
								// have to cast this aas decimal 15,3 so it doesnt come out as text in some format like 3.5E13 or whatever

								query1.Format("select top 1 cast(val as decimal(15,3)) as val, datatype from (select top %s (case when len(%s) > 0 THEN %s ELSE '%s' END) as val, event_start, \
(select datatype from %s where col_name = '%s' and module = '%s' and source = '%s') as datatype from %s.dbo.%s as LiveEventData where \
event_calc_start > ",

									m_ppParameterRule[nParameterRuleRecord]->sz_target_valueid,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_default_value,
									(g_ppromolite->m_settings.m_pszMetaConfigUnionALLView?g_ppromolite->m_settings.m_pszMetaConfigUnionALLView:"MetaConfigUnionALLView"),
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_module,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_source,
									(g_ppromolite->m_settings.m_pszCortexDB?g_ppromolite->m_settings.m_pszCortexDB:"Cortex"),
									(g_ppromolite->m_settings.m_pszLiveEventData?g_ppromolite->m_settings.m_pszLiveEventData:"LiveEventData")
									);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Parameter query 1 for %s (%s): %s ", m_ppParameterRule[nParameterRuleRecord]->sz_name, m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_type, query1);//  Sleep(100); //(Dispatch message)

								if((pAutoObj)&&((pAutoObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_SENTINEL))
								{
								//sentinel

									//65535 to exclude the flags for upcounter, post upcounter, etc....
								query2.Format(" and (event_status is NULL or event_status & 65535 = 0)%s order by event_start) as Temp order by event_start desc",
									whereclause
									);
								}
								else
								if((pAutoObj)&&((pAutoObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_HELIOS))
								{
								//helios
								query2.Format(" and (event_status is NULL or ( event_status > 4 and event_status < 10) )%s order by event_start) as Temp order by event_start desc",
									whereclause
									);
								}
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Parameter query 2 for %s (%s): %s", m_ppParameterRule[nParameterRuleRecord]->sz_name,m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_type, query2);//  Sleep(100); //(Dispatch message)

							}
							else
							{

								query1.Format("select top 1 val, datatype from (select top %s (case when len(%s) > 0 THEN %s ELSE '%s' END) as val, event_start, \
(select datatype from %s where col_name = '%s' and module = '%s' and source = '%s') as datatype from %s.dbo.%s as LiveEventData where \
event_calc_start > ",

									m_ppParameterRule[nParameterRuleRecord]->sz_target_valueid,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_default_value,
									(g_ppromolite->m_settings.m_pszMetaConfigUnionALLView?g_ppromolite->m_settings.m_pszMetaConfigUnionALLView:"MetaConfigUnionALLView"),
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_col_name,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_module,
									m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_source,
									(g_ppromolite->m_settings.m_pszCortexDB?g_ppromolite->m_settings.m_pszCortexDB:"Cortex"),
									(g_ppromolite->m_settings.m_pszLiveEventData?g_ppromolite->m_settings.m_pszLiveEventData:"LiveEventData")
									);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Parameter query 1 for %s (%s): %s ", m_ppParameterRule[nParameterRuleRecord]->sz_name, m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_type, query1);//  Sleep(100); //(Dispatch message)
								if((pAutoObj)&&((pAutoObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_SENTINEL))
								{
								//sentinel
//								query2.Format(" and ( event_status is NULL or event_status = 0) and %s = '%s' order by event_start) as Temp order by event_start desc",
//									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
//									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
	//								);
									//65535 to exclude the flags for upcounter, post upcounter, etc....
								query2.Format(" and ( event_status is NULL or event_status & 65535 = 0)%s order by event_start) as Temp order by event_start desc",
									whereclause
									);
								}
								else
								if((pAutoObj)&&((pAutoObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_HELIOS))
								{

								//helios
//								query2.Format(" and ( event_status is NULL or ( event_status > 4 and event_status < 10) ) and %s = '%s' order by event_start) as Temp order by event_start desc",
//									m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_col_name,
//									m_ppParameterRule[nParameterRuleRecord]->sz_criterion_value
	//								);
								//helios
								query2.Format(" and ( event_status is NULL or ( event_status > 4 and event_status < 10) )%s order by event_start) as Temp order by event_start desc",
									whereclause
									);
								}

//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Parameter query 2 for %s (%s): %s", m_ppParameterRule[nParameterRuleRecord]->sz_name,m_ppParameterRule[nParameterRuleRecord]->sz_returned_property_type, query2);//  Sleep(100); //(Dispatch message)
							}
						}

						whereclause = "";

						int num;
						ParameterRuleQuery_t* per = new ParameterRuleQuery_t;
						if(per)
						{
							per->sz_name = m_ppParameterRule[nParameterRuleRecord]->sz_name;
							per->sz_type = m_ppParameterRule[nParameterRuleRecord]->sz_target_criterion_type;
							per->sz_query1 = query1;
							per->sz_query2 = query2;
							if((m_ppParameterRuleQuery)&&(nIndex<m_nNumParameterRuleQueries))
							{
								if(m_ppParameterRuleQuery[nIndex]) delete m_ppParameterRuleQuery[nIndex];
								m_ppParameterRuleQuery[nIndex] = per;
								nIndex++;
							}
							else
							{
								// have to realloc
								ParameterRuleQuery_t** ppObj = new ParameterRuleQuery_t*[nIndex+1];
								if(ppObj)
								{
									num=0;
									if(m_ppParameterRuleQuery)
									{
										while(num<m_nNumParameterRuleQueries)
										{
											ppObj[num] = m_ppParameterRuleQuery[num];
											num++;
										}
										delete [] m_ppParameterRuleQuery;
									}
									ppObj[num] = per;
									nIndex = num+1;

									m_nNumParameterRuleQueries = nIndex;
									m_ppParameterRuleQuery = ppObj;

								}
								else
								{
									delete per; per=NULL;
									continue;
								}
							}
						}
					}
				}
				
				nParameterRuleRecord++;
			} // while

			LeaveCriticalSection(&m_critParameterRules);



			return nReturn;
		}
		else
		{
		LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Parameter rules returned 0 objects.  %s", errorstring);//  Sleep(100); //(Dispatch message)
		}
	}
	return PROMOLITE_ERROR;
}


int CPromoLiteData::GetTimingColumns(char* pszInfo)
{
	if((g_ppromolite)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
			"select distinct promolite_Timing_Col_Name as col_name from %s.dbo.%s", 
			(((g_ppromolite->m_settings.m_pszDefaultDB)&&(strlen(g_ppromolite->m_settings.m_pszDefaultDB)))?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
			(((g_ppromolite->m_settings.m_pszTriggerInfoView)&&(strlen(g_ppromolite->m_settings.m_pszTriggerInfoView)))?g_ppromolite->m_settings.m_pszTriggerInfoView:"TriggerInfo")
			);

if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TIMING) 	
{ g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Timing Column SQL: %s", szSQL); }

//		ReleaseRecordSet();
		EnterCriticalSection(&g_ppromolite->m_data.m_critSQL);
		char errorstring[DB_SQLSTRING_MAXLEN];
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
//		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = PROMOLITE_ERROR;
			int nIndex = 0;
			int nTemp = 0;
			while ((!prs->IsEOF()))
			{
				try
				{
					int num;

					CString* per = new CString;
					if(per)
					{
						if((m_ppszTimingColName)&&(nIndex<m_nNumTimingColNames))
						{
							if(m_ppszTimingColName[nIndex]) delete m_ppszTimingColName[nIndex];
							m_ppszTimingColName[nIndex] = per;
							nIndex++;
						}
						else
						{
							// have to realloc
							CString** ppObj = new CString*[nIndex+1];
							if(ppObj)
							{
								num=0;
								if(m_ppszTimingColName)
								{
									while(num<m_nNumTimingColNames)
									{
										ppObj[num] = m_ppszTimingColName[num];
										num++;
									}
									delete [] m_ppszTimingColName;
								}
								ppObj[num] = per;
								nIndex = num+1;
	EnterCriticalSection(&m_critEventRules);
								m_nNumTimingColNames = nIndex;
								m_ppszTimingColName = ppObj;
	LeaveCriticalSection(&m_critEventRules);
							}
							else
							{
								delete per; per=NULL;
								continue;
							}
						}

						prs->GetFieldValue("col_name", *per);//HARDCODE
						per->TrimLeft(); per->TrimRight();

if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_TIMING) 	
{ g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Timing column name obtained: %s", *per);}
					}
				}
				catch( ... )
				{
				}

				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "removing non found rules");  Sleep(100); //(Dispatch message)
			LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);


			return nReturn;
		}
		else
		{
		LeaveCriticalSection(&m_critSQL);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Timing columns returned 0 objects.  %s", errorstring);//  Sleep(100); //(Dispatch message)
		}
	}
	return PROMOLITE_ERROR;
}




int CPromoLiteData::GetEventRules(char* pszInfo)
{
	if((g_ppromolite)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
			"SELECT eventid, ruleid, col_name, compare_type, value, type, span_clips FROM %s where user_statusid = 1 order by eventid, ruleid", //user_statusid = 1 gets only active items.
			((g_ppromolite->m_settings.m_pszEventRules)&&(strlen(g_ppromolite->m_settings.m_pszEventRules)))?g_ppromolite->m_settings.m_pszEventRules:"EventRuleView");

//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Event rule SQL: %s", szSQL); // Sleep(100); //(Dispatch message)

//		ReleaseRecordSet();
		EnterCriticalSection(&g_ppromolite->m_data.m_critSQL);
		char errorstring[DB_SQLSTRING_MAXLEN];
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
//		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = PROMOLITE_ERROR;
			int nIndex = 0;
			int nTemp = 0;
			while ((!prs->IsEOF()))
			{
				try
				{
					int num;

					EventRule_t* per = new EventRule_t;
					if(per)
					{
						if((m_ppEventRule)&&(nIndex<m_nNumEventRules))
						{
							if(m_ppEventRule[nIndex]) delete m_ppEventRule[nIndex];
							m_ppEventRule[nIndex] = per;
							nIndex++;
						}
						else
						{
							// have to realloc
							EventRule_t** ppObj = new EventRule_t*[nIndex+1];
							if(ppObj)
							{
								num=0;
								if(m_ppEventRule)
								{
									while(num<m_nNumEventRules)
									{
										ppObj[num] = m_ppEventRule[num];
										num++;
									}
									delete [] m_ppEventRule;
								}
								ppObj[num] = per;
								nIndex = num+1;
	EnterCriticalSection(&m_critEventRules);
								m_nNumEventRules = nIndex;
								m_ppEventRule = ppObj;
	LeaveCriticalSection(&m_critEventRules);
							}
							else
							{
								delete per; per=NULL;
								continue;
							}
						}

						prs->GetFieldValue("eventid", per->sz_eventid);//HARDCODE
						per->sz_eventid.TrimLeft(); per->sz_eventid.TrimRight();
						prs->GetFieldValue("ruleid", per->sz_ruleid);//HARDCODE
						per->sz_ruleid.TrimLeft(); per->sz_ruleid.TrimRight();
						prs->GetFieldValue("col_name", per->sz_col_name);//HARDCODE
						per->sz_col_name.TrimLeft(); per->sz_col_name.TrimRight();
						prs->GetFieldValue("compare_type", per->sz_compare_type);//HARDCODE
						per->sz_compare_type.TrimLeft(); per->sz_compare_type.TrimRight();

/*

create table LU_Comparison_Type (compare_type char(2) NOT NULL, name varchar(32), description varchar(256));
insert into LU_Comparison_Type (compare_type, name, description) values ('==', '<code>&#61;</code>', 'Equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('>', '<code>&#62;</code>', 'Greater than');
insert into LU_Comparison_Type (compare_type, name, description) values ('<', '<code>&#60;</code>', 'Less than');
insert into LU_Comparison_Type (compare_type, name, description) values ('>=', '<code>&#8805;</code>', 'Greater than or equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('<=', '<code>&#8804;</code>', 'Less than or equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('!=', '<code>&#8800;</code>', 'Not equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('~=', '<code>&#8776;</code>', 'Partial Comparison');
insert into LU_Comparison_Type (compare_type, name, description) values ('&', '<code>&#8715;</code>', 'Contains any bitflags');
insert into LU_Comparison_Type (compare_type, name, description) values ('=&', '<code>&#8839;</code>', 'Contains all bitflags');
insert into LU_Comparison_Type (compare_type, name, description) values ('!&', '<code>&#8713;</code>', 'Does not contain bitflags');
*/
	
					if(per->sz_compare_type.Compare("==")==0)				per->nComparisonType = PROMOLITE_RULE_COMPARE_EQUALS;
					else if(per->sz_compare_type.Compare(">")==0)		per->nComparisonType = PROMOLITE_RULE_COMPARE_GT;
					else if(per->sz_compare_type.Compare("<")==0)		per->nComparisonType = PROMOLITE_RULE_COMPARE_LT;
					else if(per->sz_compare_type.Compare(">=")==0)	per->nComparisonType = PROMOLITE_RULE_COMPARE_GTOE;
					else if(per->sz_compare_type.Compare("<=")==0)	per->nComparisonType = PROMOLITE_RULE_COMPARE_LTOE;
					else if(per->sz_compare_type.Compare("!=")==0)	per->nComparisonType = PROMOLITE_RULE_COMPARE_NOTEQUAL;
					else if(per->sz_compare_type.Compare("~=")==0)	per->nComparisonType = PROMOLITE_RULE_COMPARE_PARTIAL;
					else if(per->sz_compare_type.Compare("&")==0)		per->nComparisonType = PROMOLITE_RULE_COMPARE_AND;
					else if(per->sz_compare_type.Compare("=&")==0)	per->nComparisonType = PROMOLITE_RULE_COMPARE_ALL;
					else if(per->sz_compare_type.Compare("!&")==0)	per->nComparisonType = PROMOLITE_RULE_COMPARE_NOT;
					else per->nComparisonType=PROMOLITE_RULE_COMPARE_UNKNOWN;

					prs->GetFieldValue("value", per->sz_value);//HARDCODE
					per->sz_value.TrimLeft(); per->sz_value.TrimRight();
					prs->GetFieldValue("type", per->sz_type);//HARDCODE
					per->sz_type.TrimLeft(); per->sz_type.TrimRight();
					prs->GetFieldValue("span_clips", per->sz_span_clips);//HARDCODE
					per->sz_span_clips.TrimLeft(); per->sz_span_clips.TrimRight();

//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Event rule obtained for eventid = %s", per->sz_eventid); // Sleep(100); //(Dispatch message)
					}
				}
				catch( ... )
				{
				}

				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "removing non found rules");  Sleep(100); //(Dispatch message)
			LeaveCriticalSection(&g_ppromolite->m_data.m_critSQL);

			EnterCriticalSection(&m_critEventRules);

			// build all the queries.

			nIndex = 0;

			CString whereclause = "";
			CString wtemp = "";

			CString query1 = "";
			CString query2 = "";

			int nEventRuleRecord = 0;
			while((m_ppEventRule)&&(nEventRuleRecord<m_nNumEventRules))
			{
				if((m_ppEventRule[nEventRuleRecord])&&(m_ppEventRule[nEventRuleRecord]->sz_col_name.GetLength()))
				{
					bool numerictype = true;
					if(
							(m_ppEventRule[nEventRuleRecord]->sz_type.CompareNoCase("Text")==0)	
						||(m_ppEventRule[nEventRuleRecord]->sz_type.CompareNoCase("Selection")==0)	
						)
						numerictype = false;

					switch(m_ppEventRule[nEventRuleRecord]->nComparisonType)
					{
					case PROMOLITE_RULE_COMPARE_UNKNOWN://			0xffff  // type unknown
						{
							wtemp = whereclause;
						} break;
					case PROMOLITE_RULE_COMPARE_EQUALS://				0x0001  // == equals
						{
							if(numerictype)
								wtemp.Format("%s and %s = %s", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
							else
								wtemp.Format("%s and %s = '%s'", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);

						} break;
					case PROMOLITE_RULE_COMPARE_GT://						0x0002  // > greater than
						{
							if(numerictype)
								wtemp.Format("%s and %s > %s", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
							else
								wtemp.Format("%s and %s > '%s'", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
						} break;
					case PROMOLITE_RULE_COMPARE_LT://						0x0003  // < less than
						{
							if(numerictype)
								wtemp.Format("%s and %s < %s", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
							else
								wtemp.Format("%s and %s < '%s'", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
						} break;
					case PROMOLITE_RULE_COMPARE_GTOE://					0x0004  // >= greater than or equal to
						{
							if(numerictype)
								wtemp.Format("%s and (%s > %s or %s = %s)", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value,
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
							else
								wtemp.Format("%s and (%s > '%s' or %s = '%s')", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value,
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
						} break;
					case PROMOLITE_RULE_COMPARE_LTOE://					0x0005  // <= less than or equal to
						{
							if(numerictype)
								wtemp.Format("%s and (%s < %s or %s = %s)", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value,
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
							else
								wtemp.Format("%s and (%s < '%s' or %s = '%s')", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value,
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
						} break;
					case PROMOLITE_RULE_COMPARE_NOTEQUAL://			0x0006  // != equals
						{
							if(numerictype)
								wtemp.Format("%s and %s <> %s", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
							else
								wtemp.Format("%s and %s <> '%s'", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
						} break;
					case PROMOLITE_RULE_COMPARE_PARTIAL://			0x0007  // ~= partial comparison
						{
							// regardless of numerictype
							wtemp.Format("%s and %s LIKE '%s'", 
								whereclause, 
								m_ppEventRule[nEventRuleRecord]->sz_col_name,
								m_ppEventRule[nEventRuleRecord]->sz_value
								);
						} break;
					case PROMOLITE_RULE_COMPARE_AND://			0x0008  //  &  contains any of the bitflags
						{
							if(numerictype)
								wtemp.Format("%s and (%s & %s) <> 0 ", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
							else
								wtemp.Format("%s and %s LIKE '%%%s%%'", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
						} break;
					case PROMOLITE_RULE_COMPARE_ALL://			0x0009  // =& contains all the bitflags
						{
							if(numerictype)
								wtemp.Format("%s and (%s & %s) = %s", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
							else
								wtemp.Format("%s and %s = '%s'", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
						} break;
					case PROMOLITE_RULE_COMPARE_NOT://			0x000a  // !& does not contain any the bitflag
						{
							if(numerictype)
								wtemp.Format("%s and (%s & %s) = 0 ",
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
							else
								wtemp.Format("%s and %s NOT LIKE '%%%s%%'", 
									whereclause, 
									m_ppEventRule[nEventRuleRecord]->sz_col_name,
									m_ppEventRule[nEventRuleRecord]->sz_value
									);
						} break;
					}
				

					whereclause = wtemp;

					bool trigger = false;
					if(nEventRuleRecord==m_nNumEventRules-1) trigger = true;
					if(  
						  (nEventRuleRecord<m_nNumEventRules-1) 
						&&(m_ppEventRule[nEventRuleRecord]->sz_eventid.Compare(m_ppEventRule[nEventRuleRecord+1]->sz_eventid)) // different
						) trigger = true;


					if((trigger)&&(whereclause.GetLength()))
					{
						query1.Format("UPDATE %s.dbo.%s SET status = 1 from %s.dbo.%s as Analysis, \
%s.dbo.%s as LiveEventData where Analysis.automation_event_itemid = LiveEventData.itemid and Analysis.promolite_eventid = %s %s",
									(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
									(g_ppromolite->m_settings.m_pszAnalysis?g_ppromolite->m_settings.m_pszAnalysis:"Analysis"),
									(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
									(g_ppromolite->m_settings.m_pszAnalysis?g_ppromolite->m_settings.m_pszAnalysis:"Analysis"),
									(g_ppromolite->m_settings.m_pszCortexDB?g_ppromolite->m_settings.m_pszCortexDB:"Cortex"),
									(g_ppromolite->m_settings.m_pszLiveEventData?g_ppromolite->m_settings.m_pszLiveEventData:"LiveEventData"),
									m_ppEventRule[nEventRuleRecord]->sz_eventid, whereclause

								);

						query2.Format("INSERT INTO %s.dbo.%s \
(promolite_eventid, automation_event_itemid, status) select %s, LiveEventData.itemid, 1 from %s.dbo.%s as LiveEventData \
left join %s.dbo.%s as Analysis on LiveEventData.itemid = Analysis.automation_event_itemid and Analysis.promolite_eventid = %s \
where Analysis.automation_event_itemid is NULL %s",

									(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
									(g_ppromolite->m_settings.m_pszAnalysis?g_ppromolite->m_settings.m_pszAnalysis:"Analysis"),
									m_ppEventRule[nEventRuleRecord]->sz_eventid,
									(g_ppromolite->m_settings.m_pszCortexDB?g_ppromolite->m_settings.m_pszCortexDB:"Cortex"),
									(g_ppromolite->m_settings.m_pszLiveEventData?g_ppromolite->m_settings.m_pszLiveEventData:"LiveEventData"),
									(g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite"),
									(g_ppromolite->m_settings.m_pszAnalysis?g_ppromolite->m_settings.m_pszAnalysis:"Analysis"),
									m_ppEventRule[nEventRuleRecord]->sz_eventid,whereclause
								);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "query 1 for event rule[%d]: %s", nEventRuleRecord, query1);//  Sleep(100); //(Dispatch message)
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "query 2 for event rule[%d]: %s", nEventRuleRecord, query2);//  Sleep(100); //(Dispatch message)

						whereclause = "";

						int num;
						EventRuleQuery_t* per = new EventRuleQuery_t;
						if(per)
						{
							per->sz_query1 = query1;
							per->sz_query2 = query2;
							if((m_ppEventRuleQuery)&&(nIndex<m_nNumEventRuleQueries))
							{
								if(m_ppEventRuleQuery[nIndex]) delete m_ppEventRuleQuery[nIndex];
								m_ppEventRuleQuery[nIndex] = per;
								nIndex++;
							}
							else
							{
								// have to realloc
								EventRuleQuery_t** ppObj = new EventRuleQuery_t*[nIndex+1];
								if(ppObj)
								{
									num=0;
									if(m_ppEventRuleQuery)
									{
										while(num<m_nNumEventRuleQueries)
										{
											ppObj[num] = m_ppEventRuleQuery[num];
											num++;
										}
										delete [] m_ppEventRuleQuery;
									}
									ppObj[num] = per;
									nIndex = num+1;

									m_nNumEventRuleQueries = nIndex;
									m_ppEventRuleQuery = ppObj;

								}
								else
								{
									delete per; per=NULL;
									continue;
								}
							}
						}
					}
				}
				
				nEventRuleRecord++;
			} // while

			LeaveCriticalSection(&m_critEventRules);

			return nReturn;
		}
		else
		{
		LeaveCriticalSection(&m_critSQL);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Event rules returned 0 objects.  %s", errorstring);//  Sleep(100); //(Dispatch message)
		}
	}
	return PROMOLITE_ERROR;
}



int CPromoLiteData::GetMappings(char* pszInfo)
{
	if((g_ppromolite)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_ppromolite->m_settings.m_pszMappings)&&(strlen(g_ppromolite->m_settings.m_pszMappings)))?g_ppromolite->m_settings.m_pszMappings:"Mapping");

//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "rule SQL: %s", szSQL);  Sleep(100); //(Dispatch message)

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = PROMOLITE_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
/*
	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;
	unsigned short m_usComparisonType;
	unsigned short m_usDestinationType;
	unsigned short m_usSearchType;
	unsigned short m_usActionType;
	int m_nMappingID;

	char* m_pszFieldName;
	char* m_pszParamName;
	char* m_pszCriterion;
	char* m_pszEventLocation;

create table Rules (rule_type tinyint, field_name varchar(32), param_name varchar(32), comparison varchar(10), 
criterion varchar(64), event_location varchar(32), dest_type int, search_type tinyint, action tinyint, 
ruleid int IDENTITY (1, 1) NOT NULL);
				
*/
				CString szField = "";
				CString szParam = "";
				CString szCrtierion = "";
				CString szLocation = "";
				CString szTemp = "";
				int nMappingID = -1;
				int nType = -1;
				int nComparisonType = -1;
				int nDestinationType = -1;
				int nSearchType = -1;
				int nActionType = -1;
				int nTemp;
				bool bFound = false;
				try
				{
					prs->GetFieldValue("rule_type", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nType = nTemp;
					}
					prs->GetFieldValue("field_name", szField);//HARDCODE
					szField.TrimLeft(); szField.TrimRight();
					prs->GetFieldValue("param_name", szParam);//HARDCODE
					szParam.TrimLeft(); szParam.TrimRight();
					prs->GetFieldValue("comparison", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();

					if(szTemp.Compare("==")==0) nComparisonType = PROMOLITE_RULE_COMPARE_EQUALS;
					else if(szTemp.Compare("!=")==0) nComparisonType = PROMOLITE_RULE_COMPARE_NOTEQUAL;
					else if(szTemp.Compare(">=")==0) nComparisonType = PROMOLITE_RULE_COMPARE_GTOE;
					else if(szTemp.Compare("<=")==0) nComparisonType = PROMOLITE_RULE_COMPARE_LTOE;

					prs->GetFieldValue("criterion", szCrtierion);//HARDCODE
					szCrtierion.TrimLeft(); szCrtierion.TrimRight();
					prs->GetFieldValue("event_location", szLocation);//HARDCODE
					szLocation.TrimLeft(); szLocation.TrimRight();

					prs->GetFieldValue("dest_type", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
/*
// if it were hex.

						char* p=szTemp.GetBuffer(1);
						nTemp = m_bu.xtol(p, strlen(p));
						szTemp.ReleaseBuffer();
*/
						// but we will just make it decimal
						nTemp = atoi(szTemp);
						if(nTemp>0) nDestinationType = nTemp;
					}
					prs->GetFieldValue("search_type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nSearchType = nTemp;
					}
					prs->GetFieldValue("action", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nActionType = nTemp;
					}
					prs->GetFieldValue("ruleid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nMappingID = nTemp;
					}

//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "rule obtained: %d", nMappingID);  Sleep(100); //(Dispatch message)

				}
				catch( ... )
				{
				}

				if((m_ppMappingObj)&&(m_nNumMappingObjects))
				{
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "rule obtained: %d, checking exists", nMappingID);  Sleep(100); //(Dispatch message)
					nTemp=0;
					while(nTemp<m_nNumMappingObjects)
					{
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "rule obtained: %d, checking exists against #%d", nMappingID, nTemp);  Sleep(100); //(Dispatch message)
						if(m_ppMappingObj[nTemp])
						{
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "rule obtained: %d against #%d, not null", nMappingID, nTemp);  Sleep(100); //(Dispatch message)
							if((nMappingID>=0)&&(nMappingID == m_ppMappingObj[nTemp]->m_nMappingID))
							{
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "rule obtained: %d found");  Sleep(100); //(Dispatch message)
								bFound = true;
								// override with the new changes:
								if(nType>=0) m_ppMappingObj[nTemp]->m_ulType = (unsigned long) nType;
								if(nComparisonType>=0) m_ppMappingObj[nTemp]->m_usComparisonType = (unsigned short) nComparisonType;
								if(nDestinationType>=0) m_ppMappingObj[nTemp]->m_ulDestinationType = (unsigned long) nDestinationType;
								if(nSearchType>=0) m_ppMappingObj[nTemp]->m_usSearchType = (unsigned short) nSearchType;
								if(nActionType>=0) m_ppMappingObj[nTemp]->m_usActionType = (unsigned short) nActionType;

								if(	(szField.GetLength()>0)&&((m_ppMappingObj[nTemp]->m_pszFieldName==NULL)||(szField.CompareNoCase(m_ppMappingObj[nTemp]->m_pszFieldName))) )
								{
									if(m_ppMappingObj[nTemp]->m_pszFieldName) free(m_ppMappingObj[nTemp]->m_pszFieldName);
									m_ppMappingObj[nTemp]->m_pszFieldName = (char*)malloc(szField.GetLength()+1); 
									if(m_ppMappingObj[nTemp]->m_pszFieldName) sprintf(m_ppMappingObj[nTemp]->m_pszFieldName, szField);
								}

								if(	(szParam.GetLength()>0)&&((m_ppMappingObj[nTemp]->m_pszParamName==NULL)||(szParam.CompareNoCase(m_ppMappingObj[nTemp]->m_pszParamName))) )
								{
									if(m_ppMappingObj[nTemp]->m_pszParamName) free(m_ppMappingObj[nTemp]->m_pszParamName);
									m_ppMappingObj[nTemp]->m_pszParamName = (char*)malloc(szParam.GetLength()+1); 
									if(m_ppMappingObj[nTemp]->m_pszParamName) sprintf(m_ppMappingObj[nTemp]->m_pszParamName, szParam);
								}

								if(	(szCrtierion.GetLength()>0)&&((m_ppMappingObj[nTemp]->m_pszCriterion==NULL)||(szCrtierion.CompareNoCase(m_ppMappingObj[nTemp]->m_pszCriterion))) )
								{
									if(m_ppMappingObj[nTemp]->m_pszCriterion) free(m_ppMappingObj[nTemp]->m_pszCriterion);
									m_ppMappingObj[nTemp]->m_pszCriterion = (char*)malloc(szCrtierion.GetLength()+1); 
									if(m_ppMappingObj[nTemp]->m_pszCriterion) sprintf(m_ppMappingObj[nTemp]->m_pszCriterion, szCrtierion);
								}

								if(	(szLocation.GetLength()>0)&&((m_ppMappingObj[nTemp]->m_pszEventLocation==NULL)||(szLocation.CompareNoCase(m_ppMappingObj[nTemp]->m_pszEventLocation))) )
								{
									if(m_ppMappingObj[nTemp]->m_pszEventLocation) free(m_ppMappingObj[nTemp]->m_pszEventLocation);
									m_ppMappingObj[nTemp]->m_pszEventLocation = (char*)malloc(szLocation.GetLength()+1); 
									if(m_ppMappingObj[nTemp]->m_pszEventLocation) sprintf(m_ppMappingObj[nTemp]->m_pszEventLocation, szLocation);
								}

								m_ppMappingObj[nTemp]->m_ulFlags = PROMOLITE_FLAG_FOUND;
							}
						}
						nTemp++;
					}
				}
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "finished search for rule %d", nMappingID);  Sleep(100); //(Dispatch message)

				if((!bFound)&&(nMappingID>=0)) // have to add.
				{
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "adding rule %d", nMappingID);  Sleep(100); //(Dispatch message)

					CPromoLiteMappingObject* pscho = new CPromoLiteMappingObject;
					if(pscho)
					{
						CPromoLiteMappingObject** ppObj = new CPromoLiteMappingObject*[m_nNumMappingObjects+1];
						if(ppObj)
						{
							int o=0;
							if((m_ppMappingObj)&&(m_nNumMappingObjects>0))
							{
								while(o<m_nNumMappingObjects)
								{
									ppObj[o] = m_ppMappingObj[o];
									o++;
								}
								delete [] m_ppMappingObj;

							}
							ppObj[m_nNumMappingObjects] = pscho;
							m_ppMappingObj = ppObj;
							m_nNumMappingObjects++;

//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "adding rule %d, type is %d", nMappingID, nType);  Sleep(100); //(Dispatch message)
							pscho->m_nMappingID = nMappingID;
							if(nType>=0) pscho->m_ulType = (unsigned long) nType;
							if(nComparisonType>=0) pscho->m_usComparisonType = (unsigned short) nComparisonType;
							if(nDestinationType>=0) pscho->m_ulDestinationType = (unsigned long) nDestinationType;
							if(nSearchType>=0) pscho->m_usSearchType = (unsigned short) nSearchType;
							if(nActionType>=0) pscho->m_usActionType = (unsigned short) nActionType;
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "adding rule %d, nActionType is %d", nMappingID, nComparisonType);  Sleep(100); //(Dispatch message)

							if(szField.GetLength()>0)
							{
								if(pscho->m_pszFieldName) free(pscho->m_pszFieldName);
								pscho->m_pszFieldName = (char*)malloc(szField.GetLength()+1); 
								if(pscho->m_pszFieldName) sprintf(pscho->m_pszFieldName, szField);
							}

//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "adding rule %d, szField is %s", nMappingID, szField);  Sleep(100); //(Dispatch message)
							if(szParam.GetLength()>0)
							{
								if(pscho->m_pszParamName) free(pscho->m_pszParamName);
								pscho->m_pszParamName = (char*)malloc(szParam.GetLength()+1); 
								if(pscho->m_pszParamName) sprintf(pscho->m_pszParamName, szParam);
							}

//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "adding rule %d, szParam is %s", nMappingID, szParam);  Sleep(100); //(Dispatch message)
							if(szCrtierion.GetLength()>0)
							{
								if(pscho->m_pszCriterion) free(pscho->m_pszCriterion);
								pscho->m_pszCriterion = (char*)malloc(szCrtierion.GetLength()+1); 
								if(pscho->m_pszCriterion) sprintf(pscho->m_pszCriterion, szCrtierion);
							}

//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "adding rule %d, szCrtierion is %s", nMappingID, szCrtierion);  Sleep(100); //(Dispatch message)
							if(	szLocation.GetLength()>0)
							{
								if(pscho->m_pszEventLocation) free(pscho->m_pszEventLocation);
								pscho->m_pszEventLocation = (char*)malloc(szLocation.GetLength()+1); 
								if(pscho->m_pszEventLocation) sprintf(pscho->m_pszEventLocation, szLocation);
							}

//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "adding rule %d, szLocation is %s", nMappingID, szLocation);  Sleep(100); //(Dispatch message)
							pscho->m_ulFlags = PROMOLITE_FLAG_FOUND;


						}
						else
							delete pscho;
					}
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "removing non found rules");  Sleep(100); //(Dispatch message)
			LeaveCriticalSection(&m_critSQL);

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<m_nNumMappingObjects)
			{
				if((m_ppMappingObj)&&(m_ppMappingObj[nIndex]))
				{
					if((m_ppMappingObj[nIndex]->m_ulFlags)&PROMOLITE_FLAG_FOUND)
					{
						(m_ppMappingObj[nIndex]->m_ulFlags) &= ~PROMOLITE_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(m_ppMappingObj[nIndex])
						{
							delete m_ppMappingObj[nIndex];
							m_nNumMappingObjects--;

							int nTemp=nIndex;
							while(nTemp<m_nNumMappingObjects)
							{
								m_ppMappingObj[nTemp]=m_ppMappingObj[nTemp+1];
								nTemp++;
							}
							m_ppMappingObj[nTemp] = NULL;
						} else nIndex++;
					}
				}
				else
					nIndex++;
			}

			return nReturn;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return PROMOLITE_ERROR;
}

int CPromoLiteData::GetParameterQueryIndex(CString name)
{
	int i=0;
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "m_nNumParameterRuleQueries = %d, %s array", m_nNumParameterRuleQueries, m_ppParameterRuleQuery?"not null":"NULL" ); // Sleep(50);//(Dispatch message)
	while((i<m_nNumParameterRuleQueries)&&(m_ppParameterRuleQuery))
	{
		if(m_ppParameterRuleQuery[i])
		{
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Checking %s [%d] against %s", m_ppParameterRuleQuery[i]->sz_name, 1, name ); // Sleep(50);//(Dispatch message)
			if(m_ppParameterRuleQuery[i]->sz_name.Compare(name)==0)
			{
				return i;
			}

		}
		i++;
	}

	return -1;
}


int CPromoLiteData::ApplyMapping(int nRuleIndex, char* pszData, char** ppszEventName, char** ppszExplicitExtension)
{
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "** Applying rule %d to %s", nRuleIndex, pszData);  Sleep(5);//(Dispatch message)
	if((nRuleIndex>=0)&&(m_ppMappingObj)&&(nRuleIndex<m_nNumMappingObjects)&&(pszData)&&(strlen(pszData)>0))
	{
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "***  applying rule %d to %s", nRuleIndex, pszData);  Sleep(5);//(Dispatch message)
		if((m_ppMappingObj[nRuleIndex])&&(m_ppMappingObj[nRuleIndex]->m_nMappingID>0))
		{
/*
	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned long m_ulType;
	unsigned short m_usComparisonType;
	unsigned long m_ulDestinationType;
	unsigned short m_usSearchType;
	unsigned short m_usActionType;
	int m_nMappingID;

	char* m_pszFieldName;
	char* m_pszParamName;
	char* m_pszCriterion;
	char* m_pszEventLocation;
*/

/*
create table Rules (rule_type tinyint, field_name varchar(32), param_name varchar(32), comparison varchar(10), criterion varchar(64), event_location varchar(32), dest_type int, search_type tinyint, action tinyint, ruleid int IDENTITY (1, 1) NOT NULL);

------------------------------------
rule_type: databased in PromoLite.RuleTypes (ruletype, name)

values:
1 - Presmaster
2 - XML
3 - Imagestore

------------------------------------
comparison: stored in PromoLite\util.asp function name: OptionListComparison

values:
<=
>=
==
!=

------------------------------------
dest_type: databased in Prospero.LU_Destination_Types

values:
1000 - DVG
1001 - HDVG
2001 - Imagestore 2
2002 - Intuition
2003 - Imagestore 300
2004 - Imagestore HD

------------------------------------
search_type: stored in PromoLite\util.asp function name: OptionListSearchTypes

values:
0 - Explicit
1 - Audio
2 - Video
3 - Audio & Video

------------------------------------
action: stored in PromoLite\util.asp function name: OptionListActionTypes

values:
0 - Normal
1 - NULL OXT

*/
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "before applying rule %d with dest_type = %d to %s", nRuleIndex, m_ppMappingObj[nRuleIndex]->m_ulDestinationType, pszData);  Sleep(50);//(Dispatch message)

			switch(m_ppMappingObj[nRuleIndex]->m_ulType)
			{
			default:
			case PROMOLITE_RULE_TYPE_UNKNOWN://					0x00000000  // type unknown
				{
					return PROMOLITE_ERROR;
				} break;
			case PROMOLITE_RULE_TYPE_PRESMASTER://				0x00000001  // for Helios, with presmaster data in <data> XML field
				{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "presmaster rule");  Sleep(50);//(Dispatch message)
					char pszFieldName[MAX_PATH];
					char pszEndFieldName[MAX_PATH];
					if(
						  (m_ppMappingObj[nRuleIndex]->m_pszFieldName)
						&&(strlen(m_ppMappingObj[nRuleIndex]->m_pszFieldName)>0)
						)
					{
						sprintf(pszFieldName,"<%s>", m_ppMappingObj[nRuleIndex]->m_pszFieldName);
						sprintf(pszEndFieldName,"</%s>", m_ppMappingObj[nRuleIndex]->m_pszFieldName);
					}
					else
					{
						strcpy(pszFieldName, "<data>");
						strcpy(pszEndFieldName, "</data>");
					}
					if(
						  (m_ppMappingObj[nRuleIndex]->m_pszParamName)
						&&(strlen(m_ppMappingObj[nRuleIndex]->m_pszParamName)>0)
						)
					{
						char* pchBegin = strstr(pszData, pszFieldName);
						bool bParsed=false;
						while((pchBegin)&&(!bParsed))
						{
							pchBegin += strlen(pszFieldName);
							char* pchEnd = strstr(pchBegin, pszEndFieldName);
							if(pchEnd)
							{
								// search within the data to find the param name.
								// presmaster protocol is like:

								// EventName|Type:Video|Key:1
								int  nKeyer = 0;
								CSafeBufferUtil sbu;
								char* pchEvent = NULL;
								char pszSafeEvent[MAX_PATH];
								memset(pszSafeEvent, 0, MAX_PATH);

								if((pchEnd-pchBegin)>0) pchEvent = sbu.Token(pchBegin, pchEnd-pchBegin, "|", MODE_SINGLEDELIM);
								if(pchEvent)
								{
									if(strlen(pchEvent))
									{
										strcpy(pszSafeEvent, pchEvent);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "presmaster rule with %s", pchEvent);  Sleep(50);//(Dispatch message)
										// have a valid event name, so can continue.
										char* pch = NULL;
										while(pch==NULL)
										{
											char* pchKeyer = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM); 
											if(pchKeyer)
											{
												char pszParamName[MAX_PATH];

												strupr(pchKeyer);
												sprintf(pszParamName, "%s:", m_ppMappingObj[nRuleIndex]->m_pszParamName);
												strupr(pszParamName);
												pch = strstr(pchKeyer, pszParamName);
												if(pch)
												{
													pch+=strlen(pszParamName);
													nKeyer = atoi(pch);
													bParsed = true;
													break;
												}
											}
											else break;
										}
									}
								}
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "presmaster rule with %s parsed %d", pszSafeEvent, bParsed);  Sleep(50);//(Dispatch message)

								if(bParsed)
								{
									char* pchCrit = m_ppMappingObj[nRuleIndex]->m_pszCriterion;
									int nTestPassed = 0;
									if((pchCrit)&&(strlen(pchCrit)>0))
									{
										if(((*pchCrit)>47)&&((*pchCrit)<58)) // its all numerical
										{
											int nValue = nKeyer;
											int nCrit  = atoi(pchCrit);
		//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse04: %d, %d", nValue, nCrit);  Sleep(50);//(Dispatch message)

				
											if(
													( (m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_EQUALS)
													&&(nValue==nCrit) )
												||( (m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_NOTEQUAL)
													&&(nValue!=nCrit) )
												||( (m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_GTOE)
													&&(nValue>=nCrit) )
												||( (m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_LTOE)
													&&(nValue<=nCrit) )
												) 
											{
												nTestPassed=1;
											}
											//	nTestPassed=0;
		//									g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse05: %s, %d%s%d", nTestPassed>0?"passed":"NOT passed",nValue, m_ppMappingObj[nRuleIndex]->m_usComparisonType==PROMOLITE_RULE_COMPARE_GTOE?">=":"<=",nCrit);  Sleep(50);//(Dispatch message)
										} // if numerical
									}  // if criterion has a length
			//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse05a: %s", pch);  Sleep(50);//(Dispatch message)
			
									if(nTestPassed>0)
									{
		//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse06a: %s", pch);  Sleep(50);//(Dispatch message)
										if((ppszEventName==NULL)&&(ppszExplicitExtension==NULL))
										{
											return PROMOLITE_SUCCESS;
										}
									
										// if were here, we have to parse.
										char* pchFileName = (char*)malloc(strlen(pszSafeEvent)+1);
										if(pchFileName)
										{
											sprintf(pchFileName, "%s", pszSafeEvent);
										// pchFileName should now hold the event name
											if(m_ppMappingObj[nRuleIndex]->m_usSearchType != PROMOLITE_RULE_SEARCH_EXPLICIT)
											{
											// now check for an extension, strip it if not explicit.
												bool bDot = true;
												char* pch = strrchr(pchFileName, '.');
												if(pch) *pch = 0; // null term it!
												else bDot= false;

												if(strlen(pchFileName)>0)
												{
													if(ppszEventName)
													{
	//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse08: ppszEventName %d", ppszEventName);  Sleep(50);//(Dispatch message)
														*ppszEventName = pchFileName;
	//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse08: %s final", *ppszEventName);  Sleep(50);//(Dispatch message)
													}
													// else free(pchReturn);
															
													if(ppszExplicitExtension)
													{
														if(bDot)
														{
															pch++;
															if(strlen(pch)>0)
															{																
																char* pchReturnExt = (char*)malloc(strlen(pch)+1);
																if(pchReturnExt)
																{
																	strcpy(pchReturnExt, pch);
																	*ppszExplicitExtension = pchReturnExt;
																}	else *ppszExplicitExtension = NULL;
															}	else *ppszExplicitExtension = NULL;
														} else *ppszExplicitExtension = NULL;
													}

													if(ppszEventName == NULL) free(pchFileName);
	//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse08: %s final", *ppszEventName);  Sleep(50);//(Dispatch message)

													return PROMOLITE_SUCCESS;
												}
												else // strlen pchFileName
												{
													free(pchFileName);
													if(ppszEventName)
													{
														*ppszEventName = NULL;
													}
													
													if(ppszExplicitExtension)
													{
														if(bDot)
														{
															*ppszExplicitExtension = NULL;  // no filename, so just fail out.
															/*
															pch++;
															if(strlen(pch)>0)
															{
																char* pchExtReturn = (char*)malloc(strlen(pch)+1);
																if(pchExtReturn)
																{
																	strcpy(pchExtReturn, pch);
																	*ppszExplicitExtension = pchExtReturn;
																} else *ppszExplicitExtension = NULL;
															} else *ppszExplicitExtension = NULL;
															*/
														} else *ppszExplicitExtension = NULL;
													}
												}  // strlen return was 0
											}
											else  //PROMOLITE_RULE_SEARCH_EXPLICIT
											{
	//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse31: file %s", pchReturn);  Sleep(50);//(Dispatch message)
	//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse32: ppszEventName %d", ppszEventName);  Sleep(50);//(Dispatch message)
												if(ppszEventName)
												{
													*ppszEventName = pchFileName;
	//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse33: final %s", *ppszEventName);  Sleep(50);//(Dispatch message)
												}

												if(ppszExplicitExtension)
												{
												// now check for an extension, strip it if not explicit.
													bool bDot = true;
													char* pch = strrchr(pchFileName, '.');
													if(pch) *pch = 0; // null term it!
													else bDot= false;
													if(bDot)
													{
														pch++;
														if(strlen(pch)>0)
														{																
															char* pchReturnExt = (char*)malloc(strlen(pch)+1);
															if(pchReturnExt)
															{
																strcpy(pchReturnExt, pch);
																*ppszExplicitExtension = pchReturnExt;
															}	else *ppszExplicitExtension = NULL;
														}	else *ppszExplicitExtension = NULL;
													} else *ppszExplicitExtension = NULL;
												}
													
												if(ppszEventName==NULL) free(pchFileName);

												return PROMOLITE_SUCCESS;
											}//PROMOLITE_RULE_SEARCH_EXPLICIT
										}
			//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse47: exiting with %s", pch);  Sleep(50);//(Dispatch message)
									}//if(nTestPassed>0)
								}//parsed
							}// else no end tag, was malformed data
							pchBegin = strstr(pchBegin, pszFieldName);  // try again.
						}// while begin tag, (else non valid data)
					}// else no valid param
				} break; 
			case PROMOLITE_RULE_TYPE_XML://							0x00000002  // for Helios, with logo data in <logos> XML sub-fields .. free form tho
				{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "xml rule");  Sleep(50);//(Dispatch message)

					if(
						  (m_ppMappingObj[nRuleIndex]->m_pszParamName)
						&&(strlen(m_ppMappingObj[nRuleIndex]->m_pszParamName)>0)
						&&(m_ppMappingObj[nRuleIndex]->m_pszEventLocation)
						&&(strlen(m_ppMappingObj[nRuleIndex]->m_pszEventLocation)>0)
						&&(m_ppMappingObj[nRuleIndex]->m_pszFieldName)
						&&(strlen(m_ppMappingObj[nRuleIndex]->m_pszFieldName)>0)
						)
					{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "xml rule really");  Sleep(50);//(Dispatch message)
						char pszFieldName[MAX_PATH];
						char pszEndFieldName[MAX_PATH];
						sprintf(pszFieldName,"<%s>", m_ppMappingObj[nRuleIndex]->m_pszFieldName);
						sprintf(pszEndFieldName,"</%s>", m_ppMappingObj[nRuleIndex]->m_pszFieldName);
						char* pchBegin = strstr(pszData, pszFieldName);
						bool bParsed=false;
						while((pchBegin)&&(!bParsed))
						{
							pchBegin += strlen(pszFieldName);
							char* pchEnd = strstr(pchBegin, pszEndFieldName);
							if(pchEnd)
							{
								// search within the data to find the param name.

								// looking for <logo6><field>1</field><data>EventName</data></logo6>
								// if field = 0,  no logo
								char pszParamName[MAX_PATH];
								char pszEndParamName[MAX_PATH];
								sprintf(pszParamName,"<%s>", m_ppMappingObj[nRuleIndex]->m_pszParamName);
								sprintf(pszEndParamName,"</%s>", m_ppMappingObj[nRuleIndex]->m_pszParamName);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "xml rule %s and %s", pszParamName,pszEndParamName);  Sleep(50);//(Dispatch message)

								char* pchBeginParam = strstr(pchBegin, pszParamName);
								if(pchBeginParam) pchBeginParam += strlen(pszParamName);
								char* pchEndParam = strstr(pchBeginParam, pszEndParamName);

								if((pchBeginParam)&&(pchEndParam)&&(pchBeginParam<=pchEndParam)&&(pchEndParam<pchEnd)) // <= because can be blank
								{
									bParsed = true;
									char pszFieldContents[MAX_PATH];
									memset(pszFieldContents,0,MAX_PATH);
									memcpy(pszFieldContents, pchBeginParam, pchEndParam-pchBeginParam);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "xml rule %s and %s contents = %s", pszParamName,pszEndParamName, pszFieldContents);  Sleep(50);//(Dispatch message)
									char* pchCrit = m_ppMappingObj[nRuleIndex]->m_pszCriterion;
									int nTestPassed = 0;
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "xml rule %s and %s contents: %s ?= %s", pszParamName,pszEndParamName, pszFieldContents,pchCrit);  Sleep(50);//(Dispatch message)

									if((pchCrit)&&(strlen(pchCrit)>0))
									{
										if(
												( (m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_EQUALS)
												&&(strcmp(pchCrit, pszFieldContents)==0) )
											||( (m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_NOTEQUAL)
												&&(strcmp(pchCrit, pszFieldContents)!=0) )
											||( (m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_GTOE)
												&&(strcmp(pchCrit, pszFieldContents)>=0) )
											||( (m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_LTOE)
												&&(strcmp(pchCrit, pszFieldContents)<=0) )
											) 
										{
											nTestPassed=1;
										}
									}  // if criterion has a length
									else
									{
										//zero length;
										if(strlen(pszFieldContents)<=0)
										{
											if(m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_EQUALS)
												nTestPassed=1;
										}
										else
										{
											if(m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_NOTEQUAL)
												nTestPassed=1;
										}
									}
									if(nTestPassed>0)
									{
										// have to check valid event name
										if((ppszEventName==NULL)&&(ppszExplicitExtension==NULL))
										{
											return PROMOLITE_SUCCESS;
										}

										sprintf(pszParamName,"<%s>", m_ppMappingObj[nRuleIndex]->m_pszEventLocation);
										sprintf(pszEndParamName,"</%s>", m_ppMappingObj[nRuleIndex]->m_pszEventLocation);

										char* pchBeginParam = strstr(pchBegin, pszParamName);
										if(pchBeginParam) pchBeginParam += strlen(pszParamName);
										char* pchEndParam = strstr(pchBeginParam, pszEndParamName);

										char* pchFileName = NULL;

										if((pchBeginParam)&&(pchEndParam)&&(pchBeginParam<pchEndParam)&&(pchEndParam<pchEnd))  // only < because need a valid event name
										{
											char pszFieldContents[MAX_PATH];
											memset(pszFieldContents,0,MAX_PATH);
											memcpy(pszFieldContents, pchBeginParam, pchEndParam-pchBeginParam);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "xml rule %s and %s contents: %s ?= %s", pszParamName,pszEndParamName, pszFieldContents,pchCrit);  Sleep(50);//(Dispatch message)
											if(strlen(pszFieldContents)>0)
											{
												pchFileName = (char*)malloc(strlen(pszFieldContents)+1);
												if(pchFileName)
												{
													sprintf(pchFileName, "%s", pszFieldContents);
												}
											}
										}

										// if were here, we have to parse.
										if(pchFileName)
										{
											if(strlen(pchFileName)>0)
											{
											// pchFileName should now hold the event name
												if(m_ppMappingObj[nRuleIndex]->m_usSearchType != PROMOLITE_RULE_SEARCH_EXPLICIT)
												{
												// now check for an extension, strip it if not explicit.
													bool bDot = true;
													char* pch = strrchr(pchFileName, '.');
													if(pch) *pch = 0; // null term it!
													else bDot= false;

													if(strlen(pchFileName)>0)
													{
														if(ppszEventName)
														{
		//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse08: ppszEventName %d", ppszEventName);  Sleep(50);//(Dispatch message)
															*ppszEventName = pchFileName;
		//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse08: %s final", *ppszEventName);  Sleep(50);//(Dispatch message)
														}
														// else free(pchReturn);
														
														if(ppszExplicitExtension)
														{
															if(bDot)
															{
																pch++;
																if(strlen(pch)>0)
																{																
																	char* pchReturnExt = (char*)malloc(strlen(pch)+1);
																	if(pchReturnExt)
																	{
																		strcpy(pchReturnExt, pch);
																		*ppszExplicitExtension = pchReturnExt;
																	}	else *ppszExplicitExtension = NULL;
																}	else *ppszExplicitExtension = NULL;
															} else *ppszExplicitExtension = NULL;
														}

														if(ppszEventName == NULL) free(pchFileName);
		//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse08: %s final", *ppszEventName);  Sleep(50);//(Dispatch message)

														return PROMOLITE_SUCCESS;
													}
													else // strlen pchFileName
													{
														free(pchFileName);
														if(ppszEventName)
														{
															*ppszEventName = NULL;
														}
														
														if(ppszExplicitExtension)
														{
															*ppszExplicitExtension = NULL;  // no filename, so just fail out.
															/*
															if(bDot)
															{
																pch++;
																if(strlen(pch)>0)
																{
																	char* pchExtReturn = (char*)malloc(strlen(pch)+1);
																	if(pchExtReturn)
																	{
																		strcpy(pchExtReturn, pch);
																		*ppszExplicitExtension = pchExtReturn;
																	} else *ppszExplicitExtension = NULL;
																} else *ppszExplicitExtension = NULL;
															} else *ppszExplicitExtension = NULL;
															*/
														}
													}  // strlen return was 0
												}
												else  //PROMOLITE_RULE_SEARCH_EXPLICIT
												{
		//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse31: file %s", pchReturn);  Sleep(50);//(Dispatch message)
		//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse32: ppszEventName %d", ppszEventName);  Sleep(50);//(Dispatch message)
													if(ppszEventName)
													{
														*ppszEventName = pchFileName;
		//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse33: final %s", *ppszEventName);  Sleep(50);//(Dispatch message)
													}

													if(ppszExplicitExtension)
													{
													// now check for an extension, strip it if not explicit.
														bool bDot = true;
														char* pch = strrchr(pchFileName, '.');
														if(pch) *pch = 0; // null term it!
														else bDot= false;
														if(bDot)
														{
															pch++;
															if(strlen(pch)>0)
															{																
																char* pchReturnExt = (char*)malloc(strlen(pch)+1);
																if(pchReturnExt)
																{
																	strcpy(pchReturnExt, pch);
																	*ppszExplicitExtension = pchReturnExt;
																}	else *ppszExplicitExtension = NULL;
															}	else *ppszExplicitExtension = NULL;
														} else *ppszExplicitExtension = NULL;
													}
														
													if(ppszEventName==NULL) free(pchFileName);

													return PROMOLITE_SUCCESS;
												}//PROMOLITE_RULE_SEARCH_EXPLICIT
											}
										}
									}
								}
							}// else no end tag, was malformed data
							pchBegin = strstr(pchBegin, pszFieldName);  // try again.
						}// while begin tag, (else non valid data)
					}// else no valid param				
				} break;
			case PROMOLITE_RULE_TYPE_IMAGESTORE://				0x00000003  // for Sentinel, without presmaster, data in title field
				{
					// Field name is in Title, so assume that is what is passed in
					if(
						  (m_ppMappingObj[nRuleIndex]->m_pszParamName)
						&&(strlen(m_ppMappingObj[nRuleIndex]->m_pszParamName)>0)
						)
					{
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "applying rule %d to %s", nRuleIndex, pszData);  Sleep(50);//(Dispatch message)
						// the protocol is: we are looking for, in the string:
						// LOAD:n filename
						// LOADI:n filename
						// where n is a keyer layer number, followed by whitespace, followed by event name which may or may not have an extension

						char* pch = NULL;
						char* pchParam = (char*) malloc(strlen(m_ppMappingObj[nRuleIndex]->m_pszParamName)+2);
						if(pchParam)
						{
							sprintf(pchParam, "%s:",m_ppMappingObj[nRuleIndex]->m_pszParamName );
							pch = strstr(pszData, pchParam);
							free(pchParam);
						}
						 
						char* pchCrit = m_ppMappingObj[nRuleIndex]->m_pszCriterion;
						int nTestPassed = 0;
						if((pch)&&(pch==pszData)) // must be at beginning
						{
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse00: %s, param %s, criterion = %s", pch, m_ppMappingObj[nRuleIndex]->m_pszParamName, pchCrit);  Sleep(50);//(Dispatch message)

							pch += (strlen(m_ppMappingObj[nRuleIndex]->m_pszParamName)+1); 
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse01: %s", pch);  Sleep(50);//(Dispatch message)

//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse02a: %s", pch);  Sleep(50);//(Dispatch message)

							if((pchCrit)&&(strlen(pchCrit)>0))
							{
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse03: %s", pchCrit);  Sleep(50);//(Dispatch message)
								if(((*pch)>47)&&((*pch)<58)&&((*pchCrit)>47)&&((*pchCrit)<58)) // its all numerical
								{
									int nValue = atoi(pch);
									int nCrit  = atoi(pchCrit);
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse04: %d, %d", nValue, nCrit);  Sleep(50);//(Dispatch message)

	
									if(
										  ( (m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_EQUALS)
											&&(nValue==nCrit) )
										||( (m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_NOTEQUAL)
											&&(nValue!=nCrit) )
										||( (m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_GTOE)
											&&(nValue>=nCrit) )
										||( (m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_LTOE)
											&&(nValue<=nCrit) )
										) 
									{
										nTestPassed=1;
									}
									//	nTestPassed=0;

/*
									switch(m_ppMappingObj[nRuleIndex]->m_usComparisonType)
									{
									case PROMOLITE_RULE_COMPARE_EQUALS://				0x0001  // == equals
										{
											if(nValue==nCrit) bTestPassed=true;
										} break;
									case PROMOLITE_RULE_COMPARE_NOTEQUAL://			0x0002  // != equals
										{
											if(nValue!=nCrit) bTestPassed=true;
										} break;
									case PROMOLITE_RULE_COMPARE_GTOE://					0x0003  // >= greater than or equal to
										{
											if(nValue>=nCrit) bTestPassed=true;
										} break;
									case PROMOLITE_RULE_COMPARE_LTOE://					0x0004  // <= less than or equal to
										{
											if(nValue<=nCrit) bTestPassed=true;
										} break;
									default:
									case PROMOLITE_RULE_COMPARE_UNKNOWN://				0xffff  // type unknown
										{
										}	break;
									}  // switch
*/
//									g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse05: %s, %d%s%d", nTestPassed>0?"passed":"NOT passed",nValue, m_ppMappingObj[nRuleIndex]->m_usComparisonType==PROMOLITE_RULE_COMPARE_GTOE?">=":"<=",nCrit);  Sleep(50);//(Dispatch message)
								} // if numerical
							}  // if criterion has a length
							else
							{
								//zero length;
								if(((*pch)==0)||(isspace(*pch)))
								{
									if(m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_EQUALS)
										nTestPassed=1;
								}
								else
								{
									if(m_ppMappingObj[nRuleIndex]->m_usComparisonType == PROMOLITE_RULE_COMPARE_NOTEQUAL)
										nTestPassed=1;
								}
							}

//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse05a: %s", pch);  Sleep(50);//(Dispatch message)

							if(nTestPassed>0)
							{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse06a: %s", pch);  Sleep(50);//(Dispatch message)
								if((ppszEventName==NULL)&&(ppszExplicitExtension==NULL))
								{
									return PROMOLITE_SUCCESS;
								}

								// if were here, we have to parse.
								char* pchFileName = (char*)malloc(strlen(pch)+1);
								if(pchFileName)
								{
									sprintf(pchFileName, "%s", pch+1);

									while(( pchFileName[0]!=0)&&(isspace(pchFileName[0])) )
									{
										int nLen = strlen(pchFileName);
										int t=0;
										while(t<nLen)
										{
											pchFileName[t]=pchFileName[t+1]; // should get the term 0 too!
											t++;
										}
									}
									if(strlen(pchFileName)>0)
									{
									// pchFileName should now hold the event name
										if(m_ppMappingObj[nRuleIndex]->m_usSearchType != PROMOLITE_RULE_SEARCH_EXPLICIT)
										{
										// now check for an extension, strip it if not explicit.
											bool bDot = true;
											pch = strrchr(pchFileName, '.');
											if(pch) *pch = 0; // null term it!
											else bDot= false;

											if(strlen(pchFileName)>0)
											{
												if(ppszEventName)
												{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse08: ppszEventName %d", ppszEventName);  Sleep(50);//(Dispatch message)
													*ppszEventName = pchFileName;
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse08: %s final", *ppszEventName);  Sleep(50);//(Dispatch message)
												}
												// else free(pchReturn);
												
												if(ppszExplicitExtension)
												{
													if(bDot)
													{
														pch++;
														if(strlen(pch)>0)
														{																
															char* pchReturnExt = (char*)malloc(strlen(pch)+1);
															if(pchReturnExt)
															{
																strcpy(pchReturnExt, pch);
																*ppszExplicitExtension = pchReturnExt;
															}	else *ppszExplicitExtension = NULL;
														}	else *ppszExplicitExtension = NULL;
													} else *ppszExplicitExtension = NULL;
												}

												if(ppszEventName == NULL) free(pchFileName);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse08: %s final", *ppszEventName);  Sleep(50);//(Dispatch message)

												return PROMOLITE_SUCCESS;
											}
											else // strlen pchFileName
											{
												free(pchFileName);
												if(ppszEventName)
												{
													*ppszEventName = NULL;
												}
												
												if(ppszExplicitExtension)
												{
													*ppszExplicitExtension = NULL;  // no filename, so just fail out.
													/*
													if(bDot)
													{
														pch++;
														if(strlen(pch)>0)
														{
															char* pchExtReturn = (char*)malloc(strlen(pch)+1);
															if(pchExtReturn)
															{
																strcpy(pchExtReturn, pch);
																*ppszExplicitExtension = pchExtReturn;
															} else *ppszExplicitExtension = NULL;
														} else *ppszExplicitExtension = NULL;
													} else *ppszExplicitExtension = NULL;
													*/
												}
											}  // strlen return was 0
										}
										else  //PROMOLITE_RULE_SEARCH_EXPLICIT
										{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse31: file %s", pchReturn);  Sleep(50);//(Dispatch message)
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse32: ppszEventName %d", ppszEventName);  Sleep(50);//(Dispatch message)
											if(ppszEventName)
											{
												*ppszEventName = pchFileName;
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse33: final %s", *ppszEventName);  Sleep(50);//(Dispatch message)
											}

											if(ppszExplicitExtension)
											{
											// now check for an extension, strip it if not explicit.
												bool bDot = true;
												pch = strrchr(pchFileName, '.');
												if(pch) *pch = 0; // null term it!
												else bDot= false;
												if(bDot)
												{
													pch++;
													if(strlen(pch)>0)
													{																
														char* pchReturnExt = (char*)malloc(strlen(pch)+1);
														if(pchReturnExt)
														{
															strcpy(pchReturnExt, pch);
															*ppszExplicitExtension = pchReturnExt;
														}	else *ppszExplicitExtension = NULL;
													}	else *ppszExplicitExtension = NULL;
												} else *ppszExplicitExtension = NULL;
											}
												
											if(ppszEventName==NULL) free(pchFileName);

											return PROMOLITE_SUCCESS;
										}//PROMOLITE_RULE_SEARCH_EXPLICIT
									}
								}
							}
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse47: exiting with %s", pch);  Sleep(50);//(Dispatch message)

						}
					}
				} break;
			}
		}
	}
//	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "parse48: returning");  Sleep(50);//(Dispatch message)
	return PROMOLITE_ERROR;
}

int CPromoLiteData::CheckMapping(char* pszData, unsigned short usAutomationType)  // just makes sure SOME rule applies.
{
	if((pszData)&&(strlen(pszData)>0)&&(m_ppMappingObj)&&(m_nNumMappingObjects>0))
	{
		int nRuleIndex = 0;
		while(nRuleIndex < m_nNumMappingObjects)
		{
			if(m_ppMappingObj[nRuleIndex])
			{
				if(
						(
							(usAutomationType == PROMOLITE_DEP_AUTO_SENTINEL)
						&&(m_ppMappingObj[nRuleIndex]->m_ulType == PROMOLITE_RULE_TYPE_IMAGESTORE)
						)
					||(
							(usAutomationType == PROMOLITE_DEP_AUTO_HELIOS)
						&&(
								(m_ppMappingObj[nRuleIndex]->m_ulType == PROMOLITE_RULE_TYPE_PRESMASTER)
							||(m_ppMappingObj[nRuleIndex]->m_ulType == PROMOLITE_RULE_TYPE_XML)
							)
						)
					)
				{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Rule %d checking", nRuleIndex);  Sleep(50);//(Dispatch message)

					if(ApplyMapping(nRuleIndex, pszData) >= PROMOLITE_SUCCESS) return PROMOLITE_SUCCESS;
				}
			}
			nRuleIndex++;
		}
	}
	return PROMOLITE_ERROR;
}

int	CPromoLiteData::GetRuleIndex(int nMappingID)
{
	if((m_ppMappingObj)&&(m_nNumMappingObjects>0))
	{
		int nRuleIndex = 0;
		while(nRuleIndex<m_nNumMappingObjects)
		{
			if(m_ppMappingObj[nRuleIndex])
			{
				if(m_ppMappingObj[nRuleIndex]->m_nMappingID == nMappingID) return nRuleIndex;
			}
			nRuleIndex++;
		}
	}
	return PROMOLITE_ERROR;
}

int CPromoLiteData::ReleaseRecordSet(bool bResetIncrementor)
{
	if(m_prsEvents)  // it may already be finished out and closed
	{
		m_prsEvents->Close();
		delete m_prsEvents;
		m_prsEvents = NULL;
	}
	if (bResetIncrementor) m_nEventCheckIndex = -1;
	return PROMOLITE_SUCCESS;
}

int CPromoLiteData::ReturnNumberOfAnalysisRecords(char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.
	if((m_pdb)&&(m_pdbConn))
	{
		char errorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT COUNT(1) AS totalcount FROM %s.dbo.%s",
				g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite",			// the Default DB name
				g_ppromolite->m_settings.m_pszTriggerAnalysisView?g_ppromolite->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView"   // the Analysis table name
			);
		
//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

		int nNumFound = 0;
		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
					CString szTemp;
					int nTemp=0;
					try
					{
						// get id!
						prs->GetFieldValue("totalcount", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							nTemp = atoi(szTemp);
						}
					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
							if(m_pmsgr)
							{
								char errorstring[DB_ERRORSTRING_LEN];
								_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
								Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
							}
*/
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							// The error code is in e->m_nRetCode
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
						}
						else 
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
						}
						e->Delete();
					} 
					catch( ... )
					{
					}

					prs->Close();
					delete prs;
					prs = NULL;

					if(nTemp>=0)
					{
						LeaveCriticalSection(&m_critSQL);
						return nTemp;
					}

				// prs->MoveNext();
			} //if(!prs->IsEOF())
			if(prs)
			{
				prs->Close();
				delete prs;
			}	
		}
		else  //else prs null
		{
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
		}
		LeaveCriticalSection(&m_critSQL);

	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}

	return -1;
}



int CPromoLiteData::ReturnModuleIndex(CPromoLiteEventObject* pObj, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.

	if(pObj)
	{
		int nDestModule=0;

		while(nDestModule<g_ppromolite->m_settings.m_nNumEndpointsInstalled)
		{
			if((g_ppromolite->m_settings.m_ppEndpointObject)&&(g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]))
			{
				if(g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_usType&PROMOLITE_DEP_EDGE_MASK)
				{
					if(pObj->m_sz_vc64_module_dbname.CompareNoCase(g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName)==0)
					{
						// this is it.
						return nDestModule;
						break;
					}
				}
			}
			nDestModule++;
		}
	}
	return -1;
}

DiskSpaceObject_t*  CPromoLiteData::ReturnDestinationDiskSpace(CPromoLiteEventObject* pObj, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.

	int nDestModule=ReturnModuleIndex(pObj, pszInfo);

	if(
		  (pObj)&&(m_pdb)&&(m_pdbConn)
			&&(nDestModule>=0)
			&&(g_ppromolite->m_settings.m_nNumEndpointsInstalled>0)
			&&(nDestModule<g_ppromolite->m_settings.m_nNumEndpointsInstalled)
			&&(g_ppromolite->m_settings.m_ppEndpointObject)
			&&(g_ppromolite->m_settings.m_ppEndpointObject[nDestModule])
		)
	{
//		ReleaseRecordSet(); // have to release so we can get new results.
		char errorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		CString szTemp = "Destinations";  // we didnt't find it. just use the default name
		if(nDestModule>=0) szTemp = g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination?g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination:"Destinations";

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT diskspace_free, diskspace_total, diskspace_threshold, (1.00 - cast((diskspace_free/case when diskspace_total = 0 then 1 else diskspace_total end) AS decimal(3,2))) * 100 AS percentutil FROM %s.dbo.%s WHERE destinationid = %s",
				pObj->m_sz_vc64_module_dbname,			// the edge device DB name
				szTemp,
				pObj->m_sz_n_destinationid
			);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ReturnDestinationDiskSpace SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

		int nNumFound = 0;
		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				DiskSpaceObject_t* pDiskSpaceReturn = new DiskSpaceObject_t;
				if(pDiskSpaceReturn)
				{
					pDiskSpaceReturn->dblDiskFree=-1;
					pDiskSpaceReturn->dblDiskTotal=-1;
					pDiskSpaceReturn->dblDiskThreshold=-1;
					pDiskSpaceReturn->dblDiskPercentUtilized=-1;

					double dblTemp;
					try
					{
						// get id!
						prs->GetFieldValue("diskspace_free", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							dblTemp = atof(szTemp);
							if(dblTemp>=0) pDiskSpaceReturn->dblDiskFree=dblTemp;
						}

						prs->GetFieldValue("diskspace_total", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							dblTemp = atof(szTemp);
							if(dblTemp>=0) pDiskSpaceReturn->dblDiskTotal=dblTemp;
						}

						prs->GetFieldValue("diskspace_threshold", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							dblTemp = atof(szTemp);
							if(dblTemp>=0) pDiskSpaceReturn->dblDiskThreshold=dblTemp;
						}

						prs->GetFieldValue("percentutil", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							dblTemp = atof(szTemp);
							if(dblTemp>=0) pDiskSpaceReturn->dblDiskPercentUtilized=dblTemp;
						}
					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
							if(m_pmsgr)
							{
								char errorstring[DB_ERRORSTRING_LEN];
								_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
								Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
							}
*/
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							// The error code is in e->m_nRetCode
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
						}
						else 
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
						}
						e->Delete();
					} 
					catch( ... )
					{
					}

					prs->Close();
					delete prs;
					LeaveCriticalSection(&m_critSQL);

					return pDiskSpaceReturn;
				} 
				else 
				{
					if(pszInfo) sprintf(pszInfo, "Error allocating disk space info object");
				}

				// prs->MoveNext();
			} //if(!prs->IsEOF())
		
			prs->Close();
			delete prs;
		}
		else  //else prs null
		{
//g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:debug", "ReturnDestinationDiskSpace error: %s", errorstring);  Sleep(50);//(Dispatch message)
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
		}
		LeaveCriticalSection(&m_critSQL);

	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return NULL;

} 

/*
CString CPromoLiteData::ReturnParam(CString pszName, double dblTime)
{
			if((m_pdb)&&(m_pdbConn))
			{
CString szTemp;
		char errorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];

												bool bGotit=false;
												CString target_valueid, target_criterion_col_name, criterion_value, returned_property_col_name, default_value;

//_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select target_valueid, target_criterion_col_name, criterion_value, returned_property_col_name, [default_value] from [Parameters] where [name] = '%s'",pszName);
_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from Analysis");

g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
		EnterCriticalSection(&m_critSQL);
CRecordset* prsParam = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

												if(prsParam != NULL)
												{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� not null");  Sleep(50);//(Dispatch message)
													if((!prsParam->IsEOF())&&(!g_bKillThread))
													{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "� NEOF");  Sleep(50);//(Dispatch message)
														try
														{
															prs->GetFieldValue("target_valueid", target_valueid);
															prs->GetFieldValue("target_criterion_col_name", target_criterion_col_name);
															prs->GetFieldValue("criterion_value", criterion_value);
															prs->GetFieldValue("returned_property_col_name", returned_property_col_name);
															prs->GetFieldValue("default_value", default_value);

															bGotit=true;
														}
														catch(CException *e)// CDBException *e, CMemoryException *m)  
														{
															if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
															{
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);  Sleep(50);//(Dispatch message)
															}
															else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
															{
																// The error code is in e->m_nRetCode
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: out of memory");
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
															}
															else 
															{
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught other exception.\n%s", szSQL);
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
															}
															e->Delete();
														} 
														catch( ... )
														{
														}
													}
													prsParam->Close();
													delete prsParam;
												}
		LeaveCriticalSection(&g_m_critSQL);

if(bGotit)
{
_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top 1 val, datatype from (select top ' + %s + ' (case when len(' + %s + ') > 0 THEN ' + %s + ' ELSE ''' + %s + ''' END) as val, event_start, (select datatype from MetaConfigUnionALLView \
where col_name = ''' + %s + ''') as datatype from cortex.dbo.LiveEventData as LiveEventData where event_start > ' + %f + ' and \
(event_status is NULL or event_status = 0) and ' + %s + ' = ''' + %s + ''' order by event_start) as Temp order by event_start desc'",
					target_valueid,returned_property_col_name,returned_property_col_name,
					default_value,returned_property_col_name,
					dblTime,target_criterion_col_name,criterion_value
					);
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "2� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
		EnterCriticalSection(&m_critSQL);

prsParam = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);




}
else
{
	prsParam = NULL;
}


												if(prsParam != NULL)
												{
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "2� not null");  Sleep(50);//(Dispatch message)
													if((!prsParam->IsEOF())&&(!g_bKillThread))
													{
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "2� NEOF");  Sleep(50);//(Dispatch message)
														try
														{
															prsParam->GetFieldValue("val", szTemp);

														}
														catch(CException *e)// CDBException *e, CMemoryException *m)  
														{
															if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
															{
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);  Sleep(50);//(Dispatch message)
															}
															else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
															{
																// The error code is in e->m_nRetCode
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught exception: out of memory");
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
															}
															else 
															{
g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Retrieve: Caught other exception.\n%s", szSQL);
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
															}
															e->Delete();
														} 
														catch( ... )
														{
														}
													}
													prsParam->Close();
		LeaveCriticalSection(&m_critSQL);

													delete prsParam;
													return szTemp;
												}		LeaveCriticalSection(&m_critSQL);
											}
											return "";

}

*/
CFileMetaDataObject*  CPromoLiteData::ReturnFileMetaDataObject(char* pszFilename, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.

	if( (pszFilename)
			&&(strlen(pszFilename))
			&&(m_nIndexMetadataEndpoint>=0)
			&&(g_ppromolite->m_settings.m_ppEndpointObject)
			&&(g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint])
			&&(m_pdb)&&(m_pdbConn)
		 )
	{
//		ReleaseRecordSet(); // have to release so we can get new results.
		char errorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];

		char* pszEncodedFilename = m_pdb->EncodeQuotes(pszFilename);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s.dbo.%s WHERE sys_filename = '%s' \
AND sys_file_flags <> %d AND sys_file_flags <> %d",
				g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName?g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",			// the Default DB name
				g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata?g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata",   // the LiveEvents table name
				pszEncodedFilename?pszEncodedFilename:pszFilename,
				ARCHIVIST_FILEFLAG_ARCHIVED,
				ARCHIVIST_FILEFLAG_DELETED
			);
		if(pszEncodedFilename) free(pszEncodedFilename);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ReturnFileMetaDataObject SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

		int nNumFound = 0;
		if(prs != NULL) 
		{
			CFileMetaDataObject* pFileObjectReturn = NULL;
			if(!prs->IsEOF()) pFileObjectReturn = new CFileMetaDataObject;
			if(pFileObjectReturn)
			{
				while(!prs->IsEOF())
				{ 
					if(nNumFound>0)
					{
						// non unique!
						pFileObjectReturn->m_bUnique = false;
						break;
					}

					nNumFound++;
					CString szTemp;
					try
					{
						prs->GetFieldValue("sys_filename", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_vc256_sys_filename, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_filepath", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_vc256_sys_filepath, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

/*						prs->GetFieldValue("sys_linked_file", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_vc256_sys_linked_file, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}
*/
						prs->GetFieldValue("sys_description", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_vc256_sys_description, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_operator", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_vc64_sys_operator, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_type", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_type, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_duration", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_duration, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_valid_from", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_valid_from, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_expires_after", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_expires_after, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_ingest_date", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_ingest_date, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_file_flags", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_file_flags, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_file_size", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_dbl_sys_file_size, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_file_timestamp", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_dbl_sys_file_timestamp, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_created_on", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_created_on, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_created_by", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_vc32_sys_created_by, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_last_modified_on", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_sys_last_modified_on, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("sys_last_modified_by", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_vc32_sys_last_modified_by, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("promolite_last_used", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_promolite_last_used, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("promolite_times_used", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pFileObjectReturn->SetField(&pFileObjectReturn->m_sz_n_promolite_times_used, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
							if(m_pmsgr)
							{
								char errorstring[DB_ERRORSTRING_LEN];
								_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
								Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
							}
*/
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							// The error code is in e->m_nRetCode
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
						}
						else 
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
						}
						e->Delete();
					} 
					catch( ... )
					{
					}

					prs->MoveNext();
				} //while not eof
				
				prs->Close();
				delete prs;
				LeaveCriticalSection(&m_critSQL);
				return pFileObjectReturn;
			}
			else 
			{
				if(pszInfo) sprintf(pszInfo, "Error allocating file metadata object");
			}
			prs->Close();
			delete prs;
		}
		else  //else prs null
		{
//g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:debug", "ReturnFileMetaDataObject ERROR: %s", errorstring);  Sleep(50);//(Dispatch message)
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
		}
		LeaveCriticalSection(&m_critSQL);
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return NULL;

} 
/*
CDestinationMediaObject*  CPromoLiteData::ReturnDestinationMediaObjectToDelete(CPromoLiteEventObject* pObj, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.

	int nDestModule=ReturnModuleIndex(pObj, pszInfo);

	if(
		  (pObj)&&(m_pdb)&&(m_pdbConn)
			&&(nDestModule>=0)
			&&(g_ppromolite->m_settings.m_nNumEndpointsInstalled>1)
			&&(nDestModule<g_ppromolite->m_settings.m_nNumEndpointsInstalled)
			&&(g_ppromolite->m_settings.m_ppEndpointObject)
			&&(g_ppromolite->m_settings.m_ppEndpointObject[nDestModule])
			&&(m_nIndexMetadataEndpoint>=0)
			&&(m_nIndexMetadataEndpoint<g_ppromolite->m_settings.m_nNumEndpointsInstalled)
			&&(g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint])
		)
	{
	// use db data to figure out which is the statistical best file to delete.
		char pszFilename[MAX_PATH];
		char pszPartition[MAX_PATH];
		strcpy(pszFilename, "");
		memset(pszPartition, 0, MAX_PATH);

		if(g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_usType == PROMOLITE_DEP_EDGE_PROSPERO)
		{
			if(pObj->m_nCurrentFileSearch < 0)
			{ // main file
				if((pObj->m_pszMainFile)&&(strlen(pObj->m_pszMainFile)))
				{
					char* pchPartition = strchr(pObj->m_pszMainFile, '/'); // have to remove partition.
					if(pchPartition)
					{
						strcpy(pszFilename, pchPartition+1);
						strncpy(pszPartition, pObj->m_pszMainFile, pchPartition-pObj->m_pszMainFile);
					}
				}
			}
			else
			if(pObj->m_nCurrentFileSearch < pObj->m_nNumChildren)
			{
				if((pObj->m_ppszChildren)&&(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch])&&(strlen(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch])))
				{
					char* pchPartition = strchr(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch], '/'); // have to remove partition.
					if(pchPartition)
					{
						strcpy(pszFilename, pchPartition+1);
						strncpy(pszPartition, pObj->m_ppszChildren[pObj->m_nCurrentFileSearch], pchPartition-(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch]));
					}
				}
			}

			if(strlen(pszFilename)) // we arent actually using filename, but we needed it to get the partition from which to delete...
			{
//				ReleaseRecordSet(); // have to release so we can get new results.

				// first go for the ones with an expiry date!
				_timeb timestamp;
				_ftime( &timestamp );

				char errorstring[DB_ERRORSTRING_LEN];
				char szSQL[DB_SQLSTRING_MAXLEN];
				CString szTemp = "Destinations_Media";  // we didn't find it. just use the default name
				if(nDestModule>=0) szTemp = g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestinationMedia?g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestinationMedia:"Destinations_Media";

				CString szMetadata = "File_Metadata";
				if(m_nIndexMetadataEndpoint>=0) 
					szMetadata = g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata?g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszMetadata:"File_Metadata";

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 destinations_media.file_name FROM %s.dbo.%s as file_metadata \
JOIN %s.dbo.%s as destinations_media ON file_metadata.sys_filename = destinations_media.file_name \
WHERE (destinations_media.host = '%s' and destinations_media.partition = '%s' \
AND (destinations_media.promolite_local_last_used < %d \
AND destinations_media.promolite_local_last_used IS NOT NULL) \
AND (file_metadata.sys_ingest_date > 0 AND file_metadata.sys_ingest_date IS NOT NULL)) \
AND (file_metadata.sys_expires_after < %d OR file_metadata.sys_file_flags = %d) \
ORDER BY destinations_media.file_size DESC, destinations_media.promolite_local_last_used ASC",

						g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName?g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",
						szMetadata,
						pObj->m_sz_vc64_module_dbname,			// the edge device DB name
						szTemp,
						pObj->m_sz_vc64_dest_host,
						pszPartition,
						(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0) -(g_ppromolite->m_settings.m_ulDeletionThreshold) ), // local time....//[2 days ago local unix time]
						(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0) -(g_ppromolite->m_settings.m_ulDeletionThreshold) ), // local time....//[2 days ago local unix time]
	//					time(NULL)-(g_ppromolite->m_settings.m_ulDeletionThreshold), //[2 days ago unix time]
	//					time(NULL)-(g_ppromolite->m_settings.m_ulDeletionThreshold),  //[2 days ago unix time]
						ARCHIVIST_FILEFLAG_DELETED
					);
		
				CDestinationMediaObject* pDestMediaReturn = new CDestinationMediaObject;
				if(pDestMediaReturn)
				{
					EnterCriticalSection(&m_critSQL);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ReturnDestinationMediaObjectToDelete SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
					CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

					int nNumFound = 0;
					if(prs != NULL) 
					{
						if(!prs->IsEOF())
						{
							try
							{
								prs->GetFieldValue("file_name", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								if(szTemp.GetLength())
								{
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc256_file_name, szTemp.GetBuffer(1));
									szTemp.ReleaseBuffer();
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc16_partition, pszPartition);
									nNumFound++;
								}
							}
							catch(CException *e)// CDBException *e, CMemoryException *m)  
							{
								if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
		/*
									if(m_pmsgr)
									{
										char errorstring[DB_ERRORSTRING_LEN];
										_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
										Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
									}
		* /
								}
								else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
								{
									// The error code is in e->m_nRetCode
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
		//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
								}
								else 
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
		//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
								}
								e->Delete();
							} 
							catch( ... )
							{
							}

							if(nNumFound>0)
							{
								prs->Close();
								delete prs;

								LeaveCriticalSection(&m_critSQL);
								return pDestMediaReturn;
							}

						// prs->MoveNext();
						} //if(!prs->IsEOF())
					
						prs->Close();
						delete prs;


						// if we are here, we didnt find one to return...
// so, now try... 2.(least used by promolite) order by promolite_times_used asc, file_size desc where last used by promolite last used is null or more than 2 days old and times_used is not null.

						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 destinations_media.file_name FROM %s.dbo.%s as file_metadata \
JOIN %s.dbo.%s as destinations_media ON file_metadata.sys_filename = destinations_media.file_name \
WHERE (destinations_media.host = '%s' and destinations_media.partition = '%s' \
AND (destinations_media.promolite_local_last_used < %d \
AND destinations_media.promolite_local_last_used IS NOT NULL) \
AND (file_metadata.sys_ingest_date > 0 AND file_metadata.sys_ingest_date IS NOT NULL)) \
ORDER BY destinations_media.promolite_local_last_used ASC, destinations_media.file_size DESC",

								g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName?g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",
								szMetadata,
								pObj->m_sz_vc64_module_dbname,			// the edge device DB name
								szTemp,
								pObj->m_sz_vc64_dest_host,
								pszPartition,
								(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0) -(g_ppromolite->m_settings.m_ulDeletionThreshold) ) // local time....//[2 days ago local unix time]
//								time(NULL)-(g_ppromolite->m_settings.m_ulDeletionThreshold) //[2 days ago unix time]
							);
							
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ReturnDestinationMediaObjectToDelete(2) SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
						prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
						if(prs != NULL) 
						{
							if(!prs->IsEOF())
							{
								try
								{
									prs->GetFieldValue("file_name", szTemp);//HARDCODE
									szTemp.TrimLeft(); szTemp.TrimRight();
									if(szTemp.GetLength())
									{
										pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc256_file_name, szTemp.GetBuffer(1));
										szTemp.ReleaseBuffer();
										pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc16_partition, pszPartition);
										nNumFound++;
									}
								}
								catch(CException *e)// CDBException *e, CMemoryException *m)  
								{
									if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
									{
										if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
			/*
										if(m_pmsgr)
										{
											char errorstring[DB_ERRORSTRING_LEN];
											_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
											Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
										}
			* /
									}
									else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
									{
										// The error code is in e->m_nRetCode
										if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
			//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
									}
									else 
									{
										if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
			//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
									}
									e->Delete();
								} 
								catch( ... )
								{
								}

								if(nNumFound>0)
								{
									prs->Close();
									delete prs;
									LeaveCriticalSection(&m_critSQL);
									return pDestMediaReturn;
								}

							// prs->MoveNext();
							} //if(!prs->IsEOF())
						
							prs->Close();
							delete prs;
						// if we are here, we STILL didnt find one to return...
// so, now try... 3. oldest transfer_date  

							if(g_ppromolite->m_settings.m_bAutoDeleteOldest)
							{
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 destinations_media.file_name FROM %s.dbo.%s as file_metadata \
JOIN %s.dbo.%s as destinations_media ON file_metadata.sys_filename = destinations_media.file_name \
WHERE (destinations_media.host = '%s' and destinations_media.partition = '%s' \
AND (destinations_media.promolite_local_last_used < %d \
AND destinations_media.promolite_local_last_used IS NOT NULL) \
AND (file_metadata.sys_ingest_date > 0 AND file_metadata.sys_ingest_date IS NOT NULL)) \
ORDER BY destinations_media.transfer_date DESC",
									g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName?g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexMetadataEndpoint]->m_pszDBName:"Archivist",
									szMetadata,
									pObj->m_sz_vc64_module_dbname,			// the edge device DB name
									szTemp,
									pObj->m_sz_vc64_dest_host,
									pszPartition,
									(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0) -(g_ppromolite->m_settings.m_ulDeletionThreshold) ) // local time....//[2 days ago local unix time]
//									time(NULL)-(g_ppromolite->m_settings.m_ulDeletionThreshold) //[2 days ago unix time]
									);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ReturnDestinationMediaObjectToDelete(3) SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
								prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

								if(prs != NULL) 
								{
									if(!prs->IsEOF())
									{
										try
										{
											prs->GetFieldValue("file_name", szTemp);//HARDCODE
											szTemp.TrimLeft(); szTemp.TrimRight();
											if(szTemp.GetLength())
											{
												pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc256_file_name, szTemp.GetBuffer(1));
												szTemp.ReleaseBuffer();
												pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc16_partition, pszPartition);
												nNumFound++;
											}
										}
										catch(CException *e)// CDBException *e, CMemoryException *m)  
										{
											if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
											{
												if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
					/*
												if(m_pmsgr)
												{
													char errorstring[DB_ERRORSTRING_LEN];
													_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
													Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
												}
					* /
											}
											else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
											{
												// The error code is in e->m_nRetCode
												if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
					//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
											}
											else 
											{
												if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
					//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
											}
											e->Delete();
										} 
										catch( ... )
										{
										}

										if(nNumFound>0)
										{
											prs->Close();
											delete prs;

											LeaveCriticalSection(&m_critSQL);
											return pDestMediaReturn;
										}

									// prs->MoveNext();
									} //if(!prs->IsEOF())
								
									prs->Close();
									delete prs;
								}
								else  //else prs null
								{
//g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:debug", "ReturnDestinationMediaObjectToDelete(2) ERROR: %s", errorstring);  Sleep(50);//(Dispatch message)
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
								}
							}
						}
						else  //else prs null
						{
//g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:debug", "ReturnDestinationMediaObjectToDelete(1) ERROR: %s", errorstring);  Sleep(50);//(Dispatch message)
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
						}

					}
					else  //else prs null
					{
//g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:debug", "ReturnDestinationMediaObjectToDelete ERROR: %s", errorstring);  Sleep(50);//(Dispatch message)
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
					}
					LeaveCriticalSection(&m_critSQL);
				} // allocated the object.
				else 
				{
					if(pszInfo) sprintf(pszInfo, "Error allocating destination media object");
				}

			}
			else
			{
				if(pszInfo) sprintf(pszInfo, "Invalid filename");
			}
		}
		else
		{
			if(pszInfo) sprintf(pszInfo, "Unsupported device.");
		}
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return NULL;

}
*/

CDestinationMediaObject*  CPromoLiteData::ReturnDestinationMediaObject(CPromoLiteEventObject* pObj, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.

	int nDestModule=ReturnModuleIndex(pObj, pszInfo);

	if(
		  (pObj)&&(m_pdb)&&(m_pdbConn)
			&&(nDestModule>=0)
			&&(g_ppromolite->m_settings.m_nNumEndpointsInstalled>0)
			&&(nDestModule<g_ppromolite->m_settings.m_nNumEndpointsInstalled)
			&&(g_ppromolite->m_settings.m_ppEndpointObject)
			&&(g_ppromolite->m_settings.m_ppEndpointObject[nDestModule])
		)
	{
		char pszFilename[MAX_PATH];
		strcpy(pszFilename, "");
		if(g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_usType == PROMOLITE_DEP_EDGE_PROSPERO)
		{
			if(pObj->m_nCurrentFileSearch < 0)
			{ // main file
				if((pObj->m_pszMainFile)&&(strlen(pObj->m_pszMainFile)))
				{
					char* pchPartition = strchr(pObj->m_pszMainFile, '/'); // have to remove partition.
					if(pchPartition) strcpy(pszFilename, pchPartition+1);
				}
			}
			else
			if(pObj->m_nCurrentFileSearch < pObj->m_nNumChildren)
			{
				if((pObj->m_ppszChildren)&&(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch])&&(strlen(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch])))
				{
					char* pchPartition = strchr(pObj->m_ppszChildren[pObj->m_nCurrentFileSearch], '/'); // have to remove partition.
					if(pchPartition) strcpy(pszFilename, pchPartition+1);
				}
			}

			if(strlen(pszFilename))
			{
//				ReleaseRecordSet(); // have to release so we can get new results.
				char errorstring[DB_ERRORSTRING_LEN];
				char szSQL[DB_SQLSTRING_MAXLEN];
				CString szTemp = "Destinations_Media";  // we didn't find it. just use the default name
				if(nDestModule>=0) szTemp = g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestinationMedia?g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestinationMedia:"Destinations_Media";
				char* pszEncodedFilename = m_pdb->EncodeQuotes(pszFilename);
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s.dbo.%s WHERE filename = %s AND host = '%s'",
						pObj->m_sz_vc64_module_dbname,			// the edge device DB name
						szTemp,
						pszEncodedFilename?pszEncodedFilename:pszFilename,
						pObj->m_sz_vc64_dest_host
					);
				if(pszEncodedFilename) free(pszEncodedFilename);
			
				EnterCriticalSection(&m_critSQL);
				CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

				int nNumFound = 0;
				if(prs != NULL) 
				{
					if(!prs->IsEOF())
					{
						CDestinationMediaObject* pDestMediaReturn = new CDestinationMediaObject;
						if(pDestMediaReturn)
						{
							try
							{
								prs->GetFieldValue("file_name", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								if(szTemp.GetLength())
								{
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc256_file_name, szTemp.GetBuffer(1));
									szTemp.ReleaseBuffer();
								}

								prs->GetFieldValue("transfer_date", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								if(szTemp.GetLength())
								{
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_n_transfer_date, szTemp.GetBuffer(1));
									szTemp.ReleaseBuffer();
								}

								prs->GetFieldValue("partition", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								if(szTemp.GetLength())
								{
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_vc16_partition, szTemp.GetBuffer(1));
									szTemp.ReleaseBuffer();
								}

								prs->GetFieldValue("file_size", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								if(szTemp.GetLength())
								{
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_dbl_file_size, szTemp.GetBuffer(1));
									szTemp.ReleaseBuffer();
								}

								prs->GetFieldValue("PromoLite_local_last_used", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								if(szTemp.GetLength())
								{
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_n_PromoLite_local_last_used, szTemp.GetBuffer(1));
									szTemp.ReleaseBuffer();
								}

								prs->GetFieldValue("PromoLite_local_times_used", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								if(szTemp.GetLength())
								{
									pDestMediaReturn->SetField(&pDestMediaReturn->m_sz_n_PromoLite_local_times_used, szTemp.GetBuffer(1));
									szTemp.ReleaseBuffer();
								}

							}
							catch(CException *e)// CDBException *e, CMemoryException *m)  
							{
								if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
		/*
									if(m_pmsgr)
									{
										char errorstring[DB_ERRORSTRING_LEN];
										_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
										Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
									}
		*/
								}
								else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
								{
									// The error code is in e->m_nRetCode
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
		//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
								}
								else 
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
		//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
								}
								e->Delete();
							} 
							catch( ... )
							{
							}

							prs->Close();
							delete prs;
							LeaveCriticalSection(&m_critSQL);

							return pDestMediaReturn;
						} 
						else 
						{
							if(pszInfo) sprintf(pszInfo, "Error allocating destination media object");
						}

						// prs->MoveNext();
					} //if(!prs->IsEOF())
				
					prs->Close();
					delete prs;
				}
				else  //else prs null
				{
					if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
				}
				LeaveCriticalSection(&m_critSQL);

			}
			else
			{
				if(pszInfo) sprintf(pszInfo, "Invalid filename");
			}
		}
		else
		{
			if(pszInfo) sprintf(pszInfo, "Unsupported device.");
		}
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return NULL;
} 

unsigned long CPromoLiteData::ReturnDestinationFlags(CPromoLiteEventObject* pObj, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.

	int nDestModule=ReturnModuleIndex(pObj, pszInfo);

	if(
		  (pObj)&&(m_pdb)&&(m_pdbConn)
			&&(nDestModule>=0)
			&&(g_ppromolite->m_settings.m_nNumEndpointsInstalled>0)
			&&(nDestModule<g_ppromolite->m_settings.m_nNumEndpointsInstalled)
			&&(g_ppromolite->m_settings.m_ppEndpointObject)
			&&(g_ppromolite->m_settings.m_ppEndpointObject[nDestModule])
		)
	{
//		ReleaseRecordSet(); // have to release so we can get new results.
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];

		CString szTemp = "Destinations";  // we didnt't find it. just use the default name
		if(nDestModule>=0) szTemp = g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination?g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_pszDestination:"Destinations";
		
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT flags FROM %s.dbo.%s WHERE destinationid = %s",
			pObj->m_sz_vc64_module_dbname,			// the edge device DB name
			szTemp,
			pObj->m_sz_n_destinationid
			);
		
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				CString szTemp;
				unsigned long ulTemp = PROMOLITE_FLAGS_INVALID;
				try
				{
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						ulTemp = atol(szTemp);
					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				prs->Close();
				delete prs;
				LeaveCriticalSection(&m_critSQL);

				return ulTemp;
				// prs->MoveNext();
			} //if(!prs->IsEOF())
		
			prs->Close();
			delete prs;
		}
		else  //else prs null
		{
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
		}			
		LeaveCriticalSection(&m_critSQL);

	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return PROMOLITE_FLAGS_INVALID;

}


unsigned long CPromoLiteData::ReturnChannelFlags(CPromoLiteEventObject* pObj, char* pszInfo)
{
//create table Channels (ID int NOT NULL identity(1,1), flags int NOT NULL, 
//description varchar(256), server varchar(32), listid int);

	_ftime( &m_timebTick );  // we're still alive.
	if(
		  (pObj)&&(m_pdb)&&(m_pdbConn)
			&&(m_nIndexAutomationEndpoint>=0)
			&&(g_ppromolite->m_settings.m_nNumEndpointsInstalled>0)
			&&(m_nIndexAutomationEndpoint<g_ppromolite->m_settings.m_nNumEndpointsInstalled)
			&&(g_ppromolite->m_settings.m_ppEndpointObject)
			&&(g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexAutomationEndpoint])
		)
	{
//		ReleaseRecordSet(); // have to release so we can get new results.
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT flags FROM %s.dbo.%s WHERE ID = %s",
			g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexAutomationEndpoint]->m_pszDBName?g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexAutomationEndpoint]->m_pszDBName:"Helios",			// the edge device DB name could be Sentinel too. hmm on this default.
			g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexAutomationEndpoint]->m_pszChannel?g_ppromolite->m_settings.m_ppEndpointObject[m_nIndexAutomationEndpoint]->m_pszChannel:"Channels",   // the channels table name
			pObj->m_sz_n_channelid
			);
		
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				CString szTemp;
				unsigned long ulTemp = PROMOLITE_FLAGS_INVALID;
				try
				{
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						ulTemp = atol(szTemp);
					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				prs->Close();
				delete prs;
				LeaveCriticalSection(&m_critSQL);

				return ulTemp;
				// prs->MoveNext();
			} //if(!prs->IsEOF())
		
			prs->Close();
			delete prs;
		}
		else  //else prs null
		{
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
		}
		LeaveCriticalSection(&m_critSQL);
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return PROMOLITE_FLAGS_INVALID;

}

char* CPromoLiteData::ReturnPartition(CPromoLiteEventObject* pObj, char* pszFilename, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.

	char* pchExt = NULL;
	int nDestModule=-1;
	
	if((pszFilename)&&(strlen(pszFilename))) pchExt = strrchr(pszFilename, '.');
	if(pchExt)
	{ 
		pchExt++;
		nDestModule=ReturnModuleIndex(pObj, pszInfo);
	}
	if((pObj)&&(m_pdb)&&(m_pdbConn)&&(pchExt)&&(strlen(pchExt))&&(nDestModule>=0)&&(nDestModule<g_ppromolite->m_settings.m_nNumEndpointsInstalled)
		&&(g_ppromolite->m_settings.m_ppEndpointObject)&&(g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]))
	{
//		ReleaseRecordSet(); // have to release so we can get new results.
		char errorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];


		char* pchEncodedExt = m_pdb->EncodeQuotes(pchExt);
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT partition FROM %s.dbo.%s WHERE criterion = '%s'",
				g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName?g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_pszDBName:"Propsero", 
				g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_pszFileTypes?g_ppromolite->m_settings.m_ppEndpointObject[nDestModule]->m_pszFileTypes:"FileTypeMapping",  // edge device DB and table name;
				pchEncodedExt?pchEncodedExt:pchExt
			);
		if(pchEncodedExt) free(pchEncodedExt);
		
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

		int nNumFound = 0;
		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				CString szTemp="";
				try
				{
					prs->GetFieldValue("partition", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				if(szTemp.GetLength()>0)
				{
					pchExt = (char*)malloc(szTemp.GetLength()+1);
					if(pchExt)
					{
						sprintf(pchExt, "%s", szTemp);

						prs->Close();
						delete prs;
						LeaveCriticalSection(&m_critSQL);

						return pchExt;
					}
					else
					{
						if(pszInfo) strcpy(pszInfo, "Error allocating buffer");
					}

				}
				else
				{
					if(pszInfo) strcpy(pszInfo, "No partition information was returned");
				}
				// prs->MoveNext();
			} //if(!prs->IsEOF())
		
			prs->Close();
			delete prs;
		}
		else  //else prs null
		{
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
		}
		LeaveCriticalSection(&m_critSQL);

	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return NULL;

} 

CPromoLiteQueueObject* CPromoLiteData::ReturnFromEndpointQueue(CPromoLiteEventObject* pObj, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.
	if((pObj)&&(m_pdb)&&(m_pdbConn))
	{
//		ReleaseRecordSet(); // have to release so we can get new results.
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s.dbo.%s WHERE event_itemid = %s",
				pObj->m_sz_vc64_module_dbname,			// the edge device DB name
				pObj->m_sz_vc64_module_dbqueue,   // the Queue table name
				pObj->m_sz_n_event_itemid
			);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "--------  ReturnFromEndpointQueue SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
		
		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);

		if(prs != NULL) 
		{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "--------  ReturnFromEndpointQueue prs not null");  Sleep(50); //(Dispatch message)
			if(!prs->IsEOF())
			{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "--------  ReturnFromEndpointQueue prs not EOF");  Sleep(50); //(Dispatch message)
				CPromoLiteQueueObject* pQueueReturn = new CPromoLiteQueueObject;
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "--------  ReturnFromEndpointQueue pQueueReturn");  Sleep(50); //(Dispatch message)

				if(pQueueReturn)
				{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "--------  ReturnFromEndpointQueue pQueueReturn not null");  Sleep(50); //(Dispatch message)
					CString szTemp;
					int nTemp;
					try
					{
						// get id!
						prs->GetFieldValue("itemid", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "--------  ReturnFromEndpointQueue itemid %s", szTemp);  Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							nTemp = atoi(szTemp);
							if(nTemp>=0) pQueueReturn->m_nItemID = nTemp;
						}

						prs->GetFieldValue("filename_local", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "--------  ReturnFromEndpointQueue filename_local %s", szTemp);  Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							pQueueReturn->SetField(&pQueueReturn->m_pszFilenameLocal, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("filename_remote", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "--------  ReturnFromEndpointQueue filename_remote %s", szTemp);  Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							pQueueReturn->SetField(&pQueueReturn->m_pszFilenameRemote, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("action", szTemp);//HARDCODE
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "--------  ReturnFromEndpointQueue action %s", szTemp);  Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							nTemp = atoi(szTemp);
							if(nTemp>=0) pQueueReturn->m_nActionID = nTemp;
						}

						prs->GetFieldValue("host", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "--------  ReturnFromEndpointQueue host %s", szTemp);  Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							pQueueReturn->SetField(&pQueueReturn->m_pszHost, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("timestamp", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "--------  ReturnFromEndpointQueue timestamp %s", szTemp);  Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							pQueueReturn->m_dblTimestamp = atof(szTemp);
						}

						prs->GetFieldValue("username", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "--------  ReturnFromEndpointQueue username %s", szTemp);  Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							pQueueReturn->SetField(&pQueueReturn->m_pszUsername, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

						prs->GetFieldValue("event_itemid", szTemp);//HARDCODE
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "--------  ReturnFromEndpointQueue event_itemid %s", szTemp);  Sleep(50); //(Dispatch message)
						if(szTemp.GetLength())
						{
							nTemp = atoi(szTemp);
							if(nTemp>=0) pQueueReturn->m_nEventItemID = nTemp;
						}
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "--------  ReturnFromEndpointQueue event item id %d", pQueueReturn->m_nEventItemID);  Sleep(50); //(Dispatch message)

						prs->GetFieldValue("message", szTemp);//HARDCODE
						szTemp.TrimLeft(); szTemp.TrimRight();
						if(szTemp.GetLength())
						{
							pQueueReturn->SetField(&pQueueReturn->m_pszMessage, szTemp.GetBuffer(1));
							szTemp.ReleaseBuffer();
						}

					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
							if(m_pmsgr)
							{
								char errorstring[DB_ERRORSTRING_LEN];
								_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
								Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
							}
*/
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							// The error code is in e->m_nRetCode
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
						}
						else 
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
						}
						e->Delete();
					} 
					catch( ... )
					{
					}

					prs->Close();
					delete prs;
					LeaveCriticalSection(&m_critSQL);

					return pQueueReturn;
				} 
				else 
				{
					if(pszInfo) sprintf(pszInfo, "Error allocating queue object");
				}

				// prs->MoveNext();
			} //if(!prs->IsEOF())
		
			prs->Close();
			delete prs;
		}
		else  //else prs null
		{
			if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%s", errorstring);
		}
		LeaveCriticalSection(&m_critSQL);

	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return NULL;
}

int CPromoLiteData::RemoveFromEndpointQueue(CPromoLiteEventObject* pObj, int nItemID, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.
	if((pObj)&&(m_pdb)&&(m_pdbConn))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s WHERE itemid = %d",  //HARDCODE
					pObj->m_sz_vc64_module_dbname,
					pObj->m_sz_vc64_module_dbqueue,
					nItemID
				);

//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "-------------  RemoveFromEndpointQueue SQL: %s", szSQL);  Sleep(50); //(Dispatch message)

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
		{
		//**MSG
//		g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)
			LeaveCriticalSection(&m_critSQL);
			return PROMOLITE_ERROR;
		}
		LeaveCriticalSection(&m_critSQL);
		return PROMOLITE_SUCCESS;
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return PROMOLITE_ERROR;
}

int CPromoLiteData::ScheduleEndpointQueue(CPromoLiteEventObject* pObj, char* pszFilenameLocal, char* pszFilenameRemote, int nAction, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.
	if((pObj)&&(m_pdb)&&(m_pdbConn))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		char* pchEncodedFileLocal = m_pdb->EncodeQuotes(pszFilenameLocal);
		char* pchEncodedFileRemote = m_pdb->EncodeQuotes(pszFilenameRemote);
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(filename_local, filename_remote, action, host, timestamp, username, event_itemid) \
VALUES ('%s','%s', %d, '%s', %d.%d, 'sys', %s)", //HARDCODE
															pObj->m_sz_vc64_module_dbname,
															pObj->m_sz_vc64_module_dbqueue,
															pchEncodedFileLocal?pchEncodedFileLocal:"",
															pchEncodedFileRemote?pchEncodedFileRemote:"",
															nAction,
															pObj->m_sz_vc64_dest_host,
															m_timebTick.time,
															m_timebTick.millitm,
															pObj->m_sz_n_event_itemid
															);
		if(pchEncodedFileLocal) free(pchEncodedFileLocal);
		if(pchEncodedFileRemote) free(pchEncodedFileRemote);

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ScheduleEndpointQueue SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
//		char errorstring[DB_ERRORSTRING_LEN]; if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
		{
		//**MSG
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
			LeaveCriticalSection(&m_critSQL);
			return PROMOLITE_ERROR;
		}
		LeaveCriticalSection(&m_critSQL);
		return PROMOLITE_SUCCESS;
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return PROMOLITE_ERROR;
}

int CPromoLiteData::SendGraphicsCommand(int nCmd, char* chDataBuffer)
{
/*
	if(!m_bNetClientConnected)
	{
		if(m_net.OpenConnection(
			(g_ppromolite->m_settings.m_pszGfxHost?g_ppromolite->m_settings.m_pszGfxHost:"127.0.0.1"),
			g_ppromolite->m_settings.m_nGfxPort, 
			&m_socket) < NET_SUCCESS)
		{
			m_bNetClientConnected = false;
			m_socket = NULL;
		}
		else
		{
			m_bNetClientConnected = true;
		}
	}

	if((m_bNetClientConnected)&&(m_socket))
	{
		char* pch = NULL;
		if((chDataBuffer)&&(strlen(chDataBuffer))) pch = (char*)malloc(strlen(chDataBuffer)+1);
		if(pch) memcpy(pch, chDataBuffer, strlen(chDataBuffer)+1); // includes term 0;
		m_netdata.m_ucCmd = nCmd&0xff;
		if(m_netdata.m_pucData!=NULL){	free(m_netdata.m_pucData); m_netdata.m_pucData=NULL;} //destroy the buffer;
		m_netdata.m_pucData=(unsigned char*)pch;
		m_netdata.m_ulDataLen = (pch?strlen(pch):0);
		m_netdata.m_ucType = ((NET_TYPE_PROTOCOL1|NET_TYPE_KEEPOPEN|NET_SND_KEEPOPENLCL)|(chDataBuffer?NET_TYPE_HASDATA:0)); // has data but no subcommand.
		int n = m_net.SendData(&m_netdata, m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT);//|NET_SND_NO_RX);
		if(m_netdata.m_pucData!=NULL)
		{
			free(m_netdata.m_pucData);  //destroy the buffer;
			m_netdata.m_pucData=NULL;
		}
		if(n < NET_SUCCESS)
		{
			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Net error %d sending cmd 0x%02x buffer: %s", n, nCmd, chDataBuffer?chDataBuffer:"");  //(Dispatch message)
			m_net.CloseConnection(m_socket);
			m_bNetClientConnected = false;
			m_socket = NULL;
		}
		else
		{
			m_net.SendData(NULL, m_socket, 5000, 0, NET_SND_CLNTACK, NULL);
			return PROMOLITE_SUCCESS;
		}
	}
*/
	return PROMOLITE_ERROR;
}


/*
int CPromoLiteData::SimpleSetStatus(CPromoLiteEventObject* pObj, int nStatus, bool bUpdateSearchFiles, bool bFileIndex, int nAppDataAux, char* pszInfo)
{
	_ftime( &m_timebTick );  // we're still alive.
	if((pObj)&&(m_pdb)&&(m_pdbConn))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		if(nAppDataAux>0)
		{
			if(bUpdateSearchFiles)
			{
				char* pszEncodedSearchFile = m_pdb->EncodeQuotes(pObj->m_sz_vc1024_search_files);
				if(bFileIndex)
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d, search_files = '%s', file_index = %d, app_data_aux = %d WHERE itemid = %s",
							g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite",			// the Default DB name
							g_ppromolite->m_settings.m_pszLiveEvents?g_ppromolite->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							pszEncodedSearchFile?pszEncodedSearchFile:pObj->m_sz_vc1024_search_files,
							pObj->m_nCurrentFileSearch,
							nAppDataAux,
							pObj->m_sz_n_itemid
						);
				}
				else
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d, search_files = '%s', app_data_aux = %d WHERE itemid = %s",
							g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite",			// the Default DB name
							g_ppromolite->m_settings.m_pszLiveEvents?g_ppromolite->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							pszEncodedSearchFile?pszEncodedSearchFile:pObj->m_sz_vc1024_search_files,
							nAppDataAux,
							pObj->m_sz_n_itemid
						);
				}
				if(pszEncodedSearchFile) free(pszEncodedSearchFile);
			}
			else
			{
				if(bFileIndex)
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d, file_index = %d, app_data_aux = %d WHERE itemid = %s",
							g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite",			// the Default DB name
							g_ppromolite->m_settings.m_pszLiveEvents?g_ppromolite->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							pObj->m_nCurrentFileSearch,
							nAppDataAux,
							pObj->m_sz_n_itemid
						);
				}
				else
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d, app_data_aux = %d WHERE itemid = %s",
							g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite",			// the Default DB name
							g_ppromolite->m_settings.m_pszLiveEvents?g_ppromolite->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							nAppDataAux,
							pObj->m_sz_n_itemid
						);
				}
			}
		}
		else  // dont update app data
		{
			if(bUpdateSearchFiles)
			{
				char* pszEncodedSearchFile = m_pdb->EncodeQuotes(pObj->m_sz_vc1024_search_files);
				if(bFileIndex)
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d, search_files = '%s', file_index = %d WHERE itemid = %s",
							g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite",			// the Default DB name
							g_ppromolite->m_settings.m_pszLiveEvents?g_ppromolite->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							pszEncodedSearchFile?pszEncodedSearchFile:pObj->m_sz_vc1024_search_files,
							pObj->m_nCurrentFileSearch,
							pObj->m_sz_n_itemid
						);
				}
				else
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d, search_files = '%s' WHERE itemid = %s",
							g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite",			// the Default DB name
							g_ppromolite->m_settings.m_pszLiveEvents?g_ppromolite->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							pszEncodedSearchFile?pszEncodedSearchFile:pObj->m_sz_vc1024_search_files,
							pObj->m_sz_n_itemid
						);
				}
				if(pszEncodedSearchFile) free(pszEncodedSearchFile);
			}
			else
			{
				if(bFileIndex)
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d, file_index = %d WHERE itemid = %s",
							g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite",			// the Default DB name
							g_ppromolite->m_settings.m_pszLiveEvents?g_ppromolite->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							pObj->m_nCurrentFileSearch,
							pObj->m_sz_n_itemid
						);
				}
				else
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = %d, transfer_date = %d WHERE itemid = %s",
							g_ppromolite->m_settings.m_pszDefaultDB?g_ppromolite->m_settings.m_pszDefaultDB:"PromoLite",			// the Default DB name
							g_ppromolite->m_settings.m_pszLiveEvents?g_ppromolite->m_settings.m_pszLiveEvents:"Events",   // the LiveEvents table name
							nStatus,
							m_timebTick.time,
							pObj->m_sz_n_itemid
						);
				}
			}
		}

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "SimpleSetStatus SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
//		char errorstring[DB_ERRORSTRING_LEN];		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
		{
		//**MSG
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
			LeaveCriticalSection(&m_critSQL);
			return PROMOLITE_ERROR;
		}
		LeaveCriticalSection(&m_critSQL);
		return PROMOLITE_SUCCESS;
	}
	else
	{
		if(pszInfo) sprintf(pszInfo, "Invalid parameters, or null database members");
	}
	return PROMOLITE_ERROR;
}

int CPromoLiteData::UpdateSearchFiles(CPromoLiteEventObject* pObj, int nStatus)
{
	if((pObj)&&(nStatus>=0))
	{
		pObj->m_sz_vc1024_search_files.Format("%d%s",
				pObj->m_nCurrentFileSearch<0?nStatus:pObj->m_nMainFileStatus, 
				pObj->m_pszMainFile
			);

		int c=0;
		CString szTemp;
		while(c<pObj->m_nNumChildren)
		{
			szTemp = pObj->m_sz_vc1024_search_files;
			pObj->m_sz_vc1024_search_files.Format("%s|%d%s",
				szTemp,
				c==pObj->m_nCurrentFileSearch?nStatus:pObj->m_pnChildrenStatus[c], 
				pObj->m_ppszChildren[c]
				);
			c++;
		}
/*
// file status codes:
0 not yet checked
1 checking
2 checked and present
3 transferring
4 transferred
5 error
6 skipped
* /

		return PROMOLITE_SUCCESS;
	}
	return PROMOLITE_ERROR;
}

*/

/*
int	CPromoLiteData::ParseFiles(char* pszSourceData, char** pszFile, int* pnStatus, char*** pppszChildren, int** ppnChildrenStatus, int* pnNumChildren)
{
	if((pszSourceData)&&(strlen(pszSourceData)>0)&&(pszFile)&&(pnStatus)&&(pppszChildren)&&(ppnChildrenStatus)&&(pnNumChildren))
	{
//		*pszFile = NULL; *pnStatus=0; *pppszChildren=NULL; *ppnChildrenStatus=NULL; *pnNumChildren=NULL; return 1;

		CSafeBufferUtil sbu;
		char* pchReturn;
		char* pch = sbu.Token(pszSourceData, strlen(pszSourceData), "|");
		if(pch)
		{
			*pnStatus = (int)((*pch)-48); // ascii code. translated to numerical
			pch++;

			pchReturn = (char*)malloc(strlen(pch)+1);
			if(pchReturn)
			{
				strcpy(pchReturn, pch);
				*pszFile = pchReturn;
			}
			else return PROMOLITE_ERROR;
		}
		pch = sbu.Token(NULL, NULL, "|");
		int nNumChildren=0;
		int* pnChildrenStatus=NULL;
		char** ppszChildren= NULL;
		while(pch)
		{
			int* pnChStatus= new int[nNumChildren+1];
			char** ppszCh = new char*[nNumChildren+1];

			if((pnChStatus)&&(ppszCh))
			{
				if(pnChildrenStatus)
				{
					int i=0;
					while(i<nNumChildren)
					{
						pnChStatus[i] = pnChildrenStatus[i];
						i++;
					}
					delete [] pnChildrenStatus;
				}

				if(ppszChildren)
				{
					int i=0;
					while(i<nNumChildren)
					{
						ppszCh[i] = ppszChildren[i];
						i++;
					}
					delete [] ppszChildren;
				}

				pnChildrenStatus = pnChStatus;
				ppszChildren = ppszCh;


				pnChildrenStatus[nNumChildren] = (int)(*pch) - 48; // 48 is char code for 0.
				if((pnChildrenStatus[nNumChildren]<0)||(pnChildrenStatus[nNumChildren]>9)) // has to be single digit
					pnChildrenStatus[nNumChildren] = 0;
				pch++;

				pchReturn = (char*)malloc(strlen(pch)+1);
				if(pchReturn)
				{
					strcpy(pchReturn, pch);
					ppszChildren[nNumChildren] = pchReturn;
				}
				nNumChildren++;
			}
			pch = sbu.Token(NULL, NULL, "|");
		}
		*pppszChildren = ppszChildren;
		*ppnChildrenStatus = pnChildrenStatus;
		*pnNumChildren = nNumChildren;
	}
	return PROMOLITE_ERROR;
}
*/

int CPromoLiteData::GetConnections(char* pszInfo)
{
	if((g_ppromolite)&&(m_pdbConn)&&(m_pdb))
	{

/*		
		
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY server",  //HARDCODE
			((g_ppromolite->m_settings.m_pszChannelInfo)&&(strlen(g_ppromolite->m_settings.m_pszChannelInfo)))?g_ppromolite->m_settings.m_pszChannelInfo:"ChannelInfo");
//		g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "GetConnections");  Sleep(250); //(Dispatch message)
*/
		CPromoLiteEndpointObject* pEndObj = NULL;
		unsigned long ulAutoType = PROMOLITE_DEP_UNKNOWN;
		if(
			  (g_ppromolite->m_settings.m_ppEndpointObject)
			&&(g_ppromolite->m_data.m_nIndexAutomationEndpoint>=0)
			&&(g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexAutomationEndpoint])
			)
		{
			pEndObj = g_ppromolite->m_settings.m_ppEndpointObject[g_ppromolite->m_data.m_nIndexAutomationEndpoint];
		}
		else return PROMOLITE_ERROR;
		char szSQL[DB_SQLSTRING_MAXLEN];

		if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_SENTINEL)
		{
			ulAutoType = PROMOLITE_DEP_AUTO_SENTINEL;

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, server, listid, server_time, server_status, server_basis, server_changed, server_last_update, \
list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update%s FROM %s ORDER by server, listid",
				(g_ppromolite->m_settings.m_bChannelInfoViewHasDesc?", channel_description":""),
				(g_ppromolite->m_settings.m_pszChannelInfo?g_ppromolite->m_settings.m_pszChannelInfo:"ChannelInfo")
				);
		}
		else
		if((pEndObj->m_usType&PROMOLITE_DEP_AUTO_MASK) == PROMOLITE_DEP_AUTO_HELIOS)
		{
			ulAutoType = PROMOLITE_DEP_AUTO_HELIOS;
/*
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, conn_ip, conn_port, server_time, server_status, server_basis, server_changed, server_last_update, \
list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update FROM %s ORDER by conn_ip, conn_port",
				(g_ppromolite->m_settings.m_pszChannelInfo?g_ppromolite->m_settings.m_pszChannelInfo:"ChannelInfo")
				);
*/

// hmm same as sentinel
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, server, listid, server_time, server_status, server_basis, server_changed, server_last_update, \
list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update%s FROM %s ORDER by server, listid",
				(g_ppromolite->m_settings.m_bChannelInfoViewHasDesc?", channel_description":""),
				(g_ppromolite->m_settings.m_pszChannelInfo?g_ppromolite->m_settings.m_pszChannelInfo:"ChannelInfo")
				);
		}
		else return PROMOLITE_ERROR;


if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_CHANNELS) 	
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Get Channels SQL: %s", szSQL );  // Sleep(50);//(Dispatch message)


		CPromoLiteAutomationChannelObject tempChannelObj;

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = PROMOLITE_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
/*
				CString szHost="";
				CString szClient="";
				CString szDesc="";
				CString szTemp;
				unsigned long ulFlags;   // various flags
			//	unsigned short usType;
				bool bFlagsFound = false;
*/
				try
				{
					if(tempChannelObj.m_pszServerName) free(tempChannelObj.m_pszServerName); 
					tempChannelObj.m_pszServerName = NULL;
					if(tempChannelObj.m_pszDesc) free(tempChannelObj.m_pszDesc); 
					tempChannelObj.m_pszDesc = NULL;
				}
				catch(...)
				{
					g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:Exception", "Get Channels exception freeing temp var" );  // Sleep(50);//(Dispatch message)
				}

				int nTemp = -1;
				CString szServer = "";
				bool bFound = false;

				try
				{
					CString szTemp;
					prs->GetFieldValue("ID", szTemp);
					tempChannelObj.m_nChannelID = atoi(szTemp);
					
					if(ulAutoType == PROMOLITE_DEP_AUTO_SENTINEL)
					{
						prs->GetFieldValue("server", szServer);
						tempChannelObj.m_pszServerName = (char*)malloc(szServer.GetLength()+1);
						if( tempChannelObj.m_pszServerName ) sprintf(tempChannelObj.m_pszServerName, "%s", szServer);

						prs->GetFieldValue("listid", szTemp);
						tempChannelObj.m_nHarrisListNum = atoi(szTemp);
					}
					else
					if(ulAutoType == PROMOLITE_DEP_AUTO_HELIOS)
					{
/*
						prs->GetFieldValue("conn_ip", szServer);
						tempChannelObj.m_pszServerName = (char*)malloc(szServer.GetLength()+1);
						if( tempChannelObj.m_pszServerName ) sprintf(tempChannelObj.m_pszServerName, "%s", szServer);

						prs->GetFieldValue("conn_port", szTemp);
						tempChannelObj.m_nOmnibusPort = atoi(szTemp);
*/
						prs->GetFieldValue("server", szServer);
						tempChannelObj.m_pszServerName = (char*)malloc(szServer.GetLength()+1);
						if( tempChannelObj.m_pszServerName ) sprintf(tempChannelObj.m_pszServerName, "%s", szServer);

						prs->GetFieldValue("listid", szTemp);
						tempChannelObj.m_nHarrisListNum = atoi(szTemp);  //false, but not used.
					}

					prs->GetFieldValue("server_time", szTemp);
					tempChannelObj.m_dblServertime = atof(szTemp);

					prs->GetFieldValue("server_status", szTemp);
					tempChannelObj.m_nServerStatus = atoi(szTemp);

					prs->GetFieldValue("server_basis", szTemp);
					tempChannelObj.m_nServerBasis = atoi(szTemp);

					prs->GetFieldValue("server_changed", szTemp);
					tempChannelObj.m_nServerChanged = atoi(szTemp);

					prs->GetFieldValue("server_last_update", szTemp);
					tempChannelObj.m_dblServerLastUpdate = atof(szTemp);

					prs->GetFieldValue("list_state", szTemp);
					tempChannelObj.m_nListState = atoi(szTemp);

					prs->GetFieldValue("list_changed", szTemp);
					tempChannelObj.m_nListChanged = atoi(szTemp);

					prs->GetFieldValue("list_display", szTemp);
					tempChannelObj.m_nListDisplay = atoi(szTemp);

					prs->GetFieldValue("list_syschange", szTemp);
					tempChannelObj.m_nListSysChange = atoi(szTemp);

					prs->GetFieldValue("list_count", szTemp);
					tempChannelObj.m_nListCount = atoi(szTemp);

					prs->GetFieldValue("list_lookahead", szTemp);
					tempChannelObj.m_nListLookahead = atoi(szTemp);

					prs->GetFieldValue("list_last_update", szTemp);
					tempChannelObj.m_dblListLastUpdate = atof(szTemp);

					if(g_ppromolite->m_settings.m_bChannelInfoViewHasDesc)
					{
						prs->GetFieldValue("channel_description", szTemp);
						if(szTemp.GetLength())
						{
if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_CHANNELS) 	
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "Channel [%s] obtained", szTemp );  // Sleep(50);//(Dispatch message)

							tempChannelObj.m_pszDesc = (char*)malloc(szTemp.GetLength()+1);
							if( tempChannelObj.m_pszDesc ) sprintf(tempChannelObj.m_pszDesc, "%s", szTemp);
if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_CHANNELS) 	
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "temp channel object has [%s]", tempChannelObj.m_pszDesc );  // Sleep(50);//(Dispatch message)
						}
					}


				}
				catch( ... )
				{
//if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_CHANNELS) 	
					g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:Exception", "Get Channels SQL: %s\r\nException: %s", szSQL, (pszInfo?pszInfo:"") );  // Sleep(50);//(Dispatch message)

				}

				if((pEndObj->m_ppChannelObj)&&(pEndObj->m_nNumChannelObjects))
				{
					nTemp=0;
					while(nTemp<pEndObj->m_nNumChannelObjects)
					{
//			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "c2");  Sleep(250); //(Dispatch message)
						if(pEndObj->m_ppChannelObj[nTemp])
						{
							if(
								  (szServer.GetLength()>0)
								&&(
										(					
											(ulAutoType == PROMOLITE_DEP_AUTO_SENTINEL)
										&&(szServer.CompareNoCase(pEndObj->m_ppChannelObj[nTemp]->m_pszServerName)==0)
										&&(tempChannelObj.m_nHarrisListNum == pEndObj->m_ppChannelObj[nTemp]->m_nHarrisListNum)
										)
									||(					
											(ulAutoType == PROMOLITE_DEP_AUTO_HELIOS)
										&&(tempChannelObj.m_nChannelID == pEndObj->m_ppChannelObj[nTemp]->m_nChannelID)
										)
									)
								)
							{
							
								bFound = true;
								// update description
								if(g_ppromolite->m_settings.m_bChannelInfoViewHasDesc)
								{
									if(
											(!pEndObj->m_ppChannelObj[nTemp]->m_pszDesc)&&(tempChannelObj.m_pszDesc)
										  ||((pEndObj->m_ppChannelObj[nTemp]->m_pszDesc)&&(tempChannelObj.m_pszDesc)&&(strcmp(pEndObj->m_ppChannelObj[nTemp]->m_pszDesc, tempChannelObj.m_pszDesc)))
										)
									{
if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_CHANNELS) 	
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "updating description temp [%s]", tempChannelObj.m_pszDesc );  // Sleep(50);//(Dispatch message)


										char* pch = pEndObj->m_ppChannelObj[nTemp]->m_pszDesc;
										pEndObj->m_ppChannelObj[nTemp]->m_pszDesc = tempChannelObj.m_pszDesc;
										tempChannelObj.m_pszDesc = NULL;
										try{ if(pch) free(pch);} 
										catch(...){	if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_CHANNELS) 	g_ppromolite->m_msgr.DM(MSG_ICONERROR, NULL, "PromoLite:debug", "exception freeing memory" );}
									}
								}
//			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "c3");  Sleep(250); //(Dispatch message)
								pEndObj->m_ppChannelObj[nTemp]->m_bKillAutomationThread = false;
								pEndObj->m_ppChannelObj[nTemp]->m_pPromoLite = g_ppromolite;
								pEndObj->m_ppChannelObj[nTemp]->m_pEndpoint = pEndObj;

//								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning analysis for %s:%d...", szServer, tempChannelObj.m_nHarrisListID);
//								g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLite:destination_change", errorstring);    //(Dispatch message)
								if(pEndObj->m_ppChannelObj[nTemp]->m_bAutomationThreadStarted == false)
								{

									//**** if connect set following status
									if(_beginthread(PromoLiteAutomationThread, 0, (void*)pEndObj->m_ppChannelObj[nTemp])==-1)
									{
										//error.

										//**MSG
									}
									//**** should check return value....
								}

/*
								Sleep(30);
								if(pEndObj->m_ppChannelObj[nTemp]->m_bNearAnalysisThreadStarted == false)
								{
									if(_beginthread(PromoLiteNearAnalysisThread, 0, (void*)pEndObj->m_ppChannelObj[nTemp])==-1)
									{
										//error.

										//**MSG
									}
									//**** should check return value....
								}
*/
								Sleep(30);
								if(pEndObj->m_ppChannelObj[nTemp]->m_bTriggerThreadStarted == false)
								{
									if(_beginthread(PromoLiteTriggerThread, 0, (void*)pEndObj->m_ppChannelObj[nTemp])==-1)
									{
										//error.

										//**MSG
									}
									//**** should check return value....
								}
								Sleep(30);

								pEndObj->m_ppChannelObj[nTemp]->m_ulFlags = PROMOLITE_FLAG_FOUND;

							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szServer.GetLength()>0)) // have to add.
				{
//			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "adding %s", szHost);  Sleep(250); //(Dispatch message)
					CPromoLiteAutomationChannelObject* pscno = new CPromoLiteAutomationChannelObject;
					if(pscno)
					{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CPromoLiteAutomationChannelObject** ppObj = new CPromoLiteAutomationChannelObject*[pEndObj->m_nNumChannelObjects+1];
						if(ppObj)
						{
//g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((pEndObj->m_ppChannelObj)&&(pEndObj->m_nNumChannelObjects>0))
							{
								while(o<pEndObj->m_nNumChannelObjects)
								{
									ppObj[o] = pEndObj->m_ppChannelObj[o];
									o++;
								}
								delete [] pEndObj->m_ppChannelObj;

							}
							ppObj[o] = pscno;
							pEndObj->m_ppChannelObj = ppObj;
							pEndObj->m_nNumChannelObjects++;

//							ppObj[o]->m_pszServerName = (char*)malloc(szServer.GetLength()+1); 
//							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, "%s", szServer);
							ppObj[o]->m_pszServerName = tempChannelObj.m_pszServerName;  // just assign it;
							tempChannelObj.m_pszServerName = NULL;//and reset it

						
							ppObj[o]->m_bKillAutomationThread = false;
							ppObj[o]->m_pPromoLite = g_ppromolite;
							ppObj[o]->m_pEndpoint = pEndObj;


							ppObj[o]->m_nChannelID = tempChannelObj.m_nChannelID;  // the unique Channel ID within Sentinel setup. (assigned externally)
							ppObj[o]->m_nHarrisListNum = tempChannelObj.m_nHarrisListNum;  // the 1-based List # on the associated ADC-100 server
							ppObj[o]->m_nOmnibusPort = tempChannelObj.m_nOmnibusPort; // the Omnibus port number

							ppObj[o]->m_dblServertime = tempChannelObj.m_dblServertime; // in number of ms since epoch
							ppObj[o]->m_dblServerLastUpdate = tempChannelObj.m_dblServerLastUpdate; // in number of ms since epoch
							ppObj[o]->m_nServerStatus = tempChannelObj.m_nServerStatus;
							ppObj[o]->m_nServerBasis = tempChannelObj.m_nServerBasis;
							ppObj[o]->m_nServerChanged = tempChannelObj.m_nServerChanged;

							ppObj[o]->m_dblListLastUpdate = tempChannelObj.m_dblListLastUpdate;   // in number of ms since epoch
							ppObj[o]->m_nListState = tempChannelObj.m_nListState;
							ppObj[o]->m_nListChanged = -1; //tempChannelObj.m_nListChanged; // forces change
							ppObj[o]->m_nListDisplay = tempChannelObj.m_nListDisplay;
							ppObj[o]->m_nListSysChange = tempChannelObj.m_nListSysChange;
							ppObj[o]->m_nListCount = tempChannelObj.m_nListCount;
							ppObj[o]->m_nListLookahead = tempChannelObj.m_nListLookahead;

							if((g_ppromolite->m_settings.m_bChannelInfoViewHasDesc)&&(tempChannelObj.m_pszDesc))
							{

								ppObj[o]->m_pszDesc = tempChannelObj.m_pszDesc; // just assign it;
								tempChannelObj.m_pszDesc = NULL;//and reset it

if(g_ppromolite->m_settings.m_ulDebug&PROMOLITE_DEBUG_CHANNELS) 	
	g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "new object description [%s]", ppObj[o]->m_pszDesc );  // Sleep(50);//(Dispatch message)
							}


							if(ppObj[o]->m_bAutomationThreadStarted == false)
							{
//								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning analysis for %s:%d...", szServer, tempChannelObj.m_nHarrisListID);
//								g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLite:destination_change", errorstring);    //(Dispatch message)

								//**** if connect set following status
								if(_beginthread(PromoLiteAutomationThread, 0, (void*)ppObj[o])==-1)
								{
									//error.

									//**MSG
								}
								//**** should check return value....
							}

/*
							Sleep(30);
							if(ppObj[o]->m_bNearAnalysisThreadStarted == false)
							{
								if(_beginthread(PromoLiteNearAnalysisThread, 0, (void*)ppObj[o])==-1)
								{
									//error.

									//**MSG
								}
								//**** should check return value....
							}
*/
							Sleep(30);
							if(ppObj[o]->m_bTriggerThreadStarted == false)
							{
								if(_beginthread(PromoLiteTriggerThread, 0, (void*)ppObj[o])==-1)
								{
									//error.

									//**MSG
								}
								//**** should check return value....
							}
							Sleep(30);

							ppObj[o]->m_ulFlags = PROMOLITE_FLAG_FOUND;
							
						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<pEndObj->m_nNumChannelObjects)
			{
				if((pEndObj->m_ppChannelObj)&&(pEndObj->m_ppChannelObj[nIndex]))
				{
					if((pEndObj->m_ppChannelObj[nIndex]->m_ulFlags)&PROMOLITE_FLAG_FOUND)
					{
						(pEndObj->m_ppChannelObj[nIndex]->m_ulFlags) &= ~PROMOLITE_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(pEndObj->m_ppChannelObj[nIndex])
						{
							if(ulAutoType == PROMOLITE_DEP_AUTO_SENTINEL)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending analysis and triggering for %s:%d...", 
														pEndObj->m_ppChannelObj[nIndex]->m_pszServerName,pEndObj->m_ppChannelObj[nIndex]->m_nHarrisListNum );  
							}
							else
							if(ulAutoType == PROMOLITE_DEP_AUTO_HELIOS)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending analysis and triggering for Omnibus stream %d...", 
														pEndObj->m_ppChannelObj[nIndex]->m_nChannelID );  
							}
							g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLite:destination_remove", errorstring);    //(Dispatch message)
//									pEndObj->m_ppChannelObj[nTemp]->m_pDlg->OnDisconnect();

							//**** disconnect
							pEndObj->m_ppChannelObj[nIndex]->m_bKillAutomationThread = true;

							int ixi = 0;
							while (
											(
												(pEndObj->m_ppChannelObj[nIndex]->m_bAutomationThreadStarted)
											||(pEndObj->m_ppChannelObj[nIndex]->m_bTriggerThreadStarted)	
//											||(pEndObj->m_ppChannelObj[nIndex]->m_bNearAnalysisThreadStarted)	
											)
										&&(ixi<500) // half a second max, this stalls the get thread.
										)
							{
								ixi++;
								Sleep(1);
							}
							//g_adc.DisconnectServer(pEndObj->m_ppChannelObj[nIndex]->m_pszServerName);
							//**** should check return value....
							if(ulAutoType == PROMOLITE_DEP_AUTO_SENTINEL)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ended analysis and triggering for %s:%d...", 
													pEndObj->m_ppChannelObj[nIndex]->m_pszServerName,pEndObj->m_ppChannelObj[nIndex]->m_nHarrisListNum );  
							}
							else
							if(ulAutoType == PROMOLITE_DEP_AUTO_HELIOS)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ended analysis and triggering for  Omnibus stream %d...", 
														pEndObj->m_ppChannelObj[nIndex]->m_nChannelID );  
							}
							g_ppromolite->m_msgr.DM(MSG_ICONINFO, NULL, "PromoLite:destination_remove", errorstring);    //(Dispatch message)
							

							delete pEndObj->m_ppChannelObj[nIndex];
							pEndObj->m_nNumChannelObjects--;

							int nTemp=nIndex;
							while(nTemp<pEndObj->m_nNumChannelObjects)
							{
								pEndObj->m_ppChannelObj[nTemp]=pEndObj->m_ppChannelObj[nTemp+1];
								nTemp++;
							}
							pEndObj->m_ppChannelObj[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

//			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
	}
//			g_ppromolite->m_msgr.DM(MSG_ICONHAND, NULL, "PromoLite:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return PROMOLITE_ERROR;
}





