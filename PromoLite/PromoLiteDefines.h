// PromoLiteDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(PROMOLITEDEFINES_H_INCLUDED)
#define PROMOLITEDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define PROMOLITE_CURRENT_VERSION		"2.3.0.3"


// modes
#define PROMOLITE_MODE_DEFAULT			0x00000000  // exclusive
#define PROMOLITE_MODE_LISTENER			0x00000001  // exclusive
#define PROMOLITE_MODE_CLONE				0x00000002  // exclusive
#define PROMOLITE_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define PROMOLITE_MODE_VOLATILE			0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define PROMOLITE_MODE_MASK					0x0000000f  // 

// default port values.
//#define PROMOLITE_PORT_FILE				80		
#define PROMOLITE_PORT_CMD					20680		
#define PROMOLITE_PORT_STATUS				20681		

#define PROMOLITE_PORT_INVALID			0	

#define PROMOLITE_FLAGS_INVALID			0xffffffff	 
#define PROMOLITE_FLAG_DISABLED			0x0000	 // default
#define PROMOLITE_FLAG_ENABLED			0x0001	
#define PROMOLITE_FLAG_FOUND				0x1000

#define PROMOLITE_FLAG_TRIGGERED			0x00000001  // VALUES are different from Nucleus
#define PROMOLITE_FLAG_ERROR					0x00010000  // VALUES are different from Nucleus
#define PROMOLITE_FLAG_NOTRUN					0x00020000  // VALUES are different from Nucleus

#define PROMOLITE_FLAG_ANALYZED				0x00000010
#define PROMOLITE_FLAG_REANALYZED			0x00000020

#define PROMOLITE_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded

// installed dependencies
#define PROMOLITE_DEP_UNKNOWN								0x0000  // type unknown
#define PROMOLITE_DEP_DATA_ARCHIVIST				0x0002  // Archivist is installed
#define PROMOLITE_DEP_DATA_MASK							0x0002  // mask for data bit  // there is only one choice at the moment!
#define PROMOLITE_DEP_AUTO_HELIOS						0x0010  // Omnibus is installed (Helios)
#define PROMOLITE_DEP_AUTO_SENTINEL					0x0020  // Harris is installed (Sentinel)
#define PROMOLITE_DEP_AUTO_MASK							0x00f0  // mask for automation bit
#define PROMOLITE_DEP_EDGE_PROSPERO					0x0100  // Miranda is installed (Prospero)
#define PROMOLITE_DEP_EDGE_LUMINARY					0x0200  // Chyron (CAL) is installed (Luminary)
#define PROMOLITE_DEP_EDGE_RADIANCE					0x0300  // Orad is installed (Radiance)
#define PROMOLITE_DEP_EDGE_LIBRETTO					0x0400  // Chyron (Lyric) is installed (Libretto)
#define PROMOLITE_DEP_EDGE_BARBERO					0x0500  // Evertz inerface FTP
#define PROMOLITE_DEP_EDGE_MASK							0x0f00  // mask for edge device bit (device to have stuff transferred to)

// rule types
#define PROMOLITE_RULE_TYPE_UNKNOWN						0x00000000  // type unknown
#define PROMOLITE_RULE_TYPE_PRESMASTER				0x00000001  // for Helios, with presmaster data in <data> XML field
#define PROMOLITE_RULE_TYPE_XML								0x00000002  // for Helios, with logo data in <logos> XML sub-fields .. free form tho
#define PROMOLITE_RULE_TYPE_IMAGESTORE				0x00000003  // type unknown

//comparison types
/*
create table LU_Comparison_Type (compare_type char(2) NOT NULL, name varchar(32), description varchar(256));
insert into LU_Comparison_Type (compare_type, name, description) values ('==', '<code>&#61;</code>', 'Equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('>', '<code>&#62;</code>', 'Greater than');
insert into LU_Comparison_Type (compare_type, name, description) values ('<', '<code>&#60;</code>', 'Less than');
insert into LU_Comparison_Type (compare_type, name, description) values ('>=', '<code>&#8805;</code>', 'Greater than or equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('<=', '<code>&#8804;</code>', 'Less than or equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('!=', '<code>&#8800;</code>', 'Not equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('~=', '<code>&#8776;</code>', 'Partial Comparison');
insert into LU_Comparison_Type (compare_type, name, description) values ('&', '<code>&#8715;</code>', 'Contains any bitflags');
insert into LU_Comparison_Type (compare_type, name, description) values ('=&', '<code>&#8839;</code>', 'Contains all bitflags');
insert into LU_Comparison_Type (compare_type, name, description) values ('!&', '<code>&#8713;</code>', 'Does not contain bitflags');
*/
#define PROMOLITE_RULE_COMPARE_UNKNOWN			0xffff  // type unknown
#define PROMOLITE_RULE_COMPARE_EQUALS				0x0001  // == equals
#define PROMOLITE_RULE_COMPARE_GT						0x0002  // >  greater than
#define PROMOLITE_RULE_COMPARE_LT						0x0003  // <  less than
#define PROMOLITE_RULE_COMPARE_GTOE					0x0004  // >= greater than or equal to
#define PROMOLITE_RULE_COMPARE_LTOE					0x0005  // <= less than or equal to
#define PROMOLITE_RULE_COMPARE_NOTEQUAL			0x0006  // != equals
#define PROMOLITE_RULE_COMPARE_PARTIAL			0x0007  // ~= partial comparison
#define PROMOLITE_RULE_COMPARE_AND					0x0008  // &  contains any of the bitflags
#define PROMOLITE_RULE_COMPARE_ALL					0x0009  // =& contains all the bitflags
#define PROMOLITE_RULE_COMPARE_NOT					0x000a  // !& does not contain any the bitflag


//destination types (decimal)
#define PROMOLITE_RULE_DESTTYPE_UNKNOWN							0000  // type unknown

#define PROMOLITE_RULE_DESTTYPE_ORAD_DVG						1000  // Orad DVG
#define PROMOLITE_RULE_DESTTYPE_ORAD_HDVG						1001  // Orad HDVG

#define PROMOLITE_RULE_DESTTYPE_MIRANDA_IS2					2001  // Imagestore 2
#define PROMOLITE_RULE_DESTTYPE_MIRANDA_INT					2002  // Intuition
#define PROMOLITE_RULE_DESTTYPE_MIRANDA_IS300				2003  // Imagestore 300
#define PROMOLITE_RULE_DESTTYPE_MIRANDA_ISHD				2004  // Imagestore HD

#define PROMOLITE_RULE_DESTTYPE_CHYRON_LEX					3000	// Duet LEX
#define PROMOLITE_RULE_DESTTYPE_CHYRON_HYPERX				3001  // Duet HyperX
#define PROMOLITE_RULE_DESTTYPE_CHYRON_MICROX				3002  // Duet MicroX
#define PROMOLITE_RULE_DESTTYPE_CHYRON_CHANNELBOX		3003  // Channel Box
#define PROMOLITE_RULE_DESTTYPE_CHYRON_CALBOX				3004  // CAL Box

#define PROMOLITE_RULE_DESTTYPE_EVERTZ_9625LG				4000 
#define PROMOLITE_RULE_DESTTYPE_EVERTZ_9625LGA			4001
#define PROMOLITE_RULE_DESTTYPE_EVERTZ_9725LGA			4002
#define PROMOLITE_RULE_DESTTYPE_EVERTZ_9725LG				4003
#define PROMOLITE_RULE_DESTTYPE_EVERTZ_HD9625LG			4004
#define PROMOLITE_RULE_DESTTYPE_EVERTZ_HD9625LGA		4005
#define PROMOLITE_RULE_DESTTYPE_EVERTZ_HD9725LG			4006
#define PROMOLITE_RULE_DESTTYPE_EVERTZ_HD9725LGA		4007

#define PROMOLITE_RULE_DESTTYPE_HARRIS_ICONII				5000


#define PROMOLITE_HARRIS_ICONII_LSFORMAT_NUM4				0 // 4 digit numerical LayoutSalvoFormat
#define PROMOLITE_HARRIS_ICONII_LSFORMAT_NUM8				1 // 8 digit numerical LayoutSalvoFormat
#define PROMOLITE_HARRIS_ICONII_LSFORMAT_ALPHA			2 // alphanumerical string LayoutSalvoFormat


//search types
#define PROMOLITE_RULE_SEARCH_UNKNOWN				0xffff  // type unknown
#define PROMOLITE_RULE_SEARCH_EXPLICIT				0x0000  // search for the exact file only
#define PROMOLITE_RULE_SEARCH_AUDIO					0x0001  // search for corresponding audio file
#define PROMOLITE_RULE_SEARCH_VIDEO					0x0002  // search for corresponding video file
#define PROMOLITE_RULE_SEARCH_AV							0x0003  // search for corresponding audio and video files

//action types
#define PROMOLITE_RULE_ACTION_UNKNOWN				0xffff  // type unknown
#define PROMOLITE_RULE_ACTION_NORMAL					0x0000  // just error if nothing found...
#define PROMOLITE_RULE_ACTION_NULLOXT				0x0001  // if not video file found, create null OXT


#define PROMOLITE_EVENTSTATUS_NONE						0x00000000 //nothing found.
#define PROMOLITE_EVENTSTATUS_AUDIO					0x00001000 //audio found.
#define PROMOLITE_EVENTSTATUS_VIDEO					0x00002000 //video found.
#define PROMOLITE_EVENTSTATUS_FONT						0x00004000 //fonts found. 
#define PROMOLITE_EVENTSTATUS_TEM						0x00008000 //tem found
#define PROMOLITE_EVENTSTATUS_TEMLOC					0x00010000 //tem retrieved locally
#define PROMOLITE_EVENTSTATUS_SUBMASK				0x00000fff //mask - tem subelements enumerated.
#define PROMOLITE_EVENTSTATUS_ERRORSENT_XFER	0x00100000 //an error was sent out already indicating transfer failure
#define PROMOLITE_EVENTSTATUS_ERRORSENT_META	0x00200000 //an error was sent out already indicating no metadata
#define PROMOLITE_EVENTSTATUS_ERRORSENT_NOXT	0x00400000 //an error was sent out already indicating no null oxt file found


// status
#define PROMOLITE_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define PROMOLITE_STATUS_UNKNOWN							0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define PROMOLITE_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define PROMOLITE_STATUS_ERROR								0x00000020  // error (red icon)
#define PROMOLITE_STATUS_CONN								0x00000030  // ready (green icon)	
#define PROMOLITE_STATUS_OK									0x00000030  // ready (green icon)	
#define PROMOLITE_STATUS_RUN									0x00000040  // in progress, running, owned etc (blue icon);	
#define PROMOLITE_ICON_MASK									0x00000070  // mask	

#define PROMOLITE_STATUS_SUSPEND							0x00000080  // suspended	(yellow icon please)

#define PROMOLITE_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define PROMOLITE_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define PROMOLITE_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define PROMOLITE_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define PROMOLITE_STATUS_CMDSVR_MASK					0x00007000  // command server mask bits

#define PROMOLITE_STATUS_STATUSSVR_START			0x00010000  // starting the status server
#define PROMOLITE_STATUS_STATUSSVR_RUN				0x00020000  // status server running
#define PROMOLITE_STATUS_STATUSSVR_END				0x00030000  // status server shutting down
#define PROMOLITE_STATUS_STATUSSVR_ERROR			0x00040000  // status server error
#define PROMOLITE_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define PROMOLITE_STATUS_THREAD_START				0x00100000  // starting the main thread
#define PROMOLITE_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define PROMOLITE_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define PROMOLITE_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define PROMOLITE_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define PROMOLITE_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define PROMOLITE_STATUS_THREAD_MASK					0x00f00000  // main thread mask bits

// various failures...
#define PROMOLITE_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define PROMOLITE_STATUS_FAIL_DB							0x20000000  // could not get DB
#define PROMOLITE_STATUS_FAIL_MASK						0xf0000000  // failure code mask bits

//return values
#define PROMOLITE_SUCCESS   0
#define PROMOLITE_ERROR	   -1

// commands

#define PROMOLITE_CMD_GETSCENE					0xa0 // gets scene info
#define PROMOLITE_CMD_GETTHUMB					0xa1 // gets a thumbnail
#define PROMOLITE_CMD_GETMEDIA					0xa2 // gets media list from a destination

#define PROMOLITE_CMD_QUEUE							0xc0 // puts thing in queue and waits for response
#define PROMOLITE_CMD_QUEUE_ASYNC				0xc1 // puts thing in queue
#define PROMOLITE_CMD_QUEUE_QUERY				0xc2 // retrieves answer from queue (has deletion option)
#define PROMOLITE_CMD_CMDQUEUE					0xc5 // puts thing in command queue and waits for response
#define PROMOLITE_CMD_CMDQUEUE_ASYNC		0xc6 // puts thing in command queue and returns immediately
#define PROMOLITE_CMD_CMDQUEUE_QUERY		0xc7 // retrieves answer from queue (has deletion option)
#define PROMOLITE_CMD_DIRECT						0xcf // skips queues, commands box directly

#define PROMOLITE_CMD_EXSET							0xea // sets exchange counter to a specific value
#define PROMOLITE_CMD_EXINC							0xeb // increments exchange counter
#define PROMOLITE_CMD_EXGET							0xec // gets exchange mod value
#define PROMOLITE_CMD_MODSET						0xed // sets exchange mod value, skips exchange table
#define PROMOLITE_CMD_MODINC						0xee // increments exchange mod value, skips exchange table


// default filenames
#define PROMOLITE_SETTINGS_FILE_SETTINGS	  "promolite.csr"		// csr = cortex settings redirect
#define PROMOLITE_SETTINGS_FILE_DEFAULT	  "promolite.csf"		// csf = cortex settings file

// debug defines
#define PROMOLITE_DEBUG_TRIGGER				0x00000001
#define PROMOLITE_DEBUG_ANALYZE				0x00000002
#define PROMOLITE_DEBUG_RULES					0x00000004
#define PROMOLITE_DEBUG_PARAMS				0x00000008
#define PROMOLITE_DEBUG_PARAMPARSE		0x00000010
#define PROMOLITE_DEBUG_TIMING				0x00000020
#define PROMOLITE_DEBUG_EVENTRULE			0x00000040
#define PROMOLITE_DEBUG_PROCESS				0x00000080
#define PROMOLITE_DEBUG_CHANNELS			0x00000100
#define PROMOLITE_DEBUG_COMM					0x00000200
#define PROMOLITE_DEBUG_DONE					0x00000400


#endif // !defined(PROMOLITEDEFINES_H_INCLUDED)
