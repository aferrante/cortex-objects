// InjectorSettings.h: interface for the CInjectorSettings class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INJECTORSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
#define AFX_INJECTORSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "InjectorDefines.h"
#include "../../Common/MFC/ODBC/DBUtil.h"

class CInjectorSettings  
{
public:
	CInjectorSettings();
	virtual ~CInjectorSettings();

//	int InsertSetting(ChannelSetting_t* pSetting, int index);
//	int DeleteSetting(int index);  // removes from array
	int Settings(bool bRead);
	int ChannelSettings(bool bRead);
	int GetFromDatabase(char* pszInfo=NULL);  //only get.  Interface publishes
	char* ProcessString(char* pszString, bool bFreeIncomingString);

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;

	char* m_pszName;  // familiar name of this instance (and therefore the name of the Injector database)
	char* m_pszType;  // familiar name of the type.
	char* m_pszProject;  // familiar name of the project name.
	unsigned long m_ulMainMode;

	// ports
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;


//	ChannelSetting_t** m_ppChannelSetting;
//	int m_nChannelSettings;
	int m_nNumChannels;

	// The following flags determine what settings are loaded. 
	// The specifics do not need to be stored in cortex settings, they are pushed to the various objects.

	// messaging for Injector
	bool m_bUseLog;			// write a log file
	bool m_bUseEmail;		// send an email on failures (or commanded from remotes)
	bool m_bUseNetwork; // send a message to a remote host
	bool m_bReportSuccessfulOperation;
	bool m_bLogTransfers;
	bool m_bLogNetworkErrors; // enables messaging into network object (logs socket errors, etc).
	char* m_pszFileSpec;
	char* m_pszMailSpec;
	char* m_pszProcessedFileSpec;
	char* m_pszProcessedMailSpec;
	bool m_bUseXMLClientLog;			// write a log file
	bool m_bMillisecondMessaging;			// use millisecond resolution for messages and asrun

	int m_nThreadDwellMS;
	bool m_bDebugLists;  // if true, dumps the temp and events databases
	int  m_nDebugListTopCount;  //if positive, just prints out the top n recored in the events tables.
	bool m_bDebugSQL;  // if true, prints out the SQL calls, all but event inserts
	bool m_bDebugInsertSQL;  // if true, prints out the event insert SQL calls
	unsigned long m_ulDebug;  // prints out debug statements that & with this.
	char* m_pszDebugOrder; // the order by clause for debug


	int m_nAutoPurgeMessageDays;
	int m_nAutoPurgeAsRunDays;
	int m_nAutoPurgeInterval;

	bool m_bUseQueue;			// use the queue for commands


	// automation
	int m_nTriggerAdvanceMS; // number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)
	int m_nAnalyzeAutomationDwellMS; // number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once.
	int m_nAnalyzeAutomationForceMS; // number of milliseconds after a change, that if there are changes pending, we MUST be force analysis 
	bool m_bUseLocalClock;  // use the computer's clock, not the transmitted server time
	bool m_bUseUTC; // use UTC time, not local/DST time.
	int m_nAutomationIntervalMS; // number of milliseconds between channel time checks
	bool m_bChannelInfoViewHasDesc;  // if true, obtains description

	int m_nProgramAutomationDwellMS; // number of milliseconds programming is delayed after a change (prevents hammering programming when a bunch of list changes happen all at once.
	int m_nProgramAutomationForceMS; // number of milliseconds after a change, that if there are changes pending, we MUST be force programming 
	int m_nProgramAutomationSafetyMS; // number of milliseconds before the next primary plays, that we attempt programming (need some quiet time to get it done right) 
	int m_nProgramAutomationDefeatMS; // number of milliseconds after an automatic insertion or deletion, to wait before addressing the programming stack again.  This staggers the insertions so if you append a large list, and there are lots of changes all at once, the changes NOT induced by the insertions do not allow more insertions to happen without considering the inserted items.
//	int m_nProgramAutomationIntervalMS; // number of milliseconds between schedule db change checks

	int m_nProgramAutomationTrimMidnightMS; // number of milliseconds to trim items that end at midnight (so they don't go into the next day in the visible schedule)
	unsigned long m_ulAutomationTimeToleranceMS;  // an on air time must be plus or minus this many milliseconds to be considered identical

	int m_nAdvanceSchedulingThresholdMaxDays;
	int m_nAdvanceSchedulingThresholdMinMins;
	int m_nSchedulingIncrementMins;
	int m_nMinSchedulingThresholdMS;
	int m_nTrafficModInterval;  //seconds
	bool m_bExtraRestrictionInfo;		
	bool m_bInsertionTitleReplace;		


	// file handling

	// database stuff
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;
	char* m_pszDatabase;

	char* m_pszSettings;  // the Settings table name
	char* m_pszExchange;  // the Exchange table name
	char* m_pszMessages;  // the Messages table name
	char* m_pszCommandQueue;			// the Command Queue table name
	char* m_pszQueue;			// the Queue table name
	char* m_pszChannelInfo;  // the m_ChannelInfo view name
	char* m_pszAsRun;  // the As-run table name
	char* m_pszSchedule;  // the Schedule table name
	char* m_pszRestrictions;  // the Restrictions table name

// other settings
	unsigned long m_ulModsIntervalMS;  // interval on which to check database mods

	char* m_pszLicense;  // the License Key
	char* m_pszIconPath;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

	// Sentinel network comm
	char* m_pszSentinelHost;  // name of the server on which sentinel is running
	unsigned short m_usSentinelPort;  // port on which Sentinel is listening.
	char* m_pszSentinelDBName;  // the Sentinel database name
	char* m_pszSentinelTraffic;  // the Sentinel traffic table name
	char* m_pszSentinelEvents;  // the Sentinel events table name
	char* m_pszSentinelExchange;  // the Sentinel exchange table name
	unsigned long m_ulSentinelPingIntervalMS;  // interval on which to ping Sentinel to see if it is there
	unsigned long m_ulSentinelRetryIntervalMS;  // interval on which to try reconnecting if disconnected
	unsigned long m_ulSentinelErrorIntervalMS;  // interval after disconnected that it becomes an error

	// operating mode
	bool m_bAutoSchedulerMode;
	bool m_bManualInjectorMode;
	bool m_bSuppressListEdits; // debug mode - do everything except edit and delete from list
	bool m_bCompareRestrictions;  // otherwise replace them without restriction

};

#endif // !defined(AFX_INJECTORSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
