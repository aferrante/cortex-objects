// InjectorDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(INJECTORDEFINES_H_INCLUDED)
#define INJECTORDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define INJECTOR_CURRENT_VERSION		"1.0.1.5"

/*  removed. not used
typedef struct ChannelSetting_t
{
	char* pszDesc;
	char* pszString;
	int nValue;
	int nChannelUID;
} ChannelSetting_t;
*/

// modes
#define INJECTOR_MODE_DEFAULT			0x00000000  // exclusive
#define INJECTOR_MODE_LISTENER			0x00000001  // exclusive
#define INJECTOR_MODE_CLONE				0x00000002  // exclusive
#define INJECTOR_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define INJECTOR_MODE_VOLATILE			0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define INJECTOR_MODE_MASK					0x0000000f  // 

// default port values.
//#define INJECTOR_PORT_FILE				80		
#define INJECTOR_PORT_CMD					20660		
#define INJECTOR_PORT_STATUS				20661		

#define INJECTOR_PORT_INVALID			0	

#define INJECTOR_FLAG_DISABLED			0x0000	 // default
#define INJECTOR_FLAG_ENABLED				0x0001	
#define INJECTOR_FLAG_FOUND					0x1000
#define INJECTOR_FLAG_INSERTED			0x0010
#define INJECTOR_FLAG_SUPPRESSED		0x0020
#define INJECTOR_FLAG_OVERRIDDEN		0x0040
#define INJECTOR_FLAG_AUTOINIT			0x0100  //AUTOMATION is initialized
#define INJECTOR_FLAG_RESTRICTINIT	0x0200  //restrition thread is initialized

#define INJECTOR_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded


#define INJECTOR_FILEFLAG_RECORDONLY			0x00000000
#define INJECTOR_FILEFLAG_FILEXFER				0x00000001
#define INJECTOR_FILEFLAG_FILECOPIED			0x00000002
#define INJECTOR_FILEFLAG_ARCHIVED				0x00000004
#define INJECTOR_FILEFLAG_DELETED				0x00000008


// status
#define INJECTOR_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define INJECTOR_STATUS_UNKNOWN						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define INJECTOR_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define INJECTOR_STATUS_ERROR							0x00000020  // error (red icon)
#define INJECTOR_STATUS_CONN								0x00000030  // ready (green icon)	
#define INJECTOR_STATUS_OK									0x00000030  // ready (green icon)	
#define INJECTOR_STATUS_RUN								0x00000040  // in progress, running, owned etc (blue icon);	
#define INJECTOR_ICON_MASK									0x00000070  // mask	

#define INJECTOR_STATUS_SUSPEND						0x00000080  // suspended	(yellow icon please)

#define INJECTOR_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define INJECTOR_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define INJECTOR_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define INJECTOR_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define INJECTOR_STATUS_CMDSVR_MASK				0x00007000  // command server mask bits

#define INJECTOR_STATUS_STATUSSVR_START		0x00010000  // starting the status server
#define INJECTOR_STATUS_STATUSSVR_RUN			0x00020000  // status server running
#define INJECTOR_STATUS_STATUSSVR_END			0x00030000  // status server shutting down
#define INJECTOR_STATUS_STATUSSVR_ERROR		0x00040000  // status server error
#define INJECTOR_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define INJECTOR_STATUS_THREAD_START				0x00100000  // starting the main thread
#define INJECTOR_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define INJECTOR_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define INJECTOR_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define INJECTOR_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define INJECTOR_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define INJECTOR_STATUS_THREAD_MASK				0x00f00000  // main thread mask bits

// various failures...
#define INJECTOR_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define INJECTOR_STATUS_FAIL_DB						0x20000000  // could not get DB
#define INJECTOR_STATUS_FAIL_MASK					0xf0000000  // failure code mask bits

// some default values
#define INJECTOR_DEF_MAX_ALLOW_MS						3600000 // 1 hour
#define INJECTOR_DEF_MIN_BIFURC_MS					5000 // 5 sec
#define INJECTOR_DEF_POLL_MS								10000  // 10 sec


//return values
#define INJECTOR_RESTRICTED   1
#define INJECTOR_NOT_RESTRICTED   0
#define INJECTOR_SUCCESS   0
#define INJECTOR_ERROR	   -1
#define INJECTOR_FILE_EXISTS  -2

// restriction types
#define INJECTOR_RESTRICTION_WHOLEDAY   0
#define INJECTOR_RESTRICTION_WEEKDAY		1
#define INJECTOR_RESTRICTION_STARTDAY   2
#define INJECTOR_RESTRICTION_ENDDAY			3


// default filenames
#define INJECTOR_SETTINGS_FILE_SETTINGS	  "injector.csr"		// csr = cortex settings redirect
#define INJECTOR_SETTINGS_FILE_DEFAULT	  "injector.csf"		// csf = cortex settings file

// debug defines
#define INJECTOR_DEBUG_TIMING						0x00000001
#define INJECTOR_DEBUG_EXCHANGE					0x00000002
#define INJECTOR_DEBUG_CONNECT					0x00000004
#define INJECTOR_DEBUG_CONNECTSTATUS		0x00000008
#define INJECTOR_DEBUG_CHANNELS					0x00000010
#define INJECTOR_DEBUG_SCHEDULE					0x00000020
#define INJECTOR_DEBUG_PROGRAMMING			0x00000040
#define INJECTOR_DEBUG_COMM							0x00000200
#define INJECTOR_DEBUG_LIVEEVENTS				0x00001000
#define INJECTOR_DEBUG_TRAFFICEVENTS		0x00002000
#define INJECTOR_DEBUG_COMPARE					0x00004000
#define INJECTOR_DEBUG_CRITSEC					0x00008000



#endif // !defined(INJECTORDEFINES_H_INCLUDED)




/*
	scheduled_on is the on air time.
	create table Schedule (itemid int identity(1,1), channelid int, clipid varchar(64), segment int, scheduled_on decimal(20,3), duration int, clip_title varchar(256), notes varchar(4096), status int, created_on int, created_by varchar(32), traffic_file_id int);
*/