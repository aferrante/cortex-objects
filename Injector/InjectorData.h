// InjectorData.h: interface for the CInjectorData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INJECTORDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
#define AFX_INJECTORDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <winsock2.h>
#include <sys/timeb.h>
#include "../../Common/TXT/BufferUtil.h" 
#include "../../Common/LAN/NetUtil.h" 
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"
#include "../../Common/KEY/LicenseKey.h"
//#include "../../Common/FILE/DirUtil.h"



#define INJECTOR_ARRAY_SCHEDULE								0
#define INJECTOR_ARRAY_SCHEDULE_RESTRICTION		1
#define INJECTOR_ARRAY_TRAFFIC								2
#define INJECTOR_ARRAY_LIVE										3
#define INJECTOR_ARRAY_SCHEDULE_DISPLAY				4



#define INJECTOR_ARRAY_FIND_CONTAINED					0x00000000  // the passed in time in contained in the event
#define INJECTOR_ARRAY_FIND_AFTER 						0x00000001  // the event is after the passed in time
#define INJECTOR_ARRAY_FIND_AFTER_OR_AT				0x00000002  // the event is after or at the passed in time
#define INJECTOR_ARRAY_FIND_USETOLERANCE			0x00000100  // the event is after or at the passed in time
#define INJECTOR_ARRAY_FIND_TYPEMASK					0x0000000f
#define INJECTOR_ARRAY_FIND_NOTFOUND					0x00000010  // only find ones not already found.


// these are data reference objects, pulled from the database.
// actual connection, list, and event objects are in the CADC object



class CInjectorScheduleObject  
{
public:
	CInjectorScheduleObject();
	virtual ~CInjectorScheduleObject();

	int m_nID;
	int m_nChannelID;
	int m_nFileID;
	int m_nPosition;

	char* m_pszEventID;
	char* m_pszEventTitle;
	int m_nSegment;
	double m_dblOnAirTimeLocal;
	int m_nDurationMS;
	int m_nSOMMS;
	unsigned long m_ulStatus;  //event status, if used. in database
	// was this:
//	unsigned short m_usControl;
	// changed to this, to include extended control in the upper word
	unsigned long m_ulControl;
	unsigned short m_usType;

	char* m_pszCreator;
	char* m_pszRecKey;
	char* m_pszCaller;

	unsigned long m_ulFlags;  // flags we can set internally in injector

};


typedef struct SecondaryInfo_t
{
	char* pszID;          // the ID of the secondary to apply these settings to, found in secondaries file.
	int nBeginOffsetMS;   // ms to add to on air time offset
	int nEndOffsetMS;     // ms to subtract from duration after adjusted for air time offset
	bool bAdjustDurToPri; // adjust the secondaries duration to that of the primary, before offsets are applied (otherwise use as found)
} SecondaryInfo_t;



/*

class CInjectorConnectionObject  
{
public:
	CInjectorConnectionObject();
	virtual ~CInjectorConnectionObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	char* m_pszServerName;
	char* m_pszClientName;
	char* m_pszDesc;

//	CAConnection* m_pAPIConn;  // pointer to the associated API connection.

	// control
	bool m_bKillConnThread;
	bool m_bConnThreadStarted;
};

*/

class CInjectorUtilObject  
{
public:
	CInjectorUtilObject();
	virtual ~CInjectorUtilObject();
	int AddItem(CInjectorScheduleObject*** pppSched, int* pnCount, CInjectorScheduleObject* pItem);  // returns index
};


class CInjectorChannelObject  
{
public:
	CInjectorChannelObject();
	virtual ~CInjectorChannelObject();
	int InsertScheduleItem(CInjectorScheduleObject* pSchedObj, int nArray);
//	int DeleteScheduleItem(int index, int nArray);  // removes from array
	int FindScheduleItem(double dblTime, int nArray, int nFlags=INJECTOR_ARRAY_FIND_CONTAINED);  // returns index
	char* CreateRecKey(bool bMilliseconds=true);
	int IsRestricted(CInjectorScheduleObject* pSchedObj);
	int FindNextNonAllow(CInjectorScheduleObject* pSchedObj);

	int ADCInsertEventBuffer(unsigned char* pEvents, int nBufferLen, int nPosition); 
	int ADCDeleteEvents(int nPosition, int nNumber); 
	int SentinelPing(); 
	

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	int m_nChannelID;  // the unique Channel ID within Injector setup. (assigned externally)
	int m_nHarrisListNum;  // the 1-based List # on the associated ADC-100 server

	char* m_pszServerName;
	char* m_pszDesc;
	CRITICAL_SECTION m_critServerInfo;

	CADC m_adc;


//	CAConnection* m_pAPIConn;  // pointer to the associated API connection.

// control
//	bool* m_pbKillConnThread;
	bool m_bKillAutomationThread;
	bool m_bAutomationThreadStarted;
	bool m_bSchedulerThreadStarted;  // restrictions
	bool m_bProgrammingThreadStarted; // programming
	_timeb m_timebAutomationTick; // the last time check inside the thread
	_timeb m_timebRestrictionTick; // the last time check inside the thread
	_timeb m_timebProgrammingTick; // the last time check inside the thread
	_timeb m_timebLastProgramTick; // the last time a programming list change was commanded
	// temp
	double m_dblAutomationTimeEstimate; // the harris server time estimate in unixtim.ms
	double m_dblAutomationTimeDiffMS; // difference in milliseconds between the time harris was set and the time it was read


	//advisories
	bool m_bLiveEventsChanged;
	bool m_bTrafficEventsChanged;
	bool m_bLiveEventsScheduleChanged;

	CRITICAL_SECTION m_critAdvisories;


	double m_dblServertime; // in number of ms since epoch
	double m_dblServerLastUpdate; // in number of ms since epoch
	int m_nServerStatus;
	int m_nServerBasis;
	int m_nServerChanged;

	double m_dblListLastUpdate;   // in number of ms since epoch
	int m_nListState;
	int m_nListChanged;
	int m_nListDisplay;
	int m_nListSysChange;
	int m_nListCount;
	int m_nListLookahead;

	int m_nAnalysisCounter;


	CInjectorScheduleObject** m_ppScheduleRestrictions;
	int m_nNumScheduleRestrictions;
	CRITICAL_SECTION m_critScheduleRestrictions;

	CInjectorScheduleObject** m_ppLiveEvents;
	int m_nNumLiveEvents;
	CRITICAL_SECTION m_critLiveEvents;

	CInjectorScheduleObject** m_ppTrafficEvents;
	int m_nNumTrafficEvents;
	CRITICAL_SECTION m_critTrafficEvents;

	CInjectorScheduleObject** m_ppScheduleEvents;
	int m_nNumScheduleEvents;
	CRITICAL_SECTION m_critScheduleEvents;

	CInjectorScheduleObject** m_ppDisplayEvents;
	int m_nNumDisplayEvents;
	CRITICAL_SECTION m_critDisplayEvents;

	double m_dblNextOnAir;
	double m_dblLastOnAir;

	CRITICAL_SECTION m_critSentinelSocket;
	SOCKET m_socketSentinel;  // let's make a separate connection to Sentinel for each channel we want to control.
	CNetUtil m_net;				// have a separate one for each thread

	// need to put some channel specific settings here;
	// for automated video insertion.
	char* m_pszSettingInsertionAllowID;
	char* m_pszSettingInsertionPrimaryFile;
	char* m_pszSettingAllowPrimaryFile;
	
	char* m_pszSettingInsertionSecondariesFile;

	SecondaryInfo_t** m_ppsecSettingSecInfo;
	int m_nNumSecInfo;
	CRITICAL_SECTION m_critSecInfo;

	int m_nSettingMinBifurcationDurMS;
	int m_nSettingMaxAllowDurMS;
	bool m_bSettingCoalesceAllow;

};


class CInjectorData  
{
public:
	CInjectorData();
	virtual ~CInjectorData();

	// util object
	CBufferUtil m_bu;

  // hostname.  have to get it, store it here
	char*	m_pszHost;	// the name of the host
	char*	m_pszCompleteHost;	// the complete name of the host

	char*	m_pszCortexHost;	// the name of the cortex host
	unsigned short m_usCortexCommandPort;
	unsigned short m_usCortexStatusPort;

/*
	CInjectorConnectionObject** m_ppConnObj;
	int m_nNumConnectionObjects;
*/
	CInjectorChannelObject** m_ppChannelObj;
	int m_nNumChannelObjects;

	_timeb m_timebTick; // the last time check inside the thread
	_timeb m_timebLastStatus; // the time of the last status given to cortex
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for cortex global status (each obj has their own as well)

	int m_nSettingsMod;
	int m_nChannelsMod;
//	int m_nConnectionsMod;
	int m_nScheduleMod;
	int m_nLastSettingsMod;
	int m_nLastChannelsMod;
//	int m_nLastScheduleMod;
//	int m_nLastConnectionsMod;
	int m_nRestrictionsMod;
	int m_nLastRestrictionsMod;

	bool m_bNetworkMessagingInitialized;
	bool m_bCheckModsWarningSent;
	bool m_bCheckMsgsWarningSent;
	bool m_bCheckAsRunWarningSent;

	_timeb m_timebAutoPurge; // the last time autopurge was run

	int m_nQueueMod;
	int m_nLastQueueMod;

	int m_nLastTrafficMod;

	bool m_bProcessSuspended;
//	bool m_bAutomationThreadStarted;
//	double m_dblLastAutomationChange;  //global change
//	_timeb m_timebAutomationTick;

	char* GetStatusText(unsigned long* pulStatus);  // allocates mem, must free after use
	int		SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError = false);
	int		GetHost();

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;
	CDBUtil* m_pdb2;
	CDBconn* m_pdb2Conn;

	int CheckSentinelMods(char* pszInfo=NULL);
	int CheckDatabaseMods(char* pszInfo=NULL);
	int CheckMessages(char* pszInfo=NULL);
	int CheckAsRun(char* pszInfo=NULL);
	int IncrementDatabaseMods(char* pszTableName, char* pszInfo=NULL);
//	int GetConnections(char* pszInfo=NULL);
	int GetChannels(char* pszInfo=NULL);
	int GetRestrictions(char* pszInfo=NULL);
	int GetQueue(char* pszInfo=NULL);

	CLicenseKey m_key;

	// global events
	CInjectorScheduleObject** m_ppRestrictions;
	int m_nNumRestrictions;
	CRITICAL_SECTION m_critRestrictions;


	bool m_bQuietKill;
	CRITICAL_SECTION m_critSQL;
	CRITICAL_SECTION m_critChannels;
	CRITICAL_SECTION m_critClients;
	CRITICAL_SECTION m_critTraffic;

private:
	CRITICAL_SECTION m_critText;
	char* m_pszStatus;	// parseable string

//	unsigned long m_ulReqCount;
//	unsigned long m_ulSvcCount;
};

#endif // !defined(AFX_INJECTORDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
