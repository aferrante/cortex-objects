// InjectorSettings.cpp: implementation of the CInjectorSettings.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "InjectorDefines.h"
#include "InjectorMain.h" 
#include "InjectorSettings.h"
#include "../../Common/FILE/DirUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CDirUtil	g_dir;				// watch folder utilities.
extern CInjectorMain* g_pinjector;
extern CInjectorApp theApp;


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CInjectorSettings::CInjectorSettings()
{
	m_pdb = NULL;
	m_pdbConn = NULL;

	m_pszName = NULL;
	m_pszType = NULL;  // familiar name of the type.
	m_pszProject = NULL;  // familiar name of the project name.
	m_ulMainMode = INJECTOR_MODE_DEFAULT;

	m_nAutoPurgeMessageDays = 30; // default
	m_nAutoPurgeAsRunDays = 30; // default
	m_nAutoPurgeInterval = 300;  // 5 minutes should be enough.

//	m_ppChannelSetting=NULL;
//	m_nChannelSettings=0;
	m_nNumChannels=0;

	// ports
	m_usCommandPort	= INJECTOR_PORT_CMD;
	m_usStatusPort	= INJECTOR_PORT_STATUS;

	m_nThreadDwellMS = 1000;

	// messaging for Injector
	m_bUseLog = true;			// write a log file
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host
	m_bReportSuccessfulOperation = false;
	m_bLogTransfers=false;
	m_pszFileSpec = NULL;
	m_pszMailSpec = NULL;
	m_pszProcessedFileSpec = NULL;
	m_pszProcessedMailSpec = NULL;

	m_bUseXMLClientLog = false;
	m_bMillisecondMessaging=true;			// use millisecond resolution for messages and asrun

	m_bUseQueue = false;			// use the queue for commands


	// DSN params
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;
	m_pszDatabase = NULL;

	m_pszSettings = NULL;  // the Settings table name
	m_pszExchange = NULL;  // the Exchange table name
	m_pszMessages = NULL;  // the Messages table name
	m_pszQueue = NULL; // the Queue table name
	m_pszCommandQueue = NULL;
	m_pszChannelInfo = NULL;
	m_pszAsRun = NULL;  // the As-run table name
	m_pszSchedule = NULL;  // the Schedule table name
	m_pszRestrictions = NULL;  // the Restrictions table name

	// file handling

// other settings
	m_ulModsIntervalMS = 3000;

	m_pszLicense=NULL;  // the License Key
	m_pszIconPath=NULL;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

	m_bDebugLists= false;  // if true, dumps the temp and events databases
	m_nDebugListTopCount=-1;  //if positive, just prints out the top n recored in the events tables.
	m_bDebugSQL=false;  // if true, prints out the SQL calls
	m_bDebugInsertSQL=false;  // if true, prints out the event insert SQL calls
	m_ulDebug = 0;
	m_pszDebugOrder = NULL;

	m_nTriggerAdvanceMS=0;
	m_nAnalyzeAutomationDwellMS = 1000; // number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once.
	m_nAnalyzeAutomationForceMS = 2500; // number of milliseconds after a change, that if there are changes pending, we MUST be force analysis 
	m_nAutomationIntervalMS = 3000;  // just more than preroll amount seems good.
	m_bChannelInfoViewHasDesc = true;

	m_nProgramAutomationDwellMS = 5000; // number of milliseconds programming is delayed after a change (prevents hammering programming when a bunch of list changes happen all at once.
	m_nProgramAutomationForceMS = 10000; // number of milliseconds after a change, that if there are changes pending, we MUST be force programming 
	m_nProgramAutomationSafetyMS = 25000; // number of milliseconds before the next primary plays, that we attempt programming (need some quiet time to get it done right) I choose 25 secs, 20 for standard network timeout + 5 arbitrary seconds added
//	m_nProgramAutomationIntervalMS = 60000; // number of milliseconds between schedule db change checks
	m_nProgramAutomationTrimMidnightMS = 1; // number of milliseconds to trim items that end at midnight (so they don't go into the next day in the visible schedule)
	m_nProgramAutomationDefeatMS=10000; // number of milliseconds after an automatic insertion or deletion, to wait before addressing the programming stack again.  This staggers the insertions so if you append a large list, and there are lots of changes all at once, the changes NOT induced by the insertions do not allow more insertions to happen without considering the inserted items.

	m_ulAutomationTimeToleranceMS = 500;  // an on air time must be plus or minus this many milliseconds to be considered identical

	m_nMinSchedulingThresholdMS=10000;

	m_nAdvanceSchedulingThresholdMaxDays = 30;
	m_nAdvanceSchedulingThresholdMinMins = 15;
	m_nSchedulingIncrementMins = 5;

	m_nTrafficModInterval=10;  //seconds

	m_bExtraRestrictionInfo = false;
	m_bInsertionTitleReplace = false;

	m_bUseLocalClock = false;
	m_bUseUTC = false;

	// operating mode
	m_bAutoSchedulerMode=false;
	m_bManualInjectorMode=false;
	m_bSuppressListEdits = true; //for safety
	m_bCompareRestrictions = true;

	m_pszSentinelHost=NULL;  // name of the server on which sentinel is running
	m_usSentinelPort=10670;  // port on which Sentinel is listening.
	m_pszSentinelDBName=NULL;  // the Sentinel database name
	m_pszSentinelTraffic=NULL;  // the Sentinel traffic table name
	m_pszSentinelEvents=NULL;  // the Sentinel events table name
	m_pszSentinelExchange=NULL;  // the Sentinel exchange table name
	m_ulSentinelPingIntervalMS = 10000;  // interval on which to ping Sentinel to see if it is there
	m_ulSentinelRetryIntervalMS = 10000;  // interval on which to try reconnecting if disconnected
	m_ulSentinelErrorIntervalMS = 60000;  // interval after disconnected that it becomes an error


}

CInjectorSettings::~CInjectorSettings()
{
	if(m_pszSentinelHost) free(m_pszSentinelHost); // must use malloc to allocate
	if(m_pszSentinelDBName) free(m_pszSentinelDBName); // must use malloc to allocate
	if(m_pszSentinelTraffic) free(m_pszSentinelTraffic); // must use malloc to allocate
	if(m_pszSentinelEvents) free(m_pszSentinelEvents); // must use malloc to allocate
	if(m_pszSentinelExchange) free(m_pszSentinelExchange); // must use malloc to allocate



	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_pszType) free(m_pszType); // must use malloc to allocate
	if(m_pszProject) free(m_pszProject); // must use malloc to allocate
	if(m_pszDSN) free(m_pszDSN); // must use malloc to allocate
	if(m_pszUser) free(m_pszUser); // must use malloc to allocate
	if(m_pszPW) free(m_pszPW); // must use malloc to allocate
	if(m_pszDatabase) free(m_pszDatabase); // must use malloc to allocate

	if(m_pszSettings) free(m_pszSettings); // must use malloc to allocate
	if(m_pszExchange) free(m_pszExchange); // must use malloc to allocate
	if(m_pszMessages) free(m_pszMessages); // must use malloc to allocate
	if(m_pszQueue) free(m_pszQueue); // must use malloc to allocate
	if(m_pszCommandQueue) free(m_pszCommandQueue); // must use malloc to allocate
	if(m_pszChannelInfo) free(m_pszChannelInfo); // must use malloc to allocate
	if(m_pszAsRun) free(m_pszAsRun);

	if(m_pszLicense) free(m_pszLicense); // must use malloc to allocate
	if(m_pszIconPath) free(m_pszIconPath); // must use malloc to allocate	
	if(m_pszFileSpec) free(m_pszFileSpec); // must use malloc to allocate	
	if(m_pszMailSpec) free(m_pszMailSpec); // must use malloc to allocate	
	if(m_pszProcessedFileSpec) free(m_pszProcessedFileSpec); // must use malloc to allocate	
	if(m_pszProcessedMailSpec) free(m_pszProcessedMailSpec); // must use malloc to allocate	
	if(m_pszDebugOrder) free(m_pszDebugOrder); // must use malloc to allocate
/*
	int i=0;
	if(m_ppChannelSetting)
	{
		while(i<m_nChannelSettings)
		{
			if(m_ppChannelSetting[i])
			{
				try
				{
					if(m_ppChannelSetting[i]->pszDesc) free(m_ppChannelSetting[i]->pszDesc);
					if(m_ppChannelSetting[i]->pszString) free(m_ppChannelSetting[i]->pszString);
					delete m_ppChannelSetting[i];
				}
				catch(...)
				{
				}
			}
			i++;
		}
		try{ delete [] m_ppChannelSetting;} catch(...){}
	}

	m_ppChannelSetting = NULL;
*/
}

/*
int CInjectorSettings::InsertSetting(ChannelSetting_t* pSetting, int index)
{
	if(pSetting)
	{
		ChannelSetting_t** ppObj = new ChannelSetting_t*[m_nChannelSettings+1];
		if(ppObj)
		{
			ChannelSetting_t** ppDelObj = m_ppChannelSetting;
			int o=0;
			if((m_ppChannelSetting)&&(m_nChannelSettings>0))
			{
				if((index<0)||(index>=m_nChannelSettings))
				{
					while(o<m_nChannelSettings)
					{
						ppObj[o] = m_ppChannelSetting[o];
						o++;
					}
					ppObj[m_nChannelSettings] = pSetting;
					index = m_nChannelSettings;
				}
				else
				{
					while(o<index)
					{
						ppObj[o] = m_ppChannelSetting[o];
						o++;
					}
					ppObj[o] = pSetting;
					while(o<m_nChannelSettings)
					{
						ppObj[o+1] = m_ppChannelSetting[o];
						o++;
					}
				}
			}
			else
			{
				ppObj[m_nChannelSettings] = pSetting;  // just add the one
				index = m_nChannelSettings;
			}

			m_ppChannelSetting = ppObj;
			if(ppDelObj)
			{
				try{delete [] ppDelObj;} catch(...){}
			}
			m_nChannelSettings++;
			return index;
		}
	}

	return INJECTOR_ERROR;
}

int CInjectorSettings::DeleteSetting(int index)  // removes from array
{
	if((index>=0)&&(index<m_nChannelSettings)&&(m_ppChannelSetting))
	{
		ChannelSetting_t** ppDelObj = m_ppChannelSetting;
		if(m_nChannelSettings>1)
		{
			ChannelSetting_t** ppObj = new ChannelSetting_t*[m_nChannelSettings-1];
			if(ppObj)
			{
				ChannelSetting_t* pDelEventObj = NULL;
				m_nChannelSettings--;
				int o=0;
				while(o<index)
				{
					ppObj[o] = m_ppChannelSetting[o];
					o++;
				}
				pDelEventObj = m_ppChannelSetting[o];
				while(o<m_nChannelSettings)
				{
					ppObj[o] = m_ppChannelSetting[o+1];
					o++;
				}

				m_ppChannelSetting = ppObj;
				if(ppDelObj) {try{ delete [] ppDelObj; } catch(...){}}
				if(pDelEventObj) 
				{
					try
					{ 
						if(pDelEventObj->pszDesc) free(pDelEventObj->pszDesc);
						if(pDelEventObj->pszString) free(pDelEventObj->pszString);
						delete pDelEventObj;
					} 
					catch(...){}
				}
				return INJECTOR_SUCCESS;
			}
		}
		else
		{
			if(m_nChannelSettings==1)
			{
				try
				{
					if(m_ppChannelSetting[0]->pszDesc) free(m_ppChannelSetting[0]->pszDesc);
					if(m_ppChannelSetting[0]->pszString) free(m_ppChannelSetting[0]->pszString);
					delete m_ppChannelSetting[0];
				}
				catch(...)
				{
				}

			}
			m_nChannelSettings = 0;
			m_ppChannelSetting = NULL;
			if(ppDelObj) {try{ delete [] ppDelObj; } catch(...){}}
			return INJECTOR_SUCCESS;
		}
	}
	return INJECTOR_ERROR;
}

*/

int CInjectorSettings::Settings(bool bRead)
{
	char pszFilename[MAX_PATH];
	strcpy(pszFilename, "");
//		AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, INJECTOR_SETTINGS_FILE_DEFAULT);  // cortex settings file
	CFileUtil file;
	// get settings.
	file.GetSettings(pszFilename, false); 

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
		// load up the values on the settings object
			m_pszName = file.GetIniString("Main", "Name", "Injector", m_pszName);
			m_pszType = file.GetIniString("Main", "Type", "Injector", m_pszType);
			m_pszProject = file.GetIniString("Main", "Project", "VDS", m_pszProject);
			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);

			m_nThreadDwellMS = file.GetIniInt("Main", "ThreadDwellMS", 1000);

			m_bDebugLists = file.GetIniInt("Database", "DebugLists", 0)?true:false;  // if true, dumps the temp and events databases
			m_nDebugListTopCount = file.GetIniInt("Database", "DebugListTopCount", -1);  //if positive, just prints out the top n recored in the events tables.
			m_bDebugSQL = file.GetIniInt("Database", "DebugSQL", 0)?true:false;  // if true, prints out the SQL calls
			m_bDebugInsertSQL = file.GetIniInt("Database", "DebugInsertSQL", 0)?true:false;  // if true, prints out the event insert SQL calls
			m_ulDebug = file.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.
			m_pszDebugOrder = file.GetIniString("Database", "DebugOrder", "(case when parent_position < 0 then ((event_position)/1000.0) else (parent_calc_start + (event_position-parent_position)/1000.0) end)", m_pszDebugOrder); // the order by clause for debug

			m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", INJECTOR_PORT_CMD);
			m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", INJECTOR_PORT_STATUS);

			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);

			// recompile license key params
			if(g_pinjector->m_data.m_key.m_pszLicenseString) free(g_pinjector->m_data.m_key.m_pszLicenseString);
			g_pinjector->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(m_pszLicense)+1);
			if(g_pinjector->m_data.m_key.m_pszLicenseString)
			sprintf(g_pinjector->m_data.m_key.m_pszLicenseString, "%s", m_pszLicense);

			g_pinjector->m_data.m_key.InterpretKey();

			char errorstring[MAX_MESSAGE_LENGTH];
			if(g_pinjector->m_data.m_key.m_bValid)
			{
				unsigned long i=0;
				while(i<g_pinjector->m_data.m_key.m_ulNumParams)
				{
					if((g_pinjector->m_data.m_key.m_ppszParams)
						&&(g_pinjector->m_data.m_key.m_ppszValues)
						&&(g_pinjector->m_data.m_key.m_ppszParams[i])
						&&(g_pinjector->m_data.m_key.m_ppszValues[i]))
					{
						if(stricmp(g_pinjector->m_data.m_key.m_ppszParams[i], "max")==0)
						{
							//g_pinjector->m_data.m_nMaxLicensedChannels = atoi(g_pinjector->m_data.m_key.m_ppszValues[i]);
						}
					}
					i++;
				}
			
				if(
						(
							(!g_pinjector->m_data.m_key.m_bExpires)
						||((g_pinjector->m_data.m_key.m_bExpires)&&(!g_pinjector->m_data.m_key.m_bExpired))
						||((g_pinjector->m_data.m_key.m_bExpires)&&(g_pinjector->m_data.m_key.m_bExpireForgiveness)&&(g_pinjector->m_data.m_key.m_ulExpiryDate+g_pinjector->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!g_pinjector->m_data.m_key.m_bMachineSpecific)
						||((g_pinjector->m_data.m_key.m_bMachineSpecific)&&(g_pinjector->m_data.m_key.m_bValidMAC))
						)
					)
				{
					// this overrides stuff so let's not let it do that.

										// let's just not override this in a green condition.  
										// comment it out.
//					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//					g_pinjector->m_data.SetStatusText(errorstring, INJECTOR_STATUS_OK);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
					g_pinjector->m_data.SetStatusText(errorstring, INJECTOR_STATUS_ERROR);
				}
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				g_pinjector->m_data.SetStatusText(errorstring, INJECTOR_STATUS_ERROR);
			}


			m_pszIconPath = file.GetIniString("FileServer", "IconPath", "C:\\Inetpub\\wwwroot\\Cortex\\injector\\images\\", m_pszIconPath);

			m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;

			m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
			m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;
			m_bUseXMLClientLog = file.GetIniInt("Messager", "UseXMLClientLog", 0)?true:false;
			m_bMillisecondMessaging = file.GetIniInt("Messager", "MillisecondMessaging", 1)?true:false;			// use millisecond resolution for messages and asrun

			m_nAutoPurgeMessageDays = file.GetIniInt("AutoPurge", "AutoPurgeMessageDays", 30);
			m_nAutoPurgeAsRunDays = file.GetIniInt("AutoPurge", "AutoPurgeAsRunDays", 30);
			m_nAutoPurgeInterval  = file.GetIniInt("AutoPurge", "AutoPurgeInterval", 300);  // 5 minutes should be enough.

			m_bUseQueue = file.GetIniInt("Database", "UseQueue", 1)?true:false; // use the queue for commands

			m_nNumChannels= file.GetIniInt("ChannelSpecific", "NumberOfChannelSections", 0); 

			m_pszDSN = file.GetIniString("Database", "DSN", m_pszName?m_pszName:"Injector", m_pszDSN);
			m_pszUser = file.GetIniString("Database", "DBUser", "sa", m_pszUser);
			m_pszPW = file.GetIniString("Database", "DBPassword", "", m_pszPW);
			m_pszDatabase = file.GetIniString("Database", "DBDefault", m_pszName?m_pszName:"Injector", m_pszDatabase);

			m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings", m_pszSettings);  // the Settings table name
			m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange", m_pszExchange);  // the Exchange table name
			m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages", m_pszMessages);  // the Messages table name
			m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue", m_pszQueue);  // the Queue table name
			m_pszChannelInfo = file.GetIniString("Database", "ChannelInfoView", "ChannelInfo", m_pszChannelInfo);  // the  m_pszChannelInfo name
			m_pszAsRun=file.GetIniString("Database", "AsRunTableName", "AsRun", m_pszAsRun);  // the As-run table name
			m_pszCommandQueue = file.GetIniString("Database", "CommandQueueTableName", "Command_Queue", m_pszCommandQueue);  // the Queue table name
			m_pszSchedule = file.GetIniString("Database", "ScheduleTableName", "Schedule", m_pszSchedule);  // the Schedule table name
			m_pszRestrictions = file.GetIniString("Database", "RestrictionsTableName", "Restrictions", m_pszRestrictions);  // the Schedule table name

	//		m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels");  // the Channels table name
	//		m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Connections");  // the Connections table name
	//		m_pszLiveEvents = file.GetIniString("Database", "LiveEventsTableName", "Events");  // the LiveEvents table name

			m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 3000);  // in milliseconds
			m_nTrafficModInterval= file.GetIniInt("Automation", "TrafficModInterval", INJECTOR_DEF_POLL_MS); //seconds

			m_nProgramAutomationDwellMS = file.GetIniInt("Automation", "ProgramAutomationDwellMS", 5000); // number of milliseconds programming is delayed after a change (prevents hammering programming when a bunch of list changes happen all at once.
			m_nProgramAutomationForceMS = file.GetIniInt("Automation", "ProgramAutomationForceMS", 10000); // number of milliseconds after a change, that if there are changes pending, we MUST be force programming 
			m_nProgramAutomationSafetyMS = file.GetIniInt("Automation", "ProgramAutomationSafetyMS", 25000); // number of milliseconds before the next primary plays, that we attempt programming (need some quiet time to get it done right) I choose 25 secs, 20 for standard network timeout + 5 arbitrary seconds added
//			m_nProgramAutomationIntervalMS = file.GetIniInt("Automation", "ProgramAutomationIntervalMS", 60000); // number of milliseconds between schedule db change checks
			m_nProgramAutomationTrimMidnightMS = file.GetIniInt("Automation", "ProgramAutomationTrimMidnightMS", 1); // number of milliseconds to trim items that end at midnight (so they don't go into the next day in the visible schedule)
			m_nProgramAutomationDefeatMS = file.GetIniInt("Automation", "ProgramAutomationDefeatMS", 10000); // number of milliseconds after an automatic insertion or deletion, to wait before addressing the programming stack again.  This staggers the insertions so if you append a large list, and there are lots of changes all at once, the changes NOT induced by the insertions do not allow more insertions to happen without considering the inserted items.

			m_ulAutomationTimeToleranceMS = file.GetIniInt("Automation", "AutomationTimeToleranceMS", 500);  // an on air time must be plus or minus this many milliseconds to be considered identical

			m_nAnalyzeAutomationDwellMS = file.GetIniInt("Automation", "AnalyzeAutomationDwellMS", 1000); // number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once.
			m_nAnalyzeAutomationForceMS = file.GetIniInt("Automation", "AnalyzeAutomationForceMS", 2500); // number of milliseconds after a change, that if there are changes pending, we MUST be force analysis 
			m_nAutomationIntervalMS = file.GetIniInt("Automation", "AutomationIntervalMS", 3000); // number of milliseconds between channel time checks
			m_bChannelInfoViewHasDesc = file.GetIniInt("Automation", "ChannelInfoViewHasDesc", 1)?true:false;

			m_nTriggerAdvanceMS = file.GetIniInt("Automation", "TriggerAdvanceMS", 0);// number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)
//			m_nAnalyzeRulesDwellMS = file.GetIniInt("Automation", "AnalyzeRulesDwellMS", 1000);
//			m_nAnalyzeParameterDwellMS = file.GetIniInt("Automation", "AnalyzeParameterDwellMS", 2500);
//			m_nAnalyzeTimingDwellMS = file.GetIniInt("Automation", "AnalyzeTimingDwellMS", 30000);
			m_bUseLocalClock = file.GetIniInt("Automation", "UseLocalClock", 0)?true:false;
			m_bUseUTC = file.GetIniInt("Automation", "UseUTC", 0)?true:false;

			m_bAutoSchedulerMode = file.GetIniInt("ControlMode", "AutoSchedulerMode", 0)?true:false;
			m_bManualInjectorMode = file.GetIniInt("ControlMode", "ManualInjectorMode", 0)?true:false;
			m_bSuppressListEdits = file.GetIniInt("ControlMode", "SuppressListEdits", 1)?true:false;
			m_bCompareRestrictions = file.GetIniInt("ControlMode", "CompareRestrictions", 1)?true:false;


			m_pszSentinelHost=file.GetIniString("Sentinel", "ListenHost", "localhost", m_pszSentinelHost);  // name of the server on which sentinel is running
			m_usSentinelPort=file.GetIniInt("Sentinel", "ListenPort", 10670);  // port on which Sentinel is listening.
			m_pszSentinelDBName=file.GetIniString("Sentinel", "DatabaseName", "Sentinel", m_pszSentinelDBName);  // the Sentinel database name
			m_pszSentinelTraffic=file.GetIniString("Sentinel", "TrafficTableName", "Traffic_Events", m_pszSentinelTraffic);  // the Sentinel traffic table name
			m_pszSentinelEvents=file.GetIniString("Sentinel", "EventsTableName", "Events", m_pszSentinelEvents);  // the Sentinel events table name
			m_pszSentinelExchange=file.GetIniString("Sentinel", "ExchangeTableName", "Exchange", m_pszSentinelExchange);  // the Sentinel exchange table name

			m_ulSentinelPingIntervalMS = file.GetIniInt("Sentinel", "PingIntervalMS", 10000);  // interval on which to ping Sentinel to see if it is there
			m_ulSentinelRetryIntervalMS = file.GetIniInt("Sentinel", "RetryIntervalMS", 10000);  // interval on which to try reconnecting if disconnected
			m_ulSentinelErrorIntervalMS = file.GetIniInt("Sentinel", "ErrorIntervalMS", 60000);  // interval after disconnected that it becomes an error
			
			m_nAdvanceSchedulingThresholdMaxDays = file.GetIniInt("Schedule", "AdvanceSchedulingThresholdMax", 30);
			m_nAdvanceSchedulingThresholdMinMins = file.GetIniInt("Schedule", "AdvanceSchedulingThresholdMin", 15);
			m_nSchedulingIncrementMins = file.GetIniInt("Schedule", "ScheduleIncrement", 5);

			m_nMinSchedulingThresholdMS = file.GetIniInt("Schedule", "MinSchedulingThresholdMS", 10000);

			m_bExtraRestrictionInfo = file.GetIniInt("Schedule", "UseExtraRestrictionInfo", 0)?true:false;
			m_bInsertionTitleReplace = file.GetIniInt("Schedule", "InsertionTitleReplace", 0)?true:false;



			m_pszFileSpec = file.GetIniString("Messager", "LogFileIni", "Logs\\Injector|YD||1|", m_pszFileSpec);
				// SMTP address[: override port (optional)] | distribution lists | from email |  subject spec | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
			m_pszMailSpec = file.GetIniString("Messager", "SMTPIni", "mail.server.com|f3:recipient@address.com|\"someone\"<from@here.com>|%P %T from %h|%h|C:\\injectorsmtp.txt|1|1|0", m_pszMailSpec);
			if(m_pszFileSpec) 
			{
				if(m_pszProcessedFileSpec)
				{
					try{ free(m_pszProcessedFileSpec); } catch(...) {}
				}
				m_pszProcessedFileSpec = ProcessString(m_pszFileSpec, false);
			}
			
			if(m_pszMailSpec) 
			{
				if(m_pszProcessedMailSpec)
				{
					try{ free(m_pszProcessedMailSpec); } catch(...) {}
				}
				m_pszProcessedMailSpec = ProcessString(m_pszMailSpec, false);
			}

		}
		else //write
		{
			file.SetIniString("Main", "Name", m_pszName);
			file.SetIniString("Main", "Type", m_pszType);
			file.SetIniString("Main", "Project", m_pszProject);
			file.SetIniString("License", "Key", m_pszLicense);

			file.SetIniInt("Main", "ThreadDwellMS", m_nThreadDwellMS);

			file.SetIniInt("Database", "DebugLists", m_bDebugLists?1:0);  // if true, dumps the temp and events databases
			file.SetIniInt("Database", "DebugListTopCount", m_nDebugListTopCount);  //if positive, just prints out the top n recored in the events tables.
			file.SetIniInt("Database", "DebugSQL", m_bDebugSQL?1:0);  // if true, prints out the SQL calls
			file.SetIniInt("Database", "DebugInsertSQL", m_bDebugInsertSQL?1:0);  // if true, prints out the event insert SQL calls
			file.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.
			file.SetIniString("Database", "DebugOrder", m_pszDebugOrder); // the order by clause for debug

			file.SetIniInt("CommandServer", "ListenPort", m_usCommandPort);
			file.SetIniInt("StatusServer", "ListenPort", m_usStatusPort);

			file.SetIniString("FileServer", "IconPath", m_pszIconPath);

			file.SetIniInt("Messager", "UseEmail", m_bUseEmail?1:0);
			file.SetIniInt("Messager", "UseNet", m_bUseNetwork?1:0);
			file.SetIniInt("Messager", "UseLog", m_bUseLog?1:0);
			file.SetIniInt("Messager", "ReportSuccessfulOperation", m_bReportSuccessfulOperation?1:0);
			file.SetIniInt("Messager", "LogTransfers", m_bLogTransfers?1:0);
			file.SetIniInt("Messager", "UseXMLClientLog", m_bUseXMLClientLog?true:false);
			file.SetIniInt("Messager", "MillisecondMessaging", m_bMillisecondMessaging?true:false);		// use millisecond resolution for messages and asrun


			file.SetIniInt("AutoPurge", "AutoPurgeMessageDays", m_nAutoPurgeMessageDays);
			file.SetIniInt("AutoPurge", "AutoPurgeAsRunDays", m_nAutoPurgeAsRunDays);
			file.SetIniInt("AutoPurge", "AutoPurgeInterval", m_nAutoPurgeInterval);  // 5 minutes should be enough.


			file.SetIniInt("Database", "UseQueue", m_bUseQueue?1:0); // use the queue for commands

// read only?  nah, let the entries at least be created.
			file.SetIniInt("ChannelSpecific", "NumberOfChannelSections", m_nNumChannels); 

			file.SetIniString("Database", "DSN", m_pszDSN);
			file.SetIniString("Database", "DBUser", m_pszUser);
			file.SetIniString("Database", "DBPassword", m_pszPW);
			file.SetIniString("Database", "DBDefault", m_pszDatabase );

			file.SetIniString("Database", "SettingsTableName", m_pszSettings);  // the Settings table name
			file.SetIniString("Database", "ExchangeTableName", m_pszExchange);  // the Exchange table name
			file.SetIniString("Database", "MessagesTableName", m_pszMessages);  // the Messages table name
			file.SetIniString("Database", "QueueTableName", m_pszQueue);  // the Queue table name
			file.SetIniString("Database", "ChannelInfoView", m_pszChannelInfo);  // the  m_pszChannelInfo name
			file.SetIniString("Database", "AsRunTableName", m_pszAsRun);  // the As-run table name
			file.SetIniString("Database", "CommandQueueTableName", m_pszCommandQueue);  // the commandQueue table name
			file.SetIniString("Database", "ScheduleTableName", m_pszSchedule);  // the Schedule table name
			file.SetIniString("Database", "RestrictionsTableName", m_pszRestrictions );  // the Schedule table name

			file.SetIniInt("Database", "ModificationCheckInterval", m_ulModsIntervalMS);  // in milliseconds

			file.SetIniInt("Automation", "AnalyzeAutomationDwellMS", m_nAnalyzeAutomationDwellMS, "number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once)."); // number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once).
			file.SetIniInt("Automation", "AnalyzeAutomationForceMS", m_nAnalyzeAutomationForceMS, "number of milliseconds after a change, that if there are changes pending, we MUST force analysis"); // number of milliseconds after a change, that if there are changes pending, we MUST force analysis 
			file.SetIniInt("Automation", "AutomationIntervalMS", m_nAutomationIntervalMS, "number of milliseconds between channel time and status checks"); // number of milliseconds between channel time checks
			file.SetIniInt("Automation", "ChannelInfoViewHasDesc", m_bChannelInfoViewHasDesc?1:0);

//			file.SetIniInt("Automation", "AnalyzeRulesDwellMS",  m_nAnalyzeRulesDwellMS);
//			file.SetIniInt("Automation", "AnalyzeParameterDwellMS",  m_nAnalyzeParameterDwellMS);
//			file.SetIniInt("Automation", "AnalyzeTimingDwellMS",  m_nAnalyzeTimingDwellMS);

			file.SetIniInt("Automation", "TriggerAdvanceMS", m_nTriggerAdvanceMS);// number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)
			file.SetIniInt("Automation", "UseLocalClock", m_bUseLocalClock?1:0);
			file.SetIniInt("Automation", "UseUTC", m_bUseUTC?1:0);  
			file.SetIniInt("Automation", "TrafficModInterval", m_nTrafficModInterval); //seconds

			file.SetIniInt("Automation", "ProgramAutomationDwellMS", m_nProgramAutomationDwellMS, "number of milliseconds programming is delayed after list analysis (prevents hammering programming when a bunch of list changes happen all at once)."); // number of milliseconds programming is delayed after a change (prevents hammering programming when a bunch of list changes happen all at once).
			file.SetIniInt("Automation", "ProgramAutomationForceMS", m_nProgramAutomationForceMS, "number of milliseconds after list analysis, that if there are changes pending, we MUST force programming"); // number of milliseconds after a change, that if there are changes pending, we MUST be force programming 
			file.SetIniInt("Automation", "ProgramAutomationSafetyMS", m_nProgramAutomationSafetyMS, "minimum number of milliseconds before the next primary plays, that we attempt programming"); // number of milliseconds before the next primary plays, that we attempt programming (need some quiet time to get it done right) I choose 25 secs, 20 for standard network timeout + 5 arbitrary seconds added
//			file.SetIniInt("Automation", "ProgramAutomationIntervalMS", m_nProgramAutomationIntervalMS, "number of milliseconds between schedule db change checks"); // minimum number of milliseconds between schedule db change checks
			file.SetIniInt("Automation", "ProgramAutomationTrimMidnightMS", m_nProgramAutomationTrimMidnightMS); // number of milliseconds to trim items that end at midnight (so they don't go into the next day in the visible schedule)
			file.SetIniInt("Automation", "ProgramAutomationDefeatMS", m_nProgramAutomationDefeatMS); // number of milliseconds after an automatic insertion or deletion, to wait before addressing the programming stack again.  This staggers the insertions so if you append a large list, and there are lots of changes all at once, the changes NOT induced by the insertions do not allow more insertions to happen without considering the inserted items.

			file.SetIniInt("Automation", "AutomationTimeToleranceMS", m_ulAutomationTimeToleranceMS);  // an on air time must be plus or minus this many milliseconds to be considered identical

			file.SetIniInt("ControlMode", "AutoSchedulerMode", m_bAutoSchedulerMode?1:0);
			file.SetIniInt("ControlMode", "ManualInjectorMode", m_bManualInjectorMode?1:0);
			file.SetIniInt("ControlMode", "SuppressListEdits", m_bSuppressListEdits?1:0);
			file.SetIniInt("ControlMode", "CompareRestrictions", m_bCompareRestrictions?1:0);

			file.SetIniString("Sentinel", "ListenHost", m_pszSentinelHost);  // name of the server on which sentinel is running
			file.SetIniInt("Sentinel", "ListenPort", m_usSentinelPort);  // port on which Sentinel is listening.
			file.SetIniString("Sentinel", "DatabaseName", m_pszSentinelDBName);  // the Sentinel database name
			file.SetIniString("Sentinel", "TrafficTableName", m_pszSentinelTraffic);  // the Sentinel traffix table name
			file.SetIniString("Sentinel", "EventsTableName", m_pszSentinelEvents);    // the Sentinel events table name
			file.SetIniString("Sentinel", "ExchangeTableName", m_pszSentinelExchange);  // the Sentinel exchange table name
			file.SetIniInt("Sentinel", "PingIntervalMS", m_ulSentinelPingIntervalMS );  // interval on which to ping Sentinel to see if it is there
			file.SetIniInt("Sentinel", "RetryIntervalMS", m_ulSentinelRetryIntervalMS );  // interval on which to try reconnecting if disconnected
			file.SetIniInt("Sentinel", "ErrorIntervalMS", m_ulSentinelErrorIntervalMS );  // interval after disconnected that it becomes an error

			file.SetIniInt("Schedule", "AdvanceSchedulingThresholdMax", m_nAdvanceSchedulingThresholdMaxDays);
			file.SetIniInt("Schedule", "AdvanceSchedulingThresholdMin", m_nAdvanceSchedulingThresholdMinMins);
			file.SetIniInt("Schedule", "ScheduleIncrement", m_nSchedulingIncrementMins);
			file.SetIniInt("Schedule", "UseExtraRestrictionInfo", m_bExtraRestrictionInfo?1:0);
			file.SetIniInt("Schedule", "InsertionTitleReplace", m_bInsertionTitleReplace?1:0);
			file.SetIniInt("Schedule", "MinSchedulingThresholdMS", m_nMinSchedulingThresholdMS);


			file.SetIniString("Messager", "LogFileIni", m_pszFileSpec);
			file.SetIniString("Messager", "SMTPIni", m_pszMailSpec);

			file.SetSettings(pszFilename, false);  // have to have correct filename

		}
		return INJECTOR_SUCCESS;
	}
	return INJECTOR_ERROR;
}

int CInjectorSettings::ChannelSettings(bool bRead)
{
	char pszFilename[MAX_PATH];
	strcpy(pszFilename, "");
//		AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, INJECTOR_SETTINGS_FILE_DEFAULT);  // cortex settings file
	CFileUtil file;
	// get settings.
	file.GetSettings(pszFilename, false); 

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
			m_nNumChannels= file.GetIniInt("ChannelSpecific", "NumberOfChannelSections", 0); 

			int i =0;
			int nValue=-1;
			char* pszString = NULL;
			char errorstring[MAX_MESSAGE_LENGTH];
			while(i<m_nNumChannels)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Channel %03d", i);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CHANNELS)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "ChannelSettings", "checking %s on %d channels", errorstring, g_pinjector->m_data.m_nNumChannelObjects);  //(Dispatch message)
				pszString = file.GetIniString(errorstring, "description", ""); 
				nValue=-1;
				if((pszString)&&(g_pinjector->m_data.m_ppChannelObj))
				{
					EnterCriticalSection(&g_pinjector->m_data.m_critChannels);
					CInjectorChannelObject* pObj = NULL;
					int x=0;
					while(x<g_pinjector->m_data.m_nNumChannelObjects)
					{
						if(g_pinjector->m_data.m_ppChannelObj[x])
						{
							if(g_pinjector->m_data.m_ppChannelObj[x]->m_pszDesc)
							{
								//case insensitive
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CHANNELS)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "ChannelSettings", "comparing %s and %s", g_pinjector->m_data.m_ppChannelObj[x]->m_pszDesc, pszString);  //(Dispatch message)
								if(stricmp(g_pinjector->m_data.m_ppChannelObj[x]->m_pszDesc, pszString)==0)
								{
									nValue = g_pinjector->m_data.m_ppChannelObj[x]->m_nChannelID;
									pObj = g_pinjector->m_data.m_ppChannelObj[x];
									break;
								}
							}
						}
						x++;
					}

					// here we have the uid of the channel in nValue, and also a non-null channel object pointer, if we found something that matches.
					// Now, get all the channel specific settings and go.

					if(pObj)
					{
	EnterCriticalSection(&pObj->m_critSecInfo);
						pObj->m_pszSettingInsertionAllowID=file.GetIniString(errorstring, "InsertionAllowID", "", pObj->m_pszSettingInsertionAllowID);
						pObj->m_pszSettingInsertionSecondariesFile=file.GetIniString(errorstring, "InsertionSecondariesFile", "", pObj->m_pszSettingInsertionSecondariesFile);
						pObj->m_nSettingMinBifurcationDurMS=file.GetIniInt(errorstring, "MinBifurcationDurMS", 5000); // 5 seconds
						pObj->m_nSettingMaxAllowDurMS = file.GetIniInt(errorstring, "MaxAllowDurMS", 3600000);  // 1 hour
						pObj->m_bSettingCoalesceAllow = file.GetIniInt(errorstring, "CoalesceAllow", 1)?true:false;

						pObj->m_pszSettingInsertionPrimaryFile=file.GetIniString(errorstring, "InsertionPrimaryFile", "", pObj->m_pszSettingInsertionPrimaryFile);
						pObj->m_pszSettingAllowPrimaryFile=file.GetIniString(errorstring, "AllowPrimaryFile", "", pObj->m_pszSettingAllowPrimaryFile);

						int nNumSec = pObj->m_nNumSecInfo;

						pObj->m_nNumSecInfo = file.GetIniInt(errorstring, "NumSecondaryInfo", 0); 
						if(pObj->m_nNumSecInfo<0) pObj->m_nNumSecInfo=0;

						if(nNumSec != pObj->m_nNumSecInfo) // realloc array
						{
							int qq=0;
							while(qq<nNumSec)
							{
								if(pObj->m_ppsecSettingSecInfo[qq])
								{
									if(pObj->m_ppsecSettingSecInfo[qq]->pszID) {	try {free(pObj->m_ppsecSettingSecInfo[qq]->pszID);} catch(...){} }
									try {delete pObj->m_ppsecSettingSecInfo[qq];} catch(...){}
								}
								qq++;
							}

							try {delete [] pObj->m_ppsecSettingSecInfo;} catch(...){}
							pObj->m_ppsecSettingSecInfo = NULL;

							if(pObj->m_nNumSecInfo>0)
							{
								pObj->m_ppsecSettingSecInfo = new SecondaryInfo_t*[pObj->m_nNumSecInfo];
								if(pObj->m_ppsecSettingSecInfo)
								{
									int qq=0;
									while(qq<pObj->m_nNumSecInfo)
									{
										pObj->m_ppsecSettingSecInfo[qq] = new SecondaryInfo_t;
										if(pObj->m_ppsecSettingSecInfo[qq])
										{
											pObj->m_ppsecSettingSecInfo[qq]->pszID = NULL; // must initialize
											pObj->m_ppsecSettingSecInfo[qq]->nBeginOffsetMS = 0; // must initialize
											pObj->m_ppsecSettingSecInfo[qq]->nEndOffsetMS = 0; // must initialize
											pObj->m_ppsecSettingSecInfo[qq]->bAdjustDurToPri = false; // must initialize
										}
										qq++;
									}

								}
								else
								{
									// report error
		g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "ChannelSettings", "Error allocating secondary settings array");  //(Dispatch message)
								}
							}
						}
						if((pObj->m_nNumSecInfo>0)&&(pObj->m_ppsecSettingSecInfo))
						{
							char settingstring[MAX_MESSAGE_LENGTH];
							int qq=0;
							while(qq<pObj->m_nNumSecInfo)
							{
								if( pObj->m_ppsecSettingSecInfo[qq] )
								{
									_snprintf(settingstring, MAX_MESSAGE_LENGTH-1, "Secondary_%02d_ID", qq);
									pObj->m_ppsecSettingSecInfo[qq]->pszID = file.GetIniString(errorstring, settingstring, "", pObj->m_ppsecSettingSecInfo[qq]->pszID);
									_snprintf(settingstring, MAX_MESSAGE_LENGTH-1, "Secondary_%02d_BeginOffset", qq);
									pObj->m_ppsecSettingSecInfo[qq]->nBeginOffsetMS = file.GetIniInt(errorstring, settingstring, 0);
									_snprintf(settingstring, MAX_MESSAGE_LENGTH-1, "Secondary_%02d_BeginOffset", qq);
									pObj->m_ppsecSettingSecInfo[qq]->nEndOffsetMS = file.GetIniInt(errorstring, settingstring, 0);
									_snprintf(settingstring, MAX_MESSAGE_LENGTH-1, "Secondary_%02d_BeginOffset", qq);
									pObj->m_ppsecSettingSecInfo[qq]->bAdjustDurToPri = file.GetIniInt(errorstring, settingstring, 0)?true:false;
								}
								qq++;
							}
						}

	LeaveCriticalSection(&pObj->m_critSecInfo);

					}

					LeaveCriticalSection(&g_pinjector->m_data.m_critChannels);
				}


				if(pszString) { try{free(pszString);} catch(...){} }
				pszString = NULL;

				i++;
			}

		}
		else //write
		{

// read only?  nah, let the entries at least be created.
			file.SetIniInt("ChannelSpecific", "NumberOfChannelSections", m_nNumChannels); 


			int i =0;
			int nValue=-1;
			char* pszString = NULL;
			char errorstring[MAX_MESSAGE_LENGTH];
			while(i<m_nNumChannels)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Channel %03d", i);
				pszString = file.GetIniString(errorstring, "description", ""); 
				nValue=-1;
				if((pszString)&&(g_pinjector->m_data.m_ppChannelObj))
				{
					EnterCriticalSection(&g_pinjector->m_data.m_critChannels);
					CInjectorChannelObject* pObj = NULL;
					int x=0;
					while(x<g_pinjector->m_data.m_nNumChannelObjects)
					{
						if(g_pinjector->m_data.m_ppChannelObj[x])
						{
							if(g_pinjector->m_data.m_ppChannelObj[x]->m_pszDesc)
							{
								//case insensitive
								if(stricmp(g_pinjector->m_data.m_ppChannelObj[x]->m_pszDesc, pszString)==0)
								{
									nValue = g_pinjector->m_data.m_ppChannelObj[x]->m_nChannelID;
									pObj = g_pinjector->m_data.m_ppChannelObj[x];
									break;
								}
							}
						}
						x++;
					}

					// here we have the uid of the channel in nValue, and also a non-null channel object pointer, if we found something that matches.
					// Now, get all the channel specific settings and go.

					if(pObj)
					{
	EnterCriticalSection(&pObj->m_critSecInfo);
						file.SetIniString(errorstring, "InsertionAllowID", pObj->m_pszSettingInsertionAllowID);
						file.SetIniString(errorstring, "InsertionSecondariesFile", pObj->m_pszSettingInsertionSecondariesFile);
						file.SetIniInt(errorstring, "MinBifurcationDurMS", pObj->m_nSettingMinBifurcationDurMS); 
						file.SetIniInt(errorstring, "MaxAllowDurMS", pObj->m_nSettingMaxAllowDurMS);  // 1 hour
						file.SetIniInt(errorstring, "CoalesceAllow", pObj->m_bSettingCoalesceAllow?1:0);
						file.SetIniString(errorstring, "InsertionPrimaryFile", pObj->m_pszSettingInsertionPrimaryFile);
						file.SetIniString(errorstring, "AllowPrimaryFile", pObj->m_pszSettingAllowPrimaryFile);

						if((pObj->m_nNumSecInfo>0)&&(pObj->m_ppsecSettingSecInfo))
						{
							char settingstring[MAX_MESSAGE_LENGTH];
							int qq=0;
							while(qq<pObj->m_nNumSecInfo)
							{
								if( pObj->m_ppsecSettingSecInfo[qq] )
								{
									_snprintf(settingstring, MAX_MESSAGE_LENGTH-1, "Secondary_%02d_ID", qq);
									file.SetIniString(errorstring, settingstring, pObj->m_ppsecSettingSecInfo[qq]->pszID, "the ID of the secondary to apply these settings to, found in secondaries file");
									_snprintf(settingstring, MAX_MESSAGE_LENGTH-1, "Secondary_%02d_BeginOffset", qq);
									file.SetIniInt(errorstring, settingstring, pObj->m_ppsecSettingSecInfo[qq]->nBeginOffsetMS, "ms to add to on air time offset");
									_snprintf(settingstring, MAX_MESSAGE_LENGTH-1, "Secondary_%02d_BeginOffset", qq);
									file.SetIniInt(errorstring, settingstring, pObj->m_ppsecSettingSecInfo[qq]->nEndOffsetMS, "ms to subtract from duration after adjusted for air time offset");
									_snprintf(settingstring, MAX_MESSAGE_LENGTH-1, "Secondary_%02d_BeginOffset", qq);
									file.SetIniInt(errorstring, settingstring, pObj->m_ppsecSettingSecInfo[qq]->bAdjustDurToPri?1:0, "adjust the secondaries duration to that of the primary, before offsets are applied (otherwise use as found)");
								}
								qq++;
							}
						}

	LeaveCriticalSection(&pObj->m_critSecInfo);


					}

					LeaveCriticalSection(&g_pinjector->m_data.m_critChannels);
				}


				if(pszString) { try{free(pszString);} catch(...){} }
				pszString = NULL;

				i++;
			}


			file.SetSettings(pszFilename, false);  // have to have correct filename

		}
		return INJECTOR_SUCCESS;
	}
	return INJECTOR_ERROR;
}

int CInjectorSettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	if((m_pszSettings)&&(strlen(m_pszSettings)>0)&&(m_pdbConn)&&(m_pdb)&&(m_pdbConn->m_bConnected)&&(Settings(true)==INJECTOR_SUCCESS))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = INJECTOR_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCategory="";
				CString szParameter="";
				CString szValue="";
				CString szTemp="";
				int min, max;
				bool bmin = false, bmax = false;
				try
				{
					prs->GetFieldValue("category", szCategory);  //HARDCODE
					prs->GetFieldValue("parameter", szParameter);  //HARDCODE
					prs->GetFieldValue("value", szValue);  //HARDCODE
					prs->GetFieldValue("min_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						min = atoi(szTemp);
						bmin = true;
					}
					prs->GetFieldValue("max_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						max = atoi(szTemp);
						bmax = true;
					}
				}
				catch( ... )
				{
				}

				int nLength = szValue.GetLength();
				if(szCategory.CompareNoCase("Main")==0)
				{
					if(szParameter.CompareNoCase("Name")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszName)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszName) free(m_pszName);
								m_pszName = pch;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("License")==0)
				{
					if(szParameter.CompareNoCase("Key")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszLicense)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLicense) free(m_pszLicense);
								m_pszLicense = pch;

								// recompile license key params
								if(g_pinjector->m_data.m_key.m_pszLicenseString) free(g_pinjector->m_data.m_key.m_pszLicenseString);
								g_pinjector->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
								if(g_pinjector->m_data.m_key.m_pszLicenseString)
								sprintf(g_pinjector->m_data.m_key.m_pszLicenseString, "%s", szValue);

								g_pinjector->m_data.m_key.InterpretKey();

								char errorstring[MAX_MESSAGE_LENGTH];
								if(g_pinjector->m_data.m_key.m_bValid)
								{
									unsigned long i=0;
									while(i<g_pinjector->m_data.m_key.m_ulNumParams)
									{
										if((g_pinjector->m_data.m_key.m_ppszParams)
											&&(g_pinjector->m_data.m_key.m_ppszValues)
											&&(g_pinjector->m_data.m_key.m_ppszParams[i])
											&&(g_pinjector->m_data.m_key.m_ppszValues[i]))
										{
											if(stricmp(g_pinjector->m_data.m_key.m_ppszParams[i], "max")==0)
											{
//												g_pinjector->m_data.m_nMaxLicensedDevices = atoi(g_pinjector->m_data.m_key.m_ppszValues[i]);
											}
										}
										i++;
									}
								
									if(
											(
												(!g_pinjector->m_data.m_key.m_bExpires)
											||((g_pinjector->m_data.m_key.m_bExpires)&&(!g_pinjector->m_data.m_key.m_bExpired))
											||((g_pinjector->m_data.m_key.m_bExpires)&&(g_pinjector->m_data.m_key.m_bExpireForgiveness)&&(g_pinjector->m_data.m_key.m_ulExpiryDate+g_pinjector->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
											)
										&&(
												(!g_pinjector->m_data.m_key.m_bMachineSpecific)
											||((g_pinjector->m_data.m_key.m_bMachineSpecific)&&(g_pinjector->m_data.m_key.m_bValidMAC))
											)
										)
									{
					// this overrides stuff so let's not let it do that.

										// let's just not override this in a green condition.  
										// comment it out.
//										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//										g_pinjector->m_data.SetStatusText(errorstring, INJECTOR_STATUS_OK);
									}
									else
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
										g_pinjector->m_data.SetStatusText(errorstring, INJECTOR_STATUS_ERROR);
									}
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
									g_pinjector->m_data.SetStatusText(errorstring, INJECTOR_STATUS_ERROR);
								}

							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("CommandServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usCommandPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("StatusServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usStatusPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Messager")==0)
				{
					if(szParameter.CompareNoCase("UseEmail")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseEmail = true;
							else m_bUseEmail = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseNet")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseNetwork = true;
							else m_bUseNetwork = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseLog = true;
							else m_bUseLog = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseXMLClientLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseXMLClientLog = true;
							else m_bUseXMLClientLog = false;
						}
					}
					else
					if(szParameter.CompareNoCase("ReportSuccessfulOperation")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bReportSuccessfulOperation = true;
							else m_bReportSuccessfulOperation = false;
						}
					}
				}
				else
/*
				if(szCategory.CompareNoCase("FileHandling")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("DeleteSourceFileOnTransfer")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bDeleteSourceFileOnTransfer = true;
							else m_bDeleteSourceFileOnTransfer = false;
						}
					}
				}
				else
*/
				if(szCategory.CompareNoCase("Database")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("SettingsTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszSettings)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszSettings) free(m_pszSettings);
								m_pszSettings = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ExchangeTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszExchange)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszExchange) free(m_pszExchange);
								m_pszExchange = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("MessagesTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszMessages)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszMessages) free(m_pszMessages);
								m_pszMessages = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("QueueTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszQueue)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszQueue) free(m_pszQueue);
								m_pszQueue = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("CommandQueueTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszCommandQueue)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszCommandQueue) free(m_pszCommandQueue);
								m_pszCommandQueue = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("AsRunTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszAsRun)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszAsRun) free(m_pszAsRun);
								m_pszAsRun = pch;
							}
						}
					}

/*
					else
					if(szParameter.CompareNoCase("ConnectionsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszConnections) free(m_pszConnections);
								m_pszConnections = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("LiveEventsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLiveEvents) free(m_pszLiveEvents);
								m_pszLiveEvents = pch;
							}
						}
					}
*/
					else
					if(szParameter.CompareNoCase("ModificationCheckInterval")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_ulModsIntervalMS = nLength;
						}
					}
				}
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
					if(szParameter.CompareNoCase("Messages")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeMessageDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
					else
					if(szParameter.CompareNoCase("AsRun_Log")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeAsRunDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Schedule")==0)
				{
					if(szParameter.CompareNoCase("AdvanceSchedulingThresholdMax")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_nAdvanceSchedulingThresholdMaxDays = nLength;
						}
					}
					else
					if(szParameter.CompareNoCase("AdvanceSchedulingThresholdMin")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_nAdvanceSchedulingThresholdMinMins = nLength; 
						}
					}
					else
					if(szParameter.CompareNoCase("ScheduleIncrement")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_nSchedulingIncrementMins = nLength; 
						}
					}
					else
					if(szParameter.CompareNoCase("UseExtraRestrictionInfo")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bExtraRestrictionInfo = true;
							else m_bExtraRestrictionInfo = false;
						}
					}
					else 
					if(szParameter.CompareNoCase("InsertionTitleReplace")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bInsertionTitleReplace = true;
							else m_bInsertionTitleReplace = false;
						}
					}
					else 
					if(szParameter.CompareNoCase("min_clip_duration")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_nMinSchedulingThresholdMS = nLength; // cant use negative numbiz
						}
					}
				}
				
				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			if(pszInfo)
			{
				_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
			}
			delete prs;
			prs = NULL;

			Settings(false); //write
			return INJECTOR_SUCCESS;
		}
	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s%s%s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. ",
				m_pdb?"":"Database pointer was NULL. ",
				m_pdbConn?"":"Connection pointer was NULL. ",
				((m_pdbConn)&&(!m_pdbConn->m_bConnected))?"Database not connected. ":""
				);
		}
	}
	return INJECTOR_ERROR;
}

char* CInjectorSettings::ProcessString(char* pszString, bool bFreeIncomingString)
{
	if(pszString)
	{
		char pszOutput[4096];
		int nInLen=strlen(pszString);
		int nOutLen=0;
		char* pch = pszString;
		char* pchEnd = min((pch+4096),(pszString+nInLen));
		while((*pch!=0)&&(pch<pchEnd))
		{
			if(*pch == '%')
			{
				// we are in a specifier.
				// here is a list of ours:
				// %h  local hostname
		
				pch++;
				if((*pch!=0)&&(pch<pchEnd))
				{
					if(*pch == 'h')
					{
						if((g_pinjector->m_data.m_pszHost)&&(strlen(g_pinjector->m_data.m_pszHost)))
						{
							char* pszTemp = (char*)malloc(strlen(g_pinjector->m_data.m_pszHost)+1);
							if(pszTemp)
							{
								strcpy(pszTemp, g_pinjector->m_data.m_pszHost);
							
								char* pchPlus = pszTemp;
								while((*pchPlus != 0)&&(nOutLen<4096))
								{
									pszOutput[nOutLen]= *pchPlus;
									nOutLen++;
									pchPlus++;
								}
								free(pszTemp);
							}
						}
						pch++;
					}
					else
					{
						// some other specifier, can be anything.
						pszOutput[nOutLen]= *(pch-1); // do add the %, this may get further processed
						nOutLen++;
						pszOutput[nOutLen]= *pch;
						nOutLen++;
						pch++;
					}
				}
				else
				{
					pszOutput[nOutLen]= *(pch-1);
					nOutLen++;
				}
				
			}
			else
			{
				pszOutput[nOutLen]= *pch;
				nOutLen++;
				pch++;
			}
		}

		pszOutput[nOutLen]=0;

		pch = (char*)malloc(strlen(pszOutput)+1);
		if(pch)
		{
			strcpy(pch,pszOutput);
			if(bFreeIncomingString) 
			{
				try {free(pszString);} catch(...){}
			}
		}
		return pch;
	}
	return NULL;
}


