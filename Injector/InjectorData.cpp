// InjectorData.cpp: implementation of the CInjectorData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <process.h>
#include "Injector.h"
#include "InjectorHandler.h" 
#include "InjectorMain.h" 
#include "InjectorData.h"
#include "..\Sentinel\SentinelDefines.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CInjectorMain* g_pinjector;
extern CInjectorApp theApp;
//extern CADC g_adc; 	// the Harris ADC object

extern bool g_bKillThread;

void InjectorAutomationThread(void* pvArgs);
void InjectorScheduleRestrictionThread(void* pvArgs);
void InjectorProgrammingThread(void* pvArgs);

#define HARDSTART (1<<autoplay)|(1<<autothread)|(1<<autoswitch)|(1<<autotimed)


//////////////////////////////////////////////////////////////////////
// CInjectorConnectionObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*
CInjectorConnectionObject::CInjectorConnectionObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_pszClientName = NULL;
	m_ulStatus	= INJECTOR_STATUS_UNINIT;
	m_ulFlags = INJECTOR_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
//	m_pAPIConn = NULL;
	m_bKillConnThread = true;
	m_bConnThreadStarted = false;

}

CInjectorConnectionObject::~CInjectorConnectionObject()
{
	m_bKillConnThread = true;
	while(	m_bConnThreadStarted ) Sleep(1);

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
	if(m_pszClientName) free(m_pszClientName); // must use malloc to allocate
}

*/

//////////////////////////////////////////////////////////////////////
// CInjectorScheduleObject Construction/Destruction
//////////////////////////////////////////////////////////////////////



CInjectorScheduleObject::CInjectorScheduleObject()
{
	m_nID=-1;
	m_nChannelID=-1;
	m_nFileID=-1;
	m_nPosition=-1;

	m_pszEventID=NULL;
	m_pszEventTitle=NULL;
	m_nSegment=-1;
	m_dblOnAirTimeLocal=-1.0;
	m_nDurationMS=0;
	m_nSOMMS=-1;  // i.e. not used.
	m_ulStatus=INJECTOR_STATUS_UNINIT;  // various states
//	m_usControl=0xffff; // not defined  was this.  changed to include extended control (revent.extrathree)
	m_ulControl=0xffffffff; // not defined
	m_usType=0xffff; // not defined

	m_pszCreator=NULL;
	m_pszRecKey=NULL;
	m_ulFlags=INJECTOR_STATUS_UNINIT;
	m_pszCaller=NULL;
}

CInjectorScheduleObject::~CInjectorScheduleObject()
{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)
{	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event pointers", "[%s] deleting event at %d->%d", 
		m_pszCaller?m_pszCaller:"null", this, m_pszEventID);  //(Dispatch message)
}

	if(m_pszEventID) free(m_pszEventID); // must use malloc to allocate
	if(m_pszEventTitle) free(m_pszEventTitle); // must use malloc to allocate
	if(m_pszCreator) free(m_pszCreator); // must use malloc to allocate
	if(m_pszRecKey) free(m_pszRecKey); // must use malloc to allocate
	if(m_pszCaller) free(m_pszCaller); // must use malloc to allocate
	
}

//////////////////////////////////////////////////////////////////////
// CInjectorUtilObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CInjectorUtilObject::CInjectorUtilObject()
{
}
CInjectorUtilObject::~CInjectorUtilObject()
{
}

//////////////////////////////////////////////////////////////////////
// CInjectorChannelObject Construction/Destruction
//////////////////////////////////////////////////////////////////////



CInjectorChannelObject::CInjectorChannelObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_ulStatus	= INJECTOR_STATUS_UNINIT;
	m_ulFlags = INJECTOR_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
	m_nChannelID = -1; // unassigned
	m_nHarrisListNum = -1; // unassigned
	m_bKillAutomationThread = true;
	m_bAutomationThreadStarted = false;
	m_bSchedulerThreadStarted = false;
	m_bProgrammingThreadStarted  = false; // programming
	m_bLiveEventsChanged = false;
	m_bTrafficEventsChanged = false;
	m_bLiveEventsScheduleChanged = false;

	m_dblNextOnAir=-1.0;
	m_dblLastOnAir=-1.0;
	m_dblServertime=-1.0;
	m_dblServerLastUpdate=-1.0;
	m_nServerStatus=-1;
	m_nServerBasis=-1;
	m_nServerChanged=-1;

	m_dblListLastUpdate=-1.0;
	m_nListState=-1;
	m_nListChanged=-1;
	m_nListDisplay=-1;
	m_nListSysChange=-1;
	m_nListCount=-1;
	m_nListLookahead=-1;

	m_nAnalysisCounter=0;

	_ftime(&m_timebAutomationTick);
	_ftime(&m_timebRestrictionTick);
	_ftime(&m_timebProgrammingTick);
	_ftime(&m_timebLastProgramTick);
	
	m_dblAutomationTimeEstimate = -1.0; // the harris server time estimate
	m_dblAutomationTimeDiffMS=0.0; // difference in milliseconds between the time harris was set and the time it was read


	m_ppScheduleRestrictions=NULL;
	m_nNumScheduleRestrictions=0;
	InitializeCriticalSection(&m_critScheduleRestrictions);

	m_ppDisplayEvents=NULL;
	m_nNumDisplayEvents=0;
	InitializeCriticalSection(&m_critDisplayEvents);


	m_ppLiveEvents=NULL;
	m_nNumLiveEvents=0;
	InitializeCriticalSection(&m_critLiveEvents);

	m_ppTrafficEvents=NULL;
	m_nNumTrafficEvents=0;
	InitializeCriticalSection(&m_critTrafficEvents);

	m_ppScheduleEvents=NULL;
	m_nNumScheduleEvents=0;
	InitializeCriticalSection(&m_critScheduleEvents);

	m_socketSentinel = NULL;
	InitializeCriticalSection(&m_critSentinelSocket);
	
	InitializeCriticalSection(&m_critServerInfo);

	InitializeCriticalSection(&m_critAdvisories);


	// channel specific settings:
	// for automated video insertion.
	m_pszSettingInsertionAllowID=NULL;
	m_pszSettingInsertionSecondariesFile=NULL;
	m_nSettingMinBifurcationDurMS=INJECTOR_DEF_MIN_BIFURC_MS; // 5 seconds
	m_nSettingMaxAllowDurMS = INJECTOR_DEF_MAX_ALLOW_MS;  // 1 hour
	m_bSettingCoalesceAllow = true;
	m_pszSettingInsertionPrimaryFile=NULL;
	m_pszSettingAllowPrimaryFile=NULL;
	m_ppsecSettingSecInfo = NULL;
	m_nNumSecInfo=0;
	InitializeCriticalSection(&m_critSecInfo);
}


CInjectorChannelObject::~CInjectorChannelObject()
{
	m_bKillAutomationThread = true;

//g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:channel_destory", "%d %d %d", m_bAutomationThreadStarted,m_bSchedulerThreadStarted, m_bProgrammingThreadStarted);    //(Dispatch message)

	while((	m_bAutomationThreadStarted )||(	m_bSchedulerThreadStarted )||( m_bProgrammingThreadStarted )) Sleep(1);

	EnterCriticalSection(&m_critScheduleRestrictions);
	
	if((m_nNumScheduleRestrictions>0)&&(m_ppScheduleRestrictions))
	{
		int i=0;
		while(i<m_nNumScheduleRestrictions)
		{
			if(m_ppScheduleRestrictions[i])
			{
				try {delete m_ppScheduleRestrictions[i];} catch(...){}
			}
			i++;
		}
		try {delete [] m_ppScheduleRestrictions;} catch(...){}
	}
	m_nNumScheduleRestrictions = 0;
	m_ppScheduleRestrictions = NULL;

	LeaveCriticalSection(&m_critScheduleRestrictions);
//g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:channel_destory", "here 01");    //(Dispatch message)

	EnterCriticalSection(&m_critLiveEvents);
	
	if((m_nNumDisplayEvents>0)&&(m_ppDisplayEvents))
	{
		int i=0;
		while(i<m_nNumDisplayEvents)
		{
			if(m_ppDisplayEvents[i])
			{
				try {delete m_ppDisplayEvents[i];} catch(...){}
			}
			i++;
		}
		try {delete [] m_ppDisplayEvents;} catch(...){}
	}
	m_nNumDisplayEvents = 0;
	m_ppDisplayEvents = NULL;

	LeaveCriticalSection(&m_critLiveEvents);


//g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:channel_destory", "here 02");    //(Dispatch message)

	EnterCriticalSection(&m_critLiveEvents);
	
	if((m_nNumLiveEvents>0)&&(m_ppLiveEvents))
	{
		int i=0;
		while(i<m_nNumLiveEvents)
		{
			if(m_ppLiveEvents[i])
			{
				try {delete m_ppLiveEvents[i];} catch(...){}
			}
			i++;
		}
		try {delete [] m_ppLiveEvents;} catch(...){}
	}
	m_nNumLiveEvents = 0;
	m_ppLiveEvents = NULL;

	LeaveCriticalSection(&m_critLiveEvents);
//g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:channel_destory", "here 03, %d", m_nNumTrafficEvents);    //(Dispatch message)


	EnterCriticalSection(&m_critTrafficEvents);
//g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:channel_destory", "here 03b, %d", m_nNumTrafficEvents);    //(Dispatch message)
	
	if((m_nNumTrafficEvents>0)&&(m_ppTrafficEvents))
	{
		int i=0;
//g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:channel_destory", "here 03c, %d", m_nNumTrafficEvents);    //(Dispatch message)
		while(i<m_nNumTrafficEvents)
		{
			if(m_ppTrafficEvents[i])
			{
				try {delete m_ppTrafficEvents[i];} catch(...){}
			}
			i++;
		}
		try {delete [] m_ppTrafficEvents;} catch(...){}
	}
	m_nNumTrafficEvents = 0;
	m_ppTrafficEvents = NULL;

	LeaveCriticalSection(&m_critTrafficEvents);
//g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:channel_destory", "here 04");    //(Dispatch message)

	EnterCriticalSection(&m_critScheduleEvents);
	
	if((m_nNumScheduleEvents>0)&&(m_ppScheduleEvents))
	{
		int i=0;
		while(i<m_nNumScheduleEvents)
		{
			if(m_ppScheduleEvents[i])
			{
				try {delete m_ppScheduleEvents[i];} catch(...){}
			}
			i++;
		}
		try {delete [] m_ppScheduleEvents;} catch(...){}
	}
	m_nNumScheduleEvents = 0;
	m_ppScheduleEvents = NULL;

	LeaveCriticalSection(&m_critScheduleEvents);

//g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:channel_destory", "here 05");    //(Dispatch message)

	EnterCriticalSection(&m_critServerInfo);
	
	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
	if(m_pszSettingInsertionAllowID) free(m_pszSettingInsertionAllowID); // must use malloc to allocate
	if(m_pszSettingInsertionSecondariesFile) free(m_pszSettingInsertionSecondariesFile); // must use malloc to allocate
	if(m_pszSettingInsertionPrimaryFile) free(m_pszSettingInsertionPrimaryFile); // must use malloc to allocate
	if(m_pszSettingAllowPrimaryFile) free(m_pszSettingAllowPrimaryFile); // must use malloc to allocate

	LeaveCriticalSection(&m_critServerInfo);

	EnterCriticalSection(&m_critSecInfo);

	
	if((m_nNumSecInfo>0)&&(m_ppsecSettingSecInfo))
	{
		int i=0;
		while(i<m_nNumSecInfo)
		{
			if(m_ppsecSettingSecInfo[i])
			{
				if(m_ppsecSettingSecInfo[i]->pszID) {	try {free(m_ppsecSettingSecInfo[i]->pszID);} catch(...){} }
				try {delete m_ppsecSettingSecInfo[i];} catch(...){}
			}
			i++;
		}
		try {delete [] m_ppsecSettingSecInfo;} catch(...){}
	}
	m_nNumSecInfo = 0;
	m_ppsecSettingSecInfo = NULL;

	LeaveCriticalSection(&m_critSecInfo);
//g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:channel_destroy", "here 06");    //(Dispatch message)


	DeleteCriticalSection(&m_critSecInfo);
	DeleteCriticalSection(&m_critScheduleRestrictions);
	DeleteCriticalSection(&m_critLiveEvents);
	DeleteCriticalSection(&m_critTrafficEvents);
	DeleteCriticalSection(&m_critScheduleEvents);
	DeleteCriticalSection(&m_critSentinelSocket);
	DeleteCriticalSection(&m_critDisplayEvents);
	DeleteCriticalSection(&m_critServerInfo);
	DeleteCriticalSection(&m_critAdvisories);

}

int CInjectorChannelObject::IsRestricted(CInjectorScheduleObject* pSchedObj)
{
	if(pSchedObj)
	{
		if((m_ppScheduleRestrictions)&&(m_nNumScheduleRestrictions>0))
		{
			int nReturn = INJECTOR_NOT_RESTRICTED;
			int o=0;
			while(o<m_nNumScheduleRestrictions)
			{
				if(m_ppScheduleRestrictions[o])
				{
					if(
							(
								(m_ppScheduleRestrictions[o]->m_dblOnAirTimeLocal<=pSchedObj->m_dblOnAirTimeLocal)
							&&(m_ppScheduleRestrictions[o]->m_dblOnAirTimeLocal+(((double)(m_ppScheduleRestrictions[o]->m_nDurationMS))/1000.0) >pSchedObj->m_dblOnAirTimeLocal)
							)
							||
							(
								(m_ppScheduleRestrictions[o]->m_dblOnAirTimeLocal<=pSchedObj->m_dblOnAirTimeLocal+ (((double)(pSchedObj->m_nDurationMS))/1000.0))
							&&(m_ppScheduleRestrictions[o]->m_dblOnAirTimeLocal+(((double)(m_ppScheduleRestrictions[o]->m_nDurationMS))/1000.0) > (pSchedObj->m_dblOnAirTimeLocal+ (((double)(pSchedObj->m_nDurationMS))/1000.0)))
							)
							||
							(
								(m_ppScheduleRestrictions[o]->m_dblOnAirTimeLocal>=pSchedObj->m_dblOnAirTimeLocal)
							&&(m_ppScheduleRestrictions[o]->m_dblOnAirTimeLocal+(((double)(m_ppScheduleRestrictions[o]->m_nDurationMS))/1000.0) < (pSchedObj->m_dblOnAirTimeLocal+ (((double)(pSchedObj->m_nDurationMS))/1000.0)))
							)
						)
					{
						// the on air time of the item is inside a restriction.
						// this case:
						//  [ restriction item  {sched item} ]
						// and this case too  [ restriction item  {sched item  ]       }

						// or the end of the item is within a restriction
						// this case:
						//  {sched item [ restriction item  } ]

						// or the item completely overlaps a restriction
						// this case:
						//  {sched item [ restriction item   ] }

						return INJECTOR_RESTRICTED;
					}
				}
				o++;
			}
		}
		else return INJECTOR_NOT_RESTRICTED; // not restricted
	}
	return INJECTOR_ERROR;

}

int CInjectorChannelObject::FindNextNonAllow(CInjectorScheduleObject* pSchedObj)
{
	if(pSchedObj)
	{
		if((m_ppLiveEvents)&&(m_nNumLiveEvents>0))
		{
			double dblTime =pSchedObj->m_dblOnAirTimeLocal + (((double)(pSchedObj->m_nDurationMS))/1000.0);
			int nReturn = INJECTOR_ERROR;
			int o=0;
			while(o<m_nNumLiveEvents)
			{
				if(m_ppLiveEvents[o]->m_dblOnAirTimeLocal >= dblTime)
				{
					if(
						  (m_ppLiveEvents[o]->m_pszEventID)
						&&(m_pszSettingInsertionAllowID)
						&&(strcmp(m_ppLiveEvents[o]->m_pszEventID, m_pszSettingInsertionAllowID)) //differs
						)
					{
						return o;
					}
				}
				o++;
			}
		}
	}
	return INJECTOR_ERROR;
}

int CInjectorChannelObject::FindScheduleItem(double dblTime, int nArray, int nFlags)  // returns index
{
	CInjectorScheduleObject*** pppItems = NULL;
	int* pnNumItems = NULL;
	switch(nArray)
	{
	case INJECTOR_ARRAY_SCHEDULE:								//0
		{
			pppItems = &m_ppScheduleEvents;
			pnNumItems = &m_nNumScheduleEvents;
		} break;
	case INJECTOR_ARRAY_SCHEDULE_RESTRICTION:		//1
		{
			pppItems = &m_ppScheduleRestrictions;
			pnNumItems = &m_nNumScheduleRestrictions;
		} break;
	case INJECTOR_ARRAY_TRAFFIC:								//2
		{
			pppItems = &m_ppTrafficEvents;
			pnNumItems = &m_nNumTrafficEvents;
		} break;
	case INJECTOR_ARRAY_LIVE:										//3
		{
			pppItems = &m_ppLiveEvents;
			pnNumItems = &m_nNumLiveEvents;
		} break;
	case INJECTOR_ARRAY_SCHEDULE_DISPLAY:				//4
		{
			pppItems = &m_ppDisplayEvents;
			pnNumItems = &m_nNumDisplayEvents;
		} break;
	default: break;
	}
	if((pppItems==NULL)||(pnNumItems==NULL))
	{
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE))
{
	EnterCriticalSection(&m_critServerInfo);
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "FindScheduleItem restrictions", "%s:%d NULL array %d and/or counter %d pointer in %d", 
	m_pszServerName, m_nHarrisListNum, (pppItems==NULL), (pnNumItems==NULL), nArray);  //(Dispatch message)
	LeaveCriticalSection(&m_critServerInfo);
}		return INJECTOR_ERROR;
	}
	int o=0;
	int contained=INJECTOR_ERROR;
	if(((*pppItems))&&((*pnNumItems)>0))
	{
		// have to find sorted insertion point
if(0)//(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE))
{
	EnterCriticalSection(&m_critServerInfo);
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "FindScheduleItem restrictions", "%s:%d searching array %d with %d items on %d", 
	m_pszServerName, m_nHarrisListNum, (*pppItems), (*pnNumItems), nArray);  //(Dispatch message)
	LeaveCriticalSection(&m_critServerInfo);
}
		while(o<(*pnNumItems))
		{
			if((*pppItems)[o])
			{
if(0)//(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE))
{
	EnterCriticalSection(&m_critServerInfo);
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "FindScheduleItem restrictions", "%s:%d searching array, checking: %.3f to %.3f against %.3f  on %d", 
	m_pszServerName, m_nHarrisListNum, (*pppItems)[o]->m_dblOnAirTimeLocal, (*pppItems)[o]->m_dblOnAirTimeLocal+(((double)((*pppItems)[o]->m_nDurationMS))/1000.0),dblTime,  nArray);  //(Dispatch message)
	LeaveCriticalSection(&m_critServerInfo);
}
				switch(nFlags&INJECTOR_ARRAY_FIND_TYPEMASK)
				{
				case INJECTOR_ARRAY_FIND_AFTER_OR_AT://				2  // the event is after or at the passed in time
					{
						if(
							((*pppItems)[o]->m_dblOnAirTimeLocal >= dblTime-.00001 - ((nFlags&INJECTOR_ARRAY_FIND_USETOLERANCE)?((g_pinjector->m_settings.m_ulAutomationTimeToleranceMS)/1000.0):0.0))
							)
							return o;
					} break;
				case INJECTOR_ARRAY_FIND_AFTER:// 						1  // the event is after the passed in time
					{
						if(
								((*pppItems)[o]->m_dblOnAirTimeLocal > dblTime)
							)
							return o;
					} break;
				case INJECTOR_ARRAY_FIND_CONTAINED://					0  // the passed in time is contained in the event
				default:
					{
							// check subsequent ones for cutoff using contained var
			if(0)//g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_COMPARE))	 // removed, compare is working, logging slows it down
			{
	EnterCriticalSection(&m_critServerInfo);
				g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "FindScheduleItem", "%s:%d comparing: %.3f <= %.3f < %.3f", 
							m_pszServerName, m_nHarrisListNum,
							(*pppItems)[o]->m_dblOnAirTimeLocal,dblTime,
							(*pppItems)[o]->m_dblOnAirTimeLocal+(((double)((*pppItems)[o]->m_nDurationMS))/1000.0) 
							);  //(Dispatch message)
	LeaveCriticalSection(&m_critServerInfo);
			}
						if(
								((*pppItems)[o]->m_dblOnAirTimeLocal<=dblTime+.00001+((nFlags&INJECTOR_ARRAY_FIND_USETOLERANCE)?((g_pinjector->m_settings.m_ulAutomationTimeToleranceMS)/1000.0):0.0))
							&&((*pppItems)[o]->m_dblOnAirTimeLocal+(((double)((*pppItems)[o]->m_nDurationMS))/1000.0) > dblTime-((nFlags&INJECTOR_ARRAY_FIND_USETOLERANCE)?((g_pinjector->m_settings.m_ulAutomationTimeToleranceMS)/1000.0):0.0))
							)
						{
							if(nFlags&INJECTOR_ARRAY_FIND_NOTFOUND)
							{
								if(!((*pppItems)[o]->m_ulFlags&INJECTOR_FLAG_FOUND))
								{
									contained = o;
									//return o;
			if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_COMPARE))	
			{
	EnterCriticalSection(&m_critServerInfo);
				g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "FindScheduleItem", "%s:%d found flagged @ %d", 
							m_pszServerName, m_nHarrisListNum,contained);
	LeaveCriticalSection(&m_critServerInfo);
			}
								}
							}
							else
							{
								contained = o;
								//return o;
			if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_COMPARE))	
			{
	EnterCriticalSection(&m_critServerInfo);
				g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "FindScheduleItem", "%s:%d found @ %d", 
							m_pszServerName, m_nHarrisListNum, contained);
	LeaveCriticalSection(&m_critServerInfo);
			}
							}

						}
						else
						if(contained>=0)
						{
							if((*pppItems)[o]->m_dblOnAirTimeLocal<((*pppItems)[contained]->m_dblOnAirTimeLocal+(((double)((*pppItems)[contained]->m_nDurationMS))/1000.0)))
							{
								// this hard start trumps the previous event, but it did not contain the sought for item.
								// this most likely means it comes after this.  do not return.
							}
							else
							{
								// the item is after the contained item, so go ahead and return
				if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_COMPARE))	
				{
	EnterCriticalSection(&m_critServerInfo);
					g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "FindScheduleItem", "%s:%d returning contained @ %d", 
								m_pszServerName, m_nHarrisListNum,contained);
	LeaveCriticalSection(&m_critServerInfo);
				}
								return contained;
							}
						}
							
					} break;
				}
			}
			o++;
		}
	}
	return contained; // got to the end possibly.  if contained was not found, -1 is returned.  if it was found, it is ok.
}

char* CInjectorChannelObject::CreateRecKey(bool bMilliseconds)
{
	// create an 8 digit ASCII code based on the time to the MS.

	_timeb timestamp;
	_ftime(&timestamp);

	__int64  i64Value;
	if(bMilliseconds) i64Value = (__int64)(timestamp.time)*1000 + (__int64)(timestamp.millitm);
	else i64Value = (__int64)(timestamp.time);

	// a 8 digit base 64 number can go up to: 4 398 046 511 104
	// maximum unixtime to the millisecond is 4 294 967 295 999
	// perfect.

	CBufferUtil bu;
	return bu.ReturnBase64Number(i64Value, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-");
}

int CInjectorUtilObject::AddItem(CInjectorScheduleObject*** pppSched, int* pnCount, CInjectorScheduleObject* pItem)  // returns index
{
	if(pItem)
	{
		CInjectorScheduleObject** ppObj = new CInjectorScheduleObject*[(*pnCount)+1];
		if(ppObj)
		{
			CInjectorScheduleObject** ppDelObj = (*pppSched);
			int o = (*pnCount);
			int index = 0;
			if((ppDelObj)&&(o>0))
			{
				while(index<o)
				{
					ppObj[index] = ppDelObj[index];
					index++;
				}
			}

			ppObj[index] = pItem;
			*pnCount = index+1;
			*pppSched = ppObj;

			if(ppDelObj)
			{
				try{delete [] ppDelObj;} catch(...){}
			}
			return index;
		}
	}

	return INJECTOR_ERROR;

}

int CInjectorChannelObject::InsertScheduleItem(CInjectorScheduleObject* pSchedObj, int nArray)
{
	CInjectorScheduleObject*** pppItems = NULL;
	int* pnNumItems = NULL;
	switch(nArray)
	{
	case INJECTOR_ARRAY_SCHEDULE:								//0
		{
			pppItems = &m_ppScheduleEvents;
			pnNumItems = &m_nNumScheduleEvents;
		} break;
	case INJECTOR_ARRAY_SCHEDULE_RESTRICTION:		//1
		{
			pppItems = &m_ppScheduleRestrictions;
			pnNumItems = &m_nNumScheduleRestrictions;
		} break;
	case INJECTOR_ARRAY_TRAFFIC:								//2
		{
			pppItems = &m_ppTrafficEvents;
			pnNumItems = &m_nNumTrafficEvents;
		} break;
	case INJECTOR_ARRAY_LIVE:										//3
		{
			pppItems = &m_ppLiveEvents;
			pnNumItems = &m_nNumLiveEvents;
		} break;
	case INJECTOR_ARRAY_SCHEDULE_DISPLAY:				//4
		{
			pppItems = &m_ppDisplayEvents;
			pnNumItems = &m_nNumDisplayEvents;
		} break;
	default: break;
	}
	if((pppItems==NULL)||(pnNumItems==NULL))	return INJECTOR_ERROR;

	if(pSchedObj)
	{
		CInjectorScheduleObject** ppObj = new CInjectorScheduleObject*[(*pnNumItems)+1];
		if(ppObj)
		{
			CInjectorScheduleObject** ppDelObj = (*pppItems);
			int o=0;
			int index = -1;
			if(((*pppItems))&&((*pnNumItems)>0))
			{
				// have to find sorted insertion point

				while(o<(*pnNumItems))
				{
					if((*pppItems)[o]->m_dblOnAirTimeLocal<=pSchedObj->m_dblOnAirTimeLocal)
					{
						ppObj[o] = (*pppItems)[o];
						o++;
					}
					else
					{
						o++; break;
					}
				}
				ppObj[o] = pSchedObj;
				index=o;
				while(o<(*pnNumItems))
				{
					ppObj[o+1] = (*pppItems)[o];
					o++;
				}
			}
			else
			{
				ppObj[(*pnNumItems)] = pSchedObj;  // just add the one
				index = (*pnNumItems);
			}

			(*pppItems) = ppObj;
			if(ppDelObj)
			{
				try{delete [] ppDelObj;} catch(...){}
			}
			(*pnNumItems)++;
			return index;
		}
	}

	return INJECTOR_ERROR;
}

/*
int CInjectorChannelObject::DeleteScheduleItem(int index, int nArray)  // removes from array
{
	CInjectorScheduleObject*** pppItems = NULL;
	int* pnNumItems = NULL;
	switch(nArray)
	{
	case INJECTOR_ARRAY_SCHEDULE:								//0
		{
			pppItems = &m_ppScheduleEvents;
			pnNumItems = &m_nNumScheduleEvents;
		} break;
	case INJECTOR_ARRAY_SCHEDULE_RESTRICTION:		//1
		{
			pppItems = &m_ppScheduleRestrictions;
			pnNumItems = &m_nNumScheduleRestrictions;
		} break;
	case INJECTOR_ARRAY_TRAFFIC:								//2
		{
			pppItems = &m_ppTrafficEvents;
			pnNumItems = &m_nNumTrafficEvents;
		} break;
	case INJECTOR_ARRAY_LIVE:										//3
		{
			pppItems = &m_ppLiveEvents;
			pnNumItems = &m_nNumLiveEvents;
		} break;
	case INJECTOR_ARRAY_SCHEDULE_DISPLAY:				//4
		{
			pppItems = &m_ppDisplayEvents;
			pnNumItems = &m_nNumDisplayEvents;
		} break;
	default: break;
	}
	if((pppItems==NULL)||(pnNumItems==NULL))	return INJECTOR_ERROR;

	if((index>=0)&&(index<(*pnNumItems))&&((*pppItems)))
	{
		CInjectorScheduleObject** ppDelObj = (*pppItems);
		if((*pnNumItems)>1)
		{
			CInjectorScheduleObject** ppObj = new CInjectorScheduleObject*[(*pnNumItems)-1];
			if(ppObj)
			{
				CInjectorScheduleObject* pDelEventObj = NULL;
				(*pnNumItems)--;
				int o=0;
				while(o<index)
				{
					ppObj[o] = (*pppItems)[o];
					o++;
				}
				pDelEventObj = (*pppItems)[o];
				while(o<(*pnNumItems))
				{
					ppObj[o] = (*pppItems)[o+1];
					o++;
				}

				(*pppItems) = ppObj;
				if(ppDelObj) {try{ delete [] ppDelObj; } catch(...){}}
				if(pDelEventObj) 
				{
					try
					{ 
						delete pDelEventObj;
					} 
					catch(...){}
				}
				return INJECTOR_SUCCESS;
			}
		}
		else
		{
			if((*pnNumItems)==1)
			{
				try
				{
					delete (*pppItems)[0];
				}
				catch(...)
				{
				}

			}
			(*pnNumItems) = 0;
			(*pppItems) = NULL;
			if(ppDelObj) {try{ delete [] ppDelObj; } catch(...){}}
			return INJECTOR_SUCCESS;
		}
	}
	return INJECTOR_ERROR;
}
*/


int CInjectorChannelObject::SentinelPing()
{
	CNetData netdata; 

	netdata.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_KEEPOPEN;
	netdata.m_ucCmd =  CX_CMD_NULL;//	   0x00
	netdata.m_ucSubCmd = SENTINEL_SCMD_NOSEC;//SENTINEL_SCMD_TIESEC;       // the subcommand byte do explicit, use count

if(0)//g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_COMM))
{	
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelPing", "pinging for %s:%d", 
											 m_pszServerName, m_nHarrisListNum);  //(Dispatch message)
//Sleep(500);
}

EnterCriticalSection(&m_critSentinelSocket);
	int nRV = NET_ERROR;
	if((m_socketSentinel != NULL)&&(!g_bKillThread)&&(!m_bKillAutomationThread))
	{
if(0)//g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_COMM))
{	
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelPing", "calling ping for %s:%d", m_pszServerName, m_nHarrisListNum);  //(Dispatch message)
//Sleep(500);
}
		nRV = m_net.SendData(&netdata, m_socketSentinel, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT);
if(0)//g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_COMM))
{	
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelPing", "called ping");  //(Dispatch message)
//Sleep(500);
}
	}

if(0)//g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_COMM))
{g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelPing", "ping returned %d for %s:%d",  
											 nRV, m_pszServerName, m_nHarrisListNum);  //(Dispatch message)
//Sleep(500);
}


	if(nRV>=NET_SUCCESS)
	{
if(0)//g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_COMM))
{g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelPing", "pinged for %s:%d", 
											 m_pszServerName, m_nHarrisListNum);  //(Dispatch message)
//Sleep(500);
}
		m_net.SendData(NULL, m_socketSentinel, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
if(0)//g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_COMM))
{g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "ADCInsertEventBuffer", "sending ack");  //(Dispatch message)
//Sleep(500);
}

		if(netdata.m_ucCmd == NET_CMD_ACK)
		{
			//no subcommand for ping
/*			if(netdata.m_ucSubCmd == NET_CMD_ACK)
			{
if(0)//g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_COMM))
{g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "SentinelPing", "ping acked on %s:%d",
											  m_pszServerName, m_nHarrisListNum);  //(Dispatch message)
//Sleep(500);

}
LeaveCriticalSection(&m_critSentinelSocket);
	LeaveCriticalSection(&m_critServerInfo);
				_ftime(&m_timebLastProgramTick);

				return INJECTOR_SUCCESS;
			} // else call to modify failed
			else
			{
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_COMM))
{g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "SentinelPing", "Code 0x%02x, pinging for %s:%d failed: %s",
											 netdata.m_ucSubCmd,
											 m_pszServerName, m_nHarrisListNum, netdata.m_pucData?((char*)netdata.m_pucData):"no error message available");  //(Dispatch message)
//Sleep(500);
}
			}
*/
		}//else command to sentinel failed
		else
		{
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_COMM))
{g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "SentinelPing", "Error 0x%02x pinging for %s:%d",
										 netdata.m_ucCmd,
										 m_pszServerName, m_nHarrisListNum);  //(Dispatch message)
//Sleep(500);
}
		}
	}// else network layer failed
	else
	{
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_COMM))
{g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "SentinelPing", "Network error %d pinging for %s:%d",
									 nRV,
									 m_pszServerName, m_nHarrisListNum);  //(Dispatch message)
//Sleep(500);
}

// force a re-connect
		m_net.CloseConnection(m_socketSentinel);
		m_socketSentinel=NULL;

	}
LeaveCriticalSection(&m_critSentinelSocket);
	return INJECTOR_ERROR;

}

int CInjectorChannelObject::ADCInsertEventBuffer(unsigned char* pEvents, int nBufferLen, int nPosition )
{
	EnterCriticalSection(&m_critServerInfo);
	if(g_pinjector->m_settings.m_bSuppressListEdits)
	{
		// just report
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "ADCInsertEventBuffer", "STUB inserting item @ %d on %s:%d", 
											 nPosition, m_pszServerName, m_nHarrisListNum);  //(Dispatch message)

		LeaveCriticalSection(&m_critServerInfo);

		_ftime(&m_timebLastProgramTick);

		return INJECTOR_SUCCESS;

	}
	else
	{
		CNetData netdata; 

		netdata.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;
		netdata.m_ucCmd =  SENTINEL_CMD_INSERT;//	   0xa0
		netdata.m_ucSubCmd = SENTINEL_SCMD_NOSEC;//SENTINEL_SCMD_TIESEC;       // the subcommand byte do explicit, use count

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH+nBufferLen); 
		if(pch)
		{
			sprintf(pch, "%s|%s|%d|%d|%d|%d|", 
				"Injector",
				m_pszServerName, m_nHarrisListNum,
				nPosition, 
				ADC_NORMAL|ADC_PROTECT_DONE|ADC_GETEVENTS_EVENTLOOK, 
				1  // this is ignored, count is taken from buffer.
				); 

			netdata.m_pucData =  (unsigned char*) pch;
			netdata.m_ulDataLen = strlen(pch);

			//if( event.m_usType ) // need to check extended events

			memcpy( netdata.m_pucData + netdata.m_ulDataLen, pEvents, nBufferLen);
			netdata.m_ulDataLen += nBufferLen;
		}

if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))
{	
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "ADCInsertEventBuffer", "inserting item @ %d on %s:%d", 
											 nPosition, m_pszServerName, m_nHarrisListNum);  //(Dispatch message)
//Sleep(500);
}

EnterCriticalSection(&m_critSentinelSocket);
	int nRV = NET_ERROR;
	if((m_socketSentinel != NULL)&&(!g_bKillThread)&&(!m_bKillAutomationThread))
	{
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))
{	
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "ADCInsertEventBuffer", "calling insert");  //(Dispatch message)
//Sleep(500);
}
		nRV = m_net.SendData(&netdata, m_socketSentinel, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT);
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))
{	
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "ADCInsertEventBuffer", "called insert");  //(Dispatch message)
//Sleep(500);
}
	}

if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
{g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "ADCInsertEventBuffer", "insertion returned %d", 
											 nRV);  //(Dispatch message)
//Sleep(500);
}


	if(nRV>=NET_SUCCESS)
	{
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
{g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "ADCInsertEventBuffer", "inserted item @ %d on %s:%d", 
											 nPosition, m_pszServerName, m_nHarrisListNum);  //(Dispatch message)
//Sleep(500);
}
		m_net.SendData(NULL, m_socketSentinel, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
{g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "ADCInsertEventBuffer", "sending ack");  //(Dispatch message)
//Sleep(500);
}

		if(netdata.m_ucCmd == NET_CMD_ACK)
		{
			if(netdata.m_ucSubCmd == NET_CMD_ACK)
			{
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
{g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "ADCInsertEventBuffer", "inserted buffer @ %d on %s:%d",
											 nPosition, m_pszServerName, m_nHarrisListNum);  //(Dispatch message)
//Sleep(500);

}
LeaveCriticalSection(&m_critSentinelSocket);
	LeaveCriticalSection(&m_critServerInfo);
				_ftime(&m_timebLastProgramTick);

				return INJECTOR_SUCCESS;
			} // else call to modify failed
			else
			{
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
{g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "ADCInsertEventBuffer", "Code 0x%02x, insertion of buffer @ %d on %s:%d failed: %s",
											 netdata.m_ucSubCmd,
											 nPosition, m_pszServerName, m_nHarrisListNum, netdata.m_pucData?((char*)netdata.m_pucData):"no error message available");  //(Dispatch message)
//Sleep(500);
}
			}
		}//else command to sentinel failed
		else
		{
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
{g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "ADCInsertEventBuffer", "Error 0x%02x inserting buffer @ %d on %s:%d",
										 netdata.m_ucCmd,
										 nPosition, m_pszServerName, m_nHarrisListNum);  //(Dispatch message)
//Sleep(500);
}
		}
	}// else network layer failed
	else
	{
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
{g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "ADCInsertEventBuffer", "Network error %d inserting buffer @ %d on %s:%d",
									 nRV,
									 nPosition, m_pszServerName, m_nHarrisListNum);  //(Dispatch message)
//Sleep(500);
}

// force a re-connect
		m_net.CloseConnection(m_socketSentinel);
		m_socketSentinel=NULL;

	}
LeaveCriticalSection(&m_critSentinelSocket);
	}
	LeaveCriticalSection(&m_critServerInfo);
		
	return INJECTOR_ERROR;
}

int CInjectorChannelObject::ADCDeleteEvents(int nPosition, int nNumber)
{
	EnterCriticalSection(&m_critServerInfo);
	if(g_pinjector->m_settings.m_bSuppressListEdits)
	{
		// just report
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "ADCDeleteEvents", "STUB deleting %d item%s @ %d on %s:%d", 
											 nNumber, ((nNumber>1)?"s":""), nPosition, m_pszServerName, m_nHarrisListNum);  //(Dispatch message)

		LeaveCriticalSection(&m_critServerInfo);
		_ftime(&m_timebLastProgramTick);
		return INJECTOR_SUCCESS;

	}
	else
	{
		CNetData netdata; 

		netdata.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;
		netdata.m_ucCmd =  SENTINEL_CMD_DELETE;//	   0xa1
		netdata.m_ucSubCmd = SENTINEL_SCMD_NOSEC;//SENTINEL_SCMD_TIESEC;       // the subcommand byte do explicit, use count

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
		if(pch)
		{
			sprintf(pch, "%s|%s|%d|%d|%d|%d", 
				"Injector",
				m_pszServerName, m_nHarrisListNum,
				nPosition, 
				ADC_NORMAL|ADC_PROTECT_PLAY|ADC_PROTECT_DONE|ADC_GETEVENTS_EVENTLOOK, 
				nNumber
				); 

			netdata.m_pucData =  (unsigned char*) pch;
			netdata.m_ulDataLen = strlen(pch);
		}

if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "ADCDeleteEvents", "deleting %d item%s @ %d on %s:%d", 
											 nNumber, ((nNumber>1)?"s":""), nPosition, m_pszServerName, m_nHarrisListNum);  //(Dispatch message)
EnterCriticalSection(&m_critSentinelSocket);
	int nRV = NET_ERROR;
	if((m_socketSentinel != NULL)&&(!g_bKillThread)&&(!m_bKillAutomationThread))
	{
		nRV = m_net.SendData(&netdata, m_socketSentinel, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT);
	}
	if(nRV>=NET_SUCCESS)
	{
		m_net.SendData(NULL, m_socketSentinel, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);

		if(netdata.m_ucCmd == NET_CMD_ACK)
		{
			if(netdata.m_ucSubCmd == NET_CMD_ACK)
			{
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "ADCDeleteEvents", "deleted %d item%s @ %d on %s:%d", 
											 nNumber, ((nNumber>1)?"s":""), nPosition, m_pszServerName, m_nHarrisListNum);  //(Dispatch message)

LeaveCriticalSection(&m_critSentinelSocket);
	LeaveCriticalSection(&m_critServerInfo);

				_ftime(&m_timebLastProgramTick);
				return INJECTOR_SUCCESS;
			} // else call to modify failed
			else
			{
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "ADCDeleteEvents", "Code 0x%02x, deletion of %d item%s @ %d on %s:%d failed: %s", 
											 netdata.m_ucSubCmd,
											 nNumber, ((nNumber>1)?"s":""), nPosition, m_pszServerName, m_nHarrisListNum, netdata.m_pucData?((char*)netdata.m_pucData):"no error message available");  //(Dispatch message)

			}
		}//else command to sentinel failed
		else
		{
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "ADCDeleteEvents", "Error 0x%02x deleting %d item%s @ %d on %s:%d", 
										 netdata.m_ucCmd,
										 nNumber, ((nNumber>1)?"s":""), nPosition, m_pszServerName, m_nHarrisListNum);  //(Dispatch message)

		}
	}// else network layer failed
	else
	{
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "ADCDeleteEvents", "Network error %d deleting %d item%s @ %d on %s:%d", 
									 nRV,
									 nNumber, ((nNumber>1)?"s":""), nPosition, m_pszServerName, m_nHarrisListNum);  //(Dispatch message)

// force a re-connect
		m_net.CloseConnection(m_socketSentinel);
		m_socketSentinel=NULL;

	}
LeaveCriticalSection(&m_critSentinelSocket);
	}
	LeaveCriticalSection(&m_critServerInfo);
		
	return INJECTOR_ERROR;
} 




//////////////////////////////////////////////////////////////////////
// CInjectorData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CInjectorData::CInjectorData()
{
	InitializeCriticalSection(&m_critText);
	InitializeCriticalSection(&m_critSQL);
	InitializeCriticalSection(&m_critChannels);
	InitializeCriticalSection(&m_critClients);
	InitializeCriticalSection(&m_critTraffic);
	InitializeCriticalSection(&m_critRestrictions);
	
	// messaging...
	m_bNetworkMessagingInitialized=false;

	// global events
	m_ppRestrictions=NULL;
	m_nNumRestrictions=0;


//	m_ppConnObj = NULL;
//	m_nNumConnectionObjects = 0;
	m_ppChannelObj = NULL;
	m_nNumChannelObjects = 0;

	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebAutoPurge ); // the last time autopurge was run
	_ftime( &m_timebTick );
	m_pszHost = NULL;			// the name of the host
	m_pszCompleteHost = NULL;	// the complete name of the host
	m_pszCortexHost = NULL;			// the name of the cortex host
	m_usCortexCommandPort = INJECTOR_PORT_CMD;
	m_usCortexStatusPort = INJECTOR_PORT_STATUS;
	m_nSettingsMod = -1;
	m_nChannelsMod = -1;
//	m_nConnectionsMod = -1;
	m_nLastSettingsMod = -1;
	m_nLastChannelsMod = -1;
//	m_nLastConnectionsMod = -1;
	m_nScheduleMod=-1;
//	m_nLastScheduleMod=-1;

	m_nRestrictionsMod=-1;
	m_nLastRestrictionsMod=-1;


	m_nLastTrafficMod =-27;

	m_nQueueMod =-1;
	m_nLastQueueMod =-1;

	m_pdb = NULL;
	m_pdbConn = NULL;
	m_pdb2 = NULL;
	m_pdb2Conn = NULL;

	m_bQuietKill = false;

	m_bProcessSuspended = false;
//	m_bAutomationThreadStarted=false;
//	m_dblLastAutomationChange= -1.0;

}

CInjectorData::~CInjectorData()
{

/*
	if(m_ppConnObj)
	{
		int i=0;
		while(i<m_nNumConnectionObjects)
		{
			if(m_ppConnObj[i]) delete m_ppConnObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppConnObj; // delete array of pointers to objects, must use new to allocate
	}

	if(m_ppChannelObj)
	{
		int i=0;
		while(i<m_nNumChannelObjects)
		{
			if(m_ppChannelObj[i]) delete m_ppChannelObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppChannelObj; // delete array of pointers to objects, must use new to allocate
	}
*/
	if(m_pszHost) free(m_pszHost);	// must use malloc to allocate
	if(m_pszCompleteHost) free(m_pszCompleteHost);	// must use malloc to allocate
	if(m_pszCortexHost) free(m_pszCortexHost);	// must use malloc to allocate

	// global events
	EnterCriticalSection(&m_critRestrictions);
	
	if((m_nNumRestrictions>0)&&(m_ppRestrictions))
	{
		int i=0;
		while(i<m_nNumRestrictions)
		{
			if(m_ppRestrictions[i])
			{
				try {delete m_ppRestrictions[i];} catch(...){}
			}
			i++;
		}
		try {delete [] m_ppRestrictions;} catch(...){}
	}
	m_nNumRestrictions = 0;
	m_ppRestrictions = NULL;

	LeaveCriticalSection(&m_critRestrictions);
	


	DeleteCriticalSection(&m_critRestrictions);


	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critSQL);
	DeleteCriticalSection(&m_critChannels);
	DeleteCriticalSection(&m_critClients);
	DeleteCriticalSection(&m_critTraffic);

}

char* CInjectorData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CInjectorData::SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError)
{
	int nRV = CX_ERROR;
	bool bError=false;

	if(!bOverwriteExistingError)
	{
		if(
			  ((m_ulFlags&INJECTOR_ICON_MASK) == INJECTOR_STATUS_ERROR)
			||((m_ulFlags&INJECTOR_STATUS_CMDSVR_MASK) == INJECTOR_STATUS_CMDSVR_ERROR)
			||((m_ulFlags&INJECTOR_STATUS_STATUSSVR_MASK) ==  INJECTOR_STATUS_STATUSSVR_ERROR)
			||((m_ulFlags&INJECTOR_STATUS_THREAD_MASK) == INJECTOR_STATUS_THREAD_ERROR)
			||((m_ulFlags&INJECTOR_STATUS_FAIL_MASK) == INJECTOR_STATUS_FAIL_DB)
			)	bError=true;
	}

	if (ulStatus&INJECTOR_ICON_MASK)
	{
		m_ulFlags &= ~INJECTOR_ICON_MASK;
		m_ulFlags |= (ulStatus&INJECTOR_ICON_MASK);
	}
	if (ulStatus&INJECTOR_STATUS_CMDSVR_MASK)
	{
		m_ulFlags &= ~INJECTOR_STATUS_CMDSVR_MASK;
		m_ulFlags |= (ulStatus&INJECTOR_STATUS_CMDSVR_MASK);
	}
	if (ulStatus&INJECTOR_STATUS_STATUSSVR_MASK)
	{
		m_ulFlags &= ~INJECTOR_STATUS_STATUSSVR_MASK;
		m_ulFlags |= (ulStatus&INJECTOR_STATUS_STATUSSVR_MASK);
	}
	if (ulStatus&INJECTOR_STATUS_THREAD_MASK)
	{
		m_ulFlags &= ~INJECTOR_STATUS_THREAD_MASK;
		m_ulFlags |= (ulStatus&INJECTOR_STATUS_THREAD_MASK);
	}
	if (ulStatus&INJECTOR_STATUS_FAIL_MASK)
	{
		m_ulFlags &= ~INJECTOR_STATUS_FAIL_MASK;
		m_ulFlags |= (ulStatus&INJECTOR_STATUS_FAIL_MASK);
	}

	if(bError)
	{
		m_ulFlags &= ~INJECTOR_ICON_MASK;
		m_ulFlags |= INJECTOR_STATUS_ERROR;
	}

	if((pszText)&&(!bError))
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
//				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
//			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_pinjector->m_settings.m_pszIconPath)&&(strlen(g_pinjector->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_pinjector->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_pinjector->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_pinjector->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_pinjector->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_pinjector->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}

// utility
int	CInjectorData::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszCompleteHost) free(m_pszCompleteHost);
						m_pszCompleteHost=pch;

						pch = (char*) malloc(nValue+1);
						if(pch!=NULL)
						{
							if(m_pszHost) free(m_pszHost);
							m_pszHost=pch;

							strcpy(m_pszHost, m_pszCompleteHost);
							pch = strchr(m_pszHost, '.');
							if(pch)
							{
								*pch = 0;
							}

							return INJECTOR_SUCCESS;
						}
					}
				}
			}
		}
	}
	return INJECTOR_ERROR;
}



int CInjectorData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_pinjector)&&(g_pinjector->m_settings.m_pszExchange)&&(strlen(g_pinjector->m_settings.m_pszExchange)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s set mod = \
(SELECT case when max(mod) is null then 1 when max(mod) >= %d \
then 1 else max(mod) + 1 end from %s WHERE criterion = '%s') WHERE criterion = '%s'",
			g_pinjector->m_settings.m_pszExchange,
			INJECTOR_DB_MOD_MAX,
			g_pinjector->m_settings.m_pszExchange,
			szTemp, szTemp		
			);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			return INJECTOR_SUCCESS;
		}
	}
	return INJECTOR_ERROR;
}

int CInjectorData::CheckMessages(char* pszInfo)
{
	if((g_pinjector)&&(m_pdbConn)&&(m_pdb)
		&&(g_pinjector->m_settings.m_pszMessages)&&(strlen(g_pinjector->m_settings.m_pszMessages)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_pinjector->m_settings.m_pszMessages)&&(strlen(g_pinjector->m_settings.m_pszMessages)))?g_pinjector->m_settings.m_pszMessages:"Messages",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_pinjector->m_settings.m_nAutoPurgeMessageDays*86400) // local time....
			);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return INJECTOR_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return INJECTOR_ERROR;
}

int CInjectorData::CheckAsRun(char* pszInfo)
{
	if((g_pinjector)&&(m_pdbConn)&&(m_pdb)
		&&(g_pinjector->m_settings.m_pszAsRun)&&(strlen(g_pinjector->m_settings.m_pszAsRun)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_pinjector->m_settings.m_pszAsRun)&&(strlen(g_pinjector->m_settings.m_pszAsRun)))?g_pinjector->m_settings.m_pszAsRun:"AsRun_Log",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_pinjector->m_settings.m_nAutoPurgeAsRunDays*86400) // local time....
			);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return INJECTOR_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return INJECTOR_ERROR;
}

int CInjectorData::CheckSentinelMods(char* pszInfo)
{
	if((g_pinjector)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
//		char errorstring[DB_ERRORSTRING_LEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT mod FROM %s.dbo.%s WHERE criterion = 'DBT_%s'",
				(g_pinjector->m_settings.m_pszSentinelDBName?g_pinjector->m_settings.m_pszSentinelDBName:"Sentinel"),
				(g_pinjector->m_settings.m_pszSentinelExchange?g_pinjector->m_settings.m_pszSentinelExchange:"Exchange"),
				(((g_pinjector->m_settings.m_pszSentinelTraffic)&&(strlen(g_pinjector->m_settings.m_pszSentinelTraffic)))?g_pinjector->m_settings.m_pszSentinelTraffic:"Traffic_Events")
				);

//if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_EXCHANGE)
//	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get Sentinel mods", "%s:%d SQL [%s]", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum, szSQL);  //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = INJECTOR_ERROR;
//			int nIndex = 0;
			if ((!(prs->IsEOF()))&&(prs))
			{
//				CString szCriterion;
//				CString szFlag;
				CString szMod;
//				CString szTemp;
				try
				{
//					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
//					prs->GetFieldValue("flag", szFlag);//HARDCODE
//					szFlag.TrimLeft(); szFlag.TrimRight();
					prs->GetFieldValue("mod", szMod);//HARDCODE
					nReturn = atoi(szMod);
					if(nReturn>0)
					{
						EnterCriticalSection(&g_pinjector->m_data.m_critTraffic);
						if(nReturn!=m_nLastTrafficMod) m_nLastTrafficMod = nReturn;
						LeaveCriticalSection(&g_pinjector->m_data.m_critTraffic);

					}
				}
				catch( ... )
				{
					// dont need this, not in while
//					prs->Close();
//					delete prs;
//					prs=NULL;
//					break;
				}
//				nIndex++;
//				prs->MoveNext();
			}
			prs->Close();

			delete prs;
//			prs = NULL;
			return nReturn;
		}
	}
	return INJECTOR_ERROR;
}

int CInjectorData::CheckDatabaseMods(char* pszInfo)
{
	if((g_pinjector)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY criterion DESC, mod ASC",  // puts suspend at the top, and orders timestamped mods with oldest first! 
			((g_pinjector->m_settings.m_pszExchange)&&(strlen(g_pinjector->m_settings.m_pszExchange)))?g_pinjector->m_settings.m_pszExchange:"Exchange");

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			bool bFileChanges = false;
			int nReturn = INJECTOR_ERROR;
			int nIndex = 0;
			while ((!(prs->IsEOF()))&&(!bFileChanges)&&(prs))
			{
				CString szCriterion;
				CString szFlag;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("flag", szFlag);//HARDCODE
					szFlag.TrimLeft(); szFlag.TrimRight();
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
					prs->Close();
					delete prs;
					prs=NULL;
					break;
				}

//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "retrieved %s, %s, %s", szCriterion, szFlag, szMod);    //(Dispatch message)


				if((g_pinjector->m_settings.m_pszSettings)&&(strlen(g_pinjector->m_settings.m_pszSettings)))
				{
					szTemp.Format("DBT_%s",g_pinjector->m_settings.m_pszSettings);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nSettingsMod = nReturn;
					}
				}

				if((g_pinjector->m_settings.m_pszQueue)&&(strlen(g_pinjector->m_settings.m_pszQueue)))
				{
					szTemp.Format("DBT_%s",g_pinjector->m_settings.m_pszQueue);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nQueueMod = nReturn;
					}
				} 
				
				if((g_pinjector->m_settings.m_pszChannelInfo)&&(strlen(g_pinjector->m_settings.m_pszChannelInfo)))
				{
					szTemp.Format("DBT_%s",g_pinjector->m_settings.m_pszChannelInfo);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nChannelsMod = nReturn;
					}
				}
			
				if((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))
				{
					szTemp.Format("DBT_%s",g_pinjector->m_settings.m_pszSchedule);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						EnterCriticalSection(&g_pinjector->m_data.m_critTraffic); // just use the traffic crit, its ok
						if(nReturn!=m_nScheduleMod) m_nScheduleMod = nReturn;
						LeaveCriticalSection(&g_pinjector->m_data.m_critTraffic); // just use the traffic crit, its ok
					}
				}

				if((g_pinjector->m_settings.m_pszRestrictions)&&(strlen(g_pinjector->m_settings.m_pszRestrictions)))
				{
					szTemp.Format("DBT_%s",g_pinjector->m_settings.m_pszRestrictions);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						EnterCriticalSection(&g_pinjector->m_data.m_critRestrictions); 
						if(nReturn!=m_nRestrictionsMod) m_nRestrictionsMod = nReturn;
						LeaveCriticalSection(&g_pinjector->m_data.m_critRestrictions); 
					}
				}

			
				// get the suspend

				szTemp.Format("Suspend");
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					nReturn = atoi(szMod);
					if(nReturn>0)
					{
						if(!m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Injector is suspended.");  
							g_pinjector->m_data.SetStatusText(errorstring, CX_STATUS_UNKNOWN);
							g_pinjector->m_msgr.DM(MSG_ICONNONE, NULL, "Injector:suspend", "*** Injector has been suspended. ***"); // Sleep(50); //(Dispatch message)
							g_pinjector->SendMsg(CX_SENDMSG_INFO, "Injector:suspend", "Injector has been suspended");
							m_bProcessSuspended = true;
						}
					}
					else
					{
						if(m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Injector is running.");  
							g_pinjector->m_data.SetStatusText(errorstring, CX_STATUS_OK);
							g_pinjector->m_msgr.DM(MSG_ICONNONE, NULL, "Injector:resume", "*** Injector has been resumed. ***");//  Sleep(50); //(Dispatch message)
							g_pinjector->SendMsg(CX_SENDMSG_INFO, "Injector:resume", "Injector has been resumed.");
							m_bProcessSuspended = false;
						}
					}
				}

				nIndex++;
				prs->MoveNext();
			}
			if(prs)
			{
				prs->Close();

				try
				{
					delete prs;
				}
				catch(...)
				{
				}
			}
			prs = NULL;
			return nReturn;
		}
	}
	return INJECTOR_ERROR;
}




/*
int CInjectorData::GetChannels(char* pszInfo)
{
	if((g_pinjector)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_pinjector->m_settings.m_pszChannels)&&(strlen(g_pinjector->m_settings.m_pszChannels)))?g_pinjector->m_settings.m_pszChannels:"Channels");

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = INJECTOR_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szServer = "";
				CString szDesc = "";
				CString szTemp = "";
				unsigned long ulFlags;   // various flags
				int nListID = -1;
				int nChannelID = -1;
				int nTemp;
				bool bFound = false;
				bool bFlagsFound = false;
				try
				{
					prs->GetFieldValue("ID", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nChannelID = nTemp;
					}
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bFlagsFound = true;
						ulFlags = atol(szTemp);
					}
					prs->GetFieldValue("description", szDesc);//HARDCODE
					prs->GetFieldValue("server", szServer);//HARDCODE
					szServer.TrimLeft(); szServer.TrimRight();
					// **** if servername is >16 chars, need to truncate and flag error somewhere.  message?

					prs->GetFieldValue("listid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nListID = nTemp;
					}
				}
				catch( ... )
				{
				}

				if((g_pinjector->m_data.m_ppChannelObj)&&(g_pinjector->m_data.m_nNumChannelObjects))
				{
					nTemp=0;
					while(nTemp<g_pinjector->m_data.m_nNumChannelObjects)
					{
						if(g_pinjector->m_data.m_ppChannelObj[nTemp])
						{
							if(
									(szServer.GetLength()>0)
								&&(szServer.CompareNoCase(g_pinjector->m_data.m_ppChannelObj[nTemp]->m_pszServerName)==0)
								&&(nListID==g_pinjector->m_data.m_ppChannelObj[nTemp]->m_nHarrisListID)
								)
							{
								bFound = true;
								// override with the new changes:
								if(nChannelID>=0) g_pinjector->m_data.m_ppChannelObj[nTemp]->m_nChannelID = nChannelID;

								if(
										(szDesc.GetLength()>0)
									&&(szDesc.CompareNoCase(g_pinjector->m_data.m_ppChannelObj[nTemp]->m_pszDesc))
									)
								{
									if(g_pinjector->m_data.m_ppChannelObj[nTemp]->m_pszDesc) free(g_pinjector->m_data.m_ppChannelObj[nTemp]->m_pszDesc);

									g_pinjector->m_data.m_ppChannelObj[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(g_pinjector->m_data.m_ppChannelObj[nTemp]->m_pszDesc) sprintf(g_pinjector->m_data.m_ppChannelObj[nTemp]->m_pszDesc, szDesc);
								}

								if((bFlagsFound)&&(ulFlags&INJECTOR_FLAG_ENABLED)&&(!((g_pinjector->m_data.m_ppChannelObj[nTemp]->m_ulFlags)&INJECTOR_FLAG_ENABLED)))
								{

// ****  do something here to check out if the list is able to be activated, that is, if it's on a connected connection

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating list: %s...", 
										g_pinjector->m_data.m_ppChannelObj[nTemp]->m_pszDesc?g_pinjector->m_data.m_ppChannelObj[nTemp]->m_pszDesc:g_pinjector->m_data.m_ppChannelObj[nTemp]->m_pszServerName);  
									g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_change", errorstring);    //(Dispatch message)

// **** if it's able to be activated, set the status to connected.
									g_pinjector->m_data.m_ppChannelObj[nTemp]->m_ulStatus = INJECTOR_STATUS_CONN;
								}
								

								if((bFlagsFound)&&(!(ulFlags&INJECTOR_FLAG_ENABLED))&&((g_pinjector->m_data.m_ppChannelObj[nTemp]->m_ulFlags)&INJECTOR_FLAG_ENABLED))
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating list: %s...", 
										g_pinjector->m_data.m_ppChannelObj[nTemp]->m_pszDesc?g_pinjector->m_data.m_ppChannelObj[nTemp]->m_pszDesc:g_pinjector->m_data.m_ppChannelObj[nTemp]->m_pszServerName);  
									g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_change", errorstring);    //(Dispatch message)

									g_pinjector->m_data.m_ppChannelObj[nTemp]->m_ulStatus = INJECTOR_STATUS_NOTCON;
								}

								// and set up the flags
								if(bFlagsFound) g_pinjector->m_data.m_ppChannelObj[nTemp]->m_ulFlags = ulFlags|INJECTOR_FLAG_FOUND;
								else g_pinjector->m_data.m_ppChannelObj[nTemp]->m_ulFlags |= INJECTOR_FLAG_FOUND;
							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szServer.GetLength()>0)&&(nListID>0)) // have to add.
				{

					CInjectorChannelObject* pscho = new CInjectorChannelObject;
					if(pscho)
					{
						CInjectorChannelObject** ppObj = new CInjectorChannelObject*[g_pinjector->m_data.m_nNumChannelObjects+1];
						if(ppObj)
						{
							int o=0;
							if((g_pinjector->m_data.m_ppChannelObj)&&(g_pinjector->m_data.m_nNumChannelObjects>0))
							{
								while(o<g_pinjector->m_data.m_nNumChannelObjects)
								{
									ppObj[o] = g_pinjector->m_data.m_ppChannelObj[o];
									o++;
								}
								delete [] g_pinjector->m_data.m_ppChannelObj;

							}
							ppObj[g_pinjector->m_data.m_nNumChannelObjects] = pscho;
							g_pinjector->m_data.m_ppChannelObj = ppObj;
							g_pinjector->m_data.m_nNumChannelObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szServer.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, szServer);

							ppObj[o]->m_nHarrisListID = nListID;

							if(nChannelID>=0) ppObj[o]->m_nChannelID = nChannelID;

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, szDesc);
							}
//							ppObj[o]->m_usType = not used;
							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|INJECTOR_FLAG_FOUND;
							else ppObj[o]->m_ulFlags |= INJECTOR_FLAG_FOUND;
								

// ****  do something here to check out if the list is able to be activated, that is, if it's on a connected connection
							if((ppObj[o]->m_ulFlags)&INJECTOR_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating list: %s...", 
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName);  
								g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_change", errorstring);    //(Dispatch message)

// **** if it's able to be activated, set the status to connected.
								ppObj[o]->m_ulStatus = INJECTOR_STATUS_CONN;
							}

						}
						else
							delete pscho;
					}
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			prs = NULL;

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<g_pinjector->m_data.m_nNumChannelObjects)
			{
				if((g_pinjector->m_data.m_ppChannelObj)&&(g_pinjector->m_data.m_ppChannelObj[nIndex]))
				{
					if((g_pinjector->m_data.m_ppChannelObj[nIndex]->m_ulFlags)&INJECTOR_FLAG_FOUND)
					{
						(g_pinjector->m_data.m_ppChannelObj[nIndex]->m_ulFlags) &= ~INJECTOR_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(g_pinjector->m_data.m_ppChannelObj[nIndex])
						{
							if((g_pinjector->m_data.m_ppChannelObj[nIndex]->m_ulFlags)&INJECTOR_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating list: %s...", 
									g_pinjector->m_data.m_ppChannelObj[nIndex]->m_pszDesc?g_pinjector->m_data.m_ppChannelObj[nIndex]->m_pszDesc:g_pinjector->m_data.m_ppChannelObj[nIndex]->m_pszServerName);  
								g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_remove", errorstring);    //(Dispatch message)
								g_pinjector->m_data.m_ppChannelObj[nIndex]->m_ulStatus = INJECTOR_STATUS_NOTCON;
							}

							delete g_pinjector->m_data.m_ppChannelObj[nIndex];
							g_pinjector->m_data.m_nNumChannelObjects--;

							int nTemp=nIndex;
							while(nTemp<g_pinjector->m_data.m_nNumChannelObjects)
							{
								g_pinjector->m_data.m_ppChannelObj[nTemp]=g_pinjector->m_data.m_ppChannelObj[nTemp+1];
								nTemp++;
							}
							g_pinjector->m_data.m_ppChannelObj[nTemp] = NULL;
						} else nIndex++;
					}
				}
				else
					nIndex++;
			}

			return nReturn;
		}
	}
	return INJECTOR_ERROR;
}

int CInjectorData::GetConnections(char* pszInfo)
{
	if((g_pinjector)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY server",  //HARDCODE
			((g_pinjector->m_settings.m_pszConnections)&&(strlen(g_pinjector->m_settings.m_pszConnections)))?g_pinjector->m_settings.m_pszConnections:"Connections");
//		g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "GetConnections");  Sleep(250); //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = INJECTOR_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szHost="";
				CString szClient="";
				CString szDesc="";
				CString szTemp;
				unsigned long ulFlags;   // various flags
			//	unsigned short usType;
				int nTemp = -1;
				bool bFlagsFound = false;
				bool bFound = false;
				try
				{
					prs->GetFieldValue("server", szHost);//HARDCODE
					szHost.TrimLeft(); szHost.TrimRight();
					prs->GetFieldValue("description", szDesc);//HARDCODE
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulFlags = atol(szTemp);
					}
					prs->GetFieldValue("client", szClient);//HARDCODE
					szClient.TrimLeft(); szClient.TrimRight();
				}
				catch( ... )
				{
				}

				if((g_pinjector->m_data.m_ppConnObj)&&(g_pinjector->m_data.m_nNumConnectionObjects))
				{
					nTemp=0;
					while(nTemp<g_pinjector->m_data.m_nNumConnectionObjects)
					{
//			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "c2");  Sleep(250); //(Dispatch message)
						if(g_pinjector->m_data.m_ppConnObj[nTemp])
						{
							if((szHost.GetLength()>0)&&(szHost.CompareNoCase(g_pinjector->m_data.m_ppConnObj[nTemp]->m_pszServerName)==0))
							{
								bFound = true;
//			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "c3");  Sleep(250); //(Dispatch message)

								if(
										((bFlagsFound)&&(!(ulFlags&INJECTOR_FLAG_ENABLED))&&((g_pinjector->m_data.m_ppConnObj[nTemp]->m_ulFlags)&INJECTOR_FLAG_ENABLED))
									||(
												(szClient.GetLength()>0)&&(szClient.CompareNoCase(g_pinjector->m_data.m_ppConnObj[nTemp]->m_pszClientName)) // different client name
											&&(
													((bFlagsFound)&&(ulFlags&INJECTOR_FLAG_ENABLED))
												||((!bFlagsFound)&&((g_pinjector->m_data.m_ppConnObj[nTemp]->m_ulFlags)&INJECTOR_FLAG_ENABLED))
												)
										)
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", 
										g_pinjector->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_pinjector->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_pinjector->m_data.m_ppConnObj[nTemp]->m_pszServerName);  
									g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_change", errorstring);    //(Dispatch message)
//									g_pinjector->m_data.m_ppConnObj[nTemp]->m_pDlg->OnDisconnect();
									//**** disconnect

//									g_adc.DisconnectServer(g_pinjector->m_data.m_ppConnObj[nTemp]->m_pszServerName);
									g_pinjector->m_data.m_ppConnObj[nTemp]->m_bKillConnThread = true;
									//**** should check return value....

									g_pinjector->m_data.m_ppConnObj[nTemp]->m_ulStatus = INJECTOR_STATUS_NOTCON;
								}

								if(
										((bFlagsFound)&&(ulFlags&INJECTOR_FLAG_ENABLED)&&(!((g_pinjector->m_data.m_ppConnObj[nTemp]->m_ulFlags)&INJECTOR_FLAG_ENABLED)))
									||(
												(szClient.GetLength()>0)&&(szClient.CompareNoCase(g_pinjector->m_data.m_ppConnObj[nTemp]->m_pszClientName)) // different client name
											&&(
													((bFlagsFound)&&(ulFlags&INJECTOR_FLAG_ENABLED))
												||((!bFlagsFound)&&((g_pinjector->m_data.m_ppConnObj[nTemp]->m_ulFlags)&INJECTOR_FLAG_ENABLED))
												)
										)
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s to %s...", 
										g_pinjector->m_data.m_ppConnObj[nTemp]->m_pszClientName?g_pinjector->m_data.m_ppConnObj[nTemp]->m_pszClientName:"Injector",  
										g_pinjector->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_pinjector->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_pinjector->m_data.m_ppConnObj[nTemp]->m_pszServerName);  
									g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_change", errorstring);    //(Dispatch message)
//									g_pinjector->m_data.m_ppConnObj[nTemp]->m_pDlg->OnButtonConnect();
									//**** if connect set following status

									g_pinjector->m_data.m_ppConnObj[nTemp]->m_bKillConnThread = false;
									g_pinjector->m_data.m_ppConnObj[nTemp]->m_pAPIConn = NULL;
									if(_beginthread(InjectorConnectionThread, 0, (void*)g_pinjector->m_data.m_ppConnObj[nTemp])==-1)
									{
										//error.

										//**MSG
							
										
									}
									//**** should check return value....

									g_pinjector->m_data.m_ppConnObj[nTemp]->m_ulStatus = INJECTOR_STATUS_CONN;
								}

								if(bFlagsFound) g_pinjector->m_data.m_ppConnObj[nTemp]->m_ulFlags = ulFlags|INJECTOR_FLAG_FOUND;
								else g_pinjector->m_data.m_ppConnObj[nTemp]->m_ulFlags |= INJECTOR_FLAG_FOUND;

							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szHost.GetLength()>0)) // have to add.
				{
//			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "adding %s", szHost);  Sleep(250); //(Dispatch message)
					CInjectorConnectionObject* pscno = new CInjectorConnectionObject;
					if(pscno)
					{
//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CInjectorConnectionObject** ppObj = new CInjectorConnectionObject*[g_pinjector->m_data.m_nNumConnectionObjects+1];
						if(ppObj)
						{
//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((g_pinjector->m_data.m_ppConnObj)&&(g_pinjector->m_data.m_nNumConnectionObjects>0))
							{
								while(o<g_pinjector->m_data.m_nNumConnectionObjects)
								{
									ppObj[o] = g_pinjector->m_data.m_ppConnObj[o];
									o++;
								}
								delete [] g_pinjector->m_data.m_ppConnObj;

							}
							ppObj[o] = pscno;
							g_pinjector->m_data.m_ppConnObj = ppObj;
							g_pinjector->m_data.m_nNumConnectionObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szHost.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, "%s", szHost);

							if(szClient.GetLength()<=0)
							{
								szClient = "Injector";
							}

							if(szClient.GetLength()>0)
							{
								ppObj[o]->m_pszClientName = (char*)malloc(szClient.GetLength()+1); 
								if(ppObj[o]->m_pszClientName) sprintf(ppObj[o]->m_pszClientName, "%s", szClient);
							}

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}
//							ppObj[o]->m_usType = not used;
							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|INJECTOR_FLAG_FOUND;
							else ppObj[o]->m_ulFlags |= INJECTOR_FLAG_FOUND;
								

							if((ppObj[o]->m_ulFlags)&INJECTOR_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s to %s...", 
									((ppObj[o]->m_pszClientName)&&(strlen(ppObj[o]->m_pszClientName)))?ppObj[o]->m_pszClientName:"Injector",  
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName);  
								g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_change", errorstring);    //(Dispatch message)

								//**** if connect 

								// create a connection object in g_adc.
								// associate that with the new CInjectorConnectionObject;
								// start the thread.
								ppObj[o]->m_bKillConnThread = false;
								ppObj[o]->m_pAPIConn = NULL;
									g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "beginning thread for %s", szHost);  Sleep(250); //(Dispatch message)
								if(_beginthread(InjectorConnectionThread, 0, (void*)ppObj[o])==-1)
								{
									//error.

									//**MSG
									g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "problem adding %s", szHost);  Sleep(250); //(Dispatch message)

						
									
								}
							//**** should check return value....

								ppObj[o]->m_ulStatus = INJECTOR_STATUS_CONN;
							}
						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			prs = NULL;
//			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<g_pinjector->m_data.m_nNumConnectionObjects)
			{
				if((g_pinjector->m_data.m_ppConnObj)&&(g_pinjector->m_data.m_ppConnObj[nIndex]))
				{
					if((g_pinjector->m_data.m_ppConnObj[nIndex]->m_ulFlags)&INJECTOR_FLAG_FOUND)
					{
						(g_pinjector->m_data.m_ppConnObj[nIndex]->m_ulFlags) &= ~INJECTOR_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(g_pinjector->m_data.m_ppConnObj[nIndex])
						{
							if((g_pinjector->m_data.m_ppConnObj[nIndex]->m_ulFlags)&INJECTOR_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", 
									g_pinjector->m_data.m_ppConnObj[nIndex]->m_pszDesc?g_pinjector->m_data.m_ppConnObj[nIndex]->m_pszDesc:g_pinjector->m_data.m_ppConnObj[nIndex]->m_pszServerName);  
								g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_remove", errorstring);    //(Dispatch message)
//									g_pinjector->m_data.m_ppConnObj[nTemp]->m_pDlg->OnDisconnect();

								//**** disconnect

								g_pinjector->m_data.m_ppConnObj[nIndex]->m_bKillConnThread = false;
								while(g_pinjector->m_data.m_ppConnObj[nIndex]->m_bConnThreadStarted) Sleep(1);

								//g_adc.DisconnectServer(g_pinjector->m_data.m_ppConnObj[nIndex]->m_pszServerName);
								//**** should check return value....

								g_pinjector->m_data.m_ppConnObj[nIndex]->m_ulStatus = INJECTOR_STATUS_NOTCON;
							}

							delete g_pinjector->m_data.m_ppConnObj[nIndex];
							g_pinjector->m_data.m_nNumConnectionObjects--;

							int nTemp=nIndex;
							while(nTemp<g_pinjector->m_data.m_nNumConnectionObjects)
							{
								g_pinjector->m_data.m_ppConnObj[nTemp]=g_pinjector->m_data.m_ppConnObj[nTemp+1];
								nTemp++;
							}
							g_pinjector->m_data.m_ppConnObj[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

//			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
	}
//			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)
	return INJECTOR_ERROR;
}

*/


int CInjectorData::GetQueue(char* pszInfo)
{
	// following removed to deal with wait mode below.
/*
	if(g_miranda.m_bTransferring)
	{
		// something in progress, must wait.
		return INJECTOR_ERROR;
	}
*/
	
	if((g_pinjector)&&(m_pdbConn)&&(m_pdb))
	{

		// let's just get the top (oldest timestamp) item in the queue, and deal with it.

		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE action <> 128 ORDER BY timestamp ASC", //HARDCODE
			((g_pinjector->m_settings.m_pszQueue)&&(strlen(g_pinjector->m_settings.m_pszQueue)))?g_pinjector->m_settings.m_pszQueue:"Queue");

//create table Queue (itemid int identity(1,1) NOT NULL, filename_source varchar(256), filename_dest varchar(256), 
//action int, host varchar(64), timestamp float, username varchar(32));

		CString szFilenameSource = "";
		CString szFilenameDest = "";
		CString szServer = "";
		CString szUsername = "";
		CString szTemp = "";
		int nItemID = -1;
		bool bItemFound = false;
		double dblTimestamp=-57;   // timestamp
		int nAction = -1;
		int nTemp;

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = INJECTOR_ERROR;
			int nIndex = 0;
//			while ((!prs->IsEOF()))
			if ((!prs->IsEOF()))  // just do the one record, if there is one
			{
				try
				{
					prs->GetFieldValue("itemid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nItemID = atoi(szTemp);
						bItemFound = true;
					}
					prs->GetFieldValue("filename_source", szFilenameSource);//HARDCODE
					szFilenameSource.TrimLeft(); szFilenameSource.TrimRight();
					prs->GetFieldValue("filename_dest", szFilenameDest);//HARDCODE
					szFilenameDest.TrimLeft(); szFilenameDest.TrimRight();
					prs->GetFieldValue("action", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nAction = nTemp;
					}
					prs->GetFieldValue("host", szServer);//HARDCODE
					szServer.TrimLeft(); szServer.TrimRight();

					prs->GetFieldValue("timestamp", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						dblTimestamp = atof(szTemp);
					}
					prs->GetFieldValue("username", szUsername);//HARDCODE
					szUsername.TrimLeft(); szUsername.TrimRight();
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();

				} 
				catch( ... )
				{
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			prs = NULL;

//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "got %d things from queue", nIndex);   Sleep(250);//(Dispatch message)

			if((nReturn>0)&&(bItemFound))
			{
				// process the request, then remove it from the queue
//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "got item %d from queue", nItemID);   Sleep(250);//(Dispatch message)


				// send remove SQL
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE itemid = %d",  //HARDCODE
					((g_pinjector->m_settings.m_pszQueue)&&(strlen(g_pinjector->m_settings.m_pszQueue)))?g_pinjector->m_settings.m_pszQueue:"Queue",
					nItemID
					);

/*
Injector:
Queue action IDs
1  delete file from store
2  delete metadata record
3  delete file from store AND delete metadata record
4  move file (will update path in metadata record)
8  archive - moves file to some path, updates metadata record with archive info (date, etc)
19 purge (removes all files and metadata records for files that have non-registered filetypes)
34 clean (removes all metadata records where the file doesn't exist - not archived, and with an ingest date [means, dont delete a placeholder metadata record in anticipation of a new file coming in] ).

Queue table:
create table Queue (itemid int identity(1,1) NOT NULL, filename_source varchar(256), filename_dest varchar(256), 
action int, host varchar(64),timestamp int, username varchar(50));
*/

				if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
				{
					//**MSG
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

				}
			}

			return nReturn;
		}
	}
	return INJECTOR_ERROR;
}

int CInjectorData::GetRestrictions(char* pszInfo)
{
	if((g_pinjector)&&(m_pdbConn)&&(m_pdb))
	{
		char errorstring[MAX_MESSAGE_LENGTH];
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT id, type, start_time, end_time, \
date, weekday FROM %s WHERE type < 2 AND type >= 0 ORDER BY date",  // we only care about types 0 and 1.
			((g_pinjector->m_settings.m_pszRestrictions)&&(strlen(g_pinjector->m_settings.m_pszRestrictions)))?g_pinjector->m_settings.m_pszRestrictions:"Restrictions"
			);

if(g_pinjector->m_settings.m_bDebugSQL)
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get existing daily restriction items", "SQL [%s]", szSQL);  //(Dispatch message)

		int nIndex=0;
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs != NULL) // no error
		{
			int nNumItems=0;
			CInjectorScheduleObject** ppObj = NULL;
			while((!prs->IsEOF())&&(!g_bKillThread))
			{
//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "� NEOF");  Sleep(50);//(Dispatch message)

				CInjectorScheduleObject* pObj =  new CInjectorScheduleObject;
				if(pObj)
				{
					pObj->m_nChannelID = -1;
					// don't bother with creator

					bool bError = false;
					try
					{
						CString szTemp;
						prs->GetFieldValue("id", szTemp);
						pObj->m_nID = atoi(szTemp);

						prs->GetFieldValue("type", szTemp);
						pObj->m_usType = (unsigned short)atoi(szTemp);

						switch(pObj->m_usType)
						{
						case INJECTOR_RESTRICTION_WHOLEDAY://   0
							{
								prs->GetFieldValue("date", szTemp);
								pObj->m_dblOnAirTimeLocal = atof(szTemp);
							} break;
						case INJECTOR_RESTRICTION_WEEKDAY://		1
							{
								prs->GetFieldValue("weekday", szTemp);
								pObj->m_nDurationMS = atoi(szTemp)-1;  // -1 to get to 0-based index
							} break;
						case INJECTOR_RESTRICTION_STARTDAY://   2
							{
								prs->GetFieldValue("end_time", szTemp);
								pObj->m_dblOnAirTimeLocal = atof(szTemp)/1000.0;
							} break;
						case INJECTOR_RESTRICTION_ENDDAY://			3
							{
								prs->GetFieldValue("start_time", szTemp);
								pObj->m_dblOnAirTimeLocal = atof(szTemp)/1000.0;
							} break;
						}

if(g_pinjector->m_settings.m_bDebugSQL)
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get daily restriction items", "retrieved: %d type %d, value=%.3f", 
								pObj->m_nID,
								pObj->m_usType,
								(pObj->m_usType==INJECTOR_RESTRICTION_WEEKDAY)?((double)(pObj->m_nDurationMS)):pObj->m_dblOnAirTimeLocal
		);  //(Dispatch message)
}

					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						bError = true;
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
							g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get existing daily restriction items", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get existing daily restriction items", "Retrieve: Caught exception: out of memory.\n%s", szSQL);
	//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
						}
						else 
						{
							g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get existing daily restriction items", "Retrieve: Caught other exception.\n%s", szSQL);
	//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
						}
						e->Delete();

					} 
					catch( ... )
					{
						bError = true;
						g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get existing daily restriction items", "Retrieve: Caught exception.\n%s", szSQL);
					}

					if(bError)
					{
						try{ delete pObj; } catch(...){}
					}
					else
					{

						CInjectorScheduleObject** ppObjNew = new CInjectorScheduleObject*[nNumItems+1];
						if(ppObjNew)
						{
							int i=0;
							if(ppObj)
							{
								while(i<nNumItems)
								{
									ppObjNew[i] = ppObj[i];
									i++;
								}
								try{ delete [] ppObj; } catch(...){}
							}
							ppObjNew[i]=pObj;
							nNumItems=i+1;

							ppObj = ppObjNew;

						}
						else
						{
							try{ delete pObj; } catch(...){}
						}

					}
				}
				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			delete prs;

			//here we are finished populating the array, swap out the real array

			// just wipe the old array:
			if((m_nNumRestrictions>0)&&(m_ppRestrictions))
			{
				int i=0;
				while(i<m_nNumRestrictions)
				{
					if(m_ppRestrictions[i])
					{
						m_ppRestrictions[i]->m_pszCaller = (char*)malloc(64); if(m_ppRestrictions[i]->m_pszCaller) strcpy(m_ppRestrictions[i]->m_pszCaller, "get_restrictions");
						try {delete m_ppRestrictions[i];} catch(...){}
					}
					i++;
				}
				try {delete [] m_ppRestrictions;} catch(...){}
			}
			// and reassign
			m_nNumRestrictions = nNumItems;
			m_ppRestrictions = ppObj;

			if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
			{
				g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Got daily restriction items", "%d daily restriction items obtained.", m_nNumRestrictions );  //(Dispatch message)
				int g=0;
				while(g<m_nNumRestrictions)
				{
					if(m_ppRestrictions[g])
					{
						g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Daily restriction items", "  restriction items %d, type %d, value %.3f.", g,
							m_ppRestrictions[g]->m_usType, 
							(m_ppRestrictions[g]->m_usType==1)?((double)m_ppRestrictions[g]->m_nDurationMS):m_ppRestrictions[g]->m_dblOnAirTimeLocal
							);  //(Dispatch message)
					}

					g++;
				}
			}
			return INJECTOR_SUCCESS;

		}
		else
		{
		//	prs was null
			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get existing daily restriction items", "Retrieve returned a NULL recordset.\n%s\n%s", szSQL, errorstring);
		}
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Got existing daily restriction items", "%d daily restriction items retrieved.", nIndex );  //(Dispatch message)

	}
	return INJECTOR_ERROR;
}

int CInjectorData::GetChannels(char* pszInfo)
{
	if((g_pinjector)&&(m_pdbConn)&&(m_pdb))
	{

/*		
		
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY server",  //HARDCODE
			((g_pinjector->m_settings.m_pszChannelInfo)&&(strlen(g_pinjector->m_settings.m_pszChannelInfo)))?g_pinjector->m_settings.m_pszChannelInfo:"ChannelInfo");
//		g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "GetConnections");  Sleep(250); //(Dispatch message)
*/

		CBufferUtil bu;

		char errorstring[MAX_MESSAGE_LENGTH];
		char szSQL[DB_SQLSTRING_MAXLEN];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, server, listid, server_time, server_status, server_basis, server_changed, server_last_update, \
list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update%s FROM %s ORDER by server, listid",
				(g_pinjector->m_settings.m_bChannelInfoViewHasDesc?", channel_description":""),
				(g_pinjector->m_settings.m_pszChannelInfo?g_pinjector->m_settings.m_pszChannelInfo:"ChannelInfo")
				);

		CInjectorChannelObject tempChannelObj;
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CHANNELS)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "GetChannels SQL", "GetChannels [%s]", szSQL);  //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = INJECTOR_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
/*
				CString szHost="";
				CString szClient="";
				CString szDesc="";
				CString szTemp;
				unsigned long ulFlags;   // various flags
			//	unsigned short usType;
				bool bFlagsFound = false;
*/
				if(tempChannelObj.m_pszServerName) free(tempChannelObj.m_pszServerName); 
				tempChannelObj.m_pszServerName = NULL;
				if(tempChannelObj.m_pszDesc) free(tempChannelObj.m_pszDesc); 
				tempChannelObj.m_pszDesc = NULL;

				int nTemp = -1;
				CString szServer = "";
				bool bFound = false;

				try
				{
					CString szTemp;
					prs->GetFieldValue("ID", szTemp);
					tempChannelObj.m_nChannelID = atoi(szTemp);
					
					prs->GetFieldValue("server", szServer);
					tempChannelObj.m_pszServerName = (char*)malloc(szServer.GetLength()+1);
					if( tempChannelObj.m_pszServerName ) sprintf(tempChannelObj.m_pszServerName, "%s", szServer);

					prs->GetFieldValue("listid", szTemp);
					tempChannelObj.m_nHarrisListNum = atoi(szTemp);

					prs->GetFieldValue("server_time", szTemp);
					tempChannelObj.m_dblServertime = atof(szTemp);

					prs->GetFieldValue("server_status", szTemp);
					tempChannelObj.m_nServerStatus = atoi(szTemp);

					prs->GetFieldValue("server_basis", szTemp);
					tempChannelObj.m_nServerBasis = (int)bu.xtol(szTemp.GetBuffer(0),2);  //expected width is 2 for normal frame rates

					prs->GetFieldValue("server_changed", szTemp);
					tempChannelObj.m_nServerChanged = atoi(szTemp);

					prs->GetFieldValue("server_last_update", szTemp);
					tempChannelObj.m_dblServerLastUpdate = atof(szTemp);

					prs->GetFieldValue("list_state", szTemp);
					tempChannelObj.m_nListState = atoi(szTemp);

					prs->GetFieldValue("list_changed", szTemp);
					tempChannelObj.m_nListChanged = atoi(szTemp);

					prs->GetFieldValue("list_display", szTemp);
					tempChannelObj.m_nListDisplay = atoi(szTemp);

					prs->GetFieldValue("list_syschange", szTemp);
					tempChannelObj.m_nListSysChange = atoi(szTemp);

					prs->GetFieldValue("list_count", szTemp);
					tempChannelObj.m_nListCount = atoi(szTemp);

					prs->GetFieldValue("list_lookahead", szTemp);
					tempChannelObj.m_nListLookahead = atoi(szTemp);

					prs->GetFieldValue("list_last_update", szTemp);
					tempChannelObj.m_dblListLastUpdate = atof(szTemp);

					if(g_pinjector->m_settings.m_bChannelInfoViewHasDesc)
					{
						prs->GetFieldValue("channel_description", szTemp);
						if(szTemp.GetLength())
						{
							tempChannelObj.m_pszDesc = (char*)malloc(szTemp.GetLength()+1);
							if( tempChannelObj.m_pszDesc ) sprintf(tempChannelObj.m_pszDesc, "%s", szTemp);
						}
					}

				}
				catch( ... )
				{
				}

				if((m_ppChannelObj)&&(m_nNumChannelObjects))
				{
					nTemp=0;
					while(nTemp<m_nNumChannelObjects)
					{
//			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "c2");  Sleep(250); //(Dispatch message)
						if(m_ppChannelObj[nTemp])
						{
	EnterCriticalSection(&m_ppChannelObj[nTemp]->m_critServerInfo);
							if(
								  (szServer.GetLength()>0)
								&&(
										(					
										  (szServer.CompareNoCase(m_ppChannelObj[nTemp]->m_pszServerName)==0)
										&&(tempChannelObj.m_nHarrisListNum == m_ppChannelObj[nTemp]->m_nHarrisListNum)
										)
									)
								)
							{
	LeaveCriticalSection(&m_ppChannelObj[nTemp]->m_critServerInfo);
							
								bFound = true;
								// update description
								if(g_pinjector->m_settings.m_bChannelInfoViewHasDesc)
								{
									if(
											(!m_ppChannelObj[nTemp]->m_pszDesc)&&(tempChannelObj.m_pszDesc)
										  ||((m_ppChannelObj[nTemp]->m_pszDesc)&&(tempChannelObj.m_pszDesc)&&(strcmp(m_ppChannelObj[nTemp]->m_pszDesc, tempChannelObj.m_pszDesc)))
										)
									{
										char* pch = m_ppChannelObj[nTemp]->m_pszDesc;
										m_ppChannelObj[nTemp]->m_pszDesc = tempChannelObj.m_pszDesc;
										tempChannelObj.m_pszDesc = NULL;
										if(pch) free(pch);
									}
								}

//			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "c3");  Sleep(250); //(Dispatch message)
								m_ppChannelObj[nTemp]->m_bKillAutomationThread = false;
//								m_ppChannelObj[nTemp]->m_pInjector = g_pinjector;
//								m_ppChannelObj[nTemp]->m_pEndpoint = pEndObj;

//								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning analysis for %s:%d...", szServer, tempChannelObj.m_nHarrisListID);
//								g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_change", errorstring);    //(Dispatch message)
								if(m_ppChannelObj[nTemp]->m_bAutomationThreadStarted == false)
								{
	EnterCriticalSection(&m_ppChannelObj[nTemp]->m_critServerInfo);
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning automation analysis for %s:%d (%s)...", m_ppChannelObj[nTemp]->m_pszServerName, m_ppChannelObj[nTemp]->m_nHarrisListNum, (m_ppChannelObj[nTemp]->m_pszDesc?m_ppChannelObj[nTemp]->m_pszDesc:(m_ppChannelObj[nTemp]->m_pszServerName?m_ppChannelObj[nTemp]->m_pszServerName:"unknown")));
	LeaveCriticalSection(&m_ppChannelObj[nTemp]->m_critServerInfo);
									g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_change", errorstring);    //(Dispatch message)

									//**** if connect set following status
									if(_beginthread(InjectorAutomationThread, 0, (void*)m_ppChannelObj[nTemp])==-1)
									{
										//error.
	EnterCriticalSection(&m_ppChannelObj[nTemp]->m_critServerInfo);
										g_pinjector->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Injector:automation", "Error starting automation thread for %s:%d (%s)", m_ppChannelObj[nTemp]->m_pszServerName, m_ppChannelObj[nTemp]->m_nHarrisListNum, (m_ppChannelObj[nTemp]->m_pszDesc?m_ppChannelObj[nTemp]->m_pszDesc:(m_ppChannelObj[nTemp]->m_pszServerName?m_ppChannelObj[nTemp]->m_pszServerName:"unknown")));  //Sleep(250); //(Dispatch message)
	LeaveCriticalSection(&m_ppChannelObj[nTemp]->m_critServerInfo);
										//**MSG
									}
									//**** should check return value....
								}
								if(m_ppChannelObj[nTemp]->m_bSchedulerThreadStarted == false)
								{
	EnterCriticalSection(&m_ppChannelObj[nTemp]->m_critServerInfo);
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning scheduler for %s:%d (%s)...", m_ppChannelObj[nTemp]->m_pszServerName, m_ppChannelObj[nTemp]->m_nHarrisListNum, (m_ppChannelObj[nTemp]->m_pszDesc?m_ppChannelObj[nTemp]->m_pszDesc:(m_ppChannelObj[nTemp]->m_pszServerName?m_ppChannelObj[nTemp]->m_pszServerName:"unknown")));
	LeaveCriticalSection(&m_ppChannelObj[nTemp]->m_critServerInfo);
									g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_change", errorstring);    //(Dispatch message)

									//**** if connect set following status
									if(_beginthread(InjectorScheduleRestrictionThread, 0, (void*)m_ppChannelObj[nTemp])==-1)
									{
										//error.
	EnterCriticalSection(&m_ppChannelObj[nTemp]->m_critServerInfo);
										g_pinjector->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Injector:scheduler", "Error starting scheduler thread for %s:%d (%s)", m_ppChannelObj[nTemp]->m_pszServerName, m_ppChannelObj[nTemp]->m_nHarrisListNum, (m_ppChannelObj[nTemp]->m_pszDesc?m_ppChannelObj[nTemp]->m_pszDesc:(m_ppChannelObj[nTemp]->m_pszServerName?m_ppChannelObj[nTemp]->m_pszServerName:"unknown")));  //Sleep(250); //(Dispatch message)
	LeaveCriticalSection(&m_ppChannelObj[nTemp]->m_critServerInfo);
										//**MSG
									}
									//**** should check return value....
								}

								if(m_ppChannelObj[nTemp]->m_bProgrammingThreadStarted == false)
								{
	EnterCriticalSection(&m_ppChannelObj[nTemp]->m_critServerInfo);
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning progamming for %s:%d (%s)...", m_ppChannelObj[nTemp]->m_pszServerName, m_ppChannelObj[nTemp]->m_nHarrisListNum, (m_ppChannelObj[nTemp]->m_pszDesc?m_ppChannelObj[nTemp]->m_pszDesc:(m_ppChannelObj[nTemp]->m_pszServerName?m_ppChannelObj[nTemp]->m_pszServerName:"unknown")));
	LeaveCriticalSection(&m_ppChannelObj[nTemp]->m_critServerInfo);
									g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_change", errorstring);    //(Dispatch message)

									//**** if connect set following status
									if(_beginthread(InjectorProgrammingThread, 0, (void*)m_ppChannelObj[nTemp])==-1)
									{
										//error.
	EnterCriticalSection(&m_ppChannelObj[nTemp]->m_critServerInfo);
										g_pinjector->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Injector:programming", "Error starting programming thread for %s:%d (%s)", m_ppChannelObj[nTemp]->m_pszServerName, m_ppChannelObj[nTemp]->m_nHarrisListNum, (m_ppChannelObj[nTemp]->m_pszDesc?m_ppChannelObj[nTemp]->m_pszDesc:(m_ppChannelObj[nTemp]->m_pszServerName?m_ppChannelObj[nTemp]->m_pszServerName:"unknown")));  //Sleep(250); //(Dispatch message)
	LeaveCriticalSection(&m_ppChannelObj[nTemp]->m_critServerInfo);
										//**MSG
									}
									//**** should check return value....
								}



/*
								Sleep(30);
								if(m_ppChannelObj[nTemp]->m_bNearAnalysisThreadStarted == false)
								{
									if(_beginthread(InjectorNearAnalysisThread, 0, (void*)m_ppChannelObj[nTemp])==-1)
									{
										//error.

										//**MSG
									}
									//**** should check return value....
								}
*/
								Sleep(30);
/*
								if(m_ppChannelObj[nTemp]->m_bTriggerThreadStarted == false)
								{
									if(_beginthread(InjectorTriggerThread, 0, (void*)m_ppChannelObj[nTemp])==-1)
									{
										//error.

										//**MSG
									}
									//**** should check return value....
								}
								Sleep(30);

*/
								m_ppChannelObj[nTemp]->m_ulFlags |= INJECTOR_FLAG_FOUND;

							}
							else
							{
								LeaveCriticalSection(&m_ppChannelObj[nTemp]->m_critServerInfo);
							}

						}
						nTemp++;
					}
				}

				if((!bFound)&&(szServer.GetLength()>0)) // have to add.
				{
//			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "adding %s", szHost);  Sleep(250); //(Dispatch message)
					CInjectorChannelObject* pscno = new CInjectorChannelObject;
					if(pscno)
					{
//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CInjectorChannelObject** ppObj = new CInjectorChannelObject*[m_nNumChannelObjects+1];
						if(ppObj)
						{
//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((m_ppChannelObj)&&(m_nNumChannelObjects>0))
							{
								while(o<m_nNumChannelObjects)
								{
									ppObj[o] = m_ppChannelObj[o];
									o++;
								}
								delete [] m_ppChannelObj;

							}
							ppObj[o] = pscno;
							m_ppChannelObj = ppObj;
							m_nNumChannelObjects++;

//							ppObj[o]->m_pszServerName = (char*)malloc(szServer.GetLength()+1); 
//							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, "%s", szServer);
	EnterCriticalSection(&ppObj[o]->m_critServerInfo);
							ppObj[o]->m_pszServerName = tempChannelObj.m_pszServerName;  // just assign it;
	LeaveCriticalSection(&ppObj[o]->m_critServerInfo);
							tempChannelObj.m_pszServerName = NULL;//and reset it

						
							ppObj[o]->m_bKillAutomationThread = false;
//							ppObj[o]->m_pInjector = g_pinjector;
//							ppObj[o]->m_pEndpoint = pEndObj;


							ppObj[o]->m_nChannelID = tempChannelObj.m_nChannelID;  // the unique Channel ID within Sentinel setup. (assigned externally)
							ppObj[o]->m_nHarrisListNum = tempChannelObj.m_nHarrisListNum;  // the 1-based List # on the associated ADC-100 server
//							ppObj[o]->m_nOmnibusPort = tempChannelObj.m_nOmnibusPort; // the Omnibus port number

							ppObj[o]->m_dblServertime = tempChannelObj.m_dblServertime; // in number of ms since epoch
							ppObj[o]->m_dblServerLastUpdate = tempChannelObj.m_dblServerLastUpdate; // in number of ms since epoch
							ppObj[o]->m_nServerStatus = tempChannelObj.m_nServerStatus;
							ppObj[o]->m_nServerBasis = tempChannelObj.m_nServerBasis;
							ppObj[o]->m_nServerChanged = tempChannelObj.m_nServerChanged;

							ppObj[o]->m_dblListLastUpdate = tempChannelObj.m_dblListLastUpdate;   // in number of ms since epoch
							ppObj[o]->m_nListState = tempChannelObj.m_nListState;
							ppObj[o]->m_nListChanged = -1; //tempChannelObj.m_nListChanged; // forces change
							ppObj[o]->m_nListDisplay = tempChannelObj.m_nListDisplay;
							ppObj[o]->m_nListSysChange = tempChannelObj.m_nListSysChange;
							ppObj[o]->m_nListCount = tempChannelObj.m_nListCount;
							ppObj[o]->m_nListLookahead = tempChannelObj.m_nListLookahead;

							if((g_pinjector->m_settings.m_bChannelInfoViewHasDesc)&&(tempChannelObj.m_pszDesc))
							{
								ppObj[o]->m_pszDesc = tempChannelObj.m_pszDesc; // just assign it;
								tempChannelObj.m_pszDesc = NULL;//and reset it
							}

							if(ppObj[o]->m_bAutomationThreadStarted == false)
							{
	EnterCriticalSection(&ppObj[o]->m_critServerInfo);
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning automation analysis for %s:%d (%s)...", ppObj[o]->m_pszServerName, ppObj[o]->m_nHarrisListNum, (ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:(ppObj[o]->m_pszServerName?ppObj[o]->m_pszServerName:"unknown")));
	LeaveCriticalSection(&ppObj[o]->m_critServerInfo);
								g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_change", errorstring);    //(Dispatch message)

								//**** if connect set following status
								if(_beginthread(InjectorAutomationThread, 0, (void*)ppObj[o])==-1)
								{
									//error.
	EnterCriticalSection(&ppObj[o]->m_critServerInfo);
										g_pinjector->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Injector:automation", "Error starting automation thread for %s:%d (%s)", ppObj[o]->m_pszServerName, ppObj[o]->m_nHarrisListNum, (ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:(ppObj[o]->m_pszServerName?ppObj[o]->m_pszServerName:"unknown")));  //Sleep(250); //(Dispatch message)
	LeaveCriticalSection(&ppObj[o]->m_critServerInfo);

									//**MSG
								}
								//**** should check return value....
							}
							if(ppObj[o]->m_bSchedulerThreadStarted == false)
							{
	EnterCriticalSection(&ppObj[o]->m_critServerInfo);
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning scheduler for %s:%d (%s)...", ppObj[o]->m_pszServerName, ppObj[o]->m_nHarrisListNum, (ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:(ppObj[o]->m_pszServerName?ppObj[o]->m_pszServerName:"unknown")));
	LeaveCriticalSection(&ppObj[o]->m_critServerInfo);
								g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_change", errorstring);    //(Dispatch message)

								//**** if connect set following status
								if(_beginthread(InjectorScheduleRestrictionThread, 0, (void*)ppObj[o])==-1)
								{
									//error.
	EnterCriticalSection(&ppObj[o]->m_critServerInfo);
										g_pinjector->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Injector:scheduler", "Error starting scheduler thread for %s:%d (%s)", ppObj[o]->m_pszServerName, ppObj[o]->m_nHarrisListNum, (ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:(ppObj[o]->m_pszServerName?ppObj[o]->m_pszServerName:"unknown")));  //Sleep(250); //(Dispatch message)
	LeaveCriticalSection(&ppObj[o]->m_critServerInfo);

									//**MSG
								}
								//**** should check return value....
							}
							if(ppObj[o]->m_bProgrammingThreadStarted == false)
							{
	EnterCriticalSection(&ppObj[o]->m_critServerInfo);
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning progamming for %s:%d (%s)...", ppObj[o]->m_pszServerName, ppObj[o]->m_nHarrisListNum, (ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:(ppObj[o]->m_pszServerName?ppObj[o]->m_pszServerName:"unknown")));
	LeaveCriticalSection(&ppObj[o]->m_critServerInfo);
								g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_change", errorstring);    //(Dispatch message)

								//**** if connect set following status
								if(_beginthread(InjectorProgrammingThread, 0, (void*)ppObj[o])==-1)
								{
									//error.
	EnterCriticalSection(&ppObj[o]->m_critServerInfo);
										g_pinjector->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Injector:programming", "Error starting programming thread for %s:%d (%s)", ppObj[o]->m_pszServerName, ppObj[o]->m_nHarrisListNum, (ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:(ppObj[o]->m_pszServerName?ppObj[o]->m_pszServerName:"unknown")));  //Sleep(250); //(Dispatch message)
	LeaveCriticalSection(&ppObj[o]->m_critServerInfo);

									//**MSG
								}
								//**** should check return value....
							}

/*
							Sleep(30);
							if(ppObj[o]->m_bNearAnalysisThreadStarted == false)
							{
								if(_beginthread(InjectorNearAnalysisThread, 0, (void*)ppObj[o])==-1)
								{
									//error.

									//**MSG
								}
								//**** should check return value....
							}

							Sleep(30);
							if(ppObj[o]->m_bTriggerThreadStarted == false)
							{
								if(_beginthread(InjectorTriggerThread, 0, (void*)ppObj[o])==-1)
								{
									//error.

									//**MSG
								}
								//**** should check return value....
							}
*/
							Sleep(30);

							ppObj[o]->m_ulFlags |= INJECTOR_FLAG_FOUND;
							
						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<m_nNumChannelObjects)
			{
				if((m_ppChannelObj)&&(m_ppChannelObj[nIndex]))
				{
					if((m_ppChannelObj[nIndex]->m_ulFlags)&INJECTOR_FLAG_FOUND)
					{
						(m_ppChannelObj[nIndex]->m_ulFlags) &= ~INJECTOR_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(m_ppChannelObj[nIndex])
						{
	EnterCriticalSection(&m_ppChannelObj[nIndex]->m_critServerInfo);
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending analysis and triggering for %s:%d...", 
														m_ppChannelObj[nIndex]->m_pszServerName, m_ppChannelObj[nIndex]->m_nHarrisListNum );  
	LeaveCriticalSection(&m_ppChannelObj[nIndex]->m_critServerInfo);
							g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_remove", errorstring);    //(Dispatch message)
//									m_ppChannelObj[nTemp]->m_pDlg->OnDisconnect();

							//**** disconnect
							m_ppChannelObj[nIndex]->m_bKillAutomationThread = true;

							int ixi = 0;
							while (
											(
												(m_ppChannelObj[nIndex]->m_bAutomationThreadStarted)
											||(m_ppChannelObj[nIndex]->m_bSchedulerThreadStarted)
											||(m_ppChannelObj[nIndex]->m_bProgrammingThreadStarted)
//											||(m_ppChannelObj[nIndex]->m_bTriggerThreadStarted)	
//											||(m_ppChannelObj[nIndex]->m_bNearAnalysisThreadStarted)	
											)
										&&(ixi<500) // half a second max, this stalls the get thread.
										)
							{
								ixi++;
								Sleep(1);
							}
							//g_adc.DisconnectServer(m_ppChannelObj[nIndex]->m_pszServerName);
							//**** should check return value....

	EnterCriticalSection(&m_ppChannelObj[nIndex]->m_critServerInfo);
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ended analysis and triggering for %s:%d...", 
													m_ppChannelObj[nIndex]->m_pszServerName, m_ppChannelObj[nIndex]->m_nHarrisListNum );  
	LeaveCriticalSection(&m_ppChannelObj[nIndex]->m_critServerInfo);
							g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_remove", errorstring);    //(Dispatch message)
							

							CInjectorChannelObject* pTemp = m_ppChannelObj[nIndex];

			//				try{delete m_ppChannelObj[nIndex];} catch(...){}  // stalls, no idea why


/*
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s:%d channels =%d index=%d", 
													m_ppChannelObj[nIndex]->m_pszServerName,m_ppChannelObj[nIndex]->m_nHarrisListNum , m_nNumChannelObjects, nIndex);  
							g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_remove", errorstring);    //(Dispatch message)
Sleep(201);
*/
							m_nNumChannelObjects--;

							int nTemp=nIndex;
							while(nTemp<m_nNumChannelObjects)
							{
								m_ppChannelObj[nTemp]=m_ppChannelObj[nTemp+1];
								nTemp++;
							}
							m_ppChannelObj[nTemp] = NULL;

							g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_remove", "about to delete");    //(Dispatch message)
							try{delete pTemp;} catch(...){} 
							g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_remove", "deleted");    //(Dispatch message)

						}
						else nIndex++;
					}
				}
				else
					nIndex++;
							g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_remove", "index = %d", nIndex);    //(Dispatch message)
			}
							g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "Injector:destination_remove", "exit");    //(Dispatch message)

//			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
		else
		{
			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channels", "Retrieve returned a NULL recordset.\n%s\n%s", szSQL, errorstring);
		}
	}
//			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return INJECTOR_ERROR;
}



void InjectorAutomationThread(void* pvArgs)
{
	CInjectorChannelObject* pChannelObj = (CInjectorChannelObject*) pvArgs;
	if(pChannelObj==NULL) return;

	pChannelObj->m_ulFlags &= ~INJECTOR_FLAG_AUTOINIT;
	pChannelObj->m_bAutomationThreadStarted=true;
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	char msgsrc[MAX_MESSAGE_LENGTH];

	EnterCriticalSection(&pChannelObj->m_critServerInfo);
	_snprintf(msgsrc, MAX_MESSAGE_LENGTH-1, "Injector_automation  (%s:%d)", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning automation thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);

	g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "InjectorAutomationThread", errorstring);    //(Dispatch message)

	strcpy(errorstring, "");
	CDBUtil db;
	CInjectorUtilObject util;

	EnterCriticalSection(&pChannelObj->m_critServerInfo);
	CDBconn* pdbConn = db.CreateNewConnection(g_pinjector->m_settings.m_pszDSN, g_pinjector->m_settings.m_pszUser, g_pinjector->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			g_pinjector->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Injector:automation_database_connect", errorstring);  //(Dispatch message)
			pdbConn = g_pinjector->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", g_pinjector->m_settings.m_pszDSN, g_pinjector->m_settings.m_pszUser, g_pinjector->m_settings.m_pszPW); 
		g_pinjector->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Injector:automation_database_init", errorstring);  //(Dispatch message)
		pdbConn = g_pinjector->m_data.m_pdbConn;

		//**MSG
	}
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);

	CBufferUtil bu;


	CInjectorChannelObject tempChannelObj;
	bool bChangesPending = false;
	_timeb timebPending;
	_timeb timebChange;
	_ftime(&timebChange);
	_timeb timebSQL;
	_ftime(&timebSQL);

	timebChange.time -= (g_pinjector->m_settings.m_nAutomationIntervalMS/1000)+1; // set it back so it goes immediately the first time around.
	timebSQL.time = timebChange.time;

	while((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
	{
		// check automation list for changes (mod)
		bool bChanges = false;
		bool bTimeChanges = false;
		bool bForceChanges = false;

//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "%d %d %d %d %d.%03d", g_pinjector->m_data.m_nIndexAutomationEndpoint, g_pinjector->m_settings.m_nNumEndpointsInstalled, g_pinjector->m_settings.m_ppEndpointObject, g_pinjector->m_settings.m_ppEndpointObject[g_pinjector->m_data.m_nIndexAutomationEndpoint], g_pinjector->m_data.m_timebTick.time, g_pinjector->m_data.m_timebTick.millitm);   Sleep(50);//(Dispatch message)

		if(
			  (!g_pinjector->m_data.m_bProcessSuspended)
			&&(g_pinjector->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!g_pinjector->m_data.m_key.m_bExpires)
				||((g_pinjector->m_data.m_key.m_bExpires)&&(!g_pinjector->m_data.m_key.m_bExpired))
				||((g_pinjector->m_data.m_key.m_bExpires)&&(g_pinjector->m_data.m_key.m_bExpireForgiveness)&&(g_pinjector->m_data.m_key.m_ulExpiryDate+g_pinjector->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!g_pinjector->m_data.m_key.m_bMachineSpecific)
				||((g_pinjector->m_data.m_key.m_bMachineSpecific)&&(g_pinjector->m_data.m_key.m_bValidMAC))
				)

			)
		{
//			CInjectorEndpointObject* pAutoObj = g_pinjector->m_settings.m_ppEndpointObject[g_pinjector->m_data.m_nIndexAutomationEndpoint];

	_ftime(&pChannelObj->m_timebAutomationTick); // the last time check inside the thread
			// check the channel info view for 
			tempChannelObj.m_nListChanged = -1;
			tempChannelObj.m_dblListLastUpdate = -1.0;

			if(
//				  (!g_pinjector->m_settings.m_bUseLocalClock) // no need to retrieve from db if using local time.  we getting list change info elsewhere anyway
//				&&  //not so in injector, must get the list status.
				  (g_pinjector->m_settings.m_pszChannelInfo)&&(strlen(g_pinjector->m_settings.m_pszChannelInfo))
				&&(// only do on interval
				    (g_pinjector->m_data.m_timebTick.time > (timebSQL.time+g_pinjector->m_settings.m_nAutomationIntervalMS/1000))
					||(
					    (g_pinjector->m_data.m_timebTick.time==(timebSQL.time+g_pinjector->m_settings.m_nAutomationIntervalMS/1000))
						&&(g_pinjector->m_data.m_timebTick.millitm>(timebSQL.millitm+g_pinjector->m_settings.m_nAutomationIntervalMS%1000))
						)
					)
				)
			{


	EnterCriticalSection(&pChannelObj->m_critServerInfo);
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT server_time, server_status, server_basis, server_changed, server_last_update, \
list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update FROM %s WHERE server = '%s' AND listid = %d",
						(g_pinjector->m_settings.m_pszChannelInfo?g_pinjector->m_settings.m_pszChannelInfo:"ChannelInfo"),
						pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum
						);
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_TIMING)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel timing", "%s SQL [%s]", msgsrc, szSQL);  //(Dispatch message)
				CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
				if(prs != NULL) // no error
				{
					if(prs->IsEOF()) // no record found!
					{
						pChannelObj->m_ulStatus = INJECTOR_STATUS_NOTCON;
						// trigger a refresh.
						g_pinjector->m_data.m_nLastChannelsMod = -1; // causes GetChannels to be called.
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_TIMING)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel timing", "%s no records", msgsrc);  //(Dispatch message)

							// probably sentinel channel was deactivated, correct condition, not NULL! so just reset the timer so we are not hammering it.
							_ftime(&timebSQL);

					}
					else
//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "� not null");  Sleep(50);//(Dispatch message)
					if((!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
					{
//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "� NEOF");  Sleep(50);//(Dispatch message)
						try
						{
							CString szTemp;
							prs->GetFieldValue("server_time", szTemp);
							tempChannelObj.m_dblServertime = atof(szTemp);

							prs->GetFieldValue("server_status", szTemp);
							tempChannelObj.m_nServerStatus = atoi(szTemp);

							prs->GetFieldValue("server_basis", szTemp);
							if(szTemp.GetLength())
							{
								tempChannelObj.m_nServerBasis = (int)bu.xtol(szTemp.GetBuffer(0),2);  //expected width is 2 for normal frame rates
								szTemp.ReleaseBuffer();
							}
							else tempChannelObj.m_nServerBasis = 0x29;//default NTSC

							prs->GetFieldValue("server_changed", szTemp);
							tempChannelObj.m_nServerChanged = atoi(szTemp);

							prs->GetFieldValue("server_last_update", szTemp);
							tempChannelObj.m_dblServerLastUpdate = atof(szTemp);

							prs->GetFieldValue("list_state", szTemp);
							tempChannelObj.m_nListState = atoi(szTemp);

							prs->GetFieldValue("list_changed", szTemp);
							tempChannelObj.m_nListChanged = atoi(szTemp);

							prs->GetFieldValue("list_display", szTemp);
							tempChannelObj.m_nListDisplay = atoi(szTemp);

							prs->GetFieldValue("list_syschange", szTemp);
							tempChannelObj.m_nListSysChange = atoi(szTemp);

							prs->GetFieldValue("list_count", szTemp);
							tempChannelObj.m_nListCount = atoi(szTemp);

							prs->GetFieldValue("list_lookahead", szTemp);
							tempChannelObj.m_nListLookahead = atoi(szTemp);

							prs->GetFieldValue("list_last_update", szTemp);
							tempChannelObj.m_dblListLastUpdate = atof(szTemp);

							pChannelObj->m_ulStatus = INJECTOR_STATUS_CONN;

							_ftime(&timebSQL);

						}
						catch(...)
						{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_TIMING)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel timing", "%s exception", msgsrc);  //(Dispatch message)
						}
					}
					delete prs;
				}
				else
				{
					// error
					pChannelObj->m_ulStatus = INJECTOR_STATUS_ERROR;//							0x00000020  // error (red icon)
					// trigger a refresh.
					g_pinjector->m_data.m_nLastChannelsMod = -1; // causes GetChannels to be called. (may as well...)
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_TIMING)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel timing", "%s null", msgsrc);  //(Dispatch message)
				}

			}
	
			_ftime( &pChannelObj->m_timebAutomationTick );  // we're still alive.

			if(( tempChannelObj.m_nListChanged > 0 )&&(tempChannelObj.m_nListDisplay > 0 ))
			{
				if(
					  (tempChannelObj.m_nListChanged != pChannelObj->m_nListChanged)
					||(tempChannelObj.m_nListDisplay != pChannelObj->m_nListDisplay)
					||(tempChannelObj.m_nServerChanged != pChannelObj->m_nServerChanged)
					||(tempChannelObj.m_nListState != pChannelObj->m_nListState)
					)
				{
					pChannelObj->m_dblServertime  = tempChannelObj.m_dblServertime;
					pChannelObj->m_dblServerLastUpdate  = tempChannelObj.m_dblServerLastUpdate;
					pChannelObj->m_nServerStatus  = tempChannelObj.m_nServerStatus;
					pChannelObj->m_nServerBasis  = tempChannelObj.m_nServerBasis;
					pChannelObj->m_nServerChanged  = tempChannelObj.m_nServerChanged;

					pChannelObj->m_dblListLastUpdate  = tempChannelObj.m_dblListLastUpdate;
					pChannelObj->m_nListState  = tempChannelObj.m_nListState;
					pChannelObj->m_nListChanged  = tempChannelObj.m_nListChanged;
					pChannelObj->m_nListDisplay  = tempChannelObj.m_nListDisplay;
					pChannelObj->m_nListSysChange  = tempChannelObj.m_nListSysChange;
					pChannelObj->m_nListCount  = tempChannelObj.m_nListCount;
					pChannelObj->m_nListLookahead  = tempChannelObj.m_nListLookahead;
					
// following commented out to remain false until pending and force are checked.
// NO,we want to update the time right away.  however, we want to delay the event analysis.
					bChanges = true;
// OK this is not related at all to event analysis anymore
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_TIMING)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel timing", "%s list change", msgsrc);  //(Dispatch message)
					

					if(!bChangesPending)
					{
						_ftime(&timebChange); // initialize this for the interval
					}

					bChangesPending = true;
					_ftime(&timebPending);
					timebPending.time += g_pinjector->m_settings.m_nAnalyzeAutomationDwellMS/1000;
					timebPending.millitm += g_pinjector->m_settings.m_nAnalyzeAutomationDwellMS%1000;
					if(timebPending.millitm>999)
					{
						timebPending.time++;
						timebPending.millitm -= 1000;
					}
				}
				else
				if((tempChannelObj.m_dblServerLastUpdate>0.0)&&(tempChannelObj.m_dblServerLastUpdate != pChannelObj->m_dblServerLastUpdate)) // we have a new update of server time
				{
					pChannelObj->m_dblServertime  = tempChannelObj.m_dblServertime;
					pChannelObj->m_dblServerLastUpdate  = tempChannelObj.m_dblServerLastUpdate;
					bTimeChanges= true;
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_TIMING)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel timing", "%s server time change", msgsrc);  //(Dispatch message)
				}
			}
			else if(tempChannelObj.m_dblServerLastUpdate > 0.0)
			{
				if((tempChannelObj.m_dblServerLastUpdate>0.0)&&(tempChannelObj.m_dblServerLastUpdate != pChannelObj->m_dblServerLastUpdate)) // we have a new update of server time
				{
					pChannelObj->m_dblServertime  = tempChannelObj.m_dblServertime;
					pChannelObj->m_dblServerLastUpdate  = tempChannelObj.m_dblServerLastUpdate;
					bTimeChanges= true;
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_TIMING)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel timing", "%s server time change with no list change", msgsrc);  //(Dispatch message)
				}
			}


			if(bChangesPending)
			{
				double dblTime = ((double)(timebChange.time))*1000.0 + (double)timebChange.millitm;
				if((tempChannelObj.m_dblListLastUpdate>0.0)&&(tempChannelObj.m_dblListLastUpdate < (dblTime-g_pinjector->m_settings.m_nAnalyzeAutomationForceMS)))
				{
					bForceChanges = true;  // because we changed a while ago.  want to go immediately.
				}
			}

if(0)//g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_TIMING)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel timing", "%s get live: %d && %d||%d||%d||%d||%d", 
	msgsrc,
				  (bChangesPending),
					(bForceChanges),
				  (g_pinjector->m_data.m_timebTick.time>timebPending.time),
					(
					  (g_pinjector->m_data.m_timebTick.time==timebPending.time)
					&&(g_pinjector->m_data.m_timebTick.millitm>timebPending.millitm)
					),
					(g_pinjector->m_data.m_timebTick.time > (timebChange.time+g_pinjector->m_settings.m_nAnalyzeAutomationForceMS/1000)),
					(
					  (g_pinjector->m_data.m_timebTick.time==(timebChange.time+g_pinjector->m_settings.m_nAnalyzeAutomationForceMS/1000))
					&&(g_pinjector->m_data.m_timebTick.millitm>(timebChange.millitm+g_pinjector->m_settings.m_nAnalyzeAutomationForceMS%1000))
					)
					
	);  //(Dispatch message)

			// dwell changes - another change will reset the time, suppressing the previous change
			if(
				  (bChangesPending)
				&&(
						(bForceChanges)
				  ||(g_pinjector->m_data.m_timebTick.time>timebPending.time)
					||(
					    (g_pinjector->m_data.m_timebTick.time==timebPending.time)
						&&(g_pinjector->m_data.m_timebTick.millitm>timebPending.millitm)
						)
					||(g_pinjector->m_data.m_timebTick.time > (timebChange.time+g_pinjector->m_settings.m_nAnalyzeAutomationForceMS/1000))
					||(
					    (g_pinjector->m_data.m_timebTick.time==(timebChange.time+g_pinjector->m_settings.m_nAnalyzeAutomationForceMS/1000))
						&&(g_pinjector->m_data.m_timebTick.millitm>(timebChange.millitm+g_pinjector->m_settings.m_nAnalyzeAutomationForceMS%1000))
						)
					)
				)
			{

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", 
		"%s changes pending%d forced%d", msgsrc, bChangesPending, bForceChanges);  //(Dispatch message)
//Sleep(200);
}
				bForceChanges = false;
				bChangesPending = false;
/*
				if(pChannelObj->m_nAnalysisCounter >= INT_MAX) pChannelObj->m_nAnalysisCounter = 0;
				else		pChannelObj->m_nAnalysisCounter++;
*/
//following moved to immediate from delayed.... 
				if(pChannelObj->m_nAnalysisCounter >= INT_MAX) pChannelObj->m_nAnalysisCounter = 0;
				else	pChannelObj->m_nAnalysisCounter++;

//				pChannelObj->m_bNearEventsChanged = true;
				// do some delayed stuff here.

				// get the events here...

				//get automation items.

/*
class CInjectorScheduleObject  
{
public:
	CInjectorScheduleObject();
	virtual ~CInjectorScheduleObject();

	int m_nID;
	int m_nChannelID;
	int m_nFileID;

	char* m_pszEventID;
	char* m_pszEventTitle;
	int m_nSegment;
	double m_dblOnAirTimeLocal;
	int m_nDurationMS;
	unsigned long m_ulStatus;  //event status, if used. in database

	char* m_pszCreator;
	char* m_pszRecKey;

	unsigned long m_ulFlags;  // flags we can set internally in injector

};

*/

			//	get primaries only, use calc times.
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", 
		"%s entering to format SQL", msgsrc);  //(Dispatch message)
//Sleep(200);
}


	EnterCriticalSection(&pChannelObj->m_critServerInfo);
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT itemid, event_clip, event_title, event_segment, \
event_calc_start, event_calc_duration, event_type, \
event_status, event_id, event_position, event_time_mode \
FROM %s.dbo.%s WHERE ((event_type & 128) = 0) AND server_name = '%s' AND server_list = %d \
ORDER BY event_position",
						(g_pinjector->m_settings.m_pszSentinelDBName?g_pinjector->m_settings.m_pszSentinelDBName:"Sentinel"),
						(g_pinjector->m_settings.m_pszSentinelEvents?g_pinjector->m_settings.m_pszSentinelEvents:"Events"),
						pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum
						);
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s SQL [%s]", msgsrc, szSQL);  //(Dispatch message)

//Sleep(200);
}
				int nIndex=0;
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s calling retrieve with pdbConn=%d", msgsrc, pdbConn);  //(Dispatch message)

//Sleep(200);
}
				CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s called retrieve (if err:[%s])", msgsrc, errorstring);  //(Dispatch message)

//Sleep(200);
}


if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s retrieved", msgsrc);  //(Dispatch message)
//Sleep(200);
				if(prs != NULL) // no error
				{
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s retrieved not null, %d %d %d", msgsrc,
	(!prs->IsEOF()), (!g_bKillThread), (!pChannelObj->m_bKillAutomationThread));  //(Dispatch message)
//Sleep(200);

					CInjectorScheduleObject** ppLiveEvents = NULL;
					int nLiveEvents = 0;

//if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
//	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s assigned and initialized", msgsrc);  //(Dispatch message)
//Sleep(200);

//					CRITICAL_SECTION m_critLiveEvents;
					while((!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
					{
//if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
//	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s create obj", msgsrc);  //(Dispatch message)
//Sleep(200);

						CInjectorScheduleObject* pItem = new CInjectorScheduleObject;
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s created pItem %d", msgsrc,pItem);


						if(pItem)
						{
							pItem->m_nChannelID = pChannelObj->m_nChannelID;
							CString szTemp;
							CString szField;

							try
							{
								szField = "uid";
								prs->GetFieldValue("itemid", szTemp);

//if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
//	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s field check %s: [%s]",msgsrc, szField,szTemp);

								pItem->m_nID = atoi(szTemp);

								szField = "Clip ID";
								prs->GetFieldValue("event_clip", szTemp);
//if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
//	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s field check %s: [%s]",msgsrc, szField,szTemp);
								if(szTemp.GetLength()>0)
								{
if(0)//(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s got %s:%s", msgsrc, szField, szTemp);  //(Dispatch message)
//Sleep(200);
}
									pItem->m_pszEventID = (char*)malloc(szTemp.GetLength()+1);
if(0)//(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s %s:%s malloc returned %d", msgsrc, szField, szTemp, pItem->m_pszEventID);  //(Dispatch message)
//Sleep(200);
}
									if(pItem->m_pszEventID)
									{
if(0)//(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s %s:%s malloc OK", msgsrc, szField, szTemp);  //(Dispatch message)
//Sleep(200);
}
										sprintf(pItem->m_pszEventID, "%s", szTemp);
if(0)//(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s %s:%s copied %s", msgsrc, szField, szTemp, pItem->m_pszEventID);  //(Dispatch message)
//Sleep(200);
}
//										strcpy(pItem->m_pszEventID, szTemp.GetBuffer(0));
//										szTemp.ReleaseBuffer();
									}
								}

								szField = "Title";
								prs->GetFieldValue("event_title", szTemp);
//if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
//	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s field check %s: [%s]",msgsrc, szField,szTemp);
								if(szTemp.GetLength()>0)
								{
if(0)//(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s got %s:%s", msgsrc, szField, szTemp);  //(Dispatch message)
//Sleep(200);
}
									pItem->m_pszEventTitle = (char*)malloc(szTemp.GetLength()+1);
if(0)//(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s %s:%s malloc returned %d", msgsrc, szField, szTemp, pItem->m_pszEventTitle);  //(Dispatch message)
//Sleep(200);
}
									if(pItem->m_pszEventTitle)
									{
if(0)//(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s %s:%s malloc OK", msgsrc, szField, szTemp);  //(Dispatch message)
//Sleep(200);
}
										sprintf(pItem->m_pszEventTitle, "%s", szTemp);
if(0)//(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s %s:%s copied %s", msgsrc, szField, szTemp, pItem->m_pszEventTitle);  //(Dispatch message)
//Sleep(200);
}
//										strcpy(pItem->m_pszEventTitle, szTemp.GetBuffer(0));
//										szTemp.ReleaseBuffer();
									}

								}

								szField = "segment";
								prs->GetFieldValue("event_segment", szTemp);
//if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
//	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s field check %s: [%s]",msgsrc, szField,szTemp);
								if(szTemp.GetLength()>0)
								{
if(0)//(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s got %s:%s", msgsrc, szField, szTemp);  //(Dispatch message)
//Sleep(200);
}
									pItem->m_nSegment = atoi(szTemp);
								}
								else pItem->m_nSegment = -1;

								szField = "start";
								prs->GetFieldValue("event_calc_start", szTemp);
//if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
//	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s field check %s: [%s]",msgsrc, szField,szTemp);
								pItem->m_dblOnAirTimeLocal = atof(szTemp);

								szField = "duration";
								prs->GetFieldValue("event_calc_duration", szTemp);
//if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
//	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s field check %s: [%s]",msgsrc, szField,szTemp);
								pItem->m_nDurationMS = atoi(szTemp);

								szField = "type";
								prs->GetFieldValue("event_type", szTemp);
//if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
//	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s field check %s: [%s]",msgsrc, szField,szTemp);
								pItem->m_usType = (unsigned short) atol(szTemp);

								szField = "status";
								prs->GetFieldValue("event_status", szTemp);
//if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
//	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s field check %s: [%s]",msgsrc, szField,szTemp);
								pItem->m_ulStatus = atol(szTemp);

								szField = "rec key";
								prs->GetFieldValue("event_id", szTemp);
//if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
//	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s field check %s: [%s]",msgsrc, szField,szTemp);
								if(szTemp.GetLength()>0)
								{
									pItem->m_pszRecKey = (char*)malloc(szTemp.GetLength()+1);
									if(pItem->m_pszRecKey)
									{
										sprintf(pItem->m_pszRecKey, "%s", szTemp);
//										strcpy(pItem->m_pszRecKey, szTemp.GetBuffer(0));
//										szTemp.ReleaseBuffer();
									}
								}
								szField = "position";
								prs->GetFieldValue("event_position", szTemp);
//if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
//	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s field check %s: [%s]",msgsrc, szField,szTemp);
								pItem->m_nPosition = atoi(szTemp);

								szField = "time mode";
								prs->GetFieldValue("event_time_mode", szTemp);
//if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
//	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s field check %s: [%s]",msgsrc, szField,szTemp);
								pItem->m_ulControl = atol(szTemp);


if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s got event %d: %d[%s][%s] %.3f dur=%d, 0x%08x [%s]", 
		msgsrc, 
								pItem->m_nPosition,
								pItem->m_nID,
								pItem->m_pszEventID?pItem->m_pszEventID:"",
								pItem->m_pszEventTitle?pItem->m_pszEventTitle:"",
								//pItem->m_nSegment
								pItem->m_dblOnAirTimeLocal,
								pItem->m_nDurationMS,
								pItem->m_ulStatus,
								pItem->m_pszRecKey?pItem->m_pszRecKey:""
		);  //(Dispatch message)

//	Sleep(500);
}

								util.AddItem(&ppLiveEvents, &nLiveEvents, pItem);
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s added item, total now %d - last [%s]", 
		msgsrc, nLiveEvents, ppLiveEvents[nLiveEvents-1]->m_pszEventID?ppLiveEvents[nLiveEvents-1]->m_pszEventID:"");
//	Sleep(500);
}
							}
							catch(CException *e)// CDBException *e, CMemoryException *m)  
							{
								if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
								{
	g_pinjector->m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Get channel events", "%s SQL db exception: %s\nlast value retrieved: %s:%s, %d records.\r\n[%s]", 
		msgsrc, ((CDBException *) e)->m_strError, szField, szTemp, nIndex, szSQL);  //(Dispatch message)
								}
								else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
								{
	g_pinjector->m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Get channel events", "%s SQL memory exception: last value retrieved: %s:%s, %d records.\r\n[%s]", 
		msgsrc, szField, szTemp, nIndex, szSQL);  //(Dispatch message)
								}
								else 
								{
	g_pinjector->m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Get channel events", "%s SQL exception: last value retrieved: %s:%s, %d records.\r\n[%s]", 
		msgsrc, szField, szTemp, nIndex, szSQL);  //(Dispatch message)
								}
								e->Delete();
							} 
							catch(...)
							{
	g_pinjector->m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Get channel events", "%s SQL exception: last value retrieved: %s:%s, %d records.\r\n[%s]", 
		msgsrc, szField, szTemp, nIndex, szSQL);  //(Dispatch message)
//	Sleep(200);
							}
						}
						else
						{//error///
	g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "Get channel events", "%s error: could not allocate %d object to store results of: [%s]", 
		msgsrc, nIndex, szSQL);  //(Dispatch message)
						}
						nIndex++;
						prs->MoveNext();
					}
					prs->Close();


					try { delete prs;} catch(...){}
					prs=NULL;				

					// get all swapalicious



					// simple assingnment.
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entering live crit 101", 
	msgsrc);  //(Dispatch message)
					EnterCriticalSection(&pChannelObj->m_critLiveEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entered live crit 101", 
	msgsrc);  //(Dispatch message)

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)
{	
	if(pChannelObj->m_ppLiveEvents)
	{
		int q=0;
		while(q<pChannelObj->m_nNumLiveEvents)
		{
			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event OLD", "%s live events: %d [%s] @ %d (0x%08x) @%d->%d", 
				msgsrc, 
				q,
				pChannelObj->m_ppLiveEvents[q]->m_pszEventID?pChannelObj->m_ppLiveEvents[q]->m_pszEventID:"(null)",
				pChannelObj->m_ppLiveEvents[q]->m_nPosition,
				pChannelObj->m_ppLiveEvents[q]->m_ulStatus,
				pChannelObj->m_ppLiveEvents[q],pChannelObj->m_ppLiveEvents[q]->m_pszEventID
				);  //(Dispatch message)
//	Sleep(100);
		q++;
		}
	}
	else
	{
			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event BEFORE", "%s live events: NULL array", 
				msgsrc);
	}
}


					CInjectorScheduleObject** ppTemp = pChannelObj->m_ppLiveEvents;
					int nTemp = pChannelObj->m_nNumLiveEvents;

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)
{	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event pointers", "%s Live events at %d", 
	msgsrc, pChannelObj->m_ppLiveEvents);  //(Dispatch message)
}
					pChannelObj->m_ppLiveEvents   = ppLiveEvents;
					pChannelObj->m_nNumLiveEvents = nLiveEvents;
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)
{	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event pointers", "%s Live events now at %d", 
	msgsrc, pChannelObj->m_ppLiveEvents);  //(Dispatch message)
}
					
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s leaving live crit 101", 
	msgsrc);  //(Dispatch message)

					LeaveCriticalSection(&pChannelObj->m_critLiveEvents);

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s left live crit 101", 
	msgsrc);  //(Dispatch message)


if(0)//g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)
{	
	if(pChannelObj->m_ppLiveEvents)
	{
		int q=0;
		while(q<pChannelObj->m_nNumLiveEvents)
		{
			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event BEFORE", "%s live events: %d [%s] @ %d (0x%08x) @%d->%d", 
				msgsrc, 
				q,
				pChannelObj->m_ppLiveEvents[q]->m_pszEventID?pChannelObj->m_ppLiveEvents[q]->m_pszEventID:"(null)",
				pChannelObj->m_ppLiveEvents[q]->m_nPosition,
				pChannelObj->m_ppLiveEvents[q]->m_ulStatus,
				pChannelObj->m_ppLiveEvents[q],pChannelObj->m_ppLiveEvents[q]->m_pszEventID
				);  //(Dispatch message)
//	Sleep(100);
		q++;
		}
	}
	else
	{
			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event BEFORE", "%s live events: NULL array", 
				msgsrc);
	}
}



					// then delete.
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s deleting old live array at %d", msgsrc, ppTemp);  //(Dispatch message)

					nLiveEvents = 0;
					if(ppTemp)
					{
						while(nLiveEvents<nTemp)
						{
							if(ppTemp[nLiveEvents])
							{
if(0)//g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)
{	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event pointers", "%s deleting event at %d->%d", 
	msgsrc, ppTemp[nLiveEvents], ppTemp[nLiveEvents]->m_pszEventID);  //(Dispatch message)
}

						ppTemp[nLiveEvents]->m_pszCaller = (char*)malloc(64); if(ppTemp[nLiveEvents]->m_pszCaller) strcpy(ppTemp[nLiveEvents]->m_pszCaller, "get_automation_items");


								// have to NULL out all the things, otherwise they get freed on delete, but we have transferred them over to the other array!
								// no its not true, this should be OK
/*
								ppTemp[nLiveEvents]->m_pszCreator = NULL;
								ppTemp[nLiveEvents]->m_pszEventID = NULL;
								ppTemp[nLiveEvents]->m_pszEventTitle = NULL;
								ppTemp[nLiveEvents]->m_pszRecKey = NULL;
*/
								try { delete ppTemp[nLiveEvents];} catch(...){}
							}
							nLiveEvents++;
						}
						try { delete [] ppTemp;} catch(...){}
					}

	EnterCriticalSection(&pChannelObj->m_critAdvisories);

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s setting live events changed to TRUE (bchg%d bTchg%d)", msgsrc, bChanges, bTimeChanges);  //(Dispatch message)
					pChannelObj->m_bLiveEventsChanged = true; //signal restrictions thread
//					pChannelObj->m_bLiveEventsScheduleChanged = true; // signal programming thread // NO, let's let the new restrictions be calculated first, then let that thread signal the scheduler.
	LeaveCriticalSection(&pChannelObj->m_critAdvisories);
				}
				else
				{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s SQL error: %s\r\n[%s]", msgsrc, errorstring, szSQL);  //(Dispatch message)
				}

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)
{	
	if(pChannelObj->m_ppLiveEvents)
	{
		int q=0;
		while(q<pChannelObj->m_nNumLiveEvents)
		{
//			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event AFTER", "%s live events: %d [%s] @ %d (0x%08x) @%d->%d", 
			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event AFTER", "%s live events: %d [%s] @ %d (0x%08x) @%.3f dur %d", 
				msgsrc, 
				q,
				pChannelObj->m_ppLiveEvents[q]->m_pszEventID?pChannelObj->m_ppLiveEvents[q]->m_pszEventID:"(null)",
				pChannelObj->m_ppLiveEvents[q]->m_nPosition,
				pChannelObj->m_ppLiveEvents[q]->m_ulStatus,
//				pChannelObj->m_ppLiveEvents[q],pChannelObj->m_ppLiveEvents[q]->m_pszEventID
				pChannelObj->m_ppLiveEvents[q]->m_dblOnAirTimeLocal, pChannelObj->m_ppLiveEvents[q]->m_nDurationMS
				);  //(Dispatch message)
//	Sleep(100);
		q++;
		}
	}
	else
	{
			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event AFTER", "%s live events: NULL array", 
				msgsrc);
	}
}



			}
/*
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s CHECK ME ********** %d", msgsrc, clock());  //(Dispatch message) 
Sleep(200);
*/

if(0)//g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s check TRUE (bchg%d bTchg%d)", msgsrc, bChanges, bTimeChanges);  //(Dispatch message)

			if((bChanges)||(bTimeChanges))
			{



//				pChannelObj->m_bTriggerEventsChanged = true;
//				pChannelObj->m_bFarEventsChanged = true;
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_TIMING)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel timing", "%s CHANGE (ev%d,tm%d) server time assignment %d", 
	msgsrc,
	bChanges, bTimeChanges, clock());  //(Dispatch message)
//Sleep(200);


//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "Automation changes exist %d", pAutoObj->m_nModLiveEvents);  // Sleep(50);//(Dispatch message)
				// g_pinjector->m_data.m_nEventLastMax = 0; // start at the beginning....
// actually dont reset this.
				bChanges = false;
				bTimeChanges = false;  // have to reset the fact that we go the time change.
				bool bSQLError = false;
				// if changes
				// {}
				//   Close the m_prsEvents recordset and delete it, set it back to NULL.

//				g_pinjector->m_data.ReleaseRecordSet();

				_ftime( &pChannelObj->m_timebAutomationTick); 

					//update the servertime.
//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "Automation svr atof? %f", timeval);  Sleep(50);//(Dispatch message)
						
				if((g_pinjector->m_settings.m_bUseLocalClock)||(pChannelObj->m_dblServerLastUpdate<=0.0))
				{
					if(g_pinjector->m_settings.m_bUseUTC)
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
								((double)(pChannelObj->m_timebAutomationTick.time))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + g_pinjector->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}
					else
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
								((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
							+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + g_pinjector->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}
				}
				else
				{
					if(pChannelObj->m_dblServerLastUpdate>0.0)
					{
						/*
						pChannelObj->m_dblAutomationTimeDiffMS = 
							(((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
							+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm) 
							- pChannelObj->m_dblServerLastUpdate;
						*/
						pChannelObj->m_dblAutomationTimeDiffMS = 
							((double)pChannelObj->m_timebAutomationTick.time)*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm
							- pChannelObj->m_dblServerLastUpdate;
					// DEMO only, need to handle midnight.
//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "Automation svr time in unix? %f", timeval);  Sleep(50);//(Dispatch message)
//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "Automation svr time diffMS? %d", nDiff);  Sleep(50);//(Dispatch message)
						pChannelObj->m_dblAutomationTimeEstimate = (pChannelObj->m_dblServertime + pChannelObj->m_dblAutomationTimeDiffMS 
							+ (double)g_pinjector->m_settings.m_nTriggerAdvanceMS)/1000.0;
//										g_pinjector->m_data.m_dblAutomationTimeEstimate = (double)timebTempTime.time;

						g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "%s Automation time estimate %.3f", msgsrc, pChannelObj->m_dblAutomationTimeEstimate);//  Sleep(50);//(Dispatch message)
					}
				}
				// else automation type unknown, so nothing
			} // automation changes exist

/////////////////////////////////////////////////////////
			else  // no automation changes
			{

//if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_TIMING)	
//	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel timing", "%s no-change server time assignment", msgsrc);  //(Dispatch message)
				_ftime( &pChannelObj->m_timebAutomationTick); 
					// just update the time based on estimate
				if((g_pinjector->m_settings.m_bUseLocalClock)||(pChannelObj->m_dblServerLastUpdate<=0.0))
				{
					if(g_pinjector->m_settings.m_bUseUTC)
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
								((double)(pChannelObj->m_timebAutomationTick.time))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + g_pinjector->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}
					else
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
								((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
							+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + g_pinjector->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}
				}
				else
				{
					if(pChannelObj->m_dblServerLastUpdate>0.0)
					{
						/*
						pChannelObj->m_dblAutomationTimeDiffMS = (((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
							+(pChannelObj->m_timebAutomationTick.dstflag?3600:0)))*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm) 
							- pChannelObj->m_dblServerLastUpdate;
							*/

						pChannelObj->m_dblAutomationTimeDiffMS = 
							((double)pChannelObj->m_timebAutomationTick.time)*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm
							- pChannelObj->m_dblServerLastUpdate;

						pChannelObj->m_dblAutomationTimeEstimate = (pChannelObj->m_dblServertime + pChannelObj->m_dblAutomationTimeDiffMS 
							+ (double)g_pinjector->m_settings.m_nTriggerAdvanceMS)/1000.0;
					}
				}

/*
				g_pinjector->m_data.m_dblAutomationTimeEstimate = g_pinjector->m_data.m_dblHarrisTime + ((double)(g_pinjector->m_data.m_nHarrisTimeDiffMS)/1000.0) + (double)g_pinjector->m_settings.m_nTriggerAdvanceMS/1000.0
					+ ((double)timebTempTime.time - (double)g_pinjector->m_data.m_timebHarrisCheckTimeLocal.time)
					+ ((double)timebTempTime.millitm/1000.0 - (double)g_pinjector->m_data.m_timebHarrisCheckTimeLocal.millitm/1000.0); // the difference beween now and last check
//										g_pinjector->m_data.m_dblAutomationTimeEstimate = (double)timebTempTime.time;
//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "Automation estimate time now %f", g_pinjector->m_data.m_dblAutomationTimeEstimate);  Sleep(50);//(Dispatch message)
*/

			}
if(0)//g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s out %d", msgsrc, clock());  //(Dispatch message)

			pChannelObj->m_ulFlags |= INJECTOR_FLAG_AUTOINIT;
		} // if license etc

if(0)//g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_LIVEEVENTS)
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get channel events", "%s looping %d)", msgsrc, clock());  //(Dispatch message)

		Sleep(1); // dont peg processor
	} // while((!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))

	while((pChannelObj->m_bProgrammingThreadStarted)||(pChannelObj->m_bSchedulerThreadStarted)) Sleep(10);  // TODO put a timeout on this.


	db.RemoveConnection(pdbConn);
	Sleep(30);
	EnterCriticalSection(&pChannelObj->m_critServerInfo);
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending automation thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);
	g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "InjectorAutomationThread", errorstring);    //(Dispatch message)
	pChannelObj->m_bAutomationThreadStarted=false;
	pChannelObj->m_ulFlags &= ~INJECTOR_FLAG_AUTOINIT;

  Sleep(50);//(Dispatch message)
	_endthread();
	Sleep(150);

}

void InjectorScheduleRestrictionThread(void* pvArgs)
{
/*
	scheduled_on is the on air time.
	create table Schedule (itemid int identity(1,1), channelid int, clipid varchar(64), segment int, scheduled_on decimal(20,3), duration int, clip_title varchar(256), notes varchar(4096), status int, created_on int, created_by varchar(32), traffic_file_id int);
*/
	
	CInjectorChannelObject* pChannelObj = (CInjectorChannelObject*) pvArgs;
	if(pChannelObj==NULL) return;

	pChannelObj->m_bSchedulerThreadStarted=true;
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];

	char msgsrc[MAX_MESSAGE_LENGTH];

	EnterCriticalSection(&pChannelObj->m_critServerInfo);
	_snprintf(msgsrc, MAX_MESSAGE_LENGTH-1, "Injector_restriction (%s:%d)", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning schedule restriction thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);


	g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "InjectorScheduleRestrictionThread", errorstring);    //(Dispatch message)

	strcpy(errorstring, "");
	CDBUtil db;
	CInjectorUtilObject util;

	EnterCriticalSection(&pChannelObj->m_critServerInfo);

	CDBconn* pdbConn = db.CreateNewConnection(g_pinjector->m_settings.m_pszDSN, g_pinjector->m_settings.m_pszUser, g_pinjector->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			g_pinjector->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Injector:scheduler_database_connect", errorstring);  //(Dispatch message)
			pdbConn = g_pinjector->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", g_pinjector->m_settings.m_pszDSN, g_pinjector->m_settings.m_pszUser, g_pinjector->m_settings.m_pszPW); 
		g_pinjector->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Injector:scheduler_database_init", errorstring);  //(Dispatch message)
		pdbConn = g_pinjector->m_data.m_pdbConn;

		//**MSG
	}
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);

  /////////////////////////////////////////////////////////////////////////////////////////////////////
	// The purpose of this thread is to analyze the sentinel traffic database and schedule restrictions.
	// The way this is going to work is that each file is analyzed for an "allow" ID.
	// Starting from midnight onward to the beginning of the first instance of allow ID, a placeholder
	// schedule item will be inserted, thereby blocking all other scheduling for that channel.
	// The thread initializes by detemining what is in the schedule database, purging items from previous days, 
	// then it enters the main loop where it polls the traffic database for new info (on startup it will all be new)
	// then it goes through each file and finds if the already databased items match the ones in there, for each day
	// If not, then it adds/deletes what it needs to. and continues to poll sentinel for traffic changes.
	// this is not that efficient because Sentinel currently only has a mod counter on the entire traffic table
	// instead of per channel.  I may change this someday, if I am still alive and/or it still matters.


	// start me up!
	// first just purge old, all that is unnecessary.
	// have to go by channel time, not computer time.  so have to wait for a time est from the automation thread.

	while((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)
		&&(!(pChannelObj->m_ulFlags&INJECTOR_FLAG_AUTOINIT))
		&&((pChannelObj->m_dblServerLastUpdate<0.0)||(pChannelObj->m_dblAutomationTimeEstimate<0.0)))
	{
		Sleep(100);
	}
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s: automation release", msgsrc);
	g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "InjectorScheduleRestrictionThread", errorstring);    //(Dispatch message)
	


	if((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
	{
/*
		create table Schedule (itemid int identity(1,1), 
		channelid int, clipid varchar(64), segment int, 
		scheduled_on decimal(20,3), duration int, clip_title varchar(256), 
		notes varchar(4096), status int, created_on int, 
		created_by varchar(32), traffic_file_id int);
*/	
		//(duration+1000) is to be safe on any rounding error that might occur.
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE channelid = %d AND (scheduled_on+((duration+1000)/1000)) < %.3f AND created_by = 'sys'", // sys only, and can ignore old stuff.  must time sort
			((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
			pChannelObj->m_nChannelID,
			pChannelObj->m_dblAutomationTimeEstimate // local time....
			);

		if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//error
		}

			// let's clear our memory.
		EnterCriticalSection(&pChannelObj->m_critScheduleRestrictions);
		
		if((pChannelObj->m_nNumScheduleRestrictions>0)&&(pChannelObj->m_ppScheduleRestrictions))
		{
			int i=0;
			while(i<pChannelObj->m_nNumScheduleRestrictions)
			{
				if(pChannelObj->m_ppScheduleRestrictions[i])
				{
					try {delete pChannelObj->m_ppScheduleRestrictions[i];} catch(...){}
				}
				i++;
			}
			try {delete [] pChannelObj->m_ppScheduleRestrictions;} catch(...){}
		}
		pChannelObj->m_nNumScheduleRestrictions = 0;
		pChannelObj->m_ppScheduleRestrictions = NULL;

		LeaveCriticalSection(&pChannelObj->m_critScheduleRestrictions);

		EnterCriticalSection(&pChannelObj->m_critDisplayEvents);
		
		if((pChannelObj->m_nNumDisplayEvents>0)&&(pChannelObj->m_ppDisplayEvents))
		{
			int i=0;
			while(i<pChannelObj->m_nNumDisplayEvents)
			{
				if(pChannelObj->m_ppDisplayEvents[i])
				{
					try {delete pChannelObj->m_ppDisplayEvents[i];} catch(...){}
				}
				i++;
			}
			try {delete [] pChannelObj->m_ppDisplayEvents;} catch(...){}
		}
		pChannelObj->m_nNumDisplayEvents = 0;
		pChannelObj->m_ppDisplayEvents = NULL;

		LeaveCriticalSection(&pChannelObj->m_critDisplayEvents);


		// now ingest what is left: (need to do this in case there were changes to traffic files while we were offline)


		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT itemid, clipid, segment, scheduled_on, \
duration, clip_title, status FROM %s WHERE channelid = %d AND created_by = 'sys' ORDER BY scheduled_on",  // sys only!
			((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
			pChannelObj->m_nChannelID
			);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get existing schedule restriction items", "%s SQL [%s]", msgsrc, szSQL);  //(Dispatch message)

		int nIndex=0;
		CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
		if(prs != NULL) // no error
		{
			while((!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
			{
//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "� NEOF");  Sleep(50);//(Dispatch message)

				CInjectorScheduleObject* pObj =  new CInjectorScheduleObject;
				if(pObj)
				{
					pObj->m_nChannelID = pChannelObj->m_nChannelID;
					// don't bother with creator

					bool bError = false;
					try
					{
						CString szTemp;
						prs->GetFieldValue("itemid", szTemp);
						pObj->m_nID = atoi(szTemp);

						prs->GetFieldValue("clipid", szTemp);
						if(szTemp.GetLength()>0)
						{
							pObj->m_pszEventID = (char*)malloc(szTemp.GetLength()+1);
							if(pObj->m_pszEventID)
							{
								sprintf(pObj->m_pszEventID, "%s", szTemp);
								//strcpy(pObj->m_pszEventID, szTemp.GetBuffer(0));
							}
						}
						prs->GetFieldValue("segment", szTemp);
						pObj->m_nSegment = atoi(szTemp);

						prs->GetFieldValue("scheduled_on", szTemp);
						pObj->m_dblOnAirTimeLocal = atof(szTemp);

						prs->GetFieldValue("duration", szTemp);
						pObj->m_nDurationMS = atoi(szTemp);

						prs->GetFieldValue("clip_title", szTemp);
						if(szTemp.GetLength()>0)
						{
							pObj->m_pszEventTitle = (char*)malloc(szTemp.GetLength()+1);
							if(pObj->m_pszEventTitle)
							{
								sprintf(pObj->m_pszEventTitle, "%s", szTemp);
//								strcpy(pObj->m_pszEventTitle, szTemp.GetBuffer(0));
							}
						}

						prs->GetFieldValue("status", szTemp);
						pObj->m_ulStatus = atoi(szTemp);
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get init schedule restriction items", "%s got event: %d[%s][%s] %.3f dur=%d, 0x%08x [%s]", msgsrc, 
								pObj->m_nID,
								pObj->m_pszEventID,
								pObj->m_pszEventTitle,
								//pObj->m_nSegment
								pObj->m_dblOnAirTimeLocal,
								pObj->m_nDurationMS,
								pObj->m_ulStatus,
								pObj->m_pszRecKey?pObj->m_pszRecKey:""
		);  //(Dispatch message)
}

					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						bError = true;
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
							g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get existing schedule restriction items", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get existing schedule restriction items", "Retrieve: Caught exception: out of memory.\n%s", szSQL);
	//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
						}
						else 
						{
							g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get existing schedule restriction items", "Retrieve: Caught other exception.\n%s", szSQL);
	//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
						}
						e->Delete();

					} 
					catch( ... )
					{
						bError = true;
						g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get existing schedule restriction items", "Retrieve: Caught exception.\n%s", szSQL);
					}

					if(bError)
					{
						try{ delete pObj; } catch(...){}
					}
					else
					{
						if(g_pinjector->m_settings.m_bExtraRestrictionInfo) // use diplay items.
						{
		EnterCriticalSection(&pChannelObj->m_critDisplayEvents);
							pChannelObj->InsertScheduleItem(pObj, INJECTOR_ARRAY_SCHEDULE_DISPLAY);
		LeaveCriticalSection(&pChannelObj->m_critDisplayEvents);
						}
						else
						{
		EnterCriticalSection(&pChannelObj->m_critScheduleRestrictions);
							pChannelObj->InsertScheduleItem(pObj, INJECTOR_ARRAY_SCHEDULE_RESTRICTION);
		LeaveCriticalSection(&pChannelObj->m_critScheduleRestrictions);
						}
					}
				}
				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			try{ delete prs; } catch(...){}
		}
		else
		{
		//	prs was null
			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get existing schedule restriction items", "Retrieve returned a NULL recordset.\n%s", szSQL);
		}
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Got existing schedule restriction items", "%d schedule restriction items retrieved on %s", nIndex, msgsrc);  //(Dispatch message)

	}

	int nTrafficMod = -1;
	int nLastTrafficMod=-1;
	int nLastRestrictionMod=-1;
	double dblLastLast = -1.0;


	while((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
	{
		_ftime( &pChannelObj->m_timebRestrictionTick);  // its here right now...  maybe should be inside if if schedulermode
		pChannelObj->m_bTrafficEventsChanged = false;

		if(g_pinjector->m_settings.m_bAutoSchedulerMode)
		{
		// in this loop, check out the
			EnterCriticalSection(&g_pinjector->m_data.m_critTraffic);
			nLastTrafficMod = g_pinjector->m_data.m_nLastTrafficMod;
			LeaveCriticalSection(&g_pinjector->m_data.m_critTraffic);

			if(nLastTrafficMod!=nTrafficMod)  //on init this will be true
			{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_TRAFFICEVENTS)
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get traffic items", "%s mods: %d %d", msgsrc, nLastTrafficMod, nTrafficMod);  //(Dispatch message)

		// procedure:
		// get LIVE items for the channel from the sentinel events table.  actually take it from memory - changes will publish it to there.
		// even if the list is not playing, what is in it trumps the traffic file for the same period.
		// get the live items and go thru the list to create exclusion items.
		// when we get to the end of the list get items for the channel traffic, ordered by time.
		// go thru them until we hit the time at the end of the live list, within an event, or after.
		// for each item we create check to see if it is there, if it is mark the original
		// if not, then create new, and add it to canon (marked) and database.
		// at the end, delete from canon and db, any unmarked items get deleted (from db as well).  
		// this will also clean up the past!
		// one difficulty is to understand what is at the beginning.
		// another is to not create a restriction item in the live list for things that have been inserted automatically.

//				bool bInitializedTime = FALSE;

				pChannelObj->m_bTrafficEventsChanged = true;
				
						// have to analyze and deal - use calc start
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT itemid, traffic_file_id, event_clip, event_title, event_segment, \
event_calc_start, event_calc_duration, event_status, event_id FROM %s.dbo.%s WHERE list_id = %d AND (event_type & 128)= 0 AND (event_start+((event_duration+1000)/1000)) > %.3f \
ORDER BY event_calc_start", 
					((g_pinjector->m_settings.m_pszSentinelDBName)&&(strlen(g_pinjector->m_settings.m_pszSentinelDBName)))?g_pinjector->m_settings.m_pszSentinelDBName:"Sentinel",
					((g_pinjector->m_settings.m_pszSentinelTraffic)&&(strlen(g_pinjector->m_settings.m_pszSentinelTraffic)))?g_pinjector->m_settings.m_pszSentinelTraffic:"Traffic_Events",
					pChannelObj->m_nChannelID, 
					pChannelObj->m_dblAutomationTimeEstimate // local time.... only bother with things in the future.
					);
//we only want primary events because we are using those to determine time.
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_TRAFFICEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get traffic items", "%s SQL [%s]", msgsrc, szSQL);  //(Dispatch message)

/*
CREATE TABLE Traffic_Events(
itemid int identity(1,1), 
traffic_file_id int, 
event_key varchar(256), 
server_name varchar(32), 
server_list int, 
list_id int, 
event_id varchar(32), 
event_clip varchar(64), 
event_segment int, 
event_title varchar(64), 
event_data varchar(4096),
event_type int, 
event_status int, 
event_time_mode int, 
event_start decimal(20,3), 
event_duration int, 
event_calc_start decimal(20,3), 
event_calc_duration int, 
event_calc_end decimal(20,3), 
event_position int, 
event_last_update decimal(20,3), 
parent_uid int, 
parent_key varchar(256), 
parent_id varchar(32), 
parent_clip varchar(64), 
parent_segment int, 
parent_title varchar(64), 
parent_type int, 
parent_status int, 
parent_time_mode int, 
parent_start decimal(20,3), 
parent_duration int, 
parent_calc_start decimal(20,3), 
parent_calc_duration int, 
parent_calc_end decimal(20,3), 
parent_position int, 
app_data varchar(512), 
app_data_aux int);   
*/

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_TRAFFICEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get traffic items", "%s SQL [%s]", msgsrc, szSQL);  //(Dispatch message)


				int nIndex=0;
				CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
				if(prs != NULL) // no error
				{
					CInjectorScheduleObject** ppTrafficEvents = NULL;
					int nTrafficEvents = 0;

//					CRITICAL_SECTION m_critTrafficEvents;
					while((!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
					{
						CInjectorScheduleObject* pItem = new CInjectorScheduleObject;
						if(pItem)
						{
							pItem->m_nChannelID = pChannelObj->m_nChannelID;
							CString szTemp;

							try
							{
								prs->GetFieldValue("itemid", szTemp);
								pItem->m_nID = atoi(szTemp);

								prs->GetFieldValue("traffic_file_id", szTemp);
								pItem->m_nFileID = atoi(szTemp);

								prs->GetFieldValue("event_clip", szTemp);
								if(szTemp.GetLength()>0)
								{
									pItem->m_pszEventID = (char*)malloc(szTemp.GetLength()+1);
									if(pItem->m_pszEventID)
									{
										sprintf(pItem->m_pszEventID, "%s", szTemp);
									//	strcpy(pItem->m_pszEventID, szTemp);
									}
								}
								prs->GetFieldValue("event_title", szTemp);
								if(szTemp.GetLength()>0)
								{
									pItem->m_pszEventTitle = (char*)malloc(szTemp.GetLength()+1);
									if(pItem->m_pszEventTitle)
									{
										sprintf(pItem->m_pszEventTitle, "%s", szTemp);
										//strcpy(pItem->m_pszEventTitle, szTemp);
									}
								}

								prs->GetFieldValue("event_segment", szTemp);
								pItem->m_nSegment = atoi(szTemp);

								prs->GetFieldValue("event_calc_start", szTemp);
								pItem->m_dblOnAirTimeLocal = atof(szTemp);

								prs->GetFieldValue("event_calc_duration", szTemp);
								pItem->m_nDurationMS = atoi(szTemp);

								prs->GetFieldValue("event_status", szTemp);
								pItem->m_ulStatus = atol(szTemp);

								prs->GetFieldValue("event_id", szTemp);
								if(szTemp.GetLength()>0)
								{
									pItem->m_pszRecKey = (char*)malloc(szTemp.GetLength()+1);
									if(pItem->m_pszRecKey)
									{
										sprintf(pItem->m_pszRecKey, "%s", szTemp);
										//strcpy(pItem->m_pszRecKey, szTemp);
									}
								}

								util.AddItem(&ppTrafficEvents, &nTrafficEvents, pItem);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_TRAFFICEVENTS)||(g_pinjector->m_settings.m_bDebugSQL))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get traffic items", "%s got event: %d[%s][%s] %.3f dur=%d, 0x%08x [%s]", msgsrc, 
								pItem->m_nID,
								pItem->m_pszEventID?pItem->m_pszEventID:"",
								pItem->m_pszEventTitle?pItem->m_pszEventTitle:"",
								//pItem->m_nSegment
								pItem->m_dblOnAirTimeLocal,
								pItem->m_nDurationMS,
								pItem->m_ulStatus,
								pItem->m_pszRecKey?pItem->m_pszRecKey:""
		);  //(Dispatch message)
}
							}
							catch(...)
							{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get traffic items", "%s SQL exception: last value retrieved: %s, %d records.\r\n[%s]", msgsrc, szTemp, nIndex, szSQL);  //(Dispatch message)
							}
						}
						else
						{//error///
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get traffic items", "%s error: could not allocate %d object to store results of: [%s]", msgsrc, nIndex, szSQL);  //(Dispatch message)
						}
						nIndex++;
						prs->MoveNext();
					}
					prs->Close();


					try { delete prs;} catch(...){}
					prs=NULL;				

					// get all swapalicious
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_TRAFFICEVENTS))
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get traffic items", "%s got %d traffic items", msgsrc, nIndex);  //(Dispatch message)

					// simple assingnment.
					EnterCriticalSection(&pChannelObj->m_critTrafficEvents);
					CInjectorScheduleObject** ppTemp = pChannelObj->m_ppTrafficEvents;
					int nTemp = pChannelObj->m_nNumTrafficEvents;

					pChannelObj->m_ppTrafficEvents   = ppTrafficEvents;
					pChannelObj->m_nNumTrafficEvents = nTrafficEvents;
					
					LeaveCriticalSection(&pChannelObj->m_critTrafficEvents);

					// then delete.

					nTrafficEvents = 0;
					if(ppTemp)
					{
						while(nTrafficEvents<nTemp)
						{
							if(ppTemp[nTrafficEvents])
							{
								ppTemp[nTrafficEvents]->m_pszCaller = (char*)malloc(64); if(ppTemp[nTrafficEvents]->m_pszCaller) strcpy(ppTemp[nTrafficEvents]->m_pszCaller, "get_traffic_items");

/*
								ppTemp[nTrafficEvents]->m_pszEventID=NULL;
								ppTemp[nTrafficEvents]->m_pszEventTitle=NULL;
								ppTemp[nTrafficEvents]->m_pszCreator=NULL;
								ppTemp[nTrafficEvents]->m_pszRecKey=NULL;
*/
								try { delete ppTemp[nTrafficEvents];} catch(...){}
							}
							nTrafficEvents++;
						}
						try { delete [] ppTemp;} catch(...){}
					}
				}
				else
				{
				//	prs was null
					g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get traffic items", "Retrieve returned a NULL recordset.\n%s", szSQL);
				}

				nTrafficMod = nLastTrafficMod;
			}

	EnterCriticalSection(&pChannelObj->m_critAdvisories);

			if(
				  (pChannelObj->m_bLiveEventsChanged)
				||(pChannelObj->m_bTrafficEventsChanged)
				||(nLastRestrictionMod != g_pinjector->m_data.m_nLastRestrictionsMod)
				)
			{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorScheduleRestrictionThread", "%s restriction analysis: %d %d %d!=?%d", 
		msgsrc, 
		pChannelObj->m_bLiveEventsChanged, pChannelObj->m_bTrafficEventsChanged, nLastRestrictionMod, g_pinjector->m_data.m_nLastRestrictionsMod);  //(Dispatch message)
//Sleep(100);

				bool bLiveEventsChanged = pChannelObj->m_bLiveEventsChanged;
				if(pChannelObj->m_bLiveEventsChanged)	pChannelObj->m_bLiveEventsChanged = false;
				if(pChannelObj->m_bTrafficEventsChanged) pChannelObj->m_bTrafficEventsChanged = false;  // reset this.
				nLastRestrictionMod = g_pinjector->m_data.m_nLastRestrictionsMod;
				// recalc the restrictions!


				//reset these right at the beginning, so that if they change again while we are doing the process, we will hit it.
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorScheduleRestrictionThread", "%s restriction analysis reset: %d %d %d=%d", 
		msgsrc, 
		pChannelObj->m_bLiveEventsChanged, pChannelObj->m_bTrafficEventsChanged, nLastRestrictionMod, g_pinjector->m_data.m_nLastRestrictionsMod);  //(Dispatch message)
//Sleep(100);
	LeaveCriticalSection(&pChannelObj->m_critAdvisories);

/*
				// what we are doing is removing any restriction that is finished prior to now.
				// then, we find the playing event and we check restrictions to make sure it is restricted.
				// then we go down the list of live items looking for the allow ID.

				// removing any restriction that is finished prior to now.

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE channelid = %d AND (scheduled_on+((duration+1000)/1000)) < %.3f AND created_by = 'sys'", // sys only, and can ignore old stuff.
					((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
					pChannelObj->m_nChannelID,
					pChannelObj->m_dblAutomationTimeEstimate // local time....
					);

				if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
				{
					//error
				}

					// let's synchronize our memory.
				EnterCriticalSection(&pChannelObj->m_critScheduleRestrictions);
				
				if((pChannelObj->m_nNumScheduleRestrictions>0)&&(pChannelObj->m_ppScheduleRestrictions))
				{
					int i=0;
					while(i<pChannelObj->m_nNumScheduleRestrictions)
					{
						if(
							  (pChannelObj->m_ppScheduleRestrictions[i])
							)
						{ 

							if((pChannelObj->m_ppScheduleRestrictions[i]->m_dblOnAirTimeLocal + ((double)(pChannelObj->m_ppScheduleRestrictions[i]->m_nDurationMS))/1000.0)< pChannelObj->m_dblAutomationTimeEstimate)
							{
								int nRV = INJECTOR_ERROR;
								try {nRV = DeleteScheduleItem(i, INJECTOR_ARRAY_SCHEDULE_RESTRICTION);} catch(...){}

								if(nRV>INJECTOR_ERROR)
								{
									i--;
								}
							}
							else break; // they are time sorted, so if we encounter one that is not done yet, the rest are all not done yet.
						}
						i++;
					}
				}
				LeaveCriticalSection(&pChannelObj->m_critScheduleRestrictions);
*/

				// let's do this a different way.
				// lets calc a whole new memory array based on the new info.
				// Then we can do a mem compare on the existing real array.
				// if stuff is identical, we update the item ID on the new mem array.
        // if not identical, no problem we just go down the list and update the restrictions one by one in the database, 
				// then dump the new mem array over to the "real" one.


				double dblOnAirTimeLocal = pChannelObj->m_dblAutomationTimeEstimate;
				int nDurationMS = 0;
				bool bInRestrict = true;  // by default.

				// new array
				CInjectorScheduleObject** ppScheduleRestrictions=NULL;
				CInjectorScheduleObject*  pScheduleRestriction=NULL;
				int nNumScheduleRestrictions = 0;

				// new display array
				CInjectorScheduleObject** ppScheduleDisplay=NULL;
				CInjectorScheduleObject*  pScheduleDisplay=NULL;
				int nNumScheduleDisplays = 0;

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s finding first live event", 
	msgsrc);  //(Dispatch message)
//Sleep(100);
}
				// first find the now event.
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entering live crit 01", 
	msgsrc);  //(Dispatch message)
				EnterCriticalSection(&pChannelObj->m_critLiveEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entered live crit 01", 
	msgsrc);  //(Dispatch message)
		
				

/*
// this doesnt work becuase a hard start can be trumped by a later playing event.  
				int i=0;
				if((pChannelObj->m_nNumLiveEvents>0)&&(pChannelObj->m_ppLiveEvents))
				{
					while(i<pChannelObj->m_nNumLiveEvents)
					{
						if (pChannelObj->m_ppLiveEvents[i])
						{ 

							if((pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal + ((double)(pChannelObj->m_ppLiveEvents[i]->m_nDurationMS))/1000.0) > pChannelObj->m_dblAutomationTimeEstimate)
							{
								break; // they are time sorted, so if we encounter one that is not done yet, the rest are all not done yet.
							}
						}
						i++;
					}
				}
				*/

// so instead let's just find the next non-done one by status, then check the time.
				
				int i=0;
				if((pChannelObj->m_nNumLiveEvents>0)&&(pChannelObj->m_ppLiveEvents))
				{
					while(i<pChannelObj->m_nNumLiveEvents)
					{
						if (pChannelObj->m_ppLiveEvents[i])
						{ 
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event", "%s finding first live event: chk %d [%s] @ %d (0x%08x) with %.3f and %d", 
	msgsrc, i,
	pChannelObj->m_ppLiveEvents[i]->m_pszEventID?pChannelObj->m_ppLiveEvents[i]->m_pszEventID:"(null)",
	pChannelObj->m_ppLiveEvents[i]->m_nPosition,
	pChannelObj->m_ppLiveEvents[i]->m_ulStatus,
	pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal,
	pChannelObj->m_ppLiveEvents[i]->m_nDurationMS);  //(Dispatch message)
//Sleep(100);
}
							if(!(pChannelObj->m_ppLiveEvents[i]->m_ulStatus&((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped))))
							{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event", "%s finding first live event: chk %d [%s] @ %d (0x%08x) NOT PLAYED, end %.3f>?%.3f", 
	msgsrc, i,
	pChannelObj->m_ppLiveEvents[i]->m_pszEventID?pChannelObj->m_ppLiveEvents[i]->m_pszEventID:"(null)",
	pChannelObj->m_ppLiveEvents[i]->m_nPosition,
	pChannelObj->m_ppLiveEvents[i]->m_ulStatus,
	(pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal + ((double)(pChannelObj->m_ppLiveEvents[i]->m_nDurationMS))/1000.0) ,
	pChannelObj->m_dblAutomationTimeEstimate
	);  //(Dispatch message)
//Sleep(100);
}

								if((pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal + ((double)(pChannelObj->m_ppLiveEvents[i]->m_nDurationMS))/1000.0) 
									 > pChannelObj->m_dblAutomationTimeEstimate)
								{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event", "%s FOUND first live event: %d [%s] @ %d (0x%08x)", 
	msgsrc, i,
	pChannelObj->m_ppLiveEvents[i]->m_pszEventID?pChannelObj->m_ppLiveEvents[i]->m_pszEventID:"(null)",
	pChannelObj->m_ppLiveEvents[i]->m_nPosition,
	pChannelObj->m_ppLiveEvents[i]->m_ulStatus
	);  //(Dispatch message)
//Sleep(100);
}
									break; // they are time sorted, so if we encounter one that is not done yet, the rest are all not done yet.
								}
							}
						}
						i++;
					}
				}


if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)
{	
	if(pChannelObj->m_ppLiveEvents)
	{
		int q=0;
		while(q<pChannelObj->m_nNumLiveEvents)
		{
			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event", "%s live events: %d [%s] @ %d (0x%08x)", 
				msgsrc, 
				q,
				pChannelObj->m_ppLiveEvents[q]->m_pszEventID?pChannelObj->m_ppLiveEvents[q]->m_pszEventID:"(null)",
				pChannelObj->m_ppLiveEvents[q]->m_nPosition,
				pChannelObj->m_ppLiveEvents[q]->m_ulStatus
				);  //(Dispatch message)
//	Sleep(100);
		q++;
		}
	}
	else
	{
			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Live event", "%s live events: NULL array", 
				msgsrc);
	}
}
				if(i<pChannelObj->m_nNumLiveEvents) //found an event that is playing or in the future.
				{
					// go down the list of live objects.
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s found first live event @ %d of %d with %.3f (vs. now %.3f) and %d", 
	msgsrc, i, pChannelObj->m_nNumLiveEvents,
	pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal,
	pChannelObj->m_dblAutomationTimeEstimate,
	pChannelObj->m_ppLiveEvents[i]->m_nDurationMS);  //(Dispatch message)
//Sleep(200);

					// if the item's ID = the allow ID, then end the schedule restriction with the start time of the item minus one frame

					if(pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal < pChannelObj->m_dblAutomationTimeEstimate)
					{
						if((i<pChannelObj->m_nNumLiveEvents-1)&&(pChannelObj->m_ppLiveEvents[i+1]))
							pChannelObj->m_dblNextOnAir = pChannelObj->m_ppLiveEvents[i+1]->m_dblOnAirTimeLocal;
						else
							pChannelObj->m_dblNextOnAir = pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal+ ((double)(pChannelObj->m_ppLiveEvents[i]->m_nDurationMS))/1000.0;


if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s NEXT = %.3f", 
	msgsrc, 
	pChannelObj->m_dblNextOnAir);

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s playing event @ %d of %d with %.3f (vs. now %.3f) and %d", 
	msgsrc, i, pChannelObj->m_nNumLiveEvents,
	pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal,
	pChannelObj->m_dblAutomationTimeEstimate,
	pChannelObj->m_ppLiveEvents[i]->m_nDurationMS);  //(Dispatch message)
						// item is playing
						// if the item is the allow ID, we are not in a restriction
						// if the item is an item in the db schedule, we are not in a restriction
						// else we are in a restriction.

						if((pChannelObj->m_ppLiveEvents[i]->m_pszEventID)&&(pChannelObj->m_pszSettingInsertionAllowID)&&(strcmp(pChannelObj->m_ppLiveEvents[i]->m_pszEventID, pChannelObj->m_pszSettingInsertionAllowID)==0))
						{
							bInRestrict = false; 
						}
						else
						{
							bInRestrict = true; // unless a scheduled db item is found playing
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entering schedule crit 02", 
	msgsrc);  //(Dispatch message)
			EnterCriticalSection(&pChannelObj->m_critScheduleEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entered schedule crit 02", 
	msgsrc);  //(Dispatch message)


if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_SCHEDULE", msgsrc, pChannelObj->m_dblAutomationTimeEstimate);
							int nRV = pChannelObj->FindScheduleItem(pChannelObj->m_dblAutomationTimeEstimate, INJECTOR_ARRAY_SCHEDULE);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_SCHEDULE returned %d", msgsrc, pChannelObj->m_dblAutomationTimeEstimate, nRV);
							if(nRV>=0)
							{
								// check it.
								if((pChannelObj->m_ppScheduleEvents[nRV]->m_pszEventID)&&(pChannelObj->m_ppLiveEvents[i]->m_pszEventID)&&(strcmp(pChannelObj->m_ppScheduleEvents[nRV]->m_pszEventID, pChannelObj->m_ppLiveEvents[i]->m_pszEventID)==0))
								{
									// we are good.
									bInRestrict = false; 
									
								}
								else dblOnAirTimeLocal = pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal; // reset this guy.
							}

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s leaving schedule crit 02", 
	msgsrc);  //(Dispatch message)
				LeaveCriticalSection(&pChannelObj->m_critScheduleEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s left schedule crit 02", 
	msgsrc);  //(Dispatch message)

						}
					}
					else
					{
						pChannelObj->m_dblNextOnAir = pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal;

						// item is in the future must be waiting in the list. call this in a restriction.
						bInRestrict = true; 
					}
/*
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s HERE now",
		msgsrc);  //(Dispatch message)
//Sleep(200);
*/
					if(bInRestrict)
					{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s creating new restriction object",
		msgsrc);  //(Dispatch message)

						bool bWaiting = false;
					  pScheduleRestriction = new CInjectorScheduleObject;
						if(pScheduleRestriction != NULL)
						{
							pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAirTimeLocal;
							if(
									(pChannelObj->m_dblNextOnAir >= pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal - 0.0001)
								&&(pChannelObj->m_dblNextOnAir <= pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal + 0.0001)
								)
							{
								bWaiting = true;
								// upcoming event, so restriction ends at beginning
								dblOnAirTimeLocal = pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal;
								i--; // want to check this guy again, to terminate the restriction we just makde while waiting for him.
							}
							else
							{
								dblOnAirTimeLocal = pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal+ ((double)(pChannelObj->m_ppLiveEvents[i]->m_nDurationMS))/1000.0;
							}
						}
						else
						{
							// report error
						}

						if(g_pinjector->m_settings.m_bExtraRestrictionInfo)
						{
							pScheduleDisplay = new CInjectorScheduleObject;
							if(pScheduleDisplay)
							{
								if(bWaiting == true)
								{
									// upcoming event, so restriction ends at beginning
									pScheduleDisplay->m_dblOnAirTimeLocal = pScheduleRestriction->m_dblOnAirTimeLocal;
									pScheduleDisplay->m_nDurationMS = (int)(1000.0*(pChannelObj->m_ppLiveEvents[i+1]->m_dblOnAirTimeLocal - pScheduleRestriction->m_dblOnAirTimeLocal));
									pScheduleDisplay->m_pszEventID = (char*)malloc(64);
									if(pScheduleDisplay->m_pszEventID) strcpy(pScheduleDisplay->m_pszEventID, "Restricted");

									// add it!
									while (
													((unsigned long)pScheduleDisplay->m_dblOnAirTimeLocal)/86400
												!=((unsigned long)(pScheduleDisplay->m_dblOnAirTimeLocal + ((double)(pScheduleDisplay->m_nDurationMS)/1000.0)))/86400
												) // we span midnight, try this instead of > 24 hours.

//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
									{
										CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
										if(pNew != NULL)
										{													
											pNew->m_dblOnAirTimeLocal = pScheduleDisplay->m_dblOnAirTimeLocal;
//											double dblOnAir = pScheduleDisplay->m_dblOnAirTimeLocal; // no need now + 86400.0; // added 24 hours to it, so it goes over midnight
//											pScheduleDisplay->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
//											pNew->m_nDurationMS = ((int)(pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
//											pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

											pScheduleDisplay->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
											pNew->m_nDurationMS = (int)((pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
											pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

											if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
												pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;
											if(pNew->m_nDurationMS > 0)
											{
												if(	pScheduleDisplay->m_pszEventID ) 
												{
													pNew->m_pszEventID = (char*)malloc(strlen(pScheduleDisplay->m_pszEventID)+1);
													if(pNew->m_pszEventID) strcpy(pNew->m_pszEventID, pScheduleDisplay->m_pszEventID);
												}
												util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pNew);
												pNew->m_nFileID = pScheduleDisplay->m_nFileID;
											}
											else
											{
												pNew->m_pszCaller = (char*)malloc(64); if(pNew->m_pszCaller) strcpy(pNew->m_pszCaller, "restriction+01");

												// just destroy it, zero or negative duration makes no sense
												try{ delete pNew;} catch(...){}
											}
										}
										else
										{
											// report error
											break;  // just cancel out, the rest will just have to do - it may break the display but still works.
										}
									}
									
									if(pScheduleDisplay->m_nDurationMS > 0)
									{
										util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pScheduleDisplay);
									}
									else
									{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+01");
										// just destroy it, zero or negative duration makes no sense
										try{ delete pScheduleDisplay;} catch(...){}
									}



								}
								else
								{
									// in a restricted event
									*pScheduleDisplay = *pChannelObj->m_ppLiveEvents[i];
									if(pChannelObj->m_ppLiveEvents[i]->m_pszEventID)
									{
										pScheduleDisplay->m_pszEventID = (char*)malloc(strlen(pChannelObj->m_ppLiveEvents[i]->m_pszEventID)+1);
										if(pScheduleDisplay->m_pszEventID) strcpy(pScheduleDisplay->m_pszEventID, pChannelObj->m_ppLiveEvents[i]->m_pszEventID);
									}
									if(pChannelObj->m_ppLiveEvents[i]->m_pszEventTitle)
									{
										pScheduleDisplay->m_pszEventTitle = (char*)malloc(strlen(pChannelObj->m_ppLiveEvents[i]->m_pszEventTitle)+1);
										if(pScheduleDisplay->m_pszEventTitle) strcpy(pScheduleDisplay->m_pszEventTitle, pChannelObj->m_ppLiveEvents[i]->m_pszEventTitle);
									}
									pScheduleDisplay->m_pszCreator = NULL;
									pScheduleDisplay->m_pszRecKey = NULL;
									// add it!
									while (
													((unsigned long)pScheduleDisplay->m_dblOnAirTimeLocal)/86400
												!=((unsigned long)(pScheduleDisplay->m_dblOnAirTimeLocal + ((double)(pScheduleDisplay->m_nDurationMS)/1000.0)))/86400
												) // we span midnight, try this instead of > 24 hours.

//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
									{
										CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
										if(pNew != NULL)
										{													
											pNew->m_dblOnAirTimeLocal = pScheduleDisplay->m_dblOnAirTimeLocal;
//											double dblOnAir = pScheduleDisplay->m_dblOnAirTimeLocal; // no need now + 86400.0; // added 24 hours to it, so it goes over midnight
//											pScheduleDisplay->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
//											pNew->m_nDurationMS = ((int)(pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
//											pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

											pScheduleDisplay->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
											pNew->m_nDurationMS = (int)((pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
											pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

											if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
												pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;
											if(pNew->m_nDurationMS > 0)
											{
												if(	pScheduleDisplay->m_pszEventID ) 
												{
													pNew->m_pszEventID = (char*)malloc(strlen(pScheduleDisplay->m_pszEventID)+1);
													if(pNew->m_pszEventID) strcpy(pNew->m_pszEventID, pScheduleDisplay->m_pszEventID);
												}
												util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pNew);
												pNew->m_nFileID = pScheduleDisplay->m_nFileID;
											}
											else
											{
												// just destroy it, zero or negative duration makes no sense
												try{ delete pNew;} catch(...){}
											}
										}
										else
										{
											// report error
											break;  // just cancel out, the rest will just have to do - it may break the display but still works.
										}
									}
									
									if(pScheduleDisplay->m_nDurationMS > 0)
									{
										util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pScheduleDisplay);
									}
									else
									{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+02");
										// just destroy it, zero or negative duration makes no sense
										try{ delete pScheduleDisplay;} catch(...){}
									}

								}
							}
						}
						i++;  // increment to next
					}
/*
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s HERE 02 now",
		msgsrc);  //(Dispatch message)
Sleep(200);
*/
					while(i<pChannelObj->m_nNumLiveEvents)
					{
						// check next event, if it is the allow id, we toggle if nec.
						// if it is a schedule event we toggle if nec.

						if(pChannelObj->m_ppLiveEvents[i])
						{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s checking live [%s] against [%s] to allow",
		msgsrc,
		pChannelObj->m_ppLiveEvents[i]->m_pszEventID?pChannelObj->m_ppLiveEvents[i]->m_pszEventID:"null",
		pChannelObj->m_pszSettingInsertionAllowID?pChannelObj->m_pszSettingInsertionAllowID:"null"
		);  //(Dispatch message)
							if((pChannelObj->m_ppLiveEvents[i]->m_pszEventID)&&(pChannelObj->m_pszSettingInsertionAllowID)&&(strcmp(pChannelObj->m_ppLiveEvents[i]->m_pszEventID, pChannelObj->m_pszSettingInsertionAllowID)==0))
							{
								if((bInRestrict)&&(pScheduleRestriction))
								{
									//terminate this
									pScheduleRestriction->m_nDurationMS = (int)((pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal - pScheduleRestriction->m_dblOnAirTimeLocal)*1000.0);

						
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s allowed, terminating restriction with %.3f, %d",
		msgsrc, pScheduleRestriction->m_dblOnAirTimeLocal, pScheduleRestriction->m_nDurationMS);

									while (
													((unsigned long)pScheduleRestriction->m_dblOnAirTimeLocal)/86400
												!=((unsigned long)(pScheduleRestriction->m_dblOnAirTimeLocal + ((double)(pScheduleRestriction->m_nDurationMS)/1000.0)))/86400
												) // we span midnight, try this instead of > 24 hours.

//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
									{
										CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
										if(pNew != NULL)
										{													
											pNew->m_dblOnAirTimeLocal = pScheduleRestriction->m_dblOnAirTimeLocal;
//											double dblOnAir = pScheduleRestriction->m_dblOnAirTimeLocal; // no need now + 86400.0; // added 24 hours to it, so it goes over midnight
//											pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
//											pNew->m_nDurationMS = ((int)(pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
//											pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;

											pScheduleRestriction->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
											pNew->m_nDurationMS = (int)((pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
											pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;

											if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
												pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;
											if(pNew->m_nDurationMS > 0)
											{
												util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pNew); // add this guy, then go around...
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s adding split item with %.3f, %d",
		msgsrc, pNew->m_dblOnAirTimeLocal, pNew->m_nDurationMS);

												pNew->m_nFileID = pScheduleRestriction->m_nFileID;
											}
											else
											{
												pNew->m_pszCaller = (char*)malloc(64); if(pNew->m_pszCaller) strcpy(pNew->m_pszCaller, "restriction+02");
												// just destroy it, zero or negative duration makes no sense
												try{ delete pNew;} catch(...){}
											}
										}
										else
										{
											// report error
											break;  // just cancel out, the rest will just have to do - it may break the display but still works.
										}
									}
									
									if(pScheduleRestriction->m_nDurationMS > 0)
									{
										util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pScheduleRestriction);
									}
									else
									{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+03");
										// just destroy it, zero or negative duration makes no sense
										try{ delete pScheduleRestriction;} catch(...){}
									}
									pScheduleRestriction = NULL; // reset after added to stack

								}
								else
								{// must check for gaps between allowed events.
									//GAP
									if(dblOnAirTimeLocal<pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal) // gap exists.
									{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s gap exists between allowed events",
		msgsrc);



										if(g_pinjector->m_settings.m_bExtraRestrictionInfo)
										{
											pScheduleDisplay = new CInjectorScheduleObject;
											if(pScheduleDisplay)
											{
												// upcoming event, so restriction ends at beginning
												pScheduleDisplay->m_dblOnAirTimeLocal = dblOnAirTimeLocal;
												pScheduleDisplay->m_nDurationMS = (int)((pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal - dblOnAirTimeLocal)*1000.0);
												pScheduleDisplay->m_pszEventID = (char*)malloc(64);
												if(pScheduleDisplay->m_pszEventID) strcpy(pScheduleDisplay->m_pszEventID, "Restricted");
												// add it!
												while (
																((unsigned long)pScheduleDisplay->m_dblOnAirTimeLocal)/86400
															!=((unsigned long)(pScheduleDisplay->m_dblOnAirTimeLocal + ((double)(pScheduleDisplay->m_nDurationMS)/1000.0)))/86400
															) // we span midnight, try this instead of > 24 hours.

			//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
												{
													CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
													if(pNew != NULL)
													{													
														pNew->m_dblOnAirTimeLocal = pScheduleDisplay->m_dblOnAirTimeLocal;
			//											double dblOnAir = pScheduleDisplay->m_dblOnAirTimeLocal; // no need now + 86400.0; // added 24 hours to it, so it goes over midnight
			//											pScheduleDisplay->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
			//											pNew->m_nDurationMS = ((int)(pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
			//											pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

														pScheduleDisplay->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
														pNew->m_nDurationMS = (int)((pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
														pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

														if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
															pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;
														if(pNew->m_nDurationMS > 0)
														{
															if(	pScheduleDisplay->m_pszEventID ) 
															{
																pNew->m_pszEventID = (char*)malloc(strlen(pScheduleDisplay->m_pszEventID)+1);
																if(pNew->m_pszEventID) strcpy(pNew->m_pszEventID, pScheduleDisplay->m_pszEventID);
															}
															util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pNew);
															pNew->m_nFileID = pScheduleDisplay->m_nFileID;
														}
														else
														{
												pNew->m_pszCaller = (char*)malloc(64); if(pNew->m_pszCaller) strcpy(pNew->m_pszCaller, "restriction+03");
															// just destroy it, zero or negative duration makes no sense
															try{ delete pNew;} catch(...){}
														}
													}
													else
													{
														// report error
														break;  // just cancel out, the rest will just have to do - it may break the display but still works.
													}
												}
												
												if(pScheduleDisplay->m_nDurationMS > 0)
												{
													util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pScheduleDisplay);
												}
												else
												{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+04");
													// just destroy it, zero or negative duration makes no sense
													try{ delete pScheduleDisplay;} catch(...){}
												}
											}
										}


										//create new one and add it
										pScheduleRestriction = new CInjectorScheduleObject;
										if(pScheduleRestriction != NULL)
										{
											// because, start time set already above
											pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAirTimeLocal;
											pScheduleRestriction->m_nDurationMS = (int)((pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal - dblOnAirTimeLocal)*1000.0);
											pScheduleRestriction->m_nFileID = -1; // free space
											
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s gap item created with %.3f, %d",
		msgsrc, pScheduleRestriction->m_dblOnAirTimeLocal, pScheduleRestriction->m_nDurationMS);


											while (
															((unsigned long)pScheduleRestriction->m_dblOnAirTimeLocal)/86400
														!=((unsigned long)(pScheduleRestriction->m_dblOnAirTimeLocal + ((double)(pScheduleRestriction->m_nDurationMS)/1000.0)))/86400
														) // we span midnight, try this instead of > 24 hours.

	//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
											{
												CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
												if(pNew != NULL)
												{													
													pNew->m_dblOnAirTimeLocal = pScheduleRestriction->m_dblOnAirTimeLocal;
		//											double dblOnAir = pScheduleRestriction->m_dblOnAirTimeLocal; // no need now + 86400.0; // added 24 hours to it, so it goes over midnight
		//											pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
		//											pNew->m_nDurationMS = ((int)(pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
		//											pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;

													pScheduleRestriction->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
													pNew->m_nDurationMS = (int)((pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
													pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;


													if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
														pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;
													if(pNew->m_nDurationMS > 0)
													{
														util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pNew); // add this guy, then go around...
														pNew->m_nFileID = pScheduleRestriction->m_nFileID;
													}
													else
													{
												pNew->m_pszCaller = (char*)malloc(64); if(pNew->m_pszCaller) strcpy(pNew->m_pszCaller, "restriction+04");
														// just destroy it, zero or negative duration makes no sense
														try{ delete pNew;} catch(...){}
													}
												}
												else
												{
													// report error
													break;  // just cancel out, the rest will just have to do - it may break the display but still works.
												}
											}
											if(pScheduleRestriction->m_nDurationMS > 0)
											{
												util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pScheduleRestriction);
											}
											else
											{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+05");
												// just destroy it, zero or negative duration makes no sense
												try{ delete pScheduleRestriction;} catch(...){}
											}
											pScheduleRestriction = NULL; // reset after added to stack
										}
										else
										{
											// report error
										}
									}

								}
								// always reassign this.
								dblOnAirTimeLocal = pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal+ ((double)(pChannelObj->m_ppLiveEvents[i]->m_nDurationMS))/1000.0;

								bInRestrict = false; 
							}
							else
							{
								bool bAllow=false;

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entering schedule crit 03", 
	msgsrc);  //(Dispatch message)
				EnterCriticalSection(&pChannelObj->m_critScheduleEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entered schedule crit 03", 
	msgsrc);  //(Dispatch message)

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_SCHEDULE,INJECTOR_ARRAY_FIND_CONTAINED|INJECTOR_ARRAY_FIND_USETOLERANCE", msgsrc, pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal);
								int nRV = pChannelObj->FindScheduleItem(pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal, INJECTOR_ARRAY_SCHEDULE, INJECTOR_ARRAY_FIND_CONTAINED|INJECTOR_ARRAY_FIND_USETOLERANCE);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_SCHEDULE,INJECTOR_ARRAY_FIND_CONTAINED|INJECTOR_ARRAY_FIND_USETOLERANCE returned %d", msgsrc, pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal, nRV);
								if(nRV>=0)
								{
									// check it.
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s checking live [%s] against schedule [%s]",
		msgsrc,
		pChannelObj->m_ppLiveEvents[i]->m_pszEventID?pChannelObj->m_ppLiveEvents[i]->m_pszEventID:"null",
		pChannelObj->m_ppScheduleEvents[nRV]->m_pszEventID?pChannelObj->m_ppScheduleEvents[nRV]->m_pszEventID:"null"
		);  //(Dispatch message)

									if((pChannelObj->m_ppScheduleEvents[nRV]->m_pszEventID)&&(pChannelObj->m_ppLiveEvents[i]->m_pszEventID)&&(strcmp(pChannelObj->m_ppScheduleEvents[nRV]->m_pszEventID, pChannelObj->m_ppLiveEvents[i]->m_pszEventID)==0))
									{
										if((bInRestrict)&&(pScheduleRestriction))
										{
											//terminate this
											pScheduleRestriction->m_nDurationMS = (int)((pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal - pScheduleRestriction->m_dblOnAirTimeLocal)*1000.0);


											while (
															((unsigned long)pScheduleRestriction->m_dblOnAirTimeLocal)/86400
														!=((unsigned long)(pScheduleRestriction->m_dblOnAirTimeLocal + ((double)(pScheduleRestriction->m_nDurationMS)/1000.0)))/86400
														) // we span midnight, try this instead of > 24 hours.

		//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
											{
												CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
												if(pNew != NULL)
												{													
													pNew->m_dblOnAirTimeLocal = pScheduleRestriction->m_dblOnAirTimeLocal;
		//											double dblOnAir = pScheduleRestriction->m_dblOnAirTimeLocal; // no need now + 86400.0; // added 24 hours to it, so it goes over midnight
		//											pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
		//											pNew->m_nDurationMS = ((int)(pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
		//											pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;

													pScheduleRestriction->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
													pNew->m_nDurationMS = (int)((pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
													pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;

													if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
														pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;
													if(pNew->m_nDurationMS > 0)
													{
														util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pNew); // add this guy, then go around...
														pNew->m_nFileID = pScheduleRestriction->m_nFileID;
													}
													else
													{
												pNew->m_pszCaller = (char*)malloc(64); if(pNew->m_pszCaller) strcpy(pNew->m_pszCaller, "restriction+05");
														// just destroy it, zero or negative duration makes no sense
														try{ delete pNew;} catch(...){}
													}
												}
												else
												{
													// report error
													break;  // just cancel out, the rest will just have to do - it may break the display but still works.
												}
											}
											
											if(pScheduleRestriction->m_nDurationMS > 0)
											{
												util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pScheduleRestriction);
											}
											else
											{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+06");
												// just destroy it, zero or negative duration makes no sense
												try{ delete pScheduleRestriction;} catch(...){}
											}
											pScheduleRestriction = NULL; // reset after added to stack
										}
										else
										{// must check for gaps between allowed events.
											//GAP
											if(dblOnAirTimeLocal<pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal) // gap exists.
											{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s gap exists 01 between allowed events",
		msgsrc);

											if(g_pinjector->m_settings.m_bExtraRestrictionInfo)
											{
												pScheduleDisplay = new CInjectorScheduleObject;
												if(pScheduleDisplay)
												{
													// upcoming event, so restriction ends at beginning
													pScheduleDisplay->m_dblOnAirTimeLocal = dblOnAirTimeLocal;
													pScheduleDisplay->m_nDurationMS = (int)((pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal - dblOnAirTimeLocal)*1000.0);
													pScheduleDisplay->m_pszEventID = (char*)malloc(64);
													if(pScheduleDisplay->m_pszEventID) strcpy(pScheduleDisplay->m_pszEventID, "Restricted");
													// add it!
													while (
																	((unsigned long)pScheduleDisplay->m_dblOnAirTimeLocal)/86400
																!=((unsigned long)(pScheduleDisplay->m_dblOnAirTimeLocal + ((double)(pScheduleDisplay->m_nDurationMS)/1000.0)))/86400
																) // we span midnight, try this instead of > 24 hours.

				//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
													{
														CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
														if(pNew != NULL)
														{													
															pNew->m_dblOnAirTimeLocal = pScheduleDisplay->m_dblOnAirTimeLocal;
				//											double dblOnAir = pScheduleDisplay->m_dblOnAirTimeLocal; // no need now + 86400.0; // added 24 hours to it, so it goes over midnight
				//											pScheduleDisplay->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
				//											pNew->m_nDurationMS = ((int)(pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
				//											pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

															pScheduleDisplay->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
															pNew->m_nDurationMS = (int)((pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
															pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

															if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
																pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;
															if(pNew->m_nDurationMS > 0)
															{
																if(	pScheduleDisplay->m_pszEventID ) 
																{
																	pNew->m_pszEventID = (char*)malloc(strlen(pScheduleDisplay->m_pszEventID)+1);
																	if(pNew->m_pszEventID) strcpy(pNew->m_pszEventID, pScheduleDisplay->m_pszEventID);
																}
																util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pNew);
																pNew->m_nFileID = pScheduleDisplay->m_nFileID;
															}
															else
															{
												pNew->m_pszCaller = (char*)malloc(64); if(pNew->m_pszCaller) strcpy(pNew->m_pszCaller, "restriction+06");
																// just destroy it, zero or negative duration makes no sense
																try{ delete pNew;} catch(...){}
															}
														}
														else
														{
															// report error
															break;  // just cancel out, the rest will just have to do - it may break the display but still works.
														}
													}
													
													if(pScheduleDisplay->m_nDurationMS > 0)
													{
														util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pScheduleDisplay);
													}
													else
													{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+07");
														// just destroy it, zero or negative duration makes no sense
														try{ delete pScheduleDisplay;} catch(...){}
													}
												}
											}


												//create new one and add it
												pScheduleRestriction = new CInjectorScheduleObject;
												if(pScheduleRestriction != NULL)
												{
													// because, start time set already above
													pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAirTimeLocal;
													pScheduleRestriction->m_nDurationMS = (int)((pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal-dblOnAirTimeLocal)*1000.0);
													pScheduleRestriction->m_nFileID = -1; // free space
													
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s gap item 01 created with %.3f, %d",
		msgsrc, pScheduleRestriction->m_dblOnAirTimeLocal, pScheduleRestriction->m_nDurationMS);

														while (
																		((unsigned long)pScheduleRestriction->m_dblOnAirTimeLocal)/86400
																	!=((unsigned long)(pScheduleRestriction->m_dblOnAirTimeLocal + ((double)(pScheduleRestriction->m_nDurationMS)/1000.0)))/86400
																	) // we span midnight, try this instead of > 24 hours.

				//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
													{
														CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
														if(pNew != NULL)
														{													
															pNew->m_dblOnAirTimeLocal = pScheduleRestriction->m_dblOnAirTimeLocal;
				//											double dblOnAir = pScheduleRestriction->m_dblOnAirTimeLocal; // no need now + 86400.0; // added 24 hours to it, so it goes over midnight
				//											pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
				//											pNew->m_nDurationMS = ((int)(pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
				//											pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;

															pScheduleRestriction->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
															pNew->m_nDurationMS = (int)((pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
															pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;
															if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
																pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;

															if(pNew->m_nDurationMS > 0)
															{
																util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pNew); // add this guy, then go around...
																pNew->m_nFileID = pScheduleRestriction->m_nFileID;
															}
															else
															{
												pNew->m_pszCaller = (char*)malloc(64); if(pNew->m_pszCaller) strcpy(pNew->m_pszCaller, "restriction+07");
																// just destroy it, zero or negative duration makes no sense
																try{ delete pNew;} catch(...){}
															}
														}
														else
														{
															// report error
															break;  // just cancel out, the rest will just have to do - it may break the display but still works.
														}
													}
													if(pScheduleRestriction->m_nDurationMS > 0)
													{
														util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pScheduleRestriction);
													}
													else
													{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+08");
														// just destroy it, zero or negative duration makes no sense
														try{ delete pScheduleRestriction;} catch(...){}
													}
													pScheduleRestriction = NULL; // reset after added to stack
												}
												else
												{
													// report error
												}
											}
										}

										bInRestrict = false; 
										bAllow=true;
									}
									//always reassign
									dblOnAirTimeLocal = pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal+ ((double)(pChannelObj->m_ppLiveEvents[i]->m_nDurationMS))/1000.0;

								}
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s leaving schedule crit 03", 
	msgsrc);  //(Dispatch message)
				LeaveCriticalSection(&pChannelObj->m_critScheduleEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s left schedule crit 03", 
	msgsrc);  //(Dispatch message)


								if(!bAllow)
								{
									if(g_pinjector->m_settings.m_bExtraRestrictionInfo)
									{
										pScheduleDisplay = new CInjectorScheduleObject;
										if(pScheduleDisplay)
										{
											// in a restricted event
											*pScheduleDisplay = *pChannelObj->m_ppLiveEvents[i];
											if(pChannelObj->m_ppLiveEvents[i]->m_pszEventID)
											{
												pScheduleDisplay->m_pszEventID = (char*)malloc(strlen(pChannelObj->m_ppLiveEvents[i]->m_pszEventID)+1);
												if(pScheduleDisplay->m_pszEventID) strcpy(pScheduleDisplay->m_pszEventID, pChannelObj->m_ppLiveEvents[i]->m_pszEventID);
											}
											if(pChannelObj->m_ppLiveEvents[i]->m_pszEventTitle)
											{
												pScheduleDisplay->m_pszEventTitle = (char*)malloc(strlen(pChannelObj->m_ppLiveEvents[i]->m_pszEventTitle)+1);
												if(pScheduleDisplay->m_pszEventTitle) strcpy(pScheduleDisplay->m_pszEventTitle, pChannelObj->m_ppLiveEvents[i]->m_pszEventTitle);
											}
											pScheduleDisplay->m_pszCreator = NULL;
											pScheduleDisplay->m_pszRecKey = NULL;

											// add it!
											while (
															((unsigned long)pScheduleDisplay->m_dblOnAirTimeLocal)/86400
														!=((unsigned long)(pScheduleDisplay->m_dblOnAirTimeLocal + ((double)(pScheduleDisplay->m_nDurationMS)/1000.0)))/86400
														) // we span midnight, try this instead of > 24 hours.

		//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
											{
												CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
												if(pNew != NULL)
												{													
													pNew->m_dblOnAirTimeLocal = pScheduleDisplay->m_dblOnAirTimeLocal;
		//											double dblOnAir = pScheduleDisplay->m_dblOnAirTimeLocal; // no need now + 86400.0; // added 24 hours to it, so it goes over midnight
		//											pScheduleDisplay->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
		//											pNew->m_nDurationMS = ((int)(pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
		//											pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

													pScheduleDisplay->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
													pNew->m_nDurationMS = (int)((pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
													pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

													if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
														pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;
													if(pNew->m_nDurationMS > 0)
													{
														if(	pScheduleDisplay->m_pszEventID ) 
														{
															pNew->m_pszEventID = (char*)malloc(strlen(pScheduleDisplay->m_pszEventID)+1);
															if(pNew->m_pszEventID) strcpy(pNew->m_pszEventID, pScheduleDisplay->m_pszEventID);
														}
														util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pNew);
														pNew->m_nFileID = pScheduleDisplay->m_nFileID;
													}
													else
													{
												pNew->m_pszCaller = (char*)malloc(64); if(pNew->m_pszCaller) strcpy(pNew->m_pszCaller, "restriction+08");
														// just destroy it, zero or negative duration makes no sense
														try{ delete pNew;} catch(...){}
													}
												}
												else
												{
													// report error
													break;  // just cancel out, the rest will just have to do - it may break the display but still works.
												}
											}
											
											if(pScheduleDisplay->m_nDurationMS > 0)
											{
												util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pScheduleDisplay);
											}
											else
											{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+09");
												// just destroy it, zero or negative duration makes no sense
												try{ delete pScheduleDisplay;} catch(...){}
											}

											
										}
									}
									// check to see if we are not in a restriction, make one if we aren't
									if(!bInRestrict)
									{
										bInRestrict = true;
										pScheduleRestriction = new CInjectorScheduleObject;
										if(pScheduleRestriction != NULL)
										{
											pScheduleRestriction->m_dblOnAirTimeLocal = min(dblOnAirTimeLocal, pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal); //have to back it up if there was any blank space.
										}
										else
										{
											// report error
										}

									}// else it's good just keep restricting.
									else
									{
										// extend the restriction to the end of this event
										//dblOnAirTimeLocal = pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal+ ((double)(pChannelObj->m_ppLiveEvents[i]->m_nDurationMS))/1000.0;
									}
									dblOnAirTimeLocal = pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal+ ((double)(pChannelObj->m_ppLiveEvents[i]->m_nDurationMS))/1000.0;
								}
								//else // dont have to do anything, gap checked above in allow spots

							}
						}

						i++;
					}//while(i<pChannelObj->m_nNumLiveEvents)

					pChannelObj->m_dblLastOnAir = dblOnAirTimeLocal;  // last moment of on air programming

				}
				else
				{ // no live event found!
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s no live event found", 
	msgsrc);  //(Dispatch message)
//Sleep(200);
					pChannelObj->m_dblNextOnAir = -1.0;
					pChannelObj->m_dblLastOnAir = -1.0;
				}


				// here have to reset any flags past the live part.

				if(
						(bLiveEventsChanged)
					&&(pChannelObj->m_nNumLiveEvents>0) //found an event that is playing or in the future.
					&&(pChannelObj->m_dblLastOnAir > 0.0)
					&&(pChannelObj->m_dblAutomationTimeEstimate >0.0)
					&&(pChannelObj->m_dblLastOnAir > pChannelObj->m_dblAutomationTimeEstimate)
					&&((dblLastLast < pChannelObj->m_dblLastOnAir-.0001)||(dblLastLast > pChannelObj->m_dblLastOnAir+.0001))
					)
				{
					//reset
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = 0 WHERE scheduled_on > %.3f AND channelid = %d AND created_by <> 'sys'", // HARDCODE sys for safety
							((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
							pChannelObj->m_dblLastOnAir, pChannelObj->m_nChannelID
						);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule flags reset", "%s updating flags\r\nSQL: %s", 
msgsrc, szSQL);  //(Dispatch message)
					if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						//error
g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "InjectorScheduleRestrictionThread", "%s error updating flags: %s", 
msgsrc, 
errorstring);  //(Dispatch message)
					}
					else
					{
						dblLastLast = pChannelObj->m_dblLastOnAir;
					}

				}


			
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s leaving live crit 01", 
	msgsrc);  //(Dispatch message)
				LeaveCriticalSection(&pChannelObj->m_critLiveEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s left live crit 01", 
	msgsrc);  //(Dispatch message)






				// now we are out of the live events.  must continue with any traffic events.
				// first find the join point.
				// but if there were no live events, need to initialize the first event based on traffic
				//if no live events, pScheduleRestriction will be NULL;
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s checking traffic events", 
	msgsrc);  //(Dispatch message)

				EnterCriticalSection(&pChannelObj->m_critTrafficEvents);

/*
				if((pScheduleRestriction == NULL)&&(bInRestrict)) //must initialize
				{

				}
done below
*/

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_TRAFFIC, INJECTOR_ARRAY_FIND_AFTER_OR_AT", msgsrc, dblOnAirTimeLocal);
				i = pChannelObj->FindScheduleItem(dblOnAirTimeLocal, INJECTOR_ARRAY_TRAFFIC, INJECTOR_ARRAY_FIND_AFTER_OR_AT);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_TRAFFIC, INJECTOR_ARRAY_FIND_AFTER_OR_AT returned %d", msgsrc, dblOnAirTimeLocal, i);
				if(i>=0)
				{
					while(i<pChannelObj->m_nNumTrafficEvents)
					{
						// check next event, if it is the allow id, we toggle if nec.
						// if it is a schedule event we toggle if nec.

						if(pChannelObj->m_ppTrafficEvents[i])
						{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s checking traffic [%s] against [%s] to allow",
		msgsrc,
		pChannelObj->m_ppTrafficEvents[i]->m_pszEventID?pChannelObj->m_ppTrafficEvents[i]->m_pszEventID:"null",
		pChannelObj->m_pszSettingInsertionAllowID?pChannelObj->m_pszSettingInsertionAllowID:"null"
		);  //(Dispatch message)
							if((pChannelObj->m_ppTrafficEvents[i]->m_pszEventID)&&(pChannelObj->m_pszSettingInsertionAllowID)&&(strcmp(pChannelObj->m_ppTrafficEvents[i]->m_pszEventID, pChannelObj->m_pszSettingInsertionAllowID)==0))
							{
								if((bInRestrict)&&(pScheduleRestriction))
								{
									//terminate this
									pScheduleRestriction->m_nDurationMS = (int)((pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal - pScheduleRestriction->m_dblOnAirTimeLocal)*1000.0);
									pScheduleRestriction->m_nFileID = pChannelObj->m_ppTrafficEvents[i]->m_nFileID;
									

										while (
														((unsigned long)pScheduleRestriction->m_dblOnAirTimeLocal)/86400
													!=((unsigned long)(pScheduleRestriction->m_dblOnAirTimeLocal + ((double)(pScheduleRestriction->m_nDurationMS)/1000.0)))/86400
													) // we span midnight, try this instead of > 24 hours.

//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
									{
										CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
										if(pNew != NULL)
										{													
											pNew->m_dblOnAirTimeLocal = pScheduleRestriction->m_dblOnAirTimeLocal;
//											double dblOnAir = pScheduleRestriction->m_dblOnAirTimeLocal; // no need now + 86400.0; // added 24 hours to it, so it goes over midnight
//											pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
//											pNew->m_nDurationMS = ((int)(pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
//											pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;

											pScheduleRestriction->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
											pNew->m_nDurationMS = (int)((pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
											pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;

											if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
												pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;
											if(pNew->m_nDurationMS > 0)
											{
												util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pNew); // add this guy, then go around...
												pNew->m_nFileID = pScheduleRestriction->m_nFileID;
											}
											else
											{
												pNew->m_pszCaller = (char*)malloc(64); if(pNew->m_pszCaller) strcpy(pNew->m_pszCaller, "restriction+09");
												// just destroy it, zero or negative duration makes no sense
												try{ delete pNew;} catch(...){}
											}
										}
										else
										{
											// report error
											break;  // just cancel out, the rest will just have to do - it may break the display but still works.
										}
									}
									
									if(pScheduleRestriction->m_nDurationMS > 0)
									{
										util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pScheduleRestriction);
									}
									else
									{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+10");
										// just destroy it, zero or negative duration makes no sense
										try{ delete pScheduleRestriction;} catch(...){}
									}
									pScheduleRestriction = NULL; // reset after added to stack

								}
								else
								{// must check for gaps between allowed events.
									//GAP
									if(dblOnAirTimeLocal<pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal) // gap exists.
									{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s gap exists 02 between allowed events",
		msgsrc);
										if(g_pinjector->m_settings.m_bExtraRestrictionInfo)
										{
											pScheduleDisplay = new CInjectorScheduleObject;
											if(pScheduleDisplay)
											{
												// upcoming event, so restriction ends at beginning
												pScheduleDisplay->m_dblOnAirTimeLocal = dblOnAirTimeLocal;
												pScheduleDisplay->m_nDurationMS = (int)((pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal - dblOnAirTimeLocal)*1000.0);
												pScheduleDisplay->m_pszEventID = (char*)malloc(64);
												if(pScheduleDisplay->m_pszEventID) strcpy(pScheduleDisplay->m_pszEventID, "Restricted");
												// add it!
												while (
																((unsigned long)pScheduleDisplay->m_dblOnAirTimeLocal)/86400
															!=((unsigned long)(pScheduleDisplay->m_dblOnAirTimeLocal + ((double)(pScheduleDisplay->m_nDurationMS)/1000.0)))/86400
															) // we span midnight, try this instead of > 24 hours.

			//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
												{
													CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
													if(pNew != NULL)
													{													
														pNew->m_dblOnAirTimeLocal = pScheduleDisplay->m_dblOnAirTimeLocal;
			//											double dblOnAir = pScheduleDisplay->m_dblOnAirTimeLocal; // no need now + 86400.0; // added 24 hours to it, so it goes over midnight
			//											pScheduleDisplay->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
			//											pNew->m_nDurationMS = ((int)(pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
			//											pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

														pScheduleDisplay->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
														pNew->m_nDurationMS = (int)((pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
														pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

														if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
															pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;
														if(pNew->m_nDurationMS > 0)
														{
															if(	pScheduleDisplay->m_pszEventID ) 
															{
																pNew->m_pszEventID = (char*)malloc(strlen(pScheduleDisplay->m_pszEventID)+1);
																if(pNew->m_pszEventID) strcpy(pNew->m_pszEventID, pScheduleDisplay->m_pszEventID);
															}
															util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pNew);
															pNew->m_nFileID = pScheduleDisplay->m_nFileID;
														}
														else
														{
												pNew->m_pszCaller = (char*)malloc(64); if(pNew->m_pszCaller) strcpy(pNew->m_pszCaller, "restriction+10");
															// just destroy it, zero or negative duration makes no sense
															try{ delete pNew;} catch(...){}
														}
													}
													else
													{
														// report error
														break;  // just cancel out, the rest will just have to do - it may break the display but still works.
													}
												}
												
												if(pScheduleDisplay->m_nDurationMS > 0)
												{
													util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pScheduleDisplay);
												}
												else
												{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+11");
													// just destroy it, zero or negative duration makes no sense
													try{ delete pScheduleDisplay;} catch(...){}
												}
											}
										}

										//create new one and add it
										pScheduleRestriction = new CInjectorScheduleObject;
										if(pScheduleRestriction != NULL)
										{
											// because, start time set already above
											pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAirTimeLocal;
											pScheduleRestriction->m_nDurationMS = (int)((pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal - dblOnAirTimeLocal)*1000.0);
											pScheduleRestriction->m_nFileID = -1; // free space
											
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s gap item 02 created with %.3f, %d",
		msgsrc, pScheduleRestriction->m_dblOnAirTimeLocal, pScheduleRestriction->m_nDurationMS);

												while (
																((unsigned long)pScheduleRestriction->m_dblOnAirTimeLocal)/86400
															!=((unsigned long)(pScheduleRestriction->m_dblOnAirTimeLocal + ((double)(pScheduleRestriction->m_nDurationMS)/1000.0)))/86400
															) // we span midnight, try this instead of > 24 hours.

		//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
											{
	if(0)//g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s gap item 02 mods %d, %d",
		msgsrc, ((unsigned long)pScheduleRestriction->m_dblOnAirTimeLocal)%86400, ((unsigned long)(pScheduleRestriction->m_dblOnAirTimeLocal + ((double)(pScheduleRestriction->m_nDurationMS)/1000.0)))%86400);

											CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
												if(pNew != NULL)
												{													
													pNew->m_dblOnAirTimeLocal = pScheduleRestriction->m_dblOnAirTimeLocal;
		//											double dblOnAir = pScheduleRestriction->m_dblOnAirTimeLocal; // no need now + 86400.0; // added 24 hours to it, so it goes over midnight
		//											pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
		//											pNew->m_nDurationMS = ((int)(pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
		//											pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;

													pScheduleRestriction->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
													pNew->m_nDurationMS = (int)((pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
													pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;
													if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
														pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;

													if(pNew->m_nDurationMS > 0)
													{
														util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pNew); // add this guy, then go around...
														pNew->m_nFileID = pScheduleRestriction->m_nFileID;
													}
													else
													{
												pNew->m_pszCaller = (char*)malloc(64); if(pNew->m_pszCaller) strcpy(pNew->m_pszCaller, "restriction+11");
														// just destroy it, zero or negative duration makes no sense
														try{ delete pNew;} catch(...){}
													}
												}
												else
												{
													// report error
													break;  // just cancel out, the rest will just have to do - it may break the display but still works.
												}
											}
											if(pScheduleRestriction->m_nDurationMS > 0)
											{
												util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pScheduleRestriction);
											}
											else
											{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+12");
												// just destroy it, zero or negative duration makes no sense
												try{ delete pScheduleRestriction;} catch(...){}
											}
											pScheduleRestriction = NULL; // reset after added to stack
										}
										else
										{
											// report error
										}
									}

								}
								// always reassign
								dblOnAirTimeLocal = pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal+ ((double)(pChannelObj->m_ppTrafficEvents[i]->m_nDurationMS))/1000.0;

								bInRestrict = false; 
							}
							else
							{
								bool bAllow=false;
								/*  // should NOT check traffic items against the schedule.  traffic overrides anything scheduled, need to reconcile this after.
					EnterCriticalSection(&pChannelObj->m_critScheduleEvents);
								int nRV = pChannelObj->FindScheduleItem(pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal, INJECTOR_ARRAY_SCHEDULE);
								if(nRV>=0)
								{
									// check it.
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s checking traffic [%s] against schedule [%s]",
		msgsrc,
		pChannelObj->m_ppTrafficEvents[i]->m_pszEventID?pChannelObj->m_ppTrafficEvents[i]->m_pszEventID:"null",
		pChannelObj->m_ppScheduleEvents[nRV]->m_pszEventID?pChannelObj->m_ppScheduleEvents[nRV]->m_pszEventID:"null"
		);  //(Dispatch message)
									if((pChannelObj->m_ppScheduleEvents[nRV]->m_pszEventID)&&(pChannelObj->m_ppTrafficEvents[i]->m_pszEventID)&&(strcmp(pChannelObj->m_ppScheduleEvents[nRV]->m_pszEventID, pChannelObj->m_ppTrafficEvents[i]->m_pszEventID)==0))
									{
										if((bInRestrict)&&(pScheduleRestriction))
										{
											//terminate this
											pScheduleRestriction->m_nDurationMS = (int)((pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal - pScheduleRestriction->m_dblOnAirTimeLocal)*1000.0);
											pScheduleRestriction->m_nFileID = pChannelObj->m_ppTrafficEvents[i]->m_nFileID;


											while (
															((unsigned long)pScheduleRestriction->m_dblOnAirTimeLocal)/86400
														!=((unsigned long)(pScheduleRestriction->m_dblOnAirTimeLocal + ((double)(pScheduleRestriction->m_nDurationMS)/1000.0)))/86400
														) // we span midnight, try this instead of > 24 hours.

		//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
											{
												CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
												if(pNew != NULL)
												{													
													pNew->m_dblOnAirTimeLocal = pScheduleRestriction->m_dblOnAirTimeLocal;
													double dblOnAir = pScheduleRestriction->m_dblOnAirTimeLocal + 86400.0; // added 24 hours to it, so it goes over midnight
													pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
													pNew->m_nDurationMS = ((int)(pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
													pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;

													util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pNew); // add this guy, then go around...

													pNew->m_nFileID = pScheduleRestriction->m_nFileID;
												}
												else
												{
													// report error
													break;  // just cancel out, the rest will just have to do - it may break the display but still works.
												}
											}

											util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pScheduleRestriction);
											pScheduleRestriction = NULL; // reset after added to stack
										}
										else
										{// must check for gaps between allowed events.
											//GAP
											if(dblOnAirTimeLocal<pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal) // gap exists.
											{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s gap exists 03 between allowed events",
		msgsrc);
												//create new one and add it
												pScheduleRestriction = new CInjectorScheduleObject;
												if(pScheduleRestriction != NULL)
												{
													// because, start time set already above
													pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAirTimeLocal;
													pScheduleRestriction->m_nDurationMS = (int)((pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal - dblOnAirTimeLocal)*1000.0);
													pScheduleRestriction->m_nFileID = -1; // free space
													
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s gap item 03 created with %.3f, %d",
		msgsrc, pScheduleRestriction->m_dblOnAirTimeLocal, pScheduleRestriction->m_nDurationMS);

													while (
																	((unsigned long)pScheduleRestriction->m_dblOnAirTimeLocal)/86400
																!=((unsigned long)(pScheduleRestriction->m_dblOnAirTimeLocal + ((double)(pScheduleRestriction->m_nDurationMS)/1000.0)))/86400
																) // we span midnight, try this instead of > 24 hours.

				//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
													{
														CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
														if(pNew != NULL)
														{													
															pNew->m_dblOnAirTimeLocal = pScheduleRestriction->m_dblOnAirTimeLocal;
															double dblOnAir = pScheduleRestriction->m_dblOnAirTimeLocal + 86400.0; // added 24 hours to it, so it goes over midnight
															pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
															pNew->m_nDurationMS = ((int)(pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
															pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;

															util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pNew); // add this guy, then go around...

															pNew->m_nFileID = pScheduleRestriction->m_nFileID;
														}
														else
														{
															// report error
															break;  // just cancel out, the rest will just have to do - it may break the display but still works.
														}
													}
													util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pScheduleRestriction);
													pScheduleRestriction = NULL; // reset after added to stack
												}
												else
												{
													// report error
												}
											}
										}
										//always reassign
										dblOnAirTimeLocal = pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal+ ((double)(pChannelObj->m_ppTrafficEvents[i]->m_nDurationMS))/1000.0;

										bInRestrict = false; 
										bAllow=true;
									}
								}
					LeaveCriticalSection(&pChannelObj->m_critScheduleEvents);
*/
								if(!bAllow)
								{


									if(g_pinjector->m_settings.m_bExtraRestrictionInfo)
									{
										pScheduleDisplay = new CInjectorScheduleObject;
										if(pScheduleDisplay)
										{
											// in a restricted event
											*pScheduleDisplay = *pChannelObj->m_ppTrafficEvents[i];
											if(pChannelObj->m_ppTrafficEvents[i]->m_pszEventID)
											{
												pScheduleDisplay->m_pszEventID = (char*)malloc(strlen(pChannelObj->m_ppTrafficEvents[i]->m_pszEventID)+1);
												if(pScheduleDisplay->m_pszEventID) strcpy(pScheduleDisplay->m_pszEventID, pChannelObj->m_ppTrafficEvents[i]->m_pszEventID);
											}
											if(pChannelObj->m_ppTrafficEvents[i]->m_pszEventTitle)
											{
												pScheduleDisplay->m_pszEventTitle = (char*)malloc(strlen(pChannelObj->m_ppTrafficEvents[i]->m_pszEventTitle)+1);
												if(pScheduleDisplay->m_pszEventTitle) strcpy(pScheduleDisplay->m_pszEventTitle, pChannelObj->m_ppTrafficEvents[i]->m_pszEventTitle);
											}
											pScheduleDisplay->m_pszCreator = NULL;
											pScheduleDisplay->m_pszRecKey = NULL;

											// add it!
											while (
															((unsigned long)pScheduleDisplay->m_dblOnAirTimeLocal)/86400
														!=((unsigned long)(pScheduleDisplay->m_dblOnAirTimeLocal + ((double)(pScheduleDisplay->m_nDurationMS)/1000.0)))/86400
														) // we span midnight, try this instead of > 24 hours.

		//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
											{
												CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
												if(pNew != NULL)
												{													
													pNew->m_dblOnAirTimeLocal = pScheduleDisplay->m_dblOnAirTimeLocal;
		//											double dblOnAir = pScheduleDisplay->m_dblOnAirTimeLocal; // no need now + 86400.0; // added 24 hours to it, so it goes over midnight
		//											pScheduleDisplay->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
		//											pNew->m_nDurationMS = ((int)(pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
		//											pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

													pScheduleDisplay->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
													pNew->m_nDurationMS = (int)((pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
													pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

													if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
														pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;
													if(pNew->m_nDurationMS > 0)
													{
														if(	pScheduleDisplay->m_pszEventID ) 
														{
															pNew->m_pszEventID = (char*)malloc(strlen(pScheduleDisplay->m_pszEventID)+1);
															if(pNew->m_pszEventID) strcpy(pNew->m_pszEventID, pScheduleDisplay->m_pszEventID);
														}
														util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pNew);
														pNew->m_nFileID = pScheduleDisplay->m_nFileID;
													}
													else
													{
												pNew->m_pszCaller = (char*)malloc(64); if(pNew->m_pszCaller) strcpy(pNew->m_pszCaller, "restriction+12");
														// just destroy it, zero or negative duration makes no sense
														try{ delete pNew;} catch(...){}
													}
												}
												else
												{
													// report error
													break;  // just cancel out, the rest will just have to do - it may break the display but still works.
												}
											}
											
											if(pScheduleDisplay->m_nDurationMS > 0)
											{
												util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pScheduleDisplay);
											}
											else
											{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+13");
												// just destroy it, zero or negative duration makes no sense
												try{ delete pScheduleDisplay;} catch(...){}
											}
											
										}
									}

									// check to see if we are not in a restriction, make one if we aren't
									if(!bInRestrict)
									{
										bInRestrict = true;
										pScheduleRestriction = new CInjectorScheduleObject;
										if(pScheduleRestriction != NULL)
										{
											pScheduleRestriction->m_dblOnAirTimeLocal = min(dblOnAirTimeLocal, pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal);
											pScheduleRestriction->m_nFileID = pChannelObj->m_ppTrafficEvents[i]->m_nFileID;
										}
										else
										{
											// report error
										}

									}// else it's good just keep restricting. unless we didnt find any live events and this is the first one
									else
									{
										// extend the restriction to the end of this event
//										dblOnAirTimeLocal = pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal+ ((double)(pChannelObj->m_ppTrafficEvents[i]->m_nDurationMS))/1000.0;

										if(pScheduleRestriction == NULL)
										{
											pScheduleRestriction = new CInjectorScheduleObject;
											if(pScheduleRestriction != NULL)
											{
												// because, start time set already above
												pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAirTimeLocal;//pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal;
												pScheduleRestriction->m_nFileID = pChannelObj->m_ppTrafficEvents[i]->m_nFileID;
												//dblOnAirTimeLocal = pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal+ ((double)(pChannelObj->m_ppTrafficEvents[i]->m_nDurationMS))/1000.0;
											}
											else
											{
												// report error
											}
										}
									}
									//always reassign
									dblOnAirTimeLocal = pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal+ ((double)(pChannelObj->m_ppTrafficEvents[i]->m_nDurationMS))/1000.0;

								}
//								else  // already dealt with it above, so this part not needed
								else  // above stuff removed, add in time reassingment.
								{
									dblOnAirTimeLocal = pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal+ ((double)(pChannelObj->m_ppTrafficEvents[i]->m_nDurationMS))/1000.0;
								}
							}
						}

						i++;
					}//while(i<pChannelObj->m_nNumTrafficEvents)

				}

				LeaveCriticalSection(&pChannelObj->m_critTrafficEvents);

				// if we are in a restriction we must terminate it - just extend it to the max.

				if((bInRestrict)&&(pScheduleRestriction))
				{
					//terminate this
					pScheduleRestriction->m_nDurationMS = (int)((dblOnAirTimeLocal - pScheduleRestriction->m_dblOnAirTimeLocal)*1000.0);


					while (
									((unsigned long)pScheduleRestriction->m_dblOnAirTimeLocal)/86400
								!=((unsigned long)(pScheduleRestriction->m_dblOnAirTimeLocal + ((double)(pScheduleRestriction->m_nDurationMS)/1000.0)))/86400
								) // we span midnight, try this instead of > 24 hours.

//					while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
					{
						CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
						if(pNew != NULL)
						{													
							pNew->m_dblOnAirTimeLocal = pScheduleRestriction->m_dblOnAirTimeLocal;
//											double dblOnAir = pScheduleRestriction->m_dblOnAirTimeLocal; // no need now + 86400.0; // added 24 hours to it, so it goes over midnight
//											pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
//											pNew->m_nDurationMS = ((int)(pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
//											pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;

							pScheduleRestriction->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
							pNew->m_nDurationMS = (int)((pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
							pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;

							if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
								pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;

							if(pNew->m_nDurationMS > 0)
							{
								util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pNew); // add this guy, then go around...
								pNew->m_nFileID = pScheduleRestriction->m_nFileID;
							}
							else
							{
												pNew->m_pszCaller = (char*)malloc(64); if(pNew->m_pszCaller) strcpy(pNew->m_pszCaller, "restriction+13");
								// just destroy it, zero or negative duration makes no sense
								try{ delete pNew;} catch(...){}
							}
						}
						else
						{
							// report error
							break;  // just cancel out, the rest will just have to do - it may break the display but still works.
						}
					}

					if(pScheduleRestriction->m_nDurationMS > 0)
					{
						util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pScheduleRestriction);
					}
					else
					{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+14");
						// just destroy it, zero or negative duration makes no sense
						try{ delete pScheduleRestriction;} catch(...){}
					}
					pScheduleRestriction = NULL; // reset after added to stack
				}

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s calculated %d restrictions", 
	msgsrc, nNumScheduleRestrictions);  //(Dispatch message)

				// now need to restrict from last item to the end of the world


				// need to calculate the end date.
				// it is the number of m_nAdvanceSchedulingThresholdMaxDays days, that does not include restricted days.
				// sadly, it is not just number of days.
				// so have to go thru each day to figure it out.

				int nExtentDay = g_pinjector->m_settings.m_nAdvanceSchedulingThresholdMaxDays;
				EnterCriticalSection(&g_pinjector->m_data.m_critRestrictions);
				if((g_pinjector->m_data.m_ppRestrictions)&&(g_pinjector->m_data.m_nNumRestrictions>0))
				{
					int r=0, count=0;
					while(count<g_pinjector->m_settings.m_nAdvanceSchedulingThresholdMaxDays)
					{
						r++;
						long chkday = ((((unsigned long)(pChannelObj->m_dblAutomationTimeEstimate))/86400 + r)*86400);
						tm* theTime = gmtime( &chkday );

						//check that it is not excluded
						int g=0;
						
						bool bExcluded = false;
						while((g < g_pinjector->m_data.m_nNumRestrictions)&&(!bExcluded))
						{
							if(g_pinjector->m_data.m_ppRestrictions[g])
							{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s checking lookahead against restriction %d (type %d) value %.3f vs %d, %d", 
	msgsrc, 
	g, g_pinjector->m_data.m_ppRestrictions[g]->m_usType,
	(g_pinjector->m_data.m_ppRestrictions[g]->m_usType==1)?((double)g_pinjector->m_data.m_ppRestrictions[g]->m_nDurationMS):g_pinjector->m_data.m_ppRestrictions[g]->m_dblOnAirTimeLocal,
	chkday, theTime->tm_wday);  //(Dispatch message)

								switch(g_pinjector->m_data.m_ppRestrictions[g]->m_usType)
								{
								case INJECTOR_RESTRICTION_WHOLEDAY://   0
									{
										if(((long)(g_pinjector->m_data.m_ppRestrictions[g]->m_dblOnAirTimeLocal))/86400 == chkday/86400)
										{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s lookahead excluded, day %d restriction", 
	msgsrc, chkday);  //(Dispatch message)

											bExcluded = true;
										}
									} break;
								case INJECTOR_RESTRICTION_WEEKDAY://		1
									{
										if(g_pinjector->m_data.m_ppRestrictions[g]->m_nDurationMS == theTime->tm_wday)
										{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s lookahead excluded: %d weekday restriction",
	msgsrc, theTime->tm_wday);  //(Dispatch message)
											bExcluded = true;
										}
									} break;
								}

							}
							g++;
						}

						if(!bExcluded) count++;

						if(count >= g_pinjector->m_settings.m_nAdvanceSchedulingThresholdMaxDays)
						{
							break;
						}

						if((count == 0)&&(r>365)) // in a year's time we need to find one, as a safety
						{ //haven't found a single allowed day in a year - must be everything accidentally excluded, must break
							
							r = g_pinjector->m_settings.m_nAdvanceSchedulingThresholdMaxDays;
							break;
						}
					}
					nExtentDay = r;

				}
				LeaveCriticalSection(&g_pinjector->m_data.m_critRestrictions);


						


				double dblExtent = (double)((((unsigned long)(pChannelObj->m_dblAutomationTimeEstimate))/86400 + nExtentDay+1)*86400);  // + 1 to include that last day.
//				dblExtent -= ((double)(((unsigned long)(dblExtent))%86400)); // clip off, end on the day boundary. , no do it by counting days, as above.

				
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s end extent calculated at with %.3f, max days %d, real days %d, num restr = %d",
		msgsrc, dblExtent, g_pinjector->m_settings.m_nAdvanceSchedulingThresholdMaxDays, nExtentDay, g_pinjector->m_data.m_nNumRestrictions );
				
				if(dblExtent>dblOnAirTimeLocal) // possible that we have more than the threshold
				{


					if(g_pinjector->m_settings.m_bExtraRestrictionInfo)
					{
						pScheduleDisplay = new CInjectorScheduleObject;
						if(pScheduleDisplay)
						{
							pScheduleDisplay->m_dblOnAirTimeLocal = dblOnAirTimeLocal;//pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal;
							pScheduleDisplay->m_nDurationMS = (unsigned long)((dblExtent - dblOnAirTimeLocal)*1000.0);//pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal;
							pScheduleDisplay->m_pszEventID = (char*)malloc(64);
							if(pScheduleDisplay->m_pszEventID) strcpy(pScheduleDisplay->m_pszEventID, "Restricted");

							// add it!
							while (
											((unsigned long)pScheduleDisplay->m_dblOnAirTimeLocal)/86400
										!=((unsigned long)(pScheduleDisplay->m_dblOnAirTimeLocal + ((double)(pScheduleDisplay->m_nDurationMS)/1000.0)))/86400
										) // we span midnight, try this instead of > 24 hours.

//									while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
							{
								CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
								if(pNew != NULL)
								{													
									pNew->m_dblOnAirTimeLocal = pScheduleDisplay->m_dblOnAirTimeLocal;
//											double dblOnAir = pScheduleDisplay->m_dblOnAirTimeLocal; // no need now + 86400.0; // added 24 hours to it, so it goes over midnight
//											pScheduleDisplay->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
//											pNew->m_nDurationMS = ((int)(pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
//											pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

									pScheduleDisplay->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
									pNew->m_nDurationMS = (int)((pScheduleDisplay->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
									pScheduleDisplay->m_nDurationMS -= pNew->m_nDurationMS;

									if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
										pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;
									if(pNew->m_nDurationMS > 0)
									{
										if(	pScheduleDisplay->m_pszEventID ) 
										{
											pNew->m_pszEventID = (char*)malloc(strlen(pScheduleDisplay->m_pszEventID)+1);
											if(pNew->m_pszEventID) strcpy(pNew->m_pszEventID, pScheduleDisplay->m_pszEventID);
										}

										util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pNew);
										pNew->m_nFileID = pScheduleDisplay->m_nFileID;
									}
									else
									{
												pNew->m_pszCaller = (char*)malloc(64); if(pNew->m_pszCaller) strcpy(pNew->m_pszCaller, "restriction+14");
										// just destroy it, zero or negative duration makes no sense
										try{ delete pNew;} catch(...){}
									}
								}
								else
								{
									// report error
									break;  // just cancel out, the rest will just have to do - it may break the display but still works.
								}
							}
							
							if(pScheduleDisplay->m_nDurationMS > 0)
							{
								util.AddItem(&ppScheduleDisplay, &nNumScheduleDisplays, pScheduleDisplay);
							}
							else
							{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+15");
								// just destroy it, zero or negative duration makes no sense
								try{ delete pScheduleDisplay;} catch(...){}
							}
							
						}
					}

					pScheduleRestriction = new CInjectorScheduleObject;
					if(pScheduleRestriction != NULL)
					{
						// because, start time set already above
						pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAirTimeLocal;//pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal;
						pScheduleRestriction->m_nDurationMS = (unsigned long)((dblExtent - dblOnAirTimeLocal)*1000.0);//pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal;
						pScheduleRestriction->m_nFileID = -1; // nuttin
						//dblOnAirTimeLocal = pChannelObj->m_ppTrafficEvents[i]->m_dblOnAirTimeLocal+ ((double)(pChannelObj->m_ppTrafficEvents[i]->m_nDurationMS))/1000.0;

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s end extent item created with %.3f, %d",
		msgsrc, pScheduleRestriction->m_dblOnAirTimeLocal, pScheduleRestriction->m_nDurationMS);
						

						while (
										((unsigned long)pScheduleRestriction->m_dblOnAirTimeLocal)/86400
									!=((unsigned long)(pScheduleRestriction->m_dblOnAirTimeLocal + ((double)(pScheduleRestriction->m_nDurationMS)/1000.0)))/86400
									) // we span midnight, try this instead of > 24 hours.

	//					while(pScheduleRestriction->m_nDurationMS>86400000) // more than 24 hours is a prob, we need to break it up.
						{
							CInjectorScheduleObject* pNew = new CInjectorScheduleObject;
							if(pNew != NULL)
							{													
								pNew->m_dblOnAirTimeLocal = pScheduleRestriction->m_dblOnAirTimeLocal;
//								double dblOnAir = pScheduleRestriction->m_dblOnAirTimeLocal + 86400.0; // added 24 hours to it, so it goes over midnight
//								pScheduleRestriction->m_dblOnAirTimeLocal = dblOnAir - (double)(((int)dblOnAir)%86400); // subtract the part over midnight
//								pNew->m_nDurationMS = ((int)(pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal))*1000; // time before the next midnight;
//								pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;

								pScheduleRestriction->m_dblOnAirTimeLocal = (double)((((unsigned long)(pNew->m_dblOnAirTimeLocal))/86400 + 1)*86400); // next day.
								pNew->m_nDurationMS = (int)((pScheduleRestriction->m_dblOnAirTimeLocal - pNew->m_dblOnAirTimeLocal)*1000.0);
								pScheduleRestriction->m_nDurationMS -= pNew->m_nDurationMS;

								if(g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS>0)
									pNew->m_nDurationMS -= g_pinjector->m_settings.m_nProgramAutomationTrimMidnightMS;
								if(pNew->m_nDurationMS > 0)
								{

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s end extent item added with %.3f, %d",
		msgsrc, pNew->m_dblOnAirTimeLocal, pNew->m_nDurationMS);
									util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pNew); // add this guy, then go around...

									pNew->m_nFileID = pScheduleRestriction->m_nFileID;
								}
								else
								{
												pNew->m_pszCaller = (char*)malloc(64); if(pNew->m_pszCaller) strcpy(pNew->m_pszCaller, "restriction+15");
									// just destroy it, zero or negative duration makes no sense
									try{ delete pNew;} catch(...){}
								}
							}
							else
							{
								// report error
								break;  // just cancel out, the rest will just have to do - it may break the display but still works.
							}
						}

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s end extent item added with %.3f, %d",
		msgsrc, pScheduleRestriction->m_dblOnAirTimeLocal, pScheduleRestriction->m_nDurationMS);
						if(pScheduleRestriction->m_nDurationMS > 0)
						{
							util.AddItem(&ppScheduleRestrictions, &nNumScheduleRestrictions, pScheduleRestriction);
						}
						else
						{
										pScheduleDisplay->m_pszCaller = (char*)malloc(64); if(pScheduleDisplay->m_pszCaller) strcpy(pScheduleDisplay->m_pszCaller, "restriction_D+16");
							// just destroy it, zero or negative duration makes no sense
							try{ delete pScheduleRestriction;} catch(...){}
						}
						pScheduleRestriction = NULL; // reset after added to stack
					}
					else
					{
						// report error
					}
				}

				// now, need to go thru and do the compare.

				CInjectorScheduleObject*** pppEvents;
				int* ppnNumEvents;
				CRITICAL_SECTION* pcritEvents;

				CInjectorScheduleObject*** pppNewEvents;
				int* ppnNumNewEvents;

	
				if(g_pinjector->m_settings.m_bExtraRestrictionInfo)
				{
					pppEvents = &pChannelObj->m_ppDisplayEvents;
					ppnNumEvents = &pChannelObj->m_nNumDisplayEvents;
					pcritEvents = &pChannelObj->m_critDisplayEvents;
					pppNewEvents = &ppScheduleDisplay;
					ppnNumNewEvents = &nNumScheduleDisplays;



					// go thru and "trim" events if they overlap each other within the schedule increment;
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s trimming %d items", 
	msgsrc, nNumScheduleDisplays);  //(Dispatch message)

					if((nNumScheduleDisplays>0)&&(ppScheduleDisplay))
					{
						int t=0;
						while(t<nNumScheduleDisplays-1)
						{
							if((ppScheduleDisplay[t])&&(ppScheduleDisplay[t+1]))
							{
								double dblEnd = ppScheduleDisplay[t]->m_dblOnAirTimeLocal + ((double)(ppScheduleDisplay[t]->m_nDurationMS))/1000.0;
						
if(0)//g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s trimming %d events, check %.3f vs %.3f", 
	msgsrc, nNumScheduleDisplays, dblEnd, ppScheduleDisplay[t+1]->m_dblOnAirTimeLocal);  //(Dispatch message)
								int nInc = (((int)(dblEnd))/(g_pinjector->m_settings.m_nSchedulingIncrementMins*60))*(g_pinjector->m_settings.m_nSchedulingIncrementMins*60);
								int nNext = (((int)(ppScheduleDisplay[t+1]->m_dblOnAirTimeLocal))/(g_pinjector->m_settings.m_nSchedulingIncrementMins*60))*(g_pinjector->m_settings.m_nSchedulingIncrementMins*60);

if(0)//g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s trimming %d events, inc %d >=? %d next", 
	msgsrc, nNumScheduleDisplays, nInc, nNext);  //(Dispatch message)
								if( nInc >= nNext)
								{
									// we overlap in a cell
									// trim the existing event.

									dblEnd = (double)(nNext) - ppScheduleDisplay[t]->m_dblOnAirTimeLocal;
if(0)//g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s trimming event to %.3f sec (if> %.3f) ", 
	msgsrc, dblEnd, ((double)(g_pinjector->m_settings.m_nSchedulingIncrementMins*60)));  //(Dispatch message)
									if(dblEnd >= ((double)(g_pinjector->m_settings.m_nSchedulingIncrementMins*60)))
									{
										//trim
										ppScheduleDisplay[t]->m_nDurationMS = ((int)(dblEnd*1000.0));
									}
								}
							}
							t++;
						}

					}



				}
				else
				{
					pppEvents = &pChannelObj->m_ppScheduleRestrictions;
					ppnNumEvents = &pChannelObj->m_nNumScheduleRestrictions;
					pcritEvents = &pChannelObj->m_critScheduleRestrictions;
					pppNewEvents = &ppScheduleRestrictions;
					ppnNumNewEvents = &nNumScheduleRestrictions;
				}


				if(((*pppNewEvents))&&((*ppnNumNewEvents)>0))
				{


					// go thru the existing restrictions
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s comparing %d restrictions with %d items on %d", 
	msgsrc, (*ppnNumNewEvents), (*ppnNumEvents), ((*pppEvents)));  //(Dispatch message)

					EnterCriticalSection(pcritEvents);
					if(((*pppEvents))&&((*ppnNumEvents)>0))
					{

						if(g_pinjector->m_settings.m_bCompareRestrictions)
						{

							// have to compare.
							int i=0;

							while(i<(*ppnNumEvents))
							{
								if((*pppEvents)[i])
								{
									(*pppEvents)[i]->m_ulFlags &= ~INJECTOR_FLAG_FOUND; // just be sure.
								}
								i++;
							}

							i=0;
							while(i<(*ppnNumNewEvents))
							{
								if((*pppNewEvents)[i])
								{
									int nRV=-1;
									if(g_pinjector->m_settings.m_bExtraRestrictionInfo)
									{
	if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
		g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Programming", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_SCHEDULE_DISPLAY", msgsrc, (*pppNewEvents)[i]->m_dblOnAirTimeLocal);
										nRV = pChannelObj->FindScheduleItem((*pppNewEvents)[i]->m_dblOnAirTimeLocal, INJECTOR_ARRAY_SCHEDULE_DISPLAY, INJECTOR_ARRAY_FIND_NOTFOUND);
	if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
		g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Programming", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_SCHEDULE_DISPLAY returned %d", msgsrc, (*pppNewEvents)[i]->m_dblOnAirTimeLocal, nRV);
									}
									else
									{
	if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
		g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Programming", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_SCHEDULE_RESTRICTION", msgsrc, (*pppNewEvents)[i]->m_dblOnAirTimeLocal);
										nRV = pChannelObj->FindScheduleItem((*pppNewEvents)[i]->m_dblOnAirTimeLocal, INJECTOR_ARRAY_SCHEDULE_RESTRICTION, INJECTOR_ARRAY_FIND_NOTFOUND);
	if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
		g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Programming", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_SCHEDULE_RESTRICTION returned %d", msgsrc, (*pppNewEvents)[i]->m_dblOnAirTimeLocal, nRV);
									}
									if(nRV>=0)
									{
										// check if identical
										if(
												((*pppNewEvents)[i]->m_dblOnAirTimeLocal >= (*pppEvents)[nRV]->m_dblOnAirTimeLocal - (double)(g_pinjector->m_settings.m_ulAutomationTimeToleranceMS)/1000.0)
											&&((*pppNewEvents)[i]->m_dblOnAirTimeLocal <= (*pppEvents)[nRV]->m_dblOnAirTimeLocal + (double)(g_pinjector->m_settings.m_ulAutomationTimeToleranceMS)/1000.0)
											&&((*pppNewEvents)[i]->m_nDurationMS == (*pppEvents)[nRV]->m_nDurationMS)
											&&(
													(!g_pinjector->m_settings.m_bExtraRestrictionInfo)
												||(
														(
													
																((*pppNewEvents)[i]->m_pszEventID)
															&&((*pppEvents)[nRV]->m_pszEventID)
															&&(strcmp((*pppNewEvents)[i]->m_pszEventID,(*pppEvents)[nRV]->m_pszEventID)==0)
														)
	/*  // we don't actually insert the title, so just go by ID.
													&&(
																((*pppNewEvents)[i]->m_pszEventTitle)
															&&((*pppEvents)[nRV]->m_pszEventTitle)
															&&(strcmp((*pppNewEvents)[i]->m_pszEventTitle,(*pppEvents)[nRV]->m_pszEventTitle)==0)
														)
	*/
													)
												)
											)
										{
											// great! nothing to do except flag this as done, and assign the itemid
											(*pppEvents)[nRV]->m_ulFlags |= INJECTOR_FLAG_FOUND;
	if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
		g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s restriction %d exists (%.3f, %d)", 
		msgsrc, i, (*pppNewEvents)[i]->m_dblOnAirTimeLocal, (*pppNewEvents)[i]->m_nDurationMS);  //(Dispatch message)
											(*pppNewEvents)[i]->m_nID = (*pppEvents)[nRV]->m_nID;  // must assign
										}
										else
										{
											// ooo different, must update.
											char* pchID = db.EncodeQuotes((*pppNewEvents)[i]->m_pszEventID);
											char* pchTitle = db.EncodeQuotes((*pppNewEvents)[i]->m_pszEventTitle);


											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET scheduled_on = %.3f, duration = %d, clipid = '%s', clip_title = '%s' \
WHERE itemid = %d AND created_by = 'sys'", // HARDCODE sys for safety
													((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
													(*pppNewEvents)[i]->m_dblOnAirTimeLocal,
													(*pppNewEvents)[i]->m_nDurationMS,
													pchID?pchID:"",
													pchTitle?pchTitle:"",
													(*pppEvents)[nRV]->m_nID
												);

											if(pchID)	{ try{ free(pchID);} catch(...){}}
											if(pchTitle)	{ try{ free(pchTitle);} catch(...){}}

	if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
		g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s updating restriction %d (%.3f, %d)\r\nSQL: %s", 
		msgsrc, i, 
		(*pppNewEvents)[i]->m_dblOnAirTimeLocal, 
		(*pppNewEvents)[i]->m_nDurationMS, szSQL);  //(Dispatch message)
											if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
											{
												//error
				g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "InjectorScheduleRestrictionThread", "%s error updating restriction: %s", 
					msgsrc, 
					errorstring);  //(Dispatch message)
											}

											(*pppNewEvents)[i]->m_nID = (*pppEvents)[nRV]->m_nID;  // must assign
											(*pppEvents)[nRV]->m_ulFlags |= INJECTOR_FLAG_FOUND;

	// there was no reason to do any of the following stuff.
	// all we need to do is set the found flag, so we can mod the DB on the original array at the end of this, and then swap arrays, so all this woul dget deleted anyway.
	/*										
											// have to modify the times to reflect the new settings, and to ensure that this item is not found again by a separate item meeting the same criteria
											(*pppEvents)[nRV]->m_dblOnAirTimeLocal =	(*pppNewEvents)[i]->m_dblOnAirTimeLocal;
											(*pppEvents)[nRV]->m_nDurationMS =	(*pppNewEvents)[i]->m_nDurationMS;

											if((*pppEvents)[nRV]->m_pszEventID)
											{
												try
												{
													free((*pppEvents)[nRV]->m_pszEventID);
												}
												catch(...)
												{}
												(*pppEvents)[nRV]->m_pszEventID = NULL;
											}
											if((*pppNewEvents)[i]->m_pszEventID)
											{
												(*pppEvents)[nRV]->m_pszEventID = (char*)malloc(strlen((*pppNewEvents)[i]->m_pszEventID)+1);
												if((*pppEvents)[nRV]->m_pszEventID) strcpy((*pppEvents)[nRV]->m_pszEventID, (*pppNewEvents)[i]->m_pszEventID);
											}

											if((*pppEvents)[nRV]->m_pszEventTitle)
											{
												try
												{
													free((*pppEvents)[nRV]->m_pszEventTitle);
												}
												catch(...)
												{}
												(*pppEvents)[nRV]->m_pszEventTitle = NULL;
											}
											if((*pppNewEvents)[i]->m_pszEventTitle)
											{
												(*pppEvents)[nRV]->m_pszEventTitle = (char*)malloc(strlen((*pppNewEvents)[i]->m_pszEventTitle)+1);
												if((*pppEvents)[nRV]->m_pszEventTitle) strcpy((*pppEvents)[nRV]->m_pszEventTitle, (*pppNewEvents)[i]->m_pszEventTitle);
											}

		*/


										}
									}
									else // not found, must add.
									{
										int nAutoTime = (int)pChannelObj->m_dblAutomationTimeEstimate;

										char* pchID = db.EncodeQuotes((*pppNewEvents)[i]->m_pszEventID);
										char* pchTitle = db.EncodeQuotes((*pppNewEvents)[i]->m_pszEventTitle);
		

										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (channelid, clipid, segment, scheduled_on, duration, clip_title, notes, status, created_on, created_by, traffic_file_id) \
VALUES (%d, '%s', -1, %.3f, %d, '%s', 'Restricted', 0, %d, 'sys', %d)", // HARDCODE
												((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
												pChannelObj->m_nChannelID,
												pchID?pchID:"",
												(*pppNewEvents)[i]->m_dblOnAirTimeLocal,
												(*pppNewEvents)[i]->m_nDurationMS,
												pchTitle?pchTitle:"",
												nAutoTime, // local time....
												(*pppNewEvents)[i]->m_nFileID
											);

										if(pchID)	{ try{ free(pchID);} catch(...){}}
										if(pchTitle)	{ try{ free(pchTitle);} catch(...){}}
											

	if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
		g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s adding restriction %d (%.3f, %d)\r\nSQL: %s", 
		msgsrc, i, (*pppNewEvents)[i]->m_dblOnAirTimeLocal, (*pppNewEvents)[i]->m_nDurationMS, szSQL);  //(Dispatch message)
										if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
										{
											//error
			g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "InjectorScheduleRestrictionThread", "%s error inserting restriction: %s", 
				msgsrc, 
				errorstring);  //(Dispatch message)
										}

										// then must obtain back the inserted ID.

										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
											"SELECT TOP 1 itemid FROM %s WHERE scheduled_on = %.3f AND duration = %d AND traffic_file_id = %d AND created_on = %d AND \
	channelid = %d AND created_by = 'sys' ORDER BY itemid DESC",  // sys only!, get the most recent if copies.
											((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
												(*pppNewEvents)[i]->m_dblOnAirTimeLocal,
												(*pppNewEvents)[i]->m_nDurationMS,
												(*pppNewEvents)[i]->m_nFileID,
												nAutoTime,
												pChannelObj->m_nChannelID
											);

										//that should be specific enough
	if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item id", "%s SQL [%s]", msgsrc, szSQL);  //(Dispatch message)

										int nIndex=-1;
										CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
										if(prs != NULL) // no error
										{
											if((!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
											{

												bool bError = false;
												try
												{
													CString szTemp;
													prs->GetFieldValue("itemid", szTemp);
													nIndex = atoi(szTemp);

	if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
	{
		g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "%s got item ID: %d", msgsrc, 
									nIndex		);  //(Dispatch message)
	}

												}
												catch(CException *e)// CDBException *e, CMemoryException *m)  
												{
													bError = true;
													if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
													{
														g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
													}
													else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
													{
														g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Retrieve: Caught exception: out of memory.\n%s", szSQL);
								//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
													}
													else 
													{
														g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Retrieve: Caught other exception.\n%s", szSQL);
								//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
													}
													e->Delete();

												} 
												catch( ... )
												{
													bError = true;
													g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Retrieve: Caught exception.\n%s", szSQL);
												}

												if((!bError)&&(nIndex>0))
												{
													(*pppNewEvents)[i]->m_nID =  nIndex;
												}
												else
												{
											g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Could not assign item id, index = %d, error = %d",nIndex,bError);
												}
											}
							//				prs->MoveNext();
											
											prs->Close();

											try{ delete prs;} catch(...){}
										}
										else
										{
										//	prs was null
											g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Retrieve returned a NULL recordset.\n%s", szSQL);
										}
									}
								}
								i++;
							}


		//					i think i have to reconcile the two buffer here.
							//remove any that were not found
							i=0;
							while(i<(*ppnNumEvents))
							{
	if(0)//(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
		g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s deletion check: restriction %d (%d)", 
		msgsrc, i, (*pppEvents)[i]
		);  //(Dispatch message)
								if((*pppEvents)[i])
								{
	if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
		g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s deletion check: restriction %d (uid %d) %.3f, %d, flags 0x%08x", 
		msgsrc, i, (*pppEvents)[i]->m_nID, 
		(*pppEvents)[i]->m_dblOnAirTimeLocal, (*pppEvents)[i]->m_nDurationMS, (*pppEvents)[i]->m_ulFlags
		);  //(Dispatch message)
									if(!((*pppEvents)[i]->m_ulFlags&INJECTOR_FLAG_FOUND))
									{
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s where itemid = %d AND channelid=%d AND created_by = 'sys'", // HARDCODE
											((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
											(*pppEvents)[i]->m_nID,
											pChannelObj->m_nChannelID
											);

	if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
		g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s deleting restriction %d (%.3f, %d)\r\nSQL: %s", 
		msgsrc, i, (*pppEvents)[i]->m_dblOnAirTimeLocal, 
		(*pppEvents)[i]->m_nDurationMS, szSQL);  //(Dispatch message)
										if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
										{
											//error
			g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "InjectorScheduleRestrictionThread", "%s error deleting restriction: %s", 
				msgsrc, 
				errorstring);  //(Dispatch message)
										}
									}
									
									(*pppEvents)[i]->m_pszCaller = (char*)malloc(64); if((*pppEvents)[i]->m_pszCaller) strcpy((*pppEvents)[i]->m_pszCaller, "restriction_deletion");

									try {delete (*pppEvents)[i];} catch(...){}
								}
								i++;
							}
							try {delete [] (*pppEvents);} catch(...){}

							//assign
							(*ppnNumEvents) = (*ppnNumNewEvents);
							(*pppEvents) = (*pppNewEvents);
						}
						else 
						{ // replace everything:
							// first set the found flag on everything with sys
							// then insert all the new ones with no found flag
							// then delete all the ones with the found flag

							int i=0;

							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = case when status is NULL then %d else (status | %d) end WHERE created_by = 'sys'", // HARDCODE sys for safety
									((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
									INJECTOR_FLAG_FOUND, INJECTOR_FLAG_FOUND
								);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s updating restriction table. SQL: %s", msgsrc, szSQL);  //(Dispatch message)
							if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
							{
								//error
g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "InjectorScheduleRestrictionThread", "%s error updating restriction table: %s", 
	msgsrc, 
	errorstring);  //(Dispatch message)
							}
							else
							{
								// now add everything
								i=0;
								while(i<(*ppnNumNewEvents))
								{
									if((*pppNewEvents)[i])
									{
										// add to db.
		/*
				create table Schedule (itemid int identity(1,1), 
				channelid int, clipid varchar(64), segment int, 
				scheduled_on decimal(20,3), duration int, clip_title varchar(256), 
				notes varchar(4096), status int, created_on int, 
				created_by varchar(32), traffic_file_id int);
		*/	
										int nAutoTime = (int)pChannelObj->m_dblAutomationTimeEstimate;

										char* pchID = db.EncodeQuotes((*pppNewEvents)[i]->m_pszEventID);
										char* pchTitle = db.EncodeQuotes((*pppNewEvents)[i]->m_pszEventTitle);
		

										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (channelid, clipid, segment, scheduled_on, duration, clip_title, notes, status, created_on, created_by, traffic_file_id) \
VALUES (%d, '%s', -1, %.3f, %d, '%s', 'Restricted', 0, %d, 'sys', %d)", // HARDCODE
												((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
												pChannelObj->m_nChannelID,
												pchID?pchID:"",
												(*pppNewEvents)[i]->m_dblOnAirTimeLocal,
												(*pppNewEvents)[i]->m_nDurationMS,
												pchTitle?pchTitle:"",
												nAutoTime, // local time....
												(*pppNewEvents)[i]->m_nFileID
											);

										if(pchID)	{ try{ free(pchID);} catch(...){}}
										if(pchTitle)	{ try{ free(pchTitle);} catch(...){}}

		if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s inserting restriction %d (%.3f, %d)\r\nSQL: %s", 
			msgsrc, i, (*pppNewEvents)[i]->m_dblOnAirTimeLocal, (*pppNewEvents)[i]->m_nDurationMS, szSQL);  //(Dispatch message)
										if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
										{
											//error
			g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "InjectorScheduleRestrictionThread", "%s error inserting restriction: %s", 
				msgsrc, 
				errorstring);  //(Dispatch message)
										}


											// then must obtain back the inserted ID.

											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
												"SELECT TOP 1 itemid FROM %s WHERE scheduled_on = %.3f AND duration = %d AND traffic_file_id = %d AND created_on = %d AND \
channelid = %d AND created_by = 'sys' AND status='0' ORDER BY itemid DESC",  // sys only!, get the most recent if copies.
												((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
													(*pppNewEvents)[i]->m_dblOnAirTimeLocal,
													(*pppNewEvents)[i]->m_nDurationMS,
													(*pppNewEvents)[i]->m_nFileID,
													nAutoTime,
													pChannelObj->m_nChannelID
												);

											//that should be specific enough
		if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
		g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item id", "%s SQL [%s]", msgsrc, szSQL);  //(Dispatch message)

											int nIndex=-1;
											CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
											if(prs != NULL) // no error
											{
												if((!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
												{

													bool bError = false;
													try
													{
														CString szTemp;
														prs->GetFieldValue("itemid", szTemp);
														nIndex = atoi(szTemp);

		if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
		{
			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "%s got item ID: %d", msgsrc, 
										nIndex		);  //(Dispatch message)
		}

													}
													catch(CException *e)// CDBException *e, CMemoryException *m)  
													{
														bError = true;
														if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
														{
															g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
														}
														else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
														{
															g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Retrieve: Caught exception: out of memory.\n%s", szSQL);
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
														}
														else 
														{
															g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Retrieve: Caught other exception.\n%s", szSQL);
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
														}
														e->Delete();

													} 
													catch( ... )
													{
														bError = true;
														g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Retrieve: Caught exception.\n%s", szSQL);
													}

													if((!bError)&&(nIndex>0))
													{
														(*pppNewEvents)[i]->m_nID =  nIndex;
													}
													else
													{
												g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Could not assign item id, index = %d, error = %d",nIndex,bError);
													}
												}
								//				prs->MoveNext();
												
												prs->Close();

												try { delete prs;} catch(...){}
											}
											else
											{
											//	prs was null
												g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Retrieve returned a NULL recordset.\n%s", szSQL);
											}

									}
									i++;
								}


								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE (status & %d) = %d", // HARDCODE sys for safety
										((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
										INJECTOR_FLAG_FOUND, INJECTOR_FLAG_FOUND
									);

	if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s deleting from restriction table. SQL: %s", msgsrc, szSQL);  //(Dispatch message)
								if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
								{
									//error
	g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "InjectorScheduleRestrictionThread", "%s error updating restriction table: %s", 
		msgsrc, 
		errorstring);  //(Dispatch message)
								}


							}


							i=0;
							while(i<(*ppnNumEvents))
							{
								if((*pppEvents)[i])
								{
									try {delete (*pppEvents)[i];} catch(...){}
								}
								i++;
							}
							try {delete [] (*pppEvents);} catch(...){}

							//assign
							(*ppnNumEvents) = (*ppnNumNewEvents);
							(*pppEvents) = (*pppNewEvents);

						}

					}
					else  // there weren't any, so just add everything to the db now.
					{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s nothing to compare to, so inserting.", 
	msgsrc);  //(Dispatch message)

						int i=0;
						while(i<(*ppnNumNewEvents))
						{
							if((*pppNewEvents)[i])
							{
								// add to db.
/*
		create table Schedule (itemid int identity(1,1), 
		channelid int, clipid varchar(64), segment int, 
		scheduled_on decimal(20,3), duration int, clip_title varchar(256), 
		notes varchar(4096), status int, created_on int, 
		created_by varchar(32), traffic_file_id int);
*/	
								int nAutoTime = (int)pChannelObj->m_dblAutomationTimeEstimate;

								
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (channelid, clipid, segment, scheduled_on, duration, clip_title, notes, status, created_on, created_by, traffic_file_id) \
VALUES (%d, '', -1, %.3f, %d, '', 'Restricted', 0, %d, 'sys', %d)", // HARDCODE
										((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
										pChannelObj->m_nChannelID,
										(*pppNewEvents)[i]->m_dblOnAirTimeLocal,
										(*pppNewEvents)[i]->m_nDurationMS,
										nAutoTime, // local time....
										(*pppNewEvents)[i]->m_nFileID
									);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s inserting restriction %d (%.3f, %d)\r\nSQL: %s", 
	msgsrc, i, (*pppNewEvents)[i]->m_dblOnAirTimeLocal, (*pppNewEvents)[i]->m_nDurationMS, szSQL);  //(Dispatch message)
								if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
								{
									//error
	g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "InjectorScheduleRestrictionThread", "%s error inserting restriction: %s", 
		msgsrc, 
		errorstring);  //(Dispatch message)
								}


									// then must obtain back the inserted ID.

									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
										"SELECT TOP 1 itemid FROM %s WHERE scheduled_on = %.3f AND duration = %d AND traffic_file_id = %d AND created_on = %d AND \
channelid = %d AND created_by = 'sys' ORDER BY itemid DESC",  // sys only!, get the most recent if copies.
										((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
											(*pppNewEvents)[i]->m_dblOnAirTimeLocal,
											(*pppNewEvents)[i]->m_nDurationMS,
											(*pppNewEvents)[i]->m_nFileID,
											nAutoTime,
											pChannelObj->m_nChannelID
										);

									//that should be specific enough
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item id", "%s SQL [%s]", msgsrc, szSQL);  //(Dispatch message)

									int nIndex=-1;
									CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
									if(prs != NULL) // no error
									{
										if((!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
										{

											bool bError = false;
											try
											{
												CString szTemp;
												prs->GetFieldValue("itemid", szTemp);
												nIndex = atoi(szTemp);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
{
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "%s got item ID: %d", msgsrc, 
								nIndex		);  //(Dispatch message)
}

											}
											catch(CException *e)// CDBException *e, CMemoryException *m)  
											{
												bError = true;
												if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
												{
													g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
												}
												else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
												{
													g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Retrieve: Caught exception: out of memory.\n%s", szSQL);
							//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
												}
												else 
												{
													g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Retrieve: Caught other exception.\n%s", szSQL);
							//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
												}
												e->Delete();

											} 
											catch( ... )
											{
												bError = true;
												g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Retrieve: Caught exception.\n%s", szSQL);
											}

											if((!bError)&&(nIndex>0))
											{
												(*pppNewEvents)[i]->m_nID =  nIndex;
											}
											else
											{
										g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Could not assign item id, index = %d, error = %d",nIndex,bError);
											}
										}
						//				prs->MoveNext();
										
										prs->Close();

										try { delete prs;} catch(...){}
									}
									else
									{
									//	prs was null
										g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get inserted restriction item ID", "Retrieve returned a NULL recordset.\n%s", szSQL);
									}

							}
							i++;
						}
						if((*pppEvents)) {try {delete [] (*pppEvents);} catch(...){}}
					}

					//assign
					(*ppnNumEvents) = (*ppnNumNewEvents);
					(*pppEvents) = (*pppNewEvents);

					LeaveCriticalSection(pcritEvents);


				}
				else // no restrictions!  hmmm what if this was due to error?  ok, check later TODO
				{
					//just remove everything.

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE channelid = %d AND created_by = 'sys'", // sys only, and can ignore old stuff.
						((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
						pChannelObj->m_nChannelID
						);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_SCHEDULE)||(g_pinjector->m_settings.m_bDebugSQL))
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule restrictions", "%s deleting all restrictions\r\nSQL: %s", 
	msgsrc, szSQL);  //(Dispatch message)
					if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						//error
	g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "InjectorScheduleRestrictionThread", "%s error deleting all restriction: %s", 
		msgsrc, 
		errorstring);  //(Dispatch message)
					}

						// let's clear our memory.
					EnterCriticalSection(pcritEvents);
					
					if(((*ppnNumEvents)>0)&&((*pppEvents)))
					{
						int i=0;
						while(i<(*ppnNumEvents))
						{
							if((*pppEvents)[i])
							{
								(*pppEvents)[i]->m_pszCaller = (char*)malloc(64); if((*pppEvents)[i]->m_pszCaller) strcpy((*pppEvents)[i]->m_pszCaller, "restriction_deletion_CM");
								try {delete (*pppEvents)[i];} catch(...){}
							}
							i++;
						}
						try {delete [] (*pppEvents);} catch(...){}
					}
					(*ppnNumEvents) = 0;
					(*pppEvents) = NULL;

					LeaveCriticalSection(pcritEvents);

					if((*pppNewEvents)) {try {delete [] (*pppNewEvents);} catch(...){}}

				}

				if(bLiveEventsChanged)
				{
					pChannelObj->m_bLiveEventsScheduleChanged = true; // signal programming thread: let's let the new restrictions be calculated first, 
					// then let that thread signal the scheduler.
					//  don't need to signal the scheduler if a traffic file changes, the scheduler only operates on the region of time with the loaded live list.
				}

			}
			else 	LeaveCriticalSection(&pChannelObj->m_critAdvisories);


		}
		Sleep(30); // do not peg
		pChannelObj->m_ulFlags |= INJECTOR_FLAG_RESTRICTINIT;
	}	


	while(pChannelObj->m_bProgrammingThreadStarted) Sleep(10);  // TODO put a timeout on this.


	db.RemoveConnection(pdbConn);


	Sleep(30);
	EnterCriticalSection(&pChannelObj->m_critServerInfo);
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending schedule restriction thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);
	g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "InjectorScheduleRestrictionThread", errorstring);    //(Dispatch message)
	pChannelObj->m_bSchedulerThreadStarted=false;
	
	_endthread();
//	Sleep(150);

}

void InjectorProgrammingThread(void* pvArgs)
{
#pragma message(alert "this is an alert")

	CInjectorChannelObject* pChannelObj = (CInjectorChannelObject*) pvArgs;
	if(pChannelObj==NULL) return;

	pChannelObj->m_bProgrammingThreadStarted=true;
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	char msgsrc[MAX_MESSAGE_LENGTH];

	EnterCriticalSection(&pChannelObj->m_critServerInfo);
	_snprintf(msgsrc, MAX_MESSAGE_LENGTH-1, "Injector_programming (%s:%d)", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning programming thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);
	g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "InjectorProgrammingThread", errorstring);    //(Dispatch message)


	// settings
	char szControlHost[MAX_MESSAGE_LENGTH];
	unsigned short usControlPort=0;
	char* pszSettingInsertionPrimaryFile = NULL;
	char* pszSettingAllowPrimaryFile = NULL;
	char* pszSettingInsertionSecondariesFile = NULL;

	char* pszPrimaryEventBuffer = NULL;
	char* pszAllowEventBuffer = NULL;
	char* pszSecondaryEventsBuffer = NULL;
	int nNumSecondaries = 0;
	int nSecBufLen = 0;
	int nAllowBufLen = 0;
	int nPriBufLen = 0;

	strcpy(errorstring, "");
	strcpy(szControlHost, "");
	CDBUtil db;
	CInjectorUtilObject util;


	EnterCriticalSection(&pChannelObj->m_critServerInfo);
	CDBconn* pdbConn = db.CreateNewConnection(g_pinjector->m_settings.m_pszDSN, g_pinjector->m_settings.m_pszUser, g_pinjector->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			g_pinjector->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Injector:programming_database_connect", errorstring);  //(Dispatch message)
			pdbConn = g_pinjector->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", g_pinjector->m_settings.m_pszDSN, g_pinjector->m_settings.m_pszUser, g_pinjector->m_settings.m_pszPW); 
		g_pinjector->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Injector:programming_database_init", errorstring);  //(Dispatch message)
		pdbConn = g_pinjector->m_data.m_pdbConn;

		//**MSG
	}
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);

	// have to also have a connection to Sentinel

	//char* m_pszSentinelHost;  // name of the server on which sentinel is running
	//unsigned short m_usSentinelPort;  // port on which Sentinel is listening.



	bool bChangesPending = false;
	_timeb timebPending;
	_timeb timebChange;
	_ftime(&timebChange);

	double dblPending =0.0;
	double dblChange =0.0;
	double dblNow =0.0;
	double dblLastDisconnect =0.0;
	double dblLastAttempt =0.0;
	double dblLastSuppressReport = -1.0;


/*
		create table Schedule (itemid int identity(1,1), 
		channelid int, clipid varchar(64), segment int, 
		scheduled_on decimal(20,3), duration int, clip_title varchar(256), 
		notes varchar(4096), status int, created_on int, 
		created_by varchar(32), traffic_file_id int);
*/	
		//(duration+1000) is to be safe on any rounding error that might occur.

		// let's clear our memory.
	EnterCriticalSection(&pChannelObj->m_critScheduleEvents);
	
	if((pChannelObj->m_nNumScheduleEvents>0)&&(pChannelObj->m_ppScheduleEvents))
	{
		int i=0;
		while(i<pChannelObj->m_nNumScheduleEvents)
		{
			if(pChannelObj->m_ppScheduleEvents[i])
			{
				try {delete pChannelObj->m_ppScheduleEvents[i];} catch(...){}
			}
			i++;
		}
		try {delete [] pChannelObj->m_ppScheduleEvents;} catch(...){}
	}
	pChannelObj->m_nNumScheduleEvents = 0;
	pChannelObj->m_ppScheduleEvents = NULL;

	LeaveCriticalSection(&pChannelObj->m_critScheduleEvents);

	int nScheduleMod = -1;
	int nLastScheduleMod=-1;

	CInjectorScheduleObject** ppLastSchedule=NULL;
	int nNumLastScheduleEvents=0;
	bool bScheduleChanged = false;
	bool bScheduleChangeProcessed = true;
	bool bLiveEventsChanged = false;
	bool bSocketErrorReported = false;

	while((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)
		&&(!(pChannelObj->m_ulFlags&INJECTOR_FLAG_AUTOINIT))
		&&(!(pChannelObj->m_ulFlags&INJECTOR_FLAG_RESTRICTINIT))
		&&((pChannelObj->m_dblServerLastUpdate<0.0)||(pChannelObj->m_dblAutomationTimeEstimate<0.0)))
	{
		Sleep(100);
	}

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s: thread release", msgsrc);
	g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "InjectorProgrammingThread", errorstring);    //(Dispatch message)


	while((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
	{
		_ftime( &pChannelObj->m_timebProgrammingTick);  // it's here right now...  maybe should be inside if if schedulermode

		dblNow = ((double)(pChannelObj->m_timebProgrammingTick.time)) + ((double)(pChannelObj->m_timebProgrammingTick.millitm))/1000.0;

		if((g_pinjector->m_settings.m_bManualInjectorMode)||(g_pinjector->m_settings.m_bAutoSchedulerMode))
		{
			//make sure we are connected to Sentinel

			if(pChannelObj->m_socketSentinel == NULL)
			{
				// connect.
				if(	dblNow > dblLastAttempt+((double)(g_pinjector->m_settings.m_ulSentinelRetryIntervalMS))/1000.0)
				{
					_ftime(&pChannelObj->m_timebProgrammingTick);
					dblLastAttempt = dblNow;
					if((g_pinjector->m_settings.m_pszSentinelHost==NULL)||(strlen(g_pinjector->m_settings.m_pszSentinelHost)<=0))
					{
						if(!(bSocketErrorReported))
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid control host specified"); 
							g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, msgsrc, errorstring);  //(Dispatch message)
							g_pinjector->SendMsg(CX_SENDMSG_ERROR, msgsrc, errorstring);
							// set status
							//TODO
							bSocketErrorReported = true;
						}
						pChannelObj->m_socketSentinel = NULL; // insurance
						strcpy(szControlHost, "");
						usControlPort = 0;

						Sleep(100);
						continue; // can't do anything else if not connected
					} 
					//start network client
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing control client to %s:%d", 
						g_pinjector->m_settings.m_pszSentinelHost, g_pinjector->m_settings.m_usSentinelPort); 
					g_pinjector->m_msgr.DM(MSG_ICONNONE, NULL, msgsrc, errorstring);  //(Dispatch message)

					if((pChannelObj->m_socketSentinel == NULL)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
					{
						char info[8192];
				EnterCriticalSection(&pChannelObj->m_critSentinelSocket);
						g_pinjector->m_msgr.DM(MSG_ICONNONE, NULL, msgsrc, "OpenConnection" );  //(Dispatch message)
						if(pChannelObj->m_net.OpenConnection(g_pinjector->m_settings.m_pszSentinelHost, g_pinjector->m_settings.m_usSentinelPort, &pChannelObj->m_socketSentinel, 
							10000, 10000, info)<NET_SUCCESS)
						{
							g_pinjector->m_msgr.DM(MSG_ICONNONE, NULL, msgsrc, info );  //(Dispatch message)
				LeaveCriticalSection(&pChannelObj->m_critSentinelSocket);
	//						MessageBox(info, "Network Error", MB_OK);
							if(!(bSocketErrorReported))
							{
								g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, msgsrc, info);  //(Dispatch message)
								g_pinjector->SendMsg(CX_SENDMSG_ERROR, msgsrc, info);
								// set status
								//TODO
								bSocketErrorReported = true;
							}
							pChannelObj->m_socketSentinel = NULL; // insurance
							strcpy(szControlHost, "");
							usControlPort = 0;

							Sleep(100);
							continue; // can't do anything else if not connected
						}
						else
						{
				LeaveCriticalSection(&pChannelObj->m_critSentinelSocket);
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Control client connected to %s:%d", g_pinjector->m_settings.m_pszSentinelHost, g_pinjector->m_settings.m_usSentinelPort); 
							g_pinjector->m_msgr.DM(MSG_ICONNONE, NULL, msgsrc, errorstring);  //(Dispatch message)
							g_pinjector->SendMsg(CX_SENDMSG_INFO, msgsrc, errorstring);

							strcpy(szControlHost, g_pinjector->m_settings.m_pszSentinelHost);
							usControlPort = g_pinjector->m_settings.m_usSentinelPort;

							bSocketErrorReported = false; //reset
						}
					}

				}
			}
			else  //else socket not null
			{// check settings change.
				if(
						(
							(g_pinjector->m_settings.m_usSentinelPort>0)
						&&(usControlPort != g_pinjector->m_settings.m_usSentinelPort)
						)
					||(
							(g_pinjector->m_settings.m_pszSentinelHost)
						&&(strlen(g_pinjector->m_settings.m_pszSentinelHost)>0)
						&&(stricmp(szControlHost, g_pinjector->m_settings.m_pszSentinelHost))
						)
					)
				{//settings changed!

				EnterCriticalSection(&pChannelObj->m_critSentinelSocket);
					pChannelObj->m_net.CloseConnection(pChannelObj->m_socketSentinel);
					pChannelObj->m_socketSentinel=NULL;
				LeaveCriticalSection(&pChannelObj->m_critSentinelSocket);
					bSocketErrorReported = false; //reset
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Control client closed.  Settings change from %s:%d to %s:%d", szControlHost, usControlPort, g_pinjector->m_settings.m_pszSentinelHost, g_pinjector->m_settings.m_usSentinelPort); 
					g_pinjector->m_msgr.DM(MSG_ICONNONE, NULL, msgsrc, errorstring);  //(Dispatch message)
					g_pinjector->SendMsg(CX_SENDMSG_INFO, msgsrc, errorstring);

					continue; // go back up in order to connect
				}

				// here. monitor socket for disconnection.
				/// TODO

				// a. HA!

				// just send a ping and see.

				pChannelObj->SentinelPing();
				// if this fails it logs itself and nulls out.

				// then, if time send a ping.
				if(	dblNow > dblLastAttempt+((double)(g_pinjector->m_settings.m_ulSentinelRetryIntervalMS))/1000.0)
				{
					_ftime(&pChannelObj->m_timebProgrammingTick);
					dblLastAttempt = dblNow;

					//send ping.
					//TODO
					// if problem close socket and continue;
				}
			}
		}



		if(g_pinjector->m_settings.m_bAutoSchedulerMode)
		{

			// check settings

	EnterCriticalSection(&pChannelObj->m_critSecInfo);
		if(pszSettingInsertionPrimaryFile != pChannelObj->m_pszSettingInsertionPrimaryFile)
		{
			pszSettingInsertionPrimaryFile = pChannelObj->m_pszSettingInsertionPrimaryFile; // assign even if error - if a problem, the settings change will reset this.
			if((pszSettingInsertionPrimaryFile)&&(strlen(pszSettingInsertionPrimaryFile)))
			{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING)
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get primary insertion file", "%s getting file %s", 
											 msgsrc, pszSettingInsertionPrimaryFile);  //(Dispatch message)

				FILE* fp = fopen(pszSettingInsertionPrimaryFile, "rb");
				if(fp)
				{
					fseek(fp,0,SEEK_END);
					unsigned long ulLen = ftell(fp);
					fseek(fp,0,SEEK_SET);

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING)
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get primary insertion file", "%s got %d bytes in file %s", 
											 msgsrc, ulLen, pszSettingInsertionPrimaryFile);  //(Dispatch message)
					// allocate buffer and read in file contents
					if(pszPrimaryEventBuffer) free(pszPrimaryEventBuffer);
					pszPrimaryEventBuffer = (char*) malloc(ulLen+1); // for 0
					if(pszPrimaryEventBuffer!=NULL)
					{
						nPriBufLen = ulLen;
						fread(pszPrimaryEventBuffer,sizeof(char),ulLen,fp);
						memset(pszPrimaryEventBuffer+ulLen, 0, 1);
					}
					else { nPriBufLen = 0;}
					fclose(fp);
				}
				else
				{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING)
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get primary insertion file", "%s ERROR %d opening file %s", 
											 msgsrc, GetLastError(), pszSettingInsertionPrimaryFile);  //(Dispatch message)
				}
			}
			else
			{// have to free the buffer
				if(pszPrimaryEventBuffer) free(pszPrimaryEventBuffer);
				pszPrimaryEventBuffer = NULL;
				nPriBufLen = 0;
			}
		}

		if(pszSettingAllowPrimaryFile != pChannelObj->m_pszSettingAllowPrimaryFile)
		{
			pszSettingAllowPrimaryFile = pChannelObj->m_pszSettingAllowPrimaryFile; // assign even if error - if a problem, the settings change will reset this.
			if((pszSettingAllowPrimaryFile)&&(strlen(pszSettingAllowPrimaryFile)))
			{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING)
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get allow event file", "%s getting file %s", 
											 msgsrc, pszSettingAllowPrimaryFile);  //(Dispatch message)
				FILE* fp = fopen(pszSettingAllowPrimaryFile, "rb");
				if(fp)
				{
					fseek(fp,0,SEEK_END);
					unsigned long ulLen = ftell(fp);
					fseek(fp,0,SEEK_SET);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING)
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get allow insertion file", "%s got %d bytes in file %s", 
											 msgsrc, ulLen, pszSettingAllowPrimaryFile);  //(Dispatch message)

					// allocate buffer and read in file contents
					if(pszAllowEventBuffer) free(pszAllowEventBuffer);
					pszAllowEventBuffer = (char*) malloc(ulLen+1); // for 0
					if(pszAllowEventBuffer!=NULL)
					{
						nAllowBufLen = ulLen;
						fread(pszAllowEventBuffer,sizeof(char),ulLen,fp);
						memset(pszAllowEventBuffer+ulLen, 0, 1);
					}
					else { nAllowBufLen = 0;}
					fclose(fp);
				}
				else
				{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING)
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get allow insertion file", "%s ERROR %d opening file %s", 
											 msgsrc, GetLastError(), pszSettingAllowPrimaryFile);  //(Dispatch message)
				}
			}
			else
			{// have to free the buffer
				if(pszAllowEventBuffer) free(pszAllowEventBuffer);
				pszAllowEventBuffer = NULL;
				nAllowBufLen = 0;
			}
		}

		
		if(pszSettingInsertionSecondariesFile != pChannelObj->m_pszSettingInsertionSecondariesFile)
		{
			pszSettingInsertionSecondariesFile = pChannelObj->m_pszSettingInsertionSecondariesFile; // assign even if error - if a problem, the settings change will reset this.
			if((pszSettingInsertionSecondariesFile)&&(strlen(pszSettingInsertionSecondariesFile)))
			{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING)
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get secondaries file", "%s getting file %s", 
											 msgsrc, pszSettingInsertionSecondariesFile);  //(Dispatch message)
				FILE* fp = fopen(pszSettingInsertionSecondariesFile, "rb");
				if(fp)
				{
					fseek(fp,0,SEEK_END);
					unsigned long ulLen = ftell(fp);
					fseek(fp,0,SEEK_SET);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING)
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get secondaries file", "%s got %d bytes in file %s", 
											 msgsrc, ulLen, pszSettingInsertionSecondariesFile);  //(Dispatch message)

					
					// allocate buffer and read in file contents
					if(pszSecondaryEventsBuffer) free(pszSecondaryEventsBuffer);
					pszSecondaryEventsBuffer = (char*) malloc(ulLen+1); // for 0
					if(pszSecondaryEventsBuffer!=NULL)
					{
						nSecBufLen = (int)ulLen;
						fread(pszSecondaryEventsBuffer,sizeof(char),ulLen,fp);
						memset(pszSecondaryEventsBuffer+ulLen, 0, 1);
					}

					fclose(fp);


					// get num events

					if(pszSecondaryEventsBuffer!=NULL)
					{
						EnterCriticalSection(&pChannelObj->m_adc.m_crit);
						nNumSecondaries = pChannelObj->m_adc.CountEventsInStream((unsigned char*)pszSecondaryEventsBuffer, ulLen);
						LeaveCriticalSection(&pChannelObj->m_adc.m_crit);
					}
					if(nNumSecondaries<0) nNumSecondaries = 0;
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING)
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get secondaries file", "%s got %d secondaries in file %s", 
											 msgsrc, nNumSecondaries, pszSettingInsertionSecondariesFile);  //(Dispatch message)

				}
				else
				{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING)
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get secondaries file", "%s ERROR %d opening file %s", 
											 msgsrc, GetLastError(), pszSettingAllowPrimaryFile);  //(Dispatch message)
				}
			}
			else
			{// have to free the buffer
				if(pszSecondaryEventsBuffer) free(pszSecondaryEventsBuffer);
				pszSecondaryEventsBuffer = NULL;
				nNumSecondaries = 0;
			}
		}
	LeaveCriticalSection(&pChannelObj->m_critSecInfo);

		// check if schedule has changed.
		// load into memory if it has
		EnterCriticalSection(&g_pinjector->m_data.m_critTraffic);// just use the traffic crit, its ok
		nScheduleMod = g_pinjector->m_data.m_nScheduleMod;
		LeaveCriticalSection(&g_pinjector->m_data.m_critTraffic);// just use the traffic crit, its ok

		double dblBreak = dblNow + ((double)g_pinjector->m_settings.m_nProgramAutomationDwellMS)/1000.0; // number of milliseconds programming is delayed after a change (prevents hammering programming when a bunch of list changes happen all at once.

		while((pChannelObj->m_dblLastOnAir<0.0)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)&&(dblNow<dblBreak))
		{
			Sleep(10);
			_ftime( &pChannelObj->m_timebProgrammingTick);  // it's here right now...  maybe should be inside if, if in schedulermode
			dblNow = ((double)(pChannelObj->m_timebProgrammingTick.time)) + ((double)(pChannelObj->m_timebProgrammingTick.millitm))/1000.0;
		}

		if(
			  ((nLastScheduleMod!=nScheduleMod)  //on init this will be true
				||(bLiveEventsChanged)) // but if the live events have changed the retrieve boundaries may also have changed, so need to update.
//			&&(!bChangesPending) // delay getting new schedule until this is dealt with
// actually no, let's get them, but if the schedule changes are pending, great! overwrite them with the new changes.
				&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)
			)
		{

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING)
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get schedule items", "%s mods: %d %d", msgsrc, nLastScheduleMod, nScheduleMod);  //(Dispatch message)

			if(nLastScheduleMod!=nScheduleMod)
			{
				bScheduleChanged = true;
			}

			nLastScheduleMod = nScheduleMod;
		 
		// now ingest what is not in the past. // no need to get stuff that is ending within 1 second, we aren't messing with that anyway.

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT itemid, clipid, segment, scheduled_on, \
duration, clip_title, status, SOM \
FROM %s WHERE channelid = %d AND (scheduled_on + ((duration)/1000)) > %.3f AND scheduled_on < %.3f \
AND created_by <> 'sys' ORDER BY scheduled_on",  // not sys!
				((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
				pChannelObj->m_nChannelID,
				pChannelObj->m_dblAutomationTimeEstimate,
				pChannelObj->m_dblLastOnAir
				);

			// problem exists here if pChannelObj->m_dblLastOnAir <0.0 because no items come in.  of course, that is what we want.
			// the problem is when the automation system has not caught up with the status mod.  maybe we can give it a delay if last on air is negative.
			// do above.

if((g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))||(g_pinjector->m_settings.m_bDebugSQL))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get schedule items", "%s SQL [%s]", msgsrc, szSQL);  //(Dispatch message)


if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entering schedule crit 208", 
	msgsrc);  //(Dispatch message)
			EnterCriticalSection(&pChannelObj->m_critScheduleEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entered schedule crit 208", 
	msgsrc);  //(Dispatch message)


			int nIndex=0;
			CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
			if(prs != NULL) // no error
			{

//				if the schedule is not fully processed, can't overwrite with a swap.
//				so if the schedule has changed again, we can overwrite the new stuff - but don't do the swap.

				// must be live changed, so deal with new horizons by updating whole list.
					// or schedule change - still need to get the new stuff, load it in.

				if(bScheduleChangeProcessed)  // not still in progress
				{
				// need to have two buffers, to compare
				// swap the buffiz
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s SWAPPING SCHEDULE",msgsrc );

					ppLastSchedule = pChannelObj->m_ppScheduleEvents;
					nNumLastScheduleEvents = pChannelObj->m_nNumScheduleEvents;
					bScheduleChangeProcessed = false;

				}
				else
				{  // clear out old for the new stuff
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s CLEARING SCHEDULE",msgsrc );

					if((pChannelObj->m_nNumScheduleEvents>0)&&(pChannelObj->m_ppScheduleEvents))
					{
						int i=0;
						while(i<pChannelObj->m_nNumScheduleEvents)
						{
							if(pChannelObj->m_ppScheduleEvents[i])
							{
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s clearing %d->%d",msgsrc,pChannelObj->m_ppScheduleEvents[i], pChannelObj->m_ppScheduleEvents[i]->m_pszEventID);
								pChannelObj->m_ppScheduleEvents[i]->m_pszCaller = (char*)malloc(64); if(pChannelObj->m_ppScheduleEvents[i]->m_pszCaller) strcpy(pChannelObj->m_ppScheduleEvents[i]->m_pszCaller, "programming_schedule");
								try {delete pChannelObj->m_ppScheduleEvents[i];} catch(...){}
							}
							i++;
						}
						try {delete [] pChannelObj->m_ppScheduleEvents;} catch(...){}
					}
				}

				bLiveEventsChanged = false; // reset this here.  if there are further live events, it will re-get.
				//if there are no events, it never enters the below items to be done thing and stays true and hammers database.


				pChannelObj->m_ppScheduleEvents = NULL;
				pChannelObj->m_nNumScheduleEvents = 0;

				while((!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
				{
	//g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Injector:debug", "� NEOF");  Sleep(50);//(Dispatch message)

					CInjectorScheduleObject* pObj =  new CInjectorScheduleObject;
					if(pObj)
					{
						pObj->m_nChannelID = pChannelObj->m_nChannelID;
						// don't bother with creator

						bool bError = false;
						try
						{
							CString szTemp;
							prs->GetFieldValue("itemid", szTemp);
							pObj->m_nID = atoi(szTemp);

							prs->GetFieldValue("clipid", szTemp);
							if(szTemp.GetLength()>0)
							{
								pObj->m_pszEventID = (char*)malloc(szTemp.GetLength()+1);
								if(pObj->m_pszEventID)
								{
									sprintf(pObj->m_pszEventID, "%s", szTemp);
							//		strcpy(pObj->m_pszEventID, szTemp.GetBuffer(0));
								}
							}
							prs->GetFieldValue("segment", szTemp);
							if(szTemp.GetLength()>0)
							{
								pObj->m_nSegment = atoi(szTemp);
							}
							else pObj->m_nSegment = -1; // was NULL or blank, no numbah in deah

							prs->GetFieldValue("scheduled_on", szTemp);
							pObj->m_dblOnAirTimeLocal = atof(szTemp);

							prs->GetFieldValue("duration", szTemp);
							pObj->m_nDurationMS = atoi(szTemp);

							prs->GetFieldValue("clip_title", szTemp);
							if(szTemp.GetLength()>0)
							{
								pObj->m_pszEventTitle = (char*)malloc(szTemp.GetLength()+1);
								if(pObj->m_pszEventTitle)
								{
									sprintf(pObj->m_pszEventTitle, "%s", szTemp);
	//								strcpy(pObj->m_pszEventTitle, szTemp.GetBuffer(0));
								}
							}
							prs->GetFieldValue("status", szTemp);
							pObj->m_ulStatus = atol(szTemp);

							prs->GetFieldValue("SOM", szTemp);
							pObj->m_nSOMMS = atoi(szTemp);


	if((g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))||(g_pinjector->m_settings.m_bDebugSQL))
	{
		g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get schedule items", "%s got event: %d[%s][%s](seg %d) %.3f dur=%d som=%d, 0x%08x [%s]", msgsrc, 
									pObj->m_nID,
									pObj->m_pszEventID?pObj->m_pszEventID:"",
									pObj->m_pszEventTitle?pObj->m_pszEventTitle:"",
									pObj->m_nSegment,
									pObj->m_dblOnAirTimeLocal,
									pObj->m_nDurationMS,
									pObj->m_nSOMMS,
									pObj->m_ulStatus,
									pObj->m_pszRecKey?pObj->m_pszRecKey:""
			);  //(Dispatch message)
	}

						}
						catch(CException *e)// CDBException *e, CMemoryException *m)  
						{
							bError = true;
							if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
							{
								g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get schedule items", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							}
							else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
							{
								g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get schedule items", "Retrieve: Caught exception: out of memory.\n%s", szSQL);
		//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
							}
							else 
							{
								g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get schedule items", "Retrieve: Caught other exception.\n%s", szSQL);
		//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
							}
							e->Delete();

						} 
						catch( ... )
						{
							bError = true;
							g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get schedule items", "Retrieve: Caught exception.\n%s", szSQL);
						}

						if(bError)
						{
							try{ delete pObj; } catch(...){}
						}
						else
						{
							util.AddItem(&pChannelObj->m_ppScheduleEvents, &pChannelObj->m_nNumScheduleEvents, pObj );  // returns index
				//			pChannelObj->InsertScheduleItem(pObj, INJECTOR_ARRAY_SCHEDULE);
						}
					}
					nIndex++;
					prs->MoveNext();
				}
				prs->Close();

				try{ delete prs; } catch(...){}
//				bScheduleChanged = true;  // this means mods were different.  set now above.

				_ftime(&timebPending);

			}
			else
			{
			//	prs was null
				g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Get schedule items", "Retrieve returned a NULL recordset.\n%s", szSQL);
			}
			if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
			g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Got schedule items", "%d schedule items retrieved on %s", nIndex, msgsrc);  //(Dispatch message)

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s leaving schedule crit 208", 
	msgsrc);  //(Dispatch message)
			LeaveCriticalSection(&pChannelObj->m_critScheduleEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s left schedule crit 208", 
	msgsrc);  //(Dispatch message)

		}

		if((pChannelObj->m_bLiveEventsScheduleChanged == true)||(bScheduleChanged==true)) // signal programming thread: let's let the new restrictions be calculated first, 
		{
			if(pChannelObj->m_bLiveEventsScheduleChanged == true)
			{
				_ftime(&timebPending);
				pChannelObj->m_bLiveEventsScheduleChanged = false;
				bLiveEventsChanged=true;
			}
			bScheduleChanged = false;  // becuse, now we have both old and new buffiz

			if(!bChangesPending){ _ftime(&timebChange);  dblChange = ((double)(timebChange.time)) + ((double)(timebChange.millitm))/1000.0; } // initialize time of initial change.
			bChangesPending = true;
			dblPending = ((double)(timebPending.time)) + ((double)(timebPending.millitm))/1000.0;
		}

		_ftime( &pChannelObj->m_timebProgrammingTick);  // it's here right now...  maybe should be inside if if scheduler mode
		dblNow = ((double)(pChannelObj->m_timebProgrammingTick.time)) + ((double)(pChannelObj->m_timebProgrammingTick.millitm))/1000.0;

		
if(0)//g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_PROGRAMMING))	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Programming changes", "%s pending %d, {now %.3f P %.3f Ch %.3f} pending elapsed %d, force %d, next %.3f time %.3f, safe %d", 
	msgsrc,
	bChangesPending, dblNow, dblPending, dblChange,
	(dblNow>dblPending+((double)(g_pinjector->m_settings.m_nProgramAutomationDwellMS))/1000.0),
	(dblNow>dblChange+((double)(g_pinjector->m_settings.m_nProgramAutomationForceMS))/1000.0),
	pChannelObj->m_dblNextOnAir, pChannelObj->m_dblAutomationTimeEstimate, g_pinjector->m_settings.m_nProgramAutomationSafetyMS
	);  //(Dispatch message)

		if(
				(bChangesPending)
			&&(
					(dblNow>dblPending+((double)(g_pinjector->m_settings.m_nProgramAutomationDwellMS))/1000.0) // pending time elapsed
				||(dblNow>dblChange+((double)(g_pinjector->m_settings.m_nProgramAutomationForceMS))/1000.0) // forcing time elapsed
				)
			)
		{
if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_PROGRAMMING))
{	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Programming changes", "%s pending %.3f", 
	msgsrc,pChannelObj->m_dblNextOnAir);
}

			double dlbMinTriggerNext = (double)(pChannelObj->m_timebLastProgramTick.time)+ ((double)(pChannelObj->m_timebLastProgramTick.millitm + g_pinjector->m_settings.m_nProgramAutomationDefeatMS))/1000.0;
			if(
				  (	pChannelObj->m_dblNextOnAir >0.0 )  // but don't do this if there is not enough time to do it, or if there is not any live action going on.
				&&(	((int)((pChannelObj->m_dblNextOnAir - pChannelObj->m_dblAutomationTimeEstimate)*1000.0)) > g_pinjector->m_settings.m_nProgramAutomationSafetyMS )  // but don't do this if there is not enough time to do it, or if there is not any live action going on.
				&&(	dblNow > dlbMinTriggerNext)  // or if it's too soon since the last one.
				)
			{
			// an insertion or deletion will cause a new bLiveEventsChanged=true, so we can safely do one thing at a time and exit.
			if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_PROGRAMMING))	
			{
				g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s changes pending, analyzing...", 
				msgsrc);  //(Dispatch message)
//				Sleep(200);
			}

			// here, we have the new schedule in pChannelObj->m_ppScheduleEvents, pChannelObj->m_nNumScheduleEvents
			// and the old one (if there is one) in 
			// CInjectorScheduleObject** ppLastSchedule=NULL;
			// int nNumLastScheduleEvents=0;

			// first we look for deletions, we want to make sure that anything inserted that has been removed gets deleted first.
			// if there are no deletions to do, we then go down and look for the closest thing to airtime that needs to be inserted.
			// then we insert it.

		  // DELETIONS
			int i = 0;
			bool bDone = false;
			bool bDeleted = false;
			bool bInserted = false;
			if((ppLastSchedule)&&(nNumLastScheduleEvents>0)) // otherwise can't be anything to delete
			{
				if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_PROGRAMMING))	
				{
					g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s old canon exists with %d elements", 
					msgsrc,nNumLastScheduleEvents);  //(Dispatch message)
	//				Sleep(200);
				}

				if((pChannelObj->m_ppScheduleEvents)&&(pChannelObj->m_nNumScheduleEvents>0))  // otherwise just can delete all if it meets timing criteria
				{

					while(i<pChannelObj->m_nNumScheduleEvents)
					{
						if(pChannelObj->m_ppScheduleEvents[i])
						{
							int j=0;
							while(j<nNumLastScheduleEvents)
							{
								if(
									  (ppLastSchedule[j]->m_dblOnAirTimeLocal >= pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal - (double)(g_pinjector->m_settings.m_ulAutomationTimeToleranceMS)/1000.0)
									&&(ppLastSchedule[j]->m_dblOnAirTimeLocal <= pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal + (double)(g_pinjector->m_settings.m_ulAutomationTimeToleranceMS)/1000.0)
									&&(ppLastSchedule[j]->m_pszEventID)
									&&(pChannelObj->m_ppScheduleEvents[i]->m_pszEventID)
									&&(strcmp(ppLastSchedule[j]->m_pszEventID, pChannelObj->m_ppScheduleEvents[i]->m_pszEventID)==0)
									)
								{
				if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_PROGRAMMING))	
				{
					g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s found [%s] in both canons", 
					msgsrc, ppLastSchedule[j]->m_pszEventID);  //(Dispatch message)
	//				Sleep(200);
				}

									ppLastSchedule[j]->m_ulFlags |= INJECTOR_FLAG_FOUND;
									break; // no need to go further... found it, can move on to next thing to find in the i loop
								}
								j++;
							}
						}
						i++;
					}
				}
				// else delete all if it meets timing criteria - ok all not found will get deleted

				i = 0;
				while(i<nNumLastScheduleEvents)
				{
					if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_PROGRAMMING))	
					{
						g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s checking %d in old canon", 
						msgsrc, i);  //(Dispatch message)
		//				Sleep(200);
					}

					if(ppLastSchedule[i])
					{
						if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_PROGRAMMING))	
						{
							g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s checking %d:0x%08x in old canon", 
							msgsrc, i, ppLastSchedule[i]->m_ulFlags);  //(Dispatch message)
			//				Sleep(200);
						}

						if(!(ppLastSchedule[i]->m_ulFlags&INJECTOR_FLAG_FOUND))
						{
							// must delete.
							// find event in event list, and then delete it if it's there.
							// instead, let's modify the event back to the allow ID!
							// NO, can't modify, as it might be CUED in which case that won't be allowed.
							// Best to insert new item and delete the id.

							// ***BUT****
							// IF the event has PLAYED it will be removed from the canon and not found, so this will trigger deletion.
							// that is not good.
							// So if an item is done, or just in the past, we should not delete it.

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entering live crit 201", 
	msgsrc);  //(Dispatch message)
							EnterCriticalSection(&pChannelObj->m_critLiveEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entered live crit 201", 
	msgsrc);  //(Dispatch message)
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Programming", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_LIVE, INJECTOR_ARRAY_FIND_CONTAINED", msgsrc, ppLastSchedule[i]->m_dblOnAirTimeLocal);
							int nIndex = pChannelObj->FindScheduleItem(ppLastSchedule[i]->m_dblOnAirTimeLocal, INJECTOR_ARRAY_LIVE, INJECTOR_ARRAY_FIND_CONTAINED);  // returns index
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Programming", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_LIVE, INJECTOR_ARRAY_FIND_CONTAINED returned %d", msgsrc, ppLastSchedule[i]->m_dblOnAirTimeLocal, nIndex);

							bool bSuppress = false;
							if(nIndex>=0)
							{
								// check the ID.
								if(
									  (pChannelObj->m_ppLiveEvents[nIndex]->m_pszEventID)&&(ppLastSchedule[i]->m_pszEventID)
									&&(strcmp(pChannelObj->m_ppLiveEvents[nIndex]->m_pszEventID, ppLastSchedule[i]->m_pszEventID)==0)
									&&(pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal >= ppLastSchedule[i]->m_dblOnAirTimeLocal - (double)(g_pinjector->m_settings.m_ulAutomationTimeToleranceMS)/1000.0)
									&&(pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal <= ppLastSchedule[i]->m_dblOnAirTimeLocal + (double)(g_pinjector->m_settings.m_ulAutomationTimeToleranceMS)/1000.0)
									&&(pChannelObj->m_ppLiveEvents[nIndex]->m_nDurationMS == ppLastSchedule[i]->m_nDurationMS)
									&&(pChannelObj->m_ppLiveEvents[nIndex]->m_nSegment == ppLastSchedule[i]->m_nSegment)
									&&(!(pChannelObj->m_ppLiveEvents[nIndex]->m_ulStatus&((1<<eventdone)|(1<<eventpostrolled)|(1<<eventrunning)|(1<<notplayed)|(1<<eventskipped))))  // not done or playing
									&&(pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal > pChannelObj->m_dblAutomationTimeEstimate+(double)(g_pinjector->m_settings.m_nAdvanceSchedulingThresholdMinMins*60) )
									&&(pszPrimaryEventBuffer)
									)
								{
									//modify.
			if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
				g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Remove scheduled items", "removing schedule item [%s] @ %.3f retrieved @%d on %s", 
					pChannelObj->m_ppLiveEvents[nIndex]->m_pszEventID, pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal, 
					nIndex, msgsrc);  //(Dispatch message)

			//TODO


			// need to determine what to insert, if anything.
			// it may well be that if we just remove the inserted vod item, plus any sceondaries, plus a possible end item, 
			// then the intial allow ID which we cut off earlier with a hard start may just be long enough to deal.

			// nIndex is the item itself, nIndex-1 should be the logo item we cut off, nIndex+1 should be a possible logo item to replace.

			// first we check if the nIndex-1 item encompasses the entire length to the end of nIndex+1.
			// if so, we need not insert anything, just delete the nIndex and nIndex+1 items and all in between.
			// if not, we need to get the Index+1 and insert a new large item before deleteing nIndex-1 to nIndex+1 item
									int nDeleteStartIndex=nIndex;
									int nDeleteEndIndex=-1;
									int nInserts=0;
									double dblStartTime = pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal;
									double dblEndTime;
									int nDeletePosition = pChannelObj->m_ppLiveEvents[nIndex]->m_nPosition;

									int nNumItemsToDelete = nNumSecondaries;

									bool bReplacement = true;
									bool bPrevAllowExists = false;

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s check next index %d+1 <? %d", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
									 nIndex,pChannelObj->m_nNumLiveEvents);  //(Dispatch message)
//Sleep(201);

									// check the end time.
									if(nIndex+1<pChannelObj->m_nNumLiveEvents)
									{
										if(
												(pChannelObj->m_ppLiveEvents[nIndex+1]->m_pszEventID)
											&&(pChannelObj->m_pszSettingInsertionAllowID)
											&&(strcmp(pChannelObj->m_ppLiveEvents[nIndex+1]->m_pszEventID, pChannelObj->m_pszSettingInsertionAllowID)==0)
											)
										{
											// we can check it
											dblEndTime = pChannelObj->m_ppLiveEvents[nIndex+1]->m_dblOnAirTimeLocal + ((double)(pChannelObj->m_ppLiveEvents[nIndex+1]->m_nDurationMS))/1000.0 ;
											nDeleteEndIndex = nIndex+1;

											// TODO add the secondaries of the allow item
											nNumItemsToDelete = pChannelObj->m_ppLiveEvents[nIndex+1]->m_nPosition - nDeletePosition +1;

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s end time case 01 %.3f, delidx %d num %d", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
									 dblEndTime, nDeleteEndIndex, nNumItemsToDelete);  //(Dispatch message)

										}
										else
										{
											dblEndTime = pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal + ((double)(pChannelObj->m_ppLiveEvents[nIndex]->m_nDurationMS))/1000.0 ;
											nDeleteEndIndex = nIndex;
											nNumItemsToDelete = pChannelObj->m_ppLiveEvents[nIndex+1]->m_nPosition - nDeletePosition; // not including itself
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s end time case 02 %.3f, delidx %d num %d", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
									 dblEndTime, nDeleteEndIndex, nNumItemsToDelete);  //(Dispatch message)
										}
									}
									else
									{
										// have to use the end of the event itself
										dblEndTime = pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal + ((double)(pChannelObj->m_ppLiveEvents[nIndex]->m_nDurationMS))/1000.0 ;
										nDeleteEndIndex = nIndex;
										nNumItemsToDelete = nNumSecondaries + nDeletePosition + 1;
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s end time case 03 %.3f, delidx %d num %d", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
									 dblEndTime, nDeleteEndIndex, nNumItemsToDelete);  //(Dispatch message)
									}

									if((nIndex-1)<0) // no item to check, shouldnt happen, except when the cutoff make an event of zero length (or min dur TODO have to check min)
									{
										//error
										//in this case replacement is true.
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s no prev idx", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)"
									 );  //(Dispatch message)
									}
									else
									{
										// check the start time.
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s check start %s =? %s, %d <=? %d", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
									 pChannelObj->m_ppLiveEvents[nIndex-1]->m_pszEventID?pChannelObj->m_ppLiveEvents[nIndex-1]->m_pszEventID:"(null)",
									 pChannelObj->m_pszSettingInsertionAllowID?pChannelObj->m_pszSettingInsertionAllowID:"(null)",
									 pChannelObj->m_ppLiveEvents[nIndex-1]->m_nDurationMS, pChannelObj->m_nSettingMaxAllowDurMS
									 );  //(Dispatch message)
										if(
											  (pChannelObj->m_ppLiveEvents[nIndex-1]->m_pszEventID)
											&&(pChannelObj->m_pszSettingInsertionAllowID)
											&&(strcmp(pChannelObj->m_ppLiveEvents[nIndex-1]->m_pszEventID, pChannelObj->m_pszSettingInsertionAllowID)==0)
											&&(pChannelObj->m_ppLiveEvents[nIndex-1]->m_nDurationMS <= pChannelObj->m_nSettingMaxAllowDurMS)
											)
										{
											bPrevAllowExists = true;
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s check span %.3f <=? %.3f, %.3f >= %.3f", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
									 pChannelObj->m_ppLiveEvents[nIndex-1]->m_dblOnAirTimeLocal,
									 ppLastSchedule[i]->m_dblOnAirTimeLocal+.00001,
									 pChannelObj->m_ppLiveEvents[nIndex-1]->m_dblOnAirTimeLocal + ((double)(pChannelObj->m_ppLiveEvents[nIndex-1]->m_nDurationMS))/1000.0,
										dblEndTime-.00001
									 );  //(Dispatch message)

// reset the start time to be the end of the previous event in case our hard start insertion had cut off some of the previous
											dblStartTime = pChannelObj->m_ppLiveEvents[nIndex-1]->m_dblOnAirTimeLocal+ ((double)(pChannelObj->m_ppLiveEvents[nIndex-1]->m_nDurationMS))/1000.0;


											if(
													(pChannelObj->m_ppLiveEvents[nIndex-1]->m_dblOnAirTimeLocal<=ppLastSchedule[i]->m_dblOnAirTimeLocal+.00001) // tolerance
												&&(pChannelObj->m_ppLiveEvents[nIndex-1]->m_dblOnAirTimeLocal + ((double)(pChannelObj->m_ppLiveEvents[nIndex-1]->m_nDurationMS))/1000.0
												 >=dblEndTime-.00001) // tolerance
												)
											{
												// can just delete
												// TODO check max dur
												bReplacement = false;
											}

										}
										else
										{
											// not it!
											//have to replace;
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s not it", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)"
									 );  //(Dispatch message)
										}
									}

									if(pszAllowEventBuffer==NULL) bReplacement=false;

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s check replacement %d", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
									 bReplacement);  //(Dispatch message)
//Sleep(201);

									if(bReplacement) // need to build the insert buffer.
									{
										int nDuration =(int)((dblEndTime - dblStartTime)*1000.0);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s replacement duration %d", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
									 nDuration);  //(Dispatch message)

										if(nDuration<33) // one frame, nmust be drop frame error...
										{
											nDuration = 0;
											nInserts = 0;
											bReplacement = false;
										}
									}

									if(bReplacement) // if still replacement, need to build the insert buffer.
									{
										//need to figure out how many events to insert.
										nInserts = 1;
										int nDuration =(int)((dblEndTime - dblStartTime)*1000.0);

										while(nDuration>pChannelObj->m_nSettingMaxAllowDurMS)
										{
											nInserts++;
											nDuration-=pChannelObj->m_nSettingMaxAllowDurMS;
										}

									// prepare buffer
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s entering adc critsec", msgsrc);

					EnterCriticalSection(&pChannelObj->m_adc.m_crit);
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s entered adc critsec", msgsrc);
										CAList list;
										list.EnsureAllocated(nInserts);
										list.m_nEventCount = 0;
										nInserts = 0;
										nDuration =(int)((dblEndTime - dblStartTime)*1000.0);
										while(nDuration>0)
										{
											list.m_nEventCount += pChannelObj->m_adc.StreamToArray(&list, nInserts, (unsigned char*)pszAllowEventBuffer, (unsigned char)pChannelObj->m_nServerBasis, 1);

											if((list.m_nEventCount>nInserts)&&(list.m_ppEvents[nInserts]))
											{
												//reassign time, duration
												list.m_ppEvents[nInserts]->m_ulOnAirTimeMS = (unsigned long)((
														dblStartTime 
													- (
														  (double)( (((int)(dblStartTime)/86400)*86400) )
														)
																																							)*1000.0);
												if(nDuration>pChannelObj->m_nSettingMaxAllowDurMS)
												{
													list.m_ppEvents[nInserts]->m_ulDurationMS = pChannelObj->m_nSettingMaxAllowDurMS;
													dblStartTime += ((double)pChannelObj->m_nSettingMaxAllowDurMS)/1000.0;
												}
												else
												{
													list.m_ppEvents[nInserts]->m_ulDurationMS = nDuration;
												}

												if((pszAllowEventBuffer)&&(nAllowBufLen>sizeof(revent)))  // want to force an extended event.
												{
													list.m_ppEvents[nInserts]->m_pContextData = new bool;
												}
												else
												{
													if(list.m_ppEvents[nInserts]->m_pContextData)
													{
														try {delete(list.m_ppEvents[nInserts]->m_pContextData);} catch(...){}
														list.m_ppEvents[nInserts]->m_pContextData = NULL;
													}

												}

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s replacement item %d duration %d", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
									 nInserts, list.m_ppEvents[nInserts]->m_ulDurationMS);  //(Dispatch message)


											}
											nInserts++;
											nDuration-=pChannelObj->m_nSettingMaxAllowDurMS;
											
										}

										// prepare the buffer and insert!
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s create buffer", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)"
									 );  //(Dispatch message)
//Sleep(201);

										//now create an insert buffer.
										unsigned char* pch = NULL;
										int nLen = 0;
										int ins=0;
										while(ins < nInserts)
										{
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s check before EventToStream, converting %s", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
									 list.m_ppEvents[ins]->m_pszID);  //(Dispatch message)

											pChannelObj->m_adc.InitEvent();

											if(list.m_ppEvents[ins]->m_pContextData)
											{
												if(pChannelObj->m_adc.EventToStream(list.m_ppEvents[ins], ADC_FORCE_EXTENDED, (unsigned char)pChannelObj->m_nServerBasis)<=ADC_ERROR)
												{
													//report
g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "InjectorProgrammingThread", "%s error converting %s with EventToStream 1", 
									 msgsrc, 
									 list.m_ppEvents[ins]->m_pszID?list.m_ppEvents[ins]->m_pszID:"(null)");  //(Dispatch message)

												}
											}
											else
											{
												if(pChannelObj->m_adc.EventToStream(list.m_ppEvents[ins], 0, (unsigned char)pChannelObj->m_nServerBasis)<=ADC_ERROR)
												{
													//report
g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "InjectorProgrammingThread", "%s error converting %s with EventToStream 2", 
									 msgsrc, 
									 list.m_ppEvents[ins]->m_pszID?list.m_ppEvents[ins]->m_pszID:"(null)");  //(Dispatch message)
												}
											}

											//prevent freeing on event going out of scope, just want to use the values.

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s check after EventToStream [%s] [%s] %02x:%02x:%02x.%02x d%02x:%02x:%02x.%02x", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
									 pChannelObj->m_adc.m_VerifyEvent.eid,
									 pChannelObj->m_adc.m_VerifyEvent.etitle,
									 pChannelObj->m_adc.m_VerifyEvent.oahour,
									 pChannelObj->m_adc.m_VerifyEvent.oamin,
									 pChannelObj->m_adc.m_VerifyEvent.oasec,
									 pChannelObj->m_adc.m_VerifyEvent.oaframe,
									 pChannelObj->m_adc.m_VerifyEvent.edurh,
									 pChannelObj->m_adc.m_VerifyEvent.edurm,
									 pChannelObj->m_adc.m_VerifyEvent.edurs,
									 pChannelObj->m_adc.m_VerifyEvent.edurf
									 );  //(Dispatch message)
											
											if(pChannelObj->m_adc.m_VerifyEvent.Type == 0x01)
											{
												int nEventLen=sizeof(revent);
				//AfxMessageBox("Extended Event");
												unsigned char* pchNew = (unsigned char*)malloc(nLen+nEventLen+ pChannelObj->m_adc.m_usVerifyExtendedBufferLength+1);
												if(pchNew)
												{
				//foo.Format("extended event len %d %s", pChannelObj->m_adc.m_usVerifyExtendedBufferLength, pChannelObj->m_adc.m_pucVerifyExtendedEventBuffer+4);
				//AfxMessageBox(foo);
													if(pch){memcpy(pchNew, pch, nLen); free(pch);}
													memcpy(pchNew+nLen, (char*)(&pChannelObj->m_adc.m_VerifyEvent), nEventLen);
													if(pChannelObj->m_adc.m_pucVerifyExtendedEventBuffer)
													{
														memcpy(pchNew+nLen+nEventLen, pChannelObj->m_adc.m_pucVerifyExtendedEventBuffer, pChannelObj->m_adc.m_usVerifyExtendedBufferLength);
														nLen += nEventLen+pChannelObj->m_adc.m_usVerifyExtendedBufferLength;
													}
													pch = pchNew;

												}
												else
												{
//TODO report error
												}
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion: extended buffer item %d, total buflen %d", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
									 ins, nLen);  //(Dispatch message)
											}
											else
											{
				//AfxMessageBox("Non-extended Event");
												int nEventLen=sizeof(revent)-3;
												unsigned char* pchNew = (unsigned char*)malloc(nLen+nEventLen+1);
												if(pchNew)
												{

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion: len = %d,%d, bufs %d,%d, buffer+16 [%s]", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
									 nLen, nEventLen,pch,pchNew,
									 (char*)(&pChannelObj->m_adc.m_VerifyEvent)+19);  //(Dispatch message)

													if(pch)
													{
														memcpy(pchNew, pch, nLen); 
														free(pch);
													}

													memcpy(pchNew+nLen, (char*)(&pChannelObj->m_adc.m_VerifyEvent)+3, nEventLen);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion: copy buffer+16 [%s]", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
									 (char*)pchNew+nLen+16);  //(Dispatch message)

/*
FILE* fp;
char filename[260];
sprintf(filename, "buf%d.lst", ins);
fp=fopen(filename, "wb");
if(fp)
{
fwrite(pchNew+nLen, nEventLen, 1, fp);
fclose(fp);
}
*/
													nLen += nEventLen;
													pch = pchNew;
												}
												else
												{
//TODO report error

												}
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion: buffer item %d, total buflen %d, %s", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
									 ins, nLen, pChannelObj->m_adc.m_VerifyEvent.eid);  //(Dispatch message)

											}
											ins++;
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s check loop", 
									 msgsrc, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)");  //(Dispatch message)

										}

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
{
FILE* fp;
fp=fopen("injectorreplacestream.lst", "wb");
if(fp)
{
fwrite(pch, nLen, 1, fp);
fclose(fp);
}
}
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
{
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s preparing to insert %d bytes", 
									 msgsrc,nLen);  //(Dispatch message)
//Sleep(500); // let it get there.
}

				// then insert
										if(pChannelObj->ADCInsertEventBuffer(pch, nLen, nDeletePosition)<INJECTOR_SUCCESS)
										{
											//error
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s insertion unsuccesful", 
									 msgsrc/*, 
									 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)"*/);  //(Dispatch message)

//									 Sleep(500);

	EnterCriticalSection(&pChannelObj->m_critServerInfo);
										sprintf(errorstring, "%s:%d Programming", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);
										unsigned char h,m,s,f;
										pChannelObj->m_adc.ConvertMillisecondsToHMSF(
											(unsigned long)((ppLastSchedule[i]->m_dblOnAirTimeLocal - ( (double)( (((int)(ppLastSchedule[i]->m_dblOnAirTimeLocal)/86400)*86400) )	)	)*1000.0),
											&h, &m, &s, &f,pChannelObj->m_nServerBasis);

										g_pinjector->SendAsRunMsg(CX_SENDMSG_ERROR, pChannelObj->m_nChannelID, ppLastSchedule[i]->m_pszEventID, errorstring, 
											"Error inserting replacement items while deleting schedule item %s at %02d:%02d:%02d.%02d%s%s", 
											ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
											h,m,s,f,
											pChannelObj->m_pszDesc?" on ":"",
											pChannelObj->m_pszDesc?pChannelObj->m_pszDesc:""
											);

										}
										else
										{
											//success!
										}
										if(pch) free(pch);
										
									}

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
{
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s preparing to delete %d @ %d", 
									 msgsrc, 
									 nNumItemsToDelete, nDeletePosition+nInserts);  //(Dispatch message)
//Sleep(500); // let it get there.
}

									// now delete
									if(pChannelObj->ADCDeleteEvents(
										nDeletePosition+nInserts, 
										nNumItemsToDelete)<INJECTOR_SUCCESS)
									{
										//error
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s deletion unsuccesful", 
							 msgsrc/*, 
							 ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)"*/);  //(Dispatch message)
//							 Sleep(500);

	EnterCriticalSection(&pChannelObj->m_critServerInfo);
										sprintf(errorstring, "%s:%d Programming", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);
										unsigned char h,m,s,f;
										pChannelObj->m_adc.ConvertMillisecondsToHMSF(
											(unsigned long)((ppLastSchedule[i]->m_dblOnAirTimeLocal - ( (double)( (((int)(ppLastSchedule[i]->m_dblOnAirTimeLocal)/86400)*86400) )	)	)*1000.0),
											&h, &m, &s, &f,pChannelObj->m_nServerBasis);

										g_pinjector->SendAsRunMsg(CX_SENDMSG_ERROR, pChannelObj->m_nChannelID, ppLastSchedule[i]->m_pszEventID, errorstring, 
											"Error deleting schedule item %s at %02d:%02d:%02d.%02d%s%s", 
											ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
											h,m,s,f,
											pChannelObj->m_pszDesc?" on ":"",
											pChannelObj->m_pszDesc?pChannelObj->m_pszDesc:""
											);

									}
									else
									{
									//	bSuccess = true;
	EnterCriticalSection(&pChannelObj->m_critServerInfo);
										sprintf(errorstring, "%s:%d Programming", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);
										unsigned char h,m,s,f;
										pChannelObj->m_adc.ConvertMillisecondsToHMSF(
											(unsigned long)((ppLastSchedule[i]->m_dblOnAirTimeLocal - ( (double)( (((int)(ppLastSchedule[i]->m_dblOnAirTimeLocal)/86400)*86400) )	)	)*1000.0),
											&h, &m, &s, &f,pChannelObj->m_nServerBasis);

										g_pinjector->SendAsRunMsg(CX_SENDMSG_INFO, pChannelObj->m_nChannelID, ppLastSchedule[i]->m_pszEventID, errorstring, 
											"Deleted schedule item %s at %02d:%02d:%02d.%02d%s%s", 
											ppLastSchedule[i]->m_pszEventID?ppLastSchedule[i]->m_pszEventID:"(null)",
											h,m,s,f,										
											pChannelObj->m_pszDesc?" on ":"",
											pChannelObj->m_pszDesc?pChannelObj->m_pszDesc:""
											);

									}



if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
{
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s done with insertion process", 
									 msgsrc);  //(Dispatch message)
//Sleep(500); // let it get there.
}

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s leaving adc critsec", msgsrc);
					LeaveCriticalSection(&pChannelObj->m_adc.m_crit);
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s left adc critsec", msgsrc);



/*



			// first insert
									// prepare buffer
					EnterCriticalSection(&pChannelObj->m_adc.m_crit);
									CAList list;
									list.EnsureAllocated(1);
									pChannelObj->m_adc.StreamToArray(&list, 0, (unsigned char*)pszAllowEventBuffer, (unsigned char)pChannelObj->m_nServerBasis, 1);

									if((list.m_ppEvents)&&(list.m_nEventCount>0)&&(list.m_ppEvents[0]))
									{
										//reassign time, duration
										list.m_ppEvents[0]->m_ulOnAirTimeMS = (((unsigned long)pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal)%86400)*1000 + (unsigned long)(1000.0*(pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal-(double)((unsigned long)pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal)));
										list.m_ppEvents[0]->m_ulDurationMS =	(unsigned long)pChannelObj->m_ppLiveEvents[nIndex]->m_nDurationMS;
													
										pChannelObj->m_adc.EventToStream(list.m_ppEvents[0], 0, (unsigned char)pChannelObj->m_nServerBasis);

										int nLen = sizeof(revent);
										unsigned char* pch = NULL;
										if(pChannelObj->m_adc.m_VerifyEvent.Type == 0x01)
										{
			//AfxMessageBox("Extended Event");
											pch = (unsigned char*)malloc(nLen+pChannelObj->m_adc.m_usVerifyExtendedBufferLength+1);
											if(pch)
											{
			//foo.Format("extended event len %d %s", pChannelObj->m_adc.m_usVerifyExtendedBufferLength, pChannelObj->m_adc.m_pucVerifyExtendedEventBuffer+4);
			//AfxMessageBox(foo);
												memcpy(pch, &pChannelObj->m_adc.m_VerifyEvent, nLen);
												memcpy(pch+nLen, pChannelObj->m_adc.m_pucVerifyExtendedEventBuffer, pChannelObj->m_adc.m_usVerifyExtendedBufferLength);
												nLen += pChannelObj->m_adc.m_usVerifyExtendedBufferLength;
											}
											else
											{

											}
										}
										else
										{
			//AfxMessageBox("Non-extended Event");
											nLen-=3;
											pch = (unsigned char*)malloc(nLen+1);
											if(pch)
											{
												memcpy(pch, &pChannelObj->m_adc.m_VerifyEvent+3, nLen);
											}
											else
											{

											}
										}

										if(pChannelObj->ADCInsertEventBuffer(pch, nLen, pChannelObj->m_ppLiveEvents[nIndex]->m_nPosition-1)<INJECTOR_SUCCESS)
										{
											//error
											nIndex=INJECTOR_ERROR;
										}
										else
										{

				// then delete
											if(pChannelObj->ADCDeleteEvents(
												pChannelObj->m_ppLiveEvents[nIndex]->m_nPosition, 
												1+nNumSecondaries)<INJECTOR_SUCCESS)
											{
												//error
												nIndex=INJECTOR_ERROR;
											}
											else
											{

											}
										}
										if(pch) free(pch);
									}
					LeaveCriticalSection(&pChannelObj->m_adc.m_crit);
*/
								}
								else
								{

//								if(
//									  (pChannelObj->m_ppLiveEvents[nIndex]->m_pszEventID)&&(ppLastSchedule[i]->m_pszEventID)
//									&&(strcmp(pChannelObj->m_ppLiveEvents[nIndex]->m_pszEventID, ppLastSchedule[i]->m_pszEventID)==0)
//									&&(pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal >= ppLastSchedule[i]->m_dblOnAirTimeLocal - (double)(g_pinjector->m_settings.m_ulAutomationTimeToleranceMS)/1000.0)
//									&&(pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal <= ppLastSchedule[i]->m_dblOnAirTimeLocal + (double)(g_pinjector->m_settings.m_ulAutomationTimeToleranceMS)/1000.0)
//									&&(pChannelObj->m_ppLiveEvents[nIndex]->m_nDurationMS == ppLastSchedule[i]->m_nDurationMS)
//									&&(pChannelObj->m_ppLiveEvents[nIndex]->m_nSegment == ppLastSchedule[i]->m_nSegment)
//									&&(!(pChannelObj->m_ppLiveEvents[nIndex]->m_ulStatus&((1<<eventdone)|(1<<eventpostrolled)|(1<<eventrunning)|(1<<notplayed)|(1<<eventskipped))))  // not done or playing
//									&&(pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal > pChannelObj->m_dblAutomationTimeEstimate+(double)(g_pinjector->m_settings.m_nAdvanceSchedulingThresholdMinMins*60) )
//									&&(pszPrimaryEventBuffer)
//									)
//  was false, report it!


									if(
										  (!(pChannelObj->m_ppLiveEvents[nIndex]->m_ulStatus&((1<<eventdone)|(1<<eventpostrolled)|(1<<eventrunning)|(1<<notplayed)|(1<<eventskipped))))  // not done or playing
									  &&(pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal > pChannelObj->m_dblAutomationTimeEstimate+(double)(g_pinjector->m_settings.m_nAdvanceSchedulingThresholdMinMins*60) )
										)
									{

			if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
				g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Remove scheduled items", "%s schedule item [%s] @ %.3f was done. 0x%08x", 
					msgsrc,
					pChannelObj->m_ppLiveEvents[nIndex]->m_pszEventID, pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal,pChannelObj->m_ppLiveEvents[nIndex]->m_ulStatus
					);  //(Dispatch message)
										bSuppress = true;  // nothing to do so move on ahead.
									}
									else
									{


			if(g_pinjector->m_settings.m_ulDebug&(INJECTOR_DEBUG_SCHEDULE|INJECTOR_DEBUG_PROGRAMMING))	
				g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Remove scheduled items", "%s ERROR removing schedule item [%s] @ %.3f; %d %d %d %.3f, %d=?%d, %d=?%d, %d", 
					msgsrc,
					pChannelObj->m_ppLiveEvents[nIndex]->m_pszEventID, pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal,
					pChannelObj->m_ppLiveEvents[nIndex]->m_pszEventID, ppLastSchedule[i]->m_pszEventID,
					(strcmp(pChannelObj->m_ppLiveEvents[nIndex]->m_pszEventID, ppLastSchedule[i]->m_pszEventID)==0),
					ppLastSchedule[i]->m_dblOnAirTimeLocal,
					pChannelObj->m_ppLiveEvents[nIndex]->m_nDurationMS, ppLastSchedule[i]->m_nDurationMS,
					pChannelObj->m_ppLiveEvents[nIndex]->m_nSegment, ppLastSchedule[i]->m_nSegment, pszPrimaryEventBuffer
					);  //(Dispatch message)

										nIndex=INJECTOR_ERROR;
									}
								}
							}
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s leaving live crit 201", 
	msgsrc);  //(Dispatch message)
							LeaveCriticalSection(&pChannelObj->m_critLiveEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s left live crit 201", 
	msgsrc);  //(Dispatch message)

							if(nIndex>INJECTOR_ERROR) //if success
							{
								if(!bSuppress)
								{
									bDone = true;
									bDeleted = true;
								}
								// once deleted remove from array.

								ppLastSchedule[i]->m_pszCaller = (char*)malloc(64); if(ppLastSchedule[i]->m_pszCaller) strcpy(ppLastSchedule[i]->m_pszCaller, "last_schedule");

								try{delete ppLastSchedule[i];} catch(...){}
								nNumLastScheduleEvents--;

								while(i<nNumLastScheduleEvents)
								{
									ppLastSchedule[i] = ppLastSchedule[i+1];
									if(ppLastSchedule[i])
									{
										if(!(ppLastSchedule[i]->m_ulFlags&INJECTOR_FLAG_FOUND))
										{
											// more to delete.
											bDone = false;
										}
									}
									i++;
								}
								ppLastSchedule[i] = NULL;

								// here, traverse the list and coalesce any gaps between allow IDs.
							//	...TODO


							}
							//else - keep going, try the next one
							else
							{
								// couldnt delete... what should I do?
								// skip it for now, prob couldnt delete because it was manually overridden.
								// no scheduled event to update, so just go ahead
							}

						}
					}
					i++;
				}
				
			} 

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s mid-programming changes %d %d", 
											 msgsrc, bDone, bDeleted);  //(Dispatch message)

			if((!bDone)&&(!bDeleted))
			{
		  // INSERTIONS
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s checking insertions", 
											 msgsrc);  //(Dispatch message)

// this critsec moved to here, event crit sec must be outside schedule critsec
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entering live crit 202", 
	msgsrc);  //(Dispatch message)
								EnterCriticalSection(&pChannelObj->m_critLiveEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entered live crit 202", 
	msgsrc);  //(Dispatch message)


if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entering schedule crit 202", 
	msgsrc);  //(Dispatch message)
			EnterCriticalSection(&pChannelObj->m_critScheduleEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entered schedule crit 202", 
	msgsrc);  //(Dispatch message)
				if((pChannelObj->m_ppScheduleEvents)&&(pChannelObj->m_nNumScheduleEvents>0))
				{
					i=0;
					bool bRemaining = false;
					while(i<pChannelObj->m_nNumScheduleEvents)
					{
						if(pChannelObj->m_ppScheduleEvents[i])
						{
							if(
								  (!(pChannelObj->m_ppScheduleEvents[i]->m_ulStatus&INJECTOR_FLAG_INSERTED))
								 &&(pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal>pChannelObj->m_dblAutomationTimeEstimate+(double)(g_pinjector->m_settings.m_nAdvanceSchedulingThresholdMinMins*60))
								)
							{

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s insertion to be performed: %s", 
											 msgsrc, pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)");  //(Dispatch message)


								// insert this if possible.

								bool bSuccess = false;

								// first check if the spot is fully insertable.
								// the live event must be of the allow ID, and must completely encompass the event to be inserted.
								//  - actually better check the restriction table
								//  - ALSO, none of the allow events is allowed to be playing, have to check the live table.
								//  - if it is all clear in the restriction table, we then:
								// Make sure that the following non-allow event is a hard start - if not, make it so.
								// this is to prevent rippling when the allow events are adjusted.
								//  - not going to bother with this modification, as modifications are weird on Harris.
								//  - what we are going to do is create an insert buffer and replace everything that is not playing.
								// make a deletion list of any events completely emcompassed by the insert event.
								// make a modification/deletion list of the start overlap and end overlap events.
								//  - no mods, just delete, we will replace later.


								// new event buffer calc:
								// create new buffer:
								// check first allow event that overlaps with insert event.
								// create identical event with shorter duration to allow  (if dur < min, omit)
								// create insert event with hard 
								// add any secondaries (adjusting them if nec)
								// create additional allow event after hard start insertion item to take up rest of the time (if dur < min, omit)
								// insert this block.

								// delete the original first allow event, then any following events that are contained in the timespan of the above block
								// (have to count them)


								/*
// had to move this set of critsecs to ouside of schedule critsec so there is no nesting deadlock
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entering live crit 202", 
	msgsrc);  //(Dispatch message)
								EnterCriticalSection(&pChannelObj->m_critLiveEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s entered live crit 202", 
	msgsrc);  //(Dispatch message)

	*/
								// first check if the spot is fully insertable.
								// - actually better check the restriction table
								// the live event must be of the allow ID, and must completely encompass the event to be inserted.
								int nIndex = 	pChannelObj->IsRestricted(pChannelObj->m_ppScheduleEvents[i]);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s is %srestricted", 
											 msgsrc, pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 (nIndex==INJECTOR_NOT_RESTRICTED)?"not ":"");  //(Dispatch message)

								if(nIndex==INJECTOR_NOT_RESTRICTED)
								{
									// if it is all clear in the restriction table, we then: check if the allow is playing already - if so, out of luck.
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Programming", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_LIVE, INJECTOR_ARRAY_FIND_CONTAINED", msgsrc, pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal);
									nIndex = pChannelObj->FindScheduleItem(pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal, INJECTOR_ARRAY_LIVE, INJECTOR_ARRAY_FIND_CONTAINED);  // returns index
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Programming", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_LIVE, INJECTOR_ARRAY_FIND_CONTAINED returned %d", msgsrc, pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal, nIndex);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion index %d", 
											 msgsrc, pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 nIndex);  //(Dispatch message)
									if(nIndex>=0)
									{
										/*
										if(pChannelObj->m_ppLiveEvents[nIndex]->m_ulStatus&((1<<eventdone)|(1<<eventpostrolled)|(1<<eventrunning)|(1<<notplayed)|(1<<eventskipped)))
										{
											// can't = this event is passed.
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion prohibited: event status %08x on index %d", 
											 msgsrc, pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 pChannelObj->m_ppLiveEvents[nIndex]->m_ulStatus, nIndex);  //(Dispatch message)
										}
										else
										*/  //actually we will be ok, going to insert a hard start to cut off playing event!
										{
											// do this.
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion in event @ %d: assembling buffer", 
											 msgsrc, pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 pChannelObj->m_ppLiveEvents[nIndex]->m_nPosition);  //(Dispatch message)

//Sleep(500); // let it get there

											int q=0;
											int nNumItemsToInsert = 0;
											int nNumItemsToDelete = 0;
											int nIndexOfAllowBegin = nIndex;
											int nFirstIndexToDelete = -1;
											int nLastIndexToDelete = -1;
											int nDeletePosition = -1;
											bool bSimulStart=false;
											if(pChannelObj->m_nNumLiveEvents>nIndex+1)
											{
												nIndex++;
												nFirstIndexToDelete = nIndex;
												nDeletePosition = pChannelObj->m_ppLiveEvents[nIndex]->m_nPosition;
											}
											else
											{
												// last item!
												if(pChannelObj->m_nNumLiveEvents>0)
												{
													nDeletePosition = (pChannelObj->m_ppLiveEvents[pChannelObj->m_nNumLiveEvents-1]->m_nPosition)+1; // puts at end
												}
												else
												{// no items?  can;t insert I think!
													nDeletePosition=0;
												}
											}
//											int nTotalDurationMS = pChannelObj->m_ppLiveEvents[nIndex]->m_nDurationMS;
											if(
														(pChannelObj->m_ppLiveEvents[nIndexOfAllowBegin]->m_dblOnAirTimeLocal<pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal+.00001)
													&&(pChannelObj->m_ppLiveEvents[nIndexOfAllowBegin]->m_dblOnAirTimeLocal>pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal-.00001)
												)
											{
												// equivalent.
												bSimulStart=true;
												// reassign
												nFirstIndexToDelete = nIndexOfAllowBegin;
												nDeletePosition = pChannelObj->m_ppLiveEvents[nIndexOfAllowBegin]->m_nPosition;
											}
											else
											{
												// check threshold
												if(
														pChannelObj->m_ppLiveEvents[nIndexOfAllowBegin]->m_dblOnAirTimeLocal >
														(pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal-((double)(g_pinjector->m_settings.m_nMinSchedulingThresholdMS))/1000.0)
													)
												{


if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING)
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule insert threshold check", "%s item %d [%d](%.3f, %d) was too close to allow live event [%s](%.3f)", 
											 msgsrc, i, pChannelObj->m_ppScheduleEvents[i]->m_pszEventID, pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal,pChannelObj->m_ppScheduleEvents[i]->m_nDurationMS,
											 pChannelObj->m_ppLiveEvents[nIndexOfAllowBegin]->m_pszEventID,pChannelObj->m_ppLiveEvents[nIndexOfAllowBegin]->m_dblOnAirTimeLocal
											 );

													// have to move this down, it is too close.
													pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal = 
														pChannelObj->m_ppLiveEvents[nIndexOfAllowBegin]->m_dblOnAirTimeLocal+((double)(g_pinjector->m_settings.m_nMinSchedulingThresholdMS))/1000.0;
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING)
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule insert threshold check", "%s item %d, id=%d [%d](%.3f, %d) was rescheduled to obey minimum threshold.", 
											 msgsrc, i, pChannelObj->m_ppScheduleEvents[i]->m_nID,
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID, 
											 pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal,
											 pChannelObj->m_ppScheduleEvents[i]->m_nDurationMS
											 );

													// have to update the db as well.

								// update db
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET scheduled_on = %.3f WHERE itemid = %d",
											((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
											pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal,
											pChannelObj->m_ppScheduleEvents[i]->m_nID
										);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING)||(g_pinjector->m_settings.m_bDebugSQL))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule insert status update", "%s updating item %d (%.3f, %d)\r\nSQL: %s", 
msgsrc, i, pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal,pChannelObj->m_ppScheduleEvents[i]->m_nDurationMS, szSQL);  //(Dispatch message)
									if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
									{
										//error
		g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "InjectorProgrammingThread", "%s error updating item: %s", 
			msgsrc, 
			errorstring);  //(Dispatch message)
									}


												}
											}

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s entering adc critsec +", msgsrc);
					EnterCriticalSection(&pChannelObj->m_adc.m_crit);
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s entered adc critsec +", msgsrc);
											CAList list;
											list.EnsureAllocated(2 + nNumSecondaries); // 3 is allow, insert, allow. but 2 is forget the first allow, we cutting off existing one.
/*
											if(((int)(1000.0*(pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal - pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal)))>pChannelObj->m_nSettingMinBifurcationDurMS)
											{
FILE* fp;
fp=fopen("allowbuf.lst", "wb");
if(fp)
{
	fwrite(pszAllowEventBuffer, 104, 1, fp);
	fclose(fp);
}

												list.m_nEventCount = pChannelObj->m_adc.StreamToArray(&list, q, (unsigned char*)pszAllowEventBuffer, (unsigned char)pChannelObj->m_nServerBasis, 1);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s StreamToArray: buffer params array %d cnt %d, q %d, item %d, id = %s", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 list.m_ppEvents, list.m_nEventCount, q, list.m_ppEvents[q],list.m_ppEvents[q]->m_pszID);  //(Dispatch message)
												if(
														(list.m_ppEvents)
													&&(list.m_nEventCount>q)
													&&(list.m_ppEvents[q])
													)
												{
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s StreamToArray: reassigning time and dur", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)");  //(Dispatch message)
													//reassign time, duration
													list.m_ppEvents[q]->m_ulOnAirTimeMS = (((unsigned long)pChannelObj->m_ppLiveEvents[i]->m_dblOnAirTimeLocal)%86400)*1000 + (unsigned long)(1000.0*(pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal-(double)((unsigned long)pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal)));
													list.m_ppEvents[q]->m_ulDurationMS =	(unsigned long)(1000.0*(pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal - pChannelObj->m_ppLiveEvents[nIndex]->m_dblOnAirTimeLocal));
													q++;
													nNumItemsToInsert++;
												}
											}
											*/

											// we will insert a hard start.

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion: buffer params array %d cnt %d, q %d, item %d; pribuf %d", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 list.m_ppEvents, list.m_nEventCount, q, list.m_ppEvents[q],
											 pszPrimaryEventBuffer);  //(Dispatch message)

//Sleep(500);

											if(list.m_ppEvents) 
											{
												list.m_nEventCount+=pChannelObj->m_adc.StreamToArray(&list, q, (unsigned char*)pszPrimaryEventBuffer, (unsigned char)pChannelObj->m_nServerBasis, 1);

												if((list.m_nEventCount>q)&&(list.m_ppEvents[q]))
												{
													//reassign time, duration
													list.m_ppEvents[q]->m_ulOnAirTimeMS = (unsigned long)((
														  pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal 
														- (
														    (double)( (((int)(pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal)/86400)*86400) )
															)
																																								)*1000.0);


													list.m_ppEvents[q]->m_ulDurationMS =	(unsigned long)(pChannelObj->m_ppScheduleEvents[i]->m_nDurationMS);
													// ALSO SOM!

													if(pChannelObj->m_ppScheduleEvents[i]->m_nSOMMS>=0)
													{
														list.m_ppEvents[q]->m_ulSOMMS =	(unsigned long)(pChannelObj->m_ppScheduleEvents[i]->m_nSOMMS);
													}
													else
													{
														list.m_ppEvents[q]->m_ulSOMMS =	TIME_NOT_DEFINED;
													}

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion: on air: %d, dur %d", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 list.m_ppEvents[q]->m_ulOnAirTimeMS,list.m_ppEvents[q]->m_ulDurationMS);  //(Dispatch message)

													// reassign ID!
													if(pChannelObj->m_ppScheduleEvents[i]->m_pszEventID)
													{
														list.m_ppEvents[q]->m_pszID = (char*)malloc(strlen(pChannelObj->m_ppScheduleEvents[i]->m_pszEventID)+1);
														if(list.m_ppEvents[q]->m_pszID) strcpy(list.m_ppEvents[q]->m_pszID, pChannelObj->m_ppScheduleEvents[i]->m_pszEventID);
													}
													else
													{
														list.m_ppEvents[q]->m_pszID = (char*)malloc(6);
														if(list.m_ppEvents[q]->m_pszID) strcpy(list.m_ppEvents[q]->m_pszID, "error");
													}

													if(g_pinjector->m_settings.m_bInsertionTitleReplace)
													{
														// replace the title too.
														if(pChannelObj->m_ppScheduleEvents[i]->m_pszEventTitle)
														{
															list.m_ppEvents[q]->m_pszTitle = (char*)malloc(strlen(pChannelObj->m_ppScheduleEvents[i]->m_pszEventTitle)+1);
															if(list.m_ppEvents[q]->m_pszTitle) strcpy(list.m_ppEvents[q]->m_pszTitle, pChannelObj->m_ppScheduleEvents[i]->m_pszEventTitle);
														}
													}

														
													if((pszPrimaryEventBuffer)&&(nPriBufLen>sizeof(revent)))  // want to force an extended event.
													{
														list.m_ppEvents[q]->m_pContextData = new bool;
													}
													else
													{
														if(list.m_ppEvents[q]->m_pContextData)
														{
															try {delete(list.m_ppEvents[q]->m_pContextData);} catch(...){}
															list.m_ppEvents[q]->m_pContextData = NULL;
														}

													}

													nIndex = q;
													q++;
													nNumItemsToInsert++;
												}
											// secondaries here.
//	already done											pChannelObj->m_adc.CountEventsInStream(pszSecondaryEventsBuffer, nSecBufLen );

												if(/*(list.m_nEventCount>q+nNumSecondaries)&&*/(pszSecondaryEventsBuffer))
												{
													list.m_nEventCount+=pChannelObj->m_adc.StreamToArray(&list, q, (unsigned char*)pszSecondaryEventsBuffer, (unsigned char)pChannelObj->m_nServerBasis, nNumSecondaries);
													// then adjust the times etc of the secondaries according to the options.
													//TODO

													// for now, inserts as is.


													q+=nNumSecondaries;
													nNumItemsToInsert+=nNumSecondaries;
												}

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Programming", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_LIVE, INJECTOR_ARRAY_FIND_CONTAINED", msgsrc, pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal+((double)pChannelObj->m_ppScheduleEvents[i]->m_nDurationMS)/1000.0);
												// then the last item 
												nLastIndexToDelete = pChannelObj->FindScheduleItem(
													pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal+((double)pChannelObj->m_ppScheduleEvents[i]->m_nDurationMS)/1000.0, 
													INJECTOR_ARRAY_LIVE, INJECTOR_ARRAY_FIND_CONTAINED);  // returns index
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Programming", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_LIVE, INJECTOR_ARRAY_FIND_CONTAINED returned %d", msgsrc, pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal+((double)pChannelObj->m_ppScheduleEvents[i]->m_nDurationMS)/1000.0, nLastIndexToDelete);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion: endpoint index %d, count =%d", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 nLastIndexToDelete, list.m_nEventCount);  //(Dispatch message)

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion: count =%d, q %d, array %d, index=%d, item %d", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 list.m_nEventCount, q, list.m_ppEvents[q], nIndex, list.m_ppEvents[nIndex]);  //(Dispatch message)

												if(nLastIndexToDelete>=0)
												{
													//we are in one.
													list.m_nEventCount+=pChannelObj->m_adc.StreamToArray(&list, q, (unsigned char*)pszAllowEventBuffer, (unsigned char)pChannelObj->m_nServerBasis, 1);
								//						list.m_ppEvents[q]->m_ulOnAirTimeMS = (((unsigned long)pChannelObj->m_ppLiveEvents[nLastIndexToDelete]->m_dblOnAirTimeLocal)%86400)*1000 + (unsigned long)(1000.0*(pChannelObj->m_ppLiveEvents[nLastIndexToDelete]->m_dblOnAirTimeLocal-(double)((unsigned long)pChannelObj->m_ppLiveEvents[nLastIndexToDelete]->m_dblOnAirTimeLocal)));
								//						list.m_ppEvents[q]->m_ulDurationMS =	(unsigned long)(pChannelObj->m_ppLiveEvents[nLastIndexToDelete]->m_nDurationMS);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion: CHECK count =%d, q %d, array %d, index=%d, item %d", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 list.m_nEventCount, q, list.m_ppEvents[q], nIndex, list.m_ppEvents[nIndex]);  //(Dispatch message)

													if((list.m_nEventCount>q)&&(list.m_ppEvents[q])&&(list.m_ppEvents[nIndex]))
													{
														list.m_ppEvents[q]->m_ulOnAirTimeMS = list.m_ppEvents[nIndex]->m_ulOnAirTimeMS + list.m_ppEvents[nIndex]->m_ulDurationMS;  //starts after the index item
//														list.m_ppEvents[q]->m_ulDurationMS = (((unsigned long)pChannelObj->m_ppLiveEvents[nLastIndexToDelete]->m_dblOnAirTimeLocal)%86400)*1000 + (unsigned long)(1000.0*(pChannelObj->m_ppLiveEvents[nLastIndexToDelete]->m_dblOnAirTimeLocal-(double)((unsigned long)pChannelObj->m_ppLiveEvents[nLastIndexToDelete]->m_dblOnAirTimeLocal)))+(unsigned long)pChannelObj->m_ppLiveEvents[nLastIndexToDelete]->m_nDurationMS - list.m_ppEvents[q]->m_ulOnAirTimeMS;
														int nDur =	(int)((
															  pChannelObj->m_ppLiveEvents[nLastIndexToDelete]->m_dblOnAirTimeLocal 
															+ (double)(pChannelObj->m_ppLiveEvents[nLastIndexToDelete]->m_nDurationMS)/1000.0
														  - (double)(
																         ((int)(pChannelObj->m_ppLiveEvents[nLastIndexToDelete]->m_dblOnAirTimeLocal)/86400
																         )*86400
																				)
																             )*1000.0) - list.m_ppEvents[q]->m_ulOnAirTimeMS;
														

														if((pszAllowEventBuffer)&&(nAllowBufLen>sizeof(revent)))  // want to force an extended event.
														{
															list.m_ppEvents[q]->m_pContextData = new bool;
														}
														else
														{
															if(list.m_ppEvents[q]->m_pContextData)
															{
																try {delete(list.m_ppEvents[q]->m_pContextData);} catch(...){}
																list.m_ppEvents[q]->m_pContextData = NULL;
															}

														}

														while(nDur>86399000) // the mod above may make this one day off - but no events should ever be larger than 24 hours!
														{
															nDur-=86400000;
														}
														if(nDur > 0) //pChannelObj->m_nSettingMinBifurcationDurMS) //TODO - minlength stuff!
														{

															list.m_ppEvents[q]->m_ulDurationMS = (unsigned long)nDur;

//															// force odd-numbered duration.
//															if( (((unsigned char)(((nDur%1000L)/33)))&0xff)%2 == 0 )  //(int)(1000.0/30.0) = 33
//															{
//																list.m_ppEvents[q]->m_ulDurationMS+=34;  //33.33333
//															}


															q++;
															nNumItemsToInsert++;
														}
														// else it's too short, don't want to insert it.
														else
														{
															// what we really want to do is
															// see if there is a next event that is an allow event.
															// if so, absorb that one in a new next event,
															// if that event is too long (max dur) then bifurcate it and add 2.
															// TODO
														}
													}

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion: indices first %d, last %d, allow %d", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 nFirstIndexToDelete,nLastIndexToDelete,nIndexOfAllowBegin);  //(Dispatch message)


													if((nFirstIndexToDelete<0)||(nLastIndexToDelete<=nIndexOfAllowBegin))
													{
														// else nothing to delete.
														nNumItemsToDelete = 0;
													}
													else
													if(nLastIndexToDelete == nFirstIndexToDelete)
													{
														// was one item
														nNumItemsToDelete = 1;
														//TODO = delete any secondaries these might have.
													}
													else
													{
														nNumItemsToDelete = pChannelObj->m_ppLiveEvents[nLastIndexToDelete]->m_nPosition - pChannelObj->m_ppLiveEvents[nFirstIndexToDelete]->m_nPosition +1;
														//TODO = delete any secondaries these might have.
													}
												}
												else
												{
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Programming", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_LIVE, INJECTOR_ARRAY_FIND_AFTER", msgsrc, pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal+((double)pChannelObj->m_ppScheduleEvents[i]->m_nDurationMS)/1000.0);
													nLastIndexToDelete = pChannelObj->FindScheduleItem(
														pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal+((double)pChannelObj->m_ppScheduleEvents[i]->m_nDurationMS)/1000.0, 
														INJECTOR_ARRAY_LIVE, INJECTOR_ARRAY_FIND_AFTER);  // returns index

if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_COMPARE)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Programming", "%s FindScheduleItem with %.3f INJECTOR_ARRAY_LIVE, INJECTOR_ARRAY_FIND_AFTER returned %d", msgsrc, pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal+((double)pChannelObj->m_ppScheduleEvents[i]->m_nDurationMS)/1000.0, nLastIndexToDelete);

													if((nFirstIndexToDelete<0)||(nLastIndexToDelete<=nIndexOfAllowBegin))
													{
														// else nothing to delete.
														nNumItemsToDelete = 0;
													}
													else
													if(nLastIndexToDelete>=0)
													{
														// delete all between but not including.
											//			nLastIndexToDelete--;
									//					nNumItemsToDelete = nLastIndexToDelete - nFirstIndexToDelete +1;

														// pos - 1 to get the previous items plus secondaries.
														nNumItemsToDelete = (pChannelObj->m_ppLiveEvents[nLastIndexToDelete]->m_nPosition-1) - pChannelObj->m_ppLiveEvents[nFirstIndexToDelete]->m_nPosition +1;
														//TODO = delete any secondaries these might have.

													}
													else
													{
													// nothing found after.
														// delete all after this one.
														nLastIndexToDelete = pChannelObj->m_nNumLiveEvents-1;
												//		nNumItemsToDelete = nLastIndexToDelete - nFirstIndexToDelete +1;

														if((nFirstIndexToDelete<0)||(nLastIndexToDelete<=nIndexOfAllowBegin))
														{
															// else nothing to delete.
															nNumItemsToDelete = 0;
														}
														else
														if(nLastIndexToDelete == nFirstIndexToDelete)
														{
															// was one item
															nNumItemsToDelete = 1;
															//TODO = delete any secondaries these might have.
														}
														else
														{
															nNumItemsToDelete = pChannelObj->m_ppLiveEvents[nLastIndexToDelete]->m_nPosition - pChannelObj->m_ppLiveEvents[nFirstIndexToDelete]->m_nPosition +1;
														}
														//TODO = delete any secondaries these might have.

													}
												}

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion: num to insert %d; deletions %d to %d (%d)", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 nNumItemsToInsert, nFirstIndexToDelete, nLastIndexToDelete, nNumItemsToDelete);  //(Dispatch message)

												//now create an insert buffer.
												unsigned char* pch = NULL;
												int nLen = 0;
												int ins=0;
												while(ins < nNumItemsToInsert)
												{
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s check before EventToStream, converting %s", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 list.m_ppEvents[ins]->m_pszID);  //(Dispatch message)

													pChannelObj->m_adc.InitEvent();

													if(list.m_ppEvents[ins]->m_pContextData)
													{
														if(pChannelObj->m_adc.EventToStream(list.m_ppEvents[ins], ADC_FORCE_EXTENDED, (unsigned char)pChannelObj->m_nServerBasis)<=ADC_ERROR)
														{
															//report
g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "InjectorProgrammingThread", "%s error converting %s with EventToStream 3", 
									 msgsrc, 
									 list.m_ppEvents[ins]->m_pszID?list.m_ppEvents[ins]->m_pszID:"(null)");  //(Dispatch message)

														}
													}
													else
													{
														if(pChannelObj->m_adc.EventToStream(list.m_ppEvents[ins], 0, (unsigned char)pChannelObj->m_nServerBasis)<=ADC_ERROR)
														{
															//report
g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "InjectorProgrammingThread", "%s error converting %s with EventToStream 4", 
									 msgsrc, 
									 list.m_ppEvents[ins]->m_pszID?list.m_ppEvents[ins]->m_pszID:"(null)");  //(Dispatch message)

														}
													}

													//prevent freeing on event going out of scope, just want to use the values.

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s check after EventToStream",// %s", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)" );
											 //, pChannelObj->m_adc.m_VerifyEvent.eid);  //(Dispatch message)  removed EID, nulls mess it up
													
													if(pChannelObj->m_adc.m_VerifyEvent.Type == 0x01)
													{
														int nEventLen=sizeof(revent);
						//AfxMessageBox("Extended Event");
														unsigned char* pchNew = (unsigned char*)malloc(nLen+nEventLen+ pChannelObj->m_adc.m_usVerifyExtendedBufferLength+1);
														if(pchNew)
														{
						//foo.Format("extended event len %d %s", pChannelObj->m_adc.m_usVerifyExtendedBufferLength, pChannelObj->m_adc.m_pucVerifyExtendedEventBuffer+4);
						//AfxMessageBox(foo);
															if(pch){memcpy(pchNew, pch, nLen); free(pch);}
															memcpy(pchNew+nLen, (char*)(&pChannelObj->m_adc.m_VerifyEvent), nEventLen);
															if(pChannelObj->m_adc.m_pucVerifyExtendedEventBuffer)
															{
																memcpy(pchNew+nLen+nEventLen, pChannelObj->m_adc.m_pucVerifyExtendedEventBuffer, pChannelObj->m_adc.m_usVerifyExtendedBufferLength);
																nLen += nEventLen+pChannelObj->m_adc.m_usVerifyExtendedBufferLength;
															}
															pch = pchNew;

														}
														else
														{
//TODO report error
														}
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion: extended buffer item %d, total buflen %d", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 ins, nLen);  //(Dispatch message)
													}
													else
													{
						//AfxMessageBox("Non-extended Event");
														int nEventLen=sizeof(revent)-3;
														unsigned char* pchNew = (unsigned char*)malloc(nLen+nEventLen+1);
														if(pchNew)
														{

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion: len = %d,%d, bufs %d,%d, buffer+16 [%s]", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 nLen, nEventLen,pch,pchNew,
											 (char*)(&pChannelObj->m_adc.m_VerifyEvent)+19);  //(Dispatch message)

															if(pch)
															{
																memcpy(pchNew, pch, nLen); 
																free(pch);
															}

															memcpy(pchNew+nLen, (char*)(&pChannelObj->m_adc.m_VerifyEvent)+3, nEventLen);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion: copy buffer+16 [%s]", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 (char*)pchNew+nLen+16);  //(Dispatch message)

/*
FILE* fp;
char filename[260];
sprintf(filename, "buf%d.lst", ins);
fp=fopen(filename, "wb");
if(fp)
{
	fwrite(pchNew+nLen, nEventLen, 1, fp);
	fclose(fp);
}
*/
															nLen += nEventLen;
															pch = pchNew;
														}
														else
														{
//TODO report error

														}
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s insertion: buffer item %d, total buflen %d, %s", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											 ins, nLen, pChannelObj->m_adc.m_VerifyEvent.eid);  //(Dispatch message)

													}
													ins++;
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s check loop", 
											 msgsrc, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)");  //(Dispatch message)

												}
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
{

FILE* fp;
fp=fopen("injectorinsertstream.lst", "wb");
if(fp)
{
	fwrite(pch, nLen, 1, fp);
	fclose(fp);
}
}
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
{
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s preparing to insert %d bytes now", 
									 msgsrc, nLen);  //(Dispatch message)
//Sleep(500); // let it get there.
}

						// then insert
												if(pChannelObj->ADCInsertEventBuffer(pch, nLen, nDeletePosition)<INJECTOR_SUCCESS)
												{
													//error
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s insertion unsuccesful", 
											 msgsrc/*, 
											 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)"*/);  //(Dispatch message)

//Sleep(500); // let it get there.

	EnterCriticalSection(&pChannelObj->m_critServerInfo);
										sprintf(errorstring, "%s:%d Programming", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);
										unsigned char h,m,s,f;
										pChannelObj->m_adc.ConvertMillisecondsToHMSF(
											(unsigned long)((pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal - ( (double)( (((int)(pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal)/86400)*86400) )	)	)*1000.0),
											&h, &m, &s, &f,pChannelObj->m_nServerBasis);

										g_pinjector->SendAsRunMsg(CX_SENDMSG_ERROR, pChannelObj->m_nChannelID, pChannelObj->m_ppScheduleEvents[i]->m_pszEventID, errorstring, 
											"Error inserting schedule item %s at %02d:%02d:%02d.%02d%s%s", 
											pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											h,m,s,f,
											pChannelObj->m_pszDesc?" on ":"",
											pChannelObj->m_pszDesc?pChannelObj->m_pszDesc:""
											);

												}
												else
												{
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
{
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s preparing to delete %d @ %d", 
									 msgsrc, 
									 nNumItemsToDelete, nDeletePosition+nNumItemsToInsert);  //(Dispatch message)
//Sleep(500); // let it get there.
}


	if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s deleting %d items", 
												 msgsrc, 
												 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)", nNumItemsToDelete);  //(Dispatch message)
													if(nNumItemsToDelete>0)
													{

						// then delete
														if(pChannelObj->ADCDeleteEvents(
															nDeletePosition+nNumItemsToInsert, 
															nNumItemsToDelete)<INJECTOR_SUCCESS)
														{
															//error
	if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s %s deletion unsuccesful", 
												 msgsrc, 
												 pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)");  //(Dispatch message)

	EnterCriticalSection(&pChannelObj->m_critServerInfo);
										sprintf(errorstring, "%s:%d Programming", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);
										unsigned char h,m,s,f;
										pChannelObj->m_adc.ConvertMillisecondsToHMSF(
											(unsigned long)((pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal - ( (double)( (((int)(pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal)/86400)*86400) )	)	)*1000.0),
											&h, &m, &s, &f,pChannelObj->m_nServerBasis);
	

										g_pinjector->SendAsRunMsg(CX_SENDMSG_ERROR, pChannelObj->m_nChannelID, pChannelObj->m_ppScheduleEvents[i]->m_pszEventID, errorstring, 
											"Error deleting displaced items while inserting schedule item %s at %02d:%02d:%02d.%02d%s%s", 
											pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
											h,m,s,f,
											pChannelObj->m_pszDesc?" on ":"",
											pChannelObj->m_pszDesc?pChannelObj->m_pszDesc:""
											);

														}
														else
														{
															bSuccess = true;
														}
													}
													else bSuccess = true;
												}
												if(pch){ try{ free(pch);} catch(...){} }
												pch = NULL;

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
{
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s done with insertion process", 
									 msgsrc);  //(Dispatch message)
//Sleep(500); // let it get there.
}

											//	list.m_ppEvents[nIndex]->m_pszID = NULL; // null so it doesn't get freed.  we just used the other event's ID buffer temporarily.

											}

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s leaving adc critsec +", msgsrc);
					LeaveCriticalSection(&pChannelObj->m_adc.m_crit);
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s left adc critsec +", msgsrc);

										}

									}// else not there... ummm... not sure. weird.  can't happen I think.

/*
									nIndex = pChannelObj->FindNextNonAllow(pChannelObj->m_ppScheduleEvents[i]);
									if(nIndex>=0)
									{
										// check it.  
								// Make sure that the following non-allow event is a hard start - if not, make it so.
								// this is to prevent rippling when the allow events are adjusted.

										if(!(pChannelObj->m_ppLiveEvents[nIndex]->m_ulControl & (1<<autotimed)))
										{
										}
									}
*/
//  here have to continue further

								}
								else bSuccess = false;

// had to move this outside of schedule critsec so no nesting deadlock
								/*
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s leaving live crit 202", 
	msgsrc);  //(Dispatch message)
								LeaveCriticalSection(&pChannelObj->m_critLiveEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s left live crit 202", 
	msgsrc);  //(Dispatch message)
*/
								// if success:
								if(bSuccess)
								{

	EnterCriticalSection(&pChannelObj->m_critServerInfo);
									sprintf(errorstring, "%s:%d Programming", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);
									unsigned char h,m,s,f;

//			EnterCriticalSection(&pChannelObj->m_critScheduleEvents); // already in
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s entering adc critsec -", msgsrc);
					EnterCriticalSection(&pChannelObj->m_adc.m_crit);
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s entered adc critsec -", msgsrc);
									pChannelObj->m_adc.ConvertMillisecondsToHMSF(
										(unsigned long)((pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal - ( (double)( (((int)(pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal)/86400)*86400) )	)	)*1000.0),
										&h, &m, &s, &f,pChannelObj->m_nServerBasis);
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s leaving adc critsec -", msgsrc);
					LeaveCriticalSection(&pChannelObj->m_adc.m_crit);
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s left adc critsec -", msgsrc);

									g_pinjector->SendAsRunMsg(CX_SENDMSG_INFO, pChannelObj->m_nChannelID, pChannelObj->m_ppScheduleEvents[i]->m_pszEventID, errorstring, 
										"Inserted schedule item %s at %02d:%02d:%02d.%02d%s%s", 
										pChannelObj->m_ppScheduleEvents[i]->m_pszEventID?pChannelObj->m_ppScheduleEvents[i]->m_pszEventID:"(null)",
										h,m,s,f,
										pChannelObj->m_pszDesc?" on ":"",
										pChannelObj->m_pszDesc?pChannelObj->m_pszDesc:""
										);

									pChannelObj->m_ppScheduleEvents[i]->m_ulStatus |= INJECTOR_FLAG_INSERTED;
									bDone = true;
									bInserted = true;

								// update db
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d WHERE itemid = %d",
											((g_pinjector->m_settings.m_pszSchedule)&&(strlen(g_pinjector->m_settings.m_pszSchedule)))?g_pinjector->m_settings.m_pszSchedule:"Schedule",
											pChannelObj->m_ppScheduleEvents[i]->m_ulStatus,
											pChannelObj->m_ppScheduleEvents[i]->m_nID
										);

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING)||(g_pinjector->m_settings.m_bDebugSQL))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "Schedule insert status update", "%s updating item %d (%.3f, %d)\r\nSQL: %s", 
msgsrc, i, pChannelObj->m_ppScheduleEvents[i]->m_dblOnAirTimeLocal,pChannelObj->m_ppScheduleEvents[i]->m_nDurationMS, szSQL);  //(Dispatch message)
									if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
									{
										//error
		g_pinjector->m_msgr.DM(MSG_ICONERROR, NULL, "InjectorProgrammingThread", "%s error updating item: %s", 
			msgsrc, 
			errorstring);  //(Dispatch message)
									}

							

									while(i<pChannelObj->m_nNumScheduleEvents)
									{
										if(pChannelObj->m_ppScheduleEvents[i])
										{
											if(!(pChannelObj->m_ppScheduleEvents[i]->m_ulStatus&INJECTOR_FLAG_INSERTED))
											{
												// more to insert.
												bDone = false;
												bRemaining = true;
											}
										}
										i++;
									}
//			LeaveCriticalSection(&pChannelObj->m_critScheduleEvents);  already in 

								}
							//	else - nothing we just skip it
								else
								{
									// couldnt insert... what should I do?
									// skip it for now, prob couldnt insert because it was manually overridden, either by newly disallowed spots or whatever.
									// just go ahead, no need to update, it will get retried later.
								}

							}
						}
						i++;
					}
					if(!bRemaining) bDone = true;
				}
				else
				{
					bDone = true;
				}
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s leaving schedule crit 202", 
	msgsrc);  //(Dispatch message)
			LeaveCriticalSection(&pChannelObj->m_critScheduleEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s left schedule crit 202", 
	msgsrc);  //(Dispatch message)

// had to move this to outseide schedule critsec so no nesting deadlock
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s leaving live crit 202", 
	msgsrc);  //(Dispatch message)
								LeaveCriticalSection(&pChannelObj->m_critLiveEvents);
if(g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_CRITSEC)	
	g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "CritSec", "%s left live crit 202", 
	msgsrc);  //(Dispatch message)

			}

			if((bDeleted)||(bInserted))
			{
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s item was %s %s", 
											 msgsrc, bInserted?"inserted":"", bDeleted?"deleted":"");  //(Dispatch message)
				bChangesPending  = false; // this will get refreshed when we load in the changes from the harris after the edit.
			}

			if(bDone)// means the entire stack was processed.
			{
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s DONE with programming changes", 
											 msgsrc);  //(Dispatch message)
				bChangesPending    = false;
				bLiveEventsChanged = false;
				bScheduleChangeProcessed = true;

				// if we are done, we can now wipe the old schedule buffer
				if((nNumLastScheduleEvents>0)&&(ppLastSchedule))
				{
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s CLEARING LOCAL SCHEDULE",msgsrc );
					int i=0;
					while(i<nNumLastScheduleEvents)
					{
						if(ppLastSchedule[i])
						{
if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s Deleting %d->%d",msgsrc,ppLastSchedule[i], ppLastSchedule[i]->m_pszEventID);

							ppLastSchedule[i]->m_pszCaller = (char*)malloc(64); if(ppLastSchedule[i]->m_pszCaller) strcpy(ppLastSchedule[i]->m_pszCaller, "done_last_schedule");
							try {delete ppLastSchedule[i];} catch(...){}
						}
						i++;
					}
					try {delete [] ppLastSchedule;} catch(...){}
				}
				nNumLastScheduleEvents = 0;
				ppLastSchedule = NULL;

			}
		}// if(
		//		&&(	pChannelObj->m_dblNextOnAir >0.0 )  // but don't do this if there is not enough time to do it, or if there is not any live action going on.
		//		&&(	((int)((pChannelObj->m_dblNextOnAir - pChannelObj->m_dblAutomationTimeEstimate)*1000.0)) > g_pinjector->m_settings.m_nProgramAutomationSafetyMS )  // but don't do this if there is not enough time to do it, or if there is not any live action going on.
		//		)
		// this means, we wither find nothing coming up on air next. or there is not enough time to the end of the clip

		else  // let's just log the fact
		{
			if(dblLastSuppressReport<dblNow)
			{
				dblLastSuppressReport = dblNow+((double)(g_pinjector->m_settings.m_nAutomationIntervalMS))/1000.0;

if((g_pinjector->m_settings.m_ulDebug&INJECTOR_DEBUG_PROGRAMMING))
g_pinjector->m_msgr.DM(MSG_ICONHAND, NULL, "InjectorProgrammingThread", "%s have to suppress programming changes, next=%3.f, countdown to next =%d ms (vs %d ms) last=%d.%03d+%dms->%.3f", 
											 msgsrc, pChannelObj->m_dblNextOnAir,
											 (pChannelObj->m_dblNextOnAir>0)?((int)((pChannelObj->m_dblNextOnAir - pChannelObj->m_dblAutomationTimeEstimate)*1000.0)):-1 ,
												g_pinjector->m_settings.m_nProgramAutomationSafetyMS,
												pChannelObj->m_timebLastProgramTick.time, pChannelObj->m_timebLastProgramTick.millitm,
												g_pinjector->m_settings.m_nProgramAutomationDefeatMS,
												dlbMinTriggerNext);  //(Dispatch message)
			}
			Sleep(100);  // and give it a bit of a break
		}

		}// 		if(
		//		(bChangesPending)
		//	&&(
		//			(dblNow>dblPending+((double)(g_pinjector->m_settings.m_nProgramAutomationDwellMS))/1000.0) // pending time elapsed
		//		||(dblNow>dblChange+((double)(g_pinjector->m_settings.m_nProgramAutomationForceMS))/1000.0) // forcing time elapsed
		//		)
		//	)
		// this means if it is time to do after a pending change, of if we are forcing it.

		}//if(g_pinjector->m_settings.m_bAutoSchedulerMode)

		Sleep(30);// do not peg
	}	//while
	
	if(pChannelObj->m_socketSentinel != NULL)
	{

		pChannelObj->m_net.CloseConnection(pChannelObj->m_socketSentinel);
		pChannelObj->m_socketSentinel=NULL;
		bSocketErrorReported = false; //reset
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Control client connection to %s:%d closed", szControlHost, usControlPort);//, g_pinjector->m_settings.m_pszSentinelHost, g_pinjector->m_settings.m_usSentinelPort); 
		g_pinjector->m_msgr.DM(MSG_ICONNONE, NULL, msgsrc, errorstring);  //(Dispatch message)
		g_pinjector->SendMsg(CX_SENDMSG_INFO, msgsrc, errorstring);
	}

	db.RemoveConnection(pdbConn);
	Sleep(30);

	EnterCriticalSection(&pChannelObj->m_critServerInfo);
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending programming thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	LeaveCriticalSection(&pChannelObj->m_critServerInfo);

	g_pinjector->m_msgr.DM(MSG_ICONINFO, NULL, "InjectorProgrammingThread", errorstring);    //(Dispatch message)
	pChannelObj->m_bProgrammingThreadStarted=false;
	
	_endthread();
//	Sleep(150);
}



