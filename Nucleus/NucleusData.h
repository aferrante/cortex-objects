// NucleusData.h: interface for the CNucleusData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NUCLEUSDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
#define AFX_NUCLEUSDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <winsock2.h>
#include <sys/timeb.h>
#include "../../Common/TXT/BufferUtil.h" 
#include "../../Common/LAN/NetUtil.h" 
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"
#include "../../Common/KEY/LicenseKey.h"

typedef struct LyricAlias_t
{
	CString szMessage;
	int nAlias;
	bool bMessageLoaded;
	bool bMessageRead;
} LyricAlias_t;


// these are data reference objects, pulled from the database.
/*
class CNucleusConnectionObject  
{
public:
	CNucleusConnectionObject();
	virtual ~CNucleusConnectionObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	char* m_pszServerName;
	char* m_pszClientName;
	char* m_pszDesc;

//	CAConnection* m_pAPIConn;  // pointer to the associated API connection.

	// control
	bool m_bKillConnThread;
	bool m_bConnThreadStarted;
};
*/

typedef struct ParameterRule_t
{
	CString sz_parameterid; 
	CString sz_name; 
	CString sz_target_typeid; 
	CString sz_target_valueid; 
	CString sz_returned_property_module; 
	CString sz_returned_property_source; 
	CString sz_returned_property_type; 
	CString sz_returned_property_col_name;  
	CString sz_default_value; 
	CString sz_parameterruleid; 
	CString sz_target_criterion_module; 
	CString sz_target_criterion_source; 
	CString sz_target_criterion_type; 
	CString sz_target_criterion_col_name; 
	CString sz_compare_type; 
	CString sz_criterion_value;
	int nComparisonType;
} ParameterRule_t;


typedef struct ParameterRuleQuery_t
{
	CString sz_name; 
	CString sz_type; 
	CString sz_query1;
	CString sz_query2;
	CString sz_target_valueid; // added in 2.2.1.13  24-August-2011, needed, was getting it from an incorrect index
} ParameterRuleQuery_t;

typedef struct EventRuleQuery_t
{
	CString sz_query1; 
	CString sz_query2;
} EventRuleQuery_t;


/*
select 

parameterid, 
name, 
target_typeid, 
target_valueid, 
returned_property_module, 
returned_property_source, 
returned_property_type, 
returned_property_col_name,  
default_value, 
parameterruleid, 
target_criterion_module, 
target_criterion_source, 
target_criterion_type, 
target_criterion_col_name, 
compare_type, 
criterion_value
 from NUCLEUS.dbo.ParameterRuleView where name = '@pname';
*/
typedef struct EventRule_t
{
	CString sz_eventid;
	CString sz_ruleid;
	CString sz_col_name;
	CString sz_compare_type;
	CString sz_value;
	CString sz_type;
	CString sz_span_clips;
	int nComparisonType;
} EventRule_t;

typedef struct DiskSpaceObject_t
{
	double dblDiskFree;
	double dblDiskTotal;
	double dblDiskThreshold;
	double dblDiskPercentUtilized;
} DiskSpaceObject_t;

//create table File_Metadata (sys_filename varchar(256) NOT NULL, sys_filepath varchar(256), sys_linked_file varchar(256),
//   sys_description varchar(256), sys_operator varchar(64), sys_type int, sys_duration int, sys_valid_from int, 
//	 sys_expires_after int, sys_ingest_date int, 
//	 sys_file_flags int, sys_file_size float, sys_created_on int, sys_created_by varchar(32),  
//	 sys_last_modified_on int, sys_last_modified_by varchar(32), 
//   nucleus_last_used int, nucleus_times_used int);


class CNucleusEvent  
{
public:
	CNucleusEvent();
	virtual ~CNucleusEvent();

	double m_dblTriggerTime; 
	bool m_bAnalyzed;
	bool m_bReAnalyzed;
	int  m_nAnalyzedTriggerID;
	int  m_nEventID;
	int  m_nDestType;
	
	int m_nType;
	CString m_szEventName;
	CString m_szHost;
	CString m_szSource;
	CString m_szScene;
	CString m_szIdentifier;
	CString m_szValue;
	CString m_szParamDependencies;
	CString m_szDestModule;
	CString m_szLayer;

	// additional, added 2.2.1.20 (9 July 2016)
	CString m_szAutomationEventItemID;
	CString m_szAutomationEventID;
	CString m_szAutomationEventTitle;
	CString m_szAutomationEventDur;
	CString m_szAutomationEventData;
	CString m_szChannelID;
};




class CFileMetaDataObject
{
public:
	CFileMetaDataObject();
	virtual ~CFileMetaDataObject();

	bool   m_bUnique;  // false if more than one filename returned.
	char*  m_sz_vc256_sys_filename;
	char*  m_sz_vc256_sys_filepath;
//	char*  m_sz_vc256_sys_linked_file;
	char*  m_sz_vc256_sys_description;
	char*  m_sz_vc64_sys_operator;
	char*  m_sz_n_sys_type;
	char*  m_sz_n_sys_duration;
	char*  m_sz_n_sys_valid_from;
	char*  m_sz_n_sys_expires_after;
	char*  m_sz_n_sys_ingest_date;
	char*  m_sz_n_sys_file_flags;
	char*  m_sz_dbl_sys_file_size;
	char*  m_sz_dbl_sys_file_timestamp;
	char*  m_sz_n_sys_created_on;
	char*  m_sz_vc32_sys_created_by;
	char*  m_sz_n_sys_last_modified_on;
	char*  m_sz_vc32_sys_last_modified_by;
	char*  m_sz_n_nucleus_last_used;
	char*  m_sz_n_nucleus_times_used;

	int SetField(char** ppszField, char* pszData);
	int UpdateMetadata(char* pszInfo=NULL);  // increments nucleus_last_used and nucleus_times_used
	int UpdateDestinationMetadata(int nEndpointIndex, char* pszHost, char* pszInfo=NULL);  // increments Nucleus_local_last_used and Nucleus_local_times_used in Endpoint Destinations_Media table
};

//create table Destinations_Media (host varchar(64), file_name varchar(256), transfer_date int, 
//partition varchar(16), file_size float, Nucleus_local_last_used int, Nucleus_local_times_used int);


class CDestinationMediaObject
{
public:
	CDestinationMediaObject();
	virtual ~CDestinationMediaObject();

	char*  m_sz_vc256_file_name;
	char*  m_sz_n_transfer_date; // actually timestamp
	char*  m_sz_vc16_partition;
	char*  m_sz_dbl_file_size;
	char*  m_sz_n_Nucleus_local_last_used;
	char*  m_sz_n_Nucleus_local_times_used;

	int SetField(char** ppszField, char* pszData);
};


class CNucleusQueueObject  
{
public:
	CNucleusQueueObject();
	virtual ~CNucleusQueueObject();

	int    m_nItemID;
	char*  m_pszFilenameLocal;
	char*  m_pszFilenameRemote;
	int    m_nActionID;
	char*  m_pszHost;
	double m_dblTimestamp;   // timestamp
	char*  m_pszUsername;
	int    m_nEventItemID;
	char*  m_pszMessage;

	int SetField(char** ppszField, char* pszData);
};

class CNucleusMappingObject  
{
public:
	CNucleusMappingObject();
	virtual ~CNucleusMappingObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned long m_ulType;
	unsigned short m_usComparisonType;
	unsigned long m_ulDestinationType;
	unsigned short m_usSearchType;
	unsigned short m_usActionType;
	int m_nMappingID;

	char* m_pszFieldName;
	char* m_pszParamName;
	char* m_pszCriterion;
	char* m_pszEventLocation;
};


class CNucleusEventObject  
{
public:
	CNucleusEventObject();
	virtual ~CNucleusEventObject();

	// data base fields
	CString m_sz_n_itemid;
	CString m_sz_n_channelid;
	CString m_sz_dbl_event_start;
	CString m_sz_n_event_itemid;
	CString m_sz_vc32_event_id;
	CString m_sz_vc64_event_clip;
	CString m_sz_vc64_event_title;
	CString m_sz_vc4096_event_data;
	CString m_sz_n_event_status;
	CString m_sz_vc256_filename;
	CString m_sz_n_destinationid; 
	CString m_sz_n_status;
	CString m_sz_n_transfer_date;
	CString m_sz_n_app_data_aux;
	CString m_sz_n_type;
	CString m_sz_vc64_dest_host; 
	CString m_sz_vc1024_search_files;
	CString m_sz_n_file_index;
	CString m_sz_vc64_module_dbname; 
	CString m_sz_vc64_module_dbqueue; 
	CString m_sz_n_rule_id;


	char* m_pszMainFile; 
	int m_nMainFileStatus;

	int m_nCurrentFileSearch;

	char** m_ppszChildren; 
	int*  m_pnChildrenStatus; 
	int m_nNumChildren; // for use with:

	int AddChild(char* pszChild, int nStatus);  
//	int UtilParseTem(char* pszSourceFile, char*** pppszChildren, unsigned long* pulNumChildren);
};


class CNucleusData  
{
public:
	CNucleusData();
	virtual ~CNucleusData();

/*
	CNucleusEvent** m_ppEventsList;
	int m_nNumEventsList;
	CNucleusEvent** m_ppEventsNewList;
	int m_nNumEventsNewList;
	CNucleusEvent** m_ppEventsPlaying;
	int m_nNumEventsPlaying;
*/
	// util objects
	CBufferUtil m_bu;
	CNetUtil m_net;
	bool m_bNetClientConnected;
	SOCKET m_socket;
	CNetData m_netdata;

  // hostname.  have to get it, store it here
	char*	m_pszHost;	// the name of the host
	char*	m_pszCompleteHost;	// the complete name of the host

	char*	m_pszCortexHost;	// the name of the cortex host
	unsigned short m_usCortexCommandPort;
	unsigned short m_usCortexStatusPort;

	CString** m_ppszTimingColName;
	int m_nNumTimingColNames;

	EventRule_t** m_ppEventRule;
	int m_nNumEventRules;

	EventRuleQuery_t** m_ppEventRuleQuery;
	int m_nNumEventRuleQueries;

	ParameterRule_t** m_ppParameterRule;
	int m_nNumParameterRules;

	ParameterRuleQuery_t** m_ppParameterRuleQuery;
	int m_nNumParameterRuleQueries;

	CNucleusMappingObject** m_ppMappingObj;
	int m_nNumMappingObjects;

	CRecordset* m_prsEvents;

	bool m_bNetworkMessagingInitialized;
	bool m_bCheckModsWarningSent;
	bool m_bCheckMsgsWarningSent;
	bool m_bCheckAsRunWarningSent;

	_timeb m_timebAutoPurge; // the last time autopurge was run


	_timeb m_timebAutomationTick; // the last time check inside the thread
//	_timeb m_timebNearTick; // the last time check inside the thread
//	_timeb m_timebFarTick; // the last time check inside the thread
//	_timeb m_timebTriggerTick; // the last time check inside the thread


	_timeb m_timebTick; // the last time check inside the thread
	_timeb m_timebLastStatus; // the time of the last status given to cortex
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for cortex global status (each obj has their own as well)

	//////////////////////////////////////////////
	// the following is DEMO.
	// need to support multiple Harris times.
//	double m_dblHarrisTime; // the harris server time at check
//	double m_dblHarrisTimeEstimate; // the harris server time estimate
//	_timeb m_timebHarrisCheckTimeLocal; // the time sentinel sent the harris server time
//	int m_nHarrisTimeDiffMS; // difference in milliseconds between the time harris was set and the time it was read
  //////////////////////////////////////////////

	int m_nEventCheckIndex;
//	int m_nEventLastMax;
	int m_nNumberOfEvents;

	int  m_nTypeAutomationInstalled; // which automation module is installed.
	int  m_nIndexAutomationEndpoint; // which automation module is installed.

	int  m_nIndexMetadataEndpoint; // archivist module index

	int m_nSettingsMod;
//	int m_nChannelsMod;
	int m_nConnectionsMod;
//	int m_nRulesMod;
	int m_nLastSettingsMod;
//	int m_nLastChannelsMod;
	int m_nLastConnectionsMod;
//	int m_nLastRulesMod;
	int m_nMappingMod;
	int m_nLastMappingMod;
	int m_nLastEventRulesMod;
	int m_nEventRulesMod;
	int m_nLastParameterRulesMod;
	int m_nParameterRulesMod;
	int m_nLastEventsMod;
	int m_nEventsMod;


	bool m_bProcessSuspended;
	bool m_bAutomationThreadStarted;
	bool m_bGlobalAnalysisThreadStarted;
	double m_dblLastAutomationChange;  //global change

//	bool m_bFarAnalysisThreadStarted;
//	bool m_bNearAnalysisThreadStarted;
//	bool m_bFarEventsChanged;
	bool m_bNearEventsChanged;
	bool m_bForceAnalysis;
//	bool m_bTriggerEventsChanged;
//	bool m_bTriggerThreadStarted;
	bool m_bRunningAnalyses;
	bool m_bDelayingTriggerNotification;

	bool m_bRunningAutomationRulesUpdate;

	_timeb m_timebNearTick; // the last time check inside the thread


//	CNucleusEventObject* m_pTransferEvent;
	CNucleusEventObject* m_pCheckEvent;


	bool m_bTabulatorThreadKill;
	bool m_bTabulatorThreadStarted;
	SOCKET m_socketTabulator;
	//BOOL m_bInCommand;
	_timeb m_timeLastCommandTick;
	CRITICAL_SECTION m_critTabCmd;



	char* GetStatusText(unsigned long* pulStatus);  // allocates mem, must free after use
	int		SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError = false);
	int		GetHost();

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;

	CDBUtil* m_pdbMessage;
	CDBconn* m_pdbMessageConn;

	int CheckMessages(char* pszInfo=NULL);
	int CheckAsRun(char* pszInfo=NULL);
	int CheckDatabaseMods(char* pszInfo=NULL);
	int IncrementDatabaseMods(char* pszTableName, char* pszInfo=NULL);
	int GetConnections(char* pszInfo=NULL);
	int GetMappings(char* pszInfo=NULL);
	int GetTimingColumns(char* pszInfo=NULL);
	int GetEventRules(char* pszInfo=NULL);
	int GetParameterRules(char* pszInfo=NULL);
	int GetParameterQueryIndex(CString name);

	int ApplyMapping(int nRuleIndex, char* pszData, char** ppszEventName=NULL, char** ppszExplicitExtension=NULL);  // returns success (0) if it passes the rule, with event name.  the event will only include extension if explicit, otherwise it is stripped!
	int CheckMapping(char* pszData, unsigned short usAutomationType);
	int	GetRuleIndex(int nRuleID);

//	int	ParseFiles(char* pszSourceData, char** pszFile, int* pnStatus, char*** pppszChildren, int** ppnChildrenStatus, int* pnNumChildren);
	int ReleaseRecordSet(bool bResetIncrementor = true);
//	int UpdateSearchFiles(CNucleusEventObject* pObj, int nStatus);
//	int SimpleSetStatus(CNucleusEventObject* pObj, int nStatus, bool bUpdateSearchFiles=false, bool bFileIndex=false, int nAppDataAux=0, char* pszInfo=NULL);
	int ScheduleEndpointQueue(CNucleusEventObject* pObj, char* pszFilenameLocal, char* pszFilenameRemote, int nAction, char* pszInfo=NULL);
	int RemoveFromEndpointQueue(CNucleusEventObject* pObj, int nItemID, char* pszInfo=NULL);
	CNucleusQueueObject* ReturnFromEndpointQueue(CNucleusEventObject* pObj, char* pszInfo=NULL);
	DiskSpaceObject_t*  ReturnDestinationDiskSpace(CNucleusEventObject* pObj, char* pszInfo=NULL);
	CFileMetaDataObject*  ReturnFileMetaDataObject(char* pszFilename, char* pszInfo=NULL);
	CDestinationMediaObject*  ReturnDestinationMediaObject(CNucleusEventObject* pObj, char* pszInfo=NULL);
//	CDestinationMediaObject*  ReturnDestinationMediaObjectToDelete(CNucleusEventObject* pObj, char* pszInfo=NULL);
	char* ReturnPartition(CNucleusEventObject* pObj, char* pszFilename, char* pszInfo=NULL);
	unsigned long ReturnChannelFlags(CNucleusEventObject* pObj, char* pszInfo=NULL);
	unsigned long ReturnDestinationFlags(CNucleusEventObject* pObj, char* pszInfo=NULL);
	int ReturnModuleIndex(CNucleusEventObject* pObj, char* pszInfo=NULL);
	int ReturnNumberOfAnalysisRecords(char* pszInfo=NULL);

	int IncrementGlobalTimesUsed(CNucleusEventObject* pObj, char* pszInfo=NULL);
	int IncrementLocalTimesUsed(CNucleusEventObject* pObj, char* pszInfo=NULL);

	int SendGraphicsCommand(int nCmd, char* chDataBuffer);

	int SendTabulatorCommand(unsigned char** ppucDataBuffer);

	//CString ReturnParam(CString pszName, double dblTime, CString);

	CLicenseKey m_key;

	bool m_bQuietKill;
	CRITICAL_SECTION m_critMessageSQL;
	CRITICAL_SECTION m_critSQL;
	CRITICAL_SECTION m_critEventRules;
	CRITICAL_SECTION m_critParameterRules;
	CRITICAL_SECTION m_critSettings;

private:
	CRITICAL_SECTION m_critText;
	char* m_pszStatus;	// parseable string

//	unsigned long m_ulReqCount;
//	unsigned long m_ulSvcCount;
};

#endif // !defined(AFX_NUCLEUSDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
