// NucleusSettings.h: interface for the CNucleusSettings class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NUCLEUSSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
#define AFX_NUCLEUSSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "NucleusData.h"  // for endpoints
#include "NucleusDefines.h"
//#include "../Sentinel/SentinelData.h"  
#include "../../Common/MFC/ODBC/DBUtil.h"  



class CNucleusAutomationChannelObject  
{
public:
	CNucleusAutomationChannelObject();
	virtual ~CNucleusAutomationChannelObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	int m_nChannelID;  // the unique Channel ID within Sentinel setup. (assigned externally)
	int m_nHarrisListNum;  // the 1-based List # on the associated ADC-100 server, 
	int m_nOmnibusPort; // the Omnibus port number

	char* m_pszServerName;
	char* m_pszDesc;

	void* m_pNucleus;
	void* m_pEndpoint;

	double m_dblServertime; // in number of ms since epoch
	double m_dblServerLastUpdate; // in number of ms since epoch
	int m_nServerStatus;
	int m_nServerBasis;
	int m_nServerChanged;

	double m_dblListLastUpdate;   // in number of ms since epoch
	int m_nListState;
	int m_nListChanged;
	int m_nListDisplay;
	int m_nListSysChange;
	int m_nListCount;
	int m_nListLookahead;

// control
	bool m_bKillAutomationThread;  // kills all.
	bool m_bAutomationThreadStarted;
	bool m_bTriggerEventsRetrieved;
	bool m_bTriggerEventsChangedLocal;
	bool m_bTriggerEventsChanged;
	bool m_bTriggerThreadStarted;
	bool m_bTriggerQueryThreadStarted;
//	bool m_bFarAnalysisThreadStarted;
//	bool m_bNearAnalysisThreadStarted;
//	bool m_bFarEventsChanged;
//	bool m_bNearEventsChanged;
//	bool m_bNearEventsChangedDelayed;

	CRITICAL_SECTION m_critChVar;


	// temp
	double m_dblAutomationTimeEstimate; // the harris server time estimate in unixtim.ms
	double m_dblAutomationTimeDiffMS; // difference in milliseconds between the time harris was set and the time it was read

	_timeb m_timebAutomationTick; // the last time check inside the thread
	_timeb m_timebNearTick; // the last time check inside the thread
//	_timeb m_timebFarTick; // the last time check inside the thread
	_timeb m_timebTriggerTick; // the last time check inside the thread

	int m_nAnalysisCounter;


	//  Lyric specific - currently assuming one Chyron box per  - need to change at some point
	int m_nLastLyricAlias;


	LyricAlias_t** m_ppLyricAlias;
	int m_nLastAlias;
	int m_nNumAliasArray;

	int ReturnAliasIndex(CString szMessage);
	int AddAlias(CString szMessage);

	int m_nMinLyricAlias;
	int m_nMaxLyricAlias;

//	int m_nCurrentMessage;
//	bool m_bMessageLoaded; 
//	bool m_bMessageRead;  // determines whether to use U\ or U\*\  

	int AddIdentifier(int nID);
	int CheckIdentifier(int nID);

	CString ReturnParsedValue(CString szValue, CNucleusEvent* pEvent);

	CDBUtil m_db;
	CDBconn* m_pdbConn;
//	CRecordset* m_prs; // trigger records.

	CRITICAL_SECTION m_critTriggerSQL;

	int* m_pnTriggeredIDs;
	int m_nTriggeredIDs;
	CRITICAL_SECTION m_critTriggeredIDs;
	bool m_bGetTriggeredIDs;

	bool m_bNewEventRetrieve;
	
	CNucleusEvent** m_ppEvents;
	int m_nEvents;

	CNucleusEvent** m_ppNewEvents;
	int m_nNewEvents;

};





class CNucleusEndpointObject  
{
public:
	CNucleusEndpointObject();
	virtual ~CNucleusEndpointObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	int m_nModName;
	int m_nModDBName;
	int m_nModQueue;  // the Queue table name
	int m_nModExchange;  // the Exchange table name
	int m_nModLiveEvents;  // the LiveEvents table name if applicable
	int m_nModDestination;  // the Destination table name if applicable
	int m_nModDestinationMedia;  // the DestinationsMedia table name if applicable
	int m_nModChannel;  // the Channel table name if applicable
	int m_nModMetadata;  // the Metadata table name if applicable
	int m_nModFileTypes;  // the File types table name if applicable
	int m_nModConnections;  // the Connections table name if applicable
	int m_nModChannels;  // the Channels table name if applicable

	int m_nLastModName;
	int m_nLastModDBName;
	int m_nLastModQueue;  // the Queue table name
	int m_nLastModExchange;  // the Exchange table name
	int m_nLastModLiveEvents;  // the LiveEvents table name if applicable
	int m_nLastModDestination;  // the Destination table name if applicable
	int m_nLastModDestinationMedia;  // the DestinationsMedia table name if applicable
	int m_nLastModChannel;  // the Channel table name if applicable
	int m_nLastModMetadata;  // the Metadata table name if applicable
	int m_nLastModFileTypes;  // the File types table name if applicable
	int m_nLastModConnections;  // the Connections table name if applicable
	int m_nLastModChannels;  // the Channels table name if applicable

	
	char* m_pszName;
	char* m_pszDBName;
	char* m_pszQueue;  // the Queue table name
	char* m_pszCommandQueue;  // the Command Queue table name
	char* m_pszExchange;  // the Exchange table name
	char* m_pszLiveEvents;  // the LiveEvents table name if applicable
	char* m_pszDestination;  // the Destination table name if applicable
	char* m_pszDestinationMedia;  // the DestinationsMedia table name if applicable
	char* m_pszChannel;  // the Channel table name if applicable
	char* m_pszMetadata;  // the Metadata table name if applicable
	char* m_pszFileTypes;  // the File types table name if applicable
	char* m_pszConnections;  // the Connections table name if applicable
	
	int CheckDatabaseMod(CDBUtil* pdb, CDBconn* pdbConn, char* pszTableName, char* pszInfo=NULL);
	int IncrementDatabaseMods(CDBUtil* pdb, CDBconn* pdbConn, char* pszTableName, char* pszInfo=NULL);

	// automation
	CNucleusAutomationChannelObject** m_ppChannelObj;
	int m_nNumChannelObjects;
	

//	CAConnection* m_pAPIConn;  // pointer to the associated API connection.

// control
//	bool* m_pbKillConnThread;
//	bool m_bKillChannelThread;
//	bool m_bChannelThreadStarted;
};



class CNucleusSettings  
{
public:
	CNucleusSettings();
	virtual ~CNucleusSettings();

	int GetFromDatabase(char* pszInfo=NULL);  //only get.  Interface publishes
	int Settings(bool bRead);
	char* ProcessString(char* pszString, bool bFreeIncomingString);

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;

	char* m_pszName;  // familiar name of this instance (and therefore the name of the Nucleus database)
	char* m_pszType;  // familiar name of the type.
	char* m_pszProject;  // familiar name of the project name.
	unsigned long m_ulMainMode;


	int m_nAutoPurgeMessageDays;
	int m_nAutoPurgeAsRunDays;
	int m_nAutoPurgeInterval;

	int m_nThreadDwellMS;
	bool m_bDebugLists;  // if true, dumps the temp and events databases
	int  m_nDebugListTopCount;  //if positive, just prints out the top n recored in the events tables.
	bool m_bDebugSQL;  // if true, prints out the SQL calls, all but event inserts
	bool m_bDebugInsertSQL;  // if true, prints out the event insert SQL calls
	unsigned long m_ulDebug;  // prints out debug statements that & with this.
	char* m_pszDebugOrder; // the order by clause for debug

	// ports
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;

	// The following flags determine what settings are loaded. 
	// The specifics do not need to be stored in cortex settings, they are pushed to the various objects.

	// messaging for Nucleus
	bool m_bUseLog;			// write a log file
	bool m_bUseEmail;		// send an email on failures (or commanded from remotes)
	bool m_bUseNetwork; // send a message to a remote host
	char* m_pszFileSpec;
	char* m_pszMailSpec;
	char* m_pszProcessedFileSpec;
	char* m_pszProcessedMailSpec;
	bool m_bUseXMLClientLog;			// write a log file
	bool m_bLogNetworkErrors; // enables messaging into network object (logs socket errors, etc).
	bool m_bMillisecondMessaging;			// use millisecond resolution for messages and asrun
	bool m_bReadableEncodedAsRun;	

	// Harris API
//	bool m_bUseListCount; // get all events up until the list count (otherwise just up to the lookahead)
	int  m_nHarrisStatusTriggerExclude;  //if status ANDed with this is non-zero, will not be included in trigger

	// installed dependencies
	CNucleusEndpointObject** m_ppEndpointObject; // which endpoint module(s) is (are) installed.
	int  m_nNumEndpointsInstalled; // number of endpoint module(s) is (are) installed.

	bool m_bReportSuccessfulOperation;
	bool m_bLogTransfers;


	// database stuff
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;

	char* m_pszDefaultDB;			// the Default DB name
	char* m_pszCortexDB;			// the Cortex DB name

	char* m_pszSettings;  // the Settings table name
	char* m_pszExchange;  // the Exchange table name
	char* m_pszMessages;  // the Messages table name
	char* m_pszAsRun;  // the As run table name

	char* m_pszQueue;  // the Queue table name
//	char* m_pszChannels;  // the Channels table name
//	char* m_pszConnections;  // the Connections table name
	char* m_pszMappings;  // the Mappings table name
	char* m_pszEvents;  // the Events table name
	char* m_pszEventRules;  // the EventRules view name
	char* m_pszTriggerAnalysisView;  // the TriggerAnalysisView name
	char* m_pszAnalysis;  // the Analysis table name
	char* m_pszAnalyzedTriggerData;  // the AnalyzedTriggerData table name
	char* m_pszAnalyzeRulesProc;  // the spAnalyzeRules stored procedure
	int		m_nAnalyzeHarrisStatusExclude;  // exclude analysis on harris events that have this status
	char* m_pszGetParameterValueProc;  // the spGetParameterValue stored procedure
	char* m_pszParameterRuleView;  // the  ParameterRuleView name
	char* m_pszMetaConfigUnionALLView;  // the MetaConfigUnionALLView name
	char* m_pszChannelInfo;  // the m_pszChannelInfo name
	char* m_pszTriggerInfoView;  // the TriggerInfoView name

	char* m_pszTriggerSourceView;  // the TriggerSourceView name
	bool m_bUseTriggerSourceView;  // use TriggerSourceView instead of TriggerAnalysisView for the trigger get
	bool m_bExcludeAsyncTriggers;  // specifically exclude asynchronous triggers from trigger get.

	char* m_pszLiveEventData;  // the LiveEventData table name


// DEMO stuff to be removed and replaced *************************************************************************
//	char* m_pszHarrisAutomationServer;
//	int m_nHarrisAutomationList;
// DEMO stuff to be removed and replaced *************************************************************************

//	char* m_pszDefaultProject;
//	char* m_pszDefaultScene; 
//	char* m_pszDefaultHost; 

	// gfx module
//	char* m_pszGfxHost; 
//	int   m_nGfxPort; 

	//system folder
	char* m_pszSystemFolderPath;			// the path of the folder used for parse files etc.

// *************************************************************************
// old, for Direct.
	//imagestore specific 
//	char* m_pszImagestoreNullFile;				// the filename of the nullfile - must be in the system folder
//	char* m_pszIntuitionSearchExt;				// search extensions for Intuition
//	char* m_pszImagestore2SearchExt;			// search extensions for IS2
//	char* m_pszImagestore300SearchExt;		// search extensions for IS300
//	char* m_pszImagestoreHDSearchExt;			// search extensions for ISHD

	// endpoint autodeletions
//	bool m_bAutoDeleteOldest;
//	unsigned long m_ulDeletionThreshold;  // number of seconds in past to seek for files to delete
//	int m_nTransferFailureRetryTime;  // number of seconds after a transfer fails to retry it.
// *************************************************************************


	// chyron specific
	int m_nMinLyricAlias;
	int m_nMaxLyricAlias;
	bool m_bReleaseIsV_5_15; // otherwise it's the macro (E) command. E\Lyric.FrameBuffer(2).ReleasePause\\  (FB2)


	//icon specific
	int m_nLayoutSalvoFormat; // 0= 4 digit numerical, 1=8 digit numerical, 2 = strings  

	unsigned long m_ulAutomationLookahead;  // number of events to analyze (per channel)
	unsigned long m_ulTriggerReanalyzeThreshold; // number of seconds before previously analyzed on air time, to re-check event.
	unsigned long m_ulTriggerStandbyThreshold; // number of seconds before re-analyzed on air time, to set standby, which means download stuff and search harris automation for info to assemble.
	unsigned long m_ulTriggerCueThreshold; // number of seconds before re-analyzed on air time, to set cued, which means check scene loaded.
	int m_nTriggerAdvanceMS; // number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)

	int m_nAutomationIntervalMS; // number of milliseconds between channel time checks
	int m_nAnalyzeAutomationDwellMS; // number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once.
	int m_nAnalyzeAutomationForceMS; // number of milliseconds after a change, that if there are changes pending, we MUST be force analysis 
	int m_nAnalyzeRulesIntervalMS; // number of milliseconds between automation checks that cause rules analysis
	int m_nAnalyzeRulesDwellMS; // number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once.
	int m_nAnalyzeRulesForceMS; // number of milliseconds after a change, that if there are changes pending, we MUST be force rules analysis 
	int m_nAnalyzeParameterDwellMS; // number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once.
	int m_nAnalyzeParameterForceMS; // number of milliseconds to wait before forcing this to run.
	int m_nAnalyzeTimingDwellMS; // max number of milliseconds to allow timing not be re-run
	int m_nTriggerNotificationDwellMS; // number of milliseconds trigger notification is delayed after a change 

	bool m_bEnableAnalysisTriggerNotification;
	bool m_bEnableEventTriggerNotification;
	bool m_bChannelInfoViewHasDesc;  // if true, obtains description

	int m_nThreadSeparationDelayMS; // number of milliseconds between thread starts
	int m_nChannelSeparationDelayMS; // number of milliseconds between channel process starts


	bool m_bUseLocalClock;  // use the computer's clock, not the transmitted server time
	bool m_bUseUTC; // use UTC time, not local/DST time.
	bool m_bUseUTF8; // use UTF8, not ISO

	bool m_bFarAnalysisUsesCalc;
	bool m_bNearAnalysisUsesCalc;
	unsigned long m_ulModsIntervalMS;  // interval on which to check database mods

	bool m_bAutomationChangesDoNotCrossChannels;
	bool m_bUseAsynchTriggerRetrieve;

	int  m_nTriggerBuffer; // number of items to hold in a buffer for triggering
	int  m_nTriggerExclusionDelayMS; // number of milliseconds to allow past events to continue be buffered (exclude stuff further in the past).

	int  m_nInterTriggerDelayMS; // number of milliseconds to delay between SQL queries for triggers within an event
	int  m_nInterEventDelayMS; // number of milliseconds to delay between processing each event.
	int  m_nInterRulesDelayMS; // number of milliseconds to delay between processing each event rule (because it's hammery).

	
	char* m_pszLicense;  // the License Key
	char* m_pszOEMcodes;  // the possible OEM string
	char* m_pszIconPath;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

	char* m_pszCloseAll; // the close all token
	char* m_pszStopAll;  // the stop (but not close) all token.

// stuff to enable a connection to a Tabulator executable

	char* m_pszTabulatorModule;  // module name, "Tabulator"
	char* m_pszTabulatorPlugInModule;  // module plugin name for style commands, "AutomationData"
	char* m_pszTabulatorHost;  // the Tabulator host IP or name , usually the loopback
	int m_nTabulatorPort;  // the Tabulator host port, usually 10688

	bool m_bTabulatorUseHello; // sends a hello on connect
	bool m_bTabulatorReinitOnData; // shuts down socket if any data is received - nothing should be coming un-unsolicited.
	int m_nTabulatorHelloIntervalMS;  // sends a hello every so many milliseconds (or since last data - clock is reset by other traffic, point is keep-alive)
	int m_nTabulatorConsumeDataMS;  // waits this many ms before consuming unsolicited data (or shutting down socket)
	int m_nTabulatorSendTimeoutMilliseconds;  
	int m_nTabulatorReceiveTimeoutMilliseconds;
	int m_nTabulatorCommandRetries;
	bool m_bTabulatorLogHello; // logs all hello commands
	bool m_bTabulatorLogPlugIn; // logs all plug-in call commands

	unsigned long m_ulAuxFlags;

};

#endif // !defined(AFX_NUCLEUSSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
