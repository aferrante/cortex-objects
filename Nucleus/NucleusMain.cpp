// NucleusMain.cpp: implementation of the CNucleusMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "Nucleus.h"  // just included to have access to windowing environment
#include "NucleusDlg.h"  // just included to have access to windowing environment
#include "NucleusHandler.h"  // just included to have access to windowing environment

#include "NucleusMain.h"
#include <process.h>
#include "../../Common/TXT/BufferUtil.h"
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"

#include "..\Sentinel\SentinelDefines.h"
#include "..\..\Common\API\Harris\ADCDefs.h"
//#include "..\..\Common\API\Miranda\IS2Comm.h"
#include "..\..\Common\API\Miranda\IS2Core.h"
//#include "..\Radiance\RadianceDefines.h"
#include "..\Libretto\LibrettoDefines.h"
#include "..\Tabulator\TabulatorDefines.h"

#include <sys/timeb.h>
#include <time.h>
#include <direct.h>
#include <objsafe.h>
#include <atlbase.h>

// have to add rpcrt4.lib to the linker for UUID stuff


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;
bool g_bKillStatus = false;
CNucleusMain* g_pnucleus=NULL;
//CADC g_adc;
//CIS2Comm g_is2;  // inlined code - problems with CIS2Comm::UtilParseTem memory management . never figured this one out.

extern CMessager* g_pmsgr;  // from Messager.cpp
extern CNucleusApp theApp;



//void NucleusConnectionThread(void* pvArgs);
//void NucleusListThread(void* pvArgs);
void NucleusTabulatorConnectionThread(void* pvArgs);

void NucleusGlobalAutomationThread(void* pvArgs);
void NucleusGlobalAnalysisThread(void* pvArgs);
void NucleusAutomationThread(void* pvArgs);
//void NucleusFarAnalysisThread(void* pvArgs);
void NucleusTriggerThread(void* pvArgs);
void NucleusTriggerQueryThread(void* pvArgs);
void NucleusTriggerDelayNotificationThread(void* pvArgs);
void NucleusMsgSQLThread(void* pvArgs);


#define NOT_USING_NEAR_ANALYSIS

#ifndef NOT_USING_NEAR_ANALYSIS
void NucleusNearAnalysisThread(void* pvArgs);
#endif //#ifndef NOT_USING_NEAR_ANALYSIS


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNucleusMain::CNucleusMain()
{
}

CNucleusMain::~CNucleusMain()
{
}

/*

char*	CNucleusMain::NucleusTranslate(CHTTPHeader* pHeader, char* pszBuffer)			// apply nucleus scripting language
{
	return pszBuffer;
}

int		CNucleusMain::InterpretNucleusive(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the nucleusive, such as a char buffer.
	return NUCLEUS_SUCCESS;
}
*/

SOCKET*	CNucleusMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // nucleus initiates a request to an object server
{
	CNetData* pdata = new CNetData;

	pdata->m_ucType = ucType;      // defined type - indicates which protocol to use, structure of data

	pdata->m_ucCmd = ucCmd;       // the command byte
	pdata->m_ucSubCmd = ucSubCmd;       // the subcommand byte

	pdata->m_pucData = pucData;
	pdata->m_ulDataLen = ulDataLen;

	SOCKET* ps = NULL; 
	m_net.SendData(pdata, pchHost, usPort, 5000, 0, NET_SND_CMDTOSVR, ps);
//	m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);

	return ps;
}

int		CNucleusMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// nucleus replies to an object server after receiving data. (usually ack or nak)
{
	return NUCLEUS_SUCCESS;
}

int		CNucleusMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// nucleus answers a request from an object client
{
	return NUCLEUS_SUCCESS;
}


void NucleusMainThread(void* pvArgs)
{
	CNucleusApp* pApp = (CNucleusApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment
	//startup.
	g_bThreadStarted = true;

	//create the main objects.

	CNucleusMain nucleus;
	CDBUtil db;
	CDBUtil dbMsg;

	nucleus.m_data.m_ulFlags &= ~NUCLEUS_STATUS_THREAD_MASK;
	nucleus.m_data.m_ulFlags |= NUCLEUS_STATUS_THREAD_START;
	nucleus.m_data.m_ulFlags &= ~NUCLEUS_ICON_MASK;
	nucleus.m_data.m_ulFlags |= NUCLEUS_STATUS_UNINIT;

	nucleus.m_data.GetHost();

	g_pnucleus = &nucleus;


	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
//	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\Nucleus\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}



//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

//		AfxMessageBox( pApp->m_lpCmdLine );

	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// check here for Cortex IP.
			if(strlen(pch))
			{
//		AfxMessageBox( pch );
				if(nucleus.m_data.m_bu.CountChar(pch, strlen(pch), ':')>=1)  // must be of form "hostname:cmdport" where hostname can be name or ip
				{
					nucleus.m_data.m_pszCortexHost = (char*)malloc(strlen(pch)+1);
					if(nucleus.m_data.m_pszCortexHost)
					{
						strcpy(nucleus.m_data.m_pszCortexHost, pch);
//		AfxMessageBox( nucleus.m_data.m_pszCortexHost );

						char* pchd = strchr(nucleus.m_data.m_pszCortexHost, ':');
						if(pchd)
						{
							*pchd = 0;  // null term the host name;
							pchd++;
//		AfxMessageBox( nucleus.m_data.m_pszCortexHost );

							char* pchd2 = strchr(pchd, ':');
							if(pchd2)
							{
								*pchd2 = 0;  // null term the cmd port;
								pchd2++;
								if(strlen(pchd2)) nucleus.m_data.m_usCortexStatusPort = atoi(pchd2);
							}

							if(strlen(pchd)) nucleus.m_data.m_usCortexCommandPort = atoi(pchd);
						}
					}

				}
			}
			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }

	nucleus.m_settings.Settings(true); //read
/////////////////////////////////////////////////
// would comment this part out, but need to get dependencies here... once.
	char pszFilename[MAX_PATH];

	//	AfxMessageBox("3");

	strcpy(pszFilename, "");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, NUCLEUS_SETTINGS_FILE_DEFAULT);  // cortex settings file

	CFileUtil file;
	file.GetSettings(pszFilename, false); 
	char* pszParams = NULL;
// load up the values on the settings object
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
/*
		nucleus.m_settings.m_pszName = file.GetIniString("Main", "Name", "Nucleus");
		nucleus.m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");

		// compile license key params
		if(g_pnucleus->m_data.m_key.m_pszLicenseString) free(g_pnucleus->m_data.m_key.m_pszLicenseString);
		g_pnucleus->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(nucleus.m_settings.m_pszLicense)+1);
		if(g_pnucleus->m_data.m_key.m_pszLicenseString)
		sprintf(g_pnucleus->m_data.m_key.m_pszLicenseString, "%s", nucleus.m_settings.m_pszLicense);

		g_pnucleus->m_data.m_key.InterpretKey();

		if(g_pnucleus->m_data.m_key.m_bValid)
		{
			unsigned long i=0;
			while(i<g_pnucleus->m_data.m_key.m_ulNumParams)
			{
				if((g_pnucleus->m_data.m_key.m_ppszParams)
					&&(g_pnucleus->m_data.m_key.m_ppszValues)
					&&(g_pnucleus->m_data.m_key.m_ppszParams[i])
					&&(g_pnucleus->m_data.m_key.m_ppszValues[i]))
				{
					if(stricmp(g_pnucleus->m_data.m_key.m_ppszParams[i], "max")==0)
					{
//						g_pnucleus->m_data.m_nMaxLicensedDevices = atoi(g_pnucleus->m_data.m_key.m_ppszValues[i]);
					}
				}
				i++;
			}

			if(
					(
				    (!nucleus.m_data.m_key.m_bExpires)
					||((nucleus.m_data.m_key.m_bExpires)&&(!nucleus.m_data.m_key.m_bExpired))
					||((nucleus.m_data.m_key.m_bExpires)&&(nucleus.m_data.m_key.m_bExpireForgiveness)&&(nucleus.m_data.m_key.m_ulExpiryDate+nucleus.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
				    (!nucleus.m_data.m_key.m_bMachineSpecific)
					||((nucleus.m_data.m_key.m_bMachineSpecific)&&(nucleus.m_data.m_key.m_bValidMAC))
					)
				)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
				nucleus.m_data.SetStatusText(errorstring, NUCLEUS_STATUS_OK);
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				nucleus.m_data.SetStatusText(errorstring, NUCLEUS_STATUS_ERROR);
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
			nucleus.m_data.SetStatusText(errorstring, NUCLEUS_STATUS_ERROR);
		}

		nucleus.m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");

		nucleus.m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", NUCLEUS_PORT_CMD);
		nucleus.m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", NUCLEUS_PORT_STATUS);

		nucleus.m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
		nucleus.m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
		nucleus.m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
		nucleus.m_settings.m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
		nucleus.m_settings.m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;

//		nucleus.m_settings.m_bUseListCount = file.GetIniInt("HarrisAPI", "UseListCount", 0)?true:false; // get all events up until the list count (otherwise just up to the lookahead)

		nucleus.m_settings.m_pszDSN = file.GetIniString("Database", "DSN", nucleus.m_settings.m_pszName?nucleus.m_settings.m_pszName:"Nucleus");
		nucleus.m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
		nucleus.m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
		nucleus.m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
		nucleus.m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
		nucleus.m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name

//		nucleus.m_settings.m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels");  // the Channels table name
//		nucleus.m_settings.m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Connections");  // the Connections table name
		nucleus.m_settings.m_pszEvents = file.GetIniString("Database", "EventsTableName", "Events");  // the Events table name
		nucleus.m_settings.m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue");  // the Queue table name
		nucleus.m_settings.m_pszRules = file.GetIniString("Database", "RulesTableName", "Rules");  // the Rules table name
			g_pnucleus->m_settings.m_pszTriggerAnalysisView = file.GetIniString("Database", "AnalysisViewName", "TriggerAnalysisView");  // the TriggerAnalysisView name
			g_pnucleus->m_settings.m_pszAnalyzedTriggerData = file.GetIniString("Database", "AnalyzedTriggerDataTableName", "AnalyzedTriggerData");  // the AnalyzedTriggerData table name
			g_pnucleus->m_settings.m_pszAnalyzeRulesProc = file.GetIniString("Database", "AnalyzeRulesProcedureName", "spAnalyzeRules");  // the spAnalyzeRules stored procedure
			g_pnucleus->m_settings.m_pszGetParameterValueProc = file.GetIniString("Database", "GetParameterValueProcedureName", "spGetParameterValue");  // the spGetParameterValue stored procedure

		nucleus.m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
//		nucleus.m_settings.m_nTransferFailureRetryTime = file.GetIniInt("FileHandling", "TransferFailureRetryTime", 60);  // in seconds
//		nucleus.m_settings.m_bAutoDeleteOldest = file.GetIniInt("FileHandling", "AutoDeleteOldest", 1)?true:false;
//		nucleus.m_settings.m_ulDeletionThreshold = file.GetIniInt("FileHandling", "DeletionThreshold", 172800); // two days
		nucleus.m_settings.m_pszDefaultProject = file.GetIniString("Demo", "DefaultProject", "Promo_B");
		nucleus.m_settings.m_pszDefaultScene = file.GetIniString("Demo", "DefaultScene", "Promo_B");
		nucleus.m_settings.m_pszDefaultHost = file.GetIniString("Demo", "DefaultHost", "10.0.0.22");

		nucleus.m_settings.m_pszSystemFolderPath = file.GetIniString("FileHandling", "SystemFolder", "C:\\");  // must have trailing slash
		nucleus.m_settings.m_nTriggerAdvanceMS = file.GetIniInt("FileHandling", "TriggerAdvanceMS", 0);// number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)

//		nucleus.m_settings.m_pszImagestoreNullFile = file.GetIniString("Imagestore", "NullOXTFile", "null.oxt");  // name of the null file, must be in system folder
//		nucleus.m_settings.m_pszIntuitionSearchExt = file.GetIniString("Imagestore", "IntuitionSearchExt", "tem"); // search extensions for Intuition
//		nucleus.m_settings.m_pszImagestore2SearchExt = file.GetIniString("Imagestore", "Imagestore2SearchExt", "oxa,oxt,oxw");			// search extensions for IS2
//		nucleus.m_settings.m_pszImagestore300SearchExt = file.GetIniString("Imagestore", "Imagestore300SearchExt", "oxa,oxt,oxe");		// search extensions for IS300
//		nucleus.m_settings.m_pszImagestoreHDSearchExt = file.GetIniString("Imagestore", "ImagestoreHDSearchExt", "oxa,oxt,oxe");		// search extensions for ISHD

			// gfx module
		nucleus.m_settings.m_pszGfxHost = file.GetIniString("Graphics", "Host", "127.0.0.1");  // must have trailing slash; 
		nucleus.m_settings.m_nGfxPort = file.GetIniInt("Graphics", "Port", 10682); 

*/
		// now, get the dependencies.... (get only, not set at end).
		nucleus.m_settings.m_nNumEndpointsInstalled = 0;
		int nNumEndpoints = file.GetIniInt("Dependencies", "Number", 0); 
		int nDep=0;
		char szKey[64];
		while(nDep<nNumEndpoints)
		{
			sprintf(szKey, "Dependency%03d", nDep);
			pszParams = file.GetIniString("Dependencies", szKey, ""); 
			if(pszParams)
			{
				if(strlen(pszParams))
				{
					CNucleusEndpointObject* pdeo = new CNucleusEndpointObject;
					if(pdeo)
					{
						pdeo->m_pszDBName = file.GetIniString(pszParams, "DBDefault", pszParams); // db name!  real name
						pdeo->m_pszName = file.GetIniString(pszParams, "Label", pszParams); // familiar name, just a label
						pdeo->m_pszExchange = file.GetIniString(pszParams, "DBExchangeTable", "Exchange"); // Exchange table name
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBQueueTable", "Queue");// Queue table name
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszQueue = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBLiveEventsTable", "Events");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszLiveEvents = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBDestinationTable", "Destinations");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszDestination = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBDestinationMediaTable", "Destinations_Media");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszDestinationMedia = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBChannelTable", "");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszChannel = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBMetadataTable", "");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszMetadata = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}
						pszParams = file.GetIniString(pdeo->m_pszDBName, "DBFileTypesTable", "");
						if(pszParams)
						{
							if(strlen(pszParams))
							{
								pdeo->m_pszFileTypes = pszParams;
							}
							else
							{
								free(pszParams); pszParams=NULL;
							}
						}

						pszParams = file.GetIniString(pdeo->m_pszDBName, "Type", ""); 
						if(pszParams)
						{
							if(stricmp(pszParams, "Archivist")==0) { pdeo->m_usType = NUCLEUS_DEP_DATA_ARCHIVIST; nucleus.m_data.m_nIndexMetadataEndpoint = nucleus.m_settings.m_nNumEndpointsInstalled; }
							else if(stricmp(pszParams, "Helios")==0)  { pdeo->m_usType = NUCLEUS_DEP_AUTO_HELIOS; nucleus.m_data.m_nTypeAutomationInstalled=NUCLEUS_DEP_AUTO_HELIOS; nucleus.m_data.m_nIndexAutomationEndpoint=nucleus.m_settings.m_nNumEndpointsInstalled;}
							else if(stricmp(pszParams, "Sentinel")==0){ pdeo->m_usType = NUCLEUS_DEP_AUTO_SENTINEL; nucleus.m_data.m_nTypeAutomationInstalled=NUCLEUS_DEP_AUTO_SENTINEL; nucleus.m_data.m_nIndexAutomationEndpoint=nucleus.m_settings.m_nNumEndpointsInstalled;}
							else if(stricmp(pszParams, "Prospero")==0) pdeo->m_usType = NUCLEUS_DEP_EDGE_PROSPERO;
							else if(stricmp(pszParams, "Luminary")==0) pdeo->m_usType = NUCLEUS_DEP_EDGE_LUMINARY;
							else if(stricmp(pszParams, "Libretto")==0) pdeo->m_usType = NUCLEUS_DEP_EDGE_LIBRETTO;
							else if(stricmp(pszParams, "Radiance")==0) pdeo->m_usType = NUCLEUS_DEP_EDGE_RADIANCE;
							else if(stricmp(pszParams, "Barbero")==0) pdeo->m_usType = NUCLEUS_DEP_EDGE_BARBERO;
							free(pszParams); pszParams=NULL;
						}

						CNucleusEndpointObject** ppObj = new CNucleusEndpointObject*[nucleus.m_settings.m_nNumEndpointsInstalled+1];
						if(ppObj)
						{
							int o=0;
							if((nucleus.m_settings.m_ppEndpointObject)&&(nucleus.m_settings.m_nNumEndpointsInstalled>0))
							{
								while(o<nucleus.m_settings.m_nNumEndpointsInstalled)
								{
									ppObj[o] = nucleus.m_settings.m_ppEndpointObject[o];
									o++;
								}
								delete [] nucleus.m_settings.m_ppEndpointObject;

							}
							ppObj[nucleus.m_settings.m_nNumEndpointsInstalled] = pdeo;
							nucleus.m_settings.m_ppEndpointObject = ppObj;
							nucleus.m_settings.m_nNumEndpointsInstalled++;
						}
						else
							delete pdeo;
					}
				}

				free(pszParams); pszParams=NULL;
			}
			nDep++;
		}

		if(nucleus.m_settings.m_nNumEndpointsInstalled<nNumEndpoints)
		{
			//**MSG hmmmm..cant do till later on...
		}

		if(pszParams) free(pszParams); pszParams=NULL;
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
	}

	bool bUseLog = false;
	bool bUseEmail = false;
	bool bLogNetworkErrors = false;
	if(nucleus.m_settings.m_bUseLog)
	{
		bUseLog = nucleus.m_settings.m_bUseLog;

		// for logfiles, we need params, and they must be in this format:
		//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
//		pszParams = file.GetIniString("Messager", "LogFileIni", "Nucleus|YD||1|");
//		AfxMessageBox(pszParams);
		int nRegisterCode=0;

		nRegisterCode = nucleus.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
			"log", 
			nucleus.m_settings.m_pszProcessedFileSpec?nucleus.m_settings.m_pszProcessedFileSpec:nucleus.m_settings.m_pszFileSpec, 
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
			nucleus.m_data.SetStatusText(errorstring, (NUCLEUS_STATUS_FAIL_LOG|NUCLEUS_STATUS_ERROR));
		}

		if(pszParams) free(pszParams); pszParams=NULL;
	}

	nucleus.m_msgr.DM(MSG_ICONINFO, NULL, "Nucleus", "--------------------------------------------------\n\
-------------- Nucleus %s start --------------", NUCLEUS_CURRENT_VERSION);  //(Dispatch message)


	if(nucleus.m_settings.m_bUseEmail)
	{
		bUseEmail = nucleus.m_settings.m_bUseEmail;
				// for smtp, we need params, and they must be in this format:
				// SMTP address[: override port (optional)] | distribution lists | from email | subject spec | localhostname | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
				// note on distribution lists:
				// we want to give the option to map message type and severity to different address lists.
				// the distribution lists field format is this:
				// ff:x@x.com,q@q.com;0x:B@b.com
				// the : delimits the type and severity flag specifier from the email address list for that set. 
				// the email addresses are delimited by comma
				// the sets are delimited by ;
				// the delimiters are not allowed in addresses, as described here: http://en.wikipedia.org/wiki/E-mail_address
				// on the type and severity specifiers, it is like this:
				// the subfield is a 2 character hex code, sort of.
				// severity is the first char, possible values are 0,1,2,3,4, for normal, low, medium, high, critical
				// additionally, f means all
				// type is the second char, possible values are 0-8, for the follwing:
				// #define MSG_ICONNONE								0x00000000  // default, nothing
				// #define MSG_ICONQUESTION						0x00000001  // ? icon
				// #define MSG_ICONEXCLAMATION				0x00000002  // ! icon
				// #define MSG_ICONERROR							0x00000003  // X icon
				// #define MSG_ICONSTOP								0x00000004  // stop sign icon
				// #define MSG_ICONHAND								0x00000005  // hand icon
				// #define MSG_ICONINFO								0x00000006  // (i) icon 
				// #define MSG_ICONUSER1							0x00000007  
				// #define MSG_ICONUSER2							0x00000008  
				// ff means send all messages to the address list
				// 00 means send all messages with severity normal and no "icon" to the address list
				// 03 means send all messages with severity normal and error type to the address list
				// 43 means send all messages with severity critical and error type to the address list
				// you can set up different address lists with the same addresses - one email per dispatched message will be sent in the case of overlap.

		int nRegisterCode=0;

		// NOT a default handler!
		nRegisterCode = nucleus.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
			"email", 
			nucleus.m_settings.m_pszProcessedMailSpec?nucleus.m_settings.m_pszProcessedMailSpec:nucleus.m_settings.m_pszMailSpec,
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
			nucleus.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
			nucleus.m_msgr.DM(MSG_ICONERROR, NULL, "Nucleus:smtp_init", errorstring);  //(Dispatch message)
		}

//		if(pszParams) free(pszParams); pszParams=NULL;
	}

//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
//	nucleus.m_http.InitializeMessaging(&nucleus.m_msgr);
//	nucleus.m_net.InitializeMessaging(&nucleus.m_msgr);
	if(nucleus.m_settings.m_bLogNetworkErrors) 
	{
		bLogNetworkErrors = nucleus.m_settings.m_bLogNetworkErrors;
		if(nucleus.m_net.InitializeMessaging(&nucleus.m_msgr)==0)
		{
			nucleus.m_data.m_bNetworkMessagingInitialized=true;
		}
	}
//	g_is2.InitializeMessaging(&nucleus.m_msgr);


// TODO: here, get the DB and pull any override settings.
	//****
	CDBconn* pdbConn = db.CreateNewConnection(nucleus.m_settings.m_pszDSN, nucleus.m_settings.m_pszUser, nucleus.m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			nucleus.m_data.SetStatusText(errorstring, (NUCLEUS_STATUS_FAIL_DB|NUCLEUS_STATUS_ERROR));
			nucleus.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:database_connect", errorstring);  //(Dispatch message)
		}
		else
		{
			nucleus.m_settings.m_pdbConn = pdbConn;
			nucleus.m_settings.m_pdb = &db;
			nucleus.m_data.m_pdbConn = pdbConn;
			nucleus.m_data.m_pdb = &db;
			if(nucleus.m_settings.GetFromDatabase(errorstring)<NUCLEUS_SUCCESS)
			{
				nucleus.m_data.SetStatusText(errorstring, (NUCLEUS_STATUS_FAIL_DB|NUCLEUS_STATUS_ERROR));
				nucleus.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:database_get", errorstring);  //(Dispatch message)
			}
			else
			{
				nucleus.m_data.m_nLastSettingsMod = nucleus.m_data.m_nSettingsMod;
				if((!nucleus.m_settings.m_bUseEmail)&&(bUseEmail))
				{
					bUseEmail = false;
					// reset it
					nucleus.m_msgr.DM(MSG_ICONINFO, NULL, "Nucleus", "Shutting down email functions.");  //(Dispatch message)

//						Sleep(250); // let the message get there.
					nucleus.m_msgr.RemoveDestination("email");

				}
				if((!nucleus.m_settings.m_bLogNetworkErrors)&&(bLogNetworkErrors))
				{
					// reset it
					nucleus.m_msgr.DM(MSG_ICONINFO, NULL, "Nucleus", "Shutting down network logging.");  //(Dispatch message)
					if(nucleus.m_data.m_bNetworkMessagingInitialized)
					{
						nucleus.m_net.UninitializeMessaging();  // void return
						nucleus.m_data.m_bNetworkMessagingInitialized=false;
					}

				}
				if((!nucleus.m_settings.m_bUseLog)&&(bUseLog))
				{
					// reset it
					nucleus.m_msgr.DM(MSG_ICONINFO, NULL, "Nucleus", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

					Sleep(250); // let the message get there.
					nucleus.m_msgr.RemoveDestination("log");

				}
			}
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", nucleus.m_settings.m_pszDSN, nucleus.m_settings.m_pszUser, nucleus.m_settings.m_pszPW); 
		nucleus.m_data.SetStatusText(errorstring, (NUCLEUS_STATUS_FAIL_DB|NUCLEUS_STATUS_ERROR));
		nucleus.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:database_init", errorstring);  //(Dispatch message)

		//**MSG
	}


	CDBconn* pdbMessageConn = dbMsg.CreateNewConnection(nucleus.m_settings.m_pszDSN, nucleus.m_settings.m_pszUser, nucleus.m_settings.m_pszPW);
	if(pdbMessageConn)
	{
		if(dbMsg.ConnectDatabase(pdbMessageConn, errorstring)<DB_SUCCESS)
		{
			nucleus.m_data.SetStatusText(errorstring, (NUCLEUS_STATUS_FAIL_DB|NUCLEUS_STATUS_ERROR));
			nucleus.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:database_connect_msg", errorstring);  //(Dispatch message)
		}
		else
		{
			nucleus.m_data.m_pdbMessageConn = pdbMessageConn;
			nucleus.m_data.m_pdbMessage = &dbMsg;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create msg connection for %s:%s:%s", nucleus.m_settings.m_pszDSN, nucleus.m_settings.m_pszUser, nucleus.m_settings.m_pszPW); 
		nucleus.m_data.SetStatusText(errorstring, (NUCLEUS_STATUS_FAIL_DB|NUCLEUS_STATUS_ERROR));
		nucleus.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:database_init_msg", errorstring);  //(Dispatch message)

		//**MSG
	}


//init command and status listeners.
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing command server on %d", nucleus.m_settings.m_usCommandPort); 
	nucleus.m_data.SetStatusText(errorstring, NUCLEUS_STATUS_CMDSVR_START);
	nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:command_server_init", errorstring);  //(Dispatch message)

	if(nucleus.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = nucleus.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "NucleusCommandServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = NucleusCommandHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &nucleus;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &nucleus.m_net;					// pointer to the object with the Message function.


		if(nucleus.m_net.StartServer(pServer, &nucleus.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			nucleus.m_data.SetStatusText(errorstring, NUCLEUS_STATUS_CMDSVR_ERROR);
			nucleus.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:command_server_init", errorstring);  //(Dispatch message)
			nucleus.SendMsg(CX_SENDMSG_ERROR, "Nucleus:command_server_init", errorstring);
		}
		else
		{
//AfxMessageBox("Q1");
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Command server listening on %d", nucleus.m_settings.m_usCommandPort);
			nucleus.m_data.SetStatusText(errorstring, NUCLEUS_STATUS_CMDSVR_RUN);
			nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:command_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing XML server on %d", nucleus.m_settings.m_usStatusPort); 
	nucleus.m_data.SetStatusText(errorstring, NUCLEUS_STATUS_STATUSSVR_START);
	nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:xml_server_init", errorstring);  //(Dispatch message)

	if(nucleus.m_settings.m_usStatusPort>0)
	{
//AfxMessageBox("Q1");
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = nucleus.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "NucleusXMLServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = NucleusXMLHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &nucleus;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &nucleus.m_net;					// pointer to the object with the Message function.

		if(nucleus.m_net.StartServer(pServer, &nucleus.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			nucleus.m_data.SetStatusText(errorstring, NUCLEUS_STATUS_STATUSSVR_ERROR);
			nucleus.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:xml_server_init", errorstring);  //(Dispatch message)
			nucleus.SendMsg(CX_SENDMSG_ERROR, "Nucleus:xml_server_init", errorstring);
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML server listening on %d", nucleus.m_settings.m_usStatusPort);
			nucleus.m_data.SetStatusText(errorstring, NUCLEUS_STATUS_STATUSSVR_RUN);
			nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:xml_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

//AfxMessageBox("QQ");

	
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Nucleus is initializing...");
	nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:init", errorstring);  //(Dispatch message)
	nucleus.m_data.SetStatusText(errorstring, NUCLEUS_STATUS_THREAD_SPARK);

	// now connect all the registered servers
	// this is the list of servers in the db that get connected


	if(
		  (!(nucleus.m_settings.m_ulMainMode&NUCLEUS_MODE_CLONE))
		&&(nucleus.m_data.m_key.m_bValid)  // must have a valid license
		&&(
				(!nucleus.m_data.m_key.m_bExpires)
			||((nucleus.m_data.m_key.m_bExpires)&&(!nucleus.m_data.m_key.m_bExpired))
			||((nucleus.m_data.m_key.m_bExpires)&&(nucleus.m_data.m_key.m_bExpireForgiveness)&&(nucleus.m_data.m_key.m_ulExpiryDate+nucleus.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
			)
		&&(
				(!nucleus.m_data.m_key.m_bMachineSpecific)
			||((nucleus.m_data.m_key.m_bMachineSpecific)&&(nucleus.m_data.m_key.m_bValidMAC))
			)
		)
	{
//	nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "here");  Sleep(250);//(Dispatch message)
		nucleus.m_data.GetMappings();
		nucleus.m_data.GetEventRules();
		nucleus.m_data.GetTimingColumns();
		nucleus.m_data.GetParameterRules();

//	nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "X2");  Sleep(250);//(Dispatch message)
//		nucleus.m_data.GetChannels();
//	nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "X3");  Sleep(250);//(Dispatch message)
	}
//	nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "X4");  Sleep(250);//(Dispatch message)
	
	if((nucleus.m_data.m_ulFlags&NUCLEUS_ICON_MASK) != NUCLEUS_STATUS_ERROR)
	{
		nucleus.m_data.m_ulFlags &= ~NUCLEUS_ICON_MASK;
		nucleus.m_data.m_ulFlags |= NUCLEUS_STATUS_OK;  // green - we want run to be blue when something in progress
	}

//	nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "here2");  Sleep(250);//(Dispatch message)

// initialize nucleus (make this not persistent for now)
	CNetData* pdata = new CNetData;
	if(pdata)
	{

		pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA;      // defined type - indicates which protocol to use, structure of data

		pdata->m_ucCmd = CX_REQ_HELLO;       // the command byte
		pdata->m_ucSubCmd = 0;       // the subcommand byte

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
		if(pch)
		{
			sprintf(pch, "%s:%d:%d:%d:%d:%s", 
				nucleus.m_data.m_pszHost,
				nucleus.m_settings.m_usCommandPort,
				nucleus.m_settings.m_usStatusPort,
				CX_TYPE_PROCESS,
				theApp.m_nPID,
				nucleus.m_settings.m_pszName?nucleus.m_settings.m_pszName:"Nucleus"
				);

			pdata->m_pucData =  (unsigned char*) pch;
			pdata->m_ulDataLen = strlen(pch);
		}
//	nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "here3");  Sleep(250);//(Dispatch message)

		SOCKET s = NULL; 
//AfxMessageBox("sending");
//AfxMessageBox( nucleus.m_data.m_pszCortexHost );
//AfxMessageBox( (char*)pdata->m_pucData );
		if(nucleus.m_net.SendData(pdata, nucleus.m_data.m_pszCortexHost, nucleus.m_data.m_usCortexCommandPort, 5000, 0, NET_SND_CMDTOSVR, &s)>=NET_SUCCESS)
		{
			//send ack
//			AfxMessageBox("ack");
			nucleus.m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);
		}
		else
		{
//			AfxMessageBox("could not send");
		}
		nucleus.m_net.CloseConnection(s);

		delete pdata;

	}

//	nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "here4");  Sleep(250);//(Dispatch message)
	_ftime(&nucleus.m_data.m_timebAutomationTick); // the last time check inside the thread
//	_ftime(&nucleus.m_data.m_timebNearTick); // the last time check inside the thread
//	_ftime(&nucleus.m_data.m_timebFarTick); // the last time check inside the thread
//	_ftime(&nucleus.m_data.m_timebTriggerTick); // the last time check inside the thread



	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Nucleus main thread running.");  
	nucleus.m_data.SetStatusText(errorstring, NUCLEUS_STATUS_THREAD_RUN);
	nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:init", "Nucleus is initialized, main thread is running.");  Sleep(50); //(Dispatch message)
	nucleus.SendMsg(CX_SENDMSG_INFO, "Nucleus:init", "Nucleus %s main thread running.", NUCLEUS_CURRENT_VERSION);
//	nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", errorstring);  Sleep(250);//(Dispatch message)


	//have to start the automation and analysis threads
	nucleus.m_data.m_bAutomationThreadStarted=false;
//	nucleus.m_data.m_bFarAnalysisThreadStarted=false;
	nucleus.m_data.m_bGlobalAnalysisThreadStarted=false;


	if(_beginthread(NucleusGlobalAutomationThread, 0, (void*)&nucleus)==-1)
	{
		//error.  try again once
		Sleep(1000);
		if(_beginthread(NucleusGlobalAutomationThread, 0, (void*)&nucleus)==-1)
		{
			//report failure
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Nucleus global automation thread failure.");  
			nucleus.m_data.SetStatusText(errorstring, (NUCLEUS_STATUS_ERROR|NUCLEUS_STATUS_THREAD_ERROR));
			nucleus.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:global_automation_thread_init", errorstring);  //(Dispatch message)
			nucleus.SendMsg(CX_SENDMSG_ERROR, "Nucleus:global_automation_thread_init", errorstring);
		}
		else
		{
			nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:global_automation_thread_init", "Nucleus global automation thread initialized.");  //(Dispatch message)
		}
	}
	else
	{
		nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:global_automation_thread_init", "Nucleus global automation thread initialized.");  //(Dispatch message)
	}
	Sleep(250);
/*

	if(_beginthread(NucleusFarAnalysisThread, 0, (void*)&nucleus)==-1)
	{
		//error.  try again once
		Sleep(1000);
		if(_beginthread(NucleusFarAnalysisThread, 0, (void*)&nucleus)==-1)
		{
			//report failure
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Nucleus analysis thread failure.");  
			nucleus.m_data.SetStatusText(errorstring, (NUCLEUS_STATUS_THREAD_ERROR|NUCLEUS_STATUS_ERROR));
			nucleus.m_msgr.DM(MSG_ICONERROR, NULL, "Nucleus:analysis_thread_init", errorstring);  //(Dispatch message)
			nucleus.SendMsg(CX_SENDMSG_ERROR, "Nucleus:analysis_thread_init", errorstring);
		}
		else
		{
			nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:analysis_thread_init", "Nucleus analysis thread initialized.");  //(Dispatch message)
		}
	}
	else
	{
		nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:analysis_thread_init", "Nucleus analysis thread initialized.");  //(Dispatch message)
	}
	Sleep(250);

*/
	if(_beginthread(NucleusGlobalAnalysisThread, 0, (void*)&nucleus)==-1)
	{
		//error.  try again once
		Sleep(1000);
		if(_beginthread(NucleusGlobalAnalysisThread, 0, (void*)&nucleus)==-1)
		{
			//report failure
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Nucleus global analysis thread failure.");  
			nucleus.m_data.SetStatusText(errorstring, (NUCLEUS_STATUS_THREAD_ERROR|NUCLEUS_STATUS_ERROR));
			nucleus.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:near_analysis_thread_init", errorstring);  //(Dispatch message)
			nucleus.SendMsg(CX_SENDMSG_ERROR, "Nucleus:near_analysis_thread_init", errorstring);
		}
		else
		{
			nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:global_analysis_thread_init", "Nucleus global analysis thread initialized.");  //(Dispatch message)
		}
	}
	else
	{
		nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:global_analysis_thread_init", "Nucleus global analysis thread initialized.");  //(Dispatch message)
	}
	Sleep(250);
/*

	// ******************************************************************
	// We really need to spawn one trigger thread per automation channel.
	// FOR DEMO, just one thread.
	// ******************************************************************
	if(_beginthread(NucleusTriggerThread, 0, (void*)&nucleus)==-1)
	{
		//error.  try again once
		Sleep(1000);
		if(_beginthread(NucleusTriggerThread, 0, (void*)&nucleus)==-1)
		{
			//report failure
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Nucleus trigger thread failure.");  
			nucleus.m_data.SetStatusText(errorstring, (NUCLEUS_STATUS_THREAD_ERROR|NUCLEUS_STATUS_ERROR));
			nucleus.m_msgr.DM(MSG_ICONERROR, NULL, "Nucleus:trigger_thread_init", errorstring);  //(Dispatch message)
			nucleus.SendMsg(CX_SENDMSG_ERROR, "Nucleus:trigger_thread_init", errorstring);
		}
		else
		{
			nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:trigger_thread_init", "Nucleus trigger thread initialized.");  //(Dispatch message)
		}
	}
	else
	{
		nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:trigger_thread_init", "Nucleus trigger thread initialized.");  //(Dispatch message)
	}
	Sleep(250);
*/

	_timeb timebCheckMods;
	_ftime( &timebCheckMods );
///AfxMessageBox("xxxxx");

//	char szSQL[DB_SQLSTRING_MAXLEN];
	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &nucleus.m_data.m_timebTick );
//	nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%d.%03d", nucleus.m_data.m_timebTick.time, nucleus.m_data.m_timebTick.millitm);   Sleep(250);//(Dispatch message)

// monitor changes in exchange db and react.
		if(
					(nucleus.m_data.m_timebTick.time > timebCheckMods.time )
				||((nucleus.m_data.m_timebTick.time == timebCheckMods.time)&&(nucleus.m_data.m_timebTick.millitm >= timebCheckMods.millitm))
				&&(!g_bKillThread)
			)
		{
//	nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "time to check");  Sleep(250);//(Dispatch message)
			timebCheckMods.time = nucleus.m_data.m_timebTick.time + nucleus.m_settings.m_ulModsIntervalMS/1000; 
			timebCheckMods.millitm = nucleus.m_data.m_timebTick.millitm + (unsigned short)(nucleus.m_settings.m_ulModsIntervalMS%1000); // fractional second updates
			if(timebCheckMods.millitm>999)
			{
				timebCheckMods.time++;
				timebCheckMods.millitm%=1000;
			}


//	nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "checking db connected");   Sleep(250);//(Dispatch message)
			if(pdbConn)//&&(pdbConn->m_bConnected))
			{
//AfxMessageBox("x");
//	nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "checkmods");  //(Dispatch message)
//				nucleus.m_data.ReleaseRecordSet();
///				nucleus.m_data.CheckDatabaseMods();  this was there twice!  2x the db hits, oh no!

				strcpy(errorstring, "");

if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_CHECKMODS) 	
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:checkmods", "pdbConn = %d, about to check mods", pdbConn );  // Sleep(50);//(Dispatch message)
				if(nucleus.m_data.CheckDatabaseMods(errorstring)==NUCLEUS_ERROR)
				{
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_CHECKMODS) 	
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:checkmods", "ERROR checking mods" );  // Sleep(50);//(Dispatch message)
					if(!nucleus.m_data.m_bCheckModsWarningSent)
					{
						nucleus.m_msgr.DM(MSG_ICONERROR, NULL, "CheckDatabaseMods", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
						nucleus.m_data.m_bCheckModsWarningSent=true;
					}
				}
				else
				{
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_CHECKMODS) 	
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:checkmods", "Checked mods: erm %d, lerm %d", nucleus.m_data.m_nEventRulesMod, nucleus.m_data.m_nLastEventRulesMod );  // Sleep(50);//(Dispatch message)
 					if(nucleus.m_data.m_bCheckModsWarningSent)
					{
						nucleus.m_msgr.DM(MSG_ICONINFO, NULL, "CheckDatabaseMods", "Resumed checking."); // Sleep(50); //(Dispatch message)
					}
					nucleus.m_data.m_bCheckModsWarningSent=false;
				}

				if((nucleus.m_data.m_timebTick.time > nucleus.m_data.m_timebAutoPurge.time + nucleus.m_settings.m_nAutoPurgeInterval)&&(pdbMessageConn))
				{
					_ftime(&nucleus.m_data.m_timebAutoPurge);

					if(nucleus.m_settings.m_nAutoPurgeMessageDays>0)
					{
						if(nucleus.m_data.CheckMessages(errorstring)==NUCLEUS_ERROR)
						{
							if(!nucleus.m_data.m_bCheckMsgsWarningSent)
							{
								nucleus.m_msgr.DM(MSG_ICONERROR, NULL, "CheckMessages", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								nucleus.m_data.m_bCheckMsgsWarningSent=true;
							}
						}
						else
						if(nucleus.m_data.m_bCheckMsgsWarningSent)
						{
							nucleus.m_msgr.DM(MSG_ICONINFO, NULL, "CheckMessages", "Resumed checking messages for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						nucleus.m_data.m_bCheckMsgsWarningSent=false;
					}

					if(nucleus.m_settings.m_nAutoPurgeAsRunDays>0)
					{
						if(nucleus.m_data.CheckAsRun(errorstring)==NUCLEUS_ERROR)
						{
							if(!nucleus.m_data.m_bCheckAsRunWarningSent)
							{
								nucleus.m_msgr.DM(MSG_ICONERROR, NULL, "CheckAsRun", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								nucleus.m_data.m_bCheckAsRunWarningSent=true;
							}
						}
						else
						if(nucleus.m_data.m_bCheckMsgsWarningSent)
						{
							nucleus.m_msgr.DM(MSG_ICONINFO, NULL, "CheckAsRun", "Resumed checking as-run for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						nucleus.m_data.m_bCheckAsRunWarningSent=false;
					}

				}



				if(nucleus.m_data.m_nSettingsMod != nucleus.m_data.m_nLastSettingsMod)
				{
					if(nucleus.m_settings.GetFromDatabase()>=NUCLEUS_SUCCESS)
					{
						nucleus.m_data.m_nLastSettingsMod = nucleus.m_data.m_nSettingsMod;


						// check for stuff to change

						// network messaging
						if(nucleus.m_settings.m_bLogNetworkErrors) 
						{
							if(!nucleus.m_data.m_bNetworkMessagingInitialized)
							{
								if(nucleus.m_net.InitializeMessaging(&nucleus.m_msgr)==0)
								{
									nucleus.m_data.m_bNetworkMessagingInitialized=true;
								}
							}
						}
						else
						{
							if(nucleus.m_data.m_bNetworkMessagingInitialized)
							{
								nucleus.m_net.UninitializeMessaging();  // void return
								nucleus.m_data.m_bNetworkMessagingInitialized=false;
							}
						}

						// logging and email messaging:

						if(!nucleus.m_settings.m_bUseEmail)
						{
							if(bUseEmail)
							{
								bUseEmail = false;
								// reset it
								nucleus.m_msgr.DM(MSG_ICONINFO, NULL, "Nucleus", "Shutting down email functions.");  //(Dispatch message)

		//						Sleep(250); // let the message get there.
								nucleus.m_msgr.RemoveDestination("email");
							}
						}
						else
						{
							if(!bUseEmail)
							{
								bUseEmail = true;
								int nRegisterCode=0;

								// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
								nRegisterCode = nucleus.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
									"email", 
									nucleus.m_settings.m_pszProcessedMailSpec?nucleus.m_settings.m_pszProcessedMailSpec:nucleus.m_settings.m_pszMailSpec,
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
									nucleus.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
									nucleus.m_msgr.DM(MSG_ICONERROR, NULL, "Nucleus:smtp_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=nucleus.m_msgr.GetDestIndex("email");
								if(nIndex>=0)
								{
									if((nucleus.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(nucleus.m_settings.m_pszProcessedMailSpec?nucleus.m_settings.m_pszProcessedMailSpec:nucleus.m_settings.m_pszMailSpec))
									{
										if(strcmp(nucleus.m_msgr.m_ppDest[nIndex]->m_pszParams, (nucleus.m_settings.m_pszProcessedMailSpec?nucleus.m_settings.m_pszProcessedMailSpec:nucleus.m_settings.m_pszMailSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = nucleus.m_msgr.ModifyDestination(
												"email", 
												nucleus.m_settings.m_pszProcessedMailSpec?nucleus.m_settings.m_pszProcessedMailSpec:nucleus.m_settings.m_pszMailSpec,
												MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify SMTP object!\n code: %d", nRegisterCode); 
												//nucleus.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												nucleus.m_msgr.DM(MSG_ICONERROR, NULL, "Nucleus:smtp_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}

						if(!nucleus.m_settings.m_bUseLog)
						{
							if(bUseLog)
							{
								bUseLog = false;
								// reset it
								nucleus.m_msgr.DM(MSG_ICONINFO, NULL, "Nucleus", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

								Sleep(250); // let the message get there.
								nucleus.m_msgr.RemoveDestination("log");
							}
						}
						else
						{
							if(!bUseLog)
							{
								bUseLog = true;
								int nRegisterCode=0;

								nRegisterCode = nucleus.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
									"log", 
									nucleus.m_settings.m_pszProcessedFileSpec?nucleus.m_settings.m_pszProcessedFileSpec:nucleus.m_settings.m_pszFileSpec, 
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
									nucleus.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_LOG|CX_STATUS_ERROR));
									nucleus.m_msgr.DM(MSG_ICONERROR, NULL, "Nucleus:log_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=nucleus.m_msgr.GetDestIndex("log");
								if(nIndex>=0)
								{
									if((nucleus.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(nucleus.m_settings.m_pszProcessedFileSpec?nucleus.m_settings.m_pszProcessedFileSpec:nucleus.m_settings.m_pszFileSpec))
									{
										if(strcmp(nucleus.m_msgr.m_ppDest[nIndex]->m_pszParams, (nucleus.m_settings.m_pszProcessedFileSpec?nucleus.m_settings.m_pszProcessedFileSpec:nucleus.m_settings.m_pszFileSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = nucleus.m_msgr.ModifyDestination(
												"log", 
												nucleus.m_settings.m_pszProcessedFileSpec?nucleus.m_settings.m_pszProcessedFileSpec:nucleus.m_settings.m_pszFileSpec,
												MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify log object!\n code: %d", nRegisterCode); 
												//nucleus.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												nucleus.m_msgr.DM(MSG_ICONERROR, NULL, "Nucleus:log_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}








					}
				}
				if(
					  (!nucleus.m_data.m_bProcessSuspended)

					&&(nucleus.m_data.m_key.m_bValid)  // must have a valid license
					&&(
							(!nucleus.m_data.m_key.m_bExpires)
						||((nucleus.m_data.m_key.m_bExpires)&&(!nucleus.m_data.m_key.m_bExpired))
						||((nucleus.m_data.m_key.m_bExpires)&&(nucleus.m_data.m_key.m_bExpireForgiveness)&&(nucleus.m_data.m_key.m_ulExpiryDate+nucleus.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!nucleus.m_data.m_key.m_bMachineSpecific)
						||((nucleus.m_data.m_key.m_bMachineSpecific)&&(nucleus.m_data.m_key.m_bValidMAC))
						)
					)
				{
					bool bAnalysisChanges = false;
					if(nucleus.m_data.m_nMappingMod != nucleus.m_data.m_nLastMappingMod)
					{
						if(nucleus.m_data.GetMappings()>=NUCLEUS_SUCCESS)
						{
							nucleus.m_data.m_nLastMappingMod = nucleus.m_data.m_nMappingMod;
						}
					}
					if(nucleus.m_data.m_nEventRulesMod != nucleus.m_data.m_nLastEventRulesMod)
					{
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_CHECKMODS) 	
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:checkmods", "Result of checkmods: erm %d, lerm %d", nucleus.m_data.m_nEventRulesMod, nucleus.m_data.m_nLastEventRulesMod );  // Sleep(50);//(Dispatch message)
						// was this:
//						if(nucleus.m_data.GetEventRules()>=NUCLEUS_SUCCESS)
						// now this, and below timing cols removed
						if((nucleus.m_data.GetEventRules()>=NUCLEUS_SUCCESS)&&(nucleus.m_data.GetTimingColumns()>=NUCLEUS_SUCCESS))
						{
							// this was originally not forced reanalyzed... but dont know why?  did I omit accidentally?
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PROCESS) 	
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Analysis change from EventRulesMod %d", nucleus.m_data.m_nEventRulesMod );  // Sleep(50);//(Dispatch message)
							bAnalysisChanges = true;
							nucleus.m_data.m_nLastEventRulesMod = nucleus.m_data.m_nEventRulesMod;
						}
					}
					if(nucleus.m_data.m_nEventsMod != nucleus.m_data.m_nLastEventsMod)
					{
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "getting Timing Column names"); // Sleep(100); //(Dispatch message)

/*
						if(nucleus.m_data.GetTimingColumns()>=NUCLEUS_SUCCESS)
						{
							bAnalysisChanges = true;
*/ // just reset it, we do timing cols above now.
							nucleus.m_data.m_nLastEventsMod = nucleus.m_data.m_nEventsMod;
/*
						}
*/
					}
					if(nucleus.m_data.m_nParameterRulesMod != nucleus.m_data.m_nLastParameterRulesMod)
					{
						if(nucleus.m_data.GetParameterRules()>=NUCLEUS_SUCCESS)
						{
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PROCESS) 	
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Analysis change from ParameterRulesMod %d", nucleus.m_data.m_nParameterRulesMod );  // Sleep(50);//(Dispatch message)
							bAnalysisChanges = true;
							nucleus.m_data.m_nLastParameterRulesMod = nucleus.m_data.m_nParameterRulesMod;
						}
					}
					if(nucleus.m_data.m_nConnectionsMod != nucleus.m_data.m_nLastConnectionsMod)
					{

//			nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Channel info view has changed, calling GetConnections");   Sleep(10);//(Dispatch message)
						if(nucleus.m_data.GetConnections()>=NUCLEUS_SUCCESS)
						{
			nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "GetConnections returned successfully"); //  Sleep(10);//(Dispatch message)
							nucleus.m_data.m_nLastConnectionsMod = nucleus.m_data.m_nConnectionsMod;
						}
						else
						{
			nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "GetConnections returned an error"); //  Sleep(10);//(Dispatch message)

						}
					}

					if(bAnalysisChanges)
					{
						// go thru and indicate changes to each channel
						nucleus.m_data.m_bForceAnalysis = true;


/*
// moved to a delay thread.
						if(
							  (nucleus.m_settings.m_ppEndpointObject)
							&&(nucleus.m_data.m_nIndexAutomationEndpoint>=0)
							&&(nucleus.m_data.m_nIndexAutomationEndpoint<nucleus.m_settings.m_nNumEndpointsInstalled)
							&&(nucleus.m_settings.m_ppEndpointObject[nucleus.m_data.m_nIndexAutomationEndpoint])
							)
						{
							CNucleusEndpointObject* pAutoObj = nucleus.m_settings.m_ppEndpointObject[nucleus.m_data.m_nIndexAutomationEndpoint];

							if(pAutoObj) pAutoObj->m_nLastModLiveEvents = -1;  // triggers global analysis rules.
// above line now taken care of above by nucleus.m_data.m_bForceAnalysis = true;

							if((pAutoObj->m_ppChannelObj)&&(pAutoObj->m_nNumChannelObjects))
							{
								int cho=0;
								while(cho<pAutoObj->m_nNumChannelObjects)
								{
									if(pAutoObj->m_ppChannelObj[cho])
									{
										pAutoObj->m_ppChannelObj[cho]->m_bTriggerEventsChanged = true;
//										pAutoObj->m_ppChannelObj[cho]->m_bNearEventsChanged = true;

									}
									cho++;
								}
							}
						}
*/

						if((g_pnucleus->m_settings.m_bEnableEventTriggerNotification)&&(!g_pnucleus->m_data.m_bDelayingTriggerNotification))
						{
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PROCESS) 	
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "  >>* Spawning NucleusTriggerDelayNotificationThread from main thread");  // Sleep(50);//(Dispatch message)

							if(_beginthread(NucleusTriggerDelayNotificationThread, 0, (void*)NULL)==-1)
							{
								//error.
				nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Error starting trigger delay notification thread");  // Sleep(10);//(Dispatch message)

								//**MSG
							}
						}

					}

				}
/*
				if(nucleus.m_data.m_nConnectionsMod != nucleus.m_data.m_nLastConnectionsMod)
				{
//			nucleus.m_msgr.DM( MSG_ICONHAND, NULL, "Nucleus:debug", "getting connections again");   Sleep(250);//(Dispatch message)
					if(nucleus.m_data.GetConnections()>=NUCLEUS_SUCCESS)
					{
						nucleus.m_data.m_nLastConnectionsMod = nucleus.m_data.m_nConnectionsMod;
					}
				}
				if(nucleus.m_data.m_nChannelsMod != nucleus.m_data.m_nLastChannelsMod)
				{
//			nucleus.m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "getting channels again");   Sleep(250);//(Dispatch message)
					if(nucleus.m_data.GetChannels()>=NUCLEUS_SUCCESS)
					{
						nucleus.m_data.m_nLastChannelsMod = nucleus.m_data.m_nChannelsMod;
					}
				}
*/
			}
			else
			{
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_CHECKMODS) 	
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:checkmods", "pdbConn was NULL" );  // Sleep(50);//(Dispatch message)
			}
		}  // check mod interval

		MSG msg;

//		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
//			AfxGetApp()->PumpMessage();


//AfxMessageBox("zoinks");
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();

		Sleep(1); 
//		Sleep(1000);  // stalls window procedure
	}

	nucleus.m_data.m_ulFlags &= ~NUCLEUS_STATUS_THREAD_MASK;
	nucleus.m_data.m_ulFlags |= NUCLEUS_STATUS_THREAD_END;

	nucleus.m_msgr.DM(MSG_ICONINFO, NULL, "Nucleus:uninit", "Nucleus is shutting down.");  //(Dispatch message)
	nucleus.SendMsg(CX_SENDMSG_INFO, "Nucleus:uninit", "Nucleus %s is shutting down.", NUCLEUS_CURRENT_VERSION);

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Nucleus is shutting down.");  
	nucleus.m_data.m_ulFlags &= ~CX_ICON_MASK;
	nucleus.m_data.m_ulFlags |= CX_STATUS_UNKNOWN;
	nucleus.m_data.SetStatusText(errorstring, nucleus.m_data.m_ulFlags);


	nucleus.m_data.m_bTabulatorThreadKill = true;
///	nucleus.m_msgr.DM(MSG_ICONINFO, NULL, "Nucleus:uninit", "Shutting down Tabulator connection.");  //(Dispatch message)
	int kt=0;
	while((nucleus.m_data.m_bTabulatorThreadStarted)&&(kt<100))
	{
		Sleep(50);
		kt++;
	}

	if(nucleus.m_data.m_bTabulatorThreadStarted) 
	{
		nucleus.m_msgr.DM(MSG_ICONINFO, NULL, "Nucleus:uninit", "Tabulator connection aborted.");  Sleep(100); // to let the message get there.//(Dispatch message)
	}
	else
	{
		nucleus.m_msgr.DM(MSG_ICONINFO, NULL, "Nucleus:uninit", "Tabulator connection shut down.");  //(Dispatch message)
	}

	kt=0;
	BOOL bAllDone = FALSE;
	while ((kt<100)&&(!bAllDone))
/*
					(nucleus.m_data.m_bAutomationThreadStarted)
				||(nucleus.m_data.m_bNearAnalysisThreadStarted)
				||(nucleus.m_data.m_bFarAnalysisThreadStarted)
				||(nucleus.m_data.m_bTriggerThreadStarted)
				))
*/

	{

		/*

g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ending %d %d %d %d @ %d", 
											nucleus.m_data.m_bAutomationThreadStarted,
											nucleus.m_data.m_bNearAnalysisThreadStarted,
											nucleus.m_data.m_bFarAnalysisThreadStarted,
											nucleus.m_data.m_bTriggerThreadStarted, clock()
											); 
*/
		
		// have to go thru all endpoints until the threads are down.
		
		
		Sleep(50);
		_ftime( &nucleus.m_data.m_timebTick );  // we're still alive.
		kt++;
	}



// shut down all the running objects;
/*
	if(nucleus.m_data.m_ppConnObj)
	{
		int i=0;
		while(i<nucleus.m_data.m_nNumConnectionObjects)
		{
			if(nucleus.m_data.m_ppConnObj[i])
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", nucleus.m_data.m_ppConnObj[i]->m_pszDesc?nucleus.m_data.m_ppConnObj[i]->m_pszDesc:nucleus.m_data.m_ppConnObj[i]->m_pszServerName);  

// **** disconnect servers
				nucleus.m_data.SetStatusText(errorstring, nucleus.m_data.m_ulFlags);
				nucleus.m_msgr.DM(MSG_ICONINFO, NULL, "Nucleus:connection_uninit", errorstring);    //(Dispatch message)

				delete nucleus.m_data.m_ppConnObj[i];
				nucleus.m_data.m_ppConnObj[i] = NULL;
			}
			i++;
		}
		delete [] nucleus.m_data.m_ppConnObj;
	}
	if(nucleus.m_data.m_ppChannelObj)
	{
		int i=0;
		while(i<nucleus.m_data.m_nNumChannelObjects)
		{
			if(nucleus.m_data.m_ppChannelObj[i])
			{
				delete nucleus.m_data.m_ppChannelObj[i];
				nucleus.m_data.m_ppChannelObj[i] = NULL;
			}
			i++;
		}
		delete [] nucleus.m_data.m_ppChannelObj;
	}
*/

/*
	// here's a hard coded one for now
	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no need to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = nucleus.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		nucleus.m_net.OpenConnection(nucleus.m_http.m_pszHost, 10888, &s); // branding hardcode
		nucleus.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		nucleus.m_net.CloseConnection(s); // branding hardcode
	}
*/

//	m_pDlg->SetProgress(NUCLEUSDLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
//	nucleus.m_data.m_ulFlags &= ~NUCLEUS_STATUS_FILESVR_MASK;
//	nucleus.m_data.m_ulFlags |= NUCLEUS_STATUS_FILESVR_END;
//	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down file server....");  
//	nucleus.m_data.SetStatusText(errorstring, nucleus.m_data.m_ulFlags);
//	_ftime( &nucleus.m_data.m_timebTick );
//	nucleus.m_http.EndServer();
	_ftime( &nucleus.m_data.m_timebTick );
//	AfxMessageBox("shutting down command server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down command server....");  
	nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:command_server_uninit", errorstring);  //(Dispatch message)
	nucleus.m_data.SetStatusText(errorstring, NUCLEUS_STATUS_CMDSVR_END);
	_ftime( &nucleus.m_data.m_timebTick );
	nucleus.m_net.StopServer(nucleus.m_settings.m_usCommandPort, 5000, errorstring);
	_ftime( &nucleus.m_data.m_timebTick );
//	AfxMessageBox("shutting down status server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down XML server....");  
	nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:xml_server_uninit", errorstring);  //(Dispatch message)
	nucleus.m_data.SetStatusText(errorstring, NUCLEUS_STATUS_STATUSSVR_END);
	_ftime( &nucleus.m_data.m_timebTick );
	nucleus.m_net.StopServer(nucleus.m_settings.m_usStatusPort, 5000, errorstring);
	_ftime( &nucleus.m_data.m_timebTick );
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Nucleus is exiting.");  
	nucleus.m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:uninit", errorstring);  //(Dispatch message)
	nucleus.m_data.SetStatusText(errorstring, nucleus.m_data.m_ulFlags);



	// save settings.  // dont save them here.  save them on any changes in the main command loop.
	nucleus.m_settings.Settings(false); //write
/*

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// these explicts arent necessary - uncomment to write out a full file to edit...
		file.SetIniString("Main", "Name", nucleus.m_settings.m_pszName);
		file.SetIniString("License", "Key", nucleus.m_settings.m_pszLicense);

		file.SetIniString("FileServer", "IconPath", nucleus.m_settings.m_pszIconPath);
		file.SetIniInt("CommandServer", "ListenPort", nucleus.m_settings.m_usCommandPort);
		file.SetIniInt("StatusServer", "ListenPort", nucleus.m_settings.m_usStatusPort);

		file.SetIniInt("Messager", "UseEmail", nucleus.m_settings.m_bUseEmail?1:0);
		file.SetIniInt("Messager", "UseNet", nucleus.m_settings.m_bUseNetwork?1:0);
		file.SetIniInt("Messager", "UseLog", nucleus.m_settings.m_bUseLog?1:0);
		file.SetIniInt("Messager", "ReportSuccessfulOperation", nucleus.m_settings.m_bReportSuccessfulOperation?1:0);
		file.SetIniInt("Messager", "LogTransfers", nucleus.m_settings.m_bLogTransfers?1:0);

		file.SetIniString("Database", "DSN", nucleus.m_settings.m_pszDSN);
		file.SetIniString("Database", "DBUser", nucleus.m_settings.m_pszUser);
		file.SetIniString("Database", "DBPassword", nucleus.m_settings.m_pszPW);
		file.SetIniString("Database", "SettingsTableName", nucleus.m_settings.m_pszSettings);  // the Settings table name
		file.SetIniString("Database", "ExchangeTableName", nucleus.m_settings.m_pszExchange);  // the Exchange table name
		file.SetIniString("Database", "MessagesTableName", nucleus.m_settings.m_pszMessages);  // the Messages table name

//		file.SetIniInt("HarrisAPI", "UseListCount", nucleus.m_settings.m_bUseListCount?1:0); // get all events up until the list count (otherwise just up to the lookahead)

//		file.SetIniString("Database", "ChannelsTableName", nucleus.m_settings.m_pszChannels);  // the Channels table name
//		file.SetIniString("Database", "ConnectionsTableName", nucleus.m_settings.m_pszConnections);  // the Connections table name
		file.SetIniString("Database", "EventsTableName", nucleus.m_settings.m_pszEvents);  // the LiveEvents table name
		file.SetIniString("Database", "QueueTableName", nucleus.m_settings.m_pszQueue);  // the Queue table name
		file.SetIniString("Database", "RulesTableName", nucleus.m_settings.m_pszRules);  // the Rules table name
				file.SetIniString("Database", "AnalysisViewName", g_pnucleus->m_settings.m_pszTriggerAnalysisView);  // the TriggerAnalysisView name
				file.SetIniString("Database", "AnalyzedTriggerDataTableName", g_pnucleus->m_settings.m_pszAnalyzedTriggerData);  // the AnalyzedTriggerData table name
				file.SetIniString("Database", "AnalyzeRulesProcedureName", g_pnucleus->m_settings.m_pszAnalyzeRulesProc);  // the spAnalyzeRules stored procedure
				file.SetIniString("Database", "GetParameterValueProcedureName", g_pnucleus->m_settings.m_pszGetParameterValueProc);  // the spGetParameterValue stored procedure

		file.SetIniInt("Database", "ModificationCheckInterval", nucleus.m_settings.m_ulModsIntervalMS);  // in milliseconds
//		file.SetIniInt("FileHandling", "TransferFailureRetryTime", nucleus.m_settings.m_nTransferFailureRetryTime);  // in seconds
//		file.SetIniInt("FileHandling", "AutoDeleteOldest", nucleus.m_settings.m_bAutoDeleteOldest?1:0);
//		file.SetIniInt("FileHandling", "DeletionThreshold", nucleus.m_settings.m_ulDeletionThreshold );
			file.SetIniInt("FileHandling", "TriggerAdvanceMS", nucleus.m_settings.m_nTriggerAdvanceMS);// number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)

			file.SetIniString("Demo", "DefaultProject", nucleus.m_settings.m_pszDefaultProject);
			file.SetIniString("Demo", "DefaultScene", nucleus.m_settings.m_pszDefaultScene);
			file.SetIniString("Demo", "DefaultHost", nucleus.m_settings.m_pszDefaultHost);

		file.SetIniString("FileHandling", "SystemFolder", nucleus.m_settings.m_pszSystemFolderPath);  // must have trailing slash
//		file.SetIniString("Imagestore", "NullOXTFile", nucleus.m_settings.m_pszImagestoreNullFile );  // name of the null file, must be in system folder
//		file.SetIniString("Imagestore", "IntuitionSearchExt", nucleus.m_settings.m_pszIntuitionSearchExt); // search extensions for Intuition
//		file.SetIniString("Imagestore", "Imagestore2SearchExt", nucleus.m_settings.m_pszImagestore2SearchExt);			// search extensions for IS2
//		file.SetIniString("Imagestore", "Imagestore300SearchExt", nucleus.m_settings.m_pszImagestore300SearchExt);		// search extensions for IS300
//		file.SetIniString("Imagestore", "ImagestoreHDSearchExt", nucleus.m_settings.m_pszImagestoreHDSearchExt);		// search extensions for ISHD

		// gfx module
		file.SetIniString("Graphics", "Host", nucleus.m_settings.m_pszGfxHost);  // must have trailing slash; 
		file.SetIniInt("Graphics", "Port", nucleus.m_settings.m_nGfxPort); 

		file.SetSettings(NUCLEUS_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

	}

*/
	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Nucleus is exiting");
	nucleus.m_data.m_ulFlags &= ~CX_ICON_MASK;
	nucleus.m_data.m_ulFlags |= CX_STATUS_UNINIT;
	nucleus.m_data.SetStatusText(errorstring, nucleus.m_data.m_ulFlags);

	//exiting
	nucleus.m_msgr.DM(MSG_ICONINFO, NULL, "Nucleus", "-------------- Nucleus %s exit ---------------\n\
--------------------------------------------------\n", NUCLEUS_CURRENT_VERSION);  //(Dispatch message)
///	m_pDlg->SetProgress(NUCLEUSDLG_CLEAR); // no point

	_ftime( &nucleus.m_data.m_timebTick );
	nucleus.m_data.m_ulFlags &= ~NUCLEUS_STATUS_THREAD_MASK;
	nucleus.m_data.m_ulFlags |= NUCLEUS_STATUS_THREAD_ENDED;

	g_bKillStatus = true;

	Sleep(250); // let the message get there.
	nucleus.m_msgr.RemoveDestination("log");
	nucleus.m_msgr.RemoveDestination("email");
	nucleus.m_msgr.m_bKillThread = true; // do this early to give it some time to finish out.

	g_pnucleus = NULL;
	db.RemoveConnection(pdbConn);

	
	int nClock = clock() + 300; // small delay at end
	while(clock()<nClock)	{_ftime( &nucleus.m_data.m_timebTick );}
	g_bThreadStarted = false;
	g_pnucleus = NULL;
	nClock = clock() + nucleus.m_settings.m_nThreadDwellMS; // another small delay at end
	while(clock()<nClock)	{_ftime( &nucleus.m_data.m_timebTick );}
	Sleep(300); //one more small delay at end
	_endthread();
}

/*
void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((g_pmsgr)&&(g_pmsgr->m_lpfnDM))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		CMessagingObject msg;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = msg.EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = msg.EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = msg.EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch;
		pch = (char*) malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
* /
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if(pszEncodedMsg) 
			{
				ulBufferLen = strlen(pszEncodedMsg);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedCaller)
			{
				ulBufferLen = strlen(pszEncodedCaller);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedDest)
			{
				ulBufferLen = strlen(pszEncodedDest);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			g_pmsgr->m_lpfnDM(pch, ulFlags);
			free(pch);
		}
	}
}
*/


/*
// this thread is based on void HTTP10HandlerThread(void* pvArgs) from
// HTTP10.cpp.  It makes a special webserver with security, cgi parsing, and dynamic results
void NucleusHTTPThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return;}

	CNucleusMain* pNucleus = (CNucleusMain*)(pClient->m_lpObject) ;  // pointer to the global object.

// HTTP service by def is a non-persistent service, so even if the Client type is
// set to persist, we ignore that nucleusive in this handler thread

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;


		CHTTP10* phttp = (CHTTP10*)(&(pNucleus->m_http)) ;  // pointer to the global object.
		if(phttp == NULL)// cant do anything!
		{
			shutdown(pClient->m_socket, SD_BOTH);
			closesocket(pClient->m_socket);
			(*(pClient->m_pulConnections))--;
			_endthread(); return;
		}

		CHTTP10 http(phttp->m_pszRoot);  // local object for utility functions ONLY.
		// this allows the local to clone the root setting of the global, 
		// in order to do correct parsing in its own thread

		// the following are just pointers to the global obj.  
		// must NULL these out before object gets destroyed, 
		// so that global resources not freed!   !!!!!!!  important !!!!!
		http.m_pszHost = phttp->m_pszHost;
		http.m_ppszErrors = phttp->m_ppszMime;
		http.m_ppszMime = phttp->m_ppszMime;   
		http.m_ulStatus = phttp->m_ulStatus;   // important for renucleus and error
		
		unsigned char* pch;// for use with GetLine().
		unsigned long ulBufLen;// for use with GetLine().
		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		unsigned short usCode = 0;

		// get the raw buffer and process it yourself
		nReturn = http.m_net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_EOLN|EOLN_HTTP, pszStatus);
		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). 
			//was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
		{
			//error.
			if(pClient->m_lpMsgObj)
			{
				_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error receiving data.  %s", pszStatus);
				((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
			}

			usCode = 400;  //  we TRY to send this....
			if(pch!=NULL)
			{
				free(pch);  // must free the incoming data buffer 
				pch = NULL;
			}
			// create a return message buffer.
			pch = (unsigned char*)http.ErrorBuffer(NULL, usCode);   // 400 bad request
			ulBufLen = strlen((char*)pch); // valid from an error page.

			// send malformed request answer if possible  - have to tell the client that the request was not received in good order.
			nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
			if(nReturn<NET_SUCCESS)
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
				}
			}
		}
		else  // successful reception of data.
		{
			// process the data here...
			// in this sample the buffer is loaded into
			// pch, with ulBufLen as length

/*
			FILE* fp = fopen("rec.txt", "ab");
			if (fp)
			{
				fwrite(pch, 1, ulBufLen, fp );
				fflush(fp);
				fclose(fp);
			}
* /
			// parse buffer, format reply.
			CHTTPHeader header;

			if (http.ParseHeader(pch, ulBufLen, &header) < HTTP_SUCCESS)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("header not parsed\r\n\r\n", 1, strlen("header not parsed\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}
* /
				
				if(pch!=NULL) 
				{
					free(pch);  // must free the incoming data buffer 
					pch = NULL;
				}
				// create a return message buffer.
				usCode = 400;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}
			else
			{
				// here we have enough info to check security if desired, and if not, return a 401;
				// else continue....
/*
					FILE* fp = fopen("rec.txt", "ab");
					if (fp)
					{// lets write out the whole header so we can examine its full contents. including args.
						char bufferin[10000];
						sprintf(bufferin,
"header successfully parsed\r\n\
method: %d\r\n\
version: %s\r\n\
URL: %s\r\n\
Path: %s\r\n\
mimetype: %s\r\n\
user: %s\r\n\
password: %s\r\n\
num args: %ld\r\n",
	header.m_ucMethod,  // enumerated values
	header.m_pszVersion==NULL?"(null)":header.m_pszVersion,
	header.m_pszURL==NULL?"(null)":header.m_pszURL,
	header.m_pszPath==NULL?"(null)":header.m_pszPath,
	header.m_pszMimeType==NULL?"(null)":header.m_pszMimeType,
	header.m_pszUser==NULL?"(null)":header.m_pszUser,
	header.m_pszPassword==NULL?"(null)":header.m_pszPassword,
	header.m_ulNumArgs

);
		
	unsigned long k=0;
	while(k<header.m_ulNumArgs)
	{
		if((header.m_ppszArgNames)&&(header.m_ppszArgValues))
		{
			strcat(bufferin, "  ");
			strcat(bufferin, header.m_ppszArgNames[k]==NULL?"(null)":header.m_ppszArgNames[k]);
			strcat(bufferin, "=");
			strcat(bufferin, header.m_ppszArgValues[k]==NULL?"(null)":header.m_ppszArgValues[k]);
			strcat(bufferin, "\r\n");
		}
	k++;
	}

	strcat(bufferin, "headers:\r\n");
	strcat(bufferin, header.m_pszHeaders);
	strcat(bufferin, "\r\n");


			//		fwrite("header successfully parsed\r\n", 1, strlen("header successfully parsed\r\n"), fp );
						fwrite(bufferin, 1, strlen(bufferin), fp );
						fflush(fp);
						fclose(fp);
					}

* /


				if((phttp->m_pSecure)&&((header.m_pszUser==NULL)||(header.m_pszPassword==NULL)))
				{
					usCode = 401;
					pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
					ulBufLen = strlen((char*)pch); // valid from an error page.
					// buffer successfully parsed.
				}
				else 
				{
					if(pch!=NULL)
					{
						free(pch);  // must free the incoming data buffer 
						pch = NULL;
					}

					// create a return message buffer.
					switch(header.m_ucMethod)
					{
					case HTTP_GET://			1  // HTTP/1.0
					case HTTP_HEAD://			2  // HTTP/1.0
						{
							if(
									(strnicmp(header.m_pszPath, http.m_pszRoot, strlen(http.m_pszRoot)) == 0 )  // else not a part of the served file tree
								)
							{
								if(phttp->m_pSecure)
								{
									// have to check the main object for permission.
									// we are going to go by URL (asset level)
									// Loci are going to just be convenient names for the admins to set up.

									if((phttp->m_pSecure->CheckSecure(header.m_pszUser, header.m_pszPassword, NULL, header.m_pszURL))!=SECURE_SUCCESS)
									{
										usCode = 401;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
										ulBufLen = strlen((char*)pch); // valid from an error page.
										break; // get out of the switch from here.
									}
									// else it flows thru....
								}

								DWORD dwAttrib;

								dwAttrib = GetFileAttributes(header.m_pszPath);

								// check for nucleusory (renucleus nec.)
								if ( (dwAttrib!=0xffffffff)&&(dwAttrib&FILE_ATTRIBUTE_NUCLEUSORY) )  // if error, it may be because no file exists, so check in the next part.
								{
									usCode = 301;  // not necessary but why not.
									pch = (unsigned char*)http.RenucleusBuffer(&header);
									ulBufLen = strlen((char*)pch); // valid from an renucleus page.
								}
								else
								{
									// before we try to open the file, lets spend the time now (not while the file is open)
									// to figure out how long the static text respone buffer will be.
									CBufferUtil bu;
									// notice the sample "Server: NucleusServer/3.0.3.1" header included.  Pick the appname and ver, probably best
									unsigned long ulBufferLen = strlen("HTTP/1.0 200 OK\r\nServer: NucleusServer/\r\nContent-Length: \r\n\r\n");
									ulBufferLen += strlen(NUCLEUS_CURRENT_VERSION);

									if((header.m_pszMimeType)&&(strlen(header.m_pszMimeType)>0))
									{
										ulBufferLen += (strlen("Content-Type: \r\n") + strlen(header.m_pszMimeType)); // only add if known!
									}
									else // empty!
									{
										// dont free the "permanent resource", just null the pointer
										header.m_pszMimeType=NULL;
									}

									FILE* pFile = NULL;

									pFile = fopen(header.m_pszPath, "rb");
									if (pFile == NULL)  // file not openable, or not found.
									{
										usCode = 404;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 404 not found
										ulBufLen = strlen((char*)pch); // valid from an error page.
									}
									else
									{
										// this part just assembles the file into a buffer.
										// this may be modified to do dynamic html things by using the 
										// pClient->m_lpObject that refers back to some parser....

										usCode = 200; // OK  (!)
										// determine file size
										fseek(pFile, 0, SEEK_END);
										unsigned long ulFileLen = ftell(pFile);


										// here we have to parse out any nucleusives.  if this is a template file,
										// we may have a variable length.
									//	x


										if(header.m_ucMethod == HTTP_GET)
										{
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += (strlen(length)+ulFileLen);

											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you truncations at zeroed bytes

											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: NucleusServer/%s", NUCLEUS_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
											
											fclose(pFile);
										}
										else
										if(header.m_ucMethod == HTTP_HEAD)
										{
											fclose(pFile);  // we have all we need already.
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += strlen(length);
											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											//  create the header
											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: NucleusServer/%s", NUCLEUS_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												// its identical except for the body.  next line must stay commented out, its just there for ref.
												//fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
										}
									}
								}
							}
							else  // not part of the server service.
							{
								usCode = 403;
								pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 403 forbidden ...
								ulBufLen = strlen((char*)pch); // valid from an error page.
							}

						} break;
					case HTTP_POST://			3  // HTTP/1.0
						{
							usCode = 501;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 501 not implemented ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_PUT://			4  // HTTP/1.1
					case HTTP_DELETE://		5  // HTTP/1.1
					case HTTP_OPTIONS://  6  // HTTP/1.1
					case HTTP_TRACE://	  7  // HTTP/1.1
						{
							usCode = 505;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 505 http version not supported ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_UNDEF://		0  // internal
					default:
						{
							usCode = 400;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					}
				}
			}

			// send a reply, in pch., with strlen pch as length

			if(pch == NULL)  // had a problem forming the request, try to send a last ditch error code.
			{
				usCode = 500;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 500 internal server error
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}

			if(pch != NULL)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("sending:\r\n", 1, strlen("sending:\r\n"), fp );
					fwrite(pch, 1, ulBufLen, fp );
					fwrite("\r\n\r\n", 1, strlen("\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}

* /
				nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
				if(nReturn<NET_SUCCESS)
				{
					//error.
					if(pClient->m_lpMsgObj)
					{
						_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
						((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
					}
				}
			}
		}

//		char foo[34]; sprintf(foo, "code: %d", usCode);	AfxMessageBox(foo);

		http.m_pszHost = NULL;		// but do NOT free resource
		http.m_ppszErrors = NULL; // but do NOT free resource
		http.m_ppszMime = NULL;   // but do NOT free resource

		if(pch!=NULL) free(pch);  // must free the data buffer 

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}
*/


void NucleusCommandHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure in Protocol 1  //SECURE with PROTOCOL2 later
			strcpy(pszInfo, "");  // clear it
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error %d receiving data.  %s", nReturn, pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					strcpy(pszInfo, "");  // clear it
					nReturn = net.SendData(&data, pClient->m_socket, 2000, 0, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error %d sending NAK reply.  %s", nReturn, pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
						// we had an error receiving data and then anotehr trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{
//AfxMessageBox("receiving");

					// parse the command:

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					switch(data.m_ucCmd)
					{
					case CX_CMD_GETINFO: // not actually supported yet.  everything thru csf files for now.
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case NUCLEUS_CMD_EXSET://				0xea // increments exchange counter
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)
							{	
								data.m_ucSubCmd = (unsigned char) g_pnucleus->m_data.IncrementDatabaseMods((char*)data.m_pucData);
								free(data.m_pucData);  //destroy the buffer;
							}
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.

							data.m_ucType = NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case NUCLEUS_CMD_EXGET://				0xeb // gets exchange mod value
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case NUCLEUS_CMD_MODSET://			0xec // sets exchange mod value, skips exchange table
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case CX_CMD_GETSTATUS: //	0x99 // gets status info
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							unsigned long ulStatus;
							// assemble if there is 

							char* pch = g_pnucleus->m_data.GetStatusText(&ulStatus);  // allocates mem, must free after use
							if(pch)
							{
								data.m_ucSubCmd = (unsigned char)(NUCLEUS_ICON_MASK&ulStatus);
								data.m_pucData=(unsigned char*)pch;
								data.m_ulDataLen = strlen(pch);
							}
							else
							{
								data.m_ucCmd = NET_CMD_NAK;
								data.m_ucSubCmd = NET_CMD_NAK;
							}

							data.m_ucType = NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case CX_CMD_BYE:
						{
	g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "Nucleus", "The Global Kill command has been received.");  //(Dispatch message)

							bSetGlobalKill = true;
							g_pnucleus->m_data.m_bQuietKill = true;
							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					default:
						{
							//just ack
							data.m_ucCmd = NET_CMD_ACK;

		//					char repbuf[64];
							if(data.m_pucData!=NULL)
							{
		/*
								_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

								for(unsigned long q=0; q<data.m_ulDataLen; q++)
								{
									if(strlen(repbuf)<63)
										strncat(repbuf, (char*)(data.m_pucData+q), 1);
								}
								if(strlen(repbuf)<63)
									strcat(repbuf, "]");
								data.m_ulDataLen = strlen(repbuf);
								repbuf[data.m_ulDataLen] = 0;  // just in case
		*/
								free(data.m_pucData);  //destroy the buffer;
							}
		/*
							else
							{
								_snprintf(repbuf, 63, "data was NULL");
								data.m_ulDataLen = strlen(repbuf); 
							}

							data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
							if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
							else 
							{
								data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
								data.m_ulDataLen = 0;
							}
		*/
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					}

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Nucleus:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Nucleus:CommandHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.
	if (bSetGlobalKill)	//g_bKillThread = true;
	((CNucleusHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}

void NucleusStatusHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Nucleus:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Nucleus:CommandHandlerThread");
						}
						// we had an error receiving data and then anotehr trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Nucleus:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Nucleus:CommandHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}


int CNucleusMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdbMessage)&&(m_data.m_pdbMessageConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)&&(strlen(m_settings.m_pszMessages)))
	{
//		char dberrorstring[DB_ERRORSTRING_LEN];
		char szMessage[DB_SQLSTRING_MAXLEN];
		char* szSQL = (char*)malloc(DB_SQLSTRING_MAXLEN);

		if(szSQL==NULL) return NUCLEUS_ERROR;

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_data.m_pdbMessage->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdbMessage->EncodeQuotes(pszSender);

		if(m_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime) \
VALUES ('%s', '%s', %d, %d.%03d)", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				(m_settings.m_pszMessages?m_settings.m_pszMessages:"Messages"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				(m_settings.m_pszMessages?m_settings.m_pszMessages:"Messages")
				);
		}
		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);


		if(_beginthread(NucleusMsgSQLThread, 0, (void*)szSQL)==-1)
		{
			if(szSQL) free(szSQL);
			return NUCLEUS_ERROR;
		}
		else
		{
			return NUCLEUS_SUCCESS;
		}

/*	


//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_data.m_critMessageSQL);
		if(m_data.m_pdbMessage->ExecuteSQL(m_data.m_pdbMessageConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_data.m_critMessageSQL);
			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return NUCLEUS_SUCCESS;
		}
		LeaveCriticalSection(&m_data.m_critMessageSQL);
*/

	}
	return NUCLEUS_ERROR;
}
	

int CNucleusMain::SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...)
//int CNucleusMain::SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdbMessage)&&(m_data.m_pdbMessageConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszAsRun)&&(strlen(m_settings.m_pszAsRun)))
	{
//		char dberrorstring[DB_ERRORSTRING_LEN];
//		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];
		char* szSQL = (char*)malloc(DB_SQLSTRING_MAXLEN);

		if(szSQL==NULL) return NUCLEUS_ERROR;

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		if((pszEventID)&&(strlen(pszEventID)>SENDMSG_EVENT_MAXLEN)) pszEventID[SENDMSG_EVENT_MAXLEN-1]=0;
		
		char* pchMessage = m_data.m_pdbMessage->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdbMessage->EncodeQuotes(pszSender);
		char* pchEvent = m_data.m_pdbMessage->EncodeQuotes(pszEventID);
		if(m_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, channelid, eventid) \
VALUES ('%s', '%s', %d, %d.%03d, %d, '%s')", // HARDCODE
				(m_settings.m_pszAsRun?m_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm,
				nChannelID,
				pchEvent?pchEvent:""
				);
		}
		else
		{

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				(m_settings.m_pszAsRun?m_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				(m_settings.m_pszAsRun?m_settings.m_pszAsRun:"AsRun_Log")
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
		if(pchEvent) free(pchEvent);

		if(_beginthread(NucleusMsgSQLThread, 0, (void*)szSQL)==-1)
		{           
			if(szSQL) free(szSQL);
			return NUCLEUS_ERROR;
		}
		else
		{
			return NUCLEUS_SUCCESS;
		}

/*
//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_data.m_critMessageSQL);
		if(m_data.m_pdbMessage->ExecuteSQL(m_data.m_pdbMessageConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_data.m_critMessageSQL);
			m_data.IncrementDatabaseMods(m_settings.m_pszAsRun, dberrorstring);
			return NUCLEUS_SUCCESS;
		}
		LeaveCriticalSection(&m_data.m_critMessageSQL);
*/
	}
	return NUCLEUS_ERROR;
}

/*
int CNucleusMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)&&(strlen(m_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		unsigned long ulID = 0;
		// get the highest ID#;
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT MAX(id) AS MAX FROM %s", 
			m_settings.m_pszMessages			);

		CRecordset* prs = m_data.m_pdb->Retrieve(m_data.m_pdbConn, szSQL, dberrorstring);
		if(prs != NULL) 
		{
			if(!prs->IsEOF())
			{
				CString szValue;
				prs->GetFieldValue("MAX", szValue);
				unsigned long ulLastID = atol(szValue);
				ulID = ulLastID+1;
			}
			prs->Close();
			delete prs;
		}

		if(ulID>0)
		{
			va_list marker;
			// create the formatted output string
			va_start(marker, pszMessage); // Initialize variable arguments.
			_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
			va_end( marker );             // Reset variable arguments.
			_timeb timestamp;
			_ftime( &timestamp );

			char* pchMessage = m_data.m_pdb->EncodeQuotes(m_settings.m_pszMessages);
			char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) VALUES ('%s', '%s', %d, %d, %d)", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				ulID
				);

			if(pchMessage) free(pchMessage);
			if(pchSender) free(pchSender);

			if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
			{
				m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
				return NUCLEUS_SUCCESS;
			}
		}
	}
	return NUCLEUS_ERROR;
}

/*
void NucleusConnectionThread(void* pvArgs)
{
			g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "NucleusConnectionThread");  Sleep(250); //(Dispatch message)
	CNucleusConnectionObject* pConn = (CNucleusConnectionObject*) pvArgs;
	if(pConn==NULL) 
	{
		//**MSG
		return;
	}
	pConn->m_bConnThreadStarted = true;
	unsigned long ulTick = GetTickCount();
	unsigned long ulElapsedTick = GetTickCount();
			g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "NucleusConnectionThread2");  Sleep(250); //(Dispatch message)
	while(!pConn->m_bKillConnThread)
	{

		if(	pConn->m_pAPIConn )
		{
			g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "getting status");  Sleep(250); //(Dispatch message)
			// we can monitor the connection and get status
			EnterCriticalSection(&g_adc.m_crit);
			g_adc.GetStatusData(pConn->m_pAPIConn);
			LeaveCriticalSection(&g_adc.m_crit);
			ulTick = GetTickCount();

			if(pConn->m_pAPIConn->m_Status > 0)  // connected
			{
				// here we need to monitor list states and threads
				int nChannels = 0;

				while(nChannels<g_pnucleus->m_data.m_nNumChannelObjects)
				{
					if((g_pnucleus->m_data.m_ppChannelObj)&&(g_pnucleus->m_data.m_ppChannelObj[nChannels]))
					{
						if(strcmp(g_pnucleus->m_data.m_ppChannelObj[nChannels]->m_pszServerName, pConn->m_pszServerName)==0) // we are on the right adc conn.
						{
							//if the list is active, check to see if there is a thread going.
							if(g_pnucleus->m_data.m_ppChannelObj[nChannels]->m_ulFlags&NUCLEUS_FLAG_ENABLED)
							{
								if(
									  (g_pnucleus->m_data.m_ppChannelObj[nChannels]->m_nHarrisListID<0)
									||(g_pnucleus->m_data.m_ppChannelObj[nChannels]->m_nHarrisListID>=MAXSYSLISTS)
									)
								{
									// error!
									g_pnucleus->m_data.m_ppChannelObj[nChannels]->m_ulFlags &= ~NUCLEUS_FLAG_ENABLED;  //disable

								}
								else
								if( !g_pnucleus->m_data.m_ppChannelObj[nChannels]->m_bChannelThreadStarted)
								{ // start the thread!
									g_pnucleus->m_data.m_ppChannelObj[nChannels]->m_bKillChannelThread=false;
									g_pnucleus->m_data.m_ppChannelObj[nChannels]->m_pAPIConn=pConn->m_pAPIConn;
									g_pnucleus->m_data.m_ppChannelObj[nChannels]->m_pbKillConnThread=&(pConn->m_bKillConnThread);

			g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "beginning list thread");  Sleep(250); //(Dispatch message)
									if(_beginthread(NucleusListThread, 0, (void*)g_pnucleus->m_data.m_ppChannelObj[nChannels])==-1)
									{
										//error.
							
			g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "problem beginning list thread");  Sleep(250); //(Dispatch message)
										//**MSG
									}
								}
							}
							else  // disabled
							{
								if( g_pnucleus->m_data.m_ppChannelObj[nChannels]->m_bChannelThreadStarted)
								{
									// end the thread.
			g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ending list thread");  Sleep(250); //(Dispatch message)
									g_pnucleus->m_data.m_ppChannelObj[nChannels]->m_bKillChannelThread=true;
								}
							}
						}
					}
					nChannels++;
				}
			}
			else
			{
				g_adc.DisconnectServer(pConn->m_pAPIConn);  // just removes from adc list.
				pConn->m_ulStatus = NUCLEUS_STATUS_ERROR;  // due to disconnection.  resets on connect
				pConn->m_pAPIConn = NULL; // break out.
			}

			if(pConn->m_pAPIConn)
			{
				ulElapsedTick = GetTickCount();
				if(ulElapsedTick<ulTick) //wrapped
				{
					Sleep(((pConn->m_pAPIConn->m_SysData.systemfrx) == 0x24)?40:33);
				}
				else
				{
					ulElapsedTick -= ulTick;
					Sleep(((pConn->m_pAPIConn->m_SysData.systemfrx) == 0x24)?40:33 - ulElapsedTick);
				}
			}
			//else break out and try to reconnect.
		}
		else
		{
			if(pConn->m_ulFlags&NUCLEUS_FLAG_ENABLED) // active.
			{
				if((pConn->m_pszClientName)&&(strlen(pConn->m_pszClientName)<=0)) 
				{
					free(pConn->m_pszClientName);
					pConn->m_pszClientName = NULL;
				}
				if(!pConn->m_pszClientName)
				{
					pConn->m_pszClientName = (char*)malloc(strlen("Nucleus")+1);
					if(pConn->m_pszClientName) strcpy(pConn->m_pszClientName, "Nucleus");
				}
					
				CAConnection* pAPIConn = g_adc.ConnectServer(pConn->m_pszServerName, pConn->m_pszClientName);
				if(pAPIConn)
				{
					pConn->m_pAPIConn = pAPIConn; // connected.
					pConn->m_ulStatus = NUCLEUS_STATUS_CONN;
				}
			} // else don't connect, just 
			else
			{
				pConn->m_ulStatus = NUCLEUS_STATUS_NOTCON;
			}

			Sleep(2000);// wait two seconds after a connection attempt;
		}
			
	}

	g_adc.DisconnectServer(pConn->m_pszServerName);

	pConn->m_bConnThreadStarted = false;
	_endthread();
}

void NucleusListThread(void* pvArgs)
{
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "NucleusChannelThread");  Sleep(250); //(Dispatch message)
	CNucleusChannelObject* pChannel = (CNucleusChannelObject*) pvArgs;
	if(pChannel==NULL)
	{
		//**MSG
		return;
	}
	bool* pbKillConnThread = pChannel->m_pbKillConnThread;
	if(pbKillConnThread==NULL)  // the address, not what it points to
	{
		//**MSG
		return;
	}
	if(	pChannel->m_pAPIConn ) // need this to do anything.  // could get it by looping through g_adc.... later.
	{
		//**MSG
		return;
	}
	pChannel->m_bChannelThreadStarted = true;


	// lets set up some convenience vars;
	int nZeroList = pChannel->m_nHarrisListID-1;


	if((nZeroList<0)||(nZeroList>=MAXSYSLISTS)) // error on list number.
	{
		//**MSG
		return;
	}

	tlistdata* plistdata = &(pChannel->m_pAPIConn->m_ListData[nZeroList]);
	TConnectionStatus* pstatus =  &(pChannel->m_pAPIConn->m_Status);
	CAList** ppList = &(pChannel->m_pAPIConn->m_pList[nZeroList]);
	CAList*  pList = pChannel->m_pAPIConn->m_pList[nZeroList];  // volatile, always reassign
	CDBUtil* pdb = g_pnucleus->m_data.m_pdb;
	CDBconn* pdbConn = g_pnucleus->m_data.m_pdbConn;

	pChannel->m_pAPIConn->m_bListActive[nZeroList] = true;
//	unsigned long ulTick = GetTickCount();
//	unsigned long ulElapsedTick = GetTickCount();
			g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "NucleusChannelThread2");  Sleep(250); //(Dispatch message)

	tlistdata listdata;
	tlistdata lastlistdata;  // for comparison

  lastlistdata.listchanged=0;				// incremented whenever list size changes
  lastlistdata.listdisplay=0;				// incremented whenever an event changes
  lastlistdata.listsyschange=0;			// incremented when list related values are changed. 
  lastlistdata.listcount=0;					// number of events in list 
  lastlistdata.liststate=0;
			
	while((!pChannel->m_bKillChannelThread)&&((*pbKillConnThread)!=true))
	{
//		EnterCriticalSection(&g_adc.m_crit);
//		g_adc.GetStatusData(pConn->m_pAPIConn);
//		LeaveCriticalSection(&g_adc.m_crit);
		if(*pstatus > 0)  // have to be connected.
		{
			// snapshot the live list data
			EnterCriticalSection(&g_adc.m_crit);
			memcpy(&listdata, plistdata, sizeof(tlistdata));
			LeaveCriticalSection(&g_adc.m_crit);

			if((listdata.listchanged!=lastlistdata.listchanged)||(listdata.listdisplay!=lastlistdata.listdisplay)) // just do the event chage one as well for now.
			{
				// the list has changed, need to get all the events again  (will take care of any event changes as well)
				int nNumEvents = g_adc.GetEvents(pChannel->m_pAPIConn,
					pChannel->m_nHarrisListID, 
					0, //start at the top.
					g_pnucleus->m_settings.m_bUseListCount?listdata.listcount:listdata.lookahead);

				EnterCriticalSection(&(pList->m_crit));  // lock the list out
				pList = (*ppList);  // volatile, must reassign
				if((nNumEvents>=ADC_SUCCESS)&&(pList))
				{
					// deal with database.
					if((pdb!=NULL)&&(pdbConn!=NULL)&&(g_pnucleus->m_settings.m_pszLiveEvents)&&(strlen(g_pnucleus->m_settings.m_pszLiveEvents)>0))
					{
						char dberrorstring[DB_ERRORSTRING_LEN];
						char szSQL[DB_SQLSTRING_MAXLEN];

						double dblLastPrimaryEventTime = -1.0;
						unsigned short usLastPrimaryOnAirJulianDate = 0xffff;
						unsigned long ulLastPrimaryOnAirTimeMS = 0xffffffff;
						double dblServerTime = (double)pChannel->m_pAPIConn->m_usRefJulianDate - 25567.0   // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
										+ (double)pChannel->m_pAPIConn->m_ulRefTimeMS/1000.0;

						int n=0;
						while(n<nNumEvents)
						{

							if((pList->m_ppEvents)&&(pList->m_ppEvents[n])&&(pList->m_ppEvents[n]->m_pszID)&&(strlen(pList->m_ppEvents[n]->m_pszID)))
							{

								double dblEventTime = 0.0; 
								unsigned short usOnAirJulianDate = pList->m_ppEvents[n]->m_usOnAirJulianDate;

								if(usOnAirJulianDate == 0xffff)
								{
									_timeb timestamp;
									_ftime(&timestamp);

									usOnAirJulianDate = pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.

									dblEventTime = (double)usOnAirJulianDate - 25567.0   // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
										+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
								}
								else
								{
									dblEventTime = (double)pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567.0   // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
										+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
								}
									  

								unsigned short usType = pList->m_ppEvents[n]->m_usType;

								if(usType&SECONDARYEVENT)
								{
									if(ulLastPrimaryOnAirTimeMS>0.0)
									{
										dblEventTime = dblLastPrimaryEventTime 
											+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
										usOnAirJulianDate = usLastPrimaryOnAirJulianDate;
										if(pList->m_ppEvents[n]->m_ulOnAirTimeMS + ulLastPrimaryOnAirTimeMS >= 86400000 )
										{
											//wrapped days.
											usOnAirJulianDate++;
										}
									}
								}
								else
								{
									ulLastPrimaryOnAirTimeMS = pList->m_ppEvents[n]->m_ulOnAirTimeMS;
									dblLastPrimaryEventTime = dblEventTime;
									usLastPrimaryOnAirJulianDate = usOnAirJulianDate;
								}

//						select event with server, list, start time +/- tolerance, and clip ID
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s WHERE \
server_name = '%s' AND \
server_list = %d AND \
event_clip = '%s' AND \
event_start >= %f AND \
event_start <= %f",
										g_pnucleus->m_settings.m_pszLiveEvents,
										pChannel->m_pAPIConn->m_pszServerName,
										pChannel->m_nHarrisListID,
										pList->m_ppEvents[n]->m_pszID,
										dblEventTime-0.3, dblEventTime+0.3
									);

								// get
								CRecordset* prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
								int nNumFound = 0;
								int nID = -1;
								if(prs != NULL) 
								{
									if(!prs->IsEOF())
									{
										// get id!
										CString szValue;
										prs->GetFieldValue("(itemid", szValue);
										nID = atoi(szValue);
										nNumFound++;
										prs->MoveNext();
									}
									prs->Close();
									delete prs;
								}

								if((nNumFound>0)&&(nID>0))
								{
									// update it.
									// if found update it with new values, and a found flag (app_data_aux int will be the found flag 1=found)
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
server_name = '%s', \
server_list = %d, \
list_id = %d, \
event_id = '%s', \
event_clip = '%s', \
event_title = '%s', \
%s%s%s\
event_type = %d, \
event_status = %d, \
event_time_mode = %d, \
event_start = %f, \
event_duration = %d, \
event_last_update = %f, \
app_data_aux = 1 WHERE itemid = %d;",  // 1 is the found flag.
										g_pnucleus->m_settings.m_pszLiveEvents,  // table name
										pChannel->m_pAPIConn->m_pszServerName, //server_name
										pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
										pChannel->m_nChannelID, // list_id - internal lookup channel number
										pList->m_ppEvents[n]->m_pszReconcileKey?pList->m_ppEvents[n]->m_pszReconcileKey:"", //event_id
										pList->m_ppEvents[n]->m_pszID, //event_clip  (must not be null)
										pList->m_ppEvents[n]->m_pszTitle?pList->m_ppEvents[n]->m_pszTitle:"", //event_title
										pList->m_ppEvents[n]->m_pszData?"event_data = '":"", //event_data (exists)
										pList->m_ppEvents[n]->m_pszData?pList->m_ppEvents[n]->m_pszData:"", //event_data  
										pList->m_ppEvents[n]->m_pszData?"', ":"", //event_data (exists)
										pList->m_ppEvents[n]->m_usType, //event_type
										pList->m_ppEvents[n]->m_usStatus, //event_status
										pList->m_ppEvents[n]->m_usControl, //event_time_mode
										dblEventTime, //event_start
										pList->m_ppEvents[n]->m_ulDurationMS, //event_duration
										dblServerTime, //event_last_update
										nID // the unique ID of the thing
									);
// app_data = , // no app data for the above, let it be NULL or whatever it is.


								}
								else
								{
									// add it.
									// if not found, insert one with a found flag
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
server_name, \
server_list, \
list_id, \
event_id, \
event_clip, \
event_title, \
%s\
event_type, \
event_status, \
event_time_mode, \
event_start, \
event_duration, \
event_last_update, \
app_data_aux) VALUES ('%s', %d, %d, '%s', '%s', '%s', %s%s%s%d, %d, %d, %f, %d, %f, 1)",  // 1 is the found flag.
										g_pnucleus->m_settings.m_pszLiveEvents,  // table name
										pList->m_ppEvents[n]->m_pszData?"event_data, ":"", //event_data (exists)
										pChannel->m_pAPIConn->m_pszServerName, //server_name
										pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
										pChannel->m_nChannelID, // list_id - internal lookup channel number
										pList->m_ppEvents[n]->m_pszReconcileKey?pList->m_ppEvents[n]->m_pszReconcileKey:"", //event_id
										pList->m_ppEvents[n]->m_pszID, //event_clip  (must not be null)
										pList->m_ppEvents[n]->m_pszTitle?pList->m_ppEvents[n]->m_pszTitle:"", //event_title
										pList->m_ppEvents[n]->m_pszData?"'":"", //event_data (exists)
										pList->m_ppEvents[n]->m_pszData?pList->m_ppEvents[n]->m_pszData:"", //event_data  
										pList->m_ppEvents[n]->m_pszData?"', ":"", //event_data (exists)
										pList->m_ppEvents[n]->m_usType, //event_type
										pList->m_ppEvents[n]->m_usStatus, //event_status
										pList->m_ppEvents[n]->m_usControl, //event_time_mode
										dblEventTime, //event_start
										pList->m_ppEvents[n]->m_ulDurationMS, //event_duration
										dblServerTime //event_last_update
										);

								}

								if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
								{
									//**MSG
								}
							}
							n++;
						}

						///////////////////////
						// remove found flags.

//					remove all with no found flag.

						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d AND \
app_data_aux < 1",
										g_pnucleus->m_settings.m_pszLiveEvents,  // table name
										pChannel->m_pAPIConn->m_pszServerName, //server_name
										pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
									);
						if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
							//**MSG
						}
//					remove all found flags for next go-around.
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
app_data_aux = 0 WHERE \
server_name = '%s' AND \
server_list = %d",
										g_pnucleus->m_settings.m_pszLiveEvents,  // table name
										pChannel->m_pAPIConn->m_pszServerName, //server_name
										pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
									);

						if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
							//**MSG
						}

					
						// increment exchange mod counter. for events table.
						if (g_pnucleus->m_data.IncrementDatabaseMods(g_pnucleus->m_settings.m_pszLiveEvents, dberrorstring)<NUCLEUS_SUCCESS)
						{
							//**MSG
						}

					}
					// record last values if get was successful.
					memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
				}
				LeaveCriticalSection(&(pList->m_crit));
			}
/ * 			else
			if(listdata.listdisplay!=lastlistdata.listdisplay)
			{
				// an event has changed, need to find it and just deal with that one.
				// record last values
				memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
			}
* /
		}

	}
	pChannel->m_pAPIConn->m_bListActive[nZeroList] = false;
	pChannel->m_bChannelThreadStarted = false;
	_endthread();
}

*/

void NucleusTriggerQueryThread(void* pvArgs)
{

	CNucleusAutomationChannelObject* pChannelObj = (CNucleusAutomationChannelObject*) pvArgs;
	if(pChannelObj==NULL) return;
	CNucleusEndpointObject* pEndObj = (CNucleusEndpointObject*) pChannelObj->m_pEndpoint;
	if(pEndObj==NULL) return;
	CNucleusMain* pnucleus = (CNucleusMain*) pChannelObj->m_pNucleus;
	if(pnucleus==NULL) return;
	char debugsource[MAX_MESSAGE_LENGTH];
	char pluserrorstring[MAX_MESSAGE_LENGTH];
	char errorstring[MAX_MESSAGE_LENGTH];
	char szSQL[DB_SQLSTRING_MAXLEN];
	char szSQLplus[DB_SQLSTRING_MAXLEN];

	pChannelObj->m_bTriggerQueryThreadStarted = true;


	unsigned long ulAutoType  = 0;

	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
	{
		ulAutoType = NUCLEUS_DEP_AUTO_SENTINEL;
		sprintf(debugsource, "[%s:%d|%x>", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum, GetCurrentThreadId());
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Beginning trigger query thread for %s:%d", debugsource, pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	}
	else
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
	{
		ulAutoType = NUCLEUS_DEP_AUTO_HELIOS;
		sprintf(debugsource, "[%d|%x>", pChannelObj->m_nChannelID, GetCurrentThreadId());
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Beginning trigger query thread for Omnibus stream %d", debugsource, pChannelObj->m_nChannelID);
	}
	g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusTriggerQueryThread", errorstring);    //(Dispatch message)



	char lebuf[256];
	sprintf(lebuf, "Logs\\N%dQ.txt", pChannelObj->m_nChannelID);
	_timeb tmb;
	FILE* fpdbg = NULL; //in line file debug

	if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
	{
		fpdbg = fopen(lebuf, "ab");

		if(fpdbg)
		{
			_ftime(&tmb);
			fprintf(fpdbg, "[%d> %d.%03d START\r\n\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
			fclose(fpdbg);
			fpdbg = NULL;
		}

	}

	CDBUtil dbSet;
	CDBconn* pdbConnSet = NULL;
		
	while((pdbConnSet==NULL)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
	{
		pdbConnSet = dbSet.CreateNewConnection(pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW);
		if(pdbConnSet)
		{
			if(dbSet.ConnectDatabase(pdbConnSet, errorstring)<DB_SUCCESS)
			{
				sprintf(pluserrorstring, "%s %s", debugsource, errorstring);
				pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:trigger_query_database_connect", pluserrorstring);  //(Dispatch message)
				pdbConnSet = pnucleus->m_data.m_pdbConn;
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Failed to create connection for %s:%s:%s", debugsource, pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW); 
			pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:trigger_query_database_init", errorstring);  //(Dispatch message)
			pdbConnSet = pnucleus->m_data.m_pdbConn;

			//**MSG
		}
		Sleep(pnucleus->m_settings.m_nThreadSeparationDelayMS);
	}


	int nBufferedTriggers = 0;
	int nLastBufferedTriggers = 0;

	while((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
	{

				if(
						(
							(ulAutoType == NUCLEUS_DEP_AUTO_SENTINEL)
						&&(pChannelObj->m_nListState&((1<<LISTTHREADING)|(1<<LISTISPLAYING)))  // list must be threaded and playing to trigger
						&&(!(pChannelObj->m_nListState&((1<<LISTINFREEZE)|(1<<LISTINHOLD))))  // list must not be in freeze or hold to trigger
						)
					||(
							(ulAutoType == NUCLEUS_DEP_AUTO_HELIOS)
						&&(pChannelObj->m_nListState!=0)  // list must be connected
	//					&&(pChannelObj->m_nServerStatus!=0)  // server must be connected!  // comment this out to surivive adaptor disconnection retries....
						)
					)
				{

					EnterCriticalSection(&pChannelObj->m_critChVar);
					bool bFinished = !pChannelObj->m_bNewEventRetrieve;
					LeaveCriticalSection(&pChannelObj->m_critChVar);


					if((pChannelObj->m_bTriggerEventsChanged)&&(bFinished))
					{

		///// DEMOTODO get status col name in view
/*
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d * FROM %s WHERE server = '%s' AND listid = %d AND \
AnalyzedTriggerDataStatus & %d = 0 \
AND analyzed_trigger_id IS NOT NULL ORDER BY TriggerTime ASC",  pnucleus->m_settings.m_nTriggerBuffer,
					pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
					pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListID, NUCLEUS_FLAG_TRIGGERED
				);
*/

					// this exclusion of triggers already in the async buffer is new in 2.2.1.22
					szSQLplus[0] = 0; // clear it.

					if((pnucleus->m_settings.m_bExcludeAsyncTriggers)&&(pnucleus->m_settings.m_bUseAsynchTriggerRetrieve))
					{
//						if(pChannelObj->m_nTriggeredIDs > 0) // allow even if zero here/
						{
							if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:query_trigger_select", "%s entering trigger mark get for %d IDs (now=%.03f) top=%d", debugsource, pChannelObj->m_bGetTriggeredIDs, pChannelObj->m_dblAutomationTimeEstimate, ((pChannelObj->m_nTriggeredIDs>0)&&(pChannelObj->m_pnTriggeredIDs))?pChannelObj->m_pnTriggeredIDs[0]:-1); //(Dispatch message)
							// give the word to exchange
							EnterCriticalSection(&pChannelObj->m_critTriggeredIDs);
				if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:query_trigger_select", "%s entered trigger mark get for %d IDs (now=%.03f)", debugsource, pChannelObj->m_bGetTriggeredIDs, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)

							if(pChannelObj->m_nTriggeredIDs > 0)
							{
								int* pint = new int[pChannelObj->m_nTriggeredIDs];
								int nint = pChannelObj->m_nTriggeredIDs;
								if(pint)
								{
									memcpy(pint, pChannelObj->m_pnTriggeredIDs, pChannelObj->m_nTriggeredIDs*sizeof(int));
								}

								LeaveCriticalSection(&pChannelObj->m_critTriggeredIDs);
					if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:query_trigger_select", "%s %d trigger marks obtained (now=%.03f)", debugsource, nint, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)

								if(pint)
								{
									int dbix = 0;
									while(dbix<nint)
									{
										_snprintf(pluserrorstring, MAX_MESSAGE_LENGTH-1, " AND analyzed_trigger_id <> %d", pint[dbix]);
										strcat(szSQLplus, pluserrorstring);
										dbix++;
									}

									delete [] pint;
								}
							}
							else
							{
								LeaveCriticalSection(&pChannelObj->m_critTriggeredIDs);
							}
						}
					}

			EnterCriticalSection(&pChannelObj->m_critChVar);
			if(ulAutoType == NUCLEUS_DEP_AUTO_SENTINEL)
			{
				if(pnucleus->m_settings.m_bUseTriggerSourceView)
				{

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d LiveEventData.itemid, (select host from %s.dbo.Destinations where channelid = %d) as Host, TriggerSourceView.ParsedValue, TriggerSourceView.TriggerTime, \
TriggerSourceView.analyzed_trigger_id, TriggerSourceView.Source, TriggerSourceView.Scene, TriggerSourceView.Graphic_Action_Type, TriggerSourceView.Action_Name, \
LiveEventData.list_id AS channelid, LiveEventData.event_clip, LiveEventData.event_title, TriggerSourceView.event_name, '4110' AS dest_type, 'libretto' AS dest_module, \
TriggerSourceView.layer, LiveEventData.event_duration, LiveEventData.event_data, LiveEventData.event_id \
from (select itemid, list_id, event_clip, event_title, event_duration, event_data, event_id from sentinel.dbo.Events where list_id = %d and ((event_status & %d) = 0)) \
AS LiveEventData inner join %s.dbo.TriggerSourceView as TriggerSourceView \
ON LiveEventData.itemid = TriggerSourceView.automation_event_itemid \
WHERE TriggerTime > %.3f%s \
ORDER BY TriggerTime ASC",

						pnucleus->m_settings.m_nTriggerBuffer,
						"libretto", //libretto
						pChannelObj->m_nChannelID, pChannelObj->m_nChannelID,
						pnucleus->m_settings.m_nHarrisStatusTriggerExclude,
						g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus",
						pChannelObj->m_dblAutomationTimeEstimate - ((double)pnucleus->m_settings.m_nTriggerExclusionDelayMS)/1000.0,
						szSQLplus
					);

				}
				else
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d * FROM %s WHERE channelid = %d AND \
AnalyzedTriggerDataStatus & %d = 0 and AnalyzedTriggerDataFlags & %d <> 0 and ((event_status & %d) = 0)\
AND TriggerTime > %.3f%s \
AND analyzed_trigger_id IS NOT NULL ORDER BY TriggerTime ASC",  pnucleus->m_settings.m_nTriggerBuffer,
						pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
						pChannelObj->m_nChannelID, NUCLEUS_FLAG_TRIGGERED, NUCLEUS_FLAG_ANALYZED,
						pnucleus->m_settings.m_nHarrisStatusTriggerExclude,
						pChannelObj->m_dblAutomationTimeEstimate - ((double)pnucleus->m_settings.m_nTriggerExclusionDelayMS)/1000.0,
						szSQLplus
					);

				}
///			((event_status & 17) = 0) // not done or postrolled.  // we want ones that are playing or yet to be played

// SENTINEL_EVENT_POSTUPCOUNT = 0x00100000 = 1048576 decimal.
// now to suppress post upcount events, we call ((event_status & 1048593) = 0) = not done or postrolled or post upcounter
			}
			else
			if(ulAutoType == NUCLEUS_DEP_AUTO_HELIOS)
			{
				if(pnucleus->m_settings.m_bUseTriggerSourceView)
				{

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d LiveEventData.itemid, (select host from %s.dbo.Destinations where channelid = %d) as Host, TriggerSourceView.ParsedValue, TriggerSourceView.TriggerTime, \
TriggerSourceView.analyzed_trigger_id, TriggerSourceView.Source, TriggerSourceView.Scene, TriggerSourceView.Graphic_Action_Type, TriggerSourceView.Action_Name, \
LiveEventData.list_id AS channelid, LiveEventData.event_clip, LiveEventData.event_title, TriggerSourceView.event_name, '4110' AS dest_type, 'libretto' AS dest_module, \
TriggerSourceView.layer, LiveEventData.event_duration, LiveEventData.event_data, LiveEventData.event_id \
from (select itemid, list_id, event_clip, event_title, event_duration, event_data, event_id from helios.dbo.Events where list_id = %d and event_status < 10 and event_status > 4) \
AS LiveEventData inner join %s.dbo.TriggerSourceView as TriggerSourceView \
ON LiveEventData.itemid = TriggerSourceView.automation_event_itemid \
WHERE  TriggerTime > %.3f%s \
ORDER BY TriggerTime ASC",

					  pnucleus->m_settings.m_nTriggerBuffer,
					  "libretto", //libretto
					  pChannelObj->m_nChannelID, pChannelObj->m_nChannelID,
						g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus",
						pChannelObj->m_dblAutomationTimeEstimate - ((double)pnucleus->m_settings.m_nTriggerExclusionDelayMS)/1000.0,
						szSQLplus
					);

				}
				else
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d * FROM %s WHERE channelid = %d AND \
AnalyzedTriggerDataStatus & %d = 0  and AnalyzedTriggerDataFlags & %d <> 0 and event_status < 10 and event_status > 4 \
AND TriggerTime > %.3f%s \
AND analyzed_trigger_id IS NOT NULL ORDER BY TriggerTime ASC",  pnucleus->m_settings.m_nTriggerBuffer,
						pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
						pChannelObj->m_nChannelID, NUCLEUS_FLAG_TRIGGERED, NUCLEUS_FLAG_ANALYZED,
						pChannelObj->m_dblAutomationTimeEstimate - ((double)pnucleus->m_settings.m_nTriggerExclusionDelayMS)/1000.0,
						szSQLplus
					);
				}
///			event_status < 10 and event_status >4 // not done or postrolledand no error  // we want ones that are playing or yet to be played and without errors
			}
			LeaveCriticalSection(&pChannelObj->m_critChVar);

			
//	_ftime(&pChannelObj->m_timebTriggerTick); // the last time check inside the thread
//AND TriggerTime >= %lf \
//					pnucleus->m_data.m_dblAutomationTimeEstimate
//AND (event_status & 26  <> 0 ) // have to remove in order to get the backtimed ones
						
		//	&18<>0 = must be postrolled or playing
		//	&26<>0 = must be postrolled or playing - uses preroll because now we are running on calculated event trigger times
			// cant use pre-rol because time is not correct yet


			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg==NULL) fpdbg = fopen(lebuf, "ab");

				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "[%d> %d.%03d Q>\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}



if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s async_trigger SQL: %s", debugsource, szSQL); 

				EnterCriticalSection(&pChannelObj->m_critTriggerSQL);  // lock it to do the query, which may time out.

				CRecordset* prs = pChannelObj->m_db.Retrieve(pChannelObj->m_pdbConn, szSQL, errorstring);  // use the channel object's connection only


			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "[%d> %d.%03d R<\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}
				if(prs)
				{
//					EnterCriticalSection(&pChannelObj->m_critChVar);

					// let's do all the parsing here.
					pChannelObj->m_nNewEvents = 0;

					pChannelObj->m_bTriggerEventsChanged = false;
					pChannelObj->m_bTriggerEventsChangedLocal = true;// added 2.2.1.21 (17 July 2016)

					bool bInvalidTimes = false;
					bool bNewRetrieve = false;
					
					while(
								 (!bNewRetrieve)&&(!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)
							 )
					{
//		_ftime(&pChannelObj->m_timebTriggerTick); // the last time check inside the thread
						CString szTemp;
	//					int nAutomationItemID=-1;
						prs->GetFieldValue("itemid", szTemp);
						pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_nEventID = atoi(szTemp);
						prs->GetFieldValue("Host", pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szHost);
						pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szHost.TrimLeft();
						pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szHost.TrimRight();
						prs->GetFieldValue("ParsedValue", pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szValue);
						prs->GetFieldValue("TriggerTime", szTemp);

	//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Trigger time received: %s", szTemp); // Sleep(50); //(Dispatch message)
						pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_dblTriggerTime = atof(szTemp);// - 0.333;  // ten frames anticipation.

						if((int)pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_dblTriggerTime <= 0) { bInvalidTimes = true; break;}
						prs->GetFieldValue("analyzed_trigger_id", szTemp);
						if(szTemp.GetLength()) pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_nAnalyzedTriggerID = atoi(szTemp);

						prs->GetFieldValue("Source", pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szSource);
						prs->GetFieldValue("Scene", pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szScene);

						prs->GetFieldValue("Graphic_Action_Type", szTemp);
						pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_nType = atoi(szTemp);
						prs->GetFieldValue("Action_Name", pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szIdentifier);

	/////////////////////////////
						// added  added 2.2.1.20 (9 July 2016)
						prs->GetFieldValue("channelid", pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szChannelID);
						prs->GetFieldValue("event_clip", pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szAutomationEventID);
						prs->GetFieldValue("event_title", pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szAutomationEventTitle);
						prs->GetFieldValue("event_name", pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szEventName);
	//					prs->GetFieldValue("automation_event_itemid", pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szAutomationEventItemID);  // wrong ID, want event_id at end
	////////////////////////////
						
						prs->GetFieldValue("dest_type", szTemp);
						pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_nDestType = atoi(szTemp);

						prs->GetFieldValue("dest_module", pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szDestModule);
						prs->GetFieldValue("layer", pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szLayer);

	/////////////////////////////
						// added  added 2.2.1.20 (9 July 2016)
						if(pChannelObj->m_ulFlags&NUCLEUS_FLAG_EXTENDED_TAV)
						{
							prs->GetFieldValue("event_duration", pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szAutomationEventDur);
							prs->GetFieldValue("event_data", pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szAutomationEventData);
							prs->GetFieldValue("event_id", pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szAutomationEventItemID); 
						}

	/////////////////////////////

						
						// want to add promotor event name here
					//removed	 2.2.1.20 (9 July 2016) - see above pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents]->m_szEventName="";
						
						pChannelObj->m_nNewEvents++;

						prs->MoveNext();
			
						bNewRetrieve = pChannelObj->m_bTriggerEventsChanged;
					}
				
					if(prs)
					{
			//			prs->MoveNext();
						prs->Close();
						delete prs;
						prs = NULL;
//						pChannelObj->m_prs = NULL;
					}

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "[%d> %d.%03d S\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}

					LeaveCriticalSection(&pChannelObj->m_critTriggerSQL);  // unlock it.
					if((bInvalidTimes)||(bNewRetrieve))
					{
						pChannelObj->m_nNewEvents = 0; // just go around again.
					}
					else
					{

						//2.2.1.22 now must remove the IDs from the shared memory.  can only do this here, right after a retrieve, to make sure old events are not included
//						if(!bError)  // but only if no error.  otherwise have to hit it again later.  cant be error to get here.
						if(nBufferedTriggers>0) // 2.2.1.22 only do if there are any
						{

				if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:query_trigger", "%s entering trigger mark reduction of %d IDs (now=%.03f)", debugsource, pChannelObj->m_bGetTriggeredIDs, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)
						EnterCriticalSection(&pChannelObj->m_critTriggeredIDs);
				if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:query_trigger", "%s entered trigger mark reduction of %d IDs (now=%.03f)", debugsource, pChannelObj->m_bGetTriggeredIDs, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)

							if( nBufferedTriggers == pChannelObj->m_nTriggeredIDs ) // still good, just set it to 0
							{
								pChannelObj->m_nTriggeredIDs = 0;  // reset this!  2.2.1.22 - do this here.  must be done after the database is cleared. but, more things may be added in the meantime.					
							}
							else //pChannelObj->m_nTriggeredIDs must be greater, something was added to the end.
							{
								// reduce by the amount.
								int idx=0;
								while(idx<(pChannelObj->m_nTriggeredIDs-nBufferedTriggers))
								{
									pChannelObj->m_pnTriggeredIDs[idx] = pChannelObj->m_pnTriggeredIDs[idx+nBufferedTriggers];
									idx++;
								}
								pChannelObj->m_nTriggeredIDs -= nBufferedTriggers;
								if(pChannelObj->m_nTriggeredIDs<0) pChannelObj->m_nTriggeredIDs=0; // can't be less. just paranoia check here. 2.2.1.22
							}
						LeaveCriticalSection(&pChannelObj->m_critTriggeredIDs);
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:query_trigger", "%s %d trigger marks reduced (now=%.03f) resulting %d", debugsource, nBufferedTriggers, pChannelObj->m_dblAutomationTimeEstimate,pChannelObj->m_bGetTriggeredIDs); //(Dispatch message)
							nBufferedTriggers = 0;//reset
							nLastBufferedTriggers = 0;//reset
						}




						EnterCriticalSection(&pChannelObj->m_critChVar);
						pChannelObj->m_bNewEventRetrieve = true;
						LeaveCriticalSection(&pChannelObj->m_critChVar);
					}
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "[%d> %d.%03d S+\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}
				}
				else
				{
					LeaveCriticalSection(&pChannelObj->m_critTriggerSQL);  // unlock it.

	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
	{
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s Trigger Query ERROR for %s:%d:  %s",debugsource, pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum, errorstring); // Sleep(50);//(Dispatch message)
	}
	else
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
	{
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s Trigger Query ERROR for Omnibus stream %d:  %s",debugsource, pChannelObj->m_nChannelID, errorstring); // Sleep(50);//(Dispatch message)
	}
				}
			}


			// now check for triggers to be flagged as done.
			if((pChannelObj->m_nTriggeredIDs /*- nLastBufferedTriggers*/ > 0) && (pChannelObj->m_bGetTriggeredIDs))
			{
	if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:query_trigger", "%s entering trigger mark for %d IDs (now=%.03f) top=%d", debugsource, pChannelObj->m_bGetTriggeredIDs, pChannelObj->m_dblAutomationTimeEstimate, ((pChannelObj->m_nTriggeredIDs>0)&&(pChannelObj->m_pnTriggeredIDs))?pChannelObj->m_pnTriggeredIDs[0]:-1); //(Dispatch message)
				// give the word to exchange
				EnterCriticalSection(&pChannelObj->m_critTriggeredIDs);
	if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:query_trigger", "%s trigger mark swap for %d IDs (now=%.03f)", debugsource, pChannelObj->m_bGetTriggeredIDs, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)

				int nint = pChannelObj->m_nTriggeredIDs - nLastBufferedTriggers;

				if(nint<=0)
				{
					LeaveCriticalSection(&pChannelObj->m_critTriggeredIDs);
		if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:query_trigger", "%s %d trigger marks obtained (now=%.03f) all buffered complete", debugsource, nint, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)
				}
				else
				{
					int* pint = NULL;
					pint = new int[pChannelObj->m_nTriggeredIDs];
					if(pint)
					{
						memcpy(pint, pChannelObj->m_pnTriggeredIDs+(nLastBufferedTriggers)/* *sizeof(int))*/, (/*pChannelObj->m_nTriggeredIDs*/ nint)*sizeof(int));
	//					memcpy(pint, pChannelObj->m_pnTriggeredIDs+(nLastBufferedTriggers*sizeof(int)), (pChannelObj->m_nTriggeredIDs-nLastBufferedTriggers)*sizeof(int));
	//					pChannelObj->m_nTriggeredIDs = 0;  // reset this!  2.2.1.22 - don't do this here.  must be done after the database is cleared. but, more things may be added in the meantime.
					}

					pChannelObj->m_bGetTriggeredIDs = false; 
					LeaveCriticalSection(&pChannelObj->m_critTriggeredIDs);
		if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:query_trigger", "%s %d trigger marks obtained (now=%.03f)", debugsource, nint, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)

					// now run the query.
					if(pint)
					{
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d WHERE ",
								pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the AnalyzedTriggerData table name
								NUCLEUS_FLAG_TRIGGERED
							);

						int dbix = 0;
						while(dbix<nint)
						{
							if(dbix == 0)
							{
								_snprintf(pluserrorstring, MAX_MESSAGE_LENGTH-1, "analyzed_trigger_id = %d ", pint[dbix]);
							}
							else
							{
								_snprintf(pluserrorstring, MAX_MESSAGE_LENGTH-1, "OR analyzed_trigger_id = %d ",  pint[dbix]);
							}
							strcat(szSQL, pluserrorstring);
							dbix++;
						}

						bool bError = true;
		if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:query_trigger", "%s %d triggers: AnalyzedTriggerDataStatus SQL: %s", debugsource, nint, szSQL); // Sleep(50);//(Dispatch message)
						if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
						{
									//**MSG
		g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:query_trigger", "%s ERROR executing trigger SQL: %s", debugsource, errorstring);  //Sleep(50); //(Dispatch message)
						}
						else
						{
							bError = false;
							nBufferedTriggers += nint;
							nLastBufferedTriggers = nBufferedTriggers;
		if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:query_trigger", "%s executed AnalyzedTriggerDataStatus SQL: %s", debugsource, szSQL); // Sleep(50);//(Dispatch message)
						}

						delete [] pint;

					}
				}
				
			}






			Sleep(1);
		}	

	}


	dbSet.RemoveConnection(pdbConnSet);

	EnterCriticalSection(&pChannelObj->m_critTriggeredIDs);

	if(pChannelObj->m_pnTriggeredIDs)
	{
		delete [] pChannelObj->m_pnTriggeredIDs;
		pChannelObj->m_pnTriggeredIDs = NULL;
	}
	pChannelObj->m_nTriggeredIDs = 0;

	LeaveCriticalSection(&pChannelObj->m_critTriggeredIDs);



	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Ending trigger query thread for %s:%d", debugsource, pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	}
	else
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Ending trigger query thread for Omnibus stream %d", debugsource, pChannelObj->m_nChannelID);
	}
	g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusTriggerQueryThread", errorstring);    //(Dispatch message)

	pChannelObj->m_bTriggerQueryThreadStarted = false;

	Sleep(50);
	_endthread();
	Sleep(100);


}



// this is basically just a time reference thread now.
void NucleusAutomationThread(void* pvArgs)
{
	CNucleusAutomationChannelObject* pChannelObj = (CNucleusAutomationChannelObject*) pvArgs;
	if(pChannelObj==NULL) return;
	CNucleusEndpointObject* pEndObj = (CNucleusEndpointObject*) pChannelObj->m_pEndpoint;
	if(pEndObj==NULL) return;
	CNucleusMain* pnucleus = (CNucleusMain*) pChannelObj->m_pNucleus;
	if(pnucleus==NULL) return;

	char debugsource[MAX_MESSAGE_LENGTH];
	char errorstring[MAX_MESSAGE_LENGTH];

	char lebuf[256];
	sprintf(lebuf, "Logs\\N%dA.txt", pChannelObj->m_nChannelID);
	_timeb tmb;
	FILE* fpdbg = NULL; //in line file debug

	if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
	{
		fpdbg = fopen(lebuf, "ab");

		if(fpdbg)
		{
			_ftime(&tmb);
			fprintf(fpdbg, "[%d> %d.%03d START\r\n\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
			fclose(fpdbg);
			fpdbg = NULL;
		}

	}
	unsigned long ulAutoType  = 0;

	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
	{
		ulAutoType = NUCLEUS_DEP_AUTO_SENTINEL;
		sprintf(debugsource, "[%s:%d|%x>", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum, GetCurrentThreadId());
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Beginning automation thread for %s:%d", debugsource, pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	}
	else
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
	{
		ulAutoType = NUCLEUS_DEP_AUTO_HELIOS;
		sprintf(debugsource, "[%d|%x>", pChannelObj->m_nChannelID, GetCurrentThreadId());
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Beginning automation thread for Omnibus stream %d", debugsource, pChannelObj->m_nChannelID);
	}

	g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusAutomationThread", errorstring);    //(Dispatch message)

	pChannelObj->m_bAutomationThreadStarted=true;
	char szSQL[DB_SQLSTRING_MAXLEN];

/*
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning automation thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	}
	else
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning automation thread for Omnibus stream %d", pChannelObj->m_nChannelID);
	}

	g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusAutomationThread", errorstring);    //(Dispatch message)
*/

	strcpy(errorstring, "");
	CDBUtil db;


	CDBconn* pdbConn = db.CreateNewConnection(pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:automation_database_connect", errorstring);  //(Dispatch message)
			pdbConn = pnucleus->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW); 
		pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:automation_database_init", errorstring);  //(Dispatch message)
		pdbConn = pnucleus->m_data.m_pdbConn;

		//**MSG
	}



	CNucleusAutomationChannelObject tempChannelObj;
	bool bChangesPending = false;
	_timeb timebPending;
	_timeb timebChange;
	_ftime(&timebChange);
	_timeb timebSQL;
	_ftime(&timebSQL);

	timebChange.time -= (pnucleus->m_settings.m_nAutomationIntervalMS/1000)+1; // set it back so it goes immediately the first time around.
	timebSQL.time = timebChange.time;


	while((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
	{
		// check automation list for changes (mod)
		bool bChanges = false;
		bool bForceChanges = false;
		bool bTimeChanges = false;

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%d %d %d %d %d.%03d", pnucleus->m_data.m_nIndexAutomationEndpoint, pnucleus->m_settings.m_nNumEndpointsInstalled, pnucleus->m_settings.m_ppEndpointObject, pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint], pnucleus->m_data.m_timebTick.time, pnucleus->m_data.m_timebTick.millitm);   Sleep(50);//(Dispatch message)

		if(
			  (!pnucleus->m_data.m_bProcessSuspended)
			&&(pnucleus->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!pnucleus->m_data.m_key.m_bExpires)
				||((pnucleus->m_data.m_key.m_bExpires)&&(!pnucleus->m_data.m_key.m_bExpired))
				||((pnucleus->m_data.m_key.m_bExpires)&&(pnucleus->m_data.m_key.m_bExpireForgiveness)&&(pnucleus->m_data.m_key.m_ulExpiryDate+pnucleus->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!pnucleus->m_data.m_key.m_bMachineSpecific)
				||((pnucleus->m_data.m_key.m_bMachineSpecific)&&(pnucleus->m_data.m_key.m_bValidMAC))
				)

			)
		{

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg==NULL) fpdbg = fopen(lebuf, "ab");
			}
//			CNucleusEndpointObject* pAutoObj = pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint];

	_ftime(&pChannelObj->m_timebAutomationTick); // the last time check inside the thread
/*
			if((pEndObj->m_pszLiveEvents)&&(strlen(pEndObj->m_pszLiveEvents)))
			{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Checking %s", pAutoObj->m_pszLiveEvents);   Sleep(50);//(Dispatch message)
				pEndObj->m_nModLiveEvents = pEndObj->CheckDatabaseMod(&db, pdbConn, pEndObj->m_pszLiveEvents);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Checked mod %s = %d, %d last", pAutoObj->m_pszLiveEvents, pAutoObj->m_nModLiveEvents, pAutoObj->m_nLastModLiveEvents);   Sleep(50);//(Dispatch message)


				if((pAutoObj->m_nModLiveEvents>=0)&&(pAutoObj->m_nModLiveEvents!=pAutoObj->m_nLastModLiveEvents))
				{
					pAutoObj->m_dblLastAutomationChange = (double)pnucleus->m_data.m_timebAutomationTick.time + ((double)pnucleus->m_data.m_timebAutomationTick.millitm)/1000.0;
					bChanges = true;
					pnucleus->m_data.m_bFarEventsChanged = true;
					pnucleus->m_data.m_bNearEventsChanged = true;
					pnucleus->m_data.m_bTriggerEventsChanged = true;
					
				}
			}
*/
			// check the channel info view for time and changes.
			tempChannelObj.m_nListChanged = -1;
			tempChannelObj.m_dblListLastUpdate = -1.0;

			if(
//				  (!pnucleus->m_settings.m_bUseLocalClock) // no need to retrieve from db if using local time.  we getting list change info elsewhere anyway

// pnucleus->m_settings.m_bUseLocalClock will then cause pChannelObj->m_ulStatus = NUCLEUS_STATUS_CONN to never be set, resulting in ERROR being displayed continuously.
// also a problem because if the view returns no records, we dont have GetConnections called to refresh the list.
// OK, removed in 2.2.1.9 - we do the retrieve but do not set the time based on it if m_bUseLocalClock.
// We DO need to do the retrieve to get connection status and etc

//				&&
				  (pnucleus->m_settings.m_pszChannelInfo)&&(strlen(pnucleus->m_settings.m_pszChannelInfo))
				&&(// only do on interval
				    (pnucleus->m_data.m_timebTick.time > (timebSQL.time+pnucleus->m_settings.m_nAutomationIntervalMS/1000))
					||(
					    (pnucleus->m_data.m_timebTick.time==(timebSQL.time+pnucleus->m_settings.m_nAutomationIntervalMS/1000))
						&&(pnucleus->m_data.m_timebTick.millitm>(timebSQL.millitm+pnucleus->m_settings.m_nAutomationIntervalMS%1000))
						)
					)
				)
			{

				if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
				{

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT server_time, server_status, server_basis, server_changed, server_last_update, \
list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update FROM %s WHERE server = '%s' AND listid = %d",
						(g_pnucleus->m_settings.m_pszChannelInfo?g_pnucleus->m_settings.m_pszChannelInfo:"ChannelInfo"),

						pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum
						);
				}
				else
				if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT server_time, server_status, server_basis, server_changed, server_last_update, \
list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update FROM %s WHERE listid = %d",
						(g_pnucleus->m_settings.m_pszChannelInfo?g_pnucleus->m_settings.m_pszChannelInfo:"ChannelInfo"),

						pChannelObj->m_nChannelID
						);
				}



				CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
				if(prs != NULL)
				{
					if(prs->IsEOF()) // no record found!
					{
						pChannelObj->m_ulStatus = NUCLEUS_STATUS_NOTCON;
						// trigger a refresh.
						g_pnucleus->m_data.m_nLastConnectionsMod = -1; // causes GetConnections to be called.
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_CHANNELS) 	
{ g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:channelinfo", "no records found with %s", szSQL);}
					}
					else
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� not null");  Sleep(50);//(Dispatch message)
					if((!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
					{
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� NEOF");  Sleep(50);//(Dispatch message)
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_CHANNELS) 	
{ g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:channelinfo", "record found with %s", szSQL);}
						try
						{
							CString szTemp;
							prs->GetFieldValue("server_time", szTemp);
							tempChannelObj.m_dblServertime = atof(szTemp);

							prs->GetFieldValue("server_status", szTemp);
							tempChannelObj.m_nServerStatus = atoi(szTemp);

							prs->GetFieldValue("server_basis", szTemp);
							tempChannelObj.m_nServerBasis = atoi(szTemp);

							prs->GetFieldValue("server_changed", szTemp);
							tempChannelObj.m_nServerChanged = atoi(szTemp);

							prs->GetFieldValue("server_last_update", szTemp);
							tempChannelObj.m_dblServerLastUpdate = atof(szTemp);

							prs->GetFieldValue("list_state", szTemp);
							tempChannelObj.m_nListState = atoi(szTemp);

							prs->GetFieldValue("list_changed", szTemp);
							tempChannelObj.m_nListChanged = atoi(szTemp);

							prs->GetFieldValue("list_display", szTemp);
							tempChannelObj.m_nListDisplay = atoi(szTemp);

							prs->GetFieldValue("list_syschange", szTemp);
							tempChannelObj.m_nListSysChange = atoi(szTemp);

							prs->GetFieldValue("list_count", szTemp);
							tempChannelObj.m_nListCount = atoi(szTemp);

							prs->GetFieldValue("list_lookahead", szTemp);
							tempChannelObj.m_nListLookahead = atoi(szTemp);

							prs->GetFieldValue("list_last_update", szTemp);
							tempChannelObj.m_dblListLastUpdate = atof(szTemp);

							pChannelObj->m_ulStatus = NUCLEUS_STATUS_CONN;

							_ftime(&timebSQL);

						}
						catch(...)
						{
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_CHANNELS) 	
{ g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:channelinfo", "EXCEPTION with %s", szSQL);}
						}
					}
					delete prs;
					prs=NULL;
				}
				else
				{
					// error
					pChannelObj->m_ulStatus = NUCLEUS_STATUS_ERROR;//							0x00000020  // error (red icon)
					// trigger a refresh.
					g_pnucleus->m_data.m_nLastConnectionsMod = -1; // causes GetChannels to be called. (may as well...)

					pnucleus->m_msgr.DM(MSG_ICONERROR, NULL, "Nucleus:automation_channelinfo", errorstring);  //(Dispatch message)

				}

			}// if time to retrieve...
	
			_ftime( &pChannelObj->m_timebAutomationTick );  // we're still alive.


			if(( tempChannelObj.m_nListChanged > 0 )&&(tempChannelObj.m_nListDisplay > 0 ))
			{
				if(
					  (tempChannelObj.m_nListChanged != pChannelObj->m_nListChanged)
					||(tempChannelObj.m_nListDisplay != pChannelObj->m_nListDisplay)
					||(tempChannelObj.m_nServerChanged != pChannelObj->m_nServerChanged)
					||(tempChannelObj.m_nListState != pChannelObj->m_nListState)
					)
				{
	EnterCriticalSection(&pChannelObj->m_critChVar);
					pChannelObj->m_dblServertime  = tempChannelObj.m_dblServertime;
					pChannelObj->m_dblServerLastUpdate  = tempChannelObj.m_dblServerLastUpdate;
					pChannelObj->m_nServerStatus  = tempChannelObj.m_nServerStatus;
					pChannelObj->m_nServerBasis  = tempChannelObj.m_nServerBasis;
					pChannelObj->m_nServerChanged  = tempChannelObj.m_nServerChanged;

					pChannelObj->m_dblListLastUpdate  = tempChannelObj.m_dblListLastUpdate;
					pChannelObj->m_nListState  = tempChannelObj.m_nListState;
					pChannelObj->m_nListChanged  = tempChannelObj.m_nListChanged;
					pChannelObj->m_nListDisplay  = tempChannelObj.m_nListDisplay;
					pChannelObj->m_nListSysChange  = tempChannelObj.m_nListSysChange;
					pChannelObj->m_nListCount  = tempChannelObj.m_nListCount;
					pChannelObj->m_nListLookahead  = tempChannelObj.m_nListLookahead;
	LeaveCriticalSection(&pChannelObj->m_critChVar);
			
					
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "\r\n[%d> %d.%03d D>", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}

// following commented out to remain false until pending and force are checked.
// NO,we want to update the time right away.  however, we want to delay the event analysis.
					bChanges = true;
// OK this is not related at all to event analysis anymore

					if(!bChangesPending)
					{
						_ftime(&timebChange); // initialize this

					}

					bChangesPending = true;
					_ftime(&timebPending);
					timebPending.time += pnucleus->m_settings.m_nAnalyzeAutomationDwellMS/1000;
					timebPending.millitm += pnucleus->m_settings.m_nAnalyzeAutomationDwellMS%1000;
					if(timebPending.millitm>999)
					{
						timebPending.time++;
						timebPending.millitm -= 1000;
					}
				}
				else
				if((tempChannelObj.m_dblServerLastUpdate>0)&&(tempChannelObj.m_dblServerLastUpdate != pChannelObj->m_dblServerLastUpdate)) // we have a new update of server time
				{
					_ftime(&timebChange); // initialize this for the interval
	EnterCriticalSection(&pChannelObj->m_critChVar);
					pChannelObj->m_dblServertime  = tempChannelObj.m_dblServertime;
					pChannelObj->m_dblServerLastUpdate  = tempChannelObj.m_dblServerLastUpdate;
	LeaveCriticalSection(&pChannelObj->m_critChVar);
					bTimeChanges= true;
				}

			}
			else if(tempChannelObj.m_dblServerLastUpdate > 0.0)
			{
				if((tempChannelObj.m_dblServerLastUpdate>0)&&(tempChannelObj.m_dblServerLastUpdate != pChannelObj->m_dblServerLastUpdate)) // we have a new update of server time
				{
					_ftime(&timebChange); // initialize this for the interval
	EnterCriticalSection(&pChannelObj->m_critChVar);
					pChannelObj->m_dblServertime  = tempChannelObj.m_dblServertime;
					pChannelObj->m_dblServerLastUpdate  = tempChannelObj.m_dblServerLastUpdate;
	LeaveCriticalSection(&pChannelObj->m_critChVar);
					bTimeChanges= true;
				}
			}


			if(bChangesPending)
			{
				double dblTime = ((double)(timebChange.time))*1000.0 + (double)timebChange.millitm;
				if((tempChannelObj.m_dblListLastUpdate>0.0)&&(tempChannelObj.m_dblListLastUpdate < (dblTime-pnucleus->m_settings.m_nAnalyzeAutomationForceMS)))
				{
					bForceChanges = true;  // becuase we changed a while ago.  want to go immediately.
				}
			}

			// dwell changes - another change will reset the time, suppressing the previous change

			if(
				  (bChangesPending)
				&&(
						(bForceChanges)
				  ||(pnucleus->m_data.m_timebTick.time>timebPending.time)
					||(
					    (pnucleus->m_data.m_timebTick.time==timebPending.time)
						&&(pnucleus->m_data.m_timebTick.millitm>timebPending.millitm)
						)
					||(pnucleus->m_data.m_timebTick.time > (timebChange.time+pnucleus->m_settings.m_nAnalyzeAutomationForceMS/1000))
					||(
					    (pnucleus->m_data.m_timebTick.time==(timebChange.time+pnucleus->m_settings.m_nAnalyzeAutomationForceMS/1000))
						&&(pnucleus->m_data.m_timebTick.millitm>(timebChange.millitm+pnucleus->m_settings.m_nAnalyzeAutomationForceMS%1000))
						)
					)
				)
			{
				bForceChanges = false;
				bChangesPending = false;
	EnterCriticalSection(&pChannelObj->m_critChVar);
				pChannelObj->m_bTriggerEventsChanged = true;
				pChannelObj->m_bTriggerEventsChangedLocal = true;// added 2.2.1.21 (15 July 2016)
	LeaveCriticalSection(&pChannelObj->m_critChVar);

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "\r\n[%d> %d.%03d R", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}

//				if(pChannelObj->m_nAnalysisCounter >= INT_MAX) pChannelObj->m_nAnalysisCounter = 0;
//				else		pChannelObj->m_nAnalysisCounter++;

//following moved to immediate from delayed.... 
				if(pChannelObj->m_nAnalysisCounter >= INT_MAX) pChannelObj->m_nAnalysisCounter = 0;
				else	pChannelObj->m_nAnalysisCounter++;

//				pChannelObj->m_bNearEventsChanged = true;
				// do some delayed stuff here.
			}


			if((bChanges)||(bTimeChanges))
			{

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					fprintf(fpdbg, "1");
					fflush(fpdbg);
				}
			}
//				pChannelObj->m_bTriggerEventsChanged = true;  // moved to dwell.
				pChannelObj->m_bTriggerEventsChangedLocal = true;  // // added 2.2.1.21 (15 July 2016) to prevent all changes on all channels from global thread.
//				pChannelObj->m_bFarEventsChanged = true;


//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Automation changes exist %d", pAutoObj->m_nModLiveEvents);  // Sleep(50);//(Dispatch message)
				// pnucleus->m_data.m_nEventLastMax = 0; // start at the beginning....
// actually dont reset this.
				bChanges = false;
				bTimeChanges = false;  // have to reset this one too!
				bool bSQLError = false;
				// if changes
				// 
				//   Close the m_prsEvents recordset and delete it, set it back to NULL.

//				pnucleus->m_data.ReleaseRecordSet();

				_ftime( &pChannelObj->m_timebAutomationTick); 
				if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK)==NUCLEUS_DEP_AUTO_HELIOS)
				{
					// can only use local clock
	EnterCriticalSection(&pChannelObj->m_critChVar);
					if(pnucleus->m_settings.m_bUseUTC)
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
							  ((double)(pChannelObj->m_timebAutomationTick.time))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + pnucleus->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}
					else
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
							  ((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
							+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + pnucleus->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}
	LeaveCriticalSection(&pChannelObj->m_critChVar);


				}
				else
				if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK)==NUCLEUS_DEP_AUTO_SENTINEL)
				{
					//update the servertime.

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Automation svr atof? %f", timeval);  Sleep(50);//(Dispatch message)
						
//  THE following 2 ifs were switched,
	EnterCriticalSection(&pChannelObj->m_critChVar);
					if((pnucleus->m_settings.m_bUseLocalClock)||(pChannelObj->m_dblServerLastUpdate<=0.0))
					{
						// and this one was not m_bUseUTC, but m_bUseLocalClock, that is wrong.
						if(pnucleus->m_settings.m_bUseUTC)
						{
							pChannelObj->m_dblAutomationTimeEstimate = 
									((double)(pChannelObj->m_timebAutomationTick.time))
								+ ((double)(pChannelObj->m_timebAutomationTick.millitm + pnucleus->m_settings.m_nTriggerAdvanceMS))/1000.0;
						}
						else
						{
							pChannelObj->m_dblAutomationTimeEstimate = 
									((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
								+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))
								+ ((double)(pChannelObj->m_timebAutomationTick.millitm + pnucleus->m_settings.m_nTriggerAdvanceMS))/1000.0;
						}
					}
					else
					{
						if(pChannelObj->m_dblServerLastUpdate>0.0)
						{
							/*
							pChannelObj->m_dblAutomationTimeDiffMS = 
								(((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
								+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm) 
								- pChannelObj->m_dblServerLastUpdate;
							*/
							pChannelObj->m_dblAutomationTimeDiffMS = 
								((double)pChannelObj->m_timebAutomationTick.time)*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm
								- pChannelObj->m_dblServerLastUpdate;
						// DEMO only, need to handle midnight.
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Automation svr time in unix? %f", timeval);  Sleep(50);//(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Automation svr time diffMS? %d", nDiff);  Sleep(50);//(Dispatch message)
							pChannelObj->m_dblAutomationTimeEstimate = (pChannelObj->m_dblServertime + pChannelObj->m_dblAutomationTimeDiffMS 
								+ (double)pnucleus->m_settings.m_nTriggerAdvanceMS)/1000.0;
//										pnucleus->m_data.m_dblAutomationTimeEstimate = (double)timebTempTime.time;
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Automation estimate time? %f", pnucleus->m_data.m_dblAutomationTimeEstimate);  Sleep(50);//(Dispatch message)
						}
					}

	LeaveCriticalSection(&pChannelObj->m_critChVar);

				}  // type was sentinel
				// else automation type unknown, so nothing
			} // automation changes exist

/////////////////////////////////////////////////////////
			else  // no automation changes
			{
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					fprintf(fpdbg, "0");
					fflush(fpdbg);
				}
			}

				_ftime( &pChannelObj->m_timebAutomationTick); 
				if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK)==NUCLEUS_DEP_AUTO_HELIOS)
				{
					// can only use local clock

	EnterCriticalSection(&pChannelObj->m_critChVar);
					if(pnucleus->m_settings.m_bUseUTC)
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
							  ((double)(pChannelObj->m_timebAutomationTick.time))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + pnucleus->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}
					else
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
							  ((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
							+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + pnucleus->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}
	LeaveCriticalSection(&pChannelObj->m_critChVar);


				}
				else
				if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK)==NUCLEUS_DEP_AUTO_SENTINEL)
				{
	EnterCriticalSection(&pChannelObj->m_critChVar);
					// just update the time based on estimate
					if((pnucleus->m_settings.m_bUseLocalClock)||(pChannelObj->m_dblServerLastUpdate<=0.0))
					{
						if(pnucleus->m_settings.m_bUseUTC)
						{
							pChannelObj->m_dblAutomationTimeEstimate = 
									((double)(pChannelObj->m_timebAutomationTick.time))
								+ ((double)(pChannelObj->m_timebAutomationTick.millitm + pnucleus->m_settings.m_nTriggerAdvanceMS))/1000.0;
						}
						else
						{
							pChannelObj->m_dblAutomationTimeEstimate = 
									((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
								+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))
								+ ((double)(pChannelObj->m_timebAutomationTick.millitm + pnucleus->m_settings.m_nTriggerAdvanceMS))/1000.0;
						}
					}
					else
					{
						if(pChannelObj->m_dblServerLastUpdate>0.0)
						{
							/*
							pChannelObj->m_dblAutomationTimeDiffMS = (((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
								+(pChannelObj->m_timebAutomationTick.dstflag?3600:0)))*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm) 
								- pChannelObj->m_dblServerLastUpdate;
								*/

							pChannelObj->m_dblAutomationTimeDiffMS = 
								((double)pChannelObj->m_timebAutomationTick.time)*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm
								- pChannelObj->m_dblServerLastUpdate;

							pChannelObj->m_dblAutomationTimeEstimate = (pChannelObj->m_dblServertime + pChannelObj->m_dblAutomationTimeDiffMS 
								+ (double)pnucleus->m_settings.m_nTriggerAdvanceMS)/1000.0;
						}
					}

	LeaveCriticalSection(&pChannelObj->m_critChVar);

				}

/*
				pnucleus->m_data.m_dblAutomationTimeEstimate = pnucleus->m_data.m_dblHarrisTime + ((double)(pnucleus->m_data.m_nHarrisTimeDiffMS)/1000.0) + (double)pnucleus->m_settings.m_nTriggerAdvanceMS/1000.0
					+ ((double)timebTempTime.time - (double)pnucleus->m_data.m_timebHarrisCheckTimeLocal.time)
					+ ((double)timebTempTime.millitm/1000.0 - (double)pnucleus->m_data.m_timebHarrisCheckTimeLocal.millitm/1000.0); // the difference beween now and last check
//										pnucleus->m_data.m_dblAutomationTimeEstimate = (double)timebTempTime.time;
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Automation estimate time now %f", pnucleus->m_data.m_dblAutomationTimeEstimate);  Sleep(50);//(Dispatch message)
*/

			}


		} // if license etc



		Sleep(1); // dont peg processor
	} // while((!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
if(fpdbg)
{
	_ftime(&tmb);
	fprintf(fpdbg, "[%d> %d.%03d\r\n\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
//	fclose(fpdbg);
//	fpdbg = NULL;
}
			}
			else
			{
				if(fpdbg!=NULL)
				{
	fclose(fpdbg);
	fpdbg = NULL;
				}
			}

	db.RemoveConnection(pdbConn);
	Sleep(30);
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Ending automation thread for %s:%d", debugsource, pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	}
	else
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Ending automation thread for Omnibus stream %d", debugsource, pChannelObj->m_nChannelID);
	}

	g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusAutomationThread", errorstring);    //(Dispatch message)
	pChannelObj->m_bAutomationThreadStarted=false;

  Sleep(50);//(Dispatch message)
	_endthread();
	Sleep(150);

}

#ifndef NOT_USING_NEAR_ANALYSIS
// NO LONGER USING NEAR ANALYSIS
void NucleusNearAnalysisThread(void* pvArgs)
{
	CNucleusAutomationChannelObject* pChannelObj = (CNucleusAutomationChannelObject*) pvArgs;
	if(pChannelObj==NULL) return;
	CNucleusEndpointObject* pEndObj = (CNucleusEndpointObject*) pChannelObj->m_pEndpoint;
	if(pEndObj==NULL) return;
	CNucleusMain* pnucleus = (CNucleusMain*) pChannelObj->m_pNucleus;
	if(pnucleus==NULL) return;

	pChannelObj->m_bNearAnalysisThreadStarted=true;
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	unsigned long ulAutoType  = 0;

	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
	{
		ulAutoType = NUCLEUS_DEP_AUTO_SENTINEL;
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning analysis thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	}
	else
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
	{
		ulAutoType = NUCLEUS_DEP_AUTO_HELIOS;
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning analysis thread for Omnibus stream %d", pChannelObj->m_nChannelID);
	}
	g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusAnalysisThread", errorstring);    //(Dispatch message)
	
	strcpy(errorstring, "");
	CDBUtil db;
	CDBUtil dbSet;

	CDBconn* pdbConn = db.CreateNewConnection(pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:analysis_database_connect", errorstring);  //(Dispatch message)
			pdbConn = pnucleus->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW); 
		pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:analysis_database_init", errorstring);  //(Dispatch message)
		pdbConn = pnucleus->m_data.m_pdbConn;

		//**MSG
	}

	CDBconn* pdbConnSet = dbSet.CreateNewConnection(pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW);
	if(pdbConnSet)
	{
		if(dbSet.ConnectDatabase(pdbConnSet, errorstring)<DB_SUCCESS)
		{
			pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:analysis_database2_connect", errorstring);  //(Dispatch message)
			pdbConnSet = pnucleus->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW); 
		pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:analysis_database2_init", errorstring);  //(Dispatch message)
		pdbConnSet = pnucleus->m_data.m_pdbConn;

		//**MSG
	}

	while(!pChannelObj->m_bNearEventsChanged)
	{
		_ftime(&pChannelObj->m_timebNearTick); // the last time check inside the thread
		Sleep(30);
	}

//	Sleep(30000); // dont start thread analysis util after 30 sec


	double delta = 0.0;
	double wait = 0.0;
	while((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
	{
		if(
/*
			  (pnucleus->m_data.m_nIndexAutomationEndpoint>=0)
			&&(pnucleus->m_settings.m_nNumEndpointsInstalled>0)
			&&(pnucleus->m_data.m_nIndexAutomationEndpoint<pnucleus->m_settings.m_nNumEndpointsInstalled)
			&&(pnucleus->m_settings.m_ppEndpointObject)
			&&(pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint])
			&&*/
			  (!pnucleus->m_data.m_bProcessSuspended)

			&&(

					(
						(ulAutoType == NUCLEUS_DEP_AUTO_SENTINEL)
					&&(pChannelObj->m_nListState&((1<<LISTTHREADING)|(1<<LISTISPLAYING)))  // list must be threaded and playing to run analysis
//					&&(!(pChannelObj->m_nListState&((1<<LISTINFREEZE)|(1<<LISTINHOLD))))  // list can be in freeze or hold to run analysis
					)
				||(
						(ulAutoType == NUCLEUS_DEP_AUTO_HELIOS)
					&&(pChannelObj->m_nListState!=0)  // list must be connected
//					&&(pChannelObj->m_nServerStatus!=0)  // server must be connected!  // comment this out to surivive adaptor disconnection retries....
					)
				)
				
// old				&&(pChannelObj->m_nListState&((1<<LISTTHREADING)|(1<<LISTISPLAYING)))  // list must be threaded and playing to run analysis
			&&(pnucleus->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!pnucleus->m_data.m_key.m_bExpires)
				||((pnucleus->m_data.m_key.m_bExpires)&&(!pnucleus->m_data.m_key.m_bExpired))
				||((pnucleus->m_data.m_key.m_bExpires)&&(pnucleus->m_data.m_key.m_bExpireForgiveness)&&(pnucleus->m_data.m_key.m_ulExpiryDate+pnucleus->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!pnucleus->m_data.m_key.m_bMachineSpecific)
				||((pnucleus->m_data.m_key.m_bMachineSpecific)&&(pnucleus->m_data.m_key.m_bValidMAC))
				)

			)
		{
//			CNucleusEndpointObject* pAutoObj = pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint];

			//Here, we analyze things that have not yet played, that are not yet analyzed, unless its within m_ulTriggerCueThreshold seconds of play time, then it gets reanalyzed
	_ftime(&pChannelObj->m_timebNearTick); // the last time check inside the thread

			pChannelObj->m_bNearEventsChanged = false;

			while((!pChannelObj->m_bNearEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
			{
	_ftime(&pChannelObj->m_timebNearTick); // the last time check inside the thread
				// just get the exports with params to parse.
/*
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
			//		  "select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE TriggerTime > %lf and ((event_status & 19) = 0) and ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY TriggerTime ASC) ORDER BY TriggerTime ASC",
//						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_calc_start > %u) and ((event_status & 19) = 0)) ORDER BY event_calc_start ASC)  and ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY TriggerTime ASC",
						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_status & 19) = 0) ORDER BY event_calc_start ASC) and server = '%s' AND listid = %d AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY TriggerTime ASC",
//						"SELECT TOP 1 * FROM %s WHERE TriggerTime > %lf and event_status & 19 = 0 and flags & %d = 0 ORDER BY TriggerTime ASC",
						pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
					//	(unsigned long)pnucleus->m_data.m_dblAutomationTimeEstimate,
						pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListID,	NUCLEUS_FLAG_REANALYZED
					);
*/


/*
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_status & 19) = 0) and channelid = %d AND AnalyzedTriggerDataFlags <> %d  ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
						pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
						pChannelObj->m_nChannelID, pChannelObj->m_nAnalysisCounter
					);
*/
				if(ulAutoType == NUCLEUS_DEP_AUTO_SENTINEL)
				{
		
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
							"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_status & 19) = 0) and channelid = %d AND ((AnalyzedTriggerDataFlags & %d) = 0)  ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
							pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							pChannelObj->m_nChannelID, NUCLEUS_FLAG_REANALYZED
						);
				
				//			need top x where top event item id is all same where event status & 19 =0 (not play, post, done)
				}
				else
				if(ulAutoType == NUCLEUS_DEP_AUTO_HELIOS)
				{
		
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
							"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE event_status < 10 and event_status > 4 and channelid = %d AND ((AnalyzedTriggerDataFlags & %d) = 0)  ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
							pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							pChannelObj->m_nChannelID, NUCLEUS_FLAG_REANALYZED
						);
				
				//			need top x where top event item id is all same where event status < 10  and event_status > 4 (not done. no errors, and allocated or better in system)
				}
					
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Near analysis SQL: %s", szSQL); // Sleep(50);//(Dispatch message)
				int nNumRec = 0;
				CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
				if(prs != NULL)
				{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Near analysis resultset not null %d record", prs->GetRecordCount( )); // Sleep(50);//(Dispatch message)
					while((!pChannelObj->m_bNearEventsChanged)&&(!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
					{
	_ftime(&pChannelObj->m_timebNearTick); // the last time check inside the thread
						nNumRec++;
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Near analysis resultset Got %d items so far", nNumRec); // Sleep(50);//(Dispatch message)
						CString szName;
						CString szValue;
						CString szTemp;
						//double dblTime;
						double dblClipTime;
						double dblEventTime;
						double dblParentClipTime;

						int nTriggerItemID=-1;
						int nAutomationItemID=-1;
						int nNucleusEventItemID=-1;
						int nEventActionID=-1;
						int nType=-1;
						int nAnalysisID=-1;

						prs->GetFieldValue("itemid", szTemp);
						if(szTemp.GetLength()) nAutomationItemID = atoi(szTemp);

						// Host
						// ParsedValue
						//prs->GetFieldValue("TriggerTime", szTemp);
						//dblTime=atof(szTemp);
						prs->GetFieldValue("analyzed_trigger_id", szTemp);
						if(szTemp.GetLength()) nTriggerItemID = atoi(szTemp);

						prs->GetFieldValue("AnalyzedTriggerDataFlags", szTemp);
						unsigned long ulFlags = atol(szTemp);
						//AnalyzedTriggerDataStatus

						prs->GetFieldValue("nucleus_EventID", szTemp);
						if(szTemp.GetLength()) nNucleusEventItemID = atoi(szTemp);
						//Nucleus_Timing_Col_Name
						//Source
						//Scene
						prs->GetFieldValue("Graphic_Action_Type", szTemp);
						nType = atoi(szTemp);
						//Action_Name


						prs->GetFieldValue("Event_ActionID", szTemp);
						if(szTemp.GetLength()) nEventActionID = atoi(szTemp);

						//Action_Value
						prs->GetFieldValue("Action_Value", szValue);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Near Analysis value is: %s", szValue);//  Sleep(50);//(Dispatch message)
					
						prs->GetFieldValue("Analysis_ID", szTemp);
						if(szTemp.GetLength()) nAnalysisID = atoi(szTemp);

						prs->GetFieldValue("parent_calc_start", szTemp);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis got TriggerTime %s", szTemp); 
						dblParentClipTime=atof(szTemp);

						prs->GetFieldValue("event_calc_start", szTemp);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis got TriggerTime %s", szTemp); 
						dblClipTime=atof(szTemp);

						prs->GetFieldValue("event_start", szTemp);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis got TriggerTime %s", szTemp); 
						dblEventTime=atof(szTemp);

// if analysis changes an item within X seconds of now, lets refresh the trigger buffer by setting pChannelObj->m_bTriggerEventsChanged = true
								if(nType == 0) // anim
								{
									// just set to 
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "anim is retrieved");//  Sleep(50);//(Dispatch message)
									ulFlags |= NUCLEUS_FLAG_REANALYZED;
								//	ulFlags = pChannelObj->m_nAnalysisCounter;

									if(nTriggerItemID<=0)
									{
										if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
										{
											//insert
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, nucleus_eventid, event_actionid, flags, analysis_id) values \
(%d, %d, %d, %d, %d)",
													pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nNucleusEventItemID, nEventActionID, ulFlags, nAnalysisID
												);
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug1", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
											if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
											{
											//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing near SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
											}//else break;

										}
									}
									else
									{
										// update
										if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
										{
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, nucleus_eventid = %d, Event_ActionID = %d, flags = %d, analysis_id = %d WHERE analyzed_trigger_id = %d",
													pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nNucleusEventItemID, nEventActionID, ulFlags, nAnalysisID, nTriggerItemID
												);
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
											if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
											{
											//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing near SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
											}
										}
									}
								}
								else
								if(nType == 1) // export
								{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "export %s is retrieved", szValue);//  Sleep(50);//(Dispatch message)
									if(
											(szValue.GetLength())
										&&(
												((pnucleus->m_settings.m_bUseUTF8)&&(szValue.Find("§")>=0))
											||((!pnucleus->m_settings.m_bUseUTF8)&&(szValue.Find("�")>=0))
											)
										)
									{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "NEAR value has a �: %s", szValue);  Sleep(50);//(Dispatch message)
										// we have a least one param to parse
										// just set to renanalyzed
										ulFlags |= NUCLEUS_FLAG_REANALYZED;
							//			ulFlags = pChannelObj->m_nAnalysisCounter;

										// lets parse out szValue and set it up;

										CString szPreParse = szValue;
										szValue = "";

										int i=0;

										while((i<szPreParse.GetLength())&&(!pChannelObj->m_bNearEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
										{
											bool bDelimFound=false;
											char ch = szPreParse.GetAt(i);
											if(pnucleus->m_settings.m_bUseUTF8)
											{
												if((ch == '�')&&(szPreParse.GetAt(i+1) == '�'))	
												{
													bDelimFound=true; i+=2;
												}
											}
											else
											{
												if(ch == '�')
												{
													bDelimFound=true; i++;
												}
											}

											if(bDelimFound)
											{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "NEAR ch has a �");  Sleep(50);//(Dispatch message)

												CString szParam = "";
												CString szStyle = "";

												bDelimFound=false;												
												ch = szPreParse.GetAt(i);

												bool bStyle = false;
												if(pnucleus->m_settings.m_bUseUTF8)
												{
													if((ch == '�')&&(szPreParse.GetAt(i+1) == '�'))	
													{
														bDelimFound=true; i+=2;
													}
												}
												else
												{
													if(ch == '�')
													{
														bDelimFound=true; i++;
													}
												}

												
												while((!bDelimFound)&&(i<szPreParse.GetLength())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
												{
													if(!bStyle)
													{
														szParam += ch;
													}
													else
													{
														szStyle += ch;
													}
													i++;
													ch = szPreParse.GetAt(i);
													if(ch == ':')
													{
														i++;
														bStyle = true;
														ch = szPreParse.GetAt(i);
													}
													if(pnucleus->m_settings.m_bUseUTF8)
													{
														if((ch == '�')&&(szPreParse.GetAt(i+1) == '�'))	
														{
															bDelimFound=true; i+=2;
														}
													}
													else
													{
														if(ch == '�')
														{
															bDelimFound=true; i++;
														}
													}
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "near Param %s, Style %s", szParam,szStyle);  Sleep(50);//(Dispatch message)


												}
												

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "near FINAL Param %s, Style %s", szParam,szStyle);  Sleep(50);//(Dispatch message)
												// ok here, we should have the param name and possible style.
												// first lets get the param.

/*												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s '%s', %f",
														pnucleus->m_settings.m_pszGetParameterValueProc?pnucleus->m_settings.m_pszGetParameterValueProc:"spGetParameterValue",  
														szParam, dblTime
													);
*/

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
/*
												// this sets the param
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
											//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}

												// this gets it!
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM [Output]");
*/
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
			
// try 2
/*
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "{CALL %s('%s', %f)}",
														pnucleus->m_settings.m_pszGetParameterValueProc?pnucleus->m_settings.m_pszGetParameterValueProc:"spGetParameterValue",  
														szParam, dblTime
													);

*/												
				// try 3								
											
												if((!pChannelObj->m_bNearEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))												
												{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Near parameter retrieve index for [%s]", szParam); // Sleep(50);//(Dispatch message)
													EnterCriticalSection(&pnucleus->m_data.m_critParameterRules);
													int idx = pnucleus->m_data.GetParameterQueryIndex(szParam);


	//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Near parameter retrieved index %d", idx); // Sleep(50);//(Dispatch message)
													if(idx>=0)
													{
														CString timestr;  
														if(pnucleus->m_data.m_ppParameterRule[idx]->sz_target_valueid.Compare("0"))
														{
															if(pnucleus->m_settings.m_bNearAnalysisUsesCalc)
																timestr.Format("%.3f", dblClipTime);
															else
																timestr.Format("%.3f", dblEventTime);
														}
														else
														{
													//		if(pnucleus->m_settings.m_bNearAnalysisUsesCalc)
													//			timestr.Format("%.3f", dblClipTime-0.020);
													//		else
																timestr.Format("%.3f", dblParentClipTime-0.020);  // now should always be the real time
														}
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s%s%s",
																pnucleus->m_data.m_ppParameterRuleQuery[idx]->sz_query1,
																timestr,
																pnucleus->m_data.m_ppParameterRuleQuery[idx]->sz_query2
															);
//if(pnucleus->m_settings.m_bUseEmail) 	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Near � get param (%s) Query: %s",szParam, szSQL); // Sleep(50);//(Dispatch message)
													}

													LeaveCriticalSection(&pnucleus->m_data.m_critParameterRules);
												
													
													
													CRecordset* prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);






//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� SQL ERROR %s", errorstring);  Sleep(50);//(Dispatch message)


/*

												bool bGotit=false;
												CString target_valueid, target_criterion_col_name, criterion_value, returned_property_col_name, default_value;

_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select target_valueid, target_criterion_col_name, criterion_value, returned_property_col_name, [default_value] from [Parameters] where [name] = '%s'",szParam);
//_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from Analysis");

pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
CRecordset* prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);

												if(prsParam != NULL)
												{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� not null");  Sleep(50);//(Dispatch message)
													if((!prsParam->IsEOF())&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
													{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� NEOF");  Sleep(50);//(Dispatch message)
														try
														{
															prsParam->GetFieldValue("target_valueid", target_valueid);
															prsParam->GetFieldValue("target_criterion_col_name", target_criterion_col_name);
															prsParam->GetFieldValue("criterion_value", criterion_value);
															prsParam->GetFieldValue("returned_property_col_name", returned_property_col_name);
															prsParam->GetFieldValue("default_value", default_value);

															bGotit=true;
														}
														catch(CException *e)// CDBException *e, CMemoryException *m)  
														{
															if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
															{
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);  Sleep(50);//(Dispatch message)
															}
															else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
															{
																// The error code is in e->m_nRetCode
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Retrieve: Caught exception: out of memory");
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
															}
															else 
															{
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Retrieve: Caught other exception.\n%s", szSQL);
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
															}
															e->Delete();
														} 
														catch( ... )
														{
														}
													}
													prsParam->Close();
													delete prsParam;
												}

if(bGotit)
{
_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top 1 val, datatype from (select top ' + %s + ' (case when len(' + %s + ') > 0 THEN ' + %s + ' ELSE ''' + %s + ''' END) as val, event_start, (select datatype from MetaConfigUnionALLView \
where col_name = ''' + %s + ''') as datatype from cortex.dbo.LiveEventData as LiveEventData where event_start > ' + %f + ' and \
(event_status is NULL or event_status = 0) and ' + %s + ' = ''' + %s + ''' order by event_start) as Temp order by event_start desc'",
					target_valueid,returned_property_col_name,returned_property_col_name,
					default_value,returned_property_col_name,
					dblTime,target_criterion_col_name,criterion_value
					);
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "2� SQL %s", szSQL);  Sleep(50);//(Dispatch message)

prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);




}
else
{
	prsParam = NULL;
}
*/

													if(prsParam != NULL)
													{
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "NEAR ************** 2� not null"); // Sleep(50);//(Dispatch message)
														if((!prsParam->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
														{
	//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "2� NEOF");  Sleep(50);//(Dispatch message)

															CString szDataType;
															try
															{
																prsParam->GetFieldValue((short)0, szTemp);  //"val" field name
																prsParam->GetFieldValue((short)1, szDataType);  // "datatype" field name
	//															prsParam->GetFieldValue("value", szTemp);  // for use with output table
	//															prsParam->GetFieldValue("datatype", szDataType);  // sp now returns datatype
//if(pnucleus->m_settings.m_bUseEmail) 		pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "NEAR � got %s (datatype = %s) for %s, %s",szTemp, szDataType, szSQL, szPreParse);  Sleep(50);//(Dispatch message)

															}
															catch(CException *e)// CDBException *e, CMemoryException *m)  
															{
																if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
																{
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);  Sleep(50);//(Dispatch message)
																}
																else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
																{
																	// The error code is in e->m_nRetCode
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Retrieve: Caught exception: out of memory");
										//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
																}
																else 
																{
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Retrieve: Caught other exception.\n%s", szSQL);
										//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
																}
																e->Delete();
																szTemp=""; // blank to be safe
															} 
															catch( ... )
															{
																szTemp=""; // blank to be safe
															}



			//												szTemp = "Parameter!";

	//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� got %s",szTemp);  Sleep(50);//(Dispatch message)
															if(szTemp.GetLength())
															{
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� returned %s", szTemp);  Sleep(50);//(Dispatch message)
																double dblNumber = atof(szTemp);
																unsigned long ulTime = (unsigned long)(dblNumber);
	//															bool bTime = (ulTime>1)?true:false;
																bool bTime = (szDataType.CompareNoCase("Time")==0)?true:false;
																if(bTime)
																{
																	ulTime%=86400;
																	szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
	//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "szParam =  %s", szParam);  Sleep(50);//(Dispatch message)
																}
																bool bPM = true;
																if(ulTime<43200) bPM = false;
																
		
																if(szStyle.GetLength())
																{

																	// now have to get a list of styles
																	prsParam->Close();
																	delete prsParam;

																	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select actions from nucleus.dbo.styles as styles where name = '%s'", szStyle);
	//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "action near SQL: %s", szSQL);  Sleep(50); //(Dispatch message)

																	prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);
	//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "action near SQL error?: %s", errorstring);  Sleep(50); //(Dispatch message)

																	int nNumStyles = 0;
																	
																	szParam = "";
																	bool bAdded = false;
																	//unsigned long ulTimeTemp = ulTime;
																	if(prsParam != NULL)
																	{

																		if((!prsParam->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
																		{
																			prsParam->GetFieldValue("actions", szStyle);
	//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "action got: %s", szStyle);  Sleep(50); //(Dispatch message)


																			CSafeBufferUtil sbu;
																			char actions[2048];
																			sprintf(actions, "%s", szStyle);
																			char* paction = sbu.Token(actions, strlen(actions), "|");
																			bool b24 = false;

																			while((paction)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
																			{
																				if((strnicmp(paction, "snapTo(", 7)==0)&&(strlen(paction)>7))
																				{
																					int nMinutes = atoi(paction+7)*60;
																					nNumStyles++;

																					unsigned long ulTimeTemp = ((ulTime + 150)/nMinutes)*nMinutes;
																					ulTime=ulTimeTemp;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00:%02d", ((ulTime)%3600)/60); // special case 00
																						else
																							szParam.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					}
																					else
																						szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
																				}
																				else 
/*
																				if(stricmp(paction, "snapTo(5)")==0)
																				{
																					nNumStyles++;

																					unsigned long ulTimeTemp = ((ulTime + 150)/300)*300;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(15)")==0)
																				{
																					nNumStyles++;
																					unsigned long ulTimeTemp = ((ulTime +450)/900)*900;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(30)")==0)
																				{
																					nNumStyles++;
																					unsigned long ulTimeTemp = ((ulTime+900)/1800)*1800;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(60)")==0)
																				{
																					unsigned long ulTimeTemp = ((ulTime+1800)/3600)*3600;
																					ulTime=ulTimeTemp;
																					nNumStyles++;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
*/
																				if(stricmp(paction, "clock(24)")==0)
																				{
																					szTemp.Format("%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																					nNumStyles++;
																					b24 = true;
																				}
																				else 
																				if(stricmp(paction, "clock(12)")==0)
																				{
																					nNumStyles++;
																					if(ulTime>=43200) ulTime-=43200;
																					szTemp.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60);
																					b24 = false;
																				}
																				else 
																				if(stricmp(paction, "formatTime('H:MM:SS')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00:%02d:%02d", ((ulTime)%3600)/60, ((ulTime)%3600)%60); // special case 00
																						else
																							szParam.Format("%d:%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					}		
																					else
																						szParam.Format("%d:%02d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH:MM:SS')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																						szParam.Format("%02d:%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					else
																						szParam.Format("%02d:%02d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH:MM')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																						szParam.Format("%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					else
																						szParam.Format("%02d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('H:MM')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00:%02d", ((ulTime)%3600)/60); // default
																						else
																							szParam.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					}
																					else
																						szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('MM')")==0)
																				{
																					nNumStyles++;
																					szParam.Format("%02d", ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('H')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00"); // default
																						else
																							szParam.Format("%d", (ulTime)/3600); // default
																					}
																					else
																						szParam.Format("%d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600))); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																						szParam.Format("%02d", (ulTime)/3600); // default
																					else
																						szParam.Format("%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600))); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoW')")==0)
																				{ //i.e. Monday
																					nNumStyles++;

																					char buffer[48]; // need fewer;
																					long timeofthing = (long)dblNumber; 
																					tm* theTime = gmtime( &timeofthing );
																					strftime( buffer, 40, "%A", theTime );
																					szParam.Format("%s", buffer); 
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_pnucleus->m_data.m_timebTick.time - (g_pnucleus->m_data.m_timebTick.timezone*60) +(g_pnucleus->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoWex')")==0)
																				{ //i.e. Monday, but:
																					//if later today
																					//{ if > 6pm "tonight" else "later" }
																					// else if tomorrow "tomorrow" 
																					// else if > 1 week from now "coming"
																					// else "Monday" style day of week

																					unsigned long ulNow = (unsigned long)(g_pnucleus->m_data.m_timebTick.time - (g_pnucleus->m_data.m_timebTick.timezone*60) +(g_pnucleus->m_data.m_timebTick.dstflag?3600:0)); // local time....

																					if((ulNow/86400) == (((unsigned long)dblNumber)/86400)) // today
																					{
																						if((ulTime/3600)>17) // tonight, 6pm or later
																						{
																							szParam = "Tonight";
																						}
																						else
																						{
																							szParam = "Later";
																						}
																					}
																					else
																					if((ulNow/86400) == ((((unsigned long)dblNumber)/86400)-1)) // tomorrow
																					{
																						szParam = "Tomorrow";
																					}
																					else
																					if((ulNow/86400) <= ((((unsigned long)dblNumber)/86400)-7)) // next week
																					{
																						szParam = "Coming";
																					}
																					else
																					{
																						char buffer[48]; // need fewer;
																						long timeofthing = (long)ulTime; 
																						tm* theTime = gmtime( &timeofthing );
																						strftime( buffer, 40, "%A", theTime );
																						szParam.Format("%s", buffer); 
																					}

																					nNumStyles++;
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_pnucleus->m_data.m_timebTick.time - (g_pnucleus->m_data.m_timebTick.timezone*60) +(g_pnucleus->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoWa')")==0)
																				{ //i.e. Monday
																					nNumStyles++;

																					char buffer[48]; // need fewer;
																					long timeofthing = (long)dblNumber; 
																					tm* theTime = gmtime( &timeofthing );
																					strftime( buffer, 40, "%a", theTime );
																					szParam.Format("%s", buffer); 
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_pnucleus->m_data.m_timebTick.time - (g_pnucleus->m_data.m_timebTick.timezone*60) +(g_pnucleus->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoWaex')")==0)
																				{ //i.e. Monday, but:
																					//if later today
																					//{ if > 6pm "tonight" else "later" }
																					// else if tomorrow "tomorrow" 
																					// else if > 1 week from now "coming"
																					// else "Monday" style day of week

																					unsigned long ulNow = (unsigned long)(g_pnucleus->m_data.m_timebTick.time - (g_pnucleus->m_data.m_timebTick.timezone*60) +(g_pnucleus->m_data.m_timebTick.dstflag?3600:0)); // local time....

																					if((ulNow/86400) == (((unsigned long)dblNumber)/86400)) // today
																					{
																						if((ulTime/3600)>17) // tonight, 6pm or later
																						{
																							szParam = "Tonight";
																						}
																						else
																						{
																							szParam = "Later";
																						}
																					}
																					else
																					if((ulNow/86400) == ((((unsigned long)dblNumber)/86400)-1)) // tomorrow
																					{
																						szParam = "Tomorrow";
																					}
																					else
																					if((ulNow/86400) <= ((((unsigned long)dblNumber)/86400)-7)) // next week
																					{
																						szParam = "Coming";
																					}
																					else
																					{
																						char buffer[48]; // need fewer;
																						long timeofthing = (long)dblNumber; 
																						tm* theTime = gmtime( &timeofthing );
																						strftime( buffer, 40, "%a", theTime );
																						szParam.Format("%s", buffer); 
																					}

																					nNumStyles++;
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_pnucleus->m_data.m_timebTick.time - (g_pnucleus->m_data.m_timebTick.timezone*60) +(g_pnucleus->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "appendAMPM('lcase')")==0)
																				{
																					szParam = szTemp;
																					nNumStyles++;
																					szTemp.Format("%s %s", szParam, bPM?"pm":"am");
																				}
																				else 
																				if(stricmp(paction, "appendAMPM('ucase')")==0)
																				{
																					szParam = szTemp;
																					nNumStyles++;
																					szTemp.Format("%s %s", szParam, bPM?"PM":"AM");
																				}
	//else not nNumStyles++; which is why i had to do it that annoying way.


	/*
	Round,roundNumber()
	Truncate,truncateNumber()
	Filter Smut,smutFilter()
	Truncate To 20 Characters,truncateText(20)
	Truncate To 50 Characters,truncateText(50)
	Truncate To 100 Characters,truncateText(100)
*/

																				else 
																				if(stricmp(paction, "roundNumber()")==0)
																				{
																					dblNumber+= 0.5;
																					nNumStyles++;
																					szTemp.Format("%d", (int)dblNumber);
																				}
																				else 
																				if(stricmp(paction, "truncateNumber()")==0)
																				{
																					nNumStyles++;
																					szTemp.Format("%d", (int)dblNumber);
																				}
																				else 
																				if((strnicmp(paction, "truncateText(", 13)==0)&&(strlen(paction)>13))
																				{
																					int nTruncLen = atoi(paction+13);
																					nNumStyles++;
																					szParam = szTemp.Left(nTruncLen);
																					szTemp = szParam;
																				}

/*
	Convert All Text To Upper Case,textCase('upper')
	Convert All Text To Lower Case,textCase('lower')
	Convert First Letter Of First Word To Upper Case,textCase('sentence')
	Convert First Letter Of Text String To Upper Case,textCase('title')
*/
																				else 
																				if((strnicmp(paction, "textCase(", 9)==0)&&(strlen(paction)>9))
																				{
																					int nL=0;
																					bool bSpace = true;
																					char ch;
																					szParam = "";

																					while(nL<szTemp.GetLength())
																					{
																						ch = szTemp.GetAt(nL);
																						switch(*(paction+9))
																						{
																						case 'u':
																							{
																								szParam += toupper(ch);
																							} break;
																						case 'l':
																							{
																								szParam += tolower(ch);
																							} break;
																						case 's':
																							{
																								if((!(isspace(ch)))&&(bSpace))
																								{
																									bSpace = false;
																									szParam += toupper(ch);
																								}
																								else
																								{
																									szParam += ch;
																								}
																							} break;
																						case 't':
																							{
																								if(isspace(ch))
																								{
																									bSpace = true;
																									szParam += ch;
																								}
																								else
																								{
																									if(bSpace)
																									{
																										szParam += toupper(ch);
																									}
																									else
																									{
																										szParam += ch;
																									}
																									bSpace = false;
																								}
																							} break;
																						}
																						nL++;
																					}
																					nNumStyles++;
																					szTemp = szParam;
																				}


	/*
	Long Date Lower Case,formatDate('longdate', 'lcase')
	Long Date Upper Case,formatDate('longdate', 'ucase')
	Short Date Lower Case,formatDate('shortdate', 'lcase')
	Short Date Upper Case,formatDate('shortdate', 'ucase')
	Celsius,formatTemp('celsius')
	Fahrenheit,formatTemp('fahrenheit')
	*/
																				else 
																				if(stricmp(paction, "formatTemp('celsius')")==0)
																				{
																					nNumStyles++;
																					szTemp.Format("%d�C", (int)dblNumber);
																				}
																				else 
																				if(stricmp(paction, "formatTemp('fahrenheit')")==0)
																				{
																					nNumStyles++;
																					szTemp.Format("%d�F", (int)dblNumber);
																				}
																			
																				paction = sbu.Token(NULL, NULL, "|");
																			}
																			if(bTime) szValue += szTemp;
																		}
																		prsParam->Close();
																		delete prsParam;
																		prsParam = NULL;

																	}

																	if(nNumStyles==0)
																	{
																		if(bTime)
																		{
																			// format it the default way H:MM 24 hour clock
																			szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																			szValue += szTemp;
																		}
																		else 
																		{
																			szValue += szTemp;
																		}
																	}
																}
																else
																{
																	// if no style, need to understand if this is a time.
																	if(bTime)
																	{
																		// format it the default way H:MM 24 hour clock
																		szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																		szValue += szTemp;
																	}
																	else 
																	{
																		szValue += szTemp;
																	}
																}
															}
														}
														if(prsParam)
														{
															prsParam->Close();
															delete prsParam;
														}
													}
													else
													{
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "NEAR � returned NULL");  Sleep(50);//(Dispatch message)

													}
												}
											}
											else
											{
												szValue += ch;
												i++;
											}
										}
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "styles done");  Sleep(50); //(Dispatch message)

										if((!pChannelObj->m_bNearEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
										{
										if(nTriggerItemID<=0)
										{
											if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
											{
											//insert
												char* chValue = dbSet.EncodeQuotes(szValue);
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, nucleus_eventid, Event_ActionID, value, flags, analysis_id) values \
(%d, %d, %d, '%s', %d, %d)",
													pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nNucleusEventItemID, nEventActionID, 
													chValue?chValue:szValue, 
													ulFlags
												);
												if(chValue) free(chValue);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_ANALYZE) 	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug2", "near SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing near SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}
												//else break;
											}
										}
										else
										{
											// update
											if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
											{
												char* chValue = dbSet.EncodeQuotes(szValue);
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, nucleus_eventid = %d, Event_ActionID = %d, value = '%s', flags = %d, analysis_id = %d WHERE analyzed_trigger_id = %d",
														pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
														nAutomationItemID, nNucleusEventItemID, nEventActionID, 
														chValue?chValue:szValue, 
														ulFlags, nAnalysisID, nTriggerItemID
													);
												if(chValue) free(chValue);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_ANALYZE) 	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "near SQL: %s", szSQL);  //Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing near SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}
											}
										}
										}
										else
										{
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_ANALYZE) 	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "if((!pnucleus->m_data.m_bNearEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)) = true");  Sleep(50);
										}
									}
									else
									{
										// just set to renanalyzed
										ulFlags |= NUCLEUS_FLAG_REANALYZED;
//										ulFlags = pChannelObj->m_nAnalysisCounter;

										if((!pChannelObj->m_bNearEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
										if(nTriggerItemID<=0)
										{
											if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
											{
											//insert
												char* chValue = dbSet.EncodeQuotes(szValue);
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, nucleus_eventid, Event_ActionID, value, flags, analysis_id) values \
(%d, %d, %d, '%s', %d, %d)",
													pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nNucleusEventItemID, nEventActionID, 
													chValue?chValue:szValue,
													ulFlags, nAnalysisID
												);
												if(chValue) free(chValue);
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing near SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}// else break;
											}
										}
										else
										{
											// update
											char* chValue = dbSet.EncodeQuotes(szValue);
											if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, nucleus_eventid = %d, Event_ActionID = %d, value = '%s', flags = %d, analysis_id=%d WHERE analyzed_trigger_id = %d",
														pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
														nAutomationItemID, nNucleusEventItemID, nEventActionID, 
														chValue?chValue:szValue,
														ulFlags, nAnalysisID, nTriggerItemID
													);
												if(chValue) free(chValue);
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing near SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}
											}
										}

									}
								}
						
						if((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
						{
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Analysis moving next");  Sleep(50); //(Dispatch message)
							prs->MoveNext();
						}
					} //while(!prs->IsEOF())
					prs->Close();
					delete prs;
					prs = NULL;
					if(nNumRec==0)
					{
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Analysis waiting");  Sleep(50); //(Dispatch message)
						// no records. just wait a few seconds before hitting the db again.
						int ixi=0;
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_ANALYZE) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "No analysis items, waiting for changes"); //(Dispatch message)
						while((!pChannelObj->m_bNearEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)&&(ixi<5000))  // max of 5 second pause on no records.
						{
	_ftime(&pChannelObj->m_timebNearTick); // the last time check inside the thread
							Sleep(1);
							ixi++;
						}
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Analysis finished waiting");  Sleep(50); //(Dispatch message)
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_ANALYZE) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "finished waiting for changes after no analyzed items found X %d %d %d", pnucleus->m_data.m_bNearEventsChanged, g_bKillThread, pChannelObj->m_bKillAutomationThread); //(Dispatch message)
					}

				}//if(prs != NULL)
				else
				{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Near analysis resultset was null!"); // Sleep(50);//(Dispatch message)
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
	{
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Analysis ERROR for %s:%d:  %s",pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum, errorstring); // Sleep(50);//(Dispatch message)
	}
	else
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
	{
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Analysis ERROR for Omnibus stream %d:  %s",pChannelObj->m_nChannelID, errorstring); // Sleep(50);//(Dispatch message)
	}

				}

			}
		} //		if(
//			  (pnucleus->m_data.m_nIndexAutomationEndpoint>=0)
//			&&(pnucleus->m_settings.m_nNumEndpointsInstalled>0)
///			&&(pnucleus->m_settings.m_ppEndpointObject)
//			&&(pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint])
//			&&(!pnucleus->m_data.m_bProcessSuspended)
//			&&(pnucleus->m_data.m_key.m_bValid)  // must have a valid license
//			&&(
//					(!pnucleus->m_data.m_key.m_bExpires)
//				||((pnucleus->m_data.m_key.m_bExpires)&&(!pnucleus->m_data.m_key.m_bExpired))
//				||((pnucleus->m_data.m_key.m_bExpires)&&(pnucleus->m_data.m_key.m_bExpireForgiveness)&&(pnucleus->m_data.m_key.m_ulExpiryDate+pnucleus->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
//				)
//			&&(
//					(!pnucleus->m_data.m_key.m_bMachineSpecific)
//				||((pnucleus->m_data.m_key.m_bMachineSpecific)&&(pnucleus->m_data.m_key.m_bValidMAC))
//				)
//			)

		//Sleep(1); // dont peg processor
	} // while((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
	
	Sleep(60);
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending analysis thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	}
	else
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending analysis thread for Omnibus stream %d", pChannelObj->m_nChannelID);
	}
	g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusAnalysisThread", errorstring);    //(Dispatch message)

	pChannelObj->m_bNearAnalysisThreadStarted=false;
	_endthread();
	Sleep(150);

}

#endif //#ifndef NOT_USING_NEAR_ANALYSIS




void NucleusGlobalAutomationThread(void* pvArgs)
{
	CNucleusMain* pnucleus = (CNucleusMain*) pvArgs;
	if(pnucleus==NULL) return;

	pnucleus->m_data.m_bAutomationThreadStarted=true;

	g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusGlobalAutomationThread", "Beginning global automation analysis thread");    //(Dispatch message)

	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");
	CDBUtil db;

	CDBconn* pdbConn = db.CreateNewConnection(pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:automation_database_connect", errorstring);  //(Dispatch message)
			pdbConn = pnucleus->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW); 
		pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:automation_database_init", errorstring);  //(Dispatch message)
		pdbConn = pnucleus->m_data.m_pdbConn;

		//**MSG
	}


	if((pdbConn)&&(pdbConn->m_bConnected))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", //HARDCODE
			((pnucleus->m_settings.m_pszAnalysis)&&(strlen(pnucleus->m_settings.m_pszAnalysis)))?pnucleus->m_settings.m_pszAnalysis:"Analysis");


		if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//**MSG
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

		}
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", //HARDCODE
			((pnucleus->m_settings.m_pszAnalyzedTriggerData)&&(strlen(pnucleus->m_settings.m_pszAnalyzedTriggerData)))?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData");


		if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//**MSG
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

		}
	}



	bool bChangesPending = false;
	_timeb timebPending;
	_timeb timebChange;
	_ftime(&timebChange);
	timebChange.time -= (pnucleus->m_settings.m_nAnalyzeRulesIntervalMS/1000)+1;
	bool bLastProcessSuspended = 	pnucleus->m_data.m_bProcessSuspended;
	bool bTiming = false;

	while(!g_bKillThread)
	{
		// check automation list for changes (mod)
		bool bChanges = false;
		bool bForceTiming = false;

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%d %d %d %d %d.%03d", pnucleus->m_data.m_nIndexAutomationEndpoint, pnucleus->m_settings.m_nNumEndpointsInstalled, pnucleus->m_settings.m_ppEndpointObject, pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint], pnucleus->m_data.m_timebTick.time, pnucleus->m_data.m_timebTick.millitm);   Sleep(50);//(Dispatch message)

		if(
			  (!pnucleus->m_data.m_bProcessSuspended)
			&&(pnucleus->m_settings.m_ppEndpointObject)
			&&(pnucleus->m_data.m_nIndexAutomationEndpoint>=0)
			&&(pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint])
			&&(pnucleus->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!pnucleus->m_data.m_key.m_bExpires)
				||((pnucleus->m_data.m_key.m_bExpires)&&(!pnucleus->m_data.m_key.m_bExpired))
				||((pnucleus->m_data.m_key.m_bExpires)&&(pnucleus->m_data.m_key.m_bExpireForgiveness)&&(pnucleus->m_data.m_key.m_ulExpiryDate+pnucleus->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!pnucleus->m_data.m_key.m_bMachineSpecific)
				||((pnucleus->m_data.m_key.m_bMachineSpecific)&&(pnucleus->m_data.m_key.m_bValidMAC))
				)

			)
		{

			if(bLastProcessSuspended != pnucleus->m_data.m_bProcessSuspended)
			{

				bLastProcessSuspended = pnucleus->m_data.m_bProcessSuspended;
				if(!bLastProcessSuspended)
				{
					// we've resumed so clear the analysis table.
					if((pdbConn)&&(pdbConn->m_bConnected))
					{
						char szSQL[DB_SQLSTRING_MAXLEN];
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", //HARDCODE
							((pnucleus->m_settings.m_pszAnalysis)&&(strlen(pnucleus->m_settings.m_pszAnalysis)))?pnucleus->m_settings.m_pszAnalysis:"Analysis");


						if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
							//**MSG
	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

						}
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", //HARDCODE
							((pnucleus->m_settings.m_pszAnalyzedTriggerData)&&(strlen(pnucleus->m_settings.m_pszAnalyzedTriggerData)))?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData");


						if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
							//**MSG
	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

						}
					}

				}
			}


			CNucleusEndpointObject* pAutoObj = pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint];

	_ftime(&pnucleus->m_data.m_timebAutomationTick); // the last time check inside the thread

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "*** Timing check 01: %d", clock()); // Sleep(250); //(Dispatch message)


			if(
				  (pAutoObj->m_pszLiveEvents)&&(strlen(pAutoObj->m_pszLiveEvents))
				&&(// only do on interval
				    (pnucleus->m_data.m_timebTick.time > (timebChange.time+pnucleus->m_settings.m_nAnalyzeRulesIntervalMS/1000))
					||(
					    (pnucleus->m_data.m_timebTick.time==(timebChange.time+pnucleus->m_settings.m_nAnalyzeRulesIntervalMS/1000))
						&&(pnucleus->m_data.m_timebTick.millitm>(timebChange.millitm+pnucleus->m_settings.m_nAnalyzeRulesIntervalMS%1000))
						)
					)
				)
			{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Checking %s", pAutoObj->m_pszLiveEvents);   Sleep(50);//(Dispatch message)
				pAutoObj->m_nModLiveEvents = pAutoObj->CheckDatabaseMod(&db, pdbConn, pAutoObj->m_pszLiveEvents);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Checked mod %s = %d, %d last", pAutoObj->m_pszLiveEvents, pAutoObj->m_nModLiveEvents, pAutoObj->m_nLastModLiveEvents);   Sleep(50);//(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "*** Timing check 02: %d", clock()); // Sleep(250); //(Dispatch message)


				if((pAutoObj->m_nModLiveEvents>=0)&&(pAutoObj->m_nModLiveEvents!=pAutoObj->m_nLastModLiveEvents))
				{
					pAutoObj->m_nLastModLiveEvents = pAutoObj->m_nModLiveEvents;
					pnucleus->m_data.m_dblLastAutomationChange = (double)pnucleus->m_data.m_timebAutomationTick.time + ((double)pnucleus->m_data.m_timebAutomationTick.millitm)/1000.0;
					bChanges = true;

					if(!bChangesPending)
					{
						_ftime(&timebChange); // initialize this
					}

					bChangesPending = true;
					_ftime(&timebPending);
					timebPending.time += pnucleus->m_settings.m_nAnalyzeRulesDwellMS/1000;
					timebPending.millitm += pnucleus->m_settings.m_nAnalyzeRulesDwellMS%1000;
					if(timebPending.millitm>999)
					{
						timebPending.time++;
						timebPending.millitm -= 1000;
					}
					if(bTiming) bForceTiming=true;
				}
			}

	
			_ftime( &pnucleus->m_data.m_timebTick );  // we're still alive.

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "*** Timing check 03: %d", clock()); // Sleep(250); //(Dispatch message)
				// immediate changes.
			if((bChanges)&&(!bForceTiming))  //added bForceTiming=true; 2.2.1.22, otherwise we skip the force if very busy.
			{
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PROCESS) 	
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Automation changes exist %d", pAutoObj->m_nModLiveEvents);  // Sleep(50);//(Dispatch message)
				bChanges = false;
			}
			else
			{
				// if no changes, and no changes pending, check elapsed time and if expired, run the timing col queries again.
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PROCESS) 	
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "No changes exist %d, force:%d, pending:%d, timing:%d. now:%d.%03d, pending:%d.%03d", pAutoObj->m_nModLiveEvents, bForceTiming, bChangesPending, bTiming,pnucleus->m_data.m_timebTick.time,pnucleus->m_data.m_timebTick.millitm, timebPending.time, timebPending.millitm);  // Sleep(50);//(Dispatch message)
				if(
						(bForceTiming)
					||(
							(!bChangesPending)
						&&(bTiming)
						&&(
								(pnucleus->m_data.m_timebTick.time>timebPending.time)
							||(
									(pnucleus->m_data.m_timebTick.time==timebPending.time)
								&&(pnucleus->m_data.m_timebTick.millitm>timebPending.millitm)
								)
							)
						)
					)
				{
					bForceTiming=false;

					EnterCriticalSection(&pnucleus->m_data.m_critEventRules);
					int nEventRuleRecord = 0; \
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "executing %d event rule query sets: %d", pnucleus->m_data.m_nNumEventRuleQueries); // Sleep(250); //(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "*** Timing check 04: %d", clock()); // Sleep(250); //(Dispatch message)

if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TIMING) 	
{ g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "TIMING col array %d, num %d", pnucleus->m_data.m_ppszTimingColName, pnucleus->m_data.m_nNumTimingColNames); }

					while((pnucleus->m_data.m_ppszTimingColName)&&(nEventRuleRecord<pnucleus->m_data.m_nNumTimingColNames)&&(!g_bKillThread))
					{
_ftime(&pnucleus->m_data.m_timebAutomationTick); // the last time check inside the thread
						if(pnucleus->m_data.m_ppszTimingColName[nEventRuleRecord])
						{
//									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", pnucleus->m_data.m_ppszTimingColName[nEventRuleRecord]->sz_query1);
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "update %s.dbo.%s set trigger_time = cast(TriggerTimingView.%s as decimal(20,3)) \
+ cast(ActionTriggerTime/1000.00 as decimal(20,3)) \
from %s.dbo.%s as AnalyzedTriggerData, (select * from %s.dbo.TriggerTimingView where nucleus_Timing_Col_Name = '%s') as TriggerTimingView \
where AnalyzedTriggerData.automation_event_itemid = TriggerTimingView.itemid and TriggerTimingView.event_actionid = AnalyzedTriggerData.event_actionid", 

									(g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus"),
									(g_pnucleus->m_settings.m_pszAnalyzedTriggerData?g_pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"),
									*(pnucleus->m_data.m_ppszTimingColName[nEventRuleRecord]),


									(g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus"),
									(g_pnucleus->m_settings.m_pszAnalyzedTriggerData?g_pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"),

									(g_pnucleus->m_settings.m_pszCortexDB?g_pnucleus->m_settings.m_pszCortexDB:"Cortex"),
			//						(g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus"),
			//						(g_pnucleus->m_settings.m_pszTriggerAnalysisView?g_pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView"),
									*(pnucleus->m_data.m_ppszTimingColName[nEventRuleRecord])//,


			//						(g_pnucleus->m_settings.m_pszCortexDB?g_pnucleus->m_settings.m_pszCortexDB:"Cortex"),
			//						(g_pnucleus->m_settings.m_pszLiveEventData?g_pnucleus->m_settings.m_pszLiveEventData:"LiveEventData")
									
									);

//, %s.dbo.%s as LiveEventData
//triggeranalysisview.analyzed_trigger_id = AnalyzedTriggerData.analyzed_trigger_id and \ 


/*   update nucleus.dbo.AnalyzedTriggerData set trigger_time = cast(LiveEventData.[replace with col_name] as decimal(20,3))\
+ cast(ActionTriggerTime/1000.00 as decimal(20,3)) from \
(select * from nucleus.dbo.triggeranalysisview where nucleus_Timing_Col_Name = '[replace with col_name]') as triggeranalysisview, \
nucleus.dbo.AnalyzedTriggerData as AnalyzedTriggerData, cortex.dbo.LiveEventData as LiveEventData \
where triggeranalysisview.analyzed_trigger_id = AnalyzedTriggerData.analyzed_trigger_id and AnalyzedTriggerData.automation_event_itemid = LiveEventData.itemid 
*/

if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TIMING) 	
{ g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "TIMING SQL [%d]: %s", nEventRuleRecord, szSQL); }

							EnterCriticalSection(&pnucleus->m_data.m_critSQL);
							if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
							{
			pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR, retrying SQL: %s", errorstring); // Sleep(250); //(Dispatch message)
								Sleep(50); // try again.

								if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
								{
											//**MSG
			pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

								}
							}
							LeaveCriticalSection(&pnucleus->m_data.m_critSQL);
							_ftime(&timebPending);
							timebPending.time += pnucleus->m_settings.m_nAnalyzeTimingDwellMS/1000;
							timebPending.millitm += pnucleus->m_settings.m_nAnalyzeTimingDwellMS%1000;


						}
						nEventRuleRecord++;
					}
 					LeaveCriticalSection(&pnucleus->m_data.m_critEventRules);
					// moved here from below.
					bTiming = false;
					pnucleus->m_data.m_bNearEventsChanged = true; // kicks off parameter analysis thread

if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PROCESS) 	
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_automation", "Signalled event change, now:%d.%03d", pnucleus->m_data.m_timebTick.time,pnucleus->m_data.m_timebTick.millitm);  // Sleep(50);//(Dispatch message)

				}

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "*** Timing check 05: %d", clock()); // Sleep(250); //(Dispatch message)

			}

if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PROCESS) 	
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Mod %d, force:%d, pending:%d, timing:%d. now:%d.%03d, pending:%d.%03d, analyze:%d.%03d", pAutoObj->m_nModLiveEvents, bForceTiming, bChangesPending, bTiming,
	pnucleus->m_data.m_timebTick.time,pnucleus->m_data.m_timebTick.millitm, 
	timebPending.time, timebPending.millitm,
	(timebChange.time+pnucleus->m_settings.m_nAnalyzeRulesForceMS/1000),(timebChange.millitm+pnucleus->m_settings.m_nAnalyzeRulesForceMS%1000) );  // Sleep(50);//(Dispatch message)
			// dwell changes - another change will reset the time, suppressing the previous change
			if(
					(	pnucleus->m_data.m_bForceAnalysis)
				||(
						(bChangesPending)
					&&(
							(pnucleus->m_data.m_timebTick.time>timebPending.time)
						||(
								(pnucleus->m_data.m_timebTick.time==timebPending.time)
							&&(pnucleus->m_data.m_timebTick.millitm>timebPending.millitm)
							)
						||(pnucleus->m_data.m_timebTick.time > (timebChange.time+pnucleus->m_settings.m_nAnalyzeRulesForceMS/1000))
						||(
								(pnucleus->m_data.m_timebTick.time==(timebChange.time+pnucleus->m_settings.m_nAnalyzeRulesForceMS/1000))
							&&(pnucleus->m_data.m_timebTick.millitm>(timebChange.millitm+pnucleus->m_settings.m_nAnalyzeRulesForceMS%1000))
							)
						)
					)
				)
			{
				pnucleus->m_data.m_bForceAnalysis = false;
        // do this AFTER the queries have run
//				pnucleus->m_data.m_bNearEventsChanged = true; // kicks off parameter analysis thread
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Automation changes exist %d (BUT WERE PENDING)", pAutoObj->m_nModLiveEvents);  // Sleep(50);//(Dispatch message)
				// pnucleus->m_data.m_nEventLastMax = 0; // start at the beginning....
// actually dont reset this.
				bChangesPending = false;
				bool bSQLError = false;
				// if changes
				// 
				//   Close the m_prsEvents recordset and delete it, set it back to NULL.

//				pnucleus->m_data.ReleaseRecordSet();

				if(
					  ((pAutoObj->m_usType&NUCLEUS_DEP_AUTO_MASK)==NUCLEUS_DEP_AUTO_HELIOS)
					||((pAutoObj->m_usType&NUCLEUS_DEP_AUTO_MASK)==NUCLEUS_DEP_AUTO_SENTINEL)
					)
				{
				// run the rules procedure

//		EnterCriticalSection(&pnucleus->m_data.m_critSQL);
/*
					if(db.ExecuteSQL(pdbConn, 
						g_pnucleus->m_settings.m_pszAnalyzeRulesProc?g_pnucleus->m_settings.m_pszAnalyzeRulesProc:"spAnalyzeRules", 
						errorstring)<DB_SUCCESS)
					{
						bSQLError = true;
								//**MSG
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

					}
*/

					g_pnucleus->m_data.m_bRunningAutomationRulesUpdate = true;

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET status = 0",
						(g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus"),
						(g_pnucleus->m_settings.m_pszAnalysis?g_pnucleus->m_settings.m_pszAnalysis:"Analysis")
						);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "EVENT rule SQL: %s", szSQL); // Sleep(250); //(Dispatch message)

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "*** Timing check 10: %d", clock()); // Sleep(250); //(Dispatch message)
					EnterCriticalSection(&pnucleus->m_data.m_critSQL);
					if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						bSQLError = true;
								//**MSG
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

					}
					LeaveCriticalSection(&pnucleus->m_data.m_critSQL);

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "*** Timing check 11: %d", clock()); // Sleep(250); //(Dispatch message)

					if(!bSQLError)
					{
						EnterCriticalSection(&pnucleus->m_data.m_critEventRules);
						int nEventRuleRecord = 0;
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "executing %d event rule query sets: %d", pnucleus->m_data.m_nNumEventRuleQueries); // Sleep(250); //(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "*** Timing check 12: %d", clock()); // Sleep(250); //(Dispatch message)
						while((pnucleus->m_data.m_ppEventRuleQuery)&&(nEventRuleRecord<pnucleus->m_data.m_nNumEventRuleQueries)&&(!g_bKillThread))
						{
	_ftime(&pnucleus->m_data.m_timebAutomationTick); // the last time check inside the thread
							if(pnucleus->m_data.m_ppEventRuleQuery[nEventRuleRecord])
							{
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", pnucleus->m_data.m_ppEventRuleQuery[nEventRuleRecord]->sz_query1);

if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_EVENTRULE) 	
{ g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:eventrule", "EVENT rule SQL [%d]#1: %s", nEventRuleRecord, szSQL);}

								EnterCriticalSection(&pnucleus->m_data.m_critSQL);
								if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
								{
									bSQLError = true;
											//**MSG
			pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

								}
								else
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", pnucleus->m_data.m_ppEventRuleQuery[nEventRuleRecord]->sz_query2);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_EVENTRULE) 	
{ g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:eventrule", "EVENT rule SQL [%d]#2: %s", nEventRuleRecord, szSQL);}

									if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
									{
										bSQLError = true;
												//**MSG
				pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  //Sleep(250); //(Dispatch message)

									}
								}
								LeaveCriticalSection(&pnucleus->m_data.m_critSQL);
							}
							nEventRuleRecord++;
							Sleep(pnucleus->m_settings.m_nInterRulesDelayMS);
						}
 						LeaveCriticalSection(&pnucleus->m_data.m_critEventRules);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "*** Timing check 13: %d", clock()); // Sleep(250); //(Dispatch message)

						if(!bSQLError)
						{

							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s where status = 0",
									(g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus"),
									(g_pnucleus->m_settings.m_pszAnalysis?g_pnucleus->m_settings.m_pszAnalysis:"Analysis")
								);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "EVENT rule SQL: %s", szSQL); // Sleep(250); //(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "*** Timing check 14: %d", clock()); // Sleep(250); //(Dispatch message)
							EnterCriticalSection(&pnucleus->m_data.m_critSQL);
							if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
							{
								bSQLError = true;
										//**MSG
		pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  //Sleep(250); //(Dispatch message)

							}
							LeaveCriticalSection(&pnucleus->m_data.m_critSQL);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "*** Timing check 15: %d", clock()); // Sleep(250); //(Dispatch message)

							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s where cast(nucleus_eventid as varchar(32)) \
+ cast(automation_event_itemid as varchar(32)) not in (select cast(nucleus_eventid as varchar(32)) + cast(automation_event_itemid as varchar(32)) \
from %s.dbo.%s)",
									(g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus"),
									(g_pnucleus->m_settings.m_pszAnalyzedTriggerData?g_pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"),
									(g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus"),
									(g_pnucleus->m_settings.m_pszAnalysis?g_pnucleus->m_settings.m_pszAnalysis:"Analysis")
								);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "EVENT rule SQL: %s", szSQL); // Sleep(250); //(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "*** Timing check 16: %d", clock()); // Sleep(250); //(Dispatch message)
							EnterCriticalSection(&pnucleus->m_data.m_critSQL);
							if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
							{
								bSQLError = true;
										//**MSG
		pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  //Sleep(250); //(Dispatch message)

							}
							LeaveCriticalSection(&pnucleus->m_data.m_critSQL);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "*** Timing check 17: %d", clock()); // Sleep(250); //(Dispatch message)


							EnterCriticalSection(&pnucleus->m_data.m_critEventRules);
							nEventRuleRecord = 0; //re-use
	//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "executing %d event rule query sets: %d", pnucleus->m_data.m_nNumEventRuleQueries); // Sleep(250); //(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "*** Timing check 18: %d", clock()); // Sleep(250); //(Dispatch message)
							while((pnucleus->m_data.m_ppszTimingColName)&&(nEventRuleRecord<pnucleus->m_data.m_nNumTimingColNames)&&(!g_bKillThread))
							{
		_ftime(&pnucleus->m_data.m_timebAutomationTick); // the last time check inside the thread
								if(pnucleus->m_data.m_ppszTimingColName[nEventRuleRecord])
								{


									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "update %s.dbo.%s set trigger_time = cast(TriggerTimingView.%s as decimal(20,3)) \
+ cast(ActionTriggerTime/1000.00 as decimal(20,3)) \
from %s.dbo.%s as AnalyzedTriggerData, (select * from %s.dbo.TriggerTimingView where nucleus_Timing_Col_Name = '%s') as TriggerTimingView \
where AnalyzedTriggerData.automation_event_itemid = TriggerTimingView.itemid and TriggerTimingView.event_actionid = AnalyzedTriggerData.event_actionid", 

											(g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus"),
											(g_pnucleus->m_settings.m_pszAnalyzedTriggerData?g_pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"),
											*pnucleus->m_data.m_ppszTimingColName[nEventRuleRecord],


											(g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus"),
											(g_pnucleus->m_settings.m_pszAnalyzedTriggerData?g_pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"),

											(g_pnucleus->m_settings.m_pszCortexDB?g_pnucleus->m_settings.m_pszCortexDB:"Cortex"),
					//						(g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus"),
					//						(g_pnucleus->m_settings.m_pszTriggerAnalysisView?g_pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView"),
											*pnucleus->m_data.m_ppszTimingColName[nEventRuleRecord]//,


					//						(g_pnucleus->m_settings.m_pszCortexDB?g_pnucleus->m_settings.m_pszCortexDB:"Cortex"),
					//						(g_pnucleus->m_settings.m_pszLiveEventData?g_pnucleus->m_settings.m_pszLiveEventData:"LiveEventData")
											
											);


/*
//									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", pnucleus->m_data.m_ppszTimingColName[nEventRuleRecord]->sz_query1);
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "update %s.dbo.%s set trigger_time = cast(LiveEventData.%s as decimal(20,3)) \
+ cast(ActionTriggerTime/1000.00 as decimal(20,3)) from \
(select * from %s.dbo.%s where nucleus_Timing_Col_Name = '%s') as triggeranalysisview, \
%s.dbo.%s as AnalyzedTriggerData, %s.dbo.%s as LiveEventData \
where triggeranalysisview.analyzed_trigger_id = AnalyzedTriggerData.analyzed_trigger_id and \
AnalyzedTriggerData.automation_event_itemid = LiveEventData.itemid", 

											(g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus"),
											(g_pnucleus->m_settings.m_pszAnalyzedTriggerData?g_pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"),
											*pnucleus->m_data.m_ppszTimingColName[nEventRuleRecord],

											(g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus"),
											(g_pnucleus->m_settings.m_pszTriggerAnalysisView?g_pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView"),
											*pnucleus->m_data.m_ppszTimingColName[nEventRuleRecord],

											(g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus"),
											(g_pnucleus->m_settings.m_pszAnalyzedTriggerData?g_pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"),

											(g_pnucleus->m_settings.m_pszCortexDB?g_pnucleus->m_settings.m_pszCortexDB:"Cortex"),
											(g_pnucleus->m_settings.m_pszLiveEventData?g_pnucleus->m_settings.m_pszLiveEventData:"LiveEventData")
											
											);
*/
									
/*   update nucleus.dbo.AnalyzedTriggerData set trigger_time = cast(LiveEventData.[replace with col_name] as decimal(20,3))\
+ cast(ActionTriggerTime/1000.00 as decimal(20,3)) from \
(select * from nucleus.dbo.triggeranalysisview where nucleus_Timing_Col_Name = '[replace with col_name]') as triggeranalysisview, \
nucleus.dbo.AnalyzedTriggerData as AnalyzedTriggerData, cortex.dbo.LiveEventData as LiveEventData \
where triggeranalysisview.analyzed_trigger_id = AnalyzedTriggerData.analyzed_trigger_id and AnalyzedTriggerData.automation_event_itemid = LiveEventData.itemid 
*/

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "TIMING SQL [%d]: %s", nEventRuleRecord, szSQL); // Sleep(250); //(Dispatch message)
									EnterCriticalSection(&pnucleus->m_data.m_critSQL);
									if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
									{
										Sleep(50); // try again.

										if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
										{
											bSQLError = true;
													//**MSG
					pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

										}
									}
									LeaveCriticalSection(&pnucleus->m_data.m_critSQL);
									_ftime(&timebPending);
									timebPending.time += pnucleus->m_settings.m_nAnalyzeTimingDwellMS/1000;
									timebPending.millitm += pnucleus->m_settings.m_nAnalyzeTimingDwellMS%1000;


								}
								nEventRuleRecord++;
							}
 							LeaveCriticalSection(&pnucleus->m_data.m_critEventRules);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "*** Timing check 19: %d", clock()); // Sleep(250); //(Dispatch message)
						}

					}
					g_pnucleus->m_data.m_bRunningAutomationRulesUpdate = false;


//		LeaveCriticalSection(&pnucleus->m_data.m_critSQL);

/*
					if(!bSQLError) 
					{
						pAutoObj->m_nLastModLiveEvents = pAutoObj->m_nModLiveEvents;
						pnucleus->m_data.m_nEventLastMax=pnucleus->m_data.m_nNumberOfEvents-1; // in case the automation changes reduce the number of events, we still want to analyze below.
					}
*/
					// Sleep(1); 
				}  // type was sentinel
				// else automation type unknown, so nothing

				// moved here from above, to trigger analysis thread AFTER the queries have run.
//				pnucleus->m_data.m_bNearEventsChanged = true; // kicks off parameter analysis thread
				// actually realized, want to move this to after the timing queries.

				bTiming = true; 

			} // automation changes exist
/*
/////////////////////////////////////////////////////////
			else  // no automation changes
			{

				//DEMO
				// just update the time based on estimate
				_timeb timebTempTime; // the harris server time
				_ftime( &timebTempTime); 
				
				pnucleus->m_data.m_dblAutomationTimeEstimate = pnucleus->m_data.m_dblHarrisTime + ((double)(pnucleus->m_data.m_nHarrisTimeDiffMS)/1000.0) + (double)pnucleus->m_settings.m_nTriggerAdvanceMS/1000.0
					+ ((double)timebTempTime.time - (double)pnucleus->m_data.m_timebHarrisCheckTimeLocal.time)
					+ ((double)timebTempTime.millitm/1000.0 - (double)pnucleus->m_data.m_timebHarrisCheckTimeLocal.millitm/1000.0); // the difference beween now and last check
//										pnucleus->m_data.m_dblAutomationTimeEstimate = (double)timebTempTime.time;
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Automation estimate time now %f", pnucleus->m_data.m_dblAutomationTimeEstimate);  Sleep(50);//(Dispatch message)

			}
*/
		} // if license etc


		Sleep(1); // dont peg processor
	} // while((!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
	pnucleus->m_data.m_bAutomationThreadStarted=false;


	if((pdbConn)&&(pdbConn->m_bConnected))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", //HARDCODE
			((pnucleus->m_settings.m_pszAnalysis)&&(strlen(pnucleus->m_settings.m_pszAnalysis)))?pnucleus->m_settings.m_pszAnalysis:"Analysis");


		if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//**MSG
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

		}
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", //HARDCODE
			((pnucleus->m_settings.m_pszAnalyzedTriggerData)&&(strlen(pnucleus->m_settings.m_pszAnalyzedTriggerData)))?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData");


		if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//**MSG
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

		}
	}


	g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusGlobalAutomationThread", "Ending global automation analysis thread");    //(Dispatch message)

	Sleep(50);//(Dispatch message)
	_endthread();
	Sleep(150);

}

/*
void NucleusFarAnalysisThread(void* pvArgs)
{
	CNucleusAutomationChannelObject* pChannelObj = (CNucleusAutomationChannelObject*) pvArgs;
	if(pChannelObj==NULL) return;
	CNucleusEndpointObject* pEndObj = (CNucleusEndpointObject*) pChannelObj->m_pEndpoint;
	if(pEndObj==NULL) return;
	CNucleusMain* pnucleus = (CNucleusMain*) pChannelObj->m_pNucleus;
	if(pnucleus==NULL) return;

	pChannelObj->m_bFarAnalysisThreadStarted=true;
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");
	CDBUtil db;
	CDBUtil dbSet;

	CDBconn* pdbConn = db.CreateNewConnection(pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:analysis_database_connect", errorstring);  //(Dispatch message)
			pdbConn = pnucleus->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW); 
		pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:analysis_database_init", errorstring);  //(Dispatch message)
		pdbConn = pnucleus->m_data.m_pdbConn;

		//**MSG
	}

	CDBconn* pdbConnSet = dbSet.CreateNewConnection(pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW);
	if(pdbConnSet)
	{
		if(dbSet.ConnectDatabase(pdbConnSet, errorstring)<DB_SUCCESS)
		{
			pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:analysis_database2_connect", errorstring);  //(Dispatch message)
			pdbConnSet = pnucleus->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW); 
		pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:analysis_database2_init", errorstring);  //(Dispatch message)
		pdbConnSet = pnucleus->m_data.m_pdbConn;

		//**MSG
	}

	if(pdbConn)
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM Analysis");
		while((db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
		{
		//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)
		}
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
				pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"
			);
		while((db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
		{
		//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)
		}
	}

	double delta = 0.0;
	double wait = 0.0;
	while((!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
	{
		if(
			  (pnucleus->m_data.m_nIndexAutomationEndpoint>=0)
			&&(pnucleus->m_settings.m_nNumEndpointsInstalled>0)
			&&(pnucleus->m_data.m_nIndexAutomationEndpoint<pnucleus->m_settings.m_nNumEndpointsInstalled)
			&&(pnucleus->m_settings.m_ppEndpointObject)
			&&(pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint])
			&&(!pnucleus->m_data.m_bProcessSuspended)
			&&(pnucleus->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!pnucleus->m_data.m_key.m_bExpires)
				||((pnucleus->m_data.m_key.m_bExpires)&&(!pnucleus->m_data.m_key.m_bExpired))
				||((pnucleus->m_data.m_key.m_bExpires)&&(pnucleus->m_data.m_key.m_bExpireForgiveness)&&(pnucleus->m_data.m_key.m_ulExpiryDate+pnucleus->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!pnucleus->m_data.m_key.m_bMachineSpecific)
				||((pnucleus->m_data.m_key.m_bMachineSpecific)&&(pnucleus->m_data.m_key.m_bValidMAC))
				)

			)
		{
//			CNucleusEndpointObject* pAutoObj = pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint];

			//Here, we analyze things that have not yet played, that are not yet analyzed, unless its within m_ulTriggerCueThreshold seconds of play time, then it gets reanalyzed
	_ftime(&pnucleus->m_data.m_timebFarTick); // the last time check inside the thread

			pnucleus->m_data.m_bFarEventsChanged = false;
			while (
							(!pnucleus->m_data.m_bFarEventsChanged)
						&&(pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint]->m_dblLastAutomationChange + () <(pnucleus->m_data.m_timebFarTick.time+ ((double)(pnucleus->m_data.m_timebFarTick.time)) + (double)(pnucleus->m_data.m_timebFarTick.millitm)/1000.0) )
						&&(!g_bKillThread)
						&&(!pAobj->m_bKillAutomationThread)
						)
			{
				// just get the exports with params to parse.
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"SELECT * FROM %s WHERE TriggerTime > %lf AND event_status = 0 AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY TriggerTime ASC",
						pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
						pnucleus->m_data.m_dblAutomationTimeEstimate+(double)pnucleus->m_settings.m_ulTriggerCueThreshold,
						NUCLEUS_FLAG_ANALYZED
					);
	_ftime(&pnucleus->m_data.m_timebFarTick); // the last time check inside the thread

					
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis SQL: %s", szSQL); // Sleep(50);//(Dispatch message)
				CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
				int nNumRec = 0;
				if(prs != NULL)
				{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis resultset not null %d records", prs->GetRecordCount( )); // Sleep(50);//(Dispatch message)

					while((!pnucleus->m_data.m_bFarEventsChanged)&&(!prs->IsEOF())&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
					{
	_ftime(&pnucleus->m_data.m_timebFarTick); // the last time check inside the thread
						nNumRec++;
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis resultset Got %d items so far", nNumRec); // Sleep(50);//(Dispatch message)

						CString szName;
						CString szValue;
						CString szTemp;
						//double dblTime;
						double dblClipTime;
						double dblEventTime;
						int nTriggerItemID=-1;
						int nAutomationItemID=-1;
						int nNucleusEventItemID=-1;
						int nEventActionID=-1;
						int nType=-1;
						int nAnalysisID=-1;
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis getting itemid"); 

						prs->GetFieldValue("itemid", szTemp);
						if(szTemp.GetLength()) nAutomationItemID = atoi(szTemp);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis got itemid %s", szTemp); 

						// Host
						// ParsedValue
					//	prs->GetFieldValue("TriggerTime", szTemp);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis got TriggerTime %s", szTemp); 
						//dblTime=atof(szTemp);
						prs->GetFieldValue("analyzed_trigger_id", szTemp);
						if(szTemp.GetLength()) nTriggerItemID = atoi(szTemp);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis got analyzed_trigger_id %s", szTemp); 

						prs->GetFieldValue("AnalyzedTriggerDataFlags", szTemp);
						unsigned long ulFlags = atol(szTemp);
						//AnalyzedTriggerDataStatus
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis got AnalyzedTriggerDataFlags %s", szTemp); 

						prs->GetFieldValue("nucleus_EventID", szTemp);
						if(szTemp.GetLength()) nNucleusEventItemID = atoi(szTemp);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis got Nucleus_EventID %s", szTemp); 
						//Nucleus_Timing_Col_Name
						//Source
						//Scene
						prs->GetFieldValue("Graphic_Action_Type", szTemp);
						nType = atoi(szTemp);
						//Action_Name

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis got Graphic_Action_Type %s", szTemp); 

						prs->GetFieldValue("Event_ActionID", szTemp);
						if(szTemp.GetLength()) nEventActionID = atoi(szTemp);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis got Event_ActionID %s", szTemp); 

						//Action_Value
						prs->GetFieldValue("Action_Value", szValue);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis got Action_Value %s", szValue); 
					
						//Analysis_ID
						prs->GetFieldValue("Analysis_ID", szTemp);
						if(szTemp.GetLength()) nAnalysisID = atoi(szTemp);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis got Analysis_ID %s", szTemp); 

						prs->GetFieldValue("event_calc_start", szTemp);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis got TriggerTime %s", szTemp); 
						dblClipTime=atof(szTemp);

						prs->GetFieldValue("event_start", szTemp);
						dblEventTime=atof(szTemp);

						bool bCueTime = false;
						/*
						if(pnucleus->m_data.m_dblAutomationTimeEstimate+((double)pnucleus->m_settings.m_ulTriggerCueThreshold)>=dblTime)
						{
							bCueTime = true;
						}
						else
						{
							bCueTime = false;
						}
						* /
	//					if(
	//							((bCueTime)&&(!(ulFlags&NUCLEUS_FLAG_REANALYZED)))
	//						||((!bCueTime)&&(!(ulFlags&NUCLEUS_FLAG_ANALYZED)))
	//						)
						{
								if(nType == 0) // anim
								{
									// just set to 
				//					if(bCueTime)
				//						ulFlags |= NUCLEUS_FLAG_REANALYZED;
				//					else
									ulFlags |= NUCLEUS_FLAG_ANALYZED;
									if(nTriggerItemID<=0)
									{
										if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
										{
											//insert
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, nucleus_eventid, event_actionid, flags, analysis_id) values \
(%d, %d, %d, %d, %d)",
													pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nNucleusEventItemID, nEventActionID, ulFlags, nAnalysisID
												);
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug1", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
											if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
											{
											//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing far SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
											} //else break;

										}
									}
									else
									{
										// update
										if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
										{
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, nucleus_eventid = %d, Event_ActionID = %d, flags = %d, analysis_id=%d WHERE analyzed_trigger_id = %d",
													pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nNucleusEventItemID, nEventActionID, ulFlags, nAnalysisID, nTriggerItemID
												);
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
											if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
											{
											//**MSG
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing far SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
											}
										}
									}
								}
								else
								if(nType == 1) // export
								{
									if((szValue.GetLength())&&(szValue.Find("�")>=0))
									{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "FAR value has a �: %s", szValue);  Sleep(50);//(Dispatch message)
										// we have a least one param to parse
										// just set to renanalyzed
					//					if(bCueTime)
					///						ulFlags |= NUCLEUS_FLAG_REANALYZED;
					//					else
										ulFlags |= NUCLEUS_FLAG_ANALYZED;

										// lets parse out szValue and set it up;

										CString szPreParse = szValue;
										szValue = "";

										bool bInsBrk = false;

										int i=0;
										while((i<szPreParse.GetLength())&&(!pnucleus->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)) 
										{
											char ch = szPreParse.GetAt(i);
											if(ch == '�')
											{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "FAR ch has a �");  Sleep(50);//(Dispatch message)

												CString szParam = "";
												CString szStyle = "";
												i++;
												ch = szPreParse.GetAt(i);

												bool bStyle = false;
												while((ch != '�')&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
												{
													if(!bStyle)
													{
														szParam += ch;
													}
													else
													{
														szStyle += ch;
													}
													i++;
													ch = szPreParse.GetAt(i);
													if(ch == ':')
													{
														i++;
														bStyle = true;
														ch = szPreParse.GetAt(i);
													}
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "FAR Param %s, Style %s", szParam,szStyle);  Sleep(50);//(Dispatch message)


												}
												i++;

												// ok here, we should have the param name and possible style.
												// first lets get the param.
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "FAR FINAL Param %s, Style %s", szParam,szStyle);  Sleep(50);//(Dispatch message)

/*												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s '%s', %f",
														pnucleus->m_settings.m_pszGetParameterValueProc?pnucleus->m_settings.m_pszGetParameterValueProc:"spGetParameterValue",  
														szParam, dblTime
													);
* /
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
/*
												// this sets the param
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
											//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}

												// this gets it!
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM [Output]");
* /
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
			
// try 2 was like this.												
/*												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "{CALL %s('%s', %f)}",
														pnucleus->m_settings.m_pszGetParameterValueProc?pnucleus->m_settings.m_pszGetParameterValueProc:"spGetParameterValue",  
														szParam, dblTime
													);

* /
												
// this is try 3												
												if((!pnucleus->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)) 
												{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far parameter retrieve index for [%s]", szParam);  Sleep(50);//(Dispatch message)
													EnterCriticalSection(&pnucleus->m_data.m_critParameterRules);
													int idx = pnucleus->m_data.GetParameterQueryIndex(szParam);

//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far parameter retrieved index %d", idx);  Sleep(50);//(Dispatch message)
													if(idx>=0)
													{
														CString timestr;  
														if(pnucleus->m_data.m_ppParameterRule[idx]->sz_target_valueid.Compare("0"))
														{
															if(pnucleus->m_settings.m_bFarAnalysisUsesCalc)
																timestr.Format("%.3f", dblClipTime);
															else
																timestr.Format("%.3f", dblEventTime);
															
														}
														else
														{
															if(pnucleus->m_settings.m_bFarAnalysisUsesCalc)
																timestr.Format("%.3f", dblClipTime-0.020);
															else
																timestr.Format("%.3f", dblEventTime-0.020);
														}
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s%s%s",
																pnucleus->m_data.m_ppParameterRuleQuery[idx]->sz_query1,
																timestr,
																pnucleus->m_data.m_ppParameterRuleQuery[idx]->sz_query2
															);
														
//if(pnucleus->m_settings.m_bUseEmail) pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "**** FAR � get param (%s) Query: %s",szParam, szSQL);  Sleep(50);//(Dispatch message)
													}

													LeaveCriticalSection(&pnucleus->m_data.m_critParameterRules);
													
													CRecordset* prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);


//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� SQL ERROR %s", errorstring);  Sleep(50);//(Dispatch message)


/*

												bool bGotit=false;
												CString target_valueid, target_criterion_col_name, criterion_value, returned_property_col_name, default_value;

_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select target_valueid, target_criterion_col_name, criterion_value, returned_property_col_name, [default_value] from [Parameters] where [name] = '%s'",szParam);
//_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from Analysis");

pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
CRecordset* prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);

												if(prsParam != NULL)
												{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� not null");  Sleep(50);//(Dispatch message)
													if((!prsParam->IsEOF())&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
													{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� NEOF");  Sleep(50);//(Dispatch message)
														try
														{
															prsParam->GetFieldValue("target_valueid", target_valueid);
															prsParam->GetFieldValue("target_criterion_col_name", target_criterion_col_name);
															prsParam->GetFieldValue("criterion_value", criterion_value);
															prsParam->GetFieldValue("returned_property_col_name", returned_property_col_name);
															prsParam->GetFieldValue("default_value", default_value);

															bGotit=true;
														}
														catch(CException *e)// CDBException *e, CMemoryException *m)  
														{
															if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
															{
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);  Sleep(50);//(Dispatch message)
															}
															else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
															{
																// The error code is in e->m_nRetCode
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Retrieve: Caught exception: out of memory");
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
															}
															else 
															{
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Retrieve: Caught other exception.\n%s", szSQL);
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
															}
															e->Delete();
														} 
														catch( ... )
														{
														}
													}
													prsParam->Close();
													delete prsParam;
												}

if(bGotit)
{
_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top 1 val, datatype from (select top ' + %s + ' (case when len(' + %s + ') > 0 THEN ' + %s + ' ELSE ''' + %s + ''' END) as val, event_start, (select datatype from MetaConfigUnionALLView \
where col_name = ''' + %s + ''') as datatype from cortex.dbo.LiveEventData as LiveEventData where event_start > ' + %f + ' and \
(event_status is NULL or event_status = 0) and ' + %s + ' = ''' + %s + ''' order by event_start) as Temp order by event_start desc'",
					target_valueid,returned_property_col_name,returned_property_col_name,
					default_value,returned_property_col_name,
					dblTime,target_criterion_col_name,criterion_value
					);
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "2� SQL %s", szSQL);  Sleep(50);//(Dispatch message)

prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);




}
else
{
	prsParam = NULL;
}
* /

													if(prsParam != NULL)
													{
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "FAR � parameter retrieve not null");  Sleep(50);//(Dispatch message)
														if((!prsParam->IsEOF())&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
														{
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "FAR � parameter retrieve NEOF");  Sleep(50);//(Dispatch message)
															CString szDataType;

															try
															{
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "FAR � about to: prsParam->GetFieldValue(\"val\", szTemp);");  Sleep(50);//(Dispatch message)
//																prsParam->GetFieldValue((short)0, szTemp);  //"val" field name
																prsParam->GetFieldValue("val", szTemp);  //"val" field name
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "FAR � parameter got %s for %s",szTemp, szSQL);  Sleep(50);//(Dispatch message)
//																prsParam->GetFieldValue((short)1, szDataType);  // "datatype" field name
																prsParam->GetFieldValue("datatype", szDataType);  // "datatype" field name
//if(pnucleus->m_settings.m_bUseEmail) 	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "FAR � got %s (datatype = %s) for %s, %s",szTemp, szDataType, szSQL, szPreParse);  Sleep(50);//(Dispatch message)
	//															prsParam->GetFieldValue("value", szTemp);  // for use with output table
	//															prsParam->GetFieldValue("datatype", szTemp);  // sp now returns datatype

															}
															catch(CException *e)// CDBException *e, CMemoryException *m)  
															{
																if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
																{
	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);  Sleep(50);//(Dispatch message)
																}
																else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
																{
																	// The error code is in e->m_nRetCode
	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Retrieve: Caught exception: out of memory");
										//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
																}
																else 
																{
	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Retrieve: Caught other exception.\n%s", szSQL);
										//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
																}
																e->Delete();
																szTemp=""; // blank to be safe
															} 
															catch( ... )
															{
	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Retrieve: Caught other exception.\n%s", szSQL);
																szTemp=""; // blank to be safe
															}



			//												szTemp = "Parameter!";

//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "FAR � got %s",szTemp);  Sleep(50);//(Dispatch message)
															if(szTemp.GetLength())
															{
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "FAR � returned %s", szTemp);  Sleep(50);//(Dispatch message)
																unsigned long ulTime = (unsigned long)(atof(szTemp));
	//															bool bTime = (ulTime>1)?true:false;
																bool bTime = (szDataType.CompareNoCase("Time")==0)?true:false;
																if(bTime)
																{
																	ulTime%=86400;
																	szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "szParam =  %s", szParam);  Sleep(50);//(Dispatch message)
																}
																bool bPM = true;
																if(ulTime<43200) bPM = false;
																
		
																if(szStyle.GetLength())
																{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "szStyle =  %s", szStyle);  Sleep(50);//(Dispatch message)


																	// now have to get a list of styles
																	prsParam->Close();
																	delete prsParam;

																	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select actions from nucleus.dbo.styles as styles where name = '%s'", szStyle);
	//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "action far SQL: %s", szSQL);  Sleep(50); //(Dispatch message)

																	prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "action far SQL error?: %s", errorstring);  Sleep(50); //(Dispatch message)

																	int nNumStyles = 0;
																	
																	szParam = "";
																	bool bAdded = false;
																	//unsigned long ulTimeTemp = ulTime;
																	if(prsParam != NULL)
																	{

																		if((!prsParam->IsEOF())&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
																		{
																			prsParam->GetFieldValue("actions", szStyle);
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "action got: %s", szStyle);  Sleep(50); //(Dispatch message)


																			CSafeBufferUtil sbu;
																			char actions[2048];
																			sprintf(actions, "%s", szStyle);
																			char* paction = sbu.Token(actions, strlen(actions), "|");

																			while((paction)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
																			{
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "action got: %s", paction);  Sleep(50); //(Dispatch message)
																				if(stricmp(paction, "snapTo(5)")==0)
																				{
																					nNumStyles++;

																					unsigned long ulTimeTemp = ((ulTime + 150)/300)*300;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(15)")==0)
																				{
																					nNumStyles++;
																					unsigned long ulTimeTemp = ((ulTime + 450)/900)*900;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(30)")==0)
																				{
																					nNumStyles++;
																					unsigned long ulTimeTemp = ((ulTime + 900)/1800)*1800;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(60)")==0)
																				{
																					unsigned long ulTimeTemp = ((ulTime +1800)/3600)*3600;
																					ulTime=ulTimeTemp;
																					nNumStyles++;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "clock(24)")==0)
																				{
																					szTemp.Format("%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																					nNumStyles++;
																				}
																				else 
																				if(stricmp(paction, "clock(12)")==0)
																				{
																					nNumStyles++;
																					if(ulTime>=43200) ulTime-=43200;
																					szTemp.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60);
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "clock(12): %s", szTemp);  Sleep(50); //(Dispatch message)
																				}
																				else 
																				if(stricmp(paction, "formatTime('H:MM:SS')")==0)
																				{
																					nNumStyles++;
																					szParam.Format("%d:%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH:MM:SS')")==0)
																				{
																					nNumStyles++;
																					szParam.Format("%02d:%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH:MM')")==0)
																				{
																					nNumStyles++;
																					szParam.Format("%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('MM')")==0)
																				{
																					nNumStyles++;
																					szParam.Format("%02d", ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('H')")==0)
																				{
																					nNumStyles++;
																					szParam.Format("%d", (ulTime)/3600); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "appendAMPM('lcase')")==0)
																				{
																					nNumStyles++;

																					szParam = szTemp;
																					szTemp.Format("%s %s", szParam, bPM?"pm":"am");
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "appendAMPM('lcase'): %s", szTemp);  Sleep(50); //(Dispatch message)
																				}
																				else 
																				if(stricmp(paction, "appendAMPM('ucase')")==0)
																				{
																					nNumStyles++;
																					szParam = szTemp;
																					szTemp.Format("%s %s", szParam, bPM?"PM":"AM");
																				}
	//else not nNumStyles++; which is why i had to do it that annoying way.


	/*
	Round,roundNumber()
	Truncate,truncateNumber()
	Filter Smut,smutFilter()
	Truncate To 20 Characters,truncateText(20)
	Truncate To 50 Characters,truncateText(50)
	Truncate To 100 Characters,truncateText(100)

	Convert All Text To Upper Case,textCase('upper')
	Convert All Text To Lower Case,textCase('lower')
	Convert First Letter Of First Word To Upper Case,textCase('sentence')
	Convert First Letter Of Text String To Upper Case,textCase('title')

	Long Date Lower Case,formatDate('longdate', 'lcase')
	Long Date Upper Case,formatDate('longdate', 'ucase')
	Short Date Lower Case,formatDate('shortdate', 'lcase')
	Short Date Upper Case,formatDate('shortdate', 'ucase')
	Celsius,formatTemp('celsius')
	Fahrenheit,formatTemp('fahrenheit')
	* /
																			
																				paction = sbu.Token(NULL, NULL, "|");
																			}
																			if(bTime) szValue += szTemp;
																		}
																		prsParam->Close();
																		delete prsParam;
																		prsParam = NULL;

																	}

																	if(nNumStyles==0)
																	{
																		if(bTime)
																		{
																			// format it the default way H:MM 24 hour clock
																			szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																			szValue += szTemp;
																		}
																		else 
																		{
																			szValue += szTemp;
																		}
																	}
																}
																else
																{
																	// if no style, need to understand if this is a time.
																	if(bTime)
																	{
																		// format it the default way H:MM 24 hour clock
																		szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																		szValue += szTemp;
																	}
																	else 
																	{
																		szValue += szTemp;
																	}
																}
															}
														}
														if(prsParam)
														{
															prsParam->Close();
															delete prsParam;
														}
													}
													else
													{
	//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "� returned NULL");  Sleep(50);//(Dispatch message)

													}
												}
											}
											else
											{
												szValue += ch;
												i++;
											}
										}
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "styles done");  Sleep(50); //(Dispatch message)


										if((!pnucleus->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)) 
										{
										if(nTriggerItemID<=0)
										{
											if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
											{
											//insert

												char* chValue = dbSet.EncodeQuotes(szValue);
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, nucleus_eventid, Event_ActionID, value, flags, analysis_id) values \
(%d, %d, %d, '%s', %d, %d)",
													pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nNucleusEventItemID, nEventActionID, 
													chValue?chValue:szValue,
													ulFlags, nAnalysisID
												);
												if(chValue) free(chValue);
//if(pnucleus->m_settings.m_bUseEmail) 	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug2", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing far SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}
												else
												{
													bInsBrk = true;
												}
											}
										}
										else
										{
											// update
											if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
											{
												char* chValue = dbSet.EncodeQuotes(szValue);
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, nucleus_eventid = %d, Event_ActionID = %d, value = '%s', flags = %d, analysis_id = %d WHERE analyzed_trigger_id = %d",
														pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
														nAutomationItemID, nNucleusEventItemID, nEventActionID, 
													chValue?chValue:szValue,
														ulFlags, nAnalysisID, nTriggerItemID
													);
												if(chValue) free(chValue);
//if(pnucleus->m_settings.m_bUseEmail) 	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing far SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}
											}
										}
										}

/*
										int ixi=0;
										while((!pnucleus->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)&&(ixi<1000))  // 1 second pause if the SP is triggered
										{
											Sleep(1); ixi++;
										}
* /
										//if(bInsBrk) break;
									}
									else
									{
										// just set to renanalyzed
						//				if(bCueTime)
						//					ulFlags |= NUCLEUS_FLAG_REANALYZED;
						//				else
										ulFlags |= NUCLEUS_FLAG_ANALYZED;
										if((!pnucleus->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)) 
										if(nTriggerItemID<=0)
										{
											if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
											{
											//insert
												char* chValue = dbSet.EncodeQuotes(szValue);
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, nucleus_eventid, Event_ActionID, value, flags, analysis_id) values \
(%d, %d, %d, '%s', %d, %d)",
													pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nNucleusEventItemID, nEventActionID, 
													chValue?chValue:szValue, 
													ulFlags, nAnalysisID
												);
												if(chValue) free(chValue);
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing far SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												} //else break;
											}
										}
										else
										{
											// update
											if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
											{
												char* chValue = dbSet.EncodeQuotes(szValue);
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, nucleus_eventid = %d, Event_ActionID = %d, value = '%s', flags = %d, analysis_id = %d WHERE analyzed_trigger_id = %d",
														pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
														nAutomationItemID, nNucleusEventItemID, nEventActionID, 
														chValue?chValue:szValue, 
														ulFlags, nAnalysisID, nTriggerItemID
													);
												if(chValue) free(chValue);
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing far SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}
											}
										}
									}
								}
						}

						if(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread) prs->MoveNext();
						Sleep(1);
					} //while(!prs->IsEOF())
					prs->Close();
					delete prs;
					prs = NULL;

					if(nNumRec==0)
					{
						// no records. just wait.
					//	int ixi=0;
						while((!pnucleus->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))  // 2 second pause if the SP is triggered
						{
	_ftime(&pnucleus->m_data.m_timebFarTick); // the last time check inside the thread
							Sleep(1);// ixi++;
						}
					}
					else
					{
						Sleep(1);
					}
				}//if(prs != NULL)

				else
				{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis resultset was null!"); // Sleep(50);//(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Far analysis ERROR %s", errorstring); // Sleep(50);//(Dispatch message)

				}


/*
				if(delta>0.0)
				{
					wait = (double)clock();
					while((!pnucleus->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)&&(((double)clock())-wait<delta))  // wait here until there is a change
					{
						Sleep(1); // dont peg processor
					}
				}
		* /
/*
				delta = ((double)pnucleus->m_settings.m_ulTriggerCueThreshold)*500.0;
				wait = (double)clock();
				while((!pnucleus->m_data.m_bFarEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)&&(((double)clock())-wait<delta))  // wait here until there is a change
				{
					Sleep(1); // dont peg processor
				}
* /

				Sleep(500);
			}
		} //		if(
//			  (pnucleus->m_data.m_nIndexAutomationEndpoint>=0)
//			&&(pnucleus->m_settings.m_nNumEndpointsInstalled>0)
///			&&(pnucleus->m_settings.m_ppEndpointObject)
//			&&(pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint])
//			&&(!pnucleus->m_data.m_bProcessSuspended)
//			&&(pnucleus->m_data.m_key.m_bValid)  // must have a valid license
//			&&(
//					(!pnucleus->m_data.m_key.m_bExpires)
//				||((pnucleus->m_data.m_key.m_bExpires)&&(!pnucleus->m_data.m_key.m_bExpired))
//				||((pnucleus->m_data.m_key.m_bExpires)&&(pnucleus->m_data.m_key.m_bExpireForgiveness)&&(pnucleus->m_data.m_key.m_ulExpiryDate+pnucleus->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
//				)
//			&&(
//					(!pnucleus->m_data.m_key.m_bMachineSpecific)
//				||((pnucleus->m_data.m_key.m_bMachineSpecific)&&(pnucleus->m_data.m_key.m_bValidMAC))
//				)
//			)

		//Sleep(1); // dont peg processor
	} // while((!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))

	if(pdbConn)
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM Analysis");
		while((db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
		{
		//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)
		}
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
				pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"
			);
		while((db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
		{
		//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)
		}
	}


	pnucleus->m_data.m_bFarAnalysisThreadStarted=false;
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ending m_bFarAnalysisThreadStarted"); // Sleep(50);//(Dispatch message)
	_endthread();
	Sleep(150);

}
*/
void NucleusGlobalAnalysisThread(void* pvArgs)
{
	CNucleusMain* pnucleus = (CNucleusMain*) pvArgs;
	if(pnucleus==NULL) return;

	pnucleus->m_data.m_bGlobalAnalysisThreadStarted=true;

	g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusGlobalAnalysisThread", "Beginning global analysis thread");    //(Dispatch message)


	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];

	strcpy(errorstring, "");
	CDBUtil db;
	CDBUtil dbSet;
	CBufferUtil bu;

	CDBconn* pdbConn = db.CreateNewConnection(pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:analysis_database_connect", errorstring);  //(Dispatch message)
			pdbConn = pnucleus->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW); 
		pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:analysis_database_init", errorstring);  //(Dispatch message)
		pdbConn = pnucleus->m_data.m_pdbConn;

		//**MSG
	}

	CDBconn* pdbConnSet = dbSet.CreateNewConnection(pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW);
	if(pdbConnSet)
	{
		if(dbSet.ConnectDatabase(pdbConnSet, errorstring)<DB_SUCCESS)
		{
			pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:analysis_database2_connect", errorstring);  //(Dispatch message)
			pdbConnSet = pnucleus->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW); 
		pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:analysis_database2_init", errorstring);  //(Dispatch message)
		pdbConnSet = pnucleus->m_data.m_pdbConn;

		//**MSG
	}
/*
	while(!pnucleus->m_data.m_bNearEventsChanged)
	{
		_ftime(&pnucleus->m_data.m_timebNearTick); // the last time check inside the thread
		Sleep(30);
	}
*/
//	Sleep(30000); // dont start thread analysis util after 30 sec

	bool bChangesPending = false;
	_timeb timebPending;
	_timeb timebChange;

	double delta = 0.0;
	double wait = 0.0;

	CNucleusEndpointObject* pAutoObj = NULL;
	if(
			(g_pnucleus->m_settings.m_ppEndpointObject)
		&&(g_pnucleus->m_data.m_nIndexAutomationEndpoint>=0)
		&&(g_pnucleus->m_data.m_nIndexAutomationEndpoint<g_pnucleus->m_settings.m_nNumEndpointsInstalled)
		&&(g_pnucleus->m_settings.m_ppEndpointObject[g_pnucleus->m_data.m_nIndexAutomationEndpoint])
		&&(!g_bKillThread)
		)
	{
		pAutoObj = pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint];
	}


	g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusGlobalAnalysisThread", "Connected global analysis thread");    //(Dispatch message)

	while((!g_bKillThread))//&&(!pnucleus->m_data.m_bKillAutomationThread))
	{
		if(
/*
			  (pnucleus->m_data.m_nIndexAutomationEndpoint>=0)
			&&(pnucleus->m_settings.m_nNumEndpointsInstalled>0)
			&&(pnucleus->m_data.m_nIndexAutomationEndpoint<pnucleus->m_settings.m_nNumEndpointsInstalled)
			&&(pnucleus->m_settings.m_ppEndpointObject)
			&&(pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint])
			&&*/
			  (!pnucleus->m_data.m_bProcessSuspended)
//			&&(pChannelObj->m_nListState&((1<<LISTTHREADING)|(1<<LISTISPLAYING)))  // list must be threaded and playing to run analysis
			&&(pnucleus->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!pnucleus->m_data.m_key.m_bExpires)
				||((pnucleus->m_data.m_key.m_bExpires)&&(!pnucleus->m_data.m_key.m_bExpired))
				||((pnucleus->m_data.m_key.m_bExpires)&&(pnucleus->m_data.m_key.m_bExpireForgiveness)&&(pnucleus->m_data.m_key.m_ulExpiryDate+pnucleus->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!pnucleus->m_data.m_key.m_bMachineSpecific)
				||((pnucleus->m_data.m_key.m_bMachineSpecific)&&(pnucleus->m_data.m_key.m_bValidMAC))
				)

			)
		{

// moved the following line upwards and out of the loop.  no need to keep rechecking it here, this gets set once.
//			CNucleusEndpointObject* pAutoObj = pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint];

			//Here, we analyze things that have not yet played, that are not yet analyzed, unless its within m_ulTriggerCueThreshold seconds of play time, then it gets reanalyzed
			_ftime(&pnucleus->m_data.m_timebNearTick); // the last time check inside the thread

			if(pnucleus->m_data.m_bNearEventsChanged)
			{
				pnucleus->m_data.m_bNearEventsChanged = false;
				if(!bChangesPending)
				{
					_ftime(&timebChange); // initialize this
				}
				bChangesPending = true;
				_ftime(&timebPending);
				timebPending.time += pnucleus->m_settings.m_nAnalyzeParameterDwellMS/1000;
				timebPending.millitm += pnucleus->m_settings.m_nAnalyzeParameterDwellMS%1000;
				if(timebPending.millitm>999)
				{
					timebPending.time++;
					timebPending.millitm -= 1000;
				}
			}
			// dwell changes - another change will reset the time, suppressing the previous change
			if(
				  (bChangesPending)
				&&(
				    (pnucleus->m_data.m_timebNearTick.time>timebPending.time)
					||(
					    (pnucleus->m_data.m_timebNearTick.time==timebPending.time)
						&&(pnucleus->m_data.m_timebNearTick.millitm>timebPending.millitm)
						)
					||(pnucleus->m_data.m_timebTick.time > (timebChange.time+pnucleus->m_settings.m_nAnalyzeParameterForceMS/1000))
					||(
					    (pnucleus->m_data.m_timebTick.time==(timebChange.time+pnucleus->m_settings.m_nAnalyzeParameterForceMS/1000))
						&&(pnucleus->m_data.m_timebTick.millitm>(timebChange.millitm+pnucleus->m_settings.m_nAnalyzeParameterForceMS%1000))
						)
					)
				)
			{
				bChangesPending = false;

				// have to reanalyze all

//				UPDATE AnalyzedTriggerData SET set flags = (flags & (~32));			

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET flags = (flags & (~%d)) WHERE status & %d = 0",
						pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the AnalyzedTriggerData table name
						NUCLEUS_FLAG_REANALYZED, 
						NUCLEUS_FLAG_TRIGGERED
					);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_RULES)  pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Global analysis SQL: %s", szSQL); // Sleep(50);//(Dispatch message)

					if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
				//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
					}


			}

			while((!pnucleus->m_data.m_bNearEventsChanged)&&(!bChangesPending)&&(!g_bKillThread))//&&(!pnucleus->m_data.m_bKillAutomationThread))
			{
	_ftime(&pnucleus->m_data.m_timebNearTick); // the last time check inside the thread
				// just get the exports with params to parse.
/*
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
			//		  "select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE TriggerTime > %lf and ((event_status & 19) = 0) and ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY TriggerTime ASC) ORDER BY TriggerTime ASC",
//						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_calc_start > %u) and ((event_status & 19) = 0)) ORDER BY event_calc_start ASC)  and ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY TriggerTime ASC",
						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_status & 19) = 0) ORDER BY event_calc_start ASC) and server = '%s' AND listid = %d AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY TriggerTime ASC",
//						"SELECT TOP 1 * FROM %s WHERE TriggerTime > %lf and event_status & 19 = 0 and flags & %d = 0 ORDER BY TriggerTime ASC",
						pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
					//	(unsigned long)pnucleus->m_data.m_dblAutomationTimeEstimate,
						pnucleus->m_data.m_pszServerName, pnucleus->m_data.m_nHarrisListID,	NUCLEUS_FLAG_REANALYZED
					);
*/


/*
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_status & 19) = 0) and channelid = %d AND AnalyzedTriggerDataFlags <> %d  ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
						pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
						pnucleus->m_data.m_nChannelID, pnucleus->m_data.m_nAnalysisCounter
					);
*/
				if((pAutoObj==NULL)||((pAutoObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)) // defaults to sentinel
				{
	
//itemid, TriggerTime, analyzed_trigger_id, AnalyzedTriggerDataFlags, nucleus_EventID, Graphic_Action_Type, Event_ActionID, Action_Value, Analysis_ID, event_calc_start, event_start					
/*
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_status & 19) = 0) \
AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
							pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							NUCLEUS_FLAG_REANALYZED
						);
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select itemid, TriggerTime, analyzed_trigger_id, AnalyzedTriggerDataFlags, nucleus_EventID, Graphic_Action_Type, Event_ActionID, Action_Value, Analysis_ID, event_calc_start, parent_calc_start, event_start from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE ((event_status & 19) = 0) \
AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
							pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							NUCLEUS_FLAG_REANALYZED
						);
*/
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select itemid, TriggerTime, analyzed_trigger_id, AnalyzedTriggerDataFlags, nucleus_EventID, Graphic_Action_Type, Event_ActionID, Action_Value, Analysis_ID, event_calc_start, parent_calc_start, event_start \
from GlobalAnalysisView where itemid in (SELECT TOP 1 itemid FROM GlobalAnalysisView WHERE ((event_status & %d) = 0) \
AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
//							pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							pnucleus->m_settings.m_nAnalyzeHarrisStatusExclude,
							NUCLEUS_FLAG_REANALYZED
						);

				//			need top x where top event item id is all same where event status & 19 =0 (not play, post, done)
				}
				else
				if((pAutoObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
				{
		

/*



			[itemid]
     ,[TriggerTime]
     ,[analyzed_trigger_id]
     ,[AnalyzedTriggerDataFlags]
     ,[nucleus_EventID]
     ,[Graphic_Action_Type]
     ,[Event_ActionID]
     ,[Action_Value]
     ,[Analysis_ID]
     ,[event_calc_start]
     ,[parent_calc_start]
     ,[event_start]
     ,[event_status]
     ,[layer]

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select * from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE event_status < 10 and event_status > 4 \
AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
							pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							NUCLEUS_FLAG_REANALYZED
						);

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select itemid, TriggerTime, analyzed_trigger_id, AnalyzedTriggerDataFlags, nucleus_EventID, Graphic_Action_Type, Event_ActionID, Action_Value, Analysis_ID, event_calc_start, parent_calc_start, event_start from %s where itemid in (SELECT TOP 1 itemid FROM TriggerAnalysisView WHERE event_status < 10 and event_status > 4 \
AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
							pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							NUCLEUS_FLAG_REANALYZED
						);
*/

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"select itemid, TriggerTime, analyzed_trigger_id, AnalyzedTriggerDataFlags, nucleus_EventID, Graphic_Action_Type, Event_ActionID, Action_Value, Analysis_ID, event_calc_start, parent_calc_start, event_start \
from GlobalAnalysisView where itemid in (SELECT TOP 1 itemid FROM GlobalAnalysisView WHERE event_status < 10 and event_status > 4 \
AND ((AnalyzedTriggerDataFlags & %d) = 0) ORDER BY event_calc_start ASC) ORDER BY TriggerTime ASC",
//							pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
							NUCLEUS_FLAG_REANALYZED
						);

				//			need top x where top event item id is all same where event status < 10  and event_status > 4 (not done. no errors, and allocated or better in system)
				}


if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_RULES)  pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Global analysis SQL: %s", szSQL); // Sleep(50);//(Dispatch message)
				int nNumRec = 0;
				CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
				if(prs != NULL)
				{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Near analysis resultset not null %d record", prs->GetRecordCount( )); // Sleep(50);//(Dispatch message)
					while((!pnucleus->m_data.m_bNearEventsChanged)&&(!prs->IsEOF())&&(!g_bKillThread))//&&(!pnucleus->m_data.m_bKillAutomationThread))
					{
	_ftime(&pnucleus->m_data.m_timebNearTick); // the last time check inside the thread
						nNumRec++;
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Near analysis resultset Got %d items so far", nNumRec); // Sleep(50);//(Dispatch message)
						CString szName;
						CString szValue;
						CString szTemp;
						//double dblTime;
						double dblClipTime;
						double dblEventTime;
						double dblParentClipTime;
						int nTriggerItemID=-1;
						int nAutomationItemID=-1;
						int nNucleusEventItemID=-1;
						int nEventActionID=-1;
						int nType=-1;
						int nAnalysisID=-1;

						prs->GetFieldValue("itemid", szTemp);
						if(szTemp.GetLength()) nAutomationItemID = atoi(szTemp);

						// Host
						// ParsedValue
						//prs->GetFieldValue("TriggerTime", szTemp);
						//dblTime=atof(szTemp);
						prs->GetFieldValue("analyzed_trigger_id", szTemp);
						if(szTemp.GetLength()) nTriggerItemID = atoi(szTemp);

						prs->GetFieldValue("AnalyzedTriggerDataFlags", szTemp);
						unsigned long ulFlags = atol(szTemp);
						//AnalyzedTriggerDataStatus

						prs->GetFieldValue("nucleus_EventID", szTemp);
						if(szTemp.GetLength()) nNucleusEventItemID = atoi(szTemp);
						//Nucleus_Timing_Col_Name
						//Source
						//Scene
						prs->GetFieldValue("Graphic_Action_Type", szTemp);
						nType = atoi(szTemp);
						//Action_Name


						prs->GetFieldValue("Event_ActionID", szTemp);
						if(szTemp.GetLength()) nEventActionID = atoi(szTemp);

						//Action_Value
						prs->GetFieldValue("Action_Value", szValue);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Near Analysis value is: %s", szValue);//  Sleep(50);//(Dispatch message)
					
						prs->GetFieldValue("Analysis_ID", szTemp);
						if(szTemp.GetLength()) nAnalysisID = atoi(szTemp);

						prs->GetFieldValue("event_calc_start", szTemp);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Far analysis got TriggerTime %s", szTemp); 
						dblClipTime=atof(szTemp);


						prs->GetFieldValue("parent_calc_start", szTemp);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Far analysis got parent_calc_start %s", szTemp); 
						dblParentClipTime=atof(szTemp);

						prs->GetFieldValue("event_start", szTemp);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Far analysis got TriggerTime %s", szTemp); 
						dblEventTime=atof(szTemp);

								if(nType == 0) // anim
								{
									// just set to 
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "anim is retrieved");//  Sleep(50);//(Dispatch message)
									ulFlags |= (NUCLEUS_FLAG_REANALYZED|NUCLEUS_FLAG_ANALYZED);
								//	ulFlags = pnucleus->m_data.m_nAnalysisCounter;

									if(nTriggerItemID<=0)
									{
										if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
										{
											//insert
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, nucleus_eventid, event_actionid, flags, analysis_id) values \
(%d, %d, %d, %d, %d)",
													pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nNucleusEventItemID, nEventActionID, ulFlags, nAnalysisID
												);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_ANALYZE) 	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis1", "Global Analysis SQL (anim): %s", szSQL);  Sleep(50); //(Dispatch message)
											if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
											{
											//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "ERROR executing near SQL: %s", errorstring); // Sleep(50); //(Dispatch message)
											}//else break;

										}
									}
									else
									{
										// update
										if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
										{
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, nucleus_eventid = %d, Event_ActionID = %d, flags = %d, analysis_id = %d WHERE analyzed_trigger_id = %d",
													pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nNucleusEventItemID, nEventActionID, ulFlags, nAnalysisID, nTriggerItemID
												);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_ANALYZE) 	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Global Analysis SQL (anim): %s", szSQL); // Sleep(50); //(Dispatch message)
											if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
											{
											//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "ERROR executing near SQL: %s", errorstring);//  Sleep(50); //(Dispatch message)
											}
										}
									}
								}
								else
								if(nType == 1) // export
								{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "export %s is retrieved", szValue);//  Sleep(50);//(Dispatch message)
									if(
											(szValue.GetLength())
										&&(
												((pnucleus->m_settings.m_bUseUTF8)&&(szValue.Find("§")>=0))
											||((!pnucleus->m_settings.m_bUseUTF8)&&(szValue.Find("�")>=0))
											)
										)
									{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "NEAR value has a �: %s", szValue);  Sleep(50);//(Dispatch message)
										// we have a least one param to parse
										// just set to renanalyzed
										ulFlags |= (NUCLEUS_FLAG_REANALYZED|NUCLEUS_FLAG_ANALYZED);
							//			ulFlags = pnucleus->m_data.m_nAnalysisCounter;

										// lets parse out szValue and set it up;

										CString szPreParse = szValue;
										szValue = "";

										int i=0;

/*
UTF-8 has can take anywhere from 1 to 4 bytes per character. One byte UTF-8 characters are just 7-bit ASCII, 
a subset of the ISO and Windows sets, so they are left alone. The rest of the characters match byte patterns 
in certain ranges which are unlikely to appear in regular text. They can be described in a compact and 
machine-readable way by a regular expression:

var pattern = /[\xC2-\xDF][\x80-\xBF]|[\xE0-\xEF][\x80-\xBF]{2}|[\xF0-\xF4][\x80-\xBF]{3}/g

Broken down, the regular expression matches one of:

   1. A byte between 0xC2 and 0xDF, followed by a byte between 0�80 and 0xBF, or
   2. A byte between 0xE0 and 0xEF, followed by 2 bytes between 0�80 and 0xBF, or
   3. A byte between 0xF0 and 0xF4, followed by 3 bytes between 0�80 and 0xBF.

These correspond to 2-, 3-, and 4-byte UTF-8 characters.


*/



										while((i<szPreParse.GetLength())&&(!pnucleus->m_data.m_bNearEventsChanged)&&(!g_bKillThread))//&&(!pnucleus->m_data.m_bKillAutomationThread))
										{
											bool bDelimFound=false;
											char ch = szPreParse.GetAt(i);
											if(pnucleus->m_settings.m_bUseUTF8)
											{
												if((ch == '�')&&(szPreParse.GetAt(i+1) == '�'))	
												{
													bDelimFound=true; i+=2;
												}
											}
											else
											{
												if(ch == '�')
												{
													bDelimFound=true; i++;
												}
											}

											if(bDelimFound)
											{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "NEAR ch has a �");  Sleep(50);//(Dispatch message)

												CString szParam = "";
												CString szStyle = "";

												bDelimFound=false;												
												ch = szPreParse.GetAt(i);

												bool bStyle = false;  

												if(pnucleus->m_settings.m_bUseUTF8)
												{
													if((ch == '�')&&(szPreParse.GetAt(i+1) == '�'))	
													{
														bDelimFound=true; i+=2;
													}
												}
												else
												{
													if(ch == '�')
													{
														bDelimFound=true; i++;
													}
												}

												
												while((!bDelimFound)&&(i<szPreParse.GetLength())&&(!g_bKillThread))//&&(!pnucleus->m_data.m_bKillAutomationThread))
												{
													if(!bStyle)
													{
														szParam += ch;
													}
													else
													{
														szStyle += ch;
													}
													i++;
													ch = szPreParse.GetAt(i);
													if(ch == ':')
													{
														i++;
														bStyle = true;
														ch = szPreParse.GetAt(i);
													}

													if(pnucleus->m_settings.m_bUseUTF8)
													{
														if((ch == '�')&&(szPreParse.GetAt(i+1) == '�'))	
														{
															bDelimFound=true; i+=2;
														}
													}
													else
													{
														if(ch == '�')
														{
															bDelimFound=true; i++;
														}
													}

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "near Param %s, Style %s", szParam,szStyle);  Sleep(50);//(Dispatch message)

												}

if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PARAMPARSE) 	
	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "(global) FINAL Param %s, Style %s in value [%s]", szParam, szStyle, szValue);  //Sleep(50);//(Dispatch message)
												// ok here, we should have the param name and possible style.
												// first lets get the param.

/*												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s '%s', %f",
														pnucleus->m_settings.m_pszGetParameterValueProc?pnucleus->m_settings.m_pszGetParameterValueProc:"spGetParameterValue",  
														szParam, dblTime
													);
*/

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
/*
												// this sets the param
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
											//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
												}

												// this gets it!
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM [Output]");
*/
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
			
// try 2
/*
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "{CALL %s('%s', %f)}",
														pnucleus->m_settings.m_pszGetParameterValueProc?pnucleus->m_settings.m_pszGetParameterValueProc:"spGetParameterValue",  
														szParam, dblTime
													);

*/												
				// try 3								
											
												if((!pnucleus->m_data.m_bNearEventsChanged)&&(!g_bKillThread))//&&(!pnucleus->m_data.m_bKillAutomationThread))												
												{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Near parameter retrieve index for [%s]", szParam); // Sleep(50);//(Dispatch message)
													EnterCriticalSection(&pnucleus->m_data.m_critParameterRules);
													int idx = pnucleus->m_data.GetParameterQueryIndex(szParam);


	//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Near parameter retrieved index %d", idx); // Sleep(50);//(Dispatch message)
													if(idx>=0)
													{
														CString timestr;  
														// This was incorrect!  bugfix in 2.2.1.13  24-August-2011
//														if(pnucleus->m_data.m_ppParameterRule[idx]->sz_target_valueid.Compare("0"))

														// really it should be this!
														if(pnucleus->m_data.m_ppParameterRuleQuery[idx]->sz_target_valueid.Compare("0"))
														{

															if(pnucleus->m_settings.m_bNearAnalysisUsesCalc)
																timestr.Format("%.3f", dblClipTime);
															else
																timestr.Format("%.3f", dblEventTime);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PARAMPARSE) 
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "(global) (time=%s) get param (%s) is using %s with target value = %s at index %d", timestr, szParam, pnucleus->m_settings.m_bNearAnalysisUsesCalc?"calc time":"event time", pnucleus->m_data.m_ppParameterRule[idx]->sz_target_valueid, idx); // Sleep(50);//(Dispatch message)
														}
														else
														{
													//		if(pnucleus->m_settings.m_bNearAnalysisUsesCalc)
													//			timestr.Format("%.3f", dblClipTime-0.020);
													//		else
																timestr.Format("%.3f", dblParentClipTime-0.020);  // now should always be the real time
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PARAMPARSE) 
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "(global) (time=%s) get param (%s) is using parent time - .02 with target value = %s at index %d", timestr, szParam, pnucleus->m_data.m_ppParameterRule[idx]->sz_target_valueid, idx); // Sleep(50);//(Dispatch message)
														}
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s%s%s",
																pnucleus->m_data.m_ppParameterRuleQuery[idx]->sz_query1,
																timestr,
																pnucleus->m_data.m_ppParameterRuleQuery[idx]->sz_query2
															);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PARAMPARSE) 
 	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "(global) (time=%s) get param (%s) Query: %s", timestr, szParam, szSQL); // Sleep(50);//(Dispatch message)
													}

													LeaveCriticalSection(&pnucleus->m_data.m_critParameterRules);
												
													
													
													CRecordset* prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);






//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "� SQL ERROR %s", errorstring);  Sleep(50);//(Dispatch message)


/*

												bool bGotit=false;
												CString target_valueid, target_criterion_col_name, criterion_value, returned_property_col_name, default_value;

_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select target_valueid, target_criterion_col_name, criterion_value, returned_property_col_name, [default_value] from [Parameters] where [name] = '%s'",szParam);
//_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from Analysis");

pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "� SQL %s", szSQL);  Sleep(50);//(Dispatch message)
CRecordset* prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);

												if(prsParam != NULL)
												{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "� not null");  Sleep(50);//(Dispatch message)
													if((!prsParam->IsEOF())&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
													{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "� NEOF");  Sleep(50);//(Dispatch message)
														try
														{
															prsParam->GetFieldValue("target_valueid", target_valueid);
															prsParam->GetFieldValue("target_criterion_col_name", target_criterion_col_name);
															prsParam->GetFieldValue("criterion_value", criterion_value);
															prsParam->GetFieldValue("returned_property_col_name", returned_property_col_name);
															prsParam->GetFieldValue("default_value", default_value);

															bGotit=true;
														}
														catch(CException *e)// CDBException *e, CMemoryException *m)  
														{
															if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
															{
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);  Sleep(50);//(Dispatch message)
															}
															else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
															{
																// The error code is in e->m_nRetCode
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Retrieve: Caught exception: out of memory");
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
															}
															else 
															{
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Retrieve: Caught other exception.\n%s", szSQL);
									//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
															}
															e->Delete();
														} 
														catch( ... )
														{
														}
													}
													prsParam->Close();
													delete prsParam;
												}

if(bGotit)
{
_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top 1 val, datatype from (select top ' + %s + ' (case when len(' + %s + ') > 0 THEN ' + %s + ' ELSE ''' + %s + ''' END) as val, event_start, (select datatype from MetaConfigUnionALLView \
where col_name = ''' + %s + ''') as datatype from cortex.dbo.LiveEventData as LiveEventData where event_start > ' + %f + ' and \
(event_status is NULL or event_status = 0) and ' + %s + ' = ''' + %s + ''' order by event_start) as Temp order by event_start desc'",
					target_valueid,returned_property_col_name,returned_property_col_name,
					default_value,returned_property_col_name,
					dblTime,target_criterion_col_name,criterion_value
					);
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "2� SQL %s", szSQL);  Sleep(50);//(Dispatch message)

prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);




}
else
{
	prsParam = NULL;
}
*/

													if(prsParam != NULL)
													{
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "NEAR ************** 2� not null"); // Sleep(50);//(Dispatch message)
														if((!prsParam->IsEOF())&&(!g_bKillThread))//&&(!pnucleus->m_data.m_bKillAutomationThread))
														{
	//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "2� NEOF");  Sleep(50);//(Dispatch message)

															CString szDataType;
															try
															{
																prsParam->GetFieldValue((short)0, szTemp);  //"val" field name
																prsParam->GetFieldValue((short)1, szDataType);  // "datatype" field name
	//															prsParam->GetFieldValue("value", szTemp);  // for use with output table
	//															prsParam->GetFieldValue("datatype", szDataType);  // sp now returns datatype
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PARAMPARSE)
	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "(global) param got %s (datatype = %s) for %s, %s",szTemp, szDataType, szSQL, szPreParse);  Sleep(50);//(Dispatch message)

															}
															catch(CException *e)// CDBException *e, CMemoryException *m)  
															{
																if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
																{
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);  Sleep(50);//(Dispatch message)
																}
																else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
																{
																	// The error code is in e->m_nRetCode
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Retrieve: Caught exception: out of memory");
										//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
																}
																else 
																{
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Retrieve: Caught other exception.\n%s", szSQL);
										//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
																}
																e->Delete();
																szTemp=""; // blank to be safe
															} 
															catch( ... )
															{
																szTemp=""; // blank to be safe
															}



			//												szTemp = "Parameter!";

	//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "� got %s",szTemp);  Sleep(50);//(Dispatch message)
															if(szTemp.GetLength())
															{
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "� returned %s", szTemp);  Sleep(50);//(Dispatch message)
																double dblNumber = atof(szTemp);
																unsigned long ulTime = (unsigned long)(dblNumber);
																unsigned long ulOriginalTime = ulTime;
	//															bool bTime = (ulTime>1)?true:false;
																bool bTime = (szDataType.CompareNoCase("Time")==0)?true:false;
																bool bDuration = false;
																if(bTime)
																{
																	// less than one days worth of milliseconds, but a unixtime far too far in the past to be meaningful
																	// must be a millisecond duration. 
																	// have to divide by 1000 to bring it to seconds, and then we can apply the time styles to it.
																	if(ulTime<86400000)
																	{
																		ulTime/=1000;
																		bDuration = true;
																	}

																	ulTime%=86400;
																	szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
	//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "szParam =  %s", szParam);  Sleep(50);//(Dispatch message)
																}
																bool bPM = true;
																if(ulTime<43200) bPM = false;
																
		
																if(szStyle.GetLength())
																{

																	// now have to get a list of styles
																	prsParam->Close();
																	delete prsParam;

																	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select actions from %s.dbo.styles as styles where name = '%s'",
																		(g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus"),
																		szStyle
																		);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PARAMPARSE) 	
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "(global) Applying style [%s] to param [%s] in event value [%s]", szStyle, szParam, szValue);

																	prsParam = dbSet.Retrieve(pdbConnSet, szSQL, errorstring);

	//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "action near SQL error?: %s", errorstring);  Sleep(50); //(Dispatch message)

																	int nNumStyles = 0;
																	
																	szParam = "";
																	bool bAdded = false;
																	//unsigned long ulTimeTemp = ulTime;
																	if(prsParam != NULL)
																	{

																		if((!prsParam->IsEOF())&&(!g_bKillThread))//&&(!pnucleus->m_data.m_bKillAutomationThread))
																		{
																			prsParam->GetFieldValue("actions", szStyle);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PARAMPARSE) 
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "(global) style action retrieved: %s", szStyle); // Sleep(50); //(Dispatch message)


																			CSafeBufferUtil sbu;
																			char actions[2048];
																			sprintf(actions, "%s", szStyle);
																			char* paction = sbu.Token(actions, strlen(actions), "|");
																			bool b24 = bDuration; // initialize to false if not a duration.

																			while((paction)&&(!g_bKillThread))//&&(!pnucleus->m_data.m_bKillAutomationThread))
																			{

////////////////////////////////////////////////////
////////   Put Tabulator stuff in here
																				

																				if(stricmp(paction, "Tabulator(classification)")==0)
																				{
																					nNumStyles++;
																					szParam = "";

																					_ftime(&pnucleus->m_data.m_timebNearTick); // the last time check inside the thread

																					unsigned char buffer[MAX_PATH];
																					unsigned char* pbuf = buffer;
																					_snprintf((char*)buffer, MAX_PATH-1, "%s|Test_Class|%s", 
																						g_pnucleus->m_settings.m_pszTabulatorPlugInModule?g_pnucleus->m_settings.m_pszTabulatorPlugInModule:"AutomationData",
																						szTemp
																						);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PARAMPARSE) 
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "(global) style %s sending %s to Tabulator", szStyle, buffer); // Sleep(50); //(Dispatch message)
																					int nTab = g_pnucleus->m_data.SendTabulatorCommand(&pbuf);
																					if(nTab>=NUCLEUS_SUCCESS)
																					{
																						if((pbuf)&&(pbuf != buffer))
																						{
																							szParam.Format("%s", pbuf); 
																							free(pbuf);
																						}
																					}
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "Tabulator(replacement)")==0)
																				{
																					nNumStyles++;
																					szParam = "";

																					_ftime(&pnucleus->m_data.m_timebNearTick); // the last time check inside the thread

																					unsigned char buffer[MAX_PATH];
																					unsigned char* pbuf = buffer;
																					_snprintf((char*)buffer, MAX_PATH-1, "%s|Test_Replace|%.03f", 
																						g_pnucleus->m_settings.m_pszTabulatorPlugInModule?g_pnucleus->m_settings.m_pszTabulatorPlugInModule:"AutomationData",
																						dblNumber
																						);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PARAMPARSE) 
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "(global) style %s sending %s to Tabulator", szStyle, buffer); // Sleep(50); //(Dispatch message)
																					int nTab = g_pnucleus->m_data.SendTabulatorCommand(&pbuf);
																					if(nTab>=NUCLEUS_SUCCESS)
																					{
																						if((pbuf)&&(pbuf != buffer))
																						{
																							szParam.Format("%s", pbuf); 
																							free(pbuf);
																						}
																					}
																					szTemp = szParam;
																				}
																				else 

////////   Put Tabulator stuff in here
////////////////////////////////////////////////////
																				if((strnicmp(paction, "timeZone(", 9)==0)&&(strlen(paction)>9))
																				{
																					int nHours = atoi(paction+10)*((*(paction+9) =='-')? -3600: 3600);
																					nNumStyles++;

if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PARAMPARSE) 
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "(global) style %s resolves to %d (%d with %d)", szStyle, nHours , ulTime, pnucleus->m_data.m_timebNearTick.timezone); // Sleep(50); //(Dispatch message)

																					// time is already in local time, so we have to find the difference between this and that.
																					_ftime(&pnucleus->m_data.m_timebNearTick); // the last time check inside the thread


																					long ulTimeTemp = ulTime + (pnucleus->m_data.m_timebNearTick.timezone*60) // back to UTC
																						+ nHours; // and add back the selected offset

																					while(ulTimeTemp>86400) ulTimeTemp -= 86400;
																					while(ulTimeTemp<0) ulTimeTemp += 86400;
																																					
																					// reset this stuff if a differnt time zone!
																					if(ulTime<43200) bPM = false;
																					else bPM = true;


																					ulTime=(unsigned long) ulTimeTemp;

																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00:%02d", ((ulTime)%3600)/60); // special case 00
																						else
																							szParam.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					}
																					else
																						szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
																				}
																				else 
																			//	if((stricmp(paction, "timeRemaining")==0))
																				if((strnicmp(paction, "timeRemaining(", 14)==0)&&(strlen(paction)>14))
																				{
																					// will subtract "now" from the passed in time.
																					nNumStyles++;

																					bool bDebug = (strnicmp(paction, "timeRemaining(d", 15)==0)?true:false;

																					// caution, this is not automation time.

																					unsigned long ulNow = (unsigned long)(g_pnucleus->m_data.m_timebTick.time - (g_pnucleus->m_data.m_timebTick.timezone*60) +(g_pnucleus->m_data.m_timebTick.dstflag?3600:0)); // local time....

																					unsigned long ulTimeTemp = 0;
																					unsigned long ulDebugTime = ulTime;
																					if(ulOriginalTime > ulNow) ulTimeTemp = ulOriginalTime - ulNow;
																					ulTime=ulTimeTemp;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00:%02d", ((ulTime)%3600)/60); // special case 00
																						else
																							szParam.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					}
																					else
																						szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default

																					if(bDebug)
																					{
																						CString szDebug;
																						szDebug.Format("%d (%d) - %d = %d: %s", ulOriginalTime, ulDebugTime, ulNow, ulTimeTemp, szParam);
																						szParam = szDebug;
																					}
																				


if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PARAMPARSE) 
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "(global) style %s resolves to %s", szStyle, szParam); // Sleep(50); //(Dispatch message)


																				}
																				else 
																				if((strnicmp(paction, "snapTo(", 7)==0)&&(strlen(paction)>7))
																				{
																					int nMinutes = atoi(paction+7)*60;
																					nNumStyles++;

//																					unsigned long ulTimeTemp = ((ulTime + 150)/nMinutes)*nMinutes;  // five minute rounding hardcoded, removed
																					unsigned long ulTimeTemp = ((ulTime + nMinutes/2)/nMinutes)*nMinutes;
																					ulTime=ulTimeTemp;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00:%02d", ((ulTime)%3600)/60); // special case 00
																						else
																							szParam.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					}
																					else
																						szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
																				}
																				else 
/*
																				if(stricmp(paction, "snapTo(5)")==0)
																				{
																					nNumStyles++;

																					unsigned long ulTimeTemp = ((ulTime + 150)/300)*300;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(15)")==0)
																				{
																					nNumStyles++;
																					unsigned long ulTimeTemp = ((ulTime +450)/900)*900;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(30)")==0)
																				{
																					nNumStyles++;
																					unsigned long ulTimeTemp = ((ulTime+900)/1800)*1800;
																					ulTime=ulTimeTemp;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
																				if(stricmp(paction, "snapTo(60)")==0)
																				{
																					unsigned long ulTimeTemp = ((ulTime+1800)/3600)*3600;
																					ulTime=ulTimeTemp;
																					nNumStyles++;
																					szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																				}
																				else 
*/
																				if(stricmp(paction, "clock(24)")==0)
																				{
																					szTemp.Format("%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																					nNumStyles++;
																					b24 = true;
																				}
																				else 
																				if(stricmp(paction, "clock(12)")==0)
																				{
																					nNumStyles++;
																					if(!bDuration)
																					{
																						if(ulTime>=43200) ulTime-=43200;
																						szTemp.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60);
																						b24 = false;  // if a duration, suppress 12 hour clock
																					}
																					else
																					{
																						// enforce 24 hour clock
																						szTemp.Format("%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																						b24 = true;
																					}
																				}
																				else 
																				if(stricmp(paction, "formatTime('0')")==0)
																				{
																					nNumStyles++;
																					szParam = "";
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('H:MM:SS')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00:%02d:%02d", ((ulTime)%3600)/60, ((ulTime)%3600)%60); // special case 00
																						else
																							szParam.Format("%d:%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					}		
																					else
																						szParam.Format("%d:%02d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH:MM:SS')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																						szParam.Format("%02d:%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					else
																						szParam.Format("%02d:%02d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60, ((ulTime)%3600)%60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH:MM')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																						szParam.Format("%02d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					else
																						szParam.Format("%02d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('H:MM')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00:%02d", ((ulTime)%3600)/60); // default
																						else
																							szParam.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60); // default
																					}
																					else
																						szParam.Format("%d:%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600)), ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('MM')")==0)
																				{
																					nNumStyles++;
																					szParam.Format("%02d", ((ulTime)%3600)/60); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('H')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																					{
																						if(((ulTime)/3600)==0)
																							szParam.Format("00"); // default
																						else
																							szParam.Format("%d", (ulTime)/3600); // default
																					}
																					else
																						szParam.Format("%d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600))); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('HH')")==0)
																				{
																					nNumStyles++;
																					if(b24)
																						szParam.Format("%02d", (ulTime)/3600); // default
																					else
																						szParam.Format("%02d", ((((ulTime)/3600)==0)?(12):((ulTime)/3600))); // default
																					szTemp = szParam;
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoW')")==0)
																				{ //i.e. Monday
																					nNumStyles++;

																					char buffer[48]; // need fewer;
																					long timeofthing = (long)(dblNumber);
																					tm* theTime = gmtime( &timeofthing );
																					strftime( buffer, 40, "%A", theTime );
																					szParam.Format("%s", buffer); 
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_pnucleus->m_data.m_timebTick.time - (g_pnucleus->m_data.m_timebTick.timezone*60) +(g_pnucleus->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoWex')")==0)
																				{ //i.e. Monday, but:
																					//if later today
																					//{ if > 6pm "tonight" else "later" }
																					// else if tomorrow "tomorrow" 
																					// else if > 1 week from now "coming"
																					// else "Monday" style day of week

																					// caution, this is not automation time.

																					unsigned long ulNow = (unsigned long)(g_pnucleus->m_data.m_timebTick.time - (g_pnucleus->m_data.m_timebTick.timezone*60) +(g_pnucleus->m_data.m_timebTick.dstflag?3600:0)); // local time....

																					if((ulNow/86400) == (((unsigned long)dblNumber)/86400)) // today
																					{
																						if((ulTime/3600)>17) // tonight, 6pm or later
																						{
																							szParam = "Tonight";
																						}
																						else
																						{
																							szParam = "Later";
																						}
																					}
																					else
																					if((ulNow/86400) == ((((unsigned long)dblNumber)/86400)-1)) // tomorrow
																					{
																						szParam = "Tomorrow";
																					}
																					else
																					if((ulNow/86400) <= ((((unsigned long)dblNumber)/86400)-7)) // next week
																					{
																						szParam = "Coming";
																					}
																					else
																					{
																						char buffer[48]; // need fewer;
																						long timeofthing = (long)dblNumber; 
																						tm* theTime = gmtime( &timeofthing );
																						strftime( buffer, 40, "%A", theTime );
																						szParam.Format("%s", buffer); 
																					}



																					nNumStyles++;
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_pnucleus->m_data.m_timebTick.time - (g_pnucleus->m_data.m_timebTick.timezone*60) +(g_pnucleus->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoWa')")==0)
																				{ //i.e. Monday
																					nNumStyles++;

																					char buffer[48]; // need fewer;
																					long timeofthing = (long)dblNumber; 
																					tm* theTime = gmtime( &timeofthing );
																					strftime( buffer, 40, "%a", theTime );
																					szParam.Format("%s", buffer); 
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_pnucleus->m_data.m_timebTick.time - (g_pnucleus->m_data.m_timebTick.timezone*60) +(g_pnucleus->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "formatTime('DoWaex')")==0)
																				{ //i.e. Monday, but:
																					//if later today
																					//{ if > 6pm "tonight" else "later" }
																					// else if tomorrow "tomorrow" 
																					// else if > 1 week from now "coming"
																					// else "Monday" style day of week

																					unsigned long ulNow = (unsigned long)(g_pnucleus->m_data.m_timebTick.time - (g_pnucleus->m_data.m_timebTick.timezone*60) +(g_pnucleus->m_data.m_timebTick.dstflag?3600:0)); // local time....

																					if((ulNow/86400) == (((unsigned long)dblNumber)/86400)) // today
																					{
																						if((ulTime/3600)>17) // tonight, 6pm or later
																						{
																							szParam = "Tonight";
																						}
																						else
																						{
																							szParam = "Later";
																						}
																					}
																					else
																					if((ulNow/86400) == ((((unsigned long)dblNumber)/86400)-1)) // tomorrow
																					{
																						szParam = "Tomorrow";
																					}
																					else
																					if((ulNow/86400) <= ((((unsigned long)dblNumber)/86400)-7)) // next week
																					{
																						szParam = "Coming";
																					}
																					else
																					{
																						char buffer[48]; // need fewer;
																						long timeofthing = (long)dblNumber; 
																						tm* theTime = gmtime( &timeofthing );
																						strftime( buffer, 40, "%a", theTime );
																						szParam.Format("%s", buffer); 
																					}


																					nNumStyles++;
																					szTemp = szParam;
																					//szTemp.Format("%d %d: %s",(long)dblNumber, (unsigned long)(g_pnucleus->m_data.m_timebTick.time - (g_pnucleus->m_data.m_timebTick.timezone*60) +(g_pnucleus->m_data.m_timebTick.dstflag?3600:0)), szParam);
																				}
																				else 
																				if(stricmp(paction, "appendAMPM('lcase')")==0)
																				{
																					szParam = szTemp;
																					nNumStyles++;
																					if(!bDuration)
																					{
																						if(szParam.GetLength())
																							szTemp.Format("%s %s", szParam, bPM?"pm":"am");
																						else
																							szTemp.Format("%s", bPM?"pm":"am");
																					}
																				}
																				else 
																				if(stricmp(paction, "appendAMPM('ucase')")==0)
																				{
																					szParam = szTemp;
																					nNumStyles++;
																					if(!bDuration)
																					{
																						if(szParam.GetLength())
																							szTemp.Format("%s %s", szParam, bPM?"PM":"AM");
																						else
																							szTemp.Format("%s", bPM?"PM":"AM");
																					}
																				}
	//else not nNumStyles++; which is why i had to do it that annoying way.


	/*
	Round,roundNumber()
	Truncate,truncateNumber()
	Filter Smut,smutFilter()
	Truncate To 20 Characters,truncateText(20)
	Truncate To 50 Characters,truncateText(50)
	Truncate To 100 Characters,truncateText(100)
*/

																				else 
																				if(stricmp(paction, "roundNumber()")==0)
																				{
																					dblNumber+= 0.5;
																					nNumStyles++;
																					szTemp.Format("%d", (int)dblNumber);
																				}
																				else 
																				if(stricmp(paction, "truncateNumber()")==0)
																				{
																					nNumStyles++;
																					szTemp.Format("%d", (int)dblNumber);
																				}
																				else 
																				if((strnicmp(paction, "truncateText(", 13)==0)&&(strlen(paction)>13))
																				{
																					int nTruncLen = atoi(paction+13);
																					nNumStyles++;
																					szParam = szTemp.Left(nTruncLen);
																					szTemp = szParam;
																				}

/*
	Convert All Text To Upper Case,textCase('upper')
	Convert All Text To Lower Case,textCase('lower')
	Convert First Letter Of First Word To Upper Case,textCase('sentence')
	Convert First Letter Of Text String To Upper Case,textCase('title')
*/
																				else 
																				if((strnicmp(paction, "textCase(", 9)==0)&&(strlen(paction)>9))
																				{
																					int nL=0;
																					bool bSpace = true;
																					char ch;
																					szParam = "";

																					while(nL<szTemp.GetLength())
																					{
																						ch = szTemp.GetAt(nL);
																						switch(*(paction+10))
																						{
																						case 'u':
																							{
																								szParam += toupper(ch);
																							} break;
																						case 'l':
																							{
																								szParam += tolower(ch);
																							} break;
																						case 's':
																							{
																								if((!(isspace(ch)))&&(bSpace))
																								{
																									bSpace = false;
																									szParam += toupper(ch);
																								}
																								else
																								{
																									szParam += ch;
																								}
																							} break;
																						case 't':
																							{
																								if(isspace(ch))
																								{
																									bSpace = true;
																									szParam += ch;
																								}
																								else
																								{
																									if(bSpace)
																									{
																										szParam += toupper(ch);
																									}
																									else
																									{
																										szParam += ch;
																									}
																									bSpace = false;
																								}
																							} break;
																						}
																						nL++;
																					}
																					nNumStyles++;
																					szTemp = szParam;
																				}


	/*
	Long Date Lower Case,formatDate('longdate', 'lcase')
	Long Date Upper Case,formatDate('longdate', 'ucase')
	Short Date Lower Case,formatDate('shortdate', 'lcase')
	Short Date Upper Case,formatDate('shortdate', 'ucase')
	Celsius,formatTemp('celsius')
	Fahrenheit,formatTemp('fahrenheit')
	*/
																				else 
																				if(stricmp(paction, "formatTemp('celsius')")==0)
																				{
																					nNumStyles++;
																					szTemp.Format("%d�C", (int)dblNumber);
																				}
																				else 
																				if(stricmp(paction, "formatTemp('fahrenheit')")==0)
																				{
																					nNumStyles++;
																					szTemp.Format("%d�F", (int)dblNumber);
																				}
																			
																				paction = sbu.Token(NULL, NULL, "|");
																			}
																			if(bTime) szValue += szTemp;
																		}
																		prsParam->Close();
																		delete prsParam;
																		prsParam = NULL;

																	}
																	else
																	{
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "SQL returned NULL recordset: %s", errorstring); // Sleep(50); //(Dispatch message)
																	}

																	if(nNumStyles==0)
																	{
																		if(bTime)
																		{
																			// format it the default way H:MM 24 hour clock
																			szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																			szValue += szTemp;
																		}
																		else 
																		{
																			szValue += szTemp;
																		}
																	}
																}
																else
																{
																	// if no style, need to understand if this is a time.
																	if(bTime)
																	{
																		// format it the default way H:MM 24 hour clock
																		szTemp.Format("%d:%02d", (ulTime)/3600, ((ulTime)%3600)/60);
																		szValue += szTemp;
																	}
																	else 
																	{
																		szValue += szTemp;
																	}
																}
															}
														}
														if(prsParam)
														{
															prsParam->Close();
															delete prsParam;
														}
													}
													else
													{
//	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "NEAR � returned NULL");  Sleep(50);//(Dispatch message)

													}
												}
											}
											else
											{
												szValue += ch;
												i++;
											}
										}
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "styles done");  Sleep(50); //(Dispatch message)

										if((!pnucleus->m_data.m_bNearEventsChanged)&&(!g_bKillThread))//&&(!pnucleus->m_data.m_bKillAutomationThread))
										{
										if(nTriggerItemID<=0)
										{
											if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
											{
											//insert
												char* chValue = dbSet.EncodeQuotes(szValue);
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, nucleus_eventid, Event_ActionID, value, flags, analysis_id) values \
(%d, %d, %d, '%s', %d, %d)",
													pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nNucleusEventItemID, nEventActionID, 
													chValue?chValue:szValue, 
													ulFlags, nAnalysisID
												);
												if(chValue) free(chValue);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_ANALYZE) 	
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis2", "Global Analysis SQL (export): %s", szSQL);  //Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "ERROR executing near SQL: %s", errorstring); // Sleep(50); //(Dispatch message)
												}
												//else break;
											}
										}
										else
										{
											// update
											if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
											{
												char* chValue = dbSet.EncodeQuotes(szValue);
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, nucleus_eventid = %d, Event_ActionID = %d, value = '%s', flags = %d, analysis_id = %d WHERE analyzed_trigger_id = %d",
														pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
														nAutomationItemID, nNucleusEventItemID, nEventActionID, 
														chValue?chValue:szValue, 
														ulFlags, nAnalysisID, nTriggerItemID
													);
												if(chValue) free(chValue);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_ANALYZE) 	
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis2", "Global Analysis SQL (export): %s", szSQL);  //Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "ERROR executing near SQL: %s", errorstring);  //Sleep(50); //(Dispatch message)
												}
											}
										}
										}
										else
										{
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_ANALYZE) 	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "if((!pnucleus->m_data.m_bNearEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)) = true"); // Sleep(50);
										}
									}
									else
									{
										// just set to renanalyzed
										ulFlags |= (NUCLEUS_FLAG_REANALYZED|NUCLEUS_FLAG_ANALYZED);
//										ulFlags = pnucleus->m_data.m_nAnalysisCounter;

										if((!pnucleus->m_data.m_bNearEventsChanged)&&(!g_bKillThread))//&&(!pnucleus->m_data.m_bKillAutomationThread))
										if(nTriggerItemID<=0)
										{
											if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
											{
											//insert
												char* chValue = dbSet.EncodeQuotes(szValue);
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s \
(automation_event_itemid, nucleus_eventid, Event_ActionID, value, flags, analysis_id) values \
(%d, %d, %d, '%s', %d, %d)",
													pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
													nAutomationItemID, nNucleusEventItemID, nEventActionID, 
													chValue?chValue:szValue,
													ulFlags, nAnalysisID
												);
												if(chValue) free(chValue);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_ANALYZE) 	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "SQL: %s", szSQL);//  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "ERROR executing near SQL: %s", errorstring);//  Sleep(50); //(Dispatch message)
												}// else break;
											}
										}
										else
										{
											// update
											char* chValue = dbSet.EncodeQuotes(szValue);
											if((nAutomationItemID>0)&&(nNucleusEventItemID>0)&&(nEventActionID>0))
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s \
SET automation_event_itemid = %d, nucleus_eventid = %d, Event_ActionID = %d, value = '%s', flags = %d, analysis_id=%d WHERE analyzed_trigger_id = %d",
														pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the TriggerAnalysisView table name
														nAutomationItemID, nNucleusEventItemID, nEventActionID, 
														chValue?chValue:szValue,
														ulFlags, nAnalysisID, nTriggerItemID
													);
												if(chValue) free(chValue);
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_ANALYZE) 	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "SQL: %s", szSQL);//  Sleep(50); //(Dispatch message)
												if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
												{
												//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "ERROR executing near SQL: %s", errorstring); // Sleep(50); //(Dispatch message)
												}
											}
										}

									}
								}
						
						if((!g_bKillThread))//&&(!pnucleus->m_data.m_bKillAutomationThread))
						{
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Analysis moving next");  Sleep(50); //(Dispatch message)
							prs->MoveNext();

							// make a short pause between records 
							_timeb timeCheck; 
							_ftime(&timeCheck); // the last time check inside the thread

							timeCheck.time += pnucleus->m_settings.m_nInterTriggerDelayMS/1000;
							timeCheck.millitm += pnucleus->m_settings.m_nInterTriggerDelayMS%1000;
							while(timeCheck.millitm>999)
							{
								timeCheck.millitm -= 1000;
								timeCheck.time++;
							}
							
							if(g_pnucleus->m_data.m_bRunningAnalyses) // means delay thread is waiting.
							{
								g_pnucleus->m_data.m_bRunningAnalyses = false;  // causes delay thread to trigger new trigger buffer, if active.
							}
							else // delay thread not waiting, but we want to spark it if we have a new analysis result in the trigger buffer.
							{

								if(
										(g_pnucleus->m_settings.m_bEnableAnalysisTriggerNotification)
									&&(
											((pAutoObj)&&(nNumRec<=pAutoObj->m_nNumChannelObjects*pnucleus->m_settings.m_nTriggerBuffer)) // aggressive but has to be, currently no way to go channel specific, so have to set them all to refresh.
										||(nNumRec<=pnucleus->m_settings.m_nTriggerBuffer)
										)
									)
								{

									g_pnucleus->m_data.m_bRunningAnalyses = false;

									if(!g_pnucleus->m_data.m_bDelayingTriggerNotification)
									{
										bool bGoNow = true;
	if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PROCESS) 	
		g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "  >>* Spawning NucleusTriggerDelayNotificationThread from analysis thread");  // Sleep(50);//(Dispatch message)
										if(_beginthread(NucleusTriggerDelayNotificationThread, 0, (void*)(&bGoNow))==-1)
										{
											//error.
							g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Error starting trigger delay notification thread from analysis");  // Sleep(10);//(Dispatch message)

											//**MSG
										}
									}


								}
							}


							while (
											(!pnucleus->m_data.m_bNearEventsChanged)&&(!g_bKillThread)
										&&(
												(pnucleus->m_data.m_timebNearTick.time<timeCheck.time)
											||((pnucleus->m_data.m_timebNearTick.time<timeCheck.time)&&(pnucleus->m_data.m_timebNearTick.millitm<timeCheck.millitm))
											)
										)// max of 3 second pause in between events (not between records inside event).
							{
		_ftime(&pnucleus->m_data.m_timebNearTick); // the last time check inside the thread
								Sleep(1);
							}

						}
					} //					while((!pnucleus->m_data.m_bNearEventsChanged)&&(!prs->IsEOF())&&(!g_bKillThread))//&&(!pnucleus->m_data.m_bKillAutomationThread))

					prs->Close();
					delete prs;
					prs = NULL;
					if(nNumRec==0)
					{
						if(g_pnucleus->m_data.m_bRunningAnalyses) // means delay thread is waiting.
						{
							g_pnucleus->m_data.m_bRunningAnalyses = false;  // causes delay thread to trigger new trigger buffer, if active.
						}
	if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_ANALYZE) 	
		g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Analysis waiting"); // Sleep(50); //(Dispatch message)
						// no records. just wait a few seconds before hitting the db again.
				//		int ixi=0;
						while((!pnucleus->m_data.m_bNearEventsChanged)&&(!g_bKillThread)) // &&(!pnucleus->m_data.m_bKillAutomationThread)&&(ixi<5000))  // max of 5 second pause on no records.
						{
	_ftime(&pnucleus->m_data.m_timebNearTick); // the last time check inside the thread
							Sleep(1);
				//			ixi++;
						}
	if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_ANALYZE) 	
		g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Analysis finished waiting"); // Sleep(50); //(Dispatch message)
					}
					else
					{
						_timeb timeCheck; 
						_ftime(&timeCheck); // the last time check inside the thread

						timeCheck.time += pnucleus->m_settings.m_nInterEventDelayMS/1000;
						timeCheck.millitm += pnucleus->m_settings.m_nInterEventDelayMS%1000;
						while(timeCheck.millitm>999)
						{
							timeCheck.millitm -= 1000;
							timeCheck.time++;
						}
						
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Analysis waiting in between events");  //Sleep(50); //(Dispatch message)
						while (
										(!pnucleus->m_data.m_bNearEventsChanged)&&(!g_bKillThread)
									&&(
											(pnucleus->m_data.m_timebNearTick.time<timeCheck.time)
										||((pnucleus->m_data.m_timebNearTick.time<timeCheck.time)&&(pnucleus->m_data.m_timebNearTick.millitm<timeCheck.millitm))
										)
									)// max of 3 second pause in between events (not between records inside event).
						{
	_ftime(&pnucleus->m_data.m_timebNearTick); // the last time check inside the thread
							Sleep(1);
						}
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Analysis finished waiting in between events"); // Sleep(50); //(Dispatch message)
					}

				}//if(prs != NULL)
				else
				{
					if(g_pnucleus->m_data.m_bRunningAnalyses) // means delay thread is waiting.
					{
						g_pnucleus->m_data.m_bRunningAnalyses = false;  // causes delay thread to trigger new trigger buffer, if active.
					}

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Near analysis resultset was null! %s", errorstring); // Sleep(50);//(Dispatch message)
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_global_analysis", "Global Analysis ERROR:  %s", errorstring); // Sleep(50);//(Dispatch message)
					Sleep(1000); // an exception occurred.
				}

			}//			while((!pnucleus->m_data.m_bNearEventsChanged)&&(!bChangesPending)&&(!g_bKillThread))//&&(!pnucleus->m_data.m_bKillAutomationThread))

		} //		if(
//			  (pnucleus->m_data.m_nIndexAutomationEndpoint>=0)
//			&&(pnucleus->m_settings.m_nNumEndpointsInstalled>0)
///			&&(pnucleus->m_settings.m_ppEndpointObject)
//			&&(pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint])
//			&&(!pnucleus->m_data.m_bProcessSuspended)
//			&&(pnucleus->m_data.m_key.m_bValid)  // must have a valid license
//			&&(
//					(!pnucleus->m_data.m_key.m_bExpires)
//				||((pnucleus->m_data.m_key.m_bExpires)&&(!pnucleus->m_data.m_key.m_bExpired))
//				||((pnucleus->m_data.m_key.m_bExpires)&&(pnucleus->m_data.m_key.m_bExpireForgiveness)&&(pnucleus->m_data.m_key.m_ulExpiryDate+pnucleus->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
//				)
//			&&(
//					(!pnucleus->m_data.m_key.m_bMachineSpecific)
//				||((pnucleus->m_data.m_key.m_bMachineSpecific)&&(pnucleus->m_data.m_key.m_bValidMAC))
//				)
//			)

		Sleep(1); // dont peg processor
	} // while((!g_bKillThread)&&(!pnucleus->m_data.m_bKillAutomationThread))
	db.RemoveConnection(pdbConn);
	dbSet.RemoveConnection(pdbConnSet);
	pnucleus->m_data.m_bGlobalAnalysisThreadStarted=false;
	
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending global analysis thread");
	g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusGlobalAnalysisThread", errorstring);    //(Dispatch message)

	_endthread();
	Sleep(150);

}

void NucleusTriggerDelayNotificationThread(void* pvArgs)
{
	if(g_pnucleus->m_data.m_bDelayingTriggerNotification == true) return; // only 1 allowed
	g_pnucleus->m_data.m_bDelayingTriggerNotification = true;
	//delay notification here
	bool* pbGo = (bool*) pvArgs;
	if((pbGo!=NULL)&&((*pbGo)==true))
	{
		if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) 
			g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Signalling trigger buffer refresh"); //(Dispatch message)
	}
	else
	{
		if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) 
			g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Delaying trigger buffer refresh"); //(Dispatch message)
		while((!g_bKillThread)&&(g_pnucleus->m_data.m_bRunningAnalyses))
		{
			Sleep(10);// wait until done
		}

		g_pnucleus->m_data.m_bRunningAnalyses = true;
		if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) 
			g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Signalling trigger buffer delay ready"); //(Dispatch message)

		while((!g_bKillThread)&&(g_pnucleus->m_data.m_bRunningAnalyses))
		{
			Sleep(10);
		}
		if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) 
			g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Signalling trigger buffer refresh after delay"); //(Dispatch message)

	}

	// now, dwell!
	_timeb timebChange;
	_ftime(&timebChange);

	if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PROCESS) 
		g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "  --* Dwell trigger buffer notification thread"); //(Dispatch message)
	while(!g_bKillThread)
	{
		if  (
					(g_pnucleus->m_data.m_timebTick.time > (timebChange.time+g_pnucleus->m_settings.m_nTriggerNotificationDwellMS/1000))
				||(
					  (g_pnucleus->m_data.m_timebTick.time==(timebChange.time+g_pnucleus->m_settings.m_nTriggerNotificationDwellMS/1000))
					&&(g_pnucleus->m_data.m_timebTick.millitm>(timebChange.millitm+g_pnucleus->m_settings.m_nTriggerNotificationDwellMS%1000))
					)
				)
		{
			 break;
		}

		Sleep(1);
		_ftime(&timebChange);

	}
	if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PROCESS) 
		g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "  --* End dwell trigger buffer notification thread"); //(Dispatch message)

	if(
			(g_pnucleus->m_settings.m_ppEndpointObject)
		&&(g_pnucleus->m_data.m_nIndexAutomationEndpoint>=0)
		&&(g_pnucleus->m_data.m_nIndexAutomationEndpoint<g_pnucleus->m_settings.m_nNumEndpointsInstalled)
		&&(g_pnucleus->m_settings.m_ppEndpointObject[g_pnucleus->m_data.m_nIndexAutomationEndpoint])
		&&(!g_bKillThread)
		)
	{
		CNucleusEndpointObject* pAutoObj = g_pnucleus->m_settings.m_ppEndpointObject[g_pnucleus->m_data.m_nIndexAutomationEndpoint];

//		if(pAutoObj) pAutoObj->m_nLastModLiveEvents = -1;  // triggers global analysis rules.
		if((pAutoObj->m_ppChannelObj)&&(pAutoObj->m_nNumChannelObjects))
		{
			int cho=0;
			while((cho<pAutoObj->m_nNumChannelObjects)&&(!g_bKillThread))
			{
				if(pAutoObj->m_ppChannelObj[cho])
				{
						EnterCriticalSection(&pAutoObj->m_ppChannelObj[cho]->m_critChVar); // added 2.2.1.21 (15 July 2016)

						if(
							  (	pAutoObj->m_ppChannelObj[cho]->m_bTriggerEventsChangedLocal)  // // added 2.2.1.21 (15 July 2016) to prevent all changes on all channels from global thread.			
								||(!g_pnucleus->m_settings.m_bAutomationChangesDoNotCrossChannels)  // if not exclusion, just set it
							)
							pAutoObj->m_ppChannelObj[cho]->m_bTriggerEventsChanged = true;
//										pAutoObj->m_ppChannelObj[cho]->m_bNearEventsChanged = true;

						LeaveCriticalSection(&pAutoObj->m_ppChannelObj[cho]->m_critChVar); // added 2.2.1.21 (15 July 2016)
				}
				cho++;
			}
		}
	}
	if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_PROCESS) 
		g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "  <<* Ending trigger buffer notification thread"); //(Dispatch message)
	
	g_pnucleus->m_data.m_bDelayingTriggerNotification = false;

	_endthread();
}

void NucleusTriggerThread(void* pvArgs)
{
	CNucleusAutomationChannelObject* pChannelObj = (CNucleusAutomationChannelObject*) pvArgs;
	if(pChannelObj==NULL) return;
	CNucleusEndpointObject* pEndObj = (CNucleusEndpointObject*) pChannelObj->m_pEndpoint;
	if(pEndObj==NULL) return;
	CNucleusMain* pnucleus = (CNucleusMain*) pChannelObj->m_pNucleus;
	if(pnucleus==NULL) return;

	pChannelObj->m_bTriggerThreadStarted=true;
	char szCommandSQL[DB_SQLSTRING_MAXLEN];
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	char pluserrorstring[MAX_MESSAGE_LENGTH];
	char autosource[MAX_MESSAGE_LENGTH];
	char debugsource[MAX_MESSAGE_LENGTH];


	char lebuf[256];
	sprintf(lebuf, "Logs\\N%d.txt", pChannelObj->m_nChannelID);
	_timeb tmb;
	FILE* fpdbg = NULL; //in line file debug

	if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
	{
		fpdbg = fopen(lebuf, "ab");

		if(fpdbg)
		{
			_ftime(&tmb);
			fprintf(fpdbg, "[%d> %d.%03d START\r\n\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
			fclose(fpdbg);
			fpdbg = NULL;
		}

	}

	// not sensitive to a settings change.... could make it, someday.
	char TabulatorToken[MAX_PATH];
	int nTabulatorTokenLen=0;
	if((g_pnucleus->m_settings.m_pszTabulatorModule)&&(strlen(g_pnucleus->m_settings.m_pszTabulatorModule)))
	{
		_snprintf(TabulatorToken, MAX_PATH, "%s:", g_pnucleus->m_settings.m_pszTabulatorModule);
	}
	else
	{
		strcpy(TabulatorToken, "Tabulator:");
	}	
	
	nTabulatorTokenLen = strlen(TabulatorToken);

	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
	{
		sprintf(autosource, "%s:%d Trigger", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
		sprintf(debugsource, "[%s:%d|%x>", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum, GetCurrentThreadId());
	}
	else
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
	{
		sprintf(autosource, "Omnibus %d Trigger", pChannelObj->m_nChannelID);
		sprintf(debugsource, "[%d|%x>", pChannelObj->m_nChannelID, GetCurrentThreadId());
	}



	unsigned long ulAutoType  = 0;

	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
	{
		ulAutoType = NUCLEUS_DEP_AUTO_SENTINEL;
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Beginning trigger thread for %s:%d", debugsource, pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	}
	else
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
	{
		ulAutoType = NUCLEUS_DEP_AUTO_HELIOS;
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Beginning trigger thread for Omnibus stream %d", debugsource, pChannelObj->m_nChannelID);
	}
	g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusTriggerThread", errorstring);    //(Dispatch message)

	strcpy(errorstring, "");
	CDBUtil* pdb = &pChannelObj->m_db;
	CDBUtil dbSet;

	pChannelObj->m_pdbConn = pdb->CreateNewConnection(pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW);
	if(pChannelObj->m_pdbConn)
	{
		if(pdb->ConnectDatabase(pChannelObj->m_pdbConn, errorstring)<DB_SUCCESS)
		{
			sprintf(pluserrorstring, "%s %s", debugsource, errorstring);
			pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:trigger_database_connect", pluserrorstring);  //(Dispatch message)
			pChannelObj->m_pdbConn = pnucleus->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Failed to create connection for %s:%s:%s", debugsource, pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW); 
		pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:trigger_database_init", errorstring);  //(Dispatch message)
		pChannelObj->m_pdbConn = pnucleus->m_data.m_pdbConn;

		//**MSG
	}
	CDBconn* pdbConnSet = dbSet.CreateNewConnection(pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW);
	if(pdbConnSet)
	{
		if(dbSet.ConnectDatabase(pdbConnSet, errorstring)<DB_SUCCESS)
		{
			sprintf(pluserrorstring, "%s %s", debugsource, errorstring);
			pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:analysis_database2_connect", pluserrorstring);  //(Dispatch message)
			pdbConnSet = pnucleus->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Failed to create connection for %s:%s:%s", debugsource, pnucleus->m_settings.m_pszDSN, pnucleus->m_settings.m_pszUser, pnucleus->m_settings.m_pszPW); 
		pnucleus->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Nucleus:analysis_database2_init", errorstring);  //(Dispatch message)
		pdbConnSet = pnucleus->m_data.m_pdbConn;

		//**MSG
	}


	// load the damn DEMO scene.



/*
	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 Host FROM %s WHERE Host IS NOT NULL",
			pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
			NUCLEUS_FLAG_TRIGGERED
		);

	CString szHostLoad = pnucleus->m_settings.m_pszDefaultHost;
	CRecordset* prs = pdb->Retrieve(pChannelObj->m_pdbConn, szSQL, errorstring);
	if(prs != NULL)
	{
		if(!prs->IsEOF())
		{
			prs->GetFieldValue("Host", szHostLoad);
		}
		prs->Close();
		delete prs; prs=NULL;
	}


	char proj[256]; sprintf(proj, "0|%s|%s", pnucleus->m_settings.m_pszDefaultProject, pnucleus->m_settings.m_pszDefaultScene);

	char* pchEncodedLocal = pdb->EncodeQuotes(proj);
	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(local, remote, action, host, timestamp, username) \
VALUES ('%s','', %d, '%s', %d.%d, 'sys')", //HARDCODE
															"Radiance",
															"Command_Queue",
															(pchEncodedLocal?pchEncodedLocal:"0|Promo_B|Promo_B"),
															256,
															szHostLoad,
															g_pnucleus->m_data.m_timebTick.time,
															g_pnucleus->m_data.m_timebTick.millitm
															);
	if(pchEncodedLocal) free(pchEncodedLocal);

g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ScheduleEndpointQueue SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
//		char errorstring[DB_ERRORSTRING_LEN]; if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
	if(pdb->ExecuteSQL(pChannelObj->m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
	{
	//**MSG
//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
	}

*/
/*

	char proj[256]; sprintf(proj, "%s%c%s%c%s", 
		pnucleus->m_settings.m_pszDefaultHost, 28,
		pnucleus->m_settings.m_pszDefaultProject, 28,
		pnucleus->m_settings.m_pszDefaultScene);

	while((g_pnucleus->m_data.SendGraphicsCommand(RADIANCE_CMD_LOADSCENE, proj)<0)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
	{
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR RADIANCE_CMD_LOADSCENE: %s", proj);  Sleep(50); //(Dispatch message)
		int i=0;
		while ((i<100)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)) Sleep(100);
	}

	while(!pnucleus->m_data.m_bTriggerEventsChanged) Sleep(100);

	Sleep(60000); // dont start triggering util after 1 min
*/

/*
	// clear the old events.
	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
			pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData"   // the AnalyzedTriggerData table name
	
		);
*/
/*
	while((pdb->ExecuteSQL(pChannelObj->m_pdbConn, szSQL, errorstring)<DB_SUCCESS)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
	{
	//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)
	}
*/

	CIS2Core is2; // for util functions only.
	CBufferUtil bu;

	pChannelObj->m_ppEvents = new CNucleusEvent*[128];
	pChannelObj->m_nEvents = 0;

	if(pChannelObj->m_ppEvents)
	{
		while( pChannelObj->m_nEvents < 128)
		{
			pChannelObj->m_ppEvents[pChannelObj->m_nEvents] = NULL; // initialize
			CNucleusEvent* pEvent = new CNucleusEvent;
			if(pEvent)
			{
				pChannelObj->m_ppEvents[pChannelObj->m_nEvents] = pEvent;
				pChannelObj->m_nEvents++;
			}
			else break; // no infinite loop.
		}
	}

	pChannelObj->m_ppNewEvents = new CNucleusEvent*[128];
	pChannelObj->m_nNewEvents = 0;

	if(pChannelObj->m_ppNewEvents)
	{
		while( pChannelObj->m_nNewEvents < 128)
		{
			pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents] = NULL; // initialize
			CNucleusEvent* pEvent = new CNucleusEvent;
			if(pEvent)
			{
				pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents] = pEvent;
				pChannelObj->m_nNewEvents++;
			}
			else break; // no infinite loop.
		}
	}

	//reset these counts!
	pChannelObj->m_nEvents = 0;
	pChannelObj->m_nNewEvents = 0;

	EnterCriticalSection(&pnucleus->m_data.m_critSettings);

	pChannelObj->m_nMinLyricAlias=pnucleus->m_settings.m_nMinLyricAlias;
	pChannelObj->m_nMaxLyricAlias=pnucleus->m_settings.m_nMaxLyricAlias;

	LeaveCriticalSection(&pnucleus->m_data.m_critSettings);

	bool bUseLyric = (pnucleus->m_settings.m_nMaxLyricAlias>0);  // zero max means do not use.

	int nAliases = pChannelObj->m_nMaxLyricAlias - pChannelObj->m_nMinLyricAlias + 1;
	if(nAliases<=0) nAliases = 10; //min set
	pChannelObj->m_ppLyricAlias = new LyricAlias_t*[nAliases];
	pChannelObj->m_nNumAliasArray = 0;

	if(pChannelObj->m_ppLyricAlias)
	{
		while( pChannelObj->m_nNumAliasArray < nAliases)
		{
			LyricAlias_t* pAlias = new LyricAlias_t;
			if(pAlias)
			{
				pChannelObj->m_ppLyricAlias[pChannelObj->m_nNumAliasArray] = pAlias;
				pAlias->szMessage = "";
				pAlias->nAlias = -1;
				pAlias->bMessageLoaded = false;
				pAlias->bMessageRead = false;

				pChannelObj->m_nNumAliasArray++;
			}
			else break; // no infinite loop.
		}
	}

	CString szLastID;
	double delta = 0.0;
	double wait = 0.0;

	EnterCriticalSection(&pChannelObj->m_critTriggeredIDs);

	while((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
	{
		if(
/*
			  (pnucleus->m_data.m_nIndexAutomationEndpoint>=0)
			&&(pnucleus->m_settings.m_nNumEndpointsInstalled>0)
			&&(pnucleus->m_data.m_nIndexAutomationEndpoint<pnucleus->m_settings.m_nNumEndpointsInstalled)
			&&(pnucleus->m_settings.m_ppEndpointObject)
			&&(pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint])
			&&*/
			  (!pnucleus->m_data.m_bProcessSuspended)
			&&(

					(
						(ulAutoType == NUCLEUS_DEP_AUTO_SENTINEL)
					&&(pChannelObj->m_nListState&((1<<LISTTHREADING)|(1<<LISTISPLAYING)))  // list must be threaded and playing to trigger
					&&(!(pChannelObj->m_nListState&((1<<LISTINFREEZE)|(1<<LISTINHOLD))))  // list must not be in freeze or hold to trigger
					)
				||(
						(ulAutoType == NUCLEUS_DEP_AUTO_HELIOS)
					&&(pChannelObj->m_nListState!=0)  // list must be connected
//					&&(pChannelObj->m_nServerStatus!=0)  // server must be connected!  // comment this out to surivive adaptor disconnection retries....
					)
				)
			&&(pnucleus->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!pnucleus->m_data.m_key.m_bExpires)
				||((pnucleus->m_data.m_key.m_bExpires)&&(!pnucleus->m_data.m_key.m_bExpired))
				||((pnucleus->m_data.m_key.m_bExpires)&&(pnucleus->m_data.m_key.m_bExpireForgiveness)&&(pnucleus->m_data.m_key.m_ulExpiryDate+pnucleus->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!pnucleus->m_data.m_key.m_bMachineSpecific)
				||((pnucleus->m_data.m_key.m_bMachineSpecific)&&(pnucleus->m_data.m_key.m_bValidMAC))
				)

			)
		{


			if(pnucleus->m_settings.m_bUseAsynchTriggerRetrieve)
			{
				if(pChannelObj->m_bTriggerQueryThreadStarted == false)
				{
					if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Starting trigger query thread for %s:%d", debugsource, pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
					}
					else
					if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Starting trigger query thread for Omnibus stream %d", debugsource, pChannelObj->m_nChannelID);
					}
					g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusTriggerThread", errorstring);    //(Dispatch message)
					
					
					if(_beginthread(NucleusTriggerQueryThread, 0, (void*)pChannelObj)==-1)
					{
						if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s ERROR starting trigger query thread for %s:%d", debugsource, pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
						}
						else
						if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s ERROR starting trigger query thread for Omnibus stream %d", debugsource, pChannelObj->m_nChannelID);
						}
						g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusTriggerThread", errorstring);    //(Dispatch message)

						//**MSG
					}
					else
					{
						// stall here until the thread actually starts up, so as not to start more of them
						while((!pChannelObj->m_bTriggerQueryThreadStarted)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
						{
							Sleep(1);
						}
					}

					//**** should check return value....
				}
			}


		//			CNucleusEndpointObject* pAutoObj = pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint];


			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg==NULL) fpdbg = fopen(lebuf, "ab");

				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "[%d> %d.%03d Q>\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}

			CRecordset* prs = NULL;
			if(pnucleus->m_settings.m_bUseAsynchTriggerRetrieve)
			{
//				EnterCriticalSection(&pChannelObj->m_critChVar);
	EnterCriticalSection(&pChannelObj->m_critChVar);  // lock it out.

				if(pChannelObj->m_bNewEventRetrieve)
				{
					//swap!
					CNucleusEvent** ppEvents = pChannelObj->m_ppEvents;

					pChannelObj->m_ppEvents = pChannelObj->m_ppNewEvents;
					pChannelObj->m_nEvents = pChannelObj->m_nNewEvents;

					pChannelObj->m_ppNewEvents = ppEvents;
					pChannelObj->m_nNewEvents = 0;

					prs= (CRecordset*)1; //fake, gets reset later.

				}
	LeaveCriticalSection(&pChannelObj->m_critChVar);
			}
			else
			{
			///// DEMOTODO get status col name in view
/*
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d * FROM %s WHERE server = '%s' AND listid = %d AND \
AnalyzedTriggerDataStatus & %d = 0 \
AND analyzed_trigger_id IS NOT NULL ORDER BY TriggerTime ASC",  pnucleus->m_settings.m_nTriggerBuffer,
					pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
					pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListID, NUCLEUS_FLAG_TRIGGERED
				);
*/

			EnterCriticalSection(&pChannelObj->m_critChVar);
				
			if(ulAutoType == NUCLEUS_DEP_AUTO_SENTINEL)
			{
				if(pnucleus->m_settings.m_bUseTriggerSourceView)
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d LiveEventData.itemid, (select host from %s.dbo.Destinations where channelid = %d) as Host, TriggerSourceView.ParsedValue, TriggerSourceView.TriggerTime, \
TriggerSourceView.analyzed_trigger_id, TriggerSourceView.Source, TriggerSourceView.Scene, TriggerSourceView.Graphic_Action_Type, TriggerSourceView.Action_Name, \
LiveEventData.list_id AS channelid, LiveEventData.event_clip, LiveEventData.event_title, TriggerSourceView.event_name, '4110' AS dest_type, 'libretto' AS dest_module, \
TriggerSourceView.layer, LiveEventData.event_duration, LiveEventData.event_data, LiveEventData.event_id \
from (select itemid, list_id, event_clip, event_title, event_duration, event_data, event_id from sentinel.dbo.Events where list_id = %d and ((event_status & %d) = 0)) \
AS LiveEventData inner join %s.dbo.TriggerSourceView as TriggerSourceView \
ON LiveEventData.itemid = TriggerSourceView.automation_event_itemid \
WHERE TriggerTime > %.3f \
ORDER BY TriggerTime ASC",

					  pnucleus->m_settings.m_nTriggerBuffer,
					  "libretto", //libretto
					  pChannelObj->m_nChannelID, pChannelObj->m_nChannelID,
						pnucleus->m_settings.m_nHarrisStatusTriggerExclude,
						g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus",
						pChannelObj->m_dblAutomationTimeEstimate - ((double)pnucleus->m_settings.m_nTriggerExclusionDelayMS)/1000.0
					);

						
				}
				else
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d * FROM %s WHERE channelid = %d AND \
AnalyzedTriggerDataStatus & %d = 0 and AnalyzedTriggerDataFlags & %d <> 0 and ((event_status & %d) = 0)\
AND TriggerTime > %.3f \
AND analyzed_trigger_id IS NOT NULL ORDER BY TriggerTime ASC",  pnucleus->m_settings.m_nTriggerBuffer,
						pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
						pChannelObj->m_nChannelID, NUCLEUS_FLAG_TRIGGERED, NUCLEUS_FLAG_ANALYZED,
						pnucleus->m_settings.m_nHarrisStatusTriggerExclude,
						pChannelObj->m_dblAutomationTimeEstimate - ((double)pnucleus->m_settings.m_nTriggerExclusionDelayMS)/1000.0
					);
				}

///			((event_status & 17) = 0) // not done or postrolled.  // we want ones that are playing or yet to be played

// SENTINEL_EVENT_POSTUPCOUNT = 0x00100000 = 1048576 decimal.
// now to suppress post upcount events, we call ((event_status & 1048593) = 0) = not done or postrolled or post upcounter
			}
			else
			if(ulAutoType == NUCLEUS_DEP_AUTO_HELIOS)
			{
				if(pnucleus->m_settings.m_bUseTriggerSourceView)
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d LiveEventData.itemid, (select host from %s.dbo.Destinations where channelid = %d) as Host, TriggerSourceView.ParsedValue, TriggerSourceView.TriggerTime, \
TriggerSourceView.analyzed_trigger_id, TriggerSourceView.Source, TriggerSourceView.Scene, TriggerSourceView.Graphic_Action_Type, TriggerSourceView.Action_Name, \
LiveEventData.list_id AS channelid, LiveEventData.event_clip, LiveEventData.event_title, TriggerSourceView.event_name, '4110' AS dest_type, 'libretto' AS dest_module, \
TriggerSourceView.layer, LiveEventData.event_duration, LiveEventData.event_data, LiveEventData.event_id \
from (select itemid, list_id, event_clip, event_title, event_duration, event_data, event_id from helios.dbo.Events where list_id = %d and event_status < 10 and event_status > 4) \
AS LiveEventData inner join %s.dbo.TriggerSourceView as TriggerSourceView \
ON LiveEventData.itemid = TriggerSourceView.automation_event_itemid \
WHERE  TriggerTime > %.3f \
ORDER BY TriggerTime ASC",

					  pnucleus->m_settings.m_nTriggerBuffer,
					  "libretto", //libretto
					  pChannelObj->m_nChannelID, pChannelObj->m_nChannelID,
						g_pnucleus->m_settings.m_pszDefaultDB?g_pnucleus->m_settings.m_pszDefaultDB:"Nucleus",
						pChannelObj->m_dblAutomationTimeEstimate - ((double)pnucleus->m_settings.m_nTriggerExclusionDelayMS)/1000.0
					);

				}
				else
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d * FROM %s WHERE channelid = %d AND \
AnalyzedTriggerDataStatus & %d = 0  and AnalyzedTriggerDataFlags & %d <> 0 and event_status < 10 and event_status > 4 \
AND TriggerTime > %.3f \
AND analyzed_trigger_id IS NOT NULL ORDER BY TriggerTime ASC",  pnucleus->m_settings.m_nTriggerBuffer,
						pnucleus->m_settings.m_pszTriggerAnalysisView?pnucleus->m_settings.m_pszTriggerAnalysisView:"TriggerAnalysisView",   // the TriggerAnalysisView table name
						pChannelObj->m_nChannelID, NUCLEUS_FLAG_TRIGGERED, NUCLEUS_FLAG_ANALYZED,
						pChannelObj->m_dblAutomationTimeEstimate - ((double)pnucleus->m_settings.m_nTriggerExclusionDelayMS)/1000.0
					);
				}

///			event_status < 10 and event_status >4 // not done or postrolledand no error  // we want ones that are playing or yet to be played and without errors
			}
			LeaveCriticalSection(&pChannelObj->m_critChVar);

			
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "[%d> %d.%03d R>\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}
	_ftime(&pChannelObj->m_timebTriggerTick); // the last time check inside the thread
//AND TriggerTime >= %lf \
//					pnucleus->m_data.m_dblAutomationTimeEstimate
//AND (event_status & 26  <> 0 ) // have to remove in order to get the backtimed ones
						
		//	&18<>0 = must be postrolled or playing
		//	&26<>0 = must be postrolled or playing - uses preroll because now we are running on calculated event trigger times
			// cant use pre-rol because time is not correct yet

if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_trigger", "%s trigger SQL: %s", debugsource, szSQL); 

//2.1.1.21 removed crit sec  - timeouts on other channels blocked this!
//			EnterCriticalSection(&pnucleus->m_data.m_critSQL);
			
				prs = pdb->Retrieve(pChannelObj->m_pdbConn, szSQL, errorstring);

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "[%d> %d.%03d R<\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}

			}


/*
			nEvents = 0;
			if(prs != NULL)
			{}
				pnucleus->m_data.m_bTriggerEventsChanged = false;
				while((!pnucleus->m_data.m_bTriggerEventsChanged)&&(!prs->IsEOF())&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
				{
					CString szHost;
					CString szName;
					CString szValue;
					CString szTemp;
					double dblTime;
					int nType=-1;
					int nTriggerItemID=-1;
					int nAutomationItemID=-1;
					prs->GetFieldValue("itemid", szTemp);
					nAutomationItemID=atol(szTemp);
					prs->GetFieldValue("Host", szHost);
					prs->GetFieldValue("ParsedValue", szValue);
					prs->GetFieldValue("TriggerTime", szTemp);
					dblTime=atof(szTemp) - 0.333;  // ten frames anticipation.
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Got: %s, %f", szValue, dblTime);  Sleep(50);//(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "m_dblAutomationTimeEstimate>=dblTime?: %f %f", pnucleus->m_data.m_dblAutomationTimeEstimate, dblTime);  Sleep(50);//(Dispatch message)
					while((pnucleus->m_data.m_dblAutomationTimeEstimate<dblTime)&&(!pnucleus->m_data.m_bTriggerEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
					{
						Sleep(1);
					}

					if((!pnucleus->m_data.m_bTriggerEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))
					{
						delta = 0.0;
						prs->GetFieldValue("analyzed_trigger_id", szTemp);
						if(szTemp.GetLength()) nTriggerItemID = atoi(szTemp);
						prs->GetFieldValue("Graphic_Action_Type", szTemp);
						nType = atoi(szTemp);
						prs->GetFieldValue("Action_Name", szName);

//						prs->MoveNext();
//						prs->Close();
//						delete prs;
//						prs = NULL;
						// do it.
/*
						szTemp.Format("%d|%s|%s", nType+1, szName, szValue);
						char* pchEncodedLocal = dbSet.EncodeQuotes(szTemp.GetBuffer(1));
						szTemp.ReleaseBuffer();
* /
						char proj[256]; sprintf(proj, "%s%c%s%c%s", 
							pnucleus->m_settings.m_pszDefaultHost, 28,
							szName, 28,
							szValue);

						if(g_pnucleus->m_data.SendGraphicsCommand(((nType==0)?RADIANCE_CMD_PLAYANIM:RADIANCE_CMD_SETEXPORT), proj)<0)
						{
pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "error sending command: %s", proj); // Sleep(50);//(Dispatch message)
						}
						else
						{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "command sent: %s", proj); // Sleep(50);//(Dispatch message)
						}

/*
						if((nTriggerItemID>0)&&(szName.GetLength()))
						{
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "pchEncodedLocal: %s", pchEncodedLocal);  Sleep(50);//(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "want to send %s", szTemp);  Sleep(50);//(Dispatch message)

							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(local, remote, action, host, timestamp, username) \
VALUES ('%s','', %d, '%s', %d.%d, 'sys')", //HARDCODE
																"Radiance",
																"Command_Queue",
																pchEncodedLocal?pchEncodedLocal:szTemp,
																256,
																szHost,
																g_pnucleus->m_data.m_timebTick.time,
																g_pnucleus->m_data.m_timebTick.millitm
																);
							if(pchEncodedLocal) free(pchEncodedLocal);
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "cq SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
							if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
							{
						//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
							}

						* /

							// now update status
							///// DEMOTODO get item id name in view
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d WHERE analyzed_trigger_id = %d",
									pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the AnalyzedTriggerData table name
									NUCLEUS_FLAG_TRIGGERED, nTriggerItemID
								);

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "AnalyzedTriggerDataStatus SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
							if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
							{
							//**MSG
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing trigger	SQL: %s", errorstring);  Sleep(50); //(Dispatch message)
							}

					//	}

						nEvents++;

						prs->MoveNext();
					}
				}
				*/

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "[%d> %d.%03d S\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}

			bool bNewRetrieve = false;
			bool bInvalidTimes = false;

			if(prs != NULL) // was set to "1" if events are async and ready.
			{
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					fprintf(fpdbg, "X1", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}
				if(pnucleus->m_settings.m_bUseAsynchTriggerRetrieve)// added 2.2.1.21 (17 July 2016)
				{
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					fprintf(fpdbg, "X2", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}
					prs=NULL; // just reset this fake thing.

//					pChannelObj->m_bTriggerEventsChanged = false;
//					pChannelObj->m_bTriggerEventsChangedLocal = true;

	EnterCriticalSection(&pChannelObj->m_critChVar);  // lock it out.
					pChannelObj->m_bNewEventRetrieve=false;  // and this real thing
	LeaveCriticalSection(&pChannelObj->m_critChVar);  // lock it out.
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					fprintf(fpdbg, "X3", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}
				}
				else
				{
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					fprintf(fpdbg, "Y1", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}

//||((pChannelObj->m_bNewEventRetrieve)&&(pnucleus->m_settings.m_bUseAsynchTriggerRetrieve))

	EnterCriticalSection(&pChannelObj->m_critChVar);  // lock it out.
				// reset the trigger count.
				pChannelObj->m_nEvents = 0;

/*
			[itemid]
     ,[Host]
     ,[ParsedValue]
     ,[TriggerTime]
     ,[analyzed_trigger_id]
     ,[AnalyzedTriggerDataFlags]
     ,[AnalyzedTriggerDataStatus]
     ,[event_status]
     ,[nucleus_EventID]
     ,[nucleus_Timing_Col_Name]
     ,[Source]
     ,[Scene]
     ,[Graphic_Action_Type]
     ,[Action_Name]
     ,[Event_ActionID]
     ,[ActionTriggerTime]
     ,[Action_Value]
     ,[Analysis_ID]
     ,[channelid]
     ,[event_calc_start]
     ,[event_clip]
     ,[event_title]
     ,[event_name]
     ,[parent_calc_start]
     ,[event_start]
     ,[automation_event_itemid]
     ,[dest_type]
     ,[dest_module]
     ,[layer]
     ,[field_count]
     ,[event_duration]
     ,[event_data]
     ,[event_id]
				
*/
				pChannelObj->m_bTriggerEventsChanged = false;
				pChannelObj->m_bTriggerEventsChangedLocal = true;// added 2.2.1.21 (15 July 2016)

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					fprintf(fpdbg, "Y2", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}			
				while(
					     (!bNewRetrieve)&&(!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)
						 )
				{

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					fprintf(fpdbg, "r1", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}
	_ftime(&pChannelObj->m_timebTriggerTick); // the last time check inside the thread
					CString szTemp;
//					int nAutomationItemID=-1;
					prs->GetFieldValue("itemid", szTemp);

					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_nEventID = atoi(szTemp);
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					fprintf(fpdbg, "2", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}
					prs->GetFieldValue("Host", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szHost);
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					fprintf(fpdbg, "3", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szHost.TrimLeft();
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szHost.TrimRight();
					prs->GetFieldValue("ParsedValue", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szValue);
					prs->GetFieldValue("TriggerTime", szTemp);

//g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Trigger time received: %s", szTemp); // Sleep(50); //(Dispatch message)
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_dblTriggerTime = atof(szTemp);// - 0.333;  // ten frames anticipation.

					if((int)pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_dblTriggerTime <= 0) { bInvalidTimes = true; break;}
					prs->GetFieldValue("analyzed_trigger_id", szTemp);
					if(szTemp.GetLength()) pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_nAnalyzedTriggerID = atoi(szTemp);

					prs->GetFieldValue("Source", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szSource);
					prs->GetFieldValue("Scene", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szScene);

					prs->GetFieldValue("Graphic_Action_Type", szTemp);
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_nType = atoi(szTemp);
					prs->GetFieldValue("Action_Name", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szIdentifier);

/////////////////////////////
					// added  added 2.2.1.20 (9 July 2016)
					prs->GetFieldValue("channelid", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szChannelID);
					prs->GetFieldValue("event_clip", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szAutomationEventID);
					prs->GetFieldValue("event_title", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szAutomationEventTitle);
					prs->GetFieldValue("event_name", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szEventName);
//					prs->GetFieldValue("automation_event_itemid", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szAutomationEventItemID);  // wrong ID, want event_id at end
////////////////////////////
					
					prs->GetFieldValue("dest_type", szTemp);
					pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_nDestType = atoi(szTemp);

					prs->GetFieldValue("dest_module", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szDestModule);
					prs->GetFieldValue("layer", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szLayer);

/////////////////////////////
					// added  added 2.2.1.20 (9 July 2016)
					if(pChannelObj->m_ulFlags&NUCLEUS_FLAG_EXTENDED_TAV)
					{
						prs->GetFieldValue("event_duration", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szAutomationEventDur);
						prs->GetFieldValue("event_data", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szAutomationEventData);
						prs->GetFieldValue("event_id", pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szAutomationEventItemID); 
					}

/////////////////////////////
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					fprintf(fpdbg, "r128", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}

					
					// want to add promotor event name here
				//removed	 2.2.1.20 (9 July 2016) - see above pChannelObj->m_ppEvents[pChannelObj->m_nEvents]->m_szEventName="";
					
					pChannelObj->m_nEvents++;

					prs->MoveNext();
		
					bNewRetrieve = pChannelObj->m_bTriggerEventsChanged;
				}
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					fprintf(fpdbg, "Y3", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}	
				if(prs)
				{
		//			prs->MoveNext();
					prs->Close();
					delete prs;
					prs = NULL;
//					pChannelObj->m_prs = NULL;
				}
	LeaveCriticalSection(&pChannelObj->m_critChVar);  // lock it out.

				}
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					fprintf(fpdbg, "Z\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}
//2.1.1.21 removed crit sec  - timeouts on other channels blocked this!
//				LeaveCriticalSection(&pnucleus->m_data.m_critSQL);
				

if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) 	
{
int dbglist=0;
g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s ***** begin: %d events now in Trigger buffer for %s:%d", debugsource,
										pChannelObj->m_nEvents,	pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum ); // Sleep(50); //(Dispatch message)
while(dbglist<pChannelObj->m_nEvents)
{
					int harris = (int)pChannelObj->m_ppEvents[dbglist]->m_dblTriggerTime;

g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s    Trigger[%02d] atid=%d eid=%d type=%d [@%.3f (%02d:%02d:%02d)] [host:%s][scene:%s][layer:%s][id:%s][value:%s][dest:%d][mod:%s] A:[%s][%s][%s][%s]",debugsource,
							dbglist,
							pChannelObj->m_ppEvents[dbglist]->m_nAnalyzedTriggerID,
							pChannelObj->m_ppEvents[dbglist]->m_nEventID,
							pChannelObj->m_ppEvents[dbglist]->m_nType,
							pChannelObj->m_ppEvents[dbglist]->m_dblTriggerTime,
							((harris%86400)/3600),
							((harris/60)%60),
							(((harris)%60)%60),
							pChannelObj->m_ppEvents[dbglist]->m_szHost,
							pChannelObj->m_ppEvents[dbglist]->m_szScene,
							pChannelObj->m_ppEvents[dbglist]->m_szLayer,
							pChannelObj->m_ppEvents[dbglist]->m_szIdentifier,
							pChannelObj->m_ppEvents[dbglist]->m_szValue,
							pChannelObj->m_ppEvents[dbglist]->m_nDestType,
							pChannelObj->m_ppEvents[dbglist]->m_szDestModule,

	// additional, added 2.2.1.20 (9 July 2016)  otherwise will just be blank

							pChannelObj->m_ppEvents[dbglist]->m_szAutomationEventItemID,
							pChannelObj->m_ppEvents[dbglist]->m_szAutomationEventID,
							pChannelObj->m_ppEvents[dbglist]->m_szAutomationEventDur,
							pChannelObj->m_ppEvents[dbglist]->m_szAutomationEventData

							); // Sleep(20); //(Dispatch message)
dbglist++;

}

g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s ***** end Trigger buffer for %s:%d",debugsource, pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum ); // Sleep(50); //(Dispatch message)

}
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "[%d> %d.%03d T %d inv%d\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm, pChannelObj->m_nEvents, bInvalidTimes);
					fflush(fpdbg);
				}
			}


				if((pChannelObj->m_nEvents>0)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)&&(!bInvalidTimes))
				{
					bool bFirst = true;
					// now update status
					///// DEMOTODO get item id name in view
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d WHERE ",
							pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the AnalyzedTriggerData table name
							NUCLEUS_FLAG_TRIGGERED
						);

					char proj[4096];
					int n = 0;

					if(pnucleus->m_settings.m_bUseAsynchTriggerRetrieve)
					{
						while(n<pChannelObj->m_nEvents)
						{
							if(pChannelObj->CheckIdentifier(pChannelObj->m_ppEvents[n]->m_nAnalyzedTriggerID)<0) break;
							n++;
						}

	///					if(!pnucleus->m_settings.m_bExcludeAsyncTriggers)  // 2.2.1.22 - if excluding, we don't have to try to ensure a purge here, spending the time to do so.  NO, we do.  we have to allow release of the triggered ids to allow new gets, etc.
						{
							if(n>=pChannelObj->m_nEvents) // nothing left, forced to do stuff now.
							{
//								if((pChannelObj->m_nTriggeredIDs>0)/*&&(!pChannelObj->m_bGetTriggeredIDs)*/) // 2.2.1.22 commented out so it is always checked. Actually jsut make sure to leave crit sec so query thread is not blocked.
								{
	if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s forcing mark for %d IDs (now=%.03f)", debugsource, pChannelObj->m_bGetTriggeredIDs, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)
										// give the word to exchange
//										pChannelObj->m_bGetTriggeredIDs = true; 
										if(pChannelObj->m_nTriggeredIDs>0) pChannelObj->m_bGetTriggeredIDs = true; // 2.2.1.22 added the conditional, it may not be a new get, but there may be pending manips.
										int nTriggeredIDs = pChannelObj->m_nTriggeredIDs; // benchmark 2.2.1.22 it might only be reduction, not zero-ness.
										LeaveCriticalSection(&pChannelObj->m_critTriggeredIDs);
										int clocktick = clock();
//										while((pChannelObj->m_nTriggeredIDs>0)&&((clocktick+50 > clock())&&(clocktick<=clock()))&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)) Sleep(1);
										while((pChannelObj->m_nTriggeredIDs==nTriggeredIDs)&&((clocktick+100 > clock())&&(clocktick<=clock()))&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)) Sleep(1);// 2.2.1.22 benchmark
	if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_trigger", "%s %d entering forced mark ownership (now=%.03f)", debugsource, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)
										EnterCriticalSection(&pChannelObj->m_critTriggeredIDs);
	if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_trigger", "%s forced mark (now=%.03f) with %d triggers from %d", debugsource, pChannelObj->m_dblAutomationTimeEstimate, pChannelObj->m_bGetTriggeredIDs, nTriggeredIDs); //(Dispatch message)
								}
							}
						}
					}

					bool bTriggered = false;

					if(pnucleus->m_settings.m_bUseAsynchTriggerRetrieve)
					{
//						EnterCriticalSection(&pChannelObj->m_critChVar);
						bNewRetrieve = pChannelObj->m_bNewEventRetrieve;
//						LeaveCriticalSection(&pChannelObj->m_critChVar);
					}
					else
					{
						bNewRetrieve = pChannelObj->m_bTriggerEventsChanged;
					}

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "[%d> %d.%03d T+\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}

					while((n<pChannelObj->m_nEvents)&&(!bNewRetrieve)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
					{
/*
					sprintf(proj, "%s: scene[%s] %s:%s", 
							pChannelObj->m_ppEvents[n]->m_szHost,
							pChannelObj->m_ppEvents[n]->m_szScene,
							pChannelObj->m_ppEvents[n]->m_szIdentifier,
							pChannelObj->m_ppEvents[n]->m_szValue);
*/
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s trigger delay: waiting until %.3f (now=%.03f) for trigger %d with %d, %d or until changes", debugsource, 
																																							 pChannelObj->m_ppEvents[n]->m_dblTriggerTime, pChannelObj->m_dblAutomationTimeEstimate, n, pChannelObj->m_nTriggeredIDs, pChannelObj->m_bGetTriggeredIDs); //(Dispatch message)

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "[%d> %d.%03d A\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}

//						EnterCriticalSection(&pChannelObj->m_critChVar); // added 2.2.1.21 (15 July 2016)

						int nLine = __LINE__;
						bool bInCrit=true;
						int nTriggeredIDs = -1;

						while((pChannelObj->m_dblAutomationTimeEstimate<pChannelObj->m_ppEvents[n]->m_dblTriggerTime)
							&&(!bNewRetrieve)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
						{
							nLine = __LINE__;
//							LeaveCriticalSection(&pChannelObj->m_critChVar); // added 2.2.1.21 (15 July 2016)
try
{

			if((pnucleus->m_settings.m_bUseAsynchTriggerRetrieve)&&(pChannelObj->m_nTriggeredIDs>0)&&/*(!pChannelObj->m_bGetTriggeredIDs)&&*/(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
			{
				nLine = __LINE__;
	if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_trigger", "%s %d trigger marks exist (now=%.03f) top=%d", debugsource, pChannelObj->m_nTriggeredIDs, pChannelObj->m_dblAutomationTimeEstimate, ((pChannelObj->m_nTriggeredIDs>0)&&(pChannelObj->m_pnTriggeredIDs))?pChannelObj->m_pnTriggeredIDs[0]:-1); //(Dispatch message)
				if((pChannelObj->m_ppEvents[n]->m_dblTriggerTime-pChannelObj->m_dblAutomationTimeEstimate)>0.5) // if we have half a second, start this otherwise do not.
				{
					nLine = __LINE__;
					// give the word to exchange
					pChannelObj->m_bGetTriggeredIDs = true; 
					nTriggeredIDs = pChannelObj->m_nTriggeredIDs; // benchmark 2.2.1.22 it might only be reduction, not zero-ness.
					LeaveCriticalSection(&pChannelObj->m_critTriggeredIDs);
					nLine = __LINE__;
					bInCrit=false;
					int clocktick = clock();
				//	while((pChannelObj->m_nTriggeredIDs>0)&&((clocktick+100 > clock())&&(clocktick<=clock()))&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)) Sleep(1);
					while((pChannelObj->m_nTriggeredIDs==nTriggeredIDs)&&((clocktick+100 > clock())&&(clocktick<=clock()))&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)) Sleep(1);// 2.2.1.22 benchmark
	if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_trigger", "%s entering trigger mark ownership (now=%.03f)", debugsource, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)
					nLine = __LINE__;
					EnterCriticalSection(&pChannelObj->m_critTriggeredIDs);
					bInCrit=true;
	if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_trigger", "%s entered trigger mark ownership (now=%.03f) %d triggers from %d", debugsource, pChannelObj->m_dblAutomationTimeEstimate, pChannelObj->m_nTriggeredIDs, nTriggeredIDs); //(Dispatch message)
					nLine = __LINE__;
				}
			}
nLine = __LINE__;
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg){  fprintf(fpdbg, "1");	fflush(fpdbg); }
			}

							if(bUseLyric)
							{
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg){  fprintf(fpdbg, "2");	fflush(fpdbg); }
			}

//						EnterCriticalSection(&pChannelObj->m_critChVar); // added 2.2.1.21 (15 July 2016)

							if((pChannelObj->m_ppEvents[n]->m_dblTriggerTime-pChannelObj->m_dblAutomationTimeEstimate)>0.5) // if we have half a second, start this otherwise do not.
							{

//						LeaveCriticalSection(&pChannelObj->m_critChVar); // added 2.2.1.21 (15 July 2016)

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg){  fprintf(fpdbg, "X");	fflush(fpdbg); }
			}
//	EnterCriticalSection(&pnucleus->m_data.m_critSettings);

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg){  fprintf(fpdbg, "Y");	fflush(fpdbg); }
			}
								if(	
									  ( pChannelObj->m_nMinLyricAlias!=pnucleus->m_settings.m_nMinLyricAlias) 
									||(	pChannelObj->m_nMaxLyricAlias!=pnucleus->m_settings.m_nMaxLyricAlias) 
									)
								{//settings have changed
									if((pnucleus->m_settings.m_nMaxLyricAlias-pnucleus->m_settings.m_nMinLyricAlias)>(pChannelObj->m_nMaxLyricAlias-pChannelObj->m_nMinLyricAlias))
									{
										// have to increase the buffer....

	int nAliases = pnucleus->m_settings.m_nMaxLyricAlias - pnucleus->m_settings.m_nMinLyricAlias + 1;
	if(nAliases<=0) nAliases = 10; //min set
	LyricAlias_t** pNew = new LyricAlias_t*[nAliases];

	if(pNew)//pChannelObj->m_ppLyricAlias)
	{
		int i=0;
		while( i < nAliases)
		{
			if(i<pChannelObj->m_nNumAliasArray)
			{
				pNew[i] = pChannelObj->m_ppLyricAlias[i];
			}
			else
			{
				LyricAlias_t* pAlias = new LyricAlias_t;
				if(pAlias)
				{
					pNew[i] = pAlias;
					pAlias->szMessage = "";
					pAlias->nAlias = -1;
					pAlias->bMessageLoaded = false;
					pAlias->bMessageRead = false;
				}
			}
			i++;
		}
		delete [] pChannelObj->m_ppLyricAlias;
		pChannelObj->m_ppLyricAlias = pNew;
		pChannelObj->m_nNumAliasArray = nAliases;

	}

									}
									pChannelObj->m_nMinLyricAlias=pnucleus->m_settings.m_nMinLyricAlias;
									pChannelObj->m_nMaxLyricAlias=pnucleus->m_settings.m_nMaxLyricAlias;
								}
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg){  fprintf(fpdbg, "Z");	fflush(fpdbg); }
			}

//	LeaveCriticalSection(&pnucleus->m_data.m_critSettings);
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg){  fprintf(fpdbg, "Q");	fflush(fpdbg); }
			}
							}
							else
							{
//								LeaveCriticalSection(&pChannelObj->m_critChVar); // added 2.2.1.21 (15 July 2016)
							}
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg){  fprintf(fpdbg, "3");	fflush(fpdbg); }
			}

							}
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg){  fprintf(fpdbg, "4");	fflush(fpdbg); }
			}
nLine = __LINE__;
//							EnterCriticalSection(&pChannelObj->m_critChVar); // added 2.2.1.21 (15 July 2016)

	_ftime(&pChannelObj->m_timebTriggerTick); // the last time check inside the thread
nLine = __LINE__;
							Sleep(1);

							if(pnucleus->m_settings.m_bUseAsynchTriggerRetrieve)
							{
//								EnterCriticalSection(&pChannelObj->m_critChVar);
nLine = __LINE__;
								bNewRetrieve = pChannelObj->m_bNewEventRetrieve;
//								LeaveCriticalSection(&pChannelObj->m_critChVar);
							}
							else
							{
nLine = __LINE__;
								bNewRetrieve = pChannelObj->m_bTriggerEventsChanged;
							}

}
catch(...)
{
	g_pnucleus->m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Nucleus:trigger_wait", "%s caught exception @ %d with %d triggers from %d (now=%.03f)", debugsource, 
		nLine,
		pChannelObj->m_nTriggeredIDs, nTriggeredIDs,
		pChannelObj->m_dblAutomationTimeEstimate 
		); //(Dispatch message)
	if(!bInCrit)	EnterCriticalSection(&pChannelObj->m_critTriggeredIDs);
}
						}

	//					LeaveCriticalSection(&pChannelObj->m_critChVar); // added 2.2.1.21 (15 July 2016)

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
if(fpdbg)
{
	_ftime(&tmb);
	fprintf(fpdbg, "\r\n[%d> %d.%03d now=%.3f - %d %d %d\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm, pChannelObj->m_dblAutomationTimeEstimate, pChannelObj->m_bTriggerEventsChanged, g_bKillThread, pChannelObj->m_bKillAutomationThread);
	fflush(fpdbg);
}
			}


if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s finished trigger delay: now=%.3f - %d %d %d", debugsource, pChannelObj->m_dblAutomationTimeEstimate, pChannelObj->m_bTriggerEventsChanged, g_bKillThread, pChannelObj->m_bKillAutomationThread); //(Dispatch message)

						if((!bNewRetrieve)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
						{

// trigger graphics device
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
if(fpdbg){  fprintf(fpdbg, "5");	fflush(fpdbg); }
			}
							if((pChannelObj->m_ppEvents[n]->m_nAnalyzedTriggerID>0)&&(pChannelObj->m_ppEvents[n]->m_szIdentifier.GetLength()))
							{
	//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "pchEncodedLocal: %s", pchEncodedLocal);  Sleep(50);//(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "want to send %s", proj);  Sleep(50);//(Dispatch message)

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
if(fpdbg){  fprintf(fpdbg, "6");	fflush(fpdbg); }
			}


								CString szReadableII = "";
								CString szII = "";
								bool bTabulatorEvent = false;
								if(pChannelObj->m_ppEvents[n]->m_nType==0)  //anim
								{
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
if(fpdbg){  fprintf(fpdbg, "A");	fflush(fpdbg); }
			}

	/*
	[18:44] rpbutterphly: Open
	[18:44] rpbutterphly: Load
	[18:44] rpbutterphly: Play
	[18:44] rpbutterphly: Stop
	[18:45] rpbutterphly: Close
	*/
/*
3000 Duet LEX
3001 Duet HyperX
3002 Duet MicroX
3003 Channel Box
3004 CAL Box
*/

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Type is anim, with dest type: %d", pChannelObj->m_ppEvents[n]->m_nDestType);  Sleep(50);//(Dispatch message)

									switch(pChannelObj->m_ppEvents[n]->m_nDestType)
									{
									case CX_DESTTYPE_VDS_WATERCOOLER://           9000  // watercooler!
										{
										} break;

									case CX_DESTTYPE_MIRANDA_IS2://					2001  // Imagestore 2
									case CX_DESTTYPE_MIRANDA_INT://					2002  // Intuition
									case CX_DESTTYPE_MIRANDA_IS300://				2003  // Imagestore 300
									case CX_DESTTYPE_MIRANDA_HDIS://				2101  // Imagestore HD
									case CX_DESTTYPE_MIRANDA_HDINT://       2102  // Intuition HD
									case CX_DESTTYPE_MIRANDA_HDIS300://     2103  // Imagestore 300 HD
										{
											// no anims supported yet...old comment.  NOW:

											// OK, need to deal with Load (Load a file into a layer), Play (Fade up)  and Clear (Fade Down)
											//  could use CUT up and down, but for this we can just make FADE be immediate, so just use fade.
											// NOT using fade to black etc, using fade keyer, because we want background to pass.

											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Load")==0)
											{
												// layer needs to be a %1x representation of the layer number  (we have it as a string)
												szII.Format("R0%s%s",pChannelObj->m_ppEvents[n]->m_szLayer, pChannelObj->m_ppEvents[n]->m_szSource);  //using source, not scene, since IS2 only dealas with filenames, there are no scenes
//												szII.Format("R0%s%s",pChannelObj->m_ppEvents[n]->m_szLayer, pChannelObj->m_ppEvents[n]->m_szScene);
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Play")==0)
											{
												//Layer needs to be a %d representation of the layer number (we have it as a string)
												szII.Format("1%s 1",pChannelObj->m_ppEvents[n]->m_szLayer);
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Clear")==0)
											{
												//Layer needs to be a %d representation of the layer number (we have it as a string)
												szII.Format("1%s 0",pChannelObj->m_ppEvents[n]->m_szLayer);
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Command")==0)
											{
												//the whole command must be coded in, exactly as it is to be sent to the Miranda
												// can be used for anything, fade transition settings, easytext commands, squeezy, etc
												szII.Format("%s",pChannelObj->m_ppEvents[n]->m_szValue);
											}

										} break;

									case CX_DESTTYPE_CHYRON_CHANNELBOX://		3003  // Channel Box
									case CX_DESTTYPE_CHYRON_HDCHANNELBOX://        3103  // Channel Box HD
										{
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Open")==0)
											{
												szII.Format("B\\OP\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene);
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Load")==0)
											{
												szII.Format("B\\LO\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene);
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Play")==0)
											{
												szII.Format("B\\PL\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene);
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Stop")==0)
											{
												szII.Format("B\\ST\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene);
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Close")==0)
											{
												szII.Format("B\\CL\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene);
											}

											//system-wide commands
											else  
												if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase(pnucleus->m_settings.m_pszStopAll?pnucleus->m_settings.m_pszStopAll:"StopAll")==0)
											{
/*
Courtney Tompos
Software Engineer
Chyron Corporation
631-845-2141:

To stop all scenes (but leave them open), transmit:
Y\<1B>\\<CR><LF>

To close all scenes, transmit either:
Y\<CD>\\<CR><LF>
Or
Y\<FE>\\<CR><LF>
*/

												szII.Format("Y\\%c\\\\", 0x1b);
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase(pnucleus->m_settings.m_pszCloseAll?pnucleus->m_settings.m_pszCloseAll:"CloseAll")==0)
											{
												szII.Format("Y\\%c\\\\", 0xcd); // 0xcd seems to be a bit faster than 0xfe, maybe not appreciably so, but given the choice we use 0xcd
											}
											else  // prob a mistake so just use update.
											{

													//��#
												char* pch = bu.EscapeChar(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0), '\\');
												if(pch)
												{
													szII.Format("B\\UP\\%s\\%s%s�%s\\\\", 
														pChannelObj->m_ppEvents[n]->m_szScene, 
														pChannelObj->m_ppEvents[n]->m_szIdentifier, 
														(pnucleus->m_settings.m_bUseUTF8?"�":""),
														pch);
													free(pch);
												}
												else
												{
													szII.Format("B\\UP\\%s\\%s%s�%s\\\\", 
														pChannelObj->m_ppEvents[n]->m_szScene, 
														pChannelObj->m_ppEvents[n]->m_szIdentifier, 
														(pnucleus->m_settings.m_bUseUTF8?"�":""),
														pChannelObj->m_ppEvents[n]->m_szValue);
												}
											}
										} break;
										// following cases are "Lyric"
//									case CX_DESTTYPE_CHYRON_LEX://					3000	// Duet LEX
//									case CX_DESTTYPE_CHYRON_HYPERX://				3001  // Duet HyperX
//									case CX_DESTTYPE_CHYRON_MICROX://				3002  // Duet MicroX
									case CX_DESTTYPE_CHYRON_LYRIC://               3005  // Generic Lyric Intelligent Interface, any hardware
									case CX_DESTTYPE_CHYRON_HDLYRIC://             3105  // Generic Lyric Intelligent Interface, any hardware, HD
									case CX_DESTTYPE_CHYRON_CALBOX://				3004  // CAL Box
									case CX_DESTTYPE_CHYRON_HDCALBOX:// 3104  // CAL Box HD
										{
/*
[15:33] rpbutterphly: load   = W command
[15:33] rpbutterphly: play   = not V\6   always use  Y\<D5><F3> for release, and immediate return
[15:33] rpbutterphly: release = V\5\15
[15:33] rpbutterphly: unload  //not impl
[15:33] rpbutterphly: clear    Y\<D5><FE><Frame Buffer>\\<CR><LF>

*/
						//					if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Load")==0)
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Open")==0)  // changed to Open.
											{
/*
												if(pChannelObj->m_nLastLyricAlias<0)
												{
													pChannelObj->m_nLastLyricAlias = pnucleus->m_settings.m_nMinLyricAlias;
												}
												else
												{
													pChannelObj->m_nLastLyricAlias++;  
													if (pChannelObj->m_nLastLyricAlias>pnucleus->m_settings.m_nMaxLyricAlias)
														pChannelObj->m_nLastLyricAlias = pnucleus->m_settings.m_nMinLyricAlias;
												}
												szII.Format("W\\%d\\%s\\", pChannelObj->m_nLastLyricAlias, pChannelObj->m_ppEvents[n]->m_szScene);
*/
///// new method
												int idxA = pChannelObj->ReturnAliasIndex(pChannelObj->m_ppEvents[n]->m_szScene);

												if(idxA<0)
												{
													idxA = pChannelObj->AddAlias(pChannelObj->m_ppEvents[n]->m_szScene);
												}

												if(idxA<0)
												{												
//													szII.Format("W\\%d\\%s\\", pChannelObj->m_nLastLyricAlias, pChannelObj->m_ppEvents[n]->m_szScene); // no choice
													szII.Format("W\\%s\\%s\\", pChannelObj->m_ppEvents[n]->m_szScene, pChannelObj->m_ppEvents[n]->m_szScene); // no choice
												}
												else
												{										
													szII.Format("W\\%d\\%s\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pChannelObj->m_ppEvents[n]->m_szScene);

													// prob need to set this after a success code .... at some point.
													pChannelObj->m_ppLyricAlias[idxA]->bMessageLoaded = true;
												}

////////////////
												for(int maxfields=0; maxfields<99; maxfields++)
												{
													szII += " \\";
												}
												szII += "\\";  // term II


//												pChannelObj->m_nCurrentMessage = atoi(pChannelObj->m_ppEvents[n]->m_szScene); // not sure why bothering to do this.
//												pChannelObj->m_bMessageLoaded = true;
//												pChannelObj->m_bMessageRead = false;  // reset this guy


											}
											else
//											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Read")==0)
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Load")==0)  // Changed to Load, 
											{
												int idxA = pChannelObj->ReturnAliasIndex(pChannelObj->m_ppEvents[n]->m_szScene);
												if(idxA<0)
												{

													szII.Format("V\\5\\13\\1\\%s\\%s\\1\\\\", pChannelObj->m_ppEvents[n]->m_szLayer, pChannelObj->m_ppEvents[n]->m_szScene);
//													szII.Format("V\\5\\13\\1\\%s\\%d\\1\\\\", pChannelObj->m_ppEvents[n]->m_szLayer, pChannelObj->m_nLastLyricAlias);
												}
												else
												{
													szII.Format("V\\5\\13\\1\\%s\\%d\\1\\\\", pChannelObj->m_ppEvents[n]->m_szLayer, pChannelObj->m_ppLyricAlias[idxA]->nAlias);
													pChannelObj->m_ppLyricAlias[idxA]->bMessageRead = true; 
												}
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Play")==0)
											{
												szII.Format("Y\\%c%c%s\\\\", 0xd5, 0xf3, pChannelObj->m_ppEvents[n]->m_szLayer);  //we'll see if this works.
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Release")==0)
											{
												if(pnucleus->m_settings.m_bReleaseIsV_5_15)
												{
													szII.Format("V\\5\\15\\\\");
												}
												else
												{
/*

Andrea Guarini	aguarini@chyron.com
The only other way to release a pause is via a macro (E) command.  The
following will release a pause on Frame Buffer 2.

E\Lyric.FrameBuffer(2).ReleasePause\\

*/

													szII.Format("E\\Lyric.FrameBuffer(%s).ReleasePause\\\\", pChannelObj->m_ppEvents[n]->m_szLayer);  //we'll see if this works.

												}

											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Unload")==0)
											{
												// same as clear for now.
												szII.Format("Y\\%c%c%s\\\\", 0xd5, 0xfe, pChannelObj->m_ppEvents[n]->m_szLayer);  //we'll see if this works.
												int idxA = pChannelObj->ReturnAliasIndex(pChannelObj->m_ppEvents[n]->m_szScene);
												if(idxA>=0)
												{
													pChannelObj->m_ppLyricAlias[idxA]->bMessageLoaded = false;
												}

											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Clear")==0)
											{
												szII.Format("Y\\%c%c%s\\\\", 0xd5, 0xfe, pChannelObj->m_ppEvents[n]->m_szLayer);  //we'll see if this works.
												int idxA = pChannelObj->ReturnAliasIndex(pChannelObj->m_ppEvents[n]->m_szScene);
												if(idxA>=0)
												{
													pChannelObj->m_ppLyricAlias[idxA]->bMessageLoaded = false;
												}
											}
											else  // prob a mistake so just use update.
											{
												int idxA = pChannelObj->ReturnAliasIndex(pChannelObj->m_ppEvents[n]->m_szScene);
												if(idxA>=0)
												{
													char* pch = bu.EscapeChar(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0), '\\');
													if(pch)
													{
														if(pChannelObj->m_ppLyricAlias[idxA]->bMessageRead)
															szII.Format("U\\*\\%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pch);
														else
															szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pChannelObj->m_ppEvents[n]->m_szIdentifier, pch);
														free(pch);
													}
													else
													{
														if(pChannelObj->m_ppLyricAlias[idxA]->bMessageRead)
															szII.Format("U\\*\\%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szValue);
														else
															szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szValue);

													}
												}
											}
										} break;

									case CX_DESTTYPE_HARRIS_ICONII://				5000
									case CX_DESTTYPE_HARRIS_ICONXML://             5001    // Icon station with XML Interface
										{

/*
// IconStation II
IconStation supports four CII commands for greater output flexibility.
The first CII command allows you to load a layout and fire a Salvo.
The second CII command allows you to clear preview and clear output.
The third CII command allows you to poll the IconStation system to confirm IconStation is still running and responding to commands.
The fourth CII command allows you to update your item source text at any time.

Channel Entry
The Channel entry equals A or B.
	A = program channel
	B = preview channel

T\7\LayoutSalvo\Channel\\   - Load a Layout and Fire a Salvo
T\14\Channel\\   - Clear Preview and Clear Output
*\\   - Poll IconStation (returns *<CRLF>)
I\42\<LayoutTags><LayoutName>TheLayoutNameGoesHere</LayoutName><Region><Name>TheItemNameGoesHere</Name><Tag><Name>TheTagNameGoesHere</Name><Text>TheTextGoesHere</Text></Tag></Region></LayoutTags>\\    - Update Item Source Text
Example Command:
I\42\<LayoutTags><LayoutName>UpNext</LayoutName><Region><Name>Title1</Name><Tag> <Name>Field1</Name><Text>Economic Outlook</Text></Tag></Region></LayoutTags>\\

//stuff has this structure:
<LayoutTags>
	<LayoutName>InfoChannel</LayoutName>
	<Region> // region 1
		<Name>Crawl</Name> // the name of the region
		<Tag> // a "tag" within a region
			<Name>1-1</Name>  // predefined tag name such as Field1, Field2, 1-1, 2-1, etc, see documentation
			<Text>blarg</Text>  // text to put in the tag (field) in the region
		</Tag>
	</Region>
	<Region> // an additional region in the layout - one with multiple "tags"
		<Name>Title_area</Name>
		<Tag>
			<Name>Field1</Name>
			<Text>coming up next</Text>
		</Tag>
		<Tag>
			<Name>Field2</Name>
			<Text>more text</Text>
		</Tag>
	</Region>
</LayoutTags>


Enter the name of the data tag between the second set of <Name></Name> tags. The data
tag name depends on the item type

Item Type Examples
Title To update the text on the first line of your title item enter:
Field1
To update the text on the second line of your title item enter:
Field2
To update the text on the third line of your title item enter:
Field3
Continue entering the next field number for the following lines in
your title item.
Crawl To update the text for a crawl item enter:
1-1
Roll To update the text for a roll item enter:
1-1
Title Table To update the text in a title table item, use the row-column
naming conventation. See the following examples.
To update the text in the first row, first column enter:
1-1
To update the text in the second row, first column enter:
2-1
To update the text in the first row, second column enter:
1-2
To update the text in the second row, second column enter:
2-2

	To accomplish this, the identifier will have the following format:
	RegionName:TagName

*/


											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Load")==0)
											{
												switch(pnucleus->m_settings.m_nLayoutSalvoFormat)
												{
												default:
												case CX_HARRIS_ICONII_LSFORMAT_NUM4://				0 // 4 digit numerical LayoutSalvoFormat
													{
														szII.Format("T\\7\\%s00\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene,pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												case CX_HARRIS_ICONII_LSFORMAT_NUM8://				1 // 8 digit numerical LayoutSalvoFormat
													{
														szII.Format("T\\7\\%s0000\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene,pChannelObj->m_ppEvents[n]->m_szLayer);
													}	break;
												case CX_HARRIS_ICONII_LSFORMAT_ALPHA://			2 // alphanumerical string LayoutSalvoFormat
													{
														szII.Format("T\\7\\%s/\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene,pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												}
												
											}
											else
/*
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("LoadFire")==0)
											{
												switch(pnucleus->m_settings.m_nLayoutSalvoFormat)
												{
												default:
												case NUCLEUS_HARRIS_ICONII_LSFORMAT_NUM4://				0 // 4 digit numerical LayoutSalvoFormat
												case NUCLEUS_HARRIS_ICONII_LSFORMAT_NUM8://				1 // 8 digit numerical LayoutSalvoFormat
													{
														szII.Format("T\\7\\%s%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene,pChannelObj->m_ppEvents[n]->m_szValue,pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												case NUCLEUS_HARRIS_ICONII_LSFORMAT_ALPHA://			2 // alphanumerical string LayoutSalvoFormat
													{
														szII.Format("T\\7\\%s/%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene,pChannelObj->m_ppEvents[n]->m_szValue,pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												}
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Fire")==0)
											{
												switch(pnucleus->m_settings.m_nLayoutSalvoFormat)
												{
												default:
												case NUCLEUS_HARRIS_ICONII_LSFORMAT_NUM4://				0 // 4 digit numerical LayoutSalvoFormat
													{
														szII.Format("T\\7\\00%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szValue,pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												case NUCLEUS_HARRIS_ICONII_LSFORMAT_NUM8://				1 // 8 digit numerical LayoutSalvoFormat
													{
														szII.Format("T\\7\\0000%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szValue,pChannelObj->m_ppEvents[n]->m_szLayer);
													}	break;
												case NUCLEUS_HARRIS_ICONII_LSFORMAT_ALPHA://			2 // alphanumerical string LayoutSalvoFormat
													{
														szII.Format("T\\7\\/%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szValue,pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												}
											}
											else
*/
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Clear")==0)
											{
												szII.Format("T\\14\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szLayer);
											}
											else  // its a salvo
											{
												switch(pnucleus->m_settings.m_nLayoutSalvoFormat)
												{
												default:
												case CX_HARRIS_ICONII_LSFORMAT_NUM4://				0 // 4 digit numerical LayoutSalvoFormat
													{
														szII.Format("T\\7\\00%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												case CX_HARRIS_ICONII_LSFORMAT_NUM8://				1 // 8 digit numerical LayoutSalvoFormat
													{
														szII.Format("T\\7\\0000%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szLayer);
													}	break;
												case CX_HARRIS_ICONII_LSFORMAT_ALPHA://			2 // alphanumerical string LayoutSalvoFormat
													{
														szII.Format("T\\7\\/%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szLayer);
													} break;
												}

/*
// update a region...?
												CString szRegion; 
												CString szTag;
												char* pszValue = bu.XMLEncode(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0)); // have to XML encode any values...I think.  this is a point to be clarified.

												int nColon = pChannelObj->m_ppEvents[n]->m_szIdentifier.ReverseFind(':');
												if(nColon>=0)
												{
													szRegion=pChannelObj->m_ppEvents[n]->m_szIdentifier.Left(nColon);
													szTag=pChannelObj->m_ppEvents[n]->m_szIdentifier.Right(nColon+1);
													if(szTag.IsEmpty()) szTag = "Field1"; // "safe" default
												}
												else
												{
													szRegion = pChannelObj->m_ppEvents[n]->m_szIdentifier;
													szTag = "Field1"; // "safe" default
												}

												szII.Format("I\\42\\<LayoutTags><LayoutName>%s</LayoutName><Region><Name>%s</Name><Tag><Name>%s</Name><Text>%s</Text></Tag></Region></LayoutTags>\\\\", 
													pChannelObj->m_ppEvents[n]->m_szScene,
													szRegion, szTag, (pszValue?pszValue:""));
												if(pszValue) free(pszValue);
*/

											}

										} break;
									case CX_DESTTYPE_HARRIS_G7II://							 5002    // G7 with Intelligent Interface
									case CX_DESTTYPE_HARRIS_HDG7II://								 5102    // G7 with Intelligent Interface HD
										{

											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Open")==0)
											{
												szII.Format("I\\25\\1\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szSource);  // using source for just the scribelist.
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Load")==0)
											{

///// new method
												int idxA = pChannelObj->ReturnAliasIndex(pChannelObj->m_ppEvents[n]->m_szScene);

												if(idxA<0)
												{
													idxA = pChannelObj->AddAlias(pChannelObj->m_ppEvents[n]->m_szScene);
												}

												if(idxA<0)
												{												
//													szII.Format("W\\%d\\%s\\", pChannelObj->m_nLastLyricAlias, pChannelObj->m_ppEvents[n]->m_szScene); // no choice
													szII.Format("I\\20\\%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene, pChannelObj->m_ppEvents[n]->m_szScene); // no choice
												}
												else
												{										
													szII.Format("I\\20\\%d\\%s\\\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pChannelObj->m_ppEvents[n]->m_szScene);

													// prob need to set this after a success code .... at some point.
													pChannelObj->m_ppLyricAlias[idxA]->bMessageLoaded = true;
												}

////////////////

//												szII.Format("I\\20\\9900\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene);
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Play")==0)
											{
												int idxA = pChannelObj->ReturnAliasIndex(pChannelObj->m_ppEvents[n]->m_szScene);
												if(idxA<0)
												{

//	szCmd.Format("T\\7\\9900\\%s\\\\", m_szLayer); // alias, layer A harcoded,  //Read Still
													szII.Format("T\\7\\%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szScene, pChannelObj->m_ppEvents[n]->m_szLayer); // alias, layer A harcoded,  //Read Still
												}
												else
												{
													szII.Format("T\\7\\%d\\%s\\\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pChannelObj->m_ppEvents[n]->m_szLayer); // alias, layer A harcoded,  //Read Still
													pChannelObj->m_ppLyricAlias[idxA]->bMessageRead = true; 
												}
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Update")==0)
											{
												szII.Format("I\\36\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szLayer);
											}
											else
											if(pChannelObj->m_ppEvents[n]->m_szIdentifier.CompareNoCase("Clear")==0)
											{
												szII.Format("T\\14\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szLayer);
											}
											else  // it's a "trigger"
											{
												szII.Format("T\\31\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier);
											}


										} break;
									}

								}
								else  // export
								{
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
if(fpdbg){  fprintf(fpdbg, "B");	fflush(fpdbg); }
			}

									// if an export, just want to check first if it is a tabulator event,  if so, just do that, not all the device checking.

									// searching for the tabulator token.
									
									if(stricmp(TabulatorToken, pChannelObj->m_ppEvents[n]->m_szIdentifier.Left(nTabulatorTokenLen).GetBuffer(0))==0)
									{
										bTabulatorEvent = true;  // !

										if(pChannelObj->m_ppEvents[n]->m_szValue.GetLength()>0)
										{
											// added  added 2.2.1.20 (9 July 2016)

											// parse the value for chicken lips and other items from the view using escape sequences.
											szII.Format("%s|%s", 
														pChannelObj->m_ppEvents[n]->m_szIdentifier.Mid(nTabulatorTokenLen), 
														pChannelObj->ReturnParsedValue(pChannelObj->m_ppEvents[n]->m_szValue, pChannelObj->m_ppEvents[n]));

/*
											szII.Format("%s|%s", 
														pChannelObj->m_ppEvents[n]->m_szIdentifier.Mid(nTabulatorTokenLen), 
														pChannelObj->m_ppEvents[n]->m_szValue);
*/
										
										
										}
										else
										{
											// could be just the token, no value, then you get no pipe.
											szII.Format("%s", pChannelObj->m_ppEvents[n]->m_szIdentifier.Mid(nTabulatorTokenLen));
										}

									}	// else an actual graphic
									else
									{

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Type is export, with dest type: %d", pChannelObj->m_ppEvents[n]->m_nDestType);  Sleep(50);//(Dispatch message)
										switch(pChannelObj->m_ppEvents[n]->m_nDestType)
										{
										case CX_DESTTYPE_VDS_WATERCOOLER://           9000  // watercooler!
											{
	//Twitter:  T\username\tweet\\<CRLF>
	//FB:  F\username\wall_post\\<CRLF>  (or whatever arguments)

												if(pChannelObj->m_ppEvents[n]->m_szScene.CompareNoCase("twitter")==0)
												{
													szII.Format("T\\%s\\%s\\\\", 
														pChannelObj->m_ppEvents[n]->m_szIdentifier, 
														pChannelObj->m_ppEvents[n]->m_szValue);
												}
												else
												if(pChannelObj->m_ppEvents[n]->m_szScene.CompareNoCase("facebook")==0)
												{
													szII.Format("F\\%s\\%s\\\\", 
														pChannelObj->m_ppEvents[n]->m_szIdentifier, 
														pChannelObj->m_ppEvents[n]->m_szValue);
												}

											} break;

										case CX_DESTTYPE_MIRANDA_IS2://					2001  // Imagestore 2
										case CX_DESTTYPE_MIRANDA_INT://					2002  // Intuition
										case CX_DESTTYPE_MIRANDA_IS300://				2003  // Imagestore 300
										case CX_DESTTYPE_MIRANDA_HDIS://				2101  // Imagestore HD
										case CX_DESTTYPE_MIRANDA_HDINT://       2102  // Intuition HD
										case CX_DESTTYPE_MIRANDA_HDIS300://     2103  // Imagestore 300 HD
											{

												// may want to add easytext command Z0 here, if the identifier name is numerical and there is a use easytext setting, or some other thing
												// set up conditions for easy text detection

												if( 
														(pChannelObj->m_ppEvents[n]->m_szIdentifier.GetLength()<5) // 255 is the largest box number
													&&(pChannelObj->m_ppEvents[n]->m_szIdentifier.GetLength()>1) // and need an I or T
													&&(
															(pChannelObj->m_ppEvents[n]->m_szIdentifier.GetAt(0)=='I')
														||(pChannelObj->m_ppEvents[n]->m_szIdentifier.GetAt(0)=='T')
														)  // I for image and T for Text
													&&(bu.IsDecimalString(pChannelObj->m_ppEvents[n]->m_szIdentifier.Mid(1).GetBuffer(0))==0x01)  // is a totally decimal string.
													)
												{
													// for easy text:

													// layer needs to be rendered as %01x
													// text box number needs to be rendered as %02x
													// flags are defaulted to ET_RENDER (=1) to update things immediately
													// the text is supposed to be UTF8 encoded.

													//need to know image or text.
													if(pChannelObj->m_ppEvents[n]->m_szIdentifier.GetAt(0)=='I')
													{
														szII.Format("Z4%s%02x%s",pChannelObj->m_ppEvents[n]->m_szLayer, atoi(pChannelObj->m_ppEvents[n]->m_szIdentifier.Mid(1)), pChannelObj->m_ppEvents[n]->m_szValue);
													}
													else // text
													{
														szII.Format("Z0%s%02x%s",pChannelObj->m_ppEvents[n]->m_szLayer, atoi(pChannelObj->m_ppEvents[n]->m_szIdentifier.Mid(1)), pChannelObj->m_ppEvents[n]->m_szValue);
													}
												}
												else
												{
													// for now, we just use data source.
													// setting the datasource
													char* pch = is2.EscapeSpecialChars(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0));
													if(pch)
													{
														szII.Format("m003%s|%s",pChannelObj->m_ppEvents[n]->m_szIdentifier, pch);
														free(pch);
													}
													else
													{
														szII.Format("m003%s|%s",pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szValue);
													}
												}

											} break;
										case CX_DESTTYPE_CHYRON_CHANNELBOX://		3003  // Channel Box
										case CX_DESTTYPE_CHYRON_HDCHANNELBOX://        3103  // Channel Box HD
											{
														//��#
												char* pch = bu.EscapeChar(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0), '\\');
												if(pch)
												{
													szII.Format("B\\UP\\%s\\%s%s�%s\\\\", 
														pChannelObj->m_ppEvents[n]->m_szScene, 
														pChannelObj->m_ppEvents[n]->m_szIdentifier, 
														(pnucleus->m_settings.m_bUseUTF8?"�":""),
														pch);
													free(pch);
												}
												else
												{
													szII.Format("B\\UP\\%s\\%s%s�%s\\\\", 
														pChannelObj->m_ppEvents[n]->m_szScene, 
														pChannelObj->m_ppEvents[n]->m_szIdentifier, 
														(pnucleus->m_settings.m_bUseUTF8?"�":""),
														pChannelObj->m_ppEvents[n]->m_szValue);
												}
											} break;
											// following cases are "Lyric"
	//									case CX_DESTTYPE_CHYRON_LEX://					3000	// Duet LEX
	//									case CX_DESTTYPE_CHYRON_HYPERX://				3001  // Duet HyperX
	//									case CX_DESTTYPE_CHYRON_MICROX://				3002  // Duet MicroX
										case CX_DESTTYPE_CHYRON_LYRIC://               3005  // Generic Lyric Intelligent Interface, any hardware
										case CX_DESTTYPE_CHYRON_HDLYRIC://             3105  // Generic Lyric Intelligent Interface, any hardware, HD
										case CX_DESTTYPE_CHYRON_CALBOX://				3004  // CAL Box
										case CX_DESTTYPE_CHYRON_HDCALBOX:// 3104  // CAL Box HD
											{
												int idxA = pChannelObj->ReturnAliasIndex(pChannelObj->m_ppEvents[n]->m_szScene);
												if(idxA>=0)
												{
													char* pch = bu.EscapeChar(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0), '\\');
													if(pch)
													{
														if(pChannelObj->m_ppLyricAlias[idxA]->bMessageRead)
															szII.Format("U\\*\\%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pch);
														else
															szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pChannelObj->m_ppEvents[n]->m_szIdentifier, pch);
														free(pch);
													}
													else
													{
														if(pChannelObj->m_ppLyricAlias[idxA]->bMessageRead)
															szII.Format("U\\*\\%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szValue);
														else
															szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szValue);

													}
												}
												else
												{
	// have to try....  but can't.. theres nothing to find.
	/*
													char* pch = bu.EscapeChar(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0), '\\');
													if(pch)
													{
														if(pChannelObj->m_bMessageRead)
															szII.Format("U\\*\\%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pch);
														else
															szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_nLastLyricAlias, pChannelObj->m_ppEvents[n]->m_szIdentifier, pch);
														free(pch);
													}
													else
													{
														if(pChannelObj->m_bMessageRead)
															szII.Format("U\\*\\%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szValue);
														else
															szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_nLastLyricAlias, pChannelObj->m_ppEvents[n]->m_szIdentifier, pChannelObj->m_ppEvents[n]->m_szValue);

													}
	*/
												}
											} break;

										case CX_DESTTYPE_HARRIS_ICONII://				5000
										case CX_DESTTYPE_HARRIS_ICONXML ://            5001    // Icon station with XML Interface
											{
												CString szRegion; 
												CString szTag;
												char* pszValue = bu.XMLEncode(pChannelObj->m_ppEvents[n]->m_szValue.GetBuffer(0)); // have to XML encode any values...I think.  this is a point to be clarified.

												int nColon = pChannelObj->m_ppEvents[n]->m_szIdentifier.ReverseFind(':');
												if(nColon>=0)
												{
													szRegion=pChannelObj->m_ppEvents[n]->m_szIdentifier.Left(nColon);
													szTag=pChannelObj->m_ppEvents[n]->m_szIdentifier.Mid(nColon+1);
													if(szTag.IsEmpty()) szTag = "Field1"; // "safe" default
												}
												else
												{
													szRegion = pChannelObj->m_ppEvents[n]->m_szIdentifier;
													szTag = "Field1"; // "safe" default
												}

												szII.Format("I\\42\\<LayoutTags><LayoutName>%s</LayoutName><Region><Name>%s</Name><Tag><Name>%s</Name><Text>%s</Text></Tag></Region></LayoutTags>\\\\", 
													pChannelObj->m_ppEvents[n]->m_szScene,
													szRegion, szTag, (pszValue?pszValue:""));
												if(pszValue) free(pszValue);

	/*
	// IconStation II
	IconStation supports four CII commands for greater output flexibility.
	The first CII command allows you to load a layout and fire a Salvo.
	The second CII command allows you to clear preview and clear output.
	The third CII command allows you to poll the IconStation system to confirm IconStation is still running and responding to commands.
	The fourth CII command allows you to update your item source text at any time.

	Channel Entry
	The Channel entry equals A or B.
		A = program channel
		B = preview channel

	T\7\LayoutSalvo\Channel\\   - Load a Layout and Fire a Salvo
	T\14\Channel\\   - Clear Preview and Clear Output
	*\\   - Poll IconStation (returns *<CRLF>)
	I\42\<LayoutTags><LayoutName>TheLayoutNameGoesHere</LayoutName><Region><Name>TheItemNameGoesHere</Name><Tag><Name>TheTagNameGoesHere</Name><Text>TheTextGoesHere</Text></Tag></Region></LayoutTags>\\    - Update Item Source Text
	Example Command:
	I\42\<LayoutTags><LayoutName>UpNext</LayoutName><Region><Name>Title1</Name><Tag> <Name>Field1</Name><Text>Economic Outlook</Text></Tag></Region></LayoutTags>\\

	//stuff has this structure:
	<LayoutTags>
		<LayoutName>InfoChannel</LayoutName>
		<Region> // region 1
			<Name>Crawl</Name> // the name of the region
			<Tag> // a "tag" within a region
				<Name>1-1</Name>  // predefined tag name such as Field1, Field2, 1-1, 2-1, etc, see documentation
				<Text>blarg</Text>  // text to put in the tag (field) in the region
			</Tag>
		</Region>
		<Region> // an additional region in the layout - one with multiple "tags"
			<Name>Title_area</Name>
			<Tag>
				<Name>Field1</Name>
				<Text>coming up next</Text>
			</Tag>
			<Tag>
				<Name>Field2</Name>
				<Text>more text</Text>
			</Tag>
		</Region>
	</LayoutTags>
	*/




											} break;

										case CX_DESTTYPE_HARRIS_G7II://							 5002    // G7 with Intelligent Interface
										case CX_DESTTYPE_HARRIS_HDG7II://								 5102    // G7 with Intelligent Interface HD
											{

												szII.Format("I\\35\\%s\\%s\\%s\\\\", pChannelObj->m_ppEvents[n]->m_szLayer,
													pChannelObj->m_ppEvents[n]->m_szIdentifier, 
													pChannelObj->m_ppEvents[n]->m_szValue);

											} break;

										}//switch g type
									} // else a graphic
								}// export

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
if(fpdbg){  fprintf(fpdbg, "C");	fflush(fpdbg); }
			}

								int nLen = szII.GetLength();
								if(nLen>0)  //ignore blank stuff, failure
								{

									if(bTabulatorEvent)
									{

										if(g_pnucleus->m_data.m_socketTabulator)
										{

											CNetData data;
											data.m_ucType = (NET_TYPE_PROTOCOL1|NET_TYPE_KEEPOPEN|NET_SND_KEEPOPENLCL); // has data but no subcommand.
											data.m_ucCmd		= TABULATOR_CMD_PLUGINCALL;
											data.m_pucData	= (unsigned char*)malloc(nLen+1);
											if(data.m_pucData)
											{
												memcpy(data.m_pucData, szII.GetBuffer(0), nLen);
												data.m_pucData[nLen]=0;
												data.m_ulDataLen = nLen;
												data.m_ucType |= NET_TYPE_HASDATA;
											}


								_timeb timecheck[4];
			pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s Entering send", debugsource );

								_ftime(&timecheck[0]);
	EnterCriticalSection(&g_pnucleus->m_data.m_critTabCmd);
								_ftime(&timecheck[1]);

			pnucleus->m_msgr.DM(MSG_ICONNONE, NULL, "Nucleus:debug", "%s sending to %s at %s:%d with: %s", debugsource,
									g_pnucleus->m_settings.m_pszTabulatorModule?g_pnucleus->m_settings.m_pszTabulatorModule:"Tabulator",
									g_pnucleus->m_settings.m_pszTabulatorHost?g_pnucleus->m_settings.m_pszTabulatorHost:"null",
									g_pnucleus->m_settings.m_nTabulatorPort, 
									szII);
//				g_pnucleus->m_data.m_bInCommand = TRUE;
											int nReturn = g_pnucleus->m_data.m_net.SendData(&data, g_pnucleus->m_data.m_socketTabulator, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, errorstring);
								_ftime(&timecheck[2]);
											
											if(nReturn<NET_SUCCESS)
											{
												// log it.
												g_pnucleus->m_data.m_net.CloseConnection(g_pnucleus->m_data.m_socketTabulator, errorstring);
												g_pnucleus->m_data.m_socketTabulator =  NULL;

//ASRUN
g_pnucleus->SendAsRunMsg(CX_SENDMSG_ERROR, pChannelObj->m_nChannelID, pChannelObj->m_ppEvents[n]->m_szEventName.GetBuffer(0), autosource, "Error %d sending to %s at %s:%d with: %s", 
													nReturn,
													g_pnucleus->m_settings.m_pszTabulatorModule?g_pnucleus->m_settings.m_pszTabulatorModule:"Tabulator",
													g_pnucleus->m_settings.m_pszTabulatorHost?g_pnucleus->m_settings.m_pszTabulatorHost:"null",
													g_pnucleus->m_settings.m_nTabulatorPort, 
													szII);
			pnucleus->m_msgr.DM(MSG_ICONERROR, NULL, "Nucleus:debug", "%s Error %d (%s) sending to %s at %s:%d with: %s", debugsource,
									nReturn,errorstring,
									g_pnucleus->m_settings.m_pszTabulatorModule?g_pnucleus->m_settings.m_pszTabulatorModule:"Tabulator",
									g_pnucleus->m_settings.m_pszTabulatorHost?g_pnucleus->m_settings.m_pszTabulatorHost:"null",
									g_pnucleus->m_settings.m_nTabulatorPort, 
									szII);
											}
											else
											{
//ASRUN

	if(data.m_ucSubCmd&0x80)
	{
		// negative value!
g_pnucleus->SendAsRunMsg(CX_SENDMSG_ERROR, pChannelObj->m_nChannelID, pChannelObj->m_ppEvents[n]->m_szEventName.GetBuffer(0), autosource, "Successfully executed %s; returned error: %02x,%02x %s (%d bytes data)", 
												szII,
												data.m_ucCmd,    // the command byte
												data.m_ucSubCmd,   // the subcommand byte
												(((data.m_pucData)&&(data.m_ulDataLen))?(char*)data.m_pucData:""),
												data.m_ulDataLen
												);

			if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) 
				 pnucleus->m_msgr.DM(MSG_ICONERROR, NULL, "Nucleus:debug", "%s Action %s executed successfully; returned error: %02x,%02x %s (%d bytes data)", debugsource,
											szII,
											data.m_ucCmd,    // the command byte
											data.m_ucSubCmd,   // the subcommand byte
											(((data.m_pucData)&&(data.m_ulDataLen))?(char*)data.m_pucData:""),
											data.m_ulDataLen
										); // Sleep(250); //(Dispatch message)
	}
	else
	{

g_pnucleus->SendAsRunMsg(CX_SENDMSG_INFO, pChannelObj->m_nChannelID, pChannelObj->m_ppEvents[n]->m_szEventName.GetBuffer(0), autosource, "Successfully executed %s with return data: %02x,%02x %s (%d bytes data)", 
												szII,
												data.m_ucCmd,    // the command byte
												data.m_ucSubCmd,   // the subcommand byte
												(((data.m_pucData)&&(data.m_ulDataLen))?(char*)data.m_pucData:""),
												data.m_ulDataLen
												);

			if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) 
				 pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s Action %s executed successfully with return data: %02x,%02x %s (%d bytes data)", debugsource,
											szII,
											data.m_ucCmd,    // the command byte
											data.m_ucSubCmd,   // the subcommand byte
											(((data.m_pucData)&&(data.m_ulDataLen))?(char*)data.m_pucData:""),
											data.m_ulDataLen
										); // Sleep(250); //(Dispatch message)
	}
												// just send an ack
												g_pnucleus->m_data.m_net.SendData(NULL, g_pnucleus->m_data.m_socketTabulator, 5000, 0, NET_SND_CLNTACK, errorstring);


			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER)
			{
				pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s command %d sent [H est time %f][trig %f]: %s", debugsource,
					pChannelObj->m_ppEvents[n]->m_nEventID,
					pChannelObj->m_dblAutomationTimeEstimate, 
					pChannelObj->m_ppEvents[n]->m_dblTriggerTime, szII); // Sleep(50);//(Dispatch message)
			}
												bTriggered = true;
			if(pnucleus->m_settings.m_bUseAsynchTriggerRetrieve)
			{
				pChannelObj->AddIdentifier(pChannelObj->m_ppEvents[n]->m_nAnalyzedTriggerID);
			}
			else
			{

												if(bFirst)
												{
													bFirst = false;
													sprintf(proj, "analyzed_trigger_id = %d ", pChannelObj->m_ppEvents[n]->m_nAnalyzedTriggerID );
												}
												else
												{
													sprintf(proj, "OR analyzed_trigger_id = %d ", pChannelObj->m_ppEvents[n]->m_nAnalyzedTriggerID );
												}

												strcat(szSQL, proj);

			}

											}
//				g_pnucleus->m_data.m_bInCommand = FALSE;
	LeaveCriticalSection(&g_pnucleus->m_data.m_critTabCmd);
								_ftime(&timecheck[3]);

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER)
			{
				pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s command %d.%03d > %d.%03d [%d] %d.%03d > %d.%03d", debugsource,
									timecheck[0].time, timecheck[0].millitm,
									timecheck[1].time, timecheck[1].millitm,
					pChannelObj->m_ppEvents[n]->m_nEventID,
									timecheck[2].time, timecheck[2].millitm,
									timecheck[3].time, timecheck[3].millitm
									);
			}
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
if(fpdbg){  fprintf(fpdbg, "\r\ncommand %d.%03d > %d.%03d [%d] %d.%03d > %d.%03d\r\n",
									timecheck[0].time, timecheck[0].millitm,
									timecheck[1].time, timecheck[1].millitm,
					pChannelObj->m_ppEvents[n]->m_nEventID,
									timecheck[2].time, timecheck[2].millitm,
									timecheck[3].time, timecheck[3].millitm
										);	fflush(fpdbg); }
			}


										}
										else
										{
											// can't send!

//ASRUN
g_pnucleus->SendAsRunMsg(CX_SENDMSG_ERROR, pChannelObj->m_nChannelID, pChannelObj->m_ppEvents[n]->m_szEventName.GetBuffer(0), autosource, "Error sending to %s at %s:%d with: %s", 
												g_pnucleus->m_settings.m_pszTabulatorModule?g_pnucleus->m_settings.m_pszTabulatorModule:"Tabulator",
												g_pnucleus->m_settings.m_pszTabulatorHost?g_pnucleus->m_settings.m_pszTabulatorHost:"null",
												g_pnucleus->m_settings.m_nTabulatorPort, 
												szII);

pnucleus->m_msgr.DM(MSG_ICONERROR, NULL, "Nucleus:debug", "%s NULL socket error sending to %s at %s:%d with: %s", debugsource,
									g_pnucleus->m_settings.m_pszTabulatorModule?g_pnucleus->m_settings.m_pszTabulatorModule:"Tabulator",
									g_pnucleus->m_settings.m_pszTabulatorHost?g_pnucleus->m_settings.m_pszTabulatorHost:"null",
									g_pnucleus->m_settings.m_nTabulatorPort, 
									szII);
										}

										// send it!
									}
									else
									{



		//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "II is now: %s", szII);  Sleep(50);//(Dispatch message)
										char* pchEncodedLocal = dbSet.EncodeQuotes(szII);

/*
										if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
										{
											sprintf(errorstring, "%s:%d Trigger", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
										}
										else
										if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
										{
											sprintf(errorstring, "Omnibus %d Trigger", pChannelObj->m_nChannelID);
										}

*/

		// want to change it to the following:
//g_pnucleus->SendAsRunMsg(CX_SENDMSG_INFO, errorstring, "%s: Triggered graphics device at %s with: %s", pChannelObj->m_ppEvents[n]->m_szEventName, pChannelObj->m_ppEvents[n]->m_szHost, szII);

										if((pchEncodedLocal)&&(strlen(pchEncodedLocal)>0))
										{
											// need to find out unique identifier and put that in the vent Id for the queue, then spin a thread to get the return message back and THEN update the as-run properly

											_snprintf(szCommandSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(local, remote, action, host, timestamp, username) \
VALUES ('%s','', %d, '%s', %d.%03d, 'sys')", //HARDCODE
																		pChannelObj->m_ppEvents[n]->m_szDestModule,//"Libretto", //pEndObj->m_pszDBName?pEndObj->m_pszDBName:"Libretto",  // need to get this.
											//							pEndObj->m_pszQueue?pEndObj->m_pszQueue:"Queue",  // ok need to change this too!  argh! its not the endpoint object, that is the automation module....
																		"Command_Queue",  // ok need to change this too!  argh! its not the endpoint object, that is the automation module....
																		pchEncodedLocal?pchEncodedLocal:szII,
																		256,
																		pChannelObj->m_ppEvents[n]->m_szHost,
																		g_pnucleus->m_data.m_timebTick.time,
																		g_pnucleus->m_data.m_timebTick.millitm
																			);
		//	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Trigger SQL: %s", szCommandSQL); // Sleep(50); //(Dispatch message)
											if(dbSet.ExecuteSQL(pdbConnSet, szCommandSQL, errorstring)<DB_SUCCESS)
											{
//AS-RUN!
g_pnucleus->SendAsRunMsg(CX_SENDMSG_ERROR, pChannelObj->m_nChannelID, pChannelObj->m_ppEvents[n]->m_szEventName.GetBuffer(0), autosource, "Error triggering device at %s with: %s", pChannelObj->m_ppEvents[n]->m_szHost, szII);

										//**MSG
				g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s ERROR executing SQL: %s", debugsource, errorstring); // Sleep(50); //(Dispatch message)
											}
											else
											{
//AS-RUN!
g_pnucleus->SendAsRunMsg(CX_SENDMSG_INFO, pChannelObj->m_nChannelID, pChannelObj->m_ppEvents[n]->m_szEventName.GetBuffer(0), autosource, "Triggered device at %s with: %s", pChannelObj->m_ppEvents[n]->m_szHost, szII);


			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER)
			{
				pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s command %d sent [H est time %f][trig %f]: %s", debugsource,
					pChannelObj->m_ppEvents[n]->m_nEventID,
					pChannelObj->m_dblAutomationTimeEstimate, 
					pChannelObj->m_ppEvents[n]->m_dblTriggerTime, szII); // Sleep(50);//(Dispatch message)
			}
												bTriggered = true;

			if(pnucleus->m_settings.m_bUseAsynchTriggerRetrieve)
			{
				pChannelObj->AddIdentifier(pChannelObj->m_ppEvents[n]->m_nAnalyzedTriggerID);
			}
			else
			{

												if(bFirst)
												{
													bFirst = false;
													sprintf(proj, "analyzed_trigger_id = %d ", pChannelObj->m_ppEvents[n]->m_nAnalyzedTriggerID );
												}
												else
												{
													sprintf(proj, "OR analyzed_trigger_id = %d ", pChannelObj->m_ppEvents[n]->m_nAnalyzedTriggerID );
												}

												strcat(szSQL, proj);
			}

											}// db fail
										} // encoded ok
										if(pchEncodedLocal) free(pchEncodedLocal);
									}
/*
								// now update status
								///// DEMOTODO get item id name in view
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d WHERE analyzed_trigger_id = %d",
										pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the AnalyzedTriggerData table name
										NUCLEUS_FLAG_TRIGGERED, pChannelObj->m_ppEvents[n]->m_nAnalyzedTriggerID
									);

	//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "AnalyzedTriggerDataStatus SQL: %s", szSQL);  Sleep(50);//(Dispatch message)
								if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
								{
								//**MSG
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "ERROR executing trigger SQL: %s", errorstring); // Sleep(50); //(Dispatch message)
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "error sending command: %s", szII); // Sleep(50);//(Dispatch message)
								}
*/
								} // sz len >0
							}// ident len >0






//							if((bTriggered)&&((n>=pChannelObj->m_nEvents-1)||(pChannelObj->m_ppEvents[n]->m_dblTriggerTime<pChannelObj->m_ppEvents[n+1]->m_dblTriggerTime-0.666)||(pChannelObj->m_bTriggerEventsChanged)))  // have 2/3 a sec. or if thing have changed we need to set the ones we already did.
							if(
								  (bTriggered)
								&&(
								    (n>=pChannelObj->m_nEvents-1)
									||(pChannelObj->m_ppEvents[n]->m_dblTriggerTime<pChannelObj->m_ppEvents[n+1]->m_dblTriggerTime-0.666) // have 2/3 a sec. or if thing have changed we need to set the ones we already did.
									||(bNewRetrieve)
									)
								)  
							{

								if((pnucleus->m_settings.m_bUseAsynchTriggerRetrieve)/*&&(!pChannelObj->m_bGetTriggeredIDs)*/)
								{

									if(n>=pChannelObj->m_nEvents-1) pChannelObj->m_bTriggerEventsChanged=true; // 2.2.1.22 have to force a new event get.

						if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_trigger", "%s %d post trigger marks exist (now=%.03f) top=%d with n=%d/%d", debugsource, pChannelObj->m_nTriggeredIDs, pChannelObj->m_dblAutomationTimeEstimate, (((pChannelObj->m_nTriggeredIDs>0)&&(pChannelObj->m_pnTriggeredIDs))?pChannelObj->m_pnTriggeredIDs[0]:-1) ,n,pChannelObj->m_nEvents); //(Dispatch message)

										// give the word to exchange
									pChannelObj->m_bGetTriggeredIDs = true; 
									int nTriggeredIDs = pChannelObj->m_nTriggeredIDs; // benchmark 2.2.1.22 it might only be reduction, not zero-ness.
									LeaveCriticalSection(&pChannelObj->m_critTriggeredIDs);
									int clocktick = clock();
								//	while((pChannelObj->m_nTriggeredIDs>0)&&((clocktick+100 > clock())&&(clocktick<=clock()))&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)) Sleep(1);
									while((pChannelObj->m_nTriggeredIDs==nTriggeredIDs)&&((clocktick+100 > clock())&&(clocktick<=clock()))&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)) Sleep(1);// 2.2.1.22 benchmark
						if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_trigger", "%s entering post trigger mark ownership (now=%.03f)", debugsource, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)
									EnterCriticalSection(&pChannelObj->m_critTriggeredIDs);
						if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_trigger", "%s entered post trigger mark ownership (now=%.03f) %d triggers from %d", debugsource, pChannelObj->m_dblAutomationTimeEstimate, pChannelObj->m_nTriggeredIDs, nTriggeredIDs); //(Dispatch message)
									
								}

								else
								
								{




		if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s AnalyzedTriggerDataStatus SQL: %s", debugsource, szSQL); // Sleep(50);//(Dispatch message)
									if(dbSet.ExecuteSQL(pdbConnSet, szSQL, errorstring)<DB_SUCCESS)
									{
									//**MSG
		g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s ERROR executing trigger SQL: %s", debugsource, errorstring);  //Sleep(50); //(Dispatch message)
									}
									else
									{
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d WHERE ",
												pnucleus->m_settings.m_pszAnalyzedTriggerData?pnucleus->m_settings.m_pszAnalyzedTriggerData:"AnalyzedTriggerData",   // the AnalyzedTriggerData table name
												NUCLEUS_FLAG_TRIGGERED
											);
										bTriggered = false;
										bFirst = true;
									}
								}
							}
						}
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
if(fpdbg)
{
	_ftime(&tmb);
	fprintf(fpdbg, "[%d> %d.%03d\r\n\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
//	fclose(fpdbg);
//	fpdbg = NULL;
}
			}
			else
			{
				if(fpdbg!=NULL)
				{
	fclose(fpdbg);
	fpdbg = NULL;
				}
			}
						
						n++;
					}
				}


				if((pChannelObj->m_nEvents == 0)&&(!bInvalidTimes)) // got nothing in the buffer.
				{
if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s Nothing in trigger buffer, waiting for changes", debugsource); //(Dispatch message)
					
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "[%d> %d.%03d W0\r\n", pChannelObj->m_nChannelID, tmb.time, tmb.millitm);
					fflush(fpdbg);
				}
			}

					if(pnucleus->m_settings.m_bUseAsynchTriggerRetrieve)
					{
						EnterCriticalSection(&pChannelObj->m_critChVar);
						bNewRetrieve = pChannelObj->m_bNewEventRetrieve;
						LeaveCriticalSection(&pChannelObj->m_critChVar);
					}
					else
					{
						bNewRetrieve = pChannelObj->m_bTriggerEventsChanged;
					}


					while((!bNewRetrieve)&&/*(!pnucleus->m_data.m_bNearEventsChanged)&&*/(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))  // take out reset on near events changed, that's all the time, constantly interrupting.  just use new retrieve.
//					while((!bNewRetrieve)&&(!pnucleus->m_data.m_bNearEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
//					while((!pChannelObj->m_bTriggerEventsChanged)&&(!pnucleus->m_data.m_bNearEventsChanged)&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
					{
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "1");
					fflush(fpdbg);
				}
			}

							if(bUseLyric)
							{
	EnterCriticalSection(&pnucleus->m_data.m_critSettings);

								if(
										( pChannelObj->m_nMinLyricAlias!=pnucleus->m_settings.m_nMinLyricAlias) 
									||(	pChannelObj->m_nMaxLyricAlias!=pnucleus->m_settings.m_nMaxLyricAlias) 
									)
							{//settings have changed
								if((pnucleus->m_settings.m_nMaxLyricAlias-pnucleus->m_settings.m_nMinLyricAlias)>(pChannelObj->m_nMaxLyricAlias-pChannelObj->m_nMinLyricAlias))
								{
									// have to increase the buffer....

int nAliases = pnucleus->m_settings.m_nMaxLyricAlias - pnucleus->m_settings.m_nMinLyricAlias + 1;
if(nAliases<=0) nAliases = 10; //min set
LyricAlias_t** pNew = new LyricAlias_t*[nAliases];

if(pNew)//pChannelObj->m_ppLyricAlias)
{
	int i=0;
	while( i < nAliases)
	{
		if(i<pChannelObj->m_nNumAliasArray)
		{
			pNew[i] = pChannelObj->m_ppLyricAlias[i];
		}
		else
		{
			LyricAlias_t* pAlias = new LyricAlias_t;
			if(pAlias)
			{
				pNew[i] = pAlias;
				pAlias->szMessage = "";
				pAlias->nAlias = -1;
				pAlias->bMessageLoaded = false;
				pAlias->bMessageRead = false;
			}
		}
		i++;
	}
	delete [] pChannelObj->m_ppLyricAlias;
	pChannelObj->m_ppLyricAlias = pNew;
	pChannelObj->m_nNumAliasArray = nAliases;

}

								}
								pChannelObj->m_nMinLyricAlias=pnucleus->m_settings.m_nMinLyricAlias;
								pChannelObj->m_nMaxLyricAlias=pnucleus->m_settings.m_nMaxLyricAlias;
							}
	LeaveCriticalSection(&pnucleus->m_data.m_critSettings);


							}


			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					fprintf(fpdbg, "2");
					fflush(fpdbg);
				}
			}
						
_ftime(&pChannelObj->m_timebTriggerTick); // the last time check inside the thread


		
						Sleep(1);


						if(pnucleus->m_settings.m_bUseAsynchTriggerRetrieve)
						{
						////2.2.1.22 - here have to release ownership of the thigie to allow the next get to occur and bNewRetrieve to be set!
//							if(pChannelObj->m_nTriggeredIDs>0)  // allow this even if zero.
							{

						if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_trigger_", "%s %d empty buffer, trigger marks exist (now=%.03f)", debugsource, pChannelObj->m_nTriggeredIDs, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)

										// give the word to exchange
									pChannelObj->m_bGetTriggeredIDs = true; 
									int nTriggeredIDs = pChannelObj->m_nTriggeredIDs; // benchmark 2.2.1.22 it might only be reduction, not zero-ness.  it also might just be an information get.  give only 10 ms, since its a small while in a big while
									LeaveCriticalSection(&pChannelObj->m_critTriggeredIDs);
									int clocktick = clock();
								//	while((pChannelObj->m_nTriggeredIDs>0)&&((clocktick+10 > clock())&&(clocktick<=clock()))&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)) Sleep(1);
									while((pChannelObj->m_nTriggeredIDs==nTriggeredIDs)&&((clocktick+10 > clock())&&(clocktick<=clock()))&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)) Sleep(1);// 2.2.1.22 benchmark
						if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_trigger", "%s entering empty buffer trigger mark ownership (now=%.03f)", debugsource, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)
									EnterCriticalSection(&pChannelObj->m_critTriggeredIDs);
						if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_trigger", "%s entered empty buffer trigger mark ownership (now=%.03f) %d triggers from %d", debugsource, pChannelObj->m_dblAutomationTimeEstimate, pChannelObj->m_nTriggeredIDs, nTriggeredIDs); //(Dispatch message)
									
							}


							EnterCriticalSection(&pChannelObj->m_critChVar);
							bNewRetrieve = pChannelObj->m_bNewEventRetrieve;
							LeaveCriticalSection(&pChannelObj->m_critChVar);
						}
						else
						{
							bNewRetrieve = pChannelObj->m_bTriggerEventsChanged;
						}

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					fprintf(fpdbg, "3");
					fflush(fpdbg);
				}
			}

					}
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_LOGINLINE)
			{
				if(fpdbg)
				{
					_ftime(&tmb);
					fprintf(fpdbg, "4\r\n");
					fflush(fpdbg);
				}
			}

if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s finished waiting for changes after empty trigger buffer %dn %dne %dgk %dka", debugsource, bNewRetrieve, pnucleus->m_data.m_bNearEventsChanged, g_bKillThread, pChannelObj->m_bKillAutomationThread); //(Dispatch message)
				} //	else  // finished or expired the buffer, go around again.
			}
			else // no records yet
			{
				if(!pnucleus->m_settings.m_bUseAsynchTriggerRetrieve)  // this is only an error if we are inline.  If not, the error is handled in the automation thread.
				{
		if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
		{
	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s Trigger ERROR for %s:%d:  %s",debugsource, pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum, errorstring); // Sleep(50);//(Dispatch message)
		}
		else
		if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
		{
	pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "%s Trigger ERROR for Omnibus stream %d:  %s",debugsource, pChannelObj->m_nChannelID, errorstring); // Sleep(50);//(Dispatch message)
		}

//2.1.1.21 removed crit sec  - timeouts on other channels blaocked this!
//				LeaveCriticalSection(&pnucleus->m_data.m_critSQL);
				}

				// need to reliquish critsec for processing.
//				if(pChannelObj->m_nTriggeredIDs>0)  // allow this even if zero.
				{

			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_trigger_", "%s %d null trigger mark release (now=%.03f)", debugsource, pChannelObj->m_nTriggeredIDs, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)

							// give the word to exchange
//						pChannelObj->m_bGetTriggeredIDs = true; 
						int nTriggeredIDs = pChannelObj->m_nTriggeredIDs; // benchmark 2.2.1.22 it might only be reduction, not zero-ness.  it also might just be an information get.  give only 10 ms, since its a small while in a big while
						LeaveCriticalSection(&pChannelObj->m_critTriggeredIDs);
						int clocktick = clock();
					//	while((pChannelObj->m_nTriggeredIDs>0)&&((clocktick+10 > clock())&&(clocktick<=clock()))&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)) Sleep(1);
						while((pChannelObj->m_nTriggeredIDs==nTriggeredIDs)&&((clocktick+10 > clock())&&(clocktick<=clock()))&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread)) Sleep(1);// 2.2.1.22 benchmark
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_trigger", "%s entering null trigger mark ownership (now=%.03f)", debugsource, pChannelObj->m_dblAutomationTimeEstimate); //(Dispatch message)
						EnterCriticalSection(&pChannelObj->m_critTriggeredIDs);
			if(pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_TRIGGER_ASYNC) g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug_trigger", "%s entered null trigger mark ownership (now=%.03f) %d triggers from %d", debugsource, pChannelObj->m_dblAutomationTimeEstimate, pChannelObj->m_nTriggeredIDs, nTriggeredIDs); //(Dispatch message)
						
				}

	//			Sleep(1); // don't peg processor
			}


		} //		if(
//			  (pnucleus->m_data.m_nIndexAutomationEndpoint>=0)
//			&&(pnucleus->m_settings.m_nNumEndpointsInstalled>0)
///			&&(pnucleus->m_settings.m_ppEndpointObject)
//			&&(pnucleus->m_settings.m_ppEndpointObject[pnucleus->m_data.m_nIndexAutomationEndpoint])
//			&&(!pnucleus->m_data.m_bProcessSuspended)
//			&&(pnucleus->m_data.m_key.m_bValid)  // must have a valid license
//			&&(
//					(!pnucleus->m_data.m_key.m_bExpires)
//				||((pnucleus->m_data.m_key.m_bExpires)&&(!pnucleus->m_data.m_key.m_bExpired))
//				||((pnucleus->m_data.m_key.m_bExpires)&&(pnucleus->m_data.m_key.m_bExpireForgiveness)&&(pnucleus->m_data.m_key.m_ulExpiryDate+pnucleus->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
//				)
//			&&(
//					(!pnucleus->m_data.m_key.m_bMachineSpecific)
//				||((pnucleus->m_data.m_key.m_bMachineSpecific)&&(pnucleus->m_data.m_key.m_bValidMAC))
//				)
//			)
		else 		
		{
			Sleep(1); // dont peg processor
		}

	_ftime(&pChannelObj->m_timebTriggerTick); // the last time check inside the thread

/*
		if(delta>0.0)
		{
			wait = (double)clock();
			while((!pnucleus->m_data.m_bTriggerEventsChanged)&&(!g_bKillThread)&&(!pAobj->m_bKillAutomationThread)&&(((double)clock())-wait<delta))  // wait here until there is a change
			{
				Sleep(1); // dont peg processor
			}
		}
*/
	} // while((!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))

	LeaveCriticalSection(&pChannelObj->m_critTriggeredIDs);

			if(pChannelObj->m_bTriggerQueryThreadStarted)
			{
/*				if(pnucleus->m_settings.m_bUseAsynchTriggerRetrieve)  // only log it if it was started.
				{
					if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Ending trigger query thread for %s:%d", debugsource, pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
					}
					else
					if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Ending trigger query thread for Omnibus stream %d", debugsource, pChannelObj->m_nChannelID);
					}
					g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusTriggerThread",  errorstring);    //(Dispatch message)
					
				}
*/
				while(pChannelObj->m_bTriggerQueryThreadStarted)
				{
					Sleep(1);
				}
			}



	pChannelObj->m_nEvents = 0;

	if(pChannelObj->m_ppEvents)
	{
		while( pChannelObj->m_nEvents < 128)
		{
			if(	pChannelObj->m_ppEvents[pChannelObj->m_nEvents] ) delete pChannelObj->m_ppEvents[pChannelObj->m_nEvents];
			pChannelObj->m_nEvents++;
		}
		delete [] pChannelObj->m_ppEvents;
		pChannelObj->m_ppEvents = NULL;
		pChannelObj->m_nEvents = 0;
	}

	pChannelObj->m_nNewEvents = 0;

	if(pChannelObj->m_ppNewEvents)
	{
		while( pChannelObj->m_nNewEvents < 128)
		{
			if(	pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents] ) delete pChannelObj->m_ppNewEvents[pChannelObj->m_nNewEvents];
			pChannelObj->m_nNewEvents++;
		}
		delete [] pChannelObj->m_ppNewEvents;
		pChannelObj->m_ppNewEvents = NULL;
		pChannelObj->m_nNewEvents = 0;
	}

	if(pChannelObj->m_ppLyricAlias)
	{
		while( pChannelObj->m_nNumAliasArray > 0 )
		{
			if(	pChannelObj->m_ppLyricAlias[pChannelObj->m_nNumAliasArray-1] ) delete pChannelObj->m_ppLyricAlias[pChannelObj->m_nNumAliasArray-1];
			pChannelObj->m_nNumAliasArray--;
		}
		delete [] pChannelObj->m_ppLyricAlias;

		pChannelObj->m_ppLyricAlias = NULL;
	}

	pdb->RemoveConnection(pChannelObj->m_pdbConn);
	pChannelObj->m_pdbConn = NULL;
//	dbSet.RemoveConnection(pdbConnSet);

	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_SENTINEL)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Ending trigger thread for %s:%d", debugsource, pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	}
	else
	if((pEndObj->m_usType&NUCLEUS_DEP_AUTO_MASK) == NUCLEUS_DEP_AUTO_HELIOS)
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s Ending trigger thread for Omnibus stream %d", debugsource, pChannelObj->m_nChannelID);
	}
	g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "NucleusTriggerThread", errorstring);    //(Dispatch message)

	pChannelObj->m_bTriggerThreadStarted=false;

	Sleep(90);
	_endthread();
	Sleep(100);

}



void NucleusXMLHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;
	
	char szNucleusSource[MAX_PATH]; 
	strcpy(szNucleusSource, "NucleusXMLHandler");

	CBufferUtil bu;

	HRESULT hRes;
	try
	{  

#if _WIN32_WINNT >= 0x0400
		hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		hRes = CoInitialize(NULL);
#endif
	}
	catch(...)
	{
		if(g_pnucleus) g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, szNucleusSource, "Exception in CoInitialize");  //(Dispatch message)
	}


	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		char dberrorstring[DB_ERRORSTRING_LEN];
//		char xmlerrorstring[MAX_MESSAGE_LENGTH];
		char errorstring[MAX_MESSAGE_LENGTH];
//		char szSQL[DB_SQLSTRING_MAXLEN];
		strcpy(dberrorstring, "");
		strcpy(errorstring, "");

//		char element[MAX_MESSAGE_LENGTH];

//		AfxMessageBox(szNucleusSource);

		CNetUtil net(false); // local object for utility functions.
		CCortexUtil util;  // cortex utility functions.

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		bool bCloseCommand = false;  // make these not persistent unless a persist command comes in.
		bool bPersist = false;  // make these not persistent unless a persist command comes in.
		int nPeriodic = -1;  
		int nTimed = -1;  

		char* pchBuffer = NULL;
		unsigned long ulBufferLen = 0;
		char* pch = NULL;
		char* pchXML = NULL;
		char* pchXMLStream = NULL;
		unsigned long ulAccumulatedBufferLen = 0;
		unsigned long ulRetry = 0;

		unsigned long ulConnLastMessage = 0;
		unsigned long ulConnTimeout = 0;
		
		char filename[MAX_PATH];
		char lastrxfilename[MAX_PATH];
		char lasttxfilename[MAX_PATH];
		int nRxDupes=0;
		int nTxDupes=0;

		FILE* fp = NULL;

		_timeb timestamp;
		_timeb timeactive;
		_timeb timeperiodic;

		_ftime(&timeactive);
		_ftime(&timeperiodic);


	// 	(*(pClient->m_pulConnections))++;//already exists

		IXMLDOMDocumentPtr pDoc(__uuidof(DOMDocument));  // let's have only one object per connection
//	 g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread D"); Sleep(100); //(Dispatch message)
		HRESULT hr;//= pDoc.CreateInstance(__uuidof(DOMDocument));
//	 g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread E");  //(Dispatch message)
		hr = pDoc->put_async(VARIANT_FALSE);
//	 g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread F");  //(Dispatch message)
										
		// Remove document safety options
		IObjectSafety* pSafety=NULL;
		DWORD dwSupported, dwEnabled;
			

		if ( SUCCEEDED(pDoc->QueryInterface(IID_IObjectSafety, (void**)&pSafety)))
		{
			pSafety->GetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, &dwSupported, &dwEnabled );
			pSafety->SetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, dwSupported, 0 );
		}

		// initialize random seed
		srand ( time(NULL) );


		CCortexMessage msg;

		// allocate all the static ones
//		char* m_pchResponse[CX_XML_BUFFER_COUNT] = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};

//		AfxMessageBox("formatting");

		msg.FormatContent(CX_XML_BUFFER_CORTEXBEGIN, MAX_MESSAGE_LENGTH, "<cortex src=\"%s\" version=\"%s\">", 
			(g_pnucleus->m_settings.m_pszName?g_pnucleus->m_settings.m_pszName:"Nucleus"), 
			CX_XML_VERSION
			);


		msg.SetContent(CX_XML_BUFFER_CORTEXEND, "</cortex>");

		msg.SetContent(CX_XML_BUFFER_FLAGSEND, "</flags>");

		msg.SetContent(CX_XML_BUFFER_DATAEND, "</data>");

		//  ack and nak are the same length so we can allocate it, just replace contents later
		msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

//		AfxMessageBox("set");



		if (FAILED(hRes))
		{
			sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established, but CoInitialize failed.  Unable to process XML.", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,
				pClient->m_socket);

			if(g_pnucleus)
			{
				g_pnucleus->m_msgr.DM(MSG_ICONERROR, NULL, szNucleusSource, errorstring);  //(Dispatch message)
				g_pnucleus->SendMsg(CX_SENDMSG_ERROR, szNucleusSource, errorstring);
			}

			goto cleanup;
		}

		
		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);
		if(g_pnucleus)
		{
			g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, szNucleusSource, errorstring);  //(Dispatch message)
			g_pnucleus->SendMsg(CX_SENDMSG_INFO, szNucleusSource, errorstring);
		}



		while ( 
									(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
//								&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
								&&(pClient->m_socket!=NULL)
								&&(!bCloseCommand)
					)
		{


			pchBuffer = NULL;
			ulBufferLen = 0;

			
			int nReturnCode = net.GetLine((unsigned char**)&pchBuffer, &ulBufferLen, pClient->m_socket, NET_RCV_ONCE, errorstring);
			if(nReturnCode == NET_SUCCESS)
			{
				ulRetry = 0;
				//process any received XML.
				// have to keep accumulating until we find a </cortex> tag.
				if((pchBuffer)&&(ulBufferLen)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
				{
					int nLen = 0;
					if(pchXMLStream) nLen = ulAccumulatedBufferLen;
					pch = (char*) malloc(nLen+ulBufferLen+1);  //term 0
					if(pch)
					{
						char* pchEnd = NULL;
						char* pchNext = NULL;
						if(pchXMLStream)  // we have an old buffer.
						{
							memcpy(pch, pchXMLStream, ulAccumulatedBufferLen);
							memcpy(pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen);
							free(pchXMLStream); 
							pchXMLStream = pch;  // reassign!
							ulAccumulatedBufferLen += ulBufferLen;
							*(pch+ulAccumulatedBufferLen) = 0;  // null term
						}
						else
						{
							// this is new.
							// first we have to skip all chars that are not a '<'
							// pchEnd = strchr(pchBuffer, '<');  // have to find first tag, we are only interested in XML.
							// used to use strchr but if there are leading zeros in the buffer, we never get past them

							pchEnd = pchBuffer;
							while((*pchEnd!='<')&&(pchEnd<pchBuffer+ulBufferLen)) pchEnd++;

							if(pchEnd<pchBuffer+ulBufferLen)
							{
								strcpy(pch, pchEnd);
								ulAccumulatedBufferLen += (ulBufferLen-(pchEnd-pchBuffer));
								pchXMLStream = pch;  // reassign!
								*(pch+ulAccumulatedBufferLen) = 0;  // null term
							}
							else  // not found!
							{
								free(pch);
								pchXMLStream = NULL;
							}
						}

						if(pchBuffer) free(pchBuffer);
						pchBuffer = NULL;
						if(pchXMLStream)
						{

							pchEnd = strstr(pchXMLStream, "</cortex>");
							while((pchEnd)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
							{
							// found a token.
								pchEnd+=strlen("</cortex>");

								pchNext = pchEnd;
								while((*pchNext!='<')&&(pchNext<pchEnd+strlen(pchEnd))) pchNext++;

								if(pchNext<pchEnd+strlen(pchEnd))
								{
									// we found a remainder.
									nLen = strlen(pchNext);
									pch = (char*) malloc(nLen+1);  //term 0
									if(pch)
									{
										memcpy(pch, pchNext, nLen);
										*(pch+nLen) = 0;
									}
								}
								else pch = NULL;


								pchXML = pchXMLStream; // just use it.
								*pchEnd = 0; //null terminate it

								pchXMLStream = pch;  // take the rest of the stream.
								if(pchXMLStream) ulAccumulatedBufferLen = strlen(pchXMLStream);
								else ulAccumulatedBufferLen=0;

								// now have to deal with XML found in pchXML.
								//debug file write
								if((g_pnucleus)&&(g_pnucleus->m_settings.m_bUseXMLClientLog))
								{
//				AfxMessageBox("logging");
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_COMM) 
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, szNucleusSource, "begin log receive");  //(Dispatch message)

									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_pnucleus->m_settings.m_pszName?g_pnucleus->m_settings.m_pszName:"Nucleus"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lastrxfilename, filename)==0)
									{
										nRxDupes++;
									}
									else
									{
										nRxDupes=0;
										strcpy(lastrxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_RX.xml",nRxDupes);

//	AfxMessageBox(filename);

									fp = fopen(filename, "wb");
									if(fp)
									{
//											fwrite(filename, 1, strlen(filename),fp);
										fwrite(pchXML, 1, strlen(pchXML),fp);
										fflush(fp);
										fclose(fp);
										fp = NULL;
									}
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_COMM) 
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, szNucleusSource, "end log receive");  //(Dispatch message)

								}

								// do stuff here.

								_ftime(&timeactive);  // reset the inactivity timer.


								// first attempt to extract the rxid.
								char* pchRXID = NULL;
								int nIDlen = 0;

								char* pchFind = strstr(pchXML, "<rx rxid=\"");

								if(pchFind)
								{
									pchFind+=strlen("<rx rxid=\"");
									char* pchTerminate = pchFind;
									while(pchTerminate<pchEnd)
									{
										if(
											  (*pchTerminate == '\"')
											||(*pchTerminate == '>')
											||((pchTerminate<pchEnd)&&(*pchTerminate == '/')&&(*(pchTerminate+1) == '>'))
											)
										{
											break;
										}
										else
										{
											pchTerminate+=1;
										}
									}

									nIDlen = pchTerminate-pchFind;
									if(nIDlen>0)
									{
										pchRXID = (char*)malloc(nIDlen+1);
									}
									if(pchRXID)
									{
										memset(pchRXID, 0, nIDlen+1);
										memcpy(pchRXID, pchFind, nIDlen);

										pchTerminate = bu.XMLEncode(pchRXID); // just in case
										if(pchTerminate)
										{
											try{free(pchRXID);} catch(...){}
											pchRXID = pchTerminate;
										}
									}
								}


							//create a response UUID
								UUID uuid;
								UuidCreate(&uuid); // could check response code to be RPC_S_OK ?

					//			CoCreateGuid(&uuid);

								unsigned char* pucUUID = NULL;
								UuidToString(&uuid, &pucUUID);
								// call this later: RpcStringFree(&pucUUID);

								
								bool bResponseSent = false;  // means, sent a response already.  Otherwise have to look at the failure code and deal at the end

								msg.m_nTxStep = CX_XML_BUFFER_CORTEXBEGIN;

								//OK, let's assemble the tx element.

								if(pchRXID)
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUID);
								}


								// then, parse the XML
								msg.m_nFailure=CX_XML_ERR_SUCCESS;
								pDoc->put_async(VARIANT_FALSE);
			//				AfxMessageBox((char*)pchXML);
								hr = pDoc->loadXML((char*)pchXML);
								if(hr!=VARIANT_TRUE)
								{
								//					AfxMessageBox("bad load");
									bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML parse error: [%s]", pchXML );
									g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, szNucleusSource, errorstring);
									//let's try to send a nak.

									msg.m_nFailure=CX_XML_ERR_XMLPARSE;
									msg.m_nType = CX_XML_TYPE_UNK;
									msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
									msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML parse error %d</msg>", msg.m_nFailure);
									msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");
								}
								else
								{
	//								AfxMessageBox("yirgacheff");

if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_COMM) 
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, szNucleusSource, "begin xml parse");  //(Dispatch message)
									IXMLDOMNodeListPtr pChildNodes = pDoc->GetchildNodes();

									IXMLDOMNodePtr pNodes[CX_XML_NODECOUNT];
									int n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }

									msg.m_nType = CX_XML_TYPE_UNK;
									msg.SetType("unknown");

									bool bCommandHandled = false;
									if (pChildNodes != NULL)
									{
										IXMLDOMNodePtr pChild;
										while(pChild = pChildNodes->nextNode())
										{
											DOMNodeType nodeType;
											pChild->get_nodeType(&nodeType);

											if(nodeType == NODE_ELEMENT)
											{
												char tag[MAX_PATH]; 
												//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
												util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

											//	AfxMessageBox(tag);

												if(strcmp("cortex", tag)==0)
												{
													pNodes[CX_XML_NODE_CORTEX] = pChild;
													// now get subs to cortex

													pChildNodes = pChild->GetchildNodes();
													if (pChildNodes != NULL)
													{
														while(pChild = pChildNodes->nextNode())
														{
															pChild->get_nodeType(&nodeType);

															if(nodeType == NODE_ELEMENT)
															{
																//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
																util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

														//		AfxMessageBox(tag);

																if(strcmp("rx", tag)==0)
																{
																	pNodes[CX_XML_NODE_RX] = pChild;
																	// now get the "real" rxid;

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
																	//	AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
																			//strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));

								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("rxid", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
																				//strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));

																		//		AfxMessageBox("replace rxid.");

																				if(pchRXID)
																				{
																					try{free(pchRXID);} catch(...){}
																					pchRXID = bu.XMLEncode(tag); // just in case
																					if(pchRXID)
																					{
// reassemble tx element
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
																					}
																				}

																			}
																			else
																			if(strcmp("part", tag)==0)
																			{
																				//  multi part message
																			}
																		}
																	}

																}
																else
																if(strcmp("cmd", tag)==0)
																{
																	pNodes[CX_XML_NODE_CMD] = pChild;
																	pNodes[CX_XML_NODE_OPTIONS] = NULL;
																	pNodes[CX_XML_NODE_DATA] = NULL;

																	// now get attribs and then get subs, data and options

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
								//										AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
																			//strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));
								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("type", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
																				//strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));
																				msg.SetType(tag);

//																																		AfxMessageBox(tag);

																				if(strcmp("ack", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_ACK;
																				}
																				else
																				if(strcmp("nak", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_NAK;
																				}
																				else
																				if(strcmp("persist", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PERSIST;
																					bPersist = true;
																				}
																				else
																				if(strcmp("quit", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_QUIT;
																					bPersist = false;
																				}
																				else
																				if(strcmp("status", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_STATUS;
																				}
																				else
																				if(strcmp("bye", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_BYE;
																				}
																				else
																				if(strcmp("get_version", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_GETVERSION;
																				}
																				else
																				if(strcmp("ping", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PING;
																				}
/*
																				else

																					

/////////////////////////////////////////////////////////////////////////////////////////////////
////                                    begin Nucleus specific commands
//#define SENTINEL_XML_TYPE_GETCONN				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
//#define SENTINEL_XML_TYPE_GETLIST				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
//#define SENTINEL_XML_TYPE_GETEVENT			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
																				if(strcmp("get_conn", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETCONN;
																				}
																				else
																				if(strcmp("get_list", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETLIST;
																				}
																				else
																				if(strcmp("get_event", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETEVENT;
																				}
																					
////                                    end Nucleus specific commands
/////////////////////////////////////////////////////////////////////////////////////////////////
*/

																				
																			}
																			else
																			if(strcmp("user", tag)==0)
																			{
																			}
																			else
																			if(strcmp("password", tag)==0)
																			{
																			}
																		}
																	}


																	IXMLDOMNodeListPtr pCmdChildNodes = pChild->GetchildNodes();
																	if (pCmdChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pCmdChild;
																		while(pCmdChild = pCmdChildNodes->nextNode())
																		{
																			pCmdChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				//strncpy(tag, W2T(pCmdChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pCmdChild->GetnodeName()));
																				if(strcmp("options", tag)==0)
																				{
																					pNodes[CX_XML_NODE_OPTIONS] = pCmdChild;
																				}
																				else
																				if(strcmp("data", tag)==0)
																				{
																					pNodes[CX_XML_NODE_DATA] = pCmdChild;
																				}
																			}
																		}
																	}
																}
															}
														}
														// by here, all the necessary stuff should be filled out.

//														AfxMessageBox("here");
												

														switch(msg.m_nType)
														{
														case CX_XML_TYPE_ACK://						1	// 4.1 ack Acknowledges a message with success
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the ack

																
															} break; //case CX_XML_TYPE_ACK
														case CX_XML_TYPE_NAK://						2	// 4.2 nak Acknowledges a message with failure, or cancels
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the nak

															} break; //case CX_XML_TYPE_NAK
														case CX_XML_TYPE_PERSIST://				3	// 4.3 persist Requests a persistent connection
															{
																//persistence already done, above
																// set timeout and hearbeat intervals
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"persist\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																if(pNodes[CX_XML_NODE_OPTIONS])
																{

																	// have to return flags
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																	IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																	if (pOptChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pOptChild;
																		while(pOptChild = pOptChildNodes->nextNode())
																		{
																			pOptChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				//strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pOptChild->GetnodeName()));
																				if(strcmp("timeout", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						nTimed = atoi(pchTmp);

																						_snprintf(tag, MAX_PATH, "<timeout>%d</timeout>", nTimed);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;
																					}
																				}
																				else
																				if(strcmp("heartbeat", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						strncpy(tag, pchTmp, MAX_PATH);

																						if(strcmp("yes", tag)==0)
																						{
																							nPeriodic = 30;
																						}
																						else
																						if(strcmp("no", tag)==0)
																						{
																							nPeriodic = -1;
																						}
																						else
																						{
																							nPeriodic = atoi(tag);
																						}

																						_snprintf(tag, MAX_PATH, "<heartbeat>%d</heartbeat>", nPeriodic);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );


																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;

																					}
																				}
																			}
																		}
																	}
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																}
																
															} break; //case CX_XML_TYPE_PERSIST
														case CX_XML_TYPE_QUIT://					4	// 4.4 quit Disconnects a persistent connection
															{
																//already done, above
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"quit\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																
															} break; //case CX_XML_TYPE_QUIT
														case CX_XML_TYPE_STATUS://				5	// 4.5 status Obtains status information regarding the module
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"status\" code=\"0\">");

																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																char* pchStatus = g_pnucleus->m_data.GetStatusText(&ulDataLen);
																if(pchStatus)
																{
																	msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<status><code>0x%08x</code><text>%s</text></status>",
																		ulDataLen,
																		pchStatus	
																		);
																}
																else
																{
																	msg.SetContent(CX_XML_BUFFER_DATA, 
																		"<status><code>0xffffffff</code><text>unavailable</text></status>"
																		);

																}

																if(pchStatus){try { free(pchStatus); } catch(...){}  pchStatus=NULL;}

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


															} break; //case CX_XML_TYPE_STATUS
														case CX_XML_TYPE_BYE://						6	// 4.6 bye Causes the module to shut down
															{

g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, "Nucleus", "The Global Kill XML command has been received.");  //(Dispatch message)

																bSetGlobalKill = true;
																g_pnucleus->m_data.m_bQuietKill = true;
																bCloseCommand = true;

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"bye\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_GETVERSION://		7	// 4.7 get_version gets the version of the module as well as the build date
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_version\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<version>%s</version><build>%s %s</build>",
																		CX_CURRENT_VERSION,
																		 __DATE__,
																		 __TIME__	
																		);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_PING://					8	// 4.8 ping just sends an ack to indicate it is alive
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"ping\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE


////////////////////////////////////////////////////////
// begin Nucleus specific XML commands
/*
														case SENTINEL_XML_TYPE_GETCONN://				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_conn\" code=\"%d\">", g_pnucleus->m_data.m_nNumConnectionObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_pnucleus->m_data.m_critConns);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_pnucleus->m_data.m_ppConnObj)&&(g_pnucleus->m_data.m_nNumConnectionObjects))
																{

																	//let's see if we have any options.

																	char* pchServer = NULL;  //server name search
																	int nActive =-1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{

																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("active", tag)==0)
																					{
																				//		AfxMessageBox("active");
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}
																					//		AfxMessageBox("active 2");

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																				//			AfxMessageBox(tag);

																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_pnucleus->m_data.m_nNumConnectionObjects)
																	{

																		if(g_pnucleus->m_data.m_ppConnObj[m])
																		{
																			bool bInclude = true;
																			CNucleusConnectionObject* pObj = g_pnucleus->m_data.m_ppConnObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																	//		AfxMessageBox("here");
																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																	//			AfxMessageBox("here 2");
																			if(bInclude)
																			{
																				if(pObj->m_pAPIConn)
																				{
																					if((pObj->m_pAPIConn->m_SysData.systemfrx==0x29)
																						&&(pObj->m_pAPIConn->m_SysData.systemdf))
																					{
																						strcpy(tag, "29.97");
																					}
																					else
																					{
																						sprintf(tag, "%02x",pObj->m_pAPIConn->m_SysData.systemfrx);
																					}
																				}

																				_ftime(&timestamp);
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<aconn>\
<config active=\"%d\">\
<server>%s</server>\
<client>%s</client>\
<port>N/A</port>\
<basis>%s</basis>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<server_time>%.03f</server_time>\
<server_status>%d</server_status>\
<server_lists>%d</server_lists>\
<server_changed>%d</server_changed>\
<server_last_update>%.03f</server_last_update>\
<message>%s</message>\
</status>\
</aconn>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pObj->m_pszServerName,
																					pObj->m_pszClientName,
																					(pObj->m_pAPIConn?tag:"N/A"),
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(pObj->m_dblLastServerTimeMS/1000.0),
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? -1 : (pObj->m_pAPIConn?pObj->m_pAPIConn->m_Status:-1)),
																					(pObj->m_pAPIConn?pObj->m_pAPIConn->m_SysData.systemlistcount:-1),
																					(pObj->m_pAPIConn?pObj->m_pAPIConn->m_SysData.syschanged:-1),
																					pObj->m_dblUpdateTime,
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? (((pObj->m_ulStatus&SENTINEL_ICON_MASK) == SENTINEL_STATUS_ERROR)?"connection error":"not connected"):"")

																																										
																				);
																					
																			

																				msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}

																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;

																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_pnucleus->m_data.m_critConns);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);


															} break; //case SENTINEL_XML_TYPE_GETCONN

														case SENTINEL_XML_TYPE_GETLIST://				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_list\" code=\"%d\">", g_pnucleus->m_data.m_nNumChannelObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																
EnterCriticalSection(&g_pnucleus->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_pnucleus->m_data.m_ppChannelObj)&&(g_pnucleus->m_data.m_nNumChannelObjects))
																{

																	//let's see if we have any options.

																	char* pchServer = NULL;  //server name search
																	int nActive =-1;
																	int nID=-1;
																	int nListNum = -1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{

																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("id", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nID = atoi(pchTmp);
																								if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("list_number", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nListNum = atoi(pchTmp);
																								if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("active", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_pnucleus->m_data.m_nNumChannelObjects)
																	{

																		if(g_pnucleus->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CNucleusChannelObject* pObj = g_pnucleus->m_data.m_ppChannelObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																			if((nID>0)&&(pObj->m_nChannelID != nID))  bInclude = false;
																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				_ftime(&timestamp);

																				tlistdata* ptlist = NULL;
																				if(pObj->m_pAPIConn)
																				{
																					ptlist = &(pObj->m_pAPIConn->m_ListData[pObj->m_nHarrisListID-1]);
																				}
																				
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<alist>\
<config active=\"%d\" id=\"%d\">\
<server>%s</server>\
<list_number>%d</list_number>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<list_state>%d</list_state>\
<list_changed>%d</list_changed>\
<list_display>%d</list_display>\
<list_syschange>%d</list_syschange>\
<list_count>%d</list_count>\
<list_lookahead>%d</list_lookahead>\
<list_last_update>%.3f</list_last_update>\
</status>\
</alist>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pObj->m_nChannelID,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(ptlist?ptlist->liststate:-1),
																					(ptlist?ptlist->listchanged:-1),
																					(ptlist?ptlist->listdisplay:-1),
																					(ptlist?ptlist->listsyschange:-1),
																					(ptlist?ptlist->listcount:-1),
																					(ptlist?ptlist->lookahead:-1),
																					pObj->m_dblUpdateTime
																																										
																				);
																					
																			

																				msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_LIST_MAXPAYLOAD, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}

																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;

																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_pnucleus->m_data.m_critChannels);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case SENTINEL_XML_TYPE_GETLIST
														case SENTINEL_XML_TYPE_GETEVENT://			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
															{

																msg.ClearContent(CX_XML_BUFFER_MSG);
																int nGlobalCount =0;
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_pnucleus->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_pnucleus->m_data.m_ppChannelObj)&&(g_pnucleus->m_data.m_nNumChannelObjects))
																{

																	//let's see if we have any options.

//																	char* pchServer = NULL;  //server name search
//																	int nActive =-1;
																	int nID=-1;
																	int nLimitNum = -1;
																	int nDone = -1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{
																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("id", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nID = atoi(pchTmp);
																								if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("limit", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nLimitNum = atoi(pchTmp);
																								if(nLimitNum <=0) nLimitNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<limit>%d</limit>", nLimitNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("done", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nDone = 0;
																								else if(strcmp("1", pchTmp)==0) nDone = 1;
																								else if(strcmp("yes", pchTmp)==0) nDone = 1;
																								else if(strcmp("no", pchTmp)==0) nDone = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<done>%d</done>", nDone);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}

																					/*
																					else
																					if(strcmp("list_number", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nListNum = atoi(pchTmp);
																								if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("active", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																					* /
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_pnucleus->m_data.m_nNumChannelObjects)
																	{

																		if(g_pnucleus->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CNucleusChannelObject* pObj = g_pnucleus->m_data.m_ppChannelObj[m];

/*
																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

* /																		if(
																				//	(nID>0)&&  //commenting this out makes it mandatory!
																					(pObj->m_nChannelID != nID)
																				)  
																				bInclude = false;

//																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				// found a channel that works, now must loop thru the events, filtering if nec.

																				// need to allocate a big buffer and deal with directly, rather than allocating new for every event.
																			
/* // was this, which worked but slowly.  below is new
																				if(pObj->m_ppevents)
																				{
																					 
																					int q = 0;
																					int nCount = 0;
																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;
																						unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));

																						if(pObj->m_ppevents[q])
																						{
																							CNucleusEventObject* pEObj = pObj->m_ppevents[q];

																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;
																							if(bInclude)
																							{

																								_ftime(&timestamp);

																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
																								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																									"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_usControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CNucleusEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																								);
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																								msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );

																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
* /




																				char* pchPayload = (char*)malloc(SENTINEL_XML_EVENT_MAXPAYLOAD);
																				char* pchAppend = pchPayload;

																				// re-use errorstring, why not.
																		//		int nTimes[10] = {0,0,0,0,0,0,0,0,0,0};
																				int nClock = clock();
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_COMM) 
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, szNucleusSource, "start event payload assembly");  //(Dispatch message)

																				if((pObj->m_ppevents)&&(pchPayload))
																				{
																					unsigned long ulBufferLen = SENTINEL_XML_EVENT_MAXPAYLOAD;
																					char* pchBufferEnd = (pchPayload+SENTINEL_XML_EVENT_MAXPAYLOAD);
																					int nEventLen = 0;
																					unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));
																					int q = 0;
																					int nCount = 0;
																					int nBuffers = 1;
																					int nTempOffset;
																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;

																						if(pObj->m_ppevents[q])
																						{
																					//		nTimes[0] += (clock() - nClock); nClock = clock(); 

																							CNucleusEventObject* pEObj = pObj->m_ppevents[q];
																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;

																				//			nTimes[1] += (clock() - nClock); nClock = clock(); 
																							if(bInclude)
																							{

																							//	_ftime(&timestamp);
																					//		nTimes[2] += (clock() - nClock); nClock = clock(); 

																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
																					//		nTimes[3] += (clock() - nClock); nClock = clock(); 
																								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, 
																									"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_usControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CNucleusEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																								);
																					//		nTimes[4] += (clock() - nClock); nClock = clock(); 
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																						//	nTimes[5] += (clock() - nClock); nClock = clock(); 
																								nEventLen = strlen(errorstring)+1;//+1 for term zero

																								if(pchAppend+nEventLen>pchBufferEnd)  // if must be changed to while if SENTINEL_XML_EVENT_MAXPAYLOAD defined < MAX_MESSAGE_LENGTH (buffer vs errorstring)
																								{
																									// need to realloc
																									nBuffers++;
																									ulBufferLen = SENTINEL_XML_EVENT_MAXPAYLOAD*nBuffers;
																									char* pchBufferNew = (char*)malloc(ulBufferLen);
																									if(pchBufferNew)
																									{
																										nTempOffset = pchAppend+1 - pchPayload;
																										memcpy(pchBufferNew, pchPayload, nTempOffset);
																										try {free(pchPayload);} catch(...){}
																										pchPayload = pchBufferNew;
																										pchBufferEnd = (pchPayload+ulBufferLen);
																										pchAppend = pchPayload+nTempOffset-1;																									

																									}
																								}
																								

																								// append
																								memcpy(pchAppend, errorstring, nEventLen);
																								pchAppend += nEventLen-1; // append over the term 0 next time.


																						//	nTimes[6] += (clock() - nClock); nClock = clock(); 


/*
																								msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
* /
																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
																				msg.ClearContent(CX_XML_BUFFER_DATA);
																				if(pchPayload)
																				{
																					msg.m_pchResponse[CX_XML_BUFFER_DATA] = pchPayload;		
																					ulDataLen = pchAppend - pchPayload;
																				}
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_COMM) 
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, szNucleusSource, "end event payload assembly");  //(Dispatch message)

																			}

																		}
																		m++;
																	}

/*																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;
* /
																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_pnucleus->m_data.m_critChannels);

/* // not needed now, we tally the buffer as we go
																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																* /
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_event\" code=\"%d\">", nGlobalCount);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case SENTINEL_XML_TYPE_GETEVENT

// end Nucleus specific XML commands
////////////////////////////////////////////////////////
*/
														case CX_XML_TYPE_UNK://						0 // unknown
														default:
															{
																sprintf(errorstring, "Unsupported XML command: [%s]", (msg.m_pchType?msg.m_pchType:"") );
																g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, szNucleusSource, errorstring);
																//let's try to send a nak.

																msg.m_nType = CX_XML_TYPE_UNK;
																msg.m_nFailure=CX_XML_ERR_BADCMD;
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"%s\" code=\"%d\">", (msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Unsupported XML command [%s] (error %d)</msg>",(msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

															} break; //case CX_XML_TYPE_UNK://						0 // unknown
														}
															
														
														if(!bPersist) bCloseCommand = true;

													}
													else // no child nodes to cortex - can't identify command.
													{
														bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
														sprintf(errorstring, "Cortex XML format error: [%s]", pchXML );
														g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, szNucleusSource, errorstring);
														//let's try to send a nak.

														msg.m_nType = CX_XML_TYPE_UNK;
														msg.m_nFailure=CX_XML_ERR_NOCMD;
														msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
														msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Cortex XML format error %d</msg>", msg.m_nFailure);
														msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

													}
												}
											}
										}
									}
									else
									{
										// no child nodes, can't do nuttin.
										bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
										sprintf(errorstring, "XML format error: [%s]", pchXML );
										g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, szNucleusSource, errorstring);
										//let's try to send a nak.

										msg.m_nType = CX_XML_TYPE_UNK;
										msg.m_nFailure=CX_XML_ERR_NOCHILD;
										msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
										msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML format error %d</msg>", msg.m_nFailure);
										msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

									}
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_COMM) 
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, szNucleusSource, "end xml parse");  //(Dispatch message)

									n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }
								}

								if(!bResponseSent)
								{
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_COMM) 
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, szNucleusSource, "begin send response");  //(Dispatch message)

								// send it
		_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_pnucleus)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending response message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_pnucleus->m_msgr.DM(MSG_ICONERROR, NULL, szNucleusSource, errorstring);  //(Dispatch message)
				g_pnucleus->SendMsg(CX_SENDMSG_ERROR, szNucleusSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_COMM) 
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, szNucleusSource, "end send response");  //(Dispatch message)

								}

								// log it

								//debug file write
								if((g_pnucleus)&&(g_pnucleus->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
								{
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_COMM) 
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, szNucleusSource, "begin log response");  //(Dispatch message)
									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_pnucleus->m_settings.m_pszName?g_pnucleus->m_settings.m_pszName:"Nucleus"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lasttxfilename, filename)==0)
									{
										nTxDupes++;
									}
									else
									{
										nTxDupes=0;
										strcpy(lasttxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

									fp = fopen(filename, "wb");
									if(fp)
									{
										int b=CX_XML_BUFFER_CORTEXBEGIN;
										while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
										{
//											fwrite(filename, 1, strlen(filename),fp);
											if(msg.m_pchResponse[b])
											{
												if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

												fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
												fflush(fp);
											}
											b++;

										}
										fclose(fp);
										fp = NULL;
									}
if(g_pnucleus->m_settings.m_ulDebug&NUCLEUS_DEBUG_COMM) 
	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, szNucleusSource, "end log response");  //(Dispatch message)
								}

//AfxMessageBox("001");
								int b=CX_XML_BUFFER_TX;
								while(b<CX_XML_BUFFER_DATAEND)
								{
									if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
									{
										try { free(msg.m_pchResponse[b]); } catch(...){}
										msg.m_pchResponse[b] = NULL;
									}
									b++;
								}

//AfxMessageBox("002");
								if(pucUUID)
								{
									try { RpcStringFree(&pucUUID); } catch(...){}
								}
								pucUUID = NULL;

//AfxMessageBox("003");
								if(pchXML) 
								{
									try { free(pchXML); } catch(...){}
								}
								pchXML = NULL;
//AfxMessageBox("004");

								if(pchRXID) 
								{
									try { free(pchRXID); } catch(...){}
								}
								pchRXID = NULL;

//AfxMessageBox("005");
								if (pchXMLStream) 
									pchEnd = strstr(pchXMLStream, "</cortex>");
								else pchEnd = NULL;
//AfxMessageBox("006");
							}  // while </cortex> exists.
						}//if(pchXMLStream)
						// dont do a free(pch);  
					}  // else out of mem, so just skip
					else
					{
						// out of mem.. .log error?
					}

				} //if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))

			} // if getline succeeds.
			else
			{
				// here, we either have timed out because theres no data, or, the connection has been lost.
				// if we lost the conn, no need to re-establish, we are a client handler, let the client reconnect if they want.
				if(nReturnCode == NET_ERROR_CONN)		// connection lost
				{
					ulRetry++;

					if(g_pnucleus)
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH, "Lost connection from %d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno );

						g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, szNucleusSource, errorstring);  //(Dispatch message)
						g_pnucleus->SendMsg(CX_SENDMSG_INFO, szNucleusSource, errorstring);
					}

					break;  // break out of while loop, closes connection, starts again
				}
				else
				{
//									g_pnucleus->m_msgr.DM(MSG_ICONERROR, NULL, szNucleusSource, "check %d", clock());  //(Dispatch message)

					// we prob just havent gotten a message because no data.  
					// so, check to see if its time for publication service
									// no, do this in the stall loop


				}

			} // end of else from if(nReturnCode == NET_SUCCESS)

			// and lets check if we've timed out on the connection, no matter what the success was
			_ftime( &timestamp );
			if(
				  (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				&&(ulConnTimeout>0)
				&&(ulConnLastMessage>0)
				&&((ulConnLastMessage + ulConnTimeout) < (unsigned long)timestamp.time )
				)
			{
				// we timed out, need to disconnect
				ulRetry++;


				sprintf(errorstring, "Connection from %d.%d.%d.%d timed out.", 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno);

				if(g_pnucleus)
				{
					g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, szNucleusSource, errorstring);  //(Dispatch message)
					g_pnucleus->SendMsg(CX_SENDMSG_INFO, szNucleusSource, errorstring);
				}

				if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;} //free this memory before breaking out of the while.

				break;  // break out of while loop, closes connection, starts again
			}

			if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;}
			
			if(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) Sleep(1);  // dont peg processor




/*
			// following line fills out the data structure
//			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);
			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Nucleus:XMLHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Nucleus:XMLHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Nucleus:XMLHandlerThread");
						}
					}
				}
			}
*/
			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Nucleus:XMLHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
//						g_pnucleus->m_msgr.DM(MSG_ICONERROR, NULL, szNucleusSource, "check %d", clock());  //(Dispatch message)


						if(nPeriodic>0)
						{
							_ftime( &timestamp );
							if((timeperiodic.time + nPeriodic)< timestamp.time)
							{
								// send asynchronous message

								//create a response UUID
								UUID uuida;
								UuidCreate(&uuida); // could check response code to be RPC_S_OK ?

								unsigned char* pucUUIDa = NULL;
								UuidToString(&uuida, &pucUUIDa);

								msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUIDa);

								if(pucUUIDa)
								{
									try { RpcStringFree(&pucUUIDa); } catch(...){}
								}
								pucUUIDa = NULL;


								msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"system\" code=\"0\">");

								msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
								msg.ClearContent(CX_XML_BUFFER_FLAGS);

								unsigned long ulDataLen = 0;
								char* pchStatus = g_pnucleus->m_data.GetStatusText(&ulDataLen);
								if(pchStatus)
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0x%08x</code><text>%s</text></status><time>%d.%03d</time></system>",
										ulDataLen,
										pchStatus,
										timestamp.time, timestamp.millitm
										);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0xffffffff</code><text>unavailable</text></status><time>%d.%03d</time></system>",
										timestamp.time, timestamp.millitm
										);

								}

								if(pchStatus){try { free(pchStatus); } catch(...){} pchStatus=NULL;}

								ulDataLen = 0;
								if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
								{
									ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
								}
								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
									ulDataLen,
									net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
									);

								msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>%s status message</msg>", 
									(g_pnucleus->m_settings.m_pszName?g_pnucleus->m_settings.m_pszName:"Nucleus"));

								msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


								// now, send and log.
	////////////////////////////////////////////// code copied from above.  if modified, must be 2 places.  sorry...
									// send it
			_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_pnucleus)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending system message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_pnucleus->m_msgr.DM(MSG_ICONERROR, NULL, szNucleusSource, errorstring);  //(Dispatch message)
				g_pnucleus->SendMsg(CX_SENDMSG_ERROR, szNucleusSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
								
	//AfxMessageBox("oop");
									// log it

									//debug file write
									if((g_pnucleus)&&(g_pnucleus->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
									{
										strcpy(filename, "Logs");
										_mkdir(filename);  // if exists already np
										strcat(filename, "\\");
										sprintf(errorstring, "%d.%d.%d.%d", 
											pClient->m_si.sin_addr.s_net, 
											pClient->m_si.sin_addr.s_host, 
											pClient->m_si.sin_addr.s_lh, 
											pClient->m_si.sin_addr.s_impno);

										strcat(filename, errorstring);

										_mkdir(filename);  // if exists already np

		//								_ftime( &timestamp );  // use timestamp we just had..

										tm* theTime = localtime( &timestamp.time	);

										sprintf(errorstring, "\\%s_", 
											(g_pnucleus->m_settings.m_pszName?g_pnucleus->m_settings.m_pszName:"Nucleus"));

										strcat(filename, errorstring);

										char filenametemp[MAX_PATH];
										strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

										strcat(filename, filenametemp);

										int nOffset = strlen(filename);
										sprintf(filename+nOffset,"%03d",timestamp.millitm);

										if (strcmp(lasttxfilename, filename)==0)
										{
											nTxDupes++;
										}
										else
										{
											nTxDupes=0;
											strcpy(lasttxfilename, filename);
										}
										nOffset = strlen(filename);
										sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

										fp = fopen(filename, "wb");
										if(fp)
										{
											int b=CX_XML_BUFFER_CORTEXBEGIN;
											while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
											{
	//											fwrite(filename, 1, strlen(filename),fp);
												if(msg.m_pchResponse[b])
												{
													if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

													fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
													fflush(fp);
												}
												b++;

											}
											fclose(fp);
											fp = NULL;
										}
									}


									b=CX_XML_BUFFER_TX;
									while(b<CX_XML_BUFFER_DATAEND)
									{
										if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
										{
											try { free(msg.m_pchResponse[b]); } catch(...){}
											msg.m_pchResponse[b] = NULL;
										}
										b++;
									}
	/////////////////////////////////////////////
							}

						}

						//and check timeout
						if(nTimed>0)
						{
							_ftime( &timestamp );
							if((timeactive.time + nTimed) < timestamp.time)
							{
								//kill it

								bCloseCommand = true;
							}
							
						}


						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} 

cleanup:
//		AfxMessageBox("cleanup");
//AfxMessageBox("010");

//		pDoc->Release();  // destructor calls this so don't do it here. 
		if(pSafety) pSafety->Release();

//AfxMessageBox("011");

// cleanup:  //was here but want to uninit com etc
//AfxMessageBox("012");

		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d closed.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);

//AfxMessageBox("013");
		if(g_pnucleus)
		{
			g_pnucleus->m_msgr.DM(MSG_ICONINFO, NULL, szNucleusSource, errorstring);  //(Dispatch message)
			g_pnucleus->SendMsg(CX_SENDMSG_INFO, szNucleusSource, errorstring);
		}

//AfxMessageBox("014");

		if(pch!=NULL) { try{free(pch);} catch(...) {} } // must free the data buffer 

//AfxMessageBox("015");

		shutdown(pClient->m_socket, SD_BOTH);
//AfxMessageBox("016");
		closesocket(pClient->m_socket);
//AfxMessageBox("017");
		(*(pClient->m_pulConnections))--;
//AfxMessageBox("018");

	}
//		AfxMessageBox("end cleanup");
//AfxMessageBox("019");

	try
	{  
		CoUninitialize(); //XML
	}
	catch(...)
	{
		if(g_pnucleus) g_pnucleus->m_msgr.DM(MSG_ICONERROR, NULL, szNucleusSource, "Exception in CoUninitialize");  //(Dispatch message)
	}

//AfxMessageBox("020");

	try
	{
		delete pClient; // was created with new in the thread that spawned this one.
	}	catch(...) {}

//AfxMessageBox("021");
	if (bSetGlobalKill)	//g_bKillThread = true;
		((CNucleusHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}


void NucleusTabulatorConnectionThread(void* pvArgs)
{
	while((g_pnucleus == NULL)&&(!g_bKillThread)) Sleep (10);
	
	g_pnucleus->m_data.m_bTabulatorThreadStarted = true;

	char errorstring[MAX_MESSAGE_LENGTH];
	bool bSocketReport = false;

	int nClock = -1; 
	_timeb timebTick;
	_ftime(&timebTick);

	CBufferUtil util;

	// stay here until the automation thread starts, this way we know  that the messager dispatch has started.
	while((!g_pnucleus->m_data.m_bTabulatorThreadKill)&&(!g_bKillThread)&&(!g_pnucleus->m_data.m_bAutomationThreadStarted))
	{
		Sleep (10);
	}

	while((!g_pnucleus->m_data.m_bTabulatorThreadKill)&&(!g_bKillThread))
	{

		// monitor connection to Tabulator
		if((g_pnucleus->m_data.m_socketTabulator == NULL)&&(!g_pnucleus->m_data.m_bTabulatorThreadKill)&&(!g_bKillThread))
		{
			// Open connection
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing control client to %s:%d", g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort); 
			
			g_pnucleus->m_msgr.DM(MSG_ICONNONE, NULL, "TabulatorConnection", errorstring );  //(Dispatch message)

//			while(g_pnucleus->m_data.m_bInCommand) Sleep(1);
//			g_pnucleus->m_data.m_bInCommand = TRUE;
	EnterCriticalSection(&g_pnucleus->m_data.m_critTabCmd);

			if(g_pnucleus->m_data.m_net.OpenConnection(g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort, &g_pnucleus->m_data.m_socketTabulator, 
				(unsigned long)g_pnucleus->m_settings.m_nTabulatorSendTimeoutMilliseconds, (unsigned long)g_pnucleus->m_settings.m_nTabulatorReceiveTimeoutMilliseconds, errorstring)<NET_SUCCESS)
			{
//				g_pnucleus->m_data.m_bInCommand = FALSE;
	LeaveCriticalSection(&g_pnucleus->m_data.m_critTabCmd);

				if(!bSocketReport)
				{
		 g_pnucleus->m_msgr.DM(MSG_ICONERROR, NULL,  "TabulatorConnection", errorstring );  //(Dispatch message)
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error connecting Tabulator at %s:%d. %s", g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort, errorstring); 
		 g_pnucleus->m_msgr.DM(MSG_ICONERROR, NULL, "TabulatorConnection", errorstring);  //(Dispatch message)
					
					g_pnucleus->SendMsg(CX_SENDMSG_ERROR, "TabulatorConnection", errorstring);
					bSocketReport = true;
				}
				g_pnucleus->m_data.m_socketTabulator = NULL;
			}
			else
			{			
			//	g_pnucleus->m_data.m_bInCommand = FALSE;
	LeaveCriticalSection(&g_pnucleus->m_data.m_critTabCmd);

				bSocketReport = false;
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connected to Tabulator at %s:%d", g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort); 
		 g_pnucleus->m_msgr.DM(MSG_ICONNONE, NULL, "TabulatorConnection", errorstring );  //(Dispatch message)
				g_pnucleus->SendMsg(CX_SENDMSG_INFO, "TabulatorConnection", errorstring);


				// here, let's send a HELLO command.
				if(g_pnucleus->m_settings.m_bTabulatorUseHello)
				{
					CNetData data;
//					while(g_pnucleus->m_data.m_bInCommand) Sleep(1);
//					g_pnucleus->m_data.m_bInCommand =TRUE;
	EnterCriticalSection(&g_pnucleus->m_data.m_critTabCmd);

					unsigned char* puc = NULL;
					data.m_ucCmd = TABULATOR_CMD_HELLO;
					data.m_pucData=NULL;
					data.m_ulDataLen = 0;
					data.m_ucType = (NET_TYPE_PROTOCOL1|NET_TYPE_KEEPOPEN|NET_SND_KEEPOPENLCL); // has no data and no subcommand.

					// 
		if(g_pnucleus->m_settings.m_bTabulatorLogHello)
		{
			g_pnucleus->m_msgr.DM(MSG_ICONNONE, NULL, "TabulatorConnection", "Sending initial hello to Tabulator at %s:%d",  
					g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort
				);  //(Dispatch message)
		}

					int n = g_pnucleus->m_data.m_net.SendData(&data, g_pnucleus->m_data.m_socketTabulator, (unsigned long)g_pnucleus->m_settings.m_nTabulatorSendTimeoutMilliseconds, g_pnucleus->m_settings.m_nTabulatorCommandRetries, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT);//|NET_SND_NO_RX);

					if(n < NET_SUCCESS)
					{
						g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Net error %d sending cmd 0x%02x (hello)", n, TABULATOR_CMD_HELLO );  //(Dispatch message)
						g_pnucleus->m_data.m_net.CloseConnection(g_pnucleus->m_data.m_socketTabulator);
						g_pnucleus->m_data.m_socketTabulator = NULL;

		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Closed connection to Tabulator at %s:%d due to hello send error %d", g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort, n); 
		g_pnucleus->m_msgr.DM(MSG_ICONNONE, NULL, "TabulatorConnection", errorstring );  //(Dispatch message)
		g_pnucleus->SendMsg(CX_SENDMSG_ERROR, "TabulatorConnection", errorstring);

					}
					else
					{
						g_pnucleus->m_data.m_net.SendData(NULL, g_pnucleus->m_data.m_socketTabulator, 5000, 0, NET_SND_CLNTACK, NULL);
						_ftime(&g_pnucleus->m_data.m_timeLastCommandTick);
		if(g_pnucleus->m_settings.m_bTabulatorLogHello)
		{
			g_pnucleus->m_msgr.DM(MSG_ICONNONE, NULL, "TabulatorConnection", "Successfully sent initial hello to Tabulator at %s:%d",  
				g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort
				);  //(Dispatch message)
		}
					}
			/// hello command
			//		g_pnucleus->m_data.m_bInCommand =FALSE;
	LeaveCriticalSection(&g_pnucleus->m_data.m_critTabCmd);
				}

			}

		}


		if((g_pnucleus->m_data.m_socketTabulator != NULL)&&(!g_pnucleus->m_data.m_bTabulatorThreadKill)&&(!g_bKillThread)) // check for disconnection
		{
	EnterCriticalSection(&g_pnucleus->m_data.m_critTabCmd);
//			if(!g_pnucleus->m_data.m_bInCommand)
			{
				timeval tv;
				tv.tv_sec = 0; 
				tv.tv_usec = 500;  // timeout value
				int nNumSockets;
				fd_set fds;

				FD_ZERO(&fds);  // Zero this out each time
				FD_SET(g_pnucleus->m_data.m_socketTabulator, &fds);

				nNumSockets = select(0, &fds, NULL, NULL, &tv);
				if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
				{
					nClock = -1;
				}
				else
				if(
						(nNumSockets==0) // 0 = timed out, -1 = error
					||(!(FD_ISSET(g_pnucleus->m_data.m_socketTabulator, &fds)))
					) 
				{ 
					nClock = -1;

					// here process hello message if necessary

					// here, let's send a HELLO command.
					if((g_pnucleus->m_settings.m_bTabulatorUseHello)&&(g_pnucleus->m_settings.m_nTabulatorHelloIntervalMS>0))
					{
						_ftime(&timebTick);


						if(
							  (timebTick.time > g_pnucleus->m_data.m_timeLastCommandTick.time+(g_pnucleus->m_settings.m_nTabulatorHelloIntervalMS/1000))
							||
							  (
							    (timebTick.time == g_pnucleus->m_data.m_timeLastCommandTick.time+(g_pnucleus->m_settings.m_nTabulatorHelloIntervalMS/1000))
							  &&(timebTick.millitm > g_pnucleus->m_data.m_timeLastCommandTick.millitm+(g_pnucleus->m_settings.m_nTabulatorHelloIntervalMS%1000))
								)
							)
						{
							CNetData data;
		//					while(g_pnucleus->m_data.m_bInCommand) Sleep(1);
		//					g_pnucleus->m_data.m_bInCommand =TRUE;
			EnterCriticalSection(&g_pnucleus->m_data.m_critTabCmd);

							unsigned char* puc = NULL;
							data.m_ucCmd = TABULATOR_CMD_HELLO;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							data.m_ucType = (NET_TYPE_PROTOCOL1|NET_TYPE_KEEPOPEN|NET_SND_KEEPOPENLCL); // has no data and no subcommand.

							// 
		if(g_pnucleus->m_settings.m_bTabulatorLogHello)
		{
			g_pnucleus->m_msgr.DM(MSG_ICONNONE, NULL, "TabulatorConnection", "Sending periodic hello to Tabulator at %s:%d",  
				g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort
				);  //(Dispatch message)
		}

							int n = g_pnucleus->m_data.m_net.SendData(&data, g_pnucleus->m_data.m_socketTabulator, (unsigned long)g_pnucleus->m_settings.m_nTabulatorSendTimeoutMilliseconds, g_pnucleus->m_settings.m_nTabulatorCommandRetries, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT);//|NET_SND_NO_RX);

							if(n < NET_SUCCESS)
							{
								g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Net error %d sending cmd 0x%02x (hello).", n, TABULATOR_CMD_HELLO );  //(Dispatch message)
								g_pnucleus->m_data.m_net.CloseConnection(g_pnucleus->m_data.m_socketTabulator);
								g_pnucleus->m_data.m_socketTabulator = NULL;

				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Closed connection to Tabulator at %s:%d due to hello send error %d.", g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort, n); 
				g_pnucleus->m_msgr.DM(MSG_ICONNONE, NULL, "TabulatorConnection", errorstring );  //(Dispatch message)
				g_pnucleus->SendMsg(CX_SENDMSG_ERROR, "TabulatorConnection", errorstring);

							}
							else
							{
								g_pnucleus->m_data.m_net.SendData(NULL, g_pnucleus->m_data.m_socketTabulator, 5000, 0, NET_SND_CLNTACK, NULL);
								_ftime(&g_pnucleus->m_data.m_timeLastCommandTick);

		if(g_pnucleus->m_settings.m_bTabulatorLogHello)
		{
			g_pnucleus->m_msgr.DM(MSG_ICONNONE, NULL, "TabulatorConnection", "Successfully sent periodic hello to Tabulator at %s:%d",  
				g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort
				);  //(Dispatch message)
		}

							}
					/// hello command
					//		g_pnucleus->m_data.m_bInCommand =FALSE;
			LeaveCriticalSection(&g_pnucleus->m_data.m_critTabCmd);
						}
					}



				}
				else // there is recv data.
				{ // wait some delay.
					if(nClock<0)
					{
						nClock = clock();
					}
					else
					{
						if((clock() > nClock+g_pnucleus->m_settings.m_nTabulatorConsumeDataMS))//&&(!g_pnucleus->m_data.m_bInCommand))
						{

					//		g_pnucleus->m_data.m_bInCommand = TRUE;


							if(g_pnucleus->m_settings.m_bTabulatorReinitOnData)
							{

							// one second and nothing - could be a disconnect - just reinit, this is unsolicited data anyway
		g_pnucleus->m_data.m_net.CloseConnection(g_pnucleus->m_data.m_socketTabulator);
		g_pnucleus->m_data.m_socketTabulator = NULL;
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Closed connection from Tabulator at %s:%d due to unsolicited data", g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort); 
		g_pnucleus->m_msgr.DM(MSG_ICONNONE, NULL, "TabulatorConnection", errorstring );  //(Dispatch message)
		g_pnucleus->SendMsg(CX_SENDMSG_ERROR, "TabulatorConnection", errorstring);
							}
							else // just consume the data.
							{
								unsigned char* puc = NULL;
								unsigned long ulLen=0;
								char szError[8096];
								int nReturn=g_pnucleus->m_data.m_net.GetLine(&puc, &ulLen, g_pnucleus->m_data.m_socketTabulator, NET_RCV_ONCE, szError);
								if(nReturn<NET_SUCCESS)
								{
		g_pnucleus->m_data.m_net.CloseConnection(g_pnucleus->m_data.m_socketTabulator);
		g_pnucleus->m_data.m_socketTabulator = NULL;
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Closed connection from Tabulator at %s:%d unable to receive unsolicited data", g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort); 
		g_pnucleus->m_msgr.DM(MSG_ICONNONE, NULL, "TabulatorConnection", errorstring );  //(Dispatch message)
		g_pnucleus->SendMsg(CX_SENDMSG_ERROR, "TabulatorConnection", errorstring);

								}
								else
								{

									if(ulLen==0) //disconnected
									{
		g_pnucleus->m_data.m_net.CloseConnection(g_pnucleus->m_data.m_socketTabulator);
		g_pnucleus->m_data.m_socketTabulator = NULL;
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Closed connection from Tabulator at %s:%d disconnected receiving unsolicited data", g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort); 
		g_pnucleus->m_msgr.DM(MSG_ICONNONE, NULL, "TabulatorConnection", errorstring );  //(Dispatch message)
		g_pnucleus->SendMsg(CX_SENDMSG_ERROR, "TabulatorConnection", errorstring);
									}
									else
									{
										//log and free.

										if(puc)
										{
											unsigned long ulRecv = ulLen;
											char* pchRecv = util.ReadableHex((char*)puc, &ulRecv, MODE_FULLHEX);

											if(pchRecv)
											{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Unsolicited data from Tabulator at %s:%d. Received %d bytes: %s", g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort, ulLen, pchRecv); 
												free(pchRecv);
											}
											else
											{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Unsolicited data from Tabulator at %s:%d. Received %d bytes: %s", g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort, ulLen, puc); 
											}
		g_pnucleus->m_msgr.DM(MSG_ICONNONE, NULL, "TabulatorConnection", errorstring );  //(Dispatch message)
		g_pnucleus->SendMsg(CX_SENDMSG_INFO, "TabulatorConnection", errorstring);

										}

									}
								}
								if(puc) free(puc);




							}

					//		g_pnucleus->m_data.m_bInCommand = FALSE;

						}
					}
				}
			}
	LeaveCriticalSection(&g_pnucleus->m_data.m_critTabCmd);

		}


		Sleep(1);
	}

	if(g_pnucleus->m_data.m_socketTabulator)
	{
		// I could send the "disconnect" command to Tabulator so it does not throw an error.... someday
	EnterCriticalSection(&g_pnucleus->m_data.m_critTabCmd);
//		g_pnucleus->m_data.m_bInCommand = TRUE;
		g_pnucleus->m_data.m_net.CloseConnection(g_pnucleus->m_data.m_socketTabulator);
		g_pnucleus->m_data.m_socketTabulator = NULL;
//		g_pnucleus->m_data.m_bInCommand = FALSE;
	LeaveCriticalSection(&g_pnucleus->m_data.m_critTabCmd);
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnected from Tabulator at %s:%d", g_pnucleus->m_settings.m_pszTabulatorHost, g_pnucleus->m_settings.m_nTabulatorPort); 
		g_pnucleus->m_msgr.DM(MSG_ICONNONE, NULL, "TabulatorConnection", errorstring );  //(Dispatch message)
		g_pnucleus->SendMsg(CX_SENDMSG_INFO, "TabulatorConnection", errorstring);
	}

	g_pnucleus->m_data.m_bTabulatorThreadStarted = false;


}

void NucleusMsgSQLThread(void* pvArgs)
{
	if(g_pnucleus)
	{
		char* szSQL = (char*)pvArgs;
		char dberrorstring[DB_ERRORSTRING_LEN];
		EnterCriticalSection(&g_pnucleus->m_data.m_critMessageSQL);
		if(g_pnucleus->m_data.m_pdbMessage->ExecuteSQL(g_pnucleus->m_data.m_pdbMessageConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&g_pnucleus->m_data.m_critMessageSQL);
			g_pnucleus->m_data.IncrementDatabaseMods(g_pnucleus->m_settings.m_pszMessages, dberrorstring);
		}
		else
		{
			LeaveCriticalSection(&g_pnucleus->m_data.m_critMessageSQL);
		}
	}

	if(pvArgs)
	{
		free(pvArgs);
	}
}
