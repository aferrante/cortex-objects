; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CNucleusDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Nucleus.h"
LastPage=0

ClassCount=5
Class1=CAboutDlg
Class2=CNucleusApp
Class3=CNucleusHandler
Class4=CNucleusDlg
Class5=CNucleusSettings

ResourceCount=3
Resource1=IDD_NUCLEUS_DIALOG
Resource2=IDD_ABOUTBOX
Resource3=IDR_MENU1

[CLS:CAboutDlg]
Type=0
HeaderFile=nucleusdlg.h
ImplementationFile=nucleusdlg.cpp
BaseClass=CDialog
LastObject=CAboutDlg

[CLS:CNucleusApp]
Type=0
BaseClass=CWinApp
HeaderFile=Nucleus.h
ImplementationFile=Nucleus.cpp
Filter=N
VirtualFilter=AC
LastObject=CNucleusApp

[CLS:CNucleusHandler]
Type=0
BaseClass=CWnd
HeaderFile=NucleusHandler.h
ImplementationFile=NucleusHandler.cpp
Filter=W
VirtualFilter=WC
LastObject=CNucleusHandler

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=8
Control1=IDC_STATICTEXT_TITLE,static,1342308481
Control2=IDC_STATICTEXT_URL,static,1208090625
Control3=IDOK,button,1342373889
Control4=IDC_STATIC_LOGO,static,1342177294
Control5=IDC_URLFRAME,static,1342177298
Control6=IDC_STATIC_BUILD,static,1342308353
Control7=IDC_STATIC_URL,static,1342177294
Control8=IDC_STATICTEXT_COPYRIGHT,static,1342308353

[MNU:IDR_MENU1]
Type=1
Class=?
Command1=ID_CMD_ABOUT
Command2=ID_CMD_SHOWWND
Command3=ID_CMD_EXIT
Command4=ID_CMD_ABOUT
Command5=ID_CMD_SHOWWND
Command6=ID_CMD_EXIT
CommandCount=6

[CLS:CNucleusDlg]
Type=0
HeaderFile=NucleusDlg.h
ImplementationFile=NucleusDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CNucleusDlg

[CLS:CNucleusSettings]
Type=0
HeaderFile=NucleusSettings.h
ImplementationFile=NucleusSettings.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CNucleusSettings

[DLG:IDD_NUCLEUS_DIALOG]
Type=1
Class=CNucleusDlg
ControlCount=7
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_SETTINGS,button,1208025216
Control4=IDC_LIST1,SysListView32,1350681613
Control5=IDC_STATIC_STATUSTEXT,static,1342308352
Control6=IDC_STATIC_PROGBAR,static,1342177294
Control7=IDC_TREE1,SysTreeView32,1082130487

