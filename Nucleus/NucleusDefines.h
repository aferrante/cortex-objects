// NucleusDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(NUCLEUSDEFINES_H_INCLUDED)
#define NUCLEUSDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define NUCLEUS_CURRENT_VERSION		"2.2.1.22"


// modes
#define NUCLEUS_MODE_DEFAULT			0x00000000  // exclusive
#define NUCLEUS_MODE_LISTENER		0x00000001  // exclusive
#define NUCLEUS_MODE_CLONE				0x00000002  // exclusive
#define NUCLEUS_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define NUCLEUS_MODE_VOLATILE		0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define NUCLEUS_MODE_MASK				0x0000000f  // 

// default port values.
//#define NUCLEUS_PORT_FILE				80		
#define NUCLEUS_PORT_CMD					20680		
#define NUCLEUS_PORT_STATUS			20681		

#define NUCLEUS_PORT_INVALID			0	

#define NUCLEUS_FLAGS_INVALID			0xffffffff	 
#define NUCLEUS_FLAG_DISABLED			0x0000	 // default
#define NUCLEUS_FLAG_ENABLED			0x0001	
#define NUCLEUS_FLAG_FOUND				0x1000
#define NUCLEUS_FLAG_EXTENDED_TAV				0x0100  //extended trigger analysis view  added 2.2.1.20 (9 July 2016)

#define NUCLEUS_FLAG_OVERRIDDEN			0x00000100
#define NUCLEUS_FLAG_TRIGGERED			0x01000000
#define NUCLEUS_FLAG_ANALYZED				0x00000010
#define NUCLEUS_FLAG_REANALYZED			0x00000020

#define NUCLEUS_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded

// installed dependencies
#define NUCLEUS_DEP_UNKNOWN								0x0000  // type unknown
#define NUCLEUS_DEP_DATA_ARCHIVIST				0x0002  // Archivist is installed
#define NUCLEUS_DEP_DATA_MASK							0x0002  // mask for data bit  // there is only one choice at the moment!
#define NUCLEUS_DEP_AUTO_HELIOS						0x0010  // Omnibus is installed (Helios)
#define NUCLEUS_DEP_AUTO_SENTINEL					0x0020  // Harris is installed (Sentinel)
#define NUCLEUS_DEP_AUTO_MASK							0x00f0  // mask for automation bit
#define NUCLEUS_DEP_EDGE_PROSPERO					0x0100  // Miranda is installed (Prospero)
#define NUCLEUS_DEP_EDGE_LUMINARY					0x0200  // Chyron (CAL) is installed (Luminary)
#define NUCLEUS_DEP_EDGE_RADIANCE					0x0300  // Orad is installed (Radiance)
#define NUCLEUS_DEP_EDGE_LIBRETTO					0x0400  // Chyron (Lyric) is installed (Libretto)
#define NUCLEUS_DEP_EDGE_BARBERO					0x0500  // Evertz inerface FTP
#define NUCLEUS_DEP_EDGE_MASK							0x0f00  // mask for edge device bit (device to have stuff transferred to)

// rule types
#define NUCLEUS_RULE_TYPE_UNKNOWN						0x00000000  // type unknown
#define NUCLEUS_RULE_TYPE_PRESMASTER				0x00000001  // for Helios, with presmaster data in <data> XML field
#define NUCLEUS_RULE_TYPE_XML								0x00000002  // for Helios, with logo data in <logos> XML sub-fields .. free form tho
#define NUCLEUS_RULE_TYPE_IMAGESTORE				0x00000003  // type unknown

//comparison types
/*
create table LU_Comparison_Type (compare_type char(2) NOT NULL, name varchar(32), description varchar(256));
insert into LU_Comparison_Type (compare_type, name, description) values ('==', '<code>&#61;</code>', 'Equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('>', '<code>&#62;</code>', 'Greater than');
insert into LU_Comparison_Type (compare_type, name, description) values ('<', '<code>&#60;</code>', 'Less than');
insert into LU_Comparison_Type (compare_type, name, description) values ('>=', '<code>&#8805;</code>', 'Greater than or equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('<=', '<code>&#8804;</code>', 'Less than or equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('!=', '<code>&#8800;</code>', 'Not equal to');
insert into LU_Comparison_Type (compare_type, name, description) values ('~=', '<code>&#8776;</code>', 'Partial Comparison');
insert into LU_Comparison_Type (compare_type, name, description) values ('&', '<code>&#8715;</code>', 'Contains any bitflags');
insert into LU_Comparison_Type (compare_type, name, description) values ('=&', '<code>&#8839;</code>', 'Contains all bitflags');
insert into LU_Comparison_Type (compare_type, name, description) values ('!&', '<code>&#8713;</code>', 'Does not contain bitflags');
*/
#define NUCLEUS_RULE_COMPARE_UNKNOWN				0xffff  // type unknown
#define NUCLEUS_RULE_COMPARE_EQUALS					0x0001  // == equals
#define NUCLEUS_RULE_COMPARE_GT							0x0002  // >  greater than
#define NUCLEUS_RULE_COMPARE_LT							0x0003  // <  less than
#define NUCLEUS_RULE_COMPARE_GTOE						0x0004  // >= greater than or equal to
#define NUCLEUS_RULE_COMPARE_LTOE						0x0005  // <= less than or equal to
#define NUCLEUS_RULE_COMPARE_NOTEQUAL				0x0006  // != not equal to
#define NUCLEUS_RULE_COMPARE_PARTIAL				0x0007  // ~= partial comparison
#define NUCLEUS_RULE_COMPARE_AND						0x0008  // &  contains any of the bitflags
#define NUCLEUS_RULE_COMPARE_ALL						0x0009  // =& contains all the bitflags
#define NUCLEUS_RULE_COMPARE_NOT						0x000a  // !& does not contain any bitflag
#define NUCLEUS_RULE_COMPARE_NOTLIKE				0x000b  // !~ not partial comparison (NOT LIKE)
#define NUCLEUS_RULE_COMPARE_MOD						0x000c  // %  MOD operator gives a nonzero answer

// CALENDAR DATE AND TIME
#define NUCLEUS_RULE_COMPARE_CDAT_EQUALS		0x0011  // C= equals
#define NUCLEUS_RULE_COMPARE_CDAT_GT				0x0012  // C> greater than
#define NUCLEUS_RULE_COMPARE_CDAT_LT				0x0013  // C< less than
#define NUCLEUS_RULE_COMPARE_CDAT_GTOE			0x0014  // C� greater than or equal to
#define NUCLEUS_RULE_COMPARE_CDAT_LTOE			0x0015	// C� less than or equal to
#define NUCLEUS_RULE_COMPARE_CDAT_NOTEQUAL	0x0016  // C! not equal to

// DATE ONLY
#define NUCLEUS_RULE_COMPARE_D_EQUALS				0x0021  // D= equals
#define NUCLEUS_RULE_COMPARE_D_GT						0x0022  // D> greater than
#define NUCLEUS_RULE_COMPARE_D_LT						0x0023  // D< less than
#define NUCLEUS_RULE_COMPARE_D_GTOE					0x0024  // D� greater than or equal to
#define NUCLEUS_RULE_COMPARE_D_LTOE					0x0025  // D� less than or equal to
#define NUCLEUS_RULE_COMPARE_D_NOTEQUAL			0x0026  // D! not equal to

// TIME OF DAY
#define NUCLEUS_RULE_COMPARE_TOD_EQUALS			0x0031  // T= equals
#define NUCLEUS_RULE_COMPARE_TOD_GT					0x0032  // T> greater than
#define NUCLEUS_RULE_COMPARE_TOD_LT					0x0033  // T< less than
#define NUCLEUS_RULE_COMPARE_TOD_GTOE				0x0034  // T� greater than or equal to
#define NUCLEUS_RULE_COMPARE_TOD_LTOE				0x0035  // T� less than or equal to
#define NUCLEUS_RULE_COMPARE_TOD_NOTEQUAL		0x0036  // T! not equal to

//DAY OF WEEK
#define NUCLEUS_RULE_COMPARE_DOW_EQUALS			0x0041  // W= equals 


/*
// CALENDAR DATE AND TIME
C= equals
C> greater than
C< less than
C� greater than or equal to
C� less than or equal to
C! not equal to

// DATE ONLY
D= equals
D> greater than
D< less than
D� greater than or equal to
D� less than or equal to
D! not equal to

// TIME OF DAY
T= equals
T> greater than
T< less than
T� greater than or equal to
T� less than or equal to
T! not equal to

//DAY OF WEEK
W= equals 

*/

/*
//destination types (decimal)
#define NUCLEUS_RULE_DESTTYPE_UNKNOWN							0000  // type unknown

#define NUCLEUS_RULE_DESTTYPE_ORAD_DVG						1000  // Orad DVG
#define NUCLEUS_RULE_DESTTYPE_ORAD_HDVG						1001  // Orad HDVG

#define NUCLEUS_RULE_DESTTYPE_MIRANDA_IS2					2001  // Imagestore 2
#define NUCLEUS_RULE_DESTTYPE_MIRANDA_INT					2002  // Intuition
#define NUCLEUS_RULE_DESTTYPE_MIRANDA_IS300				2003  // Imagestore 300
#define NUCLEUS_RULE_DESTTYPE_MIRANDA_ISHD				2004  // Imagestore HD

#define NUCLEUS_RULE_DESTTYPE_CHYRON_LEX					3000	// Duet LEX
#define NUCLEUS_RULE_DESTTYPE_CHYRON_HYPERX				3001  // Duet HyperX
#define NUCLEUS_RULE_DESTTYPE_CHYRON_MICROX				3002  // Duet MicroX
#define NUCLEUS_RULE_DESTTYPE_CHYRON_CHANNELBOX		3003  // Channel Box
#define NUCLEUS_RULE_DESTTYPE_CHYRON_CALBOX				3004  // CAL Box

#define NUCLEUS_RULE_DESTTYPE_EVERTZ_9625LG				4000 
#define NUCLEUS_RULE_DESTTYPE_EVERTZ_9625LGA			4001
#define NUCLEUS_RULE_DESTTYPE_EVERTZ_9725LGA			4002
#define NUCLEUS_RULE_DESTTYPE_EVERTZ_9725LG				4003
#define NUCLEUS_RULE_DESTTYPE_EVERTZ_HD9625LG			4004
#define NUCLEUS_RULE_DESTTYPE_EVERTZ_HD9625LGA		4005
#define NUCLEUS_RULE_DESTTYPE_EVERTZ_HD9725LG			4006
#define NUCLEUS_RULE_DESTTYPE_EVERTZ_HD9725LGA		4007

#define NUCLEUS_RULE_DESTTYPE_HARRIS_ICONII				5000


#define NUCLEUS_HARRIS_ICONII_LSFORMAT_NUM4				0 // 4 digit numerical LayoutSalvoFormat
#define NUCLEUS_HARRIS_ICONII_LSFORMAT_NUM8				1 // 8 digit numerical LayoutSalvoFormat
#define NUCLEUS_HARRIS_ICONII_LSFORMAT_ALPHA			2 // alphanumerical string LayoutSalvoFormat
*/


//search types
#define NUCLEUS_RULE_SEARCH_UNKNOWN				0xffff  // type unknown
#define NUCLEUS_RULE_SEARCH_EXPLICIT				0x0000  // search for the exact file only
#define NUCLEUS_RULE_SEARCH_AUDIO					0x0001  // search for corresponding audio file
#define NUCLEUS_RULE_SEARCH_VIDEO					0x0002  // search for corresponding video file
#define NUCLEUS_RULE_SEARCH_AV							0x0003  // search for corresponding audio and video files

//action types
#define NUCLEUS_RULE_ACTION_UNKNOWN				0xffff  // type unknown
#define NUCLEUS_RULE_ACTION_NORMAL					0x0000  // just error if nothing found...
#define NUCLEUS_RULE_ACTION_NULLOXT				0x0001  // if not video file found, create null OXT


#define NUCLEUS_EVENTSTATUS_NONE						0x00000000 //nothing found.
#define NUCLEUS_EVENTSTATUS_AUDIO					0x00001000 //audio found.
#define NUCLEUS_EVENTSTATUS_VIDEO					0x00002000 //video found.
#define NUCLEUS_EVENTSTATUS_FONT						0x00004000 //fonts found. 
#define NUCLEUS_EVENTSTATUS_TEM						0x00008000 //tem found
#define NUCLEUS_EVENTSTATUS_TEMLOC					0x00010000 //tem retrieved locally
#define NUCLEUS_EVENTSTATUS_SUBMASK				0x00000fff //mask - tem subelements enumerated.
#define NUCLEUS_EVENTSTATUS_ERRORSENT_XFER	0x00100000 //an error was sent out already indicating transfer failure
#define NUCLEUS_EVENTSTATUS_ERRORSENT_META	0x00200000 //an error was sent out already indicating no metadata
#define NUCLEUS_EVENTSTATUS_ERRORSENT_NOXT	0x00400000 //an error was sent out already indicating no null oxt file found


// status
#define NUCLEUS_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define NUCLEUS_STATUS_UNKNOWN							0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define NUCLEUS_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define NUCLEUS_STATUS_ERROR								0x00000020  // error (red icon)
#define NUCLEUS_STATUS_CONN								0x00000030  // ready (green icon)	
#define NUCLEUS_STATUS_OK									0x00000030  // ready (green icon)	
#define NUCLEUS_STATUS_RUN									0x00000040  // in progress, running, owned etc (blue icon);	
#define NUCLEUS_ICON_MASK									0x00000070  // mask	

#define NUCLEUS_STATUS_SUSPEND							0x00000080  // suspended	(yellow icon please)

#define NUCLEUS_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define NUCLEUS_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define NUCLEUS_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define NUCLEUS_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define NUCLEUS_STATUS_CMDSVR_MASK					0x00007000  // command server mask bits

#define NUCLEUS_STATUS_STATUSSVR_START			0x00010000  // starting the status server
#define NUCLEUS_STATUS_STATUSSVR_RUN				0x00020000  // status server running
#define NUCLEUS_STATUS_STATUSSVR_END				0x00030000  // status server shutting down
#define NUCLEUS_STATUS_STATUSSVR_ERROR			0x00040000  // status server error
#define NUCLEUS_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define NUCLEUS_STATUS_THREAD_START				0x00100000  // starting the main thread
#define NUCLEUS_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define NUCLEUS_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define NUCLEUS_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define NUCLEUS_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define NUCLEUS_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define NUCLEUS_STATUS_THREAD_MASK					0x00f00000  // main thread mask bits

// various failures...
#define NUCLEUS_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define NUCLEUS_STATUS_FAIL_DB							0x20000000  // could not get DB
#define NUCLEUS_STATUS_FAIL_MASK						0xf0000000  // failure code mask bits

//return values
#define NUCLEUS_SUCCESS   0
#define NUCLEUS_ERROR	   -1

// commands
#define NUCLEUS_CMD_QUEUE				 0xc0 // puts thing in queue
#define NUCLEUS_CMD_CMDQUEUE		 0xc1 // puts thing in command queue
#define NUCLEUS_CMD_DIRECT			 0xc2 // skips queues, commands box directly

#define NUCLEUS_CMD_EXSET			 0xea // increments exchange counter
#define NUCLEUS_CMD_EXGET			 0xeb // gets exchange mod value
#define NUCLEUS_CMD_MODSET			 0xec // sets exchange mod value, skips exchange table

#define NUCLEUS_CMD_GETSTATUS	 0x99 // gets status info


// default filenames
#define NUCLEUS_SETTINGS_FILE_SETTINGS	  "nucleus.csr"		// csr = cortex settings redirect
#define NUCLEUS_SETTINGS_FILE_DEFAULT	  "nucleus.csf"		// csf = cortex settings file

// debug defines
#define NUCLEUS_DEBUG_TRIGGER				0x00000001
#define NUCLEUS_DEBUG_ANALYZE				0x00000002
#define NUCLEUS_DEBUG_RULES					0x00000004
#define NUCLEUS_DEBUG_PARAMS				0x00000008
#define NUCLEUS_DEBUG_PARAMPARSE		0x00000010
#define NUCLEUS_DEBUG_TIMING				0x00000020
#define NUCLEUS_DEBUG_EVENTRULE			0x00000040
#define NUCLEUS_DEBUG_PROCESS				0x00000080
#define NUCLEUS_DEBUG_CHANNELS			0x00000100
#define NUCLEUS_DEBUG_COMM					0x00000200
#define NUCLEUS_DEBUG_LOGINLINE			0x00000400
#define NUCLEUS_DEBUG_TRIGGER_ASYNC	0x00000800
#define NUCLEUS_DEBUG_CHECKMODS   	0x00001000


#endif // !defined(NUCLEUSDEFINES_H_INCLUDED)
