; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CSocketModuleDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "SocketModule.h"
LastPage=0

ClassCount=8
Class1=CAboutDlg
Class2=CSocketModuleApp
Class3=CSocketModuleHandler
Class4=CSocketModuleDlg
Class5=CSocketModuleSettings

ResourceCount=5
Resource1=IDD_SETTINGS_DIALOG
Resource2=IDD_ABOUTBOX
Resource3=IDD_LICENSE_DIALOG
Class6=CSocketModuleSettingsDlg
Resource4=IDD_SOCKETMODULE_DIALOG
Class7=CSocketModuleLicenseDlg
Class8=CSocketModuleKeySetupDlg
Resource5=IDR_MENU1

[CLS:CAboutDlg]
Type=0
HeaderFile=socketmoduledlg.h
ImplementationFile=socketmoduledlg.cpp
BaseClass=CDialog
LastObject=CAboutDlg

[CLS:CSocketModuleApp]
Type=0
BaseClass=CWinApp
HeaderFile=SocketModule.h
ImplementationFile=SocketModule.cpp
Filter=N
VirtualFilter=AC
LastObject=CSocketModuleApp

[CLS:CSocketModuleHandler]
Type=0
BaseClass=CWnd
HeaderFile=SocketModuleHandler.h
ImplementationFile=SocketModuleHandler.cpp
Filter=W
VirtualFilter=WC
LastObject=CSocketModuleHandler

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=8
Control1=IDC_STATICTEXT_TITLE,static,1342308481
Control2=IDC_STATICTEXT_URL,static,1208090625
Control3=IDOK,button,1342373889
Control4=IDC_STATIC_LOGO,static,1342177294
Control5=IDC_URLFRAME,static,1342177298
Control6=IDC_STATIC_BUILD,static,1342308353
Control7=IDC_STATIC_URL,static,1342177294
Control8=IDC_STATICTEXT_COPYRIGHT,static,1342308353

[MNU:IDR_MENU1]
Type=1
Class=?
Command1=ID_CMD_ABOUT
Command2=ID_CMD_SHOWWND
Command3=ID_CMD_EXIT
Command4=ID_CMD_ABOUT
Command5=ID_CMD_SHOWWND
Command6=ID_CMD_EXIT
CommandCount=6

[CLS:CSocketModuleDlg]
Type=0
HeaderFile=SocketModuleDlg.h
ImplementationFile=SocketModuleDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CSocketModuleDlg

[CLS:CSocketModuleSettings]
Type=0
HeaderFile=SocketModuleSettings.h
ImplementationFile=SocketModuleSettings.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CSocketModuleSettings

[DLG:IDD_SOCKETMODULE_DIALOG]
Type=1
Class=CSocketModuleDlg
ControlCount=9
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_SETTINGS,button,1207959680
Control4=IDC_LIST1,SysListView32,1350681613
Control5=IDC_STATIC_STATUSTEXT,static,1342308352
Control6=IDC_STATIC_PROGBAR,static,1342177294
Control7=IDC_TREE1,SysTreeView32,1082130487
Control8=IDC_CHECK_PUSHPIN,button,1342247043
Control9=IDC_CHECK_KEEPFOCUS,button,1208029315

[DLG:IDD_SETTINGS_DIALOG]
Type=1
Class=CSocketModuleSettingsDlg
ControlCount=17
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308354
Control4=IDC_STATIC,static,1342308354
Control5=IDC_LIST1,SysListView32,1350631429
Control6=IDC_BUTTON_ADD,button,1342242816
Control7=IDC_BUTTON_EDIT,button,1342242816
Control8=IDC_BUTTON_DELETE,button,1342242816
Control9=IDC_EDIT_PLUGIN,edit,1350631552
Control10=IDC_EDIT_TOKEN,edit,1350631552
Control11=IDC_STATIC,static,1342308352
Control12=IDC_CHECK_SUSPEND,button,1342251011
Control13=IDC_STATIC,static,1342308354
Control14=IDC_EDIT_IP,edit,1350631552
Control15=IDC_STATIC,static,1342308354
Control16=IDC_EDIT_PORT,edit,1350631552
Control17=IDC_CHECK_ASRUN,button,1342242819

[CLS:CSocketModuleSettingsDlg]
Type=0
HeaderFile=SocketModuleSettingsDlg.h
ImplementationFile=SocketModuleSettingsDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CSocketModuleSettingsDlg

[DLG:IDD_LICENSE_DIALOG]
Type=1
Class=CSocketModuleLicenseDlg
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1073807360
Control3=IDC_EDIT1,edit,1350631552

[CLS:CSocketModuleLicenseDlg]
Type=0
HeaderFile=SocketModuleLicenseDlg.h
ImplementationFile=SocketModuleLicenseDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDOK

[CLS:CSocketModuleKeySetupDlg]
Type=0
HeaderFile=SocketModuleKeySetupDlg.h
ImplementationFile=SocketModuleKeySetupDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CSocketModuleKeySetupDlg

