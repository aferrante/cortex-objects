// SocketModuleHandler.cpp : implementation file
//

#include "stdafx.h"
#include "SocketModule.h"
#include "SocketModuleDlg.h"
#include "SocketModuleMain.h"
#include "SocketModuleHandler.h"
#include "..\Tabulator\TabulatorDefines.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ICONID 100
#define LEFTCLICK_TIMER 69
#define RIGHTCLICK_TIMER 99
#define MONITOR_TIMER_ID 27
#define KILL_TIMER_ID 42
#define MONITOR_TIMER_INT 3000 // update tray icon every x ms

#define TRAY_NOTIFYICON WM_USER+500

extern CSocketModuleApp theApp;
extern bool g_bKillThread;
extern bool g_bThreadStarted;
extern CSocketModuleMain* g_psocketmodule;


/////////////////////////////////////////////////////////////////////////////
// CSocketModuleHandler

CSocketModuleHandler::CSocketModuleHandler()
{
	m_bLeftFireDoubleClick=FALSE;
	m_bRightFireDoubleClick=FALSE;
	m_bLeft=TRUE;
	m_ulLastStatusCounter = 0;
}

CSocketModuleHandler::~CSocketModuleHandler()
{
	// removes icon from tray on destroy
	NotifyIcon(NIM_DELETE, NULL);
}


BEGIN_MESSAGE_MAP(CSocketModuleHandler, CWnd)
	//{{AFX_MSG_MAP(CSocketModuleHandler)
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONUP()
	ON_WM_RBUTTONDBLCLK()
	ON_WM_TIMER()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_COMMAND(ID_CMD_ABOUT, OnCmdAbout)
	ON_COMMAND(ID_CMD_EXIT, OnCmdExit)
	ON_COMMAND(ID_CMD_SETTINGS, OnCmdSettings)
	ON_COMMAND(ID_CMD_SHOWWND, OnCmdShowwnd)
	ON_COMMAND(ID_CMD_CLEARERROR, OnCmdClearError)
	//}}AFX_MSG_MAP
	ON_MESSAGE(TRAY_NOTIFYICON, OnTrayNotify)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CSocketModuleHandler message handlers

BOOL CSocketModuleHandler::Create()
{
	return CWnd::CreateEx(WS_EX_TOOLWINDOW, AfxRegisterWndClass(0), "SocketModuleHandler",
		WS_OVERLAPPED,0,0,0,0,NULL,NULL);
}

void CSocketModuleHandler::PostNcDestroy() 
{
	CWnd::PostNcDestroy();
	delete this;
}

void CSocketModuleHandler::OnLButtonUp(UINT nFlags, CPoint point) 
{
	OnLeftClick();
}

void CSocketModuleHandler::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	OnLeftDoubleClick();
}

void CSocketModuleHandler::OnRButtonUp(UINT nFlags, CPoint point) 
{
	OnRightClick();
}

void CSocketModuleHandler::OnRButtonDblClk(UINT nFlags, CPoint point) 
{
	OnRightDoubleClick();
}

void CSocketModuleHandler::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent == LEFTCLICK_TIMER)	
	{
		KillTimer(LEFTCLICK_TIMER);
		PostMessage(WM_LBUTTONUP);
	}
	else
	if(nIDEvent == RIGHTCLICK_TIMER)	
	{
		KillTimer(RIGHTCLICK_TIMER);
		PostMessage(WM_RBUTTONUP);
	}
	else
	if(nIDEvent == KILL_TIMER_ID)	
	{
		if(!g_bThreadStarted)
		{
			if(m_pMainDlg) ((CSocketModuleDlg*)m_pMainDlg)->OnExit();

			KillTimer(KILL_TIMER_ID);
			KillTimer(MONITOR_TIMER_ID);
			PostMessage(WM_CLOSE);
		}
	}
	else
	if(nIDEvent == MONITOR_TIMER_ID)	
	{
		if(g_psocketmodule)
		{
			if(m_ulLastStatusCounter!=g_psocketmodule->m_data.m_ulStatusCounter)
			{
				m_ulLastStatusCounter=g_psocketmodule->m_data.m_ulStatusCounter;

				// must have some kind of conditional here to set the tooltip and icon
				unsigned long ulStatus = 0;
				char* pch = g_psocketmodule->m_data.GetStatusText(&ulStatus);
				if(pch)
				{
					if(strlen(pch))
					{
						if(strlen(pch)>64) *(pch+64) = 0;  // truncate.

						m_nCurrentIcon = (int)((CX_ICON_MASK&ulStatus)>>4);
						if(m_nCurrentIcon>=MAX_ICONS) m_nCurrentIcon = ICON_CXY; // the unknown state...
						NotifyIcon(NIM_MODIFY, m_hIcon[m_nCurrentIcon], pch); // 64 char max
					}
					else
					{
						NotifyIcon(NIM_MODIFY, m_hIcon[m_nCurrentIcon], "SocketModule powered by VDS"); // 64 char max
					}
					free(pch);
				}
				else
				{
					NotifyIcon(NIM_MODIFY, m_hIcon[m_nCurrentIcon], "SocketModule powered by VDS"); // 64 char max
				}
			}
		}
	}
	else
	CWnd::OnTimer(nIDEvent);
}

LONG CSocketModuleHandler::OnTrayNotify(UINT wParam, LONG lParam)
{
	UINT uIconID = (UINT)wParam;
	UINT uMouseMsg = (UINT)lParam;

	if(uIconID != ICONID) return 0;
	switch (uMouseMsg)
	{
	case WM_LBUTTONUP:
		{
			m_bLeft=TRUE;
			if(m_bLeftFireDoubleClick) PostMessage(WM_LBUTTONDBLCLK);
		}
		break;
	case WM_RBUTTONUP:
		{
			m_bLeft=FALSE;
			if(m_bRightFireDoubleClick) PostMessage(WM_RBUTTONDBLCLK);
		}
		break;
	case WM_LBUTTONDOWN:
		{
			m_bLeft=TRUE;
			m_bLeftFireDoubleClick = FALSE;
			SetTimer(LEFTCLICK_TIMER, GetDoubleClickTime(), NULL);
		}
		break;
	case WM_RBUTTONDOWN:
		{
			m_bLeft=FALSE;
			m_bRightFireDoubleClick = FALSE;
			SetTimer(RIGHTCLICK_TIMER, GetDoubleClickTime(), NULL);
		}
		break;
	case WM_LBUTTONDBLCLK:
		{
			m_bLeftFireDoubleClick = TRUE;
			KillTimer(LEFTCLICK_TIMER);
		}
		break;
	case WM_RBUTTONDBLCLK:
		{
			m_bRightFireDoubleClick = TRUE;
			SetTimer(RIGHTCLICK_TIMER, GetDoubleClickTime(), NULL);
		}
		break;
	}
	return 0;

}

BOOL CSocketModuleHandler::NotifyIcon(DWORD dwMessage, HICON hIcon, char* pszToolTip)
{
	ASSERT(dwMessage == NIM_ADD || dwMessage == NIM_DELETE || dwMessage == NIM_MODIFY);
	static HICON hCurrentIcon = NULL;
	NOTIFYICONDATA nid;
	nid.cbSize = sizeof(NOTIFYICONDATA);
	nid.hWnd = GetSafeHwnd();
	nid.uID = ICONID;
	nid.uCallbackMessage = TRAY_NOTIFYICON;
	nid.uFlags = NIF_MESSAGE;

	if((hIcon!=NULL)&&(hIcon!=hCurrentIcon))
	{
		nid.uFlags |= NIF_ICON;
		nid.hIcon = hIcon;
		hCurrentIcon=hIcon;
	}
	if(pszToolTip!=NULL)
	{
		nid.uFlags |= NIF_TIP;
		_snprintf(nid.szTip, 63, "%s", pszToolTip);
	}

	if(dwMessage&NIM_ADD) AfxMessageBox("adding");
	return Shell_NotifyIcon(dwMessage, &nid);
}

BOOL CSocketModuleHandler::NotifyIcon(DWORD dwMessage, HICON hIcon, UINT nStringResource)
{
	CString szToolTip;
	VERIFY(szToolTip.LoadString(nStringResource));
	return NotifyIcon(dwMessage, hIcon, (char*)(LPCTSTR)szToolTip);
}

void CSocketModuleHandler::OnRightClick()
{
	CPoint pt;
	GetCursorPos(&pt);
	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU1));
	int nMenu = 0;
	CMenu* pMenu; 

// use alternate menu instead of changing text
	if((m_pMainDlg->IsWindowVisible())&&(!((CSocketModuleDlg*)(m_pMainDlg))->m_bStayOnTop))
		nMenu=1; //default
	

	pMenu = menu.GetSubMenu(nMenu);

	if((pMenu != NULL)&&(g_psocketmodule)&&((g_psocketmodule->m_data.m_ulFlags&SOCKETMODULE_ICON_MASK) == SOCKETMODULE_STATUS_ERROR))
	{
		pMenu->AppendMenu(MF_SEPARATOR);
		pMenu->AppendMenu(MF_STRING, ID_CMD_CLEARERROR, "Clear error");
	}

	SetForegroundWindow();
	pMenu->TrackPopupMenu(TPM_RIGHTBUTTON, pt.x, pt.y, this);
	PostMessage(WM_USER);

}

void CSocketModuleHandler::OnRightDoubleClick()
{
}

void CSocketModuleHandler::OnLeftClick()
{
	OnCmdShowwnd();
}

void CSocketModuleHandler::OnLeftDoubleClick()
{
	OnCmdShowwnd();
}

int CSocketModuleHandler::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	for (int i=0; i<MAX_ICONS; i++)
	{
		switch(i)
		{
//		case ICON_BARS: m_hIcon[ICON_VDS] =	AfxGetApp()->LoadIcon(IDI_ICON_COMP_BARS); break;
		case ICON_VDS:  m_hIcon[ICON_VDS] =	(HICON)(LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(IDR_MAINFRAME), IMAGE_ICON, 16, 16, LR_LOADMAP3DCOLORS )); break;
		case ICON_CXY:  m_hIcon[ICON_CXY] =	(HICON)(LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON_CXY), IMAGE_ICON, 16, 16, LR_LOADMAP3DCOLORS )); break;
		case ICON_CXR:  m_hIcon[ICON_CXR] =	(HICON)(LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON_CXR), IMAGE_ICON, 16, 16, LR_LOADMAP3DCOLORS )); break;
		case ICON_CXG:  m_hIcon[ICON_CXG] =	(HICON)(LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON_CXG), IMAGE_ICON, 16, 16, LR_LOADMAP3DCOLORS )); break;
		case ICON_CXB:  m_hIcon[ICON_CXB] =	(HICON)(LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON_CXB), IMAGE_ICON, 16, 16, LR_LOADMAP3DCOLORS )); break;
		case ICON_CLR:  m_hIcon[ICON_CLR] =	(HICON)(LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON_CLR), IMAGE_ICON, 16, 16, LR_LOADMAP3DCOLORS )); break;
		}
	}
	m_nCurrentIcon=ICON_VDS;
	NotifyIcon(NIM_ADD, m_hIcon[ICON_VDS], "SocketModule powered by VDS");

	// for updating the tray icon
	SetTimer(MONITOR_TIMER_ID, MONITOR_TIMER_INT, NULL);
	OnTimer(MONITOR_TIMER_ID); // force immediate update

//	if(m_pMainDlg) m_pMainDlg->ShowWindow(SW_HIDE);  // start invisible
	if(m_pMainDlg) m_pMainDlg->ShowWindow(SW_SHOW);  // start visible
	

	return 0;
}

void CSocketModuleHandler::OnDestroy() 
{
	CWnd::OnDestroy();

	// removes icon from tray on destroy
	NotifyIcon(NIM_DELETE, NULL);
}

void CSocketModuleHandler::OnCmdAbout() 
{
	CAboutDlg dlgAbout;
	dlgAbout.DoModal();
}

void CSocketModuleHandler::OnExternalCmdExit() 
{
	OnCmdExit();
}

void CSocketModuleHandler::OnCmdClearError()
{
	if(g_psocketmodule) g_psocketmodule->m_data.SetStatusText("Error cleared", SOCKETMODULE_STATUS_OK, true);
}


void CSocketModuleHandler::OnCmdExit() 
{
	// if you want confirmation, uncomment the AfxMessageBox
	if(!g_psocketmodule->m_data.m_bQuietKill)
	{
		if(AfxMessageBox("Are you sure you want to exit?",MB_ICONQUESTION|MB_YESNO)!=IDYES) return;
	}

//	if(m_pMainDlg) m_pMainDlg->GetDlgItem(IDC_STATIC_STATUSTEXT)->SetWindowText("shutting down...");

	g_bKillThread=true;

	SetTimer(KILL_TIMER_ID, 250, NULL); // for cleanup to occur before destruction of app.
}

void CSocketModuleHandler::OnCmdSettings() 
{
	((CSocketModuleDlg*)m_pMainDlg)->ShellExecuteSettings();

//	g_settings.DoModal();
}

void CSocketModuleHandler::OnCmdShowwnd() 
{
	if(m_pMainDlg) 
	{
		if((m_pMainDlg->IsWindowVisible())&&(!((CSocketModuleDlg*)(m_pMainDlg))->m_bStayOnTop))
			m_pMainDlg->ShowWindow(SW_HIDE);
		else
		{
			m_pMainDlg->ShowWindow(SW_SHOW);
			m_pMainDlg->SetForegroundWindow();
		}
	}
}


BOOL CSocketModuleHandler::PreTranslateMessage(MSG* pMsg) 
{
/*
	// TODO: Add your specialized code here and/or call the base class
	if (pMsg->message == WM_HOTKEY)
	{
		// a hot key was pressed.
		int i = pMsg->wParam;

		EnterCriticalSection(&g_psocketmodule->m_data.m_critAsRun);
		EnterCriticalSection(&g_psocketmodule->m_data.m_critMaps);
		if((i<g_psocketmodule->m_data.m_nNumMappingObjects)&&(g_psocketmodule->m_data.m_ppMappingObj)&&(g_psocketmodule->m_data.m_ppMappingObj[i]))
		{

			if(g_psocketmodule->m_data.m_pLastObj == NULL)// start out null; new CSocketModuleMappingObject;
			{
				g_psocketmodule->m_data.m_pLastObj = new CSocketModuleMappingObject;
			}

			if(g_psocketmodule->m_data.m_pLastObj)
			{
				g_psocketmodule->m_data.m_pLastObj->m_nKeyCode = g_psocketmodule->m_data.m_ppMappingObj[i]->m_nKeyCode;
				g_psocketmodule->m_data.m_pLastObj->m_nSysChars = g_psocketmodule->m_data.m_ppMappingObj[i]->m_nSysChars;

				_ftime(&g_psocketmodule->m_data.m_pLastObj->m_tmbTransmitTime);
				g_psocketmodule->m_data.m_pLastObj->m_bFailed = false;
				g_psocketmodule->m_data.m_pLastObj->m_bActive = true;
				g_psocketmodule->m_data.m_pLastObj->m_bRequireAppFocus = false;
			}

if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_TRIGGER) 	
{
	if((i<g_psocketmodule->m_data.m_nNumMappingObjects)&&(g_psocketmodule->m_data.m_ppMappingObj)&&(g_psocketmodule->m_data.m_ppMappingObj[i]))
	{
		g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "SocketModule received a hotkey message: key code 0x%02x, syschars 0x%02x. %sactive",
			g_psocketmodule->m_data.m_ppMappingObj[i]->m_nKeyCode, g_psocketmodule->m_data.m_ppMappingObj[i]->m_nSysChars, (g_psocketmodule->m_data.m_ppMappingObj[i]->m_bActive?"":"in")
			);  //(Dispatch message)
	}
	else
	{
		g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "SocketModule received a hotkey message: identifier %d, modifiers 0x%02x.", i, pMsg->lParam);  //(Dispatch message)
	}
}

		// transmit it to Tabulator plugin if active;


			if(
					(!g_psocketmodule->m_data.m_bProcessSuspended)		
				&&(!g_bKillThread)
				&&(g_psocketmodule->m_data.m_key.m_bValid)  // must have a valid license
				&&(
						(!g_psocketmodule->m_data.m_key.m_bExpires)
					||((g_psocketmodule->m_data.m_key.m_bExpires)&&(!g_psocketmodule->m_data.m_key.m_bExpired))
					||((g_psocketmodule->m_data.m_key.m_bExpires)&&(g_psocketmodule->m_data.m_key.m_bExpireForgiveness)&&(g_psocketmodule->m_data.m_key.m_ulExpiryDate+g_psocketmodule->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
						(!g_psocketmodule->m_data.m_key.m_bMachineSpecific)
					||((g_psocketmodule->m_data.m_key.m_bMachineSpecific)&&(g_psocketmodule->m_data.m_key.m_bValidMAC))
					)
				)
			{

				char info[8192]; strcpy(info, "");
				int nReturn = SOCKETMODULE_ERROR;

				if((g_psocketmodule->m_data.m_ppMappingObj)&&(g_psocketmodule->m_data.m_pLastObj))
				{
					int i=0;
					while(i<g_psocketmodule->m_data.m_nNumMappingObjects)
					{
						if(g_psocketmodule->m_data.m_ppMappingObj[i]->m_bActive)
						{
							if(
									(g_psocketmodule->m_data.m_ppMappingObj[i]->m_nKeyCode == g_psocketmodule->m_data.m_pLastObj->m_nKeyCode)
								&&(g_psocketmodule->m_data.m_ppMappingObj[i]->m_nSysChars == g_psocketmodule->m_data.m_pLastObj->m_nSysChars)
								)
							{
								// Send it!
								g_psocketmodule->m_data.m_netdata.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data
								g_psocketmodule->m_data.m_netdata.m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
								g_psocketmodule->m_data.m_netdata.m_ucSubCmd = 0;       // the subcommand byte

								char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
								if(pch)
								{
									sprintf(pch, "%s|KeyCode|%s|%s|%d.%d.%d.%d|%d|%d", 
											g_psocketmodule->m_settings.m_pszPluginName, 
											g_psocketmodule->m_settings.m_pszUserToken, 
											g_psocketmodule->m_data.m_pszHost,
											g_psocketmodule->m_data.m_inaddr.s_net, 
											g_psocketmodule->m_data.m_inaddr.s_host, 
											g_psocketmodule->m_data.m_inaddr.s_lh, 
											g_psocketmodule->m_data.m_inaddr.s_impno,
											g_psocketmodule->m_data.m_ppMappingObj[i]->m_nKeyCode,
											g_psocketmodule->m_data.m_ppMappingObj[i]->m_nSysChars

										);
									g_psocketmodule->m_data.m_netdata.m_pucData =  (unsigned char*) pch;
									g_psocketmodule->m_data.m_netdata.m_ulDataLen = strlen(pch);

if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 	
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "Sending 0x%02x 0x%02x %s",
		g_psocketmodule->m_data.m_netdata.m_ucCmd,g_psocketmodule->m_data.m_netdata.m_ucSubCmd,
		(char*)g_psocketmodule->m_data.m_netdata.m_pucData);  //(Dispatch message)
}
		EnterCriticalSection(&g_psocketmodule->m_data.m_critSocket);
									if(!g_bKillThread)
									{

										nReturn = g_psocketmodule->m_data.m_net.SendData(&g_psocketmodule->m_data.m_netdata, g_psocketmodule->m_data.m_socket, 
																																		5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, info);
									}
									if(nReturn>=NET_SUCCESS)
									{

if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 	
{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "Reply 0x%02x 0x%02x %s", 
		g_psocketmodule->m_data.m_netdata.m_ucCmd,g_psocketmodule->m_data.m_netdata.m_ucSubCmd,
		(char*)g_psocketmodule->m_data.m_netdata.m_pucData);  //(Dispatch message)
}


// TODO check for the NAK subcommand and parse it...



										if(!g_bKillThread) 
										{
											g_psocketmodule->m_data.m_net.SendData(NULL, g_psocketmodule->m_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
										}
									}

									if(g_psocketmodule->m_data.m_netdata.m_pucData)
									{
										try{ free(g_psocketmodule->m_data.m_netdata.m_pucData);} catch(...){}
									}
									g_psocketmodule->m_data.m_netdata.m_ulDataLen=0;
									g_psocketmodule->m_data.m_netdata.m_pucData = NULL;
									try{ free(pch);} catch(...){}

		LeaveCriticalSection(&g_psocketmodule->m_data.m_critSocket);

								}
								else
								{
									sprintf(info, "could not allocate memory buffer for transmission");
								}

								g_psocketmodule->m_data.AddAsRun(g_psocketmodule->m_data.m_pLastObj, (nReturn<NET_SUCCESS), info);
								break;
							}
						}
						i++;
					}
				}
			}
		}
		LeaveCriticalSection(&g_psocketmodule->m_data.m_critMaps);
		LeaveCriticalSection(&g_psocketmodule->m_data.m_critAsRun);
	}
*/
	return CWnd::PreTranslateMessage(pMsg);
}
