// SocketModuleMain.cpp: implementation of the CSocketModuleMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "SocketModule.h"  // just included to have access to windowing environment
#include "SocketModuleDlg.h"  // just included to have access to windowing environment
#include "SocketModuleHandler.h"  // just included to have access to windowing environment

#include "SocketModuleMain.h"
#include <process.h>
#include "../../Common/TXT/BufferUtil.h"
//#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"

//#include "..\Sentinel\SentinelDefines.h"
//#include "..\..\Common\API\Harris\ADCDefs.h"
//#include "..\..\Common\API\Miranda\IS2Comm.h"
//#include "..\..\Common\API\Miranda\IS2Core.h"
//#include "..\Radiance\RadianceDefines.h"
//#include "..\Libretto\LibrettoDefines.h"

#include <sys/timeb.h>
#include <time.h>
#include <direct.h>
#include <objsafe.h>
#include <atlbase.h>

// have to add rpcrt4.lib to the linker for UUID stuff


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;
bool g_bKillStatus = false;
CSocketModuleMain* g_psocketmodule=NULL;
//CADC g_adc;
//CIS2Comm g_is2;  // inlined code - problems with CIS2Comm::UtilParseTem memory management . never figured this one out.

extern CMessager* g_pmsgr;  // from Messager.cpp
extern CSocketModuleApp theApp;

extern void SocketModuleCommandHandlerThread(void* pvArgs);
extern void SocketModuleEmulationHandlerThread(void* pvArgs);



//void SocketModuleConnectionThread(void* pvArgs);
//void SocketModuleListThread(void* pvArgs);

//void SocketModuleGlobalAutomationThread(void* pvArgs);
//void SocketModuleGlobalAnalysisThread(void* pvArgs);
//void SocketModuleAutomationThread(void* pvArgs);
//void SocketModuleFarAnalysisThread(void* pvArgs);
//void SocketModuleTriggerThread(void* pvArgs);
//void SocketModuleTriggerDelayNotificationThread(void* pvArgs);


#define NOT_USING_NEAR_ANALYSIS

#ifndef NOT_USING_NEAR_ANALYSIS
//void SocketModuleNearAnalysisThread(void* pvArgs);
#endif //#ifndef NOT_USING_NEAR_ANALYSIS


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSocketModuleMain::CSocketModuleMain()
{
}

CSocketModuleMain::~CSocketModuleMain()
{
}

/*

char*	CSocketModuleMain::SocketModuleTranslate(CHTTPHeader* pHeader, char* pszBuffer)			// apply socketmodule scripting language
{
	return pszBuffer;
}

int		CSocketModuleMain::InterpretSocketModuleive(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the socketmoduleive, such as a char buffer.
	return SOCKETMODULE_SUCCESS;
}
*/

SOCKET*	CSocketModuleMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // socketmodule initiates a request to an object server
{
	CNetData* pdata = new CNetData;

	pdata->m_ucType = ucType;      // defined type - indicates which protocol to use, structure of data

	pdata->m_ucCmd = ucCmd;       // the command byte
	pdata->m_ucSubCmd = ucSubCmd;       // the subcommand byte

	pdata->m_pucData = pucData;
	pdata->m_ulDataLen = ulDataLen;

	SOCKET* ps = NULL; 
	m_net.SendData(pdata, pchHost, usPort, 5000, 0, NET_SND_CMDTOSVR, ps);
//	m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);

	return ps;
}

int		CSocketModuleMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// socketmodule replies to an object server after receiving data. (usually ack or nak)
{
	return SOCKETMODULE_SUCCESS;
}

int		CSocketModuleMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// socketmodule answers a request from an object client
{
	return SOCKETMODULE_SUCCESS;
}


void SocketModuleMainThread(void* pvArgs)
{
	CSocketModuleApp* pApp = (CSocketModuleApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment
	//startup.
	g_bThreadStarted = true;

	//create the main objects.

	CSocketModuleMain socketmodule;
//	CDBUtil db;

	socketmodule.m_data.m_ulFlags &= ~SOCKETMODULE_STATUS_THREAD_MASK;
	socketmodule.m_data.m_ulFlags |= SOCKETMODULE_STATUS_THREAD_START;
	socketmodule.m_data.m_ulFlags &= ~SOCKETMODULE_ICON_MASK;
	socketmodule.m_data.m_ulFlags |= SOCKETMODULE_STATUS_UNINIT;

	socketmodule.m_data.GetHost();

	g_psocketmodule = &socketmodule;


	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
//	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\SocketModule\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}



//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

//		AfxMessageBox( pApp->m_lpCmdLine );

	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// check here for Cortex IP.
			if(strlen(pch))
			{
//		AfxMessageBox( pch );
				if(socketmodule.m_data.m_bu.CountChar(pch, strlen(pch), ':')>=1)  // must be of form "hostname:cmdport" where hostname can be name or ip
				{
					socketmodule.m_data.m_pszCortexHost = (char*)malloc(strlen(pch)+1);
					if(socketmodule.m_data.m_pszCortexHost)
					{
						strcpy(socketmodule.m_data.m_pszCortexHost, pch);
//		AfxMessageBox( socketmodule.m_data.m_pszCortexHost );

						char* pchd = strchr(socketmodule.m_data.m_pszCortexHost, ':');
						if(pchd)
						{
							*pchd = 0;  // null term the host name;
							pchd++;
//		AfxMessageBox( socketmodule.m_data.m_pszCortexHost );

							char* pchd2 = strchr(pchd, ':');
							if(pchd2)
							{
								*pchd2 = 0;  // null term the cmd port;
								pchd2++;
								if(strlen(pchd2)) socketmodule.m_data.m_usCortexStatusPort = atoi(pchd2);
							}

							if(strlen(pchd)) socketmodule.m_data.m_usCortexCommandPort = atoi(pchd);
						}
					}

				}
			}
			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }



	socketmodule.m_settings.Settings(true); //read
/////////////////////////////////////////////////
// would comment this part out, but need to get dependencies here... once.
	char pszFilename[MAX_PATH];

	//	AfxMessageBox("3");

	strcpy(pszFilename, "");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, SOCKETMODULE_SETTINGS_FILE_DEFAULT);  // cortex settings file

	CFileUtil file;
	file.GetSettings(pszFilename, false); 
	char* pszParams = NULL;
// load up the values on the settings object
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(pszParams) free(pszParams); pszParams=NULL;
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
	}

	bool bUseLog = false;
	bool bUseEmail = false;
	bool bLogNetworkErrors = false;
	if(socketmodule.m_settings.m_bUseLog)
	{
		bUseLog = socketmodule.m_settings.m_bUseLog;

		// for logfiles, we need params, and they must be in this format:
		//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
//		pszParams = file.GetIniString("Messager", "LogFileIni", "SocketModule|YD||1|");
//		AfxMessageBox(pszParams);
		int nRegisterCode=0;

		nRegisterCode = socketmodule.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
			"log", 
			socketmodule.m_settings.m_pszProcessedFileSpec?socketmodule.m_settings.m_pszProcessedFileSpec:socketmodule.m_settings.m_pszFileSpec, 
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
			socketmodule.m_data.SetStatusText(errorstring, (SOCKETMODULE_STATUS_FAIL_LOG|SOCKETMODULE_STATUS_ERROR));
		}

		if(pszParams) free(pszParams); pszParams=NULL;
	}

	Sleep(50);

	if(socketmodule.m_settings.m_bUseAsRunLog)
	{
		int nRegisterCode=0;

		nRegisterCode = g_psocketmodule->m_msgr.AddDestination(MSG_DESTTYPE_LOG,//|MSG_DESTTYPE_DEFAULT, 
			"asrun", 
			g_psocketmodule->m_settings.m_pszProcessedAsRunFileSpec?g_psocketmodule->m_settings.m_pszProcessedAsRunFileSpec:g_psocketmodule->m_settings.m_pszAsRunFileSpec, 
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register as-run log file!\n code: %d", nRegisterCode); 
			g_psocketmodule->m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_LOG|CX_STATUS_ERROR));
			g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, "SocketModule:asrun_log_init", errorstring);  //(Dispatch message)
		}

	}

	Sleep(200);

	socketmodule.m_msgr.DM(MSG_ICONINFO, "asrun,log", "SocketModule", "--------------------------------------------------\n\
-------------- SocketModule %s start --------------", SOCKETMODULE_CURRENT_VERSION);  //(Dispatch message)




//	socketmodule.m_settings.ReadKeys();  // do this here so we have logging...


	if(socketmodule.m_settings.m_bUseEmail)
	{
		bUseEmail = socketmodule.m_settings.m_bUseEmail;
				// for smtp, we need params, and they must be in this format:
				// SMTP address[: override port (optional)] | distribution lists | from email | subject spec | localhostname | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
				// note on distribution lists:
				// we want to give the option to map message type and severity to different address lists.
				// the distribution lists field format is this:
				// ff:x@x.com,q@q.com;0x:B@b.com
				// the : delimits the type and severity flag specifier from the email address list for that set. 
				// the email addresses are delimited by comma
				// the sets are delimited by ;
				// the delimiters are not allowed in addresses, as described here: http://en.wikipedia.org/wiki/E-mail_address
				// on the type and severity specifiers, it is like this:
				// the subfield is a 2 character hex code, sort of.
				// severity is the first char, possible values are 0,1,2,3,4, for normal, low, medium, high, critical
				// additionally, f means all
				// type is the second char, possible values are 0-8, for the follwing:
				// #define MSG_ICONNONE								0x00000000  // default, nothing
				// #define MSG_ICONQUESTION						0x00000001  // ? icon
				// #define MSG_ICONEXCLAMATION				0x00000002  // ! icon
				// #define MSG_ICONERROR							0x00000003  // X icon
				// #define MSG_ICONSTOP								0x00000004  // stop sign icon
				// #define MSG_ICONHAND								0x00000005  // hand icon
				// #define MSG_ICONINFO								0x00000006  // (i) icon 
				// #define MSG_ICONUSER1							0x00000007  
				// #define MSG_ICONUSER2							0x00000008  
				// ff means send all messages to the address list
				// 00 means send all messages with severity normal and no "icon" to the address list
				// 03 means send all messages with severity normal and error type to the address list
				// 43 means send all messages with severity critical and error type to the address list
				// you can set up different address lists with the same addresses - one email per dispatched message will be sent in the case of overlap.

		int nRegisterCode=0;

		// NOT a default handler!
		nRegisterCode = socketmodule.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
			"email", 
			socketmodule.m_settings.m_pszProcessedMailSpec?socketmodule.m_settings.m_pszProcessedMailSpec:socketmodule.m_settings.m_pszMailSpec,
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
			socketmodule.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
			socketmodule.m_msgr.DM(MSG_ICONERROR, NULL, "SocketModule:smtp_init", errorstring);  //(Dispatch message)
		}

//		if(pszParams) free(pszParams); pszParams=NULL;
	}

//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
//	socketmodule.m_http.InitializeMessaging(&socketmodule.m_msgr);
//	socketmodule.m_net.InitializeMessaging(&socketmodule.m_msgr);
	if(socketmodule.m_settings.m_bLogNetworkErrors) 
	{
		bLogNetworkErrors = socketmodule.m_settings.m_bLogNetworkErrors;
		if(socketmodule.m_net.InitializeMessaging(&socketmodule.m_msgr)==0)
		{
			socketmodule.m_data.m_bNetworkMessagingInitialized=true;
		}
	}


//init command and status listeners.
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing command server on %d", socketmodule.m_settings.m_usCommandPort); 
	socketmodule.m_data.SetStatusText(errorstring, SOCKETMODULE_STATUS_CMDSVR_START);
	socketmodule.m_msgr.DM(MSG_ICONNONE, NULL, "SocketModule:command_server_init", errorstring);  //(Dispatch message)

	if(socketmodule.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = socketmodule.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "SocketModuleCommandServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = SocketModuleCommandHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &socketmodule;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &socketmodule.m_net;					// pointer to the object with the Message function.


		if(socketmodule.m_net.StartServer(pServer, &socketmodule.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			socketmodule.m_data.SetStatusText(errorstring, SOCKETMODULE_STATUS_CMDSVR_ERROR);
			socketmodule.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "SocketModule:command_server_init", errorstring);  //(Dispatch message)
//			socketmodule.SendMsg(CX_SENDMSG_ERROR, "SocketModule:command_server_init", errorstring);
		}
		else
		{
//AfxMessageBox("Q1");
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Command server listening on %d", socketmodule.m_settings.m_usCommandPort);
			socketmodule.m_data.SetStatusText(errorstring, SOCKETMODULE_STATUS_CMDSVR_RUN);
			socketmodule.m_msgr.DM(MSG_ICONNONE, NULL, "SocketModule:command_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

	


	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing emulator server on %d", socketmodule.m_settings.m_usStatusPort); 
	socketmodule.m_data.SetStatusText(errorstring, SOCKETMODULE_STATUS_STATUSSVR_START);
	socketmodule.m_msgr.DM(MSG_ICONNONE, NULL, "SocketModule:emu_server_init", errorstring);  //(Dispatch message)

	if(socketmodule.m_settings.m_usStatusPort>0)
	{
//AfxMessageBox("Q1");
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = socketmodule.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.



		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "SocketModuleEmulationServer");




//		pServer->m_pszStatus;				// status buffer with error messages from thread




		pServer->m_lpfnHandler = SocketModuleEmulationHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &socketmodule;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &socketmodule.m_net;					// pointer to the object with the Message function.

		if(socketmodule.m_net.StartServer(pServer, &socketmodule.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			socketmodule.m_data.SetStatusText(errorstring, SOCKETMODULE_STATUS_STATUSSVR_ERROR);
			socketmodule.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "SocketModule:emu_server_init", errorstring);  //(Dispatch message)
//			socketmodule.SendMsg(CX_SENDMSG_ERROR, "SocketModule:emu_server_init", errorstring);
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Emulator server listening on %d", socketmodule.m_settings.m_usStatusPort);
			socketmodule.m_data.SetStatusText(errorstring, SOCKETMODULE_STATUS_STATUSSVR_RUN);
			socketmodule.m_msgr.DM(MSG_ICONNONE, NULL, "SocketModule:emu_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

//AfxMessageBox("QQ");

	
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "SocketModule is initializing...");
	socketmodule.m_msgr.DM(MSG_ICONNONE, NULL, "SocketModule:init", errorstring);  //(Dispatch message)
	socketmodule.m_data.SetStatusText(errorstring, SOCKETMODULE_STATUS_THREAD_SPARK);

	// now connect all the registered servers
	// this is the list of servers in the db that get connected

/*
	if(
		  (!(socketmodule.m_settings.m_ulMainMode&SOCKETMODULE_MODE_CLONE))
		&&(socketmodule.m_data.m_key.m_bValid)  // must have a valid license
		&&(
				(!socketmodule.m_data.m_key.m_bExpires)
			||((socketmodule.m_data.m_key.m_bExpires)&&(!socketmodule.m_data.m_key.m_bExpired))
			||((socketmodule.m_data.m_key.m_bExpires)&&(socketmodule.m_data.m_key.m_bExpireForgiveness)&&(socketmodule.m_data.m_key.m_ulExpiryDate+socketmodule.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
			)
		&&(
				(!socketmodule.m_data.m_key.m_bMachineSpecific)
			||((socketmodule.m_data.m_key.m_bMachineSpecific)&&(socketmodule.m_data.m_key.m_bValidMAC))
			)
		)
	{
//	socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "X2");  Sleep(250);//(Dispatch message)
//		socketmodule.m_data.GetChannels();
//	socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "X3");  Sleep(250);//(Dispatch message)
	}
//	socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "X4");  Sleep(250);//(Dispatch message)
*/	
	if((socketmodule.m_data.m_ulFlags&SOCKETMODULE_ICON_MASK) != SOCKETMODULE_STATUS_ERROR)
	{
		socketmodule.m_data.m_ulFlags &= ~SOCKETMODULE_ICON_MASK;
		socketmodule.m_data.m_ulFlags |= SOCKETMODULE_STATUS_OK;  // green - we want run to be blue when something in progress
	}

//	socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "here2");  Sleep(250);//(Dispatch message)

// initialize socketmodule (make this not persistent for now)
	CNetData* pdata = new CNetData;
	if(pdata)
	{

		pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA;      // defined type - indicates which protocol to use, structure of data

		pdata->m_ucCmd = CX_REQ_HELLO;       // the command byte
		pdata->m_ucSubCmd = 0;       // the subcommand byte

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
		if(pch)
		{
			sprintf(pch, "%s:%d:%d:%d:%d:%s", 
				socketmodule.m_data.m_pszHost,
				socketmodule.m_settings.m_usCommandPort,
				socketmodule.m_settings.m_usStatusPort,
				CX_TYPE_PROCESS,
				theApp.m_nPID,
				socketmodule.m_settings.m_pszName?socketmodule.m_settings.m_pszName:"SocketModule"
				);

			pdata->m_pucData =  (unsigned char*) pch;
			pdata->m_ulDataLen = strlen(pch);
		}
//	socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "here3");  Sleep(250);//(Dispatch message)

		SOCKET s = NULL; 
//AfxMessageBox("sending");
//AfxMessageBox( socketmodule.m_data.m_pszCortexHost );
//AfxMessageBox( (char*)pdata->m_pucData );
		if(socketmodule.m_net.SendData(pdata, socketmodule.m_data.m_pszCortexHost, socketmodule.m_data.m_usCortexCommandPort, 5000, 0, NET_SND_CMDTOSVR, &s)>=NET_SUCCESS)
		{
			//send ack
//			AfxMessageBox("ack");
			socketmodule.m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);
		}
		else
		{
//			AfxMessageBox("could not send");
		}
		socketmodule.m_net.CloseConnection(s);

		delete pdata;

	}

//	socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "here4");  Sleep(250);//(Dispatch message)
//	_ftime(&socketmodule.m_data.m_timebAutomationTick); // the last time check inside the thread
//	_ftime(&socketmodule.m_data.m_timebNearTick); // the last time check inside the thread
//	_ftime(&socketmodule.m_data.m_timebFarTick); // the last time check inside the thread
//	_ftime(&socketmodule.m_data.m_timebTriggerTick); // the last time check inside the thread



	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "SocketModule main thread running.");  
	socketmodule.m_data.SetStatusText(errorstring, SOCKETMODULE_STATUS_THREAD_RUN);
	socketmodule.m_msgr.DM(MSG_ICONNONE, NULL, "SocketModule:init", "SocketModule is initialized, main thread is running.");  Sleep(50); //(Dispatch message)
//	socketmodule.SendMsg(CX_SENDMSG_INFO, "SocketModule:init", "SocketModule %s main thread running.", SOCKETMODULE_CURRENT_VERSION);
//	socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", errorstring);  Sleep(250);//(Dispatch message)

/*

	if(_beginthread(SocketModuleFarAnalysisThread, 0, (void*)&socketmodule)==-1)
	{
		//error.  try again once
		Sleep(1000);
		if(_beginthread(SocketModuleFarAnalysisThread, 0, (void*)&socketmodule)==-1)
		{
			//report failure
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "SocketModule analysis thread failure.");  
			socketmodule.m_data.SetStatusText(errorstring, (SOCKETMODULE_STATUS_THREAD_ERROR|SOCKETMODULE_STATUS_ERROR));
			socketmodule.m_msgr.DM(MSG_ICONERROR, NULL, "SocketModule:analysis_thread_init", errorstring);  //(Dispatch message)
			socketmodule.SendMsg(CX_SENDMSG_ERROR, "SocketModule:analysis_thread_init", errorstring);
		}
		else
		{
			socketmodule.m_msgr.DM(MSG_ICONNONE, NULL, "SocketModule:analysis_thread_init", "SocketModule analysis thread initialized.");  //(Dispatch message)
		}
	}
	else
	{
		socketmodule.m_msgr.DM(MSG_ICONNONE, NULL, "SocketModule:analysis_thread_init", "SocketModule analysis thread initialized.");  //(Dispatch message)
	}
	Sleep(250);

*/

	_timeb timebCheckMods;
	_ftime( &timebCheckMods );
///AfxMessageBox("xxxxx");

	_timeb timebCommFail;
	_ftime( &timebCommFail );

//	timebCommFail.time -= ((socketmodule.m_settings.m_nCommRetryMS/1000)+1);

	int nSocketErrorRpt = 0;
//	char szSQL[DB_SQLSTRING_MAXLEN];
	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &socketmodule.m_data.m_timebTick );


		if(
				(!socketmodule.m_data.m_bProcessSuspended)		
			&&(!g_bKillThread)
			&&(socketmodule.m_data.m_key.m_bValid)  // must have a valid license
			&&(
					(!socketmodule.m_data.m_key.m_bExpires)
				||((socketmodule.m_data.m_key.m_bExpires)&&(!socketmodule.m_data.m_key.m_bExpired))
				||((socketmodule.m_data.m_key.m_bExpires)&&(socketmodule.m_data.m_key.m_bExpireForgiveness)&&(socketmodule.m_data.m_key.m_ulExpiryDate+socketmodule.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
					(!socketmodule.m_data.m_key.m_bMachineSpecific)
				||((socketmodule.m_data.m_key.m_bMachineSpecific)&&(socketmodule.m_data.m_key.m_bValidMAC))
				)
			)
		{

/*
			EnterCriticalSection(&socketmodule.m_data.m_critSocket);

			if(socketmodule.m_data.m_socket==NULL)
			{
				if(
					  (socketmodule.m_data.m_timebTick.time>timebCommFail.time)
					||((socketmodule.m_data.m_timebTick.time == timebCommFail.time)&&(socketmodule.m_data.m_timebTick.millitm >= timebCommFail.millitm))
					)
						
				{
					char info[8192];
			//EnterCriticalSection(&g_data.m_critClientSocket);
					if(socketmodule.m_data.m_net.OpenConnection(socketmodule.m_settings.m_pszTabulatorHost, socketmodule.m_settings.m_usTabulatorCommandPort, &socketmodule.m_data.m_socket, 
						2000, 2000, info)<NET_SUCCESS)
					{
						timebCommFail.time = socketmodule.m_data.m_timebTick.time + (socketmodule.m_settings.m_nCommRetryMS/1000);
						timebCommFail.millitm = socketmodule.m_data.m_timebTick.millitm + (socketmodule.m_settings.m_nCommRetryMS%1000);

						while(timebCommFail.millitm>999)
						{
							timebCommFail.time++;
							timebCommFail.millitm-=1000;
						}

						if(nSocketErrorRpt==0)
						{
							g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, "SocketModule", "SocketModule failed to establish a connection to Tabulator %s on %s:%d",
								socketmodule.m_settings.m_pszPluginName,
								socketmodule.m_settings.m_pszTabulatorHost, socketmodule.m_settings.m_usTabulatorCommandPort
								);  //(Dispatch message)
							
						}
						nSocketErrorRpt++;
					}
					else
					{
	if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 	
	{
		if(nSocketErrorRpt>0)
		{
			g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "SocketModule established a connection to Tabulator %s on %s:%d after %d failures",
				socketmodule.m_settings.m_pszPluginName,
				socketmodule.m_settings.m_pszTabulatorHost, socketmodule.m_settings.m_usTabulatorCommandPort,
				nSocketErrorRpt
				);  //(Dispatch message)
		}
		else
		{
			g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "SocketModule established a connection to Tabulator %s on %s:%d",
				socketmodule.m_settings.m_pszPluginName,
				socketmodule.m_settings.m_pszTabulatorHost, socketmodule.m_settings.m_usTabulatorCommandPort
				);  //(Dispatch message)
		}
	}


						nSocketErrorRpt = 0;
			//LeaveCriticalSection(&g_data.m_critClientSocket);
					}
				}
			}
			else // let's select it to see if we have a closed connection
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				FD_SET(socketmodule.m_data.m_socket, &fds);
				nNumSockets = select(0, &fds, NULL, NULL, &tv);

				if ( nNumSockets == INVALID_SOCKET )
				{
					// report the error but keep going
					int nErrorCode = WSAGetLastError();
					char* pchError = socketmodule.m_data.m_net.WinsockEnglish(nErrorCode);

					socketmodule.m_data.m_net.CloseConnection(socketmodule.m_data.m_socket);
					socketmodule.m_data.m_socket = NULL;
					
					if(nSocketErrorRpt==0)
					{
						nSocketErrorRpt++;
						g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, "SocketModule", "Error on connection to Tabulator %s on %s:%d\n%s",
							socketmodule.m_settings.m_pszPluginName,
							socketmodule.m_settings.m_pszTabulatorHost, socketmodule.m_settings.m_usTabulatorCommandPort,
							pchError?pchError:"error message not available"
							);  //(Dispatch message)
					}
					if(pchError) LocalFree(pchError);

				}
				else
				if(
						(nNumSockets==0) // 0 = timed out, -1 = error
					||(!(FD_ISSET(socketmodule.m_data.m_socket, &fds)))
					) 
				{ 
					// nothing, there's just nothing there, which is good.
				} 
				else // there is recv data.
				{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
				
					// there should be nothing here, in the out of transaction space, so must be a disconnection.

					unsigned char* pucBuffer = NULL;
					unsigned long  ulBufferLen = 0;
					char info[8192];

					socketmodule.m_data.m_net.GetLine(&pucBuffer, &ulBufferLen, socketmodule.m_data.m_socket, NET_RCV_ONCE, info);

					if((pucBuffer==NULL)||(ulBufferLen))
					{
						if(nSocketErrorRpt==0)
						{
							nSocketErrorRpt++;
							g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, "SocketModule", "Disconnection from Tabulator %s on %s:%d\n%s",
								socketmodule.m_settings.m_pszPluginName,
								socketmodule.m_settings.m_pszTabulatorHost, socketmodule.m_settings.m_usTabulatorCommandPort,
								info
								);  //(Dispatch message)
						}
						socketmodule.m_data.m_net.CloseConnection(socketmodule.m_data.m_socket);
						socketmodule.m_data.m_socket = NULL;

					}
				}
			}

			LeaveCriticalSection(&socketmodule.m_data.m_critSocket);

*/
		}
		else
		{
/*
			EnterCriticalSection(&socketmodule.m_data.m_critSocket);
			if(socketmodule.m_data.m_socket)
			{
				socketmodule.m_data.m_net.CloseConnection(socketmodule.m_data.m_socket);
				socketmodule.m_data.m_socket = NULL;

		if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 	
		{
				g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "Disconnecting Tabulator %s on %s:%d, suspended or invalid",
					socketmodule.m_settings.m_pszPluginName,
					socketmodule.m_settings.m_pszTabulatorHost, socketmodule.m_settings.m_usTabulatorCommandPort
					);  //(Dispatch message)
		}

			}
			LeaveCriticalSection(&socketmodule.m_data.m_critSocket);
*/
		
		
		}











//	socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "%d.%03d", socketmodule.m_data.m_timebTick.time, socketmodule.m_data.m_timebTick.millitm);   Sleep(250);//(Dispatch message)
/*
// monitor changes in exchange db and react.
		if(
					(socketmodule.m_data.m_timebTick.time > timebCheckMods.time )
				||((socketmodule.m_data.m_timebTick.time == timebCheckMods.time)&&(socketmodule.m_data.m_timebTick.millitm >= timebCheckMods.millitm))
				&&(!g_bKillThread)
			)
		{
//	socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "time to check");  Sleep(250);//(Dispatch message)
			timebCheckMods.time = socketmodule.m_data.m_timebTick.time + socketmodule.m_settings.m_ulModsIntervalMS/1000; 
			timebCheckMods.millitm = socketmodule.m_data.m_timebTick.millitm + (unsigned short)(socketmodule.m_settings.m_ulModsIntervalMS%1000); // fractional second updates
			if(timebCheckMods.millitm>999)
			{
				timebCheckMods.time++;
				timebCheckMods.millitm%=1000;
			}
//	socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "checking db connected");   Sleep(250);//(Dispatch message)
			if(pdbConn)//&&(pdbConn->m_bConnected))
			{
//AfxMessageBox("x");
//	socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "checkmods");  //(Dispatch message)
//				socketmodule.m_data.ReleaseRecordSet();
///				socketmodule.m_data.CheckDatabaseMods();  this was there twice!  2x the db hits, oh no!

				strcpy(errorstring, "");

				if(socketmodule.m_data.CheckDatabaseMods(errorstring)==SOCKETMODULE_ERROR)
				{
					if(!socketmodule.m_data.m_bCheckModsWarningSent)
					{
						socketmodule.m_msgr.DM(MSG_ICONERROR, NULL, "CheckDatabaseMods", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
						socketmodule.m_data.m_bCheckModsWarningSent=true;
					}
				}
				else
				{
					if(socketmodule.m_data.m_bCheckModsWarningSent)
					{
						socketmodule.m_msgr.DM(MSG_ICONINFO, NULL, "CheckDatabaseMods", "Resumed checking."); // Sleep(50); //(Dispatch message)
					}
					socketmodule.m_data.m_bCheckModsWarningSent=false;
				}

				if(socketmodule.m_data.m_timebTick.time > socketmodule.m_data.m_timebAutoPurge.time + socketmodule.m_settings.m_nAutoPurgeInterval)
				{
					_ftime(&socketmodule.m_data.m_timebAutoPurge);

					if(socketmodule.m_settings.m_nAutoPurgeMessageDays>0)
					{
						if(socketmodule.m_data.CheckMessages(errorstring)==SOCKETMODULE_ERROR)
						{
							if(!socketmodule.m_data.m_bCheckMsgsWarningSent)
							{
								socketmodule.m_msgr.DM(MSG_ICONERROR, NULL, "CheckMessages", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								socketmodule.m_data.m_bCheckMsgsWarningSent=true;
							}
						}
						else
						if(socketmodule.m_data.m_bCheckMsgsWarningSent)
						{
							socketmodule.m_msgr.DM(MSG_ICONINFO, NULL, "CheckMessages", "Resumed checking messages for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						socketmodule.m_data.m_bCheckMsgsWarningSent=false;
					}

					if(socketmodule.m_settings.m_nAutoPurgeAsRunDays>0)
					{
						if(socketmodule.m_data.CheckAsRun(errorstring)==SOCKETMODULE_ERROR)
						{
							if(!socketmodule.m_data.m_bCheckAsRunWarningSent)
							{
								socketmodule.m_msgr.DM(MSG_ICONERROR, NULL, "CheckAsRun", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								socketmodule.m_data.m_bCheckAsRunWarningSent=true;
							}
						}
						else
						if(socketmodule.m_data.m_bCheckMsgsWarningSent)
						{
							socketmodule.m_msgr.DM(MSG_ICONINFO, NULL, "CheckAsRun", "Resumed checking as-run for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						socketmodule.m_data.m_bCheckAsRunWarningSent=false;
					}

				}



				if(socketmodule.m_data.m_nSettingsMod != socketmodule.m_data.m_nLastSettingsMod)
				{
					if(socketmodule.m_settings.GetFromDatabase()>=SOCKETMODULE_SUCCESS)
					{
						socketmodule.m_data.m_nLastSettingsMod = socketmodule.m_data.m_nSettingsMod;


						// check for stuff to change

						// network messaging
						if(socketmodule.m_settings.m_bLogNetworkErrors) 
						{
							if(!socketmodule.m_data.m_bNetworkMessagingInitialized)
							{
								if(socketmodule.m_net.InitializeMessaging(&socketmodule.m_msgr)==0)
								{
									socketmodule.m_data.m_bNetworkMessagingInitialized=true;
								}
							}
						}
						else
						{
							if(socketmodule.m_data.m_bNetworkMessagingInitialized)
							{
								socketmodule.m_net.UninitializeMessaging();  // void return
								socketmodule.m_data.m_bNetworkMessagingInitialized=false;
							}
						}

						// logging and email messaging:

						if(!socketmodule.m_settings.m_bUseEmail)
						{
							if(bUseEmail)
							{
								bUseEmail = false;
								// reset it
								socketmodule.m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "Shutting down email functions.");  //(Dispatch message)

		//						Sleep(250); // let the message get there.
								socketmodule.m_msgr.RemoveDestination("email");
							}
						}
						else
						{
							if(!bUseEmail)
							{
								bUseEmail = true;
								int nRegisterCode=0;

								// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
								nRegisterCode = socketmodule.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
									"email", 
									socketmodule.m_settings.m_pszProcessedMailSpec?socketmodule.m_settings.m_pszProcessedMailSpec:socketmodule.m_settings.m_pszMailSpec,
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
									socketmodule.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
									socketmodule.m_msgr.DM(MSG_ICONERROR, NULL, "SocketModule:smtp_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=socketmodule.m_msgr.GetDestIndex("email");
								if(nIndex>=0)
								{
									if((socketmodule.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(socketmodule.m_settings.m_pszProcessedMailSpec?socketmodule.m_settings.m_pszProcessedMailSpec:socketmodule.m_settings.m_pszMailSpec))
									{
										if(strcmp(socketmodule.m_msgr.m_ppDest[nIndex]->m_pszParams, (socketmodule.m_settings.m_pszProcessedMailSpec?socketmodule.m_settings.m_pszProcessedMailSpec:socketmodule.m_settings.m_pszMailSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = socketmodule.m_msgr.ModifyDestination(
												"email", 
												socketmodule.m_settings.m_pszProcessedMailSpec?socketmodule.m_settings.m_pszProcessedMailSpec:socketmodule.m_settings.m_pszMailSpec,
												MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify SMTP object!\n code: %d", nRegisterCode); 
												//socketmodule.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												socketmodule.m_msgr.DM(MSG_ICONERROR, NULL, "SocketModule:smtp_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}

						if(!socketmodule.m_settings.m_bUseLog)
						{
							if(bUseLog)
							{
								bUseLog = false;
								// reset it
								socketmodule.m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

								Sleep(250); // let the message get there.
								socketmodule.m_msgr.RemoveDestination("log");
							}
						}
						else
						{
							if(!bUseLog)
							{
								bUseLog = true;
								int nRegisterCode=0;

								nRegisterCode = socketmodule.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
									"log", 
									socketmodule.m_settings.m_pszProcessedFileSpec?socketmodule.m_settings.m_pszProcessedFileSpec:socketmodule.m_settings.m_pszFileSpec, 
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
									socketmodule.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_LOG|CX_STATUS_ERROR));
									socketmodule.m_msgr.DM(MSG_ICONERROR, NULL, "SocketModule:log_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=socketmodule.m_msgr.GetDestIndex("log");
								if(nIndex>=0)
								{
									if((socketmodule.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(socketmodule.m_settings.m_pszProcessedFileSpec?socketmodule.m_settings.m_pszProcessedFileSpec:socketmodule.m_settings.m_pszFileSpec))
									{
										if(strcmp(socketmodule.m_msgr.m_ppDest[nIndex]->m_pszParams, (socketmodule.m_settings.m_pszProcessedFileSpec?socketmodule.m_settings.m_pszProcessedFileSpec:socketmodule.m_settings.m_pszFileSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = socketmodule.m_msgr.ModifyDestination(
												"log", 
												socketmodule.m_settings.m_pszProcessedFileSpec?socketmodule.m_settings.m_pszProcessedFileSpec:socketmodule.m_settings.m_pszFileSpec,
												MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify log object!\n code: %d", nRegisterCode); 
												//socketmodule.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												socketmodule.m_msgr.DM(MSG_ICONERROR, NULL, "SocketModule:log_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}








					}
				}
				if(
					  (!socketmodule.m_data.m_bProcessSuspended)

					&&(socketmodule.m_data.m_key.m_bValid)  // must have a valid license
					&&(
							(!socketmodule.m_data.m_key.m_bExpires)
						||((socketmodule.m_data.m_key.m_bExpires)&&(!socketmodule.m_data.m_key.m_bExpired))
						||((socketmodule.m_data.m_key.m_bExpires)&&(socketmodule.m_data.m_key.m_bExpireForgiveness)&&(socketmodule.m_data.m_key.m_ulExpiryDate+socketmodule.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!socketmodule.m_data.m_key.m_bMachineSpecific)
						||((socketmodule.m_data.m_key.m_bMachineSpecific)&&(socketmodule.m_data.m_key.m_bValidMAC))
						)
					)
				{
					bool bAnalysisChanges = false;
					if(socketmodule.m_data.m_nMappingMod != socketmodule.m_data.m_nLastMappingMod)
					{
						if(socketmodule.m_data.GetMappings()>=SOCKETMODULE_SUCCESS)
						{
							socketmodule.m_data.m_nLastMappingMod = socketmodule.m_data.m_nMappingMod;
						}
					}
					if(socketmodule.m_data.m_nEventRulesMod != socketmodule.m_data.m_nLastEventRulesMod)
					{
						// was this:
//						if(socketmodule.m_data.GetEventRules()>=SOCKETMODULE_SUCCESS)
						// now this, and below timing cols removed
						if((socketmodule.m_data.GetEventRules()>=SOCKETMODULE_SUCCESS)&&(socketmodule.m_data.GetTimingColumns()>=SOCKETMODULE_SUCCESS))
						{
							// this was originally not forced reanalyzed... but dont know why?  did I omit accidentally?
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_PROCESS) 	
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "Analysis change from EventRulesMod %d", socketmodule.m_data.m_nEventRulesMod );  // Sleep(50);//(Dispatch message)
							bAnalysisChanges = true;
							socketmodule.m_data.m_nLastEventRulesMod = socketmodule.m_data.m_nEventRulesMod;
						}
					}
					if(socketmodule.m_data.m_nEventsMod != socketmodule.m_data.m_nLastEventsMod)
					{
//g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "getting Timing Column names"); // Sleep(100); //(Dispatch message)

/*
						if(socketmodule.m_data.GetTimingColumns()>=SOCKETMODULE_SUCCESS)
						{
							bAnalysisChanges = true;
* / // just reset it, we do timing cols above now.
							socketmodule.m_data.m_nLastEventsMod = socketmodule.m_data.m_nEventsMod;
/*
						}
* /
					}
					if(socketmodule.m_data.m_nParameterRulesMod != socketmodule.m_data.m_nLastParameterRulesMod)
					{
						if(socketmodule.m_data.GetParameterRules()>=SOCKETMODULE_SUCCESS)
						{
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_PROCESS) 	
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "Analysis change from ParameterRulesMod %d", socketmodule.m_data.m_nParameterRulesMod );  // Sleep(50);//(Dispatch message)
							bAnalysisChanges = true;
							socketmodule.m_data.m_nLastParameterRulesMod = socketmodule.m_data.m_nParameterRulesMod;
						}
					}
					if(socketmodule.m_data.m_nConnectionsMod != socketmodule.m_data.m_nLastConnectionsMod)
					{

//			socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "Channel info view has changed, calling GetConnections");   Sleep(10);//(Dispatch message)
						if(socketmodule.m_data.GetConnections()>=SOCKETMODULE_SUCCESS)
						{
			socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "GetConnections returned successfully"); //  Sleep(10);//(Dispatch message)
							socketmodule.m_data.m_nLastConnectionsMod = socketmodule.m_data.m_nConnectionsMod;
						}
						else
						{
			socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "GetConnections returned an error"); //  Sleep(10);//(Dispatch message)

						}
					}

					if(bAnalysisChanges)
					{
						// go thru and indicate changes to each channel
						socketmodule.m_data.m_bForceAnalysis = true;


/*
// moved to a delay thread.
						if(
							  (socketmodule.m_settings.m_ppEndpointObject)
							&&(socketmodule.m_data.m_nIndexAutomationEndpoint>=0)
							&&(socketmodule.m_data.m_nIndexAutomationEndpoint<socketmodule.m_settings.m_nNumEndpointsInstalled)
							&&(socketmodule.m_settings.m_ppEndpointObject[socketmodule.m_data.m_nIndexAutomationEndpoint])
							)
						{
							CSocketModuleEndpointObject* pAutoObj = socketmodule.m_settings.m_ppEndpointObject[socketmodule.m_data.m_nIndexAutomationEndpoint];

							if(pAutoObj) pAutoObj->m_nLastModLiveEvents = -1;  // triggers global analysis rules.
// above line now taken care of above by socketmodule.m_data.m_bForceAnalysis = true;

							if((pAutoObj->m_ppChannelObj)&&(pAutoObj->m_nNumChannelObjects))
							{
								int cho=0;
								while(cho<pAutoObj->m_nNumChannelObjects)
								{
									if(pAutoObj->m_ppChannelObj[cho])
									{
										pAutoObj->m_ppChannelObj[cho]->m_bTriggerEventsChanged = true;
//										pAutoObj->m_ppChannelObj[cho]->m_bNearEventsChanged = true;

									}
									cho++;
								}
							}
						}
* /

						if((g_psocketmodule->m_settings.m_bEnableEventTriggerNotification)&&(!g_psocketmodule->m_data.m_bDelayingTriggerNotification))
						{
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_PROCESS) 	
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "  >>* Spawning SocketModuleTriggerDelayNotificationThread from main thread");  // Sleep(50);//(Dispatch message)

							if(_beginthread(SocketModuleTriggerDelayNotificationThread, 0, (void*)NULL)==-1)
							{
								//error.
				socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "Error starting trigger delay notification thread");  // Sleep(10);//(Dispatch message)

								//**MSG
							}
						}

					}

				}
/*
				if(socketmodule.m_data.m_nConnectionsMod != socketmodule.m_data.m_nLastConnectionsMod)
				{
//			socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "getting connections again");   Sleep(250);//(Dispatch message)
					if(socketmodule.m_data.GetConnections()>=SOCKETMODULE_SUCCESS)
					{
						socketmodule.m_data.m_nLastConnectionsMod = socketmodule.m_data.m_nConnectionsMod;
					}
				}
				if(socketmodule.m_data.m_nChannelsMod != socketmodule.m_data.m_nLastChannelsMod)
				{
//			socketmodule.m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "getting channels again");   Sleep(250);//(Dispatch message)
					if(socketmodule.m_data.GetChannels()>=SOCKETMODULE_SUCCESS)
					{
						socketmodule.m_data.m_nLastChannelsMod = socketmodule.m_data.m_nChannelsMod;
					}
				}
* /
			}
		}  // check mod interval
*/
		MSG msg;

//		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
//			AfxGetApp()->PumpMessage();


//AfxMessageBox("zoinks");
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();

		Sleep(1); 
//		Sleep(1000);  // stalls window procedure
	}

	socketmodule.m_data.m_ulFlags &= ~SOCKETMODULE_STATUS_THREAD_MASK;
	socketmodule.m_data.m_ulFlags |= SOCKETMODULE_STATUS_THREAD_END;

	socketmodule.m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule:uninit", "SocketModule is shutting down.");  //(Dispatch message)
//	socketmodule.SendMsg(CX_SENDMSG_INFO, "SocketModule:uninit", "SocketModule %s is shutting down.", SOCKETMODULE_CURRENT_VERSION);

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "SocketModule is shutting down.");  
	socketmodule.m_data.m_ulFlags &= ~CX_ICON_MASK;
	socketmodule.m_data.m_ulFlags |= CX_STATUS_UNKNOWN;
	socketmodule.m_data.SetStatusText(errorstring, socketmodule.m_data.m_ulFlags);
/*
	EnterCriticalSection(&socketmodule.m_data.m_critSocket);

	if(socketmodule.m_data.m_socket)
	{
		socketmodule.m_data.m_net.CloseConnection(socketmodule.m_data.m_socket);
		socketmodule.m_data.m_socket = NULL;

if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 	
{
		g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "Disconnecting Tabulator %s on %s:%d",
			socketmodule.m_settings.m_pszPluginName,
			socketmodule.m_settings.m_pszTabulatorHost, socketmodule.m_settings.m_usTabulatorCommandPort
			);  //(Dispatch message)
}

	}

	LeaveCriticalSection(&socketmodule.m_data.m_critSocket);
*/

/*
	int kt=0;
	BOOL bAllDone = FALSE;
	while ((kt<100)&&(!bAllDone))
/*
					(socketmodule.m_data.m_bAutomationThreadStarted)
				||(socketmodule.m_data.m_bNearAnalysisThreadStarted)
				||(socketmodule.m_data.m_bFarAnalysisThreadStarted)
				||(socketmodule.m_data.m_bTriggerThreadStarted)
				))
* /

	{

		/*

g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, "SocketModule:debug", "ending %d %d %d %d @ %d", 
											socketmodule.m_data.m_bAutomationThreadStarted,
											socketmodule.m_data.m_bNearAnalysisThreadStarted,
											socketmodule.m_data.m_bFarAnalysisThreadStarted,
											socketmodule.m_data.m_bTriggerThreadStarted, clock()
											); 
* /
		
		// have to go thru all endpoints until the threads are down.
		
		
		Sleep(10);
		_ftime( &socketmodule.m_data.m_timebTick );  // we're still alive.
		kt++;
	}


*/
// shut down all the running objects;
/*
	if(socketmodule.m_data.m_ppConnObj)
	{
		int i=0;
		while(i<socketmodule.m_data.m_nNumConnectionObjects)
		{
			if(socketmodule.m_data.m_ppConnObj[i])
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", socketmodule.m_data.m_ppConnObj[i]->m_pszDesc?socketmodule.m_data.m_ppConnObj[i]->m_pszDesc:socketmodule.m_data.m_ppConnObj[i]->m_pszServerName);  

// **** disconnect servers
				socketmodule.m_data.SetStatusText(errorstring, socketmodule.m_data.m_ulFlags);
				socketmodule.m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule:connection_uninit", errorstring);    //(Dispatch message)

				delete socketmodule.m_data.m_ppConnObj[i];
				socketmodule.m_data.m_ppConnObj[i] = NULL;
			}
			i++;
		}
		delete [] socketmodule.m_data.m_ppConnObj;
	}
	if(socketmodule.m_data.m_ppChannelObj)
	{
		int i=0;
		while(i<socketmodule.m_data.m_nNumChannelObjects)
		{
			if(socketmodule.m_data.m_ppChannelObj[i])
			{
				delete socketmodule.m_data.m_ppChannelObj[i];
				socketmodule.m_data.m_ppChannelObj[i] = NULL;
			}
			i++;
		}
		delete [] socketmodule.m_data.m_ppChannelObj;
	}
*/

/*
	// here's a hard coded one for now
	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no need to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = socketmodule.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		socketmodule.m_net.OpenConnection(socketmodule.m_http.m_pszHost, 10888, &s); // branding hardcode
		socketmodule.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		socketmodule.m_net.CloseConnection(s); // branding hardcode
	}
*/

//	m_pDlg->SetProgress(SOCKETMODULEDLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
//	socketmodule.m_data.m_ulFlags &= ~SOCKETMODULE_STATUS_FILESVR_MASK;
//	socketmodule.m_data.m_ulFlags |= SOCKETMODULE_STATUS_FILESVR_END;
//	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down file server....");  
//	socketmodule.m_data.SetStatusText(errorstring, socketmodule.m_data.m_ulFlags);
//	_ftime( &socketmodule.m_data.m_timebTick );
//	socketmodule.m_http.EndServer();
	_ftime( &socketmodule.m_data.m_timebTick );
//	AfxMessageBox("shutting down command server.");
/*
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down command server....");  
	socketmodule.m_msgr.DM(MSG_ICONNONE, NULL, "SocketModule:command_server_uninit", errorstring);  //(Dispatch message)
	socketmodule.m_data.SetStatusText(errorstring, SOCKETMODULE_STATUS_CMDSVR_END);
	_ftime( &socketmodule.m_data.m_timebTick );
	socketmodule.m_net.StopServer(socketmodule.m_settings.m_usCommandPort, 5000, errorstring);
	_ftime( &socketmodule.m_data.m_timebTick );
//	AfxMessageBox("shutting down status server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down XML server....");  
	socketmodule.m_msgr.DM(MSG_ICONNONE, NULL, "SocketModule:xml_server_uninit", errorstring);  //(Dispatch message)
	socketmodule.m_data.SetStatusText(errorstring, SOCKETMODULE_STATUS_STATUSSVR_END);
	_ftime( &socketmodule.m_data.m_timebTick );
	socketmodule.m_net.StopServer(socketmodule.m_settings.m_usStatusPort, 5000, errorstring);
	_ftime( &socketmodule.m_data.m_timebTick );
*/
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "SocketModule is exiting.");  
	socketmodule.m_msgr.DM(MSG_ICONNONE, NULL, "SocketModule:uninit", errorstring);  //(Dispatch message)
	socketmodule.m_data.SetStatusText(errorstring, socketmodule.m_data.m_ulFlags);



	// save settings.  // dont save them here.  save them on any changes in the main command loop.
	socketmodule.m_settings.Settings(false); //write
	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "SocketModule is exiting");
	socketmodule.m_data.m_ulFlags &= ~CX_ICON_MASK;
	socketmodule.m_data.m_ulFlags |= CX_STATUS_UNINIT;
	socketmodule.m_data.SetStatusText(errorstring, socketmodule.m_data.m_ulFlags);

	//exiting
	socketmodule.m_msgr.DM(MSG_ICONINFO, "asrun,log", "SocketModule", "-------------- SocketModule %s exit ---------------\n\
--------------------------------------------------\n", SOCKETMODULE_CURRENT_VERSION);  //(Dispatch message)
///	m_pDlg->SetProgress(SOCKETMODULEDLG_CLEAR); // no point

	_ftime( &socketmodule.m_data.m_timebTick );
	socketmodule.m_data.m_ulFlags &= ~SOCKETMODULE_STATUS_THREAD_MASK;
	socketmodule.m_data.m_ulFlags |= SOCKETMODULE_STATUS_THREAD_ENDED;

	g_bKillStatus = true;

	Sleep(50); // let the message get there.
	socketmodule.m_msgr.RemoveDestination("log");
	socketmodule.m_msgr.RemoveDestination("email");

	g_psocketmodule = NULL;
//	db.RemoveConnection(pdbConn);

	
	int nClock = clock() + 50; // small delay at end
	while(clock()<nClock)	{_ftime( &socketmodule.m_data.m_timebTick );}
	g_bThreadStarted = false;
	g_psocketmodule = NULL;
	nClock = clock() + 50;//socketmodule.m_settings.m_nThreadDwellMS; // another small delay at end
	while(clock()<nClock)	{_ftime( &socketmodule.m_data.m_timebTick );}
	Sleep(100); //one more small delay at end
	_endthread();
}


#define USE_LISTENERS
#ifdef USE_LISTENERS

void SocketModuleEmulationHandlerThread(void* pvArgs)
{

	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;
	
	char szSocketModuleSource[MAX_PATH]; 
	strcpy(szSocketModuleSource, "SocketModuleEmulationHandler");

	CBufferUtil bu;

	HRESULT hRes;
	try
	{  

#if _WIN32_WINNT >= 0x0400
		hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		hRes = CoInitialize(NULL);
#endif
	}
	catch(...)
	{
		if(g_psocketmodule) g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, "Exception in CoInitialize");  //(Dispatch message)
	}


	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

//		char dberrorstring[DB_ERRORSTRING_LEN];
//		char xmlerrorstring[MAX_MESSAGE_LENGTH];
		char errorstring[MAX_MESSAGE_LENGTH];
//		char szSQL[DB_SQLSTRING_MAXLEN];
//		strcpy(dberrorstring, "");
		strcpy(errorstring, "");

//		char element[MAX_MESSAGE_LENGTH];

//		AfxMessageBox(szSocketModuleSource);

		CNetUtil net(false); // local object for utility functions.
//		CCortexUtil util;  // cortex utility functions.

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		bool bCloseCommand = false;  // make these not persistent unless a persist command comes in.
		bool bPersist = false;  // make these not persistent unless a persist command comes in.
		int nPeriodic = -1;  
		int nTimed = -1;  

		unsigned char uchMessageNum = 1;

		char* pchBuffer = NULL;
		unsigned long ulBufferLen = 0;
		char* pch = NULL;
		char* pchXML = NULL;
		char* pchXMLStream = NULL;
		unsigned long ulAccumulatedBufferLen = 0;
		unsigned long ulRetry = 0;

		unsigned long ulConnLastMessage = 0;
		unsigned long ulConnTimeout = 0;
		
//		char filename[MAX_PATH];
//		char lastrxfilename[MAX_PATH];
//		char lasttxfilename[MAX_PATH];
		int nRxDupes=0;
		int nTxDupes=0;

		FILE* fp = NULL;

		_timeb timestamp;
		_timeb timeactive;
		_timeb timeperiodic;

		_ftime(&timeactive);
		_ftime(&timeperiodic);


	// 	(*(pClient->m_pulConnections))++;//already exists
		// initialize random seed
		srand ( time(NULL) );



		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);
		if(g_psocketmodule)
		{
			g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, errorstring);  //(Dispatch message)
//			g_psocketmodule->SendMsg(CX_SENDMSG_INFO, szSocketModuleSource, errorstring);
		}

		while ( 
									(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
//								&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
								&&(pClient->m_socket!=NULL)
								&&(!bCloseCommand)
					)
		{


			pchBuffer = NULL;
			ulBufferLen = 0;

			
//			AfxMessageBox("getting");
			int nReturnCode = net.GetLine((unsigned char**)&pchBuffer, &ulBufferLen, pClient->m_socket, NET_RCV_ONCE, errorstring);
			if(nReturnCode == NET_SUCCESS)
			{

if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "received %d", ulBufferLen);  //(Dispatch message)

				ulRetry = 0;
				//process any received bytes.
				// have to keep accumulating until we have enough bytes 
				if((pchBuffer)&&(ulBufferLen)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
				{
					int nLen = 0;
					if(pchXMLStream) nLen = ulAccumulatedBufferLen;
					pch = (char*) malloc(nLen+ulBufferLen+1);  //term 0
					if(pch)
					{
						char* pchEnd = NULL;
						char* pchNext = NULL;
						if(pchXMLStream)  // we have an old buffer.
						{
							memcpy(pch, pchXMLStream, ulAccumulatedBufferLen);
							memcpy(pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen);
							free(pchXMLStream); 
							pchXMLStream = pch;  // reassign!
							ulAccumulatedBufferLen += ulBufferLen;
							*(pch+ulAccumulatedBufferLen) = 0;  // null term

//AfxMessageBox("old");
						}
						else
						{
//AfxMessageBox("new");
							// this is new.
//							just add it to the buffer
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "new buffer %d", ulBufferLen);  //(Dispatch message)

							pchEnd = pchBuffer;
//							while((*pchEnd!='<')&&(pchEnd<pchBuffer+ulBufferLen)) pchEnd++;

							if(pchEnd<pchBuffer+ulBufferLen)
							{
								memcpy(pch, pchEnd, ulBufferLen);
								ulAccumulatedBufferLen += (ulBufferLen-(pchEnd-pchBuffer));
								pchXMLStream = pch;  // reassign!
								*(pch+ulAccumulatedBufferLen) = 0;  // null term
							}
							else  // not found!
							{
								free(pch);
								pchXMLStream = NULL;
							}
						}

						if(pchBuffer) free(pchBuffer);
						pchBuffer = NULL;
						if(pchXMLStream)
						{
							// get length.
//				CString foo; foo.Format("Yo %d",ulAccumulatedBufferLen); AfxMessageBox(foo);
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "acc buflen %d", ulAccumulatedBufferLen);  //(Dispatch message)


							bool bHasMessage = false;
							bool bMulti = false;
							unsigned long ulMessageLen = 0;

							pchEnd = NULL;

							if(ulAccumulatedBufferLen>=4)
							{
								ulMessageLen = ((((unsigned char)(*(pchXMLStream+2)))<<8) | ((unsigned char)(*(pchXMLStream+3))));

if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "message len %d (%d %d)", ulMessageLen, ((unsigned char)(*(pchXMLStream+2))), ((unsigned char)(*(pchXMLStream+3))) );  //(Dispatch message)

//				CString foo; foo.Format("Yo %d =? %d (%d %d)",ulAccumulatedBufferLen,ulMessageLen,*(pchXMLStream+2),*(pchXMLStream+3)); AfxMessageBox(foo);
								if(ulMessageLen <= ulAccumulatedBufferLen)
								{
									bHasMessage = true;
									pchEnd = pchXMLStream+ulMessageLen;

if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "check multi %d %d", ((unsigned char)(*pchXMLStream)), ((unsigned char)(*(pchXMLStream+1))) );  //(Dispatch message)

									if((((unsigned char)(*pchXMLStream))==0xff)&&(((unsigned char)(*(pchXMLStream+1)))==0xff)) bMulti = true;



								}
							}
							
							int nTempLen = 0;
								//strstr(pchXMLStream, "</cortex>");
							while((pchEnd)&&(bHasMessage)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
							{
							// found a token.
					//			pchEnd+=strlen("</cortex>");

								pchNext = pchEnd;
//								while((*pchNext!='<')&&(pchNext<pchEnd+strlen(pchEnd))) pchNext++;

								if(pchNext<pchXMLStream+ulAccumulatedBufferLen)
								{
									// we found a remainder.
									nLen = ulAccumulatedBufferLen - (pchNext-pchXMLStream);
									nTempLen = nLen;
									pch = (char*) malloc(nLen+1);  //term 0
									if(pch)
									{
										memcpy(pch, pchNext, nLen);
										*(pch+nLen) = 0;
									}
								}
								else pch = NULL;

								pchXML = pchXMLStream; // just use it.
								*pchEnd = 0; //null terminate it

								pchXMLStream = pch;  // take the rest of the stream.
								if(pchXMLStream)
								{
									ulAccumulatedBufferLen -= nTempLen; //strlen(pchXMLStream);
								}
								else ulAccumulatedBufferLen=0;

								// now have to deal with XML found in pchXML.
								//debug file write
/*
								if((g_psocketmodule)&&(g_psocketmodule->m_settings.m_bUseXMLClientLog))
								{
//				AfxMessageBox("logging");
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "begin log receive");  //(Dispatch message)

									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_psocketmodule->m_settings.m_pszName?g_psocketmodule->m_settings.m_pszName:"SocketModule"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lastrxfilename, filename)==0)
									{
										nRxDupes++;
									}
									else
									{
										nRxDupes=0;
										strcpy(lastrxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_RX.xml",nRxDupes);

//	AfxMessageBox(filename);

									fp = fopen(filename, "wb");
									if(fp)
									{
//											fwrite(filename, 1, strlen(filename),fp);
										fwrite(pchXML, 1, strlen(pchXML),fp);
										fflush(fp);
										fclose(fp);
										fp = NULL;
									}
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "end log receive");  //(Dispatch message)

								}
*/



								// do stuff here.

								_ftime(&timeactive);  // reset the inactivity timer.

								unsigned long ulMessageID = 0;
								if(!bMulti)
								{
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "not multi, message id %d",ulMessageID);  //(Dispatch message)
									ulMessageID = (((*(pchXML))<<8) | (*(pchXML+1)));
								}
//			CString foo; foo.Format("multi %d; id %d (%d %d)",bMulti,ulMessageID,*(pchXML),*(pchXML+1)); AfxMessageBox(foo);


								_ftime(&timeactive);  // reset the inactivity timer.

								//and then respond.




if(ulMessageID != 3)
{



						g_psocketmodule->m_data.m_szDisplay.Format("%d bytes, %d %d", ulMessageLen, ((unsigned char)(*pchXML)), ((unsigned char)(*(pchXML+1))));
					// here we have a binary buffer.

						if((*pchXML == (char)0xff)&&(*(pchXML+1) == (char)0xff))
						{
							// multi message
							g_psocketmodule->m_data.m_szDisplay.Format("SCTE104 multi message, %d bytes\r\n\r\n", ulMessageLen);

//							AfxMessageBox(szHexBuffer);
							// lets deal:

/*
multiple_operation_message() 
{
	Reserved 2 uimsbf
	messageSize 2 uimsbf
	protocol_version 1 uimsbf
	AS_index 1 uimsbf
	message_number 1 uimsbf
	DPI_PID_index 2 uimsbf
	SCTE35_protocol_version 1 uimsbf
	timestamp() * Varies
	num_ops 1 uimsbf
	for (i=0; i < num_ops; i++ ) 
	{
		opID 2
		data_length 2
		data() * Varies
	}
}
*/


							CString szPart;

							if(ulMessageLen>=10)
							{
								szPart.Format("Reserved = 0x%02x%02x\r\n", (unsigned char)*pchXML,  (unsigned char)*(pchXML+1));
								g_psocketmodule->m_data.m_szDisplay += szPart;

								int nSize = ( ((unsigned char) (*(pchXML+2))) <<8) + ((unsigned char)(*(pchXML+3)));

								szPart.Format("messageSize = 0x%02x%02x (%d bytes)\r\n", (unsigned char)*(pchXML+2),  (unsigned char)*(pchXML+3), nSize);
								g_psocketmodule->m_data.m_szDisplay += szPart;
								szPart.Format("protocol_version = 0x%02x\r\n", (unsigned char)*(pchXML+4));
								g_psocketmodule->m_data.m_szDisplay += szPart;
								szPart.Format("AS_index = 0x%02x\r\n", (unsigned char)*(pchXML+5));
								g_psocketmodule->m_data.m_szDisplay += szPart;
//								szPart.Format("message_number = 0x%02x (%d decimal)\r\n", (unsigned char)*(pchXML+10), *(pchXML+10));
								szPart.Format("message_number = 0x%02x\r\n", (unsigned char)*(pchXML+6));
								g_psocketmodule->m_data.m_szDisplay += szPart;
								szPart.Format("DPI_PID_index = 0x%02x%02x\r\n", (unsigned char)*(pchXML+7),  (unsigned char)*(pchXML+8));
								g_psocketmodule->m_data.m_szDisplay += szPart;

								szPart.Format("SCTE35_protocol_version = 0x%02x\r\n", (unsigned char)*(pchXML+9));
								g_psocketmodule->m_data.m_szDisplay += szPart;

							}
							bool bErr = false;
							int nOffset = 10;
							if(ulMessageLen>10)
							{
								switch(*(pchXML+10))
								{
								case 0:  // time type is not required.
									{
										szPart.Format("time_type = 0x%02x (no time required)\r\n", (unsigned char)*(pchXML+nOffset));
										g_psocketmodule->m_data.m_szDisplay += szPart;
									} break;
								case 1:  // time type is UTC time
									{
										szPart.Format("time_type = 0x%02x (UTC time)\r\n", (unsigned char)*(pchXML+nOffset));
										g_psocketmodule->m_data.m_szDisplay += szPart;

										int nTime = 
											((*(pchXML+nOffset+1))<<24) + 
											((*(pchXML+nOffset+2))<<16) +
											((*(pchXML+nOffset+3))<<8) +
											((*(pchXML+nOffset+4)));

										szPart.Format("UTC_seconds = %d\r\n", nTime);
										g_psocketmodule->m_data.m_szDisplay += szPart;

										nTime = 
											((*(pchXML+nOffset+5))<<8) +
											((*(pchXML+nOffset+6)));

										szPart.Format("UTC_microseconds = %d\r\n", nTime);
										g_psocketmodule->m_data.m_szDisplay += szPart;

										nOffset += 7;

									} break;
								case 2:  // hmsf
									{
										szPart.Format("time_type = 0x%02x (HMSF)\r\n", (unsigned char)*(pchXML+nOffset));
										g_psocketmodule->m_data.m_szDisplay += szPart;
										szPart.Format("HH:MM:SS;FF = %02d:%02d:%02d;%02d\r\n", (unsigned char)*(pchXML+nOffset+1), (unsigned char)*(pchXML+nOffset+2), (unsigned char)*(pchXML+nOffset+3), (unsigned char)*(pchXML+nOffset+4));
										g_psocketmodule->m_data.m_szDisplay += szPart;
										nOffset += 5;
									} break;
								case 3:  //gpi
									{
										szPart.Format("time_type = 0x%02x (GPI)\r\n", (unsigned char)*(pchXML+nOffset));
										g_psocketmodule->m_data.m_szDisplay += szPart;
										szPart.Format("GPI_number = %02d\r\nGPI_edge = %02d\r\n", (unsigned char)*(pchXML+nOffset+1), (unsigned char)*(pchXML+nOffset+2));
										g_psocketmodule->m_data.m_szDisplay += szPart;
										nOffset += 3;
									} break;
								default:
									{
										szPart.Format("time_type = 0x%02x  - Parse Error on time type\r\n", (unsigned char)*(pchXML+nOffset));
										bErr = true;
										g_psocketmodule->m_data.m_szDisplay += szPart;
									} break;
								}
							}

							if((!bErr) && (ulMessageLen > (unsigned long)nOffset))
							{

/*
	num_ops 1 uimsbf
	for (i=0; i < num_ops; i++ ) 
	{
		opID 2
		data_length 2
		data() * Varies
	}
*/
								int nNumOps = *(pchXML+nOffset);
								szPart.Format("num_ops = %02d\r\n", nNumOps);
								g_psocketmodule->m_data.m_szDisplay += szPart;

								CString szTitle;
								szTitle = "*** UNKNOWN EVENT ***";
								
								nOffset++;
								int i=0;

								CString m_szSubItems = "";
								while(i<nNumOps)
								{
									szPart.Format("opId = 0x%02x%02x\r\n", (unsigned char)*(pchXML+nOffset),  (unsigned char)*(pchXML+nOffset+1));
									m_szSubItems += szPart;

									int nSize = ((*(pchXML+nOffset+2))<<8) + *(pchXML+nOffset+3);

									szPart.Format("data_length = 0x%02x%02x (%d bytes)\r\n", (unsigned char)*(pchXML+nOffset+2),  (unsigned char)*(pchXML+nOffset+3), nSize);
									m_szSubItems += szPart;

									if(ulMessageLen > (unsigned long)nOffset+8)
									{

										szPart.Format("SMPTE registered ID = %c%c%c%c\r\n", 
											(unsigned char)*(pchXML+nOffset+4),  
											(unsigned char)*(pchXML+nOffset+5),
											(unsigned char)*(pchXML+nOffset+6),
											(unsigned char)*(pchXML+nOffset+7)
											);
										m_szSubItems += szPart;

										switch(*(pchXML+nOffset+8))
										{
										case 1: 
											{
												szTitle = "*** STREAM SEMAPHORE ***";
												szPart.Format("proprietary_command = %d (stream semaphore)\r\n", 
													(unsigned char)*(pchXML+nOffset+8)											
													);

												m_szSubItems += szPart;
												szPart.Format("EventStartDate = %c%c%c%c%c%c%c%c%c%c\r\n", 
													(unsigned char)*(pchXML+nOffset+9),											
													(unsigned char)*(pchXML+nOffset+10),											
													(unsigned char)*(pchXML+nOffset+11),											
													(unsigned char)*(pchXML+nOffset+12),											
													(unsigned char)*(pchXML+nOffset+13),											
													(unsigned char)*(pchXML+nOffset+14),											
													(unsigned char)*(pchXML+nOffset+15),											
													(unsigned char)*(pchXML+nOffset+16),											
													(unsigned char)*(pchXML+nOffset+17),											
													(unsigned char)*(pchXML+nOffset+18)											
													);

												m_szSubItems += szPart;
												szPart.Format("EventStart = %02d:%02d:%02d;%02d\r\n", 
													(unsigned char)*(pchXML+nOffset+19),											
													(unsigned char)*(pchXML+nOffset+20),											
													(unsigned char)*(pchXML+nOffset+21),											
													(unsigned char)*(pchXML+nOffset+22)									
													);

												m_szSubItems += szPart;
												szPart.Format("EventDur = %02d:%02d:%02d;%02d\r\n", 
													(unsigned char)*(pchXML+nOffset+23),											
													(unsigned char)*(pchXML+nOffset+24),											
													(unsigned char)*(pchXML+nOffset+25),											
													(unsigned char)*(pchXML+nOffset+26)									
													);

												m_szSubItems += szPart;

												char buffer[36];
												memset(buffer, 0, 36);
												memcpy(buffer, pchXML+nOffset+27, 16);

//												AfxMessageBox(buffer);

												CString szLabel = buffer;

												szLabel.TrimLeft();
												szLabel.TrimRight();

												if(szLabel.GetLength()>0)
												{
													szPart.Format("Program ID = \"%s\"\r\n", szLabel );
												}
												else
												{
													szPart.Format("Program ID = <Blank>\r\n" );
												}
												m_szSubItems += szPart;

												memset(buffer, 0, 36);
												memcpy(buffer, pchXML+nOffset+27+16, 32);

												szLabel = buffer;

												szLabel.TrimLeft();
												szLabel.TrimRight();

												if(szLabel.GetLength()>0)
												{
													szPart.Format("Program Description = \"%s\"\r\n", szLabel );
												}
												else
												{
													szPart.Format("Program Description = <Blank>\r\n" );
												}


											} break;
										case 2: 
											{
												szTitle = "*** SUBSTITUTE COMMERCIAL SEQUENCE ***";
												szPart.Format("proprietary_command = %d (Substitute Commercial Sequence)\r\n", 
													(unsigned char)*(pchXML+nOffset+8)											
													);

												m_szSubItems += szPart;
												szPart.Format("Sequence Duration = %02d:%02d:%02d;%02d\r\n", 
													(unsigned char)*(pchXML+nOffset+9),											
													(unsigned char)*(pchXML+nOffset+10),											
													(unsigned char)*(pchXML+nOffset+11),											
													(unsigned char)*(pchXML+nOffset+12)									
													);

												m_szSubItems += szPart;

												char buffer[36];
												memset(buffer, 0, 36);
												memcpy(buffer, pchXML+nOffset+13, 16);

//												AfxMessageBox(buffer);

												CString szLabel = buffer;

												szLabel.TrimLeft();
												szLabel.TrimRight();

												if(szLabel.GetLength()>0)
												{
													szPart.Format("Last Program ID = \"%s\"\r\n", szLabel );
												}
												else
												{
													szPart.Format("Last Program ID = <Blank>\r\n" );
												}


												memset(buffer, 0, 36);
												memcpy(buffer, pchXML+nOffset+13+16, 32);

												m_szSubItems += szPart;

												szLabel = buffer;

												szLabel.TrimLeft();
												szLabel.TrimRight();

												if(szLabel.GetLength()>0)
												{
													szPart.Format("Last Program Description = \"%s\"\r\n", szLabel );
												}
												else
												{
													szPart.Format("Last Program Description = <Blank>\r\n" );
												}

											} break;
										case 3: 
											{
												szTitle = "*** SUBSTITUTE PROGRAM ***";
												szPart.Format("proprietary_command = %d (Substitute Program)\r\n", 
													(unsigned char)*(pchXML+nOffset+8)											
													);


												m_szSubItems += szPart;
												szPart.Format("Substitution Duration = %02d:%02d:%02d;%02d\r\n", 
													(unsigned char)*(pchXML+nOffset+9),											
													(unsigned char)*(pchXML+nOffset+10),											
													(unsigned char)*(pchXML+nOffset+11),											
													(unsigned char)*(pchXML+nOffset+12)									
													);

												m_szSubItems += szPart;
												szPart.Format("Offset = %02d:%02d:%02d;%02d\r\n", 
													(unsigned char)*(pchXML+nOffset+13),											
													(unsigned char)*(pchXML+nOffset+14),											
													(unsigned char)*(pchXML+nOffset+15),											
													(unsigned char)*(pchXML+nOffset+16)									
													);

												m_szSubItems += szPart;

												char buffer[36];
												memset(buffer, 0, 36);
												memcpy(buffer, pchXML+nOffset+17, 16);

//												AfxMessageBox(buffer);

												CString szLabel = buffer;

												szLabel.TrimLeft();
												szLabel.TrimRight();

												if(szLabel.GetLength()>0)
												{
													szPart.Format("Replacement ProgramID = \"%s\"\r\n", szLabel );
												}
												else
												{
													szPart.Format("Replacement ProgramID = <Blank>\r\n" );
												}

											} break;
										case 4: 
											{
												szTitle = "*** CANCEL SUBSTITUTIONS ***";
												szPart.Format("proprietary_command = %d (Cancel Substitutions)\r\n", 
													(unsigned char)*(pchXML+nOffset+8)											
													);
											} break;
										case 5: 
											{
												szTitle = "*** STREAM SYNC ***";
												szPart.Format("proprietary_command = %d (Stream Sync)\r\n", 
													(unsigned char)*(pchXML+nOffset+8)											
													);
											} break;
										default:
											{
												szTitle = "*** UNKNOWN EVENT ***";
												szPart.Format("proprietary_command = %d (unknown)\r\n", 
													(unsigned char)*(pchXML+nOffset+8)											
													);
											} break;
										}
										m_szSubItems += szPart;
									}


									nOffset += nSize+4;
									szPart.Format("\r\n");
									m_szSubItems += szPart;

									i++;
								}


								szPart = szTitle + "\r\n\r\n"+ g_psocketmodule->m_data.m_szDisplay + "\r\n\r\n" + m_szSubItems;
								g_psocketmodule->m_data.m_szDisplay = szPart;


							}




						}
						else
						{
							// single message
							g_psocketmodule->m_data.m_szDisplay.Format("SCTE104 single message, %d bytes\r\n\r\n", ulMessageLen);

							// OK, quick parse this biznitch

							CString szPart;

							if(ulMessageLen>=13)
							{
/*
single_operation_message() 
{
	opID 2 uimsbf
	messageSize 2 uimsbf
	result 2 uimsbf
	result_extension 2 uimsbf
	protocol_version 1 uimsbf
	AS_index 1 uimsbf
	message_number 1 uimsbf
	DPI_PID_index 2 uimsbf
	data() * Varies
}
*/

								szPart.Format("opId = 0x%02x%02x\r\n", (unsigned char)*pchXML,  (unsigned char)*(pchXML+1));
								g_psocketmodule->m_data.m_szDisplay += szPart;

								int nSize = ( ((unsigned char) (*(pchXML+2))) <<8) + ((unsigned char)(*(pchXML+3)));

								szPart.Format("messageSize = 0x%02x%02x (%d bytes)\r\n", (unsigned char)*(pchXML+2),  (unsigned char)*(pchXML+3), nSize);
								g_psocketmodule->m_data.m_szDisplay += szPart;

								szPart.Format("result = 0x%02x%02x\r\n", (unsigned char)*(pchXML+4),  (unsigned char)*(pchXML+5));
								g_psocketmodule->m_data.m_szDisplay += szPart;
								szPart.Format("result_extension = 0x%02x%02x\r\n", (unsigned char)*(pchXML+6),  (unsigned char)*(pchXML+7));
								g_psocketmodule->m_data.m_szDisplay += szPart;
								szPart.Format("protocol_version = 0x%02x\r\n", (unsigned char)*(pchXML+8));
								g_psocketmodule->m_data.m_szDisplay += szPart;
								szPart.Format("AS_index = 0x%02x\r\n", (unsigned char)*(pchXML+9));
								g_psocketmodule->m_data.m_szDisplay += szPart;
//								szPart.Format("message_number = 0x%02x (%d decimal)\r\n", (unsigned char)*(pchXML+10), *(pchXML+10));
								szPart.Format("message_number = 0x%02x\r\n", (unsigned char)*(pchXML+10));
								g_psocketmodule->m_data.m_szDisplay += szPart;
								szPart.Format("DPI_PID_index = 0x%02x%02x\r\n", (unsigned char)*(pchXML+11),  (unsigned char)*(pchXML+12));
								g_psocketmodule->m_data.m_szDisplay += szPart;

								if(ulMessageLen>13)
								{
									szPart.Format("data (%d bytes) = %s\r\n", ulMessageLen-13, (pchXML+13));
									g_psocketmodule->m_data.m_szDisplay += szPart;
								}
							}
							else
							{
								szPart = "Parse Error";
								g_psocketmodule->m_data.m_szDisplay += szPart;
							}

						}










}






















if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "begin send response, message id %d",ulMessageID);  //(Dispatch message)

								// send it
		_ftime(&timeperiodic);


//[00][00][00][0d][00][64][ff][ff][00][00][ab][00][00]

								unsigned char uchReturnBuffer[256];// always just send return success, single message, 13 bytes or so

								uchReturnBuffer[0]  = 0x00;
								switch(ulMessageID)
								{
								case 0x01: uchReturnBuffer[1]  = 0x02; break;
								case 0x03: uchReturnBuffer[1]  = 0x04; break;
								default: uchReturnBuffer[1]  = 0x00; break;  //unconditional success
								}

								uchReturnBuffer[2]  = 0x00;
								uchReturnBuffer[3]  = 0x0d;
								uchReturnBuffer[4]  = 0x00;
								uchReturnBuffer[5]  = 0x64;
								uchReturnBuffer[6]  = 0xff;
								uchReturnBuffer[7]  = 0xff;
								uchReturnBuffer[8]  = 0x00;
								uchReturnBuffer[9]  = 0x00;
								uchReturnBuffer[10] = uchMessageNum;  if(uchMessageNum>=255) uchMessageNum=0; uchMessageNum++; //skip 0
								uchReturnBuffer[11] = 0x00;
								uchReturnBuffer[12] = 0x00;
								uchReturnBuffer[13] = 0x00;
								uchReturnBuffer[14] = 0x00;


								unsigned long ulBufLen = 13; 
								int nReturn = net.SendLine(uchReturnBuffer, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
								if(nReturn<NET_SUCCESS)
								{
if(g_psocketmodule)
{
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending response message to %d.%d.%d.%d: %s\r\nResetting connection.", 
							nReturn,
							pClient->m_si.sin_addr.s_net, 
							pClient->m_si.sin_addr.s_host, 
							pClient->m_si.sin_addr.s_lh, 
							pClient->m_si.sin_addr.s_impno,
							pszStatus
						);
	g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, szSocketModuleSource, errorstring);  //(Dispatch message)
//				g_psocketmodule->SendMsg(CX_SENDMSG_ERROR, szSocketModuleSource, errorstring);
}
								}

if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "end send response");  //(Dispatch message)


								// log it
/*
								//debug file write
								if((g_psocketmodule)&&(g_psocketmodule->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
								{
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "begin log response");  //(Dispatch message)
									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_psocketmodule->m_settings.m_pszName?g_psocketmodule->m_settings.m_pszName:"SocketModule"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lasttxfilename, filename)==0)
									{
										nTxDupes++;
									}
									else
									{
										nTxDupes=0;
										strcpy(lasttxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

									fp = fopen(filename, "wb");
									if(fp)
									{
										int b=CX_XML_BUFFER_CORTEXBEGIN;
										while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
										{
//											fwrite(filename, 1, strlen(filename),fp);
											if(msg.m_pchResponse[b])
											{
												if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

												fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
												fflush(fp);
											}
											b++;

										}
										fclose(fp);
										fp = NULL;
									}
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "end log response");  //(Dispatch message)
								}
*/


								if(pchXML) 
								{
	EnterCriticalSection(&g_psocketmodule->m_data.m_critLastCommand);

									if(ulMessageID == 3)
									{
										g_psocketmodule->m_data.m_ulLastKeepAlive = timeactive.time;
									}
									else
									{

										if(g_psocketmodule->m_data.m_pucLastCommand)
										{
											try { free(g_psocketmodule->m_data.m_pucLastCommand); } catch(...){}

										}
										g_psocketmodule->m_data.m_ulLastCommandTime = timeactive.time;

										g_psocketmodule->m_data.m_pucLastCommand = (unsigned char*)pchXML;
										g_psocketmodule->m_data.m_ulLastCommandLength = ulMessageLen;

									}

	LeaveCriticalSection(&g_psocketmodule->m_data.m_critLastCommand);

								}
								pchXML = NULL;



//AfxMessageBox("005");
								if (pchXMLStream) 
								{
									pchEnd = NULL;
									bHasMessage = false;
									bMulti = false;
									ulMessageLen = 0;

									if(ulAccumulatedBufferLen>=4)
									{
										ulMessageLen = ((((unsigned char)(*(pchXMLStream+2)))<<8) | ((unsigned char)(*(pchXMLStream+3))));

										if(ulMessageLen <= ulAccumulatedBufferLen)
										{
											bHasMessage = true;
											pchEnd = pchXMLStream+ulMessageLen;

											if((pchXMLStream[0]==0xff)&&(pchXMLStream[1]==0xff)) bMulti = true;
										}
									}

//									pchEnd = strstr(pchXMLStream, "</cortex>");
								}
								else
								{
									pchEnd = NULL;
									bHasMessage = false;
									ulMessageLen = 0;

								}
//AfxMessageBox("006");
							}  // while </cortex> exists.
						}//if(pchXMLStream)
						// dont do a free(pch);  
					}  // else out of mem, so just skip
					else
					{
						// out of mem.. .log error?
					}

				} //if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))

			} // if getline succeeds.
			else
			{
				// here, we either have timed out because theres no data, or, the connection has been lost.
				// if we lost the conn, no need to re-establish, we are a client handler, let the client reconnect if they want.
				if(nReturnCode == NET_ERROR_CONN)		// connection lost
				{
					ulRetry++;

					if(g_psocketmodule)
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH, "Lost connection from %d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno );

						g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, errorstring);  //(Dispatch message)
//						g_psocketmodule->SendMsg(CX_SENDMSG_INFO, szSocketModuleSource, errorstring);
					}

					break;  // break out of while loop, closes connection, starts again
				}
				else
				{
//									g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, szSocketModuleSource, "check %d", clock());  //(Dispatch message)

					// we prob just havent gotten a message because no data.  
					// so, check to see if its time for publication service
									// no, do this in the stall loop


				}

			} // end of else from if(nReturnCode == NET_SUCCESS)

			// and lets check if we've timed out on the connection, no matter what the success was
			_ftime( &timestamp );
			if(
				  (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				&&(ulConnTimeout>0)
				&&(ulConnLastMessage>0)
				&&((ulConnLastMessage + ulConnTimeout) < (unsigned long)timestamp.time )
				)
			{
				// we timed out, need to disconnect
				ulRetry++;


				sprintf(errorstring, "Connection from %d.%d.%d.%d timed out.", 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno);

				if(g_psocketmodule)
				{
					g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, errorstring);  //(Dispatch message)
//					g_psocketmodule->SendMsg(CX_SENDMSG_INFO, szSocketModuleSource, errorstring);
				}

				if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;} //free this memory before breaking out of the while.

				break;  // break out of while loop, closes connection, starts again
			}

			if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;}
			
			if(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) Sleep(1);  // dont peg processor




/*
			// following line fills out the data structure
//			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);
			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "SocketModule:XMLHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "SocketModule:XMLHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "SocketModule:XMLHandlerThread");
						}
					}
				}
			}
*/
			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "SocketModule:XMLHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
//						g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, szSocketModuleSource, "check %d", clock());  //(Dispatch message)


/*
						if(nPeriodic>0)
						{
							_ftime( &timestamp );
							if((timeperiodic.time + nPeriodic)< timestamp.time)
							{
								// send asynchronous message

								//create a response UUID
								UUID uuida;
								UuidCreate(&uuida); // could check response code to be RPC_S_OK ?

								unsigned char* pucUUIDa = NULL;
								UuidToString(&uuida, &pucUUIDa);

								msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUIDa);

								if(pucUUIDa)
								{
									try { RpcStringFree(&pucUUIDa); } catch(...){}
								}
								pucUUIDa = NULL;


								msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"system\" code=\"0\">");

								msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
								msg.ClearContent(CX_XML_BUFFER_FLAGS);

								unsigned long ulDataLen = 0;
								char* pchStatus = g_psocketmodule->m_data.GetStatusText(&ulDataLen);
								if(pchStatus)
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0x%08x</code><text>%s</text></status><time>%d.%03d</time></system>",
										ulDataLen,
										pchStatus,
										timestamp.time, timestamp.millitm
										);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0xffffffff</code><text>unavailable</text></status><time>%d.%03d</time></system>",
										timestamp.time, timestamp.millitm
										);

								}

								if(pchStatus){try { free(pchStatus); } catch(...){} pchStatus=NULL;}

								ulDataLen = 0;
								if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
								{
									ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
								}
								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
									ulDataLen,
									net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
									);

								msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>%s status message</msg>", 
									(g_psocketmodule->m_settings.m_pszName?g_psocketmodule->m_settings.m_pszName:"SocketModule"));

								msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


								// now, send and log.
	////////////////////////////////////////////// code copied from above.  if modified, must be 2 places.  sorry...
									// send it
			_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_psocketmodule)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending system message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, szSocketModuleSource, errorstring);  //(Dispatch message)
				g_psocketmodule->SendMsg(CX_SENDMSG_ERROR, szSocketModuleSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
								
	//AfxMessageBox("oop");
									// log it

									//debug file write
									if((g_psocketmodule)&&(g_psocketmodule->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
									{
										strcpy(filename, "Logs");
										_mkdir(filename);  // if exists already np
										strcat(filename, "\\");
										sprintf(errorstring, "%d.%d.%d.%d", 
											pClient->m_si.sin_addr.s_net, 
											pClient->m_si.sin_addr.s_host, 
											pClient->m_si.sin_addr.s_lh, 
											pClient->m_si.sin_addr.s_impno);

										strcat(filename, errorstring);

										_mkdir(filename);  // if exists already np

		//								_ftime( &timestamp );  // use timestamp we just had..

										tm* theTime = localtime( &timestamp.time	);

										sprintf(errorstring, "\\%s_", 
											(g_psocketmodule->m_settings.m_pszName?g_psocketmodule->m_settings.m_pszName:"SocketModule"));

										strcat(filename, errorstring);

										char filenametemp[MAX_PATH];
										strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

										strcat(filename, filenametemp);

										int nOffset = strlen(filename);
										sprintf(filename+nOffset,"%03d",timestamp.millitm);

										if (strcmp(lasttxfilename, filename)==0)
										{
											nTxDupes++;
										}
										else
										{
											nTxDupes=0;
											strcpy(lasttxfilename, filename);
										}
										nOffset = strlen(filename);
										sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

										fp = fopen(filename, "wb");
										if(fp)
										{
											int b=CX_XML_BUFFER_CORTEXBEGIN;
											while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
											{
	//											fwrite(filename, 1, strlen(filename),fp);
												if(msg.m_pchResponse[b])
												{
													if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

													fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
													fflush(fp);
												}
												b++;

											}
											fclose(fp);
											fp = NULL;
										}
									}


									b=CX_XML_BUFFER_TX;
									while(b<CX_XML_BUFFER_DATAEND)
									{
										if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
										{
											try { free(msg.m_pchResponse[b]); } catch(...){}
											msg.m_pchResponse[b] = NULL;
										}
										b++;
									}
	/////////////////////////////////////////////
							}

						}
*/
						//and check timeout
						if(nTimed>0)
						{
							_ftime( &timestamp );
							if((timeactive.time + nTimed) < timestamp.time)
							{
								//kill it

								bCloseCommand = true;
							}
							
						}


						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} 

//cleanup:
//		AfxMessageBox("cleanup");
//AfxMessageBox("010");


//AfxMessageBox("011");

// cleanup:  //was here but want to uninit com etc
//AfxMessageBox("012");

		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d closed.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);

//AfxMessageBox("013");
		if(g_psocketmodule)
		{
			g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, errorstring);  //(Dispatch message)
//			g_psocketmodule->SendMsg(CX_SENDMSG_INFO, szSocketModuleSource, errorstring);
		}

//AfxMessageBox("014");

		if(pch!=NULL) { try{free(pch);} catch(...) {} } // must free the data buffer 

//AfxMessageBox("015");

		shutdown(pClient->m_socket, SD_BOTH);
//AfxMessageBox("016");
		closesocket(pClient->m_socket);
//AfxMessageBox("017");
		(*(pClient->m_pulConnections))--;
//AfxMessageBox("018");

	}
//		AfxMessageBox("end cleanup");
//AfxMessageBox("019");

	try
	{  
		CoUninitialize(); //XML
	}
	catch(...)
	{
		if(g_psocketmodule) g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, szSocketModuleSource, "Exception in CoUninitialize");  //(Dispatch message)
	}

//AfxMessageBox("020");

	try
	{
		delete pClient; // was created with new in the thread that spawned this one.
	}	catch(...) {}

//AfxMessageBox("021");
	if (bSetGlobalKill)	//g_bKillThread = true;
		((CSocketModuleHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();


}


void SocketModuleCommandHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure in Protocol 1  //SECURE with PROTOCOL2 later
			strcpy(pszInfo, "");  // clear it
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error %d receiving data.  %s", nReturn, pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					strcpy(pszInfo, "");  // clear it
					nReturn = net.SendData(&data, pClient->m_socket, 2000, 0, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error %d sending NAK reply.  %s", nReturn, pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
						// we had an error receiving data and then anotehr trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{
//AfxMessageBox("receiving");

					// parse the command:

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					switch(data.m_ucCmd)
					{
					case CX_CMD_GETINFO: // not actually supported yet.  everything thru csf files for now.
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case SOCKETMODULE_CMD_EXSET://				0xea // increments exchange counter
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)
							{	
		//						data.m_ucSubCmd = (unsigned char) g_psocketmodule->m_data.IncrementDatabaseMods((char*)data.m_pucData);
								free(data.m_pucData);  //destroy the buffer;
							}
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.

							data.m_ucType = NET_TYPE_PROTOCOL1;//NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case SOCKETMODULE_CMD_EXGET://				0xeb // gets exchange mod value
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case SOCKETMODULE_CMD_MODSET://			0xec // sets exchange mod value, skips exchange table
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case CX_CMD_GETSTATUS: //	0x99 // gets status info
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							unsigned long ulStatus;
							// assemble if there is 

							char* pch = g_psocketmodule->m_data.GetStatusText(&ulStatus);  // allocates mem, must free after use
							if(pch)
							{
								data.m_ucSubCmd = (unsigned char)(SOCKETMODULE_ICON_MASK&ulStatus);
								data.m_pucData=(unsigned char*)pch;
								data.m_ulDataLen = strlen(pch);
							}
							else
							{
								data.m_ucCmd = NET_CMD_NAK;
								data.m_ucSubCmd = NET_CMD_NAK;
							}

							data.m_ucType = NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case CX_CMD_BYE:
						{
	g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "The Global Kill command has been received.");  //(Dispatch message)

							bSetGlobalKill = true;
							g_psocketmodule->m_data.m_bQuietKill = true;
							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					default:
						{
							//just ack
							data.m_ucCmd = NET_CMD_ACK;

		//					char repbuf[64];
							if(data.m_pucData!=NULL)
							{
		/*
								_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

								for(unsigned long q=0; q<data.m_ulDataLen; q++)
								{
									if(strlen(repbuf)<63)
										strncat(repbuf, (char*)(data.m_pucData+q), 1);
								}
								if(strlen(repbuf)<63)
									strcat(repbuf, "]");
								data.m_ulDataLen = strlen(repbuf);
								repbuf[data.m_ulDataLen] = 0;  // just in case
		*/
								free(data.m_pucData);  //destroy the buffer;
							}
		/*
							else
							{
								_snprintf(repbuf, 63, "data was NULL");
								data.m_ulDataLen = strlen(repbuf); 
							}

							data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
							if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
							else 
							{
								data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
								data.m_ulDataLen = 0;
							}
		*/
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					}

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "SocketModule:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "SocketModule:CommandHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.
	if (bSetGlobalKill)	//g_bKillThread = true;
	((CSocketModuleHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}


#endif // USE_LISTENERS


#ifdef USE_XMLLISTENER

void SocketModuleStatusHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "SocketModule:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "SocketModule:CommandHandlerThread");
						}
						// we had an error receiving data and then anotehr trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "SocketModule:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "SocketModule:CommandHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}

#endif

#ifdef USE_MESSAGING

int CSocketModuleMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)&&(strlen(m_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			(m_settings.m_pszMessages?m_settings.m_pszMessages:"Messages"),
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			(m_settings.m_pszMessages?m_settings.m_pszMessages:"Messages")
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_data.m_critSQL);
		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_data.m_critSQL);
			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return SOCKETMODULE_SUCCESS;
		}
		LeaveCriticalSection(&m_data.m_critSQL);

	}
	return SOCKETMODULE_ERROR;
}

int CSocketModuleMain::SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszAsRun)&&(strlen(m_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			(m_settings.m_pszAsRun?m_settings.m_pszAsRun:"AsRun_Log"),
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			(m_settings.m_pszAsRun?m_settings.m_pszAsRun:"AsRun_Log")
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_data.m_critSQL);
		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_data.m_critSQL);
			m_data.IncrementDatabaseMods(m_settings.m_pszAsRun, dberrorstring);
			return SOCKETMODULE_SUCCESS;
		}
		LeaveCriticalSection(&m_data.m_critSQL);

	}
	return SOCKETMODULE_ERROR;
}



void SocketModuleXMLHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;
	
	char szSocketModuleSource[MAX_PATH]; 
	strcpy(szSocketModuleSource, "SocketModuleXMLHandler");

	CBufferUtil bu;

	HRESULT hRes;
	try
	{  

#if _WIN32_WINNT >= 0x0400
		hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		hRes = CoInitialize(NULL);
#endif
	}
	catch(...)
	{
		if(g_psocketmodule) g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, "Exception in CoInitialize");  //(Dispatch message)
	}


	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		char dberrorstring[DB_ERRORSTRING_LEN];
//		char xmlerrorstring[MAX_MESSAGE_LENGTH];
		char errorstring[MAX_MESSAGE_LENGTH];
//		char szSQL[DB_SQLSTRING_MAXLEN];
		strcpy(dberrorstring, "");
		strcpy(errorstring, "");

//		char element[MAX_MESSAGE_LENGTH];

//		AfxMessageBox(szSocketModuleSource);

		CNetUtil net(false); // local object for utility functions.
		CCortexUtil util;  // cortex utility functions.

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		bool bCloseCommand = false;  // make these not persistent unless a persist command comes in.
		bool bPersist = false;  // make these not persistent unless a persist command comes in.
		int nPeriodic = -1;  
		int nTimed = -1;  

		char* pchBuffer = NULL;
		unsigned long ulBufferLen = 0;
		char* pch = NULL;
		char* pchXML = NULL;
		char* pchXMLStream = NULL;
		unsigned long ulAccumulatedBufferLen = 0;
		unsigned long ulRetry = 0;

		unsigned long ulConnLastMessage = 0;
		unsigned long ulConnTimeout = 0;
		
		char filename[MAX_PATH];
		char lastrxfilename[MAX_PATH];
		char lasttxfilename[MAX_PATH];
		int nRxDupes=0;
		int nTxDupes=0;

		FILE* fp = NULL;

		_timeb timestamp;
		_timeb timeactive;
		_timeb timeperiodic;

		_ftime(&timeactive);
		_ftime(&timeperiodic);


	// 	(*(pClient->m_pulConnections))++;//already exists

		IXMLDOMDocumentPtr pDoc(__uuidof(DOMDocument));  // let's have only one object per connection
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread D"); Sleep(100); //(Dispatch message)
		HRESULT hr;//= pDoc.CreateInstance(__uuidof(DOMDocument));
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread E");  //(Dispatch message)
		hr = pDoc->put_async(VARIANT_FALSE);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread F");  //(Dispatch message)
										
		// Remove document safety options
		IObjectSafety* pSafety=NULL;
		DWORD dwSupported, dwEnabled;
			

		if ( SUCCEEDED(pDoc->QueryInterface(IID_IObjectSafety, (void**)&pSafety)))
		{
			pSafety->GetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, &dwSupported, &dwEnabled );
			pSafety->SetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, dwSupported, 0 );
		}

		// initialize random seed
		srand ( time(NULL) );


		CCortexMessage msg;

		// allocate all the static ones
//		char* m_pchResponse[CX_XML_BUFFER_COUNT] = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};

//		AfxMessageBox("formatting");

		msg.FormatContent(CX_XML_BUFFER_CORTEXBEGIN, MAX_MESSAGE_LENGTH, "<cortex src=\"%s\" version=\"%s\">", 
			(g_psocketmodule->m_settings.m_pszName?g_psocketmodule->m_settings.m_pszName:"SocketModule"), 
			CX_XML_VERSION
			);


		msg.SetContent(CX_XML_BUFFER_CORTEXEND, "</cortex>");

		msg.SetContent(CX_XML_BUFFER_FLAGSEND, "</flags>");

		msg.SetContent(CX_XML_BUFFER_DATAEND, "</data>");

		//  ack and nak are the same length so we can allocate it, just replace contents later
		msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

//		AfxMessageBox("set");



		if (FAILED(hRes))
		{
			sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established, but CoInitialize failed.  Unable to process XML.", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,
				pClient->m_socket);

			if(g_psocketmodule)
			{
				g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, szSocketModuleSource, errorstring);  //(Dispatch message)
				g_psocketmodule->SendMsg(CX_SENDMSG_ERROR, szSocketModuleSource, errorstring);
			}

			goto cleanup;
		}

		
		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);
		if(g_psocketmodule)
		{
			g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, errorstring);  //(Dispatch message)
			g_psocketmodule->SendMsg(CX_SENDMSG_INFO, szSocketModuleSource, errorstring);
		}



		while ( 
									(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
//								&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
								&&(pClient->m_socket!=NULL)
								&&(!bCloseCommand)
					)
		{


			pchBuffer = NULL;
			ulBufferLen = 0;

			
			int nReturnCode = net.GetLine((unsigned char**)&pchBuffer, &ulBufferLen, pClient->m_socket, NET_RCV_ONCE, errorstring);
			if(nReturnCode == NET_SUCCESS)
			{
				ulRetry = 0;
				//process any received XML.
				// have to keep accumulating until we find a </cortex> tag.
				if((pchBuffer)&&(ulBufferLen)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
				{
					int nLen = 0;
					if(pchXMLStream) nLen = ulAccumulatedBufferLen;
					pch = (char*) malloc(nLen+ulBufferLen+1);  //term 0
					if(pch)
					{
						char* pchEnd = NULL;
						char* pchNext = NULL;
						if(pchXMLStream)  // we have an old buffer.
						{
							memcpy(pch, pchXMLStream, ulAccumulatedBufferLen);
							memcpy(pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen);
							free(pchXMLStream); 
							pchXMLStream = pch;  // reassign!
							ulAccumulatedBufferLen += ulBufferLen;
							*(pch+ulAccumulatedBufferLen) = 0;  // null term
						}
						else
						{
							// this is new.
							// first we have to skip all chars that are not a '<'
							// pchEnd = strchr(pchBuffer, '<');  // have to find first tag, we are only interested in XML.
							// used to use strchr but if there are leading zeros in the buffer, we never get past them

							pchEnd = pchBuffer;
							while((*pchEnd!='<')&&(pchEnd<pchBuffer+ulBufferLen)) pchEnd++;

							if(pchEnd<pchBuffer+ulBufferLen)
							{
								strcpy(pch, pchEnd);
								ulAccumulatedBufferLen += (ulBufferLen-(pchEnd-pchBuffer));
								pchXMLStream = pch;  // reassign!
								*(pch+ulAccumulatedBufferLen) = 0;  // null term
							}
							else  // not found!
							{
								free(pch);
								pchXMLStream = NULL;
							}
						}

						if(pchBuffer) free(pchBuffer);
						pchBuffer = NULL;
						if(pchXMLStream)
						{

							pchEnd = strstr(pchXMLStream, "</cortex>");
							while((pchEnd)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
							{
							// found a token.
								pchEnd+=strlen("</cortex>");

								pchNext = pchEnd;
								while((*pchNext!='<')&&(pchNext<pchEnd+strlen(pchEnd))) pchNext++;

								if(pchNext<pchEnd+strlen(pchEnd))
								{
									// we found a remainder.
									nLen = strlen(pchNext);
									pch = (char*) malloc(nLen+1);  //term 0
									if(pch)
									{
										memcpy(pch, pchNext, nLen);
										*(pch+nLen) = 0;
									}
								}
								else pch = NULL;


								pchXML = pchXMLStream; // just use it.
								*pchEnd = 0; //null terminate it

								pchXMLStream = pch;  // take the rest of the stream.
								if(pchXMLStream) ulAccumulatedBufferLen = strlen(pchXMLStream);
								else ulAccumulatedBufferLen=0;

								// now have to deal with XML found in pchXML.
								//debug file write
								if((g_psocketmodule)&&(g_psocketmodule->m_settings.m_bUseXMLClientLog))
								{
//				AfxMessageBox("logging");
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "begin log receive");  //(Dispatch message)

									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_psocketmodule->m_settings.m_pszName?g_psocketmodule->m_settings.m_pszName:"SocketModule"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lastrxfilename, filename)==0)
									{
										nRxDupes++;
									}
									else
									{
										nRxDupes=0;
										strcpy(lastrxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_RX.xml",nRxDupes);

//	AfxMessageBox(filename);

									fp = fopen(filename, "wb");
									if(fp)
									{
//											fwrite(filename, 1, strlen(filename),fp);
										fwrite(pchXML, 1, strlen(pchXML),fp);
										fflush(fp);
										fclose(fp);
										fp = NULL;
									}
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "end log receive");  //(Dispatch message)

								}

								// do stuff here.

								_ftime(&timeactive);  // reset the inactivity timer.


								// first attempt to extract the rxid.
								char* pchRXID = NULL;
								int nIDlen = 0;

								char* pchFind = strstr(pchXML, "<rx rxid=\"");

								if(pchFind)
								{
									pchFind+=strlen("<rx rxid=\"");
									char* pchTerminate = pchFind;
									while(pchTerminate<pchEnd)
									{
										if(
											  (*pchTerminate == '\"')
											||(*pchTerminate == '>')
											||((pchTerminate<pchEnd)&&(*pchTerminate == '/')&&(*(pchTerminate+1) == '>'))
											)
										{
											break;
										}
										else
										{
											pchTerminate+=1;
										}
									}

									nIDlen = pchTerminate-pchFind;
									if(nIDlen>0)
									{
										pchRXID = (char*)malloc(nIDlen+1);
									}
									if(pchRXID)
									{
										memset(pchRXID, 0, nIDlen+1);
										memcpy(pchRXID, pchFind, nIDlen);

										pchTerminate = bu.XMLEncode(pchRXID); // just in case
										if(pchTerminate)
										{
											try{free(pchRXID);} catch(...){}
											pchRXID = pchTerminate;
										}
									}
								}


							//create a response UUID
								UUID uuid;
								UuidCreate(&uuid); // could check response code to be RPC_S_OK ?

					//			CoCreateGuid(&uuid);

								unsigned char* pucUUID = NULL;
								UuidToString(&uuid, &pucUUID);
								// call this later: RpcStringFree(&pucUUID);

								
								bool bResponseSent = false;  // means, sent a response already.  Otherwise have to look at the failure code and deal at the end

								msg.m_nTxStep = CX_XML_BUFFER_CORTEXBEGIN;

								//OK, let's assemble the tx element.

								if(pchRXID)
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUID);
								}


								// then, parse the XML
								msg.m_nFailure=CX_XML_ERR_SUCCESS;
								pDoc->put_async(VARIANT_FALSE);
			//				AfxMessageBox((char*)pchXML);
								hr = pDoc->loadXML((char*)pchXML);
								if(hr!=VARIANT_TRUE)
								{
								//					AfxMessageBox("bad load");
									bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML parse error: [%s]", pchXML );
									g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, errorstring);
									//let's try to send a nak.

									msg.m_nFailure=CX_XML_ERR_XMLPARSE;
									msg.m_nType = CX_XML_TYPE_UNK;
									msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
									msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML parse error %d</msg>", msg.m_nFailure);
									msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");
								}
								else
								{
	//								AfxMessageBox("yirgacheff");

if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "begin xml parse");  //(Dispatch message)
									IXMLDOMNodeListPtr pChildNodes = pDoc->GetchildNodes();

									IXMLDOMNodePtr pNodes[CX_XML_NODECOUNT];
									int n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }

									msg.m_nType = CX_XML_TYPE_UNK;
									msg.SetType("unknown");

									bool bCommandHandled = false;
									if (pChildNodes != NULL)
									{
										IXMLDOMNodePtr pChild;
										while(pChild = pChildNodes->nextNode())
										{
											DOMNodeType nodeType;
											pChild->get_nodeType(&nodeType);

											if(nodeType == NODE_ELEMENT)
											{
												char tag[MAX_PATH]; 
												//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
												util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

											//	AfxMessageBox(tag);

												if(strcmp("cortex", tag)==0)
												{
													pNodes[CX_XML_NODE_CORTEX] = pChild;
													// now get subs to cortex

													pChildNodes = pChild->GetchildNodes();
													if (pChildNodes != NULL)
													{
														while(pChild = pChildNodes->nextNode())
														{
															pChild->get_nodeType(&nodeType);

															if(nodeType == NODE_ELEMENT)
															{
																//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
																util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

														//		AfxMessageBox(tag);

																if(strcmp("rx", tag)==0)
																{
																	pNodes[CX_XML_NODE_RX] = pChild;
																	// now get the "real" rxid;

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
																	//	AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
																			//strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));

								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("rxid", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
																				//strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));

																		//		AfxMessageBox("replace rxid.");

																				if(pchRXID)
																				{
																					try{free(pchRXID);} catch(...){}
																					pchRXID = bu.XMLEncode(tag); // just in case
																					if(pchRXID)
																					{
// reassemble tx element
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
																					}
																				}

																			}
																			else
																			if(strcmp("part", tag)==0)
																			{
																				//  multi part message
																			}
																		}
																	}

																}
																else
																if(strcmp("cmd", tag)==0)
																{
																	pNodes[CX_XML_NODE_CMD] = pChild;
																	pNodes[CX_XML_NODE_OPTIONS] = NULL;
																	pNodes[CX_XML_NODE_DATA] = NULL;

																	// now get attribs and then get subs, data and options

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
								//										AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
																			//strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));
								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("type", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
																				//strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));
																				msg.SetType(tag);

//																																		AfxMessageBox(tag);

																				if(strcmp("ack", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_ACK;
																				}
																				else
																				if(strcmp("nak", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_NAK;
																				}
																				else
																				if(strcmp("persist", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PERSIST;
																					bPersist = true;
																				}
																				else
																				if(strcmp("quit", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_QUIT;
																					bPersist = false;
																				}
																				else
																				if(strcmp("status", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_STATUS;
																				}
																				else
																				if(strcmp("bye", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_BYE;
																				}
																				else
																				if(strcmp("get_version", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_GETVERSION;
																				}
																				else
																				if(strcmp("ping", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PING;
																				}
/*
																				else

																					

/////////////////////////////////////////////////////////////////////////////////////////////////
////                                    begin SocketModule specific commands
//#define SENTINEL_XML_TYPE_GETCONN				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
//#define SENTINEL_XML_TYPE_GETLIST				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
//#define SENTINEL_XML_TYPE_GETEVENT			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
																				if(strcmp("get_conn", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETCONN;
																				}
																				else
																				if(strcmp("get_list", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETLIST;
																				}
																				else
																				if(strcmp("get_event", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETEVENT;
																				}
																					
////                                    end SocketModule specific commands
/////////////////////////////////////////////////////////////////////////////////////////////////
*/

																				
																			}
																			else
																			if(strcmp("user", tag)==0)
																			{
																			}
																			else
																			if(strcmp("password", tag)==0)
																			{
																			}
																		}
																	}


																	IXMLDOMNodeListPtr pCmdChildNodes = pChild->GetchildNodes();
																	if (pCmdChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pCmdChild;
																		while(pCmdChild = pCmdChildNodes->nextNode())
																		{
																			pCmdChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				//strncpy(tag, W2T(pCmdChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pCmdChild->GetnodeName()));
																				if(strcmp("options", tag)==0)
																				{
																					pNodes[CX_XML_NODE_OPTIONS] = pCmdChild;
																				}
																				else
																				if(strcmp("data", tag)==0)
																				{
																					pNodes[CX_XML_NODE_DATA] = pCmdChild;
																				}
																			}
																		}
																	}
																}
															}
														}
														// by here, all the necessary stuff should be filled out.

//														AfxMessageBox("here");
												

														switch(msg.m_nType)
														{
														case CX_XML_TYPE_ACK://						1	// 4.1 ack Acknowledges a message with success
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the ack

																
															} break; //case CX_XML_TYPE_ACK
														case CX_XML_TYPE_NAK://						2	// 4.2 nak Acknowledges a message with failure, or cancels
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the nak

															} break; //case CX_XML_TYPE_NAK
														case CX_XML_TYPE_PERSIST://				3	// 4.3 persist Requests a persistent connection
															{
																//persistence already done, above
																// set timeout and hearbeat intervals
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"persist\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																if(pNodes[CX_XML_NODE_OPTIONS])
																{

																	// have to return flags
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																	IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																	if (pOptChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pOptChild;
																		while(pOptChild = pOptChildNodes->nextNode())
																		{
																			pOptChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				//strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pOptChild->GetnodeName()));
																				if(strcmp("timeout", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						nTimed = atoi(pchTmp);

																						_snprintf(tag, MAX_PATH, "<timeout>%d</timeout>", nTimed);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;
																					}
																				}
																				else
																				if(strcmp("heartbeat", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						strncpy(tag, pchTmp, MAX_PATH);

																						if(strcmp("yes", tag)==0)
																						{
																							nPeriodic = 30;
																						}
																						else
																						if(strcmp("no", tag)==0)
																						{
																							nPeriodic = -1;
																						}
																						else
																						{
																							nPeriodic = atoi(tag);
																						}

																						_snprintf(tag, MAX_PATH, "<heartbeat>%d</heartbeat>", nPeriodic);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );


																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;

																					}
																				}
																			}
																		}
																	}
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																}
																
															} break; //case CX_XML_TYPE_PERSIST
														case CX_XML_TYPE_QUIT://					4	// 4.4 quit Disconnects a persistent connection
															{
																//already done, above
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"quit\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																
															} break; //case CX_XML_TYPE_QUIT
														case CX_XML_TYPE_STATUS://				5	// 4.5 status Obtains status information regarding the module
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"status\" code=\"0\">");

																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																char* pchStatus = g_psocketmodule->m_data.GetStatusText(&ulDataLen);
																if(pchStatus)
																{
																	msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<status><code>0x%08x</code><text>%s</text></status>",
																		ulDataLen,
																		pchStatus	
																		);
																}
																else
																{
																	msg.SetContent(CX_XML_BUFFER_DATA, 
																		"<status><code>0xffffffff</code><text>unavailable</text></status>"
																		);

																}

																if(pchStatus){try { free(pchStatus); } catch(...){}  pchStatus=NULL;}

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


															} break; //case CX_XML_TYPE_STATUS
														case CX_XML_TYPE_BYE://						6	// 4.6 bye Causes the module to shut down
															{

g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, "SocketModule", "The Global Kill XML command has been received.");  //(Dispatch message)

																bSetGlobalKill = true;
																g_psocketmodule->m_data.m_bQuietKill = true;
																bCloseCommand = true;

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"bye\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_GETVERSION://		7	// 4.7 get_version gets the version of the module as well as the build date
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_version\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<version>%s</version><build>%s %s</build>",
																		CX_CURRENT_VERSION,
																		 __DATE__,
																		 __TIME__	
																		);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_PING://					8	// 4.8 ping just sends an ack to indicate it is alive
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"ping\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE


////////////////////////////////////////////////////////
// begin SocketModule specific XML commands
/*
														case SENTINEL_XML_TYPE_GETCONN://				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_conn\" code=\"%d\">", g_psocketmodule->m_data.m_nNumConnectionObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_psocketmodule->m_data.m_critConns);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_psocketmodule->m_data.m_ppConnObj)&&(g_psocketmodule->m_data.m_nNumConnectionObjects))
																{

																	//let's see if we have any options.

																	char* pchServer = NULL;  //server name search
																	int nActive =-1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{

																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("active", tag)==0)
																					{
																				//		AfxMessageBox("active");
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}
																					//		AfxMessageBox("active 2");

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																				//			AfxMessageBox(tag);

																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_psocketmodule->m_data.m_nNumConnectionObjects)
																	{

																		if(g_psocketmodule->m_data.m_ppConnObj[m])
																		{
																			bool bInclude = true;
																			CSocketModuleConnectionObject* pObj = g_psocketmodule->m_data.m_ppConnObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																	//		AfxMessageBox("here");
																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																	//			AfxMessageBox("here 2");
																			if(bInclude)
																			{
																				if(pObj->m_pAPIConn)
																				{
																					if((pObj->m_pAPIConn->m_SysData.systemfrx==0x29)
																						&&(pObj->m_pAPIConn->m_SysData.systemdf))
																					{
																						strcpy(tag, "29.97");
																					}
																					else
																					{
																						sprintf(tag, "%02x",pObj->m_pAPIConn->m_SysData.systemfrx);
																					}
																				}

																				_ftime(&timestamp);
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<aconn>\
<config active=\"%d\">\
<server>%s</server>\
<client>%s</client>\
<port>N/A</port>\
<basis>%s</basis>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<server_time>%.03f</server_time>\
<server_status>%d</server_status>\
<server_lists>%d</server_lists>\
<server_changed>%d</server_changed>\
<server_last_update>%.03f</server_last_update>\
<message>%s</message>\
</status>\
</aconn>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pObj->m_pszServerName,
																					pObj->m_pszClientName,
																					(pObj->m_pAPIConn?tag:"N/A"),
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(pObj->m_dblLastServerTimeMS/1000.0),
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? -1 : (pObj->m_pAPIConn?pObj->m_pAPIConn->m_Status:-1)),
																					(pObj->m_pAPIConn?pObj->m_pAPIConn->m_SysData.systemlistcount:-1),
																					(pObj->m_pAPIConn?pObj->m_pAPIConn->m_SysData.syschanged:-1),
																					pObj->m_dblUpdateTime,
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? (((pObj->m_ulStatus&SENTINEL_ICON_MASK) == SENTINEL_STATUS_ERROR)?"connection error":"not connected"):"")

																																										
																				);
																					
																			

																				msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}

																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;

																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_psocketmodule->m_data.m_critConns);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);


															} break; //case SENTINEL_XML_TYPE_GETCONN

														case SENTINEL_XML_TYPE_GETLIST://				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_list\" code=\"%d\">", g_psocketmodule->m_data.m_nNumChannelObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																
EnterCriticalSection(&g_psocketmodule->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_psocketmodule->m_data.m_ppChannelObj)&&(g_psocketmodule->m_data.m_nNumChannelObjects))
																{

																	//let's see if we have any options.

																	char* pchServer = NULL;  //server name search
																	int nActive =-1;
																	int nID=-1;
																	int nListNum = -1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{

																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("id", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nID = atoi(pchTmp);
																								if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("list_number", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nListNum = atoi(pchTmp);
																								if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("active", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_psocketmodule->m_data.m_nNumChannelObjects)
																	{

																		if(g_psocketmodule->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CSocketModuleChannelObject* pObj = g_psocketmodule->m_data.m_ppChannelObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																			if((nID>0)&&(pObj->m_nChannelID != nID))  bInclude = false;
																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				_ftime(&timestamp);

																				tlistdata* ptlist = NULL;
																				if(pObj->m_pAPIConn)
																				{
																					ptlist = &(pObj->m_pAPIConn->m_ListData[pObj->m_nHarrisListID-1]);
																				}
																				
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<alist>\
<config active=\"%d\" id=\"%d\">\
<server>%s</server>\
<list_number>%d</list_number>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<list_state>%d</list_state>\
<list_changed>%d</list_changed>\
<list_display>%d</list_display>\
<list_syschange>%d</list_syschange>\
<list_count>%d</list_count>\
<list_lookahead>%d</list_lookahead>\
<list_last_update>%.3f</list_last_update>\
</status>\
</alist>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pObj->m_nChannelID,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(ptlist?ptlist->liststate:-1),
																					(ptlist?ptlist->listchanged:-1),
																					(ptlist?ptlist->listdisplay:-1),
																					(ptlist?ptlist->listsyschange:-1),
																					(ptlist?ptlist->listcount:-1),
																					(ptlist?ptlist->lookahead:-1),
																					pObj->m_dblUpdateTime
																																										
																				);
																					
																			

																				msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_LIST_MAXPAYLOAD, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}

																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;

																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_psocketmodule->m_data.m_critChannels);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case SENTINEL_XML_TYPE_GETLIST
														case SENTINEL_XML_TYPE_GETEVENT://			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
															{

																msg.ClearContent(CX_XML_BUFFER_MSG);
																int nGlobalCount =0;
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_psocketmodule->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_psocketmodule->m_data.m_ppChannelObj)&&(g_psocketmodule->m_data.m_nNumChannelObjects))
																{

																	//let's see if we have any options.

//																	char* pchServer = NULL;  //server name search
//																	int nActive =-1;
																	int nID=-1;
																	int nLimitNum = -1;
																	int nDone = -1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{
																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("id", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nID = atoi(pchTmp);
																								if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("limit", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nLimitNum = atoi(pchTmp);
																								if(nLimitNum <=0) nLimitNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<limit>%d</limit>", nLimitNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("done", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nDone = 0;
																								else if(strcmp("1", pchTmp)==0) nDone = 1;
																								else if(strcmp("yes", pchTmp)==0) nDone = 1;
																								else if(strcmp("no", pchTmp)==0) nDone = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<done>%d</done>", nDone);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}

																					/*
																					else
																					if(strcmp("list_number", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nListNum = atoi(pchTmp);
																								if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("active", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																					* /
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_psocketmodule->m_data.m_nNumChannelObjects)
																	{

																		if(g_psocketmodule->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CSocketModuleChannelObject* pObj = g_psocketmodule->m_data.m_ppChannelObj[m];

/*
																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

* /																		if(
																				//	(nID>0)&&  //commenting this out makes it mandatory!
																					(pObj->m_nChannelID != nID)
																				)  
																				bInclude = false;

//																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				// found a channel that works, now must loop thru the events, filtering if nec.

																				// need to allocate a big buffer and deal with directly, rather than allocating new for every event.
																			
/* // was this, which worked but slowly.  below is new
																				if(pObj->m_ppevents)
																				{
																					 
																					int q = 0;
																					int nCount = 0;
																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;
																						unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));

																						if(pObj->m_ppevents[q])
																						{
																							CSocketModuleEventObject* pEObj = pObj->m_ppevents[q];

																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;
																							if(bInclude)
																							{

																								_ftime(&timestamp);

																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
																								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																									"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_usControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CSocketModuleEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																								);
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																								msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );

																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
* /




																				char* pchPayload = (char*)malloc(SENTINEL_XML_EVENT_MAXPAYLOAD);
																				char* pchAppend = pchPayload;

																				// re-use errorstring, why not.
																		//		int nTimes[10] = {0,0,0,0,0,0,0,0,0,0};
																				int nClock = clock();
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "start event payload assembly");  //(Dispatch message)

																				if((pObj->m_ppevents)&&(pchPayload))
																				{
																					unsigned long ulBufferLen = SENTINEL_XML_EVENT_MAXPAYLOAD;
																					char* pchBufferEnd = (pchPayload+SENTINEL_XML_EVENT_MAXPAYLOAD);
																					int nEventLen = 0;
																					unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));
																					int q = 0;
																					int nCount = 0;
																					int nBuffers = 1;
																					int nTempOffset;
																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;

																						if(pObj->m_ppevents[q])
																						{
																					//		nTimes[0] += (clock() - nClock); nClock = clock(); 

																							CSocketModuleEventObject* pEObj = pObj->m_ppevents[q];
																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;

																				//			nTimes[1] += (clock() - nClock); nClock = clock(); 
																							if(bInclude)
																							{

																							//	_ftime(&timestamp);
																					//		nTimes[2] += (clock() - nClock); nClock = clock(); 

																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
																					//		nTimes[3] += (clock() - nClock); nClock = clock(); 
																								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, 
																									"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_usControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CSocketModuleEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																								);
																					//		nTimes[4] += (clock() - nClock); nClock = clock(); 
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																						//	nTimes[5] += (clock() - nClock); nClock = clock(); 
																								nEventLen = strlen(errorstring)+1;//+1 for term zero

																								if(pchAppend+nEventLen>pchBufferEnd)  // if must be changed to while if SENTINEL_XML_EVENT_MAXPAYLOAD defined < MAX_MESSAGE_LENGTH (buffer vs errorstring)
																								{
																									// need to realloc
																									nBuffers++;
																									ulBufferLen = SENTINEL_XML_EVENT_MAXPAYLOAD*nBuffers;
																									char* pchBufferNew = (char*)malloc(ulBufferLen);
																									if(pchBufferNew)
																									{
																										nTempOffset = pchAppend+1 - pchPayload;
																										memcpy(pchBufferNew, pchPayload, nTempOffset);
																										try {free(pchPayload);} catch(...){}
																										pchPayload = pchBufferNew;
																										pchBufferEnd = (pchPayload+ulBufferLen);
																										pchAppend = pchPayload+nTempOffset-1;																									

																									}
																								}
																								

																								// append
																								memcpy(pchAppend, errorstring, nEventLen);
																								pchAppend += nEventLen-1; // append over the term 0 next time.


																						//	nTimes[6] += (clock() - nClock); nClock = clock(); 


/*
																								msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
* /
																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
																				msg.ClearContent(CX_XML_BUFFER_DATA);
																				if(pchPayload)
																				{
																					msg.m_pchResponse[CX_XML_BUFFER_DATA] = pchPayload;		
																					ulDataLen = pchAppend - pchPayload;
																				}
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "end event payload assembly");  //(Dispatch message)

																			}

																		}
																		m++;
																	}

/*																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;
* /
																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_psocketmodule->m_data.m_critChannels);

/* // not needed now, we tally the buffer as we go
																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																* /
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_event\" code=\"%d\">", nGlobalCount);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case SENTINEL_XML_TYPE_GETEVENT

// end SocketModule specific XML commands
////////////////////////////////////////////////////////
*/
														case CX_XML_TYPE_UNK://						0 // unknown
														default:
															{
																sprintf(errorstring, "Unsupported XML command: [%s]", (msg.m_pchType?msg.m_pchType:"") );
																g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, errorstring);
																//let's try to send a nak.

																msg.m_nType = CX_XML_TYPE_UNK;
																msg.m_nFailure=CX_XML_ERR_BADCMD;
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"%s\" code=\"%d\">", (msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Unsupported XML command [%s] (error %d)</msg>",(msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

															} break; //case CX_XML_TYPE_UNK://						0 // unknown
														}
															
														
														if(!bPersist) bCloseCommand = true;

													}
													else // no child nodes to cortex - can't identify command.
													{
														bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
														sprintf(errorstring, "Cortex XML format error: [%s]", pchXML );
														g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, errorstring);
														//let's try to send a nak.

														msg.m_nType = CX_XML_TYPE_UNK;
														msg.m_nFailure=CX_XML_ERR_NOCMD;
														msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
														msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Cortex XML format error %d</msg>", msg.m_nFailure);
														msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

													}
												}
											}
										}
									}
									else
									{
										// no child nodes, can't do nuttin.
										bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
										sprintf(errorstring, "XML format error: [%s]", pchXML );
										g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, errorstring);
										//let's try to send a nak.

										msg.m_nType = CX_XML_TYPE_UNK;
										msg.m_nFailure=CX_XML_ERR_NOCHILD;
										msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
										msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML format error %d</msg>", msg.m_nFailure);
										msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

									}
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "end xml parse");  //(Dispatch message)

									n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }
								}

								if(!bResponseSent)
								{
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "begin send response");  //(Dispatch message)

								// send it
		_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_psocketmodule)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending response message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, szSocketModuleSource, errorstring);  //(Dispatch message)
				g_psocketmodule->SendMsg(CX_SENDMSG_ERROR, szSocketModuleSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "end send response");  //(Dispatch message)

								}

								// log it

								//debug file write
								if((g_psocketmodule)&&(g_psocketmodule->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
								{
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "begin log response");  //(Dispatch message)
									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_psocketmodule->m_settings.m_pszName?g_psocketmodule->m_settings.m_pszName:"SocketModule"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lasttxfilename, filename)==0)
									{
										nTxDupes++;
									}
									else
									{
										nTxDupes=0;
										strcpy(lasttxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

									fp = fopen(filename, "wb");
									if(fp)
									{
										int b=CX_XML_BUFFER_CORTEXBEGIN;
										while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
										{
//											fwrite(filename, 1, strlen(filename),fp);
											if(msg.m_pchResponse[b])
											{
												if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

												fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
												fflush(fp);
											}
											b++;

										}
										fclose(fp);
										fp = NULL;
									}
if(g_psocketmodule->m_settings.m_ulDebug&SOCKETMODULE_DEBUG_COMM) 
	g_psocketmodule->m_msgr.DM(MSG_ICONHAND, NULL, szSocketModuleSource, "end log response");  //(Dispatch message)
								}

//AfxMessageBox("001");
								int b=CX_XML_BUFFER_TX;
								while(b<CX_XML_BUFFER_DATAEND)
								{
									if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
									{
										try { free(msg.m_pchResponse[b]); } catch(...){}
										msg.m_pchResponse[b] = NULL;
									}
									b++;
								}

//AfxMessageBox("002");
								if(pucUUID)
								{
									try { RpcStringFree(&pucUUID); } catch(...){}
								}
								pucUUID = NULL;

//AfxMessageBox("003");
								if(pchXML) 
								{
									try { free(pchXML); } catch(...){}
								}
								pchXML = NULL;
//AfxMessageBox("004");

								if(pchRXID) 
								{
									try { free(pchRXID); } catch(...){}
								}
								pchRXID = NULL;

//AfxMessageBox("005");
								if (pchXMLStream) 
									pchEnd = strstr(pchXMLStream, "</cortex>");
								else pchEnd = NULL;
//AfxMessageBox("006");
							}  // while </cortex> exists.
						}//if(pchXMLStream)
						// dont do a free(pch);  
					}  // else out of mem, so just skip
					else
					{
						// out of mem.. .log error?
					}

				} //if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))

			} // if getline succeeds.
			else
			{
				// here, we either have timed out because theres no data, or, the connection has been lost.
				// if we lost the conn, no need to re-establish, we are a client handler, let the client reconnect if they want.
				if(nReturnCode == NET_ERROR_CONN)		// connection lost
				{
					ulRetry++;

					if(g_psocketmodule)
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH, "Lost connection from %d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno );

						g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, errorstring);  //(Dispatch message)
						g_psocketmodule->SendMsg(CX_SENDMSG_INFO, szSocketModuleSource, errorstring);
					}

					break;  // break out of while loop, closes connection, starts again
				}
				else
				{
//									g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, szSocketModuleSource, "check %d", clock());  //(Dispatch message)

					// we prob just havent gotten a message because no data.  
					// so, check to see if its time for publication service
									// no, do this in the stall loop


				}

			} // end of else from if(nReturnCode == NET_SUCCESS)

			// and lets check if we've timed out on the connection, no matter what the success was
			_ftime( &timestamp );
			if(
				  (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				&&(ulConnTimeout>0)
				&&(ulConnLastMessage>0)
				&&((ulConnLastMessage + ulConnTimeout) < (unsigned long)timestamp.time )
				)
			{
				// we timed out, need to disconnect
				ulRetry++;


				sprintf(errorstring, "Connection from %d.%d.%d.%d timed out.", 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno);

				if(g_psocketmodule)
				{
					g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, errorstring);  //(Dispatch message)
					g_psocketmodule->SendMsg(CX_SENDMSG_INFO, szSocketModuleSource, errorstring);
				}

				if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;} //free this memory before breaking out of the while.

				break;  // break out of while loop, closes connection, starts again
			}

			if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;}
			
			if(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) Sleep(1);  // dont peg processor




/*
			// following line fills out the data structure
//			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);
			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "SocketModule:XMLHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "SocketModule:XMLHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "SocketModule:XMLHandlerThread");
						}
					}
				}
			}
*/
			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "SocketModule:XMLHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
//						g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, szSocketModuleSource, "check %d", clock());  //(Dispatch message)


						if(nPeriodic>0)
						{
							_ftime( &timestamp );
							if((timeperiodic.time + nPeriodic)< timestamp.time)
							{
								// send asynchronous message

								//create a response UUID
								UUID uuida;
								UuidCreate(&uuida); // could check response code to be RPC_S_OK ?

								unsigned char* pucUUIDa = NULL;
								UuidToString(&uuida, &pucUUIDa);

								msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUIDa);

								if(pucUUIDa)
								{
									try { RpcStringFree(&pucUUIDa); } catch(...){}
								}
								pucUUIDa = NULL;


								msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"system\" code=\"0\">");

								msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
								msg.ClearContent(CX_XML_BUFFER_FLAGS);

								unsigned long ulDataLen = 0;
								char* pchStatus = g_psocketmodule->m_data.GetStatusText(&ulDataLen);
								if(pchStatus)
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0x%08x</code><text>%s</text></status><time>%d.%03d</time></system>",
										ulDataLen,
										pchStatus,
										timestamp.time, timestamp.millitm
										);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0xffffffff</code><text>unavailable</text></status><time>%d.%03d</time></system>",
										timestamp.time, timestamp.millitm
										);

								}

								if(pchStatus){try { free(pchStatus); } catch(...){} pchStatus=NULL;}

								ulDataLen = 0;
								if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
								{
									ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
								}
								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
									ulDataLen,
									net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
									);

								msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>%s status message</msg>", 
									(g_psocketmodule->m_settings.m_pszName?g_psocketmodule->m_settings.m_pszName:"SocketModule"));

								msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


								// now, send and log.
	////////////////////////////////////////////// code copied from above.  if modified, must be 2 places.  sorry...
									// send it
			_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_psocketmodule)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending system message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, szSocketModuleSource, errorstring);  //(Dispatch message)
				g_psocketmodule->SendMsg(CX_SENDMSG_ERROR, szSocketModuleSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
								
	//AfxMessageBox("oop");
									// log it

									//debug file write
									if((g_psocketmodule)&&(g_psocketmodule->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
									{
										strcpy(filename, "Logs");
										_mkdir(filename);  // if exists already np
										strcat(filename, "\\");
										sprintf(errorstring, "%d.%d.%d.%d", 
											pClient->m_si.sin_addr.s_net, 
											pClient->m_si.sin_addr.s_host, 
											pClient->m_si.sin_addr.s_lh, 
											pClient->m_si.sin_addr.s_impno);

										strcat(filename, errorstring);

										_mkdir(filename);  // if exists already np

		//								_ftime( &timestamp );  // use timestamp we just had..

										tm* theTime = localtime( &timestamp.time	);

										sprintf(errorstring, "\\%s_", 
											(g_psocketmodule->m_settings.m_pszName?g_psocketmodule->m_settings.m_pszName:"SocketModule"));

										strcat(filename, errorstring);

										char filenametemp[MAX_PATH];
										strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

										strcat(filename, filenametemp);

										int nOffset = strlen(filename);
										sprintf(filename+nOffset,"%03d",timestamp.millitm);

										if (strcmp(lasttxfilename, filename)==0)
										{
											nTxDupes++;
										}
										else
										{
											nTxDupes=0;
											strcpy(lasttxfilename, filename);
										}
										nOffset = strlen(filename);
										sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

										fp = fopen(filename, "wb");
										if(fp)
										{
											int b=CX_XML_BUFFER_CORTEXBEGIN;
											while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
											{
	//											fwrite(filename, 1, strlen(filename),fp);
												if(msg.m_pchResponse[b])
												{
													if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

													fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
													fflush(fp);
												}
												b++;

											}
											fclose(fp);
											fp = NULL;
										}
									}


									b=CX_XML_BUFFER_TX;
									while(b<CX_XML_BUFFER_DATAEND)
									{
										if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
										{
											try { free(msg.m_pchResponse[b]); } catch(...){}
											msg.m_pchResponse[b] = NULL;
										}
										b++;
									}
	/////////////////////////////////////////////
							}

						}

						//and check timeout
						if(nTimed>0)
						{
							_ftime( &timestamp );
							if((timeactive.time + nTimed) < timestamp.time)
							{
								//kill it

								bCloseCommand = true;
							}
							
						}


						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} 

cleanup:
//		AfxMessageBox("cleanup");
//AfxMessageBox("010");

//		pDoc->Release();  // destructor calls this so don't do it here. 
		if(pSafety) pSafety->Release();

//AfxMessageBox("011");

// cleanup:  //was here but want to uninit com etc
//AfxMessageBox("012");

		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d closed.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);

//AfxMessageBox("013");
		if(g_psocketmodule)
		{
			g_psocketmodule->m_msgr.DM(MSG_ICONINFO, NULL, szSocketModuleSource, errorstring);  //(Dispatch message)
			g_psocketmodule->SendMsg(CX_SENDMSG_INFO, szSocketModuleSource, errorstring);
		}

//AfxMessageBox("014");

		if(pch!=NULL) { try{free(pch);} catch(...) {} } // must free the data buffer 

//AfxMessageBox("015");

		shutdown(pClient->m_socket, SD_BOTH);
//AfxMessageBox("016");
		closesocket(pClient->m_socket);
//AfxMessageBox("017");
		(*(pClient->m_pulConnections))--;
//AfxMessageBox("018");

	}
//		AfxMessageBox("end cleanup");
//AfxMessageBox("019");

	try
	{  
		CoUninitialize(); //XML
	}
	catch(...)
	{
		if(g_psocketmodule) g_psocketmodule->m_msgr.DM(MSG_ICONERROR, NULL, szSocketModuleSource, "Exception in CoUninitialize");  //(Dispatch message)
	}

//AfxMessageBox("020");

	try
	{
		delete pClient; // was created with new in the thread that spawned this one.
	}	catch(...) {}

//AfxMessageBox("021");
	if (bSetGlobalKill)	//g_bKillThread = true;
		((CSocketModuleHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}

#endif //#ifdef USE LISTENERS_AND_MESSAGING



