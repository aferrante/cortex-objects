#if !defined(AFX_SOCKETMODULELICENSEDLG_H__A59906B9_30D9_4946_82F0_96CA69B65858__INCLUDED_)
#define AFX_SOCKETMODULELICENSEDLG_H__A59906B9_30D9_4946_82F0_96CA69B65858__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SocketModuleLicenseDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSocketModuleLicenseDlg dialog

class CSocketModuleLicenseDlg : public CDialog
{
// Construction
public:
	CSocketModuleLicenseDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSocketModuleLicenseDlg)
	enum { IDD = IDD_LICENSE_DIALOG };
	CString	m_szKey;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSocketModuleLicenseDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSocketModuleLicenseDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SOCKETMODULELICENSEDLG_H__A59906B9_30D9_4946_82F0_96CA69B65858__INCLUDED_)
