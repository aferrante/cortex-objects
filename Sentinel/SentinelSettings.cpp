// SentinelSettings.cpp: implementation of the CSentinelSettings.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <process.h>

#include "SentinelDefines.h"
#include "SentinelSettings.h"
#include "SentinelMain.h" 
#include "../../Common/FILE/DirUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CSentinelMain* g_psentinel;
extern CADC g_adc;
extern void SentinelWorkingSetMaintainThread(void* pvArgs);
extern void SentinelTrafficWatchThread(void* pvArgs);
extern void SentinelControlModuleServiceThread(void* pvArgs);

extern bool g_bKillThread;
extern CSentinelApp theApp;
extern CDirUtil	g_dir;				// watch folder utilities.



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSentinelSettings::CSentinelSettings()
{
	m_pdb = NULL;
	m_pdbConn = NULL;

	m_pszName = NULL;
	m_pszType = NULL;  // familiar name of the type.
	m_pszProject = NULL;  // familiar name of the project name.
	m_ulMainMode = SENTINEL_MODE_DEFAULT;

	// ports
	m_usCommandPort	= SENTINEL_PORT_CMD;
	m_usStatusPort	= SENTINEL_PORT_STATUS;

	m_nThreadDwellMS = 1000;

	// messaging for Sentinel
	m_bUseLog = true;			// write a log file
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host
	m_pszFileSpec = NULL;
	m_pszMailSpec = NULL;
	m_pszProcessedFileSpec = NULL;
	m_pszProcessedMailSpec = NULL;
	m_bUseXMLClientLog = false;
	m_bMillisecondMessaging=true;			// use millisecond resolution for messages and asrun


	// Harris API
	m_bUseListCount = false; // get all events up until the list count (otherwise just up to the lookahead)
	m_ulEventsPerRequest=0;  // number of events to request at one time, up until lookahead or listcount (chunking for multilists, 0= use full count at once)
	m_nEventTimeToleranceMS = 500;
	m_bCommentUsesPrimaryStatus= true; // secondary events like comments, app flag, compile ID, etc, get assigned the status of the primary.
	m_bUseTitleMatch = true; // defaulted to true because of comment usage.  false; // use title field to match unique IDs
	m_nSecondaryEventMatchMode = 1;  // 1= use offset times, 0 = use absolute times

	// DSN params
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;
	m_pszDatabase = NULL;

	m_pszSettings = NULL;  // the Settings table name
	m_pszExchange = NULL;  // the Exchange table name
	m_pszMessages = NULL;  // the Messages table name
	m_pszAsRun = NULL;

	m_nAutoPurgeMessageDays = 30;
	m_nAutoPurgeAsRunDays = 30;

	m_pszChannels = NULL;  // the Channels table name
	m_pszConnections = NULL;  // the Connections table name
	m_pszLiveEvents = NULL; // the LiveEvents table name
	m_pszLiveEventsTemp = NULL;
	m_pszClipKeyMetadataView=NULL;  // the view of the clip metadata table including key field.
	m_pszWorkingSet=NULL;  // the working set table name
	m_bUseWorkingSet = FALSE;
//	m_pszControlModules=NULL;  // the control modules table name - DLLs that get commanded by changes in Sentinel
//	m_pszInterpreterModules=NULL;  // the interpreter modules table name - DLLs that accept automation data from an external source
	m_pszExternalModules=NULL;  // the external modules table name - all DLLs!  we put it in one table now.

	m_bUseRemoteChannels=false;			// use remote channels and virtual mapping
	m_pszRemoteChannels=NULL;  // the RemoteChannels table name
	m_pszVirtualMappingView=NULL;  // the VirtualMapping View name


	m_pszRemoteSentinelDescription=NULL;  // the Remote Sentinel Description
	m_pszRemoteSentinelHost=NULL;  // the Remote Sentinel Host name
	m_nRemoteSentinelControlPort=SENTINEL_PORT_CMD;  // the Remote Sentinel Control port
	m_nRemoteSentinelXMLPort=SENTINEL_PORT_STATUS;  // the Remote Sentinel XML port


	m_ulModuleEndQueryIntervalMS = 100;  // interval on which to query end on module

	m_ulModsIntervalMS = 6000;
	m_ulServerStatusDBIntervalMS = 1000; // 1 second updates
	m_bListStatusToDB=false;  // push list status to db
	m_bListStatusInExchange = false;  // old way, in exchange table. 
	m_bStatusInTables = false;  // new way, in Connections (server status) and Channels (list status) table.
	m_bDelayStatusForEvents = true;  // do not update teh liast status in database until events have been done 
	m_bDisableEventMatchOnPosition = false;  // if true, always uses the time threshold for matching
	m_bDisableTopListPurge = true;  // if true, parses whole list before deleting done events - default to true because trouble with matchbacks on top events casues purge of top part of list - not sure fixable
	m_bDoConfirmGet = false;  // if true, after changes, gets once more just to confirm.
	m_bMultiConnectSQL = false;  // if true, uses multiple SQL connections instead of just one.
	m_nMaxDeleteItems = 10;  // max number of items to delete in a single SQL statement
	m_nConfirmGetIntervalMS = 1000;
	m_bDisconnectOnSuspend = true;
	m_nDefaultPackCount=3; // to unlock list when the unlock info is not available
	m_bClearListOnDisconnect=true; // if disconnected, clear out old list immediately

	m_bUseControlModules=false;			// use control modules
	m_bUseInterpreterModules=false;			// use interpreter modules

	m_pszEventKeyFormat =  NULL;

	m_bDebugLists= false;  // if true, dumps the temp and events databases
	m_nDebugListTopCount=-1;  //if positive, just prints out the top n recored in the events tables.
	m_bDebugSQL=false;  // if true, prints out the SQL calls
	m_bDebugInsertSQL=false;  // if true, prints out the event insert SQL calls
	m_ulDebug = 0;
	m_pszDebugOrder = NULL;

	m_ulConnTimeout=300;	// when the connection has received no update for number of seconds, the connection is restarted 
	m_ulConnectionRetries = LONG_MAX-1;		// how many times to try to re-establish ULONG_MAX-1 is 4085 years with 30 second - so half that interval!
	m_ulConnectionInterval = 30;	// how long to wait in seconds before retrying

	m_nMaxLookahead = 0;
	m_nMaxAutomationBufferMS = 0;  // number of milliseconds after an automation change to wait for further changes.
	m_nMaxAutomationForceMS = 0;  // maximum number of milliseconds after an automation change to trigger changes. (prevents a situation where continual automation changes keep extending buffer time forever)
	m_bDoNotBreakOnChange = false;
	m_bPositionShiftOnChange = false;  // if a break on change, and the positions of all gotten events are shifted consistently, shift all the rest of the ones before breaking out.
	m_bGetOnListSizeChangeOnly = false;  // ignore other list changes for getting events.


	m_pszLicense=NULL;  // the License Key
	m_pszIconPath=NULL;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

	// traffic file related
	m_bUseTraffic=false;  // use traffic files
	m_bTrafficInWorkingSet=false;  // use traffic file info in working set...
	m_pszTrafficBaseFolder=NULL;  // the watchfolder where the traffic files are kept.
	m_pszTrafficChannelMap=NULL;  // in the filename, how to map to a channel. (find token formula)
	m_pszTrafficDateMap=NULL;  // in the filename, how to map to a date.  (find token formula)
	m_bTrafficDateMapCaseInsensitive = true;

	m_nTrafficErrorInterval =30; // how many seconds to wait after an error before trying again
	m_nTrafficFolderInterval=60;  //seconds
	m_bTrafficAutoDeleteFiles=false;  // delete old traffic files
	m_bTrafficAutoBackupFiles=false;  // backup old traffic files
	m_nTrafficFileExpiry=1440;  // delete after this many minutes of the day of the file.
	m_pszTrafficBackupBaseFolder=NULL;  // the watchfolder where the traffic files are backed up.
	m_pszTrafficBackupFileMap=NULL;  // the the rest of the folder name and filename, how to map to a channel, etc.
	m_nTrafficThreadMax = -1;

	//traffic files must be in the following date range with respect to "now", old files get purged, too far files in the future are not considered.
	m_nTrafficFilesDoneDays = 3; // how many days to retain old traffic files and events
	m_nTrafficFileLookahead = 30; // how many days ahead to allow a file to be imported.
	m_pszTrafficFileAllowedExtensions = NULL;  // csv list of file extensions to search for schedules in.


	//grid related
	m_bUseGrid=false;  // use grid
	m_bGridInWorkingSet=false;  // use grid info in working set...

	m_pszTraffic=NULL;  // the traffic table name
	m_pszTrafficFiles = NULL;
	m_pszGrid=NULL;  // the grid table name

	m_bStubModeNetCalls = false;

	//XML
	m_bLocalClockToUnixtimesInXML = false;
	m_bUseChecksumInXML = false;
	m_nEventLimitInXML=-1;


	// CONTROL module settings.
	m_nCtrlModLookahead = 100; // 100 items from the top	
	m_nCtrlModMaxQueue = 1000; // 1000 gives you ten changes of 100.

}

CSentinelSettings::~CSentinelSettings()
{
	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_pszType) free(m_pszType); // must use malloc to allocate
	if(m_pszProject) free(m_pszProject); // must use malloc to allocate
	if(m_pszDSN) free(m_pszDSN); // must use malloc to allocate
	if(m_pszUser) free(m_pszUser); // must use malloc to allocate
	if(m_pszPW) free(m_pszPW); // must use malloc to allocate
	if(m_pszDatabase) free(m_pszDatabase); // must use malloc to allocate
	if(m_pszSettings) free(m_pszSettings); // must use malloc to allocate
	if(m_pszExchange) free(m_pszExchange); // must use malloc to allocate
	if(m_pszMessages) free(m_pszMessages); // must use malloc to allocate
	if(m_pszAsRun) free(m_pszAsRun); // must use malloc to allocate

	if(m_pszChannels) free(m_pszChannels); // must use malloc to allocate
	if(m_pszConnections) free(m_pszConnections); // must use malloc to allocate
	if(m_pszLiveEvents) free(m_pszLiveEvents); // must use malloc to allocate
	if(m_pszLiveEventsTemp) free(m_pszLiveEventsTemp); // must use malloc to allocate
	if(m_pszClipKeyMetadataView) free(m_pszClipKeyMetadataView); // must use malloc to allocate
	if(m_pszWorkingSet) free(m_pszWorkingSet); // must use malloc to allocate
//	if(m_pszControlModules) free(m_pszControlModules); // must use malloc to allocate
//	if(m_pszInterpreterModules) free(m_pszInterpreterModules); // must use malloc to allocate
	if(m_pszExternalModules) free(m_pszExternalModules); // must use malloc to allocate

	if(m_pszRemoteChannels) free(m_pszRemoteChannels); // must use malloc to allocate
	if(m_pszVirtualMappingView) free(m_pszVirtualMappingView); // must use malloc to allocate

	if(m_pszRemoteSentinelDescription) free(m_pszRemoteSentinelDescription); // must use malloc to allocate
	if(m_pszRemoteSentinelHost) free(m_pszRemoteSentinelHost); // must use malloc to allocate

	if(m_pszLicense) free(m_pszLicense); // must use malloc to allocate
	if(m_pszIconPath) free(m_pszIconPath); // must use malloc to allocate	
	if(m_pszEventKeyFormat) free(m_pszEventKeyFormat); // must use malloc to allocate
	if(m_pszDebugOrder) free(m_pszDebugOrder); // must use malloc to allocate
	if(m_pszFileSpec) free(m_pszFileSpec); // must use malloc to allocate	
	if(m_pszMailSpec) free(m_pszMailSpec); // must use malloc to allocate	
	if(m_pszProcessedFileSpec) free(m_pszProcessedFileSpec); // must use malloc to allocate	
	if(m_pszProcessedMailSpec) free(m_pszProcessedMailSpec); // must use malloc to allocate	

	if(m_pszTrafficBaseFolder) free(m_pszTrafficBaseFolder); // must use malloc to allocate
	if(m_pszTrafficChannelMap) free(m_pszTrafficChannelMap); // must use malloc to allocate
	if(m_pszTrafficDateMap) free(m_pszTrafficDateMap); // must use malloc to allocate
	if(m_pszTrafficBackupBaseFolder) free(m_pszTrafficBackupBaseFolder); // must use malloc to allocate
	if(m_pszTrafficBackupFileMap) free(m_pszTrafficBackupFileMap); // must use malloc to allocate
	if(m_pszTraffic) free(m_pszTraffic); // must use malloc to allocate
	if(m_pszTrafficFiles) free(m_pszTrafficFiles); // must use malloc to allocate
	if(m_pszTrafficFileAllowedExtensions) free(m_pszTrafficFileAllowedExtensions); // must use malloc to allocate

	
	if(m_pszGrid) free(m_pszGrid); // must use malloc to allocate

}

int CSentinelSettings::Settings(bool bRead)
{
	char pszFilename[MAX_PATH];
	strcpy(pszFilename, "");
//		AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, SENTINEL_SETTINGS_FILE_DEFAULT);  // cortex settings file

	CFileUtil file;
	// get settings.
	file.GetSettings(pszFilename, false); 

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
		// load up the values on the settings object
			m_pszName = file.GetIniString("Main", "Name", "Sentinel", m_pszName);
			m_pszType = file.GetIniString("Main", "Type", "Sentinel", m_pszType);
			m_pszProject = file.GetIniString("Main", "Project", "VDS", m_pszProject);
			m_nThreadDwellMS = file.GetIniInt("Main", "ThreadDwellMS", 1000);

			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);


			// recompile license key params
			if(g_psentinel->m_data.m_key.m_pszLicenseString) free(g_psentinel->m_data.m_key.m_pszLicenseString);
			g_psentinel->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(m_pszLicense)+1);
			if(g_psentinel->m_data.m_key.m_pszLicenseString)
			sprintf(g_psentinel->m_data.m_key.m_pszLicenseString, "%s", m_pszLicense);

			g_psentinel->m_data.m_key.InterpretKey();

			char errorstring[MAX_MESSAGE_LENGTH];
			if(g_psentinel->m_data.m_key.m_bValid)
			{
				unsigned long i=0;
				while(i<g_psentinel->m_data.m_key.m_ulNumParams)
				{
					if((g_psentinel->m_data.m_key.m_ppszParams)
						&&(g_psentinel->m_data.m_key.m_ppszValues)
						&&(g_psentinel->m_data.m_key.m_ppszParams[i])
						&&(g_psentinel->m_data.m_key.m_ppszValues[i]))
					{
						if(stricmp(g_psentinel->m_data.m_key.m_ppszParams[i], "max")==0)
						{
							g_psentinel->m_data.m_nMaxLicensedChannels = atoi(g_psentinel->m_data.m_key.m_ppszValues[i]);
						}
					}
					i++;
				}
			
				if(
						(
							(!g_psentinel->m_data.m_key.m_bExpires)
						||((g_psentinel->m_data.m_key.m_bExpires)&&(!g_psentinel->m_data.m_key.m_bExpired))
						||((g_psentinel->m_data.m_key.m_bExpires)&&(g_psentinel->m_data.m_key.m_bExpireForgiveness)&&(g_psentinel->m_data.m_key.m_ulExpiryDate+g_psentinel->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!g_psentinel->m_data.m_key.m_bMachineSpecific)
						||((g_psentinel->m_data.m_key.m_bMachineSpecific)&&(g_psentinel->m_data.m_key.m_bValidMAC))
						)
					)
				{
					// this overrides stuff so let's not let it do that.

										// let's just not override this in a green condition.  
										// comment it out.
//					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//					g_psentinel->m_data.SetStatusText(errorstring, SENTINEL_STATUS_OK);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
					g_psentinel->m_data.SetStatusText(errorstring, SENTINEL_STATUS_ERROR);
				}
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				g_psentinel->m_data.SetStatusText(errorstring, SENTINEL_STATUS_ERROR);
			}

			m_pszIconPath = file.GetIniString("FileServer", "IconPath", "C:\\Inetpub\\wwwroot\\Cortex\\sentinel\\images\\", m_pszIconPath);
			m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", SENTINEL_PORT_CMD);
			m_bStubModeNetCalls = file.GetIniInt("CommandServer", "StubMode", 0)?true:false;

			m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", SENTINEL_PORT_STATUS);

			m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;

			m_bUseXMLClientLog = file.GetIniInt("Messager", "UseXMLClientLog", 0)?true:false;
			m_bMillisecondMessaging = file.GetIniInt("Messager", "MillisecondMessaging", 1)?true:false;			// use millisecond resolution for messages and asrun


			m_bUseListCount = file.GetIniInt("HarrisAPI", "UseListCount", 0)?true:false; // get all events up until the list count (otherwise just up to the lookahead)
			m_ulEventsPerRequest = file.GetIniInt("HarrisAPI", "EventsPerRequest", 0); //  number of events to request at one time, up until lookahead or listcount (chunking for multilists, 0= use full count at once)
			m_nEventTimeToleranceMS  = file.GetIniInt("HarrisAPI", "EventTimeToleranceMS", 500);
			m_bCommentUsesPrimaryStatus  = file.GetIniInt("HarrisAPI", "CommentUsesPrimaryStatus", 1)?true:false; // secondary events like comments, app flag, compile ID, etc, get assigned the status of the primary.
			m_bUseTitleMatch = file.GetIniInt("HarrisAPI", "UseTitleMatch", 1)?true:false; // use title field to match unique IDs
			m_bDisconnectOnSuspend = file.GetIniInt("HarrisAPI", "DisconnectOnSuspend", 1)?true:false;
			m_bClearListOnDisconnect = file.GetIniInt("HarrisAPI", "ClearListOnDisconnect", 1)?true:false; // if disconnected, clear out old list immediately

			m_pszDSN = file.GetIniString("Database", "DSN", (m_pszName?m_pszName:"Sentinel"), m_pszDSN);
			m_pszUser = file.GetIniString("Database", "DBUser", "sa", m_pszUser);
			m_pszPW = file.GetIniString("Database", "DBPassword", "", m_pszPW);
			m_pszDatabase = file.GetIniString("Database", "DBDefault", (m_pszName?m_pszName:"Sentinel"), m_pszDatabase);
			m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings", m_pszSettings );  // the Settings table name
			m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange", m_pszExchange);  // the Exchange table name
			m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages", m_pszMessages);  // the Messages table name
			m_pszAsRun = file.GetIniString("Database", "AsRunTableName", "AsRun_Log", m_pszAsRun);  // the AsRun table name
			m_pszTraffic = file.GetIniString("Database", "TrafficTableName", "Traffic_Events", m_pszTraffic);  // the traffic table name
			m_pszTrafficFiles = file.GetIniString("Database", "TrafficFilesTableName", "Traffic_Files", m_pszTrafficFiles);  // the traffic files table name
			m_pszGrid = file.GetIniString("Database", "GridTableName", "Grid", m_pszGrid);  // the grid table name

			m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels", m_pszChannels);  // the Channels table name
			m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Connections", m_pszConnections);  // the Connections table name
			m_pszLiveEvents = file.GetIniString("Database", "LiveEventsTableName", "Events", m_pszLiveEvents);  // the LiveEvents table name
			m_pszLiveEventsTemp = file.GetIniString("Database", "TempLiveEventsTableName", "Events_Temp", m_pszLiveEventsTemp);  // the LiveEvents table name
			m_pszClipKeyMetadataView = file.GetIniString("Database", "ClipKeyMetadataView", "Clip_Metadata_With_Key_View", m_pszClipKeyMetadataView);  // the m_pszClipKeyMetadataView name
			m_pszWorkingSet = file.GetIniString("Database", "WorkingSet", "Working_Set", m_pszWorkingSet);  // the WorkingSet table name
//			m_pszControlModules = file.GetIniString("Database", "ControlModulesTableName", "Control_Modules", m_pszControlModules);  // the control modules table name - DLLs that get commanded by changes in Sentinel
//			m_pszInterpreterModules = file.GetIniString("Database", "InterpreterModulesTableName", "Interpreter_Modules", m_pszInterpreterModules);  // the interpreter modules table name - DLLs that accept automation data from an external source
			m_pszExternalModules = file.GetIniString("Database", "ExternalModulesTableName", "External_Modules", m_pszExternalModules);  // the external modules table name - all DLLs 


			m_bUseRemoteChannels=file.GetIniInt("Database", "UseRemoteChannels", 0)?true:false;		// use remote channels and virtual mapping
			m_pszRemoteChannels = file.GetIniString("Database", "RemoteChannelsTableName", "RemoteChannels", m_pszRemoteChannels);  // the RemoteChannels table name
			m_pszVirtualMappingView = file.GetIniString("Database", "VirtualMappingView", "VirtualMappingView", m_pszVirtualMappingView); // the VirtualMapping View name

			m_pszRemoteSentinelDescription=file.GetIniString("remote_sentinel", "remote_sentinel_desc", "Remote Sentinel", m_pszRemoteSentinelDescription);  // the Remote Sentinel Description
			m_pszRemoteSentinelHost=file.GetIniString("remote_sentinel", "remote_sentinel_host", "", m_pszRemoteSentinelHost);  // the Remote Sentinel Host name
			m_nRemoteSentinelControlPort=file.GetIniInt("remote_sentinel", "remote_sentinel_control_port", SENTINEL_PORT_CMD);  // the Remote Sentinel Control port
			m_nRemoteSentinelXMLPort=file.GetIniInt("remote_sentinel", "remote_sentinel_XML_port", SENTINEL_PORT_STATUS);  // the Remote Sentinel XML port

			m_nAutoPurgeMessageDays = file.GetIniInt("AutoPurge", "AutoPurgeMessageDays", 30);
			m_nAutoPurgeAsRunDays = file.GetIniInt("AutoPurge", "AutoPurgeAsRunDays", 30);
			m_nAutoPurgeInterval  = file.GetIniInt("AutoPurge", "AutoPurgeInterval", 300);  // 5 minutes should be enough.


			m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
			m_ulServerStatusDBIntervalMS = file.GetIniInt("Database", "ServerStatusDBIntervalMS", 5000);  // in milliseconds
			m_bListStatusToDB = file.GetIniInt("Database", "ListStatusToDB", 1)?true:false;
			m_bListStatusInExchange = file.GetIniInt("Database", "ListStatusInExchange", 0)?true:false;  // old way, in exchange table.
			m_bStatusInTables = file.GetIniInt("Database", "StatusInTables", 1)?true:false;// new way, in Connections (server status) and Channels (list status) table.
			m_bDelayStatusForEvents = file.GetIniInt("Database", "DelayStatusForEvents", 1)?true:false;
			m_bDisableEventMatchOnPosition = file.GetIniInt("Database", "DisableEventMatchOnPosition", 0)?true:false;
			m_bDisableTopListPurge = file.GetIniInt("Database", "DisableTopListPurge", 1)?true:false;  // if true, parses whole list before deleting done events
			m_bDoConfirmGet = file.GetIniInt("Database", "DoConfirmGet", 0)?true:false;
			m_bMultiConnectSQL = file.GetIniInt("Database", "MultiConnectSQL", 0)?true:false;
			m_bUseWorkingSet = file.GetIniInt("Database", "UseWorkingSet", 1)?true:false;


/*  //SetThreads called outside
			// allow the working set thread start check here, and also in getdatabase
			if((m_bUseWorkingSet)&&(!g_psentinel->m_data.m_bWorkingSetThreadStarted))
			{
				if(_beginthread(SentinelWorkingSetMaintainThread, 0, (void*)NULL)==-1)
				{
					//error.

					//**MSG
					g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set maintenance thread."); //(Dispatch message)

				}
				else
				{
					Sleep(100);  //let the thread start
				}
			}
*/

			m_nSecondaryEventMatchMode = file.GetIniInt("Database", "SecondaryEventMatchMode", 1);  // 1= use offset times, 0 = use absolute times
			m_nConfirmGetIntervalMS = file.GetIniInt("Database", "ConfirmGetIntervalMS", 0);  // time to wait before doing the confirm get

			m_bUseControlModules = file.GetIniInt("Modules", "UseControlModules", 0)?true:false;			// use control modules

/*  //SetThreads called outside
			
			// allow the control module service thread start check here, and also in getdatabase
			if(m_bUseControlModules)
			{
				if(!g_psentinel->m_data.m_bControlModuleServiceThreadStarted)
				{
					g_psentinel->m_data.m_bKillControlModuleServiceThread =false;

					if(_beginthread(SentinelControlModuleServiceThread, 0, (void*)NULL)==-1)
					{
						//error.

						//**MSG
						g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start control module service thread."); //(Dispatch message)

					}
					else
					{
						Sleep(100);  //let the thread start
					}

				}
			}
			else
			{
				g_psentinel->m_data.m_bKillControlModuleServiceThread =true;
			}
			*/

			m_bUseInterpreterModules = file.GetIniInt("Modules", "UseInterpreterModules", 0)?true:false;			// use interpreter modules
			m_ulModuleEndQueryIntervalMS = file.GetIniInt("Modules", "ModuleEndQueryIntervalMS",100);  // interval on which to query end on module
			// CONTROL module settings.
			m_nCtrlModLookahead  = file.GetIniInt("Modules", "CtrlModLookahead", 100); // 100 items from the top	
			m_nCtrlModMaxQueue   = file.GetIniInt("Modules", "CtrlModMaxQueue", 1000); // 1000 gives you ten changes of 100.

			m_nMaxDeleteItems = file.GetIniInt("Database", "MaxDeleteItems", 10);  // max number of items to delete in a single SQL statement
			
			m_pszEventKeyFormat = file.GetIniString("Database", "EventKeyFormat", "%i", m_pszEventKeyFormat);  
			
			m_bDebugLists = file.GetIniInt("Database", "DebugLists", 0)?true:false;  // if true, dumps the temp and events databases
			m_nDebugListTopCount = file.GetIniInt("Database", "DebugListTopCount", -1);  //if positive, just prints out the top n recored in the events tables.
			m_bDebugSQL = file.GetIniInt("Database", "DebugSQL", 0)?true:false;  // if true, prints out the SQL calls
			m_bDebugInsertSQL = file.GetIniInt("Database", "DebugInsertSQL", 0)?true:false;  // if true, prints out the event insert SQL calls
			m_ulDebug = file.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.
			m_pszDebugOrder = file.GetIniString("Database", "DebugOrder", "(case when parent_position < 0 then ((event_position)/1000.0) else (parent_calc_start + (event_position-parent_position)/1000.0) end)", m_pszDebugOrder); // the order by clause for debug

			m_ulConnTimeout=file.GetIniInt("HarrisAPI", "ConnectionTimeout", 300);	// (default value 5 mins) 
			m_ulConnectionRetries = file.GetIniInt("HarrisAPI", "ConnectionRetries", LONG_MAX-1);		// how many times to try to re-establish ULONG_MAX-1 is 4085 years with 30 second interval!
			m_ulConnectionInterval = file.GetIniInt("HarrisAPI", "ConnectionInterval", 30);	// how long to wait in seconds before 

			m_nMaxLookahead=file.GetIniInt("HarrisAPI", "MaxLookahead",0);
			m_nMaxAutomationBufferMS=file.GetIniInt("HarrisAPI", "MaxAutomationBufferMS",250);  // number of milliseconds after an automation change to wait for further changes.
			m_nMaxAutomationForceMS=file.GetIniInt("HarrisAPI", "MaxAutomationForceMS",1000);  // maximum number of milliseconds after an automation change to trigger changes. (prevents a situation where continual automation changes keep extending buffer time forever)
			m_bDoNotBreakOnChange=file.GetIniInt("HarrisAPI", "DoNotBreakOnChange", 1)?true:false;
			m_bPositionShiftOnChange=file.GetIniInt("HarrisAPI", "PositionShiftOnChange", 0)?true:false;
			m_bGetOnListSizeChangeOnly=file.GetIniInt("HarrisAPI", "GetOnListSizeChangeOnly", 0)?true:false;  // ignore other list changes for getting events.
			m_nDefaultPackCount=file.GetIniInt("HarrisAPI", "DefaultPackCount", 3); // to unlock list when the unlock info is not available

			// traffic file related
			m_bUseTraffic = file.GetIniInt("Traffic", "UseTraffic", 0)?true:false;  // use traffic files
			m_bTrafficInWorkingSet = file.GetIniInt("Traffic", "TrafficInWorkingSet", 0)?true:false;  // use traffic file info in working set...
			m_pszTrafficBaseFolder=file.GetIniString("Traffic", "TrafficBaseFolder", "", m_pszTrafficBaseFolder);  // the watchfolder where the traffic files are kept.

			m_pszTrafficChannelMap=file.GetIniString("Traffic", "TrafficChannelMap", "\\%D\\", m_pszTrafficChannelMap);  // in the filename, how to map to a channel. (find token formula)
			m_pszTrafficDateMap=file.GetIniString("Traffic", "TrafficDateMap", "\\%B %d.lst", m_pszTrafficDateMap);   // in the filename, how to map to a date.  (find token formula)

			m_nTrafficErrorInterval=file.GetIniInt("Traffic", "TrafficErrorInterval", 30);  // how many seconds to wait after an error before trying again
			m_nTrafficFolderInterval=file.GetIniInt("Traffic", "TrafficFolderInterval", 60);  //seconds
			m_bTrafficAutoDeleteFiles= file.GetIniInt("Traffic", "TrafficAutoDeleteFiles", 0)?true:false;  // delete old traffic files
			m_bTrafficAutoBackupFiles= file.GetIniInt("Traffic", "TrafficAutoBackupFiles", 0)?true:false;  // backup old traffic files
			m_nTrafficFileExpiry=file.GetIniInt("Traffic", "TrafficFileExpiry",1440);  // delete after this many minutes of the day of the file.
			m_pszTrafficBackupBaseFolder=file.GetIniString("Traffic", "TrafficBackupBaseFolder", "", m_pszTrafficBackupBaseFolder);  // the watchfolder where the traffic files are backed up.
			m_pszTrafficBackupFileMap=file.GetIniString("Traffic", "TrafficBackupFileMap", "", m_pszTrafficBackupFileMap);  // the the rest of the folder name and filename, how to map to a channel, etc.
			m_nTrafficThreadMax=file.GetIniInt("Traffic", "TrafficThreadMax", -1);  // cap simultaneous directory search threads
			//traffic files must be in the following date range with respect to "now", old files get purged, too far files in the future are not considered.
			m_nTrafficFilesDoneDays =file.GetIniInt("Traffic", "TrafficFilesDoneDays",3); // how many days to retain old traffic files and events
			m_nTrafficFileLookahead =file.GetIniInt("Traffic", "TrafficFileLookahead",30); // how many days ahead to allow a file to be imported.
			m_pszTrafficFileAllowedExtensions = file.GetIniString("Traffic", "TrafficFileAllowedExtensions", "lst", m_pszTrafficFileAllowedExtensions);  // csv list of file extensions to search for schedules in.
			m_bTrafficDateMapCaseInsensitive = file.GetIniInt("Traffic", "TrafficDateMapCaseInsensitive", 1)?true:false;  // case insensitivity on mapping


			m_bLocalClockToUnixtimesInXML = file.GetIniInt("XML", "LocalClockToUnixtimesInXML", 0)?true:false;
			m_bUseChecksumInXML = file.GetIniInt("XML", "UseChecksumInXML", 0)?true:false;
			m_nEventLimitInXML = file.GetIniInt("XML", "EventLimitInXML", -1);



			g_dir.m_nMaxDirThreads = m_nTrafficThreadMax;

			//grid related
			m_bUseGrid= file.GetIniInt("Grid", "UseGrid", 0)?true:false;  // use grid
			m_bGridInWorkingSet=file.GetIniInt("Grid", "GridInWorkingSet", 0)?true:false;  // use grid info in working set...


			m_pszFileSpec = file.GetIniString("Messager", "LogFileIni", "Logs\\Sentinel|YD||1|", m_pszFileSpec);
				// SMTP address[: override port (optional)] | distribution lists | from email |  subject spec | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
			m_pszMailSpec = file.GetIniString("Messager", "SMTPIni", "mail.server.com|f3:recipient@address.com|\"someone\"<from@here.com>|%P %T from %h|%h|C:\\sentinelsmtp.txt|1|1|0", m_pszMailSpec);
			if(m_pszFileSpec) 
			{
				if(m_pszProcessedFileSpec)
				{
					try{ free(m_pszProcessedFileSpec); } catch(...) {}
				}
				m_pszProcessedFileSpec = ProcessString(m_pszFileSpec, false);
			}
			
			if(m_pszMailSpec) 
			{
				if(m_pszProcessedMailSpec)
				{
					try{ free(m_pszProcessedMailSpec); } catch(...) {}
				}
				m_pszProcessedMailSpec = ProcessString(m_pszMailSpec, false);
			}
		}
		else //write
		{
			file.SetIniString("Main", "Name", m_pszName);
			file.SetIniString("Main", "Type", m_pszType);
			file.SetIniString("Main", "Project", m_pszProject);
			file.SetIniInt("Main", "ThreadDwellMS", m_nThreadDwellMS);

			file.SetIniString("License", "Key", m_pszLicense);

			// recompile license key params
			if(g_psentinel->m_data.m_key.m_pszLicenseString) free(g_psentinel->m_data.m_key.m_pszLicenseString);
			g_psentinel->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(m_pszLicense)+1);
			if(g_psentinel->m_data.m_key.m_pszLicenseString)
			sprintf(g_psentinel->m_data.m_key.m_pszLicenseString, "%s", m_pszLicense);

			g_psentinel->m_data.m_key.InterpretKey();

			char errorstring[MAX_MESSAGE_LENGTH];
			if(g_psentinel->m_data.m_key.m_bValid)
			{
				unsigned long i=0;
				while(i<g_psentinel->m_data.m_key.m_ulNumParams)
				{
					if((g_psentinel->m_data.m_key.m_ppszParams)
						&&(g_psentinel->m_data.m_key.m_ppszValues)
						&&(g_psentinel->m_data.m_key.m_ppszParams[i])
						&&(g_psentinel->m_data.m_key.m_ppszValues[i]))
					{
						if(stricmp(g_psentinel->m_data.m_key.m_ppszParams[i], "max")==0)
						{
							g_psentinel->m_data.m_nMaxLicensedChannels = atoi(g_psentinel->m_data.m_key.m_ppszValues[i]);
						}
					}
					i++;
				}
			
				if(
						(
							(!g_psentinel->m_data.m_key.m_bExpires)
						||((g_psentinel->m_data.m_key.m_bExpires)&&(!g_psentinel->m_data.m_key.m_bExpired))
						||((g_psentinel->m_data.m_key.m_bExpires)&&(g_psentinel->m_data.m_key.m_bExpireForgiveness)&&(g_psentinel->m_data.m_key.m_ulExpiryDate+g_psentinel->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!g_psentinel->m_data.m_key.m_bMachineSpecific)
						||((g_psentinel->m_data.m_key.m_bMachineSpecific)&&(g_psentinel->m_data.m_key.m_bValidMAC))
						)
					)
				{
					// this overrides stuff so let's not let it do that.

										// let's just not override this in a green condition.  
										// comment it out.
//					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//					g_psentinel->m_data.SetStatusText(errorstring, SENTINEL_STATUS_OK);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
					g_psentinel->m_data.SetStatusText(errorstring, SENTINEL_STATUS_ERROR);
				}
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				g_psentinel->m_data.SetStatusText(errorstring, SENTINEL_STATUS_ERROR);
			}


			file.SetIniString("FileServer", "IconPath", m_pszIconPath);
			file.SetIniInt("CommandServer", "ListenPort", m_usCommandPort);
			file.SetIniInt("CommandServer", "StubMode", m_bStubModeNetCalls?1:0);
			file.SetIniInt("StatusServer", "ListenPort", m_usStatusPort);

			file.SetIniInt("Messager", "UseEmail", m_bUseEmail?1:0);
			file.SetIniInt("Messager", "UseNet", m_bUseNetwork?1:0);
			file.SetIniInt("Messager", "UseLog", m_bUseLog?1:0);
			file.SetIniInt("Messager", "UseXMLClientLog", m_bUseXMLClientLog?1:0);
			file.SetIniInt("Messager", "MillisecondMessaging", m_bMillisecondMessaging?1:0);		// use millisecond resolution for messages and asrun

			file.SetIniString("Database", "DSN", m_pszDSN);
			file.SetIniString("Database", "DBUser", m_pszUser);
			file.SetIniString("Database", "DBPassword", m_pszPW);
			file.SetIniString("Database", "DBDefault", m_pszDatabase );
			file.SetIniString("Database", "SettingsTableName", m_pszSettings);  // the Settings table name
			file.SetIniString("Database", "ExchangeTableName", m_pszExchange);  // the Exchange table name
			file.SetIniString("Database", "MessagesTableName", m_pszMessages);  // the Messages table name
			file.SetIniString("Database", "AsRunTableName", m_pszAsRun);  // the AsRun table name
			file.SetIniString("Database", "TrafficTableName", m_pszTraffic);  // the traffic table name
			file.SetIniString("Database", "TrafficFilesTableName", m_pszTrafficFiles);  // the traffic files table name
			file.SetIniString("Database", "GridTableName", m_pszGrid);  // the grid table name
//			file.SetIniString("Database", "ControlModulesTableName", m_pszControlModules);  // the control modules table name - DLLs that get commanded by changes in Sentinel
//			file.SetIniString("Database", "InterpreterModulesTableName", m_pszInterpreterModules);  // the interpreter modules table name - DLLs that accept automation data from an external source
			file.SetIniString("Database", "ExternalModulesTableName", m_pszExternalModules);  // the external modules table name - all DLLs 


			file.SetIniInt("Database", "UseRemoteChannels", m_bUseRemoteChannels?1:0);		// use remote channels and virtual mapping
			file.SetIniString("Database", "RemoteChannelsTableName", m_pszRemoteChannels);  // the RemoteChannels table name
			file.SetIniString("Database", "VirtualMappingView", m_pszVirtualMappingView); // the VirtualMapping View name

			file.SetIniString("remote_sentinel", "remote_sentinel_desc", m_pszRemoteSentinelDescription);  // the Remote Sentinel Description
			file.SetIniString("remote_sentinel", "remote_sentinel_host", m_pszRemoteSentinelHost);  // the Remote Sentinel Host name
			file.SetIniInt("remote_sentinel", "remote_sentinel_control_port", m_nRemoteSentinelControlPort);  // the Remote Sentinel Control port
			file.SetIniInt("remote_sentinel", "remote_sentinel_XML_port", m_nRemoteSentinelXMLPort);  // the Remote Sentinel XML port


			file.SetIniInt("HarrisAPI", "UseListCount", m_bUseListCount?1:0); // get all events up until the list count (otherwise just up to the lookahead)
			file.SetIniInt("HarrisAPI", "EventsPerRequest", m_ulEventsPerRequest); //  number of events to request at one time, up until lookahead or listcount (chunking for multilists, 0= use full count at once)
			file.SetIniInt("HarrisAPI", "EventTimeToleranceMS", m_nEventTimeToleranceMS);
			file.SetIniInt("HarrisAPI", "CommentUsesPrimaryStatus", m_bCommentUsesPrimaryStatus?1:0); // secondary events like comments, app flag, compile ID, etc, get assigned the status of the primary.
			file.SetIniInt("HarrisAPI", "UseTitleMatch", m_bUseTitleMatch?1:0); // use title field to match unique IDs
			file.SetIniInt("HarrisAPI", "DisconnectOnSuspend", m_bDisconnectOnSuspend?1:0);
			file.SetIniInt("HarrisAPI", "ClearListOnDisconnect", m_bClearListOnDisconnect?1:0); // if disconnected, clear out old list immediately

			file.SetIniString("Database", "ChannelsTableName", m_pszChannels);  // the Channels table name
			file.SetIniString("Database", "ConnectionsTableName", m_pszConnections);  // the Connections table name
			file.SetIniString("Database", "LiveEventsTableName", m_pszLiveEvents);  // the LiveEvents table name
			file.SetIniString("Database", "TempLiveEventsTableName", m_pszLiveEventsTemp);  // the LiveEvents table name
			file.SetIniString("Database", "ClipKeyMetadataView", m_pszClipKeyMetadataView);  // the m_pszClipKeyMetadataView name
			file.SetIniString("Database", "WorkingSet", m_pszWorkingSet);  // the WorkingSet table name

			file.SetIniInt("Database", "ModificationCheckInterval", m_ulModsIntervalMS);  // in milliseconds
			file.SetIniInt("Database", "ServerStatusDBIntervalMS", m_ulServerStatusDBIntervalMS);  // in milliseconds
			file.SetIniInt("Database", "ListStatusToDB", m_bListStatusToDB?1:0);
			file.SetIniInt("Database", "ListStatusInExchange", m_bListStatusInExchange?1:0);  // old way, in exchange table.
			file.SetIniInt("Database", "StatusInTables", m_bStatusInTables?1:0);// new way, in Connections (server status) and Channels (list status) table.
			file.SetIniInt("Database", "DelayStatusForEvents", m_bDelayStatusForEvents?1:0);
			file.SetIniInt("Database", "DisableEventMatchOnPosition", m_bDisableEventMatchOnPosition?1:0);
			file.SetIniInt("Database", "DisableTopListPurge", m_bDisableTopListPurge?1:0);  // if true, parses whole list before deleting done events
			file.SetIniInt("Database", "DoConfirmGet", m_bDoConfirmGet?1:0);
			file.SetIniInt("Database", "SecondaryEventMatchMode", m_nSecondaryEventMatchMode);  // 1= use offset times, 0 = use absolute times
			file.SetIniInt("Database", "ConfirmGetIntervalMS", m_nConfirmGetIntervalMS);  // time to wait before doing the confirm get

			file.SetIniInt("Database", "MultiConnectSQL", m_bMultiConnectSQL?1:0);
			file.SetIniInt("Database", "MaxDeleteItems", m_nMaxDeleteItems);  // max number of items to delete in a single SQL statement
			file.SetIniString("Database", "EventKeyFormat", m_pszEventKeyFormat);  

			file.SetIniInt("Database", "DebugLists", m_bDebugLists?1:0);  // if true, dumps the temp and events databases
			file.SetIniInt("Database", "DebugListTopCount", m_nDebugListTopCount);  //if positive, just prints out the top n recored in the events tables.
			file.SetIniInt("Database", "DebugSQL", m_bDebugSQL?1:0);  // if true, prints out the SQL calls
			file.SetIniInt("Database", "DebugInsertSQL", m_bDebugInsertSQL?1:0);  // if true, prints out the event insert SQL calls
			file.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.
			file.SetIniString("Database", "DebugOrder", m_pszDebugOrder); // the order by clause for debug
			file.SetIniInt("Database", "UseWorkingSet", m_bUseWorkingSet?1:0);

			file.SetIniInt("Modules", "UseControlModules", m_bUseControlModules?1:0);			// use control modules
			file.SetIniInt("Modules", "UseInterpreterModules", m_bUseInterpreterModules?1:0);		// use interpreter modules
			file.SetIniInt("Modules", "ModuleEndQueryIntervalMS",m_ulModuleEndQueryIntervalMS);  // interval on which to query end on module
			file.SetIniInt("Modules", "CtrlModLookahead", m_nCtrlModLookahead);
			file.SetIniInt("Modules", "CtrlModMaxQueue", m_nCtrlModMaxQueue); 

			file.SetIniInt("HarrisAPI", "ConnectionTimeout", m_ulConnTimeout);	// (default value 2 mins) 
			file.SetIniInt("HarrisAPI", "ConnectionRetries", m_ulConnectionRetries);		// how many times to try to re-establish ULONG_MAX-1 is 4085 years with 30 second interval!
			file.SetIniInt("HarrisAPI", "ConnectionInterval", m_ulConnectionInterval);	// how long to wait in seconds before retrying

			file.SetIniInt("HarrisAPI", "MaxLookahead", m_nMaxLookahead);
			file.SetIniInt("HarrisAPI", "MaxAutomationBufferMS", m_nMaxAutomationBufferMS);  // number of milliseconds after an automation change to wait for further changes.
			file.SetIniInt("HarrisAPI", "MaxAutomationForceMS", m_nMaxAutomationForceMS);  // maximum number of milliseconds after an automation change to trigger changes. (prevents a situation where continual automation changes keep extending buffer time forever)
			file.SetIniInt("HarrisAPI", "DoNotBreakOnChange", m_bDoNotBreakOnChange?1:0);
			file.SetIniInt("HarrisAPI", "PositionShiftOnChange", m_bPositionShiftOnChange?1:0);
			file.SetIniInt("HarrisAPI", "GetOnListSizeChangeOnly", m_bGetOnListSizeChangeOnly?1:0);  // ignore other list changes for getting events.
			file.SetIniInt("HarrisAPI", "DefaultPackCount", m_nDefaultPackCount); // to unlock list when the unlock info is not available

			file.SetIniInt("AutoPurge", "AutoPurgeMessageDays", m_nAutoPurgeMessageDays);
			file.SetIniInt("AutoPurge", "AutoPurgeAsRunDays", m_nAutoPurgeAsRunDays);
			file.SetIniInt("AutoPurge", "AutoPurgeInterval", m_nAutoPurgeInterval);  // 5 minutes should be enough.

			// traffic file related
			file.SetIniInt("Traffic", "UseTraffic", m_bUseTraffic?1:0);  // use traffic files
			file.SetIniInt("Traffic", "TrafficInWorkingSet", m_bTrafficInWorkingSet?1:0);  // use traffic file info in working set...
			file.SetIniString("Traffic", "TrafficBaseFolder", m_pszTrafficBaseFolder);  // the watchfolder where the traffic files are kept.
			file.SetIniString("Traffic", "TrafficChannelMap", m_pszTrafficChannelMap);  // in the filename, how to map to a channel. (find token formula)
			file.SetIniString("Traffic", "TrafficDateMap", m_pszTrafficDateMap);   // in the filename, how to map to a date.  (find token formula)
			file.SetIniInt("Traffic", "TrafficErrorInterval", m_nTrafficErrorInterval);  // how many seconds to wait after an error before trying again
			file.SetIniInt("Traffic", "TrafficFolderInterval", m_nTrafficFolderInterval);  //seconds
			file.SetIniInt("Traffic", "TrafficAutoDeleteFiles", m_bTrafficAutoDeleteFiles?1:0);  // delete old traffic files
			file.SetIniInt("Traffic", "TrafficAutoBackupFiles", m_bTrafficAutoBackupFiles?1:0);  // backup old traffic files
			file.SetIniInt("Traffic", "TrafficFileExpiry",m_nTrafficFileExpiry);  // delete after this many minutes of the day of the file.
			file.SetIniString("Traffic", "TrafficBackupBaseFolder", m_pszTrafficBackupBaseFolder);  // the watchfolder where the traffic files are backed up.
			file.SetIniString("Traffic", "TrafficBackupFileMap", m_pszTrafficBackupFileMap);  // the the rest of the folder name and filename, how to map to a channel, etc.
			file.SetIniInt("Traffic", "TrafficThreadMax", m_nTrafficThreadMax);  // cap simultaneous directory search threads

			//traffic files must be in the following date range with respect to "now", old files get purged, too far files in the future are not considered.
			file.SetIniInt("Traffic", "TrafficFilesDoneDays",m_nTrafficFilesDoneDays); // how many days to retain old traffic files and events
			file.SetIniInt("Traffic", "TrafficFileLookahead",m_nTrafficFileLookahead); // how many days ahead to allow a file to be imported.
			file.SetIniString("Traffic", "TrafficFileAllowedExtensions", m_pszTrafficFileAllowedExtensions);  // csv list of file extensions to search for schedules in.


			//grid related
			file.SetIniInt("Grid", "UseGrid", m_bUseGrid?1:0);  // use grid
			file.SetIniInt("Grid", "GridInWorkingSet", m_bGridInWorkingSet?1:0);  // use grid info in working set...


			file.SetIniString("Messager", "LogFileIni", m_pszFileSpec);
			file.SetIniString("Messager", "SMTPIni", m_pszMailSpec);

			file.SetIniInt("XML", "LocalClockToUnixtimesInXML", m_bLocalClockToUnixtimesInXML?1:0);
			file.SetIniInt("XML", "UseChecksumInXML", m_bUseChecksumInXML?1:0);
			file.SetIniInt("XML", "EventLimitInXML", m_nEventLimitInXML);

			file.SetSettings(pszFilename, false);  // have to have correct filename

		}
		return SENTINEL_SUCCESS;
	}
	return SENTINEL_ERROR;
}


int CSentinelSettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	if((m_pszSettings)&&(strlen(m_pszSettings)>0)&&(m_pdbConn)&&(m_pdb)&&(m_pdbConn->m_bConnected)&&(Settings(true)==SENTINEL_SUCCESS))  //read has to succeed
	{
/*
		char pszFilename[MAX_PATH];

		strcpy(pszFilename, SENTINEL_SETTINGS_FILE_DEFAULT);  // sentinel settings file

		CFileUtil file;
		file.GetSettings(pszFilename, false); 
	// load up the values on the settings object
		if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
		{
			g_psentinel->m_settings.m_pszName = file.GetIniString("Main", "Name", "Sentinel");
			g_psentinel->m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");

			g_psentinel->m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");
			g_psentinel->m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", SENTINEL_PORT_CMD);
			g_psentinel->m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", SENTINEL_PORT_STATUS);

			g_psentinel->m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			g_psentinel->m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			g_psentinel->m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;

			g_psentinel->m_settings.m_bUseListCount = file.GetIniInt("HarrisAPI", "UseListCount", 0)?true:false; // get all events up until the list count (otherwise just up to the lookahead)
			g_psentinel->m_settings.m_ulEventsPerRequest = file.GetIniInt("HarrisAPI", "EventsPerRequest", 0); //  number of events to request at one time, up until lookahead or listcount (chunking for multilists, 0= use full count at once)

			g_psentinel->m_settings.m_pszDSN = file.GetIniString("Database", "DSN", g_psentinel->m_settings.m_pszName?g_psentinel->m_settings.m_pszName:"Sentinel");
			g_psentinel->m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
			g_psentinel->m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
			g_psentinel->m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
			g_psentinel->m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
			g_psentinel->m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name

			g_psentinel->m_settings.m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels");  // the Channels table name
			g_psentinel->m_settings.m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Connections");  // the Connections table name
			g_psentinel->m_settings.m_pszLiveEvents = file.GetIniString("Database", "LiveEventsTableName", "Events");  // the LiveEvents table name
			g_psentinel->m_settings.m_pszLiveEventsTemp = file.GetIniString("Database", "TempLiveEventsTableName", "Events_Temp");  // the LiveEvents table name

			g_psentinel->m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
			g_psentinel->m_settings.m_ulServerStatusDBIntervalMS = file.GetIniInt("Database", "ServerStatusDBIntervalMS", 5000);  // in milliseconds
			g_psentinel->m_settings.m_bListStatusToDB = file.GetIniInt("Database", "ListStatusToDB", 1)?true:false;
			
		}

*/
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = SENTINEL_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCategory="";
				CString szParameter="";
				CString szValue="";
				CString szTemp="";
				int min, max;
				bool bmin = false, bmax = false;
				try
				{
					prs->GetFieldValue("category", szCategory);  //HARDCODE
					prs->GetFieldValue("parameter", szParameter);  //HARDCODE
					prs->GetFieldValue("value", szValue);  //HARDCODE
					prs->GetFieldValue("min_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						min = atoi(szTemp);
						bmin = true;
					}
					prs->GetFieldValue("max_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						max = atoi(szTemp);
						bmax = true;
					}
				}
				catch( ... )
				{
				}

				int nLength = szValue.GetLength();
				if(szCategory.CompareNoCase("Main")==0)
				{
					if(szParameter.CompareNoCase("Name")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszName)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszName) free(m_pszName);
								m_pszName = pch;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("License")==0)
				{
					if(szParameter.CompareNoCase("Key")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszLicense)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLicense) free(m_pszLicense);
								m_pszLicense = pch;

								// recompile license key params
								if(g_psentinel->m_data.m_key.m_pszLicenseString) free(g_psentinel->m_data.m_key.m_pszLicenseString);
								g_psentinel->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
								if(g_psentinel->m_data.m_key.m_pszLicenseString)
								sprintf(g_psentinel->m_data.m_key.m_pszLicenseString, "%s", szValue);

								g_psentinel->m_data.m_key.InterpretKey();

								char errorstring[MAX_MESSAGE_LENGTH];
								if(g_psentinel->m_data.m_key.m_bValid)
								{
									unsigned long i=0;
									while(i<g_psentinel->m_data.m_key.m_ulNumParams)
									{
										if((g_psentinel->m_data.m_key.m_ppszParams)
											&&(g_psentinel->m_data.m_key.m_ppszValues)
											&&(g_psentinel->m_data.m_key.m_ppszParams[i])
											&&(g_psentinel->m_data.m_key.m_ppszValues[i]))
										{
											if(stricmp(g_psentinel->m_data.m_key.m_ppszParams[i], "max")==0)
											{
												g_psentinel->m_data.m_nMaxLicensedChannels = atoi(g_psentinel->m_data.m_key.m_ppszValues[i]);
											}
										}
										i++;
									}
								
									if(
											(
												(!g_psentinel->m_data.m_key.m_bExpires)
											||((g_psentinel->m_data.m_key.m_bExpires)&&(!g_psentinel->m_data.m_key.m_bExpired))
											||((g_psentinel->m_data.m_key.m_bExpires)&&(g_psentinel->m_data.m_key.m_bExpireForgiveness)&&(g_psentinel->m_data.m_key.m_ulExpiryDate+g_psentinel->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
											)
										&&(
												(!g_psentinel->m_data.m_key.m_bMachineSpecific)
											||((g_psentinel->m_data.m_key.m_bMachineSpecific)&&(g_psentinel->m_data.m_key.m_bValidMAC))
											)
										)
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
										g_psentinel->m_data.SetStatusText(errorstring, SENTINEL_STATUS_OK);
									}
									else
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
										g_psentinel->m_data.SetStatusText(errorstring, SENTINEL_STATUS_ERROR);
									}
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
									g_psentinel->m_data.SetStatusText(errorstring, SENTINEL_STATUS_ERROR);
								}

							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("CommandServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usCommandPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("StatusServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usStatusPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Messager")==0)
				{
					if(szParameter.CompareNoCase("UseEmail")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseEmail = true;
							else m_bUseEmail = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseNet")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseNetwork = true;
							else m_bUseNetwork = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseLog = true;
							else m_bUseLog = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseXMLClientLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseXMLClientLog = true;
							else m_bUseXMLClientLog = false;
						}
					}

				}
				else
				if(szCategory.CompareNoCase("Modules")==0)
				{

					if(szParameter.CompareNoCase("UseControlModules")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseControlModules = true;
							else m_bUseControlModules = false;
/*
							if(m_bUseControlModules)
							{
								if(!g_psentinel->m_data.m_bControlModuleServiceThreadStarted)
								{
									g_psentinel->m_data.m_bKillControlModuleServiceThread =false;

									if(_beginthread(SentinelControlModuleServiceThread, 0, (void*)NULL)==-1)
									{
										//error.

										//**MSG
										g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start control module service thread."); //(Dispatch message)

									}
									else
									{
										Sleep(100);  //let the thread start
									}

								}
							}
							else
							{
								g_psentinel->m_data.m_bKillControlModuleServiceThread =true;
							}
							*/
						}
					}
					else
					if(szParameter.CompareNoCase("UseInterpreterModules")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseInterpreterModules = true;
							else m_bUseInterpreterModules = false;
						}
					}
					else
					if(szParameter.CompareNoCase("CtrlModLookahead")==0)
					{
						if(nLength>0)
						{
							m_nCtrlModLookahead = atoi(szValue);
						}
					}
					else
					if(szParameter.CompareNoCase("CtrlModMaxQueue")==0)
					{
						if(nLength>0)
						{
							m_nCtrlModMaxQueue = atoi(szValue);
						}
					}

				}
				else
				if(szCategory.CompareNoCase("HarrisAPI")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("UseListCount")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseListCount = true;
							else m_bUseListCount = false;
						}
					}
					else
					if(szParameter.CompareNoCase("EventsPerRequest")==0)
					{
						if(nLength>0)
						{
							m_ulEventsPerRequest = atoi(szValue);
						}
					}
					else
					if(szParameter.CompareNoCase("EventTimeToleranceMS")==0)
					{
						if(nLength>0)
						{
							m_nEventTimeToleranceMS = atoi(szValue);
						}
					}
					else
					if(szParameter.CompareNoCase("ConnectionTimeout")==0)
					{
						if(nLength>0)
						{
							m_ulConnTimeout = atol(szValue);
							int i=0;
						}
					}
					else
					if(szParameter.CompareNoCase("ConnectionRetries")==0)
					{
						if(nLength>0)
						{
							m_ulConnectionRetries = atol(szValue);
							int i=0;
						}
					}
					else
					if(szParameter.CompareNoCase("ConnectionInterval")==0)
					{
						if(nLength>0)
						{
							m_ulConnectionInterval = atol(szValue);
							int i=0;
						}
					}
					else
					if(szParameter.CompareNoCase("DoNotBreakOnChange")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bDoNotBreakOnChange = true;
							else m_bDoNotBreakOnChange = false;
						}
					}
					else
					if(szParameter.CompareNoCase("PositionShiftOnChange")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bPositionShiftOnChange = true;
							else m_bPositionShiftOnChange = false;
						}
					}
					else
					if(szParameter.CompareNoCase("GetOnListSizeChangeOnly")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bGetOnListSizeChangeOnly = true;
							else m_bGetOnListSizeChangeOnly = false;
						}
					}
					else
					if(szParameter.CompareNoCase("MaxLookahead")==0)
					{
						if(nLength>0)
						{
							m_nMaxLookahead = atoi(szValue);
						}
					}
					else
					if(szParameter.CompareNoCase("MaxAutomationBufferMS")==0)
					{
						if(nLength>0)
						{
							m_nMaxAutomationBufferMS = atoi(szValue);
						}
					}
					else
					if(szParameter.CompareNoCase("MaxAutomationForceMS")==0)
					{
						if(nLength>0)
						{
							m_nMaxAutomationForceMS = atoi(szValue);
						}
					}


				}
				else
				if(szCategory.CompareNoCase("Traffic")==0)
				{
					if(szParameter.CompareNoCase("TrafficBaseFolder")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszTrafficBaseFolder)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);

								bool bBeginNewWatch = false;

								if(m_pszTrafficBaseFolder)
								{
									if(stricmp(m_pszTrafficBaseFolder, pch)) bBeginNewWatch=true;
								}
								else	bBeginNewWatch = true;
								

								if(bBeginNewWatch)
								{
									g_psentinel->m_data.m_bTrafficWatchWarningSent = false;
									char errorstring[MAX_MESSAGE_LENGTH];

									int nReturn = g_dir.EndWatch();
									g_psentinel->SendMsg(CX_SENDMSG_INFO, "Sentinel:EndWatch", "EndWatch: ending current watchfolder process on %s.", g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)"); 

									bool bBreak=false;
									while(!bBreak)
									{
										EnterCriticalSection(&g_dir.m_critChangeThreads);
										if(g_dir.m_ulChangeThreads<=0)
										{
											bBreak=true;
										}
										else
										{
											Sleep(10);
										}
										LeaveCriticalSection(&g_dir.m_critChangeThreads);
									}

									Sleep(500); // let the thread end

									if(m_pszTrafficBaseFolder) free(m_pszTrafficBaseFolder);
									m_pszTrafficBaseFolder = pch;

									nReturn = g_dir.SetWatchFolder(g_psentinel->m_settings.m_pszTrafficBaseFolder);
									if(nReturn<DIRUTIL_SUCCESS)
									{
										// error
										//TODO report
_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error setting watchfolder.");  
g_psentinel->m_data.SetStatusText(errorstring, SENTINEL_STATUS_ERROR);

										if(!g_psentinel->m_data.m_bTrafficWatchWarningSent)
										{
											g_psentinel->SendMsg(CX_SENDMSG_ERROR, "Sentinel:SetWatchFolder", "SetWatchFolder returned %d with %s", nReturn, g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)");
											g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:SetWatchFolder", "SetWatchFolder returned %d with %s", nReturn, g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)"); //  Sleep(100);//(Dispatch message)
					//					g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:SetWatchFolder", "SetWatchFolder returned %d with %s", nReturn, g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)");  //(Dispatch message)
										}
										g_psentinel->m_data.m_bTrafficWatchWarningSent = true;
									}
									else
									{
										g_psentinel->m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:SetWatchFolder", "SetWatchFolder succeeded with %s", g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)");  // Sleep(100);//(Dispatch message)

										nReturn = g_dir.BeginWatch( (unsigned long)g_psentinel->m_settings.m_nTrafficFolderInterval, true);
										if(nReturn<DIRUTIL_SUCCESS)
										{
											// error
											//TODO report
_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error beginning watchfolder process.");  
g_psentinel->m_data.SetStatusText(errorstring, SENTINEL_STATUS_ERROR);

											if(!g_psentinel->m_data.m_bTrafficWatchWarningSent)
											{
												g_psentinel->SendMsg(CX_SENDMSG_ERROR, "Sentinel:BeginWatch", "BeginWatch returned %d;  could not begin watchfolder thread on %s.", nReturn, g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)");
												g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:BeginWatch", "BeginWatch returned %d;  could not begin watchfolder thread on %s.", nReturn, g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)"); //  Sleep(100);//(Dispatch message)
						//						g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Direct:BeginWatch", "BeginWatch returned %d;  could not begin watchfolder thread on %s.", nReturn, g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)");  //(Dispatch message)
											}
											g_psentinel->m_data.m_bTrafficWatchWarningSent = true;
										}
										else
										{
											g_psentinel->SendMsg(CX_SENDMSG_INFO, "Sentinel:BeginWatch", "BeginWatch: began watch on %s.", g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)"); 
											g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:SetWatchFolder", "SetWatchFolder: began watch on %s.", g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)"); //  Sleep(100);//(Dispatch message)
					//						g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Direct:SetWatchFolder", "SetWatchFolder: began watch on %s", g_psentinel->m_settings.m_pszTrafficBaseFolder);  //(Dispatch message)

											g_psentinel->m_data.m_bTrafficWatchWarningSent = false; //resets
_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "watching folder");  
g_psentinel->m_data.SetStatusText(errorstring, SENTINEL_STATUS_OK);

											// subtract half the interval so that the watch folder checks for changes, staggered with us checking the results in the else below.
											g_psentinel->m_data.m_timeTrafficWatch.time -= g_psentinel->m_settings.m_nTrafficFolderInterval/2000; // second resolution is fine.
					/*						watchtime.millitm += (unsigned short)((g_psentinel->m_settings.m_ulWatchIntervalMS%1000)/2); // fractional second updates
											if(watchtime.millitm>999)
											{
												watchtime.time++;
												watchtime.millitm%=1000;
											}
					*/
										}
									}
								}
								else
								{
									if(m_pszTrafficBaseFolder) free(m_pszTrafficBaseFolder);
									m_pszTrafficBaseFolder = pch;

								}
							}
						}
					}
					else
					if(szParameter.CompareNoCase("TrafficFilesDoneDays")==0)
					{
						if(nLength>0)
						{
							m_nTrafficFilesDoneDays = atoi(szValue);
						}
					}
					else
					if(szParameter.CompareNoCase("TrafficFileLookahead")==0)
					{
						if(nLength>0)
						{
							m_nTrafficFileLookahead = atoi(szValue);
						}
					}
					else
					if(szParameter.CompareNoCase("TrafficFolderInterval")==0)
					{
						if(nLength>0)
						{
							m_nTrafficFolderInterval = atoi(szValue);
						}
					}
					else
					if(szParameter.CompareNoCase("TrafficErrorInterval")==0)
					{
						if(nLength>0)
						{
							m_nTrafficErrorInterval = atoi(szValue);
						}
					}
					else
					if(szParameter.CompareNoCase("TrafficFileAllowedExtensions")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszTrafficFileAllowedExtensions)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszTrafficFileAllowedExtensions) free(m_pszTrafficFileAllowedExtensions);
								m_pszTrafficFileAllowedExtensions = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("TrafficDateMapCaseInsensitive")==0)
					{
						if(nLength>0)
						{
							if(szValue.CompareNoCase("true")==0) m_bTrafficDateMapCaseInsensitive=true;
							else	m_bTrafficDateMapCaseInsensitive = ((atoi(szValue)>0)?true:false);
						}
					}

				}
				else


				if(szCategory.CompareNoCase("Database")==0)
				{
					// we are not going to allow DSN params to change via the DB.

					if(szParameter.CompareNoCase("EventKeyFormat")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszEventKeyFormat)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszEventKeyFormat) free(m_pszEventKeyFormat);
								m_pszEventKeyFormat = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("SettingsTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszSettings)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszSettings) free(m_pszSettings);
								m_pszSettings = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ExchangeTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszExchange)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszExchange) free(m_pszExchange);
								m_pszExchange = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("MessagesTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszMessages)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszMessages) free(m_pszMessages);
								m_pszMessages = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ChannelsTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszChannels)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszChannels) free(m_pszChannels);
								m_pszChannels = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ConnectionsTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszConnections)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszConnections) free(m_pszConnections);
								m_pszConnections = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("LiveEventsTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszLiveEvents)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLiveEvents) free(m_pszLiveEvents);
								m_pszLiveEvents = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ClipKeyMetadataView")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszClipKeyMetadataView)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszClipKeyMetadataView) free(m_pszClipKeyMetadataView);
								m_pszClipKeyMetadataView = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("UseWorkingSet")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseWorkingSet = true;
							else m_bUseWorkingSet = false;

/*  //SetThreads called outside
							if((m_bUseWorkingSet)&&(!g_psentinel->m_data.m_bWorkingSetThreadStarted))
							{
								if(_beginthread(SentinelWorkingSetMaintainThread, 0, (void*)NULL)==-1)
								{
									//error.

									//**MSG
									g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set maintenance thread."); //(Dispatch message)

								}
								else
								{
									Sleep(100);  //let the thread start
								}
							}
*/
						}
					}
					else
					if(szParameter.CompareNoCase("WorkingSet")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszWorkingSet)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszWorkingSet) free(m_pszWorkingSet);
								m_pszWorkingSet = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("TempLiveEventsTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszLiveEventsTemp)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLiveEventsTemp) free(m_pszLiveEventsTemp);
								m_pszLiveEventsTemp = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ExternalModulesTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszExternalModules)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszExternalModules) free(m_pszExternalModules);
								m_pszExternalModules = pch;
							}
						}
					}
/*					
					else
					if(szParameter.CompareNoCase("InterpreterModulesTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszInterpreterModules)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszInterpreterModules) free(m_pszInterpreterModules);
								m_pszInterpreterModules = pch;
							}
						}
					}
*/
					else
					if(szParameter.CompareNoCase("ModificationCheckInterval")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_ulModsIntervalMS = nLength;
						}
					}
					else
					if(szParameter.CompareNoCase("ServerStatusDBIntervalMS")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_ulServerStatusDBIntervalMS = nLength;
						}
					}
					else
					if(szParameter.CompareNoCase("ListStatusToDB")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bListStatusToDB = true;
							else m_bListStatusToDB = false;
						}
					}
					else
					if(szParameter.CompareNoCase("RemoteChannelsTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszRemoteChannels)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszRemoteChannels) free(m_pszRemoteChannels);
								m_pszRemoteChannels = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("VirtualMappingView")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszVirtualMappingView)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszVirtualMappingView) free(m_pszVirtualMappingView);
								m_pszVirtualMappingView = pch;
							}
						}
					}


				}
				else
				if(szCategory.CompareNoCase("remote_sentinel")==0)
				{
					if(szParameter.CompareNoCase("remote_sentinel_desc")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszRemoteSentinelDescription)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszRemoteSentinelDescription) free(m_pszRemoteSentinelDescription);
								m_pszRemoteSentinelDescription = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("remote_sentinel_host")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszRemoteSentinelHost)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszRemoteSentinelHost) free(m_pszRemoteSentinelHost);
								m_pszRemoteSentinelHost = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("remote_sentinel_control_port")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_nRemoteSentinelControlPort = nLength; 
						}
					}
					else
					if(szParameter.CompareNoCase("remote_sentinel_XML_port")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_nRemoteSentinelXMLPort = nLength; 
						}
					}
				}

				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
					if(szParameter.CompareNoCase("Messages")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeMessageDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
				}
/*
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
					if(szParameter.CompareNoCase("AsRun_Log")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeAsRunDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
				}
*/				

				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			if(pszInfo)
			{
				_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
			}
			delete prs;

			// save settings.  // dont save them here.  save them on any changes in the main command loop.
			Settings(false); //write
/*
			if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
			{
				// these explicts arent necessary - uncomment to write out a full file to edit...
				file.SetIniString("Main", "Name", g_psentinel->m_settings.m_pszName);
				file.SetIniString("License", "Key", g_psentinel->m_settings.m_pszLicense);

				file.SetIniString("FileServer", "IconPath", g_psentinel->m_settings.m_pszIconPath);
				file.SetIniInt("CommandServer", "ListenPort", g_psentinel->m_settings.m_usCommandPort);
				file.SetIniInt("StatusServer", "ListenPort", g_psentinel->m_settings.m_usStatusPort);

				file.SetIniInt("Messager", "UseEmail", g_psentinel->m_settings.m_bUseEmail?1:0);
				file.SetIniInt("Messager", "UseNet", g_psentinel->m_settings.m_bUseNetwork?1:0);
				file.SetIniInt("Messager", "UseLog", g_psentinel->m_settings.m_bUseLog?1:0);

				file.SetIniString("Database", "DSN", g_psentinel->m_settings.m_pszDSN);
				file.SetIniString("Database", "DBUser", g_psentinel->m_settings.m_pszUser);
				file.SetIniString("Database", "DBPassword", g_psentinel->m_settings.m_pszPW);
				file.SetIniString("Database", "SettingsTableName", g_psentinel->m_settings.m_pszSettings);  // the Settings table name
				file.SetIniString("Database", "ExchangeTableName", g_psentinel->m_settings.m_pszExchange);  // the Exchange table name
				file.SetIniString("Database", "MessagesTableName", g_psentinel->m_settings.m_pszMessages);  // the Messages table name

				file.SetIniInt("HarrisAPI", "UseListCount", g_psentinel->m_settings.m_bUseListCount?1:0); // get all events up until the list count (otherwise just up to the lookahead)
				file.SetIniInt("HarrisAPI", "EventsPerRequest", g_psentinel->m_settings.m_ulEventsPerRequest); //  number of events to request at one time, up until lookahead or listcount (chunking for multilists, 0= use full count at once)

				file.SetIniString("Database", "ChannelsTableName", g_psentinel->m_settings.m_pszChannels);  // the Channels table name
				file.SetIniString("Database", "ConnectionsTableName", g_psentinel->m_settings.m_pszConnections);  // the Connections table name
				file.SetIniString("Database", "LiveEventsTableName", g_psentinel->m_settings.m_pszLiveEvents);  // the LiveEvents table name
				file.SetIniString("Database", "TempLiveEventsTableName", g_psentinel->m_settings.m_pszLiveEventsTemp);  // the LiveEvents table name

				file.SetIniInt("Database", "ModificationCheckInterval", g_psentinel->m_settings.m_ulModsIntervalMS);  // in milliseconds
				file.SetIniInt("Database", "ServerStatusDBIntervalMS", g_psentinel->m_settings.m_ulServerStatusDBIntervalMS);  // in milliseconds
				file.SetIniInt("Database", "ListStatusToDB", g_psentinel->m_settings.m_bListStatusToDB?1:0);

				file.SetSettings(SENTINEL_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

			}


*/
			return SENTINEL_SUCCESS;
		}
	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s%s%s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. ",
				m_pdb?"":"Database pointer was NULL. ",
				m_pdbConn?"":"Connection pointer was NULL. ",
				((m_pdbConn)&&(!m_pdbConn->m_bConnected))?"Database not connected. ":""
				);
		}
	}
	return SENTINEL_ERROR;
}

char* CSentinelSettings::ProcessString(char* pszString, bool bFreeIncomingString)
{
	if(pszString)
	{
		char pszOutput[4096];
		int nInLen=strlen(pszString);
		int nOutLen=0;
		char* pch = pszString;
		char* pchEnd = min((pch+4096),(pszString+nInLen));
		while((*pch!=0)&&(pch<pchEnd))
		{
			if(*pch == '%')
			{
				// we are in a specifier.
				// here is a list of ours:
				// %h  local hostname
		
				pch++;
				if((*pch!=0)&&(pch<pchEnd))
				{
					if(*pch == 'h')
					{
						if((g_psentinel->m_data.m_pszHost)&&(strlen(g_psentinel->m_data.m_pszHost)))
						{
							char* pszTemp = (char*)malloc(strlen(g_psentinel->m_data.m_pszHost)+1);
							if(pszTemp)
							{
								strcpy(pszTemp, g_psentinel->m_data.m_pszHost);
							
								char* pchPlus = pszTemp;
								while((*pchPlus != 0)&&(nOutLen<4096))
								{
									pszOutput[nOutLen]= *pchPlus;
									nOutLen++;
									pchPlus++;
								}
								free(pszTemp);
							}
						}
						pch++;
					}
					else
					{
						// some other specifier, can be anything.
						pszOutput[nOutLen]= *(pch-1); // do add the %, this may get further processed
						nOutLen++;
						pszOutput[nOutLen]= *pch;
						nOutLen++;
						pch++;
					}
				}
				else
				{
					pszOutput[nOutLen]= *(pch-1);
					nOutLen++;
				}
				
			}
			else
			{
				pszOutput[nOutLen]= *pch;
				nOutLen++;
				pch++;
			}
		}

		pszOutput[nOutLen]=0;

		pch = (char*)malloc(strlen(pszOutput)+1);
		if(pch)
		{
			strcpy(pch,pszOutput);
			if(bFreeIncomingString) 
			{
				try {free(pszString);} catch(...){}
			}
		}
		return pch;
	}
	return NULL;
}


