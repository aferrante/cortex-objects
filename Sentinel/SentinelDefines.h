// SentinelDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(SENTINELDEFINES_H_INCLUDED)
#define SENTINELDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define SENTINEL_CURRENT_VERSION		"2.1.1.22"


// modes
#define SENTINEL_MODE_DEFAULT			0x00000000  // exclusive
#define SENTINEL_MODE_LISTENER		0x00000001  // exclusive
#define SENTINEL_MODE_CLONE				0x00000002  // exclusive
#define SENTINEL_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define SENTINEL_MODE_VOLATILE		0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define SENTINEL_MODE_MASK				0x0000000f  // 

// default port values.
//#define SENTINEL_PORT_FILE				80		
#define SENTINEL_PORT_CMD					10670		
#define SENTINEL_PORT_STATUS			10671		

#define SENTINEL_PORT_INVALID			0	

#define SENTINEL_FLAG_DISABLED		0x0000	 // default
#define SENTINEL_FLAG_ENABLED			0x0001	
#define SENTINEL_FLAG_FOUND				0x1000
#define SENTINEL_FLAG_INIT				0x2000

#define SENTINEL_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded

#define SENTINEL_THREAD_MAX_DELAY				5000


#define SENTINEL_TYPE_REMOTE			0
#define SENTINEL_TYPE_LOCAL				1


// status
#define SENTINEL_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define SENTINEL_STATUS_UNKNOWN							0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define SENTINEL_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define SENTINEL_STATUS_ERROR								0x00000020  // error (red icon)
#define SENTINEL_STATUS_CONN								0x00000030  // ready (green icon)	
#define SENTINEL_STATUS_OK									0x00000030  // ready (green icon)	
#define SENTINEL_STATUS_RUN									0x00000040  // in progress, running, owned etc (blue icon);	
#define SENTINEL_ICON_MASK									0x00000070  // mask	

#define SENTINEL_STATUS_SUSPEND							0x00000080  // suspended	(yellow icon please)

#define SENTINEL_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define SENTINEL_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define SENTINEL_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define SENTINEL_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define SENTINEL_STATUS_CMDSVR_MASK					0x00007000  // command server mask bits

#define SENTINEL_STATUS_STATUSSVR_START			0x00010000  // starting the status server
#define SENTINEL_STATUS_STATUSSVR_RUN				0x00020000  // status server running
#define SENTINEL_STATUS_STATUSSVR_END				0x00030000  // status server shutting down
#define SENTINEL_STATUS_STATUSSVR_ERROR			0x00040000  // status server error
#define SENTINEL_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define SENTINEL_STATUS_THREAD_START				0x00100000  // starting the main thread
#define SENTINEL_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define SENTINEL_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define SENTINEL_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define SENTINEL_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define SENTINEL_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define SENTINEL_STATUS_THREAD_MASK					0x00f00000  // main thread mask bits

// various failures...
#define SENTINEL_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define SENTINEL_STATUS_FAIL_DB							0x20000000  // could not get DB
#define SENTINEL_STATUS_FAIL_MASK						0xf0000000  // failure code mask bits

//return values
#define SENTINEL_SUCCESS   0
#define SENTINEL_ERROR	   -1



// default filenames
#define SENTINEL_SETTINGS_FILE_SETTINGS	  "sentinel.csr"		// csr = cortex settings redirect
#define SENTINEL_SETTINGS_FILE_DEFAULT	  "sentinel.csf"		// csf = cortex settings file


// event status
#define SENTINEL_EVENT_NEW									0x00000000  // no flags yet
#define SENTINEL_EVENT_ACQUIRED							0x00000001  // the m_event member has been assigned
#define SENTINEL_EVENT_INSERTED							0x00000002  // inserted into DB
#define SENTINEL_EVENT_PROCESSED						0x00000004  // processed since last status change
#define SENTINEL_EVENT_ALLOCATED						0x00000008  // new memory object
#define SENTINEL_EVENT_DELETING							0x00000010  // deleting out of DB 
#define SENTINEL_EVENT_DELETED							0x00000020  // deleted out of DB 
#define SENTINEL_EVENT_TIMESHIFTER					0x00100000  // event is after an upcounter or manual start. 
#define SENTINEL_EVENT_UPCOUNT							0x00200000  // event is upcounting. 
#define SENTINEL_EVENT_HARD									0x00400000  // event is hard start. 
#define SENTINEL_EVENT_MANUAL								0x00800000  // event is manual start. 
#define SENTINEL_EVENT_CUTOFFRISK						0x01000000  // event has risk of being not run or cut short
#define SENTINEL_EVENT_TIMEMASK							0x01f00000  // event time control mask. 
#define SENTINEL_EVENT_NODEVICE							0x00010000  // event has no device assignment 
#define SENTINEL_EVENT_CUED									0x00020000  // event has a minor device assignment (port assigned = CUED!) (actually, device may be in standby as well...but let it go for now)
#define SENTINEL_EVENT_LOOKOUT							0x00040000  // event is outside the lookahead
#define SENTINEL_EVENT_NULLPARENT						0x00080000  // event parent on air time update to be suppressed
#define SENTINEL_EVENT_MATCHED							0x00000040  // event has already been matched this round.


#define SENTINEL_EVENT_DIFFERS							0x20000000  // event differs because parents are different
#define SENTINEL_EVENT_IDENTICAL						0x00000000  // event is identical
#define SENTINEL_EVENT_DIFFERS_TYPE					0x00000001  // event differs on type
#define SENTINEL_EVENT_DIFFERS_SEG					0x00000002  // event differs on segment
#define SENTINEL_EVENT_DIFFERS_OAT					0x00000004  // event differs on on air time
#define SENTINEL_EVENT_DIFFERS_DATE					0x00000008  // event differs on julian date
#define SENTINEL_EVENT_DIFFERS_DUR					0x00000010  // event differs on duration
#define SENTINEL_EVENT_DIFFERS_STATUS				0x00000020  // event differs on status
#define SENTINEL_EVENT_DIFFERS_CTRL					0x00000040  // event differs on time control
#define SENTINEL_EVENT_DIFFERS_POS					0x00000080  // event differs on position
#define SENTINEL_EVENT_DIFFERS_CALCOAT			0x00000100  // event differs on calculated on air time
#define SENTINEL_EVENT_DIFFERS_CALCDUR			0x00000200  // event differs on calculated duration

#define SENTINEL_EVENT_DIFFERS_ID						0x00001000  // event differs on ID
#define SENTINEL_EVENT_DIFFERS_TITLE				0x00002000  // event differs on TITLE
#define SENTINEL_EVENT_DIFFERS_DATA					0x00004000  // event differs on DATA
#define SENTINEL_EVENT_DIFFERS_RECKEY				0x00008000  // event differs on RECKEY

#define SENTINEL_EVENT_DIFFERS_KEY					0x00010000  // event differs on the assembled key

#define SENTINEL_PARENT_DIFFERS_UID					0x10000000  // event differs on type

/*
#define SENTINEL_PARENT_DIFFERS_TYPE				0x00010000  // event differs on type
#define SENTINEL_PARENT_DIFFERS_SEG					0x00020000  // event differs on segment
#define SENTINEL_PARENT_DIFFERS_OAT					0x00040000  // event differs on on air time
#define SENTINEL_PARENT_DIFFERS_DATE				0x00080000  // event differs on julian date
#define SENTINEL_PARENT_DIFFERS_DUR					0x00100000  // event differs on duration
#define SENTINEL_PARENT_DIFFERS_STATUS			0x00200000  // event differs on status
#define SENTINEL_PARENT_DIFFERS_CTRL				0x00400000  // event differs on status
#define SENTINEL_PARENT_DIFFERS_POS					0x00800000  // event differs on status
#define SENTINEL_PARENT_DIFFERS_CALCOAT			0x01000000  // event differs on status
#define SENTINEL_PARENT_DIFFERS_CALCDUR			0x02000000  // event differs on status

#define SENTINEL_PARENT_DIFFERS_ID					0x04000000  // event differs on ID
#define SENTINEL_PARENT_DIFFERS_TITLE				0x08000000  // event differs on TITLE
*/

#define SENTINEL_EVENT_MAXKEYLEN						1024

#define SENTINEL_MATCH_TIME						0
#define SENTINEL_MATCH_REF						1
#define SENTINEL_MATCH_POS						2

#define SENTINEL_MATCH_SECONDARY_TIME						0
#define SENTINEL_MATCH_SECONDARY_OFFSET					1


// commands
#define SENTINEL_CMD_INSERT	   0xa0
#define SENTINEL_CMD_DELETE	   0xa1
#define SENTINEL_CMD_GET		   0xa2
#define SENTINEL_CMD_LOCK		   0xa3
#define SENTINEL_CMD_MOVE		   0xa4
#define SENTINEL_CMD_MOD		   0xa5
#define SENTINEL_CMD_SWAP		   0xa6
#define SENTINEL_CMD_FIND		   0xa7  // just return postion in list
#define SENTINEL_CMD_DEL_ID	   0xa8  // delete by list of IDs, not index

// subcommands
#define SENTINEL_SCMD_TIESEC	 0x08 // tie secondaries to primaries in the command
#define SENTINEL_SCMD_NOSEC	   0x00 // explicit command, do NOT tie secondaries to primaries in the command
#define SENTINEL_SCMD_UNLOCK   0x08
#define SENTINEL_SCMD_LOCK		 0x00

#define SENTINEL_CMD_QUEUE		   0xc0 // puts thing in queue
#define SENTINEL_CMD_CMDQUEUE		 0xc1 // puts thing in command queue
#define SENTINEL_CMD_DIRECT			 0xc2 // skips queues, commands box directly

#define SENTINEL_CMD_EXSET			 0xea // increments exchange counter
#define SENTINEL_CMD_EXGET			 0xeb // gets exchange mod value
#define SENTINEL_CMD_MODSET			 0xec // sets exchange mod value, skips exchange table


// sentinel traffic file status
#define SENTINEL_TRAFFIC_ERROR				-1
#define SENTINEL_TRAFFIC_NONE					0
#define SENTINEL_TRAFFIC_ANALYZING		1
#define SENTINEL_TRAFFIC_IMPORTED			2


// debug defines
#define SENTINEL_DEBUG_TIMING					0x00000001
#define SENTINEL_DEBUG_MATCH					0x00000002
#define SENTINEL_DEBUG_CONNECT				0x00000004
#define SENTINEL_DEBUG_CONNECTSTATUS  0x00000008
#define SENTINEL_DEBUG_WORKINGSET		  0x00000010
#define SENTINEL_DEBUG_TRAFFIC			  0x00000020
#define SENTINEL_DEBUG_LISTOPS			  0x00000040
#define SENTINEL_DEBUG_SEARCH					0x00000080
#define SENTINEL_DEBUG_CHANGES				0x00000100
#define SENTINEL_DEBUG_COMM						0x00000200
#define SENTINEL_DEBUG_CONTROL				0x00000400  // control modules
#define SENTINEL_DEBUG_SERVICE				0x00000800  // servicing control modules
#define SENTINEL_DEBUG_API						0x00001000  // API calls
#define SENTINEL_DEBUG_DELAY					0x00100000  // add delays where appropriate to the logging


#define WATCH_RECHK			0x00000020  // watchfolder needs to recheck a file	...
#define WATCH_PROCESSED	0x00000040  // a file was processed


// Sentinel specific XML commands
#define SENTINEL_XML_TYPE_GETCONN				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
#define SENTINEL_XML_TYPE_GETLIST				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
#define SENTINEL_XML_TYPE_GETEVENT			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .

#define SENTINEL_XML_LIST_MAXPAYLOAD	  0x00ffff  //65535
#define SENTINEL_XML_EVENT_MAXPAYLOAD	  0xffffff  //16777215 enough?

// Sentinel control module commands.
#define DLLCMD_ACONN	0x0100 //	Sent to the DLL when a connection to an automation system is established or disconnected. Disconnection is indicated by the aconn.status.server_status element.�
#define DLLCMD_ALIST	0x0101 //	Sent to the DLL when monitoring for a list on an active connection is begun or discontinued.  If the connection on which the list information is coming has terminated, then a separate DLLCMD_ALIST command is not sent, as the monitoring may still be active, and will resume when the connection is restored.  Discontinuation of list monitoring is indicated by the active attribute of the alist.config element.�
#define DLLCMD_AITEM	0x0102 //	Sent to the DLL when an event on a monitored list is new, has changed, or is removed.  In the case that the event is removed, the aitem.info.position element will contain a value of -1, and no other aitem.info elements are included.�


#define SENTINEL_CHANGELIST_ARRAYSIZE_INC 1024

#endif // !defined(SENTINELDEFINES_H_INCLUDED)



/*
CREATE TABLE Events(event_uid int, event_key varchar(256), server_name varchar(32), server_list int, 
list_id int, event_id varchar(32), event_clip varchar(64), event_segment int, event_title varchar(64), 
event_data varchar(4096),event_type int, event_status int, event_time_mode int, event_start decimal(20,3), 
event_duration int, event_calc_start decimal(20,3), event_calc_duration int, event_calc_end decimal(20,3), 
event_position int, event_last_update decimal(20,3), parent_uid int, parent_key varchar(256), 
parent_id varchar(32), parent_clip varchar(64), parent_segment int, parent_title varchar(64), parent_type int, 
parent_status int, parent_time_mode int, parent_start decimal(20,3), parent_duration int, parent_calc_start decimal(20,3), 
parent_calc_duration int, parent_calc_end decimal(20,3), parent_position int, app_data varchar(512), app_data_aux int);   
*/


/*

AUTOPLAY             P
AUTOSWITCH           S
AUTOTHREAD           T
AUTORECORD           R
HARDTIME             O
UPCOUNTER            U
MANUALSTART          M
AUTOCONTACTSTART     C
AUTOMARKTIME         N
AUTODEADROLL         D
SWITCHVIDEOONLY      V
SWITCHREJOIN         J
USERBITONLY          B
SWITCHAVIndependent  E
When AUTOPLAY, AUTOSWITCH, and AUTOTHREAD are set they will display as an A.


0   AVEVENT,         "",
1   AUDIOEVENT,      "",
2   VIDEOEVENT,      "",
16  BREAKEVENT,      "",
32  LOGOEVENT,       "",
33  BREAKSYNC,       "sSYN",
128 SECAVEVENT,      "sAV",
129 SECAUDIOEVENT,   "sA",
130 SECVIDEOEVENT,   "sV",
131 SECKEYEVENT,     "sKEY",
132 SECTRANSKEY,     "sTKY",
133 SECBACKAVEVENT,  "bAV",
136 SECBACKTIMEGPI,  "bGPI",
137 SECGPIEVENT,     "sGPI",
144 SECTRANSAUDOVER, "sTAO",
145 SECAUDOVER,      "sAOV",
160 SECDATAEVENT,    "sDAT",
164 SECSYSTEMEVENT,  "sSYS",
165 SECBACKSYSTEM,   "bSYS",
176 SECRECORDSWITCH, "sRSW",
177 SECSOURCESWITCH, "sSRC", or "sXP" "Crosspoint"
181 SECRECORDDEVICE, "?",
224 SECCOMMENT,      "****",
225 SECCOMPILEID,    "cmID",
226 SECAPPFLAG,      "",
227 SECBARTERSPOT,   "sBAR",
255 INVALIDEVENT,    "INV"

*/