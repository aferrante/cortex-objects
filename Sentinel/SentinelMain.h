// SentinelMain.h: interface for the CSentinelMain class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SENTINELMAIN_H__2FA150AF_62FD_474D_9F15_E4CA67BA3807__INCLUDED_)
#define AFX_SENTINELMAIN_H__2FA150AF_62FD_474D_9F15_E4CA67BA3807__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Sentinel.h" 
#include "../../Common/MSG/msg.h"
#include "../../Common/MSG/Messager.h"
#include "../../Common/TXT/FileUtil.h"
//#include "../../Common/HTTP/HTTP10.h"  // includes CMessagingObject
#include "SentinelSettings.h"
#include "SentinelData.h"

#define SENDMSG_SENDER_MAXLEN 64
#define SENDMSG_EVENT_MAXLEN 64
#define SENDMSG_MESSAGE_MAXLEN 512


// global message function so that dialogs etc (external objects) can write to the log etc.
//void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations);

// global main thread - this is the business right here!
void SentinelMainThread(void* pvArgs);

// cortex http thread - we pass in the main cortex object, do all the cgi programming and security checking.
//void SentinelHTTPThread(void* pvArgs);

void SentinelCommandHandlerThread(void* pvArgs);
void SentinelStatusHandlerThread(void* pvArgs);
void SentinelXMLHandlerThread(void* pvArgs);

class CSentinelMain  
{
public:
	CSentinelMain();
	virtual ~CSentinelMain();

	CMessager						m_msgr;				// main messager // the one and only messager object
	CSentinelSettings		m_settings;		// main mem storage for settings.  settings file objects are local to the SentinelMainThread.
	CSentinelData				m_data;				// internal variables
	CNetUtil						m_net;				// have a separate one for the status and command services.

	// core net functions
	SOCKET*		SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char SubCmd=CX_CMD_NULL, unsigned char* pucData=NULL, unsigned long ulDataLen=0, char* pchUser=NULL, char* pchPw=NULL);  // cortex initiates a request to an object server
	int		SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd=CX_CMD_NULL, unsigned char* pucData=NULL, unsigned long ulDataLen=0, char* pchUser=NULL, char* pchPw=NULL);		// cortex replies to an object server after receiving data. (usually ack or nak)
	int		SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd=CX_CMD_NULL, unsigned char* pucData=NULL, unsigned long ulDataLen=0, char* pchUser=NULL, char* pchPw=NULL);		// cortex answers a request from an object client

	// move following to a Messager destination object...
	int SendMsg(int nType, char* pszSender, char* pszError, ...);
	int SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...);

};

#endif // !defined(AFX_SENTINELMAIN_H__2FA150AF_62FD_474D_9F15_E4CA67BA3807__INCLUDED_)
