// SentinelSettings.h: interface for the CSentinelSettings class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SENTINELSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
#define AFX_SENTINELSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "SentinelDefines.h"
#include "../../Common/MFC/ODBC/DBUtil.h"

class CSentinelSettings  
{
public:
	CSentinelSettings();
	virtual ~CSentinelSettings();

	int GetFromDatabase(char* pszInfo=NULL);  //only get.  Interface publishes
	int Settings(bool bRead);
	char* ProcessString(char* pszString, bool bFreeIncomingString);

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;

	char* m_pszName;  // familiar name of this instance (and therefore the name of the Sentinel database)
	char* m_pszType;  // familiar name of the type.
	char* m_pszProject;  // familiar name of the project name.
	unsigned long m_ulMainMode;

	// ports
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;

	int m_nAutoPurgeMessageDays;
	int m_nAutoPurgeAsRunDays;
	int m_nAutoPurgeInterval;


	// The following flags determine what settings are loaded. 
	// The specifics do not need to be stored in cortex settings, they are pushed to the various objects.

	// messaging for Sentinel
	bool m_bUseLog;			// write a log file
	bool m_bUseEmail;		// send an email on failures (or commanded from remotes)
	bool m_bUseNetwork; // send a message to a remote host
	char* m_pszMailSpec;
	char* m_pszFileSpec;
	char* m_pszProcessedFileSpec;
	char* m_pszProcessedMailSpec;
	bool m_bUseXMLClientLog;			// write a log file
	bool m_bLogNetworkErrors; // enables messaging into network object (logs socket errors, etc).

	// Harris API
	bool m_bUseListCount; // get all events up until the list count (otherwise just up to the lookahead)
	bool m_bCommentUsesPrimaryStatus; // secondary events like comments, app flag, compile ID, etc, get assigned the status of the primary.
	unsigned long m_ulEventsPerRequest;  // number of events to request at one time, up until lookahead or listcount (chunking for multilists, 0= use full count at once)
	bool m_bUseTitleMatch; // use title field to match unique IDs

	int m_nEventTimeToleranceMS;
	int m_nThreadDwellMS;

	int m_nMaxLookahead;
	int m_nMaxAutomationBufferMS;  // number of milliseconds after an automation change to wait for further changes.
	int m_nMaxAutomationForceMS;  // maximum number of milliseconds after an automation change to trigger changes. (prevents a situation where continual automation changes keep extending buffer time forever)
	bool m_bDoNotBreakOnChange;  // just continue getting
	bool m_bPositionShiftOnChange;  // if a break on change, and the positions of all gotten events are shifted consistently, shift all the rest of the ones before breaking out.
	bool m_bGetOnListSizeChangeOnly;  // ignore other list changes for getting events.
	bool m_bUseWorkingSet;  // use working set
	bool m_bDisconnectOnSuspend; // if suspended, disconnect API conn
	bool m_bClearListOnDisconnect; // if disconnected, clear out old list immediately


	// traffic file related
	bool m_bUseTraffic;  // use traffic files
	bool m_bTrafficInWorkingSet;  // use traffic file info in working set...
	char* m_pszTrafficBaseFolder;  // the watchfolder where the traffic files are kept.

	char* m_pszTrafficChannelMap;  // in the filename, how to map to a channel. (find token formula)
	char* m_pszTrafficDateMap;  // in the filename, how to map to a date.  (find token formula)
	bool  m_bTrafficDateMapCaseInsensitive;
	int   m_nTrafficThreadMax;  // cap the number of simultaneous directory search threads.

	int  m_nTrafficErrorInterval; // how many seconds to wait after an error before trying again
	int  m_nTrafficFolderInterval; // how many seconds to wait between watchfolder checks.
	bool m_bTrafficAutoDeleteFiles;  // delete old traffic files
	bool m_bTrafficAutoBackupFiles;  // backup old traffic files
	int  m_nTrafficFileExpiry;  // delete after this many minutes of the day of the file.
	char* m_pszTrafficBackupBaseFolder;  // the watchfolder where the traffic files are backed up.
	char* m_pszTrafficBackupFileMap;  // the the rest of the folder name and filename, how to map to a channel, etc.
	//traffic files must be in the following date range with respect to "now", old files get purged, too far files in the future are not considered.
	int  m_nTrafficFilesDoneDays; // how many days to retain old traffic files and events
	int  m_nTrafficFileLookahead; // how many days ahead to allow a file to be imported.
	char* m_pszTrafficFileAllowedExtensions;  // csv list of file extensions to search for schedules in.


	//grid related
	bool m_bUseGrid;  // use grid
	bool m_bGridInWorkingSet;  // use grid info in working set...
	// no info on grids yet...

	// database stuff
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;
	char* m_pszDatabase;

	char* m_pszSettings;  // the Settings table name
	char* m_pszExchange;  // the Exchange table name
	char* m_pszMessages;  // the Messages table name
	char* m_pszAsRun;  // the As run table name

	char* m_pszChannels;  // the Channels table name
	char* m_pszConnections;  // the Connections table name
	char* m_pszLiveEvents;  // the LiveEvents table name
	char* m_pszLiveEventsTemp;  // the temporary LiveEvents table name
	char* m_pszClipKeyMetadataView;  // the view of the clip metadata table including key field.
	char* m_pszWorkingSet;  // the working set table name
//	char* m_pszControlModules;  // the control modules table name - DLLs that get commanded by changes in Sentinel
//	char* m_pszInterpreterModules;  // the interpreter modules table name - DLLs that accept automation data from an external source

	bool m_bUseRemoteChannels;			// use remote channels and virtual mapping
	char* m_pszRemoteChannels;  // the RemoteChannels table name
	char* m_pszVirtualMappingView;  // the VirtualMapping View name

	char* m_pszRemoteSentinelDescription;  // the Remote Sentinel Description
	char* m_pszRemoteSentinelHost;  // the Remote Sentinel Host name
	int m_nRemoteSentinelControlPort;  // the Remote Sentinel Control port
	int m_nRemoteSentinelXMLPort;  // the Remote Sentinel XML port

	char* m_pszExternalModules;  // the external modules table name - all DLLs!  we put it in one table now.
	
	bool m_bUseControlModules;			// use control modules
	bool m_bUseInterpreterModules;			// use interpreter modules

	bool m_bMillisecondMessaging;			// use millisecond resolution for messages and asrun
	unsigned long m_ulModuleEndQueryIntervalMS;  // interval on which to query end on module

	char* m_pszTraffic;  // the traffic table name
	char* m_pszTrafficFiles;  // the traffic files table name
	char* m_pszGrid;  // the grid table name

	unsigned long m_ulModsIntervalMS;  // interval on which to check database mods

	unsigned long m_ulServerStatusDBIntervalMS;  // interval on which to insert server status to database; 0= do not ever insert
	bool m_bListStatusToDB;  // push list status to db
	bool m_bListStatusInExchange;  // old way, in exchange table. 
	bool m_bStatusInTables;  // new way, in Connections (server status) and Channels (list status) table.
	bool m_bDelayStatusForEvents;  // do not update the last status in database until events have been done 
	bool m_bDisableEventMatchOnPosition;  // if true, always uses the time threshold for matching
	bool m_bDisableTopListPurge;  // if true, parses whole list before deleting done events
	bool m_bDoConfirmGet;  // if true, after changes, gets once more just to confirm.
	bool m_bMultiConnectSQL;  // if true, uses multiple SQL connections instead of just one.
	int m_nMaxDeleteItems;  // max number of items to delete in a single SQL statement
	int m_nDefaultPackCount; // to unlock list when the unlock info is not available

	char* m_pszEventKeyFormat; // the format string for the event key field

	int  m_nSecondaryEventMatchMode;  // 1= use offset times, 0 = use absolute times
	int  m_nConfirmGetIntervalMS;  // time to wait before doing the confirm get

	// XML
	bool m_bLocalClockToUnixtimesInXML; //not implemented yet
	bool m_bUseChecksumInXML;
	int m_nEventLimitInXML;


	// CONTROL module settings.
	int m_nCtrlModLookahead;  //per list
	int m_nCtrlModMaxQueue;  //global


	// debug
	bool m_bDebugLists;  // if true, dumps the temp and events databases
	int  m_nDebugListTopCount;  //if positive, just prints out the top n recored in the events tables.
	bool m_bDebugSQL;  // if true, prints out the SQL calls, all but event inserts
	bool m_bDebugInsertSQL;  // if true, prints out the event insert SQL calls
	unsigned long m_ulDebug;  // prints out debug statements that & with this.
	char* m_pszDebugOrder; // the order by clause for debug

	unsigned long m_ulConnTimeout;	// when the connection has received no update for number of seconds, the connection is restarted 
	unsigned long m_ulConnectionRetries;		// how many times to try to re-establish ULONG_MAX-1 is 4085 years with 30 second interval!
	unsigned long m_ulConnectionInterval;	// how long to wait in seconds before retrying
	
	char* m_pszLicense;  // the License Key
	char* m_pszIconPath;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

	// external control settings.
	bool m_bStubModeNetCalls;
};

#endif // !defined(AFX_SENTINELSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
