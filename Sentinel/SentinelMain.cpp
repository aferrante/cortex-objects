// SentinelMain.cpp: implementation of the CSentinelMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "Sentinel.h"  // just included to have access to windowing environment
#include "SentinelDlg.h"  // just included to have access to windowing environment
#include "SentinelHandler.h"  // just included to have access to windowing environment

#include "SentinelMain.h"
#include <process.h>
#include "../../Common/TXT/BufferUtil.h"
#include "../../Common/MFC/ODBC/DBUtil.h"
#include "../../Common/API/Harris/ADC.h"
#include "../../Common/FILE/DirUtil.h"

#include <sys/timeb.h>
#include <time.h>
#include <direct.h>
#include <objsafe.h>
#include <atlbase.h>

// have to add rpcrt4.lib to the linker for UUID stuff

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#undef SENTINEL_THREAD_MAX_DELAY
#define SENTINEL_THREAD_MAX_DELAY				60000
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;
bool g_bKillStatus = false;
CSentinelMain* g_psentinel=NULL;
CADC g_adc;
CDirUtil	g_dir;				// watch folder utilities.

extern CMessager* g_pmsgr;  // from Messager.cpp
extern CSentinelApp theApp;


#define SENTINEL_MANAGED_DB  // does matching in memory and eases up the db transactions

void SentinelConnectionThread(void* pvArgs);
void SentinelListThread(void* pvArgs);
void SentinelWorkingSetMaintainThread(void* pvArgs);
void SentinelWorkingSetAdjustThread(void* pvArgs);
void SentinelTrafficWatchThread(void* pvArgs);
void SentinelControlModuleServiceThread(void* pvArgs);
void SentinelPublishChangeThread(void* pvArgs);
void SocketMonitorThread(void* pvArgs);



//BOOL g_bDebug = FALSE;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSentinelMain::CSentinelMain()
{
}

CSentinelMain::~CSentinelMain()
{
}

/*

char*	CSentinelMain::SentinelTranslate(CHTTPHeader* pHeader, char* pszBuffer)			// apply sentinel scripting language
{
	return pszBuffer;
}

int		CSentinelMain::InterpretDirective(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the directive, such as a char buffer.
	return SENTINEL_SUCCESS;
}
*/

SOCKET*	CSentinelMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // sentinel initiates a request to an object server
{
	CNetData* pdata = new CNetData;

	pdata->m_ucType = ucType;      // defined type - indicates which protocol to use, structure of data

	pdata->m_ucCmd = ucCmd;       // the command byte
	pdata->m_ucSubCmd = ucSubCmd;       // the subcommand byte

	pdata->m_pucData = pucData;
	pdata->m_ulDataLen = ulDataLen;

	SOCKET* ps = NULL; 
	m_net.SendData(pdata, pchHost, usPort, 5000, 0, NET_SND_CMDTOSVR, ps);
//	m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);

	return ps;
}

int		CSentinelMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// sentinel replies to an object server after receiving data. (usually ack or nak)
{
	return SENTINEL_SUCCESS;
}

int		CSentinelMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// sentinel answers a request from an object client
{
	return SENTINEL_SUCCESS;
}


void SentinelMainThread(void* pvArgs)
{
	CSentinelApp* pApp = (CSentinelApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment
	//startup.
	g_bThreadStarted = true;

	//create the main objects.

	CSentinelMain sentinel;
	CDBUtil db;

	sentinel.m_data.m_ulFlags &= ~SENTINEL_STATUS_THREAD_MASK;
	sentinel.m_data.m_ulFlags |= SENTINEL_STATUS_THREAD_START;
	sentinel.m_data.m_ulFlags &= ~SENTINEL_ICON_MASK;
	sentinel.m_data.m_ulFlags |= SENTINEL_STATUS_UNINIT;

	sentinel.m_data.GetHost();

	g_psentinel = &sentinel;


	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
//	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\Sentinel\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}



//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

//		AfxMessageBox( pApp->m_lpCmdLine );

	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// check here for Cortex IP.
			if(strlen(pch))
			{
//		AfxMessageBox( pch );
				if(sentinel.m_data.m_bu.CountChar(pch, strlen(pch), ':')>=1)  // must be of form "hostname:cmdport" where hostname can be name or ip
				{
					sentinel.m_data.m_pszCortexHost = (char*)malloc(strlen(pch)+1);
					if(sentinel.m_data.m_pszCortexHost)
					{
						strcpy(sentinel.m_data.m_pszCortexHost, pch);
//		AfxMessageBox( sentinel.m_data.m_pszCortexHost );

						char* pchd = strchr(sentinel.m_data.m_pszCortexHost, ':');
						if(pchd)
						{
							*pchd = 0;  // null term the host name;
							pchd++;
//		AfxMessageBox( sentinel.m_data.m_pszCortexHost );

							char* pchd2 = strchr(pchd, ':');
							if(pchd2)
							{
								*pchd2 = 0;  // null term the cmd port;
								pchd2++;
								if(strlen(pchd2)) sentinel.m_data.m_usCortexStatusPort = atoi(pchd2);
							}

							if(strlen(pchd)) sentinel.m_data.m_usCortexCommandPort = atoi(pchd);
						}
					}

				}
			}
			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }

	// get settings.
	sentinel.m_settings.Settings(true); //read
//	char* pszParams = NULL;
/*
	char pszFilename[MAX_PATH];

	strcpy(pszFilename, SENTINEL_SETTINGS_FILE_DEFAULT);  // sentinel settings file

	CFileUtil file;
	file.GetSettings(pszFilename, false); 
// load up the values on the settings object
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		sentinel.m_settings.m_pszName = file.GetIniString("Main", "Name", "Sentinel");
		sentinel.m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");

		// compile license key params
		if(g_psentinel->m_data.m_key.m_pszLicenseString) free(g_psentinel->m_data.m_key.m_pszLicenseString);
		g_psentinel->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(sentinel.m_settings.m_pszLicense)+1);
		if(g_psentinel->m_data.m_key.m_pszLicenseString)
		sprintf(g_psentinel->m_data.m_key.m_pszLicenseString, "%s", sentinel.m_settings.m_pszLicense);

		g_psentinel->m_data.m_key.InterpretKey();

		if(g_psentinel->m_data.m_key.m_bValid)
		{
			unsigned long i=0;
			while(i<g_psentinel->m_data.m_key.m_ulNumParams)
			{
				if((g_psentinel->m_data.m_key.m_ppszParams)
					&&(g_psentinel->m_data.m_key.m_ppszValues)
					&&(g_psentinel->m_data.m_key.m_ppszParams[i])
					&&(g_psentinel->m_data.m_key.m_ppszValues[i]))
				{
					if(stricmp(g_psentinel->m_data.m_key.m_ppszParams[i], "max")==0)
					{
//						g_psentinel->m_data.m_nMaxLicensedDevices = atoi(g_psentinel->m_data.m_key.m_ppszValues[i]);
					}
				}
				i++;
			}

			if(
					(
				    (!sentinel.m_data.m_key.m_bExpires)
					||((sentinel.m_data.m_key.m_bExpires)&&(!sentinel.m_data.m_key.m_bExpired))
					||((sentinel.m_data.m_key.m_bExpires)&&(sentinel.m_data.m_key.m_bExpireForgiveness)&&(sentinel.m_data.m_key.m_ulExpiryDate+sentinel.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
				    (!sentinel.m_data.m_key.m_bMachineSpecific)
					||((sentinel.m_data.m_key.m_bMachineSpecific)&&(sentinel.m_data.m_key.m_bValidMAC))
					)
				)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
				sentinel.m_data.SetStatusText(errorstring, SENTINEL_STATUS_OK);
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				sentinel.m_data.SetStatusText(errorstring, SENTINEL_STATUS_ERROR);
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
			sentinel.m_data.SetStatusText(errorstring, SENTINEL_STATUS_ERROR);
		}
/*
		sentinel.m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");
		sentinel.m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", SENTINEL_PORT_CMD);
		sentinel.m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", SENTINEL_PORT_STATUS);

		sentinel.m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
		sentinel.m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
		sentinel.m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;

		sentinel.m_settings.m_bUseListCount = file.GetIniInt("HarrisAPI", "UseListCount", 0)?true:false; // get all events up until the list count (otherwise just up to the lookahead)
		sentinel.m_settings.m_ulEventsPerRequest = file.GetIniInt("HarrisAPI", "EventsPerRequest", 0); //  number of events to request at one time, up until lookahead or listcount (chunking for multilists, 0= use full count at once)


		sentinel.m_settings.m_pszDSN = file.GetIniString("Database", "DSN", sentinel.m_settings.m_pszName?sentinel.m_settings.m_pszName:"Sentinel");
		sentinel.m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
		sentinel.m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
		sentinel.m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
		sentinel.m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
		sentinel.m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name

		sentinel.m_settings.m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels");  // the Channels table name
		sentinel.m_settings.m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Connections");  // the Connections table name
		sentinel.m_settings.m_pszLiveEvents = file.GetIniString("Database", "LiveEventsTableName", "Events");  // the LiveEvents table name
		sentinel.m_settings.m_pszLiveEventsTemp = file.GetIniString("Database", "TempLiveEventsTableName", "Events_Temp");  // the LiveEvents table name

		sentinel.m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
		sentinel.m_settings.m_ulServerStatusDBIntervalMS = file.GetIniInt("Database", "ServerStatusDBIntervalMS", 5000);  // in milliseconds
		sentinel.m_settings.m_bListStatusToDB = file.GetIniInt("Database", "ListStatusToDB", 1)?true:false;

		if(pszParams) free(pszParams); pszParams=NULL;
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
	}
*/

	bool bUseLog = false;
	bool bUseEmail = false;
	bool bLogNetworkErrors = false;
	if(sentinel.m_settings.m_bUseLog)
	{
		bUseLog = sentinel.m_settings.m_bUseLog;

		// for logfiles, we need params, and they must be in this format:
		//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
//		pszParams = file.GetIniString("Messager", "LogFileIni", "Sentinel|YD||1|");
		int nRegisterCode=0;

		nRegisterCode = sentinel.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT,
			"log", 
			sentinel.m_settings.m_pszProcessedFileSpec?sentinel.m_settings.m_pszProcessedFileSpec:sentinel.m_settings.m_pszFileSpec, 
			errorstring);

		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
			sentinel.m_data.SetStatusText(errorstring, (SENTINEL_STATUS_FAIL_LOG|SENTINEL_STATUS_ERROR));
		}

//		if(pszParams) free(pszParams); pszParams=NULL;
	}
	Sleep(200); // let the thing be registered first

	sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "--------------------------------------------------\n\
-------------- Sentinel %s start --------------", SENTINEL_CURRENT_VERSION);  //(Dispatch message)

/*
		sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "netDisconnect %d", netDisconnect);  //(Dispatch message)
		sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "netConnected %d", netConnected);  //(Dispatch message)
		sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "netAborted %d", netAborted);  //(Dispatch message)
		sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "netPending %d", netPending);  //(Dispatch message)
		sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "netNoConnection %d", netNoConnection);  //(Dispatch message)
		sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "netSessionClosed %d", netSessionClosed);  //(Dispatch message)
*/
	if(sentinel.m_settings.m_bUseEmail)
	{
		bUseEmail = sentinel.m_settings.m_bUseEmail;
				// for smtp, we need params, and they must be in this format:
				// SMTP address[: override port (optional)] | distribution lists | from email | subject spec | localhostname | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
				// note on distribution lists:
				// we want to give the option to map message type and severity to different address lists.
				// the distribution lists field format is this:
				// ff:x@x.com,q@q.com;0x:B@b.com
				// the : delimits the type and severity flag specifier from the email address list for that set. 
				// the email addresses are delimited by comma
				// the sets are delimited by ;
				// the delimiters are not allowed in addresses, as described here: http://en.wikipedia.org/wiki/E-mail_address
				// on the type and severity specifiers, it is like this:
				// the subfield is a 2 character hex code, sort of.
				// severity is the first char, possible values are 0,1,2,3,4, for normal, low, medium, high, critical
				// additionally, f means all
				// type is the second char, possible values are 0-8, for the follwing:
				// #define MSG_ICONNONE								0x00000000  // default, nothing
				// #define MSG_ICONQUESTION						0x00000001  // ? icon
				// #define MSG_ICONEXCLAMATION				0x00000002  // ! icon
				// #define MSG_ICONERROR							0x00000003  // X icon
				// #define MSG_ICONSTOP								0x00000004  // stop sign icon
				// #define MSG_ICONHAND								0x00000005  // hand icon
				// #define MSG_ICONINFO								0x00000006  // (i) icon 
				// #define MSG_ICONUSER1							0x00000007  
				// #define MSG_ICONUSER2							0x00000008  
				// ff means send all messages to the address list
				// 00 means send all messages with severity normal and no "icon" to the address list
				// 03 means send all messages with severity normal and error type to the address list
				// 43 means send all messages with severity critical and error type to the address list
				// you can set up different address lists with the same addresses - one email per dispatched message will be sent in the case of overlap.

		int nRegisterCode=0;

		// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
		nRegisterCode = sentinel.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
			"email", 
			sentinel.m_settings.m_pszProcessedMailSpec?sentinel.m_settings.m_pszProcessedMailSpec:sentinel.m_settings.m_pszMailSpec,
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
			sentinel.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
			sentinel.m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:smtp_init", errorstring);  //(Dispatch message)
		}
	}
	
//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
//	sentinel.m_http.InitializeMessaging(&sentinel.m_msgr);
	//sentinel.m_net.InitializeMessaging(&sentinel.m_msgr);
	if(sentinel.m_settings.m_bLogNetworkErrors) 
	{
		bLogNetworkErrors = sentinel.m_settings.m_bLogNetworkErrors;
		if(sentinel.m_net.InitializeMessaging(&sentinel.m_msgr)==0)
		{
			sentinel.m_data.m_bNetworkMessagingInitialized=true;
		}
	}


	g_adc.InitializeMessaging(&sentinel.m_msgr);
//	db.InitializeMessaging(&sentinel.m_msgr);


// TODO: here, get the DB and pull any override settings.
	//****
	CDBconn* pdbConn = db.CreateNewConnection(sentinel.m_settings.m_pszDSN, sentinel.m_settings.m_pszUser, sentinel.m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			sentinel.m_data.SetStatusText(errorstring, (SENTINEL_STATUS_FAIL_DB|SENTINEL_STATUS_ERROR));
			sentinel.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Sentinel:database_connect", errorstring);  //(Dispatch message)
		}
		else
		{
			sentinel.m_settings.m_pdbConn = pdbConn;
			sentinel.m_settings.m_pdb = &db;
			sentinel.m_data.m_pdbConn = pdbConn;
			sentinel.m_data.m_pdb = &db;
			if(sentinel.m_settings.GetFromDatabase(errorstring)<SENTINEL_SUCCESS)
			{
				sentinel.m_data.SetStatusText(errorstring, (SENTINEL_STATUS_FAIL_DB|SENTINEL_STATUS_ERROR));
				sentinel.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Sentinel:database_get", errorstring);  //(Dispatch message)
			}
			else
			{
				sentinel.m_data.m_nLastUid = sentinel.m_data.GetLastUID(errorstring);

				if(sentinel.m_data.m_nLastUid<=SENTINEL_ERROR)
				{
					sentinel.m_data.m_nLastUid = 1;// start at the beginning.
					sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:uid_init", "Unable to obtain starting UID. [%s]", errorstring);  //(Dispatch message)
				}
				else
				{
					sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:uid_init", "Starting UID obtained (%d)", sentinel.m_data.m_nLastUid);  //(Dispatch message)
				}
				sentinel.m_data.SetThreads(errorstring);
			}

			// do the following even if the get database fails.
			if(
					(sentinel.m_settings.m_pszLiveEvents)
				&&(strlen(sentinel.m_settings.m_pszLiveEvents)>0)
				)
			{

				char szSQL[DB_SQLSTRING_MAXLEN];
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
													sentinel.m_settings.m_pszLiveEvents  // table name
												);
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "SentinelChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

				EnterCriticalSection(&sentinel.m_data.m_critSQL);
				if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
				{
						//**MSG
				}
				LeaveCriticalSection(&sentinel.m_data.m_critSQL);
/*
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
													sentinel.m_settings.m_pszLiveEventsTemp  // table name
												);
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "SentinelChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

				EnterCriticalSection(&sentinel.m_data.m_critSQL);
				if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
				{
						//**MSG
				}
				LeaveCriticalSection(&sentinel.m_data.m_critSQL);
*/
				
				EnterCriticalSection(&sentinel.m_data.m_critIncrement);
				if (sentinel.m_data.IncrementDatabaseMods(sentinel.m_settings.m_pszLiveEvents, errorstring)<SENTINEL_SUCCESS)
				{
					//**MSG
				}
				LeaveCriticalSection(&sentinel.m_data.m_critIncrement);

			}

			if((!sentinel.m_settings.m_bUseEmail)&&(bUseEmail))
			{
				bUseEmail = false;
				// reset it
				sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "Shutting down email functions.");  //(Dispatch message)

//						Sleep(250); // let the message get there.
				sentinel.m_msgr.RemoveDestination("email");

			}
			if((!sentinel.m_settings.m_bLogNetworkErrors)&&(bLogNetworkErrors))
			{
				// reset it
				sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "Shutting down network logging.");  //(Dispatch message)
				if(sentinel.m_data.m_bNetworkMessagingInitialized)
				{
					sentinel.m_net.UninitializeMessaging();  // void return
					sentinel.m_data.m_bNetworkMessagingInitialized=false;
				}

			}
			if((!sentinel.m_settings.m_bUseLog)&&(bUseLog))
			{
				// reset it
				sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

				Sleep(250); // let the message get there.
				sentinel.m_msgr.RemoveDestination("log");

			}
			
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", sentinel.m_settings.m_pszDSN, sentinel.m_settings.m_pszUser, sentinel.m_settings.m_pszPW); 
		sentinel.m_data.SetStatusText(errorstring, (SENTINEL_STATUS_FAIL_DB|SENTINEL_STATUS_ERROR));
		sentinel.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Sentinel:database_init", errorstring);  //(Dispatch message)

		//**MSG
	}



//init command and status listeners.
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing command server on %d", sentinel.m_settings.m_usCommandPort); 
	sentinel.m_data.SetStatusText(errorstring, SENTINEL_STATUS_CMDSVR_START);
	sentinel.m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:command_server_init", errorstring);  Sleep(100); //(Dispatch message)

	if(sentinel.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = sentinel.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "SentinelCommandServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = SentinelCommandHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &sentinel;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &sentinel.m_net;					// pointer to the object with the Message function.


		if(sentinel.m_net.StartServer(pServer, &sentinel.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			sentinel.m_data.SetStatusText(errorstring, SENTINEL_STATUS_CMDSVR_ERROR);
			sentinel.m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:command_server_init", errorstring);  //(Dispatch message)
			sentinel.SendMsg(CX_SENDMSG_ERROR|MSG_PRI_HIGH, "Sentinel:command_server_init", errorstring);
		}
		else
		{
//AfxMessageBox("Q1");
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Command server listening on %d", sentinel.m_settings.m_usCommandPort);
			sentinel.m_data.SetStatusText(errorstring, SENTINEL_STATUS_CMDSVR_RUN);
			sentinel.m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:command_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing XML server on %d", sentinel.m_settings.m_usStatusPort); 
	sentinel.m_data.SetStatusText(errorstring, SENTINEL_STATUS_STATUSSVR_START);
	sentinel.m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:xml_server_init", errorstring);  Sleep(100);//(Dispatch message)

	if(sentinel.m_settings.m_usStatusPort>0)
	{
//AfxMessageBox("Q1");
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = sentinel.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "SentinelXMLServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = SentinelXMLHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &sentinel;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &sentinel.m_net;					// pointer to the object with the Message function.

		if(sentinel.m_net.StartServer(pServer, &sentinel.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			sentinel.m_data.SetStatusText(errorstring, SENTINEL_STATUS_STATUSSVR_ERROR);
			sentinel.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Sentinel:xml_server_init", errorstring);  //(Dispatch message)
			sentinel.SendMsg(CX_SENDMSG_ERROR, "Sentinel:xml_server_init", errorstring);
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML server listening on %d", sentinel.m_settings.m_usStatusPort);
			sentinel.m_data.SetStatusText(errorstring, SENTINEL_STATUS_STATUSSVR_RUN);
			sentinel.m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:xml_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

//AfxMessageBox("QQ");

	sentinel.m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:build_check", "Build date: %s %s", __DATE__, __TIME__ );//(Dispatch message)
	
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Sentinel is connecting registered servers...");
	sentinel.m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:automation_server_init", errorstring);  Sleep(100);//(Dispatch message)
	sentinel.m_data.SetStatusText(errorstring, SENTINEL_STATUS_THREAD_SPARK);

	// now connect all the registered servers
	// this is the list of servers in the db that get connected


	if(!(sentinel.m_settings.m_ulMainMode&SENTINEL_MODE_CLONE))
	{
		if(g_psentinel->m_settings.m_bUseControlModules)
		{
			sentinel.m_data.GetControlModules();
		}

		// get connections from database - set up Harris API connections
		sentinel.m_data.GetConnections();
//	sentinel.m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "X2");  Sleep(250);//(Dispatch message)
		sentinel.m_data.GetChannels();
//	sentinel.m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "X3");  Sleep(250);//(Dispatch message)

		if(g_psentinel->m_settings.m_bUseRemoteChannels)
		{
			sentinel.m_data.GetRemoteChannels();
			sentinel.m_data.GetVirtualChannels();
		}
	}
//	sentinel.m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "X4");  Sleep(250);//(Dispatch message)
	
	if((sentinel.m_data.m_ulFlags&SENTINEL_ICON_MASK) != SENTINEL_STATUS_ERROR)
	{
		sentinel.m_data.m_ulFlags &= ~SENTINEL_ICON_MASK;
		sentinel.m_data.m_ulFlags |= SENTINEL_STATUS_OK;  // green - we want run to be blue when something in progress
	}


// initialize cortex (make this not persistent for now)
	CNetData* pdata = new CNetData;
	if(pdata)
	{

		pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA;      // defined type - indicates which protocol to use, structure of data

		pdata->m_ucCmd = CX_REQ_HELLO;       // the command byte
		pdata->m_ucSubCmd = 0;       // the subcommand byte

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
		if(pch)
		{
			sprintf(pch, "%s:%d:%d:%d:%d:%s", 
				sentinel.m_data.m_pszHost,
				sentinel.m_settings.m_usCommandPort,
				sentinel.m_settings.m_usStatusPort,
				CX_TYPE_RESOURCE,
				theApp.m_nPID,
				sentinel.m_settings.m_pszName?sentinel.m_settings.m_pszName:"Sentinel"
				);

			pdata->m_pucData =  (unsigned char*) pch;
			pdata->m_ulDataLen = strlen(pch);
		}

		SOCKET s = NULL; 
//AfxMessageBox("sending");
//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
//AfxMessageBox( (char*)pdata->m_pucData );
		if(sentinel.m_net.SendData(pdata, sentinel.m_data.m_pszCortexHost, sentinel.m_data.m_usCortexCommandPort, 5000, 0, NET_SND_CMDTOSVR, &s)>=NET_SUCCESS)
		{
			//send ack
//			AfxMessageBox("ack");
			sentinel.m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);
		}
		else
		{
//			AfxMessageBox("could not send");
		}
		sentinel.m_net.CloseConnection(s);

		try{delete pdata;} catch(...){}

	}


	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Sentinel main thread running.");  
	sentinel.m_data.SetStatusText(errorstring, SENTINEL_STATUS_THREAD_RUN);
//	sentinel.m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", errorstring);  Sleep(250);//(Dispatch message)
	sentinel.SendMsg(CX_SENDMSG_INFO, "Sentinel:init", "Sentinel %s main thread running.", SENTINEL_CURRENT_VERSION);

	_timeb timebCheckMods;
	_ftime( &timebCheckMods );
	_timeb timebRemote;
	_ftime( &timebRemote );
///AfxMessageBox("xxxxx");

	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &sentinel.m_data.m_timebTick );
//	sentinel.m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%d.%03d", sentinel.m_data.m_timebTick.time, sentinel.m_data.m_timebTick.millitm);   Sleep(250);//(Dispatch message)

// monitor changes in exchange db and react.
		if(
					(sentinel.m_data.m_timebTick.time > timebCheckMods.time )
				||((sentinel.m_data.m_timebTick.time == timebCheckMods.time)&&(sentinel.m_data.m_timebTick.millitm >= timebCheckMods.millitm))
				&&(!g_bKillThread)
//				&&(!sentinel.m_data.m_bProcessSuspended)  // cant do this here MUST! be able to call CheckDatabaseMods inside. 
			)
		{
//	sentinel.m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "time to check");  Sleep(250);//(Dispatch message)
			timebCheckMods.time = sentinel.m_data.m_timebTick.time + sentinel.m_settings.m_ulModsIntervalMS/1000; 
			timebCheckMods.millitm = sentinel.m_data.m_timebTick.millitm + (unsigned short)(sentinel.m_settings.m_ulModsIntervalMS%1000); // fractional second updates
			if(timebCheckMods.millitm>999)
			{
				timebCheckMods.time++;
				timebCheckMods.millitm%=1000;
			}
//	sentinel.m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "checking db connected");   Sleep(250);//(Dispatch message)
			if(pdbConn)//&&(pdbConn->m_bConnected))  // allow the checkmods call to reconnect if nec
			{
//AfxMessageBox("x");
//sentinel.m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "checkmods %d", sentinel.m_data.m_bCheckModsWarningSent);  //(Dispatch message)
				strcpy(errorstring, "");

				if(sentinel.m_data.CheckDatabaseMods(errorstring)==SENTINEL_ERROR)
				{
					if(!sentinel.m_data.m_bCheckModsWarningSent)
					{
						sentinel.m_msgr.DM(MSG_ICONERROR, NULL, "CheckDatabaseMods", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
						sentinel.m_data.m_bCheckModsWarningSent=true;
					}
				}
				else
				{
					if(sentinel.m_data.m_bCheckModsWarningSent)
					{
						sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "CheckDatabaseMods", "Resumed checking."); // Sleep(50); //(Dispatch message)
					}
					sentinel.m_data.m_bCheckModsWarningSent=false;
				}

				if(sentinel.m_data.m_timebTick.time > sentinel.m_data.m_timebAutoPurge.time + sentinel.m_settings.m_nAutoPurgeInterval)
				{
					_ftime(&sentinel.m_data.m_timebAutoPurge);
					if(sentinel.m_settings.m_nAutoPurgeMessageDays>0)
					{
						if(sentinel.m_data.CheckMessages(errorstring)==SENTINEL_ERROR)
						{
							if(!sentinel.m_data.m_bCheckMsgsWarningSent)
							{
								sentinel.m_msgr.DM(MSG_ICONERROR, NULL, "CheckMessages", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								sentinel.m_data.m_bCheckMsgsWarningSent=true;
							}
						}
						else
						if(sentinel.m_data.m_bCheckMsgsWarningSent)
						{
							sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "CheckMessages", "Resumed checking messages for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						sentinel.m_data.m_bCheckMsgsWarningSent=false;
					}

	
					if((sentinel.m_settings.m_nAutoPurgeAsRunDays>0)&&(g_psentinel->m_settings.m_bUseControlModules)) // as run is just for control modules
					{
						if(sentinel.m_data.CheckAsRun(errorstring)==SENTINEL_ERROR)
						{
							if(!sentinel.m_data.m_bCheckAsRunWarningSent)
							{
								sentinel.m_msgr.DM(MSG_ICONERROR, NULL, "CheckAsRun", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								sentinel.m_data.m_bCheckAsRunWarningSent=true;
							}
						}
						else
						if(sentinel.m_data.m_bCheckMsgsWarningSent)
						{
							sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "CheckAsRun", "Resumed checking as-run for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						sentinel.m_data.m_bCheckAsRunWarningSent=false;
					}

	
				}
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "%d", clock() ); // Sleep(50); //(Dispatch message)


				if(!sentinel.m_data.m_bProcessSuspended)
				{
					if(sentinel.m_data.m_nSettingsMod != sentinel.m_data.m_nLastSettingsMod)
					{
						if(sentinel.m_settings.GetFromDatabase()>=SENTINEL_SUCCESS)
						{
							sentinel.m_data.m_nLastSettingsMod = sentinel.m_data.m_nSettingsMod;
							sentinel.m_data.SetThreads(errorstring);

							// check for stuff to change

							// network messaging
							if(sentinel.m_settings.m_bLogNetworkErrors) 
							{
								if(!sentinel.m_data.m_bNetworkMessagingInitialized)
								{
									if(sentinel.m_net.InitializeMessaging(&sentinel.m_msgr)==0)
									{
										sentinel.m_data.m_bNetworkMessagingInitialized=true;
									}
								}
							}
							else
							{
								if(sentinel.m_data.m_bNetworkMessagingInitialized)
								{
									sentinel.m_net.UninitializeMessaging();  // void return
									sentinel.m_data.m_bNetworkMessagingInitialized=false;
								}
							}

							// logging and email messaging:

							if(!sentinel.m_settings.m_bUseEmail)
							{
								if(bUseEmail)
								{
									bUseEmail = false;
									// reset it
									sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "Shutting down email functions.");  //(Dispatch message)

			//						Sleep(250); // let the message get there.
									sentinel.m_msgr.RemoveDestination("email");
								}
							}
							else
							{
								if(!bUseEmail)
								{
									bUseEmail = true;
									int nRegisterCode=0;

									// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
									nRegisterCode = sentinel.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
										"email", 
										sentinel.m_settings.m_pszProcessedMailSpec?sentinel.m_settings.m_pszProcessedMailSpec:sentinel.m_settings.m_pszMailSpec,
										errorstring);
									if (nRegisterCode != MSG_SUCCESS) 
									{
										// inform the windowing environment
							//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
										sentinel.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
										sentinel.m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:smtp_reinit", errorstring);  //(Dispatch message)
									}
								}
								else
								{ // check for change
									int nIndex=sentinel.m_msgr.GetDestIndex("email");
									if(nIndex>=0)
									{
										if((sentinel.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(sentinel.m_settings.m_pszProcessedMailSpec?sentinel.m_settings.m_pszProcessedMailSpec:sentinel.m_settings.m_pszMailSpec))
										{
											if(strcmp(sentinel.m_msgr.m_ppDest[nIndex]->m_pszParams, (sentinel.m_settings.m_pszProcessedMailSpec?sentinel.m_settings.m_pszProcessedMailSpec:sentinel.m_settings.m_pszMailSpec)))
											{
												int nRegisterCode=0;

												// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
												nRegisterCode = sentinel.m_msgr.ModifyDestination(
													"email", 
													sentinel.m_settings.m_pszProcessedMailSpec?sentinel.m_settings.m_pszProcessedMailSpec:sentinel.m_settings.m_pszMailSpec,
													MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
													errorstring);
												if (nRegisterCode != MSG_SUCCESS) 
												{
													// inform the windowing environment
										//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

													_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify SMTP object!\n code: %d", nRegisterCode); 
													//sentinel.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
													sentinel.m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:smtp_change", errorstring);  //(Dispatch message)
												}
											}
										}
									}
								}
							}

							if(!sentinel.m_settings.m_bUseLog)
							{
								if(bUseLog)
								{
									bUseLog = false;
									// reset it
									sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

									Sleep(250); // let the message get there.
									sentinel.m_msgr.RemoveDestination("log");
								}
							}
							else
							{
								if(!bUseLog)
								{
									bUseLog = true;
									int nRegisterCode=0;

									nRegisterCode = sentinel.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
										"log", 
										sentinel.m_settings.m_pszProcessedFileSpec?sentinel.m_settings.m_pszProcessedFileSpec:sentinel.m_settings.m_pszFileSpec, 
										errorstring);
									if (nRegisterCode != MSG_SUCCESS) 
									{
										// inform the windowing environment
							//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
										sentinel.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_LOG|CX_STATUS_ERROR));
										sentinel.m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:log_reinit", errorstring);  //(Dispatch message)
									}
								}
								else
								{ // check for change
									int nIndex=sentinel.m_msgr.GetDestIndex("log");
									if(nIndex>=0)
									{
										if((sentinel.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(sentinel.m_settings.m_pszProcessedFileSpec?sentinel.m_settings.m_pszProcessedFileSpec:sentinel.m_settings.m_pszFileSpec))
										{
											if(strcmp(sentinel.m_msgr.m_ppDest[nIndex]->m_pszParams, (sentinel.m_settings.m_pszProcessedFileSpec?sentinel.m_settings.m_pszProcessedFileSpec:sentinel.m_settings.m_pszFileSpec)))
											{
												int nRegisterCode=0;

												// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
												nRegisterCode = sentinel.m_msgr.ModifyDestination(
													"log", 
													sentinel.m_settings.m_pszProcessedFileSpec?sentinel.m_settings.m_pszProcessedFileSpec:sentinel.m_settings.m_pszFileSpec,
													MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
													errorstring);
												if (nRegisterCode != MSG_SUCCESS) 
												{
													// inform the windowing environment
										//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

													_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify log object!\n code: %d", nRegisterCode); 
													//sentinel.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
													sentinel.m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:log_change", errorstring);  //(Dispatch message)
												}
											}
										}
									}
								}
							}


/// now add control modules.

						// have to go thru them and grab any settings and feed them in.
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "entering extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)

//AfxMessageBox("settings change");
			EnterCriticalSection(&g_psentinel->m_data.m_critExternalModules);

//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "entered extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)

							if((g_psentinel->m_settings.m_bUseControlModules)&&(g_psentinel->m_data.m_ppControlObj)&&(g_psentinel->m_data.m_nNumControlObjects>0))
							{
//AfxMessageBox("module change");

								CDBUtil dbCMS;
								CDBconn* pdbConnCMS = dbCMS.CreateNewConnection(g_psentinel->m_settings.m_pszDSN, g_psentinel->m_settings.m_pszUser, g_psentinel->m_settings.m_pszPW);
								if(pdbConnCMS)
								{
									if(dbCMS.ConnectDatabase(pdbConnCMS, errorstring)<DB_SUCCESS)
									{
										g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:database_connect(control_module_settings)", errorstring);  //(Dispatch message)
									}
									else
									{

										int i=0;
										while(i<g_psentinel->m_data.m_nNumControlObjects)
										{
											if(
													(g_psentinel->m_data.m_ppControlObj[i])
												&&(g_psentinel->m_data.m_ppControlObj[i]->m_hinstDLL)
												&&(g_psentinel->m_data.m_ppControlObj[i]->m_lpfnDllCtrl)
												&&(g_psentinel->m_data.m_ppControlObj[i]->m_ulFlags&SENTINEL_FLAG_ENABLED)  // only enabled ones
												)
											{
												char szSQL[DB_SQLSTRING_MAXLEN];
												char dberrorstring[DB_ERRORSTRING_LEN];

												// retrieve any settings for this thing

/*
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT category_display_name, value, parameter_display_name, min_len, \
max_len, pattern, min_value, max_value, description, sys_only  FROM %s WHERE category = '�%d�%s' AND parameter = '%s'",
				(g_psentinel->m_settings.m_pszSettings?g_psentinel->m_settings.m_pszSettings:"Settings"),
				ppObj[o]->m_nID, 
				ppSettings[d]->m_psz_category_vc64, 
				ppSettings[d]->m_psz_parameter_vc64
				);
*/
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT category, parameter, value FROM %s WHERE category LIKE '�%d�%%'",
													(g_psentinel->m_settings.m_pszSettings?g_psentinel->m_settings.m_pszSettings:"Settings"),
													g_psentinel->m_data.m_ppControlObj[i]->m_nID
													);
	//AfxMessageBox("retrieving stuff");

												CCortexDBSetting** ppSettings = NULL;
												int nNumSettings = 0 ;

												CRecordset* prsset = dbCMS.Retrieve(pdbConnCMS, szSQL, dberrorstring);
												if(prsset)
												{

													while(!prsset->IsEOF())
													{
														try
														{
															CCortexDBSetting** ppNewSettings = new CCortexDBSetting*[nNumSettings+1];

															if(ppNewSettings)
															{
																if(ppSettings)
																{
																	int s=0;
																	while(s<nNumSettings)
																	{
																		ppNewSettings[s] = ppSettings[s];
																		s++;
																	}

																	try{ delete [] ppSettings; }catch(...){}
																}
																ppSettings = ppNewSettings;


																CCortexDBSetting* pSettingObj = new CCortexDBSetting;
																if(pSettingObj)
																{
																	ppSettings[nNumSettings] = pSettingObj;

																	CString szTemp;
																	prsset->GetFieldValue("category", szTemp);
																	ppSettings[nNumSettings]->m_psz_category_vc64 = (char*)malloc(szTemp.GetLength()+1);
																	int nMarker = szTemp.ReverseFind('�');  // must exist since it is in the query
																	if(ppSettings[nNumSettings]->m_psz_category_vc64) sprintf(ppSettings[nNumSettings]->m_psz_category_vc64, "%s", szTemp.Mid(nMarker+1));
																	
																	prsset->GetFieldValue("parameter", szTemp);
																	ppSettings[nNumSettings]->m_psz_parameter_vc64 = (char*)malloc(szTemp.GetLength()+1);
																	if(ppSettings[nNumSettings]->m_psz_parameter_vc64) sprintf(ppSettings[nNumSettings]->m_psz_parameter_vc64, "%s", szTemp);

																	prsset->GetFieldValue("value", szTemp);
																	ppSettings[nNumSettings]->m_psz_value_vc256 = (char*)malloc(szTemp.GetLength()+1);
																	if(ppSettings[nNumSettings]->m_psz_value_vc256) sprintf(ppSettings[nNumSettings]->m_psz_value_vc256, "%s", szTemp);

																	nNumSettings++;
																}
															}
														}
														catch(...)
														{
														}	
														
														prsset->MoveNext();
															
													}
													prsset->Close();
													try{ delete prsset; } catch(...){}
												}

													// now assemble settings buffer
												if((ppSettings)&&(nNumSettings>0))
												{
													char* pch = NULL;  // just for safety

													int d=0;
													int nLen = 0;
													while(d<nNumSettings)
													{
														strcpy(szSQL, "");
														if(ppSettings[d])
														{
															_snprintf(szSQL, DB_SQLSTRING_MAXLEN, "<setting><category>%s</category><parameter>%s</parameter><value>%s</value></setting>", 
																ppSettings[d]->m_psz_category_vc64?ppSettings[d]->m_psz_category_vc64:"", 
																ppSettings[d]->m_psz_parameter_vc64?ppSettings[d]->m_psz_parameter_vc64:"", 
																ppSettings[d]->m_psz_value_vc256?ppSettings[d]->m_psz_value_vc256:"");
														
														}
														
														int nRec = strlen(szSQL);
														if(nRec)
														{
															char* pchNew = (char*)malloc(nLen + nRec + 1);
															if(pchNew)
															{
																if(pch)
																{
																	memcpy(pchNew, pch, nLen);
																	try {free(pch);} catch(...){}
																}
																memcpy(pchNew+nLen, szSQL, nRec);
																nLen+=nRec;
																*(pchNew+nLen) = 0;

																pch = pchNew;
															}
														}
														d++;
													}

													int nCode = g_psentinel->m_data.m_ppControlObj[i]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_SET_SETTINGS);
													if(nCode<SENTINEL_SUCCESS)
													{
														/// do something, report?
														_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d setting module settings in module %s: %s (%s)", nCode,
																			g_psentinel->m_data.m_ppControlObj[i]->m_pszModule, g_psentinel->m_data.m_ppControlObj[i]->m_pszName,g_psentinel->m_data.m_ppControlObj[i]->m_pszDesc );
														g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_settings", errorstring);    //(Dispatch message)
													}

													if(pch)
													{
														try{ free(pch); }
														catch(...) {}
													}

													// done with the settings, so delete:

													while(d<nNumSettings)
													{
														if(ppSettings[d])
														{
															try{ delete ppSettings[d]; } catch(...){}
														}
														d++;
													}
													try{ delete [] ppSettings; } catch(...){}

												}

											}
										
											i++;
										}

									}
								
									dbCMS.RemoveConnection(pdbConnCMS);
	
								}
								else
								{
										g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:database_new_connect(control_module_settings)", "Could not create new database connection.");  //(Dispatch message)
								}
							}
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "leaving extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
			LeaveCriticalSection(&g_psentinel->m_data.m_critExternalModules);
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "left extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)

						} //if(sentinel.m_settings.GetFromDatabase()>=SENTINEL_SUCCESS)
						
					}
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "process chk %d", clock() ); // Sleep(50); //(Dispatch message)

					if(
							(sentinel.m_data.m_key.m_bValid)  // must have a valid license
						&&(
								(!sentinel.m_data.m_key.m_bExpires)
							||((sentinel.m_data.m_key.m_bExpires)&&(!sentinel.m_data.m_key.m_bExpired))
							||((sentinel.m_data.m_key.m_bExpires)&&(sentinel.m_data.m_key.m_bExpireForgiveness)&&(sentinel.m_data.m_key.m_ulExpiryDate+sentinel.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
							)
						&&(
								(!sentinel.m_data.m_key.m_bMachineSpecific)
							||((sentinel.m_data.m_key.m_bMachineSpecific)&&(sentinel.m_data.m_key.m_bValidMAC))
							)
						)
					{
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:control_modules", "checking mods: current:%d last:%d", sentinel.m_data.m_nControllersMod, sentinel.m_data.m_nLastControllersMod ); // Sleep(50); //(Dispatch message)

						if((g_psentinel->m_settings.m_bUseControlModules)&&(sentinel.m_data.m_nControllersMod != sentinel.m_data.m_nLastControllersMod))
						{

//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:control_modules", "GetControlModules()" ); // Sleep(50); //(Dispatch message)
		//			sentinel.m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "getting connections again");   Sleep(250);//(Dispatch message)
							if(sentinel.m_data.GetControlModules()>=SENTINEL_SUCCESS)
							{
								sentinel.m_data.m_nLastControllersMod = sentinel.m_data.m_nControllersMod;
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:control_modules", "assigned %d %d", sentinel.m_data.m_nLastControllersMod, sentinel.m_data.m_nControllersMod ); // Sleep(50); //(Dispatch message)
							}
						}

						if(sentinel.m_data.m_nConnectionsMod != sentinel.m_data.m_nLastConnectionsMod)
						{
		//			sentinel.m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "getting connections again");   Sleep(250);//(Dispatch message)
							if(sentinel.m_data.GetConnections()>=SENTINEL_SUCCESS)
							{
								sentinel.m_data.m_nLastConnectionsMod = sentinel.m_data.m_nConnectionsMod;
							}
						}
						if(sentinel.m_data.m_nChannelsMod != sentinel.m_data.m_nLastChannelsMod)
						{
		//			sentinel.m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "getting channels again");   Sleep(250);//(Dispatch message)
							if(sentinel.m_data.GetChannels()>=SENTINEL_SUCCESS)
							{
								sentinel.m_data.m_nLastChannelsMod = sentinel.m_data.m_nChannelsMod;
							}
						}

						if((g_psentinel->m_settings.m_bUseRemoteChannels)&&(sentinel.m_data.m_nRemoteChannelsMod != sentinel.m_data.m_nLastRemoteChannelsMod))
						{
							if(sentinel.m_data.GetRemoteChannels()>=SENTINEL_SUCCESS)
							{
								sentinel.m_data.m_nLastRemoteChannelsMod = sentinel.m_data.m_nRemoteChannelsMod;
							}
						}

						if((g_psentinel->m_settings.m_bUseRemoteChannels)&&(sentinel.m_data.m_nVirtualChannelsMod != sentinel.m_data.m_nLastVirtualChannelsMod))
						{
							if(sentinel.m_data.GetVirtualChannels()>=SENTINEL_SUCCESS)
							{
								sentinel.m_data.m_nLastVirtualChannelsMod = sentinel.m_data.m_nVirtualChannelsMod;
							}
						}

					}
				}
			}
		}
//AfxMessageBox("zoinks");
		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();


		if(
				(sentinel.m_settings.m_bUseRemoteChannels)
			&&(sentinel.m_data.m_socketRemote == NULL)
			&&(sentinel.m_settings.m_pszRemoteSentinelHost != NULL)
			)
		{
			if(
						(sentinel.m_data.m_timebTick.time > timebRemote.time )
					||((sentinel.m_data.m_timebTick.time == timebRemote.time)&&(sentinel.m_data.m_timebTick.millitm >= timebRemote.millitm))
					&&(!g_bKillThread)
				)
			{

/*
		char* m_pszRemoteSentinelDescription;  // the Remote Sentinel Description
		char* m_pszRemoteSentinelHost;  // the Remote Sentinel Host name
		int m_nRemoteSentinelControlPort;  // the Remote Sentinel Control port
		int m_nRemoteSentinelXMLPort;  // the Remote Sentinel XML port
*/
				timebRemote = sentinel.m_data.m_timebTick;
				timebRemote.time+=10; // retry interval hard coded for now.

	EnterCriticalSection(&sentinel.m_data.m_critRemoteSocket);
				if(sentinel.m_data.m_net.OpenConnection(sentinel.m_settings.m_pszRemoteSentinelHost, 
					                                      sentinel.m_settings.m_nRemoteSentinelXMLPort, 
																								&sentinel.m_data.m_socketRemote
																							 ) < NET_SUCCESS)
				{
					sentinel.m_data.m_socketRemote = NULL; // just enforce
//					sentinel.m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%d.%03d", sentinel.m_data.m_timebTick.time, sentinel.m_data.m_timebTick.millitm); //(Dispatch message)
	LeaveCriticalSection(&sentinel.m_data.m_critRemoteSocket);
					sentinel.m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Error connecting to remote Sentinel at %s:%d", sentinel.m_settings.m_pszRemoteSentinelHost, sentinel.m_settings.m_nRemoteSentinelXMLPort); //(Dispatch message)
				}
				else
				{
	LeaveCriticalSection(&sentinel.m_data.m_critRemoteSocket);

					sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:remote_socket", "Connected to remote Sentinel at %s:%d", sentinel.m_settings.m_pszRemoteSentinelHost, sentinel.m_settings.m_nRemoteSentinelXMLPort); //(Dispatch message)
					sentinel.m_data.m_bMonitorSocket=false;
					while(sentinel.m_data.m_bSocketMonitorStarted) Sleep(10);
					sentinel.m_data.m_bMonitorSocket=true;
					_beginthread(SocketMonitorThread, 0, (void*)(&sentinel.m_data));

				}

			}
		}
		Sleep(5); 
//		Sleep(1000);  // stalls window procedure
	}

	sentinel.m_data.m_ulFlags &= ~SENTINEL_STATUS_THREAD_MASK;
	sentinel.m_data.m_ulFlags |= SENTINEL_STATUS_THREAD_END;



	sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:uninit", "Sentinel is shutting down.");  //(Dispatch message)
	sentinel.SendMsg(CX_SENDMSG_INFO, "Sentinel:uninit", "Sentinel %s is shutting down.", SENTINEL_CURRENT_VERSION);

	sentinel.m_data.m_bTrafficKillWatch=true;

	int nClock = clock() + SENTINEL_THREAD_MAX_DELAY; // delay if necessary
	while((clock()<nClock)&&(sentinel.m_data.m_bTrafficWatchStarted))
	{
		_ftime( &sentinel.m_data.m_timebTick );
		Sleep(10);
	}


	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Sentinel is shutting down.");  
	sentinel.m_data.m_ulFlags &= ~CX_ICON_MASK;
	sentinel.m_data.m_ulFlags |= CX_STATUS_UNKNOWN;
	sentinel.m_data.SetStatusText(errorstring, sentinel.m_data.m_ulFlags);


	sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:uninit", "Sentinel is shutting down the automation connections.");  //(Dispatch message)

	BOOL bDisconnected = FALSE;

	while(!bDisconnected)
	{
		_ftime( &sentinel.m_data.m_timebTick );
		EnterCriticalSection(&sentinel.m_data.m_critConns);
		if(sentinel.m_data.m_ppConnObj)
		{
			bDisconnected = TRUE;
			int i=0;
			while(i<sentinel.m_data.m_nNumConnectionObjects)
			{
				if(sentinel.m_data.m_ppConnObj[i])
				{
					if(sentinel.m_data.m_ppConnObj[i]->m_bConnThreadStarted)
					{
						bDisconnected = FALSE;
						break;
					}
				}

				i++;
			}
		}
		else
		{
			bDisconnected = TRUE;
		}

		LeaveCriticalSection(&sentinel.m_data.m_critConns);
		Sleep(10);
	}


	sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:uninit", "Sentinel is shutting down the control module service.");  //(Dispatch message)

//AfxMessageBox("shutting down control module service");

	sentinel.m_data.m_bKillControlModuleServiceThread = true;

	sentinel.m_data.m_bMonitorSocket=false;

	nClock = clock() + SENTINEL_THREAD_MAX_DELAY; // delay if necessary
	while((clock()<nClock)&&(sentinel.m_data.m_bControlModuleServiceThreadStarted))
	{
		_ftime( &sentinel.m_data.m_timebTick );
		Sleep(10);
	}


//AfxMessageBox("shutting down control registered modules");



// shut down all the running objects;
	sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:uninit", "Sentinel is shutting down registered modules.");  //(Dispatch message)
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "C entering extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)


	EnterCriticalSection(&sentinel.m_data.m_critExternalModules);
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "C entered extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
	if(sentinel.m_data.m_ppControlObj)
	{
		int nIndex = 0;
		while(nIndex<sentinel.m_data.m_nNumControlObjects)
		{
			if((sentinel.m_data.m_ppControlObj)&&(sentinel.m_data.m_ppControlObj[nIndex]))
			{
				{
					if(sentinel.m_data.m_ppControlObj[nIndex]->m_lpfnDllCtrl)
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending control for %s (%s)", 
													sentinel.m_data.m_ppControlObj[nIndex]->m_pszName,sentinel.m_data.m_ppControlObj[nIndex]->m_pszDesc );  
						g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
	//									sentinel.m_data.m_ppControlObj[nTemp]->m_pDlg->OnDisconnect();

						//**** disconnect
	//							sentinel.m_data.m_ppControlObj[nIndex]->m_bKillAutomationThread = true;

	/*
	#define DLLCMD_DEINIT					0x0020 // A signal to the DLL that it should run any internal de-initialization routines. After the DEINIT command is received the DLL should prepare to finalize any messaging back to the calling application.
	#define DLLCMD_MSGEND					0x0021 // A signal to the DLL that it should not send any further messages through any messaging callbacks, including as-run.
	#define DLLCMD_QUERY					0x0022 // A query to the DLL to ask it if it has finished deinitialization.  This command will repeat until a succesful return, or a de-init timeout has elapsed.
	#define DLLCMD_DLLEND					0x002f // A signal to the DLL that it is about to be unloaded.
	*/
						char* pch = (char*)malloc(64);
						if(pch)
						{
							sprintf(pch, "%d", sentinel.m_data.m_ppControlObj[nIndex]->m_nID);
						}
						char* pchOld = pch; 

						int nCode = sentinel.m_data.m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_DEINIT);
						if(nCode<SENTINEL_SUCCESS)
						{
							/// do something, report?
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d de-initializing module %s: %s (%s)", nCode,
												sentinel.m_data.m_ppControlObj[nIndex]->m_pszModule, sentinel.m_data.m_ppControlObj[nIndex]->m_pszName, sentinel.m_data.m_ppControlObj[nIndex]->m_pszDesc );
							g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
						}
						else
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Module %s: %s (%s) de-initialized with [%s] code %d", 
								sentinel.m_data.m_ppControlObj[nIndex]->m_pszModule, sentinel.m_data.m_ppControlObj[nIndex]->m_pszName, sentinel.m_data.m_ppControlObj[nIndex]->m_pszDesc, (((pch)&&(pch!=pchOld))?pch:""), nCode );
							g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
						}

						if((pch)&&(pch!=pchOld))
						{
							try{ sentinel.m_data.m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
							catch(...) {}
							pch = NULL;
							if(pchOld)
							{
								try{ free(pchOld); }
								catch(...) {}
								pchOld = NULL;
							}
						}
						if(pch)
						{
							try{ free(pch); }
							catch(...) {}
							pch = NULL;
						}

						pch = (char*)malloc(64);
						if(pch)
						{
							sprintf(pch, "%d", sentinel.m_data.m_ppControlObj[nIndex]->m_nID);
						}
						pchOld = pch; 

						nCode = sentinel.m_data.m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_MSGEND);
						if(nCode<SENTINEL_SUCCESS)
						{
							/// do something, report?
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d commanding message end in module %s: %s (%s)", nCode,
												sentinel.m_data.m_ppControlObj[nIndex]->m_pszModule, sentinel.m_data.m_ppControlObj[nIndex]->m_pszName, sentinel.m_data.m_ppControlObj[nIndex]->m_pszDesc );
							g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
						}
						else
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Module %s: %s (%s) ended messaging with [%s] code %d", 
								sentinel.m_data.m_ppControlObj[nIndex]->m_pszModule, sentinel.m_data.m_ppControlObj[nIndex]->m_pszName, sentinel.m_data.m_ppControlObj[nIndex]->m_pszDesc, (((pch)&&(pch!=pchOld))?pch:""), nCode );
							g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
						}

						if((pch)&&(pch!=pchOld))
						{
							try{ sentinel.m_data.m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
							catch(...) {}
							pch = NULL;
							if(pchOld)
							{
								try{ free(pchOld); }
								catch(...) {}
								pchOld = NULL;
							}
						}
						if(pch)
						{
							try{ free(pch); }
							catch(...) {}
							pch = NULL;
						}

						bool bReleased = false;
						_timeb tmChk;
						_ftime(&tmChk);

						if(sentinel.m_data.m_ppControlObj[nIndex]->m_nDeinitTimeoutMS>0)
						{
							tmChk.time += sentinel.m_data.m_ppControlObj[nIndex]->m_nDeinitTimeoutMS/1000;
							tmChk.millitm += sentinel.m_data.m_ppControlObj[nIndex]->m_nDeinitTimeoutMS%1000;
							while(tmChk.millitm>999)
							{
								tmChk.millitm-=1000;
								tmChk.time++;
							}
						}
						else
						{
							tmChk.time++;
						}

						_timeb tmNow;
						_ftime(&tmNow);

						do
						{

							pch = (char*)malloc(64);
							if(pch)
							{
								sprintf(pch, "%d", sentinel.m_data.m_ppControlObj[nIndex]->m_nID);
							}
							pchOld = pch; 

							int nCode = sentinel.m_data.m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_QUERY);
							if(nCode<SENTINEL_SUCCESS)
							{
								/// do something, report?
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d querying control end in module %s: %s (%s)", nCode,
													sentinel.m_data.m_ppControlObj[nIndex]->m_pszModule, sentinel.m_data.m_ppControlObj[nIndex]->m_pszName, sentinel.m_data.m_ppControlObj[nIndex]->m_pszDesc );
								g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
							}
							else
							{
								bReleased = true;
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Module %s: %s (%s) query end succeeded with [%s] code %d", 
									sentinel.m_data.m_ppControlObj[nIndex]->m_pszModule, sentinel.m_data.m_ppControlObj[nIndex]->m_pszName, sentinel.m_data.m_ppControlObj[nIndex]->m_pszDesc, (((pch)&&(pch!=pchOld))?pch:""), nCode );
								g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
							}

							if((pch)&&(pch!=pchOld))
							{
								try{ sentinel.m_data.m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
								catch(...) {}
								pch = NULL;
								if(pchOld)
								{
									try{ free(pchOld); }
									catch(...) {}
									pchOld = NULL;
								}
							}
							if(pch)
							{
								try{ free(pch); }
								catch(...) {}
								pch = NULL;
							}

							_timeb tmSleep;
							_ftime(&tmSleep);

							tmSleep.time +=	g_psentinel->m_settings.m_ulModuleEndQueryIntervalMS/1000;
							tmSleep.millitm +=  (unsigned short)(g_psentinel->m_settings.m_ulModuleEndQueryIntervalMS%1000);
							while(tmSleep.millitm>999)
							{
								tmSleep.millitm-=1000;
								tmSleep.time++;
							}
//							g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", "waiting %d->%d.%3d < %d.%3d   %d %d ", 
//								g_psentinel->m_settings.m_ulModuleEndQueryIntervalMS/1000, tmSleep.time, tmSleep.millitm, tmNow.time, tmNow.millitm, bReleased, g_bKillThread);    //(Dispatch message)
							while (
											(!bReleased)
										&&(
												(tmNow.time<tmSleep.time)
											||((tmNow.time==tmSleep.time)&&(tmNow.millitm<tmSleep.millitm))
											)
//															&&(!g_bKillThread) //don't do this on query end
										)
							{
//								AfxMessageBox("s");
//								g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:module_remove", "waiting %d->%d %d", g_psentinel->m_settings.m_ulModuleEndQueryIntervalMS/1000, tmSleep.time, tmNow.time);    //(Dispatch message)
								Sleep(1);
								_ftime(&tmNow);
							}
//								AfxMessageBox("q");

						}	while (
											(!bReleased)
										&&(
												(tmNow.time<tmChk.time)
											||((tmNow.time==tmChk.time)&&(tmNow.millitm<tmChk.millitm))
											)
										);

						//g_adc.DisconnectServer(sentinel.m_data.m_ppControlObj[nIndex]->m_pszServerName);
						//**** should check return value....


						pch = (char*)malloc(64);
						if(pch)
						{
							sprintf(pch, "%d", sentinel.m_data.m_ppControlObj[nIndex]->m_nID);
						}
						pchOld = pch; 

						nCode = sentinel.m_data.m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_DLLEND);
						if(nCode<SENTINEL_SUCCESS)
						{
							/// do something, report?
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d commanding control end in module %s: %s (%s)", nCode,
												sentinel.m_data.m_ppControlObj[nIndex]->m_pszModule, sentinel.m_data.m_ppControlObj[nIndex]->m_pszName, sentinel.m_data.m_ppControlObj[nIndex]->m_pszDesc );
							g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
						}
						else
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Module %s: %s (%s) ended control with [%s] code %d", 
								sentinel.m_data.m_ppControlObj[nIndex]->m_pszModule, sentinel.m_data.m_ppControlObj[nIndex]->m_pszName, sentinel.m_data.m_ppControlObj[nIndex]->m_pszDesc, (((pch)&&(pch!=pchOld))?pch:""), nCode );
							g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
						}

						if((pch)&&(pch!=pchOld))
						{
							try{ sentinel.m_data.m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
							catch(...) {}
							pch = NULL;
							if(pchOld)
							{
								try{ free(pchOld); }
								catch(...) {}
								pchOld = NULL;
							}
						}
						if(pch)
						{
							try{ free(pch); }
							catch(...) {}
							pch = NULL;
						}


						_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ended control for %s (%s)", 
												sentinel.m_data.m_ppControlObj[nIndex]->m_pszName,sentinel.m_data.m_ppControlObj[nIndex]->m_pszDesc );  
						g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
						

						try
						{
#pragma message(alert "this crashes, asserts on some machines even though try-catch.  must fix")
//							AfxFreeLibrary(sentinel.m_data.m_ppControlObj[nIndex]->m_hinstDLL);  // crashes!  not sure why - lets try again.
						}
						catch(...)
						{
						}

						sentinel.m_data.m_ppControlObj[nIndex]->m_hinstDLL = NULL;
						sentinel.m_data.m_ppControlObj[nIndex]->m_lpfnDllCtrl = NULL;

						Sleep(100); // let it settle

						try
						{
							delete sentinel.m_data.m_ppControlObj[nIndex];
						}
						catch(...)
						{
						}

						sentinel.m_data.m_nNumControlObjects--;

						int nTemp=nIndex;
						while(nTemp<sentinel.m_data.m_nNumControlObjects)
						{
							sentinel.m_data.m_ppControlObj[nTemp]=sentinel.m_data.m_ppControlObj[nTemp+1];
							nTemp++;
						}
						sentinel.m_data.m_ppControlObj[nTemp] = NULL;
					}
					else nIndex++;
				}
			}
			else
				nIndex++;
		}
		try
		{
			delete [] sentinel.m_data.m_ppControlObj;
		}
		catch(...)
		{
		}

	}
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "C leaving extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
	LeaveCriticalSection(&sentinel.m_data.m_critExternalModules);
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "C left extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)


//AfxMessageBox("completed shutting down control registered modules");


	/*
// don't need this because shutting the connections down kills the channels
	sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:uninit", "Sentinel is shutting down channel management.");  //(Dispatch message)
	EnterCriticalSection(&sentinel.m_data.m_critChannels);

	if(sentinel.m_data.m_ppChannelObj)
	{
		while(sentinel.m_data.m_nNumChannelObjects>0)
		{
			int i = sentinel.m_data.m_nNumChannelObjects-1;
			if(sentinel.m_data.m_ppChannelObj[i])
			{
				sentinel.m_data.m_ppChannelObj[i]->m_ulFlags = SENTINEL_FLAG_DISABLED; // so killing thread wont start it up again
				sentinel.m_data.m_ppChannelObj[i]->m_bKillChannelThread = true;
				
				while(sentinel.m_data.m_ppChannelObj[i]->m_bChannelThreadStarted) Sleep(10);//(may be dangerous stall here)

				sentinel.m_data.m_nNumChannelObjects--;

				try{ delete  sentinel.m_data.m_ppChannelObj[i];} catch(...){}
				sentinel.m_data.m_ppChannelObj[i] = NULL;
			}
		}
		try{delete [] sentinel.m_data.m_ppChannelObj;} catch(...){}
		sentinel.m_data.m_ppChannelObj = NULL;
		sentinel.m_data.m_nNumChannelObjects=0;
	}
	LeaveCriticalSection(&sentinel.m_data.m_critChannels);
*/

/* and done need this either, because killing the app kills the connection threads
	sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:uninit", "Sentinel is shutting down connection management.");  //(Dispatch message)
	if(sentinel.m_data.m_ppConnObj)
	{
		while(sentinel.m_data.m_nNumConnectionObjects>0)
		{
			int i = sentinel.m_data.m_nNumConnectionObjects-1;
			if(sentinel.m_data.m_ppConnObj[i])
			{
				// **** disconnect servers
				sentinel.m_data.m_ppConnObj[i]->m_ulFlags = SENTINEL_FLAG_DISABLED; // so killing thread wont start it up again
				sentinel.m_data.m_ppConnObj[i]->m_bKillConnThread = true;

				if((sentinel.m_data.m_ppConnObj[i]->m_ulStatus&SENTINEL_ICON_MASK) == SENTINEL_STATUS_CONN)
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", sentinel.m_data.m_ppConnObj[i]->m_pszDesc?sentinel.m_data.m_ppConnObj[i]->m_pszDesc:sentinel.m_data.m_ppConnObj[i]->m_pszServerName);  
					sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:connection_uninit", errorstring);    //(Dispatch message)
					sentinel.m_data.SetStatusText(errorstring, sentinel.m_data.m_ulFlags);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Removing %s...", sentinel.m_data.m_ppConnObj[i]->m_pszDesc?sentinel.m_data.m_ppConnObj[i]->m_pszDesc:sentinel.m_data.m_ppConnObj[i]->m_pszServerName);  
					sentinel.m_data.SetStatusText(errorstring, sentinel.m_data.m_ulFlags);
				}


				while(sentinel.m_data.m_ppConnObj[i]->m_bConnThreadStarted) Sleep(50);  //(may be dangerous stall here)

				sentinel.m_data.m_nNumConnectionObjects--;

				try{ delete sentinel.m_data.m_ppConnObj[i];} catch(...){}
				sentinel.m_data.m_ppConnObj[i] = NULL;
			}
		}
		try{ delete [] sentinel.m_data.m_ppConnObj;} catch(...){}
		sentinel.m_data.m_nNumConnectionObjects = 0;
		sentinel.m_data.m_ppConnObj=NULL;
	}
*/
	sentinel.m_data.SetLastUID();  // record the last UID
	// requires that the reocrd be there.

	sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:uninit", "Sentinel is shutting down the working set.");  //(Dispatch message)
	nClock = clock() + SENTINEL_THREAD_MAX_DELAY; // delay if necessary
	while((clock()<nClock)&&(sentinel.m_data.m_bWorkingSetThreadStarted))
	{
		_ftime( &sentinel.m_data.m_timebTick );
		Sleep(10);
	}
		



/*
	// here's a hard coded one for now
	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no need to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = sentinel.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		sentinel.m_net.OpenConnection(sentinel.m_http.m_pszHost, 10888, &s); // branding hardcode
		sentinel.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		sentinel.m_net.CloseConnection(s); // branding hardcode
	}
*/

//	m_pDlg->SetProgress(SENTINELDLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
//	sentinel.m_data.m_ulFlags &= ~SENTINEL_STATUS_FILESVR_MASK;
//	sentinel.m_data.m_ulFlags |= SENTINEL_STATUS_FILESVR_END;
//	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down file server....");  
//	sentinel.m_data.SetStatusText(errorstring, sentinel.m_data.m_ulFlags);
//	_ftime( &sentinel.m_data.m_timebTick );
//	sentinel.m_http.EndServer();
	_ftime( &sentinel.m_data.m_timebTick );
//	AfxMessageBox("shutting down command server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down command server....");
	sentinel.m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:command_server_uninit", errorstring);   Sleep(100);  //(Dispatch message)
	sentinel.m_data.SetStatusText(errorstring, SENTINEL_STATUS_CMDSVR_END);
	_ftime( &sentinel.m_data.m_timebTick );
	sentinel.m_net.StopServer(sentinel.m_settings.m_usCommandPort, 5000, errorstring);
	_ftime( &sentinel.m_data.m_timebTick );
//	AfxMessageBox("shutting down status server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down XML server....");  
	sentinel.m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:xml_server_uninit", errorstring);   Sleep(100);  //(Dispatch message)
	sentinel.m_data.SetStatusText(errorstring, SENTINEL_STATUS_STATUSSVR_END);
	_ftime( &sentinel.m_data.m_timebTick );
	sentinel.m_net.StopServer(sentinel.m_settings.m_usStatusPort, 5000, errorstring);
	_ftime( &sentinel.m_data.m_timebTick );
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Sentinel is exiting.");  
	sentinel.m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:uninit", errorstring);   Sleep(100);  //(Dispatch message)
	sentinel.m_data.SetStatusText(errorstring, sentinel.m_data.m_ulFlags);

//AfxMessageBox("ended some other things");


	// save settings.  // dont save them here.  save them on any changes in the main command loop.

	sentinel.m_settings.Settings(false); //write

//AfxMessageBox("wrote some settings out");

/*
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// these explicts arent necessary - uncomment to write out a full file to edit...
		file.SetIniString("Main", "Name", sentinel.m_settings.m_pszName);
		file.SetIniString("License", "Key", sentinel.m_settings.m_pszLicense);

		file.SetIniString("FileServer", "IconPath", sentinel.m_settings.m_pszIconPath);
		file.SetIniInt("CommandServer", "ListenPort", sentinel.m_settings.m_usCommandPort);
		file.SetIniInt("StatusServer", "ListenPort", sentinel.m_settings.m_usStatusPort);

		file.SetIniInt("Messager", "UseEmail", sentinel.m_settings.m_bUseEmail?1:0);
		file.SetIniInt("Messager", "UseNet", sentinel.m_settings.m_bUseNetwork?1:0);
		file.SetIniInt("Messager", "UseLog", sentinel.m_settings.m_bUseLog?1:0);

		file.SetIniString("Database", "DSN", sentinel.m_settings.m_pszDSN);
		file.SetIniString("Database", "DBUser", sentinel.m_settings.m_pszUser);
		file.SetIniString("Database", "DBPassword", sentinel.m_settings.m_pszPW);
		file.SetIniString("Database", "SettingsTableName", sentinel.m_settings.m_pszSettings);  // the Settings table name
		file.SetIniString("Database", "ExchangeTableName", sentinel.m_settings.m_pszExchange);  // the Exchange table name
		file.SetIniString("Database", "MessagesTableName", sentinel.m_settings.m_pszMessages);  // the Messages table name

		file.SetIniInt("HarrisAPI", "UseListCount", sentinel.m_settings.m_bUseListCount?1:0); // get all events up until the list count (otherwise just up to the lookahead)
		file.SetIniInt("HarrisAPI", "EventsPerRequest", sentinel.m_settings.m_ulEventsPerRequest); //  number of events to request at one time, up until lookahead or listcount (chunking for multilists, 0= use full count at once)

		file.SetIniString("Database", "ChannelsTableName", sentinel.m_settings.m_pszChannels);  // the Channels table name
		file.SetIniString("Database", "ConnectionsTableName", sentinel.m_settings.m_pszConnections);  // the Connections table name
		file.SetIniString("Database", "LiveEventsTableName", sentinel.m_settings.m_pszLiveEvents);  // the LiveEvents table name
		file.SetIniString("Database", "TempLiveEventsTableName", sentinel.m_settings.m_pszLiveEventsTemp);  // the LiveEvents table name

		file.SetIniInt("Database", "ModificationCheckInterval", sentinel.m_settings.m_ulModsIntervalMS);  // in milliseconds
		file.SetIniInt("Database", "ServerStatusDBIntervalMS", sentinel.m_settings.m_ulServerStatusDBIntervalMS );  // in milliseconds
		file.SetIniInt("Database", "ListStatusToDB", 	sentinel.m_settings.m_bListStatusToDB?1:0);

		file.SetSettings(SENTINEL_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

	}
*/

	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Sentinel is exiting");
	sentinel.m_data.m_ulFlags &= ~CX_ICON_MASK;
	sentinel.m_data.m_ulFlags |= CX_STATUS_UNINIT;
	sentinel.m_data.SetStatusText(errorstring, sentinel.m_data.m_ulFlags);


	//exiting
	sentinel.m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "-------------- Sentinel %s exit ---------------\n\
--------------------------------------------------\n", SENTINEL_CURRENT_VERSION);  //(Dispatch message)
///	m_pDlg->SetProgress(SENTINELDLG_CLEAR); // no point

	_ftime( &sentinel.m_data.m_timebTick );
	sentinel.m_data.m_ulFlags &= ~SENTINEL_STATUS_THREAD_MASK;
	sentinel.m_data.m_ulFlags |= SENTINEL_STATUS_THREAD_ENDED;

//	((CSentinelDlg*)((CSentinelHandler*)theApp.m_pMainWnd)->m_pMainDlg)->KillTimer(2);
//	((CSentinelDlg*)((CSentinelHandler*)theApp.m_pMainWnd)->m_pMainDlg)->KillTimer(3);
	g_bKillStatus = true;
	Sleep(250); // let the message get there.
	sentinel.m_msgr.RemoveDestination("log");
	sentinel.m_msgr.RemoveDestination("email");
	sentinel.m_msgr.m_bKillThread = true; // do this early to give it some time to finish out.

//AfxMessageBox("removing db connection");

	db.RemoveConnection(pdbConn);
//AfxMessageBox("dwell 1");
	
	nClock = clock() + 250; // small delay at end
	while(clock()<nClock)	{_ftime( &sentinel.m_data.m_timebTick );}
	g_bThreadStarted = false;
//AfxMessageBox("setting global pointer to NULL");
//AfxMessageBox("set global pointer to NULL");
	nClock = clock() + sentinel.m_settings.m_nThreadDwellMS; // another small delay at end
	while(clock()<nClock)	{_ftime( &sentinel.m_data.m_timebTick );}
//AfxMessageBox("dwell 2");
	g_psentinel = NULL;
	Sleep(250); //one more small delay at end
//AfxMessageBox("ending thread");
	_endthread();
}

/*
void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((g_pmsgr)&&(g_pmsgr->m_lpfnDM))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		CMessagingObject msg;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = msg.EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = msg.EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = msg.EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch;
		pch = (char*) malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
* /
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if(pszEncodedMsg) 
			{
				ulBufferLen = strlen(pszEncodedMsg);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedCaller)
			{
				ulBufferLen = strlen(pszEncodedCaller);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedDest)
			{
				ulBufferLen = strlen(pszEncodedDest);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			g_pmsgr->m_lpfnDM(pch, ulFlags);
			free(pch);
		}
	}
}
*/


/*
// this thread is based on void HTTP10HandlerThread(void* pvArgs) from
// HTTP10.cpp.  It makes a special webserver with security, cgi parsing, and dynamic results
void SentinelHTTPThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return;}

	CSentinelMain* pSentinel = (CSentinelMain*)(pClient->m_lpObject) ;  // pointer to the global object.

// HTTP service by def is a non-persistent service, so even if the Client type is
// set to persist, we ignore that directive in this handler thread

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;


		CHTTP10* phttp = (CHTTP10*)(&(pSentinel->m_http)) ;  // pointer to the global object.
		if(phttp == NULL)// cant do anything!
		{
			shutdown(pClient->m_socket, SD_BOTH);
			closesocket(pClient->m_socket);
			(*(pClient->m_pulConnections))--;
			_endthread(); return;
		}

		CHTTP10 http(phttp->m_pszRoot);  // local object for utility functions ONLY.
		// this allows the local to clone the root setting of the global, 
		// in order to do correct parsing in its own thread

		// the following are just pointers to the global obj.  
		// must NULL these out before object gets destroyed, 
		// so that global resources not freed!   !!!!!!!  important !!!!!
		http.m_pszHost = phttp->m_pszHost;
		http.m_ppszErrors = phttp->m_ppszMime;
		http.m_ppszMime = phttp->m_ppszMime;   
		http.m_ulStatus = phttp->m_ulStatus;   // important for redirect and error
		
		unsigned char* pch;// for use with GetLine().
		unsigned long ulBufLen;// for use with GetLine().
		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		unsigned short usCode = 0;

		// get the raw buffer and process it yourself
		nReturn = http.m_net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_EOLN|EOLN_HTTP, pszStatus);
		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). 
			//was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
		{
			//error.
			if(pClient->m_lpMsgObj)
			{
				_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error receiving data.  %s", pszStatus);
				((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
			}

			usCode = 400;  //  we TRY to send this....
			if(pch!=NULL)
			{
				free(pch);  // must free the incoming data buffer 
				pch = NULL;
			}
			// create a return message buffer.
			pch = (unsigned char*)http.ErrorBuffer(NULL, usCode);   // 400 bad request
			ulBufLen = strlen((char*)pch); // valid from an error page.

			// send malformed request answer if possible  - have to tell the client that the request was not received in good order.
			nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
			if(nReturn<NET_SUCCESS)
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
				}
			}
		}
		else  // successful reception of data.
		{
			// process the data here...
			// in this sample the buffer is loaded into
			// pch, with ulBufLen as length

/*
			FILE* fp = fopen("rec.txt", "ab");
			if (fp)
			{
				fwrite(pch, 1, ulBufLen, fp );
				fflush(fp);
				fclose(fp);
			}
* /
			// parse buffer, format reply.
			CHTTPHeader header;

			if (http.ParseHeader(pch, ulBufLen, &header) < HTTP_SUCCESS)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("header not parsed\r\n\r\n", 1, strlen("header not parsed\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}
* /
				
				if(pch!=NULL) 
				{
					free(pch);  // must free the incoming data buffer 
					pch = NULL;
				}
				// create a return message buffer.
				usCode = 400;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}
			else
			{
				// here we have enough info to check security if desired, and if not, return a 401;
				// else continue....
/*
					FILE* fp = fopen("rec.txt", "ab");
					if (fp)
					{// lets write out the whole header so we can examine its full contents. including args.
						char bufferin[10000];
						sprintf(bufferin,
"header successfully parsed\r\n\
method: %d\r\n\
version: %s\r\n\
URL: %s\r\n\
Path: %s\r\n\
mimetype: %s\r\n\
user: %s\r\n\
password: %s\r\n\
num args: %ld\r\n",
	header.m_ucMethod,  // enumerated values
	header.m_pszVersion==NULL?"(null)":header.m_pszVersion,
	header.m_pszURL==NULL?"(null)":header.m_pszURL,
	header.m_pszPath==NULL?"(null)":header.m_pszPath,
	header.m_pszMimeType==NULL?"(null)":header.m_pszMimeType,
	header.m_pszUser==NULL?"(null)":header.m_pszUser,
	header.m_pszPassword==NULL?"(null)":header.m_pszPassword,
	header.m_ulNumArgs

);
		
	unsigned long k=0;
	while(k<header.m_ulNumArgs)
	{
		if((header.m_ppszArgNames)&&(header.m_ppszArgValues))
		{
			strcat(bufferin, "  ");
			strcat(bufferin, header.m_ppszArgNames[k]==NULL?"(null)":header.m_ppszArgNames[k]);
			strcat(bufferin, "=");
			strcat(bufferin, header.m_ppszArgValues[k]==NULL?"(null)":header.m_ppszArgValues[k]);
			strcat(bufferin, "\r\n");
		}
	k++;
	}

	strcat(bufferin, "headers:\r\n");
	strcat(bufferin, header.m_pszHeaders);
	strcat(bufferin, "\r\n");


			//		fwrite("header successfully parsed\r\n", 1, strlen("header successfully parsed\r\n"), fp );
						fwrite(bufferin, 1, strlen(bufferin), fp );
						fflush(fp);
						fclose(fp);
					}

* /


				if((phttp->m_pSecure)&&((header.m_pszUser==NULL)||(header.m_pszPassword==NULL)))
				{
					usCode = 401;
					pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
					ulBufLen = strlen((char*)pch); // valid from an error page.
					// buffer successfully parsed.
				}
				else 
				{
					if(pch!=NULL)
					{
						free(pch);  // must free the incoming data buffer 
						pch = NULL;
					}

					// create a return message buffer.
					switch(header.m_ucMethod)
					{
					case HTTP_GET://			1  // HTTP/1.0
					case HTTP_HEAD://			2  // HTTP/1.0
						{
							if(
									(strnicmp(header.m_pszPath, http.m_pszRoot, strlen(http.m_pszRoot)) == 0 )  // else not a part of the served file tree
								)
							{
								if(phttp->m_pSecure)
								{
									// have to check the main object for permission.
									// we are going to go by URL (asset level)
									// Loci are going to just be convenient names for the admins to set up.

									if((phttp->m_pSecure->CheckSecure(header.m_pszUser, header.m_pszPassword, NULL, header.m_pszURL))!=SECURE_SUCCESS)
									{
										usCode = 401;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
										ulBufLen = strlen((char*)pch); // valid from an error page.
										break; // get out of the switch from here.
									}
									// else it flows thru....
								}

								DWORD dwAttrib;

								dwAttrib = GetFileAttributes(header.m_pszPath);

								// check for directory (redirect nec.)
								if ( (dwAttrib!=0xffffffff)&&(dwAttrib&FILE_ATTRIBUTE_DIRECTORY) )  // if error, it may be because no file exists, so check in the next part.
								{
									usCode = 301;  // not necessary but why not.
									pch = (unsigned char*)http.RedirectBuffer(&header);
									ulBufLen = strlen((char*)pch); // valid from an redirect page.
								}
								else
								{
									// before we try to open the file, lets spend the time now (not while the file is open)
									// to figure out how long the static text respone buffer will be.
									CBufferUtil bu;
									// notice the sample "Server: SentinelServer/3.0.3.1" header included.  Pick the appname and ver, probably best
									unsigned long ulBufferLen = strlen("HTTP/1.0 200 OK\r\nServer: SentinelServer/\r\nContent-Length: \r\n\r\n");
									ulBufferLen += strlen(SENTINEL_CURRENT_VERSION);

									if((header.m_pszMimeType)&&(strlen(header.m_pszMimeType)>0))
									{
										ulBufferLen += (strlen("Content-Type: \r\n") + strlen(header.m_pszMimeType)); // only add if known!
									}
									else // empty!
									{
										// dont free the "permanent resource", just null the pointer
										header.m_pszMimeType=NULL;
									}

									FILE* pFile = NULL;

									pFile = fopen(header.m_pszPath, "rb");
									if (pFile == NULL)  // file not openable, or not found.
									{
										usCode = 404;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 404 not found
										ulBufLen = strlen((char*)pch); // valid from an error page.
									}
									else
									{
										// this part just assembles the file into a buffer.
										// this may be modified to do dynamic html things by using the 
										// pClient->m_lpObject that refers back to some parser....

										usCode = 200; // OK  (!)
										// determine file size
										fseek(pFile, 0, SEEK_END);
										unsigned long ulFileLen = ftell(pFile);


										// here we have to parse out any directives.  if this is a template file,
										// we may have a variable length.
									//	x


										if(header.m_ucMethod == HTTP_GET)
										{
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += (strlen(length)+ulFileLen);

											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you truncations at zeroed bytes

											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: SentinelServer/%s", SENTINEL_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
											
											fclose(pFile);
										}
										else
										if(header.m_ucMethod == HTTP_HEAD)
										{
											fclose(pFile);  // we have all we need already.
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += strlen(length);
											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											//  create the header
											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: SentinelServer/%s", SENTINEL_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												// its identical except for the body.  next line must stay commented out, its just there for ref.
												//fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
										}
									}
								}
							}
							else  // not part of the server service.
							{
								usCode = 403;
								pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 403 forbidden ...
								ulBufLen = strlen((char*)pch); // valid from an error page.
							}

						} break;
					case HTTP_POST://			3  // HTTP/1.0
						{
							usCode = 501;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 501 not implemented ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_PUT://			4  // HTTP/1.1
					case HTTP_DELETE://		5  // HTTP/1.1
					case HTTP_OPTIONS://  6  // HTTP/1.1
					case HTTP_TRACE://	  7  // HTTP/1.1
						{
							usCode = 505;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 505 http version not supported ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_UNDEF://		0  // internal
					default:
						{
							usCode = 400;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					}
				}
			}

			// send a reply, in pch., with strlen pch as length

			if(pch == NULL)  // had a problem forming the request, try to send a last ditch error code.
			{
				usCode = 500;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 500 internal server error
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}

			if(pch != NULL)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("sending:\r\n", 1, strlen("sending:\r\n"), fp );
					fwrite(pch, 1, ulBufLen, fp );
					fwrite("\r\n\r\n", 1, strlen("\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}

* /
				nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
				if(nReturn<NET_SUCCESS)
				{
					//error.
					if(pClient->m_lpMsgObj)
					{
						_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
						((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
					}
				}
			}
		}

//		char foo[34]; sprintf(foo, "code: %d", usCode);	AfxMessageBox(foo);

		http.m_pszHost = NULL;		// but do NOT free resource
		http.m_ppszErrors = NULL; // but do NOT free resource
		http.m_ppszMime = NULL;   // but do NOT free resource

		if(pch!=NULL) free(pch);  // must free the data buffer 

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}
*/


void SentinelCommandHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;
		CSafeBufferUtil sbu;


		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure in Protocol 1  //SECURE with PROTOCOL2 later
			strcpy(pszInfo, "");  // clear it
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error %d receiving data.  %s", nReturn, pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					strcpy(pszInfo, "");  // clear it
					nReturn = net.SendData(&data, pClient->m_socket, 2000, 0, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error %d sending NAK reply.  %s", nReturn, pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
						// we had an error receiving data and then anotehr trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{
//AfxMessageBox("receiving");

					// parse the command:

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					switch(data.m_ucCmd)
					{
					case CX_CMD_GETINFO: // not actually supported yet.  everything thru csf files for now.
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;


						// system commands!
					case SENTINEL_CMD_INSERT://	   0xa0
						{

							// need to know:
							// username,  what server, what list, what index, options like lock list, data with the event stream to insert
							CAConnection* pConn = NULL;
							int nList=-1;
							int nIndex=-1;
							int nNumEvents=0;
							unsigned long ulFlags = ADC_NORMAL;
							bool bParsed = false;

							if(data.m_pucData!=NULL)
							{
g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel insert", "data was [%s] len = %d", (char*)data.m_pucData, data.m_ulDataLen);  //(Dispatch message)

								char* pchUser = sbu.Token((char*)data.m_pucData, data.m_ulDataLen, "|", MODE_SINGLEDELIM);
								if((pchUser)&&(strlen(pchUser)))
								{
									char* pchServer = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
									if((pchServer)&&(strlen(pchServer)))
									{
										char* pchList = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
										if((pchList)&&(strlen(pchList)))
										{
											nList = atoi(pchList);
											// ok cool... have user server and list
											int nChIndex = g_psentinel->m_data.ReturnChannelIndex(pchServer, nList);

g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel insert", "channel index was %d for %s:%d", nChIndex, pchServer, nList);  //(Dispatch message)

											if((nChIndex>=0)&&(g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_ppChannelObj[nChIndex]))
											{ 
												if((g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ulFlags&SENTINEL_FLAG_ENABLED)&&(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppAPIConn))
												{
													pConn = *(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppAPIConn);
													char* pchIndex = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
													if((pchIndex)&&(strlen(pchIndex)))
													{
														nIndex = atoi(pchIndex);
														char* pchOptions = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
														if((pchOptions)&&(strlen(pchOptions)))
														{
															ulFlags = atol(pchOptions);
														}//if((pchOptions)&&(strlen(pchOptions)))

														pchOptions = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
														if((pchOptions)&&(strlen(pchOptions)))
														{
															nNumEvents = atoi(pchOptions);

															if(nNumEvents>0)
															{
																unsigned char* pucEvents = (unsigned char*)(pchOptions+ strlen(pchOptions) + 1); // 1 gets us past the delimiter
																
																// OK now we can do something.
																if(g_psentinel->m_settings.m_bStubModeNetCalls)
																{
																	nNumEvents=ADC_SUCCESS;
																}
																else
																{
g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "%s requests insertion of %d events at %d on %s:%s", 
	pchUser, nNumEvents, nIndex+1, pchServer, pchList);  //(Dispatch message)

																	EnterCriticalSection(&g_adc.m_crit);
																	if(g_adc.m_pucEventStreamBuffer) {try{ free(g_adc.m_pucEventStreamBuffer);} catch(...){}}
																	g_adc.m_pucEventStreamBuffer = pucEvents;
																	g_adc.m_usStreamBufferLength = (unsigned short)(data.m_ulDataLen - (pucEvents - (unsigned char*)pchUser));
																	nNumEvents = g_adc.InsertEventAt(pConn, nList, nIndex, ulFlags);
g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "Insertion on %s:%s returned %d (buffer len was %d) %d %d %d", 
	pchServer, pchList, nNumEvents,g_adc.m_usStreamBufferLength, data.m_ulDataLen, pucEvents, data.m_pucData);  //(Dispatch message)
																	g_adc.m_pucEventStreamBuffer = NULL;
																	g_adc.m_usStreamBufferLength = 0;
																	LeaveCriticalSection(&g_adc.m_crit);
																}
																if(nNumEvents>=ADC_SUCCESS)
																{
																	bParsed = true;

							data.m_ucCmd = NET_CMD_ACK;
							data.m_ucSubCmd = NET_CMD_ACK; // no good, general error for now
							if(data.m_pucData!=NULL){	try{ free(data.m_pucData);} catch(...){}}  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.

																}
																else
																{
																}//if(nNumEvents>=ADC_SUCCESS) was false
															}//if(nNumEvents>0)
							else
g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel insert", "if(nNumEvents>0) was false");  //(Dispatch message)
														}//if((pchOptions)&&(strlen(pchOptions)))
							else
g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel insert", "if((pchOptions)&&(strlen(pchOptions))) was false");  //(Dispatch message)
													}//if((pchIndex)&&(strlen(pchIndex)))
							else
g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel insert", "if((pchIndex)&&(strlen(pchIndex))) was false");  //(Dispatch message)
												}//if(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ulFlags&SENTINEL_FLAG_ENABLED)
							else
g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel insert", "if(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ulFlags&SENTINEL_FLAG_ENABLED) was false");  //(Dispatch message)
											}//if((nChIndex>=0)&&(g_psentinel->m_data.m_ppChannelObj)&&(g_plibretto->m_data.m_ppChannelObj[nChIndex]))
							else
g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel insert", "if((nChIndex>=0)&&(g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_ppChannelObj[nChIndex])) was false");  //(Dispatch message)
										}//if((pchList)&&(strlen(pchList)))
							else
g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel insert", "if((pchList)&&(strlen(pchList))) was false");  //(Dispatch message)
									}//if((pchServer)&&(strlen(pchServer)))
							else
g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel insert", "if((pchUser)&&(strlen(pchUser))) was false");  //(Dispatch message)
								}//if((pchUser)&&(strlen(pchUser)))
							else
g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel insert", "if((pchUser)&&(strlen(pchUser))) was false");  //(Dispatch message)
							}//if(data.m_pucData!=NULL)
							else
g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel insert", "data.m_pucData was NULL");  //(Dispatch message)


							if(!bParsed)
							{
								data.m_ucCmd = NET_CMD_ACK;
								data.m_ucSubCmd = NET_CMD_NAK; // no good, general error for now
								if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.
							}

						} break;
					case SENTINEL_CMD_DELETE://	   0xa1
						{
							// need to know:
							// username,  what server, what list, what index, options like lock list, number of events to delete
							bool bDelSec = false;
							if((data.m_ucType&NET_TYPE_HASSUBC)&&(data.m_ucSubCmd==SENTINEL_SCMD_TIESEC)) bDelSec = true;

							CAConnection* pConn = NULL;
							int nList=-1;
							int nIndex=-1;
							int nNumEvents=0;
							unsigned long ulFlags = ADC_NORMAL;
							bool bParsed = false;

							if(data.m_pucData!=NULL)
							{
								char* pchUser = sbu.Token((char*)data.m_pucData, data.m_ulDataLen, "|", MODE_SINGLEDELIM);
								if((pchUser)&&(strlen(pchUser)))
								{
									char* pchServer = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
									if((pchServer)&&(strlen(pchServer)))
									{
										char* pchList = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
										if((pchList)&&(strlen(pchList)))
										{
											nList = atoi(pchList);
											// ok cool... have user server and list
											int nChIndex = g_psentinel->m_data.ReturnChannelIndex(pchServer, nList);
											if((nChIndex>=0)&&(g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_ppChannelObj[nChIndex]))
											{ 
												if((g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ulFlags&SENTINEL_FLAG_ENABLED)&&(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppAPIConn))
												{
													pConn = (*(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppAPIConn));
													char* pchIndex = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
													if((pchIndex)&&(strlen(pchIndex)))
													{
														nIndex = atoi(pchIndex);
														char* pchOptions = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
														if((pchOptions)&&(strlen(pchOptions)))
														{
															ulFlags = atol(pchOptions);
														}//if((pchOptions)&&(strlen(pchOptions)))

														pchOptions = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
														if((pchOptions)&&(strlen(pchOptions)))
														{
															nNumEvents = atoi(pchOptions);

															if(nNumEvents>0)
															{
																
																// OK now we can do something.
																if(bDelSec) // we need to check the nNumEvents for whatever is there and 
																	//update the number to in clude all the secondaries of all primaries in the span
																{
																}
																else // just delete the number
																{
																	if(g_psentinel->m_settings.m_bStubModeNetCalls)
																	{
																		nNumEvents=ADC_SUCCESS;
																	}
																	else
																	{
	g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "%s requests deletion of %d events at %d on %s:%s", 
		pchUser, nNumEvents, nIndex+1, pchServer, pchList);  //(Dispatch message)

																		EnterCriticalSection(&g_adc.m_crit);
																		nNumEvents = g_adc.DeleteEventAt(pConn, nList, nIndex, nNumEvents, ulFlags);
																		LeaveCriticalSection(&g_adc.m_crit);
	g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "Deletion on %s:%s returned %d", 
		pchServer, pchList, nNumEvents);  //(Dispatch message)
																	}
																	if(nNumEvents>=ADC_SUCCESS)
																	{
																		bParsed = true;

								data.m_ucCmd = NET_CMD_ACK;
								data.m_ucSubCmd = NET_CMD_ACK; // no good, general error for now
								if(data.m_pucData!=NULL) {	try{ free(data.m_pucData);} catch(...){}}  //destroy the buffer;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.

																	}
																	else
																	{
																	}//if(nNumEvents>=ADC_SUCCESS) was false
																}//	if(bDelSec) was false
															}//if(nNumEvents>0)
														}//if((pchOptions)&&(strlen(pchOptions)))
													}//if((pchIndex)&&(strlen(pchIndex)))
												}//if(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ulFlags&SENTINEL_FLAG_ENABLED)
											}//if((nChIndex>=0)&&(g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_ppChannelObj[nChIndex]))
										}//if((pchList)&&(strlen(pchList)))
									}//if((pchServer)&&(strlen(pchServer)))
								}//if((pchUser)&&(strlen(pchUser)))
							}//if(data.m_pucData!=NULL)


							if(!bParsed)
							{
								data.m_ucCmd = NET_CMD_ACK;
								data.m_ucSubCmd = NET_CMD_NAK; // no good, general error for now
								if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.
							}

						} break;
					case SENTINEL_CMD_GET://		   0xa2
						{
							// need to know:
							// username,  what server, what list, what index, options like lock list, number of events to get.

							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case SENTINEL_CMD_LOCK://		   0xa3
						{
							// need to know:
							// username,  what server, what list, operation is lock or unlock
							bool bLock = false;
							if((data.m_ucType&NET_TYPE_HASSUBC)&&(data.m_ucSubCmd==SENTINEL_SCMD_LOCK)) bLock = true;

							CAConnection* pConn = NULL;
							int nList=-1;
							int nReturn=-1;
							unsigned long ulFlags = ADC_NORMAL;
							bool bParsed = false;

							if(data.m_pucData!=NULL)
							{
								char* pchUser = sbu.Token((char*)data.m_pucData, data.m_ulDataLen, "|", MODE_SINGLEDELIM);
								if((pchUser)&&(strlen(pchUser)))
								{
									char* pchServer = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
									if((pchServer)&&(strlen(pchServer)))
									{
										char* pchList = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
										if((pchList)&&(strlen(pchList)))
										{
											g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "List %slock from %s for %s:%s.", (bLock?"":"un"), pchUser, pchServer, pchList);  //(Dispatch message)

											nList = atoi(pchList);
											// ok cool... have user server and list
											int nChIndex = g_psentinel->m_data.ReturnChannelIndex(pchServer, nList);
											if((nChIndex>=0)&&(g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_ppChannelObj[nChIndex]))
											{ 
												if((g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ulFlags&SENTINEL_FLAG_ENABLED)&&(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppAPIConn))
												{
													pConn = (*(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppAPIConn));
													if(bLock)
													{
														if(g_psentinel->m_settings.m_bStubModeNetCalls)
														{
															nReturn=ADC_SUCCESS;
														}
														else
														{
															EnterCriticalSection(&g_adc.m_crit);
															nReturn = g_adc.LockList(pConn, nList);
															LeaveCriticalSection(&g_adc.m_crit);
														}
													}
													else
													{
														if(g_psentinel->m_settings.m_bStubModeNetCalls)
														{
															nReturn=ADC_SUCCESS;
														}
														else
														{
															EnterCriticalSection(&g_adc.m_crit);
															nReturn = g_adc.UnlockList(pConn, nList);
															LeaveCriticalSection(&g_adc.m_crit);
														}
													}

													bParsed = true;

													if(nReturn>=ADC_SUCCESS)
													{
	g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "List %slock from %s for %s:%s returned %d.", (bLock?"":"un"), pchUser, pchServer, pchList, nReturn);  //(Dispatch message)

						data.m_ucCmd = NET_CMD_ACK;
						if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
						data.m_ulDataLen = 0;
						// has no data, just ack.
						data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
													} //if(nReturn>=0)
													else
													{
	g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "ERROR List %slock from %s for %s:%s returned %d.", (bLock?"":"un"), pchUser, pchServer, pchList, nReturn);  //(Dispatch message)
						data.m_ucCmd = NET_CMD_ACK;
						data.m_ucSubCmd = NET_CMD_NAK;//(0-nReturn); //negate it.

						if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
						data.m_ulDataLen = 0;
						// has no data, just ack.
						data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data and subcommand.
													}

												}//if(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ulFlags&SENTINEL_FLAG_ENABLED)
											}//if((nChIndex>=0)&&(g_psentinel->m_data.m_ppChannelObj)&&(g_plibretto->m_data.m_ppChannelObj[nChIndex]))
										}//if((pchList)&&(strlen(pchList)))
									}//if((pchServer)&&(strlen(pchServer)))
								}//if((pchUser)&&(strlen(pchUser)))
							}//if(data.m_pucData!=NULL)


							if(!bParsed)
							{
								data.m_ucCmd = NET_CMD_ACK; // no good, general error for now
								data.m_ucSubCmd = NET_CMD_NAK; // no good, general error for now
								if(data.m_pucData!=NULL) {	try{ free(data.m_pucData);} catch(...){}}  //destroy the buffer;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.
							}

						} break;
					case SENTINEL_CMD_MOVE://		   0xa4
						{
							// need to know:
							// username,  what server, what list, what index, options like lock list, number of events to move, target index.

							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case SENTINEL_CMD_MOD://		   0xa5
						{
							// need to know:
							// username,  what server, what list, what index, options like lock list, data with the event stream to insert
							CAConnection* pConn = NULL;
							int nList=-1;
							int nIndex=-1;
							int nNumEvents=0;
							unsigned long ulFlags = ADC_NORMAL;
							bool bParsed = false;

							if(data.m_pucData!=NULL)
							{
								char* pchUser = sbu.Token((char*)data.m_pucData, data.m_ulDataLen, "|", MODE_SINGLEDELIM);
								if((pchUser)&&(strlen(pchUser)))
								{
									char* pchServer = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
									if((pchServer)&&(strlen(pchServer)))
									{
										char* pchList = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
										if((pchList)&&(strlen(pchList)))
										{
											nList = atoi(pchList);
											// ok cool... have user server and list
											int nChIndex = g_psentinel->m_data.ReturnChannelIndex(pchServer, nList);
											if((nChIndex>=0)&&(g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_ppChannelObj[nChIndex]))
											{ 
												if((g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ulFlags&SENTINEL_FLAG_ENABLED)&&(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppAPIConn))
												{
													pConn = (*(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppAPIConn));
													char* pchIndex = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
													if((pchIndex)&&(strlen(pchIndex)))
													{
														nIndex = atoi(pchIndex);
														char* pchOptions = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
														if((pchOptions)&&(strlen(pchOptions)))
														{
															ulFlags = atol(pchOptions);
														}//if((pchOptions)&&(strlen(pchOptions)))

														pchOptions = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
														if((pchOptions)&&(strlen(pchOptions)))
														{
															nNumEvents = atoi(pchOptions);

															if(nNumEvents>0)
															{
																unsigned char* pucEvents = (unsigned char*)(pchOptions+ strlen(pchOptions) + 1); // 1 gets us pas the delimiter
																
																// OK now we can do something.
																if(g_psentinel->m_settings.m_bStubModeNetCalls)
																{
																	nNumEvents=ADC_SUCCESS;
																}
																else
																{
g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "%s requests modification of %d events at %d on %s:%s", 
	pchUser, nNumEvents, nIndex+1, pchServer, pchList);  //(Dispatch message)

																	EnterCriticalSection(&g_adc.m_crit);
																	if(g_adc.m_pucEventStreamBuffer) {try{ free(g_adc.m_pucEventStreamBuffer);} catch(...){}}
																	g_adc.m_pucEventStreamBuffer = pucEvents;
																	g_adc.m_usStreamBufferLength = (unsigned short)(data.m_ulDataLen - (pucEvents - (unsigned char*)pchUser));

FILE* fp;
fp=fopen("sentinelstream.lst", "wb");
if(fp)
{
	fwrite(g_adc.m_pucEventStreamBuffer, g_adc.m_usStreamBufferLength, 1, fp);
	fclose(fp);
}

																	nNumEvents = g_adc.ModifyEventAt(pConn, nList, nIndex, ulFlags);
																	g_adc.m_pucEventStreamBuffer = NULL;
																	g_adc.m_usStreamBufferLength = 0;
																	LeaveCriticalSection(&g_adc.m_crit);
g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "Modification on %s:%s returned %d", 
	pchServer, pchList, nNumEvents);  //(Dispatch message)
																}
																if(nNumEvents>=ADC_SUCCESS)
																{
																	bParsed = true;

							data.m_ucCmd = NET_CMD_ACK;
							data.m_ucSubCmd = NET_CMD_ACK; // no good, general error for now
							if(data.m_pucData!=NULL) {	try{ free(data.m_pucData);} catch(...){}}  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.

																}
																else
																{
																}//if(nNumEvents>=ADC_SUCCESS) was false
															}//if(nNumEvents>0)
														}//if((pchOptions)&&(strlen(pchOptions)))
													}//if((pchIndex)&&(strlen(pchIndex)))
												}//if(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ulFlags&SENTINEL_FLAG_ENABLED)
											}//if((nChIndex>=0)&&(g_psentinel->m_data.m_ppChannelObj)&&(g_plibretto->m_data.m_ppChannelObj[nChIndex]))
										}//if((pchList)&&(strlen(pchList)))
									}//if((pchServer)&&(strlen(pchServer)))
								}//if((pchUser)&&(strlen(pchUser)))
							}//if(data.m_pucData!=NULL)


							if(!bParsed)
							{
								data.m_ucCmd = NET_CMD_ACK;
								data.m_ucSubCmd = NET_CMD_NAK; // no good, general error for now
								if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.
							}


						} break;
					case SENTINEL_CMD_SWAP://		   0xa6
						{
							// need to know:
							// username,  what server, what list, what index, options like lock list, number of events in block A, target index, num events block B

							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL) {	try{ free(data.m_pucData);} catch(...){}}  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case SENTINEL_CMD_FIND://		   0xa7  // just return postion in list
						{
							// need to know:
							// username,  what server, what list, what index to start looking at, what ID to find, options like lock list
							bool bCountSec = false;
							if((data.m_ucType&NET_TYPE_HASSUBC)&&(data.m_ucSubCmd==SENTINEL_SCMD_TIESEC)) bCountSec = true;

							CAConnection* pConn = NULL;
							int nList=-1;
							int nIndex=-1;
							int nReturn=-1;
							int nSec=-1;
							unsigned long ulFlags = ADC_NORMAL;
							bool bParsed = false;

							if(data.m_pucData!=NULL)
							{
								char* pchUser = sbu.Token((char*)data.m_pucData, data.m_ulDataLen, "|", MODE_SINGLEDELIM);
								if((pchUser)&&(strlen(pchUser)))
								{
									char* pchServer = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
									if((pchServer)&&(strlen(pchServer)))
									{
										char* pchList = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
										if((pchList)&&(strlen(pchList)))
										{
											nList = atoi(pchList);
											// ok cool... have user server and list
											int nChIndex = g_psentinel->m_data.ReturnChannelIndex(pchServer, nList);
											if((nChIndex>=0)&&(g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_ppChannelObj[nChIndex]))
											{ 
												if((g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ulFlags&SENTINEL_FLAG_ENABLED)&&(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppAPIConn))
												{
													pConn = (*(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppAPIConn));
													if(pConn)
													{
														char* pchIndex = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
														if((pchIndex)&&(strlen(pchIndex)))
														{
															nIndex = atoi(pchIndex);

															char* pchID = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
															if((pchID)&&(strlen(pchID)))
															{
																//if(g_settings.m_bStubModeNetCalls)  no need for stub on this.  just finding locally in mem


																// find options if exists.
																char* pchOptions = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
																if((pchOptions)&&(strlen(pchOptions)))
																{
																	ulFlags = atol(pchOptions);
																}//if((pchOptions)&&(strlen(pchOptions)))
	g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "%s requests finding of event %s starting at %d on %s:%s", 
		pchUser, pchID, nIndex+1, pchServer, pchList);  //(Dispatch message)


															EnterCriticalSection(&g_adc.m_crit);
		if((g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppevents)&&(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_nNumEvents>0))
		{
			int i = nIndex;
			if(i<0) i=0;
			int nLimit = g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_nNumEvents;
			if((ulFlags&ADC_GETEVENTS_EVENTLOOK)&&(nLimit<pConn->m_ListData[nList].lookahead)) nLimit= pConn->m_ListData[nList].lookahead;
			while(i<nLimit)
			{
				if(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppevents[i])
				{
					if(nSec>=0)
					{
						if(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppevents[i]->m_event.m_usType&SECONDARYEVENT)
						{
							nSec++;
						}
						else break;
					}

					if(
							(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppevents[i]->m_event.m_pszID)
						&&(strcmp(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppevents[i]->m_event.m_pszID, pchID)==0)
						)
					{
						if(
							  ((!(ulFlags&ADC_PROTECT_DONE))||((ulFlags&ADC_PROTECT_DONE)&&(!(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppevents[i]->m_event.m_usStatus&(1<<eventdone)))))
							&&((!(ulFlags&ADC_PROTECT_PLAY))||((ulFlags&ADC_PROTECT_PLAY)&&(!(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ppevents[i]->m_event.m_usStatus&(1<<eventrunning)))))
							)
						{
							nReturn = i;
							nSec = 0; // starts counting
							if(!bCountSec) break;
						}
					}
				}
				i++;
			}
		}
															LeaveCriticalSection(&g_adc.m_crit);

	g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "Find of %s on %s:%s returned %d (%d sec)", 
		pchID, pchServer, pchList, ((nReturn<0)?nReturn:(nReturn+1)), nSec);  //(Dispatch message)

																bParsed = true;

														//		if(nReturn>=ADC_SUCCESS) // we made it this far so let's just send the return code.
																{
									data.m_ucCmd = NET_CMD_ACK;
									data.m_ucSubCmd = nSec;
									if(data.m_pucData!=NULL) {	try{ free(data.m_pucData);} catch(...){}}  //destroy the buffer;
									data.m_ulDataLen = 0;
									data.m_pucData=(unsigned char*)malloc(MAX_PATH);
									if(data.m_pucData)
									{
										pchID = (char*) data.m_pucData;
										sprintf(pchID, "%d", nReturn);
										data.m_ulDataLen = strlen(pchID);

									}
									// has no data, just ack.
									data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
																} //if(nReturn>=0)

															}//if((pchID)&&(strlen(pchID)))
														}//	if((pchIndex)&&(strlen(pchIndex)))
													}//if(pConn)
												}//if(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ulFlags&SENTINEL_FLAG_ENABLED)
											}//if((nChIndex>=0)&&(g_psentinel->m_data.m_ppChannelObj)&&(g_plibretto->m_data.m_ppChannelObj[nChIndex]))
										}//if((pchList)&&(strlen(pchList)))
									}//if((pchServer)&&(strlen(pchServer)))
								}//if((pchUser)&&(strlen(pchUser)))
							}//if(data.m_pucData!=NULL)


							if(!bParsed)
							{
								data.m_ucCmd = NET_CMD_ACK; // no good, general error for now
								data.m_ucSubCmd = NET_CMD_NAK; // no good, general error for now
								if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								// has no data, just ack.
								data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.
							}

						} break;
					case SENTINEL_CMD_DEL_ID://	   0xa8  // delete by list of IDs, not index
						{
							// need to know:
							// username,  what server, what list, ID List, options like lock list, number of events to delete

							bool bDelSec = false;
							if((data.m_ucType&NET_TYPE_HASSUBC)&&(data.m_ucSubCmd==SENTINEL_SCMD_TIESEC)) bDelSec = true;

							CAConnection* pConn = NULL;
							int nList=-1;
							int nIndex=-1;
							int nNumEvents=0;
							unsigned long ulFlags = ADC_NORMAL;

							if(data.m_pucData!=NULL)
							{
								char* pchUser = sbu.Token((char*)data.m_pucData, data.m_ulDataLen, "|", MODE_SINGLEDELIM);
								if((pchUser)&&(strlen(pchUser)))
								{
									char* pchServer = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
									if((pchServer)&&(strlen(pchServer)))
									{
										char* pchList = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
										if((pchList)&&(strlen(pchList)))
										{
											nList = atoi(pchList);
											// ok cool... have user server and list
											int nChIndex = g_psentinel->m_data.ReturnChannelIndex(pchServer, nList);
											if((nChIndex>=0)&&(g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_ppChannelObj[nChIndex]))
											{ 
												if(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ulFlags&SENTINEL_FLAG_ENABLED)
												{
													char* pchIndex = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
													if((pchIndex)&&(strlen(pchIndex)))
													{
														nIndex = atoi(pchIndex);
														char* pchOptions = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
														if((pchOptions)&&(strlen(pchOptions)))
														{
															ulFlags = atol(pchOptions);
														}//if((pchOptions)&&(strlen(pchOptions)))

														pchOptions = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
														if((pchOptions)&&(strlen(pchOptions)))
														{
															nNumEvents = atoi(pchOptions);

															// OK now we can do something.
															if(bDelSec) // we need to check the nNumEvents for whatever is there and 
																//update the number to in clude all the secondaries of all primaries in the span
															{
															}
															else // just delete the number
															{
															}

														}//if((pchOptions)&&(strlen(pchOptions)))
													}//if((pchIndex)&&(strlen(pchIndex)))
												}//if(g_psentinel->m_data.m_ppChannelObj[nChIndex]->m_ulFlags&SENTINEL_FLAG_ENABLED)
											}//if((nChIndex>=0)&&(g_psentinel->m_data.m_ppChannelObj)&&(g_plibretto->m_data.m_ppChannelObj[nChIndex]))
										}//if((pchList)&&(strlen(pchList)))
									}//if((pchServer)&&(strlen(pchServer)))
								}//if((pchUser)&&(strlen(pchUser)))
							}//if(data.m_pucData!=NULL)



							int nReturn =-1;
							EnterCriticalSection(&g_adc.m_crit);
							nReturn = g_adc.DeleteEventAt(pConn, nList, nIndex, nNumEvents, ulFlags);
							LeaveCriticalSection(&g_adc.m_crit);

							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL) {	try{ free(data.m_pucData);} catch(...){}}  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;



// end sys commands


					case SENTINEL_CMD_EXSET://				0xea // increments exchange counter
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)
							{	
								data.m_ucSubCmd = (unsigned char) g_psentinel->m_data.IncrementDatabaseMods((char*)data.m_pucData);
								try{ free(data.m_pucData);} catch(...){}  //destroy the buffer;
							}
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.

							data.m_ucType = NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case SENTINEL_CMD_EXGET://				0xeb // gets exchange mod value
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL) {	try{ free(data.m_pucData);} catch(...){}}  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case SENTINEL_CMD_MODSET://			0xec // sets exchange mod value, skips exchange table
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL) {	try{ free(data.m_pucData);} catch(...){}}  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case CX_CMD_GETSTATUS: //	0x99 // gets status info
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL) {	try{ free(data.m_pucData);} catch(...){}}  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							unsigned long ulStatus;
							// assemble if there is 

							char* pch = g_psentinel->m_data.GetStatusText(&ulStatus);  // allocates mem, must free after use
							if(pch)
							{
								data.m_ucSubCmd = (unsigned char)(SENTINEL_ICON_MASK&ulStatus);
								data.m_pucData=(unsigned char*)pch;
								data.m_ulDataLen = strlen(pch);
								data.m_ucType = NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data.
							}
							else
							{
								data.m_ucCmd = NET_CMD_NAK;
								data.m_ucSubCmd = NET_CMD_NAK;
								data.m_ucType = NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1;//NET_TYPE_HASDATA; 
							}


						} break;

					case CX_CMD_BYE:
						{
	g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "The Global Kill command has been received.");  //(Dispatch message)

							bSetGlobalKill = true;
							g_psentinel->m_data.m_bQuietKill = true;
							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL) {	try{ free(data.m_pucData);} catch(...){}}  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					default:
						{
							//just ack
							data.m_ucCmd = NET_CMD_ACK;

		//					char repbuf[64];
							if(data.m_pucData!=NULL)
							{
		/*
								_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

								for(unsigned long q=0; q<data.m_ulDataLen; q++)
								{
									if(strlen(repbuf)<63)
										strncat(repbuf, (char*)(data.m_pucData+q), 1);
								}
								if(strlen(repbuf)<63)
									strcat(repbuf, "]");
								data.m_ulDataLen = strlen(repbuf);
								repbuf[data.m_ulDataLen] = 0;  // just in case
		*/
								try{ free(data.m_pucData);} catch(...){}  //destroy the buffer;
							}
		/*
							else
							{
								_snprintf(repbuf, 63, "data was NULL");
								data.m_ulDataLen = strlen(repbuf); 
							}

							data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
							if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
							else 
							{
								data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
								data.m_ulDataLen = 0;
							}
		*/
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					}

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Sentinel:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Sentinel:CommandHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.
	if (bSetGlobalKill)	//g_bKillThread = true;
	((CSentinelHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}

void SentinelStatusHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Sentinel:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Sentinel:CommandHandlerThread");
						}
						// we had an error receiving data and then anotehr trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Sentinel:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Sentinel:CommandHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}

int CSentinelMain::SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...)
{
	EnterCriticalSection(&m_data.m_critAsRun);
 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszAsRun)&&(strlen(m_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		if((pszEventID)&&(strlen(pszEventID)>SENDMSG_EVENT_MAXLEN)) pszEventID[SENDMSG_EVENT_MAXLEN-1]=0;
		
		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);
		char* pchEvent = m_data.m_pdb->EncodeQuotes(pszEventID);
		if(m_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, channelid, eventid) \
VALUES ('%s', '%s', %d, %d.%03d, %d, '%s')", // HARDCODE
				(m_settings.m_pszAsRun?m_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm,
				nChannelID,
				pchEvent?pchEvent:""
				);
		}
		else
		{

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				(m_settings.m_pszAsRun?m_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				(m_settings.m_pszAsRun?m_settings.m_pszAsRun:"AsRun_Log")
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
		if(pchEvent) free(pchEvent);

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_data.m_critSQL);
		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_data.m_critSQL);
			m_data.IncrementDatabaseMods(m_settings.m_pszAsRun, dberrorstring);
	LeaveCriticalSection(&m_data.m_critAsRun);
			return SENTINEL_SUCCESS;
		}
		LeaveCriticalSection(&m_data.m_critSQL);

	}
	LeaveCriticalSection(&m_data.m_critAsRun);
	return SENTINEL_ERROR;
}




int CSentinelMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{
	EnterCriticalSection(&m_data.m_critMessaging);

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)&&(strlen(m_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

		if(m_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime) \
VALUES ('%s', '%s', %d, %d.%03d)", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				m_settings.m_pszMessages
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);

		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
	LeaveCriticalSection(&m_data.m_critMessaging);
			return SENTINEL_SUCCESS;
		}

		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

	}
	LeaveCriticalSection(&m_data.m_critMessaging);
	return SENTINEL_ERROR;
}

void SentinelTrafficWatchThread(void* pvArgs)
{
	if((g_psentinel)&&(g_psentinel->m_data.m_bTrafficWatchStarted)) return; // allow only one watch thread.
	g_psentinel->m_data.m_bTrafficWatchStarted=true;
	g_psentinel->m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:SentinelTrafficWatchThread", "starting thread");  //(Dispatch message)
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	char dberrorstring[DB_ERRORSTRING_LEN];

	
	CSentinelUtil util;

	CADC adcutil;  // for util functions only.

	bool bInitDB = false; // have to suck in the db and purge old stuff on import.

	strcpy(errorstring, "");
	char pszPath[MAX_PATH+2]; // just a string for temp paths
//SQL_INVALID_HANDLE
//AFX_SQL_ERROR_FIELD_SCHEMA_MISMATCH
	CDBUtil db;
	CDBconn* pdbConn = db.CreateNewConnection(g_psentinel->m_settings.m_pszDSN, g_psentinel->m_settings.m_pszUser, g_psentinel->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			g_psentinel->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Sentinel:traffic_database_connect", errorstring);  //(Dispatch message)
		}
		else
		{
			g_psentinel->m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:traffic_database_connect", "database connected");  //(Dispatch message)
		}
	}
//	else	?

	_ftime( &g_psentinel->m_data.m_timeTrafficWatch );

	int nLastTime = g_psentinel->m_data.m_timeTrafficWatch.time;

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "we are watching being SET TO TRUE");  //(Dispatch message)
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "we are watching SET TO TRUE");  //(Dispatch message)

	while((!g_psentinel->m_data.m_bTrafficKillWatch)&&(!g_bKillThread))
	{

		if(
			  (g_psentinel->m_settings.m_pszTrafficBaseFolder)
			&&(!g_bKillThread)
			&&(!g_psentinel->m_data.m_bProcessSuspended)
			&&(g_psentinel->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!g_psentinel->m_data.m_key.m_bExpires)
				||((g_psentinel->m_data.m_key.m_bExpires)&&(!g_psentinel->m_data.m_key.m_bExpired))
				||((g_psentinel->m_data.m_key.m_bExpires)&&(g_psentinel->m_data.m_key.m_bExpireForgiveness)&&(g_psentinel->m_data.m_key.m_ulExpiryDate+g_psentinel->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!g_psentinel->m_data.m_key.m_bMachineSpecific)
				||((g_psentinel->m_data.m_key.m_bMachineSpecific)&&(g_psentinel->m_data.m_key.m_bValidMAC))
				)
			)
		{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Watch");  //(Dispatch message)

//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "WATCH", "w%d i%d a%d", g_dir.m_bWatchFolder,g_dir.m_bInitialized, g_dir.m_bAnalyzing);  //(Dispatch message)
			if(!g_dir.m_bWatchFolder)
			{

				g_dir.m_ptimebIncrementor = &g_psentinel->m_data.m_timebTick;

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "we arent watching yet");  //(Dispatch message)
				int nReturn = g_dir.SetWatchFolder(g_psentinel->m_settings.m_pszTrafficBaseFolder);
				if(nReturn<DIRUTIL_SUCCESS)
				{
					// error
_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error setting watchfolder.");  
g_psentinel->m_data.SetStatusText(errorstring, SENTINEL_STATUS_ERROR);

					if(!g_psentinel->m_data.m_bTrafficWatchWarningSent)
					{
						g_psentinel->SendMsg(CX_SENDMSG_ERROR, "Sentinel:SetWatchFolder", "SetWatchFolder returned %d with %s", nReturn, g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)");
						g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:SetWatchFolder", "SetWatchFolder returned %d with %s", nReturn, g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)");  // Sleep(100);//(Dispatch message)
//					g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:SetWatchFolder", "SetWatchFolder returned %d with %s", nReturn, g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)");  //(Dispatch message)
					}
					g_psentinel->m_data.m_bTrafficWatchWarningSent = true;
				}
				else
				{
//					g_psentinel->SendMsg(1, "Sentinel:SetWatchFolder", "SetWatchFolder succeeded with %s", g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)"); 
					g_psentinel->m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:SetWatchFolder", "SetWatchFolder succeeded with %s", g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)"); //  Sleep(100);//(Dispatch message)

					if(!g_bKillThread)
					{
						nReturn = g_dir.BeginWatch( (unsigned long)(g_psentinel->m_settings.m_nTrafficFolderInterval*1000), true);//m_nTrafficFolderInterval is seconds
						
						if(nReturn<DIRUTIL_SUCCESS)
						{
							// error
							//TODO report
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error beginning watchfolder process.");  
	g_psentinel->m_data.SetStatusText(errorstring, SENTINEL_STATUS_ERROR);

							if(!g_psentinel->m_data.m_bTrafficWatchWarningSent)
							{
								g_psentinel->SendMsg(CX_SENDMSG_ERROR, "Sentinel:BeginWatch", "BeginWatch returned %d;  could not begin watchfolder thread on %s.", nReturn, g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)");
								g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:BeginWatch", "BeginWatch returned %d;  could not begin watchfolder thread on %s.", nReturn, g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)"); //  Sleep(100);//(Dispatch message)
		//						g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Direct:BeginWatch", "BeginWatch returned %d;  could not begin watchfolder thread on %s.", nReturn, g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)");  //(Dispatch message)
							}
							g_psentinel->m_data.m_bTrafficWatchWarningSent = true;
						}
						else
						{
							g_psentinel->m_data.m_bTrafficWatchWarningSent = false;

							g_psentinel->SendMsg(CX_SENDMSG_INFO, "Sentinel:BeginWatch", "BeginWatch: began watch on %s.", g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)"); 
							g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:BeginWatch", "BeginWatch: began watch on %s.", g_psentinel->m_settings.m_pszTrafficBaseFolder?g_psentinel->m_settings.m_pszTrafficBaseFolder:"(null)");  // Sleep(100);//(Dispatch message)
	//						g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Direct:SetWatchFolder", "SetWatchFolder: began watch on %s", g_psentinel->m_settings.m_pszTrafficBaseFolder);  //(Dispatch message)

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "watching folder...");  
	g_psentinel->m_data.SetStatusText(errorstring, SENTINEL_STATUS_OK);

							// subtract half the interval so that the watch folder checks for changes, staggered with us checking the results in the else below.
							g_psentinel->m_data.m_timeTrafficWatch.time -= g_psentinel->m_settings.m_nTrafficFolderInterval/2000; // second resolution is fine.
	/*						watchtime.millitm += (unsigned short)((g_psentinel->m_settings.m_ulWatchIntervalMS%1000)/2); // fractional second updates
							if(watchtime.millitm>999)
							{
								watchtime.time++;
								watchtime.millitm%=1000;
							}
	*/
						}
					}
				}
			}
			else // we are watching!
			if(
					(g_dir.m_bInitialized)// but are we initialized
				&&(!g_dir.m_bAnalyzing) // and we arent currently analyzing the changes.
				&&(!g_bKillThread)&&(!g_psentinel->m_data.m_bProcessSuspended)
				)
			{
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "we are watching");  //(Dispatch message)

				_ftime( &g_psentinel->m_data.m_timeTrafficWatch ); // added this when watching to purge done files...  purge doesnt occur when traffic watch is not active.



				if(!bInitDB) // have to suck in the db and purge old stuff on import.
				{


					if(pdbConn)
					{

						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s WHERE traffic_timestamp < %d", 
						((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
						((g_psentinel->m_settings.m_pszTrafficFiles)&&(strlen(g_psentinel->m_settings.m_pszTrafficFiles)))?g_psentinel->m_settings.m_pszTrafficFiles:"Traffic_Files",
							g_psentinel->m_data.m_timebTick.time - (g_psentinel->m_settings.m_nTrafficFilesDoneDays*86400)- (g_psentinel->m_data.m_timebTick.timezone*60)+(g_psentinel->m_data.m_timebTick.dstflag?3600:0)
							);

		if(g_psentinel->m_settings.m_bDebugSQL) 
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Traffic:init_db", "executing: [%s]", szSQL);
//												EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
						if(db.ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
							//message out.
							g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Traffic:init_db", "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
						}
//												LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);


						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s WHERE traffic_file_id not in (SELECT traffic_file_id from %s.dbo.%s)", 
							((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
							((g_psentinel->m_settings.m_pszTraffic)&&(strlen(g_psentinel->m_settings.m_pszTraffic)))?g_psentinel->m_settings.m_pszTraffic:"Traffic_Events",
							((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
							((g_psentinel->m_settings.m_pszTrafficFiles)&&(strlen(g_psentinel->m_settings.m_pszTrafficFiles)))?g_psentinel->m_settings.m_pszTrafficFiles:"Traffic_Files"
							);

if(g_psentinel->m_settings.m_bDebugSQL) 
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Traffic:init_db", "executing: [%s]", szSQL);
//												EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
						if(db.ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
							//message out.
							g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Traffic:init_db", "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
						}
//												LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);


						nLastTime = g_psentinel->m_data.m_timeTrafficWatch.time;
						bInitDB = true;
					}
				}

//				int nPendingID = -1;
//				int nPendingRetries = -1;
//				bool bPendingBackupOnly = false;
				CDirEntry* pDirChange = NULL;

				int nWatchChanges = g_dir.QueryChanges();
				if(nWatchChanges>0)
				{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%d watchfolder changes found", nWatchChanges);  //(Dispatch message)

					// if there's an addition, grab the file path in the inbox, and create
					// the same folder if necessary, copy the file there, delete out of the inbox
					// (dont do a move, in case theres a prob transferring)
					// once the file is on the file server, update the metadata in the database
					int numchanges = 0; // debug only
					int numFileChanges = 0;
					int numEventChanges = 0;
					_timeb diskchecktime;
					_ftime(&diskchecktime);
					diskchecktime.time-=6;  // init so it goes.

					// live changes we will process all in this inner while loop, but pendings we will 
					// do once per big loop (so we can check for more live changes in between)
					while(
						     (!g_bKillThread)&&(!g_psentinel->m_data.m_bProcessSuspended)
								 &&
								 (
									 (
										 (nWatchChanges>0)
									 &&(g_dir.GetDirChange(&pDirChange)>0)
									 &&(pDirChange)
									 )
								 )
							 )

					{
						numchanges++;  // debug only
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%s: %s\\%s (%d changes this round)", ((pDirChange->m_ulFileAttribs&FILE_ATTRIBUTE_DIRECTORY)?" dir":"file"), pDirChange->m_szFullPath, pDirChange->m_szFilename, numchanges);//  Sleep(100);
						_ftime( &g_psentinel->m_data.m_timebTick );
						switch(pDirChange->m_ulFlags&(~WATCH_CHK)) // ignore the check flag
						{
						case WATCH_INIT://		0x00000000  // was there at beginning	
						case WATCH_ADD: //		0x00000001  // was added	
						case WATCH_CHG: //		0x00000003  // was chg	 // need to update the metadata if changed.
						case WATCH_RECHK://		0x00000020  // watchfolder needs to recheck a file	...
							{

								if((pDirChange->m_szFilename)&&(pDirChange->m_szFullPath))
								{
									if((strcmp(pDirChange->m_szFilename, "."))&&(strcmp(pDirChange->m_szFilename, ".."))) // only if its a real file or dir
									{
										if(pDirChange->m_ulFileAttribs&FILE_ATTRIBUTE_DIRECTORY)
										{
											// can ignore

										/*
											if(stricmp(g_psentinel->m_settings.m_pszTrafficBaseFolder, g_psentinel->m_settings.m_pszDestinationFolderPath))  // only do if different
											{
												// mkdir is fine if dir is there already
												sprintf(pszPath, "%s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);
												char* pchRoot = NULL;
												if(strnicmp(pszPath, g_psentinel->m_settings.m_pszTrafficBaseFolder, strlen(g_psentinel->m_settings.m_pszTrafficBaseFolder))==0)
												{
													pchRoot = pszPath + strlen(g_psentinel->m_settings.m_pszTrafficBaseFolder);
													char path_buffer[MAX_PATH+1];
													char dir_buffer[MAX_PATH+1];
													sprintf(path_buffer, "%s%s", g_psentinel->m_settings.m_pszDestinationFolderPath, pchRoot);

													//_mkdir(pszPath);
													pchRoot = strstr(path_buffer, ":\\");  //drive delim.
													int nBegin=0;
													if(pchRoot) nBegin = pchRoot-path_buffer+2;
												}
											}
											*/
							
										}
										else // it's a file
										{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "file found: %s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);//  Sleep(100);


											// have to add this to the database
//the table def is
/*
CREATE TABLE Traffic_Files(\
traffic_file_id int identity(1,1) NOT NULL, \
file_path varchar(256) NOT NULL, \
file_timestamp int, \
file_size decimal(20,3), \
traffic_timestamp int, \
traffic_status int, \
traffic_events int, \
server_name varchar(32), \
server_list int);
*/


											//first see if it's a filename we care aboot.
											bool bProceed = true;
											int nListLen = strlen(g_psentinel->m_settings.m_pszTrafficFileAllowedExtensions);
											if((g_psentinel->m_settings.m_pszTrafficFileAllowedExtensions)&&(nListLen))
											{
												// we have a meaningful setting, do a compare on extension
												bProceed = false;
												char* pszList = (char*)malloc(nListLen+1);
												if(pszList)
												{
													int chp=0;
													while(chp<=nListLen) // copies term zero
													{
														*(pszList+chp) = toupper(*(g_psentinel->m_settings.m_pszTrafficFileAllowedExtensions+chp));
														chp++;
													}
												
											
													sprintf(pszPath, "%s", pDirChange->m_szFilename);
	//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "file is %s", pszPath); Sleep(100); //(Dispatch message)

													char* pchDot = strrchr(pszPath, '.');
													if(pchDot)
													{
														pchDot++;
														char extension[MAX_PATH];
														chp=0;
														while(*(pchDot+chp)!=0)
														{
															*(extension+chp) = toupper(*(pchDot+chp));
															chp++;
														}
														*(extension+chp) = 0; // zero term
															//sprintf(extension, "%s", pchDot);
														if(strlen(extension))
														{
	//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "extension is %s, %d ", extension, nFileTypeIndex); Sleep(100); //(Dispatch message)

															pchDot = strstr(pszList, extension);
															if(pchDot != NULL)
															{
																if(
																		(pszList == pchDot)
																	||((pchDot>pszList)&&(*(pchDot-1)==','))
																	)
																{
																	pchDot+=strlen(extension);
																	char* pchEnd = pszList+nListLen;
																	if(
																			(pchEnd == pchDot)
																		||((pchDot<pchEnd)&&(*(pchDot+1)==','))
																		)
																	{
																		bProceed = true;
																	}
																}
															}
														}
													}

													free(pszList);
												}
											}

											unsigned long ulDate = TIME_NOT_DEFINED;
											char* pszServerName = NULL;
											int nHarrisList=-1;
											CSentinelChannelObject* pChannel = NULL;
											char pszMessageSource[MAX_PATH];
											int nTrafficObjectindex = -1;
											CSentinelTrafficObject* pObj = NULL;

											int nChannel=-1;
											sprintf(pszPath, "%s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);
											if(bProceed)
											{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "file to be processed: %s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);//  Sleep(100);
												// need to find out 
												// 1) what day is this file for (so we can exclude it if it is in the past)

								// harris unixtime is "local unixtime" no DST calc.
												ulDate = util.DateFromName(pszPath);
												if(ulDate == TIME_NOT_DEFINED) bProceed=false;
											}

											if(bProceed)
											{
												char buffer[48];
			tm* theTime = gmtime( (long*)(&ulDate)	);
			strftime( buffer, 40, "%Y-%b-%d %H:%M:%S", theTime );

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "*FOUND*", "file to be processed: %s\\%s for date %d (%s)", 
	pDirChange->m_szFullPath, pDirChange->m_szFilename,
	ulDate, buffer);//  Sleep(100);


												// 2) what channel is this 
												nChannel = util.ChannelFromName(pszPath, &pszServerName, &nHarrisList, (void**)(&pChannel));
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Traffic", "Channel %d for %s", nChannel, pszPath);

												if(nChannel <0)
												{
													bProceed=false;
												}
												else
												{
													// we succeeded, so we should have server name, list, etc.
													// let's see if we have the object already
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Traffic", "Channel pointer %d for channel %d, date %d", pChannel, nChannel, ulDate);
//Sleep(100);
													if(pChannel)
													{
														nTrafficObjectindex = pChannel->FindTrafficObjectByDate(ulDate);
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Traffic", "Traffic object index %d found on channel %d",nTrafficObjectindex, nChannel);
//Sleep(100);

														if(nTrafficObjectindex>=0) // clear it.
														{
															pChannel->m_pptraffic[nTrafficObjectindex]->DeleteAllTrafficEvents();
															pObj = pChannel->m_pptraffic[nTrafficObjectindex];
														}
														else
														{
															// insert the object
															pObj = new CSentinelTrafficObject;
															if(pObj)
															{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Traffic", "inserting traffic object");
																nTrafficObjectindex = pChannel->InsertTrafficObject(pObj);// add to end
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Traffic", "Traffic object inserted");

																if(nTrafficObjectindex>=ADC_SUCCESS)  
																{
																	pObj->m_ulDate = ulDate;
																}
																else
																{
																	try{ delete pObj;} catch(...){}
																	pObj = NULL;
																}
															}
														}
													}
												}
											}

											if(bProceed)
											{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Traffic", "marking processed for %s\\%s", pDirChange->m_szFullPath,pDirChange->m_szFilename); 
//Sleep(100);


												EnterCriticalSection(&g_dir.m_critDirCanon);
				int x = g_dir.FindDirEntryIndex(pDirChange->m_szFullPath, pDirChange->m_szFilename, (pDirChange->m_szFilename==NULL)?pDirChange->m_szDOSFilename:NULL);
				if(x>=0) g_dir.m_ppDirCanon[x]->m_ulFlags |= WATCH_PROCESSED;
												LeaveCriticalSection(&g_dir.m_critDirCanon);


												if(pChannel)
												{
													_snprintf(pszMessageSource, MAX_PATH, "%s:%s:%d (%s) Traffic", 
														g_psentinel->m_settings.m_pszName,
														pChannel->m_pszServerName, //server_name
														pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
														pChannel->m_pszDesc);

												}
												else
												{
													_snprintf(pszMessageSource, MAX_PATH, "[unknown] Traffic");
												}

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "file to be processed: %s\\%s on Channel id %d", 
	pDirChange->m_szFullPath, pDirChange->m_szFilename,
	nChannel );//  Sleep(100);
												// after we know these things, we can then figure out what to do with these files.

												// if the file is past the done threshold we dont have to care, the later code will get it
												// if the file is in the done threshold, we could add it... but for now, nothing.  if there was something there before lets not wipe it, even if someone changes a file meant for the past.
												// if the file is today or within the lookahead, we add its events to the tables.  (though live events should always trump)
												// if the file is out past the lookahead we allocate a pointer to indicate this and let the time check deal with it later


// let's check for existence.
/*
dbo.Traffic_Files

traffic_file_id int identity
file_path varchar(256)
file_timestamp int
file_size decimal (20,3)
traffic_timestamp int
traffic_status int
traffic_events int
server_name varchar(32)
server_list int


dbo.Traffic_Events

CREATE TABLE Traffic_Events(
itemid int identity(1,1), 
traffic_file_id int, 
event_key varchar(256), 
server_name varchar(32), 
server_list int, 
list_id int, 
event_id varchar(32), 
event_clip varchar(64), 
event_segment int, 
event_title varchar(64), 
event_data varchar(4096),
event_type int, 
event_status int, 
event_time_mode int, 
event_start decimal(20,3), 
event_duration int, 
event_calc_start decimal(20,3), 
event_calc_duration int, 
event_calc_end decimal(20,3), 
event_position int, 
event_last_update decimal(20,3), 
parent_uid int, 
parent_key varchar(256), 
parent_id varchar(32), 
parent_clip varchar(64), 
parent_segment int, 
parent_title varchar(64), 
parent_type int, 
parent_status int, 
parent_time_mode int, 
parent_start decimal(20,3), 
parent_duration int, 
parent_calc_start decimal(20,3), 
parent_calc_duration int, 
parent_calc_end decimal(20,3), 
parent_position int, 
app_data varchar(512), 
app_data_aux int);   
*/




												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT traffic_file_id, traffic_status FROM %s.dbo.%s WHERE file_path = '%s\\%s'", 
													((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
													((g_psentinel->m_settings.m_pszTrafficFiles)&&(strlen(g_psentinel->m_settings.m_pszTrafficFiles)))?g_psentinel->m_settings.m_pszTrafficFiles:"Traffic_Files",
													pDirChange->m_szFullPath, 
													pDirChange->m_szFilename
													);

												int nFileID = -1;

//												EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
												CRecordset* prs = db.Retrieve(pdbConn, szSQL, dberrorstring);
												if(prs)
												{
													//while (!prs->IsEOF())
													if (!prs->IsEOF())
													{
														CString szTemp;
														try
														{
															prs->GetFieldValue("traffic_file_id", szTemp);//HARDCODE
															nFileID = atoi(szTemp);
															// prs->GetFieldValue("traffic_status", szTemp);//HARDCODE
															// deal with status some other time.
														}
														catch( ... )
														{
														}
														//prs->MoveNext();
													}
													prs->Close();

													try{delete prs;} catch(...){}
													prs=NULL;
												}
//												LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);


												// let's read the file in.
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "opening %s...", 	pszPath );//  Sleep(100);

												int nNumEvents = 0;

												FILE* fp = NULL;
												char* pchFileBuffer = NULL;
												unsigned long ulFileLen = 0;
												fp = fopen(pszPath, "rb");
												if(fp)
												{

													fseek(fp,0,SEEK_END);
													ulFileLen = ftell(fp);
													fseek(fp,0,SEEK_SET);

													// allocate buffer and read in file contents
													pchFileBuffer = (char*) malloc(ulFileLen+1); // for 0
													if(pchFileBuffer!=NULL)
													{
														fread(pchFileBuffer, sizeof(char), ulFileLen, fp);
														memset(pchFileBuffer+ulFileLen, 0, 1);
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
{
	// save this file down 
	FILE* fpWrite = fopen("parse.lst", "wb");
	if(fpWrite)
	{
		fwrite(pchFileBuffer, sizeof(char), ulFileLen, fpWrite);
		fflush(fpWrite);
		fclose(fpWrite);
	}
}


														try
														{
															nNumEvents = adcutil.CountEventsInStream((unsigned char*)pchFileBuffer, ulFileLen);
														}
														catch(...)
														{
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Traffic", "Exception parsing event count in %s...", 	pszPath );//  Sleep(100);
															nNumEvents = -1;
														}
													} // else this is a problem, need to recheck it later on.
													else nNumEvents = -1;
													fclose(fp);
												
												}
												// else this is a problem, need to recheck it later on.
												else nNumEvents = -1;

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "opened %s, file ID is %d, num=%d, len=%d", 	pszPath , nFileID, nNumEvents,ulFileLen); // Sleep(100);
												

												if(nFileID>0) // we have one.
												{
													// need to clear it out and re-add.... OR do we want to go event by event and change?
													// Let's just clear events and re-add for now.

													// first delete all events for the file

													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s WHERE traffic_file_id = %d", 
														((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
														((g_psentinel->m_settings.m_pszTraffic)&&(strlen(g_psentinel->m_settings.m_pszTraffic)))?g_psentinel->m_settings.m_pszTraffic:"Traffic_Events",
														nFileID
														);

if(g_psentinel->m_settings.m_bDebugSQL) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pszMessageSource, "executing: [%s]", szSQL); //Sleep(100);
//													EnterCriticalSection(&g_psentinel->m_data.m_critSQL);

													if(db.ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
													{
														//message out.
														g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pszMessageSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
													}
//													LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

													//then update the file record with new status and params.
/*
if(g_psentinel->m_settings.m_bDebugSQL) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pszMessageSource, "executed: [%s]", szSQL); //Sleep(100);
if(g_psentinel->m_settings.m_bDebugSQL) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pszMessageSource, "about to mod %s.dbo.%s, %d, %d, %d, %d, %d: [%s:%d] %d", 
														((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
														((g_psentinel->m_settings.m_pszTrafficFiles)&&(strlen(g_psentinel->m_settings.m_pszTrafficFiles)))?g_psentinel->m_settings.m_pszTrafficFiles:"Traffic_Files",
														util.FileTimeToUnixTime(pDirChange->m_ftFileModified, TRUE), 
														ulFileLen,
														ulDate,
														SENTINEL_TRAFFIC_ANALYZING,
														nNumEvents,
														pszServerName?pszServerName:"unknown", 
														nHarrisList,
														nFileID
	); Sleep(100);
*/
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET \
file_timestamp = %d, \
file_size = %d, \
traffic_timestamp = %d, \
traffic_status = %d, \
traffic_events = %d, \
server_name = '%s', \
server_list = %d \
WHERE traffic_file_id = %d", 
														((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
														((g_psentinel->m_settings.m_pszTrafficFiles)&&(strlen(g_psentinel->m_settings.m_pszTrafficFiles)))?g_psentinel->m_settings.m_pszTrafficFiles:"Traffic_Files",
														util.FileTimeToUnixTime(pDirChange->m_ftFileModified, TRUE), 
														ulFileLen,
														ulDate,
														SENTINEL_TRAFFIC_ANALYZING,
														nNumEvents,
														pszServerName?pszServerName:"unknown", 
														nHarrisList,
														nFileID
														);

if(g_psentinel->m_settings.m_bDebugSQL) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pszMessageSource, "executing: [%s]", szSQL); //Sleep(100);


//													EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
													if(db.ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
													{
														//message out.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pszMessageSource, "SQL error: %s", dberrorstring); // Sleep(100); //(Dispatch message)
													}
//													LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

												}
												else
												{
													//new, need to add with a status.  
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s (\
file_path, file_timestamp, file_size, traffic_timestamp, traffic_status, traffic_events, server_name, server_list) \
VALUES ('%s', %d, %d, %d, %d, %d, '%s', %d)", 
														((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
														((g_psentinel->m_settings.m_pszTrafficFiles)&&(strlen(g_psentinel->m_settings.m_pszTrafficFiles)))?g_psentinel->m_settings.m_pszTrafficFiles:"Traffic_Files",
														pszPath,
														util.FileTimeToUnixTime(pDirChange->m_ftFileModified, TRUE), 
														ulFileLen,
														ulDate,
														SENTINEL_TRAFFIC_ANALYZING,
														nNumEvents,
														pszServerName?pszServerName:"unknown", 
														nHarrisList
														
														);

if(g_psentinel->m_settings.m_bDebugSQL) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pszMessageSource, "executing: [%s]", szSQL);


//													EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
													if(db.ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
													{
														//message out.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pszMessageSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
													}
//													LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

													// then retrieve the nFileID

													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT traffic_file_id, traffic_status FROM %s.dbo.%s WHERE file_path = '%s\\%s'", 
														((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
														((g_psentinel->m_settings.m_pszTrafficFiles)&&(strlen(g_psentinel->m_settings.m_pszTrafficFiles)))?g_psentinel->m_settings.m_pszTrafficFiles:"Traffic_Files",
														pDirChange->m_szFullPath, 
														pDirChange->m_szFilename
														);
//													EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
if(g_psentinel->m_settings.m_bDebugSQL) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pszMessageSource, "executing: [%s]", szSQL);

													CRecordset* prs = db.Retrieve(pdbConn, szSQL, dberrorstring);
													if(prs)
													{
														//while (!prs->IsEOF())
														if (!prs->IsEOF())
														{
															CString szTemp;
															try
															{
																prs->GetFieldValue("traffic_file_id", szTemp);//HARDCODE
																nFileID = atoi(szTemp);
																// prs->GetFieldValue("traffic_status", szTemp);//HARDCODE
																// deal with status some other time.
															}
															catch( ... )
															{
															}
															//prs->MoveNext();
														}
														prs->Close();

														delete prs;
														prs=NULL;
													}
//													LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
												}
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "opened %s, file ID is now %d", 	pszPath , nFileID);  //Sleep(1000);

												if(nFileID>0)  // we were able to insert.
												{

													numFileChanges++;
													if(pObj)
													{
														pObj->m_nID = nFileID;
														if(pObj->m_pszFileName)
														{
															try{ free(pObj->m_pszFileName); } catch(...){}
															pObj->m_pszFileName = NULL;
														}
														pObj->m_pszFileName = (char*)malloc(strlen(pszPath)+1);
														if(pObj->m_pszFileName) strcpy(pObj->m_pszFileName, pszPath);
														pObj->m_pChannel = pChannel;													
													}

													int nEventCount = -1;//error

													//OK so let's parse the file and insert all the events.
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "about to parse file ID %d", 	nFileID);  //Sleep(100);

													if((pchFileBuffer!=NULL)&&(pObj!=NULL))
													{
														nEventCount=0;

														BYTE systemfrx = 0x29;
														
														if(nNumEvents>0)
														{
															EnterCriticalSection(&(pObj->m_List.m_crit));  //  critical section.
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "EnsureAllocated", "allocate for file ID %d", 	nFileID);  //Sleep(100);
															pObj->m_List.EnsureAllocated(nNumEvents); // should check ADC_SUCCESS, but for now we assume success

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "StreamToArray", "about to parse stream for file ID %d", 	nFileID);  //Sleep(100);
															try
															{
																if((pChannel)&&(pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn)))
																{
if(pChannel->m_pcritAPI)
{
	EnterCriticalSection(pChannel->m_pcritAPI);
																	systemfrx = (*(pChannel->m_ppAPIConn))->m_SysData.systemfrx;
	LeaveCriticalSection(pChannel->m_pcritAPI);
}
else
{
																	systemfrx = (*(pChannel->m_ppAPIConn))->m_SysData.systemfrx;
}
																	nEventCount = adcutil.StreamToArray(&(pObj->m_List), 0, (unsigned char*) pchFileBuffer, systemfrx, nNumEvents);
																}
																else
																{
																	//default ntsc
																	systemfrx = 0x29;
																	nEventCount = adcutil.StreamToArray(&(pObj->m_List), 0, (unsigned char*) pchFileBuffer, 0x29, nNumEvents);
																}
															}
															catch(...)
															{
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Traffic", "Exception parsing event count in %s...", 	pszPath );//  Sleep(100);

																nEventCount = -1;
																nNumEvents = -1;
															}

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "StreamToArray", "parsed file ID %d, count=%d", 	nFileID, nEventCount);  //Sleep(100);

															LeaveCriticalSection(&(pObj->m_List.m_crit));  //  critical section.

															if(nEventCount<0)
															{
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pszMessageSource, "An error occurred parsing the file [%s]", pszPath);
															}
															else
															{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pszMessageSource, "stream to array succeeded for file ID %d", 	nFileID); // Sleep(100);

																numEventChanges++;
																// go thru and set it up...
																pObj->m_List.m_nEventCount = nEventCount;
																nNumEvents = 0;

	unsigned short usHARRISDONEORPLAY = ((1<<eventdone)|(1<<eventpostrolled)|(1<<eventrunning)|(1<<notplayed)|(1<<eventskipped));
	unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));
	unsigned short usHARRISHARD = (1<<autotimed);

																// set up a running total of times and such
				bool bPostTimeshifter = false;  // must reset this for top of list.
				bool bPostTimeshiftPrimary = false;  // must invalidate times after an upcounting event, but not for secondaries, necessarily.

				double dblLastCalcPrimaryEventTime = -1.0;
				double dblLastCalcEventTime = -1.0;
				double dblLastPrimaryEventTime = -1.0;
				double dblLastEventTime = -1.0;
				unsigned short usLastPrimaryOnAirJulianDate = DATE_NOT_DEFINED;
				unsigned long ulLastPrimaryOnAirTimeMS = TIME_NOT_DEFINED;  // day offset.
//				unsigned long ulLastPrimaryOnAirDurMS = TIME_NOT_DEFINED;
				double dblLastPrimaryOnAirDur = 0.0; //seconds
				unsigned long ulLastDate = ulDate;  // day.
				CSentinelEventObject* pParent = NULL;


																while(nNumEvents<pObj->m_List.m_nEventCount)
																{
																	// add sentinel events to match ADC events.
																	if((pObj->m_List.m_ppEvents)&&(pObj->m_List.m_ppEvents[nNumEvents]))
																	{
																		CSentinelEventObject* pEvent =  new CSentinelEventObject;
																		if(pEvent)
																		{
																			pObj->InsertTrafficEvent(pEvent);
																			pEvent->m_nFlags=SENTINEL_EVENT_NEW;
																			
																			double dblEventTime = 0.0; 
																			double dblCalcEventTime = 0.0; 
																			double dblOffsetTime = (pObj->m_List.m_ppEvents[nNumEvents]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)?(0.000):((double)pObj->m_List.m_ppEvents[nNumEvents]->m_ulOnAirTimeMS/1000.0);
																			pEvent->m_event.m_usType = pObj->m_List.m_ppEvents[nNumEvents]->m_usType;


																			if(pEvent->m_event.m_usType&SECONDARYEVENT)
																			{
											switch(pEvent->m_event.m_usType&0xff)
											{
											case SECBACKAVEVENT:
											case SECBACKTIMEGPI:
											case SECBACKSYSTEM:
												{

													if((dblLastPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
													{
														dblEventTime = ((double)(ulLastDate)) - dblOffsetTime;
													}
													else
													{
														dblEventTime = dblLastPrimaryEventTime - dblOffsetTime;
													}

													if((dblLastCalcPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
													{
														dblCalcEventTime = ((double)(ulLastDate)) - dblOffsetTime;
													}
													else
													{
														dblCalcEventTime = dblLastCalcPrimaryEventTime - dblOffsetTime;
													}
												} break;
											case SECCOMMENT:
											case SECCOMPILEID:// (Byte)(225)
											case SECAPPFLAG:// (Byte)(226)
											case SECBARTERSPOT:// (Byte)(227)
											case INVALIDEVENT:
												{
													if((dblLastPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
													{
														dblEventTime = ((double)(ulLastDate));
													}
													else
													{
														dblEventTime = dblLastPrimaryEventTime;
													}

													if((dblLastCalcPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
													{
														dblCalcEventTime = ((double)(ulLastDate));
													}
													else
													{
														dblCalcEventTime = dblLastCalcPrimaryEventTime;
													}													// a comment, for instance, has TIME_NOT_DEFINED. but we want it to come after the primary it belongs to,
													// so we used to give it .001 seconds.  Now we give it the correct actual time, 
													// but sort it on parent_start_calc + (event_position-parent_position)/1000.0, or more correctly
													// order by parent_position<0?((event_position)/1000.0):(parent_calc_start + (event_position-parent_position)/1000.0)
												} break;
											default:
												{
													if((dblLastPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
													{
														dblEventTime = ((double)(ulLastDate)) + dblOffsetTime;
													}
													else
													{
														dblEventTime = dblLastPrimaryEventTime + dblOffsetTime;
													}

													if((dblLastCalcPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
													{
														dblCalcEventTime = ((double)(ulLastDate)) + dblOffsetTime;
													}
													else
													{
														dblCalcEventTime = dblLastCalcPrimaryEventTime + dblOffsetTime;
													}
												} break;
											}
																			}
																			else  // else its a primary event
																			{

											if(ulLastPrimaryOnAirTimeMS!=TIME_NOT_DEFINED)  // should be the only check on valid last primary.
											{
												if(ulLastPrimaryOnAirTimeMS > pObj->m_List.m_ppEvents[nNumEvents]->m_ulOnAirTimeMS)
												{
													// we wrapped days.
													ulLastDate+=86400;
												}
											}

											dblEventTime = ((double)(ulLastDate)) + dblOffsetTime;

											if(
												  (pObj->m_List.m_ppEvents[nNumEvents]->m_usStatus&usHARRISDONEORPLAY)||  // dont need for traffic lists, but can't hurt.
													(pObj->m_List.m_ppEvents[nNumEvents]->m_ulControl&usHARRISHARD)// hard start
												)
											{
												dblCalcEventTime = dblEventTime;
											}
											else
											{
												if(ulLastPrimaryOnAirTimeMS!=TIME_NOT_DEFINED)  // should be the only check on valid last primary.
												{
													dblCalcEventTime = dblLastCalcPrimaryEventTime + dblLastPrimaryOnAirDur;
												}
												else
												{
													dblCalcEventTime = dblEventTime;
												}
											}
											dblLastPrimaryOnAirDur = ((pObj->m_List.m_ppEvents[nNumEvents]->m_ulDurationMS==TIME_NOT_DEFINED)?0.0:((double)pObj->m_List.m_ppEvents[nNumEvents]->m_ulDurationMS/1000.0));
											ulLastPrimaryOnAirTimeMS = pObj->m_List.m_ppEvents[nNumEvents]->m_ulOnAirTimeMS;
											dblLastPrimaryEventTime = dblEventTime;
											dblLastCalcPrimaryEventTime = dblCalcEventTime;
											pParent = pEvent;

																			}

										dblLastCalcEventTime = dblCalcEventTime;
										dblLastEventTime = dblEventTime;

										pEvent->m_event.m_pszID = pObj->m_List.m_ppEvents[nNumEvents]->m_pszID; 
										pEvent->m_event.m_pszTitle = pObj->m_List.m_ppEvents[nNumEvents]->m_pszTitle; 
										pEvent->m_event.m_pszData = pObj->m_List.m_ppEvents[nNumEvents]->m_pszData; 
										pEvent->m_event.m_pszReconcileKey = pObj->m_List.m_ppEvents[nNumEvents]->m_pszReconcileKey; 
										pEvent->m_event.m_ucSegment = pObj->m_List.m_ppEvents[nNumEvents]->m_ucSegment; 
										pEvent->m_event.m_ulOnAirTimeMS = pObj->m_List.m_ppEvents[nNumEvents]->m_ulOnAirTimeMS; 
										pEvent->m_event.m_usOnAirJulianDate = pObj->m_List.m_ppEvents[nNumEvents]->m_usOnAirJulianDate; 
										pEvent->m_event.m_ulDurationMS = pObj->m_List.m_ppEvents[nNumEvents]->m_ulDurationMS; 
										pEvent->m_event.m_usStatus = pObj->m_List.m_ppEvents[nNumEvents]->m_usStatus; 
										pEvent->m_event.m_ulControl = pObj->m_List.m_ppEvents[nNumEvents]->m_ulControl; 
										pEvent->m_event.m_usDevice = pObj->m_List.m_ppEvents[nNumEvents]->m_usDevice; 

										if(bPostTimeshifter)
										{
											if(bPostTimeshiftPrimary)
											{
												pEvent->m_nFlags|=SENTINEL_EVENT_TIMESHIFTER;
											}
											else
											{
												if(pObj->m_List.m_ppEvents[nNumEvents]->m_usType&SECONDARYEVENT)
												{
													pEvent->m_nFlags|=SENTINEL_EVENT_CUTOFFRISK;
												}
												else
												{
													pEvent->m_nFlags|=SENTINEL_EVENT_TIMESHIFTER;
													bPostTimeshiftPrimary = true;
												}
											}
										}
										
										// check to see if upcounting event.
										if(pEvent->m_event.m_ulControl&(1<<autoupcount))
										{
											pEvent->m_nFlags|=SENTINEL_EVENT_UPCOUNT;
											if(!(pObj->m_List.m_ppEvents[nNumEvents]->m_usStatus&usHARRISDONE))
											{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pszMessageSource, "--  event [%s][%s]: non-done UPCOUNTER",pObj->m_List.m_ppEvents[nNumEvents]->m_pszID,pObj->m_List.m_ppEvents[nNumEvents]->m_pszTitle);

												bPostTimeshifter = true;
											}
										}
										if(pEvent->m_event.m_ulControl&(1<<manualstart))
										{
											pEvent->m_nFlags|=SENTINEL_EVENT_MANUAL;
											if(!(pObj->m_List.m_ppEvents[nNumEvents]->m_usStatus&usHARRISDONEORPLAY))
											{
												pEvent->m_nFlags|=SENTINEL_EVENT_TIMESHIFTER; // manual events themselves are timeshifters.
												bPostTimeshiftPrimary = true;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pszMessageSource, "--  event [%s][%s]: non-done MANUAL",pObj->m_List.m_ppEvents[nNumEvents]->m_pszID,pObj->m_List.m_ppEvents[nNumEvents]->m_pszTitle);
												bPostTimeshifter = true;
											}
										}
										else
										if(pEvent->m_event.m_ulControl&(1<<autotimed))
										{
											pEvent->m_nFlags|=SENTINEL_EVENT_HARD;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pszMessageSource, "--  event [%s][%s]: HARD START",pObj->m_List.m_ppEvents[nNumEvents]->m_pszID,pObj->m_List.m_ppEvents[nNumEvents]->m_pszTitle);
										}
										

										pEvent->m_nPosition=nNumEvents;  // the Harris list position
										pEvent->m_dblTime     = dblEventTime;
										pEvent->m_dblCalcTime = dblLastCalcEventTime;
										pEvent->m_nCalcDur = ((pObj->m_List.m_ppEvents[nNumEvents]->m_ulDurationMS==TIME_NOT_DEFINED)?0:pObj->m_List.m_ppEvents[nNumEvents]->m_ulDurationMS); // have to cut this off if the next event is a hard start

										pEvent->m_pParent = pParent;

										pObj->AssembleKey(nNumEvents, g_psentinel->m_settings.m_pszEventKeyFormat, systemfrx);

										if(pObj->m_List.m_ppEvents[nNumEvents]->m_pszID)
										{
											pEvent->m_event.m_pszID = (char*)malloc(strlen(pObj->m_List.m_ppEvents[nNumEvents]->m_pszID)+1); 
											if(pEvent->m_event.m_pszID) strcpy(pEvent->m_event.m_pszID,pObj->m_List.m_ppEvents[nNumEvents]->m_pszID);
											pEvent->m_pszEncodedID = pObj->m_db.EncodeQuotes(pObj->m_List.m_ppEvents[nNumEvents]->m_pszID);
										}

										if(pObj->m_List.m_ppEvents[nNumEvents]->m_pszTitle)
										{
											pEvent->m_event.m_pszTitle = (char*)malloc(strlen(pObj->m_List.m_ppEvents[nNumEvents]->m_pszTitle)+1); 
											if(pEvent->m_event.m_pszTitle) strcpy(pEvent->m_event.m_pszTitle,pObj->m_List.m_ppEvents[nNumEvents]->m_pszTitle);
											pEvent->m_pszEncodedTitle = pObj->m_db.EncodeQuotes(pObj->m_List.m_ppEvents[nNumEvents]->m_pszTitle);
										}
										if(pObj->m_List.m_ppEvents[nNumEvents]->m_pszData)
										{
											pEvent->m_event.m_pszData = (char*)malloc(strlen(pObj->m_List.m_ppEvents[nNumEvents]->m_pszData)+1); 
											if(pEvent->m_event.m_pszData) strcpy(pEvent->m_event.m_pszData,pObj->m_List.m_ppEvents[nNumEvents]->m_pszData);
											pEvent->m_pszEncodedData = pObj->m_db.EncodeQuotes(pObj->m_List.m_ppEvents[nNumEvents]->m_pszData);
										}
										if(pObj->m_List.m_ppEvents[nNumEvents]->m_pszReconcileKey)
										{
											pEvent->m_event.m_pszReconcileKey = (char*)malloc(strlen(pObj->m_List.m_ppEvents[nNumEvents]->m_pszReconcileKey)+1); 
											if(pEvent->m_event.m_pszReconcileKey) strcpy(pEvent->m_event.m_pszReconcileKey,pObj->m_List.m_ppEvents[nNumEvents]->m_pszReconcileKey);
											pEvent->m_pszEncodedRecKey = pObj->m_db.EncodeQuotes(pObj->m_List.m_ppEvents[nNumEvents]->m_pszReconcileKey);
										}

//										pEvent->m_nPrecedingUid = nPrecedingUid;
/*

							//create a UUID from which to get the identity...
								UUID uuid;
								UuidCreate(&uuid); // could check response code to be RPC_S_OK ?

					//			CoCreateGuid(&uuid);

								unsigned char* pucUUID = NULL;
								UuidToString(&uuid, &pucUUID);
								// call this later: RpcStringFree(&pucUUID);
*/
										//not going to use UUID (at least, for now)


if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pszMessageSource, "about to insert event [%s] for file ID %d", pEvent->m_pszEncodedID,	nFileID); //Sleep(100);
										
										pEvent->m_uid = g_psentinel->m_data.GetNewUID();
																		// then update SQL and set it
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s (\
itemid, \
traffic_file_id, \
event_key, \
server_name, \
server_list, \
list_id, \
event_id, \
event_clip, \
event_segment, \
event_title, \
event_data, \
event_type, \
event_status, \
event_time_mode, \
event_start, \
event_duration, \
event_calc_start, \
event_calc_duration, \
event_calc_end, \
event_position, \
event_last_update, \
parent_uid, \
parent_key, \
parent_id, \
parent_clip, \
parent_segment, \
parent_title, \
parent_type, \
parent_status, \
parent_time_mode, \
parent_start, \
parent_duration, \
parent_calc_start, \
parent_calc_duration, \
parent_calc_end, \
parent_position, \
app_data, \
app_data_aux) \
VALUES (%d, %d, '%s','%s', %d, %d, '%s', '%s', %d, '%s', '%s', %d, %d, %d, %3f, %d, %.3f, %d, %.3f, %d, %d.%03d, \
%d, '%s', '%s', '%s', %d, '%s', %d, %d, %d, %.3f, %d, %.3f, %d, %.3f, %d, '%s', '')",  // make the last one blank, we don't need 
																		((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
																		((g_psentinel->m_settings.m_pszTraffic)&&(strlen(g_psentinel->m_settings.m_pszTraffic)))?g_psentinel->m_settings.m_pszTraffic:"Traffic_Events",
pEvent->m_uid,
nFileID, //traffic_file_id int,
(pEvent->m_pszEncodedKey?pEvent->m_pszEncodedKey:""), //event_key varchar(256), 
(pszServerName?pszServerName:"unknown"), //server_name varchar(32), 
nHarrisList, //server_list int, 
nChannel, //list_id int, 
(pEvent->m_pszEncodedRecKey?pEvent->m_pszEncodedRecKey:""), //event_id varchar(32), 
(pEvent->m_pszEncodedID?pEvent->m_pszEncodedID:""), //event_clip varchar(64), 
((pEvent->m_event.m_ucSegment==0xff)?-1:pEvent->m_event.m_ucSegment),//event_segment int, 
(pEvent->m_pszEncodedTitle?pEvent->m_pszEncodedTitle:""),//event_title varchar(64), 
(pEvent->m_pszEncodedData?pEvent->m_pszEncodedData:""), //event_data varchar(4096),
pEvent->m_event.m_usType,//event_type int, 
(pEvent->m_event.m_usStatus|((pEvent->m_nFlags&SENTINEL_EVENT_TIMEMASK)?(pEvent->m_nFlags&SENTINEL_EVENT_TIMEMASK):0)), //event_status int, 
pEvent->m_event.m_ulControl, //event_time_mode int, 
pEvent->m_dblTime, //event_start decimal(20,3), 
pEvent->m_event.m_ulDurationMS, //event_duration int, 
pEvent->m_dblCalcTime, //event_calc_start decimal(20,3), 
pEvent->m_nCalcDur,//event_calc_duration int, 
(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0)),//event_calc_end decimal(20,3), 
pEvent->m_nPosition+1,//event_position int, since it is one-based in Harris.
(unsigned long)(g_psentinel->m_data.m_timebTick.time - (g_psentinel->m_data.m_timebTick.timezone*60)+(g_psentinel->m_data.m_timebTick.dstflag?3600:0)), // local time....g_psentinel->m_data.m_timebTick, //event_last_update decimal(20,3), 
g_psentinel->m_data.m_timebTick.millitm,
(pParent?pParent->m_uid:-1),//parent_uid
(((pParent)&&(pParent->m_pszEncodedKey))?pParent->m_pszEncodedKey:""),//parent_key varchar(256), 
(((pParent)&&(pParent->m_pszEncodedRecKey))?pParent->m_pszEncodedRecKey:""),//parent_id varchar(32), 
(((pParent)&&(pParent->m_pszEncodedID))?pParent->m_pszEncodedID:""),//parent_clip varchar(64), 
(pParent?pParent->m_event.m_ucSegment:-1), //parent_segment int, 
(((pParent)&&(pParent->m_pszEncodedTitle))?pParent->m_pszEncodedTitle:""),//parent_title varchar(64), 
(pParent?pParent->m_event.m_usType:0),//parent_type int, 
(pParent?(pParent->m_event.m_usStatus|((pParent->m_nFlags&SENTINEL_EVENT_TIMEMASK)?(pParent->m_nFlags&SENTINEL_EVENT_TIMEMASK):0)):0),//parent_status int, 
(pParent?pParent->m_event.m_ulControl:-1),//parent_time_mode int, 
(pParent?pParent->m_dblTime:0),//parent_start decimal(20,3), 
(pParent?pParent->m_event.m_ulDurationMS:-1),//parent_duration int, 
(pParent?pParent->m_dblCalcTime:0),//parent_calc_start decimal(20,3), 
(pParent?pParent->m_nCalcDur:-1),//parent_calc_duration int, 
(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0), //parent_calc_end decimal(20,3), 
(pParent?(pParent->m_nPosition+1):-1),//parent_position int, 
""//pucUUID//app_data varchar(512), // made it blank, we are not using.
//app_data_aux int
);  
if(g_psentinel->m_settings.m_bDebugSQL) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pszMessageSource, "executing: [%s]", szSQL);  //Sleep(100);


//																			EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
																			if(db.ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
																			{
																				//message out.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pszMessageSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
																			}
//																			LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

/* //not using uuid
																			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT itemid, FROM %s.dbo.%s WHERE app_data = '%s'", 
																				((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
																				((g_psentinel->m_settings.m_pszTraffic)&&(strlen(g_psentinel->m_settings.m_pszTraffic)))?g_psentinel->m_settings.m_pszTraffic:"Traffic_Events",
																				pucUUID
																				);

//																			EnterCriticalSection(&g_psentinel->m_data.m_critSQL);

																			CRecordset* prs = db.Retrieve(pdbConn, szSQL, dberrorstring);
																			if(prs)
																			{
																				//while (!prs->IsEOF())
																				if (!prs->IsEOF())
																				{
																					CString szTemp;
																					try
																					{
																						prs->GetFieldValue("itemid", szTemp);//HARDCODE
																						pEvent->m_uid = atoi(szTemp);
																						// prs->GetFieldValue("traffic_status", szTemp);//HARDCODE
																						// deal with status some other time.
																					}
																					catch( ... )
																					{
																					}
																					//prs->MoveNext();
																				}
																				prs->Close();

																				delete prs;
																				prs=NULL;
																			}
//																			LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
																		
																			try{ RpcStringFree(&pucUUID);} catch(...){}
*/
																		}
																		else
																		{
																			//error
																			nEventCount = -1;  break;
																		}
																	}
																	else
																	{
																		nEventCount = -1;  break;
																	}

																	

																	nNumEvents++;
																} // while
															}// no error parsing file
														}// if(nNumEvents>0)
													}//if(pchFileBuffer!=NULL)
													else
													{
														nEventCount = -1;  break;
													}


													if(nEventCount<0)
													{
														// error!
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET \
file_timestamp = %d, \
file_size = %d, \
traffic_timestamp = %d, \
traffic_status = %d, \
traffic_events = %d, \
server_name = '%s', \
server_list = %d \
WHERE traffic_file_id = %d", 
															((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
															((g_psentinel->m_settings.m_pszTrafficFiles)&&(strlen(g_psentinel->m_settings.m_pszTrafficFiles)))?g_psentinel->m_settings.m_pszTrafficFiles:"Traffic_Files",
															util.FileTimeToUnixTime(pDirChange->m_ftFileModified, TRUE), 
															ulFileLen,
															ulDate,
															SENTINEL_TRAFFIC_ERROR,
															nNumEvents,
															pszServerName?pszServerName:"unknown", 
															nHarrisList,
															nFileID
															);

	if(g_psentinel->m_settings.m_bDebugSQL) 
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pszMessageSource, "executing: [%s]", szSQL);


//														EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
														if(db.ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
														{
															//message out.
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pszMessageSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
														}
//														LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

// set this file up again for a recheck later on.

//														error interval?

													}
													else
													{
														// success, mark file as done
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s.dbo.%s SET \
file_timestamp = %d, \
file_size = %d, \
traffic_timestamp = %d, \
traffic_status = %d, \
traffic_events = %d, \
server_name = '%s', \
server_list = %d \
WHERE traffic_file_id = %d", 
															((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
															((g_psentinel->m_settings.m_pszTrafficFiles)&&(strlen(g_psentinel->m_settings.m_pszTrafficFiles)))?g_psentinel->m_settings.m_pszTrafficFiles:"Traffic_Files",
															util.FileTimeToUnixTime(pDirChange->m_ftFileModified, TRUE), 
															ulFileLen,
															ulDate,
															SENTINEL_TRAFFIC_IMPORTED,
															nNumEvents,
															pszServerName?pszServerName:"unknown", 
															nHarrisList,
															nFileID
															);

	if(g_psentinel->m_settings.m_bDebugSQL) 
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pszMessageSource, "executing: [%s]", szSQL);


//														EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
														if(db.ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
														{
															//message out.
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pszMessageSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
														}
//														LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
														
													}
												}
												else
												{
													// couldnt get unique ID.
													if((nTrafficObjectindex>=0)&&(pChannel)) pChannel->DeleteTrafficObject(nTrafficObjectindex);
												}


												if(pchFileBuffer)
												{ 
													try{ free(pchFileBuffer);}
													catch(...) {}
													pchFileBuffer = NULL;
												}

												

											} // proceed
											//else ignore this file.
										}  // its a file
									}// else FALSE=if((strcmp(pDirChange->m_szFilename, "."))&&(strcmp(pDirChange->m_szFilename, ".."))) 
								}//else FALSE = if((pDirChange->m_szFilename)&&(pDirChange->m_szFullPath))
							} break;
						case WATCH_DEL: //		0x00000002  // was del
							{
								// a file or dir was deleted in the watchfolder. (manually)
								// if a file, we need to update metadata
								// but only if:
								// WE did not delete it, because m_bDeleteSourceFileOnTransfer was true
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "DEL: %s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);  //(Dispatch message)

								if((pDirChange->m_szFilename)&&(pDirChange->m_szFullPath))
								{
									if((strcmp(pDirChange->m_szFilename, "."))&&(strcmp(pDirChange->m_szFilename, ".."))) // only if it's a real file or dir
									{
										if(pDirChange->m_ulFileAttribs&FILE_ATTRIBUTE_DIRECTORY)  // its a dir
										{
											// can ignore
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "dir deleted: %s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);//  Sleep(100);

							
										}
										else // its a file
										{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "file deleted: %s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);//  Sleep(100);


// must delete all events in that file, and the file record as well.
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT traffic_file_id, traffic_status FROM %s.dbo.%s WHERE file_path = '%s\\%s'", 
												((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
												((g_psentinel->m_settings.m_pszTrafficFiles)&&(strlen(g_psentinel->m_settings.m_pszTrafficFiles)))?g_psentinel->m_settings.m_pszTrafficFiles:"Traffic_Files",
												pDirChange->m_szFullPath, 
												pDirChange->m_szFilename
												);

											int nFileID = -1;

//											EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
											CRecordset* prs = db.Retrieve(pdbConn, szSQL, dberrorstring);
											if(prs)
											{
												//while (!prs->IsEOF())
												if (!prs->IsEOF())
												{
													CString szTemp;
													try
													{
														prs->GetFieldValue("traffic_file_id", szTemp);//HARDCODE
														nFileID = atoi(szTemp);
														// prs->GetFieldValue("traffic_status", szTemp);//HARDCODE
														// deal with status some other time.
													}
													catch( ... )
													{
													}
													//prs->MoveNext();
												}
												prs->Close();

												try{delete prs;} catch(...){}
												prs=NULL;
											}
//											LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s WHERE file_path = '%s\\%s'", 
												((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
												((g_psentinel->m_settings.m_pszTrafficFiles)&&(strlen(g_psentinel->m_settings.m_pszTrafficFiles)))?g_psentinel->m_settings.m_pszTrafficFiles:"Traffic_Files",
												pDirChange->m_szFullPath, 
												pDirChange->m_szFilename
												);

if(g_psentinel->m_settings.m_bDebugSQL) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "TrafficDelete", "executing: [%s]", szSQL);


//											EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
											if(db.ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
											{
												//message out.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "TrafficDelete", "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
											}
//											LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

											if(nFileID>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s WHERE traffic_file_id = %d", 
													((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
													((g_psentinel->m_settings.m_pszTraffic)&&(strlen(g_psentinel->m_settings.m_pszTraffic)))?g_psentinel->m_settings.m_pszTraffic:"Traffic_Events",
													nFileID
													);

if(g_psentinel->m_settings.m_bDebugSQL) 
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "TrafficDelete", "executing: [%s]", szSQL);
//												EnterCriticalSection(&g_psentinel->m_data.m_critSQL);

												if(db.ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
												{
													//message out.
													g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "TrafficDelete", "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
												}
//												LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
											}
											numFileChanges++;
											//now delete the memory object.

											char* pszServerName = NULL;
											int nHarrisList=-1;
											CSentinelChannelObject* pChannel = NULL;
//											char pszMessageSource[MAX_PATH];
											int nTrafficObjectindex = -1;
//											CSentinelTrafficObject* pObj = NULL;
											int nChannel=-1;
											sprintf(pszPath, "%s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);
											unsigned long ulDate = TIME_NOT_DEFINED;
											// 1) what day is this file for (so we can exclude it if it is in the past)

							// harris unixtime is "local unixtime" no DST calc.
											ulDate = util.DateFromName(pszPath);
											if(ulDate != TIME_NOT_DEFINED)
											{
												nChannel = util.ChannelFromName(pszPath, &pszServerName, &nHarrisList, (void**)(&pChannel));
												if(nChannel >= 0)
												{
													// we succeeded, so we should have server name, list, etc.
													// let's see if we have the object already
													if(pChannel)
													{
														nTrafficObjectindex = pChannel->FindTrafficObjectByDate(ulDate);
														if(nTrafficObjectindex>=0) // clear it.
														{
															pChannel->DeleteTrafficObject(nTrafficObjectindex);
														}
													}
												}
											}
											
										}
									}// else FALSE=if((strcmp(pDirChange->m_szFilename, "."))&&(strcmp(pDirChange->m_szFilename, ".."))) 
								}//else FALSE = if((pDirChange->m_szFilename)&&(pDirChange->m_szFullPath))
							}
							break;
						default: 
							break;  // unknown
						} //switch

/*
			// debug file
						FILE* fp;
						fp = fopen("_removedlog.log", "at");
						if(fp)
						{
							fprintf(fp, "%s\\%s %s %d %d%d %d%db 0x%08x\r\n", 	
								pDirChange->m_szFullPath?pDirChange->m_szFullPath:"(null)",
								pDirChange->m_szFilename?pDirChange->m_szFilename:"(null)",
								foo,
								pDirChange->m_ulEntryTimestamp,			// unixtime, seconds resolution, last modified
								pDirChange->m_ftFileModified.dwHighDateTime,			// unixtime, seconds resolution, last modified
								pDirChange->m_ftFileModified.dwLowDateTime,			// unixtime, seconds resolution, last modified
								pDirChange->m_ulFileSizeHigh,				// filesize		DWORD    nFileSizeHigh; 
								pDirChange->m_ulFileSizeLow,				// filesize		DWORD    nFileSizeLow;
								pDirChange->m_ulFileAttribs    // attribs returned by GetFileAttributes
								);
							fflush(fp);
							fclose(fp);
						}


						Message("%s\\%s %s %d %d%d %d%db 0x%08x", 	
							pDirChange->m_szFullPath?pDirChange->m_szFullPath:"(null)",
							pDirChange->m_szFilename?pDirChange->m_szFilename:"(null)",
							foo,
							pDirChange->m_ulEntryTimestamp,			// unixtime, seconds resolution, last modified
							pDirChange->m_ftFileModified.dwHighDateTime,			// unixtime, seconds resolution, last modified
							pDirChange->m_ftFileModified.dwLowDateTime,			// unixtime, seconds resolution, last modified
							pDirChange->m_ulFileSizeHigh,				// filesize		DWORD    nFileSizeHigh; 
							pDirChange->m_ulFileSizeLow,				// filesize		DWORD    nFileSizeLow;
							pDirChange->m_ulFileAttribs    // attribs returned by GetFileAttributes
							);
	*/
//	g_psentinel->m_msgr.DM(MSG_ICONNONE, NULL, "***debug***", "about to delete pDirChange");  //(Dispatch message)
//Sleep(200);
						if(pDirChange) { try{ delete(pDirChange);} catch(...){}}
//	g_psentinel->m_msgr.DM(MSG_ICONNONE, NULL, "***debug***", "deleted pDirChange");  //(Dispatch message)
//Sleep(200);
//	g_psentinel->m_msgr.DM(MSG_ICONNONE, NULL, "***debug***", "really deleted pDirChange");  //(Dispatch message)
//Sleep(200);

						pDirChange = NULL;

					} // while changes

g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%d changes registered, incrementing", numchanges);  //(Dispatch message)
					if(numFileChanges>0)
					{
						g_psentinel->m_data.IncrementDatabaseMods(g_psentinel->m_settings.m_pszTrafficFiles);
						if(	g_psentinel->m_data.m_nLastTrafficFileMod>=INT_MAX)
							g_psentinel->m_data.m_nLastTrafficFileMod=1;
						else
							g_psentinel->m_data.m_nLastTrafficFileMod++;
					}
					if(numEventChanges>0)
					{
						g_psentinel->m_data.IncrementDatabaseMods(g_psentinel->m_settings.m_pszTraffic);
						if(	g_psentinel->m_data.m_nLastTrafficEventsMod>=INT_MAX)
							g_psentinel->m_data.m_nLastTrafficEventsMod=1;
						else
							g_psentinel->m_data.m_nLastTrafficEventsMod++;
					}
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%d changes registered", numchanges);  //(Dispatch message)
//Sleep(100);

				}  // there are changes
				else //there are no changes, so we can do a date check if we need to
				{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "no changes %d", clock());  //(Dispatch message)
					// if the date has changed, check the done count, if older files wipe any events and the file record itself
					// then traverse the dir entry array and see if there are any files that are coming into the lookahead.
					// if so, we then... do the add procedure above by adding a CDirEntry item into the dirchange list that then
					// by adding #define WATCH_RECHK			0x00000020  // watchfolder needs to recheck a file	...
					// triggers the parse and import code above.

					// if the date has changed, check the done count, if older files wipe any events and the file record itself
					if((nLastTime/86400) != (g_psentinel->m_data.m_timeTrafficWatch.time/86400))
					{


						EnterCriticalSection(&g_psentinel->m_data.m_critChannels);
						if((g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_nNumChannelObjects))
						{
							int nCh = 0;
							while(nCh<g_psentinel->m_data.m_nNumChannelObjects)
							{
								CSentinelChannelObject* pChannelObj = g_psentinel->m_data.m_ppChannelObj[nCh];
								if(pChannelObj)
								{
									char pszMessageSource[MAX_PATH];

									if(pChannelObj)
									{
										_snprintf(pszMessageSource, MAX_PATH, "%s:%s:%d (%s) Traffic", 
											g_psentinel->m_settings.m_pszName,
											pChannelObj->m_pszServerName, //server_name
											pChannelObj->m_nHarrisListID, // server_list - actual 1-based harris list number
											pChannelObj->m_pszDesc);

									}
									else
									{
										_snprintf(pszMessageSource, MAX_PATH, "[unknown] Traffic");
									}


									if((pChannelObj->m_nNumTraffic)&&(pChannelObj->m_pptraffic))
									{
										int nTf=0;
										while(nTf<pChannelObj->m_nNumTraffic)
										{
											CSentinelTrafficObject* pObj = pChannelObj->m_pptraffic[nTf];
											if(pObj)
											{
												if(pObj->m_ulDate<(unsigned long)(g_psentinel->m_data.m_timebTick.time - (g_psentinel->m_settings.m_nTrafficFilesDoneDays*86400)- (g_psentinel->m_data.m_timebTick.timezone*60)+(g_psentinel->m_data.m_timebTick.dstflag?3600:0)))
												{
													// obsolete!  delete!

													// first delete the db records

					if(pdbConn)
					{

//						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s WHERE traffic_timestamp < %d", 
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s WHERE traffic_file_id = %d", 
						((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
						((g_psentinel->m_settings.m_pszTrafficFiles)&&(strlen(g_psentinel->m_settings.m_pszTrafficFiles)))?g_psentinel->m_settings.m_pszTrafficFiles:"Traffic_Files",
//							g_psentinel->m_data.m_timebTick.time - (g_psentinel->m_settings.m_nTrafficFilesDoneDays*86400)- (g_psentinel->m_data.m_timebTick.timezone*60)+(g_psentinel->m_data.m_timebTick.dstflag?3600:0)
							pObj->m_nID
							);

		if(g_psentinel->m_settings.m_bDebugSQL) 
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Traffic:init_db", "executing: [%s]", szSQL);
//												EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
						if(db.ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
							//message out.
							g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pszMessageSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
						}
//												LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);


						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s.dbo.%s WHERE traffic_file_id not in (SELECT traffic_file_id from %s.dbo.%s)", 
							((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
							((g_psentinel->m_settings.m_pszTraffic)&&(strlen(g_psentinel->m_settings.m_pszTraffic)))?g_psentinel->m_settings.m_pszTraffic:"Traffic_Events",
							((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
							((g_psentinel->m_settings.m_pszTrafficFiles)&&(strlen(g_psentinel->m_settings.m_pszTrafficFiles)))?g_psentinel->m_settings.m_pszTrafficFiles:"Traffic_Files"
							);

if(g_psentinel->m_settings.m_bDebugSQL) 
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pszMessageSource, "executing: [%s]", szSQL);
//												EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
						if(db.ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
							//message out.
							g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pszMessageSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
						}
//												LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

					}

													// then delete the object itself
													pChannelObj->DeleteTrafficObject(nTf);  // removes from array
													nTf--;  // back it up to resume from the correct place.

												}
											}
											nTf++;
										}
									}
								}
								nCh++;
							}


						}
						LeaveCriticalSection(&g_psentinel->m_data.m_critChannels);


					// then traverse the dir entry array and see if there are any files that are coming into the lookahead.
					// if so, we then... do the add procedure above by adding a CDirEntry item into the dirchange list that then
					// by adding #define WATCH_RECHK			0x00000020  // watchfolder needs to recheck a file	...
					// triggers the parse and import code above.

//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "CHECKING", "about to enter canon %d", clock());//  Sleep(100);

						EnterCriticalSection(&g_dir.m_critDirCanon);
						
						if((g_dir.m_ulNumDirEntries>0)&&(g_dir.m_ppDirCanon))
						{
							unsigned long x=0;
							while(x<g_dir.m_ulNumDirEntries)
							{
//	if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "CHECKING", "canon %d", x);//  Sleep(100);
								if(g_dir.m_ppDirCanon[x])
								{
									// check it to see if it needs further checking...
									if(!(g_dir.m_ppDirCanon[x]->m_ulFlags&WATCH_PROCESSED))
									{
										// check the date and set up a recheck if nec.

										unsigned long ulDate = TIME_NOT_DEFINED;
										sprintf(pszPath, "%s\\%s", g_dir.m_ppDirCanon[x]->m_szFullPath, g_dir.m_ppDirCanon[x]->m_szFilename);
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "file to be processed for recheck: %s\\%s", g_dir.m_ppDirCanon[x]->m_szFullPath, g_dir.m_ppDirCanon[x]->m_szFilename);//  Sleep(100);
											// need to find out 
											// 1) what day is this file for (so we can exclude it if it is in the past)

							// harris unixtime is "local unixtime" no DST calc.
										ulDate = util.DateFromName(pszPath);
										if(ulDate != TIME_NOT_DEFINED)
										{
											// we have a date, it was in the lookahead.
											// create a recheck entry to the change stack.

											CDirEntry* pde = new CDirEntry;

											if(pde)
											{
												pde->m_szFullPath =      (char*)malloc(strlen(g_dir.m_ppDirCanon[x]->m_szFullPath)+1);     // no filename
												if(pde->m_szFullPath)    strcpy(pde->m_szFullPath, g_dir.m_ppDirCanon[x]->m_szFullPath);
												pde->m_szFilename =      (char*)malloc(strlen(g_dir.m_ppDirCanon[x]->m_szFilename)+1);     // long filename
												if(pde->m_szFilename)    strcpy(pde->m_szFilename, g_dir.m_ppDirCanon[x]->m_szFilename);
												pde->m_szDOSFilename =   (char*)malloc(strlen(g_dir.m_ppDirCanon[x]->m_szDOSFilename)+1);  //8.3 format
												if(pde->m_szDOSFilename) strcpy(pde->m_szDOSFilename, g_dir.m_ppDirCanon[x]->m_szDOSFilename);

												// these dont matter (ACTUALLY, THEY DO!!!!)
												pde->m_ulEntryTimestamp = g_dir.m_ppDirCanon[x]->m_ulEntryTimestamp;			// unixtime, seconds resolution, last modified
												pde->m_ftFileModified   = g_dir.m_ppDirCanon[x]->m_ftFileModified;			  // FILETIME
												pde->m_ulFileSizeHigh   = g_dir.m_ppDirCanon[x]->m_ulFileSizeHigh;				// filesize		DWORD    nFileSizeHigh; 
												pde->m_ulFileSizeLow    = g_dir.m_ppDirCanon[x]->m_ulFileSizeLow;				// filesize		DWORD    nFileSizeLow;
												pde->m_ulFileAttribs    = g_dir.m_ppDirCanon[x]->m_ulFileAttribs;		    // attribs returned by GetFileAttributes
												
												pde->m_ulFlags          = WATCH_RECHK;					    // type

												g_dir.AddChange(&pde, false); // this adds to the changelog but not the cannon because WATCH_RECHK is not defined inside the thing.

												if(pde)
												{
													try{delete pde;} catch(...){}// must cleanup.
												}
											}
										}
										
									}
								}


							
								x++;
							}

						}
						LeaveCriticalSection(&g_dir.m_critDirCanon);

						nLastTime = g_psentinel->m_data.m_timeTrafficWatch.time;
					}
				}
				g_psentinel->m_data.m_bTrafficWatchInitialized = true;
			}  // we are watching
		}  // watchfolder process  and not suspended
		else // could be suspended.  have to end watch.
		{
			if(g_dir.m_bWatchFolder)
			{
				g_dir.EndWatch();

				bool bBreak=false;
				while(!bBreak)
				{
					EnterCriticalSection(&g_dir.m_critChangeThreads);
					if(g_dir.m_ulChangeThreads<=0)
					{
						bBreak=true;
					}
					else
					{
						Sleep(10);
					}
					LeaveCriticalSection(&g_dir.m_critChangeThreads);
				}

				g_psentinel->m_data.m_bTrafficWatchInitialized = false;
			}
		}


//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "end loop");  //(Dispatch message)
//Sleep(200);

		Sleep(5);  //just to not peg processor if nothing is happening.

	}

// have to end it no matter what
	if(g_dir.m_bWatchFolder)
	{
		g_dir.EndWatch();

		bool bBreak=false;
		while(!bBreak)
		{
			EnterCriticalSection(&g_dir.m_critChangeThreads);
			if(g_dir.m_ulChangeThreads<=0)
			{
				bBreak=true;
			}
			else
			{
				Sleep(10);
			}
			LeaveCriticalSection(&g_dir.m_critChangeThreads);
		}

		g_psentinel->m_data.m_bTrafficWatchInitialized = false;
	}

	db.RemoveConnection(pdbConn);

	g_psentinel->m_data.m_bTrafficWatchStarted=false;
	_endthread();
//	Sleep(50);

}


void SentinelWorkingSetMaintainThread(void* pvArgs)
{
	if((g_psentinel)&&(g_psentinel->m_data.m_bWorkingSetThreadStarted)) return; // allow only one working set thread.
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

	g_psentinel->m_data.m_bWorkingSetThreadSuppress = true;

	CDBUtil db;
	CDBconn* pdbConn = db.CreateNewConnection(g_psentinel->m_settings.m_pszDSN, g_psentinel->m_settings.m_pszUser, g_psentinel->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			g_psentinel->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Sentinel:working_set_database_connect", errorstring);  //(Dispatch message)
		}
		else
		{
			g_psentinel->m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:working_set_database_connect", "database connected");  //(Dispatch message)
		}
	}
//	else	?

	EnterCriticalSection(&g_psentinel->m_data.m_critWorkingSet);
	if(g_psentinel->m_data.m_ppWorkingSet)
	{
		int i=0;
		while(i<g_psentinel->m_data.m_nNumSetIDs)
		{
			if(g_psentinel->m_data.m_ppWorkingSet[i])
			{
				if(g_psentinel->m_data.m_ppWorkingSet[i]->pszKey) free(g_psentinel->m_data.m_ppWorkingSet[i]->pszKey);
				try{delete g_psentinel->m_data.m_ppWorkingSet[i];} // delete objects, must use new to allocate
				catch(...){}
			}
			i++;
		}
		try{delete [] g_psentinel->m_data.m_ppWorkingSet;} // delete array of pointers to objects, must use new to allocate
		catch(...){}
	}
	if(g_psentinel->m_data.m_ppWorkingSetChanges)
	{
		int i=0;
		while(i<g_psentinel->m_data.m_nNumSetChanges)
		{
			if(g_psentinel->m_data.m_ppWorkingSetChanges[i])
			{
				if(g_psentinel->m_data.m_ppWorkingSetChanges[i]->pszKey) free(g_psentinel->m_data.m_ppWorkingSetChanges[i]->pszKey);
				try{delete g_psentinel->m_data.m_ppWorkingSetChanges[i];} // delete objects, must use new to allocate
				catch(...){}
			}
			i++;
		}
		try{delete [] g_psentinel->m_data.m_ppWorkingSetChanges;} // delete array of pointers to objects, must use new to allocate
		catch(...){}
	}

	g_psentinel->m_data.m_ppWorkingSetChanges=NULL;
	g_psentinel->m_data.m_nNumSetChanges=0;

	g_psentinel->m_data.m_ppWorkingSet=NULL;
	g_psentinel->m_data.m_nNumSetIDs=0;

	LeaveCriticalSection(&g_psentinel->m_data.m_critWorkingSet);

	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
													g_psentinel->m_settings.m_pszWorkingSet  // table name
												);


if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_WORKINGSET)
{
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set init SQL: %s", szSQL);
}

	if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
	{
			//**MSG
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set init SQL exception: %s", errorstring);
	}

	g_psentinel->m_data.m_bWorkingSetThreadStarted = true;
	g_psentinel->m_data.m_bWorkingSetThreadSuppress = false;

// if working set is being shut down, no need to keep processing them, just kill it as fast as we can, proceed to table clear
	while((!g_bKillThread)&&(g_psentinel->m_settings.m_bUseWorkingSet)) // exit when killed, but only after all have been managed. NO NEED
//	while(((!g_bKillThread)||(g_psentinel->m_data.m_nNumSetIDs>0))&&(g_psentinel->m_settings.m_bUseWorkingSet)) // exit when killed, but only after all have been managed.
	{
		bool bDone = false;
		EnterCriticalSection(&g_psentinel->m_data.m_critWorkingSet);

		workingset* pItem = NULL;

		if((g_psentinel->m_data.m_ppWorkingSetChanges)&&(g_psentinel->m_data.m_nNumSetChanges))
		{
			pItem = g_psentinel->m_data.m_ppWorkingSetChanges[0];

			int i=0;
			g_psentinel->m_data.m_nNumSetChanges--;

//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_WORKINGSET)
//{
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set queue decremented, changes now: %d", g_psentinel->m_data.m_nNumSetChanges);
//}

			while((i<g_psentinel->m_data.m_nNumSetChanges)&&(!g_bKillThread))
			{
				g_psentinel->m_data.m_ppWorkingSetChanges[i] = g_psentinel->m_data.m_ppWorkingSetChanges[i+1];
				i++;
			}
			g_psentinel->m_data.m_ppWorkingSetChanges[i]=NULL;
		}
		else bDone = true;

		if((pItem)&&(pItem->pszKey)&&(strlen(pItem->pszKey)>0)&&(!g_bKillThread))
		{ 
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_WORKINGSET)
//{
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set search: %s", pItem->pszKey);
//}

			int i=0, r=-1;
			if(pItem->bAdd)
			{
				if(g_psentinel->m_data.m_ppWorkingSet)
				{
					while((i<g_psentinel->m_data.m_nNumSetIDs)&&(!g_bKillThread))
					{
						if(
								(g_psentinel->m_data.m_ppWorkingSet[i])
							&&(g_psentinel->m_data.m_ppWorkingSet[i]->pszKey)
							)
						{

							if(strcmp(g_psentinel->m_data.m_ppWorkingSet[i]->pszKey, pItem->pszKey)==0)
							{
								r=i;
								g_psentinel->m_data.m_ppWorkingSet[i]->nRefCount++;
								free(pItem->pszKey);
								try{delete pItem;} catch(...){}
								break;
							}
						}
						
						i++;
					}
				}
				if(r<0) // not found have to add.
				{
					workingset** ppItems = new workingset*[g_psentinel->m_data.m_nNumSetIDs+1];
					if(ppItems)
					{
						i=0;
						while(i<g_psentinel->m_data.m_nNumSetIDs)
						{
							if(g_psentinel->m_data.m_ppWorkingSet)
							{
								ppItems[i] = g_psentinel->m_data.m_ppWorkingSet[i];
							}
							i++;
						}
						ppItems[i] = pItem;
						ppItems[i]->nRefCount=1;
						workingset** ppItemsTemp = g_psentinel->m_data.m_ppWorkingSet;
						g_psentinel->m_data.m_ppWorkingSet = ppItems;
						g_psentinel->m_data.m_nNumSetIDs++;
						try{delete [] ppItemsTemp;} catch(...){}

						// do database add
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
							"INSERT INTO %s SELECT getDate(), %s.* FROM %s WHERE key_field = '%s'",
															g_psentinel->m_settings.m_pszWorkingSet, // table name
															g_psentinel->m_settings.m_pszClipKeyMetadataView, // view name
															g_psentinel->m_settings.m_pszClipKeyMetadataView, // view name
															pItem->pszKey
														);
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_WORKINGSET)
{
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set insert SQL: %s", szSQL);
}

						if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
								//**MSG
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set insert SQL exception: %s", errorstring);
						}

					}
					else
					{ // error, just have to ignore.
						free(pItem->pszKey);
						try{delete pItem;} catch(...){}
					}
				}
			}
			else  //delete
			{
				if(g_psentinel->m_data.m_ppWorkingSet)
				{
					while(i<g_psentinel->m_data.m_nNumSetIDs)
					{
						if(
								(g_psentinel->m_data.m_ppWorkingSet[i])
							&&(g_psentinel->m_data.m_ppWorkingSet[i]->pszKey)
							)
						{
							if(strcmp(g_psentinel->m_data.m_ppWorkingSet[i]->pszKey, pItem->pszKey)==0)
							{
								r=i;
								g_psentinel->m_data.m_ppWorkingSet[i]->nRefCount--;
								free(pItem->pszKey);
								try{delete pItem;} catch(...){}
								if(g_psentinel->m_data.m_ppWorkingSet[i]->nRefCount<=0)
								{
									// do database delete
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
										"DELETE FROM %s WHERE key_field = '%s'",
																		g_psentinel->m_settings.m_pszWorkingSet, // table name
																		g_psentinel->m_data.m_ppWorkingSet[i]->pszKey
																	);
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_WORKINGSET)
{
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set delete SQL: %s", szSQL);
}

									if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
									{
											//**MSG
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set delete SQL exception: %s", errorstring);
									}

									try{free(g_psentinel->m_data.m_ppWorkingSet[i]->pszKey);} catch(...){}
									try{delete g_psentinel->m_data.m_ppWorkingSet[i];} catch(...){}
									g_psentinel->m_data.m_nNumSetIDs--;
									while(i<g_psentinel->m_data.m_nNumSetIDs)
									{
										g_psentinel->m_data.m_ppWorkingSet[i] = g_psentinel->m_data.m_ppWorkingSet[i+1];
										i++;
									}
									g_psentinel->m_data.m_ppWorkingSet[i] = NULL;
								}
								break;
							}
						}
						
						i++;
					}
				}

			}
		}

		LeaveCriticalSection(&g_psentinel->m_data.m_critWorkingSet);
		if(bDone) Sleep(10); // else get right back and process
	}

	EnterCriticalSection(&g_psentinel->m_data.m_critWorkingSet);
	g_psentinel->m_data.m_bWorkingSetThreadSuppress = true;
	LeaveCriticalSection(&g_psentinel->m_data.m_critWorkingSet);

	// exiting, remove working set from db.
	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
													g_psentinel->m_settings.m_pszWorkingSet  // table name
												);


if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_WORKINGSET)
{
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set de-init SQL: %s", szSQL);
}

	if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
	{
			//**MSG
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set de-init SQL exception: %s", errorstring);
	}
	db.RemoveConnection(pdbConn);

	// clear mem
	EnterCriticalSection(&g_psentinel->m_data.m_critWorkingSet);
	if(g_psentinel->m_data.m_ppWorkingSet)
	{
		int i=0;
		while(i<g_psentinel->m_data.m_nNumSetIDs)
		{
			if(g_psentinel->m_data.m_ppWorkingSet[i])
			{
				if(g_psentinel->m_data.m_ppWorkingSet[i]->pszKey) free(g_psentinel->m_data.m_ppWorkingSet[i]->pszKey);
				try{delete g_psentinel->m_data.m_ppWorkingSet[i];} // delete objects, must use new to allocate
				catch(...){}
			}
			i++;
		}
		try{delete [] g_psentinel->m_data.m_ppWorkingSet;} // delete array of pointers to objects, must use new to allocate
		catch(...){}
	}
	if(g_psentinel->m_data.m_ppWorkingSetChanges)
	{
		int i=0;
		while(i<g_psentinel->m_data.m_nNumSetChanges)
		{
			if(g_psentinel->m_data.m_ppWorkingSetChanges[i])
			{
				if(g_psentinel->m_data.m_ppWorkingSetChanges[i]->pszKey) free(g_psentinel->m_data.m_ppWorkingSetChanges[i]->pszKey);
				try{delete g_psentinel->m_data.m_ppWorkingSetChanges[i];} // delete objects, must use new to allocate
				catch(...){}
			}
			i++;
		}
		try{delete [] g_psentinel->m_data.m_ppWorkingSetChanges;} // delete array of pointers to objects, must use new to allocate
		catch(...){}
	}

	g_psentinel->m_data.m_ppWorkingSetChanges=NULL;
	g_psentinel->m_data.m_nNumSetChanges=0;

	g_psentinel->m_data.m_ppWorkingSet=NULL;
	g_psentinel->m_data.m_nNumSetIDs=0;

	LeaveCriticalSection(&g_psentinel->m_data.m_critWorkingSet);
	g_psentinel->m_data.m_bWorkingSetThreadStarted = false;
}


void SentinelWorkingSetAdjustThread(void* pvArgs)
{
	workingset* pItem = (workingset*)pvArgs;
	if((pItem)&&(pItem->pszKey)&&(strlen(pItem->pszKey)>0)&&(!g_bKillThread))
	{ 
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_WORKINGSET)
//{
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set NEW: [%s]%s", pItem->pszKey, pItem->bAdd?"+":"-" );
//}
		EnterCriticalSection(&g_psentinel->m_data.m_critWorkingSet);
		if(!g_psentinel->m_data.m_bWorkingSetThreadSuppress)
		{
			workingset** ppItems = NULL;
			if(g_psentinel->m_data.m_nNumSetChanges>=g_psentinel->m_data.m_nNumSetChangesArray)
			{
				ppItems = new workingset*[g_psentinel->m_data.m_nNumSetChangesArray+SENTINEL_CHANGELIST_ARRAYSIZE_INC];
				if(ppItems)
				{
					g_psentinel->m_data.m_nNumSetChangesArray += SENTINEL_CHANGELIST_ARRAYSIZE_INC;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_WORKINGSET)
{
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set change queue array expansion to %d", g_psentinel->m_data.m_nNumSetChangesArray);
}

					if((g_psentinel->m_data.m_ppWorkingSetChanges)&&(g_psentinel->m_data.m_nNumSetChanges>0))
					{
						memcpy(ppItems, g_psentinel->m_data.m_ppWorkingSetChanges, g_psentinel->m_data.m_nNumSetChanges*sizeof(workingset*));
					}
					memset(ppItems+g_psentinel->m_data.m_nNumSetChanges, 0, SENTINEL_CHANGELIST_ARRAYSIZE_INC*sizeof(workingset*));
					if(g_psentinel->m_data.m_ppWorkingSetChanges){ try{delete [] g_psentinel->m_data.m_ppWorkingSetChanges;} catch(...){} }
					g_psentinel->m_data.m_ppWorkingSetChanges = ppItems;					
				}
			}

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_WORKINGSET)
{
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set change queue addition [%s]%s, changes now %d in %d (0x%08x)", pItem->pszKey, pItem->bAdd?"+":"-", g_psentinel->m_data.m_nNumSetChanges, g_psentinel->m_data.m_nNumSetChangesArray, g_psentinel->m_data.m_ppWorkingSetChanges);
}
			if((g_psentinel->m_data.m_nNumSetChanges<g_psentinel->m_data.m_nNumSetChangesArray)&&(g_psentinel->m_data.m_ppWorkingSetChanges))
			{
				g_psentinel->m_data.m_ppWorkingSetChanges[g_psentinel->m_data.m_nNumSetChanges] = pItem;
				g_psentinel->m_data.m_nNumSetChanges++;

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_WORKINGSET)
{
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set change queue addition [%s]%s, changes now %d", pItem->pszKey, pItem->bAdd?"+":"-", g_psentinel->m_data.m_nNumSetChanges);
/*
	int x=0;
	while(x<g_psentinel->m_data.m_nNumSetChanges)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Working set change queue: %d [%s]%s", x, g_psentinel->m_data.m_ppWorkingSetChanges[x]->pszKey, g_psentinel->m_data.m_ppWorkingSetChanges[x]->bAdd?"+":"-");
		x++;
	}
*/
}

			}
			else
			{ // error, just have to ignore.
				free(pItem->pszKey);
				try{delete pItem;} catch(...){}
			}

// changed to the above from the below, needed to make it faster, easier if you aren't allocating and copying an array every single time.

/*
			if(ppItems)
			{






				int i=0;
				while(i<g_psentinel->m_data.m_nNumSetChanges)
				{
					if(g_psentinel->m_data.m_ppWorkingSetChanges)
					{
						ppItems[i] = g_psentinel->m_data.m_ppWorkingSetChanges[i];
					}
					i++;
				}
				ppItems[i] = pItem;
				workingset** ppItemsTemp = g_psentinel->m_data.m_ppWorkingSetChanges;
				g_psentinel->m_data.m_ppWorkingSetChanges = ppItems;
				g_psentinel->m_data.m_nNumSetChanges++;
				try{delete [] ppItemsTemp;}catch(...){}
			}
			else
			{ // error, just have to ignore.
				free(pItem->pszKey);
				try{delete pItem;}catch(...){}
			}
*/
		}

		LeaveCriticalSection(&g_psentinel->m_data.m_critWorkingSet);
	}
	else  // something was wrong, like blank id, may have to free the object
	{
		if(pItem)
		{
			if(pItem->pszKey)
			{
				free(pItem->pszKey);
			}
			try{delete pItem;} catch(...){}
		}

	}
}


void SentinelConnectionThread(void* pvArgs)
{
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "SentinelConnectionThread");  Sleep(250); //(Dispatch message)
	CSentinelConnectionObject* pConn = (CSentinelConnectionObject*) pvArgs;
	if(pConn==NULL) 
	{
		//**MSG
		return;
	}
	if(pConn->m_bKillConnThread) return;

	if( pConn->m_bConnThreadStarted ) return; // already there.

	pConn->m_bConnThreadStarted = true;

	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:connection", "Server management thread for %s (%s) has begun.",
											pConn->m_pszServerName, //server_name
											pConn->m_pszDesc
		); // Sleep(100); //(Dispatch message)

	unsigned long ulTick = GetTickCount();
	unsigned long ulElapsedTick = GetTickCount();

	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];

///////////////////////////////////////////////////////////////////////////
// database problems with sharing this connection:
	CDBUtil* pdb = g_psentinel->m_data.m_pdb;
	CDBconn* pdbConn = g_psentinel->m_data.m_pdbConn;
// so, changed to independent objects
/////////////////////////////////////////////////////

	
///////////////////////////////////////////////////////////////////////////
// database problems with multiple connections!

	CDBUtil db;
//	CDBUtil* pdb = &db;

//	db.InitializeMessaging(&g_psentinel->m_msgr);

	if(g_psentinel->m_settings.m_bMultiConnectSQL)
	{
		pdb = &db;

		pdbConn = db.CreateNewConnection(g_psentinel->m_settings.m_pszDSN, g_psentinel->m_settings.m_pszUser, g_psentinel->m_settings.m_pszPW);
		if(pdbConn)
		{
			if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "Error connecting to database for %s (%s): %s",	pConn->m_pszServerName, //server_name
					pConn->m_pszDesc, errorstring);
				g_psentinel->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Sentinel:connection_database_connect", szSQL);  //(Dispatch message)
				pdbConn = g_psentinel->m_data.m_pdbConn;
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s for %s (%s)", 
				g_psentinel->m_settings.m_pszDSN, g_psentinel->m_settings.m_pszUser, g_psentinel->m_settings.m_pszPW,
				pConn->m_pszServerName, //server_name
				pConn->m_pszDesc
				); 
			g_psentinel->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Sentinel:connection_database_connect", errorstring);  //(Dispatch message)
			pdbConn = g_psentinel->m_data.m_pdbConn;

			//**MSG
		}
	}
// so, changed back to single critsec conn
/////////////////////////////////////////////////////

	CSentinelUtil util;
	CBufferUtil bu;

	unsigned long ulConnTimeoutTime = 0;
	unsigned long ulConnectionRetries = 0;
	int nStatus=0;
	_timeb timestamp;

	systemstatus	harrisSysData; 
	systemstatus	harrisLastSysData; 
	TConnectionStatus   harrisStatus = (TConnectionStatus)0xff;
	TConnectionStatus   harrisLastStatus = (TConnectionStatus)0xff;


  harrisSysData.systemlistcount = 0xff;
  harrisSysData.systemresetting = 0xff;
  harrisSysData.refframe				= 0xff;
  harrisSysData.refsec					= 0xff;
  harrisSysData.refmin					= 0xff;
  harrisSysData.refhour					= 0xff;
  harrisSysData.systemdf				=	1;
  harrisSysData.systemfrx				= 0xff;
  harrisSysData.syschanged			= 0xff;

	memcpy(&harrisLastSysData, &harrisSysData, sizeof(systemstatus));

	// following not used
  //      etclocks systemtc;
  //      etclocks currenttc;
  //      Boolean inputdf;
  //      Word systemdate;
  //      Byte error_flag;
  //      apiinc::ERROR_PACKET error_report;
  //      etdevices devicesavailable[68];


	bool bStatusChanged = false;

//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "SentinelConnectionThread2");  Sleep(250); //(Dispatch message)
	while(
			    (!pConn->m_bKillConnThread)
				&&(!g_bKillThread)  // added this.  was asserting on disconnect because still trying
				&&(ulConnectionRetries<g_psentinel->m_settings.m_ulConnectionRetries)
//				&&(!g_psentinel->m_data.m_bProcessSuspended)
				&&(g_psentinel)
				&&(g_psentinel->m_data.m_key.m_bValid)  // must have a valid license
				&&(
						(!g_psentinel->m_data.m_key.m_bExpires)
					||((g_psentinel->m_data.m_key.m_bExpires)&&(!g_psentinel->m_data.m_key.m_bExpired))
					||((g_psentinel->m_data.m_key.m_bExpires)&&(g_psentinel->m_data.m_key.m_bExpireForgiveness)&&(g_psentinel->m_data.m_key.m_ulExpiryDate+g_psentinel->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
						(!g_psentinel->m_data.m_key.m_bMachineSpecific)
					||((g_psentinel->m_data.m_key.m_bMachineSpecific)&&(g_psentinel->m_data.m_key.m_bValidMAC))
					)
				)
	{
		bStatusChanged = false;
		if(
				(harrisLastSysData.systemdf != harrisSysData.systemdf)
			||(harrisLastSysData.systemfrx != harrisSysData.systemfrx)
			||(harrisLastSysData.syschanged != harrisSysData.syschanged)
			||((harrisLastStatus&0xff)  != (harrisStatus&0xff))
			)
		{
				bStatusChanged = true;
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:conn_change", "connection changed: change %d -> %d, status %d -> %d (netConnected=%d)", 
		harrisLastSysData.syschanged, harrisSysData.syschanged, 
		harrisLastStatus&0xff, harrisStatus&0xff, netConnected); //  Sleep(250);//(Dispatch message)
				try{ memcpy(&harrisLastSysData, &harrisSysData, sizeof(systemstatus));}catch(...){}
				harrisLastStatus  = harrisStatus;
		}

		if((g_psentinel->m_settings.m_bUseControlModules)&&(g_psentinel->m_data.m_bControlModuleServiceThreadStarted)&&(bStatusChanged)&&(!g_bKillThread)) // not kill thread because service module is already shut down.
		{
			int nChanges=-1;
			if(g_psentinel->m_settings.m_nCtrlModMaxQueue>0) 
			{
				EnterCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
				nChanges = g_psentinel->m_data.m_nNumChanges;
				LeaveCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
				if(nChanges>=g_psentinel->m_settings.m_nCtrlModMaxQueue)
				{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "discarding connection change, queue is full (%d items)", nChanges); //  Sleep(250);//(Dispatch message)

					nChanges=1;
				}
				else nChanges = -1;
			}
			if(nChanges<0)
			{

#pragma message(alert "here, send a change to send xml to control modules for connection changes")
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "creating connection change for %s", pConn->m_pszServerName); //  Sleep(250);//(Dispatch message)

				CSentinelChange* pNewChange =  new CSentinelChange;
				if(pNewChange)
				{
					pNewChange->m_pObj = (void*) new CSentinelConnectionObject;
					if(pNewChange->m_pObj)
					{
						CSentinelConnectionObject* pChgConn = ((CSentinelConnectionObject*)(pNewChange->m_pObj));

						*pChgConn = *pConn;
						pChgConn->m_pszClientName = NULL;
						pChgConn->m_pszServerName = NULL;
						pChgConn->m_pszDesc = NULL;
						pNewChange->m_nType = DLLCMD_ACONN;

						pNewChange->m_pszServerName =  bu.XMLEncode(pConn->m_pszServerName);
						pNewChange->m_pszClientName = bu.XMLEncode(pConn->m_pszClientName);
						pNewChange->m_pszDesc = bu.XMLEncode(pConn->m_pszDesc);

						pNewChange->m_status = harrisLastStatus;
						memcpy(&(pNewChange->m_sys), &(harrisLastSysData), sizeof(systemstatus));

						pChgConn->m_bConnThreadStarted = false; // so it will not stall in deconstructor
						if(
								(!g_bKillThread) // not kill thread because service module is already shut down.
							&&(_beginthread(SentinelPublishChangeThread, 0, (void*)(pNewChange))==-1)
							)
						{
							//error.

							//**MSG
							g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start publish change thread."); //(Dispatch message)

							try{ delete (pNewChange); } catch(...){}
						}

						
					}
					else
					{
						try{ delete (pNewChange); } catch(...){}
					}
				}
			}
				
		}

	EnterCriticalSection(&pConn->m_critAPI);
		if(	pConn->m_pAPIConn )
		{
	LeaveCriticalSection(&pConn->m_critAPI);
			if((g_psentinel->m_data.m_bProcessSuspended)&&(g_psentinel->m_settings.m_bDisconnectOnSuspend))
			{ // disconnect
				pConn->m_ulStatus &= ~SENTINEL_ICON_MASK;
				pConn->m_ulStatus |= SENTINEL_STATUS_NOTCON;
				pConn->m_bKillConnThread = true;  // let the thread finish out.
	EnterCriticalSection(&pConn->m_critAPI);
				pConn->m_pAPIConn = NULL;
	LeaveCriticalSection(&pConn->m_critAPI);
				continue;
			}
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "getting status");  Sleep(250); //(Dispatch message)
			// we can monitor the connection and get status
	EnterCriticalSection(&pConn->m_critAPI);
			EnterCriticalSection(&g_adc.m_crit);
			try{ nStatus = g_adc.GetStatusData(pConn->m_pAPIConn);} catch(...){}
			try{ memcpy(&harrisSysData, &(pConn->m_pAPIConn->m_SysData), sizeof(systemstatus));} catch(...){}
			try{ harrisStatus = pConn->m_pAPIConn->m_Status; } catch(...){}
			LeaveCriticalSection(&g_adc.m_crit);
	LeaveCriticalSection(&pConn->m_critAPI);
			ulTick = GetTickCount();

			if(nStatus<ADC_SUCCESS)
			{
				if(ulConnTimeoutTime==0)
				{
					ulConnTimeoutTime = clock()+g_psentinel->m_settings.m_ulConnTimeout*1000;
				}
				else
				{
					if(ulConnTimeoutTime<(unsigned long)clock())
					{
	EnterCriticalSection(&pConn->m_critAPI);
						pConn->m_pAPIConn->m_Status = netDisconnect; // force it to break out and re-connect.
	LeaveCriticalSection(&pConn->m_critAPI);
						harrisStatus = netDisconnect;
					}
				}
			}
			else
			{
				ulConnTimeoutTime = 0;
			}


if((nStatus<ADC_SUCCESS)&&(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONNECTSTATUS))
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:connection", "GetStatusData failed on [%s]", pConn->m_pszServerName);  //Sleep(250); //(Dispatch message)

	EnterCriticalSection(&pConn->m_critAPI);
			if((harrisStatus&0xff) == netConnected) // connected
			{
//#pragma message(alert "remove this file business in the try catch")

//				try
//				{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONNECTSTATUS)
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:connection", "pConn->m_pAPIConn->m_Status is %d", ((harrisStatus)&0xff));  //Sleep(250); //(Dispatch message)
/*
			}
				catch(...)
				{
					FILE* fp;
					fp=fopen("exceptions.log", "ab");
					if(fp)
					{
						_timeb q;
						_ftime(&q);
						try
						{
							fprintf( fp, "%d.%3d caught exception %d", q.time, q.millitm,(unsigned char)harrisStatus);
						}
						catch(...)
						{
							fprintf( fp, "%d.%3d caught exception", q.time, q.millitm);
						}
						fclose(fp);
					}
				}
*/
	LeaveCriticalSection(&pConn->m_critAPI);

				// put the server time in the exchange table
				if((pdb)&&(pdbConn)&&((pConn->m_dblServerTimeIntervalMS>0.0)||(bStatusChanged))&&(nStatus>=ADC_SUCCESS))
				{


					// only do the following on the interval
//					pConn->m_pAPIConn->m_usRefJulianDate; // lets calc the unix time to give.
	EnterCriticalSection(&pConn->m_critAPI);
					double dblServerTime = pConn->m_dblLastServerTimeMS;
					if(pConn->m_pAPIConn)
					{
						dblServerTime = ((double)(pConn->m_pAPIConn->m_usRefJulianDate - 25567))*86400000.0   // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
									+ (double)pConn->m_pAPIConn->m_ulRefTimeMS;
					}
	LeaveCriticalSection(&pConn->m_critAPI);

					if((dblServerTime>(pConn->m_dblLastServerTimeMS+pConn->m_dblServerTimeIntervalMS))||(bStatusChanged))
					{
						pConn->m_dblLastServerTimeMS = dblServerTime;

						_ftime( &timestamp );
						pConn->m_dblUpdateTime = ((double)(timestamp.time)) + ((double)(timestamp.millitm))/1000.0;

						if(g_psentinel->m_settings.m_bStatusInTables)
						{
// new:
//create table Channels (ID int NOT NULL identity(1,1), flags int NOT NULL, description varchar(256), server varchar(32), listid int, list_state int, list_changed int, list_display int, list_syschange int, list_count int, list_lookahead int, list_last_update float);
//create table Connections (server varchar(32) NOT NULL, description varchar(256), flags int, client varchar(32), server_time float, server_status int, server_lists int, server_basis int, server_changed int, server_last_update float);
							
// the %x is a problem below if the hex value is gabbage, and comes up with letters.

//	EnterCriticalSection(&pConn->m_critAPI);
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
server_time = %.0f, \
server_status = %d, \
server_lists = %d, \
server_basis = %d, \
server_changed = %d, \
server_last_update = %d%03d WHERE \
server = '%s'",
								g_psentinel->m_settings.m_pszConnections?g_psentinel->m_settings.m_pszConnections:"Connections",
								dblServerTime,  // milliseconds since jan 1970
								(harrisStatus&0xff),
 								(harrisSysData.systemlistcount&0xff),
								((harrisSysData.systemfrx==0x29)||(harrisSysData.systemfrx==0x24))?((harrisSysData.systemfrx&0x0f)+(((harrisSysData.systemfrx&0xf0)>>4)*10)):-1,  // avoid invalid values
								(harrisSysData.syschanged&0xff),

								(unsigned long)(timestamp.time), //unixtime// - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
								timestamp.millitm,

								pConn->m_pszServerName
								);
//	LeaveCriticalSection(&pConn->m_critAPI);

			EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", szSQL);  Sleep(50); //(Dispatch message)
							if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
							{
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", errorstring); // Sleep(50); //(Dispatch message)
								// error.
							}
			LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

						}

						if(g_psentinel->m_settings.m_bListStatusInExchange)
						{

//	EnterCriticalSection(&pConn->m_critAPI);
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET flag = '%s|%d%03d|%.0f', mod = %d WHERE \
criterion = 'Server_Time' AND flag LIKE '%s%%'",
								g_psentinel->m_settings.m_pszExchange?g_psentinel->m_settings.m_pszExchange:"Exchange",
								pConn->m_pszServerName,		
								(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
								timestamp.millitm,
								dblServerTime,
								util.ConvertHMSFToMilliseconds(
									harrisSysData.refhour, 
									harrisSysData.refmin, 
									harrisSysData.refsec, 
									harrisSysData.refframe, 
									harrisSysData.systemfrx, ADC_NORMAL
									), // pass in the systemfrx from the system data // server time in milliseconds.

								pConn->m_pszServerName
								);

//	LeaveCriticalSection(&pConn->m_critAPI);

		/*  was this: num of millisecs per day.  But, bad because might not be the same day as the local time.
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET flag = '%s|%d%03d', mod = %d WHERE \
		criterion = 'Server_Time' AND flag LIKE '%s%%'",
								g_psentinel->m_settings.m_pszExchange?g_psentinel->m_settings.m_pszExchange:"Exchange",
								pConn->m_pszServerName,		
								(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
								timestamp.millitm,
								g_adc.ConvertHMSFToMilliseconds(
									harrisSysData.refhour, 
									harrisSysData.refmin, 
									harrisSysData.refsec, 
									harrisSysData.refframe, 
									harrisSysData.systemfrx, ADC_NORMAL
									), // pass in the systemfrx from the system data // server time in milliseconds.
								pConn->m_pszServerName
								);
	*/
			EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
							if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
							{
								// error.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", errorstring); // Sleep(50); //(Dispatch message)
							}
			LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
						}


					}
				}


				// here we need to monitor list states and threads
				int nChannels = 0;
EnterCriticalSection(&g_psentinel->m_data.m_critChannels);

				while(
								(nChannels<g_psentinel->m_data.m_nNumChannelObjects)
							&&(!pConn->m_bKillConnThread)&&(nStatus>=ADC_SUCCESS)
							)
				{
					if((g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_ppChannelObj[nChannels]))
					{
						CSentinelChannelObject* pChannel = g_psentinel->m_data.m_ppChannelObj[nChannels];
						if( 
							  (pChannel->m_pszServerName)
							&&(pConn->m_pszServerName)
							&&(strcmp(pChannel->m_pszServerName, pConn->m_pszServerName)==0)
							) // we are on the right adc conn.
						{
							//if the list is active, check to see if there is a thread going.
							if(pChannel->m_ulFlags&SENTINEL_FLAG_ENABLED)
							{
								if(
									  (pChannel->m_nHarrisListID<0)
									||(pChannel->m_nHarrisListID>=MAXSYSLISTS)
									)
								{
									// error!
									pChannel->m_ulFlags &= ~SENTINEL_FLAG_ENABLED;  //disable

								}
								else
								if(( !pChannel->m_bChannelThreadStarted)&&(!pConn->m_bKillConnThread))
								{ // start the thread!

									if(!g_psentinel->m_data.m_bProcessSuspended)
									{
										pChannel->m_bKillChannelThread=false;
	EnterCriticalSection(&pConn->m_critAPI);
										pChannel->m_ppAPIConn = &(pConn->m_pAPIConn);
										pChannel->m_pcritAPI = &pConn->m_critAPI;
	LeaveCriticalSection(&pConn->m_critAPI);
	//									pChannel->m_pbKillConnThread=&(pConn->m_bKillConnThread);

	//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "beginning list thread");  Sleep(250); //(Dispatch message)
										if(_beginthread(SentinelListThread, 0, (void*)pChannel)==-1)
										{
											//error.
								
	//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "problem beginning list thread");  Sleep(250); //(Dispatch message)
											//**MSG
										}
										else
										{
											Sleep(200);
											// while(!pChannel->m_bChannelThreadStarted) Sleep(1);
										}
									}
								}
							}
							else  // disabled
							{
								if( pChannel->m_bChannelThreadStarted)
								{
									// end the thread.
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "ending list thread");  Sleep(250); //(Dispatch message)
									pChannel->m_bKillChannelThread=true;
									Sleep(200); // give it a  rest in between
								}
							}
						}
					}
					nChannels++;
				}
LeaveCriticalSection(&g_psentinel->m_data.m_critChannels);

			}
			else
			{
g_psentinel->m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Sentinel:connection", "Server [%s] has been disconnected!", pConn->m_pszServerName); // Sleep(50); //(Dispatch message)
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONNECTSTATUS)
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:connection", "pConn->m_pAPIConn->m_Status is %d", (unsigned char)harrisStatus);  //Sleep(250); //(Dispatch message)

				ulConnectionRetries++;
	EnterCriticalSection(&g_adc.m_crit);
				g_adc.DisconnectServer(pConn->m_pAPIConn);  // just removes from adc list.
	LeaveCriticalSection(&g_adc.m_crit);
				pConn->m_ulStatus = SENTINEL_STATUS_ERROR;  // due to disconnection.  resets on connect
				pConn->m_pAPIConn = NULL; // break out.
	LeaveCriticalSection(&pConn->m_critAPI);

				harrisSysData.systemlistcount = 0xff;
				harrisSysData.systemresetting = 0xff;
				harrisSysData.refframe				= 0xff;
				harrisSysData.refsec					= 0xff;
				harrisSysData.refmin					= 0xff;
				harrisSysData.refhour					= 0xff;
				harrisSysData.systemdf				= 1;
				harrisSysData.systemfrx				= 0xff;
				harrisSysData.syschanged			= 0xff;

/*
		if((g_psentinel->m_settings.m_bUseControlModules)&&(g_psentinel->m_data.m_bControlModuleServiceThreadStarted)&&(pConn))
		{
#pragma message(alert "here, send a change to send xml to control modules for connection changes")
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "creating connection change on disconnect"); //  Sleep(250);//(Dispatch message)

			CSentinelChange* pNewChange =  new CSentinelChange;
			if(pNewChange)
			{
				pNewChange->m_pObj = (void*) new CSentinelConnectionObject;
				if(pNewChange->m_pObj)
				{
					CSentinelConnectionObject* pChgConn = ((CSentinelConnectionObject*)(pNewChange->m_pObj));

					*pChgConn = *pConn;
					pChgConn->m_pszClientName = NULL;
					pChgConn->m_pszServerName = NULL;
					pChgConn->m_pszDesc = NULL;
					pNewChange->m_nType = DLLCMD_ACONN;

					pNewChange->m_pszServerName =  bu.XMLEncode(pConn->m_pszServerName);
					pNewChange->m_pszClientName = bu.XMLEncode(pConn->m_pszClientName);
					pNewChange->m_pszDesc = bu.XMLEncode(pConn->m_pszDesc);

					pNewChange->m_status = harrisStatus; // not connected status...
					memcpy(&(pNewChange->m_sys), &(harrisSysData), sizeof(systemstatus)); // the status that was just reset

					pChgConn->m_bConnThreadStarted = false; // so it will not stall in deconstructor
					if(_beginthread(SentinelPublishChangeThread, 0, (void*)(pNewChange))==-1)
					{
						//error.

						//**MSG
						g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start publish change thread."); //(Dispatch message)

						try{ delete (pNewChange); } catch(...){}
					}

					
				}
				else
				{
					try{ delete (pNewChange); } catch(...){}
				}
			}
			
		}
*/
				// reset the server time in the exchange table
				if((pdb)&&(pdbConn))
				{
					_ftime( &timestamp );
					pConn->m_dblUpdateTime = ((double)(timestamp.time)) + ((double)(timestamp.millitm))/1000.0;

					if(g_psentinel->m_settings.m_bStatusInTables)
					{
// new:
//create table Channels (ID int NOT NULL identity(1,1), flags int NOT NULL, description varchar(256), server varchar(32), listid int, list_state int, list_changed int, list_display int, list_syschange int, list_count int, list_lookahead int, list_last_update float);
//create table Connections (server varchar(32) NOT NULL, description varchar(256), flags int, client varchar(32), server_time float, server_status int, server_lists int, server_basis int, server_changed int, server_last_update float);
						
					
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
server_time = -1.0, \
server_status = -1, \
server_lists = 0, \
server_basis = -1, \
server_changed = -1, \
server_last_update = %d%03d WHERE \
server = '%s'",
							g_psentinel->m_settings.m_pszConnections?g_psentinel->m_settings.m_pszConnections:"Connections",
//							dblServerTime,  // milliseconds since jan 1970
//							harrisStatus,
//							harrisSysData.systemlistcount,
// 							harrisSysData.systemfrx,
//							harrisSysData.syschanged,

							(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
							timestamp.millitm,

							pConn->m_pszServerName
							);
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", szSQL);  Sleep(50); //(Dispatch message)
		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
						if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", errorstring); // Sleep(50); //(Dispatch message)
							// error.
						}
		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

					}


					if(g_psentinel->m_settings.m_bListStatusInExchange)
					{
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET flag = '%s|%d%03d|-1', mod = %d WHERE criterion = 'Server_Time' AND flag LIKE '%s%%'",
							g_psentinel->m_settings.m_pszExchange?g_psentinel->m_settings.m_pszExchange:"Exchange",
							pConn->m_pszServerName,		
							(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
							timestamp.millitm,
							-1,
							pConn->m_pszServerName
							);
			EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
						if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
							// error.
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "servertime SQL error: %s", errorstring);  Sleep(20); //(Dispatch message)
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", errorstring); // Sleep(50); //(Dispatch message)
						}

			LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
					}

				}

			}

//	EnterCriticalSection(&pConn->m_critAPI);
			if(/*(pConn->m_pAPIConn)&&*/(!pConn->m_bKillConnThread))
			{
				ulElapsedTick = GetTickCount();
				if(ulElapsedTick<=ulTick) //wrapped or instantaneous
				{
					Sleep(((harrisSysData.systemfrx) == 0x24)?40:33);
				}
				else
				{
					ulElapsedTick -= ulTick;
					if(ulElapsedTick<(unsigned long)(((harrisSysData.systemfrx) == 0x24)?40:33))
					{
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "sleeping on status %d ms", (((harrisSysData.systemfrx) == 0x24)?40:33) - ulElapsedTick);  Sleep(250); //(Dispatch message)
						Sleep((((harrisSysData.systemfrx) == 0x24)?40:33) - ulElapsedTick);
					}  // else we already waited long.
				}
			}
//	LeaveCriticalSection(&pConn->m_critAPI);

			//else break out and try to reconnect.
		}
		else
		{
	LeaveCriticalSection(&pConn->m_critAPI);

			harrisSysData.systemlistcount = 0xff;
			harrisSysData.systemresetting = 0xff;
			harrisSysData.refframe				= 0xff;
			harrisSysData.refsec					= 0xff;
			harrisSysData.refmin					= 0xff;
			harrisSysData.refhour					= 0xff;
			harrisSysData.systemdf				= 1;
			harrisSysData.systemfrx				= 0xff;
			harrisSysData.syschanged			= 0xff;

			if(
					(pConn->m_ulFlags&SENTINEL_FLAG_ENABLED) // active.
				)
			{

				if((g_psentinel->m_data.m_bProcessSuspended)&&(g_psentinel->m_settings.m_bDisconnectOnSuspend))
				{ // disconnect
					pConn->m_ulStatus &= ~SENTINEL_ICON_MASK;
					pConn->m_ulStatus |= SENTINEL_STATUS_NOTCON;
					pConn->m_bKillConnThread = true;  // let the thread finish out. 
	EnterCriticalSection(&pConn->m_critAPI);
					pConn->m_pAPIConn = NULL;
	LeaveCriticalSection(&pConn->m_critAPI);

					continue;
				}

				if((pConn->m_pszClientName)&&(strlen(pConn->m_pszClientName)<=0)) // zero length client name
				{
					try{ free(pConn->m_pszClientName);} catch(...){}
					pConn->m_pszClientName = NULL;
				}
				if(!pConn->m_pszClientName)
				{
					pConn->m_pszClientName = (char*)malloc(strlen("Sentinel")+1);
					if(pConn->m_pszClientName) strcpy(pConn->m_pszClientName, "Sentinel");
				}
					
				char server[32];
				_snprintf(server, 32, "%s", pConn->m_pszServerName);
				char client[32];
				_snprintf(client, 32, "%s", pConn->m_pszClientName);
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONNECT) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:connection", "Waiting %u seconds before connecting to [%s] as [%s], retry %d...", ((ulConnectionRetries>0)?g_psentinel->m_settings.m_ulConnectionInterval:0), server, client, ulConnectionRetries);  //Sleep(250); //(Dispatch message)

				int nClock = clock() + g_psentinel->m_settings.m_ulConnectionInterval*1000;
				while((ulConnectionRetries>0)&&(pConn)&&(!pConn->m_bKillConnThread)&&(nClock>clock()))
				{
					Sleep(10);// wait some number of seconds after a connection attempt;
				}

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONNECT) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:connection", "Connecting to [%s] as [%s], retry %d...", server, client, ulConnectionRetries);  //Sleep(250); //(Dispatch message)

				CAConnection* pAPIConn = NULL;
				EnterCriticalSection(&g_adc.m_crit);
				try
				{
					pAPIConn = g_adc.ConnectServer(server, client);
				}
				catch(...)
				{
					pAPIConn = NULL;
				}
				LeaveCriticalSection(&g_adc.m_crit);


				if(pAPIConn!=NULL)
				{
					ulConnectionRetries = 0;
	EnterCriticalSection(&pConn->m_critAPI);
					pConn->m_pAPIConn = pAPIConn; // connected.

					//reset values
					pConn->m_pAPIConn->m_SysData.refhour  = 0xff; 
					pConn->m_pAPIConn->m_SysData.refmin   = 0xff;
					pConn->m_pAPIConn->m_SysData.refsec   = 0xff;
					pConn->m_pAPIConn->m_SysData.refframe = 0xff;
					memcpy(&harrisSysData, &(pConn->m_pAPIConn->m_SysData), sizeof(systemstatus));
					harrisStatus = pConn->m_pAPIConn->m_Status;
	LeaveCriticalSection(&pConn->m_critAPI);



					pConn->m_ulStatus &= ~SENTINEL_ICON_MASK;
					pConn->m_ulStatus |= SENTINEL_STATUS_CONN;

g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:connection", "Connected to server [%s] as [%s] on connection %d", server, client, pAPIConn); // Sleep(250); //(Dispatch message)
					// success
					// put the connection record in the exchange and connections table
/*
		if((g_psentinel->m_settings.m_bUseControlModules)&&(g_psentinel->m_data.m_bControlModuleServiceThreadStarted)&&(pConn))
		{
#pragma message(alert "here, send a change to send xml to control modules for connection changes")
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "creating connection change on connect"); //  Sleep(250);//(Dispatch message)

			CSentinelChange* pNewChange =  new CSentinelChange;
			if(pNewChange)
			{
				pNewChange->m_pObj = (void*) new CSentinelConnectionObject;
				if(pNewChange->m_pObj)
				{
					CSentinelConnectionObject* pChgConn = ((CSentinelConnectionObject*)(pNewChange->m_pObj));

					*pChgConn = *pConn;
					pChgConn->m_pszClientName = NULL;
					pChgConn->m_pszServerName = NULL;
					pChgConn->m_pszDesc = NULL;
					pNewChange->m_nType = DLLCMD_ACONN;

					pNewChange->m_pszServerName =  bu.XMLEncode(pConn->m_pszServerName);
					pNewChange->m_pszClientName = bu.XMLEncode(pConn->m_pszClientName);
					pNewChange->m_pszDesc = bu.XMLEncode(pConn->m_pszDesc);

					pNewChange->m_status = harrisLastStatus;
					memcpy(&(pNewChange->m_sys), &(harrisLastSysData), sizeof(systemstatus));

					pChgConn->m_bConnThreadStarted = false; // so it will not stall in deconstructor
					if(_beginthread(SentinelPublishChangeThread, 0, (void*)(pNewChange))==-1)
					{
						//error.

						//**MSG
						g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start publish change thread."); //(Dispatch message)

						try{ delete (pNewChange); } catch(...){}
					}

					
				}
				else
				{
					try{ delete (pNewChange); } catch(...){}
				}
			}
			
		}

*/

					if((pdb)&&(pdbConn)&&(pConn)&&(pConn->m_pszServerName))
					{
						_ftime( &timestamp );

						pConn->m_dblUpdateTime = ((double)(timestamp.time)) + ((double)(timestamp.millitm))/1000.0;
						
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
server_time = -1.0, \
server_status = -2, \
server_lists = 0, \
server_basis = -1, \
server_changed = -1, \
server_last_update = %d%03d WHERE \
server = '%s'",
							g_psentinel->m_settings.m_pszConnections?g_psentinel->m_settings.m_pszConnections:"Connections",
//							dblServerTime,  // milliseconds since jan 1970
//							harrisStatus,
//							harrisSysData.systemlistcount,
// 							harrisSysData.systemfrx,
//							harrisSysData.syschanged,

								(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
								timestamp.millitm,
								pConn->m_pszServerName
							);
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", szSQL);  Sleep(50); //(Dispatch message)
		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
						if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", errorstring); // Sleep(50); //(Dispatch message)
							// error.
						}
		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

					
						
						
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (criterion, flag, mod) VALUES ('Server_Time', '%s|%d%03d|-1', -1)",
							g_psentinel->m_settings.m_pszExchange?g_psentinel->m_settings.m_pszExchange:"Exchange",
							pConn->m_pszServerName,
							(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
							timestamp.millitm
							);
		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
						if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
								// error.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", errorstring);//  Sleep(50); //(Dispatch message)
						}

		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
					}

				}
				else
				{
					ulConnectionRetries++;
					//**MSG
	EnterCriticalSection(&pConn->m_critAPI);
					pConn->m_pAPIConn = NULL;
	LeaveCriticalSection(&pConn->m_critAPI);
					pConn->m_ulStatus &= ~SENTINEL_ICON_MASK;
					pConn->m_ulStatus |= SENTINEL_STATUS_ERROR;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONNECT) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:connection", "NULL connecting to [%s] as [%s] ", server, client);  //Sleep(250); //(Dispatch message)
				}
			} // else don't connect, just 
			else
			{ //not enabled channel
	EnterCriticalSection(&pConn->m_critAPI);
				pConn->m_pAPIConn = NULL;
	LeaveCriticalSection(&pConn->m_critAPI);
				pConn->m_ulStatus &= ~SENTINEL_ICON_MASK;
				pConn->m_ulStatus |= SENTINEL_STATUS_NOTCON;
				pConn->m_bKillConnThread = true;  // let the thread finish out.
				Sleep(30);
			}
		}
			
	}


	if((g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_nNumChannelObjects))
	{
		int nThreads=0;
		while(nThreads < g_psentinel->m_data.m_nNumChannelObjects)
		{
			if(
					(g_psentinel->m_data.m_ppChannelObj[nThreads])
				&&(g_psentinel->m_data.m_ppChannelObj[nThreads]->m_pszServerName)
				&&(pConn->m_pszServerName)
				&&(strcmp(g_psentinel->m_data.m_ppChannelObj[nThreads]->m_pszServerName, pConn->m_pszServerName)==0)
				)
			{


/*	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:connection", "Shutting down list management thread for %s:%d (%s).",
											g_psentinel->m_data.m_ppChannelObj[nThreads]->m_pszServerName, //server_name
											g_psentinel->m_data.m_ppChannelObj[nThreads]->m_nHarrisListID,
											g_psentinel->m_data.m_ppChannelObj[nThreads]->m_pszDesc
		);  Sleep(20); //(Dispatch message)
*/
				g_psentinel->m_data.m_ppChannelObj[nThreads]->m_bKillChannelThread = true;			
				if(g_psentinel->m_data.m_ppChannelObj[nThreads]->m_bChannelThreadStarted)
				{
/////////////
					
/*
					g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:connection", "Shutting down list management thread for %s:%d (%s).",
											g_psentinel->m_data.m_ppChannelObj[nThreads]->m_pszServerName, //server_name
											g_psentinel->m_data.m_ppChannelObj[nThreads]->m_nHarrisListID,
											g_psentinel->m_data.m_ppChannelObj[nThreads]->m_pszDesc
		);  //Sleep(50); //(Dispatch message)
//////////////
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:connection", "2 - Shutting down list management thread for %s:%d (%s).   %d\n\n%d %d %d",
											g_psentinel->m_data.m_ppChannelObj[nThreads]->m_pszServerName, //server_name
											g_psentinel->m_data.m_ppChannelObj[nThreads]->m_nHarrisListID,
											g_psentinel->m_data.m_ppChannelObj[nThreads]->m_pszDesc, clock(),
											(g_psentinel->m_data.m_ppChannelObj),(g_psentinel->m_data.m_ppChannelObj[nThreads]),(g_psentinel->m_data.m_ppChannelObj[nThreads]->m_bChannelThreadStarted)
		); // Sleep(50); //(Dispatch message)
*/
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:connection", "entering while");
					while((g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_ppChannelObj[nThreads])&&(g_psentinel->m_data.m_ppChannelObj[nThreads]->m_bChannelThreadStarted))
					{
//						g_psentinel->m_data.m_ppChannelObj[nThreads]->m_bKillChannelThread = true;			
/*	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:connection", "Shutting down list management thread for %s:%d (%s).   %d\n\n%d %d %d",
											g_psentinel->m_data.m_ppChannelObj[nThreads]->m_pszServerName, //server_name
											g_psentinel->m_data.m_ppChannelObj[nThreads]->m_nHarrisListID,
											g_psentinel->m_data.m_ppChannelObj[nThreads]->m_pszDesc, clock(),
											(g_psentinel->m_data.m_ppChannelObj),(g_psentinel->m_data.m_ppChannelObj[nThreads]),(g_psentinel->m_data.m_ppChannelObj[nThreads]->m_bChannelThreadStarted)
		); // Sleep(50); //(Dispatch message)
	*/
						Sleep(10);
					}
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:connection", "leaving while");
				}
			}
			nThreads++;
		}
	}
				


	EnterCriticalSection(&g_adc.m_crit);
	g_adc.DisconnectServer(pConn->m_pszServerName);
	LeaveCriticalSection(&g_adc.m_crit);
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:connection", "Server management thread for %s (%s) is ending.",
											pConn->m_pszServerName, //server_name
											pConn->m_pszDesc
		);  //Sleep(100); //(Dispatch message)

	if((g_psentinel->m_settings.m_bUseControlModules)&&(g_psentinel->m_data.m_bControlModuleServiceThreadStarted)&&(!g_bKillThread)) // not kill thread because service module is already shut down.
	{
		int nChanges=-1;
		if(g_psentinel->m_settings.m_nCtrlModMaxQueue>0) 
		{
			EnterCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
			nChanges = g_psentinel->m_data.m_nNumChanges;
			LeaveCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
			if(nChanges>=g_psentinel->m_settings.m_nCtrlModMaxQueue)
			{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "discarding connection change, queue is full (%d items)", nChanges); //  Sleep(250);//(Dispatch message)

				nChanges=1;
			}
			else nChanges = -1;
		}
		if(nChanges<0)
		{
#pragma message(alert "here, send a change to send xml to control modules for connection changes")
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "creating connection change on thread end"); //  Sleep(250);//(Dispatch message)

			CSentinelChange* pNewChange =  new CSentinelChange;
			if(pNewChange)
			{
				pNewChange->m_pObj = (void*) new CSentinelConnectionObject;
				if(pNewChange->m_pObj)
				{
					CSentinelConnectionObject* pChgConn = ((CSentinelConnectionObject*)(pNewChange->m_pObj));

					*pChgConn = *pConn;
					pChgConn->m_pszClientName = NULL;
					pChgConn->m_pszServerName = NULL;
					pChgConn->m_pszDesc = NULL;
					pNewChange->m_nType = DLLCMD_ACONN;

					pNewChange->m_pszServerName =  bu.XMLEncode(pConn->m_pszServerName);
					pNewChange->m_pszClientName = bu.XMLEncode(pConn->m_pszClientName);
					pNewChange->m_pszDesc = bu.XMLEncode(pConn->m_pszDesc);

					pNewChange->m_status = harrisLastStatus;
					memcpy(&(pNewChange->m_sys), &(harrisLastSysData), sizeof(systemstatus));

					pChgConn->m_bConnThreadStarted = false; // so it will not stall in deconstructor

					if(
							(!g_bKillThread) // not kill thread because service module is already shut down.
						&&(_beginthread(SentinelPublishChangeThread, 0, (void*)(pNewChange))==-1)
						)
					{
						//error.

						//**MSG
						g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start publish change thread."); //(Dispatch message)

						try{ delete (pNewChange); } catch(...){}
					}				
				}
				else
				{
					try{ delete (pNewChange); } catch(...){}
				}

			}
		}
	}


	if((pdb)&&(pdbConn))
	{
		pConn->m_dblUpdateTime = 0.0;
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
server_time = -1.0, \
server_status = -1, \
server_lists = 0, \
server_basis = -1, \
server_changed = -1, \
server_last_update = -1 WHERE \
server = '%s'",
			g_psentinel->m_settings.m_pszConnections?g_psentinel->m_settings.m_pszConnections:"Connections",
//							dblServerTime,  // milliseconds since jan 1970
//							harrisStatus,
//							harrisSysData.systemlistcount,
// 							harrisSysData.systemfrx,
//							harrisSysData.syschanged,

			pConn->m_pszServerName
			);
		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", szSQL);  Sleep(50); //(Dispatch message)
		if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", errorstring); // Sleep(50); //(Dispatch message)
			// error.
		}
		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE criterion = 'Server_Time' AND flag LIKE '%s%%'",
			g_psentinel->m_settings.m_pszExchange?g_psentinel->m_settings.m_pszExchange:"Exchange",
			pConn->m_pszServerName
			);
		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
		if(pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			// error.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", errorstring); // Sleep(50); //(Dispatch message)
		}

		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
	}


	EnterCriticalSection(&pConn->m_critAPI);
	pConn->m_pAPIConn = NULL;
	LeaveCriticalSection(&pConn->m_critAPI);
	pConn->m_bConnThreadStarted = false;
	_endthread();
}


#ifdef SENTINEL_MANAGED_DB // does matching in memory and eases up the db transactions

void SentinelListThread(void* pvArgs)
{
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "SentinelChannelThread");  Sleep(250); //(Dispatch message)
	CSentinelChannelObject* pChannel = (CSentinelChannelObject*) pvArgs;
	if(pChannel==NULL)
	{
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "pChannel NULL, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}
	if((pChannel->m_bKillChannelThread)||(g_bKillThread)) return;
/*
	bool* pbKillConnThread = &(pChannel->m_bKillChannelThread);//pChannel->m_pbKillConnThread;
	if(pbKillConnThread==NULL)  // the address, not what it points to
	{
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "g_psentinel NULL, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}
*/
	if((	pChannel->m_ppAPIConn == NULL)||(*(pChannel->m_ppAPIConn) == NULL)) // need this to do anything.  // could get it by looping through g_adc.... later.
	{
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "m_pAPIConn NULL, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}

	// lets set up some convenience vars;
	int nZeroList = pChannel->m_nHarrisListID-1;


	if((nZeroList<0)||(nZeroList>=MAXSYSLISTS)) // error on list number.
	{
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "nZeroList out of range, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}
	pChannel->m_bChannelThreadStarted = true;
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:list", "List management thread for %s:%d (%s) has begun.",
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
											pChannel->m_pszDesc
		); // Sleep(50); //(Dispatch message)

//bool readytotest = false;

if(pChannel->m_pcritAPI)
{
	EnterCriticalSection(pChannel->m_pcritAPI);
}

	tlistdata* plistdata = NULL;
//	TConnectionStatus* pstatus =  NULL;
//	CAList** ppList = NULL;
	CAList*  pList = NULL;  // volatile, always reassign

	if((pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn)))
	{
		plistdata = &((*(pChannel->m_ppAPIConn))->m_ListData[nZeroList]);
	//	TConnectionStatus* pstatus =  &(pChannel->m_pAPIConn->m_Status);
	//	CAList** ppList = &((*(pChannel->m_ppAPIConn))->m_pList[nZeroList]);
		pList = (*(pChannel->m_ppAPIConn))->m_pList[nZeroList];  // volatile, always reassign
	}


if(pChannel->m_pcritAPI)
{
	LeaveCriticalSection(pChannel->m_pcritAPI);
}
	if(pList) pList->m_nDefaultPackCount = g_psentinel->m_settings.m_nDefaultPackCount;

	char dberrorstring[DB_ERRORSTRING_LEN];
	char szSQL[DB_SQLSTRING_MAXLEN];
	char szSQLTemp[DB_SQLSTRING_MAXLEN];
	char szLastKey[MAX_MESSAGE_LENGTH];
	char szCurrentKey[MAX_MESSAGE_LENGTH];
	CSentinelEventObject  NewEvent;

	CBufferUtil bu;

	strcpy(szLastKey, "");

	_snprintf(pChannel->m_pszSource, MAX_MESSAGE_LENGTH-1, "%s:%s:%d (%s)",	
		g_psentinel->m_settings.m_pszName,
		pChannel->m_pszServerName, //server_name
		pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
		pChannel->m_pszDesc);

//m_bMultiConnectSQL

///////////////////////////////////////////////////////////////////////////
// datbase problems with sharing this connection:
	CDBUtil* pdb = g_psentinel->m_data.m_pdb;
	CDBconn* pdbConn = g_psentinel->m_data.m_pdbConn;
// so, changed to independent objects
/////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
// datbase problems with multiple connections!
	CDBUtil db;
//	CDBUtil* pdb = &db;

	if(g_psentinel->m_settings.m_bMultiConnectSQL)
	{
		pdb = &db;

		pdbConn = db.CreateNewConnection(g_psentinel->m_settings.m_pszDSN, g_psentinel->m_settings.m_pszUser, g_psentinel->m_settings.m_pszPW);
		if(pdbConn)
		{
			if(db.ConnectDatabase(pdbConn, dberrorstring)<DB_SUCCESS)
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "Error connecting to database for %s:%d (%s): %s",												
					pChannel->m_pszServerName, //server_name
					pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
					pChannel->m_pszDesc, 
					dberrorstring);
				g_psentinel->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Sentinel:list_database_connect", szSQL);  //(Dispatch message)
				pdbConn = g_psentinel->m_data.m_pdbConn;
			}
		}
		else
		{
			_snprintf(dberrorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s for %s:%d (%s)", 
				g_psentinel->m_settings.m_pszDSN, g_psentinel->m_settings.m_pszUser, g_psentinel->m_settings.m_pszPW,
					pChannel->m_pszServerName, //server_name
					pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
					pChannel->m_pszDesc
				); 
			g_psentinel->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Sentinel:list_database_connect", dberrorstring);  //(Dispatch message)
			pdbConn = g_psentinel->m_data.m_pdbConn;

			//**MSG
		}

	}
// so, changed back to single critsec conn
/////////////////////////////////////////////////////



if(pChannel->m_pcritAPI)
{
	EnterCriticalSection(pChannel->m_pcritAPI);
}
	if((pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn)))
		(*(pChannel->m_ppAPIConn))->m_bListActive[nZeroList] = true;
if(pChannel->m_pcritAPI)
{
	LeaveCriticalSection(pChannel->m_pcritAPI);
}
//	unsigned long ulTick = GetTickCount();
//	unsigned long ulElapsedTick = GetTickCount();
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "SentinelChannelThread2");  Sleep(250); //(Dispatch message)

	tlistdata listdata;
	tlistdata lastlistdata;  // for comparison
	tlistdata lastliststatusdata;  // for comparison

  lastlistdata.listchanged=0;				// incremented whenever list size changes
  lastlistdata.listdisplay=0;				// incremented whenever an event changes
  lastlistdata.listsyschange=0;			// incremented when list related values are changed. 
  lastlistdata.listcount=0;					// number of events in list 
  lastlistdata.liststate=0;

  lastliststatusdata.listchanged=0;				// incremented whenever list size changes
  lastliststatusdata.listdisplay=0;				// incremented whenever an event changes
  lastliststatusdata.listsyschange=0;			// incremented when list related values are changed. 
  lastliststatusdata.listcount=0;					// number of events in list 
  lastliststatusdata.liststate=0;

	_timeb timestamp;

	// put the connection record in the exchange table
	if((pdb)&&(pdbConn))
	{
		_ftime( &timestamp );
// new:
//create table Channels (ID int NOT NULL identity(1,1), flags int NOT NULL, description varchar(256), server varchar(32), listid int, list_state int, list_changed int, list_display int, list_syschange int, list_count int, list_lookahead int, list_last_update float);
//create table Connections (server varchar(32) NOT NULL, description varchar(256), flags int, client varchar(32), server_time float, server_status int, server_lists int, server_basis int, server_changed int, server_last_update float);
						
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
list_state = -2, \
list_changed = -1, \
list_display = -1, \
list_syschange = -1, \
list_count = -1, \
list_lookahead = -1, \
list_last_update = %d%03d WHERE \
server = '%s' AND listid = %d",
			g_psentinel->m_settings.m_pszChannels?g_psentinel->m_settings.m_pszChannels:"Channels",
//			listdata.liststate,
//			listdata.listchanged,				// incremented whenever list size changes
//			listdata.listdisplay,				// incremented whenever an event changes
//			listdata.listsyschange,			// incremented when list related values are changed. 
//			listdata.listcount,					// number of events in list 
//			listdata.lookahead,					// lookahead value

			(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
			timestamp.millitm,

			pChannel->m_pszServerName,
			pChannel->m_nHarrisListID
			);
if(!g_psentinel->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
		if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
		{
			// error.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
		}
if(!g_psentinel->m_settings.m_bMultiConnectSQL) LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (criterion, flag, mod) VALUES ('List_Status', '%s:%02d|%d%03d|%d|%d|%d|%d', %d)",
			g_psentinel->m_settings.m_pszExchange?g_psentinel->m_settings.m_pszExchange:"Exchange",
			pChannel->m_pszServerName,
			pChannel->m_nHarrisListID,
			(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
			timestamp.millitm,
			lastlistdata.listchanged,				// incremented whenever list size changes
			lastlistdata.listdisplay,				// incremented whenever an event changes
			lastlistdata.listsyschange,			// incremented when list related values are changed. 
			lastlistdata.listcount,					// number of events in list 
			lastlistdata.liststate
			);
if(!g_psentinel->m_settings.m_bMultiConnectSQL) 	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
		if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
		{
				// error.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
		}

if(!g_psentinel->m_settings.m_bMultiConnectSQL) 	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
	}


// delete main list
	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d",
											g_psentinel->m_settings.m_pszLiveEvents,  // table name
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
										);
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

if(!g_psentinel->m_settings.m_bMultiConnectSQL) 	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
	if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
	{
		//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
	}
if(!g_psentinel->m_settings.m_bMultiConnectSQL) 	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);



	bool bconfirmed = true;
	unsigned short usHARRISDONEORPLAY = ((1<<eventdone)|(1<<eventpostrolled)|(1<<eventrunning)|(1<<notplayed)|(1<<eventskipped));
	unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));
	unsigned short usHARRISHARD = (1<<autotimed);
	unsigned short usHARRISPREORPLAY = ((1<<eventrunning)|(1<<eventprerolled));
	bool bChangesPending = false;
	_timeb timebPending;
	_timeb timebChanged;


	while(
					(!pChannel->m_bKillChannelThread)//&&((*pbKillConnThread)!=true))
				&&(!g_bKillThread)
//				&&(!g_psentinel->m_data.m_bProcessSuspended) // suspends event activity by killing event thread.  dont do this, let connection thread manage this thread.
				&&(g_psentinel->m_data.m_key.m_bValid)  // must have a valid license
				&&(
						(!g_psentinel->m_data.m_key.m_bExpires)
					||((g_psentinel->m_data.m_key.m_bExpires)&&(!g_psentinel->m_data.m_key.m_bExpired))
					||((g_psentinel->m_data.m_key.m_bExpires)&&(g_psentinel->m_data.m_key.m_bExpireForgiveness)&&(g_psentinel->m_data.m_key.m_ulExpiryDate+g_psentinel->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
						(!g_psentinel->m_data.m_key.m_bMachineSpecific)
					||((g_psentinel->m_data.m_key.m_bMachineSpecific)&&(g_psentinel->m_data.m_key.m_bValidMAC))
					)
				)
	{
//		EnterCriticalSection(&g_adc.m_crit);
//		g_adc.GetStatusData(pConn->m_pAPIConn);
//		LeaveCriticalSection(&g_adc.m_crit);

if(pChannel->m_pcritAPI)
{
	EnterCriticalSection(pChannel->m_pcritAPI);
}
/*
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONNECTSTATUS)
{
if((pChannel->m_ppAPIConn)&&((*(pChannel->m_ppAPIConn))))
{
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:connection", "pConn->m_pAPIConn->m_Status is %d", (unsigned char)(*(pChannel->m_ppAPIConn))->m_Status);  //Sleep(250); //(Dispatch message)
}
else
{ 
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:connection", "connection null? %d", (pChannel->m_ppAPIConn==NULL));
}
}
*/
//		if((pChannel->m_pAPIConn)&&(pChannel->m_pAPIConn->m_Status > 0))  // have to be connected.
		if((pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn))&&(((*(pChannel->m_ppAPIConn))->m_Status&0xff) == netConnected))  // have to be connected. not aborted or anything else
		{
			// snapshot the live list data
			if((!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true))
			{
				EnterCriticalSection(&g_adc.m_crit);
				try	{	plistdata = &((*(pChannel->m_ppAPIConn))->m_ListData[nZeroList]); } catch(...){}
				try	{	memcpy(&listdata, plistdata, sizeof(tlistdata));} catch(...){}
				LeaveCriticalSection(&g_adc.m_crit);
			}
if(pChannel->m_pcritAPI)
{
	LeaveCriticalSection(pChannel->m_pcritAPI);
}
			bool blistsizechanged = false;
			
			if((!g_psentinel->m_data.m_bProcessSuspended)&&(listdata.listcount!=lastliststatusdata.listcount)) blistsizechanged=true; // only get changes if not suspended


if(pChannel->m_pcritAPI)
{
	EnterCriticalSection(pChannel->m_pcritAPI);
}
			if(
				  ((g_psentinel->m_settings.m_bListStatusToDB)||(g_psentinel->m_settings.m_bUseControlModules))
				&&(
						(listdata.listchanged!=lastliststatusdata.listchanged)
//					||(listdata.listdisplay!=lastliststatusdata.listdisplay)   //just list changes, not event changes - this is just list status to db
					||(listdata.listsyschange!=lastliststatusdata.listsyschange)
					||(listdata.listcount!=lastliststatusdata.listcount)
					||(listdata.liststate!=lastliststatusdata.liststate)
//					||(!bconfirmed)
					)
				&&(!pChannel->m_bKillChannelThread)
				&&(!g_bKillThread)
				&&(!g_psentinel->m_data.m_bProcessSuspended)
				&&(pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn))
				)//&&((*pbKillConnThread)!=true))
			{
if(pChannel->m_pcritAPI)
{
	LeaveCriticalSection(pChannel->m_pcritAPI);
}
//				if(bconfirmed) bconfirmed=false;
				memcpy(&lastliststatusdata, &listdata, sizeof(tlistdata));  // resets mem stuff. moved here from below, since not checking errors anyway
				// NOTE : following DB stuff uses the copied lastliststatusdata to assemble, so if this gets moved back down, must reset back to listdata

				if((g_psentinel->m_settings.m_bUseControlModules)&&(g_psentinel->m_data.m_bControlModuleServiceThreadStarted)&&(!g_bKillThread)) // not kill thread because service module is already shut down.
				{

					int nChanges=-1;
					if(g_psentinel->m_settings.m_nCtrlModMaxQueue>0) 
					{
						EnterCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
						nChanges = g_psentinel->m_data.m_nNumChanges;
						LeaveCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
						if(nChanges>=g_psentinel->m_settings.m_nCtrlModMaxQueue)
						{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "discarding list change, queue is full (%d items)", nChanges); //  Sleep(250);//(Dispatch message)

							nChanges=1;
						}
						else nChanges = -1;
					}
					if(nChanges<0)
					{

#pragma message(alert "here, send a change to send xml to control modules for list changes")
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "creating list change"); //  Sleep(250);//(Dispatch message)

						CSentinelChange* pNewChange =  new CSentinelChange;
						if(pNewChange)
						{
							pNewChange->m_pObj = (void*) new CSentinelChannelObject;
							if(pNewChange->m_pObj)
							{
								pNewChange->m_nType = DLLCMD_ALIST;

								CSentinelChannelObject* pChgChannel = ((CSentinelChannelObject*)(pNewChange->m_pObj));

								*pChgChannel = *pChannel;
								pChgChannel->m_pszServerName = NULL;
								pChgChannel->m_pszDesc = NULL;
								pChgChannel->m_ppevents = NULL;
								pChgChannel->m_nNumEvents=0;

								pChgChannel->m_pptraffic=NULL;
								pChgChannel->m_nNumTraffic=0;

								pNewChange->m_nChannelID = pChannel->m_nChannelID;
								pNewChange->m_nHarrisListID = pChannel->m_nHarrisListID;

								pNewChange->m_pszServerName =  bu.XMLEncode(pChannel->m_pszServerName);
								pNewChange->m_pszDesc = bu.XMLEncode(pChannel->m_pszDesc);

								memcpy(&(pNewChange->m_list), &(lastliststatusdata), sizeof(tlistdata));

								pChgChannel->m_bChannelThreadStarted = false; // so it will not stall in deconstructor
								if(
										(!g_bKillThread) // not kill thread because service module is already shut down.
									&&(_beginthread(SentinelPublishChangeThread, 0, (void*)(pNewChange))==-1)
									)
								{
									//error.

									//**MSG
									g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start publish change thread."); //(Dispatch message)

									try{ delete (pNewChange); } catch(...){}
								}
							
							}
							else
							{
								try{ delete (pNewChange); } catch(...){}
							}
						}
					}
				}

				if((g_psentinel->m_settings.m_bListStatusToDB)&&(pdb)&&(pdbConn))
				{
					_ftime( &timestamp );

					pChannel->m_dblUpdateTime = ((double)(timestamp.time)) + ((double)(timestamp.millitm))/1000.0;


					if(!g_psentinel->m_settings.m_bDelayStatusForEvents)
					{
						if(g_psentinel->m_settings.m_bStatusInTables)
						{
	// new:
	//create table Channels (ID int NOT NULL identity(1,1), flags int NOT NULL, description varchar(256), server varchar(32), listid int, list_state int, list_changed int, list_display int, list_syschange int, list_count int, list_lookahead int, list_last_update float);
	//create table Connections (server varchar(32) NOT NULL, description varchar(256), flags int, client varchar(32), server_time float, server_status int, server_lists int, server_basis int, server_changed int, server_last_update float);
							
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
list_state = %d, \
list_changed = %d, \
list_display = %d, \
list_syschange = %d, \
list_count = %d, \
list_lookahead = %d, \
list_last_update = %d%03d WHERE \
server = '%s' AND listid = %d",
								g_psentinel->m_settings.m_pszChannels?g_psentinel->m_settings.m_pszChannels:"Channels",
								lastliststatusdata.liststate,
								lastliststatusdata.listchanged,				// incremented whenever list size changes
								lastliststatusdata.listdisplay,				// incremented whenever an event changes
								lastliststatusdata.listsyschange,			// incremented when list related values are changed. 
								lastliststatusdata.listcount,					// number of events in list 
								lastliststatusdata.lookahead,					// lookahead value

								(unsigned long)(timestamp.time),// unixtime - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
								timestamp.millitm,

								pChannel->m_pszServerName,
								pChannel->m_nHarrisListID
								);
	//if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "List change (tables): %s", szSQL);  //Sleep(250); //(Dispatch message)
	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
							if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
							{
								// error.
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);  //Sleep(50); //(Dispatch message)
							}
	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

						}


						if(g_psentinel->m_settings.m_bListStatusInExchange)
						{
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET flag = '%s:%02d|%d%03d|%d|%d|%d|%d', mod = %d WHERE \
criterion = 'List_Status' AND flag LIKE '%s:%02d%%'",
								g_psentinel->m_settings.m_pszExchange?g_psentinel->m_settings.m_pszExchange:"Exchange",
								pChannel->m_pszServerName,
								pChannel->m_nHarrisListID,
								(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
								timestamp.millitm,
								lastliststatusdata.listchanged,				// incremented whenever list size changes
								lastliststatusdata.listdisplay,				// incremented whenever an event changes
								lastliststatusdata.listsyschange,			// incremented when list related values are changed. 
								lastliststatusdata.listcount,					// number of events in list 
								lastliststatusdata.liststate,
								pChannel->m_pszServerName,
								pChannel->m_nHarrisListID
								);
	//if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "List change (exchange): %s", szSQL);  //Sleep(250); //(Dispatch message)
	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 					EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
							if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
							{
									// error.
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);  //Sleep(50); //(Dispatch message)
							}

	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 					LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
						}
					}
				}

//				memcpy(&lastliststatusdata, &listdata, sizeof(tlistdata));  // resets mem stuff.
			}
			else
			{
if(pChannel->m_pcritAPI)
{
	LeaveCriticalSection(pChannel->m_pcritAPI);
}
			}

			bool bChanges = false;

			if(g_psentinel->m_settings.m_bGetOnListSizeChangeOnly)
			{

				if(
						(listdata.listcount!=lastlistdata.listcount)
					||(!bconfirmed)
					)
				{
					if(!bChangesPending)
					{
						_ftime(&timebChanged);
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), 
	NULL, "pending", "****>>>> Initial size change on %s at %d.%03d", pChannel->m_pszDesc, timebChanged.time, timebChanged.millitm); 
					}
					bChangesPending = true;
					_ftime(&timebPending);
									memcpy(&lastlistdata, &listdata, sizeof(tlistdata));  // moved to here from below, below finishes by setting bChangesPending = false;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), 
	NULL, "pending", "****>>>> size change on %s at %d.%03d", pChannel->m_pszDesc, timebPending.time, timebPending.millitm); 
				}
				
			}
			else
			{
//	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:connection", "listchanged %d=?%d listdisplay %d=?%d", 
//		listdata.listchanged, lastlistdata.listchanged,
//		listdata.listdisplay, lastlistdata.listdisplay);
				if(
						(listdata.listchanged!=lastlistdata.listchanged)
					||(listdata.listdisplay!=lastlistdata.listdisplay)
					||(!bconfirmed)
					)
				{
					if(!bChangesPending)
					{
						_ftime(&timebChanged);
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0),
	NULL, "pending", "****>>>> Initial changes on %s at %d.%03d", pChannel->m_pszDesc, timebChanged.time, timebChanged.millitm); 
					}

					bChangesPending = true;
					_ftime(&timebPending);
									memcpy(&lastlistdata, &listdata, sizeof(tlistdata)); // moved to here from below, below finishes by setting bChangesPending = false;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0),
	NULL, "pending", "****>>>> changes on %s at %d.%03d", pChannel->m_pszDesc, timebPending.time, timebPending.millitm); 
				}
			}

			if((bChangesPending)&&(!g_psentinel->m_data.m_bProcessSuspended))
			{
				// have to check if we are going to allow the SQL to do the gets

				if(g_psentinel->m_settings.m_nMaxAutomationBufferMS>0) // buffer it!
				{
					_ftime( &timestamp );
					// check if we are good.
					if(
						  (timestamp.time > timebPending.time + g_psentinel->m_settings.m_nMaxAutomationBufferMS/1000)
						||
							(
								(timestamp.time == timebPending.time + g_psentinel->m_settings.m_nMaxAutomationBufferMS/1000)
							&&(timestamp.millitm > timebPending.millitm + g_psentinel->m_settings.m_nMaxAutomationBufferMS%1000)
							)
						)
					{
						bChanges = true;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0),
												NULL, "pending", ">>>>>>>> Changes on %s", pChannel->m_pszDesc); 

					}
					else  // no need to do the time check on force, if already changes = true.
					// only force if we are buffering, and there are changes.  do not force if we are not buffering, stuff will be immediate.
					if(
						  (timestamp.time > timebChanged.time + g_psentinel->m_settings.m_nMaxAutomationForceMS/1000)
						||
							(
								(timestamp.time == timebChanged.time + g_psentinel->m_settings.m_nMaxAutomationForceMS/1000)
							&&(timestamp.millitm > timebChanged.millitm + g_psentinel->m_settings.m_nMaxAutomationBufferMS%1000)
							)
						)
					{
						bChanges = true;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES)
 g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0),
												NULL, "forced", ">>>>>>>> Changes on %s", pChannel->m_pszDesc);  //Sleep(50);
					}

				}
				else
				{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0),
												 NULL, "no buffer", ">>>>>>>> Changes on %s", pChannel->m_pszDesc); 
					bChanges = true;
				}
			}

//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread checking list change", pChannel->m_pszDesc);  Sleep(50); //(Dispatch message)
if(pChannel->m_pcritAPI)
{
	EnterCriticalSection(pChannel->m_pcritAPI);
}
			if(
					(bChanges)
/*
				  (
					  (listdata.listchanged!=lastlistdata.listchanged)
					||(listdata.listdisplay!=lastlistdata.listdisplay)
					||(!bconfirmed)
					)
*/
				&&(!pChannel->m_bKillChannelThread)
				&&(!g_psentinel->m_data.m_bProcessSuspended)
				&&(!g_bKillThread)
				&&(pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn))
				)//&&((*pbKillConnThread)!=true)) // just do the event change one as well for now.
			{
if(pChannel->m_pcritAPI)
{
	LeaveCriticalSection(pChannel->m_pcritAPI);
}
				int nCheckPosDiff = 0;
				int nPosDiff = 0;
				bool bPosShift = true; //all have been shifted by the same amount.
				bool bKeyChange = false;
				bool bStatusChange = false;
				bool bPlaying = false;
				bool bPostTimeshifter = false;  // must invalidate times after an upcounting event.
				bool bPostTimeshiftPrimary = false;  // must invalidate times after an upcounting event, but not for secondaries, necessarily.
				int nTodayRef = -1;  // "today" has not been determined.

				
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TIMING)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), 
	NULL, "Sentinel:debug", "[%d] %s SentinelChannelThread list state status: 0x%08x, getting events", 
	clock(), pChannel->m_pszDesc?pChannel->m_pszDesc:"(null)", listdata.liststate);  //Sleep(50); //(Dispatch message)
				// the list has changed, need to get all the events again  (will take care of any event changes as well)

				if((g_psentinel->m_settings.m_pszEventKeyFormat)&&(strcmp(g_psentinel->m_settings.m_pszEventKeyFormat, szLastKey)))
				{
					bKeyChange = true;
					_snprintf(szCurrentKey, MAX_MESSAGE_LENGTH-1, "%s", g_psentinel->m_settings.m_pszEventKeyFormat);
				}
/*
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_API)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), 
	NULL, "Sentinel:debug", "[%d] %s SentinelChannelThread list A", 
	clock(), pChannel->m_pszDesc?pChannel->m_pszDesc:"(null)");  //Sleep(50); //(Dispatch message)
*/
				CSentinelEventObject* pParent=NULL;
				CSentinelEventObject* pEvent=NULL;
				int nPrecedingUid = -1;
				double dblTimeOffset = 0.0;

				double dblEventTimeTolerance = ((double)(g_psentinel->m_settings.m_nEventTimeToleranceMS))/1000.0;
				if(dblEventTimeTolerance<=0) dblEventTimeTolerance = 0.500;

				// first need to get events until playing, get those in the DB right away.
/*
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_API)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), 
	NULL, "Sentinel:debug", "[%d] %s SentinelChannelThread list B", 
	clock(), pChannel->m_pszDesc?pChannel->m_pszDesc:"(null)");  //Sleep(50); //(Dispatch message)
*/
				int nNumEvents = -1;

				bool bDoNotBreakOnChange = g_psentinel->m_settings.m_bDoNotBreakOnChange;

				if(bDoNotBreakOnChange) EnterCriticalSection(&g_psentinel->m_data.m_critChange);
				pChannel->m_bChannelThreadGettingEvents = true;
/*
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_API)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), 
	NULL, "Sentinel:debug", "[%d] %s SentinelChannelThread list C", 
	clock(), pChannel->m_pszDesc?pChannel->m_pszDesc:"(null)");  //Sleep(50); //(Dispatch message)
*/
				int nNumToGet = (g_psentinel->m_settings.m_ulEventsPerRequest>0)?(g_psentinel->m_settings.m_ulEventsPerRequest):(g_psentinel->m_settings.m_bUseListCount?listdata.listcount:listdata.lookahead);

/*
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_API)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), 
	NULL, "Sentinel:debug", "[%d] %s SentinelChannelThread list to get %d events from %d %d?%d:%d", 
	clock(), pChannel->m_pszDesc?pChannel->m_pszDesc:"(null)", nNumToGet, g_psentinel->m_settings.m_ulEventsPerRequest, g_psentinel->m_settings.m_bUseListCount, listdata.listcount, listdata.lookahead);  //Sleep(50); //(Dispatch message)
*/

				if((g_psentinel->m_settings.m_nMaxLookahead>0)&&(g_psentinel->m_settings.m_nMaxLookahead<nNumToGet)) nNumToGet=g_psentinel->m_settings.m_nMaxLookahead;

/*
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_API)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), 
	NULL, "Sentinel:debug", "[%d] %s SentinelChannelThread list to get %d events, %d %d", 
	clock(), pChannel->m_pszDesc?pChannel->m_pszDesc:"(null)", nNumToGet, listdata.liststate, bDoNotBreakOnChange);  //Sleep(50); //(Dispatch message)
*/

				if((!(listdata.liststate&(1<<LISTISPLAYING)))||(bDoNotBreakOnChange)) // if do not break, just get all events
				{
//					nNumToGet = (g_psentinel->m_settings.m_ulEventsPerRequest!=0)?(g_psentinel->m_settings.m_ulEventsPerRequest):(g_psentinel->m_settings.m_bUseListCount?listdata.listcount:listdata.lookahead);
//					if((g_psentinel->m_settings.m_nMaxLookahead>0)&&(g_psentinel->m_settings.m_nMaxLookahead<nNumToGet)) nNumToGet=g_psentinel->m_settings.m_nMaxLookahead;
					bPlaying = false;

/*
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_API)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), 
	NULL, "Sentinel:debug", "[%d] %s SentinelChannelThread list getting %d events", 
	clock(), pChannel->m_pszDesc?pChannel->m_pszDesc:"(null)", nNumToGet);  //Sleep(50); //(Dispatch message)
*/
					if(nNumToGet>0)
					{


if(pChannel->m_pcritAPI)
{
	EnterCriticalSection(pChannel->m_pcritAPI);
}
						if(pChannel->m_ppAPIConn)
						{
							EnterCriticalSection(&g_adc.m_crit);

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_API)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), 
	NULL, "Sentinel:debug", "[%d] %s SentinelChannelThread Entered Critsec for %d events on ptr %d", 
	clock(), pChannel->m_pszDesc?pChannel->m_pszDesc:"(null)", nNumToGet, pChannel->m_ppAPIConn);  //Sleep(50); //(Dispatch message)

							nNumEvents = g_adc.GetEvents((*(pChannel->m_ppAPIConn)),
								pChannel->m_nHarrisListID, 
								0, //start at the top.
								nNumToGet,
								((bDoNotBreakOnChange?0:ADC_GETEVENTS_BREAKONCHANGE)|ADC_GETEVENTS_LIST|ADC_GETEVENTS_USEOFFSET)|(g_psentinel->m_settings.m_bUseListCount?(ADC_GETEVENTS_FULLLIST):0),
								ADC_DEFAULT_TIMEOUT,
								&bStatusChange,
								&listdata
								);
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_API)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), 
	NULL, "Sentinel:debug", "[%d] %s SentinelChannelThread obtained %d events", 
	clock(), pChannel->m_pszDesc?pChannel->m_pszDesc:"(null)", nNumEvents);  //Sleep(50); //(Dispatch message)
							LeaveCriticalSection(&g_adc.m_crit);
						}
						else
						{
							nNumEvents = ADC_NO_EVENTS;
						}
if(pChannel->m_pcritAPI)
{
	LeaveCriticalSection(pChannel->m_pcritAPI);
}
					}
					else
					{
						nNumEvents = ADC_NO_EVENTS;
					}
				}
				else
				{
//					nNumToGet = (g_psentinel->m_settings.m_bUseListCount?listdata.listcount:listdata.lookahead);
//					if((g_psentinel->m_settings.m_nMaxLookahead>0)&&(g_psentinel->m_settings.m_nMaxLookahead<nNumToGet)) nNumToGet=g_psentinel->m_settings.m_nMaxLookahead;
					bPlaying = true;
					if(nNumToGet>0)
					{
if(pChannel->m_pcritAPI)
{
	EnterCriticalSection(pChannel->m_pcritAPI);
}						if(pChannel->m_ppAPIConn)
						{
							EnterCriticalSection(&g_adc.m_crit);
							nNumEvents = g_adc.GetEventsUntilPlaying((*(pChannel->m_ppAPIConn)),
								pChannel->m_nHarrisListID, 
								0, //start at the top.
								nNumToGet,
								(ADC_GETEVENTS_BREAKONCHANGE|ADC_GETEVENTS_LIST|ADC_GETEVENTS_USEOFFSET)|(g_psentinel->m_settings.m_bUseListCount?(ADC_GETEVENTS_FULLLIST):0),
								ADC_DEFAULT_TIMEOUT,
								&bStatusChange,
								&listdata
								);
							LeaveCriticalSection(&g_adc.m_crit);
						}
						else
						{
							nNumEvents = ADC_NO_EVENTS;
						}
if(pChannel->m_pcritAPI)
{
	LeaveCriticalSection(pChannel->m_pcritAPI);
}
					}
					else
					{
						nNumEvents = ADC_NO_EVENTS;
					}
				}
				pChannel->m_bChannelThreadGettingEvents = false;

				pChannel->SetAllUnprocessed();
				bPostTimeshifter = false;  // must reset this for top of list.

if(g_psentinel->m_settings.m_ulDebug&(SENTINEL_DEBUG_SEARCH|SENTINEL_DEBUG_CHANGES|SENTINEL_DEBUG_TIMING))
{
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), 
		NULL, "GET", "[%d]\n**PROCESSING**  get %d, got %d", clock(), nNumToGet, nNumEvents); 
}


				

				BYTE systemfrx = 0x29;
				unsigned long ulRefTimeMS=0;
				double dblLastPrimaryEventTime = -1.0;
				double dblLastEventTime = -1.0;
				unsigned short usLastPrimaryOnAirJulianDate = DATE_NOT_DEFINED;
				unsigned long ulLastPrimaryOnAirTimeMS = TIME_NOT_DEFINED;  // day offset.
//				unsigned long ulLastPrimaryOnAirDurMS = TIME_NOT_DEFINED;
				double dblLastPrimaryOnAirDur = 0.0; //seconds
				double dblServerTime = -1.0;
				unsigned short usOnAirJulianDate = 0;
				if(pChannel->m_pcritAPI)
				{
	EnterCriticalSection(pChannel->m_pcritAPI);
					try
					{
						if((pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn)) ) 
						{
							usOnAirJulianDate = (*(pChannel->m_ppAPIConn))->m_usRefJulianDate; //"today"
							systemfrx = (*(pChannel->m_ppAPIConn))->m_SysData.systemfrx;
							ulRefTimeMS = (*(pChannel->m_ppAPIConn))->m_ulRefTimeMS;
							dblServerTime = ((double)((*(pChannel->m_ppAPIConn))->m_usRefJulianDate - 25567))*86400.0   // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
										+ (double)ulRefTimeMS/1000.0;
						}
					}
					catch(...)
					{
					}
	LeaveCriticalSection(pChannel->m_pcritAPI);
				}

				double dblLastCalcPrimaryEventTime = -1.0;
				double dblLastCalcEventTime = -1.0;
				int nNumSec = 0;
				int nLastParentPos = -1;
				int nLastSecondaryIndex = -1;

//				char chLastParentID[64];
//				strcpy(chLastParentID, "");

//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TIMING)	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "[%d] %s SentinelChannelThread list change, %s%d%s", nClock, pChannel->m_pszDesc, bPlaying?"playing event at ":"", nNumEvents, bPlaying?"":" events"); // Sleep(50); //(Dispatch message)

				bool bSQLerror = false;

/*
// was this:
				if((!pChannel->m_bKillChannelThread)&&(pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn))) 
					pList = (*(pChannel->m_ppAPIConn))->m_pList[nZeroList];  // volatile, always reassign
				else 
					pList = NULL;
*/
// now this:
				if(pChannel->m_pcritAPI)
				{
	EnterCriticalSection(pChannel->m_pcritAPI);
				}
				
				try
				{

					if((!pChannel->m_bKillChannelThread)&&(pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn))) 
					{
						if( (*(pChannel->m_ppAPIConn))->m_pList[nZeroList] == NULL)
						{
							(*(pChannel->m_ppAPIConn))->m_pList[nZeroList] = new CAList;  // ensure it is allocated in case there are no events.  If there ARE events later, this will just get used.
						}
						pList = (*(pChannel->m_ppAPIConn))->m_pList[nZeroList];  // volatile, always reassign
						plistdata = &((*(pChannel->m_ppAPIConn))->m_ListData[nZeroList]);
					}
					else
					{
						pList = NULL;
						plistdata = NULL;
					}
				}
				catch(...)
				{
					pList = NULL;
					plistdata = NULL;
				}



if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), NULL, "Sentinel:debug", "usOnAirJulianDate=%d, dblServerTime=%.03f on %s, %d pList=%d", 
	usOnAirJulianDate, dblServerTime, pChannel->m_pszDesc,
	(*(pChannel->m_ppAPIConn)), pList);  //Sleep(50); //(Dispatch message)

				if(
						//(nNumEvents != ADC_NO_EVENTS)&&   // commented this out, since if there are no events left we do want to erase what we have, if this was a disconnection...
					  (pList)&& // commented this out, because pList might be NULL if there are no events.  do want to come in and have any existing events be erased
						// but put it back in so that if pList is NULL it wont get dereferenced inside... put back becuase above, now, as long as the connection is OK, a new list object gets created for the no event case.
						(!pChannel->m_bKillChannelThread)&&(pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn))
					&&(!g_bKillThread)
					)//&&((*pbKillConnThread)!=true))
				{
					if(pChannel->m_pcritAPI)
					{
 	LeaveCriticalSection(pChannel->m_pcritAPI);
					}
				
					int nClock = clock();
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TIMING)	
g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), NULL, "Sentinel:debug", "[%d] %s SentinelChannelThread list change, %s%d%s", nClock, pChannel->m_pszDesc, bPlaying?"playing event at ":"", nNumEvents, bPlaying?"":" events"); // Sleep(50); //(Dispatch message)
					int nUpdates = 0;
					pChannel->m_nLastFound=-1;

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread 1 list change, %s%d%s, entering critical", pChannel->m_pszDesc, bPlaying?"playing event at ":"", nNumEvents, bPlaying?"":" events");  Sleep(50); //(Dispatch message)
					EnterCriticalSection(&(pList->m_crit));  // lock the list out
// 	EnterCriticalSection(pChannel->m_pcritAPI);
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread list change, in critical", pChannel->m_pszDesc);  Sleep(50); //(Dispatch message)
					if((nNumEvents>=ADC_NO_PLAYINGEVENT)&&(pList)&&(!pChannel->m_bKillChannelThread))//&&(pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn)))//&&((*pbKillConnThread)!=true))
					{
// 	LeaveCriticalSection(pChannel->m_pcritAPI);

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES)	
g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), NULL, "Sentinel:debug", "[%d] %s SentinelChannelThread nNumEvents=%d", nClock, pChannel->m_pszDesc, nNumEvents); // Sleep(50); //(Dispatch message)
						// deal with database.
// 	EnterCriticalSection(pChannel->m_pcritAPI);
						if((pdb!=NULL)&&(pdbConn!=NULL)&&(g_psentinel->m_settings.m_pszLiveEvents)&&(strlen(g_psentinel->m_settings.m_pszLiveEvents)>0)&&(!pChannel->m_bKillChannelThread))//&&(pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn)))
						{
// 	LeaveCriticalSection(pChannel->m_pcritAPI);

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), NULL, "SentinelChannelThread:debug", "dealing with db on %s", pChannel->m_pszDesc);//  Sleep(50); //(Dispatch message)





//CREATE TABLE Events (itemid int identity(1,1) NOT NULL, server_name varchar(32) NOT NULL, server_list int NOT NULL, list_id int NOT NULL, event_id varchar(32) NOT NULL, event_clip varchar(64) NOT NULL, event_title varchar(64) NOT NULL, event_data varchar(4096) NULL, event_type int NOT NULL, event_status int NOT NULL, event_time_mode int NOT NULL, event_start decimal(20,3) NOT NULL, event_duration int NOT NULL, event_last_update decimal(20,3) NOT NULL, event_calc_start decimal(20,3) NOT NULL, event_position int NOT NULL, parent_id varchar(32) NULL, parent_position int NULL, parent_start decimal(20,3) NULL, parent_calc_start decimal(20,3) NULL, parent_duration int, parent_calc_end decimal(20,3), event_calc_end decimal(20,3), app_data varchar(512) NULL, app_data_aux int NULL); /* ADD CONSTRAINT IX_Event UNIQUE (server_name, list_id, app_data_aux);*/
//CREATE TABLE Events_Temp (itemid int identity(1,1) NOT NULL, server_name varchar(32) NOT NULL, server_list int NOT NULL, list_id int NOT NULL, event_id varchar(32) NOT NULL, event_clip varchar(64) NOT NULL, event_title varchar(64) NOT NULL, event_data varchar(4096) NULL, event_type int NOT NULL, event_status int NOT NULL, event_time_mode int NOT NULL, event_start decimal(20,3) NOT NULL, event_duration int NOT NULL, event_last_update decimal(20,3) NOT NULL, event_calc_start decimal(20,3) NOT NULL, event_position int NOT NULL, parent_id varchar(32) NULL, parent_position int NULL, parent_start decimal(20,3) NULL, parent_calc_start decimal(20,3) NULL, parent_duration int, parent_calc_end decimal(20,3), event_calc_end decimal(20,3), app_data varchar(512) NULL, app_data_aux int NULL);

							int n=0;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), NULL, "Sentinel:debug", "nNumEvents = %d,  checking > %d on %s", nNumEvents,ADC_NO_PLAYINGEVENT, pChannel->m_pszDesc); // Sleep(50); //(Dispatch message)

// in here, lets find the first non-done primary, which will determine "today".

							while((!pChannel->m_bKillChannelThread)&&(n<nNumEvents)&&(!g_bKillThread))//&&(pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn)))//&&((*pbKillConnThread)!=true))
							{
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "seacrching %d of %d on %s", n+1, nNumEvents, pChannel->m_pszDesc); // Sleep(50); //(Dispatch message)
								if(
									  (pList->m_ppEvents)
									&&(pList->m_ppEvents[n])
									&&(!(pList->m_ppEvents[n]->m_usType&SECONDARYEVENT))
									&&(!(pList->m_ppEvents[n]->m_usStatus&usHARRISDONE))
									)
								{
									// we found it.
									nTodayRef = n;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SEARCH) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "TODAY ref primary [%s][%s] pos %d on %s", pList->m_ppEvents[n]->m_pszID, pList->m_ppEvents[n]->m_pszTitle, n, pChannel->m_pszDesc);
									break;
								}
								else
								{
									n++;
								}
							}

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Searched for non-done primary on %s", pChannel->m_pszDesc); //Sleep(50);

							//now we go back and count up offset.

							//what day is the first non-done event?
							unsigned short usOnAirJulianDateRef = usOnAirJulianDate;
							
							if((n>=0)&&(n<nNumEvents))
							{
//	EnterCriticalSection(pChannel->m_pcritAPI);
								if(pList->m_ppEvents[n]->m_ulOnAirTimeMS > ulRefTimeMS) 
								{
//	LeaveCriticalSection(pChannel->m_pcritAPI);

									// two cases.  the event is about to start later today
									// no prob

									// m_ulRefTimeMS has wrapped.
									// we can detect this by:
									if(pList->m_ppEvents[n]->m_ulOnAirTimeMS+pList->m_ppEvents[n]->m_ulDurationMS > 86400000) 
									{ // this was the event spanning midnight, and we are in the new day.
										usOnAirJulianDateRef--;
									}
								}
								else
								{
//	LeaveCriticalSection(pChannel->m_pcritAPI);
									// the event has already started, and we have not date wrapped the servertime offset.
									// no prob, it's just today.
									// or everything is done or just secondaries.

									// or another case!
									// the time is for TOMORROW - a hard time waiting to play.
									if(!(pList->m_ppEvents[n]->m_usStatus&usHARRISPREORPLAY))
									{
										if(pList->m_ppEvents[n]->m_ulControl&usHARRISHARD)
										{
											// hard start, clock time less than "now" means tomorrow
											usOnAirJulianDateRef++;
											usOnAirJulianDate = usOnAirJulianDateRef; // because everything must cascade down from this.
										}
									}

								}
								pList->m_ppEvents[n]->m_usOnAirJulianDate = usOnAirJulianDateRef; // I'm reassigning this because it's bogus anyway from Harris
							}

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "assigning reftime (n=%d): usOnAirJulianDate=%d, dblServerTime=%.03f on %s ", n, usOnAirJulianDate, dblServerTime, pChannel->m_pszDesc); // Sleep(50); //(Dispatch message)

							int nCheckEvent = n-1;  //pList->m_ppEvents[n]->m_usOnAirJulianDate is already 
							int nPrimaryEvent;
							int nsectally = 0;  //secondary tally
							while((!pChannel->m_bKillChannelThread)&&(nCheckEvent>=0)&&(n>=0)&&(nCheckEvent<nNumEvents)&&(n<nNumEvents)&&(!g_bKillThread))//&&(pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn)))//&&((*pbKillConnThread)!=true))
							{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "BING nsectally=%d nCheckEvent=%d on %s ", nsectally, nCheckEvent, pChannel->m_pszDesc); // Sleep(50); //(Dispatch message)
								if(pList->m_ppEvents[nCheckEvent]->m_usType&SECONDARYEVENT)
								{
									nsectally++;
								}
								else
								{
									nPrimaryEvent = nCheckEvent;
									//pri.
									if(pList->m_ppEvents[nCheckEvent]->m_ulOnAirTimeMS > pList->m_ppEvents[n]->m_ulOnAirTimeMS )
									{
										// this case is if you have 23:00:00 and the next item is 00:01:00 or something like that.

										// but you could also have the case where a hard time trumped another event
															// another case is that there is a hard time like this:
															// 10:00:00 something
															// 10:01:00 something else
															// 10:00:30 hard time.
															// something gets cut off, something else misses, and hard time went
															

										if(pList->m_ppEvents[n]->m_ulControl&usHARRISHARD) // we just guess that it was truncated.  not always a good guess.
										{
											// except that there is another case. 
											//  23:00:00 something from day before
											//  08:00:00 hard time.

											// so we need some kind of way to distinguish these two cases.
											// let's use the MISSED status to indicate cutoff - otherwise must be previous day.

											if(pList->m_ppEvents[nCheckEvent]->m_usStatus&(1<<notplayed))
											{										
												// so, keep it as "today"
											}
											else // it was played out, so must not have been cut off!
											{
												usOnAirJulianDateRef--;
											}


										}
										else
										{
											usOnAirJulianDateRef--;
										}
									}
									else
									if(pList->m_ppEvents[nCheckEvent]->m_ulDurationMS > pList->m_ppEvents[n]->m_ulOnAirTimeMS )
									{
										// this case is if you have 00:00:30 and dur is 23:59:59 and the next item is 00:01:00 or something like that.
										// actrually I don't get this one at all - leaving it though.  I figured it out before, it may in fact make sense
										usOnAirJulianDateRef--;
									}

									
									pList->m_ppEvents[nCheckEvent]->m_usOnAirJulianDate = usOnAirJulianDateRef; // I'm reassigning this because it's bogus anyway from Harris

									nCheckEvent++;

									// now set all the secondaries to the PRIMARY julian date - we will offset later if nec..
									while((!pChannel->m_bKillChannelThread)&&(nCheckEvent<n)&&(!g_bKillThread))//&&(pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn)))//&&((*pbKillConnThread)!=true))
									{
										pList->m_ppEvents[nCheckEvent]->m_usOnAirJulianDate = usOnAirJulianDateRef;
										nCheckEvent++;
									}
									
									n = nPrimaryEvent; 
									nCheckEvent = n; // gets decremented just below
									nsectally=0;
								}

								nCheckEvent--;
							} //while...
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "nsectally=%d nCheckEvent=%d on %s ", nsectally, nCheckEvent, pChannel->m_pszDesc); // Sleep(50); //(Dispatch message)

							// deal with secondaries at the top of the list.
							while((!pChannel->m_bKillChannelThread)&&(nsectally>0)&&(!g_bKillThread))//&&(pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn)))//&&((*pbKillConnThread)!=true))
							{
								nsectally--;
								pList->m_ppEvents[nsectally]->m_usOnAirJulianDate = usOnAirJulianDateRef;
							}
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), NULL, "SentinelChannelThread:debug", "assigned event (num=%d) reftime: usOnAirJulianDateRef=%d, dblServerTime=%.03f on %s ", nNumEvents, usOnAirJulianDateRef, dblServerTime, pChannel->m_pszDesc); // Sleep(50);// (Dispatch message)

							n=0;
							if(nNumEvents>ADC_NO_PLAYINGEVENT)
							{

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "nNumEvents>ADC_NO_PLAYINGEVENT was true on %s", pChannel->m_pszDesc);  //Sleep(50); //(Dispatch message)
/////////////////////////////////////////////////////////////////////////////////////////
								// get the next primary index, which we are guaranteed to have.  This ensures any playing secondaries are also covered in this call.

								if(bPlaying)
								{
//	EnterCriticalSection(pChannel->m_pcritAPI);
									EnterCriticalSection(&g_adc.m_crit);
									nNumEvents = g_adc.GetNextPrimaryIndex((pChannel->m_ppAPIConn?(*(pChannel->m_ppAPIConn)):NULL), // if the conn pointer is null, it just returns ADC_ERROR
										pChannel->m_nHarrisListID, 
										nNumEvents //start at playing event.
										);
									LeaveCriticalSection(&g_adc.m_crit);
//	LeaveCriticalSection(pChannel->m_pcritAPI);
									nNumEvents++; // the number, not the last index, so we increment
								}

								// following line does not check status change, we want to take whatever we got, even if status changes
pChannel->m_bChannelThreadPublishingEvents = true;

								while((!pChannel->m_bKillChannelThread)&&(n<nNumEvents)&&(pChannel->m_ppAPIConn)/*&&(*(pChannel->m_ppAPIConn))*/&&(!g_bKillThread))//&&((*pbKillConnThread)!=true))
								{

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID); // Sleep(50); //(Dispatch message)
									if((pList->m_ppEvents)&&(pList->m_ppEvents[n]))//&&(pList->m_ppEvents[n]->m_pszID)&&(strlen(pList->m_ppEvents[n]->m_pszID)))// cant exclude stuff without ID.
									{
										double dblEventTime = 0.0; 
										double dblCalcEventTime = 0.0; 

								//		if(usOnAirJulianDate == 0xffff)  // can't trust event based julian date, have to use server date
	//									{
	//										_timeb timestamp;
	//										_ftime(&timestamp);

	//										usOnAirJulianDate = pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.

	// move time calc to below julian recalc
	//										dblEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
	//											+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
	//									}
	/*
										else
										{
											dblEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0    // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
												+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
										}
	*/											
	//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s] again", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
/*
										if(usLastPrimaryOnAirJulianDate != 0xffff)
										{
	//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s] again1a", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
											usOnAirJulianDate = usLastPrimaryOnAirJulianDate;
										}
										else
										{
	//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s] again1b", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
											usOnAirJulianDate = pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.  a wrong guess.

											//if you have an event starting at 23:59, and it is currently 2 minutes later, this will be wrong.
										}
pChannel->m_pAPIConn->m_ulRefTimeMS xxx
*/
	//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s] again2", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)

										NewEvent.m_event.m_usType = pList->m_ppEvents[n]->m_usType;
										NewEvent.m_nFlags=0;  // resets, removes SENTINEL_EVENT_TIMEMASK

										NewEvent.m_nRefCount=0; // have to reset this every time.
										double dblOffsetTime = (pList->m_ppEvents[n]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)?(0.000):((double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0);

										if(NewEvent.m_event.m_usType&SECONDARYEVENT)
										{
											nNumSec++;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "SEC-event [%s][%s]: gdate:%d edate:%d time=%.03f, calc=%.03f, offset=%.03f, last pri=%.3f, last pri calc=%.3f on %s ", 
	pList->m_ppEvents[n]->m_pszID, pList->m_ppEvents[n]->m_pszTitle, usOnAirJulianDate, pList->m_ppEvents[n]->m_usOnAirJulianDate, dblEventTime, dblCalcEventTime,dblOffsetTime, dblLastPrimaryEventTime, dblLastCalcPrimaryEventTime, pChannel->m_pszDesc);  //Sleep(50); //(Dispatch message)

											
											switch(NewEvent.m_event.m_usType&0xff)
											{
											case SECBACKAVEVENT:
											case SECBACKTIMEGPI:
											case SECBACKSYSTEM:
												{
													if((dblLastPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
													{
														if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
														{
															dblEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0 - dblOffsetTime;
														}
														else
														{
															dblEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0 - dblOffsetTime;
														}
													}
													else
													{
														dblEventTime = dblLastPrimaryEventTime - dblOffsetTime;
													}

													if((dblLastCalcPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
													{
														if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
														{
															dblCalcEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0 - dblOffsetTime;
														}
														else
														{
															dblCalcEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0 - dblOffsetTime;
														}
													}
													else
													{
														dblCalcEventTime = dblLastCalcPrimaryEventTime - dblOffsetTime;
													}
												} break;
											case SECCOMMENT:
											case SECCOMPILEID:// (Byte)(225)
											case SECAPPFLAG:// (Byte)(226)
											case SECBARTERSPOT:// (Byte)(227)
											case INVALIDEVENT:
												{
													if((dblLastPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
													{
														if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
														{
															dblEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0;
														}
														else
														{
															dblEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0;
														}
													}
													else
													{
														dblEventTime = dblLastPrimaryEventTime;
													}

													if((dblLastCalcPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
													{
														if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
														{
															dblCalcEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0;
														}
														else
														{
															dblCalcEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0;
														}
													}
													else
													{
														dblCalcEventTime = dblLastCalcPrimaryEventTime;
													}
													// a comment, for instance, has TIME_NOT_DEFINED. but we want it to come after the primary it belongs to,
													// so we used to give it .001 seconds.  Now we give it the correct actual time, 
													// but sort it on parent_start_calc + (event_position-parent_position)/1000.0, or more correctly
													// order by parent_position<0?((event_position)/1000.0):(parent_calc_start + (event_position-parent_position)/1000.0)
												} break;
											default:
												{
													if((dblLastPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
													{
														if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
														{
															dblEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0 + dblOffsetTime;
														}
														else
														{
															dblEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0 + dblOffsetTime;
														}
													}
													else
													{
														dblEventTime = dblLastPrimaryEventTime + dblOffsetTime;
													}

													if((dblLastCalcPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
													{
														if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
														{
															dblCalcEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0 + dblOffsetTime;
														}
														else
														{
															dblCalcEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0 + dblOffsetTime;
														}
													}
													else
													{
														dblCalcEventTime = dblLastCalcPrimaryEventTime + dblOffsetTime;
													}
												} break;
											}
// do not set the date here.
//												usOnAirJulianDate = (usLastPrimaryOnAirJulianDate!=DATE_NOT_DEFINED)?usLastPrimaryOnAirJulianDate:pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.

/*
// dont do any wrapping on secondaries.
											if(pList->m_ppEvents[n]->m_ulOnAirTimeMS + ulLastPrimaryOnAirTimeMS >= 86400000 )
											{
												//wrapped days.
												usOnAirJulianDate++;
											}
*/
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "SEC-calcd [%s][%s]: gdate:%d edate:%d time=%.03f, calc=%.03f, offset=%.03f, last pri=%.3f, last pri calc=%.3f on %s ", 
	pList->m_ppEvents[n]->m_pszID, pList->m_ppEvents[n]->m_pszTitle, usOnAirJulianDate, pList->m_ppEvents[n]->m_usOnAirJulianDate, dblEventTime, dblCalcEventTime,dblOffsetTime, dblLastPrimaryEventTime, dblLastCalcPrimaryEventTime, pChannel->m_pszDesc);  //Sleep(50); //(Dispatch message)

										}
										else // it's a primary
										{
											nNumSec=0;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "PRI-event [%s][%s]: gdate:%d edate:%d time=%.03f, calc=%.03f, offset=%.03f on %s ", 
	pList->m_ppEvents[n]->m_pszID, pList->m_ppEvents[n]->m_pszTitle, usOnAirJulianDate, pList->m_ppEvents[n]->m_usOnAirJulianDate, dblEventTime, dblCalcEventTime,dblOffsetTime, pChannel->m_pszDesc);  //Sleep(50); //(Dispatch message)

											if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
											{
												dblEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0 + dblOffsetTime;
											}
											else
											{
/*
												if(usLastPrimaryOnAirJulianDate!=DATE_NOT_DEFINED)
												{
													usOnAirJulianDate = usLastPrimaryOnAirJulianDate;
												}
												else
												{
												}
*/

												if(n<listdata.lookahead)
												{
													if(ulLastPrimaryOnAirTimeMS!=TIME_NOT_DEFINED)  // should be the only check on valid last primary.
													{
														if(ulLastPrimaryOnAirTimeMS > pList->m_ppEvents[n]->m_ulOnAirTimeMS)
														{
//	EnterCriticalSection(pChannel->m_pcritAPI);
															if((pList->m_ppEvents[n]->m_ulControl&usHARRISHARD)&&(ulRefTimeMS<=pList->m_ppEvents[n]->m_ulOnAirTimeMS))
															{
																// this is going to cut off anything else but remain the same day.
															}
															else
															{
																// we wrapped days.
																usOnAirJulianDate++;
															}
//	LeaveCriticalSection(pChannel->m_pcritAPI);
															// OK not necessarily.
															// another case is that there is a hard time like this:
															// 10:00:00 something
															// 10:01:00 something else
															// 10:00:30 hard time.
															// something gets cut off, something else misses, and hard time starts today!
															// this only happens if the current time is < the hard time.  if >, it becomes tomorrow!
															// ok, so add all the ifs around this to prevent this case from incrementing the day.

														}
													}

													dblEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
														+ dblOffsetTime;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "----event [%s][%s]: gdate:%d edate:%d time=%.03f, calc=%.03f, offset=%.03f on %s ", 
	pList->m_ppEvents[n]->m_pszID, pList->m_ppEvents[n]->m_pszTitle, usOnAirJulianDate, pList->m_ppEvents[n]->m_usOnAirJulianDate, dblEventTime, dblCalcEventTime,dblOffsetTime, pChannel->m_pszDesc);  //Sleep(50); //(Dispatch message)


												}
												else
												{
													// we are beyond the lookahead, need to deal with estimated times, not actual as reported by harris.
													dblEventTime = dblLastPrimaryEventTime + dblLastPrimaryOnAirDur;
													// we dont care about ulLastPrimaryOnAirTimeMS for calculations after this point
													// but we do care about julian day ... so... ok actually we dont care.  that was for calc anyway.
												}
											}

/*
enum teventrunstatus {
     eventdone,
     eventrunning,
     playednextvideo,
     eventprerolled,
     eventpostrolled,
     eventidtitle,
     eventstandbyon,
     notplayed,
     eventranshort,
     eventskipped,
     eventpreped,
     eventnotswitched,
     eventpreviewed,
     rollingnext,
     eventshort,
     eventlong };
*/

											if(
												  (pList->m_ppEvents[n]->m_usStatus&usHARRISDONEORPLAY)
												||(pList->m_ppEvents[n]->m_ulControl&usHARRISHARD)// hard start
												)
											{
												dblCalcEventTime = dblEventTime;
											}
											else
											{
												if(ulLastPrimaryOnAirTimeMS!=TIME_NOT_DEFINED)  // should be the only check on valid last primary.
												{
													if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
													{// we got the today ref from the topmost non-done primary.  so we want to use that calc instead of just following on, in case the list was started and stopped a bunch of times and you have done events from the past above.
														dblCalcEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0 + dblOffsetTime;
													}
													else
													{
														dblCalcEventTime = dblLastCalcPrimaryEventTime + dblLastPrimaryOnAirDur;
													}
												}
												else
												{
													dblCalcEventTime = dblEventTime;
												}
											}

											
											dblLastPrimaryOnAirDur = ((pList->m_ppEvents[n]->m_ulDurationMS==TIME_NOT_DEFINED)?0.0:((double)pList->m_ppEvents[n]->m_ulDurationMS/1000.0));
											ulLastPrimaryOnAirTimeMS = pList->m_ppEvents[n]->m_ulOnAirTimeMS;
											dblLastPrimaryEventTime = dblEventTime;
											usLastPrimaryOnAirJulianDate = usOnAirJulianDate;
											dblLastCalcPrimaryEventTime = dblCalcEventTime;
											nLastParentPos = n;
//											strcpy(chLastParentID, pList->m_ppEvents[n]->m_pszID);

										}

										dblLastCalcEventTime = dblCalcEventTime;
										dblLastEventTime = dblEventTime;
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s] again3", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  //Sleep(50); //(Dispatch message)
//g_psentinel->m_msgr.DM(MSG_ICONUSER1, NULL, "Sentinel:debug", "before encode -> Event %d", n);//  Sleep(50); //(Dispatch message)

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "-->-event [%s][%s]: time=%.03f, calc=%.03f dur=%.03f status 0x%08x on %s ", 
	pList->m_ppEvents[n]->m_pszID, pList->m_ppEvents[n]->m_pszTitle, dblEventTime, dblCalcEventTime, dblLastPrimaryOnAirDur, pList->m_ppEvents[n]->m_usStatus, pChannel->m_pszDesc);  //Sleep(50); //(Dispatch message)

										// the ADC event values.

//										NewEvent.m_event.m_usType = pList->m_ppEvents[n]->m_usType; 
										NewEvent.m_event.m_pszID = pList->m_ppEvents[n]->m_pszID; 
										NewEvent.m_event.m_pszTitle = pList->m_ppEvents[n]->m_pszTitle; 
										NewEvent.m_event.m_pszData = pList->m_ppEvents[n]->m_pszData; 
										NewEvent.m_event.m_pszReconcileKey = pList->m_ppEvents[n]->m_pszReconcileKey; 
										NewEvent.m_event.m_ucSegment = pList->m_ppEvents[n]->m_ucSegment; 
										NewEvent.m_event.m_ulOnAirTimeMS = pList->m_ppEvents[n]->m_ulOnAirTimeMS; 
										NewEvent.m_event.m_usOnAirJulianDate = pList->m_ppEvents[n]->m_usOnAirJulianDate; 
										NewEvent.m_event.m_ulDurationMS = pList->m_ppEvents[n]->m_ulDurationMS; 
										NewEvent.m_event.m_usStatus = pList->m_ppEvents[n]->m_usStatus; 
										NewEvent.m_event.m_ulControl = pList->m_ppEvents[n]->m_ulControl; 
										NewEvent.m_event.m_usDevice = pList->m_ppEvents[n]->m_usDevice; 


										if(bPostTimeshifter)
										{
											if(bPostTimeshiftPrimary)
											{
												NewEvent.m_nFlags|=SENTINEL_EVENT_TIMESHIFTER;
											}
											else
											{
												if(pList->m_ppEvents[n]->m_usType&SECONDARYEVENT)
												{
													NewEvent.m_nFlags|=SENTINEL_EVENT_CUTOFFRISK;
												}
												else
												{
													NewEvent.m_nFlags|=SENTINEL_EVENT_TIMESHIFTER;
													bPostTimeshiftPrimary = true;
												}
											}
										}
										
										// check to see if upcounting event.
										if(NewEvent.m_event.m_ulControl&(1<<autoupcount))
										{
											NewEvent.m_nFlags|=SENTINEL_EVENT_UPCOUNT;
											if(!(pList->m_ppEvents[n]->m_usStatus&usHARRISDONE))
											{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "--  event [%s][%s]: non-done UPCOUNTER",pList->m_ppEvents[n]->m_pszID,pList->m_ppEvents[n]->m_pszTitle);

												bPostTimeshifter = true;
											}
										}
										if(NewEvent.m_event.m_ulControl&(1<<manualstart))
										{
											NewEvent.m_nFlags|=SENTINEL_EVENT_MANUAL;
											if(!(pList->m_ppEvents[n]->m_usStatus&usHARRISDONEORPLAY))
											{
												NewEvent.m_nFlags|=SENTINEL_EVENT_TIMESHIFTER; // manual events themselves are timeshifters.
												bPostTimeshiftPrimary = true;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "--  event [%s][%s]: non-done MANUAL",pList->m_ppEvents[n]->m_pszID,pList->m_ppEvents[n]->m_pszTitle);
												bPostTimeshifter = true;
											}
										}
										else
										if(NewEvent.m_event.m_ulControl&(1<<autotimed))
										{
											NewEvent.m_nFlags|=SENTINEL_EVENT_HARD;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "--  event [%s][%s]: HARD START",pList->m_ppEvents[n]->m_pszID,pList->m_ppEvents[n]->m_pszTitle);
										}
										

										NewEvent.m_nPosition=n;  // the Harris list position
										if(NewEvent.m_nPosition>=lastlistdata.lookahead) NewEvent.m_nFlags |= SENTINEL_EVENT_LOOKOUT;

										NewEvent.m_dblTime     = dblEventTime;
										NewEvent.m_dblCalcTime = dblLastCalcEventTime;
										NewEvent.m_nCalcDur = ((pList->m_ppEvents[n]->m_ulDurationMS==TIME_NOT_DEFINED)?0:pList->m_ppEvents[n]->m_ulDurationMS); // have to cut this off if the next event is a hard start

										NewEvent.m_nPrecedingUid = nPrecedingUid;

	// parent info stored in parent obj.
										if(NewEvent.m_event.m_usType&SECONDARYEVENT) 
										{
											NewEvent.m_pParent = pParent;

											if((g_psentinel->m_settings.m_bCommentUsesPrimaryStatus)&&(bPlaying)) // tweak the status
											{
												switch(NewEvent.m_event.m_usType&0xff)
												{
												case SECCOMMENT:
												case SECCOMPILEID:// (Byte)(225)
												case SECAPPFLAG:// (Byte)(226)
												case SECBARTERSPOT:// (Byte)(227)
													{
														if(pParent)
														{
															// comments and other get the status of the parent
															NewEvent.m_event.m_usStatus = pParent->m_event.m_usStatus;
														}
														else
														{
															NewEvent.m_event.m_usStatus = 1; //done. top of playing list with no parent....
														}
													} break;
												default:
													{
													} break;
												}
											}

											// calc refcount.
											if(nNumSec>1)
											{
												int nRef = nLastSecondaryIndex;

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SEARCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "REVERSE", "%d %d",nLastSecondaryIndex, nLastParentPos ); 
												while(nRef>nLastParentPos) // if no last pos, will be -1 and still work, just goes to top of list..
												{// check upwards for copies.
													// go until found or last pri position

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SEARCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "REVERSE", "\n   search: [%s] pos %d", NewEvent.m_event.m_pszID, NewEvent.m_nPosition ); 

													if(pChannel->m_ppevents[nRef]->IsMatch(&NewEvent, dblEventTimeTolerance, 0.0, NULL, SENTINEL_MATCH_TIME))//, (char*)("Reverse Search")))
													{
														// matches a previous event on time, id, etc...
														//inc refcount

														NewEvent.m_nRefCount = pChannel->m_ppevents[nRef]->m_nRefCount+1;
														break;
													}
													else
													{ // keep going
														nRef = pChannel->FindEventByPosition(pChannel->m_ppevents[nRef]->m_nPosition-1);
													}
												}
											}

										}
										else
											NewEvent.m_pParent = &NewEvent;





										// now, let's see if we can find the thing.
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SEARCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "\nFinding event %s pos %d", 	NewEvent.m_event.m_pszID, NewEvent.m_nPosition); // Sleep(50); //(Dispatch message)
										double dblNewTimeOffset=0.0;
										int nTestEventIndex = -1;
										if((!pChannel->m_bKillChannelThread)&&(!g_bKillThread))
										{
											nTestEventIndex = pChannel->FindEvent(&NewEvent, dblEventTimeTolerance, 0.0, &dblNewTimeOffset, ((NewEvent.m_event.m_usType&SECONDARYEVENT)?SENTINEL_MATCH_REF:SENTINEL_MATCH_TIME) );
										}
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SEARCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s event %s at %d", (nTestEventIndex>=0?"FOUND":"DID NOT FIND"),	NewEvent.m_event.m_pszID, nTestEventIndex);  //Sleep(50); //(Dispatch message)

										if(nTestEventIndex>=0)
										{
											pChannel->m_ppevents[nTestEventIndex]->m_nFlags |= SENTINEL_EVENT_MATCHED;
											if(g_psentinel->m_settings.m_bPositionShiftOnChange)
											{
												nCheckPosDiff = pChannel->m_ppevents[nTestEventIndex]->m_nPosition - NewEvent.m_nPosition;
												if(nPosDiff==0)
												{
													if(nCheckPosDiff>0) nPosDiff = nCheckPosDiff; // only go with events shifted up.													
												}
												else
												{
													if(nCheckPosDiff!=nPosDiff) bPosShift = false;
												}
											}

											pEvent = pChannel->m_ppevents[nTestEventIndex];
											nPrecedingUid = pEvent->m_uid;

											// found it.

											// before we do anything, since this is the first event, let's delete the previous events out of the database 
											// but not the array (the subsequent removal process at the end will take those out of the array.

											int nNumToDelete=0;

											if((!g_psentinel->m_settings.m_bDisableTopListPurge)&&(n==0)&&(pChannel->m_ppevents))
											{
												int imn=0;
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "Deleting events of %d events", pChannel->m_nNumEvents);//  Sleep(50); //(Dispatch message)
												while((imn<nTestEventIndex)&&(!pChannel->m_bKillChannelThread)&&(!g_bKillThread))
												{
													if(!(pChannel->m_ppevents[imn]->m_nFlags&SENTINEL_EVENT_DELETED))
													{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "Checking processed %s:  %d", pChannel->m_ppevents[imn]->m_pszEncodedID, (pChannel->m_ppevents[imn]->m_nFlags&SENTINEL_EVENT_PROCESSED));
														if(nNumToDelete==0)
														{
															_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE server_name = '%s' AND server_list = %d AND (itemid = %d",
																g_psentinel->m_settings.m_pszLiveEvents,
																pChannel->m_pszServerName, //server_name
																pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
																pChannel->m_ppevents[imn]->m_uid
															);
														}
														else
														{
															_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s OR itemid = %d", szSQL, pChannel->m_ppevents[imn]->m_uid);
															strcpy(szSQL, szSQLTemp);
														}
														pChannel->m_ppevents[imn]->m_nFlags |= SENTINEL_EVENT_DELETING;
														nNumToDelete++;

														if((nNumToDelete>=g_psentinel->m_settings.m_nMaxDeleteItems)||(imn>=pChannel->m_nNumEvents-1))
														{
															//do it.
															strcat(szSQL, ")");
if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread delete: %s", szSQL); // Sleep(50); //(Dispatch message)
if(!g_psentinel->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
															if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
															{
													//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
															}
															else
															{
																nNumToDelete=imn;
																while(nNumToDelete>=0)
																{
																	if(
																		  (pChannel->m_ppevents[nNumToDelete]->m_nFlags&SENTINEL_EVENT_DELETING)
																		&&(!(pChannel->m_ppevents[nNumToDelete]->m_nFlags&SENTINEL_EVENT_DELETED))
																		)
																		pChannel->m_ppevents[nNumToDelete]->m_nFlags |= SENTINEL_EVENT_DELETED;
																	else 
																		nNumToDelete = 0;
																	nNumToDelete--;
																}
															}
															nNumToDelete = 0;
if(!g_psentinel->m_settings.m_bMultiConnectSQL) LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

															
														}
													}

													imn++;
												}
											}

											if(nNumToDelete>0)
											{
												//finish it.
												strcat(szSQL, ")");
if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread delete: %s", szSQL); // Sleep(50); //(Dispatch message)
if(!g_psentinel->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
												if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
												{
											//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
												}
												else
												{
													nNumToDelete=nTestEventIndex-1;
													while(nNumToDelete>=0)
													{
														if(
																(pChannel->m_ppevents[nNumToDelete]->m_nFlags&SENTINEL_EVENT_DELETING)
															&&(!(pChannel->m_ppevents[nNumToDelete]->m_nFlags&SENTINEL_EVENT_DELETED))
															)
															pChannel->m_ppevents[nNumToDelete]->m_nFlags |= SENTINEL_EVENT_DELETED;
														else 
															nNumToDelete = 0;
														nNumToDelete--;
													}
												}
if(!g_psentinel->m_settings.m_bMultiConnectSQL) LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

												nNumToDelete=0;
											}


											bool bChange = false;
											bool bNonVolatileChange = false;
											int nDiffers = pEvent->IsIdentical(&NewEvent);

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "--  event [%s][%s] differs (%s): 0x%08x",
	pEvent->m_event.m_pszID,pEvent->m_event.m_pszTitle, ((nDiffers == SENTINEL_EVENT_IDENTICAL)?"no":"yes"), nDiffers);

											pEvent->m_nDiffers = nDiffers;
											pEvent->m_nPreviousPosition = pEvent->m_nPosition;  // always re-assign.
											pEvent->m_nPrecedingUid = NewEvent.m_nPrecedingUid;

											if(nDiffers == SENTINEL_EVENT_IDENTICAL)
											{
												if(NewEvent.m_event.m_usType&SECONDARYEVENT)
												{
													nLastSecondaryIndex = nTestEventIndex;

													// need to compare parents 
//													if((NewEvent.m_pParent == NULL) && (m_ppevents[nTestEventIndex]->m_pParent == NULL))  // identical
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "event %s at %d was identical, checking parent", 	NewEvent.m_event.m_pszID, nTestEventIndex); // Sleep(50); //(Dispatch message)

													if((pParent) && (pParent->m_nDiffers!=SENTINEL_EVENT_IDENTICAL))  
													{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "parent event %s at %d was different: 0x%08x", 	pParent->m_event.m_pszID, pParent->m_nPosition, pParent->m_nDiffers); // Sleep(50); //(Dispatch message)
														// deal
														bChange = true; // just trigger the parent changes in the SQL
														if(pParent->m_nDiffers&(SENTINEL_EVENT_DIFFERS_TYPE|SENTINEL_EVENT_DIFFERS_SEG|SENTINEL_EVENT_DIFFERS_ID|SENTINEL_EVENT_DIFFERS_TITLE|SENTINEL_EVENT_DIFFERS_DATA|SENTINEL_EVENT_DIFFERS_RECKEY))
															bNonVolatileChange = true;
													}
/*
													else
													{
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "parent event (%d) was not different (0x%08x)", 	pParent, pParent?pParent->m_nDiffers:0); // Sleep(50); //(Dispatch message)
													}
*/
												}
												else
												{
													dblTimeOffset = dblNewTimeOffset;
													pParent = pEvent; //self.
												}

											}
											else // stuff has changed.
											{
												bChange = true;

												if(nDiffers&(SENTINEL_EVENT_DIFFERS_TYPE|SENTINEL_EVENT_DIFFERS_SEG|SENTINEL_EVENT_DIFFERS_ID|SENTINEL_EVENT_DIFFERS_TITLE|SENTINEL_EVENT_DIFFERS_DATA|SENTINEL_EVENT_DIFFERS_RECKEY))
													bNonVolatileChange = true;

												if(!(NewEvent.m_event.m_usType&SECONDARYEVENT))
												{
													dblTimeOffset = dblNewTimeOffset;
													pParent = pEvent; //self.
												}
												else
												{
													nLastSecondaryIndex = nTestEventIndex;
												}
												// now change all the values.

												nDiffers = SENTINEL_EVENT_DIFFERS_TYPE;
												while((nDiffers<=pEvent->m_nDiffers)&&(!pChannel->m_bKillChannelThread)&&(!g_bKillThread))
												{
													if(pEvent->m_nDiffers&nDiffers)
													{
														// change this item.
														switch(nDiffers)
														{
														case SENTINEL_EVENT_DIFFERS_TYPE://						0x00000001  // event differs on type
															pEvent->m_event.m_usType = NewEvent.m_event.m_usType; break;
														case SENTINEL_EVENT_DIFFERS_SEG://						0x00000002  // event differs on segment
															pEvent->m_event.m_ucSegment = NewEvent.m_event.m_ucSegment; break;
														case SENTINEL_EVENT_DIFFERS_OAT://						0x00000004  // event differs on on air time
															pEvent->m_event.m_ulOnAirTimeMS = NewEvent.m_event.m_ulOnAirTimeMS;
															pEvent->m_dblTime = NewEvent.m_dblTime;
															break;
														case SENTINEL_EVENT_DIFFERS_DATE://						0x00000008  // event differs on julian date
															pEvent->m_event.m_usOnAirJulianDate = NewEvent.m_event.m_usOnAirJulianDate; break;
														case SENTINEL_EVENT_DIFFERS_DUR://						0x00000010  // event differs on duration
															pEvent->m_event.m_ulDurationMS = NewEvent.m_event.m_ulDurationMS; break;
														case SENTINEL_EVENT_DIFFERS_STATUS://					0x00000020  // event differs on status
															pEvent->m_event.m_usStatus = NewEvent.m_event.m_usStatus;
															pEvent->m_nFlags &= ~(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT); //clear it.
															pEvent->m_nFlags |= (NewEvent.m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT)); //fill it.
															pEvent->m_event.m_usDevice = NewEvent.m_event.m_usDevice;  //device status
															
															break;
														case SENTINEL_EVENT_DIFFERS_CTRL://						0x00000040  // event differs on ctrl
															pEvent->m_event.m_ulControl = NewEvent.m_event.m_ulControl; break;
														case SENTINEL_EVENT_DIFFERS_POS://						0x00000080  // event differs on position
															pEvent->m_nPosition = NewEvent.m_nPosition; 
															break;
														case SENTINEL_EVENT_DIFFERS_CALCOAT://				0x00000100  // event differs on calc on air time
															pEvent->m_dblCalcTime = NewEvent.m_dblCalcTime; break;
														case SENTINEL_EVENT_DIFFERS_CALCDUR://				0x00000200  // event differs on calculated duration
															pEvent->m_nCalcDur = NewEvent.m_nCalcDur; break;
														case SENTINEL_EVENT_DIFFERS_ID://							0x00001000  // event differs on ID
															if(pEvent->m_event.m_pszID) free(pEvent->m_event.m_pszID);
															pEvent->m_event.m_pszID = NULL;
															if(pEvent->m_pszEncodedID) free(pEvent->m_pszEncodedID);
															pEvent->m_pszEncodedID = NULL;
															if(pList->m_ppEvents[n]->m_pszID)
															{
																pEvent->m_event.m_pszID = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszID)+1); 
																if(pEvent->m_event.m_pszID) strcpy(pEvent->m_event.m_pszID,pList->m_ppEvents[n]->m_pszID);
																pEvent->m_pszEncodedID = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszID);
															}
															break;
														case SENTINEL_EVENT_DIFFERS_TITLE://					0x00002000  // event differs on TITLE
															if(pEvent->m_event.m_pszTitle) free(pEvent->m_event.m_pszTitle);
															pEvent->m_event.m_pszTitle = NULL;
															if(pEvent->m_pszEncodedTitle) free(pEvent->m_pszEncodedTitle);
															pEvent->m_pszEncodedTitle = NULL;
															if(pList->m_ppEvents[n]->m_pszTitle)
															{
																pEvent->m_event.m_pszTitle = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszTitle)+1); 
																if(pEvent->m_event.m_pszTitle) strcpy(pEvent->m_event.m_pszTitle,pList->m_ppEvents[n]->m_pszTitle);
																pEvent->m_pszEncodedTitle = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszTitle);
															}
															break;
														case SENTINEL_EVENT_DIFFERS_DATA://						0x00004000  // event differs on DATA
															if(pEvent->m_event.m_pszData) free(pEvent->m_event.m_pszData);
															pEvent->m_event.m_pszData = NULL;
															if(pEvent->m_pszEncodedData) free(pEvent->m_pszEncodedData);
															pEvent->m_pszEncodedData = NULL;
															if(pList->m_ppEvents[n]->m_pszData)
															{
																pEvent->m_event.m_pszData = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszData)+1); 
																if(pEvent->m_event.m_pszData) strcpy(pEvent->m_event.m_pszData,pList->m_ppEvents[n]->m_pszData);
																pEvent->m_pszEncodedData = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszData);
															}
															break;
														case SENTINEL_EVENT_DIFFERS_RECKEY://					0x00008000  // event differs on RECKEY
															if(pEvent->m_event.m_pszReconcileKey) free(pEvent->m_event.m_pszReconcileKey);
															pEvent->m_event.m_pszReconcileKey = NULL;
															if(pEvent->m_pszEncodedRecKey) free(pEvent->m_pszEncodedRecKey);
															pEvent->m_pszEncodedRecKey = NULL;
															if(pList->m_ppEvents[n]->m_pszReconcileKey)
															{
																pEvent->m_event.m_pszReconcileKey = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszReconcileKey)+1); 
																if(pEvent->m_event.m_pszReconcileKey) strcpy(pEvent->m_event.m_pszReconcileKey,pList->m_ppEvents[n]->m_pszReconcileKey);
																pEvent->m_pszEncodedRecKey = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszReconcileKey);
															}
															break;
														case SENTINEL_PARENT_DIFFERS_UID://						0x10000000  // event differs on type
															{

															} break;
														}
													}

													(nDiffers<<=1);
												}

											}

											if((bKeyChange)||(bNonVolatileChange))
											{
												bChange = true;
												if((!pChannel->m_bKillChannelThread)&&(!g_bKillThread))
												{
//EnterCriticalSection(pChannel->m_pcritAPI);
													pChannel->AssembleKey(nTestEventIndex, szCurrentKey, systemfrx);
//LeaveCriticalSection(pChannel->m_pcritAPI);
												}
												pEvent->m_nDiffers|=SENTINEL_EVENT_DIFFERS_KEY;
											}
											
											pEvent->m_nFlags|= SENTINEL_EVENT_PROCESSED;

											if((bChange)&&(!pChannel->m_bKillChannelThread)&&(!g_bKillThread))
											{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updating %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID); // Sleep(50); //(Dispatch message)
												pChannel->UpdateEventSQL(nTestEventIndex, pdb,	pdbConn, dblServerTime); // inserts!
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updated %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID); // Sleep(50); //(Dispatch message)
												nUpdates++;
											}
										}
										else
										if((!pChannel->m_bKillChannelThread)&&(!g_bKillThread)) // not there, or we were just killing.
										{
											nPosDiff = 0;
											bPosShift = false; //all have been shifted by the same amount.
											// not there!
											// need to reset the last found thingie 
											pChannel->m_nLastFound = -1;

//											EnterCriticalSection(&g_psentinel->m_data.m_critUID);
//											NewEvent.m_uid = 	g_psentinel->m_data.m_nLastUid;
//											g_psentinel->m_data.m_nLastUid++;
//											LeaveCriticalSection(&g_psentinel->m_data.m_critUID);

											NewEvent.m_uid = g_psentinel->m_data.GetNewUID();

											nPrecedingUid = NewEvent.m_uid;

											// create new object, keys, allocate buffers, encode...
											CSentinelEventObject* pObj = new CSentinelEventObject;
											if(pObj)
											{
												pObj->m_uid = NewEvent.m_uid;
												pObj->m_nPrecedingUid = NewEvent.m_nPrecedingUid;
												pObj->m_event.m_usType = pList->m_ppEvents[n]->m_usType;
												pObj->m_nFlags = (SENTINEL_EVENT_NEW|(NewEvent.m_nFlags&SENTINEL_EVENT_TIMEMASK));
												if(pList->m_ppEvents[n]->m_pszID)
												{
													pObj->m_event.m_pszID = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszID)+1); 
													if(pObj->m_event.m_pszID) strcpy(pObj->m_event.m_pszID,pList->m_ppEvents[n]->m_pszID);
													
												} else pObj->m_event.m_pszID = NULL;

												if(pList->m_ppEvents[n]->m_pszTitle)
												{
													pObj->m_event.m_pszTitle = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszTitle)+1); 
													if(pObj->m_event.m_pszTitle) strcpy(pObj->m_event.m_pszTitle,pList->m_ppEvents[n]->m_pszTitle);
													
												} else pObj->m_event.m_pszTitle = NULL;

												if(pList->m_ppEvents[n]->m_pszData)
												{
													pObj->m_event.m_pszData = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszData)+1); 
													if(pObj->m_event.m_pszData) strcpy(pObj->m_event.m_pszData,pList->m_ppEvents[n]->m_pszData);
													
												} else pObj->m_event.m_pszData = NULL;

												if(pList->m_ppEvents[n]->m_pszReconcileKey)
												{
													pObj->m_event.m_pszReconcileKey = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszReconcileKey)+1); 
													if(pObj->m_event.m_pszReconcileKey) strcpy(pObj->m_event.m_pszReconcileKey,pList->m_ppEvents[n]->m_pszReconcileKey);
													
												} else pObj->m_event.m_pszReconcileKey = NULL;

												pObj->m_event.m_ucSegment = pList->m_ppEvents[n]->m_ucSegment; 
												pObj->m_event.m_ulOnAirTimeMS = pList->m_ppEvents[n]->m_ulOnAirTimeMS; 
												pObj->m_event.m_usOnAirJulianDate = pList->m_ppEvents[n]->m_usOnAirJulianDate; 
												pObj->m_event.m_ulDurationMS = pList->m_ppEvents[n]->m_ulDurationMS; 
												pObj->m_event.m_usStatus = pList->m_ppEvents[n]->m_usStatus; 
												pObj->m_event.m_ulControl = pList->m_ppEvents[n]->m_ulControl; 
												pObj->m_event.m_usDevice = pList->m_ppEvents[n]->m_usDevice; 
										

												pObj->m_nRefCount = NewEvent.m_nRefCount;
												pObj->m_nPosition=n;  // the Harris list position
												pObj->m_nPreviousPosition=n;  // the Harris list position
												if(pObj->m_nPosition>=lastlistdata.lookahead) pObj->m_nFlags |= SENTINEL_EVENT_LOOKOUT;

												pObj->m_dblTime     = dblEventTime;
												pObj->m_dblCalcTime = dblLastCalcEventTime;
												pObj->m_nCalcDur = ((pList->m_ppEvents[n]->m_ulDurationMS==TIME_NOT_DEFINED)?0:pList->m_ppEvents[n]->m_ulDurationMS); // have to cut this off if the next event is a hard start

			// parent info stored in parent obj.
												if(pObj->m_event.m_usType&SECONDARYEVENT)
												{
													pObj->m_pParent = pParent;

													if((g_psentinel->m_settings.m_bCommentUsesPrimaryStatus)&&(bPlaying)) // tweak the status
													{
														switch(NewEvent.m_event.m_usType&0xff)
														{
														case SECCOMMENT:
														case SECCOMPILEID:// (Byte)(225)
														case SECAPPFLAG:// (Byte)(226)
														case SECBARTERSPOT:// (Byte)(227)
															{
																if(pParent)
																{
																	// comments and other get the status of the parent
																	pObj->m_event.m_usStatus = pParent->m_event.m_usStatus;
																}
																else
																{
																	pObj->m_event.m_usStatus = 1; //done. top of playing list with no parent....
																}
															} break;
														default:
															{
															} break;
														}
													}
												}
												else
												{
													pObj->m_pParent = (void*)pObj;
													pParent = pObj;
												}


												pObj->m_pszEncodedID = NULL;
												pObj->m_pszEncodedTitle = NULL;
												pObj->m_pszEncodedRecKey = NULL;
												pObj->m_pszEncodedData = NULL;
												if(pList->m_ppEvents[n]->m_pszID) 
													pObj->m_pszEncodedID = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszID);
												if(pList->m_ppEvents[n]->m_pszTitle) 
													pObj->m_pszEncodedTitle = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszTitle);
												if(pList->m_ppEvents[n]->m_pszData) 
													pObj->m_pszEncodedData = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszData);
												if(pList->m_ppEvents[n]->m_pszReconcileKey) 
													pObj->m_pszEncodedRecKey = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszReconcileKey);

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: inserting %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  //Sleep(50); //(Dispatch message)
												//insert
												nTestEventIndex = -1;
												if((!pChannel->m_bKillChannelThread)&&(!g_bKillThread))
												{
													nTestEventIndex =	pChannel->InsertEvent(pObj, n);
												}
												if(nTestEventIndex>=0)
												{	
													if(pObj->m_event.m_usType&SECONDARYEVENT) nLastSecondaryIndex = nTestEventIndex;

													if((!pChannel->m_bKillChannelThread)&&(!g_bKillThread))
													{
//EnterCriticalSection(pChannel->m_pcritAPI);
														pChannel->AssembleKey(nTestEventIndex, szCurrentKey, systemfrx);
//LeaveCriticalSection(pChannel->m_pcritAPI);
													}
													pObj->m_nFlags|= (SENTINEL_EVENT_ACQUIRED|SENTINEL_EVENT_PROCESSED|SENTINEL_EVENT_ALLOCATED);

													if((g_psentinel->m_settings.m_bUseWorkingSet)&&(pObj->m_pszEncodedKey))
													{
														workingset* pItem = new workingset;
														if(pItem)
														{
															pItem->bAdd = TRUE;
															pItem->pszKey = (char*)malloc(strlen(pObj->m_pszEncodedKey)+1);
															if(pItem->pszKey)
															{
																strcpy(pItem->pszKey, pObj->m_pszEncodedKey);
																if(_beginthread(SentinelWorkingSetAdjustThread, 0, (void*)pItem)==-1)
																{
																	//error.
																	DWORD dw = GetLastError(); 
																	char* pchError;
																	FormatMessage( 
																			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
																			NULL, 
																			dw,
																			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
																			(LPTSTR) &pchError, 0, NULL);

																	if(pchError)
																	{
																		//**MSG
																		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set adjust thread (insert).\r\nError code %d: %s\r\nNumber of changes: %d", dw, pchError, g_psentinel->m_data.m_nNumSetChanges); //(Dispatch message)
																		try{ LocalFree(pchError); } catch(...){}
																	}
																	else
																	{
																		//**MSG
																		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set adjust thread (insert).\r\nError code %d\r\nNumber of changes: %d", dw, g_psentinel->m_data.m_nNumSetChanges); //(Dispatch message)
																	}

																	try{delete pItem;} catch(...){}

																}
															}
															else
															{
																try{delete pItem;} catch(...){}
															}
														}
													}

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updating %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  //Sleep(50); //(Dispatch message)
													if((!pChannel->m_bKillChannelThread)&&(!g_bKillThread)) 
													{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updating %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  //Sleep(50); //(Dispatch message)
														pChannel->UpdateEventSQL(nTestEventIndex, pdb,	pdbConn, dblServerTime); // inserts!
														nUpdates++;
													}
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updated %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  //Sleep(50); //(Dispatch message)

												}
												else //error
												{
													try{delete pObj;} catch(...){}
												}
											}
										}
							
									}//if((pList->m_ppEvents)&&(pList->m_ppEvents[n])&&(pList->m_ppEvents[n]->m_pszID)&&(strlen(pList->m_ppEvents[n]->m_pszID)))
									n++;
//g_psentinel->m_msgr.DM(MSG_ICONUSER1, NULL, "Sentinel:debug", "-> Event %d", n);//  Sleep(50); //(Dispatch message)
								}//while((!pChannel->m_bKillChannelThread)&&(n<nNumEvents)&&(pChannel->m_pAPIConn))//&&((*pbKillConnThread)!=true))

pChannel->m_bChannelThreadPublishingEvents = false;

// dump tables
if(g_psentinel->m_settings.m_bDebugLists)
{
	CRecordset* prs;
	if(g_psentinel->m_settings.m_nDebugListTopCount>0)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top %d * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_nDebugListTopCount,
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}
	else
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}


if(!g_psentinel->m_settings.m_bMultiConnectSQL) 	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
	prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
	if(prs == NULL)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
	}
	else
	{
		EnterCriticalSection(&g_psentinel->m_data.m_critDebug);

		int nIndex = 0;
		CString szOut="\r\nB: EVENT Table:";
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut); // Sleep(20); //(Dispatch message)
		while ((!prs->IsEOF()))
		{
			CString szTemp="";
			szOut = "\t";
			try
			{

				prs->GetFieldValue("itemid", szTemp);  //HARDCODE
				szOut+="itemid="; szOut+=szTemp;
				prs->GetFieldValue("event_key", szTemp);  //HARDCODE
				szOut+="\tevent_key="; szOut+=szTemp;
				prs->GetFieldValue("server_name", szTemp);  //HARDCODE
				szOut+="\tserver_name="; szOut+=szTemp;
				prs->GetFieldValue("server_list", szTemp);  //HARDCODE
				szOut+="\tserver_list="; szOut+=szTemp;
				prs->GetFieldValue("list_id", szTemp);  //HARDCODE
				szOut+="\tlist_id="; szOut+=szTemp;
				prs->GetFieldValue("event_id", szTemp);  //HARDCODE
				szOut+="\tevent_id="; szOut+=szTemp;
				prs->GetFieldValue("event_clip", szTemp);  //HARDCODE
				szOut+="\tevent_clip="; szOut+=szTemp;
				prs->GetFieldValue("event_title", szTemp);  //HARDCODE
				szOut+="\tevent_title="; szOut+=szTemp;
				prs->GetFieldValue("event_data", szTemp);  //HARDCODE
				szOut+="\tevent_data="; szOut+=szTemp;
				prs->GetFieldValue("event_type", szTemp);  //HARDCODE
				szOut+="\tevent_type="; szOut+=szTemp;
				prs->GetFieldValue("event_status", szTemp);  //HARDCODE
				szOut+="\tevent_status="; szOut+=szTemp;
				prs->GetFieldValue("event_time_mode", szTemp);  //HARDCODE
				szOut+="\tevent_time_mode="; szOut+=szTemp;
				prs->GetFieldValue("event_start", szTemp);  //HARDCODE
				szOut+="\tevent_start="; szOut+=szTemp;
				prs->GetFieldValue("event_duration", szTemp);  //HARDCODE
				szOut+="\tevent_duration="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_start", szTemp);  //HARDCODE
				szOut+="\tevent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_end", szTemp);  //HARDCODE
				szOut+="\tevent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("event_position", szTemp);  //HARDCODE
				szOut+="\tevent_position="; szOut+=szTemp;
				prs->GetFieldValue("event_last_update", szTemp);  //HARDCODE
				szOut+="\tevent_last_update="; szOut+=szTemp;
				prs->GetFieldValue("parent_duration", szTemp);  //HARDCODE
				szOut+="\tparent_duration="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_start", szTemp);  //HARDCODE
				szOut+="\tparent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_end", szTemp);  //HARDCODE
				szOut+="\tparent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("parent_position", szTemp);  //HARDCODE
				szOut+="\tparent_position="; szOut+=szTemp;
				prs->GetFieldValue("app_data", szTemp);  //HARDCODE
				szOut+="\tapp_data="; szOut+=szTemp;
				prs->GetFieldValue("app_data_aux", szTemp);  //HARDCODE
				szOut+="\tapp_data_aux="; szOut+=szTemp;
				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(50); //(Dispatch message)
			}
			catch(...)
			{
			}
			prs->MoveNext();
			nIndex++;
		}

		prs->Close();

		delete prs;
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "B: EVENT table: %d items retrieved\r\n", nIndex);//  Sleep(50); //(Dispatch message)
		LeaveCriticalSection(&g_psentinel->m_data.m_critDebug);

	}
if(!g_psentinel->m_settings.m_bMultiConnectSQL) 	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

}





							} //if(nNumEvents!=ADC_NO_PLAYINGEVENT)

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TIMING)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND|((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_DELAY)?MSG_SIGNAL_POSTDELAY_200:0), 
	NULL, "Sentinel:debug", "[%d] %s SentinelChannelThread mid check status change = %d, %s%d%s", nClock, pChannel->m_pszDesc, bStatusChange, bPlaying?"playing event at ":"", nNumEvents, bPlaying?"":" events"); // Sleep(50); //(Dispatch message)
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "after nNumEvents!=ADC_NO_PLAYINGEVENT bStatusChange=%d",bStatusChange); // Sleep(50); //(Dispatch message)
							if((!pChannel->m_bKillChannelThread)&&(!bSQLerror))//&&(!bStatusChange))  // commented out status change here because we want the playing events to be committed.
							{
								if(!bStatusChange) // if bDoNotBreakOnChange, we already got everything
								{
									// we can continue on and get the rest of the items, else break out and re-get
									// then, if no status change, get the rest of the events

									// at this point, we have n = the next event we have to get
									nNumToGet = g_psentinel->m_settings.m_bUseListCount?listdata.listcount:listdata.lookahead;
									if((g_psentinel->m_settings.m_nMaxLookahead>0)&&(g_psentinel->m_settings.m_nMaxLookahead<nNumToGet)) nNumToGet=g_psentinel->m_settings.m_nMaxLookahead;

									while((!pChannel->m_bKillChannelThread)&&(!bStatusChange)&&(n<nNumToGet)&&(!g_bKillThread))
									{
										int nOffset = n;

//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TIMING)	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s SentinelChannelThread 2 getting %d events at %d, of total %d", pChannel->m_pszDesc, (g_psentinel->m_settings.m_ulEventsPerRequest!=0)?(g_psentinel->m_settings.m_ulEventsPerRequest):(nNumToGet-nOffset), nOffset, nNumToGet);  Sleep(50); //(Dispatch message)
										pChannel->m_bChannelThreadGettingEvents = true;
if(pChannel->m_pcritAPI)
{
	EnterCriticalSection(pChannel->m_pcritAPI);
}
										if(pChannel->m_ppAPIConn)
										{
											EnterCriticalSection(&g_adc.m_crit);
												nNumEvents = g_adc.GetEvents((*(pChannel->m_ppAPIConn)),
												pChannel->m_nHarrisListID, 
												nOffset, //start at the offset.
												(g_psentinel->m_settings.m_ulEventsPerRequest!=0)?(g_psentinel->m_settings.m_ulEventsPerRequest):(nNumToGet-nOffset),
												(ADC_GETEVENTS_BREAKONCHANGE|ADC_GETEVENTS_LIST|ADC_GETEVENTS_USEOFFSET)|(g_psentinel->m_settings.m_bUseListCount?ADC_GETEVENTS_FULLLIST:0), //|(g_psentinel->m_settings.m_bUseListCount?(ADC_GETEVENTS_FULLLIST):0),
												ADC_DEFAULT_TIMEOUT,
												&bStatusChange,
												&listdata
												);
											LeaveCriticalSection(&g_adc.m_crit);
										}
										else
										{
											nNumEvents = 0;
										}

if(pChannel->m_pcritAPI)
{
	LeaveCriticalSection(pChannel->m_pcritAPI);
}
										pChannel->m_bChannelThreadGettingEvents = false;

										//////////// at least, let's dump these to the db.

//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TIMING)	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s SentinelChannelThread 2 got %d events", pChannel->m_pszDesc, nNumEvents);  Sleep(50); //(Dispatch message)
pChannel->m_bChannelThreadPublishingEvents = true;

										while(
														(!pChannel->m_bKillChannelThread)&&(!bStatusChange)&&(n<nNumEvents+nOffset)&&(pChannel->m_ppAPIConn)//&&(*(pChannel->m_ppAPIConn))
													&&(!g_bKillThread)
													)//&&((*pbKillConnThread)!=true))
										{

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s SentinelChannelThread event: %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID); // Sleep(50); //(Dispatch message)
											if((pList->m_ppEvents)&&(pList->m_ppEvents[n]))//&&(pList->m_ppEvents[n]->m_pszID)&&(strlen(pList->m_ppEvents[n]->m_pszID)))// cant exclude stuff without ID.
											{

												double dblEventTime = 0.0; 
												double dblCalcEventTime = 0.0; 

										//		if(usOnAirJulianDate == 0xffff)  // can't trust event based julian date, have to use server date
			//									{
			//										_timeb timestamp;
			//										_ftime(&timestamp);

			//										usOnAirJulianDate = pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.

			// move time calc to below julian recalc
			//										dblEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
			//											+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
			//									}
			/*
												else
												{
													dblEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0    // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
														+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
												}
			*/											
			//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s] again", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
/*
												if(usLastPrimaryOnAirJulianDate != 0xffff)
												{
			//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s] again1a", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
													usOnAirJulianDate = usLastPrimaryOnAirJulianDate;
												}
												else
												{
			//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s] again1b", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
													usOnAirJulianDate = pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.
												}
*/
			//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s] again2", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)

												NewEvent.m_event.m_usType = pList->m_ppEvents[n]->m_usType;
												double dblOffsetTime = (pList->m_ppEvents[n]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)?(0.000):((double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0);

												if(NewEvent.m_event.m_usType&SECONDARYEVENT)
												{
													nNumSec++;
													

													switch(NewEvent.m_event.m_usType&0xff)
													{
													case SECBACKAVEVENT:
													case SECBACKTIMEGPI:
													case SECBACKSYSTEM:
														{
															if((dblLastPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
															{
																if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
																{
																	dblEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0 - dblOffsetTime;
																}
																else
																{
																	dblEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0 - dblOffsetTime;
																}
															}
															else
															{
																dblEventTime = dblLastPrimaryEventTime - dblOffsetTime;
															}

															if((dblLastCalcPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
															{
																if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
																{
																	dblCalcEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0 - dblOffsetTime;
																}
																else
																{
																	dblCalcEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0 - dblOffsetTime;
																}
															}
															else
															{
																dblCalcEventTime = dblLastCalcPrimaryEventTime - dblOffsetTime;
															}
														} break;
													case SECCOMMENT:
													case SECCOMPILEID:// (Byte)(225)
													case SECAPPFLAG:// (Byte)(226)
													case SECBARTERSPOT:// (Byte)(227)
													case INVALIDEVENT:
														{
															if((dblLastPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
															{
																if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
																{
																	dblEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0;
																}
																else
																{
																	dblEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0;
																}
															}
															else
															{
																dblEventTime = dblLastPrimaryEventTime;
															}

															if((dblLastCalcPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
															{
																if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
																{
																	dblCalcEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0;
																}
																else
																{
																	dblCalcEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0;
																}
															}
															else
															{
																dblCalcEventTime = dblLastCalcPrimaryEventTime;
															}
															// a comment, for instance, has TIME_NOT_DEFINED. but we want it to come after the primary it belongs to,
															// so we used to give it .001 seconds.  Now we give it the correct actual time, 
															// but sort it on parent_start_calc + (event_position-parent_position)/1000.0, or more correctly
															// order by parent_position<0?((event_position)/1000.0):(parent_calc_start + (event_position-parent_position)/1000.0)
														} break;
													default:
														{
															if((dblLastPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
															{
																if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
																{
																	dblEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0 + dblOffsetTime;
																}
																else
																{
																	dblEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0 + dblOffsetTime;
																}
															}
															else
															{
																dblEventTime = dblLastPrimaryEventTime + dblOffsetTime;
															}

															if((dblLastCalcPrimaryEventTime<0.0)||(ulLastPrimaryOnAirTimeMS==TIME_NOT_DEFINED))
															{
																if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
																{
																	dblCalcEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0 + dblOffsetTime;
																}
																else
																{
																	dblCalcEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0 + dblOffsetTime;
																}
															}
															else
															{
																dblCalcEventTime = dblLastCalcPrimaryEventTime + dblOffsetTime;
															}
														} break;
													}
/*
														usOnAirJulianDate = (usLastPrimaryOnAirJulianDate!=0xffff)?usLastPrimaryOnAirJulianDate:pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.

														if(pList->m_ppEvents[n]->m_ulOnAirTimeMS + ulLastPrimaryOnAirTimeMS >= 86400000 )
														{
															//wrapped days.
															usOnAirJulianDate++;
														}
*/
									
												}
												else // its a primary
												{
													nNumSec=0;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "PRI-event [%s][%s]: gdate:%d edate:%d time=%.03f, calc=%.03f, offset=%.03f on %s ", 
	pList->m_ppEvents[n]->m_pszID, pList->m_ppEvents[n]->m_pszTitle, usOnAirJulianDate, pList->m_ppEvents[n]->m_usOnAirJulianDate, dblEventTime, dblCalcEventTime,dblOffsetTime, pChannel->m_pszDesc);  //Sleep(50); //(Dispatch message)

													if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
													{
														dblEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0 + dblOffsetTime;
													}
													else
													{
		/*
														if(usLastPrimaryOnAirJulianDate!=DATE_NOT_DEFINED)
														{
															usOnAirJulianDate = usLastPrimaryOnAirJulianDate;
														}
														else
														{
														}
		*/

														if(n<listdata.lookahead)
														{
															if(ulLastPrimaryOnAirTimeMS!=TIME_NOT_DEFINED)  // should be the only check on valid last primary.
															{
																if(ulLastPrimaryOnAirTimeMS > pList->m_ppEvents[n]->m_ulOnAirTimeMS)
																{
//	EnterCriticalSection(pChannel->m_pcritAPI);
																	if((pList->m_ppEvents[n]->m_ulControl&usHARRISHARD)&&(ulRefTimeMS<=pList->m_ppEvents[n]->m_ulOnAirTimeMS))
																	{
																		// this is going to cut off anything else but remain the same day.
																	}
																	else
																	{
																		// we wrapped days.
																		usOnAirJulianDate++;
																	}
//	LeaveCriticalSection(pChannel->m_pcritAPI);

																	// OK not necessarily.
																	// another case is that there is a hard time like this:
																	// 10:00:00 something
																	// 10:01:00 something else
																	// 10:00:30 hard time.
																	// something gets cut off, something else misses, and hard time starts today!
																	// this only happens if the current time is < the hard time.  if >, it becomes tomorrow!
																	// ok, so add all the ifs around this to prevent this case from incrementing the day.

																}
															}

															dblEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
																+ dblOffsetTime;
		if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "----event [%s][%s]: gdate:%d edate:%d time=%.03f, calc=%.03f, offset=%.03f on %s ", 
			pList->m_ppEvents[n]->m_pszID, pList->m_ppEvents[n]->m_pszTitle, usOnAirJulianDate, pList->m_ppEvents[n]->m_usOnAirJulianDate, dblEventTime, dblCalcEventTime,dblOffsetTime, pChannel->m_pszDesc);  //Sleep(50); //(Dispatch message)


														}
														else
														{
															// we are beyond the lookahead, need to deal with estimated times, not actual as reported by harris.
															dblEventTime = dblLastPrimaryEventTime + dblLastPrimaryOnAirDur;
															// we dont care about ulLastPrimaryOnAirTimeMS for calculations after this point
															// but we do care about julian day ... so... ok actually we dont care.  that was for calc anyway.
														}
													}

		/*
		enum teventrunstatus {
				 eventdone,
				 eventrunning,
				 playednextvideo,
				 eventprerolled,
				 eventpostrolled,
				 eventidtitle,
				 eventstandbyon,
				 notplayed,
				 eventranshort,
				 eventskipped,
				 eventpreped,
				 eventnotswitched,
				 eventpreviewed,
				 rollingnext,
				 eventshort,
				 eventlong };
		*/

													if(
															(pList->m_ppEvents[n]->m_usStatus&usHARRISDONEORPLAY)
														||(pList->m_ppEvents[n]->m_ulControl&usHARRISHARD)// hard start
														)
													{
														dblCalcEventTime = dblEventTime;
													}
													else
													{
														if(ulLastPrimaryOnAirTimeMS!=TIME_NOT_DEFINED)  // should be the only check on valid last primary.
														{
															if(n<=nTodayRef) // julian date is figured out for top events only. if it is not figured out, nTodayRef = -1.
															{// we got the today ref from the topmost non-done primary.  so we want to use that calc instead of just following on, in case the list was started and stopped a bunch of times and you have done events from the past above.
																dblCalcEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0 + dblOffsetTime;
															}
															else
															{
																dblCalcEventTime = dblLastCalcPrimaryEventTime + dblLastPrimaryOnAirDur;
															}
														}
														else
														{
															dblCalcEventTime = dblEventTime;
														}
													}

													dblLastPrimaryOnAirDur = ((pList->m_ppEvents[n]->m_ulDurationMS==TIME_NOT_DEFINED)?0.0:(double)pList->m_ppEvents[n]->m_ulDurationMS/1000.0);
													ulLastPrimaryOnAirTimeMS = pList->m_ppEvents[n]->m_ulOnAirTimeMS;
													dblLastPrimaryEventTime = dblEventTime;
													usLastPrimaryOnAirJulianDate = usOnAirJulianDate;
													dblLastCalcPrimaryEventTime = dblCalcEventTime;
													nLastParentPos = n;
		//											strcpy(chLastParentID, pList->m_ppEvents[n]->m_pszID);

												}

												dblLastCalcEventTime = dblCalcEventTime;
												dblLastEventTime = dblEventTime;
		//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s] again3", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  //Sleep(50); //(Dispatch message)
		//g_psentinel->m_msgr.DM(MSG_ICONUSER1, NULL, "Sentinel:debug", "before encode -> Event %d", n);//  Sleep(50); //(Dispatch message)


												// the ADC event values.

		//										NewEvent.m_event.m_usType = pList->m_ppEvents[n]->m_usType; 
												NewEvent.m_event.m_pszID = pList->m_ppEvents[n]->m_pszID; 
												NewEvent.m_event.m_pszTitle = pList->m_ppEvents[n]->m_pszTitle; 
												NewEvent.m_event.m_pszData = pList->m_ppEvents[n]->m_pszData; 
												NewEvent.m_event.m_pszReconcileKey = pList->m_ppEvents[n]->m_pszReconcileKey; 
												NewEvent.m_event.m_ucSegment = pList->m_ppEvents[n]->m_ucSegment; 
												NewEvent.m_event.m_ulOnAirTimeMS = pList->m_ppEvents[n]->m_ulOnAirTimeMS; 
												NewEvent.m_event.m_usOnAirJulianDate = pList->m_ppEvents[n]->m_usOnAirJulianDate; 
												NewEvent.m_event.m_ulDurationMS = pList->m_ppEvents[n]->m_ulDurationMS; 


												NewEvent.m_event.m_usStatus = pList->m_ppEvents[n]->m_usStatus; 
												NewEvent.m_event.m_ulControl = pList->m_ppEvents[n]->m_ulControl; 
												NewEvent.m_event.m_usDevice = pList->m_ppEvents[n]->m_usDevice; 

												if(bPostTimeshifter)
												{
													if(bPostTimeshiftPrimary)
													{
														NewEvent.m_nFlags|=SENTINEL_EVENT_TIMESHIFTER;
													}
													else
													{
														if(pList->m_ppEvents[n]->m_usType&SECONDARYEVENT)
														{
															NewEvent.m_nFlags|=SENTINEL_EVENT_CUTOFFRISK;
														}
														else
														{
															NewEvent.m_nFlags|=SENTINEL_EVENT_TIMESHIFTER;
															bPostTimeshiftPrimary = true;
														}
													}
												}
												
												// check to see if upcounting event.
												if(NewEvent.m_event.m_ulControl&(1<<autoupcount))
												{
													NewEvent.m_nFlags|=SENTINEL_EVENT_UPCOUNT;
													if(!(pList->m_ppEvents[n]->m_usStatus&usHARRISDONE))
													{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "--  event [%s][%s]: non-done UPCOUNTER",pList->m_ppEvents[n]->m_pszID,pList->m_ppEvents[n]->m_pszTitle);
														bPostTimeshifter = true;
													}
												}
												if(NewEvent.m_event.m_ulControl&(1<<manualstart))
												{
													NewEvent.m_nFlags|=SENTINEL_EVENT_MANUAL;
													if(!(pList->m_ppEvents[n]->m_usStatus&usHARRISDONEORPLAY))
													{
														NewEvent.m_nFlags|=SENTINEL_EVENT_TIMESHIFTER; // manual events themselves are timeshifters.
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "--  event [%s][%s]: non-done MANUAL",pList->m_ppEvents[n]->m_pszID,pList->m_ppEvents[n]->m_pszTitle);
														bPostTimeshifter = true;
													}
												}
												else
												if(NewEvent.m_event.m_ulControl&(1<<autotimed))
												{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "--  event [%s][%s]: HARD START",pList->m_ppEvents[n]->m_pszID,pList->m_ppEvents[n]->m_pszTitle);
													NewEvent.m_nFlags|=SENTINEL_EVENT_HARD;
												}

												NewEvent.m_nPosition=n;  // the Harris list position
												if(NewEvent.m_nPosition>=lastlistdata.lookahead) NewEvent.m_nFlags |= SENTINEL_EVENT_LOOKOUT;

												NewEvent.m_dblTime     = dblEventTime;
												NewEvent.m_dblCalcTime = dblLastCalcEventTime;
												NewEvent.m_nCalcDur = ((pList->m_ppEvents[n]->m_ulDurationMS==TIME_NOT_DEFINED)?0:pList->m_ppEvents[n]->m_ulDurationMS); // have to cut this off if the next event is a hard start

												NewEvent.m_nPrecedingUid= nPrecedingUid;

			// parent info stored in parent obj.
												if(NewEvent.m_event.m_usType&SECONDARYEVENT) 
												{
													NewEvent.m_pParent = pParent;

													if((g_psentinel->m_settings.m_bCommentUsesPrimaryStatus)&&(bPlaying)) // tweak the status
													{
														switch(NewEvent.m_event.m_usType&0xff)
														{
														case SECCOMMENT:
														case SECCOMPILEID:// (Byte)(225)
														case SECAPPFLAG:// (Byte)(226)
														case SECBARTERSPOT:// (Byte)(227)
															{
																if(pParent)
																{
																	// comments and other get the status of the parent
																	NewEvent.m_event.m_usStatus = pParent->m_event.m_usStatus;
																}
																else
																{
																	NewEvent.m_event.m_usStatus = 1; //done. top of playing list with no parent....
																}
															} break;
														default:
															{
															} break;
														}
													}
													// calc refcount.
													if(nNumSec>1)
													{
														int nRef = nLastSecondaryIndex;
														while(nRef>nLastParentPos) // if no last pos, will be -1 and still work, just goes to top of list..
														{// check upwards for copies.
															// go until found or last pri position
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SEARCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "REVERSE", "\n   search2: [%s] pos %d", NewEvent.m_event.m_pszID, NewEvent.m_nPosition ); 
															if(pChannel->m_ppevents[nRef]->IsMatch(&NewEvent, dblEventTimeTolerance, 0.0, NULL, SENTINEL_MATCH_TIME))//, (char*)("Reverse Search")))
															{
																// matches a previous event on time, id, etc...
																//inc refcount
																NewEvent.m_nRefCount = pChannel->m_ppevents[nRef]->m_nRefCount+1;
																break;
															}
															else
															{ // keep going
																nRef = pChannel->FindEventByPosition(pChannel->m_ppevents[nRef]->m_nPosition);
															}
														}
													}

												}
												else
													NewEvent.m_pParent = &NewEvent;

												// now, let's see if we can find the thing.
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SEARCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "\n2: Finding event %s pos %d", 	NewEvent.m_event.m_pszID, NewEvent.m_nPosition); // Sleep(50); //(Dispatch message)
												double dblNewTimeOffset=0.0;
												int nTestEventIndex = -1;
												if((!pChannel->m_bKillChannelThread)&&(!g_bKillThread))
												{
													pChannel->FindEvent(&NewEvent, dblEventTimeTolerance, 0.0, &dblNewTimeOffset, ((NewEvent.m_event.m_usType&SECONDARYEVENT)?SENTINEL_MATCH_REF:SENTINEL_MATCH_TIME));
												}
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SEARCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s event %s at %d", (nTestEventIndex>=0?"FOUND":"DID NOT FIND"),	NewEvent.m_event.m_pszID, nTestEventIndex);  //Sleep(50); //(Dispatch message)
												if(nTestEventIndex>=0)
												{
													pChannel->m_ppevents[nTestEventIndex]->m_nFlags |= SENTINEL_EVENT_MATCHED;
													if(g_psentinel->m_settings.m_bPositionShiftOnChange)
													{
														nCheckPosDiff = pChannel->m_ppevents[nTestEventIndex]->m_nPosition - NewEvent.m_nPosition;
														if(nPosDiff==0)
														{
															if(nCheckPosDiff>0) nPosDiff = nCheckPosDiff; // only go with events shifted up.													
														}
														else
														{
															if(nCheckPosDiff!=nPosDiff) bPosShift = false;
														}
													}
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "pEvent is %s", 	pEvent?"not null":"null");  Sleep(50); //(Dispatch message)
													pEvent = pChannel->m_ppevents[nTestEventIndex];
													nPrecedingUid = pEvent->m_uid;
													// found it.




													// before we do anything, since this is the first event, let's delete the previous events out of the database 
													// but not the array (the subsequent removal process at the end will take those out of the array.

													int nNumToDelete=0;
		
													if((!g_psentinel->m_settings.m_bDisableTopListPurge)&&(n==0)&&(pChannel->m_ppevents))
													{
														int imn=0;
				//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "Deleting events of %d events", pChannel->m_nNumEvents);//  Sleep(50); //(Dispatch message)
														while((imn<nTestEventIndex)&&(!pChannel->m_bKillChannelThread)&&(!g_bKillThread))
														{
				//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "Checking processed %s:  %d", pChannel->m_ppevents[imn]->m_pszEncodedID, (pChannel->m_ppevents[imn]->m_nFlags&SENTINEL_EVENT_PROCESSED));
															if(!(pChannel->m_ppevents[imn]->m_nFlags&SENTINEL_EVENT_DELETED))
															{
																if(nNumToDelete==0)
																{
																	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE server_name = '%s' AND server_list = %d AND (itemid = %d",
																		g_psentinel->m_settings.m_pszLiveEvents,
																		pChannel->m_pszServerName, //server_name
																		pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
																		pChannel->m_ppevents[imn]->m_uid
																	);
																}
																else
																{
																	_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s OR itemid = %d", szSQL, pChannel->m_ppevents[imn]->m_uid);
																	strcpy(szSQL, szSQLTemp);
																}
																pChannel->m_ppevents[imn]->m_nFlags |= SENTINEL_EVENT_DELETING;
																nNumToDelete++;

																if((nNumToDelete>=g_psentinel->m_settings.m_nMaxDeleteItems)||(imn>=pChannel->m_nNumEvents-1))
																{
																	//do it.
																	strcat(szSQL, ")");
if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread delete: %s", szSQL); // Sleep(50); //(Dispatch message)
if(!g_psentinel->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
																	if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
																	{
																//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
																	}
																	else
																	{
																		nNumToDelete=imn;
																		while(nNumToDelete>=0)
																		{
																			if(
																					(pChannel->m_ppevents[nNumToDelete]->m_nFlags&SENTINEL_EVENT_DELETING)
																				&&(!(pChannel->m_ppevents[nNumToDelete]->m_nFlags&SENTINEL_EVENT_DELETED))
																				)
																				pChannel->m_ppevents[nNumToDelete]->m_nFlags |= SENTINEL_EVENT_DELETED;
																			else 
																				nNumToDelete = 0;
																			nNumToDelete--;
																		}
																	}
if(!g_psentinel->m_settings.m_bMultiConnectSQL) LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

																	nNumToDelete=0;
																}
															}

															imn++;
														}
													}

													if(nNumToDelete>0)
													{
														//finish it.
														strcat(szSQL, ")");
if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread delete: %s", szSQL); // Sleep(50); //(Dispatch message)
if(!g_psentinel->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
														if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
														{
													//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
														}
														else
														{
															nNumToDelete=nTestEventIndex-1;
															while(nNumToDelete>=0)
															{
																if(
																		(pChannel->m_ppevents[nNumToDelete]->m_nFlags&SENTINEL_EVENT_DELETING)
																	&&(!(pChannel->m_ppevents[nNumToDelete]->m_nFlags&SENTINEL_EVENT_DELETED))
																	)
																	pChannel->m_ppevents[nNumToDelete]->m_nFlags |= SENTINEL_EVENT_DELETED;
																else 
																	nNumToDelete = 0;
																nNumToDelete--;
															}
														}
if(!g_psentinel->m_settings.m_bMultiConnectSQL) LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

														nNumToDelete=0;
													}









													bool bChange = false;
													bool bNonVolatileChange = false;
													int nDiffers = pEvent->IsIdentical(&NewEvent);

													pEvent->m_nDiffers = nDiffers;
													pEvent->m_nPreviousPosition = pEvent->m_nPosition;  // always re-assign.
													pEvent->m_nPrecedingUid = NewEvent.m_nPrecedingUid;


//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "nDiffers = 0x%08x", 	nDiffers);  Sleep(50); //(Dispatch message)

													if(nDiffers == SENTINEL_EVENT_IDENTICAL)
													{
														if(NewEvent.m_event.m_usType&SECONDARYEVENT)
														{
															// need to compare parents 
		//													if((NewEvent.m_pParent == NULL) && (m_ppevents[nTestEventIndex]->m_pParent == NULL))  // identical

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "event %s at %d was identical, checking parent", 	NewEvent.m_event.m_pszID, nTestEventIndex); // Sleep(50); //(Dispatch message)

															if((pParent) && (pParent->m_nDiffers!=SENTINEL_EVENT_IDENTICAL))  
															{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "parent event %s at %d was different: 0x%08x", 	pParent->m_event.m_pszID, pParent->m_nPosition, pParent->m_nDiffers); // Sleep(50); //(Dispatch message)
																// deal
																bChange = true; // just trigger the parent changes in the SQL
																if(pParent->m_nDiffers&(SENTINEL_EVENT_DIFFERS_TYPE|SENTINEL_EVENT_DIFFERS_SEG|SENTINEL_EVENT_DIFFERS_ID|SENTINEL_EVENT_DIFFERS_TITLE|SENTINEL_EVENT_DIFFERS_DATA|SENTINEL_EVENT_DIFFERS_RECKEY))
																	bNonVolatileChange = true;
															}
/*
															else
															{
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "parent event (%d) was not different (0x%08x)", 	pParent, pParent?pParent->m_nDiffers:0); // Sleep(50); //(Dispatch message)
															}
*/
															nLastSecondaryIndex = nTestEventIndex;
														}
														else
														{
															dblTimeOffset = dblNewTimeOffset;
															pParent = pEvent;
														}
													}
													else // stuff has changed.
													{
														bChange = true;

														if(nDiffers&(SENTINEL_EVENT_DIFFERS_TYPE|SENTINEL_EVENT_DIFFERS_SEG|SENTINEL_EVENT_DIFFERS_ID|SENTINEL_EVENT_DIFFERS_TITLE|SENTINEL_EVENT_DIFFERS_DATA|SENTINEL_EVENT_DIFFERS_RECKEY))
															bNonVolatileChange = true;

														if(!(NewEvent.m_event.m_usType&SECONDARYEVENT))
														{
															dblTimeOffset = dblNewTimeOffset;
															pParent = pEvent; //self.
														}
														else
														{
															nLastSecondaryIndex = nTestEventIndex;
														}
														// now change all the values.

														nDiffers = SENTINEL_EVENT_DIFFERS_TYPE;
														while(nDiffers<=pEvent->m_nDiffers)
														{
															if(pEvent->m_nDiffers&nDiffers)
															{
																// change this item.
																switch(nDiffers)
																{
																case SENTINEL_EVENT_DIFFERS_TYPE://						0x00000001  // event differs on type
																	pEvent->m_event.m_usType = NewEvent.m_event.m_usType; break;
																case SENTINEL_EVENT_DIFFERS_SEG://						0x00000002  // event differs on segment
																	pEvent->m_event.m_ucSegment = NewEvent.m_event.m_ucSegment; break;
																case SENTINEL_EVENT_DIFFERS_OAT://						0x00000004  // event differs on on air time
																	pEvent->m_event.m_ulOnAirTimeMS = NewEvent.m_event.m_ulOnAirTimeMS;
																	pEvent->m_dblTime = NewEvent.m_dblTime;
																	break;
																case SENTINEL_EVENT_DIFFERS_DATE://						0x00000008  // event differs on julian date
																	pEvent->m_event.m_usOnAirJulianDate = NewEvent.m_event.m_usOnAirJulianDate; break;
																case SENTINEL_EVENT_DIFFERS_DUR://						0x00000010  // event differs on duration
																	pEvent->m_event.m_ulDurationMS = NewEvent.m_event.m_ulDurationMS; break;
																case SENTINEL_EVENT_DIFFERS_STATUS://					0x00000020  // event differs on status
																	pEvent->m_event.m_usStatus = NewEvent.m_event.m_usStatus; 
																	pEvent->m_nFlags &= ~(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT); //clear it.
																	pEvent->m_nFlags |= (NewEvent.m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT)); //fill it.
																	pEvent->m_event.m_usDevice = NewEvent.m_event.m_usDevice;  //reset device status
																case SENTINEL_EVENT_DIFFERS_CTRL://						0x00000040  // event differs on ctrl
																	pEvent->m_event.m_ulControl = NewEvent.m_event.m_ulControl; break;
																case SENTINEL_EVENT_DIFFERS_POS://						0x00000080  // event differs on position
																	pEvent->m_nPosition = NewEvent.m_nPosition; 
																	break;
																case SENTINEL_EVENT_DIFFERS_CALCOAT://				0x00000100  // event differs on calc on air time
																	pEvent->m_dblCalcTime = NewEvent.m_dblCalcTime; break;
																case SENTINEL_EVENT_DIFFERS_CALCDUR://				0x00000200  // event differs on calculated duration
																	pEvent->m_nCalcDur = NewEvent.m_nCalcDur; break;
																case SENTINEL_EVENT_DIFFERS_ID://							0x00001000  // event differs on ID
																	if(pEvent->m_event.m_pszID) free(pEvent->m_event.m_pszID);
																	pEvent->m_event.m_pszID = NULL;
																	if(pEvent->m_pszEncodedID) free(pEvent->m_pszEncodedID);
																	pEvent->m_pszEncodedID = NULL;
																	if(pList->m_ppEvents[n]->m_pszID)
																	{
																		pEvent->m_event.m_pszID = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszID)+1); 
																		if(pEvent->m_event.m_pszID) strcpy(pEvent->m_event.m_pszID,pList->m_ppEvents[n]->m_pszID);
																		pEvent->m_pszEncodedID = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszID);
																	}
																	break;
																case SENTINEL_EVENT_DIFFERS_TITLE://					0x00002000  // event differs on TITLE
																	if(pEvent->m_event.m_pszTitle) free(pEvent->m_event.m_pszTitle);
																	pEvent->m_event.m_pszTitle = NULL;
																	if(pEvent->m_pszEncodedTitle) free(pEvent->m_pszEncodedTitle);
																	pEvent->m_pszEncodedTitle = NULL;
																	if(pList->m_ppEvents[n]->m_pszTitle)
																	{
																		pEvent->m_event.m_pszTitle = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszTitle)+1); 
																		if(pEvent->m_event.m_pszTitle) strcpy(pEvent->m_event.m_pszTitle,pList->m_ppEvents[n]->m_pszTitle);
																		pEvent->m_pszEncodedTitle = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszTitle);
																	}
																	break;
																case SENTINEL_EVENT_DIFFERS_DATA://						0x00004000  // event differs on DATA
																	if(pEvent->m_event.m_pszData) free(pEvent->m_event.m_pszData);
																	pEvent->m_event.m_pszData = NULL;
																	if(pEvent->m_pszEncodedData) free(pEvent->m_pszEncodedData);
																	pEvent->m_pszEncodedData = NULL;
																	if(pList->m_ppEvents[n]->m_pszData)
																	{
																		pEvent->m_event.m_pszData = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszData)+1); 
																		if(pEvent->m_event.m_pszData) strcpy(pEvent->m_event.m_pszData,pList->m_ppEvents[n]->m_pszData);
																		pEvent->m_pszEncodedData = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszData);
																	}
																	break;
																case SENTINEL_EVENT_DIFFERS_RECKEY://					0x00008000  // event differs on RECKEY
																	if(pEvent->m_event.m_pszReconcileKey) free(pEvent->m_event.m_pszReconcileKey);
																	pEvent->m_event.m_pszReconcileKey = NULL;
																	if(pEvent->m_pszEncodedRecKey) free(pEvent->m_pszEncodedRecKey);
																	pEvent->m_pszEncodedRecKey = NULL;
																	if(pList->m_ppEvents[n]->m_pszReconcileKey)
																	{
																		pEvent->m_event.m_pszReconcileKey = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszReconcileKey)+1); 
																		if(pEvent->m_event.m_pszReconcileKey) strcpy(pEvent->m_event.m_pszReconcileKey,pList->m_ppEvents[n]->m_pszReconcileKey);
																		pEvent->m_pszEncodedRecKey = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszReconcileKey);
																	}
																	break;
																case SENTINEL_PARENT_DIFFERS_UID://						0x10000000  // event differs on type
																	{

																	} break;
																}
															}

															(nDiffers<<=1);
														}

													}

													if((bKeyChange)||(bNonVolatileChange))
													{
														bChange = true;
														if((!pChannel->m_bKillChannelThread)&&(!g_bKillThread))
														{
//EnterCriticalSection(pChannel->m_pcritAPI);
															pChannel->AssembleKey(nTestEventIndex, szCurrentKey, systemfrx);
//LeaveCriticalSection(pChannel->m_pcritAPI);
														}
														pEvent->m_nDiffers|=SENTINEL_EVENT_DIFFERS_KEY;
													}
													
													pEvent->m_nFlags|= SENTINEL_EVENT_PROCESSED;

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "updating event %s", 	pEvent->m_event.m_pszID);  Sleep(50); //(Dispatch message)
													if((bChange)&&(!pChannel->m_bKillChannelThread)&&(!g_bKillThread))
													{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updating %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID); // Sleep(50); //(Dispatch message)
														pChannel->UpdateEventSQL(nTestEventIndex, pdb,	pdbConn, dblServerTime); // inserts!
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updated %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID); // Sleep(50); //(Dispatch message)
														nUpdates++;
													}

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "updated event %s", 	pEvent->m_event.m_pszID);  Sleep(50); //(Dispatch message)
												}
												else
												if((!pChannel->m_bKillChannelThread)&&(!g_bKillThread)) // not there, or we were just killing
												{
													nPosDiff = 0;
													bPosShift = false; //all have been shifted by the same amount.
													// not there!
													// need to reset the last found thingie 
													pChannel->m_nLastFound = -1;

//													EnterCriticalSection(&g_psentinel->m_data.m_critUID);
//													NewEvent.m_uid = 	g_psentinel->m_data.m_nLastUid;
//													g_psentinel->m_data.m_nLastUid++;
//													LeaveCriticalSection(&g_psentinel->m_data.m_critUID);

													NewEvent.m_uid = g_psentinel->m_data.GetNewUID();
													nPrecedingUid = NewEvent.m_uid;

													// create new object, keys, allocate buffers, encode...
													CSentinelEventObject* pObj = new CSentinelEventObject;
													if(pObj)
													{
														pObj->m_uid = NewEvent.m_uid;
														pObj->m_nPrecedingUid = NewEvent.m_nPrecedingUid;
														pObj->m_event.m_usType = pList->m_ppEvents[n]->m_usType;
														pObj->m_nFlags = (SENTINEL_EVENT_NEW|(NewEvent.m_nFlags&SENTINEL_EVENT_TIMEMASK));
														if(pList->m_ppEvents[n]->m_pszID)
														{
															pObj->m_event.m_pszID = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszID)+1); 
															if(pObj->m_event.m_pszID) strcpy(pObj->m_event.m_pszID,pList->m_ppEvents[n]->m_pszID);
															
														} else pObj->m_event.m_pszID = NULL;

														if(pList->m_ppEvents[n]->m_pszTitle)
														{
															pObj->m_event.m_pszTitle = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszTitle)+1); 
															if(pObj->m_event.m_pszTitle) strcpy(pObj->m_event.m_pszTitle,pList->m_ppEvents[n]->m_pszTitle);
															
														} else pObj->m_event.m_pszTitle = NULL;

														if(pList->m_ppEvents[n]->m_pszData)
														{
															pObj->m_event.m_pszData = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszData)+1); 
															if(pObj->m_event.m_pszData) strcpy(pObj->m_event.m_pszData,pList->m_ppEvents[n]->m_pszData);
															
														} else pObj->m_event.m_pszData = NULL;

														if(pList->m_ppEvents[n]->m_pszReconcileKey)
														{
															pObj->m_event.m_pszReconcileKey = (char*)malloc(strlen(pList->m_ppEvents[n]->m_pszReconcileKey)+1); 
															if(pObj->m_event.m_pszReconcileKey) strcpy(pObj->m_event.m_pszReconcileKey,pList->m_ppEvents[n]->m_pszReconcileKey);
															
														} else pObj->m_event.m_pszReconcileKey = NULL;

														pObj->m_event.m_ucSegment = pList->m_ppEvents[n]->m_ucSegment; 
														pObj->m_event.m_ulOnAirTimeMS = pList->m_ppEvents[n]->m_ulOnAirTimeMS; 
														pObj->m_event.m_usOnAirJulianDate = pList->m_ppEvents[n]->m_usOnAirJulianDate; 
														pObj->m_event.m_ulDurationMS = pList->m_ppEvents[n]->m_ulDurationMS; 
														pObj->m_event.m_usStatus = pList->m_ppEvents[n]->m_usStatus; 
														pObj->m_event.m_ulControl = pList->m_ppEvents[n]->m_ulControl; 
														pObj->m_event.m_usDevice = pList->m_ppEvents[n]->m_usDevice; 
												
														pObj->m_nRefCount = NewEvent.m_nRefCount;
														pObj->m_nPosition=n;  // the Harris list position
														pObj->m_nPreviousPosition=n;  // the Harris list position
														pObj->m_dblTime     = dblEventTime;
														pObj->m_dblCalcTime = dblLastCalcEventTime;
														pObj->m_nCalcDur = ((pList->m_ppEvents[n]->m_ulDurationMS==TIME_NOT_DEFINED)?0:pList->m_ppEvents[n]->m_ulDurationMS); // have to cut this off if the next event is a hard start

					// parent info stored in parent obj.
														if(pObj->m_event.m_usType&SECONDARYEVENT)
														{
															pObj->m_pParent = pParent;

															if((g_psentinel->m_settings.m_bCommentUsesPrimaryStatus)&&(bPlaying)) // tweak the status
															{
																switch(NewEvent.m_event.m_usType&0xff)
																{
																case SECCOMMENT:
																case SECCOMPILEID:// (Byte)(225)
																case SECAPPFLAG:// (Byte)(226)
																case SECBARTERSPOT:// (Byte)(227)
																	{
																		if(pParent)
																		{
																			// comments and other get the status of the parent
																			pObj->m_event.m_usStatus = pParent->m_event.m_usStatus;
																		}
																		else
																		{
																			pObj->m_event.m_usStatus = 1; //done. top of playing list with no parent....
																		}
																	} break;
																default:
																	{
																	} break;
																}
															}
														}
														else
														{
															pObj->m_pParent = (void*)pObj;
															pParent = pObj;
														}

														pObj->m_pszEncodedID = NULL;
														pObj->m_pszEncodedTitle = NULL;
														pObj->m_pszEncodedRecKey = NULL;
														pObj->m_pszEncodedData = NULL;
														if(pList->m_ppEvents[n]->m_pszID) 
															pObj->m_pszEncodedID = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszID);
														if(pList->m_ppEvents[n]->m_pszTitle) 
															pObj->m_pszEncodedTitle = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszTitle);
														if(pList->m_ppEvents[n]->m_pszData) 
															pObj->m_pszEncodedData = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszData);
														if(pList->m_ppEvents[n]->m_pszReconcileKey) 
															pObj->m_pszEncodedRecKey = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszReconcileKey);

														//insert
														nTestEventIndex = -1;
														if((!pChannel->m_bKillChannelThread)&&(!g_bKillThread))
														{
															nTestEventIndex = pChannel->InsertEvent(pObj, n);
														}
														if(nTestEventIndex>=0)
														{	
															if(pObj->m_event.m_usType&SECONDARYEVENT) nLastSecondaryIndex = nTestEventIndex;

															if((!pChannel->m_bKillChannelThread)&&(!g_bKillThread))
															{
//EnterCriticalSection(pChannel->m_pcritAPI);
																pChannel->AssembleKey(nTestEventIndex, szCurrentKey, systemfrx);
//LeaveCriticalSection(pChannel->m_pcritAPI);
															}
															pObj->m_nFlags|= (SENTINEL_EVENT_ACQUIRED|SENTINEL_EVENT_PROCESSED|SENTINEL_EVENT_ALLOCATED);


															if((g_psentinel->m_settings.m_bUseWorkingSet)&&(pObj->m_pszEncodedKey))
															{
																workingset* pItem = new workingset;
																if(pItem)
																{
																	pItem->bAdd = TRUE;
																	pItem->pszKey = (char*)malloc(strlen(pObj->m_pszEncodedKey)+1);
																	if(pItem->pszKey)
																	{
																		strcpy(pItem->pszKey, pObj->m_pszEncodedKey);
																		if(_beginthread(SentinelWorkingSetAdjustThread, 0, (void*)pItem)==-1)
																		{
																			//error.
																			//error.
																			DWORD dw = GetLastError(); 
																			char* pchError;
																			FormatMessage( 
																					FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
																					NULL, 
																					dw,
																					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
																					(LPTSTR) &pchError, 0, NULL);

																			if(pchError)
																			{
																				//**MSG
																				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set adjust thread.\r\nError code %d: %s\r\nNumber of changes: %d", dw, pchError, g_psentinel->m_data.m_nNumSetChanges); //(Dispatch message)
																				try{ LocalFree(pchError); } catch(...){}
																			}
																			else
																			{
																				//**MSG
																				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set adjust thread.\r\nError code %d\r\nNumber of changes: %d", dw, g_psentinel->m_data.m_nNumSetChanges); //(Dispatch message)
																			}

																			try{delete pItem;} catch(...){}

																		}
																	}
																	else
																	{
																		try{delete pItem;} catch(...){}
																	}
																}
															}


//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updating %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID); // Sleep(50); //(Dispatch message)
															if((!pChannel->m_bKillChannelThread)&&(!g_bKillThread))
															{
																pChannel->UpdateEventSQL(nTestEventIndex, pdb,	pdbConn, dblServerTime); // inserts!
																nUpdates++;
															}
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updated %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  // Sleep(50); //(Dispatch message)
														}
														else //error
														{
															try{delete pObj;} catch(...){}
														}
													}
												}


///////////xcxc

											
											}//if((pList->m_ppEvents)&&(pList->m_ppEvents[n])&&(pList->m_ppEvents[n]->m_pszID)&&(strlen(pList->m_ppEvents[n]->m_pszID)))
											n++;
										}//while((!pChannel->m_bKillChannelThread)&&(n<nNumEvents)&&(pChannel->m_pAPIConn))//&&((*pbKillConnThread)!=true))
pChannel->m_bChannelThreadPublishingEvents = false;

									} //	while((!pChannel->m_bKillChannelThread)&&(!bStatusChange)&&(n<nNumToGet))
//									pChannel->m_bChannelThreadGettingEvents = false;

								} //if(!bStatusChange)

								// update what we got so far
//CString bar;
// dump tables
if(g_psentinel->m_settings.m_bDebugLists)
{
	CRecordset* prs;
	if(g_psentinel->m_settings.m_nDebugListTopCount>0)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top %d * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_nDebugListTopCount,
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}
	else
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}


if(!g_psentinel->m_settings.m_bMultiConnectSQL) 	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
	prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
	if(prs == NULL)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
	}
	else
	{
		EnterCriticalSection(&g_psentinel->m_data.m_critDebug);
		int nIndex = 0;
		CString szOut="\r\nC: EVENT Table:";
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(20); //(Dispatch message)
		while ((!prs->IsEOF()))
		{
			CString szTemp="";
			szOut = "     ";
			try
			{

				prs->GetFieldValue("itemid", szTemp);  //HARDCODE
				szOut+="itemid="; szOut+=szTemp;
				prs->GetFieldValue("event_key", szTemp);  //HARDCODE
				szOut+="\tevent_key="; szOut+=szTemp;
				prs->GetFieldValue("server_name", szTemp);  //HARDCODE
				szOut+="\tserver_name="; szOut+=szTemp;
				prs->GetFieldValue("server_list", szTemp);  //HARDCODE
				szOut+="\tserver_list="; szOut+=szTemp;
				prs->GetFieldValue("list_id", szTemp);  //HARDCODE
				szOut+="\tlist_id="; szOut+=szTemp;
				prs->GetFieldValue("event_id", szTemp);  //HARDCODE
				szOut+="\tevent_id="; szOut+=szTemp;
				prs->GetFieldValue("event_clip", szTemp);  //HARDCODE
				szOut+="\tevent_clip="; szOut+=szTemp;
				prs->GetFieldValue("event_title", szTemp);  //HARDCODE
				szOut+="\tevent_title="; szOut+=szTemp;
				prs->GetFieldValue("event_data", szTemp);  //HARDCODE
				szOut+="\tevent_data="; szOut+=szTemp;
				prs->GetFieldValue("event_type", szTemp);  //HARDCODE
				szOut+="\tevent_type="; szOut+=szTemp;
				prs->GetFieldValue("event_status", szTemp);  //HARDCODE
				szOut+="\tevent_status="; szOut+=szTemp;
				prs->GetFieldValue("event_time_mode", szTemp);  //HARDCODE
				szOut+="\tevent_time_mode="; szOut+=szTemp;
				prs->GetFieldValue("event_start", szTemp);  //HARDCODE
				szOut+="\tevent_start="; szOut+=szTemp;
				prs->GetFieldValue("event_duration", szTemp);  //HARDCODE
				szOut+="\tevent_duration="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_start", szTemp);  //HARDCODE
				szOut+="\tevent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_end", szTemp);  //HARDCODE
				szOut+="\tevent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("event_position", szTemp);  //HARDCODE
				szOut+="\tevent_position="; szOut+=szTemp;
				prs->GetFieldValue("event_last_update", szTemp);  //HARDCODE
				szOut+="\tevent_last_update="; szOut+=szTemp;
				prs->GetFieldValue("parent_duration", szTemp);  //HARDCODE
				szOut+="\tparent_duration="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_start", szTemp);  //HARDCODE
				szOut+="\tparent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_end", szTemp);  //HARDCODE
				szOut+="\tparent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("parent_position", szTemp);  //HARDCODE
				szOut+="\tparent_position="; szOut+=szTemp;
				prs->GetFieldValue("app_data", szTemp);  //HARDCODE
				szOut+="\tapp_data="; szOut+=szTemp;
				prs->GetFieldValue("app_data_aux", szTemp);  //HARDCODE
				szOut+="\tapp_data_aux="; szOut+=szTemp;
				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(50); //(Dispatch message)
			}
			catch(...)
			{
			}
			prs->MoveNext();
			nIndex++;
		}

		prs->Close();

		try{delete prs;} catch(...){}
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "C: EVENT table: %d items retrieved\r\n", nIndex);//  Sleep(50); //(Dispatch message)
		LeaveCriticalSection(&g_psentinel->m_data.m_critDebug);

	}
if(!g_psentinel->m_settings.m_bMultiConnectSQL) 	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

}

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TIMING)	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "[%d] %s SentinelChannelThread before deletion status change = %d, %s%d%s", nClock, pChannel->m_pszDesc, bStatusChange, bPlaying?"playing event at ":"", nNumEvents, bPlaying?"":" events"); // Sleep(50); //(Dispatch message)



								if(bStatusChange)
								{
									if((g_psentinel->m_settings.m_bPositionShiftOnChange)&&(bPosShift))
									{
										pChannel->ShiftPositions(n, nPosDiff);
									}
								}

								if(((bDoNotBreakOnChange)||(!bStatusChange))&&(!bSQLerror))
								{
									// we have a complete list, so let's delete from the event list what events we do not have in the temp list
									// only do deletes if theres no status change, we know we have a complete list.

									// have to go thru and delete and remove all that we have not processed.
									int nNumToDelete=0;

									if(pChannel->m_ppevents)
									{
										n=0;
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "Deleting events of %d events", pChannel->m_nNumEvents);//  Sleep(50); //(Dispatch message)
										while((n<pChannel->m_nNumEvents)&&(!pChannel->m_bKillChannelThread)&&(!g_bKillThread))  // if killing just let it go
										{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "Checking processed %s:  %d", pChannel->m_ppevents[n]->m_pszEncodedID, (pChannel->m_ppevents[n]->m_nFlags&SENTINEL_EVENT_PROCESSED));
											if(
												  (pChannel->m_ppevents[n])
												&&(!(pChannel->m_ppevents[n]->m_nFlags&SENTINEL_EVENT_PROCESSED)) 
												)
											{
												if(!(pChannel->m_ppevents[n]->m_nFlags&SENTINEL_EVENT_DELETED))
												{
													if(nNumToDelete==0)
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE server_name = '%s' AND server_list = %d AND (itemid = %d",
															g_psentinel->m_settings.m_pszLiveEvents,
															pChannel->m_pszServerName, //server_name
															pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
															pChannel->m_ppevents[n]->m_uid
														);
													}
													else
													{
														_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s OR itemid = %d", szSQL, pChannel->m_ppevents[n]->m_uid);
														strcpy(szSQL, szSQLTemp);
													}
													nNumToDelete++;

													if((nNumToDelete>=g_psentinel->m_settings.m_nMaxDeleteItems)||(n>=pChannel->m_nNumEvents-1))
													{
														//do it.
														strcat(szSQL, ")");
if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread delete: %s", szSQL); // Sleep(50); //(Dispatch message)
if(!g_psentinel->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
														if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
														{
												//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
														}
if(!g_psentinel->m_settings.m_bMultiConnectSQL) LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

														nNumToDelete=0;
													}
												}



												if((g_psentinel->m_settings.m_bUseWorkingSet)&&(pChannel->m_ppevents[n]->m_pszEncodedKey))
												{
													workingset* pItem = new workingset;
													if(pItem)
													{
														pItem->bAdd = FALSE;
														pItem->pszKey = (char*)malloc(strlen(pChannel->m_ppevents[n]->m_pszEncodedKey)+1);
														if(pItem->pszKey)
														{
															strcpy(pItem->pszKey, pChannel->m_ppevents[n]->m_pszEncodedKey);
															if(_beginthread(SentinelWorkingSetAdjustThread, 0, (void*)pItem)==-1)
															{
																//error.
																DWORD dw = GetLastError(); 
																char* pchError;
																FormatMessage( 
																		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
																		NULL, 
																		dw,
																		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
																		(LPTSTR) &pchError, 0, NULL);

																if(pchError)
																{
																	//**MSG
																	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set adjust thread (delete).\r\nError code %d: %s\r\nNumber of changes: %d", dw, pchError, g_psentinel->m_data.m_nNumSetChanges); //(Dispatch message)
																	try{ LocalFree(pchError); } catch(...){}
																}
																else
																{
																	//**MSG
																	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set adjust thread (delete).\r\nError code %d\r\nNumber of changes: %d", dw, g_psentinel->m_data.m_nNumSetChanges); //(Dispatch message)
																}

																try {delete pItem;} catch(...){}

															}
														}
														else
														{
															try {delete pItem;} catch(...){}
														}
													}
												}

												if(
														(g_psentinel->m_settings.m_bUseControlModules)&&(g_psentinel->m_data.m_bControlModuleServiceThreadStarted)&&(!g_bKillThread) // not kill thread because service module is already shut down.
													&&(
															(g_psentinel->m_settings.m_nCtrlModLookahead<0)
														||((g_psentinel->m_settings.m_nCtrlModLookahead>0)&&(pChannel->m_ppevents[n]->m_nPosition<=g_psentinel->m_settings.m_nCtrlModLookahead))
														)
													)
												{
													int nChanges=-1;
													if(g_psentinel->m_settings.m_nCtrlModMaxQueue>0) 
													{
														EnterCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
														nChanges = g_psentinel->m_data.m_nNumChanges;
														LeaveCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
														if(nChanges>=g_psentinel->m_settings.m_nCtrlModMaxQueue)
														{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "discarding deletion change on [%s], queue is full (%d items)", pChannel->m_ppevents[n]->m_pszEncodedID, nChanges); //  Sleep(250);//(Dispatch message)

															nChanges=1;
														}
														else nChanges = -1;
													}
													if(nChanges<0)
													{

#pragma message(alert "here, send a change to send xml to control modules for event deletion")
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "creating event change for deletion"); //  Sleep(250);//(Dispatch message)

		CSentinelChange* pNewChange =  new CSentinelChange;
		if(pNewChange)
		{
			pNewChange->m_pObj = (void*) new CSentinelEventObject;
			if(pNewChange->m_pObj)
			{
				pNewChange->m_nType = DLLCMD_AITEM;

				CSentinelEventObject* pChgEvent = ((CSentinelEventObject*)(pNewChange->m_pObj));

				*pChgEvent = *(pChannel->m_ppevents[n]);
				pChgEvent->m_pszEncodedKey = NULL;
				pChgEvent->m_pszEncodedID = NULL;
				pChgEvent->m_pszEncodedTitle = NULL;
				pChgEvent->m_pszEncodedRecKey = NULL;
				pChgEvent->m_pszEncodedData = NULL;
				pChgEvent->m_event.m_pszID = NULL;
				pChgEvent->m_event.m_pszReconcileKey = NULL;
				pChgEvent->m_event.m_pszTitle = NULL;
				pChgEvent->m_event.m_pszData = NULL;
				pChgEvent->m_event.m_pContextData = NULL;

				pChgEvent->m_nPosition = -1;  //signals deletion
//				pChgEvent->m_nPreviousPosition = pChannel->m_ppevents[n]->m_nPosition; // stores the old position  // no need, put in lookahead supression above

				pNewChange->m_nChannelID = pChannel->m_nChannelID;
				pNewChange->m_nHarrisListID = pChannel->m_nHarrisListID;

				pNewChange->m_pszServerName =  bu.XMLEncode(pChannel->m_pszServerName);
//				pNewChange->m_pszDesc = bu.XMLEncode(pChannel->m_pszDesc);
				pChgEvent->m_dblUpdateTime = dblServerTime;

				pChgEvent->m_pParent = NULL; // let's not let it refer to any other event.

/*
// don't need to bother for deletion.
				pNewChange->m_pchXMLkey =  bu.XMLEncode(pChannel->m_ppevents[n]->m_pszEncodedKey);
				pNewChange->m_pchXMLrec =  bu.XMLEncode(pChannel->m_ppevents[n]->m_event.m_pszReconcileKey);
				pNewChange->m_pchXMLclip =  bu.XMLEncode(pChannel->m_ppevents[n]->m_event.m_pszID);
				pNewChange->m_pchXMLtitle =  bu.XMLEncode(pChannel->m_ppevents[n]->m_event.m_pszTitle);
				pNewChange->m_pchXMLdata =  bu.XMLEncode(pChannel->m_ppevents[n]->m_event.m_pszData);

				if(pChannel->m_ppevents[n]->m_pParent)
				{
					pNewChange->m_nParentUID = ((CSentinelEventObject*)(pChannel->m_ppevents[n]->m_pParent))->m_uid;
				}
				else
				{
					pNewChange->m_nParentUID = -1;
				}
*/

				if(
						(!g_bKillThread) // not kill thread because service module is already shut down.
					&&(_beginthread(SentinelPublishChangeThread, 0, (void*)(pNewChange))==-1)
					)
				{
					//error.

					//**MSG
					g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start publish change thread."); //(Dispatch message)

					try{ delete (pNewChange); } catch(...){}
				}
				
			}
			else
			{
				try{ delete (pNewChange); } catch(...){}
			}
		}
													}

												}


												if(pChannel->DeleteEvent(n)<ADC_SUCCESS)
												{
													 //error
					g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Error deleting event %d.", n); //(Dispatch message)
												}
												else
												{
													n--;  // rewind one, becasue we just deleted one, thereby bringing the others up.
												}
											}
											n++;
										}

									}

									if((nNumToDelete>0)&&(!pChannel->m_bKillChannelThread)&&(!g_bKillThread)) 
									{
										//finish it.
										strcat(szSQL, ")");
if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread delete: %s", szSQL); // Sleep(50); //(Dispatch message)
if(!g_psentinel->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
										if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
										{
									//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
										}
if(!g_psentinel->m_settings.m_bMultiConnectSQL) LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

										nNumToDelete=0;
									}


// dump tables
if(g_psentinel->m_settings.m_bDebugLists)
{
	CRecordset* prs;
	if(g_psentinel->m_settings.m_nDebugListTopCount>0)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top %d * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_nDebugListTopCount,
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}
	else
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}


if(!g_psentinel->m_settings.m_bMultiConnectSQL) 	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
	prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
	if(prs == NULL)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
	}
	else
	{
		EnterCriticalSection(&g_psentinel->m_data.m_critDebug);
		int nIndex = 0;
		CString szOut="\r\nF: EVENT Table:";
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut); // Sleep(20); //(Dispatch message)
		while ((!prs->IsEOF()))
		{
			CString szTemp="";
			szOut = "     ";
			try
			{

				prs->GetFieldValue("itemid", szTemp);  //HARDCODE
				szOut+="itemid="; szOut+=szTemp;
				prs->GetFieldValue("event_key", szTemp);  //HARDCODE
				szOut+="\tevent_key="; szOut+=szTemp;
				prs->GetFieldValue("server_name", szTemp);  //HARDCODE
				szOut+="\tserver_name="; szOut+=szTemp;
				prs->GetFieldValue("server_list", szTemp);  //HARDCODE
				szOut+="\tserver_list="; szOut+=szTemp;
				prs->GetFieldValue("list_id", szTemp);  //HARDCODE
				szOut+="\tlist_id="; szOut+=szTemp;
				prs->GetFieldValue("event_id", szTemp);  //HARDCODE
				szOut+="\tevent_id="; szOut+=szTemp;
				prs->GetFieldValue("event_clip", szTemp);  //HARDCODE
				szOut+="\tevent_clip="; szOut+=szTemp;
				prs->GetFieldValue("event_title", szTemp);  //HARDCODE
				szOut+="\tevent_title="; szOut+=szTemp;
				prs->GetFieldValue("event_data", szTemp);  //HARDCODE
				szOut+="\tevent_data="; szOut+=szTemp;
				prs->GetFieldValue("event_type", szTemp);  //HARDCODE
				szOut+="\tevent_type="; szOut+=szTemp;
				prs->GetFieldValue("event_status", szTemp);  //HARDCODE
				szOut+="\tevent_status="; szOut+=szTemp;
				prs->GetFieldValue("event_time_mode", szTemp);  //HARDCODE
				szOut+="\tevent_time_mode="; szOut+=szTemp;
				prs->GetFieldValue("event_start", szTemp);  //HARDCODE
				szOut+="\tevent_start="; szOut+=szTemp;
				prs->GetFieldValue("event_duration", szTemp);  //HARDCODE
				szOut+="\tevent_duration="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_start", szTemp);  //HARDCODE
				szOut+="\tevent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_end", szTemp);  //HARDCODE
				szOut+="\tevent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("event_position", szTemp);  //HARDCODE
				szOut+="\tevent_position="; szOut+=szTemp;
				prs->GetFieldValue("event_last_update", szTemp);  //HARDCODE
				szOut+="\tevent_last_update="; szOut+=szTemp;
				prs->GetFieldValue("parent_duration", szTemp);  //HARDCODE
				szOut+="\tparent_duration="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_start", szTemp);  //HARDCODE
				szOut+="\tparent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_end", szTemp);  //HARDCODE
				szOut+="\tparent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("parent_position", szTemp);  //HARDCODE
				szOut+="\tparent_position="; szOut+=szTemp;
				prs->GetFieldValue("app_data", szTemp);  //HARDCODE
				szOut+="\tapp_data="; szOut+=szTemp;
				prs->GetFieldValue("app_data_aux", szTemp);  //HARDCODE
				szOut+="\tapp_data_aux="; szOut+=szTemp;
				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(50); //(Dispatch message)
			}
			catch(...)
			{
			}
			prs->MoveNext();
			nIndex++;
		}

		prs->Close();

		try { delete prs; } catch(...){}
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "F: EVENT table: %d items retrieved\r\n", nIndex);//  Sleep(50); //(Dispatch message)

		LeaveCriticalSection(&g_psentinel->m_data.m_critDebug);
	}
if(!g_psentinel->m_settings.m_bMultiConnectSQL) 	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

}

//readytotest = true;
								} //if(!bStatusChange)
// Let's only update the list changes if we get the full list. - NO, need to know when list size changes as opposed to just event status.
									// record last values if get was successful.
								if(!bSQLerror)
								{
//									memcpy(&lastlistdata, &listdata, sizeof(tlistdata));  // had to move this to the top so that this would not be different all the time, causing it to be delayed until it was forced!
									if(g_psentinel->m_settings.m_bDelayStatusForEvents)// success on events, so publish list status now.
									{

						pChannel->m_dblUpdateTime = ((double)(timestamp.time)) + ((double)(timestamp.millitm))/1000.0;
						if(g_psentinel->m_settings.m_bStatusInTables)
						{
	// new:
	//create table Channels (ID int NOT NULL identity(1,1), flags int NOT NULL, description varchar(256), server varchar(32), listid int, list_state int, list_changed int, list_display int, list_syschange int, list_count int, list_lookahead int, list_last_update float);
	//create table Connections (server varchar(32) NOT NULL, description varchar(256), flags int, client varchar(32), server_time float, server_status int, server_lists int, server_basis int, server_changed int, server_last_update float);
							
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
list_state = %d, \
list_changed = %d, \
list_display = %d, \
list_syschange = %d, \
list_count = %d, \
list_lookahead = %d, \
list_last_update = %d%03d WHERE \
server = '%s' AND listid = %d",
								g_psentinel->m_settings.m_pszChannels?g_psentinel->m_settings.m_pszChannels:"Channels",
								lastlistdata.liststate,
								lastlistdata.listchanged,				// incremented whenever list size changes
								lastlistdata.listdisplay,				// incremented whenever an event changes
								lastlistdata.listsyschange,			// incremented when list related values are changed. 
								lastlistdata.listcount,					// number of events in list 
								lastlistdata.lookahead,					// lookahead value

								(unsigned long)(timestamp.time),// unixtime - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
								timestamp.millitm,

								pChannel->m_pszServerName,
								pChannel->m_nHarrisListID
								);
	//if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "List change (tables): %s", szSQL);  //Sleep(250); //(Dispatch message)
	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
							if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
							{
								// error.
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);  //Sleep(50); //(Dispatch message)
							}
	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

						}


						if(g_psentinel->m_settings.m_bListStatusInExchange)
						{
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET flag = '%s:%02d|%d%03d|%d|%d|%d|%d', mod = %d WHERE \
criterion = 'List_Status' AND flag LIKE '%s:%02d%%'",
								g_psentinel->m_settings.m_pszExchange?g_psentinel->m_settings.m_pszExchange:"Exchange",
								pChannel->m_pszServerName,
								pChannel->m_nHarrisListID,
								(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
								timestamp.millitm,
								lastlistdata.listchanged,				// incremented whenever list size changes
								lastlistdata.listdisplay,				// incremented whenever an event changes
								lastlistdata.listsyschange,			// incremented when list related values are changed. 
								lastlistdata.listcount,					// number of events in list 
								lastlistdata.liststate,
								pChannel->m_pszServerName,
								pChannel->m_nHarrisListID
								);
	//if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "List change (exchange): %s", szSQL);  //Sleep(250); //(Dispatch message)
	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 					EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
							if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
							{
									// error.
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);  //Sleep(50); //(Dispatch message)
							}

	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 					LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
						}
									}

								}
								if((bStatusChange)||(bSQLerror))
								{ //	let's make sure we dont miss stuff
									lastlistdata.listdisplay = listdata.listdisplay - 1;  // display is the one that changes the most often, make sure it is different
								}

								if((!bSQLerror)&&(!bStatusChange))
								{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "no error", "<<<<<<<< pending changes processed on %s", pChannel->m_pszDesc); 
									bChangesPending=false;
									if(g_psentinel->m_settings.m_bDoConfirmGet)
									{
										if(bconfirmed) bconfirmed=false;
										else bconfirmed=true;
									}
									if(bKeyChange)
									{
										// set the last key
										_snprintf(szLastKey, MAX_MESSAGE_LENGTH-1, "%s", g_psentinel->m_settings.m_pszEventKeyFormat);
									}
								}
								
								pChannel->m_bChannelThreadPublishingEvents = false;



							}//	if(!pChannel->m_bKillChannelThread)
							// increment exchange mod counter. for events table.
EnterCriticalSection(&g_psentinel->m_data.m_critIncrement);
							if(
									(!pChannel->m_bKillChannelThread)
								&&(!g_bKillThread)
								&&(g_psentinel->m_data.IncrementDatabaseMods(g_psentinel->m_settings.m_pszLiveEvents, dberrorstring)<SENTINEL_SUCCESS)
								)
							{
								//**MSG
							}
LeaveCriticalSection(&g_psentinel->m_data.m_critIncrement);
						}//if((pdb!=NULL)&&(pdbConn!=NULL)&&(g_psentinel->m_settings.m_pszLiveEvents)&&(strlen(g_psentinel->m_settings.m_pszLiveEvents)>0)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))

						else
						{
if(pChannel->m_pcritAPI)
{
 	LeaveCriticalSection(pChannel->m_pcritAPI);
}
						}
					} //if((pList)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))  // plist is null if no events in list.
					else
					if((nNumEvents == ADC_NO_EVENTS)&&(!pChannel->m_bKillChannelThread)&&(!g_bKillThread))   // but this is correct, but only if not killing
					{
// 	LeaveCriticalSection(pChannel->m_pcritAPI);

//Clear out the Events table
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d",
							g_psentinel->m_settings.m_pszLiveEvents,
							pChannel->m_pszServerName,
							pChannel->m_nHarrisListID
							);

if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread event SQL: %s", szSQL);  //Sleep(50); //(Dispatch message)
if(!g_psentinel->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
						if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
							//**MSG
							bSQLerror = true;
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
						}

if(!g_psentinel->m_settings.m_bMultiConnectSQL) LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
						// increment exchange mod counter. for events table.
EnterCriticalSection(&g_psentinel->m_data.m_critIncrement);
						if(
								(!pChannel->m_bKillChannelThread)
							&&(!g_bKillThread)
							&&(g_psentinel->m_data.IncrementDatabaseMods(g_psentinel->m_settings.m_pszLiveEvents, dberrorstring)<SENTINEL_SUCCESS)
							)
						{
							//**MSG
						}
LeaveCriticalSection(&g_psentinel->m_data.m_critIncrement);
					// record last values if get was successful.
					//	memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
						if(!bSQLerror)
						{
							memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
							if(g_psentinel->m_settings.m_bDelayStatusForEvents)// success on events, so publish list status now.
							{

						pChannel->m_dblUpdateTime = ((double)(timestamp.time)) + ((double)(timestamp.millitm))/1000.0;
						if(g_psentinel->m_settings.m_bStatusInTables)
						{
	// new:
	//create table Channels (ID int NOT NULL identity(1,1), flags int NOT NULL, description varchar(256), server varchar(32), listid int, list_state int, list_changed int, list_display int, list_syschange int, list_count int, list_lookahead int, list_last_update float);
	//create table Connections (server varchar(32) NOT NULL, description varchar(256), flags int, client varchar(32), server_time float, server_status int, server_lists int, server_basis int, server_changed int, server_last_update float);
							
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
list_state = %d, \
list_changed = %d, \
list_display = %d, \
list_syschange = %d, \
list_count = %d, \
list_lookahead = %d, \
list_last_update = %d%03d WHERE \
server = '%s' AND listid = %d",
								g_psentinel->m_settings.m_pszChannels?g_psentinel->m_settings.m_pszChannels:"Channels",
								lastlistdata.liststate,
								lastlistdata.listchanged,				// incremented whenever list size changes
								lastlistdata.listdisplay,				// incremented whenever an event changes
								lastlistdata.listsyschange,			// incremented when list related values are changed. 
								lastlistdata.listcount,					// number of events in list 
								lastlistdata.lookahead,					// lookahead value

								(unsigned long)(timestamp.time),// unixtime - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
								timestamp.millitm,

								pChannel->m_pszServerName,
								pChannel->m_nHarrisListID
								);
	//if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "List change (tables): %s", szSQL);  //Sleep(250); //(Dispatch message)
	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
							if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
							{
								// error.
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);  //Sleep(50); //(Dispatch message)
							}
	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

						}


						if(g_psentinel->m_settings.m_bListStatusInExchange)
						{
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET flag = '%s:%02d|%d%03d|%d|%d|%d|%d', mod = %d WHERE \
criterion = 'List_Status' AND flag LIKE '%s:%02d%%'",
								g_psentinel->m_settings.m_pszExchange?g_psentinel->m_settings.m_pszExchange:"Exchange",
								pChannel->m_pszServerName,
								pChannel->m_nHarrisListID,
								(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
								timestamp.millitm,
								lastlistdata.listchanged,				// incremented whenever list size changes
								lastlistdata.listdisplay,				// incremented whenever an event changes
								lastlistdata.listsyschange,			// incremented when list related values are changed. 
								lastlistdata.listcount,					// number of events in list 
								lastlistdata.liststate,
								pChannel->m_pszServerName,
								pChannel->m_nHarrisListID
								);
	//if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "List change (exchange): %s", szSQL);  //Sleep(250); //(Dispatch message)
	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 					EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
							if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
							{
									// error.
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);  //Sleep(50); //(Dispatch message)
							}

	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 					LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
						}
							}

						}
						if((bStatusChange)||(bSQLerror))
						{ //	let's make sure we dont miss stuff
							lastlistdata.listdisplay = listdata.listdisplay - 1;  // display is the one that changes the most often, make sure it is different
						}
						if((!bSQLerror)&&(!bStatusChange))
						{
							bChangesPending=false;
							if(g_psentinel->m_settings.m_bDoConfirmGet)
							{
								if(bconfirmed) bconfirmed=false;
								else bconfirmed=true;
							}
							if(bKeyChange)
							{
								// set the last key
								_snprintf(szLastKey, MAX_MESSAGE_LENGTH-1, "%s", g_psentinel->m_settings.m_pszEventKeyFormat);
							}


							// let's check the number of events in the list.
							if(
								  (pChannel)
								&&(pChannel->m_nNumEvents)
								&&(
								    ((g_psentinel->m_settings.m_nMaxLookahead<=0)&&(pChannel->m_nNumEvents != listdata.listcount))
								  ||((g_psentinel->m_settings.m_nMaxLookahead>0)&&(pChannel->m_nNumEvents != min(listdata.listcount,g_psentinel->m_settings.m_nMaxLookahead)))
									)
								)
							{
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "Event count discrepancy: %d events, Harris has %d (lookahead is %d)", pChannel->m_nNumEvents, listdata.listcount, g_psentinel->m_settings.m_nMaxLookahead);  //Sleep(50); //(Dispatch message)
							}




							// also, need to clear out the list(s)
							if(pList)
							{
								if(pList->m_ppEvents)
								{
									for(int i=0; i<pList->m_nEventCount; i++)
									{
										if(pList->m_ppEvents[i]) { try { delete pList->m_ppEvents[i]; } catch(...){}} // must use new to allocate
									}
										try { delete [] pList->m_ppEvents;} catch(...){}  // must use new to allocate
								}

								pList->m_ppEvents = NULL;
								pList->m_nEventCount = 0;
								pList->m_nEventBufferSize = 0;
							}

							if((pChannel)&&(pChannel->m_ppevents)&&(pChannel->m_nNumEvents))
							{

								int i=0;
								if(pChannel->m_ppevents)
								{
									while(i<pChannel->m_nNumEvents)
									{
										if(pChannel->m_ppevents[i]) 
										{
											if((g_psentinel->m_settings.m_bUseWorkingSet)&&(pChannel->m_ppevents[i]->m_pszEncodedKey))
											{
												workingset* pItem = new workingset;
												if(pItem)
												{
													pItem->bAdd = FALSE;
													pItem->pszKey = (char*)malloc(strlen(pChannel->m_ppevents[i]->m_pszEncodedKey)+1);
													if(pItem->pszKey)
													{
														strcpy(pItem->pszKey, pChannel->m_ppevents[i]->m_pszEncodedKey);
														if(_beginthread(SentinelWorkingSetAdjustThread, 0, (void*)pItem)==-1)
														{
															//error.
															DWORD dw = GetLastError(); 
															char* pchError;
															FormatMessage( 
																	FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
																	NULL, 
																	dw,
																	MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
																	(LPTSTR) &pchError, 0, NULL);

															if(pchError)
															{
																//**MSG
																g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set adjust thread (delete).\r\nError code %d: %s\r\nNumber of changes: %d", dw, pchError, g_psentinel->m_data.m_nNumSetChanges); //(Dispatch message)
																try{ LocalFree(pchError); } catch(...){}
															}
															else
															{
																//**MSG
																g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set adjust thread (delete).\r\nError code %d\r\nNumber of changes: %d", dw, g_psentinel->m_data.m_nNumSetChanges); //(Dispatch message)
															}

															try {delete pItem;} catch(...){} 

														}
													}
													else
													{
														try {delete pItem;} catch(...){} 
													}
												}
											}


											try { delete pChannel->m_ppevents[i];} catch(...){} 
										}
										i++;
									}
									try{ delete [] pChannel->m_ppevents;} catch(...){}
								}

								pChannel->m_ppevents = NULL;
								pChannel->m_nNumEvents = 0;
							}

						}
					}
					else
					{
// 	LeaveCriticalSection(pChannel->m_pcritAPI);
					}
//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread leaving critical");  Sleep(20); //(Dispatch message)
					LeaveCriticalSection(&(pList->m_crit));
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TIMING)	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "[%d] %s SentinelChannelThread left critical with %d updates in %d ms", clock(), pChannel->m_pszDesc, nUpdates, clock()-nClock ); // Sleep(20); //(Dispatch message)
				}//if((pList)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))
				else 	if((!pChannel->m_bKillChannelThread)&&(!g_bKillThread))
 //pList was NULL, probably a disconnection
				{
					if(pChannel->m_pcritAPI)
					{
 	LeaveCriticalSection(pChannel->m_pcritAPI);
					}
					if((g_psentinel->m_settings.m_bClearListOnDisconnect)&&(pChannel->m_nNumEvents > 0))
					{


//Clear out the Events table
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d",
							g_psentinel->m_settings.m_pszLiveEvents,
							pChannel->m_pszServerName,
							pChannel->m_nHarrisListID
							);

if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread event SQL: %s", szSQL);  //Sleep(50); //(Dispatch message)
if(!g_psentinel->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
						if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
							//**MSG
							bSQLerror = true;
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
						}

if(!g_psentinel->m_settings.m_bMultiConnectSQL) LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
						// increment exchange mod counter. for events table.
EnterCriticalSection(&g_psentinel->m_data.m_critIncrement);
						if(
								(!pChannel->m_bKillChannelThread)
							&&(!g_bKillThread)
							&&(g_psentinel->m_data.IncrementDatabaseMods(g_psentinel->m_settings.m_pszLiveEvents, dberrorstring)<SENTINEL_SUCCESS)
							)
						{
							//**MSG
						}
LeaveCriticalSection(&g_psentinel->m_data.m_critIncrement);
					// record last values if get was successful.
					//	memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
						if(!bSQLerror)
						{
//							memcpy(&lastlistdata, &listdata, sizeof(tlistdata));

							// ifpList was null, or api conn null, we have issues, can't memcpy the status
							lastlistdata.listchanged=0;				// incremented whenever list size changes
							lastlistdata.listdisplay=0;				// incremented whenever an event changes
							lastlistdata.listsyschange=0;			// incremented when list related values are changed. 
							lastlistdata.listcount=0;					// number of events in list 
							lastlistdata.liststate=0;


							if(g_psentinel->m_settings.m_bDelayStatusForEvents)// success on events, so publish list status now.
							{

						pChannel->m_dblUpdateTime = ((double)(timestamp.time)) + ((double)(timestamp.millitm))/1000.0;
						if(g_psentinel->m_settings.m_bStatusInTables)
						{
	// new:
	//create table Channels (ID int NOT NULL identity(1,1), flags int NOT NULL, description varchar(256), server varchar(32), listid int, list_state int, list_changed int, list_display int, list_syschange int, list_count int, list_lookahead int, list_last_update float);
	//create table Connections (server varchar(32) NOT NULL, description varchar(256), flags int, client varchar(32), server_time float, server_status int, server_lists int, server_basis int, server_changed int, server_last_update float);
							
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
list_state = %d, \
list_changed = %d, \
list_display = %d, \
list_syschange = %d, \
list_count = %d, \
list_lookahead = %d, \
list_last_update = %d%03d WHERE \
server = '%s' AND listid = %d",
								g_psentinel->m_settings.m_pszChannels?g_psentinel->m_settings.m_pszChannels:"Channels",
								lastlistdata.liststate,
								lastlistdata.listchanged,				// incremented whenever list size changes
								lastlistdata.listdisplay,				// incremented whenever an event changes
								lastlistdata.listsyschange,			// incremented when list related values are changed. 
								lastlistdata.listcount,					// number of events in list 
								lastlistdata.lookahead,					// lookahead value

								(unsigned long)(timestamp.time),// unixtime - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
								timestamp.millitm,

								pChannel->m_pszServerName,
								pChannel->m_nHarrisListID
								);
	//if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "List change (tables): %s", szSQL);  //Sleep(250); //(Dispatch message)
	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
							if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
							{
								// error.
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);  //Sleep(50); //(Dispatch message)
							}
	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

						}


						if(g_psentinel->m_settings.m_bListStatusInExchange)
						{
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET flag = '%s:%02d|%d%03d|%d|%d|%d|%d', mod = %d WHERE \
criterion = 'List_Status' AND flag LIKE '%s:%02d%%'",
								g_psentinel->m_settings.m_pszExchange?g_psentinel->m_settings.m_pszExchange:"Exchange",
								pChannel->m_pszServerName,
								pChannel->m_nHarrisListID,
								(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
								timestamp.millitm,
								lastlistdata.listchanged,				// incremented whenever list size changes
								lastlistdata.listdisplay,				// incremented whenever an event changes
								lastlistdata.listsyschange,			// incremented when list related values are changed. 
								lastlistdata.listcount,					// number of events in list 
								lastlistdata.liststate,
								pChannel->m_pszServerName,
								pChannel->m_nHarrisListID
								);
	//if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "List change (exchange): %s", szSQL);  //Sleep(250); //(Dispatch message)
	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 					EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
							if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
							{
									// error.
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);  //Sleep(50); //(Dispatch message)
							}

	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 					LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
						}
							}

						}
						if((bStatusChange)||(bSQLerror))
						{ //	let's make sure we dont miss stuff
							lastlistdata.listdisplay = listdata.listdisplay - 1;  // display is the one that changes the most often, make sure it is different
						}
						if((!bSQLerror)&&(!bStatusChange))
						{
							bChangesPending=false;
							if(g_psentinel->m_settings.m_bDoConfirmGet)
							{
								if(bconfirmed) bconfirmed=false;
								else bconfirmed=true;
							}
							if(bKeyChange)
							{
								// set the last key
								_snprintf(szLastKey, MAX_MESSAGE_LENGTH-1, "%s", g_psentinel->m_settings.m_pszEventKeyFormat);
							}


							// also, need to clear out the list(s)
/*
// but not here. pList is most likely not valid, since we are disconnected
							if(pList)
							{
								if(pList->m_ppEvents)
								{
									for(int i=0; i<pList->m_nEventCount; i++)
									{
										if(pList->m_ppEvents[i]) { try { delete pList->m_ppEvents[i]; } catch(...){}} // must use new to allocate
									}
										try { delete [] pList->m_ppEvents;} catch(...){}  // must use new to allocate
								}

								pList->m_ppEvents = NULL;
								pList->m_nEventCount = 0;
								pList->m_nEventBufferSize = 0;
							}
*/




							if((pChannel)&&(pChannel->m_ppevents)&&(pChannel->m_nNumEvents))
							{

								int i=0;
								if(pChannel->m_ppevents)
								{
									while(i<pChannel->m_nNumEvents)
									{
										if(pChannel->m_ppevents[i]) 
										{
											if((g_psentinel->m_settings.m_bUseWorkingSet)&&(pChannel->m_ppevents[i]->m_pszEncodedKey))
											{
												workingset* pItem = new workingset;
												if(pItem)
												{
													pItem->bAdd = FALSE;
													pItem->pszKey = (char*)malloc(strlen(pChannel->m_ppevents[i]->m_pszEncodedKey)+1);
													if(pItem->pszKey)
													{
														strcpy(pItem->pszKey, pChannel->m_ppevents[i]->m_pszEncodedKey);
														if(_beginthread(SentinelWorkingSetAdjustThread, 0, (void*)pItem)==-1)
														{
															//error.
															DWORD dw = GetLastError(); 
															char* pchError;
															FormatMessage( 
																	FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
																	NULL, 
																	dw,
																	MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
																	(LPTSTR) &pchError, 0, NULL);

															if(pchError)
															{
																//**MSG
																g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set adjust thread (delete).\r\nError code %d: %s\r\nNumber of changes: %d", dw, pchError, g_psentinel->m_data.m_nNumSetChanges); //(Dispatch message)
																try{ LocalFree(pchError); } catch(...){}
															}
															else
															{
																//**MSG
																g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set adjust thread (delete).\r\nError code %d\r\nNumber of changes: %d", dw, g_psentinel->m_data.m_nNumSetChanges); //(Dispatch message)
															}

															try { delete pItem;} catch(...){} 

														}
													}
													else
													{
														try { delete pItem;} catch(...){} 
													}
												}
											}


											try { delete pChannel->m_ppevents[i];} catch(...){} 
										}
										i++;
									}
									try{ delete [] pChannel->m_ppevents;} catch(...){}
								}

								pChannel->m_ppevents = NULL;
								pChannel->m_nNumEvents = 0;
							}

						}


					}
				}
				else
				{
 if(pChannel->m_pcritAPI)
{
	LeaveCriticalSection(pChannel->m_pcritAPI);
}
				}
				if(bDoNotBreakOnChange) LeaveCriticalSection(&g_psentinel->m_data.m_critChange);

			} //			if(((listdata.listchanged!=lastlistdata.listchanged)||(listdata.listdisplay!=lastlistdata.listdisplay))&&(pChannel->m_pAPIConn)&&(!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true)) // just do the event chage one as well for now.

			else 
			{
if(pChannel->m_pcritAPI)
{
	LeaveCriticalSection(pChannel->m_pcritAPI);
}
			}
/*			else
			if(listdata.listdisplay!=lastlistdata.listdisplay)
			{
				// an event has changed, need to find it and just deal with that one.
				// record last values
				memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
			}
*/
		}
		else  //not connected!
		{
if(pChannel->m_pcritAPI)
{
	LeaveCriticalSection(pChannel->m_pcritAPI);
}
					if((g_psentinel->m_settings.m_bUseControlModules)&&(g_psentinel->m_data.m_bControlModuleServiceThreadStarted)&&(!g_bKillThread)) // not kill thread because service module is already shut down.
					{

						int nChanges=-1;
						if(g_psentinel->m_settings.m_nCtrlModMaxQueue>0) 
						{
							EnterCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
							nChanges = g_psentinel->m_data.m_nNumChanges;
							LeaveCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
							if(nChanges>=g_psentinel->m_settings.m_nCtrlModMaxQueue)
							{
	if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "discarding list change, queue is full (%d items)", nChanges); //  Sleep(250);//(Dispatch message)

								nChanges=1;
							}
							else nChanges = -1;
						}
						if(nChanges<0)
						{

#pragma message(alert "here, send a change to send xml to control modules for list changes")
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "creating list change on no connection"); //  Sleep(250);//(Dispatch message)

						if(	
							  (lastlistdata.listcount!=0)
							&&(lastlistdata.listdisplay!=0)
							&&(lastlistdata.listsyschange!=0)
							&&(lastlistdata.listcount!=0)
							&&(lastlistdata.liststate!=0)
							)
						{

							
							lastlistdata.listchanged=0;				// incremented whenever list size changes
							lastlistdata.listdisplay=0;				// incremented whenever an event changes
							lastlistdata.listsyschange=0;			// incremented when list related values are changed. 
							lastlistdata.listcount=0;					// number of events in list 
							lastlistdata.liststate=0;


							CSentinelChange* pNewChange =  new CSentinelChange;
							if(pNewChange)
							{
								pNewChange->m_pObj = (void*) new CSentinelChannelObject;
								if(pNewChange->m_pObj)
								{
									pNewChange->m_nType = DLLCMD_ALIST;

									CSentinelChannelObject* pChgChannel = ((CSentinelChannelObject*)(pNewChange->m_pObj));

									*pChgChannel = *pChannel;
									pChgChannel->m_pszServerName = NULL;
									pChgChannel->m_pszDesc = NULL;
									pChgChannel->m_ppevents = NULL;
									pChgChannel->m_nNumEvents=0;

									pChgChannel->m_pptraffic=NULL;
									pChgChannel->m_nNumTraffic=0;

									pNewChange->m_nChannelID = pChannel->m_nChannelID;
									pNewChange->m_nHarrisListID = pChannel->m_nHarrisListID;

									pNewChange->m_pszServerName =  bu.XMLEncode(pChannel->m_pszServerName);
									pNewChange->m_pszDesc = bu.XMLEncode(pChannel->m_pszDesc);

									memcpy(&(pNewChange->m_list), &(lastlistdata), sizeof(tlistdata));

									pChgChannel->m_bChannelThreadStarted = false; // so it will not stall in deconstructor
									if(
											(!g_bKillThread) // not kill thread because service module is already shut down.
										&&(_beginthread(SentinelPublishChangeThread, 0, (void*)(pNewChange))==-1)
										)
									{
										//error.

										//**MSG
										g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start publish change thread."); //(Dispatch message)

										try{ delete (pNewChange); } catch(...){}
									}

									
								}
								else
								{
									try{ delete (pNewChange); } catch(...){}
								}
							}

						}

						}
					}					
	// should clear db.
					if((g_psentinel->m_settings.m_bClearListOnDisconnect)&&(pChannel->m_nNumEvents > 0))
					{

						bool bSQLerror = false;

//Clear out the Events table
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d",
							g_psentinel->m_settings.m_pszLiveEvents,
							pChannel->m_pszServerName,
							pChannel->m_nHarrisListID
							);

if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread event SQL: %s", szSQL);  //Sleep(50); //(Dispatch message)
if(!g_psentinel->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
						if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
							//**MSG
							bSQLerror = true;
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
						}

if(!g_psentinel->m_settings.m_bMultiConnectSQL) LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

						// increment exchange mod counter. for events table.
EnterCriticalSection(&g_psentinel->m_data.m_critIncrement);
						if(
								(!pChannel->m_bKillChannelThread)
							&&(!g_bKillThread)
							&&(g_psentinel->m_data.IncrementDatabaseMods(g_psentinel->m_settings.m_pszLiveEvents, dberrorstring)<SENTINEL_SUCCESS)
							)
						{
							//**MSG
						}
LeaveCriticalSection(&g_psentinel->m_data.m_critIncrement);
					// record last values if get was successful.
					//	memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
						if(!bSQLerror)
						{
//							memcpy(&lastlistdata, &listdata, sizeof(tlistdata));

							// ifpList was null, or api conn null, we have issues, can't memcpy the status
							lastlistdata.listchanged=0;				// incremented whenever list size changes
							lastlistdata.listdisplay=0;				// incremented whenever an event changes
							lastlistdata.listsyschange=0;			// incremented when list related values are changed. 
							lastlistdata.listcount=0;					// number of events in list 
							lastlistdata.liststate=0;


							if(g_psentinel->m_settings.m_bDelayStatusForEvents)// success on events, so publish list status now.
							{

						pChannel->m_dblUpdateTime = ((double)(timestamp.time)) + ((double)(timestamp.millitm))/1000.0;
						if(g_psentinel->m_settings.m_bStatusInTables)
						{
	// new:
	//create table Channels (ID int NOT NULL identity(1,1), flags int NOT NULL, description varchar(256), server varchar(32), listid int, list_state int, list_changed int, list_display int, list_syschange int, list_count int, list_lookahead int, list_last_update float);
	//create table Connections (server varchar(32) NOT NULL, description varchar(256), flags int, client varchar(32), server_time float, server_status int, server_lists int, server_basis int, server_changed int, server_last_update float);
							
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
list_state = %d, \
list_changed = %d, \
list_display = %d, \
list_syschange = %d, \
list_count = %d, \
list_lookahead = %d, \
list_last_update = %d%03d WHERE \
server = '%s' AND listid = %d",
								g_psentinel->m_settings.m_pszChannels?g_psentinel->m_settings.m_pszChannels:"Channels",
								lastlistdata.liststate,
								lastlistdata.listchanged,				// incremented whenever list size changes
								lastlistdata.listdisplay,				// incremented whenever an event changes
								lastlistdata.listsyschange,			// incremented when list related values are changed. 
								lastlistdata.listcount,					// number of events in list 
								lastlistdata.lookahead,					// lookahead value

								(unsigned long)(timestamp.time),// unixtime - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
								timestamp.millitm,

								pChannel->m_pszServerName,
								pChannel->m_nHarrisListID
								);
	//if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "List change (tables): %s", szSQL);  //Sleep(250); //(Dispatch message)
	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
							if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
							{
								// error.
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);  //Sleep(50); //(Dispatch message)
							}
	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

						}


						if(g_psentinel->m_settings.m_bListStatusInExchange)
						{
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET flag = '%s:%02d|%d%03d|%d|%d|%d|%d', mod = %d WHERE \
criterion = 'List_Status' AND flag LIKE '%s:%02d%%'",
								g_psentinel->m_settings.m_pszExchange?g_psentinel->m_settings.m_pszExchange:"Exchange",
								pChannel->m_pszServerName,
								pChannel->m_nHarrisListID,
								(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
								timestamp.millitm,
								lastlistdata.listchanged,				// incremented whenever list size changes
								lastlistdata.listdisplay,				// incremented whenever an event changes
								lastlistdata.listsyschange,			// incremented when list related values are changed. 
								lastlistdata.listcount,					// number of events in list 
								lastlistdata.liststate,
								pChannel->m_pszServerName,
								pChannel->m_nHarrisListID
								);
	//if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "List change (exchange): %s", szSQL);  //Sleep(250); //(Dispatch message)
	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 					EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
							if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
							{
									// error.
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);  //Sleep(50); //(Dispatch message)
							}

	if(!g_psentinel->m_settings.m_bMultiConnectSQL) 					LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
						}
							}

						}
/*
						if((bStatusChange)||(bSQLerror))
						{ //	let's make sure we dont miss stuff
							lastlistdata.listdisplay = listdata.listdisplay - 1;  // display is the one that changes the most often, make sure it is different
						}
*/
						if((!bSQLerror))//&&(!bStatusChange))
						{
							bChangesPending=false;
/*
							if(g_psentinel->m_settings.m_bDoConfirmGet)
							{
								if(bconfirmed) bconfirmed=false;
								else bconfirmed=true;
							}

							if(bKeyChange)
							{
								// set the last key
								_snprintf(szLastKey, MAX_MESSAGE_LENGTH-1, "%s", g_psentinel->m_settings.m_pszEventKeyFormat);
							}
*/

							// also, need to clear out the list(s)
/*
// but not here. pList is most likely not valid, since we are disconnected
							if(pList)
							{
								if(pList->m_ppEvents)
								{
									for(int i=0; i<pList->m_nEventCount; i++)
									{
										if(pList->m_ppEvents[i]) { try { delete pList->m_ppEvents[i]; } catch(...){}} // must use new to allocate
									}
										try { delete [] pList->m_ppEvents;} catch(...){}  // must use new to allocate
								}

								pList->m_ppEvents = NULL;
								pList->m_nEventCount = 0;
								pList->m_nEventBufferSize = 0;
							}
*/
							if((pChannel)&&(pChannel->m_ppevents)&&(pChannel->m_nNumEvents))
							{

								int i=0;
								if(pChannel->m_ppevents)
								{
									while(i<pChannel->m_nNumEvents)
									{
										if(pChannel->m_ppevents[i]) 
										{
											if((g_psentinel->m_settings.m_bUseWorkingSet)&&(pChannel->m_ppevents[i]->m_pszEncodedKey))
											{
												workingset* pItem = new workingset;
												if(pItem)
												{
													pItem->bAdd = FALSE;
													pItem->pszKey = (char*)malloc(strlen(pChannel->m_ppevents[i]->m_pszEncodedKey)+1);
													if(pItem->pszKey)
													{
														strcpy(pItem->pszKey, pChannel->m_ppevents[i]->m_pszEncodedKey);
														if(_beginthread(SentinelWorkingSetAdjustThread, 0, (void*)pItem)==-1)
														{
															//error.
															DWORD dw = GetLastError(); 
															char* pchError;
															FormatMessage( 
																	FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
																	NULL, 
																	dw,
																	MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
																	(LPTSTR) &pchError, 0, NULL);

															if(pchError)
															{
																//**MSG
																g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set adjust thread (delete).\r\nError code %d: %s\r\nNumber of changes: %d", dw, pchError, g_psentinel->m_data.m_nNumSetChanges); //(Dispatch message)
																try{ LocalFree(pchError); } catch(...){}
															}
															else
															{
																//**MSG
																g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set adjust thread (delete).\r\nError code %d\r\nNumber of changes: %d", dw, g_psentinel->m_data.m_nNumSetChanges); //(Dispatch message)
															}

															try { delete pItem;} catch(...){} 

														}
													}
													else
													{
														try { delete pItem;} catch(...){} 
													}
												}
											}


											try { delete pChannel->m_ppevents[i];} catch(...){} 
										}
										i++;
									}
									try{ delete [] pChannel->m_ppevents;} catch(...){}
								}

								pChannel->m_ppevents = NULL;
								pChannel->m_nNumEvents = 0;
							}

						}
					}


		}

		Sleep(1); //to not peg processor
	}  // while


	NewEvent.m_event.m_pszID = NULL;
	NewEvent.m_event.m_pszTitle = NULL;
	NewEvent.m_event.m_pszData = NULL;
	NewEvent.m_event.m_pszReconcileKey = NULL;
	NewEvent.m_event.m_pContextData = NULL;

	Sleep(100);
	//		remove all, since not active, want to purge list.
	if(
			(pdb!=NULL)&&(pdbConn!=NULL)
			&&(g_psentinel->m_settings.m_pszLiveEvents)
			&&(strlen(g_psentinel->m_settings.m_pszLiveEvents)>0)
			&&(pChannel)
			&&(pChannel->m_pszServerName)
			&&(strlen(pChannel->m_pszServerName)>0)
		)
	{

// delete main list
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d",
											g_psentinel->m_settings.m_pszLiveEvents,  // table name
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
										);
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
		if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
		{
			//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
		}
if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

// delete temp list
/*
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d",
											g_psentinel->m_settings.m_pszLiveEventsTemp,  // table name
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
										);
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
		if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
		{
			//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
		}
if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
*/

		// increment exchange mod counter. for events table.
		EnterCriticalSection(&g_psentinel->m_data.m_critIncrement);
		if (g_psentinel->m_data.IncrementDatabaseMods(g_psentinel->m_settings.m_pszLiveEvents, dberrorstring)<SENTINEL_SUCCESS)
		{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "Increment Error: %s", dberrorstring);  Sleep(50); //(Dispatch message)
			//**MSG
		}
		LeaveCriticalSection(&g_psentinel->m_data.m_critIncrement);

	}
	if((g_psentinel->m_settings.m_bUseControlModules)&&(g_psentinel->m_data.m_bControlModuleServiceThreadStarted)&&(!g_bKillThread)) // not kill thread because service module is already shut down.
	{
		int nChanges=-1;
		if(g_psentinel->m_settings.m_nCtrlModMaxQueue>0) 
		{
			EnterCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
			nChanges = g_psentinel->m_data.m_nNumChanges;
			LeaveCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
			if(nChanges>=g_psentinel->m_settings.m_nCtrlModMaxQueue)
			{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "discarding list change, queue is full (%d items)", nChanges); //  Sleep(250);//(Dispatch message)

				nChanges=1;
			}
			else nChanges = -1;
		}
		if(nChanges<0)
		{

#pragma message(alert "here, send a change to send xml to control modules for list changes")
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "creating list change on thread end"); //  Sleep(250);//(Dispatch message)

			lastlistdata.listchanged=0;				// incremented whenever list size changes
			lastlistdata.listdisplay=0;				// incremented whenever an event changes
			lastlistdata.listsyschange=0;			// incremented when list related values are changed. 
			lastlistdata.listcount=0;					// number of events in list 
			lastlistdata.liststate=0;

			CSentinelChange* pNewChange =  new CSentinelChange;
			if(pNewChange)
			{
				pNewChange->m_pObj = (void*) new CSentinelChannelObject;
				if(pNewChange->m_pObj)
				{
					pNewChange->m_nType = DLLCMD_ALIST;

					CSentinelChannelObject* pChgChannel = ((CSentinelChannelObject*)(pNewChange->m_pObj));

					*pChgChannel = *pChannel;
					pChgChannel->m_pszServerName = NULL;
					pChgChannel->m_pszDesc = NULL;
					pChgChannel->m_ppevents = NULL;
					pChgChannel->m_nNumEvents=0;

					pChgChannel->m_pptraffic=NULL;
					pChgChannel->m_nNumTraffic=0;

					pNewChange->m_nChannelID = pChannel->m_nChannelID;
					pNewChange->m_nHarrisListID = pChannel->m_nHarrisListID;

					pNewChange->m_pszServerName =  bu.XMLEncode(pChannel->m_pszServerName);
					pNewChange->m_pszDesc = bu.XMLEncode(pChannel->m_pszDesc);

					memcpy(&(pNewChange->m_list), &(lastlistdata), sizeof(tlistdata));

					pChgChannel->m_bChannelThreadStarted = false; // so it will not stall in deconstructor
					if(
							(!g_bKillThread) // not kill thread because service module is already shut down.
						&&(_beginthread(SentinelPublishChangeThread, 0, (void*)(pNewChange))==-1)
						)
					{
						//error.

						//**MSG
						g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start publish change thread."); //(Dispatch message)

						try{ delete (pNewChange); } catch(...){}
					}

					
				}
				else
				{
					try{ delete (pNewChange); } catch(...){}
				}
			}
		}

	}
	if((pdb)&&(pdbConn))
	{
// new:
//create table Channels (ID int NOT NULL identity(1,1), flags int NOT NULL, description varchar(256), server varchar(32), listid int, list_state int, list_changed int, list_display int, list_syschange int, list_count int, list_lookahead int, list_last_update float);
//create table Connections (server varchar(32) NOT NULL, description varchar(256), flags int, client varchar(32), server_time float, server_status int, server_lists int, server_basis int, server_changed int, server_last_update float);
						
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
list_state = -1, \
list_changed = -1, \
list_display = -1, \
list_syschange = -1, \
list_count = -1, \
list_lookahead = -1, \
list_last_update = -1 WHERE \
server = '%s' AND listid = %d",
			g_psentinel->m_settings.m_pszChannels?g_psentinel->m_settings.m_pszChannels:"Channels",
//			listdata.liststate,
//			listdata.listchanged,				// incremented whenever list size changes
//			listdata.listdisplay,				// incremented whenever an event changes
//			listdata.listsyschange,			// incremented when list related values are changed. 
//			listdata.listcount,					// number of events in list 
//			listdata.lookahead,					// lookahead value

//			(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
//			timestamp.millitm,

			pChannel->m_pszServerName,
			pChannel->m_nHarrisListID
			);
if(!g_psentinel->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
		if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
		{
			// error.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
		}
if(!g_psentinel->m_settings.m_bMultiConnectSQL) LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

	

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE criterion = 'List_Status' AND flag LIKE '%s:%02d%%'",
			g_psentinel->m_settings.m_pszExchange?g_psentinel->m_settings.m_pszExchange:"Exchange",
			pChannel->m_pszServerName,
			pChannel->m_nHarrisListID
			);
if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
		if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
		{
			// error.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
		}

if(!g_psentinel->m_settings.m_bMultiConnectSQL) 		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
	}

	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:list", "List management thread for %s:%d (%s) is ending.",
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
											pChannel->m_pszDesc
		);  
	
	Sleep(100); //(Dispatch message)

	int nTemp=0;
if(pChannel->m_pcritAPI)
{
	EnterCriticalSection(pChannel->m_pcritAPI);
}
	if((pChannel->m_ppAPIConn)&&(*(pChannel->m_ppAPIConn)))
	{
		(*(pChannel->m_ppAPIConn))->m_bListActive[nZeroList] = false;
/*
		nTemp=pChannel->m_pAPIConn->m_pList[nZeroList]->m_nEventCount-1;

		pChannel->m_pAPIConn->m_pList[nZeroList]->m_nEventCount = 0;

		if(pChannel->m_pAPIConn->m_pList[nZeroList]->m_ppEvents)
		{
			while(nTemp>=0)
			{
				if(pChannel->m_pAPIConn->m_pList[nZeroList]->m_ppEvents[nTemp])
					delete pChannel->m_pAPIConn->m_pList[nZeroList]->m_ppEvents[nTemp];
				pChannel->m_pAPIConn->m_pList[nZeroList]->m_ppEvents[nTemp] = NULL;
				nTemp--;
			}
//			delete [] pChannel->m_pAPIConn->m_pList[nZeroList]->m_ppEvents;
//			pChannel->m_pAPIConn->m_pList[nZeroList]->m_ppEvents = NULL;
		}
*/
	}
if(pChannel->m_pcritAPI)
{
	LeaveCriticalSection(pChannel->m_pcritAPI);
}
	nTemp=pChannel->m_nNumEvents-1;
	pChannel->m_nNumEvents = 0;

	if(pChannel->m_ppevents)
	{
		while(nTemp>=0)
		{

										if(pChannel->m_ppevents[nTemp]) 
										{
											if((g_psentinel->m_settings.m_bUseWorkingSet)&&(pChannel->m_ppevents[nTemp]->m_pszEncodedKey))
											{
												workingset* pItem = new workingset;
												if(pItem)
												{
													pItem->bAdd = FALSE;
													pItem->pszKey = (char*)malloc(strlen(pChannel->m_ppevents[nTemp]->m_pszEncodedKey)+1);
													if(pItem->pszKey)
													{
														strcpy(pItem->pszKey, pChannel->m_ppevents[nTemp]->m_pszEncodedKey);
														if(_beginthread(SentinelWorkingSetAdjustThread, 0, (void*)pItem)==-1)
														{
															//error.
															DWORD dw = GetLastError(); 
															char* pchError;
															FormatMessage( 
																	FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
																	NULL, 
																	dw,
																	MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
																	(LPTSTR) &pchError, 0, NULL);

															if(pchError)
															{
																//**MSG
																g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set adjust thread (delete).\r\nError code %d: %s\r\nNumber of changes: %d", dw, pchError, g_psentinel->m_data.m_nNumSetChanges); //(Dispatch message)
																try{ LocalFree(pchError); } catch(...){}
															}
															else
															{
																//**MSG
																g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set adjust thread (delete).\r\nError code %d\r\nNumber of changes: %d", dw, g_psentinel->m_data.m_nNumSetChanges); //(Dispatch message)
															}

															try { delete pItem;} catch(...){} 

														}
													}
													else
													{
														try { delete pItem;} catch(...){} 
													}
												}
											}


											try { delete pChannel->m_ppevents[nTemp];} catch(...){} 
										}

//			if(pChannel->m_ppevents[nTemp])
//				delete pChannel->m_ppevents[nTemp];
			pChannel->m_ppevents[nTemp] = NULL;
			nTemp--;
		}
		try { delete [] pChannel->m_ppevents;} catch(...){} 
		pChannel->m_ppevents = NULL;

	}


	pChannel->m_bChannelThreadStarted = false;
	_endthread();
}


#else //! SENTINEL_MANAGED_DB


void SentinelListThread(void* pvArgs)
{
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "SentinelChannelThread");  Sleep(250); //(Dispatch message)
	CSentinelChannelObject* pChannel = (CSentinelChannelObject*) pvArgs;
	if(pChannel==NULL)
	{
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "pChannel NULL, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}
	if(pChannel->m_bKillChannelThread) return;
/*
	bool* pbKillConnThread = &(pChannel->m_bKillChannelThread);//pChannel->m_pbKillConnThread;
	if(pbKillConnThread==NULL)  // the address, not what it points to
	{
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "g_psentinel NULL, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}
*/
	if(	pChannel->m_pAPIConn == NULL) // need this to do anything.  // could get it by looping through g_adc.... later.
	{
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "m_pAPIConn NULL, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}

	// lets set up some convenience vars;
	int nZeroList = pChannel->m_nHarrisListID-1;


	if((nZeroList<0)||(nZeroList>=MAXSYSLISTS)) // error on list number.
	{
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "nZeroList out of range, returning");  Sleep(250); //(Dispatch message)
		//**MSG
		return;
	}
	pChannel->m_bChannelThreadStarted = true;
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:list", "List management thread for %s:%d (%s) has begun.",
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
											pChannel->m_pszDesc
		); // Sleep(50); //(Dispatch message)

//bool readytotest = false;

	tlistdata* plistdata = &(pChannel->m_pAPIConn->m_ListData[nZeroList]);
//	TConnectionStatus* pstatus =  &(pChannel->m_pAPIConn->m_Status);
	CAList** ppList = &(pChannel->m_pAPIConn->m_pList[nZeroList]);
	CAList*  pList = pChannel->m_pAPIConn->m_pList[nZeroList];  // volatile, always reassign


	char dberrorstring[DB_ERRORSTRING_LEN];
	char szSQL[DB_SQLSTRING_MAXLEN];
	char pChannel->m_pszSource[MAX_MESSAGE_LENGTH];

	_snprintf(pChannel->m_pszSource, MAX_MESSAGE_LENGTH-1, "Sentinel:%s:%d (%s)",												
		pChannel->m_pszServerName, //server_name
		pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
		pChannel->m_pszDesc);

//m_bMultiConnectSQL

///////////////////////////////////////////////////////////////////////////
// datbase problems with sharing this connection:
	CDBUtil* pdb = g_psentinel->m_data.m_pdb;
	CDBconn* pdbConn = g_psentinel->m_data.m_pdbConn;
// so, changed to independent objects
/////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
// datbase problems with multiple connections!
	CDBUtil db;
//	CDBUtil* pdb = &db;

	if(g_psentinel->m_settings.m_bMultiConnectSQL)
	{
		pdb = &db;

		pdbConn = db.CreateNewConnection(g_psentinel->m_settings.m_pszDSN, g_psentinel->m_settings.m_pszUser, g_psentinel->m_settings.m_pszPW);
		if(pdbConn)
		{
			if(db.ConnectDatabase(pdbConn, dberrorstring)<DB_SUCCESS)
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "Error connecting to database for %s:%d (%s): %s",												
					pChannel->m_pszServerName, //server_name
					pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
					pChannel->m_pszDesc, 
					dberrorstring);
				g_psentinel->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Sentinel:list_database_connect", szSQL);  //(Dispatch message)
				pdbConn = g_psentinel->m_data.m_pdbConn;
			}
		}
		else
		{
			_snprintf(dberrorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s for %s:%d (%s)", 
				g_psentinel->m_settings.m_pszDSN, g_psentinel->m_settings.m_pszUser, g_psentinel->m_settings.m_pszPW,
					pChannel->m_pszServerName, //server_name
					pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
					pChannel->m_pszDesc
				); 
			g_psentinel->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Sentinel:list_database_connect", dberrorstring);  //(Dispatch message)
			pdbConn = g_psentinel->m_data.m_pdbConn;

			//**MSG
		}

	}
// so, changed back to single critsec conn
/////////////////////////////////////////////////////



	pChannel->m_pAPIConn->m_bListActive[nZeroList] = true;
//	unsigned long ulTick = GetTickCount();
//	unsigned long ulElapsedTick = GetTickCount();
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "SentinelChannelThread2");  Sleep(250); //(Dispatch message)

	tlistdata listdata;
	tlistdata lastlistdata;  // for comparison

  lastlistdata.listchanged=0;				// incremented whenever list size changes
  lastlistdata.listdisplay=0;				// incremented whenever an event changes
  lastlistdata.listsyschange=0;			// incremented when list related values are changed. 
  lastlistdata.listcount=0;					// number of events in list 
  lastlistdata.liststate=0;

	_timeb timestamp;

	// put the connection record in the exchange table
	if((pdb)&&(pdbConn))
	{
		_ftime( &timestamp );
// new:
//create table Channels (ID int NOT NULL identity(1,1), flags int NOT NULL, description varchar(256), server varchar(32), listid int, list_state int, list_changed int, list_display int, list_syschange int, list_count int, list_lookahead int, list_last_update float);
//create table Connections (server varchar(32) NOT NULL, description varchar(256), flags int, client varchar(32), server_time float, server_status int, server_lists int, server_basis int, server_changed int, server_last_update float);
						
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
list_state = -2, \
list_changed = -1, \
list_display = -1, \
list_syschange = -1, \
list_count = -1, \
list_lookahead = -1, \
list_last_update = %d%03d WHERE \
server = '%s' AND listid = %d",
			g_psentinel->m_settings.m_pszChannels?g_psentinel->m_settings.m_pszChannels:"Channels",
//			listdata.liststate,
//			listdata.listchanged,				// incremented whenever list size changes
//			listdata.listdisplay,				// incremented whenever an event changes
//			listdata.listsyschange,			// incremented when list related values are changed. 
//			listdata.listcount,					// number of events in list 
//			listdata.lookahead,					// lookahead value

			(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
			timestamp.millitm,

			pChannel->m_pszServerName,
			pChannel->m_nHarrisListID
			);
EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
		if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
		{
			// error.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);  Sleep(50); //(Dispatch message)
		}
LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (criterion, flag, mod) VALUES ('List_Status', '%s:%02d|%d%03d|%d|%d|%d|%d', %d)",
			g_psentinel->m_settings.m_pszExchange?g_psentinel->m_settings.m_pszExchange:"Exchange",
			pChannel->m_pszServerName,
			pChannel->m_nHarrisListID,
			(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
			timestamp.millitm,
			lastlistdata.listchanged,				// incremented whenever list size changes
			lastlistdata.listdisplay,				// incremented whenever an event changes
			lastlistdata.listsyschange,			// incremented when list related values are changed. 
			lastlistdata.listcount,					// number of events in list 
			lastlistdata.liststate
			);
	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
		if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
		{
				// error.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);  Sleep(50); //(Dispatch message)
		}

	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
	}

	bool bconfirmed = true;


	while(
					(!pChannel->m_bKillChannelThread)//&&((*pbKillConnThread)!=true))
				&&(g_psentinel->m_data.m_key.m_bValid)  // must have a valid license
				&&(
						(!g_psentinel->m_data.m_key.m_bExpires)
					||((g_psentinel->m_data.m_key.m_bExpires)&&(!g_psentinel->m_data.m_key.m_bExpired))
					||((g_psentinel->m_data.m_key.m_bExpires)&&(g_psentinel->m_data.m_key.m_bExpireForgiveness)&&(g_psentinel->m_data.m_key.m_ulExpiryDate+g_psentinel->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
						(!g_psentinel->m_data.m_key.m_bMachineSpecific)
					||((g_psentinel->m_data.m_key.m_bMachineSpecific)&&(g_psentinel->m_data.m_key.m_bValidMAC))
					)
				)
	{
//		EnterCriticalSection(&g_adc.m_crit);
//		g_adc.GetStatusData(pConn->m_pAPIConn);
//		LeaveCriticalSection(&g_adc.m_crit);
		if((pChannel->m_pAPIConn)&&(pChannel->m_pAPIConn->m_Status > 0))  // have to be connected.
		{
			// snapshot the live list data
			if((!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true))
			{
				EnterCriticalSection(&g_adc.m_crit);
				memcpy(&listdata, plistdata, sizeof(tlistdata));
				LeaveCriticalSection(&g_adc.m_crit);
			}

			bool blistsizechanged = false;
			
			if(listdata.listcount!=lastlistdata.listcount) blistsizechanged=true;

			if(
				  (g_psentinel->m_settings.m_bListStatusToDB)
				&&(pChannel->m_pAPIConn)
				&&(!pChannel->m_bKillChannelThread)
				&&(
						(listdata.listchanged!=lastlistdata.listchanged)
//					||(listdata.listdisplay!=lastlistdata.listdisplay)   //just list changes, not event changes - this is just list status to db
					||(listdata.listsyschange!=lastlistdata.listsyschange)
					||(listdata.listcount!=lastlistdata.listcount)
					||(listdata.liststate!=lastlistdata.liststate)
//					||(!bconfirmed)
					)
				)//&&((*pbKillConnThread)!=true))
			{
//				if(bconfirmed) bconfirmed=false;

				if((pdb)&&(pdbConn))
				{
					_ftime( &timestamp );

					if(g_psentinel->m_settings.m_bStatusInTables)
					{
// new:
//create table Channels (ID int NOT NULL identity(1,1), flags int NOT NULL, description varchar(256), server varchar(32), listid int, list_state int, list_changed int, list_display int, list_syschange int, list_count int, list_lookahead int, list_last_update float);
//create table Connections (server varchar(32) NOT NULL, description varchar(256), flags int, client varchar(32), server_time float, server_status int, server_lists int, server_basis int, server_changed int, server_last_update float);
						
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
list_state = %d, \
list_changed = %d, \
list_display = %d, \
list_syschange = %d, \
list_count = %d, \
list_lookahead = %d, \
list_last_update = %d%03d WHERE \
server = '%s' AND listid = %d",
							g_psentinel->m_settings.m_pszChannels?g_psentinel->m_settings.m_pszChannels:"Channels",
							listdata.liststate,
							listdata.listchanged,				// incremented whenever list size changes
							listdata.listdisplay,				// incremented whenever an event changes
							listdata.listsyschange,			// incremented when list related values are changed. 
							listdata.listcount,					// number of events in list 
							listdata.lookahead,					// lookahead value

							(unsigned long)(timestamp.time),// unixtime - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
							timestamp.millitm,

							pChannel->m_pszServerName,
							pChannel->m_nHarrisListID
							);
		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
						if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
							// error.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);  Sleep(50); //(Dispatch message)
						}
		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

					}


					if(g_psentinel->m_settings.m_bListStatusInExchange)
					{
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET flag = '%s:%02d|%d%03d|%d|%d|%d|%d', mod = %d WHERE \
criterion = 'List_Status' AND flag LIKE '%s:%02d%%'",
							g_psentinel->m_settings.m_pszExchange?g_psentinel->m_settings.m_pszExchange:"Exchange",
							pChannel->m_pszServerName,
							pChannel->m_nHarrisListID,
							(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
							timestamp.millitm,
							listdata.listchanged,				// incremented whenever list size changes
							listdata.listdisplay,				// incremented whenever an event changes
							listdata.listsyschange,			// incremented when list related values are changed. 
							listdata.listcount,					// number of events in list 
							listdata.liststate,
							pChannel->m_pszServerName,
							pChannel->m_nHarrisListID
							);
					EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
						if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
								// error.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);  Sleep(50); //(Dispatch message)
						}

					LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
					}
				}
			}


//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread checking list change", pChannel->m_pszDesc);  Sleep(50); //(Dispatch message)
			if(
				  (
					  (listdata.listchanged!=lastlistdata.listchanged)
					||(listdata.listdisplay!=lastlistdata.listdisplay)
					||(!bconfirmed)
					)
				&&(pChannel->m_pAPIConn)&&(!pChannel->m_bKillChannelThread)
				)//&&((*pbKillConnThread)!=true)) // just do the event change one as well for now.
			{


				bool bStatusChange = false;
				bool bPlaying = false;
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread list change, getting events", pChannel->m_pszDesc);  Sleep(50); //(Dispatch message)
				// the list has changed, need to get all the events again  (will take care of any event changes as well)


				double dblEventTimeTolerance = ((double)(g_psentinel->m_settings.m_nEventTimeToleranceMS))/1000.0;
				if(dblEventTimeTolerance<=0) dblEventTimeTolerance = 0.500;

				// first need to get events until playing, get those in the DB right away.

				int nNumEvents = -1;
				pChannel->m_bChannelThreadGettingEvents = true;

				if(!(listdata.liststate&(1<<LISTISPLAYING)))
				{
					bPlaying = false;
					EnterCriticalSection(&g_adc.m_crit);
					nNumEvents = g_adc.GetEvents(pChannel->m_pAPIConn,
						pChannel->m_nHarrisListID, 
						0, //start at the top.
						(g_psentinel->m_settings.m_ulEventsPerRequest!=0)?(g_psentinel->m_settings.m_ulEventsPerRequest):(g_psentinel->m_settings.m_bUseListCount?listdata.listcount:listdata.lookahead),
						(ADC_GETEVENTS_BREAKONCHANGE|ADC_GETEVENTS_LIST|ADC_GETEVENTS_USEOFFSET)|(g_psentinel->m_settings.m_bUseListCount?(ADC_GETEVENTS_FULLLIST):0),
						ADC_DEFAULT_TIMEOUT,
						&bStatusChange,
						&listdata
						);
					LeaveCriticalSection(&g_adc.m_crit);
				}
				else
				{
					bPlaying = true;
					EnterCriticalSection(&g_adc.m_crit);
					nNumEvents = g_adc.GetEventsUntilPlaying(pChannel->m_pAPIConn,
						pChannel->m_nHarrisListID, 
						0, //start at the top.
						g_psentinel->m_settings.m_bUseListCount?listdata.listcount:listdata.lookahead,
						(ADC_GETEVENTS_BREAKONCHANGE|ADC_GETEVENTS_LIST|ADC_GETEVENTS_USEOFFSET)|(g_psentinel->m_settings.m_bUseListCount?(ADC_GETEVENTS_FULLLIST):0),
						ADC_DEFAULT_TIMEOUT,
						&bStatusChange,
						&listdata
						);
					LeaveCriticalSection(&g_adc.m_crit);
				}
				pChannel->m_bChannelThreadGettingEvents = false;



/////////////////////////////////////////////////////////////////////////////////////////
/////////////// new method using temp table.

			// first, clear out temp table. for just this list.

				bool bSQLerror = false;
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d",
					g_psentinel->m_settings.m_pszLiveEventsTemp,
					pChannel->m_pszServerName,
					pChannel->m_nHarrisListID
					);

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "SentinelChannelThread clear temp: %s", szSQL);  Sleep(50); //(Dispatch message)
EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
				if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
				{
					//**MSG
					bSQLerror = true;

g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);  Sleep(50); //(Dispatch message)
				}
LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

				if(bSQLerror) continue;  // cant do anything if table not cleared.

				if((!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn)) 
					pList = pChannel->m_pAPIConn->m_pList[nZeroList];  // volatile, always reassign
				else 
					pList = NULL;

				if((nNumEvents != ADC_NO_EVENTS)&&(pList)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))//&&((*pbKillConnThread)!=true))
				{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread list change, %s%d%s, entering critical", pChannel->m_pszDesc, bPlaying?"playing event at ":"", nNumEvents, bPlaying?"":" events");  Sleep(50); //(Dispatch message)
					EnterCriticalSection(&(pList->m_crit));  // lock the list out
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread list change, in critical", pChannel->m_pszDesc);  Sleep(50); //(Dispatch message)
					if((nNumEvents>=ADC_NO_PLAYINGEVENT)&&(pList)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))//&&((*pbKillConnThread)!=true))
					{
						// deal with database.
						if((pdb!=NULL)&&(pdbConn!=NULL)&&(g_psentinel->m_settings.m_pszLiveEvents)&&(strlen(g_psentinel->m_settings.m_pszLiveEvents)>0)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))
						{
//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread dealing with db", pChannel->m_pszDesc);  Sleep(50); //(Dispatch message)


							double dblLastPrimaryEventTime = -1.0;
							double dblLastEventTime = -1.0;
							unsigned short usLastPrimaryOnAirJulianDate = 0xffff;
							unsigned long ulLastPrimaryOnAirTimeMS = 0xffffffff;
							unsigned long ulLastPrimaryOnAirDurMS = 0xffffffff;
							double dblServerTime = ((double)(pChannel->m_pAPIConn->m_usRefJulianDate - 25567))*86400.0   // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
											+ (double)pChannel->m_pAPIConn->m_ulRefTimeMS/1000.0;

							double dblLastCalcPrimaryEventTime = -1.0;
							double dblLastCalcEventTime = -1.0;
							int nLastParentPos = -1;
							char chLastParentID[64];
							strcpy(chLastParentID, "");



//CREATE TABLE Events (itemid int identity(1,1) NOT NULL, server_name varchar(32) NOT NULL, server_list int NOT NULL, list_id int NOT NULL, event_id varchar(32) NOT NULL, event_clip varchar(64) NOT NULL, event_title varchar(64) NOT NULL, event_data varchar(4096) NULL, event_type int NOT NULL, event_status int NOT NULL, event_time_mode int NOT NULL, event_start decimal(20,3) NOT NULL, event_duration int NOT NULL, event_last_update decimal(20,3) NOT NULL, event_calc_start decimal(20,3) NOT NULL, event_position int NOT NULL, parent_id varchar(32) NULL, parent_position int NULL, parent_start decimal(20,3) NULL, parent_calc_start decimal(20,3) NULL, parent_duration int, parent_calc_end decimal(20,3), event_calc_end decimal(20,3), app_data varchar(512) NULL, app_data_aux int NULL); /* ADD CONSTRAINT IX_Event UNIQUE (server_name, list_id, app_data_aux);*/
//CREATE TABLE Events_Temp (itemid int identity(1,1) NOT NULL, server_name varchar(32) NOT NULL, server_list int NOT NULL, list_id int NOT NULL, event_id varchar(32) NOT NULL, event_clip varchar(64) NOT NULL, event_title varchar(64) NOT NULL, event_data varchar(4096) NULL, event_type int NOT NULL, event_status int NOT NULL, event_time_mode int NOT NULL, event_start decimal(20,3) NOT NULL, event_duration int NOT NULL, event_last_update decimal(20,3) NOT NULL, event_calc_start decimal(20,3) NOT NULL, event_position int NOT NULL, parent_id varchar(32) NULL, parent_position int NULL, parent_start decimal(20,3) NULL, parent_calc_start decimal(20,3) NULL, parent_duration int, parent_calc_end decimal(20,3), event_calc_end decimal(20,3), app_data varchar(512) NULL, app_data_aux int NULL);



							int n=0;

							if(nNumEvents>ADC_NO_PLAYINGEVENT)
							{

/////////////////////////////////////////////////////////////////////////////////////////
/////////////// new method using temp table.

							// then, insert everything into temp table.
								// get the next primary index, which we are guaranteed to have.  This ensures any playing secondaries are also covered in this call.

								if(bPlaying)
								{
									EnterCriticalSection(&g_adc.m_crit);
										nNumEvents = g_adc.GetNextPrimaryIndex(pChannel->m_pAPIConn,
										pChannel->m_nHarrisListID, 
										nNumEvents //start at playing event.
										);
									LeaveCriticalSection(&g_adc.m_crit);
									nNumEvents++; // the number, not the last index, so we increment
								}

								// following line does not check status change, we want to take whatever we got, even if status changes
pChannel->m_bChannelThreadPublishingTempEvents = true;

								while((!pChannel->m_bKillChannelThread)&&(n<nNumEvents)&&(pChannel->m_pAPIConn))//&&((*pbKillConnThread)!=true))
								{

	//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
									if((pList->m_ppEvents)&&(pList->m_ppEvents[n])&&(pList->m_ppEvents[n]->m_pszID)&&(strlen(pList->m_ppEvents[n]->m_pszID)))
									{

										double dblEventTime = 0.0; 
										double dblCalcEventTime = 0.0; 
										unsigned short usOnAirJulianDate = pList->m_ppEvents[n]->m_usOnAirJulianDate;

								//		if(usOnAirJulianDate == 0xffff)  // can't trust event based julian date, have to use server date
	//									{
	//										_timeb timestamp;
	//										_ftime(&timestamp);

	//										usOnAirJulianDate = pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.

	// move time calc to below julian recalc
	//										dblEventTime = ((double)(usOnAirJulianDate - 25567))*86400  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
	//											+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
	//									}
	/*
										else
										{
											dblEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400    // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
												+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
										}
	*/											
	//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s] again", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)



										if(usLastPrimaryOnAirJulianDate != 0xffff)
										{
	//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s] again1a", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
											usOnAirJulianDate = usLastPrimaryOnAirJulianDate;
										}
										else
										{
	//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s] again1b", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
											usOnAirJulianDate = pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.
										}

	//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s] again2", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)

										unsigned short usType = pList->m_ppEvents[n]->m_usType;

										if(usType&SECONDARYEVENT)
										{
											if(ulLastPrimaryOnAirTimeMS!=0xffffffff)
											{
												if(
														((usType&0xff) == SECBACKAVEVENT)
													||((usType&0xff) == SECBACKTIMEGPI)
													||((usType&0xff) == SECBACKSYSTEM)
													)
												{
													dblEventTime = ((dblLastPrimaryEventTime <0.0)?0.0:dblLastPrimaryEventTime)
														- (pList->m_ppEvents[n]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)?(0.000):((double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0);
													
													dblCalcEventTime = ((dblLastCalcPrimaryEventTime <0.0)?0.0:dblLastCalcPrimaryEventTime)
														- (pList->m_ppEvents[n]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)?(0.000):((double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0);

												}
												else
												{
													if((pList->m_ppEvents[n]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)||(((usType&0xff)>=SECCOMMENT)&&((usType&0xff)<=INVALIDEVENT)))
													{ //prob a commment
														dblEventTime = ((dblLastEventTime<0.0)?((dblLastPrimaryEventTime<0.0)?0.0:dblLastPrimaryEventTime):(dblLastEventTime))
															+ ((((usType&0xff)>=SECCOMMENT)&&((usType&0xff)<=INVALIDEVENT))?(0.001):(0.000)); // a comment, for instance, has TIME_NOT_DEFINED. but we want it to come after theprimary it belongs to, so we give it .001 seconds
														
														dblCalcEventTime = ((dblLastCalcEventTime<0.0)?((dblLastCalcPrimaryEventTime<0.0)?0.0:dblLastCalcPrimaryEventTime):(dblLastCalcEventTime))
															+ ((((usType&0xff)>=SECCOMMENT)&&((usType&0xff)<=INVALIDEVENT))?(0.001):(0.000)); // a comment, for instance, has TIME_NOT_DEFINED. but we want it to come after theprimary it belongs to, so we give it .001 seconds
													}
													else
													{
														dblEventTime = ((dblLastPrimaryEventTime<0.0)?0.0:dblLastPrimaryEventTime)
															+ ((double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0) 
															+ (pList->m_ppEvents[n]->m_ulOnAirTimeMS==0?(0.001):(0.000)); 
													
														dblCalcEventTime = ((dblLastCalcPrimaryEventTime<0.0)?0.0:dblLastCalcPrimaryEventTime)
															+ ((double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0) 
															+ (pList->m_ppEvents[n]->m_ulOnAirTimeMS==0?(0.001):(0.000)); 
													}
												}
												usOnAirJulianDate = (usLastPrimaryOnAirJulianDate!=0xffff)?usLastPrimaryOnAirJulianDate:pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.

												if(pList->m_ppEvents[n]->m_ulOnAirTimeMS + ulLastPrimaryOnAirTimeMS >= 86400000 )
												{
													//wrapped days.
													usOnAirJulianDate++;
												}
											}
											else
											{
												// no last primary
												dblEventTime = (pList->m_ppEvents[n]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)?(0.001):((double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0);
												dblCalcEventTime = (pList->m_ppEvents[n]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)?(0.001):((double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0);
											}
										}
										else // its a primary
										{

											if(n<listdata.lookahead)
											{
												if(ulLastPrimaryOnAirTimeMS!=0xffffffff)
												{
													if(ulLastPrimaryOnAirTimeMS > pList->m_ppEvents[n]->m_ulOnAirTimeMS)
													{
														// we wrapped days.
														usOnAirJulianDate++;
													}
												}

												dblEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
													+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
											}
											else
											{
												// we are beyond the lookahead, need to deal with estimated times, not actual as reported by harris.
												dblEventTime = dblLastPrimaryEventTime + (double)ulLastPrimaryOnAirDurMS/1000.0;
												// we dont care about ulLastPrimaryOnAirTimeMS for caluclations after this point
												// but we do care about julian day ... so... ok actually we dont care.  that was for calc anyway.
											}

/*
enum teventrunstatus {
     eventdone,
     eventrunning,
     playednextvideo,
     eventprerolled,
     eventpostrolled,
     eventidtitle,
     eventstandbyon,
     notplayed,
     eventranshort,
     eventskipped,
     eventpreped,
     eventnotswitched,
     eventpreviewed,
     rollingnext,
     eventshort,
     eventlong };
*/
											if(
												  (pList->m_ppEvents[n]->m_usStatus&((1<<eventdone)|(1<<eventpostrolled)|(1<<eventrunning)|(1<<notplayed)|(1<<eventskipped)))
												||(pList->m_ppEvents[n]->m_ulControl&(1<<autotimed))// hard start
												)
												dblCalcEventTime = dblEventTime;
											else
												dblCalcEventTime = ((dblLastCalcPrimaryEventTime<0.0)?dblEventTime:dblLastCalcPrimaryEventTime) + ((ulLastPrimaryOnAirDurMS == 0xffffffff)?0.0:(double)ulLastPrimaryOnAirDurMS/1000.0);

											ulLastPrimaryOnAirTimeMS = pList->m_ppEvents[n]->m_ulOnAirTimeMS;
											ulLastPrimaryOnAirDurMS = pList->m_ppEvents[n]->m_ulDurationMS;
											dblLastPrimaryEventTime = dblEventTime;
											usLastPrimaryOnAirJulianDate = usOnAirJulianDate;

											dblLastCalcPrimaryEventTime = dblCalcEventTime;
											nLastParentPos = n;
											strcpy(chLastParentID, pList->m_ppEvents[n]->m_pszID);

										}

										dblLastCalcEventTime = dblCalcEventTime;
										dblLastEventTime = dblEventTime;
	//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: %d [%s] again3", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
//g_psentinel->m_msgr.DM(MSG_ICONUSER1, NULL, "Sentinel:debug", "before encode -> Event %d", n);//  Sleep(50); //(Dispatch message)

										char* pchID = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszID);
										char* pchTitle = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszTitle);
										char* pchRecKey = NULL;
										char* pchData = NULL;
										char* pchParentID = pdb->EncodeQuotes(chLastParentID);
										
										if(pList->m_ppEvents[n]->m_pszData) 
											pchData = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszData);
										if(pList->m_ppEvents[n]->m_pszReconcileKey) 
											pchRecKey = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszReconcileKey);

//g_psentinel->m_msgr.DM(MSG_ICONUSER1, NULL, "Sentinel:debug", "before format -> Event %d", n);//  Sleep(50); //(Dispatch message)

										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
server_name, \
server_list, \
list_id, \
event_id, \
event_clip, \
event_title, \
%s\
event_type, \
event_status, \
event_time_mode, \
event_start, \
event_duration, \
event_last_update, \
event_calc_start, \
event_position, \
parent_id, \
parent_position, \
parent_start, \
parent_calc_start, \
parent_duration, \
parent_calc_end, \
event_calc_end) VALUES ('%s', %d, %d, '%s', '%s', '%s', %s%s%s%d, %d, %d, %.3f, %d, %.3f, %.3f, %d, '%s', %d, %.3f, %.3f, %d, %.3f, %.3f)",
												g_psentinel->m_settings.m_pszLiveEventsTemp,  // table name
												pList->m_ppEvents[n]->m_pszData?"event_data, ":"", //event_data (exists)
												pChannel->m_pszServerName, //server_name
												pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
												pChannel->m_nChannelID, // list_id - internal lookup channel number
												pchRecKey?pchRecKey:(pList->m_ppEvents[n]->m_pszReconcileKey?pList->m_ppEvents[n]->m_pszReconcileKey:""), //event_id
												pchID?pchID:pList->m_ppEvents[n]->m_pszID, //event_clip  (must not be null)
												pchTitle?pchTitle:"", //event_title
												pchData?"'":"", //event_data (exists)
												pchData?pchData:"", //event_data  
												pchData?"', ":"", //event_data (exists)
												pList->m_ppEvents[n]->m_usType, //event_type
												pList->m_ppEvents[n]->m_usStatus, //event_status
												pList->m_ppEvents[n]->m_ulControl, //event_time_mode
												dblEventTime, //event_start
												pList->m_ppEvents[n]->m_ulDurationMS, //event_duration
												dblServerTime, //event_last_update
												dblCalcEventTime, //calculated event time
												n, // position.
												pchParentID?pchParentID:chLastParentID, //parent ID
												nLastParentPos, //parent pos
												dblLastPrimaryEventTime, // scheduled time of parent event
												dblLastCalcPrimaryEventTime, //calculated event time of parent event
												((ulLastPrimaryOnAirDurMS == 0xffffffff)?-1:ulLastPrimaryOnAirDurMS),
												(dblLastCalcPrimaryEventTime + ((ulLastPrimaryOnAirDurMS == 0xffffffff)?(-0.0):(double)ulLastPrimaryOnAirDurMS/1000.0)),
												(((usType&0xff)<SECCOMMENT)?(dblCalcEventTime + (double)pList->m_ppEvents[n]->m_ulDurationMS/1000.0):(dblLastCalcPrimaryEventTime + (double)ulLastPrimaryOnAirDurMS/1000.0))
												);
									
if(g_psentinel->m_settings.m_bDebugInsertSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "SentinelChannelThread event insert A: %s", szSQL);  //Sleep(250); //(Dispatch message)
//g_psentinel->m_msgr.DM(MSG_ICONUSER1, NULL, "Sentinel:debug", "before insert -> Event %d", n);//  Sleep(50); //(Dispatch message)

		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
										if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
										{
											bSQLerror =  true;

											//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
										}
//g_psentinel->m_msgr.DM(MSG_ICONUSER1, NULL, "Sentinel:debug", "after insert -> Event %d", n);//  Sleep(50); //(Dispatch message)
		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
										
										if(pchID) free(pchID);
										if(pchTitle) free(pchTitle);
										if(pchRecKey) free(pchRecKey);
										if(pchData) free(pchData);
										if(pchParentID) free(pchParentID);
									
									}//if((pList->m_ppEvents)&&(pList->m_ppEvents[n])&&(pList->m_ppEvents[n]->m_pszID)&&(strlen(pList->m_ppEvents[n]->m_pszID)))
									n++;
//g_psentinel->m_msgr.DM(MSG_ICONUSER1, NULL, "Sentinel:debug", "-> Event %d", n);//  Sleep(50); //(Dispatch message)
								}//while((!pChannel->m_bKillChannelThread)&&(n<nNumEvents)&&(pChannel->m_pAPIConn))//&&((*pbKillConnThread)!=true))

pChannel->m_bChannelThreadPublishingTempEvents = false;

// dealt with events up to playing, lets update them immediately


// dump tables
if(g_psentinel->m_settings.m_bDebugLists)
{
	CRecordset* prs;

	if(g_psentinel->m_settings.m_nDebugListTopCount>0)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top %d * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_nDebugListTopCount,
						g_psentinel->m_settings.m_pszLiveEventsTemp,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}
	else
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_pszLiveEventsTemp,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}

	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
	prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
	if(prs == NULL)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
	}
	else
	{
		int nIndex = 0;
		CString szOut="\r\nInitial event retrive, temp table filled.\r\nA: TEMP Table:";
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);  //Sleep(20); //(Dispatch message)
		while ((!prs->IsEOF()))
		{
			CString szTemp="";
			szOut = "     ";
			try
			{
				prs->GetFieldValue("itemid", szTemp);  //HARDCODE
				szOut+="itemid="; szOut+=szTemp;
				prs->GetFieldValue("event_key", szTemp);  //HARDCODE
				szOut+="\tevent_key="; szOut+=szTemp;
				prs->GetFieldValue("server_name", szTemp);  //HARDCODE
				szOut+="\tserver_name="; szOut+=szTemp;
				prs->GetFieldValue("server_list", szTemp);  //HARDCODE
				szOut+="\tserver_list="; szOut+=szTemp;
				prs->GetFieldValue("list_id", szTemp);  //HARDCODE
				szOut+="\tlist_id="; szOut+=szTemp;
				prs->GetFieldValue("event_id", szTemp);  //HARDCODE
				szOut+="\tevent_id="; szOut+=szTemp;
				prs->GetFieldValue("event_clip", szTemp);  //HARDCODE
				szOut+="\tevent_clip="; szOut+=szTemp;
				prs->GetFieldValue("event_title", szTemp);  //HARDCODE
				szOut+="\tevent_title="; szOut+=szTemp;
				prs->GetFieldValue("event_data", szTemp);  //HARDCODE
				szOut+="\tevent_data="; szOut+=szTemp;
				prs->GetFieldValue("event_type", szTemp);  //HARDCODE
				szOut+="\tevent_type="; szOut+=szTemp;
				prs->GetFieldValue("event_status", szTemp);  //HARDCODE
				szOut+="\tevent_status="; szOut+=szTemp;
				prs->GetFieldValue("event_time_mode", szTemp);  //HARDCODE
				szOut+="\tevent_time_mode="; szOut+=szTemp;
				prs->GetFieldValue("event_start", szTemp);  //HARDCODE
				szOut+="\tevent_start="; szOut+=szTemp;
				prs->GetFieldValue("event_duration", szTemp);  //HARDCODE
				szOut+="\tevent_duration="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_start", szTemp);  //HARDCODE
				szOut+="\tevent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_end", szTemp);  //HARDCODE
				szOut+="\tevent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("event_position", szTemp);  //HARDCODE
				szOut+="\tevent_position="; szOut+=szTemp;
				prs->GetFieldValue("event_last_update", szTemp);  //HARDCODE
				szOut+="\tevent_last_update="; szOut+=szTemp;
				prs->GetFieldValue("parent_duration", szTemp);  //HARDCODE
				szOut+="\tparent_duration="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_start", szTemp);  //HARDCODE
				szOut+="\tparent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_end", szTemp);  //HARDCODE
				szOut+="\tparent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("parent_position", szTemp);  //HARDCODE
				szOut+="\tparent_position="; szOut+=szTemp;
				prs->GetFieldValue("app_data", szTemp);  //HARDCODE
				szOut+="\tapp_data="; szOut+=szTemp;
				prs->GetFieldValue("app_data_aux", szTemp);  //HARDCODE
				szOut+="\tapp_data_aux="; szOut+=szTemp;
				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(50); //(Dispatch message)
			}
			catch(...)
			{
			}
			prs->MoveNext();
			nIndex++;
		}

		prs->Close();

		delete prs;
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "A: TEMP table: %d items retrieved\r\n", nIndex);//  Sleep(50); //(Dispatch message)

	}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

}


// dump tables
if(g_psentinel->m_settings.m_bDebugLists)
{
	CRecordset* prs;
	if(g_psentinel->m_settings.m_nDebugListTopCount>0)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top %d * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_nDebugListTopCount,
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}
	else
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}


	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
	prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
	if(prs == NULL)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
	}
	else
	{
		int nIndex = 0;
		CString szOut="\r\nA: EVENT Table:";
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut); // Sleep(20); //(Dispatch message)
		while ((!prs->IsEOF()))
		{
			CString szTemp="";
			szOut = "     ";
			try
			{
				prs->GetFieldValue("itemid", szTemp);  //HARDCODE
				szOut+="itemid="; szOut+=szTemp;
				prs->GetFieldValue("event_key", szTemp);  //HARDCODE
				szOut+="\tevent_key="; szOut+=szTemp;
				prs->GetFieldValue("server_name", szTemp);  //HARDCODE
				szOut+="\tserver_name="; szOut+=szTemp;
				prs->GetFieldValue("server_list", szTemp);  //HARDCODE
				szOut+="\tserver_list="; szOut+=szTemp;
				prs->GetFieldValue("list_id", szTemp);  //HARDCODE
				szOut+="\tlist_id="; szOut+=szTemp;
				prs->GetFieldValue("event_id", szTemp);  //HARDCODE
				szOut+="\tevent_id="; szOut+=szTemp;
				prs->GetFieldValue("event_clip", szTemp);  //HARDCODE
				szOut+="\tevent_clip="; szOut+=szTemp;
				prs->GetFieldValue("event_title", szTemp);  //HARDCODE
				szOut+="\tevent_title="; szOut+=szTemp;
				prs->GetFieldValue("event_data", szTemp);  //HARDCODE
				szOut+="\tevent_data="; szOut+=szTemp;
				prs->GetFieldValue("event_type", szTemp);  //HARDCODE
				szOut+="\tevent_type="; szOut+=szTemp;
				prs->GetFieldValue("event_status", szTemp);  //HARDCODE
				szOut+="\tevent_status="; szOut+=szTemp;
				prs->GetFieldValue("event_time_mode", szTemp);  //HARDCODE
				szOut+="\tevent_time_mode="; szOut+=szTemp;
				prs->GetFieldValue("event_start", szTemp);  //HARDCODE
				szOut+="\tevent_start="; szOut+=szTemp;
				prs->GetFieldValue("event_duration", szTemp);  //HARDCODE
				szOut+="\tevent_duration="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_start", szTemp);  //HARDCODE
				szOut+="\tevent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_end", szTemp);  //HARDCODE
				szOut+="\tevent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("event_position", szTemp);  //HARDCODE
				szOut+="\tevent_position="; szOut+=szTemp;
				prs->GetFieldValue("event_last_update", szTemp);  //HARDCODE
				szOut+="\tevent_last_update="; szOut+=szTemp;
				prs->GetFieldValue("parent_duration", szTemp);  //HARDCODE
				szOut+="\tparent_duration="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_start", szTemp);  //HARDCODE
				szOut+="\tparent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_end", szTemp);  //HARDCODE
				szOut+="\tparent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("parent_position", szTemp);  //HARDCODE
				szOut+="\tparent_position="; szOut+=szTemp;
				prs->GetFieldValue("app_data", szTemp);  //HARDCODE
				szOut+="\tapp_data="; szOut+=szTemp;
				prs->GetFieldValue("app_data_aux", szTemp);  //HARDCODE
				szOut+="\tapp_data_aux="; szOut+=szTemp;
				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(50); //(Dispatch message)
			}
			catch(...)
			{
			}
			prs->MoveNext();
			nIndex++;
		}

		prs->Close();

		delete prs;
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "A: EVENT table: %d items retrieved\r\n", nIndex);//  Sleep(50); //(Dispatch message)

	}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

}


								pChannel->m_bChannelThreadPublishingEvents = true;


								if((!blistsizechanged)&&(!g_psentinel->m_settings.m_bDisableEventMatchOnPosition)) // go by list position
								{
//if(g_bDebug) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "no list size change");
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET Events.list_id = Temp.list_id, Events.event_id = Temp.event_id, Events.event_data = Temp.event_data, Events.event_type = Temp.event_type, Events.event_status = Temp.event_status, Events.event_time_mode = Temp.event_time_mode, Events.event_start = Temp.event_start, Events.event_duration = Temp.event_duration, Events.event_last_update = Temp.event_last_update, Events.event_calc_start = Temp.event_calc_start, Events.event_position = Temp.event_position, Events.parent_id = Temp.parent_id, Events.parent_position = Temp.parent_position, Events.parent_start = Temp.parent_start, Events.parent_calc_start = Temp.parent_calc_start, Events.parent_duration = Temp.parent_duration, Events.parent_calc_end = Temp.parent_calc_end, Events.event_calc_end = Temp.event_calc_end, Events.app_data = Temp.app_data, Events.app_data_aux = Temp.app_data_aux \
FROM %s as Events, %s as Temp WHERE Events.server_name = Temp.server_name and Events.server_list = Temp.server_list and Events.event_clip = Temp.event_clip and Events.event_title = Temp.event_title and Events.event_position = Temp.event_position",
										g_psentinel->m_settings.m_pszLiveEvents,
										g_psentinel->m_settings.m_pszLiveEvents,
										g_psentinel->m_settings.m_pszLiveEventsTemp
										);
//if(readytotest) {bar.Format("Update: No list size change, going by app_data_aux: \r\n\r\n%s", szSQL); AfxMessageBox(bar);}
if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread 01 NLC update from temp: %s", szSQL); // Sleep(50); //(Dispatch message)
	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
									if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
									{
										bSQLerror = true;
									//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
									}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
//if(readytotest) {bar.Format("Update: Executed: \r\n\r\n%s", szSQL); AfxMessageBox(bar);}
								}
								else // otherwise go by time (and status)
								{

									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET Events.list_id = Temp.list_id, Events.event_id = Temp.event_id, Events.event_data = Temp.event_data, Events.event_type = Temp.event_type, Events.event_status = Temp.event_status, Events.event_time_mode = Temp.event_time_mode, Events.event_start = Temp.event_start, Events.event_duration = Temp.event_duration, Events.event_last_update = Temp.event_last_update, Events.event_calc_start = Temp.event_calc_start, Events.event_position = Temp.event_position, Events.parent_id = Temp.parent_id, Events.parent_position = Temp.parent_position, Events.parent_start = Temp.parent_start, Events.parent_calc_start = Temp.parent_calc_start, Events.parent_duration = Temp.parent_duration, Events.parent_calc_end = Temp.parent_calc_end, Events.event_calc_end = Temp.event_calc_end, Events.app_data = Temp.app_data, Events.app_data_aux = Temp.app_data_aux \
FROM %s as Events, %s as Temp WHERE Events.server_name = Temp.server_name and Events.server_list = Temp.server_list and Events.event_clip = Temp.event_clip and Events.event_title = Temp.event_title and Events.event_calc_start >= (Temp.event_calc_start - %0.03f) and Events.event_calc_start <= (Temp.event_calc_start + %0.03f) and (Events.event_position-Events.parent_position) = (Temp.event_position-Temp.parent_position)", // and not ((Temp.event_status & 2 = 2) and (Temp.event_type & 128 = 0)) ",
										g_psentinel->m_settings.m_pszLiveEvents,
										g_psentinel->m_settings.m_pszLiveEvents,
										g_psentinel->m_settings.m_pszLiveEventsTemp,
											dblEventTimeTolerance,
											dblEventTimeTolerance
										);
//if(readytotest) {bar.Format("Update: List size change, replacing non-playing events: \r\n\r\n%s", szSQL); AfxMessageBox(bar);}
if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread 02 LSC update from temp: %s", szSQL); // Sleep(50); //(Dispatch message)
	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
									if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
									{
									//**MSG
										bSQLerror = true;
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
									}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
//if((readytotest)&&(blistsizechanged)) {bar.Format("Update: Executed: \r\n\r\n%s", szSQL); AfxMessageBox(bar);}
								}
								pChannel->m_bChannelThreadPublishingEvents = false;

// dump tables
if(g_psentinel->m_settings.m_bDebugLists)
{
	CRecordset* prs;
	if(g_psentinel->m_settings.m_nDebugListTopCount>0)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top %d * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_nDebugListTopCount,
						g_psentinel->m_settings.m_pszLiveEventsTemp,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}
	else
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_pszLiveEventsTemp,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}

	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);

	prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
	if(prs == NULL)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
	}
	else
	{
		int nIndex = 0;
		CString szOut="\r\nB: TEMP Table:";
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);  //Sleep(20); //(Dispatch message)
		while ((!prs->IsEOF()))
		{
			CString szTemp="";
			szOut = "     ";
			try
			{
				prs->GetFieldValue("itemid", szTemp);  //HARDCODE
				szOut+="itemid="; szOut+=szTemp;
				prs->GetFieldValue("event_key", szTemp);  //HARDCODE
				szOut+="\tevent_key="; szOut+=szTemp;
				prs->GetFieldValue("server_name", szTemp);  //HARDCODE
				szOut+="\tserver_name="; szOut+=szTemp;
				prs->GetFieldValue("server_list", szTemp);  //HARDCODE
				szOut+="\tserver_list="; szOut+=szTemp;
				prs->GetFieldValue("list_id", szTemp);  //HARDCODE
				szOut+="\tlist_id="; szOut+=szTemp;
				prs->GetFieldValue("event_id", szTemp);  //HARDCODE
				szOut+="\tevent_id="; szOut+=szTemp;
				prs->GetFieldValue("event_clip", szTemp);  //HARDCODE
				szOut+="\tevent_clip="; szOut+=szTemp;
				prs->GetFieldValue("event_title", szTemp);  //HARDCODE
				szOut+="\tevent_title="; szOut+=szTemp;
				prs->GetFieldValue("event_data", szTemp);  //HARDCODE
				szOut+="\tevent_data="; szOut+=szTemp;
				prs->GetFieldValue("event_type", szTemp);  //HARDCODE
				szOut+="\tevent_type="; szOut+=szTemp;
				prs->GetFieldValue("event_status", szTemp);  //HARDCODE
				szOut+="\tevent_status="; szOut+=szTemp;
				prs->GetFieldValue("event_time_mode", szTemp);  //HARDCODE
				szOut+="\tevent_time_mode="; szOut+=szTemp;
				prs->GetFieldValue("event_start", szTemp);  //HARDCODE
				szOut+="\tevent_start="; szOut+=szTemp;
				prs->GetFieldValue("event_duration", szTemp);  //HARDCODE
				szOut+="\tevent_duration="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_start", szTemp);  //HARDCODE
				szOut+="\tevent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_end", szTemp);  //HARDCODE
				szOut+="\tevent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("event_position", szTemp);  //HARDCODE
				szOut+="\tevent_position="; szOut+=szTemp;
				prs->GetFieldValue("event_last_update", szTemp);  //HARDCODE
				szOut+="\tevent_last_update="; szOut+=szTemp;
				prs->GetFieldValue("parent_duration", szTemp);  //HARDCODE
				szOut+="\tparent_duration="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_start", szTemp);  //HARDCODE
				szOut+="\tparent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_end", szTemp);  //HARDCODE
				szOut+="\tparent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("parent_position", szTemp);  //HARDCODE
				szOut+="\tparent_position="; szOut+=szTemp;
				prs->GetFieldValue("app_data", szTemp);  //HARDCODE
				szOut+="\tapp_data="; szOut+=szTemp;
				prs->GetFieldValue("app_data_aux", szTemp);  //HARDCODE
				szOut+="\tapp_data_aux="; szOut+=szTemp;
				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(50); //(Dispatch message)
			}
			catch(...)
			{
			}
			prs->MoveNext();
			nIndex++;
		}

		prs->Close();

		delete prs;
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "B: TEMP table: %d items retrieved\r\n", nIndex);//  Sleep(50); //(Dispatch message)

	}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

}


// dump tables
if(g_psentinel->m_settings.m_bDebugLists)
{
	CRecordset* prs;
	if(g_psentinel->m_settings.m_nDebugListTopCount>0)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top %d * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_nDebugListTopCount,
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}
	else
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}


	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
	prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
	if(prs == NULL)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
	}
	else
	{
		int nIndex = 0;
		CString szOut="\r\nB: EVENT Table:";
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut); // Sleep(20); //(Dispatch message)
		while ((!prs->IsEOF()))
		{
			CString szTemp="";
			szOut = "     ";
			try
			{
				prs->GetFieldValue("itemid", szTemp);  //HARDCODE
				szOut+="itemid="; szOut+=szTemp;
				prs->GetFieldValue("event_key", szTemp);  //HARDCODE
				szOut+="\tevent_key="; szOut+=szTemp;
				prs->GetFieldValue("server_name", szTemp);  //HARDCODE
				szOut+="\tserver_name="; szOut+=szTemp;
				prs->GetFieldValue("server_list", szTemp);  //HARDCODE
				szOut+="\tserver_list="; szOut+=szTemp;
				prs->GetFieldValue("list_id", szTemp);  //HARDCODE
				szOut+="\tlist_id="; szOut+=szTemp;
				prs->GetFieldValue("event_id", szTemp);  //HARDCODE
				szOut+="\tevent_id="; szOut+=szTemp;
				prs->GetFieldValue("event_clip", szTemp);  //HARDCODE
				szOut+="\tevent_clip="; szOut+=szTemp;
				prs->GetFieldValue("event_title", szTemp);  //HARDCODE
				szOut+="\tevent_title="; szOut+=szTemp;
				prs->GetFieldValue("event_data", szTemp);  //HARDCODE
				szOut+="\tevent_data="; szOut+=szTemp;
				prs->GetFieldValue("event_type", szTemp);  //HARDCODE
				szOut+="\tevent_type="; szOut+=szTemp;
				prs->GetFieldValue("event_status", szTemp);  //HARDCODE
				szOut+="\tevent_status="; szOut+=szTemp;
				prs->GetFieldValue("event_time_mode", szTemp);  //HARDCODE
				szOut+="\tevent_time_mode="; szOut+=szTemp;
				prs->GetFieldValue("event_start", szTemp);  //HARDCODE
				szOut+="\tevent_start="; szOut+=szTemp;
				prs->GetFieldValue("event_duration", szTemp);  //HARDCODE
				szOut+="\tevent_duration="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_start", szTemp);  //HARDCODE
				szOut+="\tevent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_end", szTemp);  //HARDCODE
				szOut+="\tevent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("event_position", szTemp);  //HARDCODE
				szOut+="\tevent_position="; szOut+=szTemp;
				prs->GetFieldValue("event_last_update", szTemp);  //HARDCODE
				szOut+="\tevent_last_update="; szOut+=szTemp;
				prs->GetFieldValue("parent_duration", szTemp);  //HARDCODE
				szOut+="\tparent_duration="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_start", szTemp);  //HARDCODE
				szOut+="\tparent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_end", szTemp);  //HARDCODE
				szOut+="\tparent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("parent_position", szTemp);  //HARDCODE
				szOut+="\tparent_position="; szOut+=szTemp;
				prs->GetFieldValue("app_data", szTemp);  //HARDCODE
				szOut+="\tapp_data="; szOut+=szTemp;
				prs->GetFieldValue("app_data_aux", szTemp);  //HARDCODE
				szOut+="\tapp_data_aux="; szOut+=szTemp;
				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(50); //(Dispatch message)
			}
			catch(...)
			{
			}
			prs->MoveNext();
			nIndex++;
		}

		prs->Close();

		delete prs;
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "B: EVENT table: %d items retrieved\r\n", nIndex);//  Sleep(50); //(Dispatch message)

	}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

}





							} //if(nNumEvents!=ADC_NO_PLAYINGEVENT)

							if((!pChannel->m_bKillChannelThread)&&(!bSQLerror))//&&(!bStatusChange))  // commented out status change here because we want the playing events to be committed.
							{
								if(!bStatusChange)
								{
									// we can continue on and get the rest of the items, else break out and re-get
									// then, if no status change, get the rest of the events

									// at this point, we have n = the next event we have to get
									nNumToGet = g_psentinel->m_settings.m_bUseListCount?listdata.listcount:listdata.lookahead;

									while((!pChannel->m_bKillChannelThread)&&(!bStatusChange)&&(n<nNumToGet))
									{
										int nOffset = n;

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s SentinelChannelThread getting %d events at %d, of total %d", pChannel->m_pszDesc, (g_psentinel->m_settings.m_ulEventsPerRequest!=0)?(g_psentinel->m_settings.m_ulEventsPerRequest):(nNumToGet-nOffset), nOffset, nNumToGet);  Sleep(50); //(Dispatch message)
										pChannel->m_bChannelThreadGettingEvents = true;
										EnterCriticalSection(&g_adc.m_crit);
											nNumEvents = g_adc.GetEvents(pChannel->m_pAPIConn,
											pChannel->m_nHarrisListID, 
											nOffset, //start at the offset.
											(g_psentinel->m_settings.m_ulEventsPerRequest!=0)?(g_psentinel->m_settings.m_ulEventsPerRequest):(nNumToGet-nOffset),
											(ADC_GETEVENTS_BREAKONCHANGE|ADC_GETEVENTS_LIST|ADC_GETEVENTS_USEOFFSET)|(g_psentinel->m_settings.m_bUseListCount?ADC_GETEVENTS_FULLLIST:0), //|(g_psentinel->m_settings.m_bUseListCount?(ADC_GETEVENTS_FULLLIST):0),
											ADC_DEFAULT_TIMEOUT,
											&bStatusChange,
											&listdata
											);
										LeaveCriticalSection(&g_adc.m_crit);
										pChannel->m_bChannelThreadGettingEvents = false;

										//////////// at least, let's dump these to the db.

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s SentinelChannelThread got %d events", pChannel->m_pszDesc, nNumEvents);  Sleep(50); //(Dispatch message)
pChannel->m_bChannelThreadPublishingTempEvents = true;

										while((!pChannel->m_bKillChannelThread)&&(!bStatusChange)&&(n<nNumEvents+nOffset)&&(pChannel->m_pAPIConn))//&&((*pbKillConnThread)!=true))
										{

			//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s SentinelChannelThread event: %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
											if((pList->m_ppEvents)&&(pList->m_ppEvents[n])&&(pList->m_ppEvents[n]->m_pszID)&&(strlen(pList->m_ppEvents[n]->m_pszID)))
											{

												double dblEventTime = 0.0; 
												double dblCalcEventTime = 0.0; 
												unsigned short usOnAirJulianDate = pList->m_ppEvents[n]->m_usOnAirJulianDate;

										//		if(usOnAirJulianDate == 0xffff)  // can't trust event based julian date, have to use server date
			//									{
			//										_timeb timestamp;
			//										_ftime(&timestamp);

			//										usOnAirJulianDate = pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.

			// move time calc to below julian recalc
			//										dblEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
			//											+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
			//									}
			/*
												else
												{
													dblEventTime = ((double)(pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567))*86400.0    // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
														+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
												}
			*/											
			//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s SentinelChannelThread event: %d [%s] again", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)



												if(usLastPrimaryOnAirJulianDate != 0xffff)
												{
			//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s SentinelChannelThread event: %d [%s] again1a", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
													usOnAirJulianDate = usLastPrimaryOnAirJulianDate;
												}
												else
												{
			//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s SentinelChannelThread event: %d [%s] again1b", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)
													usOnAirJulianDate = pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.
												}

			//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s SentinelChannelThread event: %d [%s] again2", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)

												unsigned short usType = pList->m_ppEvents[n]->m_usType;

												if(usType&SECONDARYEVENT)
												{
													if(ulLastPrimaryOnAirTimeMS!=0xffffffff)
													{
														if(
																((usType&0xff) == SECBACKAVEVENT)
															||((usType&0xff) == SECBACKTIMEGPI)
															||((usType&0xff) == SECBACKSYSTEM)
															)
														{
															dblEventTime = ((dblLastPrimaryEventTime<0.0)?0.0:dblLastPrimaryEventTime)
																- (pList->m_ppEvents[n]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)?(0.000):((double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0);

															dblCalcEventTime = ((dblLastCalcPrimaryEventTime <0.0)?0.0:dblLastCalcPrimaryEventTime)
																- (pList->m_ppEvents[n]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)?(0.000):((double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0);

														}
														else
														{
															if((pList->m_ppEvents[n]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)||(((usType&0xff)>=SECCOMMENT)&&((usType&0xff)<=INVALIDEVENT)))
															{ //prob a commment
																dblEventTime = ((dblLastEventTime<0.0)?((dblLastPrimaryEventTime<0.0)?0.0:dblLastPrimaryEventTime):(dblLastEventTime))
																	+ ((((usType&0xff)>=SECCOMMENT)&&((usType&0xff)<=INVALIDEVENT))?(0.001):(0.000)); // a comment, for instance, has TIME_NOT_DEFINED. but we want it to come after the primary it belongs to, so we give it .001 seconds

																dblCalcEventTime = ((dblLastCalcEventTime<0.0)?((dblLastCalcPrimaryEventTime<0.0)?0.0:dblLastCalcPrimaryEventTime):(dblLastCalcEventTime))
																	+ ((((usType&0xff)>=SECCOMMENT)&&((usType&0xff)<=INVALIDEVENT))?(0.001):(0.000)); // a comment, for instance, has TIME_NOT_DEFINED. but we want it to come after the primary it belongs to, so we give it .001 seconds

															}
															else
															{
																dblEventTime = ((dblLastPrimaryEventTime<0.0)?0.0:dblLastPrimaryEventTime)
																	+ ((double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0)
																	+ (pList->m_ppEvents[n]->m_ulOnAirTimeMS==0?(0.001):(0.000)); 

																dblCalcEventTime = ((dblLastCalcPrimaryEventTime<0.0)?0.0:dblLastCalcPrimaryEventTime)
																	+ ((double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0) 
																	+ (pList->m_ppEvents[n]->m_ulOnAirTimeMS==0?(0.001):(0.000)); 
															}
														}
														usOnAirJulianDate = (usLastPrimaryOnAirJulianDate!=0xffff)?usLastPrimaryOnAirJulianDate:pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.

														if(pList->m_ppEvents[n]->m_ulOnAirTimeMS + ulLastPrimaryOnAirTimeMS >= 86400000 )
														{
															//wrapped days.
															usOnAirJulianDate++;
														}
													}
													else
													{
														// no last primary
														dblEventTime = (pList->m_ppEvents[n]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)?(0.001):((double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0);
														dblCalcEventTime = (pList->m_ppEvents[n]->m_ulOnAirTimeMS==TIME_NOT_DEFINED)?(0.001):((double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0);
													}
												}
												else
												{
													if(n<listdata.lookahead)
													{
														if(ulLastPrimaryOnAirTimeMS!=0xffffffff)
														{
															if(ulLastPrimaryOnAirTimeMS > pList->m_ppEvents[n]->m_ulOnAirTimeMS)
															{
																// we wrapped days.
																usOnAirJulianDate++;
															}
														}

														dblEventTime = ((double)(usOnAirJulianDate - 25567))*86400.0  // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
															+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
													}
													else
													{
														// we are beyond the lookahead, need to deal with estimated times, not actual as reported by harris.
														dblEventTime = dblLastPrimaryEventTime + (double)ulLastPrimaryOnAirDurMS/1000.0;
														// we dont care about ulLastPrimaryOnAirTimeMS for caluclations after this point
														// but we do care about julian day ... so... ok actually we dont care.  that was for calc anyway.
													}

/*
enum teventrunstatus {
     eventdone,
     eventrunning,
     playednextvideo,
     eventprerolled,
     eventpostrolled,
     eventidtitle,
     eventstandbyon,
     notplayed,
     eventranshort,
     eventskipped,
     eventpreped,
     eventnotswitched,
     eventpreviewed,
     rollingnext,
     eventshort,
     eventlong };
*/
													if(
															(pList->m_ppEvents[n]->m_usStatus&((1<<eventdone)|(1<<eventpostrolled)|(1<<eventrunning)|(1<<notplayed)|(1<<eventskipped)))
														||(pList->m_ppEvents[n]->m_ulControl&(1<<autotimed))// hard start
														)
														dblCalcEventTime = dblEventTime;
													else
														dblCalcEventTime = ((dblLastCalcPrimaryEventTime<0.0)?dblEventTime:dblLastCalcPrimaryEventTime) + ((ulLastPrimaryOnAirDurMS == 0xffffffff)?0.0:(double)ulLastPrimaryOnAirDurMS/1000.0);
													
													ulLastPrimaryOnAirTimeMS = pList->m_ppEvents[n]->m_ulOnAirTimeMS;
													ulLastPrimaryOnAirDurMS = pList->m_ppEvents[n]->m_ulDurationMS;
													dblLastPrimaryEventTime = dblEventTime;
													usLastPrimaryOnAirJulianDate = usOnAirJulianDate;

													dblLastCalcPrimaryEventTime = dblCalcEventTime;
													nLastParentPos = n;
													strcpy(chLastParentID, pList->m_ppEvents[n]->m_pszID);

												}

												dblLastCalcEventTime = dblCalcEventTime;
												dblLastEventTime = dblEventTime;
			//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s SentinelChannelThread event: %d [%s] again3", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  Sleep(50); //(Dispatch message)

												char* pchID = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszID);
												char* pchTitle = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszTitle);
												char* pchRecKey = NULL;
												char* pchData = NULL;
												char* pchParentID = pdb->EncodeQuotes(chLastParentID);
												
												if(pList->m_ppEvents[n]->m_pszData) 
													pchData = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszData);
												if(pList->m_ppEvents[n]->m_pszReconcileKey) 
													pchRecKey = pdb->EncodeQuotes(pList->m_ppEvents[n]->m_pszReconcileKey);


												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
server_name, \
server_list, \
list_id, \
event_id, \
event_clip, \
event_title, \
%s\
event_type, \
event_status, \
event_time_mode, \
event_start, \
event_duration, \
event_last_update, \
event_calc_start, \
event_position, \
parent_id, \
parent_position, \
parent_start, \
parent_calc_start, \
parent_duration, \
parent_calc_end, \
event_calc_end) VALUES ('%s', %d, %d, '%s', '%s', '%s', %s%s%s%d, %d, %d, %.3f, %d, %.3f, %.3f, %d, '%s', %d, %.3f, %.3f, %d, %.3f, %.3f)",
														g_psentinel->m_settings.m_pszLiveEventsTemp,  // table name
														pList->m_ppEvents[n]->m_pszData?"event_data, ":"", //event_data (exists)
														pChannel->m_pszServerName, //server_name
														pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
														pChannel->m_nChannelID, // list_id - internal lookup channel number
														pchRecKey?pchRecKey:(pList->m_ppEvents[n]->m_pszReconcileKey?pList->m_ppEvents[n]->m_pszReconcileKey:""), //event_id
														pchID?pchID:pList->m_ppEvents[n]->m_pszID, //event_clip  (must not be null)
														pchTitle?pchTitle:"", //event_title
														pchData?"'":"", //event_data (exists)
														pchData?pchData:"", //event_data  
														pchData?"', ":"", //event_data (exists)
														pList->m_ppEvents[n]->m_usType, //event_type
														pList->m_ppEvents[n]->m_usStatus, //event_status
														pList->m_ppEvents[n]->m_ulControl, //event_time_mode
														dblEventTime, //event_start
														pList->m_ppEvents[n]->m_ulDurationMS, //event_duration
														dblServerTime, //event_last_update
														dblCalcEventTime, //calculated event time
														n, // position.
														pchParentID?pchParentID:chLastParentID, //parent ID
														nLastParentPos, //parent pos
														dblLastPrimaryEventTime, // scheduled time of parent event
														dblLastCalcPrimaryEventTime, //calculated event time of parent event
														((ulLastPrimaryOnAirDurMS == 0xffffffff)?-1:ulLastPrimaryOnAirDurMS),
														(dblLastCalcPrimaryEventTime + ((ulLastPrimaryOnAirDurMS == 0xffffffff)?(-0.0):(double)ulLastPrimaryOnAirDurMS/1000.0)),
														(((usType&0xff)<SECCOMMENT)?(dblCalcEventTime + (double)pList->m_ppEvents[n]->m_ulDurationMS/1000.0):(dblLastCalcPrimaryEventTime + (double)ulLastPrimaryOnAirDurMS/1000.0))
														);

if(g_psentinel->m_settings.m_bDebugInsertSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread event insert B: %s", szSQL); // Sleep(250); //(Dispatch message)

				EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
												if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
												{
													bSQLerror = true;
													//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
												}
				LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
												
												if(pchID) free(pchID);
												if(pchTitle) free(pchTitle);
												if(pchRecKey) free(pchRecKey);
												if(pchData) free(pchData);
												if(pchParentID) free(pchParentID);
											
											}//if((pList->m_ppEvents)&&(pList->m_ppEvents[n])&&(pList->m_ppEvents[n]->m_pszID)&&(strlen(pList->m_ppEvents[n]->m_pszID)))
											n++;
										}//while((!pChannel->m_bKillChannelThread)&&(n<nNumEvents)&&(pChannel->m_pAPIConn))//&&((*pbKillConnThread)!=true))
pChannel->m_bChannelThreadPublishingTempEvents = false;

									} //	while((!pChannel->m_bKillChannelThread)&&(!bStatusChange)&&(n<nNumToGet))
//									pChannel->m_bChannelThreadGettingEvents = false;

								} //if(!bStatusChange)

								// update what we got so far
//CString bar;
								pChannel->m_bChannelThreadPublishingEvents = true;
// new method:
//Update records in the Events table that match the Events in the Events_Temp table matching on several fields and using 0.30 +/- window for the event_start time

// dump tables
if(g_psentinel->m_settings.m_bDebugLists)
{
	CRecordset* prs;
	if(g_psentinel->m_settings.m_nDebugListTopCount>0)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top %d * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_nDebugListTopCount,
						g_psentinel->m_settings.m_pszLiveEventsTemp,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}
	else
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_pszLiveEventsTemp,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}


	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
	prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
	if(prs == NULL)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
	}
	else
	{
		int nIndex = 0;
		CString szOut="\r\nSecondary initialization get\r\nC: TEMP Table:";
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut); // Sleep(20); //(Dispatch message)
		while ((!prs->IsEOF()))
		{
			CString szTemp="";
			szOut = "     ";
			try
			{
				prs->GetFieldValue("itemid", szTemp);  //HARDCODE
				szOut+="itemid="; szOut+=szTemp;
				prs->GetFieldValue("event_key", szTemp);  //HARDCODE
				szOut+="\tevent_key="; szOut+=szTemp;
				prs->GetFieldValue("server_name", szTemp);  //HARDCODE
				szOut+="\tserver_name="; szOut+=szTemp;
				prs->GetFieldValue("server_list", szTemp);  //HARDCODE
				szOut+="\tserver_list="; szOut+=szTemp;
				prs->GetFieldValue("list_id", szTemp);  //HARDCODE
				szOut+="\tlist_id="; szOut+=szTemp;
				prs->GetFieldValue("event_id", szTemp);  //HARDCODE
				szOut+="\tevent_id="; szOut+=szTemp;
				prs->GetFieldValue("event_clip", szTemp);  //HARDCODE
				szOut+="\tevent_clip="; szOut+=szTemp;
				prs->GetFieldValue("event_title", szTemp);  //HARDCODE
				szOut+="\tevent_title="; szOut+=szTemp;
				prs->GetFieldValue("event_data", szTemp);  //HARDCODE
				szOut+="\tevent_data="; szOut+=szTemp;
				prs->GetFieldValue("event_type", szTemp);  //HARDCODE
				szOut+="\tevent_type="; szOut+=szTemp;
				prs->GetFieldValue("event_status", szTemp);  //HARDCODE
				szOut+="\tevent_status="; szOut+=szTemp;
				prs->GetFieldValue("event_time_mode", szTemp);  //HARDCODE
				szOut+="\tevent_time_mode="; szOut+=szTemp;
				prs->GetFieldValue("event_start", szTemp);  //HARDCODE
				szOut+="\tevent_start="; szOut+=szTemp;
				prs->GetFieldValue("event_duration", szTemp);  //HARDCODE
				szOut+="\tevent_duration="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_start", szTemp);  //HARDCODE
				szOut+="\tevent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_end", szTemp);  //HARDCODE
				szOut+="\tevent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("event_position", szTemp);  //HARDCODE
				szOut+="\tevent_position="; szOut+=szTemp;
				prs->GetFieldValue("event_last_update", szTemp);  //HARDCODE
				szOut+="\tevent_last_update="; szOut+=szTemp;
				prs->GetFieldValue("parent_duration", szTemp);  //HARDCODE
				szOut+="\tparent_duration="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_start", szTemp);  //HARDCODE
				szOut+="\tparent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_end", szTemp);  //HARDCODE
				szOut+="\tparent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("parent_position", szTemp);  //HARDCODE
				szOut+="\tparent_position="; szOut+=szTemp;
				prs->GetFieldValue("app_data", szTemp);  //HARDCODE
				szOut+="\tapp_data="; szOut+=szTemp;
				prs->GetFieldValue("app_data_aux", szTemp);  //HARDCODE
				szOut+="\tapp_data_aux="; szOut+=szTemp;
				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(50); //(Dispatch message)
			}
			catch(...)
			{
			}
			prs->MoveNext();
			nIndex++;
		}

		prs->Close();

		delete prs;
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "C: TEMP table: %d items retrieved\r\n", nIndex);//  Sleep(50); //(Dispatch message)

	}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

}


// dump tables
if(g_psentinel->m_settings.m_bDebugLists)
{
	CRecordset* prs;
	if(g_psentinel->m_settings.m_nDebugListTopCount>0)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top %d * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_nDebugListTopCount,
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}
	else
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}


	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
	prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
	if(prs == NULL)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
	}
	else
	{
		int nIndex = 0;
		CString szOut="\r\nC: EVENT Table:";
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(20); //(Dispatch message)
		while ((!prs->IsEOF()))
		{
			CString szTemp="";
			szOut = "     ";
			try
			{
				prs->GetFieldValue("itemid", szTemp);  //HARDCODE
				szOut+="itemid="; szOut+=szTemp;
				prs->GetFieldValue("event_key", szTemp);  //HARDCODE
				szOut+="\tevent_key="; szOut+=szTemp;
				prs->GetFieldValue("server_name", szTemp);  //HARDCODE
				szOut+="\tserver_name="; szOut+=szTemp;
				prs->GetFieldValue("server_list", szTemp);  //HARDCODE
				szOut+="\tserver_list="; szOut+=szTemp;
				prs->GetFieldValue("list_id", szTemp);  //HARDCODE
				szOut+="\tlist_id="; szOut+=szTemp;
				prs->GetFieldValue("event_id", szTemp);  //HARDCODE
				szOut+="\tevent_id="; szOut+=szTemp;
				prs->GetFieldValue("event_clip", szTemp);  //HARDCODE
				szOut+="\tevent_clip="; szOut+=szTemp;
				prs->GetFieldValue("event_title", szTemp);  //HARDCODE
				szOut+="\tevent_title="; szOut+=szTemp;
				prs->GetFieldValue("event_data", szTemp);  //HARDCODE
				szOut+="\tevent_data="; szOut+=szTemp;
				prs->GetFieldValue("event_type", szTemp);  //HARDCODE
				szOut+="\tevent_type="; szOut+=szTemp;
				prs->GetFieldValue("event_status", szTemp);  //HARDCODE
				szOut+="\tevent_status="; szOut+=szTemp;
				prs->GetFieldValue("event_time_mode", szTemp);  //HARDCODE
				szOut+="\tevent_time_mode="; szOut+=szTemp;
				prs->GetFieldValue("event_start", szTemp);  //HARDCODE
				szOut+="\tevent_start="; szOut+=szTemp;
				prs->GetFieldValue("event_duration", szTemp);  //HARDCODE
				szOut+="\tevent_duration="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_start", szTemp);  //HARDCODE
				szOut+="\tevent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_end", szTemp);  //HARDCODE
				szOut+="\tevent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("event_position", szTemp);  //HARDCODE
				szOut+="\tevent_position="; szOut+=szTemp;
				prs->GetFieldValue("event_last_update", szTemp);  //HARDCODE
				szOut+="\tevent_last_update="; szOut+=szTemp;
				prs->GetFieldValue("parent_duration", szTemp);  //HARDCODE
				szOut+="\tparent_duration="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_start", szTemp);  //HARDCODE
				szOut+="\tparent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_end", szTemp);  //HARDCODE
				szOut+="\tparent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("parent_position", szTemp);  //HARDCODE
				szOut+="\tparent_position="; szOut+=szTemp;
				prs->GetFieldValue("app_data", szTemp);  //HARDCODE
				szOut+="\tapp_data="; szOut+=szTemp;
				prs->GetFieldValue("app_data_aux", szTemp);  //HARDCODE
				szOut+="\tapp_data_aux="; szOut+=szTemp;
				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(50); //(Dispatch message)
			}
			catch(...)
			{
			}
			prs->MoveNext();
			nIndex++;
		}

		prs->Close();

		delete prs;
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "C: EVENT table: %d items retrieved\r\n", nIndex);//  Sleep(50); //(Dispatch message)

	}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

}

								if((!blistsizechanged)&&(!g_psentinel->m_settings.m_bDisableEventMatchOnPosition)) // go by list position
								{
//if(g_bDebug) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "no change");
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET Events.list_id = Temp.list_id, Events.event_id = Temp.event_id, Events.event_data = Temp.event_data, Events.event_type = Temp.event_type, Events.event_status = Temp.event_status, Events.event_time_mode = Temp.event_time_mode, Events.event_start = Temp.event_start, Events.event_duration = Temp.event_duration, Events.event_last_update = Temp.event_last_update, Events.event_calc_start = Temp.event_calc_start, Events.event_position = Temp.event_position, Events.parent_id = Temp.parent_id, Events.parent_position = Temp.parent_position, Events.parent_start = Temp.parent_start, Events.parent_calc_start = Temp.parent_calc_start, Events.parent_duration = Temp.parent_duration, Events.parent_calc_end = Temp.parent_calc_end, Events.event_calc_end = Temp.event_calc_end, Events.app_data = Temp.app_data, Events.app_data_aux = Temp.app_data_aux \
FROM %s as Events, %s as Temp WHERE Events.server_name = Temp.server_name and Events.server_list = Temp.server_list and Events.event_clip = Temp.event_clip and Events.event_title = Temp.event_title and Events.event_position = Temp.event_position",
										g_psentinel->m_settings.m_pszLiveEvents,
										g_psentinel->m_settings.m_pszLiveEvents,
										g_psentinel->m_settings.m_pszLiveEventsTemp
										);
//if(readytotest) {bar.Format("Update: No list size change, going by app_data_aux: \r\n\r\n%s", szSQL); AfxMessageBox(bar);}
if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread 03 NLC update from temp: %s", szSQL);//  Sleep(50); //(Dispatch message)
	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
									if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
									{
										bSQLerror = true;
									//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
									}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
//if(readytotest) {bar.Format("Update: Executed: \r\n\r\n%s", szSQL); AfxMessageBox(bar);}
								}
								else // otherwise go by time (and status)

								{
/*

if(g_bDebug) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "packing");
									// list packing occurs in harris after a primary event plays.  the list changes size because of dat.
									// first deal with the playing event
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET Events.list_id = Temp.list_id, Events.event_id = Temp.event_id, Events.event_data = Temp.event_data, Events.event_type = Temp.event_type, Events.event_status = Temp.event_status, Events.event_time_mode = Temp.event_time_mode, Events.event_start = Temp.event_start, Events.event_duration = Temp.event_duration, Events.event_last_update = Temp.event_last_update, Events.event_calc_start = Temp.event_calc_start, Events.event_position = Temp.event_position, Events.parent_id = Temp.parent_id, Events.parent_position = Temp.parent_position, Events.parent_start = Temp.parent_start, Events.parent_calc_start = Temp.parent_calc_start, Events.parent_duration = Temp.parent_duration, Events.parent_calc_end = Temp.parent_calc_end, Events.event_calc_end = Temp.event_calc_end, Events.app_data = Temp.app_data, Events.app_data_aux = Temp.app_data_aux \
FROM (select top 1 Events.itemid as eventitemid, Temp.* from (select * from %s as Events where Events.server_name = '%s' and Events.server_list = %d and (Events.event_type & 128 = 0) and ((Events.event_status & 17 = 0) or (Events.event_status & 19 = 0))) as Events \
join (select * from %s as Events_Temp where (Events_Temp.event_status & 2 = 2) and (Events_Temp.event_type & 128 = 0) and \
Events_Temp.server_name = '%s' and Events_Temp.server_list = %d) \
as Temp on Events.event_clip = Temp.event_clip and Events.event_title = Temp.event_title \
and Events.server_name = Temp.server_name and Events.server_list = Temp.server_list \
order by Events.event_position) as Temp where Events.itemid = Temp.eventitemid",	
	//17 = postrolled or done

	
										g_psentinel->m_settings.m_pszLiveEvents,
										g_psentinel->m_settings.m_pszLiveEvents,
										pChannel->m_pszServerName, //server_name
										pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
										g_psentinel->m_settings.m_pszLiveEventsTemp,
										pChannel->m_pszServerName, //server_name
										pChannel->m_nHarrisListID  // server_list - actual 1-based harris list number
										);
//if(readytotest) {bar.Format("Update: List size change, replacing playing event: \r\n\r\n%s", szSQL); AfxMessageBox(bar);}

//									playing non-secondary in temp.  
//									match to 
//									smallest start time non-secondary event that has playing and not done and not postroll status in the event list


	//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread update from temp: %s", szSQL);  Sleep(50); //(Dispatch message)
	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
									if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
									{
									//**MSG
										bSQLerror = true;
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
									}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
//if((readytotest)&&(blistsizechanged)) {bar.Format("Update: Executed: \r\n\r\n%s", szSQL); AfxMessageBox(bar);}
									// then deal with the non playing events

//									must exclude the playing one as per above.
*/

									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET Events.list_id = Temp.list_id, Events.event_id = Temp.event_id, Events.event_data = Temp.event_data, Events.event_type = Temp.event_type, Events.event_status = Temp.event_status, Events.event_time_mode = Temp.event_time_mode, Events.event_start = Temp.event_start, Events.event_duration = Temp.event_duration, Events.event_last_update = Temp.event_last_update, Events.event_calc_start = Temp.event_calc_start, Events.event_position = Temp.event_position, Events.parent_id = Temp.parent_id, Events.parent_position = Temp.parent_position, Events.parent_start = Temp.parent_start, Events.parent_calc_start = Temp.parent_calc_start, Events.parent_duration = Temp.parent_duration, Events.parent_calc_end = Temp.parent_calc_end, Events.event_calc_end = Temp.event_calc_end, Events.app_data = Temp.app_data, Events.app_data_aux = Temp.app_data_aux \
FROM %s as Events, %s as Temp WHERE Events.server_name = Temp.server_name and Events.server_list = Temp.server_list and Events.event_clip = Temp.event_clip and Events.event_title = Temp.event_title and Events.event_calc_start >= (Temp.event_calc_start - %0.03f) and Events.event_calc_start <= (Temp.event_calc_start + %0.03f) and (Events.event_position-Events.parent_position) = (Temp.event_position-Temp.parent_position)", // and not ((Temp.event_status & 2 = 2) and (Temp.event_type & 128 = 0)) ",
										g_psentinel->m_settings.m_pszLiveEvents,
										g_psentinel->m_settings.m_pszLiveEvents,
										g_psentinel->m_settings.m_pszLiveEventsTemp,
											dblEventTimeTolerance,
											dblEventTimeTolerance
										);
//if(readytotest) {bar.Format("Update: List size change, replacing non-playing events: \r\n\r\n%s", szSQL); AfxMessageBox(bar);}
if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread 04 LSC update from temp: %s", szSQL);//  Sleep(50); //(Dispatch message)
	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
									if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
									{
									//**MSG
										bSQLerror = true;
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
									}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
//if((readytotest)&&(blistsizechanged)) {bar.Format("Update: Executed: \r\n\r\n%s", szSQL); AfxMessageBox(bar);}
								}

// dump tables
if(g_psentinel->m_settings.m_bDebugLists)
{
	CRecordset* prs;
	if(g_psentinel->m_settings.m_nDebugListTopCount>0)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top %d * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_nDebugListTopCount,
						g_psentinel->m_settings.m_pszLiveEventsTemp,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}
	else
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_pszLiveEventsTemp,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}


	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
	prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
	if(prs == NULL)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
	}
	else
	{
		int nIndex = 0;
		CString szOut="\r\nD: TEMP Table:";
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut); // Sleep(20); //(Dispatch message)
		while ((!prs->IsEOF()))
		{
			CString szTemp="";
			szOut = "     ";
			try
			{
				prs->GetFieldValue("itemid", szTemp);  //HARDCODE
				szOut+="itemid="; szOut+=szTemp;
				prs->GetFieldValue("event_key", szTemp);  //HARDCODE
				szOut+="\tevent_key="; szOut+=szTemp;
				prs->GetFieldValue("server_name", szTemp);  //HARDCODE
				szOut+="\tserver_name="; szOut+=szTemp;
				prs->GetFieldValue("server_list", szTemp);  //HARDCODE
				szOut+="\tserver_list="; szOut+=szTemp;
				prs->GetFieldValue("list_id", szTemp);  //HARDCODE
				szOut+="\tlist_id="; szOut+=szTemp;
				prs->GetFieldValue("event_id", szTemp);  //HARDCODE
				szOut+="\tevent_id="; szOut+=szTemp;
				prs->GetFieldValue("event_clip", szTemp);  //HARDCODE
				szOut+="\tevent_clip="; szOut+=szTemp;
				prs->GetFieldValue("event_title", szTemp);  //HARDCODE
				szOut+="\tevent_title="; szOut+=szTemp;
				prs->GetFieldValue("event_data", szTemp);  //HARDCODE
				szOut+="\tevent_data="; szOut+=szTemp;
				prs->GetFieldValue("event_type", szTemp);  //HARDCODE
				szOut+="\tevent_type="; szOut+=szTemp;
				prs->GetFieldValue("event_status", szTemp);  //HARDCODE
				szOut+="\tevent_status="; szOut+=szTemp;
				prs->GetFieldValue("event_time_mode", szTemp);  //HARDCODE
				szOut+="\tevent_time_mode="; szOut+=szTemp;
				prs->GetFieldValue("event_start", szTemp);  //HARDCODE
				szOut+="\tevent_start="; szOut+=szTemp;
				prs->GetFieldValue("event_duration", szTemp);  //HARDCODE
				szOut+="\tevent_duration="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_start", szTemp);  //HARDCODE
				szOut+="\tevent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_end", szTemp);  //HARDCODE
				szOut+="\tevent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("event_position", szTemp);  //HARDCODE
				szOut+="\tevent_position="; szOut+=szTemp;
				prs->GetFieldValue("event_last_update", szTemp);  //HARDCODE
				szOut+="\tevent_last_update="; szOut+=szTemp;
				prs->GetFieldValue("parent_duration", szTemp);  //HARDCODE
				szOut+="\tparent_duration="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_start", szTemp);  //HARDCODE
				szOut+="\tparent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_end", szTemp);  //HARDCODE
				szOut+="\tparent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("parent_position", szTemp);  //HARDCODE
				szOut+="\tparent_position="; szOut+=szTemp;
				prs->GetFieldValue("app_data", szTemp);  //HARDCODE
				szOut+="\tapp_data="; szOut+=szTemp;
				prs->GetFieldValue("app_data_aux", szTemp);  //HARDCODE
				szOut+="\tapp_data_aux="; szOut+=szTemp;
				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(50); //(Dispatch message)
			}
			catch(...)
			{
			}
			prs->MoveNext();
			nIndex++;
		}

		prs->Close();

		delete prs;
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "D: TEMP table: %d items retrieved\r\n", nIndex);//  Sleep(50); //(Dispatch message)

	}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

}


// dump tables
if(g_psentinel->m_settings.m_bDebugLists)
{
	CRecordset* prs;
	if(g_psentinel->m_settings.m_nDebugListTopCount>0)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top %d * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_nDebugListTopCount,
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}
	else
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}


	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
	prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
	if(prs == NULL)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
	}
	else
	{
		int nIndex = 0;
		CString szOut="\r\nD: EVENT Table:";
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut); // Sleep(20); //(Dispatch message)
		while ((!prs->IsEOF()))
		{
			CString szTemp="";
			szOut = "     ";
			try
			{
				prs->GetFieldValue("itemid", szTemp);  //HARDCODE
				szOut+="itemid="; szOut+=szTemp;
				prs->GetFieldValue("event_key", szTemp);  //HARDCODE
				szOut+="\tevent_key="; szOut+=szTemp;
				prs->GetFieldValue("server_name", szTemp);  //HARDCODE
				szOut+="\tserver_name="; szOut+=szTemp;
				prs->GetFieldValue("server_list", szTemp);  //HARDCODE
				szOut+="\tserver_list="; szOut+=szTemp;
				prs->GetFieldValue("list_id", szTemp);  //HARDCODE
				szOut+="\tlist_id="; szOut+=szTemp;
				prs->GetFieldValue("event_id", szTemp);  //HARDCODE
				szOut+="\tevent_id="; szOut+=szTemp;
				prs->GetFieldValue("event_clip", szTemp);  //HARDCODE
				szOut+="\tevent_clip="; szOut+=szTemp;
				prs->GetFieldValue("event_title", szTemp);  //HARDCODE
				szOut+="\tevent_title="; szOut+=szTemp;
				prs->GetFieldValue("event_data", szTemp);  //HARDCODE
				szOut+="\tevent_data="; szOut+=szTemp;
				prs->GetFieldValue("event_type", szTemp);  //HARDCODE
				szOut+="\tevent_type="; szOut+=szTemp;
				prs->GetFieldValue("event_status", szTemp);  //HARDCODE
				szOut+="\tevent_status="; szOut+=szTemp;
				prs->GetFieldValue("event_time_mode", szTemp);  //HARDCODE
				szOut+="\tevent_time_mode="; szOut+=szTemp;
				prs->GetFieldValue("event_start", szTemp);  //HARDCODE
				szOut+="\tevent_start="; szOut+=szTemp;
				prs->GetFieldValue("event_duration", szTemp);  //HARDCODE
				szOut+="\tevent_duration="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_start", szTemp);  //HARDCODE
				szOut+="\tevent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_end", szTemp);  //HARDCODE
				szOut+="\tevent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("event_position", szTemp);  //HARDCODE
				szOut+="\tevent_position="; szOut+=szTemp;
				prs->GetFieldValue("event_last_update", szTemp);  //HARDCODE
				szOut+="\tevent_last_update="; szOut+=szTemp;
				prs->GetFieldValue("parent_duration", szTemp);  //HARDCODE
				szOut+="\tparent_duration="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_start", szTemp);  //HARDCODE
				szOut+="\tparent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_end", szTemp);  //HARDCODE
				szOut+="\tparent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("parent_position", szTemp);  //HARDCODE
				szOut+="\tparent_position="; szOut+=szTemp;
				prs->GetFieldValue("app_data", szTemp);  //HARDCODE
				szOut+="\tapp_data="; szOut+=szTemp;
				prs->GetFieldValue("app_data_aux", szTemp);  //HARDCODE
				szOut+="\tapp_data_aux="; szOut+=szTemp;
				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(50); //(Dispatch message)
			}
			catch(...)
			{
			}
			prs->MoveNext();
			nIndex++;
		}

		prs->Close();

		delete prs;
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "D: EVENT table: %d items retrieved\r\n", nIndex);//  Sleep(50); //(Dispatch message)

	}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

}

// new method:
//Inserts the new events into the Events table


//								if((!bStatusChange)&&(!bSQLerror)) // just added !bStatusChange because the inserts were getting freaky.. hopefully will fix.
								if(!bSQLerror) // ok, can't do status change in this because if so, once the status has changed ever, it will never hit this again... so cant use.
									// not sure if the above assessment is (still) true - need to test.
								{
/*
									if((!bStatusChange)&&(!blistsizechanged)&&(!g_psentinel->m_settings.m_bDisableEventMatchOnPosition)) // go by list position
//									if(0)//!blistsizechanged) // go by list position


									{
										// this is the case that causes the double events.  (why, I am not sure)...
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (server_name, server_list, list_id, event_id, event_clip, event_title, event_data, event_type, event_status, event_time_mode, event_start, event_duration, event_last_update, event_calc_start, event_position, parent_id, parent_position, parent_start, parent_calc_start, parent_duration, parent_calc_end, event_calc_end, app_data, app_data_aux) \
SELECT Temp.server_name, Temp.server_list, Temp.list_id, Temp.event_id, Temp.event_clip, Temp.event_title, Temp.event_data, Temp.event_type, Temp.event_status, Temp.event_time_mode, Temp.event_start, Temp.event_duration, Temp.event_last_update, Temp.event_calc_start, Temp.event_position, Temp.parent_id, Temp.parent_position, Temp.parent_start, Temp.parent_calc_start, Temp.parent_duration, Temp.parent_calc_end, Temp.event_calc_end, Temp.app_data, Temp.app_data_aux \
FROM %s as Temp LEFT JOIN %s as Events ON \
Temp.server_name = Events.server_name and Temp.server_list = Events.server_list and Temp.event_clip = Events.event_clip and Temp.event_title = Events.event_title and Temp.event_position = Events.event_position and (Events.event_position-Events.parent_position) = (Temp.event_position-Temp.parent_position) where Events.event_clip is NULL",
											g_psentinel->m_settings.m_pszLiveEvents,
											g_psentinel->m_settings.m_pszLiveEventsTemp,
											g_psentinel->m_settings.m_pszLiveEvents
											);
	//if(readytotest) {	bar.Format("Insert: No list size change, going by app_data_aux: \r\n\r\n%s", szSQL); AfxMessageBox(bar);}
EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
									if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
									{
										bSQLerror = true;
									//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
									}
LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
									}
									else // otherwise go by time (and status)
*/
									{
										// normally we should not insert, if the list size has decreased - something just fell off the top of the list.
										// we have a problem matching up times...
//										if(listdata.listcount>lastlistdata.listcount) // only do if the list is larger than the last.
										{
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (server_name, server_list, list_id, event_id, event_clip, event_title, event_data, event_type, event_status, event_time_mode, event_start, event_duration, event_last_update, event_calc_start, event_position, parent_id, parent_position, parent_start, parent_calc_start, parent_duration, parent_calc_end, event_calc_end, app_data, app_data_aux) \
SELECT Temp.server_name, Temp.server_list, Temp.list_id, Temp.event_id, Temp.event_clip, Temp.event_title, Temp.event_data, Temp.event_type, Temp.event_status, Temp.event_time_mode, Temp.event_start, Temp.event_duration, Temp.event_last_update, Temp.event_calc_start, Temp.event_position, Temp.parent_id, Temp.parent_position, Temp.parent_start, Temp.parent_calc_start, Temp.parent_duration, Temp.parent_calc_end, Temp.event_calc_end, Temp.app_data, Temp.app_data_aux \
FROM %s as Temp LEFT JOIN %s as Events ON \
Temp.server_name = Events.server_name and Temp.server_list = Events.server_list and Temp.event_clip = Events.event_clip and Temp.event_title = Events.event_title and Temp.event_calc_start >= (Events.event_calc_start - %0.03f) and Temp.event_calc_start <= (Events.event_calc_start + %0.03f) and (Events.event_position-Events.parent_position) = (Temp.event_position-Temp.parent_position) where Events.event_clip is NULL",
//Temp.server_name = Events.server_name and Temp.server_list = Events.server_list and Temp.event_clip = Events.event_clip and Temp.event_title = Events.event_title and ( (Temp.event_start >= (Events.event_start - %0.03f) and Temp.event_calc_start <= (Events.event_calc_start + %0.03f)) OR (Temp.app_data_aux = Events.app_data_aux - (lastlistdata.listcount - listdata.listcount)) ) where Events.event_clip is NULL",
											g_psentinel->m_settings.m_pszLiveEvents,
											g_psentinel->m_settings.m_pszLiveEventsTemp,
											g_psentinel->m_settings.m_pszLiveEvents,
											dblEventTimeTolerance,
											dblEventTimeTolerance
											);
										}
										// if we miss something, it will get cleaned up in the next round where list size does not change.
//if(readytotest) {	bar.Format("Insert: List size change, going by time: \r\n\r\n%s", szSQL); AfxMessageBox(bar);}
if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread 05 LC%d insert SQL: %s", blistsizechanged, szSQL); // Sleep(50); //(Dispatch message)
EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
									if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
									{
										bSQLerror = true;
									//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
									}
LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
//AfxMessageBox("inserted");

									}
	//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread insert from temp: %s", szSQL);  Sleep(50); //(Dispatch message)
								}
//if((readytotest)&&(blistsizechanged)) {bar.Format("Insert: Executed: \r\n\r\n%s", szSQL); AfxMessageBox(bar);}
								
// dump tables
if(g_psentinel->m_settings.m_bDebugLists)
{
	CRecordset* prs;
	if(g_psentinel->m_settings.m_nDebugListTopCount>0)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top %d * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_nDebugListTopCount,
						g_psentinel->m_settings.m_pszLiveEventsTemp,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}
	else
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_pszLiveEventsTemp,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}

	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
	prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
	if(prs == NULL)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
	}
	else
	{
		int nIndex = 0;
		CString szOut="\r\nE: TEMP Table:";
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);  //Sleep(20); //(Dispatch message)
		while ((!prs->IsEOF()))
		{
			CString szTemp="";
			szOut = "     ";
			try
			{
				prs->GetFieldValue("itemid", szTemp);  //HARDCODE
				szOut+="itemid="; szOut+=szTemp;
				prs->GetFieldValue("event_key", szTemp);  //HARDCODE
				szOut+="\tevent_key="; szOut+=szTemp;
				prs->GetFieldValue("server_name", szTemp);  //HARDCODE
				szOut+="\tserver_name="; szOut+=szTemp;
				prs->GetFieldValue("server_list", szTemp);  //HARDCODE
				szOut+="\tserver_list="; szOut+=szTemp;
				prs->GetFieldValue("list_id", szTemp);  //HARDCODE
				szOut+="\tlist_id="; szOut+=szTemp;
				prs->GetFieldValue("event_id", szTemp);  //HARDCODE
				szOut+="\tevent_id="; szOut+=szTemp;
				prs->GetFieldValue("event_clip", szTemp);  //HARDCODE
				szOut+="\tevent_clip="; szOut+=szTemp;
				prs->GetFieldValue("event_title", szTemp);  //HARDCODE
				szOut+="\tevent_title="; szOut+=szTemp;
				prs->GetFieldValue("event_data", szTemp);  //HARDCODE
				szOut+="\tevent_data="; szOut+=szTemp;
				prs->GetFieldValue("event_type", szTemp);  //HARDCODE
				szOut+="\tevent_type="; szOut+=szTemp;
				prs->GetFieldValue("event_status", szTemp);  //HARDCODE
				szOut+="\tevent_status="; szOut+=szTemp;
				prs->GetFieldValue("event_time_mode", szTemp);  //HARDCODE
				szOut+="\tevent_time_mode="; szOut+=szTemp;
				prs->GetFieldValue("event_start", szTemp);  //HARDCODE
				szOut+="\tevent_start="; szOut+=szTemp;
				prs->GetFieldValue("event_duration", szTemp);  //HARDCODE
				szOut+="\tevent_duration="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_start", szTemp);  //HARDCODE
				szOut+="\tevent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_end", szTemp);  //HARDCODE
				szOut+="\tevent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("event_position", szTemp);  //HARDCODE
				szOut+="\tevent_position="; szOut+=szTemp;
				prs->GetFieldValue("event_last_update", szTemp);  //HARDCODE
				szOut+="\tevent_last_update="; szOut+=szTemp;
				prs->GetFieldValue("parent_duration", szTemp);  //HARDCODE
				szOut+="\tparent_duration="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_start", szTemp);  //HARDCODE
				szOut+="\tparent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_end", szTemp);  //HARDCODE
				szOut+="\tparent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("parent_position", szTemp);  //HARDCODE
				szOut+="\tparent_position="; szOut+=szTemp;
				prs->GetFieldValue("app_data", szTemp);  //HARDCODE
				szOut+="\tapp_data="; szOut+=szTemp;
				prs->GetFieldValue("app_data_aux", szTemp);  //HARDCODE
				szOut+="\tapp_data_aux="; szOut+=szTemp;
				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(50); //(Dispatch message)
			}
			catch(...)
			{
			}
			prs->MoveNext();
			nIndex++;
		}

		prs->Close();

		delete prs;
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "E: TEMP table: %d items retrieved\r\n", nIndex);//  Sleep(50); //(Dispatch message)

	}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

}


// dump tables
if(g_psentinel->m_settings.m_bDebugLists)
{
	CRecordset* prs;
	if(g_psentinel->m_settings.m_nDebugListTopCount>0)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top %d * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_nDebugListTopCount,
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}
	else
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}


	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
	prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
	if(prs == NULL)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
	}
	else
	{
		int nIndex = 0;
		CString szOut="\r\nE: EVENT Table:";
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut); // Sleep(20); //(Dispatch message)
		while ((!prs->IsEOF()))
		{
			CString szTemp="";
			szOut = "     ";
			try
			{
				prs->GetFieldValue("itemid", szTemp);  //HARDCODE
				szOut+="itemid="; szOut+=szTemp;
				prs->GetFieldValue("event_key", szTemp);  //HARDCODE
				szOut+="\tevent_key="; szOut+=szTemp;
				prs->GetFieldValue("server_name", szTemp);  //HARDCODE
				szOut+="\tserver_name="; szOut+=szTemp;
				prs->GetFieldValue("server_list", szTemp);  //HARDCODE
				szOut+="\tserver_list="; szOut+=szTemp;
				prs->GetFieldValue("list_id", szTemp);  //HARDCODE
				szOut+="\tlist_id="; szOut+=szTemp;
				prs->GetFieldValue("event_id", szTemp);  //HARDCODE
				szOut+="\tevent_id="; szOut+=szTemp;
				prs->GetFieldValue("event_clip", szTemp);  //HARDCODE
				szOut+="\tevent_clip="; szOut+=szTemp;
				prs->GetFieldValue("event_title", szTemp);  //HARDCODE
				szOut+="\tevent_title="; szOut+=szTemp;
				prs->GetFieldValue("event_data", szTemp);  //HARDCODE
				szOut+="\tevent_data="; szOut+=szTemp;
				prs->GetFieldValue("event_type", szTemp);  //HARDCODE
				szOut+="\tevent_type="; szOut+=szTemp;
				prs->GetFieldValue("event_status", szTemp);  //HARDCODE
				szOut+="\tevent_status="; szOut+=szTemp;
				prs->GetFieldValue("event_time_mode", szTemp);  //HARDCODE
				szOut+="\tevent_time_mode="; szOut+=szTemp;
				prs->GetFieldValue("event_start", szTemp);  //HARDCODE
				szOut+="\tevent_start="; szOut+=szTemp;
				prs->GetFieldValue("event_duration", szTemp);  //HARDCODE
				szOut+="\tevent_duration="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_start", szTemp);  //HARDCODE
				szOut+="\tevent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_end", szTemp);  //HARDCODE
				szOut+="\tevent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("event_position", szTemp);  //HARDCODE
				szOut+="\tevent_position="; szOut+=szTemp;
				prs->GetFieldValue("event_last_update", szTemp);  //HARDCODE
				szOut+="\tevent_last_update="; szOut+=szTemp;
				prs->GetFieldValue("parent_duration", szTemp);  //HARDCODE
				szOut+="\tparent_duration="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_start", szTemp);  //HARDCODE
				szOut+="\tparent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_end", szTemp);  //HARDCODE
				szOut+="\tparent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("parent_position", szTemp);  //HARDCODE
				szOut+="\tparent_position="; szOut+=szTemp;
				prs->GetFieldValue("app_data", szTemp);  //HARDCODE
				szOut+="\tapp_data="; szOut+=szTemp;
				prs->GetFieldValue("app_data_aux", szTemp);  //HARDCODE
				szOut+="\tapp_data_aux="; szOut+=szTemp;
				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(50); //(Dispatch message)
			}
			catch(...)
			{
			}
			prs->MoveNext();
			nIndex++;
		}

		prs->Close();

		delete prs;
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "E: EVENT table: %d items retrieved\r\n", nIndex);//  Sleep(50); //(Dispatch message)

	}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

}
								if((!bStatusChange)&&(!bSQLerror))
								{
									// we have a complete list, so let's delete from the event list what events we do not have in the temp list
									// only do deletes if theres no status change, we know we have a complete list.


									if((!blistsizechanged)&&(!g_psentinel->m_settings.m_bDisableEventMatchOnPosition)) // go by list position
									{
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE cast(server_name as varchar(64)) + cast(server_list as varchar(64)) + cast(event_clip as varchar(64)) + cast(event_title as varchar(64)) + convert(varchar(64), cast(event_calc_start as decimal(38,3))) NOT IN \
(SELECT cast(Temp.server_name as varchar(64)) + cast(Temp.server_list as varchar(64)) + cast(Temp.event_clip as varchar(64)) + cast(Temp.event_title as varchar(64)) + convert(varchar(64), cast(Temp.event_calc_start as decimal(38,3))) FROM %s AS Temp \
JOIN %s AS Events ON Temp.server_name = Events.server_name AND Temp.server_list = Events.server_list AND Temp.event_clip = Events.event_clip AND Temp.event_title = Events.event_title AND Temp.event_position = Events.event_position) AND \
server_name = '%s' AND server_list = %d",
										g_psentinel->m_settings.m_pszLiveEvents,
										g_psentinel->m_settings.m_pszLiveEventsTemp,
										g_psentinel->m_settings.m_pszLiveEvents,
										pChannel->m_pszServerName, //server_name
										pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
										);
									}
									else // otherwise go by time (and status)
									{
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE cast(server_name as varchar(64)) + cast(server_list as varchar(64)) + cast(event_clip as varchar(64)) + cast(event_title as varchar(64)) + convert(varchar(64), cast(event_calc_start as decimal(38,3))) NOT IN \
(SELECT cast(Temp.server_name as varchar(64)) + cast(Temp.server_list as varchar(64)) + cast(Temp.event_clip as varchar(64)) + cast(Temp.event_title as varchar(64)) + convert(varchar(64), cast(Temp.event_calc_start as decimal(38,3))) FROM %s AS Temp \
JOIN %s AS Events ON Temp.server_name = Events.server_name AND Temp.server_list = Events.server_list AND Temp.event_clip = Events.event_clip AND Temp.event_title = Events.event_title AND Temp.event_calc_start >= (Events.event_calc_start - %0.03f) AND Temp.event_calc_start <= (Events.event_calc_start + %0.03f) and (Events.event_position-Events.parent_position) = (Temp.event_position-Temp.parent_position)) AND \
server_name = '%s' AND server_list = %d",
										g_psentinel->m_settings.m_pszLiveEvents,
										g_psentinel->m_settings.m_pszLiveEventsTemp,
										g_psentinel->m_settings.m_pszLiveEvents,
												dblEventTimeTolerance,
												dblEventTimeTolerance,
										pChannel->m_pszServerName, //server_name
										pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
										);
									}

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread delete from temp");  Sleep(50);
if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread 06 LC%d delete from temp: %s",blistsizechanged, szSQL); // Sleep(50); //(Dispatch message)
EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
									if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
									{
								//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
									}

LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);



// dump tables
if(g_psentinel->m_settings.m_bDebugLists)
{
	CRecordset* prs;
	if(g_psentinel->m_settings.m_nDebugListTopCount>0)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top %d * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_nDebugListTopCount,
						g_psentinel->m_settings.m_pszLiveEventsTemp,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}
	else
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_pszLiveEventsTemp,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}


	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
	prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
	if(prs == NULL)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
	}
	else
	{
		int nIndex = 0;
		CString szOut="\r\nF: TEMP Table:";
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut); // Sleep(20); //(Dispatch message)
		while ((!prs->IsEOF()))
		{
			CString szTemp="";
			szOut = "     ";
			try
			{
				prs->GetFieldValue("itemid", szTemp);  //HARDCODE
				szOut+="itemid="; szOut+=szTemp;
				prs->GetFieldValue("event_key", szTemp);  //HARDCODE
				szOut+="\tevent_key="; szOut+=szTemp;
				prs->GetFieldValue("server_name", szTemp);  //HARDCODE
				szOut+="\tserver_name="; szOut+=szTemp;
				prs->GetFieldValue("server_list", szTemp);  //HARDCODE
				szOut+="\tserver_list="; szOut+=szTemp;
				prs->GetFieldValue("list_id", szTemp);  //HARDCODE
				szOut+="\tlist_id="; szOut+=szTemp;
				prs->GetFieldValue("event_id", szTemp);  //HARDCODE
				szOut+="\tevent_id="; szOut+=szTemp;
				prs->GetFieldValue("event_clip", szTemp);  //HARDCODE
				szOut+="\tevent_clip="; szOut+=szTemp;
				prs->GetFieldValue("event_title", szTemp);  //HARDCODE
				szOut+="\tevent_title="; szOut+=szTemp;
				prs->GetFieldValue("event_data", szTemp);  //HARDCODE
				szOut+="\tevent_data="; szOut+=szTemp;
				prs->GetFieldValue("event_type", szTemp);  //HARDCODE
				szOut+="\tevent_type="; szOut+=szTemp;
				prs->GetFieldValue("event_status", szTemp);  //HARDCODE
				szOut+="\tevent_status="; szOut+=szTemp;
				prs->GetFieldValue("event_time_mode", szTemp);  //HARDCODE
				szOut+="\tevent_time_mode="; szOut+=szTemp;
				prs->GetFieldValue("event_start", szTemp);  //HARDCODE
				szOut+="\tevent_start="; szOut+=szTemp;
				prs->GetFieldValue("event_duration", szTemp);  //HARDCODE
				szOut+="\tevent_duration="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_start", szTemp);  //HARDCODE
				szOut+="\tevent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_end", szTemp);  //HARDCODE
				szOut+="\tevent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("event_position", szTemp);  //HARDCODE
				szOut+="\tevent_position="; szOut+=szTemp;
				prs->GetFieldValue("event_last_update", szTemp);  //HARDCODE
				szOut+="\tevent_last_update="; szOut+=szTemp;
				prs->GetFieldValue("parent_duration", szTemp);  //HARDCODE
				szOut+="\tparent_duration="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_start", szTemp);  //HARDCODE
				szOut+="\tparent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_end", szTemp);  //HARDCODE
				szOut+="\tparent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("parent_position", szTemp);  //HARDCODE
				szOut+="\tparent_position="; szOut+=szTemp;
				prs->GetFieldValue("app_data", szTemp);  //HARDCODE
				szOut+="\tapp_data="; szOut+=szTemp;
				prs->GetFieldValue("app_data_aux", szTemp);  //HARDCODE
				szOut+="\tapp_data_aux="; szOut+=szTemp;
				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(50); //(Dispatch message)
			}
			catch(...)
			{
			}
			prs->MoveNext();
			nIndex++;
		}

		prs->Close();

		delete prs;
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "F: TEMP table: %d items retrieved\r\n", nIndex);//  Sleep(50); //(Dispatch message)

	}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

}


// dump tables
if(g_psentinel->m_settings.m_bDebugLists)
{
	CRecordset* prs;
	if(g_psentinel->m_settings.m_nDebugListTopCount>0)
	{

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select top %d * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_nDebugListTopCount,
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}
	else
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select * from %s where server_name = '%s' AND server_list = %d order by %s",
						g_psentinel->m_settings.m_pszLiveEvents,
						pChannel->m_pszServerName,
						pChannel->m_nHarrisListID,
						g_psentinel->m_settings.m_pszDebugOrder
						);
	}


	EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
	prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
	if(prs == NULL)
	{
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
	}
	else
	{
		int nIndex = 0;
		CString szOut="\r\nF: EVENT Table:";
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut); // Sleep(20); //(Dispatch message)
		while ((!prs->IsEOF()))
		{
			CString szTemp="";
			szOut = "     ";
			try
			{
				prs->GetFieldValue("itemid", szTemp);  //HARDCODE
				szOut+="itemid="; szOut+=szTemp;
				prs->GetFieldValue("event_key", szTemp);  //HARDCODE
				szOut+="\tevent_key="; szOut+=szTemp;
				prs->GetFieldValue("server_name", szTemp);  //HARDCODE
				szOut+="\tserver_name="; szOut+=szTemp;
				prs->GetFieldValue("server_list", szTemp);  //HARDCODE
				szOut+="\tserver_list="; szOut+=szTemp;
				prs->GetFieldValue("list_id", szTemp);  //HARDCODE
				szOut+="\tlist_id="; szOut+=szTemp;
				prs->GetFieldValue("event_id", szTemp);  //HARDCODE
				szOut+="\tevent_id="; szOut+=szTemp;
				prs->GetFieldValue("event_clip", szTemp);  //HARDCODE
				szOut+="\tevent_clip="; szOut+=szTemp;
				prs->GetFieldValue("event_title", szTemp);  //HARDCODE
				szOut+="\tevent_title="; szOut+=szTemp;
				prs->GetFieldValue("event_data", szTemp);  //HARDCODE
				szOut+="\tevent_data="; szOut+=szTemp;
				prs->GetFieldValue("event_type", szTemp);  //HARDCODE
				szOut+="\tevent_type="; szOut+=szTemp;
				prs->GetFieldValue("event_status", szTemp);  //HARDCODE
				szOut+="\tevent_status="; szOut+=szTemp;
				prs->GetFieldValue("event_time_mode", szTemp);  //HARDCODE
				szOut+="\tevent_time_mode="; szOut+=szTemp;
				prs->GetFieldValue("event_start", szTemp);  //HARDCODE
				szOut+="\tevent_start="; szOut+=szTemp;
				prs->GetFieldValue("event_duration", szTemp);  //HARDCODE
				szOut+="\tevent_duration="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_start", szTemp);  //HARDCODE
				szOut+="\tevent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("event_calc_end", szTemp);  //HARDCODE
				szOut+="\tevent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("event_position", szTemp);  //HARDCODE
				szOut+="\tevent_position="; szOut+=szTemp;
				prs->GetFieldValue("event_last_update", szTemp);  //HARDCODE
				szOut+="\tevent_last_update="; szOut+=szTemp;
				prs->GetFieldValue("parent_duration", szTemp);  //HARDCODE
				szOut+="\tparent_duration="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_start", szTemp);  //HARDCODE
				szOut+="\tparent_calc_start="; szOut+=szTemp;
				prs->GetFieldValue("parent_calc_end", szTemp);  //HARDCODE
				szOut+="\tparent_calc_end="; szOut+=szTemp;
				prs->GetFieldValue("parent_position", szTemp);  //HARDCODE
				szOut+="\tparent_position="; szOut+=szTemp;
				prs->GetFieldValue("app_data", szTemp);  //HARDCODE
				szOut+="\tapp_data="; szOut+=szTemp;
				prs->GetFieldValue("app_data_aux", szTemp);  //HARDCODE
				szOut+="\tapp_data_aux="; szOut+=szTemp;
				g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s", szOut);//  Sleep(50); //(Dispatch message)
			}
			catch(...)
			{
			}
			prs->MoveNext();
			nIndex++;
		}

		prs->Close();

		delete prs;
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "F: EVENT table: %d items retrieved\r\n", nIndex);//  Sleep(50); //(Dispatch message)

	}
	LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

}

//readytotest = true;
								} //if(!bStatusChange)
// Let's only update the list changes if we get the full list. - NO, need to know when list size changes as opposed to just event status.
									// record last values if get was successful.
								if(!bSQLerror) memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
								if((bStatusChange)||(bSQLerror))
								{ //	let's make sure we dont miss stuff
									lastlistdata.listdisplay = listdata.listdisplay - 1;  // display is the one that changes the most often, make sure it is different
								}

								if((!bSQLerror)&&(!bStatusChange)&&(g_psentinel->m_settings.m_bDoConfirmGet))
								{
									if(bconfirmed) bconfirmed=false;
									else bconfirmed=true;
								}
								pChannel->m_bChannelThreadPublishingEvents = false;



							}//	if(!pChannel->m_bKillChannelThread)
							// increment exchange mod counter. for events table.
EnterCriticalSection(&g_psentinel->m_data.m_critIncrement);
							if (g_psentinel->m_data.IncrementDatabaseMods(g_psentinel->m_settings.m_pszLiveEvents, dberrorstring)<SENTINEL_SUCCESS)
							{
								//**MSG
							}
LeaveCriticalSection(&g_psentinel->m_data.m_critIncrement);
						}//if((pdb!=NULL)&&(pdbConn!=NULL)&&(g_psentinel->m_settings.m_pszLiveEvents)&&(strlen(g_psentinel->m_settings.m_pszLiveEvents)>0)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))

					} //if((pList)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))  // plist is null if no events in list.
					else
					if(nNumEvents == ADC_NO_EVENTS)  // but this is correct
					{
//Clear out the Events table
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d",
							g_psentinel->m_settings.m_pszLiveEvents,
							pChannel->m_pszServerName,
							pChannel->m_nHarrisListID
							);

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread event: %s", szSQL);  Sleep(50); //(Dispatch message)
EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
						if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
							//**MSG
							bSQLerror = true;
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring);  Sleep(50); //(Dispatch message)
						}

LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
						// increment exchange mod counter. for events table.
EnterCriticalSection(&g_psentinel->m_data.m_critIncrement);
						if (g_psentinel->m_data.IncrementDatabaseMods(g_psentinel->m_settings.m_pszLiveEvents, dberrorstring)<SENTINEL_SUCCESS)
						{
							//**MSG
						}
LeaveCriticalSection(&g_psentinel->m_data.m_critIncrement);
					// record last values if get was successful.
					//	memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
						if(!bSQLerror) memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
						if((bStatusChange)||(bSQLerror))
						{ //	let's make sure we dont miss stuff
							lastlistdata.listdisplay = listdata.listdisplay - 1;  // display is the one that changes the most often, make sure it is different
						}
						if((!bSQLerror)&&(!bStatusChange)&&(g_psentinel->m_settings.m_bDoConfirmGet))
						{
							if(bconfirmed) bconfirmed=false;
							else bconfirmed=true;
						}

					}
//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread leaving critical");  Sleep(20); //(Dispatch message)
					LeaveCriticalSection(&(pList->m_crit));
//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "%s SentinelChannelThread left critical", pChannel->m_pszDesc);  Sleep(20); //(Dispatch message)
				}//if((pList)&&(!pChannel->m_bKillChannelThread)&&(pChannel->m_pAPIConn))

			} //			if(((listdata.listchanged!=lastlistdata.listchanged)||(listdata.listdisplay!=lastlistdata.listdisplay))&&(pChannel->m_pAPIConn)&&(!pChannel->m_bKillChannelThread))//&&((*pbKillConnThread)!=true)) // just do the event chage one as well for now.

/*			else
			if(listdata.listdisplay!=lastlistdata.listdisplay)
			{
				// an event has changed, need to find it and just deal with that one.
				// record last values
				memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
			}
*/
		}

		Sleep(1); //to not peg processor
	}  // while

	Sleep(100);
	//		remove all, since not active, want to purge list.
	if(
			(pdb!=NULL)&&(pdbConn!=NULL)
			&&(g_psentinel->m_settings.m_pszLiveEvents)
			&&(strlen(g_psentinel->m_settings.m_pszLiveEvents)>0)
			&&(pChannel)
			&&(pChannel->m_pszServerName)
			&&(strlen(pChannel->m_pszServerName)>0)
		)
	{

// delete main list
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d",
											g_psentinel->m_settings.m_pszLiveEvents,  // table name
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
										);
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
		if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
		{
			//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
		}
		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

// delete temp list
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d",
											g_psentinel->m_settings.m_pszLiveEventsTemp,  // table name
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
										);
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread event: %s", szSQL);  Sleep(250); //(Dispatch message)

		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
		if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
		{
			//**MSG
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
		}
		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
		// increment exchange mod counter. for events table.
		EnterCriticalSection(&g_psentinel->m_data.m_critIncrement);
		if (g_psentinel->m_data.IncrementDatabaseMods(g_psentinel->m_settings.m_pszLiveEvents, dberrorstring)<SENTINEL_SUCCESS)
		{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "Increment Error: %s", dberrorstring);  Sleep(50); //(Dispatch message)
			//**MSG
		}
		LeaveCriticalSection(&g_psentinel->m_data.m_critIncrement);

	}

	if((pdb)&&(pdbConn))
	{
// new:
//create table Channels (ID int NOT NULL identity(1,1), flags int NOT NULL, description varchar(256), server varchar(32), listid int, list_state int, list_changed int, list_display int, list_syschange int, list_count int, list_lookahead int, list_last_update float);
//create table Connections (server varchar(32) NOT NULL, description varchar(256), flags int, client varchar(32), server_time float, server_status int, server_lists int, server_basis int, server_changed int, server_last_update float);
						
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
list_state = -1, \
list_changed = -1, \
list_display = -1, \
list_syschange = -1, \
list_count = -1, \
list_lookahead = -1, \
list_last_update = -1 WHERE \
server = '%s' AND listid = %d",
			g_psentinel->m_settings.m_pszChannels?g_psentinel->m_settings.m_pszChannels:"Channels",
//			listdata.liststate,
//			listdata.listchanged,				// incremented whenever list size changes
//			listdata.listdisplay,				// incremented whenever an event changes
//			listdata.listsyschange,			// incremented when list related values are changed. 
//			listdata.listcount,					// number of events in list 
//			listdata.lookahead,					// lookahead value

//			(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
//			timestamp.millitm,

			pChannel->m_pszServerName,
			pChannel->m_nHarrisListID
			);
EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
		if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
		{
			// error.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, pChannel->m_pszSource, "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
		}
LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

	

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE criterion = 'List_Status' AND flag LIKE '%s:%02d%%'",
			g_psentinel->m_settings.m_pszExchange?g_psentinel->m_settings.m_pszExchange:"Exchange",
			pChannel->m_pszServerName,
			pChannel->m_nHarrisListID
			);
		EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
		if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
		{
			// error.
g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
		}

		LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
	}

	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:list", "List management thread for %s:%d (%s) is ending.",
											pChannel->m_pszServerName, //server_name
											pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
											pChannel->m_pszDesc
		);  
	
	Sleep(100); //(Dispatch message)

	if(pChannel->m_pAPIConn)	pChannel->m_pAPIConn->m_bListActive[nZeroList] = false;
	pChannel->m_bChannelThreadStarted = false;
	_endthread();
}


#endif



void SentinelXMLHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;
	
	char szSentinelSource[MAX_PATH]; 
	strcpy(szSentinelSource, "SentinelXMLHandler");

//AfxMessageBox("HERE");
	unsigned char* pucTransmitBuffer = (unsigned char*)malloc(SENTINEL_XML_EVENT_MAXPAYLOAD+1);
//AfxMessageBox("HERE NOW");

	if(pucTransmitBuffer==NULL) { _endthread(); return; }  // can't create response


	CBufferUtil bu;

	HRESULT hRes;
	try
	{  

#if _WIN32_WINNT >= 0x0400
		hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		hRes = CoInitialize(NULL);
#endif
	}
	catch(...)
	{
		if(g_psentinel) g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, szSentinelSource, "Exception in CoInitialize");  //(Dispatch message)
	}


	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		char dberrorstring[DB_ERRORSTRING_LEN];
//		char xmlerrorstring[MAX_MESSAGE_LENGTH];
		char errorstring[MAX_MESSAGE_LENGTH];
//		char szSQL[DB_SQLSTRING_MAXLEN];
		strcpy(dberrorstring, "");
		strcpy(errorstring, "");
//		char element[MAX_MESSAGE_LENGTH];

//		AfxMessageBox(szSentinelSource);

		CNetUtil net(false); // local object for utility functions.
		CCortexUtil util;  // cortex utility functions.

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		bool bCloseCommand = false;  // make these not persistent unless a persist command comes in.
		bool bPersist = false;  // make these not persistent unless a persist command comes in.
		int nPeriodic = -1;  
		int nTimed = -1;  

		char* pchBuffer = NULL;
		unsigned long ulBufferLen = 0;
		char* pch = NULL;
		char* pchXML = NULL;
		char* pchXMLStream = NULL;
		unsigned long ulAccumulatedBufferLen = 0;
		unsigned long ulRetry = 0;

		unsigned long ulConnLastMessage = 0;
		unsigned long ulConnTimeout = 0;
		
		char filename[MAX_PATH];
		char lastrxfilename[MAX_PATH];
		char lasttxfilename[MAX_PATH];
		int nRxDupes=0;
		int nTxDupes=0;

		FILE* fp = NULL;

		_timeb timestamp;
		_timeb timeactive;
		_timeb timeperiodic;

		_ftime(&timeactive);
		_ftime(&timeperiodic);


	// 	(*(pClient->m_pulConnections))++;//already exists

		IXMLDOMDocumentPtr pDoc(__uuidof(DOMDocument));  // let's have only one object per connection
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread D"); Sleep(100); //(Dispatch message)
		HRESULT hr;//= pDoc.CreateInstance(__uuidof(DOMDocument));
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread E");  //(Dispatch message)
		hr = pDoc->put_async(VARIANT_FALSE);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread F");  //(Dispatch message)
										
		// Remove document safety options
		IObjectSafety* pSafety=NULL;
		DWORD dwSupported, dwEnabled;
			

		if ( SUCCEEDED(pDoc->QueryInterface(IID_IObjectSafety, (void**)&pSafety)))
		{
			pSafety->GetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, &dwSupported, &dwEnabled );
			pSafety->SetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, dwSupported, 0 );
		}

		// initialize random seed
		srand ( time(NULL) );


		CCortexMessage msg;

		// allocate all the static ones
//		char* m_pchResponse[CX_XML_BUFFER_COUNT] = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};

//		AfxMessageBox("formatting");

		msg.FormatContent(CX_XML_BUFFER_CORTEXBEGIN, MAX_MESSAGE_LENGTH, "<cortex src=\"%s\" version=\"%s\">", 
			(g_psentinel->m_settings.m_pszName?g_psentinel->m_settings.m_pszName:"Sentinel"), 
			CX_XML_VERSION
			);


		msg.SetContent(CX_XML_BUFFER_CORTEXEND, "</cortex>");

		msg.SetContent(CX_XML_BUFFER_FLAGSEND, "</flags>");

		msg.SetContent(CX_XML_BUFFER_DATAEND, "</data>");

		//  ack and nak are the same length so we can allocate it, just replace contents later
		msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

//		AfxMessageBox("set");



		if (FAILED(hRes))
		{
			sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established, but CoInitialize failed.  Unable to process XML.", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,
				pClient->m_socket);

			if(g_psentinel)
			{
				g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, szSentinelSource, errorstring);  //(Dispatch message)
				g_psentinel->SendMsg(CX_SENDMSG_ERROR, szSentinelSource, errorstring);
			}

			goto cleanup;
		}

		
		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);
		if(g_psentinel)
		{
			g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, szSentinelSource, errorstring);  //(Dispatch message)
			g_psentinel->SendMsg(CX_SENDMSG_INFO, szSentinelSource, errorstring);
		}



		while ( 
									(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
//								&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
								&&(pClient->m_socket!=NULL)
								&&(!bCloseCommand)
					)
		{


			pchBuffer = NULL;
			ulBufferLen = 0;

			
			int nReturnCode = net.GetLine((unsigned char**)&pchBuffer, &ulBufferLen, pClient->m_socket, NET_RCV_ONCE, errorstring);
			if(nReturnCode == NET_SUCCESS)
			{
				ulRetry = 0;
				//process any received XML.
				// have to keep accumulating until we find a </cortex> tag.
				if((pchBuffer)&&(ulBufferLen)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
				{
					int nLen = 0;
					if(pchXMLStream) nLen = ulAccumulatedBufferLen;
					pch = (char*) malloc(nLen+ulBufferLen+1);  //term 0
					if(pch)
					{
						char* pchEnd = NULL;
						char* pchNext = NULL;
						if(pchXMLStream)  // we have an old buffer.
						{
							memcpy(pch, pchXMLStream, ulAccumulatedBufferLen);
							memcpy(pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen);
							free(pchXMLStream); 
							pchXMLStream = pch;  // reassign!
							ulAccumulatedBufferLen += ulBufferLen;
							*(pch+ulAccumulatedBufferLen) = 0;  // null term
						}
						else
						{
							// this is new.
							// first we have to skip all chars that are not a '<'
							// pchEnd = strchr(pchBuffer, '<');  // have to find first tag, we are only interested in XML.
							// used to use strchr but if there are leading zeros in the buffer, we never get past them

							pchEnd = pchBuffer;
							while((*pchEnd!='<')&&(pchEnd<pchBuffer+ulBufferLen)) pchEnd++;

							if(pchEnd<pchBuffer+ulBufferLen)
							{
								strcpy(pch, pchEnd);
								ulAccumulatedBufferLen += (ulBufferLen-(pchEnd-pchBuffer));
								pchXMLStream = pch;  // reassign!
								*(pch+ulAccumulatedBufferLen) = 0;  // null term
							}
							else  // not found!
							{
								free(pch);
								pchXMLStream = NULL;
							}
						}

						if(pchBuffer) free(pchBuffer);
						pchBuffer = NULL;
						if(pchXMLStream)
						{

							pchEnd = strstr(pchXMLStream, "</cortex>");
							while((pchEnd)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
							{
							// found a token.
								pchEnd+=strlen("</cortex>");

								pchNext = pchEnd;
								while((*pchNext!='<')&&(pchNext<pchEnd+strlen(pchEnd))) pchNext++;

								if(pchNext<pchEnd+strlen(pchEnd))
								{
									// we found a remainder.
									nLen = strlen(pchNext);
									pch = (char*) malloc(nLen+1);  //term 0
									if(pch)
									{
										memcpy(pch, pchNext, nLen);
										*(pch+nLen) = 0;
									}
								}
								else pch = NULL;


								pchXML = pchXMLStream; // just use it.
								*pchEnd = 0; //null terminate it

								pchXMLStream = pch;  // take the rest of the stream.
								if(pchXMLStream) ulAccumulatedBufferLen = strlen(pchXMLStream);
								else ulAccumulatedBufferLen=0;

								// now have to deal with XML found in pchXML.
								//debug file write
								if((g_psentinel)&&(g_psentinel->m_settings.m_bUseXMLClientLog))
								{
//				AfxMessageBox("logging");
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "begin log receive");  //(Dispatch message)

									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_psentinel->m_settings.m_pszName?g_psentinel->m_settings.m_pszName:"Sentinel"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lastrxfilename, filename)==0)
									{
										nRxDupes++;
									}
									else
									{
										nRxDupes=0;
										strcpy(lastrxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_RX.xml",nRxDupes);

//	AfxMessageBox(filename);

									fp = fopen(filename, "wb");
									if(fp)
									{
//											fwrite(filename, 1, strlen(filename),fp);
										fwrite(pchXML, 1, strlen(pchXML),fp);
										fflush(fp);
										fclose(fp);
										fp = NULL;
									}
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "end log receive");  //(Dispatch message)

								}

								// do stuff here.

								_ftime(&timeactive);  // reset the inactivity timer.


								// first attempt to extract the rxid.
								char* pchRXID = NULL;
								int nIDlen = 0;

								char* pchFind = strstr(pchXML, "<rx rxid=\"");

								if(pchFind)
								{
									pchFind+=strlen("<rx rxid=\"");
									char* pchTerminate = pchFind;
									while(pchTerminate<pchEnd)
									{
										if(
											  (*pchTerminate == '\"')
											||(*pchTerminate == '>')
											||((pchTerminate<pchEnd)&&(*pchTerminate == '/')&&(*(pchTerminate+1) == '>'))
											)
										{
											break;
										}
										else
										{
											pchTerminate+=1;
										}
									}

									nIDlen = pchTerminate-pchFind;
									if(nIDlen>0)
									{
										pchRXID = (char*)malloc(nIDlen+1);
									}
									if(pchRXID)
									{
										memset(pchRXID, 0, nIDlen+1);
										memcpy(pchRXID, pchFind, nIDlen);

										pchTerminate = bu.XMLEncode(pchRXID); // just in case
										if(pchTerminate)
										{
											try{free(pchRXID);} catch(...){}
											pchRXID = pchTerminate;
										}
									}
								}


							//create a response UUID
								UUID uuid;
								UuidCreate(&uuid); // could check response code to be RPC_S_OK ?

					//			CoCreateGuid(&uuid);

								unsigned char* pucUUID = NULL;
								UuidToString(&uuid, &pucUUID);
								// call this later: RpcStringFree(&pucUUID);

								
								bool bResponseSent = false;  // means, sent a response already.  Otherwise have to look at the failure code and deal at the end

								msg.m_nTxStep = CX_XML_BUFFER_CORTEXBEGIN;

								//OK, let's assemble the tx element.

								if(pchRXID)
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUID);
								}


								// then, parse the XML
								msg.m_nFailure=CX_XML_ERR_SUCCESS;
								pDoc->put_async(VARIANT_FALSE);
			//				AfxMessageBox((char*)pchXML);
								hr = pDoc->loadXML((char*)pchXML);
								if(hr!=VARIANT_TRUE)
								{
								//					AfxMessageBox("bad load");
									bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML parse error: [%s]", pchXML );
									g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, szSentinelSource, errorstring);
									//let's try to send a nak.

									msg.m_nFailure=CX_XML_ERR_XMLPARSE;
									msg.m_nType = CX_XML_TYPE_UNK;
									msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
									msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML parse error %d</msg>", msg.m_nFailure);
									msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");
								}
								else
								{
	//								AfxMessageBox("yirgacheff");

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "begin xml parse of [%s]", (char*)pchXML);  //(Dispatch message)
									IXMLDOMNodeListPtr pChildNodes = pDoc->GetchildNodes();

									IXMLDOMNodePtr pNodes[CX_XML_NODECOUNT];
									int n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }

									msg.m_nType = CX_XML_TYPE_UNK;
									msg.SetType("unknown");

									bool bCommandHandled = false;
									if (pChildNodes != NULL)
									{
										IXMLDOMNodePtr pChild;
										while(pChild = pChildNodes->nextNode())
										{
											DOMNodeType nodeType;
											pChild->get_nodeType(&nodeType);

											if(nodeType == NODE_ELEMENT)
											{
												char tag[MAX_PATH]; 
												//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
												util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

											//	AfxMessageBox(tag);

												if(strcmp("cortex", tag)==0)
												{
													pNodes[CX_XML_NODE_CORTEX] = pChild;
													// now get subs to cortex

													pChildNodes = pChild->GetchildNodes();
													if (pChildNodes != NULL)
													{
														while(pChild = pChildNodes->nextNode())
														{
															pChild->get_nodeType(&nodeType);

															if(nodeType == NODE_ELEMENT)
															{
																//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
																util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

														//		AfxMessageBox(tag);

																if(strcmp("rx", tag)==0)
																{
																	pNodes[CX_XML_NODE_RX] = pChild;
																	// now get the "real" rxid;

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
																	//	AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
																			//strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));
								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("rxid", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
																				//strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));

																		//		AfxMessageBox("replace rxid.");

																				if(pchRXID)
																				{
																					try{free(pchRXID);} catch(...){}
																					pchRXID = bu.XMLEncode(tag); // just in case
																					if(pchRXID)
																					{
// reassemble tx element
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
																					}
																				}

																			}
																			else
																			if(strcmp("part", tag)==0)
																			{
																				//  multi part message
																			}
																		}
																	}

																}
																else
																if(strcmp("cmd", tag)==0)
																{
																	pNodes[CX_XML_NODE_CMD] = pChild;
																	pNodes[CX_XML_NODE_OPTIONS] = NULL;
																	pNodes[CX_XML_NODE_DATA] = NULL;

																	// now get attribs and then get subs, data and options

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
								//										AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
																			//strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));
								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("type", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
																				//strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));
																				msg.SetType(tag);

//																																		AfxMessageBox(tag);

																				if(strcmp("ack", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_ACK;
																				}
																				else
																				if(strcmp("nak", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_NAK;
																				}
																				else
																				if(strcmp("persist", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PERSIST;
																					bPersist = true;
																				}
																				else
																				if(strcmp("quit", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_QUIT;
																					bPersist = false;
																				}
																				else
																				if(strcmp("status", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_STATUS;
																				}
																				else
																				if(strcmp("bye", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_BYE;
																				}
																				else
																				if(strcmp("get_version", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_GETVERSION;
																				}
																				else
																				if(strcmp("ping", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PING;
																				}
																				else

																					

/////////////////////////////////////////////////////////////////////////////////////////////////
////                                    begin Sentinel specific commands
//#define SENTINEL_XML_TYPE_GETCONN				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
//#define SENTINEL_XML_TYPE_GETLIST				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
//#define SENTINEL_XML_TYPE_GETEVENT			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
																				if(strcmp("get_conn", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETCONN;
																				}
																				else
																				if(strcmp("get_list", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETLIST;
																				}
																				else
																				if(strcmp("get_event", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETEVENT;
																				}
																					
////                                    end Sentinel specific commands
/////////////////////////////////////////////////////////////////////////////////////////////////


																				
																			}
																			else
																			if(strcmp("user", tag)==0)
																			{
																			}
																			else
																			if(strcmp("password", tag)==0)
																			{
																			}
																		}
																	}


																	IXMLDOMNodeListPtr pCmdChildNodes = pChild->GetchildNodes();
																	if (pCmdChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pCmdChild;
																		while(pCmdChild = pCmdChildNodes->nextNode())
																		{
																			pCmdChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				//strncpy(tag, W2T(pCmdChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pCmdChild->GetnodeName()));
																				if(strcmp("options", tag)==0)
																				{
																					pNodes[CX_XML_NODE_OPTIONS] = pCmdChild;
																				}
																				else
																				if(strcmp("data", tag)==0)
																				{
																					pNodes[CX_XML_NODE_DATA] = pCmdChild;
																				}
																			}
																		}
																	}
																}
															}
														}
														// by here, all the necessary stuff should be filled out.

//														AfxMessageBox("here");
												

														switch(msg.m_nType)
														{
														case CX_XML_TYPE_ACK://						1	// 4.1 ack Acknowledges a message with success
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the ack

																
															} break; //case CX_XML_TYPE_ACK
														case CX_XML_TYPE_NAK://						2	// 4.2 nak Acknowledges a message with failure, or cancels
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the nak

															} break; //case CX_XML_TYPE_NAK
														case CX_XML_TYPE_PERSIST://				3	// 4.3 persist Requests a persistent connection
															{
																//persistence already done, above
																// set timeout and hearbeat intervals
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"persist\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																if(pNodes[CX_XML_NODE_OPTIONS])
																{

																	// have to return flags
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																	IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																	if (pOptChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pOptChild;
																		while(pOptChild = pOptChildNodes->nextNode())
																		{
																			pOptChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				//strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pOptChild->GetnodeName()));
																				if(strcmp("timeout", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						nTimed = atoi(pchTmp);

																						_snprintf(tag, MAX_PATH, "<timeout>%d</timeout>", nTimed);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;
																					}
																				}
																				else
																				if(strcmp("heartbeat", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						strncpy(tag, pchTmp, MAX_PATH);

																						if((stricmp("yes", tag)==0)||(stricmp("true", tag)==0))
																						{
																							nPeriodic = 30;
																						}
																						else
																						if((stricmp("no", tag)==0)||(stricmp("false", tag)==0))
																						{
																							nPeriodic = -1;
																						}
																						else
																						{
																							nPeriodic = atoi(tag);
																						}

																						_snprintf(tag, MAX_PATH, "<heartbeat>%d</heartbeat>", nPeriodic);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );


																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;

																					}
																				}
																			}
																		}
																	}
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																}
																
															} break; //case CX_XML_TYPE_PERSIST
														case CX_XML_TYPE_QUIT://					4	// 4.4 quit Disconnects a persistent connection
															{
																//already done, above
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"quit\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																
															} break; //case CX_XML_TYPE_QUIT
														case CX_XML_TYPE_STATUS://				5	// 4.5 status Obtains status information regarding the module
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"status\" code=\"0\">");

																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																char* pchStatus = g_psentinel->m_data.GetStatusText(&ulDataLen);
																if(pchStatus)
																{
																	msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<status><code>0x%08x</code><text>%s</text></status>",
																		ulDataLen,
																		pchStatus	
																		);
																}
																else
																{
																	msg.SetContent(CX_XML_BUFFER_DATA, 
																		"<status><code>0xffffffff</code><text>unavailable</text></status>"
																		);

																}

																if(pchStatus){try { free(pchStatus); } catch(...){}  pchStatus=NULL;}

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																if(g_psentinel->m_settings.m_bUseChecksumInXML)
																{
																	msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																		ulDataLen,
																		net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																		);
																}
																else
																{
																	msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\">",
																		ulDataLen
																		);
																}
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


															} break; //case CX_XML_TYPE_STATUS
														case CX_XML_TYPE_BYE://						6	// 4.6 bye Causes the module to shut down
															{

g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel", "The Global Kill XML command has been received.");  //(Dispatch message)

																bSetGlobalKill = true;
																g_psentinel->m_data.m_bQuietKill = true;
																bCloseCommand = true;

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"bye\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_GETVERSION://		7	// 4.7 get_version gets the version of the module as well as the build date
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_version\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<version>%s</version><build>%s %s</build>",
																		CX_CURRENT_VERSION,
																		 __DATE__,
																		 __TIME__	
																		);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}

																if(g_psentinel->m_settings.m_bUseChecksumInXML)
																{
																	msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																		ulDataLen,
																		net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																		);
																}
																else
																{
																	msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\">",
																		ulDataLen
																		);
																}

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_PING://					8	// 4.8 ping just sends an ack to indicate it is alive
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"ping\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE


////////////////////////////////////////////////////////
// begin Sentinel specific XML commands

														case SENTINEL_XML_TYPE_GETCONN://				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_conn\" code=\"%d\">", g_psentinel->m_data.m_nNumConnectionObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_psentinel->m_data.m_critConns);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_psentinel->m_data.m_ppConnObj)&&(g_psentinel->m_data.m_nNumConnectionObjects))
																{

																	//let's see if we have any options.

																	char* pchServer = NULL;  //server name search
																	int nActive =-1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{

																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					//strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					util.ConvertWideToT(tag, MAX_PATH, &(pOptChild->GetnodeName()));
																					if(strcmp("active", tag)==0)
																					{
																				//		AfxMessageBox("active");
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(stricmp("yes", pchTmp)==0) nActive = 1;
																								else if(stricmp("no", pchTmp)==0) nActive = 0;
																								else if(stricmp("true", pchTmp)==0) nActive = 1;
																								else if(stricmp("false", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}
																					//		AfxMessageBox("active 2");

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																				//			AfxMessageBox(tag);

																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_psentinel->m_data.m_nNumConnectionObjects)
																	{

																		if(g_psentinel->m_data.m_ppConnObj[m])
																		{
																			bool bInclude = true;
																			CSentinelConnectionObject* pObj = g_psentinel->m_data.m_ppConnObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																	//		AfxMessageBox("here");
																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																	//			AfxMessageBox("here 2");
																			if(bInclude)
																			{
																				systemstatus				sys; 
																				TConnectionStatus   status;

																				sys.systemlistcount=-1;
																				sys.syschanged=-1;
																				sys.systemfrx=0x29;
																				sys.systemdf = 1;
																				
																				status = netNoConnection;


																				EnterCriticalSection(&pObj->m_critAPI);

																				if(pObj->m_pAPIConn)
																				{
																					memcpy(&sys, &pObj->m_pAPIConn->m_SysData, sizeof(systemstatus));
																					memcpy(&status, &pObj->m_pAPIConn->m_Status, sizeof(TConnectionStatus));
																					LeaveCriticalSection(&pObj->m_critAPI);
																					if((sys.systemfrx==0x29)
																						&&(sys.systemdf))
																					{
																						strcpy(tag, "29.97");
																					}
																					else
																					{
																						sprintf(tag, "%02x",sys.systemfrx);
																					}
																				}
																				else
																				{
																					LeaveCriticalSection(&pObj->m_critAPI);
																					sprintf(tag, "N/A");
																				}

																				_ftime(&timestamp);
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<aconn>\
<config active=\"%d\">\
<server>%s</server>\
<client>%s</client>\
<port>N/A</port>\
<basis>%s</basis>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<server_time>%.03f</server_time>\
<server_status>%d</server_status>\
<server_lists>%d</server_lists>\
<server_changed>%d</server_changed>\
<server_last_update>%.03f</server_last_update>\
<message>%s</message>\
</status>\
</aconn>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pObj->m_pszServerName,
																					pObj->m_pszClientName,
																					tag,
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(pObj->m_dblLastServerTimeMS/1000.0),
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? -1 : (status&0xff)),
																					(sys.systemlistcount==0xff)?-1:(sys.systemlistcount&0xff),
																					(sys.syschanged&0xff),
																					pObj->m_dblUpdateTime,
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? (((pObj->m_ulStatus&SENTINEL_ICON_MASK) == SENTINEL_STATUS_ERROR)?"connection error":"not connected"):"")
																																										
																				);
																					
																			

																				msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}

																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;

																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_psentinel->m_data.m_critConns);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}

																if(g_psentinel->m_settings.m_bUseChecksumInXML)
																{
																	msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																		ulDataLen,
																		net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																		);
																}
																else
																{
																	msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\">",
																		ulDataLen
																		);
																}

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);


															} break; //case SENTINEL_XML_TYPE_GETCONN

														case SENTINEL_XML_TYPE_GETLIST://				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_list\" code=\"%d\">", g_psentinel->m_data.m_nNumChannelObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																


														//		need to add provisions for returning virtual 
																//first loop thru the actual channels
																//after, loop thru the virtuals.


																//let's see if we have any options.

																char* pchServer = NULL;  //server name search
																int nActive =-1;
																int nID=-1;
																int nListNum = -1;

																if(pNodes[CX_XML_NODE_OPTIONS])
																{

																	// have to return flags
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																	IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																	if (pOptChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pOptChild;
																		while(pOptChild = pOptChildNodes->nextNode())
																		{
																			pOptChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				//strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pOptChild->GetnodeName()));
																				if(strcmp("id", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						if(strlen(pchTmp)>0)
																						{
																							nID = atoi(pchTmp);
																							if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																							// all other input ignored
																						}

																						_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;
																					}
																				}
																				else
																				if(strcmp("list_number", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						if(strlen(pchTmp)>0)
																						{
																							nListNum = atoi(pchTmp);
																							if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																							// all other input ignored
																						}

																						_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;
																					}
																				}
																				else
																				if(strcmp("active", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						if(strlen(pchTmp)>0)
																						{
																							if(strcmp("0", pchTmp)==0) nActive = 0;
																							else if(strcmp("1", pchTmp)==0) nActive = 1;
																							else if(stricmp("yes", pchTmp)==0) nActive = 1;
																							else if(stricmp("no", pchTmp)==0) nActive = 0;
																							else if(stricmp("true", pchTmp)==0) nActive = 1;
																							else if(stricmp("false", pchTmp)==0) nActive = 0;
																							// all other input ignored
																						}

																						_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;
																					}
																				}
																				else
																				if(strcmp("server", tag)==0)
																				{
																					pchServer = msg.XMLTextNodeValue(pOptChild);
																					if(pchServer)
																					{
																						_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						// dont free this, we are going to deal with it later
																						//try {free(pchServer);} catch(...){}
																					}
																				}
																			}
																		}
																	}
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																}




EnterCriticalSection(&g_psentinel->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_nNumChannelObjects))
																{


																	int m = 0;
																	while(m<g_psentinel->m_data.m_nNumChannelObjects)
																	{

																		if(g_psentinel->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CSentinelChannelObject* pObj = g_psentinel->m_data.m_ppChannelObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																			if((nID>0)&&(pObj->m_nChannelID != nID))  bInclude = false;
																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				_ftime(&timestamp);

																				tlistdata tlist;
																				tlist.liststate = -1;
																				tlist.listchanged = -1;
																				tlist.listdisplay = -1;
																				tlist.listsyschange = -1;
																				tlist.listcount = -1;
																				tlist.lookahead = -1;



																				if(pObj->m_pcritAPI) EnterCriticalSection(pObj->m_pcritAPI);

																				try
																				{
																					if((pObj->m_ppAPIConn)&&(*(pObj->m_ppAPIConn)))
																					{
																						memcpy(&tlist, &((*(pObj->m_ppAPIConn))->m_ListData[pObj->m_nHarrisListID-1]), sizeof(tlistdata));
																					}
																				}
																				catch(...)
																				{
																				}
																				if(pObj->m_pcritAPI) LeaveCriticalSection(pObj->m_pcritAPI);
																				
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<alist>\
<config active=\"%d\" id=\"%d\">\
<server>%s</server>\
<list_number>%d</list_number>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<list_state>%d</list_state>\
<list_changed>%d</list_changed>\
<list_display>%d</list_display>\
<list_syschange>%d</list_syschange>\
<list_count>%d</list_count>\
<list_lookahead>%d</list_lookahead>\
<list_last_update>%.3f</list_last_update>\
</status>\
</alist>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pObj->m_nChannelID,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					tlist.liststate,
																					tlist.listchanged,
																					tlist.listdisplay,
																					tlist.listsyschange,
																					tlist.listcount,
																					tlist.lookahead,
																					pObj->m_dblUpdateTime
																																										
																				);
																					
											

																				msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_LIST_MAXPAYLOAD, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}


																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_psentinel->m_data.m_critChannels);

// now do virtual

EnterCriticalSection(&g_psentinel->m_data.m_critVirtualChannels);

																if((g_psentinel->m_data.m_ppVirtualChannelObj)&&(g_psentinel->m_data.m_nNumVirtualChannelObjects))
																{
																	int m = 0;
																	while(m<g_psentinel->m_data.m_nNumVirtualChannelObjects)
																	{

																		if(g_psentinel->m_data.m_ppVirtualChannelObj[m])
																		{

																			bool bInclude = true;
																			CSentinelVirtualChannelObject* pObj = g_psentinel->m_data.m_ppVirtualChannelObj[m];


																			if(pObj->m_usType == SENTINEL_TYPE_REMOTE)
																			{
																				// get the <alist> from the remote server
																				if(g_psentinel->m_data.m_socketRemote==NULL)  // no connection, need to report this
																				{
																					// local info only, look it up.
																					CSentinelChannelObject* pLocalObj = NULL;

	EnterCriticalSection(&g_psentinel->m_data.m_critRemoteChannels);

																					int lo=0;
																					if(g_psentinel->m_data.m_ppRemoteChannelObj)
																					{
																						while(lo<g_psentinel->m_data.m_nNumRemoteChannelObjects)
																						{
																							if(g_psentinel->m_data.m_ppRemoteChannelObj[lo])
																							{
																								if(	g_psentinel->m_data.m_ppRemoteChannelObj[lo]->m_nChannelID == pObj->m_nMapID )
																								{
																									pLocalObj = g_psentinel->m_data.m_ppRemoteChannelObj[lo];
																									break;
																								}
																							}
																							lo++;
																						}
																					}

																					if(pLocalObj)
																					{
																						if((pchServer)&&(pLocalObj->m_pszServerName)&&(strcmp(pchServer,pLocalObj->m_pszServerName)!=0)) bInclude = false;

																						if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)&&(pLocalObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																						else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))&&(!(pLocalObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																						if((nID>0)&&(pObj->m_nChannelID != nID))  bInclude = false;  // has to be the public virutal channel ID
																						if((nListNum>0)&&(pLocalObj->m_nHarrisListID != nListNum))  bInclude = false;
																					}
																					else bInclude = false;  // because didn't find a local object with the ID.

																					if(bInclude)
																					{
																						// just add up the remote info that we have locally.
																						// and something that indicates not connected.



																					}

	LeaveCriticalSection(&g_psentinel->m_data.m_critRemoteChannels);


																				}
																				else
																				{
																					//get the <alist> from the remote soyvah
																				}
																			}
																			else
																			{
																				// local, look it up.
																				CSentinelChannelObject* pLocalObj = NULL;

EnterCriticalSection(&g_psentinel->m_data.m_critChannels);

																				int lo=0;
																				if(g_psentinel->m_data.m_ppChannelObj)
																				{
																					while(lo<g_psentinel->m_data.m_nNumChannelObjects)
																					{
																						if(g_psentinel->m_data.m_ppChannelObj[lo])
																						{
																							if(	g_psentinel->m_data.m_ppChannelObj[lo]->m_nChannelID == pObj->m_nMapID )
																							{
																								pLocalObj = g_psentinel->m_data.m_ppChannelObj[lo];
																								break;
																							}
																						}
																						lo++;
																					}
																				}

																				if(pLocalObj)
																				{
																					if((pchServer)&&(pLocalObj->m_pszServerName)&&(strcmp(pchServer,pLocalObj->m_pszServerName)!=0)) bInclude = false;

																					if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)&&(pLocalObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																					else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))&&(!(pLocalObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																					if((nID>0)&&(pObj->m_nChannelID != nID))  bInclude = false;  // has to be the public virutal channel ID
																					if((nListNum>0)&&(pLocalObj->m_nHarrisListID != nListNum))  bInclude = false;
																				}
																				else bInclude = false;  // because didn't find a local object with the ID.

																				if(bInclude)
																				{
																					_ftime(&timestamp);

																					tlistdata tlist;
																					tlist.liststate = -1;
																					tlist.listchanged = -1;
																					tlist.listdisplay = -1;
																					tlist.listsyschange = -1;
																					tlist.listcount = -1;
																					tlist.lookahead = -1;



																					if(pLocalObj->m_pcritAPI) EnterCriticalSection(pLocalObj->m_pcritAPI);

																					try
																					{
																						if((pLocalObj->m_ppAPIConn)&&(*(pLocalObj->m_ppAPIConn)))
																						{
																							memcpy(&tlist, &((*(pLocalObj->m_ppAPIConn))->m_ListData[pLocalObj->m_nHarrisListID-1]), sizeof(tlistdata));
																						}
																					}
																					catch(...)
																					{
																					}
																					if(pLocalObj->m_pcritAPI) LeaveCriticalSection(pLocalObj->m_pcritAPI);
																					
																					msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																						"<alist>\
<config active=\"%d\" id=\"%d\" virtual=\"1\">\
<server>%s</server>\
<list_number>%d</list_number>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<list_state>%d</list_state>\
<list_changed>%d</list_changed>\
<list_display>%d</list_display>\
<list_syschange>%d</list_syschange>\
<list_count>%d</list_count>\
<list_lookahead>%d</list_lookahead>\
<list_last_update>%.3f</list_last_update>\
</status>\
</alist>",
																						(((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)&&(pLocalObj->m_ulFlags&SENTINEL_FLAG_ENABLED))?1:0),
																						pObj->m_nChannelID,
																						pLocalObj->m_pszServerName,
																						pLocalObj->m_nHarrisListID,
																						pObj->m_pszDesc, // replace desc with virtual channel desc
																						timestamp.time, timestamp.millitm,
																						tlist.liststate,
																						tlist.listchanged,
																						tlist.listdisplay,
																						tlist.listsyschange,
																						tlist.listcount,
																						tlist.lookahead,
																						pLocalObj->m_dblUpdateTime
																																											
																					);
																						
												

																					msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_LIST_MAXPAYLOAD, "%s%s", 
																						(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																						(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																						);
																					
																					msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																				}
LeaveCriticalSection(&g_psentinel->m_data.m_critChannels);
																			}



																		}
																		m++;
																	}


																}

LeaveCriticalSection(&g_psentinel->m_data.m_critVirtualChannels);


																if(pchServer)
																{ try {free(pchServer);} catch(...){} }
																pchServer= NULL;

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}

																if(g_psentinel->m_settings.m_bUseChecksumInXML)
																{
																	msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																		ulDataLen,
																		net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																		);
																}
																else
																{
																	msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\">",
																		ulDataLen
																		);
																}

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case SENTINEL_XML_TYPE_GETLIST
														case SENTINEL_XML_TYPE_GETEVENT://			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
															{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "get_event received");  //(Dispatch message)

																msg.ClearContent(CX_XML_BUFFER_MSG);
																int nGlobalCount =0;
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_psentinel->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_nNumChannelObjects))
																{

																	//let's see if we have any options.

//																	char* pchServer = NULL;  //server name search
//																	int nActive =-1;
																	int nID=-1;
																	int nLimitNum = -1;

																	if(g_psentinel->m_settings.m_nEventLimitInXML>0)
																		nLimitNum = g_psentinel->m_settings.m_nEventLimitInXML;  //max, can be further limited by limit tag

																	int nDone = -1;
																	bool bSource=0;  // defaults to off, since ID is required to get any data, so you know the source.  and it is one list at a time

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{
																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					//strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					util.ConvertWideToT(tag, MAX_PATH, &(pOptChild->GetnodeName()));
																					if(strcmp("id", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nID = atoi(pchTmp);
																								if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("limit", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								int nLimit = atoi(pchTmp);
																								if(nLimit > 0) 
																								{
																									if(nLimitNum > nLimit) nLimitNum = nLimit; // further restriction.
																									 // = -1;  // zero and negative are invalid IDs
																								}
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<limit>%d</limit>", nLimitNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("done", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nDone = 0;
																								else if(strcmp("1", pchTmp)==0) nDone = 1;
																								else if(stricmp("yes", pchTmp)==0) nDone = 1;
																								else if(stricmp("no", pchTmp)==0) nDone = 0;
																								else if(stricmp("true", pchTmp)==0) nDone = 1;
																								else if(stricmp("false", pchTmp)==0) nDone = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<done>%d</done>", nDone);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("source", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) bSource = false;
																								else if(strcmp("1", pchTmp)==0) bSource = true;
																								else if(stricmp("yes", pchTmp)==0) bSource = true;
																								else if(stricmp("no", pchTmp)==0) bSource = false;
																								else if(stricmp("true", pchTmp)==0) bSource = true;
																								else if(stricmp("false", pchTmp)==0) bSource = false;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<source>%d</source>", (bSource?1:0));
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}

																					/*
																					else
																					if(strcmp("list_number", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nListNum = atoi(pchTmp);
																								if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("active", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(stricmp("yes", pchTmp)==0) nActive = 1;
																								else if(stricmp("no", pchTmp)==0) nActive = 0;
																								else if(stricmp("true", pchTmp)==0) nActive = 1;
																								else if(stricmp("false", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																					*/
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "get_event event data start");  //(Dispatch message)

																	int m = 0;
																	while(m<g_psentinel->m_data.m_nNumChannelObjects)
																	{

																		if(g_psentinel->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CSentinelChannelObject* pObj = g_psentinel->m_data.m_ppChannelObj[m];

/*
																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

*/																		if(
																				//	(nID>0)&&  //commenting this out makes it mandatory!
																					(pObj->m_nChannelID != nID)
																				)  
																				bInclude = false;

//																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				// found a channel that works, now must loop thru the events, filtering if nec.

																				// need to allocate a big buffer and deal with directly, rather than allocating new for every event.
																			
/* // was this, which worked but slowly.  below is new
																				if(pObj->m_ppevents)
																				{
																					 
																					int q = 0;
																					int nCount = 0;
																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;
																						unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));

																						if(pObj->m_ppevents[q])
																						{
																							CSentinelEventObject* pEObj = pObj->m_ppevents[q];

																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;
																							if(bInclude)
																							{

																								_ftime(&timestamp);

																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
																								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																									"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_ulControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CSentinelEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																								);
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																								msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );

																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
*/




																				char* pchPayload = (char*)malloc(SENTINEL_XML_EVENT_MAXPAYLOAD);
																				char* pchAppend = pchPayload;

																				// re-use errorstring, why not.
																		//		int nTimes[10] = {0,0,0,0,0,0,0,0,0,0};
																				int nClock = clock();
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "start event payload assembly");  //(Dispatch message)
int starttime = clock();
int perf_encode_begin = 0;
int perf_encode_end = 0;
int perf_format_end = 0;
int perf_realloc_begin = 0;
int perf_realloc_end = 0;
int perf_memcp_end = 0;

int perf_encode_total = 0;
int perf_format_total = 0;
int perf_free_total = 0;
int perf_realloc_total = 0;
int perf_memcp_total = 0;

																				if((pObj->m_ppevents)&&(pchPayload))
																				{
																					unsigned long ulBufferLen = SENTINEL_XML_EVENT_MAXPAYLOAD;
																					char* pchBufferEnd = (pchPayload+SENTINEL_XML_EVENT_MAXPAYLOAD);
																					int nEventLen = 0;
																					unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));
																					int q = 0;
																					int nCount = 0;
																					int nBuffers = 1;
																					int nTempOffset;

																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;

																						if(pObj->m_ppevents[q])
																						{
																					//		nTimes[0] += (clock() - nClock); nClock = clock(); 

																							CSentinelEventObject* pEObj = pObj->m_ppevents[q];
																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;

																				//			nTimes[1] += (clock() - nClock); nClock = clock(); 
																							if(bInclude)
																							{

																							//	_ftime(&timestamp);
																					//		nTimes[2] += (clock() - nClock); nClock = clock(); 

perf_encode_begin = clock();
																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
perf_encode_end = clock();
perf_encode_total += (perf_encode_end - perf_encode_begin);
																					//		nTimes[3] += (clock() - nClock); nClock = clock(); 

																								if(bSource)
																								{
																									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, 
																										"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<SOM>%d</SOM>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_ulControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulSOMMS,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CSentinelEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																									);
																								}
																								else
																								{
																									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, 
																										"<aitem id=\"%d\">\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<SOM>%d</SOM>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_ulControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulSOMMS,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CSentinelEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																									);
																								}
																					//		nTimes[4] += (clock() - nClock); nClock = clock(); 
perf_format_end = clock();
perf_format_total += (perf_format_end - perf_encode_end);
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																						//	nTimes[5] += (clock() - nClock); nClock = clock(); 
																								nEventLen = strlen(errorstring)+1;//+1 for term zero
perf_realloc_begin = clock();
perf_free_total += (perf_realloc_begin - perf_format_end );

																								if(pchAppend+nEventLen>pchBufferEnd)  // if must be changed to while if SENTINEL_XML_EVENT_MAXPAYLOAD defined < MAX_MESSAGE_LENGTH (buffer vs errorstring)
																								{
																									// need to realloc
																									nBuffers++;
																									ulBufferLen = SENTINEL_XML_EVENT_MAXPAYLOAD*nBuffers;
																									char* pchBufferNew = (char*)malloc(ulBufferLen);
																									if(pchBufferNew)
																									{
																										nTempOffset = pchAppend+1 - pchPayload;
																										memcpy(pchBufferNew, pchPayload, nTempOffset);
																										try {free(pchPayload);} catch(...){}
																										pchPayload = pchBufferNew;
																										pchBufferEnd = (pchPayload+ulBufferLen);
																										pchAppend = pchPayload+nTempOffset-1;																									

																									}
																								}
																								
perf_realloc_end = clock();
perf_realloc_total += (perf_realloc_end - perf_realloc_begin );

																								// append
																								memcpy(pchAppend, errorstring, nEventLen);
																								pchAppend += nEventLen-1; // append over the term 0 next time.

perf_memcp_end = clock();
perf_memcp_total += (perf_memcp_end - perf_realloc_end );

																						//	nTimes[6] += (clock() - nClock); nClock = clock(); 


/*
																								msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
*/
																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
																				msg.ClearContent(CX_XML_BUFFER_DATA);
																				if(pchPayload)
																				{
																					msg.m_pchResponse[CX_XML_BUFFER_DATA] = pchPayload;		
																					ulDataLen = pchAppend - pchPayload;
																				}

int perf_total = clock()-starttime;
int perf_idle_total = perf_total-	perf_encode_total-	perf_format_total-	perf_free_total-	perf_realloc_total-	perf_memcp_total;

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "end event payload assembly: total %dms\n\
	perf_encode_total = %dms (%.3f%%),\n\
	perf_format_total = %dms (%.3f%%),\n\
	perf_free_total = %dms (%.3f%%),\n\
	perf_realloc_total = %dms (%.3f%%),\n\
	perf_memcp_total = %dms (%.3f%%),\n\
	perf_idle_total = %dms (%.3f%%),\n",
	perf_total, 
	perf_encode_total, (100.0*(double)perf_encode_total/(double)perf_total),
	perf_format_total, (100.0*(double)perf_format_total/(double)perf_total),
	perf_free_total, (100.0*(double)perf_free_total/(double)perf_total),
	perf_realloc_total, (100.0*(double)perf_realloc_total/(double)perf_total),
	perf_memcp_total, (100.0*(double)perf_memcp_total/(double)perf_total),
	perf_idle_total, (100.0*(double)perf_idle_total/(double)perf_total)

	);  //(Dispatch message)

																			}

																		}
																		m++;
																	}

/*																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;
*/
																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_psentinel->m_data.m_critChannels);

/* // not needed now, we tally the buffer as we go
																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																*/
																if(g_psentinel->m_settings.m_bUseChecksumInXML)
																{
																	msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																		ulDataLen,
																		net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																		);
																}
																else
																{
																	msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\">",
																		ulDataLen
																		);
																}

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_event\" code=\"%d\">", nGlobalCount);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case SENTINEL_XML_TYPE_GETEVENT

// end Sentinel specific XML commands
////////////////////////////////////////////////////////
														case CX_XML_TYPE_UNK://						0 // unknown
														default:
															{
																sprintf(errorstring, "Unsupported XML command: [%s]", (msg.m_pchType?msg.m_pchType:"") );
																g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, szSentinelSource, errorstring);
																//let's try to send a nak.

																msg.m_nType = CX_XML_TYPE_UNK;
																msg.m_nFailure=CX_XML_ERR_BADCMD;
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"%s\" code=\"%d\">", (msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Unsupported XML command [%s] (error %d)</msg>",(msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

															} break; //case CX_XML_TYPE_UNK://						0 // unknown
														}
															
														
														if(!bPersist) bCloseCommand = true;

													}
													else // no child nodes to cortex - can't identify command.
													{
														bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
														sprintf(errorstring, "Cortex XML format error: [%s]", pchXML );
														g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, szSentinelSource, errorstring);
														//let's try to send a nak.

														msg.m_nType = CX_XML_TYPE_UNK;
														msg.m_nFailure=CX_XML_ERR_NOCMD;
														msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
														msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Cortex XML format error %d</msg>", msg.m_nFailure);
														msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

													}
												}
											}
										}
									}
									else
									{
										// no child nodes, can't do nuttin.
										bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
										sprintf(errorstring, "XML format error: [%s]", pchXML );
										g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, szSentinelSource, errorstring);
										//let's try to send a nak.

										msg.m_nType = CX_XML_TYPE_UNK;
										msg.m_nFailure=CX_XML_ERR_NOCHILD;
										msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
										msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML format error %d</msg>", msg.m_nFailure);
										msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

									}
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "end xml parse");  //(Dispatch message)

									n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }
								}

								if(!bResponseSent)
								{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "begin send response");  //(Dispatch message)
int starttime = clock();

								// send it
		_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;


									// to avoid small packets in separate sends, which has been shown to have performance problems,
									// we will assemble a large buffer and send it all at once.  If there is more data, we will send another large buffer, etc.


									// unsigned char pucTransmitBuffer[SENTINEL_XML_EVENT_MAXPAYLOAD];
									unsigned long ulTransBufPtr=0; //start at the beginning.
//									unsigned long ulTransBufLen=0; //start with nothing
									bool bTransErr = false;

									while((b<CX_XML_BUFFER_COUNT)&&(!bTransErr))
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											unsigned long ulBufPtr = 0;

											if(ulTransBufPtr+ulBufLen>SENTINEL_XML_EVENT_MAXPAYLOAD)
											{
												ulBufPtr = SENTINEL_XML_EVENT_MAXPAYLOAD - ulTransBufPtr;
												memcpy(pucTransmitBuffer+ulTransBufPtr, msg.m_pchResponse[b], ulBufPtr);
												ulTransBufPtr = SENTINEL_XML_EVENT_MAXPAYLOAD;
											}
											else
											{
												memcpy(pucTransmitBuffer+ulTransBufPtr, msg.m_pchResponse[b], ulBufLen);
												// should check return code but ah, memcpy is pretty reliable (I just jinxed it) <- attempt at reverse jinx  (<- d'oh jinxed it again)
												ulTransBufPtr+=ulBufLen;
											}

											bool bRemainder = false;
											
											while((ulTransBufPtr>=SENTINEL_XML_EVENT_MAXPAYLOAD)||(b>=CX_XML_BUFFER_COUNT-1)) // if buffer is full, or we are at the end.
											{
												pucTransmitBuffer[ulTransBufPtr]=0;
												// following lines, uncomment for debugging
//												net.m_ulFlags = (NET_FLAGS_LOGTIMING_RECV|NET_FLAGS_LOGTIMING_SEND);
//												net.InitializeMessaging(&(g_psentinel->m_msgr));
												int perf_send_start=clock();

/*
	if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "begin send response %d", b );  //(Dispatch message)
												int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
	if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "end send response %d: %d ms, %d:%d [%s]", b, clock()- perf_send_start, nReturn, ulBufLen,
		(b==10)?msg.m_pchResponse[b]:"");  //(Dispatch message)
*/
												
	if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "begin send response %d, %d bytes", b, ulTransBufPtr );  //(Dispatch message)
												int nReturn = net.SendLine((unsigned char*)pucTransmitBuffer, ulTransBufPtr, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
	if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "end send response %d: %d ms, %d:%d", b, clock()- perf_send_start, nReturn, ulTransBufPtr);  //(Dispatch message)
												
												
												bRemainder = false;
												ulTransBufPtr = 0;

												
												if(nReturn<NET_SUCCESS)
												{
				if(g_psentinel)
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending response message to %d.%d.%d.%d: %s\r\nResetting connection.", 
											nReturn,
											pClient->m_si.sin_addr.s_net, 
											pClient->m_si.sin_addr.s_host, 
											pClient->m_si.sin_addr.s_lh, 
											pClient->m_si.sin_addr.s_impno,
											pszStatus
										);
					g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, szSentinelSource, errorstring);  //(Dispatch message)
					g_psentinel->SendMsg(CX_SENDMSG_ERROR, szSentinelSource, errorstring);
				}

													bTransErr = true;
													break; // break out and discontinue sending.
												}
												else
												{
													if(ulBufPtr>0)
													{ 
														// have some leftover stuff to send
														memcpy(pucTransmitBuffer, msg.m_pchResponse[b]+ulBufPtr, ulBufLen-ulBufPtr);
														// should check return code but ah, memcpy is pretty reliable (I just jinxed it) <- attempt at reverse jinx  (<- d'oh jinxed it again)
														ulTransBufPtr=ulBufLen;
														ulBufPtr = 0;

														if(b>=CX_XML_BUFFER_COUNT-1) bRemainder = true; // at the end, we might have some left over, so have to go back and transmit the remainder.
													}

													msg.m_nTxStep = b;

													if((b>=CX_XML_BUFFER_COUNT-1)&&(!bRemainder)) break; // break out of the transmit loop if there is nothing left to transmit
												}

											} //while stuff to be transmitted

										}
										b++;
									}
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "end send response %d ms", clock()- starttime );  //(Dispatch message)

								}

								// log it

								//debug file write
								if((g_psentinel)&&(g_psentinel->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
								{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "begin log response");  //(Dispatch message)
									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_psentinel->m_settings.m_pszName?g_psentinel->m_settings.m_pszName:"Sentinel"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lasttxfilename, filename)==0)
									{
										nTxDupes++;
									}
									else
									{
										nTxDupes=0;
										strcpy(lasttxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

									fp = fopen(filename, "wb");
									if(fp)
									{
										int b=CX_XML_BUFFER_CORTEXBEGIN;
										while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
										{
//											fwrite(filename, 1, strlen(filename),fp);
											if(msg.m_pchResponse[b])
											{
												if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

												fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
												fflush(fp);
											}
											b++;

										}
										fclose(fp);
										fp = NULL;
									}
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "end log response");  //(Dispatch message)
								}

//AfxMessageBox("001");
								int b=CX_XML_BUFFER_TX;
								while(b<CX_XML_BUFFER_DATAEND)
								{
									if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
									{
										try { free(msg.m_pchResponse[b]); } catch(...){}
										msg.m_pchResponse[b] = NULL;
									}
									b++;
								}

//AfxMessageBox("002");
								if(pucUUID)
								{
									try { RpcStringFree(&pucUUID); } catch(...){}
								}
								pucUUID = NULL;

//AfxMessageBox("003");
								if(pchXML) 
								{
									try { free(pchXML); } catch(...){}
								}
								pchXML = NULL;
//AfxMessageBox("004");

								if(pchRXID) 
								{
									try { free(pchRXID); } catch(...){}
								}
								pchRXID = NULL;

//AfxMessageBox("005");
								if (pchXMLStream) 
									pchEnd = strstr(pchXMLStream, "</cortex>");
								else pchEnd = NULL;
//AfxMessageBox("006");
							}  // while </cortex> exists.
						}//if(pchXMLStream)
						// dont do a free(pch);  
					}  // else out of mem, so just skip
					else
					{
						// out of mem.. .log error?
					}

				} //if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))

			} // if getline succeeds.
			else
			{
				// here, we either have timed out because theres no data, or, the connection has been lost.
				// if we lost the conn, no need to re-establish, we are a client handler, let the client reconnect if they want.
				if(nReturnCode == NET_ERROR_CONN)		// connection lost
				{
					ulRetry++;

					if(g_psentinel)
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH, "Lost connection from %d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno );

						g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, szSentinelSource, errorstring);  //(Dispatch message)
						g_psentinel->SendMsg(CX_SENDMSG_INFO, szSentinelSource, errorstring);
					}

					break;  // break out of while loop, closes connection, starts again
				}
				else
				{
//									g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, szSentinelSource, "check %d", clock());  //(Dispatch message)

					// we prob just havent gotten a message because no data.  
					// so, check to see if its time for publication service
									// no, do this in the stall loop


				}

			} // end of else from if(nReturnCode == NET_SUCCESS)

			// and lets check if we've timed out on the connection, no matter what the success was
			_ftime( &timestamp );
			if(
				  (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				&&(ulConnTimeout>0)
				&&(ulConnLastMessage>0)
				&&((ulConnLastMessage + ulConnTimeout) < (unsigned long)timestamp.time )
				)
			{
				// we timed out, need to disconnect
				ulRetry++;


				sprintf(errorstring, "Connection from %d.%d.%d.%d timed out.", 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno);

				if(g_psentinel)
				{
					g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, szSentinelSource, errorstring);  //(Dispatch message)
					g_psentinel->SendMsg(CX_SENDMSG_INFO, szSentinelSource, errorstring);
				}

				if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;} //free this memory before breaking out of the while.

				break;  // break out of while loop, closes connection, starts again
			}

			if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;}
			
			if(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) Sleep(1);  // dont peg processor




/*
			// following line fills out the data structure
//			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);
			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Sentinel:XMLHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Sentinel:XMLHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Sentinel:XMLHandlerThread");
						}
					}
				}
			}
*/
			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Sentinel:XMLHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
//						g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, szSentinelSource, "check %d", clock());  //(Dispatch message)


						if(nPeriodic>0)
						{
							_ftime( &timestamp );
							if((timeperiodic.time + nPeriodic)< timestamp.time)
							{
								// send asynchronous message

								//create a response UUID
								UUID uuida;
								UuidCreate(&uuida); // could check response code to be RPC_S_OK ?

								unsigned char* pucUUIDa = NULL;
								UuidToString(&uuida, &pucUUIDa);

								msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUIDa);

								if(pucUUIDa)
								{
									try { RpcStringFree(&pucUUIDa); } catch(...){}
								}
								pucUUIDa = NULL;


								msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"system\" code=\"0\">");

								msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
								msg.ClearContent(CX_XML_BUFFER_FLAGS);

								unsigned long ulDataLen = 0;
								char* pchStatus = g_psentinel->m_data.GetStatusText(&ulDataLen);
								if(pchStatus)
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0x%08x</code><text>%s</text></status><time>%d.%03d</time></system>",
										ulDataLen,
										pchStatus,
										timestamp.time, timestamp.millitm
										);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0xffffffff</code><text>unavailable</text></status><time>%d.%03d</time></system>",
										timestamp.time, timestamp.millitm
										);

								}

								if(pchStatus){try { free(pchStatus); } catch(...){} pchStatus=NULL;}

								ulDataLen = 0;
								if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
								{
									ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
								}

								if(g_psentinel->m_settings.m_bUseChecksumInXML)
								{
									msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
										ulDataLen,
										net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
										);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\">",
										ulDataLen
										);
								}



								msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>%s status message</msg>", 
									(g_psentinel->m_settings.m_pszName?g_psentinel->m_settings.m_pszName:"Sentinel"));

								msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


								// now, send and log.
	////////////////////////////////////////////// code copied from above.  if modified, must be 2 places.  sorry...
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "begin send response");  //(Dispatch message)
int starttime = clock();
									// send it
			_ftime(&timeperiodic);
								int b=CX_XML_BUFFER_CORTEXBEGIN;
								// to avoid small packets in separate sends, which has been shown to have performance problems,
								// we will assemble a large buffer and send it all at once.  If there is more data, we will send another large buffer, etc.


								// unsigned char pucTransmitBuffer[SENTINEL_XML_EVENT_MAXPAYLOAD];
								unsigned long ulTransBufPtr=0; //start at the beginning.
//									unsigned long ulTransBufLen=0; //start with nothing
								bool bTransErr = false;

								while((b<CX_XML_BUFFER_COUNT)&&(!bTransErr))
								{
									if(msg.m_pchResponse[b])
									{
										if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
										else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
										else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

										unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
										unsigned long ulBufPtr = 0;

										if(ulTransBufPtr+ulBufLen>SENTINEL_XML_EVENT_MAXPAYLOAD)
										{
											ulBufPtr = SENTINEL_XML_EVENT_MAXPAYLOAD - ulTransBufPtr;
											memcpy(pucTransmitBuffer+ulTransBufPtr, msg.m_pchResponse[b], ulBufPtr);
											ulTransBufPtr = SENTINEL_XML_EVENT_MAXPAYLOAD;
										}
										else
										{
											memcpy(pucTransmitBuffer+ulTransBufPtr, msg.m_pchResponse[b], ulBufLen);
											// should check return code but ah, memcpy is pretty reliable (I just jinxed it) <- attempt at reverse jinx  (<- d'oh jinxed it again)
											ulTransBufPtr+=ulBufLen;
										}

										bool bRemainder = false;

										while((ulTransBufPtr>=SENTINEL_XML_EVENT_MAXPAYLOAD)||(b>=CX_XML_BUFFER_COUNT-1)) // if buffer is full, or we are at the end.
										{
											pucTransmitBuffer[ulTransBufPtr]=0;
											// following lines, uncomment for debugging
//												net.m_ulFlags = (NET_FLAGS_LOGTIMING_RECV|NET_FLAGS_LOGTIMING_SEND);
//												net.InitializeMessaging(&(g_psentinel->m_msgr));
											int perf_send_start=clock();

/*
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "begin send response %d", b );  //(Dispatch message)
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "end send response %d: %d ms, %d:%d [%s]", b, clock()- perf_send_start, nReturn, ulBufLen,
	(b==10)?msg.m_pchResponse[b]:"");  //(Dispatch message)
*/
											
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "begin send response %d, %d bytes", b, ulTransBufPtr );  //(Dispatch message)
											int nReturn = net.SendLine((unsigned char*)pucTransmitBuffer, ulTransBufPtr, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "end send response %d: %d ms, %d:%d", b, clock()- perf_send_start, nReturn, ulTransBufPtr);  //(Dispatch message)
											
											
											bRemainder = false;
											ulTransBufPtr = 0;

											nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_psentinel)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending system message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, szSentinelSource, errorstring);  //(Dispatch message)
				g_psentinel->SendMsg(CX_SENDMSG_ERROR, szSentinelSource, errorstring);
			}

												bTransErr = true;
												break; // break out and discontinue sending.
											}
											else
											{
												if(ulBufPtr>0)
												{ 
													// have some leftover stuff to send
													memcpy(pucTransmitBuffer, msg.m_pchResponse[b]+ulBufPtr, ulBufLen-ulBufPtr);
													// should check return code but ah, memcpy is pretty reliable (I just jinxed it) <- attempt at reverse jinx  (<- d'oh jinxed it again)
													ulTransBufPtr=ulBufLen;
													ulBufPtr = 0;

													if(b>=CX_XML_BUFFER_COUNT-1) bRemainder = true; // at the end, we might have some left over, so have to go back and transmit the remainder.
												}

												msg.m_nTxStep = b;

												if((b>=CX_XML_BUFFER_COUNT-1)&&(!bRemainder)) break; // break out of the transmit loop if there is nothing left to transmit
											}
										} //while stuff to be transmitted
									}
									b++;
								}
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, szSentinelSource, "end send response %d ms", clock()- starttime );  //(Dispatch message)
							
//AfxMessageBox("oop");
								// log it

								//debug file write
								if((g_psentinel)&&(g_psentinel->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
								{
									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

	//								_ftime( &timestamp );  // use timestamp we just had..

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_psentinel->m_settings.m_pszName?g_psentinel->m_settings.m_pszName:"Sentinel"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lasttxfilename, filename)==0)
									{
										nTxDupes++;
									}
									else
									{
										nTxDupes=0;
										strcpy(lasttxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

									fp = fopen(filename, "wb");
									if(fp)
									{
										int b=CX_XML_BUFFER_CORTEXBEGIN;
										while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
										{
//											fwrite(filename, 1, strlen(filename),fp);
											if(msg.m_pchResponse[b])
											{
												if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

												fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
												fflush(fp);
											}
											b++;

										}
										fclose(fp);
										fp = NULL;
									}
								}


								b=CX_XML_BUFFER_TX;
								while(b<CX_XML_BUFFER_DATAEND)
								{
									if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
									{
										try { free(msg.m_pchResponse[b]); } catch(...){}
										msg.m_pchResponse[b] = NULL;
									}
									b++;
								}
	/////////////////////////////////////////////
							}

						}

						//and check timeout
						if(nTimed>0)
						{
							_ftime( &timestamp );
							if((timeactive.time + nTimed) < timestamp.time)
							{
								//kill it

								bCloseCommand = true;
							}
							
						}


						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} 

cleanup:
//		AfxMessageBox("cleanup");
//AfxMessageBox("010");

//		pDoc->Release();  // destructor calls this so don't do it here. 
		if(pSafety) pSafety->Release();

//AfxMessageBox("011");

// cleanup:  //was here but want to uninit com etc
//AfxMessageBox("012");

		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d closed.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);

//AfxMessageBox("013");
		if(g_psentinel)
		{
			g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, szSentinelSource, errorstring);  //(Dispatch message)
			g_psentinel->SendMsg(CX_SENDMSG_INFO, szSentinelSource, errorstring);
		}

//AfxMessageBox("014");

		if(pch!=NULL) { try{free(pch);} catch(...) {} } // must free the data buffer 

//AfxMessageBox("015");

		shutdown(pClient->m_socket, SD_BOTH);
//AfxMessageBox("016");
		closesocket(pClient->m_socket);
//AfxMessageBox("017");
		(*(pClient->m_pulConnections))--;
//AfxMessageBox("018");

	}
//		AfxMessageBox("end cleanup");
//AfxMessageBox("019");

	try
	{  
		CoUninitialize(); //XML
	}
	catch(...)
	{
		if(g_psentinel) g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, szSentinelSource, "Exception in CoUninitialize");  //(Dispatch message)
	}

//AfxMessageBox("020");

	try
	{
		delete pClient; // was created with new in the thread that spawned this one.
	}	catch(...) {}

//AfxMessageBox("021");
	if (bSetGlobalKill)	//g_bKillThread = true;
		((CSentinelHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();


//		AfxMessageBox("end thread");
}


void SentinelControlModuleServiceThread(void* pvArgs)
{
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "***> entering"); //  Sleep(250);//(Dispatch message)
	EnterCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "***> entered"); //  Sleep(250);//(Dispatch message)
	g_psentinel->m_data.m_bControlModuleServiceThreadStarted=true;
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "***> leaving"); //  Sleep(250);//(Dispatch message)
	LeaveCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "***> left"); //  Sleep(250);//(Dispatch message)
	int nLastMod=-27;
	_timeb timestamp;

	g_psentinel->m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:debug", "Control module service thread started."); //(Dispatch message)


	CBufferUtil bu;
	CCortexMessage msg;
	char errorstring[DB_ERRORSTRING_LEN];
	int nCode=0;

	// initialize a nice big array
	EnterCriticalSection(&g_psentinel->m_data.m_critControlModuleService);

	g_psentinel->m_data.m_nNumChanges=0;
	g_psentinel->m_data.m_nNumChangesArray=0;
	g_psentinel->m_data.m_nChangesArrayIndex=0;
	g_psentinel->m_data.m_ppChanges = new CSentinelChange*[SENTINEL_CHANGELIST_ARRAYSIZE_INC];

	if(g_psentinel->m_data.m_ppChanges)
	{
		g_psentinel->m_data.m_nNumChangesArray = SENTINEL_CHANGELIST_ARRAYSIZE_INC;
	}

	
	LeaveCriticalSection(&g_psentinel->m_data.m_critControlModuleService);

	while((!g_bKillThread)&&(!g_psentinel->m_data.m_bKillControlModuleServiceThread))
	{
		_ftime(&timestamp);

		if(g_psentinel->m_data.m_nLastControllersMod!=nLastMod)
		{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SERVICE)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "checking control module mods diff %d != %d, array %d (num %d)", g_psentinel->m_data.m_nLastControllersMod, nLastMod, g_psentinel->m_data.m_ppControlObj, g_psentinel->m_data.m_nNumControlObjects); //  Sleep(250);//(Dispatch message)
			// check for new modules to initialize.
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "D entering extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
			EnterCriticalSection(&g_psentinel->m_data.m_critExternalModules);
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "D entered extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)

			if(g_psentinel->m_data.m_ppControlObj)
			{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SERVICE)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "checking control module init"); //  Sleep(250);//(Dispatch message)
				int i=0;
				while(i<g_psentinel->m_data.m_nNumControlObjects)
				{
if((g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SERVICE)&&(g_psentinel->m_data.m_ppControlObj[i]))	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "checking control module %d: %d %d %d 0x%08x", 
	i, 
	(g_psentinel->m_data.m_ppControlObj[i]),
	g_psentinel->m_data.m_ppControlObj[i]->m_hinstDLL,
	g_psentinel->m_data.m_ppControlObj[i]->m_lpfnDllCtrl,
	g_psentinel->m_data.m_ppControlObj[i]->m_ulFlags); //  Sleep(250);//(Dispatch message)
					if(
						  (g_psentinel->m_data.m_ppControlObj[i])
						&&(g_psentinel->m_data.m_ppControlObj[i]->m_hinstDLL)
						&&(g_psentinel->m_data.m_ppControlObj[i]->m_lpfnDllCtrl)
						&&(g_psentinel->m_data.m_ppControlObj[i]->m_ulFlags&SENTINEL_FLAG_INIT)
						&&(g_psentinel->m_data.m_ppControlObj[i]->m_ulFlags&SENTINEL_FLAG_ENABLED)  // only enabled ones
						)
					{
						//initialize channels and events
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SERVICE)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "initializing control module"); //  Sleep(250);//(Dispatch message)
						int ch = 0;
						EnterCriticalSection(&g_psentinel->m_data.m_critChannels);
						while(ch<g_psentinel->m_data.m_nNumChannelObjects)
						{
						if(
									(g_psentinel->m_data.m_ppChannelObj[ch])
								&&(g_psentinel->m_data.m_ppControlObj[i]->IsChannelAssociated(g_psentinel->m_data.m_ppChannelObj[ch]->m_nChannelID)==CX_SUCCESS)
								)
							{
								// have an associated channel
								// first send connection info
								//initialize connections
								int c=0;
								EnterCriticalSection(&g_psentinel->m_data.m_critConns);
								while(c<g_psentinel->m_data.m_nNumConnectionObjects)
								{
									// need to find out if this thing is associated with a new channel
									if((g_psentinel->m_data.m_ppConnObj[c])&&(g_psentinel->m_data.m_ppConnObj[c]->m_pszServerName)&&g_psentinel->m_data.m_ppChannelObj[ch]->m_pszServerName)
									{
										if(strcmp(g_psentinel->m_data.m_ppConnObj[c]->m_pszServerName, g_psentinel->m_data.m_ppChannelObj[ch]->m_pszServerName)==0)
										{
											//great!  this is the connection

											CSentinelConnectionObject* pObj = g_psentinel->m_data.m_ppConnObj[c];

											char tag[128];
											systemstatus				sys; 
											TConnectionStatus   status;

											sys.systemlistcount=-1;
											sys.syschanged=-1;
											sys.systemfrx=0x29;
											sys.systemdf = 1;
											
											status = netNoConnection;

											EnterCriticalSection(&pObj->m_critAPI);

											if(pObj->m_pAPIConn)
											{
												memcpy(&sys, &pObj->m_pAPIConn->m_SysData, sizeof(systemstatus));
												memcpy(&status, &pObj->m_pAPIConn->m_Status, sizeof(TConnectionStatus));
												LeaveCriticalSection(&pObj->m_critAPI);
												if((sys.systemfrx==0x29)
													&&(sys.systemdf))
												{
													strcpy(tag, "29.97");
												}
												else
												{
													sprintf(tag, "%02x",sys.systemfrx);
												}
											}
											else
											{
												LeaveCriticalSection(&pObj->m_critAPI);
												sprintf(tag, "N/A");
											}
											

											msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																					"<aconn>\
<config active=\"%d\">\
<server>%s</server>\
<client>%s</client>\
<port>N/A</port>\
<basis>%s</basis>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<server_time>%.03f</server_time>\
<server_status>%d</server_status>\
<server_lists>%d</server_lists>\
<server_changed>%d</server_changed>\
<server_last_update>%.03f</server_last_update>\
<message>%s</message>\
</status>\
</aconn>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pObj->m_pszServerName,
																					pObj->m_pszClientName,
																					tag,
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(pObj->m_dblLastServerTimeMS/1000.0),
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? -1 : (status&0xff)),
																					(sys.systemlistcount==0xff)?-1:(sys.systemlistcount&0xff),
																					(sys.syschanged&0xff),
																					pObj->m_dblUpdateTime,
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? (((pObj->m_ulStatus&SENTINEL_ICON_MASK) == SENTINEL_STATUS_ERROR)?"connection error":"not connected"):"")

																																										
																				);

											

								nCode = g_psentinel->m_data.m_ppControlObj[i]->m_lpfnDllCtrl((void**)(&(msg.m_pchResponse[CX_XML_BUFFER_DATA])), DLLCMD_ACONN);

								if(nCode<SENTINEL_SUCCESS)
								{
									/// do something, report?
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending connection info for %s to module %s: %s (%s)", nCode,
														pObj->m_pszServerName, g_psentinel->m_data.m_ppControlObj[i]->m_pszModule, g_psentinel->m_data.m_ppControlObj[i]->m_pszName,g_psentinel->m_data.m_ppControlObj[i]->m_pszDesc );
									g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:control_module_service", errorstring);    //(Dispatch message)
								}


											break; // just do it once, in case more channels on one connection.
										}
									}
									c++;
								}

								LeaveCriticalSection(&g_psentinel->m_data.m_critConns);


								//then send channel info

								CSentinelChannelObject* pObj = g_psentinel->m_data.m_ppChannelObj[ch];

								_ftime(&timestamp);

								tlistdata tlist;
								tlist.liststate = -1;
								tlist.listchanged = -1;
								tlist.listdisplay = -1;
								tlist.listsyschange = -1;
								tlist.listcount = -1;
								tlist.lookahead = -1;
								if(pObj->m_pcritAPI)  // otherwise, prob not conected
								{
									EnterCriticalSection(pObj->m_pcritAPI);

									if((pObj->m_ppAPIConn)&&(*(pObj->m_ppAPIConn)))
									{
										memcpy(&tlist, &((*(pObj->m_ppAPIConn))->m_ListData[pObj->m_nHarrisListID-1]), sizeof(tlistdata));
									}
									LeaveCriticalSection(pObj->m_pcritAPI);
								}

								msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
									"<alist>\
<config active=\"%d\" id=\"%d\">\
<server>%s</server>\
<list_number>%d</list_number>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<list_state>%d</list_state>\
<list_changed>%d</list_changed>\
<list_display>%d</list_display>\
<list_syschange>%d</list_syschange>\
<list_count>%d</list_count>\
<list_lookahead>%d</list_lookahead>\
<list_last_update>%.3f</list_last_update>\
</status>\
</alist>",
									((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
									pObj->m_nChannelID,
									pObj->m_pszServerName,
									pObj->m_nHarrisListID,
									pObj->m_pszDesc,
									timestamp.time, timestamp.millitm,
									tlist.liststate,
									tlist.listchanged,
									tlist.listdisplay,
									tlist.listsyschange,
									tlist.listcount,
									tlist.lookahead,
									pObj->m_dblUpdateTime
																														
								);

								nCode = g_psentinel->m_data.m_ppControlObj[i]->m_lpfnDllCtrl((void**)(&(msg.m_pchResponse[CX_XML_BUFFER_DATA])), DLLCMD_ALIST);
								if(nCode<SENTINEL_SUCCESS)
								{
									/// do something, report?
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending channel info for %s:%d to module %s: %s (%s)", nCode,
														pObj->m_pszServerName, pObj->m_nHarrisListID, g_psentinel->m_data.m_ppControlObj[i]->m_pszModule, g_psentinel->m_data.m_ppControlObj[i]->m_pszName,g_psentinel->m_data.m_ppControlObj[i]->m_pszDesc );
									g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:control_module_service", errorstring);    //(Dispatch message)
								}



								// then send event info

								int e=0;
								while((pObj->m_ppevents)&&(e<pObj->m_nNumEvents))
								{
									if(pObj->m_ppevents[e])
									{
										CSentinelEventObject* pEObj = pObj->m_ppevents[e];

										char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
										char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
										char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
										char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
										char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
										
										msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH,  
												"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<SOM>%d</SOM>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_ulControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulSOMMS,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CSentinelEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																									);
											

								nCode = g_psentinel->m_data.m_ppControlObj[i]->m_lpfnDllCtrl((void**)(&(msg.m_pchResponse[CX_XML_BUFFER_DATA])), DLLCMD_AITEM);
								if(nCode<SENTINEL_SUCCESS)
								{
									/// do something, report?
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending event info for %s to module %s: %s (%s)", nCode,
														(pchXMLclip?pchXMLclip:""), g_psentinel->m_data.m_ppControlObj[i]->m_pszModule, g_psentinel->m_data.m_ppControlObj[i]->m_pszName,g_psentinel->m_data.m_ppControlObj[i]->m_pszDesc );
									g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:control_module_service", errorstring);    //(Dispatch message)
								}

										if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
										if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
										if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
										if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
										if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

									}
									e++;
								}

								// send traffic info  - later
#pragma message(alert "add traffic info to this later")

							}
							ch++;
						}
						LeaveCriticalSection(&g_psentinel->m_data.m_critChannels);


						g_psentinel->m_data.m_ppControlObj[i]->m_ulFlags &= ~SENTINEL_FLAG_INIT;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SERVICE)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "removing control module init 0x%08x", g_psentinel->m_data.m_ppControlObj[i]->m_ulFlags); //  Sleep(250);//(Dispatch message)

					}
					i++;
				}
			}
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "D leaving extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
			LeaveCriticalSection(&g_psentinel->m_data.m_critExternalModules);
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "D left extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)

			nLastMod = g_psentinel->m_data.m_nLastControllersMod;
		}

		// do any requests.
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "***+ entering"); //  Sleep(250);//(Dispatch message)
		EnterCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "***+ entered"); //  Sleep(250);//(Dispatch message)
		if(g_psentinel->m_data.m_ppChanges)
		{
			if(g_psentinel->m_data.m_nNumChanges>0)
			{

				CSentinelChange* pChg = g_psentinel->m_data.m_ppChanges[g_psentinel->m_data.m_nChangesArrayIndex];
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SERVICE)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "changes are now %d in the service loop %d %d %d", 
	g_psentinel->m_data.m_nNumChanges, pChg, pChg?((int)(pChg->m_pObj)):-1, pChg?pChg->m_nChannelID:-1); //  Sleep(250);//(Dispatch message)
				int i=g_psentinel->m_data.m_nChangesArrayIndex;
				if((pChg)&&(pChg->m_pObj)&&((pChg->m_nChannelID>0)||(pChg->m_nType==DLLCMD_ACONN)))  // DLLCMD_ACONN doesnt have a channel association.
				{
					// do the magic here.
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "change is valid"); //  Sleep(250);//(Dispatch message)


					bool bBufferMade = false;
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "*E entering extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
					EnterCriticalSection(&g_psentinel->m_data.m_critExternalModules);
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "*E entered extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)

					if(g_psentinel->m_data.m_ppControlObj)
					{
						int i=0;
						while(i<g_psentinel->m_data.m_nNumControlObjects)
						{
/*
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "checking obj %d of %d 0x%08x, %d=>[%s] ?%d",
	i+1, g_psentinel->m_data.m_nNumControlObjects, g_psentinel->m_data.m_ppControlObj[i]->m_ulFlags, 
	pChg->m_nChannelID, g_psentinel->m_data.m_ppControlObj[i]->m_pszChannelIds, g_psentinel->m_data.m_ppControlObj[i]->IsChannelAssociated(pChg->m_nChannelID)); //  Sleep(250);//(Dispatch message)
*/
							if(
									(g_psentinel->m_data.m_ppControlObj[i])
								&&(g_psentinel->m_data.m_ppControlObj[i]->m_hinstDLL)
								&&(g_psentinel->m_data.m_ppControlObj[i]->m_lpfnDllCtrl)
								&&(g_psentinel->m_data.m_ppControlObj[i]->m_ulFlags&SENTINEL_FLAG_ENABLED)
								&&(!(g_psentinel->m_data.m_ppControlObj[i]->m_ulFlags&SENTINEL_FLAG_INIT)) // must be initialized first.
								&&((g_psentinel->m_data.m_ppControlObj[i]->IsChannelAssociated(pChg->m_nChannelID)==CX_SUCCESS)||(pChg->m_nType==DLLCMD_ACONN))// DLLCMD_ACONN doesnt have a channel association. have to search it here.
								)
							{

								switch(pChg->m_nType)
								{
									
								case DLLCMD_ACONN://	0x0100: //	Sent to the DLL when a connection to an automation system is established or disconnected. Disconnection is indicated by the aconn.status.server_status element.�
									{

										// have to check the channel association on connection.
										CSentinelConnectionObject* pObj = (CSentinelConnectionObject*)pChg->m_pObj;
										if(pObj)
										{

//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "checking plugin on conn for DLLCMD_ACONN"); //  Sleep(250);//(Dispatch message)

											if(g_psentinel->m_data.CheckPluginOnConn(g_psentinel->m_data.m_ppControlObj[i]->m_pszChannelIds, pChg->m_pszServerName)==SENTINEL_SUCCESS)
											{


//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "sending DLLCMD_ACONN (%d)", bBufferMade); //  Sleep(250);//(Dispatch message)

										if(!bBufferMade)
										{

											char tag[128];

											if(pChg->m_sys.systemfrx==-1)
											{
												sprintf(tag, "N/A");
											}
											else
											{
												if((pChg->m_sys.systemfrx==0x29)
													&&(pChg->m_sys.systemdf))
												{
													strcpy(tag, "29.97");
												}
												else
												{
													sprintf(tag, "%02x",pChg->m_sys.systemfrx);
												}
											}
												

											msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																				"<aconn>\
<config active=\"%d\">\
<server>%s</server>\
<client>%s</client>\
<port>N/A</port>\
<basis>%s</basis>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<server_time>%.03f</server_time>\
<server_status>%d</server_status>\
<server_lists>%d</server_lists>\
<server_changed>%d</server_changed>\
<server_last_update>%.03f</server_last_update>\
<message>%s</message>\
</status>\
</aconn>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pChg->m_pszServerName?pChg->m_pszServerName:"",
																					pChg->m_pszClientName?pChg->m_pszClientName:"",
																					tag,
																					pChg->m_pszDesc?pChg->m_pszDesc:"",
																					pChg->m_timestamp.time, pChg->m_timestamp.millitm,
																					(pObj->m_dblLastServerTimeMS/1000.0),
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? -1 : (pChg->m_status&0xff)),
																					(pChg->m_sys.systemlistcount==0xff)?-1:(pChg->m_sys.systemlistcount&0xff),
																					(pChg->m_sys.syschanged&0xff),
																					pObj->m_dblUpdateTime,
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? (((pObj->m_ulStatus&SENTINEL_ICON_MASK) == SENTINEL_STATUS_ERROR)?"connection error":"not connected"):"")

																																										
																				);
											bBufferMade = true;

										}

										nCode = g_psentinel->m_data.m_ppControlObj[i]->m_lpfnDllCtrl((void**)(&(msg.m_pchResponse[CX_XML_BUFFER_DATA])), DLLCMD_ACONN);
										if(nCode<SENTINEL_SUCCESS)
										{
											/// do something, report?
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending connection info for %s to module %s: %s (%s)", nCode,
																pChg->m_pszServerName, g_psentinel->m_data.m_ppControlObj[i]->m_pszModule, g_psentinel->m_data.m_ppControlObj[i]->m_pszName,g_psentinel->m_data.m_ppControlObj[i]->m_pszDesc );
											g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:control_module_service", errorstring);    //(Dispatch message)
										}
									



											}
										}


									} break;
									case DLLCMD_ALIST://	0x0101: //	Sent to the DLL when monitoring for a list on an active connection is begun or discontinued.  If the connection on which the list information is coming has terminated, then a separate DLLCMD_ALIST command is not sent, as the monitoring may still be active, and will resume when the connection is restored.  Discontinuation of list monitoring is indicated by the active attribute of the alist.config element.�
									{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SERVICE)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "sending DLLCMD_ALIST (%d)", bBufferMade); //  Sleep(250);//(Dispatch message)

										if(!bBufferMade)
										{
											CSentinelChannelObject* pObj = (CSentinelChannelObject*)pChg->m_pObj;

											msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
												"<alist>\
<config active=\"%d\" id=\"%d\">\
<server>%s</server>\
<list_number>%d</list_number>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<list_state>%d</list_state>\
<list_changed>%d</list_changed>\
<list_display>%d</list_display>\
<list_syschange>%d</list_syschange>\
<list_count>%d</list_count>\
<list_lookahead>%d</list_lookahead>\
<list_last_update>%.3f</list_last_update>\
</status>\
</alist>",
												((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
												pObj->m_nChannelID,
												pChg->m_pszServerName,
												pObj->m_nHarrisListID,
												pChg->m_pszDesc,
												pChg->m_timestamp.time, pChg->m_timestamp.millitm,
												pChg->m_list.liststate,
												pChg->m_list.listchanged,
												pChg->m_list.listdisplay,
												pChg->m_list.listsyschange,
												pChg->m_list.listcount,
												pChg->m_list.lookahead,
												pObj->m_dblUpdateTime
																																	
											);
											
											bBufferMade = true;

										}

										nCode = g_psentinel->m_data.m_ppControlObj[i]->m_lpfnDllCtrl((void**)(&(msg.m_pchResponse[CX_XML_BUFFER_DATA])), DLLCMD_ALIST);
										if(nCode<SENTINEL_SUCCESS)
										{
											/// do something, report?
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending channel info for %s:%d to module %s: %s (%s)", nCode,
																pChg->m_pszServerName, ((CSentinelChannelObject*)(pChg->m_pObj))->m_nHarrisListID, g_psentinel->m_data.m_ppControlObj[i]->m_pszModule, g_psentinel->m_data.m_ppControlObj[i]->m_pszName,g_psentinel->m_data.m_ppControlObj[i]->m_pszDesc );
											g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:control_module_service", errorstring);    //(Dispatch message)
										}



									} break;
									case DLLCMD_AITEM://	0x0102: //	Sent to the DLL when an event on a monitored list is new, has changed, or is removed.  In the case that the event is removed, the aitem.info.position element will contain a value of -1, and no other aitem.info elements are included.�
									{

//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SERVICE)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "* sending DLLCMD_AITEM (%d)", bBufferMade); //  Sleep(250);//(Dispatch message)

										if(!bBufferMade)
										{
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SERVICE)	
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "* buffer not made" ); // Sleep(50); //(Dispatch message)

											CSentinelEventObject* pObj = (CSentinelEventObject*)pChg->m_pObj;

											if(pObj->m_nPosition<0)  //deletion!
											{
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SERVICE)	
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "* deletion" ); // Sleep(50); //(Dispatch message)


												msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
													"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<position>%d</position>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pObj->m_uid,
																					pChg->m_pszServerName?pChg->m_pszServerName:"",
																					pChg->m_nHarrisListID,
																					pChg->m_nChannelID,
																					pObj->m_nPosition,
																					pObj->m_dblUpdateTime
																																										
																									);
											}
											else
											{
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SERVICE)	
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "* aitem" ); // Sleep(50); //(Dispatch message)

												msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
													"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<SOM>%d</SOM>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pObj->m_uid,
																					pChg->m_pszServerName?pChg->m_pszServerName:"",
																					pChg->m_nHarrisListID,
																					pChg->m_nChannelID,
																					pChg->m_pchXMLkey?pChg->m_pchXMLkey:"",
																					pChg->m_pchXMLrec?pChg->m_pchXMLrec:"",
																					pChg->m_pchXMLclip?pChg->m_pchXMLclip:"",
																					((pObj->m_event.m_ucSegment==0xff)?-1:pObj->m_event.m_ucSegment),
																					pChg->m_pchXMLtitle?pChg->m_pchXMLtitle:"",
																					pChg->m_pchXMLdata?pChg->m_pchXMLdata:"",
																					pObj->m_event.m_usType,
																					pObj->m_event.m_usStatus,
																					pObj->m_event.m_ulControl,
																					pObj->m_dblTime,
																					pObj->m_event.m_ulSOMMS,
																					pObj->m_event.m_ulDurationMS,
																					pObj->m_dblCalcTime,
																					pObj->m_nPosition,
																					pChg->m_nParentUID,
																					pObj->m_dblUpdateTime
																																										
																									);
											}
											bBufferMade = true;


										}

//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SERVICE)	
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "* sending into DLL" ); // Sleep(50); //(Dispatch message)
										try
										{
											nCode = g_psentinel->m_data.m_ppControlObj[i]->m_lpfnDllCtrl((void**)(&(msg.m_pchResponse[CX_XML_BUFFER_DATA])), DLLCMD_AITEM);
										}
										catch(...)
										{
											g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:process_check", "Exception sending buffer to DLL %s, code %d", g_psentinel->m_data.m_ppControlObj[i]->m_pszModule, nCode ); // Sleep(50); //(Dispatch message)
										}
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SERVICE)	
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "* DLL returned %d", nCode ); // Sleep(50); //(Dispatch message)
										if(nCode<SENTINEL_SUCCESS)
										{
											/// do something, report?
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending event info for %s to module %s: %s (%s)", nCode,
																pChg->m_pchXMLclip, g_psentinel->m_data.m_ppControlObj[i]->m_pszModule, g_psentinel->m_data.m_ppControlObj[i]->m_pszName,g_psentinel->m_data.m_ppControlObj[i]->m_pszDesc );
											g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:control_module_service", errorstring);    //(Dispatch message)
										}

									} break;
								}
									
							}
							i++;
						}
					}
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SERVICE)	
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "*E leaving extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
					LeaveCriticalSection(&g_psentinel->m_data.m_critExternalModules);
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_SERVICE)	
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "*E left extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)


					try{ delete pChg; } catch(...){}
					pChg=NULL;
					g_psentinel->m_data.m_nChangesArrayIndex++;
					g_psentinel->m_data.m_nNumChanges--;
					if(g_psentinel->m_data.m_nChangesArrayIndex>=g_psentinel->m_data.m_nNumChangesArray)
						g_psentinel->m_data.m_nChangesArrayIndex = 0; // ring buffer
				}
			}
			else
			{
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "***++ leaving"); //  Sleep(250);//(Dispatch message)
//				LeaveCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "***++ left"); //  Sleep(250);//(Dispatch message)
				Sleep(1);  // fast while when changes.
			}
		}
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "***+ leaving"); //  Sleep(250);//(Dispatch message)
		LeaveCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "***+ left"); //  Sleep(250);//(Dispatch message)
	
	}  // Big while, but want it to be fast if there are changes to process.
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "***- entering"); //  Sleep(250);//(Dispatch message)
	EnterCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "***- entered"); //  Sleep(250);//(Dispatch message)

	if(g_psentinel->m_data.m_ppChanges)
	{	
		int i=g_psentinel->m_data.m_nChangesArrayIndex;
		int j=0;
		while((j<g_psentinel->m_data.m_nNumChanges)&&(i<g_psentinel->m_data.m_nNumChangesArray))
		{
			if(g_psentinel->m_data.m_ppChanges[i]){ try{ delete g_psentinel->m_data.m_ppChanges[i]; } catch(...){}} // delete objects, must use new to allocate
			i++;
			j++;
		}
		try{ delete [] g_psentinel->m_data.m_ppChanges;  } catch(...){}// delete array of pointers to objects, must use new to allocate
	}

	g_psentinel->m_data.m_ppChanges = NULL;
	g_psentinel->m_data.m_nNumChanges=0;
	g_psentinel->m_data.m_nNumChangesArray=0;
	g_psentinel->m_data.m_nChangesArrayIndex=0;

	g_psentinel->m_data.m_bControlModuleServiceThreadStarted=false;
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "***- leaving"); //  Sleep(250);//(Dispatch message)
	LeaveCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "***- left"); //  Sleep(250);//(Dispatch message)
	g_psentinel->m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:debug", "Control module service thread ended."); //(Dispatch message)

}

void SentinelPublishChangeThread(void* pvArgs)
{
	if((pvArgs==NULL)||(!g_psentinel->m_data.m_bControlModuleServiceThreadStarted)||(!g_psentinel->m_settings.m_bUseControlModules)) return;
	CSentinelChange* pChg = (CSentinelChange*)pvArgs;
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "*** entering change, changes now %d", g_psentinel->m_data.m_nNumChanges); //  Sleep(250);//(Dispatch message)

	_ftime(&(pChg->m_timestamp));
	EnterCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "*** entered change, changes now %d", g_psentinel->m_data.m_nNumChanges); //  Sleep(250);//(Dispatch message)

	if(g_psentinel->m_data.m_nNumChanges >= g_psentinel->m_data.m_nNumChangesArray) // have to grow the buffa
	{
		
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "*** growing the change buffer (%d)", g_psentinel->m_data.m_nNumChanges); //  Sleep(250);//(Dispatch message)

		CSentinelChange** ppChanges = new CSentinelChange*[(g_psentinel->m_data.m_nNumChangesArray+SENTINEL_CHANGELIST_ARRAYSIZE_INC)];

		if(ppChanges)
		{
			int j=0;
			int i=g_psentinel->m_data.m_nChangesArrayIndex;
			
			while(j<g_psentinel->m_data.m_nNumChanges)
			{
				ppChanges[j] = g_psentinel->m_data.m_ppChanges[i];

				if(i>=g_psentinel->m_data.m_nNumChangesArray) i=0; // go around the ring.

				j++;
			}

			g_psentinel->m_data.m_nChangesArrayIndex=0;
			g_psentinel->m_data.m_nNumChangesArray += SENTINEL_CHANGELIST_ARRAYSIZE_INC;

			if(g_psentinel->m_data.m_ppChanges){ try{ delete [] g_psentinel->m_data.m_ppChanges; } catch(...){} }

			g_psentinel->m_data.m_ppChanges = ppChanges;

		}
	}

	if(g_psentinel->m_data.m_nNumChanges < g_psentinel->m_data.m_nNumChangesArray) // if there is buffa room
	{
		int i = g_psentinel->m_data.m_nChangesArrayIndex + g_psentinel->m_data.m_nNumChanges;
		if(i>=g_psentinel->m_data.m_nNumChangesArray)
		{
			i-= g_psentinel->m_data.m_nNumChangesArray; // ring!
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "ring buffer index %d", g_psentinel->m_data.m_nChangesArrayIndex); //  Sleep(250);//(Dispatch message)
		}
		g_psentinel->m_data.m_ppChanges[i] = pChg;
		g_psentinel->m_data.m_nNumChanges++;
	}
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "added change, changes now %d", g_psentinel->m_data.m_nNumChanges); //  Sleep(250);//(Dispatch message)

//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "*** leaving change, changes now %d", g_psentinel->m_data.m_nNumChanges); //  Sleep(250);//(Dispatch message)

	LeaveCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "*** left change, changes now %d", g_psentinel->m_data.m_nNumChanges); //  Sleep(250);//(Dispatch message)
}


// [0]
// [1]
// [2]  index 
// [3]
// [4]
// array = 5


void SocketMonitorThread(void* pvArgs)
{
	CSentinelData* p = (CSentinelData*)pvArgs;
	if(p)
	{
		p->m_bSocketMonitorStarted = TRUE;
		while(p->m_bMonitorSocket)
		{
			timeval tv;
			tv.tv_sec = 0; 
			tv.tv_usec = 500;  // timeout value
			int nNumSockets;
			fd_set fds;

			FD_ZERO(&fds);  // Zero this out each time
			FD_SET(p->m_socketRemote, &fds);

			nNumSockets = select(0, &fds, NULL, NULL, &tv);
			if ( nNumSockets == SOCKET_ERROR ) 
			{
			//	p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
	EnterCriticalSection(&p->m_critRemoteSocket);
				p->m_net.CloseConnection(p->m_socketRemote);
				p->m_socketRemote = NULL;
	LeaveCriticalSection(&p->m_critRemoteSocket);
			}
			else
			if(
					(nNumSockets==0) // 0 = timed out, -1 = error
				||(!(FD_ISSET(p->m_socketRemote, &fds)))
				) 
			{ 
				// no data
			//	p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
			}
			else // there is recv data.
			{ // wait some delay.
				if((clock() > p->m_nRemoteClock+1000)&&(!p->m_bInRemoteCommand))
				{
				//	p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(TRUE);
						// receive the bytes.
		//recv the response.
//		unsigned char* puc2 = NULL;
//		unsigned long ulLen2=0
					unsigned char* puc = NULL;
					unsigned long ulLen=0;
					char szError[8096];
	EnterCriticalSection(&p->m_critRemoteSocket);
					int nReturn=p->m_net.GetLine(&puc, &ulLen, p->m_socketRemote, NET_RCV_ONCE, szError);
					if(nReturn<NET_SUCCESS)
					{
			//			CString attaturk;  attaturk.Format("Error %d sending command, return code %d\r\n%s: %d", WSAGetLastError(), nReturn,szError);
						p->m_net.CloseConnection(p->m_socketRemote);
						p->m_socketRemote = NULL;
			///			AfxMessageBox(attaturk);
					}
					else
					{
						if(ulLen==0) //disconnected
						{
							p->m_net.CloseConnection(p->m_socketRemote);
							p->m_socketRemote = NULL;
			//				AfxMessageBox("disconnected");
						}
						else
						{
							if(puc)
							{
			/*
			if(m_bLogTransactions)
			{
						FILE* logfp = fopen(LOGFILENAME, "ab");
						if(logfp)
						{
							_timeb timestamp;
							_ftime(&timestamp);
							char logtmbuf[48]; // need 33;
							tm* theTime = localtime( &timestamp.time	);
							strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
							fprintf(logfp, "%s%03d Recd: ", logtmbuf, timestamp.millitm);
							fwrite((char*)puc, sizeof(char), ulLen, logfp);
							fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
							fclose(logfp);
						}
			}
			*/
			//	GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText((char*)puc);

								p->m_nRemoteClock = clock();

								free(puc);
							}
						}
					}
	LeaveCriticalSection(&p->m_critRemoteSocket);

					
				}
			}
			if(p->m_bMonitorSocket) Sleep(100);
		}
		p->m_bSocketMonitorStarted = FALSE;
	}
}



