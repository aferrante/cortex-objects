// SentinelData.cpp: implementation of the CSentinelData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <process.h>
#include "Sentinel.h"
#include "SentinelHandler.h" 
#include "SentinelMain.h" 
#include "SentinelData.h"
//#include "../../Common/FILE/DirUtil.h"
#include <objsafe.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CSentinelMain* g_psentinel;
extern CSentinelApp theApp;
extern CADC g_adc; 	// the Harris ADC object
extern bool g_bKillThread;
//extern CDirUtil	g_dir;				// watch folder utilities.



extern void SentinelConnectionThread(void* pvArgs);
extern void SentinelWorkingSetMaintainThread(void* pvArgs);
//extern void SentinelWorkingSetAdjustThread(void* pvArgs);
extern void SentinelTrafficWatchThread(void* pvArgs);
extern void SentinelPublishChangeThread(void* pvArgs);
extern void SentinelControlModuleServiceThread(void* pvArgs);


void  __stdcall MessageDispatch( unsigned long ulFlags, char* pszDestination, char* pszCaller, char* pszMessage )
{
	if(g_psentinel)
	{
		g_psentinel->m_msgr.DM(ulFlags, pszDestination, pszCaller, pszMessage);
	}
}

int  __stdcall SendMsg( int nType, char* pszSender, char* pszMessage )
{
	if(g_psentinel)
	{
		return g_psentinel->SendMsg(nType, pszSender, pszMessage );
	}
	return SENTINEL_ERROR;
}

int   __stdcall SendAsRunMsg( int nType, char* pszSender, char* pszMessage, char* pszEventID, int nChannelID )
{
	if(g_psentinel)
	{
		return g_psentinel->SendAsRunMsg( nType, nChannelID, pszEventID, pszSender, pszMessage );
	}
	return SENTINEL_ERROR;
}

int  __stdcall SetExternalModuleStatus( int nModuleID, int nType, char* pszStatus )
{
	if(g_psentinel)
	{
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "A entering extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
		EnterCriticalSection(&g_psentinel->m_data.m_critExternalModules);
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "A entered extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
		if((g_psentinel->m_data.m_ppControlObj)&&(g_psentinel->m_data.m_nNumControlObjects))
		{
			int i=0;
			while(i<g_psentinel->m_data.m_nNumControlObjects)
			{
				if(g_psentinel->m_data.m_ppControlObj[i])
				{
					if(g_psentinel->m_data.m_ppControlObj[i]->m_nID == nModuleID)
					{
						g_psentinel->m_data.m_ppControlObj[i]->m_ulStatus = (unsigned long) nType;
						if(g_psentinel->m_data.m_ppControlObj[i]->m_pszStatus)  try{ free(g_psentinel->m_data.m_ppControlObj[i]->m_pszStatus); } catch(...){}
						g_psentinel->m_data.m_ppControlObj[i]->m_pszStatus = NULL;
						if((pszStatus)&&(strlen(pszStatus)))
						{
							g_psentinel->m_data.m_ppControlObj[i]->m_pszStatus = (char*) malloc(strlen(pszStatus)+1);

							if(g_psentinel->m_data.m_ppControlObj[i]->m_pszStatus)
							{
								strcpy(g_psentinel->m_data.m_ppControlObj[i]->m_pszStatus, pszStatus);
							}
						}

 						if((g_psentinel->m_data.m_pdb)&&(g_psentinel->m_data.m_pdbConn)&&(g_psentinel->m_data.m_ppControlObj[i]->m_pszStatus)
							&&(g_psentinel->m_settings.m_pszExternalModules)&&(strlen(g_psentinel->m_settings.m_pszExternalModules)))
						{
							char dberrorstring[DB_ERRORSTRING_LEN];
							char szSQL[DB_SQLSTRING_MAXLEN];
							
							if(strlen(g_psentinel->m_data.m_ppControlObj[i]->m_pszStatus)>SETSTATUS_MESSAGE_MAXLEN)
							{
								*(g_psentinel->m_data.m_ppControlObj[i]->m_pszStatus+SETSTATUS_MESSAGE_MAXLEN)=0;
							}
							char* pchMessage = g_psentinel->m_data.m_pdb->EncodeQuotes(g_psentinel->m_data.m_ppControlObj[i]->m_pszStatus);

							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message='%s', status=%d WHERE ID=%d",
								g_psentinel->m_settings.m_pszExternalModules,
								g_psentinel->m_data.m_ppControlObj[i]->m_pszStatus,
								g_psentinel->m_data.m_ppControlObj[i]->m_ulStatus,
								g_psentinel->m_data.m_ppControlObj[i]->m_nID
								);

							if(pchMessage) free(pchMessage);

							EnterCriticalSection(&g_psentinel->m_data.m_critSQL);

							if(g_psentinel->m_data.m_pdb->ExecuteSQL(g_psentinel->m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
							{
								// nothing
							}

							LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

							LeaveCriticalSection(&g_psentinel->m_data.m_critExternalModules);

							return SENTINEL_SUCCESS;
						}
						break;
					}
				}
				i++;
			}
		}
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "A leaving extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
		LeaveCriticalSection(&g_psentinel->m_data.m_critExternalModules);
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "A left extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
	}
	return SENTINEL_ERROR;
}



//////////////////////////////////////////////////////////////////////
// CSentinelChange Construction/Destruction
//////////////////////////////////////////////////////////////////////
CSentinelChange::CSentinelChange()
{
	m_pObj=NULL;
	m_nType = -1;
	m_nChannelID=-1;
	m_nParentUID=-1;
	m_nHarrisListID=-1;

	m_pchXMLkey=NULL;
	m_pchXMLrec=NULL;
	m_pchXMLclip=NULL;
	m_pchXMLtitle=NULL;
	m_pchXMLdata=NULL;

	m_pszServerName=NULL;
	m_pszClientName=NULL;
	m_pszDesc=NULL;

	m_list.liststate = -1;
	m_list.listchanged = -1;
	m_list.listdisplay = -1;
	m_list.listsyschange = -1;
	m_list.listcount = -1;
	m_list.lookahead = -1;

	// NTSC default
	m_sys.systemlistcount=-1;
	m_sys.syschanged=-1;
	m_sys.systemfrx=0x29;
	m_sys.systemdf = 1;
	
	m_status = netNoConnection;

}

CSentinelChange::~CSentinelChange()
{
	if(m_pObj)
	{
		try{ delete m_pObj;} catch(...){}
	}

	if(m_pchXMLkey) { try{ free(m_pchXMLkey);} catch(...){}}
	if(m_pchXMLrec) { try{ free(m_pchXMLrec);} catch(...){}}
	if(m_pchXMLclip) { try{ free(m_pchXMLclip);} catch(...){}}
	if(m_pchXMLtitle) { try{ free(m_pchXMLtitle);} catch(...){}}
	if(m_pchXMLdata) { try{ free(m_pchXMLdata);} catch(...){}}
	if(m_pszServerName) { try{ free(m_pszServerName);} catch(...){}}
	if(m_pszClientName) { try{ free(m_pszClientName);} catch(...){}}
	if(m_pszDesc) { try{ free(m_pszDesc);} catch(...){}}
}


//////////////////////////////////////////////////////////////////////
// CSentinelUtil Construction/Destruction
//////////////////////////////////////////////////////////////////////
CSentinelUtil::CSentinelUtil()
{
}

CSentinelUtil::~CSentinelUtil()
{
}

//////////////////////////////////////////////////////////////////////
// utility
//////////////////////////////////////////////////////////////////////

unsigned long CSentinelUtil::ConvertHMSFToMilliseconds(unsigned char ucHours, unsigned char ucMinutes, unsigned char ucSeconds, unsigned char ucFrames, unsigned char ucFrameBasis, unsigned long ulFlags) // pass in the systemfrx from the system data
{
	if(ulFlags!=ADC_NORMAL)
	{
/*
		char temp[32];
		sprintf(temp, "%02x", ucHours);		ucHours = (unsigned char)(atoi(temp)); 
		sprintf(temp, "%02x", ucMinutes);	ucMinutes = (unsigned char)(atoi(temp)); 
		sprintf(temp, "%02x", ucSeconds);	ucSeconds = (unsigned char)(atoi(temp)); 
		sprintf(temp, "%02x", ucFrames);	ucFrames = (unsigned char)(atoi(temp));
*/		
		unsigned char ucTemp = 	(ucHours&0x0f)+(((ucHours&0xf0)>>4)*10); ucHours = ucTemp;
		ucTemp = 	(ucMinutes&0x0f)+(((ucMinutes&0xf0)>>4)*10); ucMinutes = ucTemp;
		ucTemp = 	(ucSeconds&0x0f)+(((ucSeconds&0xf0)>>4)*10); ucSeconds = ucTemp;
		ucTemp = 	(ucFrames&0x0f)+(((ucFrames&0xf0)>>4)*10); ucFrames = ucTemp;
	}
	if((ucHours>23)||(ucHours<0)) return TIME_NOT_DEFINED;
	if((ucMinutes>59)||(ucMinutes<0)) return TIME_NOT_DEFINED;
	if((ucSeconds>59)||(ucSeconds<0)) return TIME_NOT_DEFINED;
	if(ucFrameBasis == 0x24) //PAL (Harris uses hex encoded value)
	{
		if((ucFrames>24)||(ucFrames<0)) return TIME_NOT_DEFINED;
		return (ucHours*3600000+ucMinutes*60000+ucSeconds*1000+(ucFrames*1000)/25); //parens for rounding error
	}
	else //default to NTSC
	{
		if((ucFrames>29)||(ucFrames<0)) return TIME_NOT_DEFINED;
		return (ucHours*3600000+ucMinutes*60000+ucSeconds*1000+(ucFrames*1000)/30); //parens for rounding error
	}
}

void  CSentinelUtil::ConvertMillisecondsToHMSF(unsigned long ulMilliseconds, unsigned char* pucHours, unsigned char* pucMinutes, unsigned char* pucSeconds, unsigned char* pucFrames, unsigned char ucFrameBasis, unsigned long ulFlags) // pass in the systemfrx from the system data
{	
//	CString foo; foo.Format("%d",ulMilliseconds );
//	AfxMessageBox(foo);

	if(ulMilliseconds == TIME_NOT_DEFINED)
	{
		*pucHours		= 0xff;
		*pucMinutes = 0xff;	
		*pucSeconds = 0xff;
		*pucFrames  = 0xff;
		return;
	}

	*pucHours		= ((unsigned char)(ulMilliseconds/3600000L))&0xff;
	*pucMinutes = ((unsigned char)((ulMilliseconds/60000L)%60))&0xff;	
	*pucSeconds = ((unsigned char)(((ulMilliseconds/1000L)%60)%60))&0xff;

	// frame calc had an issue where you could end up with or .30 frames if the millisecond count
	// was in between 989 and 999 for ntsc
	if(ucFrameBasis == 0x24) //PAL (Harris uses hex encoded value)
	{
		*pucFrames	= ((unsigned char)(((ulMilliseconds%1000L)/40)))&0xff;  //(int)(1000.0/25.0) = 40
	}
	else //default to NTSC
	{
		// have to make it reversible.  So anything above 989 gets converted to 29.
		*pucFrames	= ((unsigned char)(min(29, ((ulMilliseconds%1000L)/33))))&0xff;  //(int)(1000.0/30.0) = 33
	}
	if(ulFlags!=ADC_NORMAL)
	{
		unsigned char ucTemp = *pucHours;
		*pucHours = ((ucTemp/10)*16) + (ucTemp%10);
		ucTemp = *pucMinutes;
		*pucMinutes = ((ucTemp/10)*16) + (ucTemp%10);
		ucTemp = *pucSeconds;
		*pucSeconds = ((ucTemp/10)*16) + (ucTemp%10);
		ucTemp = *pucFrames;
		*pucFrames = ((ucTemp/10)*16) + (ucTemp%10);
	}
}


char* CSentinelUtil::PreprocessDateSpecifiers(char* pszFormatString, long lDate)
{
	if(pszFormatString)
	{
		char pszOutput[MAX_PATH<<1];
		int nInLen=strlen(pszFormatString);
		int nOutLen=0;
		char* pch = pszFormatString;
		char* pchEnd = min((pch+(MAX_PATH<<1)),(pszFormatString+nInLen));
		while((*pch!=0)&&(pch<pchEnd))
		{
			if(*pch == '%')
			{
				// we are in a specifier.
				pch++;
				if((*pch!=0)&&(pch<pchEnd))
				{
					if(*pch == 'D')  // our special preprocess specifier
					{
						tm* theTime = gmtime( &lDate	);
						if(theTime)
						{
							char pszTemp[MAX_PATH];
							sprintf(pszTemp, "%d", theTime->tm_mday);
							char* pchPlus = pszTemp;
							while((*pchPlus != 0)&&(nOutLen<(MAX_PATH<<1)))
							{
								pszOutput[nOutLen]= *pchPlus;
								nOutLen++;
								pchPlus++;
							}
						}
						pch++;
					}
					else
					{
						// some other specifier, can be anything.
						pszOutput[nOutLen]= *(pch-1);
						nOutLen++;
						pszOutput[nOutLen]= *pch;
						nOutLen++;
						pch++;
					}
				}
				else
				{
					pszOutput[nOutLen]= *(pch-1);
					nOutLen++;
				}
				
			}
			else
			{
				pszOutput[nOutLen]= *pch;
				nOutLen++;
				pch++;
			}
		}

		pszOutput[nOutLen]=0;

		pch = (char*)malloc(strlen(pszOutput)+1);
		if(pch)
		{
			strcpy(pch,pszOutput);
		}
		return pch;
	}
	return NULL;
}

unsigned long CSentinelUtil::DateFromName(char* pszFilename)
{
	unsigned long ulDate = TIME_NOT_DEFINED;
	if(
		  (pszFilename)&&(strlen(pszFilename)>0)
		&&(g_psentinel->m_settings.m_pszTrafficDateMap)&&(strlen(g_psentinel->m_settings.m_pszTrafficDateMap)>0)
		&&(!g_bKillThread)
		)
	{
		// have to figure out what date from strft variables
		// like  "\\%B %d.lst"

		// OK have to add a few specifier stha twe will override:

		// %D same as %d but format change to remove leading zeros.
		// OK I think tha tis the only one we need right now.

		// a few ways to do this.
		//1) build every possibility of a date for an entire year (let that be the horizon) and then compare it
		//2) traverse the character array looking for % and then a strft param, then check the filename for the possibilities, continue traversing, etc.

		// #1 is lame but prob makes the most sense, less complicated and maybe ends up being the same number of compares.

		//ok first find today;
		_timeb timestamp;
		_ftime( &timestamp );


		// make it localtime because harris time is localtime.
		long ulToday = (long)((timestamp.time) - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)); // local time....is done further down below
		long ulLimit = ulToday+(86400*min(g_psentinel->m_settings.m_nTrafficFileLookahead, 365)); // hardcoded 1 year.
		long ulTemp = ulToday%86400;
		ulToday -= ((g_psentinel->m_settings.m_nTrafficFilesDoneDays*86400)+ulTemp);

		char* pchDateMapSource;
		char pszDateMapSource[MAX_PATH];
		char pszDateMap[MAX_PATH];
		char pszDateMapCaps[MAX_PATH];
		char pszFilenameCaps[MAX_PATH];

		strcpy(pszDateMapSource, g_psentinel->m_settings.m_pszTrafficDateMap);


		int nLen = strlen(pszFilename);
		if(g_psentinel->m_settings.m_bTrafficDateMapCaseInsensitive)
		{
			int chp=0;
			while((chp<=nLen)&&(!g_bKillThread)) // copies term zero
			{
				*(pszFilenameCaps+chp) = toupper(*(pszFilename+chp));
				chp++;
			}
		}
		else
		{
			strcpy(pszFilenameCaps, pszFilename);
		}


if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checking date map [%s] on %s", pszDateMapSource, pszFilenameCaps);//  Sleep(100);

		while((!g_bKillThread)&&(ulToday<ulLimit))
		{
			pchDateMapSource = PreprocessDateSpecifiers(pszDateMapSource, ulToday);
			if(pchDateMapSource)
			{
				strftime( pszDateMap, MAX_PATH, pchDateMapSource, gmtime( &ulToday	) );
				try{ free(pchDateMapSource);} catch(...){}
			}
			else
			{
				strftime( pszDateMap, MAX_PATH, pszDateMapSource, gmtime( &ulToday	) );
			}

			if(g_psentinel->m_settings.m_bTrafficDateMapCaseInsensitive)
			{
				int chp=0;
				int nLen2 = strlen(pszDateMap);
				while((chp<=nLen2)&&(!g_bKillThread)) // copies term zero
				{
					*(pszDateMapCaps+chp) = toupper(*(pszDateMap+chp));
					chp++;
				}
			}
			else
			{
				strcpy(pszDateMapCaps, pszDateMap);
			}

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "checking date [%s] (%d) in %s", pszDateMapCaps,ulToday, pszFilenameCaps);//  Sleep(100);

			if(strlen(pszDateMapCaps)>0)
			{
				if(strstr(pszFilenameCaps, pszDateMapCaps)!=NULL)
				{
					ulDate = ulToday;
					break;
				}
			}

			ulToday += 86400; // one days worth of seconds
		}
	}

	return ulDate;
}

int CSentinelUtil::ChannelFromName(char* pszFilename, char** ppszServer, int* pnList, void** ppChannel)
{
	int nChannel=-1;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "ChannelFromName", "checking channel with [%s]", pszFilename?pszFilename:"(null)");//  Sleep(100);
	if(
		  (pszFilename)&&(strlen(pszFilename)>0)
		&&(g_psentinel)&&(g_psentinel->m_data.m_nNumChannelObjects)&&(g_psentinel->m_data.m_ppChannelObj)
		&&(g_psentinel->m_settings.m_pszTrafficChannelMap)&&(strlen(g_psentinel->m_settings.m_pszTrafficChannelMap)>0)
		&&(!g_bKillThread)
		)
	{
		int c=0;
		char pszChannelMapSource[MAX_PATH];
		char pszChannelMap[MAX_PATH];
		strcpy(pszChannelMapSource, g_psentinel->m_settings.m_pszTrafficChannelMap);
		// have to go thru all the channels.
		EnterCriticalSection(&g_psentinel->m_data.m_critChannels);
		while((!g_bKillThread)&&(c<g_psentinel->m_data.m_nNumChannelObjects))
		{
				// assemble the test string.
			char pszTemp[MAX_PATH];
			int nLen = strlen(pszChannelMapSource);

//			pszChannelMap[0] = 0;
//			pszTemp[0] = 0;
			memset(pszChannelMap, 0, MAX_PATH);
			memset(pszTemp, 0, MAX_PATH);


			int i=0; 
			int j=0;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "ChannelFromName", "checking channel map source [%s]", pszChannelMapSource);//  Sleep(100);
			while ((j<nLen)&&(i<MAX_PATH-1))
			{
				switch( *(pszChannelMapSource+j))
				{
				case '%':
					{
						j++;
						strcpy(pszTemp, pszChannelMap);
						switch( *(pszChannelMapSource+j))
						{
						case '%': // percent sign
							{
								_snprintf(pszChannelMap, MAX_PATH-1, "%s%%", pszTemp);
							} break;
						case 'D': // channel description
							{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "ChannelFromName", "checking channel map assembly [%s][%s]", pszTemp, ((g_psentinel->m_data.m_ppChannelObj[c]->m_pszDesc)&&(strlen(g_psentinel->m_data.m_ppChannelObj[c]->m_pszDesc)))?g_psentinel->m_data.m_ppChannelObj[c]->m_pszDesc:"");//  Sleep(100);
								_snprintf(pszChannelMap, MAX_PATH-1, "%s%s", pszTemp, ((g_psentinel->m_data.m_ppChannelObj[c]->m_pszDesc)&&(strlen(g_psentinel->m_data.m_ppChannelObj[c]->m_pszDesc)))?g_psentinel->m_data.m_ppChannelObj[c]->m_pszDesc:"");
							} break;
						case 'S': // server name
							{
								_snprintf(pszChannelMap, MAX_PATH-1, "%s%s", pszTemp, ((g_psentinel->m_data.m_ppChannelObj[c]->m_pszServerName)&&(strlen(g_psentinel->m_data.m_ppChannelObj[c]->m_pszServerName)))?g_psentinel->m_data.m_ppChannelObj[c]->m_pszServerName:"");
							} break;
						case 'L': // server list
							{
								_snprintf(pszChannelMap, MAX_PATH-1, "%s%d", pszTemp, g_psentinel->m_data.m_ppChannelObj[c]->m_nHarrisListID);
							} break;
						}
						i = strlen(pszChannelMap);
					} break;
				default:
					{ // nothing;
						pszChannelMap[i] = *(pszChannelMapSource+j);
						i++;
					} break;
				}
				j++;
			}
			pszChannelMap[i] = 0; //null term

			// now that we have assembled the test thingie, we can check the thing thing with the thing.
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "ChannelFromName", "checking channel [%s]", pszChannelMap);//  Sleep(100);

			if(strlen(pszChannelMap)>0)
			{
				if(strstr(pszFilename, pszChannelMap)!=NULL)
				{
					nChannel = g_psentinel->m_data.m_ppChannelObj[c]->m_nChannelID;
					if(ppszServer){ (*ppszServer) = g_psentinel->m_data.m_ppChannelObj[c]->m_pszServerName; }
					if(pnList){ (*pnList) = g_psentinel->m_data.m_ppChannelObj[c]->m_nHarrisListID; }
					if(ppChannel){ (*ppChannel) = g_psentinel->m_data.m_ppChannelObj[c]; }
					
					break;
				}
			}

			c++;
		}
		LeaveCriticalSection(&g_psentinel->m_data.m_critChannels);
	}
	return nChannel;
}


unsigned long CSentinelUtil::FileTimeToUnixTime(FILETIME ft, BOOL bLocal)
{
	__int64 uxtm = 0;

	uxtm = (((__int64)(ft.dwHighDateTime)) << 32) + ft.dwLowDateTime;
	uxtm -= 116444736000000000;
	uxtm /= 10000000;

	if(bLocal)
	{
		_timeb timestamp;
		_ftime( &timestamp );
		uxtm -= ((timestamp.timezone*60)-(timestamp.dstflag?3600:0));
	}
	return ((unsigned long)(uxtm));
}

//////////////////////////////////////////////////////////////////////
// CSentinelConnectionObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSentinelConnectionObject::CSentinelConnectionObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_pszClientName = NULL;
	m_ulStatus	= SENTINEL_STATUS_UNINIT;
	m_ulFlags = SENTINEL_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
	m_pAPIConn = NULL;
	m_bKillConnThread = true;
	m_bConnThreadStarted = false;
	m_dblLastServerTimeMS = 0.0;
	m_dblUpdateTime = 0.0;
	m_dblServerTimeIntervalMS = 33.0;
	m_bListStatusToDB = false;

	InitializeCriticalSection(&m_critAPI);
	
}

CSentinelConnectionObject::~CSentinelConnectionObject()
{
	m_bKillConnThread = true;
	while(	m_bConnThreadStarted ) Sleep(1);

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
	if(m_pszClientName) free(m_pszClientName); // must use malloc to allocate
	DeleteCriticalSection(&m_critAPI);
}



//////////////////////////////////////////////////////////////////////
// CSentinelEventObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSentinelEventObject::CSentinelEventObject()
{
	m_uid=-1;  // the unique Event ID
	m_nPrecedingUid=-1;
	m_nPosition=-1;  // the Harris list position
	m_nPreviousPosition=-1;  // the previous Harris list position

	m_nFlags=SENTINEL_EVENT_NEW;  // inserted in db, processed, etc.
	m_nDiffers=SENTINEL_EVENT_IDENTICAL;

	m_dblTime=-1.0;
	m_dblCalcTime=-1.0;
	m_nCalcDur=-1;
	m_nRefCount=0;

	m_pszEncodedKey=NULL;
	m_pszEncodedID=NULL;
	m_pszEncodedTitle=NULL;
	m_pszEncodedRecKey=NULL;
	m_pszEncodedData=NULL;

	m_dblUpdateTime = 0.0;
	// parent info.
	m_pParent=NULL;
}

CSentinelEventObject::~CSentinelEventObject()
{
	if(m_pszEncodedKey) free(m_pszEncodedKey);
	if(m_pszEncodedID) free(m_pszEncodedID);
	if(m_pszEncodedTitle) free(m_pszEncodedTitle);
	if(m_pszEncodedRecKey) free(m_pszEncodedRecKey);
	if(m_pszEncodedData) free(m_pszEncodedData);
}

int CSentinelEventObject::IsIdentical(CSentinelEventObject* pEvent)  // is exactly identical
{
	int nReturn = SENTINEL_EVENT_DIFFERS;
	if(m_nFlags&SENTINEL_EVENT_ACQUIRED)
	{
		nReturn = SENTINEL_EVENT_IDENTICAL;

		if(m_event.m_usType != pEvent->m_event.m_usType)	nReturn|=SENTINEL_EVENT_DIFFERS_TYPE;
		if(m_event.m_ucSegment != pEvent->m_event.m_ucSegment)	nReturn|=SENTINEL_EVENT_DIFFERS_SEG;

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CHANGES) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "SentinelChannelThread:debug", "--  event [%s][%s] on air time %.3f (off:%d ms), input %.3f (off:%d ms)",
	m_event.m_pszID,m_event.m_pszTitle,m_dblTime, m_event.m_ulOnAirTimeMS, pEvent->m_dblTime, pEvent->m_event.m_ulOnAirTimeMS);

//		if((m_event.m_ulOnAirTimeMS != pEvent->m_event.m_ulOnAirTimeMS)|| // ostensibly, if the offset changes, dblTime is going to change.  so just do one compare
		if(m_dblTime != pEvent->m_dblTime)
		{
			// let's check to see if this is a secondary, and the new time is because its primary was purged.
 // let's not do any of this.  the problem is that we lose primary on-air time, so let's not reassign primary on air time to nothing if we lose it.  do that in the assignment area
			if(
				  (pEvent->m_pParent == NULL) // no parent - primaries have themselves as parents so this serves as a secondary check as well)
//				&&(pEvent->m_event.m_usStatus&usHARRISDONE) // in the purge zone.  I guess this could happen not by purging but by someone deleting a primary off the top... so let's omit this
//				&&(m_pParent != NULL)  // this doesnt work because we migh be looking at a second go-around of an already primary-purged sec.
//				&&(pEvent->m_dblTime < (m_dblTime - 86400.0))  // let's make sure it's grossly off first, in the right direction  // this doesnt work because we know it is "today"
				)
			{
/*  don't bother with this.  jsut check if the offset is the same
				// let's not use the normal event tolerance setting for this check, the secondary timing should be much tighter.
//				double dblEventTimeTolerance = ((double)(g_psentinel->m_settings.m_nEventTimeToleranceMS))/1000.0;
				
				//let's check todays seconds only
				double dblDayOffset = m_dblTime - (86400.0*((double)((unsigned long)(m_dblTime/86400.0))));

				if(
					  (dblDayOffset < pEvent->m_dblTime - 0.001)
					||(dblDayOffset > pEvent->m_dblTime + 0.001)
					)
				{
					nReturn|=SENTINEL_EVENT_DIFFERS_OAT; // actually changed
				}
*/
				if(m_event.m_ulOnAirTimeMS != pEvent->m_event.m_ulOnAirTimeMS)
				{
					nReturn|=SENTINEL_EVENT_DIFFERS_OAT; // actually changed
				}
				// otherwise it hasn't differed, do not update it.
/*
// do this outside
				else
				{
					// add suppression for parent oat field update.
					pEvent->m_nFlags |= SENTINEL_EVENT_NULLPARENT;
				}
*/
			}
			else
				nReturn|=SENTINEL_EVENT_DIFFERS_OAT;  // as appears in the list
		}
		if(m_event.m_usOnAirJulianDate != pEvent->m_event.m_usOnAirJulianDate)	nReturn|=SENTINEL_EVENT_DIFFERS_DATE;  //offset from January 1, 1900
		if(m_event.m_ulDurationMS != pEvent->m_event.m_ulDurationMS)	nReturn|=SENTINEL_EVENT_DIFFERS_DUR;
		if((m_event.m_usStatus != pEvent->m_event.m_usStatus)||(m_event.m_usDevice != pEvent->m_event.m_usDevice))	nReturn|=SENTINEL_EVENT_DIFFERS_STATUS;
		if((m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT)) != (pEvent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT)) )	nReturn|=SENTINEL_EVENT_DIFFERS_STATUS;
		if(m_event.m_ulControl != pEvent->m_event.m_ulControl)	nReturn|=SENTINEL_EVENT_DIFFERS_CTRL;
		if(m_nPosition != pEvent->m_nPosition)	nReturn|=SENTINEL_EVENT_DIFFERS_POS;
		if(m_dblCalcTime != pEvent->m_dblCalcTime)	nReturn|=SENTINEL_EVENT_DIFFERS_CALCOAT;
		if(m_nCalcDur != pEvent->m_nCalcDur)	nReturn|=SENTINEL_EVENT_DIFFERS_CALCDUR;

		if((m_event.m_pszID)&&(pEvent->m_event.m_pszID))
		{
			if(strcmp(m_event.m_pszID, pEvent->m_event.m_pszID))	nReturn|=SENTINEL_EVENT_DIFFERS_ID;
		}
		else if((m_event.m_pszID)||(pEvent->m_event.m_pszID)) // one is null, the other not.
		{
			nReturn|=SENTINEL_EVENT_DIFFERS_ID;
		} // else they are both null, so thats identical.

		if((m_event.m_pszTitle)&&(pEvent->m_event.m_pszTitle))
		{
			if(strcmp(m_event.m_pszTitle, pEvent->m_event.m_pszTitle))	nReturn|=SENTINEL_EVENT_DIFFERS_TITLE;
		}
		else if((m_event.m_pszTitle)||(pEvent->m_event.m_pszTitle)) // one is null, the other not.
		{
			nReturn|=SENTINEL_EVENT_DIFFERS_TITLE;
		} // else they are both null, so thats identical.

		if((m_event.m_pszData)&&(pEvent->m_event.m_pszData))
		{
			if(strcmp(m_event.m_pszData, pEvent->m_event.m_pszData))	nReturn|=SENTINEL_EVENT_DIFFERS_DATA;
		}
		else if((m_event.m_pszData)||(pEvent->m_event.m_pszData)) // one is null, the other not.
		{
			nReturn|=SENTINEL_EVENT_DIFFERS_DATA;
		} // else they are both null, so thats identical.

		if((m_event.m_pszReconcileKey)&&(pEvent->m_event.m_pszReconcileKey))
		{
			if(strcmp(m_event.m_pszReconcileKey, pEvent->m_event.m_pszReconcileKey))	nReturn|=SENTINEL_EVENT_DIFFERS_RECKEY;
		}
		else if((m_event.m_pszReconcileKey)||(pEvent->m_event.m_pszReconcileKey)) // one is null, the other not.
		{
			nReturn|=SENTINEL_EVENT_DIFFERS_RECKEY;
		} // else they are both null, so thats identical.

/*
		CSentinelEventObject* pParent = (CSentinelEventObject*)m_pParent;
		CSentinelEventObject* pEventParent = (CSentinelEventObject*)event.m_pParent;

		if(
			  ((pParent==NULL)&&(pEventParent==NULL)) 
			||(pParent==pEventParent) 
			) return nReturn;

		if(
					||(
							(pParent!=NULL)
						&&(pEventParent!=NULL)
						&&(pParent->m_nPosition == pEventParent->m_nPosition)
						&&(pParent->m_uid == pEventParent->m_uid)
						&&(pParent->m_dblCalcTime == pEventParent->m_dblCalcTime)
						&&(pParent->m_nCalcDur == pEventParent->m_nCalcDur)
						)
					)
				{
					return SENTINEL_EVENT_IDENTICAL;
				}
			}
*/
	}
	return nReturn;
}

bool CSentinelEventObject::IsMatch(CSentinelEventObject* pEvent, double dblTimeTolerance, double dblTimeOffset, double* pdblTimeOffset, int nMatchMode, char* pszSource)      // matches on time, id, etc...
{
	if(pdblTimeOffset) *pdblTimeOffset = 0.0;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH)
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "00 SENTINEL EVENT %sACQUIRED", ((m_nFlags&SENTINEL_EVENT_ACQUIRED)?"":"NOT ") ); 
	if((m_nFlags&SENTINEL_EVENT_ACQUIRED)&&(pEvent))
	{
		if(
				(m_event.m_usType == pEvent->m_event.m_usType)
			&&(m_event.m_ucSegment == pEvent->m_event.m_ucSegment)
			&&(
					((m_event.m_pszID)&&(pEvent->m_event.m_pszID)&&(strcmp(m_event.m_pszID, pEvent->m_event.m_pszID)==0))
				||((m_event.m_pszID==NULL)&&(pEvent->m_event.m_pszID==NULL))
				)
			&&(
					((m_event.m_pszReconcileKey)&&(pEvent->m_event.m_pszReconcileKey)&&(strcmp(m_event.m_pszReconcileKey, pEvent->m_event.m_pszReconcileKey)==0))
				||((m_event.m_pszReconcileKey==NULL)&&(pEvent->m_event.m_pszReconcileKey==NULL))
				)
			&&(
					(!g_psentinel->m_settings.m_bUseTitleMatch)
				||(
						(g_psentinel->m_settings.m_bUseTitleMatch)
					&&(
							((m_event.m_pszTitle)&&(pEvent->m_event.m_pszTitle)&&(strcmp(m_event.m_pszTitle, pEvent->m_event.m_pszTitle)==0))
						||((m_event.m_pszTitle==NULL)&&(pEvent->m_event.m_pszTitle==NULL))
						)
					)
				)
			)
		{
			if(pEvent->m_event.m_usType&SECONDARYEVENT)
			{

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "chk sec in");
				CSentinelEventObject* pParent = (CSentinelEventObject*)m_pParent;
				CSentinelEventObject* pEventParent = (CSentinelEventObject*)pEvent->m_pParent;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "01 %s [%s] ( (pEventParent==?NULL) )", ((pEventParent==NULL)?"TRUE":"FALSE"), m_event.m_pszID ); 
				if(pEventParent == NULL)
				{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "02 %s [%s] (m_event.m_ulOnAirTimeMS %d ==? pEvent->m_event.m_ulOnAirTimeMS %d)", (( m_event.m_ulOnAirTimeMS == pEvent->m_event.m_ulOnAirTimeMS )?"TRUE":"FALSE"), m_event.m_pszID, m_event.m_ulOnAirTimeMS, pEvent->m_event.m_ulOnAirTimeMS ); 
					if(m_event.m_ulOnAirTimeMS == pEvent->m_event.m_ulOnAirTimeMS)
					{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH)g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "03 %s [%s] (pEvent->m_nPrecedingUid %d >? 0)", ((pEvent->m_nPrecedingUid > 0)?"TRUE":"FALSE"), m_event.m_pszID, pEvent->m_nPrecedingUid ); 
						if(pEvent->m_nPrecedingUid > 0)
						{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH)g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "04 %s [%s] (pEvent->m_nPrecedingUid %d ==? m_nPrecedingUid %d)", ((pEvent->m_nPrecedingUid == m_nPrecedingUid)?"TRUE":"FALSE"), m_event.m_pszID, pEvent->m_nPrecedingUid, m_nPrecedingUid ); 
							if(pEvent->m_nPrecedingUid == m_nPrecedingUid)
							{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "05 %s [%s] ( (pParent==NULL)&&(pEventParent==NULL) )", (( (pParent==NULL)&&(pEventParent==NULL) )?"TRUE":"FALSE"), m_event.m_pszID ); 
								if(pParent==NULL)
								{ 
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "MATCH A: [%s] compared array uid %d (pos %d) with input event (pos %d)", m_event.m_pszID, m_uid, m_nPosition, pEvent->m_nPosition  ); 
									return true;
								}
								else
								{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "06 %s [%s] (m_nPosition %d -pParent->m_nPosition %d (= %d) >=? pEvent->m_nPosition %d)", ((m_nPosition-pParent->m_nPosition >= pEvent->m_nPosition)?"TRUE":"FALSE"), m_event.m_pszID, m_nPosition, pParent->m_nPosition, m_nPosition-pParent->m_nPosition, pEvent->m_nPosition ); 
								// the parent of the new event may have been purged.
								// if so, the old position difference must be greater than the new position.
									if(m_nPosition-pParent->m_nPosition >= pEvent->m_nPosition)  // has to be >= instead of > because the parent may have been repositioned to pos=0, and event before re-eval also has pos=0.....
									{
										// let's re-assign the time, because we have the correct time since we still see the parent.
										pEvent->m_dblTime = m_dblTime;
										pEvent->m_dblCalcTime = m_dblCalcTime;

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "MATCH B: [%s] compared array uid %d (pos %d) with input event (pos %d)", m_event.m_pszID, m_uid, m_nPosition, pEvent->m_nPosition  ); 
										return true;
									}
								}
							}
						}
						else // it's the first item,
						{
							if(pParent==NULL)
							{ 
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "MATCH C: [%s] compared array uid %d (pos %d) with input event (pos %d)", m_event.m_pszID, m_uid, m_nPosition, pEvent->m_nPosition  ); 
								return true;
							}
							else
							{
								// the parent of the new event may have been purged.
								// if so, the old position difference must be greater than the new position.
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "07 %s [%s] (m_nPosition %d -pParent->m_nPosition %d (= %d) >=? pEvent->m_nPosition %d)", ((m_nPosition-pParent->m_nPosition >= pEvent->m_nPosition)?"TRUE":"FALSE"), m_event.m_pszID, m_nPosition, pParent->m_nPosition, m_nPosition-pParent->m_nPosition, pEvent->m_nPosition ); 
								if(m_nPosition-pParent->m_nPosition >= pEvent->m_nPosition)  // has to be >= instead of > because the parent may have been repositioned to pos=0, and event before re-eval also has pos=0.....
								{
									// let's re-assign the time, because we have the correct time since we still see the parent.
									pEvent->m_dblTime = m_dblTime;
									pEvent->m_dblCalcTime = m_dblCalcTime;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "MATCH D: [%s] compared array uid %d (pos %d) with input event (pos %d)", m_event.m_pszID, m_uid, m_nPosition, pEvent->m_nPosition  ); 
									return true;
								}
							}
						}
					}
				}
				else  //if(pEventParent == NULL) was not true
				{
			//		if(m_event.m_ulOnAirTimeMS == pEvent->m_event.m_ulOnAirTimeMS) // must match time offset
					{
	if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) 
		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"),
		"08: null?%d  puid=%d  input puid=%d", 
		((pParent!=NULL)?1:0),  ((pParent!=NULL)?pParent->m_uid:-1), pEventParent->m_uid ); 
						// can match parent stuff as below
						if(
								(pParent!=NULL)
								// following line to be changed to figure out what happens when you delete a secondary event, thereby reordering other secondaries on the same pri...

	//////////////////////////////////////////////
	// can not use the position match, in case someone inserts a thing in between secondary events.  dont want to lose the one below.							
	//						&&(pParent->m_nPreviousPosition-m_nPosition == pEventParent->m_nPosition-pEvent->m_nPosition)  // since we have position, we dont need to check previous uid.
	//////////////////////////////////////////////					
	// so instead we will match a reference count of identical items, inside the if below.					
							&&(pParent->m_uid == pEventParent->m_uid)
							)
						{
	if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "MATCH E: [%s] uid %d, (pos %d) compared array parent uid %d (pos %d) with input event (pos %d) parent uid %d (pos %d)", m_event.m_pszID, m_uid, m_nPosition, pParent->m_uid, pParent->m_nPosition, pEvent->m_nPosition, pEventParent->m_uid, pEventParent->m_nPosition  ); 
							if(nMatchMode==SENTINEL_MATCH_REF)
							{
								if(pEvent->m_nRefCount == m_nRefCount) // just have to make sure this is set properly each time.
								{
	if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "MATCH E-ref: [%s] uid %d, (ref %d) compared input event (ref %d)", m_event.m_pszID, m_uid, m_nRefCount, pEvent->m_nRefCount ); 

	// has to then ALSO match on time.  if it's got different time, then it is a differnte vent with a different refcount.
								// first just check offsets.
									if(g_psentinel->m_settings.m_nSecondaryEventMatchMode!=SENTINEL_MATCH_SECONDARY_TIME)
									{
										//SENTINEL_MATCH_SECONDARY_OFFSET
										if(m_event.m_ulOnAirTimeMS == pEvent->m_event.m_ulOnAirTimeMS)
										{
											return true;
										}
									}
									else
									{
										//SENTINEL_MATCH_SECONDARY_TIME
										if(
												(m_dblCalcTime == pEvent->m_dblCalcTime + dblTimeOffset)
											||
												(
													(m_dblCalcTime >= pEvent->m_dblCalcTime - dblTimeTolerance + dblTimeOffset)
												&&(m_dblCalcTime <= pEvent->m_dblCalcTime + dblTimeTolerance + dblTimeOffset)
												)
											)
										{
											if(pdblTimeOffset) 
											{
												*pdblTimeOffset = pEvent->m_dblCalcTime - m_dblCalcTime;
											}
						if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "MATCH E-ref-time FINAL: [%s] uid %d, (pos %d) compared on air time offset only", m_event.m_pszID, m_uid, m_nPosition  ); 
											return true;

										}
									}
								}
								else
								{
	if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "NO MATCH E-ref: [%s] uid %d, (ref %d) compared input event (ref %d)", m_event.m_pszID, m_uid, m_nRefCount, pEvent->m_nRefCount ); 
								}
							}
							else 
							if(nMatchMode==SENTINEL_MATCH_POS)
								// match on position
							{
								if(pParent->m_nPreviousPosition-m_nPosition == pEventParent->m_nPosition-pEvent->m_nPosition)  // since we have position, we dont need to check previous uid.
								{

	if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "MATCH E-pos: [%s] uid %d, (pos %d) compared array parent uid %d (pos %d) with input event (pos %d) parent uid %d (pos %d)", m_event.m_pszID, m_uid, m_nPosition, pParent->m_uid, pParent->m_nPosition, pEvent->m_nPosition, pEventParent->m_uid, pEventParent->m_nPosition  ); 
									return true;
								}
								else
								{
	if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "No MATCH E-pos: [%s] uid %d, (pos %d) compared array parent uid %d (pos %d) with input event (pos %d) parent uid %d (pos %d)", m_event.m_pszID, m_uid, m_nPosition, pParent->m_uid, pParent->m_nPosition, pEvent->m_nPosition, pEventParent->m_uid, pEventParent->m_nPosition  ); 
								}
							} //else time..?
							else 
							if(nMatchMode==SENTINEL_MATCH_TIME)
								// match on time
							{
								if(g_psentinel->m_settings.m_nSecondaryEventMatchMode!=SENTINEL_MATCH_SECONDARY_TIME)
								{
									//SENTINEL_MATCH_SECONDARY_OFFSET
									// first just check offsets.
									if(m_event.m_ulOnAirTimeMS == pEvent->m_event.m_ulOnAirTimeMS)
									{
										return true;
									}
								}
								else
								{	
									//SENTINEL_MATCH_SECONDARY_TIME


									if(
											(m_dblCalcTime == pEvent->m_dblCalcTime + dblTimeOffset)
										||
											(
												(m_dblCalcTime >= pEvent->m_dblCalcTime - dblTimeTolerance + dblTimeOffset)
											&&(m_dblCalcTime <= pEvent->m_dblCalcTime + dblTimeTolerance + dblTimeOffset)
											)
										)
									{
										if(pdblTimeOffset) 
										{
											*pdblTimeOffset = pEvent->m_dblCalcTime - m_dblCalcTime;
										}
					if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "MATCH E-time: [%s] uid %d, (pos %d) compared on air time offset only", m_event.m_pszID, m_uid, m_nPosition  ); 
										return true;
									}
								}
							}
	/*
	// since the UIDs match, this is the right secondary.  
	// no need to match up the primary time, this has already been identified on parent to parent primary matching.
							if(
									(pParent->m_dblCalcTime >= pEventParent->m_dblCalcTime - dblTimeTolerance + dblTimeOffset)
								&&(pParent->m_dblCalcTime <= pEventParent->m_dblCalcTime + dblTimeTolerance + dblTimeOffset)
								)
							{
								return true;
								}
	*/
						}	
					}
				}
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "NO MATCH for input event [%s] (pos %d) parent uid %d (pos %d)", pEvent->m_event.m_pszID, pEvent->m_nPosition, pEventParent?pEventParent->m_uid:-1, pEventParent?pEventParent->m_nPosition:-1  ); 
			}
			else //primary
			{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "chk pri in");
				if(pEvent->m_nPrecedingUid == m_nPrecedingUid) // works if -1 too
				{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "MATCHING on preceding uid %d",pEvent->m_nPrecedingUid  ); 
					if(pdblTimeOffset) 
					{
						*pdblTimeOffset = pEvent->m_dblCalcTime - m_dblCalcTime;
					}
					return true;
				}
				else // non sequential compared with before.
				{// there are possible scenarios  
					//1) if the time matches, its the event.
					if(
							(m_dblCalcTime == pEvent->m_dblCalcTime + dblTimeOffset)
						||
							(
								(m_dblCalcTime >= pEvent->m_dblCalcTime - dblTimeTolerance + dblTimeOffset)
							&&(m_dblCalcTime <= pEvent->m_dblCalcTime + dblTimeTolerance + dblTimeOffset)
							)
						)
					{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "MATCHING on time %.3f = %.3f+%.3f",m_dblCalcTime ,pEvent->m_dblCalcTime, dblTimeOffset ); 
						if(pdblTimeOffset) 
						{
							*pdblTimeOffset = pEvent->m_dblCalcTime - m_dblCalcTime;
						}
						return true;  // no need to compare parents, it is primary, it IS the parent
					}
/*
					else
					//2) if the time does not match, it's either not the event, or a block of events has been inserted or deleted.
					if
					{
						// if a block is deleted, the new time will be less.
						// if a block is added, the new time 
					}
*/
				}
			}
		}
		else
		{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) 
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "NO MATCH for input event on main criteria"); 
			return false; // return here just to escape the logging below
		}
/*

// following works for primaries.		
		if(
				(m_dblCalcTime >= pEvent->m_dblCalcTime - dblTimeTolerance)
			&&(m_dblCalcTime <= pEvent->m_dblCalcTime + dblTimeTolerance)
			&&(m_event.m_ucSegment == pEvent->m_event.m_ucSegment)
			&&(
				  ((m_event.m_pszID)&&(pEvent->m_event.m_pszID)&&(strcmp(m_event.m_pszID, pEvent->m_event.m_pszID)==0))
				||((m_event.m_pszID==NULL)&&(pEvent->m_event.m_pszID==NULL))
				)
			&&(
				  ((m_event.m_pszReconcileKey)&&(pEvent->m_event.m_pszReconcileKey)&&(strcmp(m_event.m_pszReconcileKey, pEvent->m_event.m_pszReconcileKey)==0))
				||((m_event.m_pszReconcileKey==NULL)&&(pEvent->m_event.m_pszReconcileKey==NULL))
				)
			)	
		{
			if(!(pEvent->m_event.m_usType&SECONDARYEVENT)) return true;  // no need to compare parents, it is primary, it IS the parent



 need to figure out a way to uniquely match secondary events that are identical, that are at the top with no primary. currently they appear the same


g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "%s [%s] (pParent==NULL)&&(pEventParent==NULL)", (( (pParent==NULL)&&(pEventParent==NULL) )?"TRUE":"FALSE"), m_event.m_pszID ); 
//Sleep(50);

g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "%s [%s] preposdiff %d (posdiff %d), uid %d, times %.3f ~ %.3f",
	((
		(pParent!=NULL)
	&&(pEventParent!=NULL)
	&&(pParent->m_nPreviousPosition-m_nPosition == pEventParent->m_nPosition-pEvent->m_nPosition)
	&&(pParent->m_uid == pEventParent->m_uid)
	&&(pParent->m_dblCalcTime >= pEventParent->m_dblCalcTime - dblTimeTolerance)
	&&(pParent->m_dblCalcTime <= pEventParent->m_dblCalcTime + dblTimeTolerance)
	)?"TRUE":"FALSE"),
	m_event.m_pszID, (pParent?pParent->m_nPreviousPosition-m_nPosition:-1), 
	(pParent?pParent->m_nPosition-m_nPosition:-1), 
	(pParent?pParent->m_uid:-1), 
	(pParent?pParent->m_dblCalcTime:-1), 
	(pEventParent?pEventParent->m_dblCalcTime:-1));
//Sleep(50);

g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "%s [%s] pParent not null, pEventParent NULL, posdiff %d > pos %d", 
((
	(pParent!=NULL)
&&(pEventParent==NULL) // newly without a parent becuase the parent was purged from the list.
&&(pParent->m_nPosition-m_nPosition > pEvent->m_nPosition) // so it is up in the list... (the time checked out already above)
//					&&(event.m_event.m_usStatus&(1<<eventdone))  // could check for done status, but.. could be a comment at the beginning...
)?"TRUE":"FALSE"),
m_event.m_pszID,	(pParent?pParent->m_nPosition-m_nPosition:-1), pEvent->m_nPosition);
//Sleep(50);

			if(
					( (pParent==NULL)&&(pEventParent==NULL) )
				||(
						(pParent!=NULL)
					&&(pEventParent!=NULL)
					&&(pParent->m_nPreviousPosition-m_nPosition == pEventParent->m_nPosition-pEvent->m_nPosition)
					&&(pParent->m_uid == pEventParent->m_uid)
					&&(pParent->m_dblCalcTime >= pEventParent->m_dblCalcTime - dblTimeTolerance)
					&&(pParent->m_dblCalcTime <= pEventParent->m_dblCalcTime + dblTimeTolerance)
					)
				||
					(
						(pParent!=NULL)
					&&(pEventParent==NULL) // newly without a parent becuase the parent was purged from the list.
					&&(pParent->m_nPosition-m_nPosition > pEvent->m_nPosition) // so it is up in the list... (the time checked out already above)
//					&&(event.m_event.m_usStatus&(1<<eventdone))  // could check for done status, but.. could be a comment at the beginning...
					)
				)
			{
				return true;
			}
		}
		*/
	}
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_MATCH) 
g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, (pszSource?pszSource:"Match event"), "NO MATCH for input event"); 

	return false;
}

bool CSentinelEventObject::IsNoDeviceType() 
{
	if(
		  ((m_event.m_usType&SECCOMMENT) == SECCOMMENT) // comment types
		||(m_event.m_usType == SECKEYEVENT)
		||(m_event.m_usType == SECTRANSKEY)
		||(m_event.m_usType == BREAKEVENT)
		||(m_event.m_usType == BREAKSYNC)
		) return true;

	return false;
}


//////////////////////////////////////////////////////////////////////
// CSentinelChannelObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSentinelTrafficObject::CSentinelTrafficObject()
{
	CAList m_List; // the ADC events in a list.
	m_pszFileName=NULL;
	m_ppevents=NULL;
	m_nNumEvents=0;
	m_ulDate=0; //unixtime
	m_pChannel=NULL;
	m_nID = -1;
}

CSentinelTrafficObject::~CSentinelTrafficObject()
{
	int i=0;
	if(m_ppevents)
	{
		while(i<m_nNumEvents)
		{
			if(m_ppevents[i]) delete m_ppevents[i];
			i++;
		}
		delete [] m_ppevents;
	}

	m_ppevents = NULL;

	if(m_pszFileName) free(m_pszFileName); // must use malloc to allocate

}

/*
int CSentinelTrafficObject::UpdateEventSQL(int index, CDBUtil* pdb,	CDBconn* pdbConn, double dblServerTime)
{
	return SENTINEL_SUCCESS;
}
*/




//////////////////////////////////////////////////////////////////////
// CSentinelVirtualChannelObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSentinelVirtualChannelObject::CSentinelVirtualChannelObject()
{
	m_pszDesc		= NULL;

	m_ulStatus	= SENTINEL_STATUS_UNINIT;
	m_ulFlags = SENTINEL_FLAG_DISABLED;  // various states
	m_usType = SENTINEL_TYPE_LOCAL; 
	m_nID = -1; // unassigned
	m_nChannelID = -1; // unassigned
	m_nMapID = -1;  // the channel ID (remote or local, as determined by m_usType) that this virtual channel maps to.

}

CSentinelVirtualChannelObject::~CSentinelVirtualChannelObject()
{
	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
}





//////////////////////////////////////////////////////////////////////
// CSentinelChannelObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSentinelChannelObject::CSentinelChannelObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_ulStatus	= SENTINEL_STATUS_UNINIT;
	m_ulFlags = SENTINEL_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
	m_nChannelID = -1; // unassigned
	m_nHarrisListID = -1; // unassigned
//	m_pbKillConnThread = NULL;
	m_bKillChannelThread = true;
	m_bChannelThreadStarted = false;
	m_bChannelThreadGettingEvents = false;
	m_bChannelThreadPublishingTempEvents = false;
	m_bChannelThreadPublishingEvents = false;

	m_ppAPIConn = NULL;
	m_pcritAPI = NULL;

	m_dblUpdateTime = 0.0;

	m_ppevents = NULL;
	m_nNumEvents=0;

	m_pptraffic = NULL;
	m_nNumTraffic=0;

//	m_nLastUid=1;
	m_nLastFound=-1;
}

CSentinelChannelObject::~CSentinelChannelObject()
{
	m_bKillChannelThread = true;
	while(	m_bChannelThreadStarted ) Sleep(1);  // killing the thread will delete the harris event array.
	// the intermediate array needs to be deleted here

	int i=0;
	if(m_ppevents)
	{
		while(i<m_nNumEvents)
		{
			if(m_ppevents[i]) { try { delete m_ppevents[i];} catch(...){} }
			i++;
		}
		try{ delete [] m_ppevents;} catch(...){}
	}

	m_ppevents = NULL;

	i=0;
	if(m_pptraffic)
	{
		while(i<m_nNumTraffic)
		{
			if(m_pptraffic[i]) { try { delete m_pptraffic[i];} catch(...){} }
			i++;
		}
		try{ delete [] m_pptraffic;} catch(...){}
	}

	m_pptraffic = NULL;


	// do not delete traffic - let traffic watchfolder thread kill.

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
}

int CSentinelChannelObject::SetAllUnprocessed()
{
	if((m_ppevents)&&(m_nNumEvents>0))
	{
		int i=0;
		while(i<m_nNumEvents)
		{
			if(m_ppevents[i]) m_ppevents[i]->m_nFlags &= ~(SENTINEL_EVENT_PROCESSED|SENTINEL_EVENT_MATCHED);
			i++;
		}
	}
	return ADC_SUCCESS;
}

int CSentinelChannelObject::ShiftPositions(int nIndex, int nDelta)
{
	if((m_ppevents)&&(m_nNumEvents>0)&&(nIndex<m_nNumEvents)&&(nDelta>0)) // shift up only
	{
		int i=nIndex;
		while(i<m_nNumEvents)
		{
			if(m_ppevents[i]) m_ppevents[i]->m_nPosition -= nDelta;
			i++;
		}
	}
	return ADC_SUCCESS;
}

int CSentinelChannelObject::AssembleKey(int index, char* pszFormat, BYTE bcdFrameRate)
{
	if((index>=0)&&(index<m_nNumEvents)&&(m_ppevents)&&(m_ppevents[index])&&(pszFormat)&&(strlen(pszFormat)>0))
	{
		// assemble the key and encode quotes right here, so we only have to do it once.
		char pszReturn[SENTINEL_EVENT_MAXKEYLEN];
		char pszTemp[SENTINEL_EVENT_MAXKEYLEN];
		CBufferUtil bu;
		int nLen = strlen(pszFormat);

		memset(pszReturn, 0, SENTINEL_EVENT_MAXKEYLEN);
		memset(pszTemp, 0, SENTINEL_EVENT_MAXKEYLEN);
//		pszReturn[0] = 0; // this didnt work! the  sprints aren't taking the term 0 into account somehow in the loop below, and old stuff gets added on subsequent calls!
//		pszTemp[0] = 0;

		CSentinelEventObject*	pEvent = m_ppevents[index];
		CSentinelEventObject*	pParent = (CSentinelEventObject*) m_ppevents[index]->m_pParent;


		int i=0; int j=0;
		while ((j<nLen)&&(i<SENTINEL_EVENT_MAXKEYLEN-1))
		{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "assembling key from %s, index %d = %c, currently %s", 	pszFormat, j, *(pszFormat+j), pszReturn); // Sleep(50); //(Dispatch message)

			switch( *(pszFormat+j))
			{
			case '%':
				{
					j++;
					strcpy(pszTemp, pszReturn);
					switch( *(pszFormat+j))
					{
					case '%': // percent sign
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%%", pszTemp);
						} break;
					case 'M': // server name
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((m_pszServerName)&&(strlen(m_pszServerName)))?m_pszServerName:"");
						} break;
					case 'L': // server list
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, m_nHarrisListID);
						} break;
					case 'l': // internal list id
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, m_nChannelID);
						} break;
					case 'a': // decimal type
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_event.m_usType);
						} break;
					case 'b': // hex type
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s0x%04x", pszTemp, pEvent->m_event.m_usType);
						} break;
					case 'i': // id
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pEvent->m_event.m_pszID)&&(strlen(pEvent->m_event.m_pszID)))?pEvent->m_event.m_pszID:"");
						} break;
					case 't': // title
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pEvent->m_event.m_pszTitle)&&(strlen(pEvent->m_event.m_pszTitle)))?pEvent->m_event.m_pszTitle:"");
						} break;
					case 'x': // data
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pEvent->m_event.m_pszData)&&(strlen(pEvent->m_event.m_pszData)))?pEvent->m_event.m_pszData:"");
						} break;
					case 'k': // rec key
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pEvent->m_event.m_pszReconcileKey)&&(strlen(pEvent->m_event.m_pszReconcileKey)))?pEvent->m_event.m_pszReconcileKey:"");
						} break;
					case 's': // segment
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_event.m_ucSegment==0xff?-1:pEvent->m_event.m_ucSegment);  //signed unsigned 2.1.1.21
						} break;
					case 'o': // on air time milliseconds offset in current day
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_event.m_ulOnAirTimeMS);
						} break;
					case 'j': // on air julian date
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_event.m_usOnAirJulianDate);
						} break;
					case 'u': // on air unixtime
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%.3f", pszTemp, pEvent->m_dblTime);
						} break;
					case 'h': // on air HMSF
						{
							int ms = (((int)(pEvent->m_dblTime))%86400)*1000 + (int)((pEvent->m_dblTime-(double)((int)pEvent->m_dblTime))*1000.0);
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%02d:%02d:%02d.%02d", pszTemp, 
								(ms/3600000),
								((ms/60000)%60),
								(((ms/1000)%60)%60),
								((ms%1000)/((bcdFrameRate==0x24)?40:33))
								);
						} break;
					case 'd': // duration millisecs
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_event.m_ulDurationMS);
						} break;
					case '>': // calc on air unixtime
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%.3f", pszTemp, pEvent->m_dblCalcTime);
						} break;
					case ':': // calc on air HMSF
						{
							int ms = (((int)(pEvent->m_dblCalcTime))%86400)*1000 + (int)((pEvent->m_dblCalcTime-(double)((int)pEvent->m_dblCalcTime))*1000.0);
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%02d:%02d:%02d.%02d", pszTemp, 
								(ms/3600000),
								((ms/60000)%60),
								(((ms/1000)%60)%60),
								((ms%1000)/((bcdFrameRate==0x24)?40:33))
								);
						} break;
					case '-': // calc duration millisecs
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_nCalcDur);
						} break;
					case '?': // status
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_event.m_usStatus);
						} break;
					case '!': // control (decimal code)
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_event.m_ulControl);
						} break;
					case 'p': // position
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_nPosition);
						} break;
					case 'A': // parent decimal type
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_event.m_usType:-1);
						} break;
					case 'B': // parent hex type
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s0x%04x", pszTemp, pParent?pParent->m_event.m_usType:0xffff);
						} break;
					case 'I': // parent id
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pParent)&&(pParent->m_event.m_pszID)&&(strlen(pParent->m_event.m_pszID)))?pParent->m_event.m_pszID:"");
						} break;
					case 'T': // parent title
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pParent)&&(pParent->m_event.m_pszTitle)&&(strlen(pParent->m_event.m_pszTitle)))?pParent->m_event.m_pszTitle:"");
						} break;
					case 'X': // parent data
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pParent)&&(pParent->m_event.m_pszData)&&(strlen(pParent->m_event.m_pszData)))?pParent->m_event.m_pszData:"");
						} break;
					case 'K': // parent rec key
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pParent)&&(pParent->m_event.m_pszReconcileKey)&&(strlen(pParent->m_event.m_pszReconcileKey)))?pParent->m_event.m_pszReconcileKey:"");
						} break;
					case 'S': // parent segment
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_event.m_ucSegment:-1);
						} break;
					case 'O': // parent on air time milliseconds offset in current day
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_event.m_ulOnAirTimeMS:-1);
						} break;
					case 'J': // parent on air julian date
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_event.m_usOnAirJulianDate:-1);
						} break;
					case 'U': // parent on air unixtime
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%.3f", pszTemp, pParent?pParent->m_dblTime:-1);
						} break;
					case 'H': // parent on air HMSF
						{
							if(pParent)
							{
								int ms = (((int)(pParent->m_dblTime))%86400)*1000 + (int)((pParent->m_dblTime-(double)((int)pParent->m_dblTime))*1000.0);
								_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%02d:%02d:%02d.%02d", pszTemp, 
									(ms/3600000),
									((ms/60000)%60),
									(((ms/1000)%60)%60),
									((ms%1000)/((bcdFrameRate==0x24)?40:33))
									);

							}
							else
							{
								_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s00:00:00.00:", pszTemp); 
							}
						} break;
					case 'D': // parent duration millisecs
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_event.m_ulDurationMS:-1);
						} break;
					case '}': // parent calc on air unixtime
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%.3f", pszTemp, pParent?pParent->m_dblCalcTime:-1);
						} break;
					case ';': // parent calc on air HMSF
						{
							if(pParent)
							{
								int ms = (((int)(pParent->m_dblCalcTime))%86400)*1000 + (int)((pParent->m_dblCalcTime-(double)((int)pParent->m_dblCalcTime))*1000.0);
								_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%02d:%02d:%02d:%02d:", pszTemp, 
									(ms/3600000),
									((ms/60000)%60),
									(((ms/1000)%60)%60),
									((ms%1000)/((bcdFrameRate==0x24)?40:33))
									);

							}
							else
							{
								_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s00:00:00.00:", pszTemp); 
							}
						} break;
					case '_': // parent calc duration millisecs
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_nCalcDur:-1);
						} break;
					case '~': // parent status
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_event.m_usStatus:-1);
						} break;
					case '+': // parent control (decimal code)
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_event.m_ulControl:-1);
						} break;
					case 'P': // parent position
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_nPosition:-1);

/*
	unsigned short m_usType;
	char* m_pszID;
	char* m_pszTitle;
	char* m_pszData;  //must encode zero.
	char* m_pszReconcileKey;
	unsigned char  m_ucSegment;
	unsigned long  m_ulOnAirTimeMS;  // as appears in the list
	unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900
	unsigned long  m_ulDurationMS;
	unsigned short m_usStatus;
	unsigned short m_ulControl;
*/							
						} break;
					} //switch( *(pszFormat+j))
					i = strlen(pszReturn);
				} break;
			default:
				{ // nothing;
					pszReturn[i] = *(pszFormat+j);
					i++;
				} break;
/* // dont encode here.  may have to encode incoming values.  so just encode aftuh
			case '\'':
				{
					pszReturn[i] = *(pszFormat+j);
					i++;
					pszReturn[i] = *(pszFormat+j);
					i++;

				} break;
*/
			}  // switch( *(pszFormat+j))
		  j++;
		} // while ((j<nLen)&&(i<SENTINEL_EVENT_MAXKEYLEN-1))
		pszReturn[i] = 0; //null term

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "assembled key %s", 	pszReturn); // Sleep(50); //(Dispatch message)

		
		if(m_ppevents[index]->m_pszEncodedKey) free(m_ppevents[index]->m_pszEncodedKey); 
		m_ppevents[index]->m_pszEncodedKey = NULL;
		if(strlen(pszReturn))
		{
			m_ppevents[index]->m_pszEncodedKey = m_db.EncodeQuotes(pszReturn);
		}
		// else strcpy(m_ppevents[index]->m_pszEncodedKey, ""); // can;t do this, it's NULL!

		return ADC_SUCCESS;
	} //	if((index>=0)&&(index<m_nNumEvents)&&(m_ppevents)&&(m_ppevents[index])&&(pszFormat)&&(strlen(pszFormat)>0))

	return ADC_ERROR;
}

int CSentinelTrafficObject::AssembleKey(int index, char* pszFormat, BYTE bcdFrameRate)
{
	if((index>=0)&&(index<m_nNumEvents)&&(m_ppevents)&&(m_ppevents[index])&&(pszFormat)&&(strlen(pszFormat)>0))
	{
		// assemble the key and encode quotes right here, so we only have to do it once.
		char pszReturn[SENTINEL_EVENT_MAXKEYLEN];
		char pszTemp[SENTINEL_EVENT_MAXKEYLEN];
		CBufferUtil bu;
		int nLen = strlen(pszFormat);

		memset(pszReturn, 0, SENTINEL_EVENT_MAXKEYLEN);
		memset(pszTemp, 0, SENTINEL_EVENT_MAXKEYLEN);
//		pszReturn[0] = 0; // this didnt work! the  sprints aren't taking the term 0 into account somehow in the loop below, and old stuff gets added on subsequent calls!
//		pszTemp[0] = 0;

		CSentinelEventObject*	pEvent = m_ppevents[index];
		CSentinelEventObject*	pParent = (CSentinelEventObject*) m_ppevents[index]->m_pParent;
		CSentinelChannelObject* pChannel = (CSentinelChannelObject*)m_pChannel;

		int i=0; int j=0;
		while ((j<nLen)&&(i<SENTINEL_EVENT_MAXKEYLEN-1))
		{
			switch( *(pszFormat+j))
			{
			case '%':
				{
					j++;
					strcpy(pszTemp,pszReturn);
					switch( *(pszFormat+j))
					{
					case '%': // percent sign
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%%", pszTemp);
						} break;
					case 'M': // server name
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pChannel)&&(pChannel->m_pszServerName)&&(strlen(pChannel->m_pszServerName)))?pChannel->m_pszServerName:"");
						} break;
					case 'L': // server list
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, (pChannel?pChannel->m_nHarrisListID:-1));
						} break;
					case 'l': // internal list id
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, (pChannel?pChannel->m_nChannelID:-1));
						} break;
					case 'a': // decimal type
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_event.m_usType);
						} break;
					case 'b': // hex type
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s0x%04x", pszTemp, pEvent->m_event.m_usType);
						} break;
					case 'i': // id
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pEvent->m_event.m_pszID)&&(strlen(pEvent->m_event.m_pszID)))?pEvent->m_event.m_pszID:"");
						} break;
					case 't': // title
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pEvent->m_event.m_pszTitle)&&(strlen(pEvent->m_event.m_pszTitle)))?pEvent->m_event.m_pszTitle:"");
						} break;
					case 'x': // data
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pEvent->m_event.m_pszData)&&(strlen(pEvent->m_event.m_pszData)))?pEvent->m_event.m_pszData:"");
						} break;
					case 'k': // rec key
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pEvent->m_event.m_pszReconcileKey)&&(strlen(pEvent->m_event.m_pszReconcileKey)))?pEvent->m_event.m_pszReconcileKey:"");
						} break;
					case 's': // segment
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_event.m_ucSegment);
						} break;
					case 'o': // on air time milliseconds offset in current day
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_event.m_ulOnAirTimeMS);
						} break;
					case 'j': // on air julian date
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_event.m_usOnAirJulianDate);
						} break;
					case 'u': // on air unixtime
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%.3f", pszTemp, pEvent->m_dblTime);
						} break;
					case 'h': // on air HMSF
						{
							int ms = (((int)(pEvent->m_dblTime))%86400)*1000 + (int)((pEvent->m_dblTime-(double)((int)pEvent->m_dblTime))*1000.0);
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%02d:%02d:%02d.%02d", pszTemp, 
								(ms/3600000),
								((ms/60000)%60),
								(((ms/1000)%60)%60),
								((ms%1000)/((bcdFrameRate==0x24)?40:33))
								);
						} break;
					case 'd': // duration millisecs
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_event.m_ulDurationMS);
						} break;
					case '>': // calc on air unixtime
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%.3f", pszTemp, pEvent->m_dblCalcTime);
						} break;
					case ':': // calc on air HMSF
						{
							int ms = (((int)(pEvent->m_dblCalcTime))%86400)*1000 + (int)((pEvent->m_dblCalcTime-(double)((int)pEvent->m_dblCalcTime))*1000.0);
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%02d:%02d:%02d.%02d", pszTemp, 
								(ms/3600000),
								((ms/60000)%60),
								(((ms/1000)%60)%60),
								((ms%1000)/((bcdFrameRate==0x24)?40:33))
								);
						} break;
					case '-': // calc duration millisecs
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_nCalcDur);
						} break;
					case '?': // status
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_event.m_usStatus);
						} break;
					case '!': // control (decimal code)
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_event.m_ulControl);
						} break;
					case 'p': // position
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pEvent->m_nPosition);
						} break;
					case 'A': // parent decimal type
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_event.m_usType:-1);
						} break;
					case 'B': // parent hex type
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s0x%04x", pszTemp, pParent?pParent->m_event.m_usType:0xffff);
						} break;
					case 'I': // parent id
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pParent)&&(pParent->m_event.m_pszID)&&(strlen(pParent->m_event.m_pszID)))?pParent->m_event.m_pszID:"");
						} break;
					case 'T': // parent title
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pParent)&&(pParent->m_event.m_pszTitle)&&(strlen(pParent->m_event.m_pszTitle)))?pParent->m_event.m_pszTitle:"");
						} break;
					case 'X': // parent data
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pParent)&&(pParent->m_event.m_pszData)&&(strlen(pParent->m_event.m_pszData)))?pParent->m_event.m_pszData:"");
						} break;
					case 'K': // parent rec key
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%s", pszTemp, ((pParent)&&(pParent->m_event.m_pszReconcileKey)&&(strlen(pParent->m_event.m_pszReconcileKey)))?pParent->m_event.m_pszReconcileKey:"");
						} break;
					case 'S': // parent segment
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_event.m_ucSegment:-1);
						} break;
					case 'O': // parent on air time milliseconds offset in current day
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_event.m_ulOnAirTimeMS:-1);
						} break;
					case 'J': // parent on air julian date
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_event.m_usOnAirJulianDate:-1);
						} break;
					case 'U': // parent on air unixtime
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%.3f", pszTemp, pParent?pParent->m_dblTime:-1);
						} break;
					case 'H': // parent on air HMSF
						{
							if(pParent)
							{
								int ms = (((int)(pParent->m_dblTime))%86400)*1000 + (int)((pParent->m_dblTime-(double)((int)pParent->m_dblTime))*1000.0);
								_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%02d:%02d:%02d.%02d", pszTemp, 
									(ms/3600000),
									((ms/60000)%60),
									(((ms/1000)%60)%60),
									((ms%1000)/((bcdFrameRate==0x24)?40:33))
									);

							}
							else
							{
								_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s00:00:00.00:", pszTemp); 
							}
						} break;
					case 'D': // parent duration millisecs
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_event.m_ulDurationMS:-1);
						} break;
					case '}': // parent calc on air unixtime
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%.3f", pszTemp, pParent?pParent->m_dblCalcTime:-1);
						} break;
					case ';': // parent calc on air HMSF
						{
							if(pParent)
							{
								int ms = (((int)(pParent->m_dblCalcTime))%86400)*1000 + (int)((pParent->m_dblCalcTime-(double)((int)pParent->m_dblCalcTime))*1000.0);
								_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%02d:%02d:%02d:%02d:", pszTemp, 
									(ms/3600000),
									((ms/60000)%60),
									(((ms/1000)%60)%60),
									((ms%1000)/((bcdFrameRate==0x24)?40:33))
									);

							}
							else
							{
								_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s00:00:00.00:", pszTemp); 
							}
						} break;
					case '_': // parent calc duration millisecs
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_nCalcDur:-1);
						} break;
					case '~': // parent status
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_event.m_usStatus:-1);
						} break;
					case '+': // parent control (decimal code)
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_event.m_ulControl:-1);
						} break;
					case 'P': // parent position
						{
							_snprintf(pszReturn, SENTINEL_EVENT_MAXKEYLEN-1, "%s%d", pszTemp, pParent?pParent->m_nPosition:-1);

/*
	unsigned short m_usType;
	char* m_pszID;
	char* m_pszTitle;
	char* m_pszData;  //must encode zero.
	char* m_pszReconcileKey;
	unsigned char  m_ucSegment;
	unsigned long  m_ulOnAirTimeMS;  // as appears in the list
	unsigned short m_usOnAirJulianDate;  //offset from January 1, 1900
	unsigned long  m_ulDurationMS;
	unsigned short m_usStatus;
	unsigned short m_ulControl;
*/							
						} break;
					}
					i = strlen(pszReturn);
				} break;
			default:
				{ // nothing;
					pszReturn[i] = *(pszFormat+j);
					i++;
				} break;
/* // dont encode here.  may have to encode incoming values.  so just encode aftuh
			case '\'':
				{
					pszReturn[i] = *(pszFormat+j);
					i++;
					pszReturn[i] = *(pszFormat+j);
					i++;

				} break;
*/
			}
		  j++;
		}
		pszReturn[i] = 0; //null term

		if(m_ppevents[index]->m_pszEncodedKey) free(m_ppevents[index]->m_pszEncodedKey); 
		m_ppevents[index]->m_pszEncodedKey = NULL;
		if(strlen(pszReturn))
		{
			m_ppevents[index]->m_pszEncodedKey = m_db.EncodeQuotes(pszReturn);
		}
		// else strcpy(m_ppevents[index]->m_pszEncodedKey, ""); // can;t do this, it's NULL!

		return ADC_SUCCESS;
	}
	return ADC_ERROR;
}



int CSentinelTrafficObject::FindTrafficUid(int uid)  //returns index
{
	int index=-1;
	if((m_ppevents)&&(m_nNumEvents>0))
	{
		int i=0;
		while(i<m_nNumEvents)
		{
			if((m_ppevents[i])&&(m_ppevents[i]->m_uid==uid))
			{
				return i;
			}
			else
			{
			 i++;
			}
		}
	}
	return index;
}

int CSentinelTrafficObject::InsertTrafficEvent(CSentinelEventObject* pevent, int index)  // -1 = add to end.
{
	if(pevent)
	{
		CSentinelEventObject** ppObj = new CSentinelEventObject*[m_nNumEvents+1];
		if(ppObj)
		{
			CSentinelEventObject** ppDelObj = m_ppevents;
			int o=0;
			if((m_ppevents)&&(m_nNumEvents>0))
			{
				if((index<0)||(index>=m_nNumEvents))
				{
					while(o<m_nNumEvents)
					{
						ppObj[o] = m_ppevents[o];
						o++;
					}
					ppObj[m_nNumEvents] = pevent;
					index = m_nNumEvents;
				}
				else
				{
					while(o<index)
					{
						ppObj[o] = m_ppevents[o];
						o++;
					}
					ppObj[o] = pevent;
					while(o<m_nNumEvents)
					{
						ppObj[o+1] = m_ppevents[o];
						o++;
					}
				}
			}
			else
			{
				ppObj[m_nNumEvents] = pevent;  // just add the one
				index = m_nNumEvents;
			}

			m_ppevents = ppObj;
			if(ppDelObj) delete [] ppDelObj;
			m_nNumEvents++;
			return index;
		}
	}

	return ADC_ERROR;
}


int CSentinelTrafficObject::DeleteAllTrafficEvents()  // removes all from array, and array itself
{
	if((m_nNumEvents)&&(m_ppevents))
	{
		int o=0;
		while(o<m_nNumEvents)
		{
			try{ if(m_ppevents[o]) delete m_ppevents[o]; } catch(...){}
			o++;
		}
		try{ delete [] m_ppevents; } catch(...){}
	}
	m_nNumEvents = 0;
	m_ppevents = NULL;
	return ADC_SUCCESS;
}

int CSentinelTrafficObject::DeleteTrafficEvent(int index)  // removes from array
{
	if((index>=0)&&(index<m_nNumEvents)&&(m_ppevents))
	{
		CSentinelEventObject** ppDelObj = m_ppevents;
		if(m_nNumEvents>1)
		{
			CSentinelEventObject** ppObj = new CSentinelEventObject*[m_nNumEvents-1];
			if(ppObj)
			{
				CSentinelEventObject* pDelEventObj = NULL;
				m_nNumEvents--;
				int o=0;
				while(o<index)
				{
					ppObj[o] = m_ppevents[o];
					o++;
				}
				pDelEventObj = m_ppevents[o];
				bool bRemovePrimary = false;
				if(o==0) bRemovePrimary = true; // was the top event, must null out parent pointer
				while(o<m_nNumEvents)
				{
					ppObj[o] = m_ppevents[o+1];
					if(bRemovePrimary)
					{
						if(ppObj[o]->m_event.m_usType&SECONDARYEVENT)
						{
							ppObj[o]->m_pParent=NULL;
						}
						else
						{
							bRemovePrimary = false;
						}
					}
					o++;
				}

				m_ppevents = ppObj;
				if(ppDelObj) delete [] ppDelObj;
				if(pDelEventObj) delete pDelEventObj;
				return ADC_SUCCESS;
			}
		}
		else
		{
			m_nNumEvents = 0;
			m_ppevents = NULL;
			if(ppDelObj) delete [] ppDelObj;
			return ADC_SUCCESS;
		}
	}
	return ADC_ERROR;
}
int CSentinelChannelObject::FindUid(int uid)  //returns index
{
	int index=-1;
	if((m_ppevents)&&(m_nNumEvents>0))
	{
		int i=0;
		while(i<m_nNumEvents)
		{
			if((m_ppevents[i])&&(m_ppevents[i]->m_uid==uid))
			{
				return i;
			}
			else
			{
			 i++;
			}
		}
	}
	return index;
}

int CSentinelChannelObject::FindTrafficObjectByDate(unsigned long ulDate)
{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "FindTrafficObjectByDate", "entering with date %d", ulDate);
Sleep(100);
	int index=-1;
	if((m_pptraffic)&&(m_nNumTraffic>0))
	{
		int i=0;
		while(i<m_nNumTraffic)
		{
			if((m_pptraffic[i])&&(m_pptraffic[i]->m_ulDate==ulDate))
			{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "FindTrafficObjectByDate", "index %d found for date %d", i, ulDate);
Sleep(100);
				return i;
			}
			else
			{
			 i++;
			}
		}
	}
	return index;
}
int CSentinelChannelObject::FindTrafficObjectByID(int nID)
{
	int index=-1;
	if((m_pptraffic)&&(m_nNumTraffic>0))
	{
		int i=0;
		while(i<m_nNumTraffic)
		{
			if((m_pptraffic[i])&&(m_pptraffic[i]->m_nID==nID))
			{
				return i;
			}
			else
			{
			 i++;
			}
		}
	}
	return index;
}

int CSentinelChannelObject::FindTrafficObjectByPath(char* pchFilePath)
{
	int index=-1;
	if((pchFilePath)&&(m_pptraffic)&&(m_nNumTraffic>0))
	{
		int i=0;
		while(i<m_nNumTraffic)
		{
			if((m_pptraffic[i])&&(m_pptraffic[i]->m_pszFileName)&&(stricmp(m_pptraffic[i]->m_pszFileName, pchFilePath)==0))
			{
				return i;
			}
			else
			{
			 i++;
			}
		}
	}
	return index;
}


int CSentinelChannelObject::FindEvent(CSentinelEventObject* pEvent, double dblTimeToleranceMS, double dblTimeOffset, double* pdblTimeOffset, int nMatchMode)  //returns index, uses IsMatch
{
	if((pEvent)&&(m_ppevents)&&(m_nNumEvents>0))
	{
		int i=m_nLastFound+1, q=0;
		while(q<m_nNumEvents)
		{
			if(i>=m_nNumEvents) i=0;
			if(
				  (m_ppevents[i])
				&&(!(m_ppevents[i]->m_nFlags&SENTINEL_EVENT_MATCHED))  // can't have been previously matched up, it is one to one here
				&&(m_ppevents[i]->IsMatch(pEvent, dblTimeToleranceMS, dblTimeOffset, pdblTimeOffset, nMatchMode))
				)
			{
				m_nLastFound = i;
				return i;
			}
			else
			{
			 i++;
			}
			q++;
		}
	}
	return ADC_ERROR;
}

int CSentinelChannelObject::FindEventByPosition(int nPosition)  //returns index, uses IsMatch
{
	if((m_ppevents)&&(m_nNumEvents>0)&&(nPosition>=0))
	{
		int i=nPosition, q=0;  // guess that it is right there, or smaller position now, for optimization
		while(q<m_nNumEvents)
		{
			if(i>=m_nNumEvents) i=0;
			if((m_ppevents[i])&&(m_ppevents[i]->m_nPosition==nPosition))
			{
				return i;
			}
			else
			{
			 i++;
			}
			q++;
		}
	}
	return ADC_ERROR;
}

int CSentinelChannelObject::InsertTrafficObject(CSentinelTrafficObject* pObj, int index)  // -1 = add to end.
{
	if(pObj)
	{
		CSentinelTrafficObject** ppObj = new CSentinelTrafficObject*[m_nNumTraffic+1];
		if(ppObj)
		{
			CSentinelTrafficObject** ppDelObj = m_pptraffic;
			int o=0;
			if((m_pptraffic)&&(m_nNumTraffic>0))
			{
				if((index<0)||(index>=m_nNumTraffic))
				{
					while(o<m_nNumTraffic)
					{
						ppObj[o] = m_pptraffic[o];
						o++;
					}
					ppObj[m_nNumTraffic] = pObj;
					index = m_nNumTraffic;
				}
				else
				{
					while(o<index)
					{
						ppObj[o] = m_pptraffic[o];
						o++;
					}
					ppObj[o] = pObj;
					while(o<m_nNumTraffic)
					{
						ppObj[o+1] = m_pptraffic[o];
						o++;
					}
				}
			}
			else
			{
				ppObj[m_nNumTraffic] = pObj;  // just add the one
				index = m_nNumTraffic;
			}

			m_pptraffic = ppObj;
			if(ppDelObj) { try { delete [] ppDelObj;} catch(...) {}}
			m_nNumTraffic++;
			return index;
		}
	}

	return ADC_ERROR;

}

int CSentinelChannelObject::InsertEvent(CSentinelEventObject* pevent, int index)
{
	if(pevent)
	{
		CSentinelEventObject** ppObj = new CSentinelEventObject*[m_nNumEvents+1];
		if(ppObj)
		{
			CSentinelEventObject** ppDelObj = m_ppevents;
			int o=0;
			if((m_ppevents)&&(m_nNumEvents>0))
			{
				if((index<0)||(index>=m_nNumEvents))
				{
					while(o<m_nNumEvents)
					{
						ppObj[o] = m_ppevents[o];
						o++;
					}
					ppObj[m_nNumEvents] = pevent;
					index = m_nNumEvents;
				}
				else
				{
					while(o<index)
					{
						ppObj[o] = m_ppevents[o];
						o++;
					}
					ppObj[o] = pevent;
					while(o<m_nNumEvents)
					{
						ppObj[o+1] = m_ppevents[o];
						o++;
					}
				}
			}
			else
			{
				ppObj[m_nNumEvents] = pevent;  // just add the one
				index = m_nNumEvents;
			}

			m_ppevents = ppObj;
			if(ppDelObj) { try {delete [] ppDelObj;} catch(...){} }
			m_nNumEvents++;


			// no need to do this here, UpdateEventSQL deals with it

/*
			if((g_psentinel->m_settings.m_bUseControlModules)&&(g_psentinel->m_data.m_bControlModuleServiceThreadStarted))
			{
				CBufferUtil  bu;
#pragma message(alert "here, send a change to send xml to control modules for event insert")

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "creating event change, insert [%s]", pevent->m_event.m_pszID); //  Sleep(250);//(Dispatch message)

		CSentinelChange* pNewChange =  new CSentinelChange;
		if(pNewChange)
		{
			pNewChange->m_pObj = (void*) new CSentinelChannelObject;
			if(pNewChange->m_pObj)
			{
				pNewChange->m_nType = DLLCMD_AITEM;

				CSentinelEventObject* pChgEvent = ((CSentinelEventObject*)(pNewChange->m_pObj));

				*pChgEvent = *(pevent);
				pChgEvent->m_pszEncodedKey = NULL;
				pChgEvent->m_pszEncodedID = NULL;
				pChgEvent->m_pszEncodedTitle = NULL;
				pChgEvent->m_pszEncodedRecKey = NULL;
				pChgEvent->m_pszEncodedData = NULL;
				pChgEvent->m_event.m_pszID = NULL;
				pChgEvent->m_event.m_pszReconcileKey = NULL;
				pChgEvent->m_event.m_pszTitle = NULL;
				pChgEvent->m_event.m_pszData = NULL;
				pChgEvent->m_event.m_pContextData = NULL;

				pNewChange->m_nChannelID = m_nChannelID;
				pNewChange->m_nHarrisListID = m_nHarrisListID;

				pNewChange->m_pszServerName =  bu.XMLEncode(m_pszServerName);
//				pNewChange->m_pszDesc = bu.XMLEncode(pChannel->m_pszDesc);

				pNewChange->m_pchXMLkey =  bu.XMLEncode(pevent->m_pszEncodedKey);
				pNewChange->m_pchXMLrec =  bu.XMLEncode(pevent->m_event.m_pszReconcileKey);
				pNewChange->m_pchXMLclip =  bu.XMLEncode(pevent->m_event.m_pszID);
				pNewChange->m_pchXMLtitle =  bu.XMLEncode(pevent->m_event.m_pszTitle);
				pNewChange->m_pchXMLdata =  bu.XMLEncode(pevent->m_event.m_pszData);

				if(pevent->m_pParent)
				{
					pNewChange->m_nParentUID = ((CSentinelEventObject*)(pevent->m_pParent))->m_uid;
				}
				else
				{
					pNewChange->m_nParentUID = -1;
				}


				if(_beginthread(SentinelPublishChangeThread, 0, (void*)(pNewChange))==-1)
				{
					//error.

					//**MSG
					g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start publish change thread."); //(Dispatch message)

					try{ delete (pNewChange); } catch(...){}
				}
				
			}
			else
			{
				try{ delete (pNewChange); } catch(...){}
			}
		}

			}

*/
			return index;

		}
	}

	return ADC_ERROR;
}

int CSentinelChannelObject::DeleteEvent(int index)  // removes from array
{
	if((index>=0)&&(index<m_nNumEvents)&&(m_ppevents))
	{
		CSentinelEventObject** ppDelObj = m_ppevents;
		if(m_nNumEvents>1)
		{
			CSentinelEventObject** ppObj = new CSentinelEventObject*[m_nNumEvents-1];
			if(ppObj)
			{
				CSentinelEventObject* pDelEventObj = NULL;
				m_nNumEvents--;
				int o=0;
				while(o<index)
				{
					ppObj[o] = m_ppevents[o];
					o++;
				}
				pDelEventObj = m_ppevents[o];
				bool bRemovePrimary = false;
				if(o==0) bRemovePrimary = true; // was the top event, must null out parent pointer
				while(o<m_nNumEvents)
				{
					ppObj[o] = m_ppevents[o+1];
					if(bRemovePrimary)
					{
						if(ppObj[o]->m_event.m_usType&SECONDARYEVENT)
						{
							ppObj[o]->m_pParent=NULL;
						}
						else
						{
							bRemovePrimary = false;
						}
					}
					o++;
				}

				m_ppevents = ppObj;
				if(ppDelObj) {try{ delete [] ppDelObj; } catch(...){}}
				if(pDelEventObj) {try{ delete pDelEventObj; } catch(...){}}
				return ADC_SUCCESS;
			}
		}
		else
		{
			if(m_nNumEvents==1)
			{
				try{ delete m_ppevents[0]; } catch(...){}
			}
			m_nNumEvents = 0;
			m_ppevents = NULL;
			if(ppDelObj) {try{ delete [] ppDelObj; } catch(...){}}
			return ADC_SUCCESS;
		}
	}
	return ADC_ERROR;
}

int CSentinelChannelObject::DeleteTrafficObject(int index)  // removes from array
{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) 
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "DeleteTrafficObject", "entering with index %d, num=%d, p=%d", index, m_nNumTraffic, m_pptraffic);
Sleep(100);
	if((index>=0)&&(index<m_nNumTraffic)&&(m_pptraffic))
	{
		CSentinelTrafficObject** ppDelObj = m_pptraffic;
		if(m_nNumTraffic>1)
		{
			CSentinelTrafficObject** ppObj = new CSentinelTrafficObject*[m_nNumTraffic-1];
			if(ppObj)
			{
				CSentinelTrafficObject* pDelTrafficObj = NULL;
				m_nNumTraffic--;
				int o=0;
				while(o<index)
				{
					ppObj[o] = m_pptraffic[o];
					o++;
				}
				pDelTrafficObj = m_pptraffic[o];
				while(o<m_nNumTraffic)
				{
					ppObj[o] = m_pptraffic[o+1];
					o++;
				}

				m_pptraffic = ppObj;
				if(ppDelObj) {try{ delete [] ppDelObj; } catch(...){}}
				if(pDelTrafficObj) {try{ delete pDelTrafficObj; } catch(...){}}
				return ADC_SUCCESS;
			}
		}
		else
		{
			if(m_nNumTraffic==1)
			{
				try{ delete m_pptraffic[0]; } catch(...){}
			}
			m_nNumTraffic = 0;
			m_pptraffic = NULL;
			if(ppDelObj) {try{ delete [] ppDelObj; } catch(...){}}
			return ADC_SUCCESS;
		}
	}
	return ADC_ERROR;
}



int CSentinelChannelObject::UpdateEventSQL(int index, CDBUtil* pdb,	CDBconn* pdbConn, double dblServerTime)
{

/*
CREATE TABLE Events(
itemid int NOT NULL, 
event_key varchar(256), 
server_name varchar(32), 
server_list int, 
list_id int, 
event_id varchar(32), 
event_clip varchar(64), 
event_segment int, 
event_title varchar(64), 
event_data varchar(4096),
event_type int, 
event_status int, 
event_time_mode int, 
event_start decimal(20,3), 
event_duration int, 
event_calc_start decimal(20,3), 
event_calc_duration int, 
event_calc_end decimal(20,3), 
event_position int, 
event_last_update decimal(20,3), 
parent_uid int, 
parent_key varchar(256), 
parent_id varchar(32), 
parent_clip varchar(64), 
parent_segment int, 
parent_title varchar(64), 
parent_type int, 
parent_status int, 
parent_time_mode int, 
parent_start decimal(20,3), 
parent_duration int, 
parent_calc_start decimal(20,3), 
parent_calc_duration int, 
parent_calc_end decimal(20,3), 
parent_position int, 
app_data varchar(512), 
app_data_aux int);   
*/


	int nRV = ADC_ERROR;

	if(
		  (index>=0)
		&&(index<m_nNumEvents)
		&&(m_ppevents)
		&&(m_ppevents[index])
		&&(m_ppevents[index]->m_nFlags&SENTINEL_EVENT_ACQUIRED)
		&&(pdb)
		&&(pdbConn)
		&&(!m_bKillChannelThread)
		&&(!g_bKillThread)
		)
	{
		CSentinelEventObject* pEvent = m_ppevents[index];
		CSentinelEventObject* pParent = (CSentinelEventObject*)m_ppevents[index]->m_pParent;
		bool bSelf=false;
		if((pParent)&&(pEvent->m_uid == pParent->m_uid)) bSelf=true;

		char szSQL[DB_SQLSTRING_MAXLEN];
		char szSQLTemp[DB_SQLSTRING_MAXLEN];
		char dberrorstring[DB_ERRORSTRING_LEN];

//		CString foo; foo.Format("[%s] dev %04x", m_ppevents[index]->m_event.m_pszID,m_ppevents[index]->m_event.m_usDevice );
//		AfxMessageBox(foo);

// change all of following to build string based on only what has changed
//		int nClock = clock();
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updating %d [%s]", m_pszDesc, index, pEvent->m_event.m_pszID); // Sleep(50); //(Dispatch message)


		if(pEvent->m_nFlags&SENTINEL_EVENT_INSERTED)
		{


//g_psentinel->m_msgr.DM(MSG_ICONUSER1, NULL, "Sentinel:debug", "before format -> Event %d", n);//  Sleep(50); //(Dispatch message)

// note: event_uid was intended, but made itemid to conserve some old stuff so we didnt have to change a lot of stuff.

			pEvent->m_dblUpdateTime = dblServerTime;
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET event_last_update = %.3f", g_psentinel->m_settings.m_pszLiveEvents, dblServerTime);  // table name

			bool bEventStart = false;
			bool bEventCalcStart = false;
			bool bEventCalcEnd = false;
			bool bParentEventStart = false;
			bool bParentEventCalcStart = false;
			bool bParentEventCalcEnd = false;
			int nDiffers = SENTINEL_EVENT_DIFFERS_TYPE;
			while(nDiffers<=pEvent->m_nDiffers)
			{
				if(pEvent->m_nDiffers&nDiffers)
				{
					// change this item.
					switch(nDiffers)
					{
					case SENTINEL_EVENT_DIFFERS_TYPE://						0x00000001  // event differs on type
						{
							if(bSelf)
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_type = %d, parent_type = %d", szSQL, pEvent->m_event.m_usType, pEvent->m_event.m_usType); //event_type
							}
							else
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_type = %d", szSQL, pEvent->m_event.m_usType); //event_type
							}
						}
						strcpy(szSQL,szSQLTemp); break;
					case SENTINEL_EVENT_DIFFERS_SEG://						0x00000002  // event differs on segment
						{
							if(bSelf)
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_segment = %d, parent_segment = %d", szSQL, (pEvent->m_event.m_ucSegment==0xff?-1:pEvent->m_event.m_ucSegment), (pEvent->m_event.m_ucSegment==0xff?-1:pEvent->m_event.m_ucSegment)); //event_segment
							}
							else
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_segment = %d", szSQL, (pEvent->m_event.m_ucSegment==0xff?-1:pEvent->m_event.m_ucSegment)); //event_segment
							}
						}
						strcpy(szSQL,szSQLTemp); break;
					case SENTINEL_EVENT_DIFFERS_OAT://						0x00000004  // event differs on on air time
						{
							if(bSelf)
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_start = %.3f, parent_start = %.3f", szSQL, pEvent->m_dblTime, pEvent->m_dblTime); //event_start
								bParentEventStart = true;
							}
							else
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_start = %.3f", szSQL, pEvent->m_dblTime); //event_start
							}
							bEventStart = true;
						}
						strcpy(szSQL,szSQLTemp); break;

// no need to do Julian date, since the On Air time double would be affected, most likely.
//					case SENTINEL_EVENT_DIFFERS_DATE://						0x00000008  // event differs on julian date

					case SENTINEL_EVENT_DIFFERS_DUR://						0x00000010  // event differs on duration
						{
							if(bSelf)
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_duration = %d, parent_duration = %d", szSQL, pEvent->m_event.m_ulDurationMS, pEvent->m_event.m_ulDurationMS); //event_duration
							}
							else
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_duration = %d", szSQL, pEvent->m_event.m_ulDurationMS); //event_duration
							}
						}
						strcpy(szSQL,szSQLTemp); break;
					case SENTINEL_EVENT_DIFFERS_STATUS://					0x00000020  // event differs on status
						{
							if(bSelf)
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_status = %d, parent_status = %d", szSQL, 
									( pEvent->m_event.m_usStatus | ((pEvent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT))?(pEvent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT)):0) | (((pEvent->m_event.m_usDevice==0x0000)&&(!(pEvent->IsNoDeviceType())))?SENTINEL_EVENT_NODEVICE:((pEvent->m_event.m_usDevice&0x00ff)?SENTINEL_EVENT_CUED:0)) ) , 
									( pEvent->m_event.m_usStatus | ((pEvent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT))?(pEvent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT)):0) | (((pEvent->m_event.m_usDevice==0x0000)&&(!(pEvent->IsNoDeviceType())))?SENTINEL_EVENT_NODEVICE:((pEvent->m_event.m_usDevice&0x00ff)?SENTINEL_EVENT_CUED:0)) ) 
									); //event_status
							}
							else
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_status = %d", szSQL, 
									( pEvent->m_event.m_usStatus | ((pEvent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT))?(pEvent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT)):0) | (((pEvent->m_event.m_usDevice==0x0000)&&(!(pEvent->IsNoDeviceType())))?SENTINEL_EVENT_NODEVICE:((pEvent->m_event.m_usDevice&0x00ff)?SENTINEL_EVENT_CUED:0)) ) 
									); //event_status
							}
						}
						strcpy(szSQL,szSQLTemp); break;
					case SENTINEL_EVENT_DIFFERS_CTRL://						0x00000040  // event differs on ctrl
						{
							if(bSelf)
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_time_mode = %d, parent_time_mode = %d", szSQL, pEvent->m_event.m_ulControl, pEvent->m_event.m_ulControl); //event_time_mode
							}
							else
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_time_mode = %d", szSQL, pEvent->m_event.m_ulControl); //event_time_mode
							}
						}
						strcpy(szSQL,szSQLTemp); break;
					case SENTINEL_EVENT_DIFFERS_POS://						0x00000080  // event differs on position
						{
							if(bSelf)
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_position = %d, parent_position = %d", szSQL, pEvent->m_nPosition, pEvent->m_nPosition); //event_position
							}
							else
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_position = %d", szSQL, pEvent->m_nPosition); //event_position
							}
						}
						strcpy(szSQL,szSQLTemp); break;
					case SENTINEL_EVENT_DIFFERS_CALCOAT://				0x00000100  // event differs on calc on air time
						{
							if(bSelf)
							{
								if(bEventCalcStart)
								{
									if(!bEventCalcEnd)
									{
										if(bParentEventCalcEnd)
										{
											_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_end = %.3f",
												szSQL,
												(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0))); //event_calc_start
										}
										else
										{
											_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_end = %.3f, parent_calc_end = %.3f",
												szSQL,
												(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0)), 
												(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0))); //event_calc_start
										}
									}
								}
								else
								{
									if(bEventCalcEnd)
									{
										if(bParentEventCalcStart)
										{
											_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_start = %.3f",
												szSQL, pEvent->m_dblCalcTime); //event_calc_start
										}
										else
										{
											_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_start = %.3f, parent_calc_start = %.3f",
												szSQL, pEvent->m_dblCalcTime, pEvent->m_dblCalcTime); //event_calc_start
										}
									}
									else
									{
										if(bParentEventCalcStart)
										{
											if(bParentEventCalcEnd)
											{
												_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_start = %.3f, event_calc_end = %.3f",
													szSQL, pEvent->m_dblCalcTime,
													(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0))); //event_calc_start
											}
											else
											{
												_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_start = %.3f, event_calc_end = %.3f, parent_calc_end = %.3f",
													szSQL, pEvent->m_dblCalcTime,
													(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0)), 
													(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0))); //event_calc_start
											}

										}
										else
										{
											if(bParentEventCalcEnd)
											{
												_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_start = %.3f, parent_calc_start = %.3f, event_calc_end = %.3f",
													szSQL, pEvent->m_dblCalcTime, pEvent->m_dblCalcTime,
													(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0))); //event_calc_start
											}
											else
											{
												_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_start = %.3f, parent_calc_start = %.3f, event_calc_end = %.3f, parent_calc_end = %.3f",
													szSQL, pEvent->m_dblCalcTime, pEvent->m_dblCalcTime,
													(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0)), 
													(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0))); //event_calc_start
											}
										}
									}
									bParentEventCalcStart = true;
								}
								bParentEventCalcEnd = true;
							}
							else
							{
								if(bEventCalcStart)
								{

									if(!bEventCalcEnd)
									{
										_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_end = %.3f", 
											szSQL, 
											(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0))); //event_calc_start
									}
								}
								else
								{

									if(bEventCalcEnd)
									{
										_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_start = %.3f", 
											szSQL, pEvent->m_dblCalcTime); //event_calc_start
									}
									else
									{
										_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_start = %.3f, event_calc_end = %.3f", 
											szSQL, pEvent->m_dblCalcTime,
											(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0))); //event_calc_start
									}
								}
							}
							bEventCalcStart = true;
							bEventCalcEnd = true;
						}
						strcpy(szSQL,szSQLTemp); break;
					case SENTINEL_EVENT_DIFFERS_CALCDUR://				0x00000200  // event differs on calculated duration
						{
							if(bSelf)
							{
								if(bEventCalcEnd)
								{
									_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_duration = %d, parent_calc_duration = %d",
										szSQL, pEvent->m_nCalcDur, pEvent->m_nCalcDur); //event_calc_duration
								}
								else
								{
									if(bParentEventCalcEnd)
									{
										_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_duration = %d, parent_calc_duration = %d, event_calc_end = %.3f",
											szSQL, pEvent->m_nCalcDur, pEvent->m_nCalcDur,
											(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0)) ); //event_calc_duration
									}
									else
									{
										_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_duration = %d, parent_calc_duration = %d, event_calc_end = %.3f, parent_calc_end = %.3f",
											szSQL, pEvent->m_nCalcDur, pEvent->m_nCalcDur,
											(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0)),
											(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0))); //event_calc_duration
									}
								}
								bParentEventCalcEnd = true;
							}
							else
							{
								if(bEventCalcEnd)
								{
									_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_duration = %d",
										szSQL, pEvent->m_nCalcDur); //event_calc_duration
								}
								else
								{
									_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_calc_duration = %d, event_calc_end = %.3f",
										szSQL, pEvent->m_nCalcDur,
										(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0))); //event_calc_duration
								}
							}
							bEventCalcEnd = true;
						}
						strcpy(szSQL,szSQLTemp); break;
					case SENTINEL_EVENT_DIFFERS_ID://							0x00001000  // event differs on ID
						{
							if(bSelf)
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_clip = '%s', parent_clip = '%s'", szSQL, 
									(pEvent->m_pszEncodedID?pEvent->m_pszEncodedID:""),
									(pEvent->m_pszEncodedID?pEvent->m_pszEncodedID:"")); //event_clip
							}
							else
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_clip = '%s'", szSQL, 
									(pEvent->m_pszEncodedID?pEvent->m_pszEncodedID:"")); //event_clip
							}
						}
						strcpy(szSQL,szSQLTemp); break;
					case SENTINEL_EVENT_DIFFERS_TITLE://					0x00002000  // event differs on TITLE
						{
							if(bSelf)
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_title = '%s', parent_title = '%s'", szSQL, 
									(pEvent->m_pszEncodedTitle?pEvent->m_pszEncodedTitle:""),
									(pEvent->m_pszEncodedTitle?pEvent->m_pszEncodedTitle:"")); //event_title
							}
							else
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_title = '%s'", szSQL, 
									(pEvent->m_pszEncodedTitle?pEvent->m_pszEncodedTitle:"")); //event_title
							}
						}
						strcpy(szSQL,szSQLTemp); break;
					case SENTINEL_EVENT_DIFFERS_DATA://						0x00004000  // event differs on DATA
						{
							_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_data = '%s'", szSQL, 
									(pEvent->m_pszEncodedData?pEvent->m_pszEncodedData:"")); //event_id
						}
						strcpy(szSQL,szSQLTemp); break;
					case SENTINEL_EVENT_DIFFERS_RECKEY://					0x00008000  // event differs on RECKEY
						{
							if(bSelf)
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_id = '%s', parent_id = '%s'", szSQL, 
									(pEvent->m_pszEncodedRecKey?pEvent->m_pszEncodedRecKey:""),
									(pEvent->m_pszEncodedRecKey?pEvent->m_pszEncodedRecKey:"")); //event_id
							}
							else
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_id = '%s'", szSQL, 
									(pEvent->m_pszEncodedRecKey?pEvent->m_pszEncodedRecKey:"")); //event_id
							}
						}
						strcpy(szSQL,szSQLTemp); break;
					case SENTINEL_EVENT_DIFFERS_KEY://					0x00010000  // event differs on the assembled key
						{
							if(bSelf)
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_key = '%s', parent_key = '%s'", szSQL, 
									(pEvent->m_pszEncodedKey?pEvent->m_pszEncodedKey:""),
									(pEvent->m_pszEncodedKey?pEvent->m_pszEncodedKey:"")); //event_key
							}
							else
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_key = '%s'", szSQL, 
									(pEvent->m_pszEncodedKey?pEvent->m_pszEncodedKey:"")); //event_key
							}
						}
						strcpy(szSQL,szSQLTemp); break;
					}
				}

				(nDiffers<<=1);
			}


			if((!bSelf)&&(pParent!=NULL))  // this must be a secondary event!
			{
				nDiffers = SENTINEL_EVENT_DIFFERS_TYPE;
				while(nDiffers<=pParent->m_nDiffers)
				{
					if(pParent->m_nDiffers&nDiffers)
					{
						// change this item.
						switch(nDiffers)
						{
						case SENTINEL_EVENT_DIFFERS_TYPE://						0x00000001  // event differs on type
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_type = %d", szSQL, pParent->m_event.m_usType); //event_type
							}
							strcpy(szSQL,szSQLTemp); break;
						case SENTINEL_EVENT_DIFFERS_SEG://						0x00000002  // event differs on segment
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_segment = %d", szSQL, 
									(pParent->m_event.m_ucSegment==0xff?-1:pParent->m_event.m_ucSegment)); //parent_segment
							}
							strcpy(szSQL,szSQLTemp); break;
						case SENTINEL_EVENT_DIFFERS_OAT://						0x00000004  // event differs on on air time  
							{
								// since the parent event differs, the secondary takes its time as an offset, so must update event time too.
								// but only if not already in there
								if(bEventStart)
								{
									if(!bParentEventStart)
									{
										_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_start = %.3f", szSQL, pParent->m_dblTime); //parent_start
									}
								}
								else
								{
									if(bParentEventStart)
									{
										_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_start = %.3f", szSQL, pEvent->m_dblTime); //parent_start
									}
									else
									{
										_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, event_start = %.3f, parent_start = %.3f", szSQL, pEvent->m_dblTime, pParent->m_dblTime); //parent_start
									}
								}
								bParentEventStart = true;
							}
							strcpy(szSQL,szSQLTemp); break;

	// no need to do Julian date, since the On Air time double would be affected, most likely.
	//					case SENTINEL_EVENT_DIFFERS_DATE://						0x00000008  // event differs on julian date

						case SENTINEL_EVENT_DIFFERS_DUR://						0x00000010  // event differs on duration
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_duration = %d", szSQL, pParent->m_event.m_ulDurationMS); //parent_duration
							}
							strcpy(szSQL,szSQLTemp); break;
						case SENTINEL_EVENT_DIFFERS_STATUS://					0x00000020  // event differs on status
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_status = %d", szSQL, 
									//                             this one                                   and this one were pEvent, a bug I believe.
									( pParent->m_event.m_usStatus | ((pParent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT))?(pParent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT)):0) | (((pParent->m_event.m_usDevice==0x0000)&&(!(pParent->IsNoDeviceType())))?SENTINEL_EVENT_NODEVICE:((pParent->m_event.m_usDevice&0x00ff)?SENTINEL_EVENT_CUED:0)) )
									); //parent_status
							}
							strcpy(szSQL,szSQLTemp); break;
						case SENTINEL_EVENT_DIFFERS_CTRL://						0x00000040  // event differs on ctrl
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_time_mode = %d", szSQL, pParent->m_event.m_ulControl); //parent_time_mode
							}
							strcpy(szSQL,szSQLTemp); break;
						case SENTINEL_EVENT_DIFFERS_POS://						0x00000080  // event differs on position
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_position = %d", szSQL, pParent->m_nPosition); //parent_position
							}
							strcpy(szSQL,szSQLTemp); break;
						case SENTINEL_EVENT_DIFFERS_CALCOAT://				0x00000100  // event differs on calc on air time
							{
								// since the parent event differs, the calculated on air time must already be accounted for above
								if(bParentEventCalcStart)
								{
									if(!bParentEventCalcEnd)
									{
										_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_calc_end = %.3f", 
												szSQL,
												pParent->m_dblCalcTime + (double)pParent->m_nCalcDur/1000.0); //parent_calc_start
									}
								}
								else
								{
									if(bParentEventCalcEnd)
									{
										_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_calc_start = %.3f", 
												szSQL, pParent->m_dblCalcTime ); //parent_calc_start
									}
									else
									{
										_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_calc_start = %.3f, parent_calc_end = %.3f", 
												szSQL, pParent->m_dblCalcTime,
												pParent->m_dblCalcTime + (double)pParent->m_nCalcDur/1000.0); //parent_calc_start
									}
								}
								bParentEventCalcStart = true;
								bParentEventCalcEnd = true;
							}
							strcpy(szSQL,szSQLTemp); break;
						case SENTINEL_EVENT_DIFFERS_CALCDUR://				0x00000200  // event differs on calculated duration
							{
								if(bParentEventCalcEnd)
								{
									_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_calc_duration = %d",
											szSQL, pParent->m_nCalcDur ); //parent_calc_duration
								}
								else
								{
									_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_calc_duration = %d, parent_calc_end = %.3f",
											szSQL, pParent->m_nCalcDur,
											pParent->m_dblCalcTime + (double)pParent->m_nCalcDur/1000.0); //parent_calc_duration
								}
								bParentEventCalcEnd = true;
							}
							strcpy(szSQL,szSQLTemp); break;
						case SENTINEL_EVENT_DIFFERS_ID://							0x00001000  // event differs on ID
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_clip = '%s'", szSQL, 
										(pParent->m_pszEncodedID?pParent->m_pszEncodedID:"")); //parent_clip
							}
							strcpy(szSQL,szSQLTemp); break;
						case SENTINEL_EVENT_DIFFERS_TITLE://					0x00002000  // event differs on TITLE
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_title = '%s'", szSQL, 
										(pParent->m_pszEncodedTitle?pParent->m_pszEncodedTitle:"")); //parent_title
							}
							strcpy(szSQL,szSQLTemp); break;
/*
						case SENTINEL_EVENT_DIFFERS_DATA://						0x00004000  // event differs on DATA
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_data = '%s'", szSQL, 
										(pParent->m_pszEncodedData?pParent->m_pszEncodedData:"")); //parent_id
							}
							strcpy(szSQL,szSQLTemp); break;
*/
						case SENTINEL_EVENT_DIFFERS_RECKEY://					0x00008000  // event differs on RECKEY
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_id = '%s'", szSQL, 
										(pParent->m_pszEncodedRecKey?pParent->m_pszEncodedRecKey:"")); //parent_id
							}
							strcpy(szSQL,szSQLTemp); break;
						case SENTINEL_EVENT_DIFFERS_KEY://					0x00010000  // event differs on the assembled key
							{
								_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s, parent_key = '%s'", szSQL, 
										(pParent->m_pszEncodedKey?pParent->m_pszEncodedKey:"")); //parent_key
							}
							strcpy(szSQL,szSQLTemp); break;
						}
					}

					(nDiffers<<=1);
				}
			}


			_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s WHERE itemid = %d", szSQL, pEvent->m_uid); //itemid
			strcpy(szSQL,szSQLTemp);

//  */
/*
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
event_key = '%s', \
event_id = '%s', \
event_clip = '%s', \
event_segment = %d, \
event_title = '%s', \
%s%s%s%s\
event_type = %d, \
event_status = %d, \
event_time_mode = %d, \
event_start = %.3f, \
event_duration = %d, \
event_calc_start = %.3f, \
event_calc_duration = %d, \
event_calc_end = %.3f, \
event_position = %d, \
event_last_update = %.3f, \
parent_uid = %d, \
parent_key = '%s', \
parent_id = '%s', \
parent_clip = '%s', \
parent_segment = %d, \
parent_title = '%s', \
parent_type = %d, \
parent_status = %d, \
parent_time_mode = %d, \
parent_start = %.3f, \
parent_duration = %d, \
parent_calc_start = %.3f, \
parent_calc_duration = %d, \
parent_calc_end = %.3f, \
parent_position = %d \
WHERE itemid = %d",

				g_psentinel->m_settings.m_pszLiveEvents,  // table name

// event fields
				(pEvent->m_pszEncodedKey?pEvent->m_pszEncodedKey:""),
				(pEvent->m_pszEncodedRecKey?pEvent->m_pszEncodedRecKey:(pEvent->m_event.m_pszReconcileKey?pEvent->m_event.m_pszReconcileKey:"")), //event_id
				(pEvent->m_pszEncodedID?pEvent->m_pszEncodedID:(pEvent->m_event.m_pszID?pEvent->m_event.m_pszID:"")), //event_clip  (must not be null)
				(pEvent->m_event.m_ucSegment==0xff?-1:pEvent->m_event.m_ucSegment),
				(pEvent->m_pszEncodedTitle?pEvent->m_pszEncodedTitle:""), //event_title
				(pEvent->m_pszEncodedData?"event_data, ":""), //event_data (exists)
				(pEvent->m_pszEncodedData?"'":""), //event_data (exists)
				(pEvent->m_pszEncodedData?pEvent->m_pszEncodedData:""), //event_data  
				(pEvent->m_pszEncodedData?"', ":""), //event_data (exists)
				pEvent->m_event.m_usType, //event_type
				pEvent->m_event.m_usStatus, //event_status
				pEvent->m_event.m_ulControl, //event_time_mode
				pEvent->m_dblTime, //event_start
				pEvent->m_event.m_ulDurationMS, //event_duration
				pEvent->m_dblCalcTime, //event_calc_start
				pEvent->m_nCalcDur, //event_calc_duration
				(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0)),
				pEvent->m_nPosition,
				dblServerTime, //server time
// parent fields
				(bSelf?pEvent->m_uid:pParent->m_uid),
				(bSelf?(pEvent->m_pszEncodedKey?pEvent->m_pszEncodedKey:""):(pParent->m_pszEncodedKey?pParent->m_pszEncodedKey:"")),
				(bSelf?(pEvent->m_pszEncodedRecKey?pEvent->m_pszEncodedRecKey:""):(pParent->m_pszEncodedRecKey?pParent->m_pszEncodedRecKey:"")),
				(bSelf?(pEvent->m_pszEncodedID?pEvent->m_pszEncodedID:""):(pParent->m_pszEncodedID?pParent->m_pszEncodedID:"")),
				(bSelf?(pEvent->m_event.m_ucSegment==0xff?-1:pEvent->m_event.m_ucSegment):(pParent?(pParent->m_event.m_ucSegment==0xff?-1:pParent->m_event.m_ucSegment):-1)),
				(bSelf?(pEvent->m_pszEncodedTitle?pEvent->m_pszEncodedTitle:""):(pParent->m_pszEncodedTitle?pParent->m_pszEncodedTitle:"")),
				(bSelf?(pEvent->m_event.m_usType):(pParent?pParent->m_event.m_usType:-1)),
				(bSelf?(pEvent->m_event.m_usStatus):(pParent?pParent->m_event.m_usStatus:-1)),
				(bSelf?(pEvent->m_event.m_ulControl):(pParent?pParent->m_event.m_ulControl:-1)),
				(bSelf?(pEvent->m_dblTime):(pParent?pParent->m_dblTime:-1.0)),
				(bSelf?(pEvent->m_event.m_ulDurationMS):(pParent?pParent->m_event.m_ulDurationMS:-1)),
				(bSelf?(pEvent->m_dblCalcTime):(pParent?pParent->m_dblCalcTime:-1.0)),
				(bSelf?(pEvent->m_nCalcDur):(pParent?pParent->m_nCalcDur:-1)),
				(bSelf?((((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0))):(pParent?(pParent->m_dblCalcTime + (double)pParent->m_nCalcDur/1000.0):-1.0)),
				(bSelf?(pEvent->m_nPosition):(pParent?pParent->m_nPosition:-1)),
// where
				pEvent->m_uid
				);


/*
			// removed these from the where clause because they do not change.
AND server_name = '%s' \
AND server_list = %d \
AND list_id = %d",
				m_pszServerName, //server_name
				m_nHarrisListID, // server_list - actual 1-based harris list number
				m_nChannelID, // list_id - internal lookup channel number

*/
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updating %d [%s] built string %d ms", m_pszDesc, index, pEvent->m_event.m_pszID, clock()-nClock); // Sleep(50); //(Dispatch message)

if(g_psentinel->m_settings.m_bDebugInsertSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "SentinelChannelThread event update A: %s", szSQL);  //Sleep(250); //(Dispatch message)
//g_psentinel->m_msgr.DM(MSG_ICONUSER1, NULL, "Sentinel:debug", "before insert -> Event %d", n);//  Sleep(50); //(Dispatch message)

if(!g_psentinel->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
			if((!m_bKillChannelThread)&&(!g_bKillThread))
			{
//if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, m_pszSource, "SentinelChannelThread update: %s", szSQL); // Sleep(50); //(Dispatch message)
				if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
				{
					//**MSG
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
				}
				else
				{
					pEvent->m_nFlags |= SENTINEL_EVENT_INSERTED;
					nRV = ADC_SUCCESS;
				}
			}
//g_psentinel->m_msgr.DM(MSG_ICONUSER1, NULL, "Sentinel:debug", "after insert -> Event %d", n);//  Sleep(50); //(Dispatch message)
if(!g_psentinel->m_settings.m_bMultiConnectSQL) LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updated %d [%s] %d ms", m_pszDesc, index, pEvent->m_event.m_pszID, clock()-nClock); // Sleep(50); //(Dispatch message)
		}
		else // have to insert
		{

//g_psentinel->m_msgr.DM(MSG_ICONUSER1, NULL, "Sentinel:debug", "before format -> Event %d", n);//  Sleep(50); //(Dispatch message)
// note: event_uid was intended, but made itemid to conserve some old stuff so we didnt have to change a lot of stuff.

			pEvent->m_dblUpdateTime = dblServerTime;
			if(pParent)
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
itemid, \
event_key, \
server_name, \
server_list, \
list_id, \
event_id, \
event_clip, \
event_segment, \
event_title, \
%s\
event_type, \
event_status, \
event_time_mode, \
event_start, \
event_duration, \
event_calc_start, \
event_calc_duration, \
event_calc_end, \
event_position, \
event_last_update, \
parent_uid, \
parent_key, \
parent_id, \
parent_clip, \
parent_segment, \
parent_title, \
parent_type, \
parent_status, \
parent_time_mode, \
parent_start, \
parent_duration, \
parent_calc_start, \
parent_calc_duration, \
parent_calc_end, \
parent_position) VALUES \
(%d, '%s', '%s', %d, %d, '%s', '%s', %d, '%s', %s%s%s%d, %d, %d, %.3f, %d, %.3f, %d, %.3f, %d, %.3f, \
%d, '%s', '%s', '%s', %d, '%s', %d, %d, %d, %.3f, %d, %.3f, %d, %.3f, %d)",
					g_psentinel->m_settings.m_pszLiveEvents,  // table name
					(pEvent->m_pszEncodedData?"event_data, ":""), //event_data (exists)

	// event fields
					pEvent->m_uid,
					(pEvent->m_pszEncodedKey?pEvent->m_pszEncodedKey:""),
					m_pszServerName, //server_name
					m_nHarrisListID, // server_list - actual 1-based harris list number
					m_nChannelID, // list_id - internal lookup channel number
					(pEvent->m_pszEncodedRecKey?pEvent->m_pszEncodedRecKey:(pEvent->m_event.m_pszReconcileKey?pEvent->m_event.m_pszReconcileKey:"")), //event_id
					(pEvent->m_pszEncodedID?pEvent->m_pszEncodedID:(pEvent->m_event.m_pszID?pEvent->m_event.m_pszID:"")), //event_clip  (must not be null)
					(pEvent->m_event.m_ucSegment==0xff?-1:pEvent->m_event.m_ucSegment),
					(pEvent->m_pszEncodedTitle?pEvent->m_pszEncodedTitle:""), //event_title
					(pEvent->m_pszEncodedData?"'":""), //event_data (exists)
					(pEvent->m_pszEncodedData?pEvent->m_pszEncodedData:""), //event_data  
					(pEvent->m_pszEncodedData?"', ":""), //event_data (exists)
					pEvent->m_event.m_usType, //event_type
					pEvent->m_event.m_usStatus|((pEvent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT))?(pEvent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT)):0)|(((pEvent->m_event.m_usDevice==0x0000)&&(!(pEvent->IsNoDeviceType())))?SENTINEL_EVENT_NODEVICE:((pEvent->m_event.m_usDevice&0x00ff)?SENTINEL_EVENT_CUED:0)) , //event_status
					pEvent->m_event.m_ulControl, //event_time_mode
					pEvent->m_dblTime, //event_start
					pEvent->m_event.m_ulDurationMS, //event_duration
					pEvent->m_dblCalcTime, //event_calc_start
					pEvent->m_nCalcDur, //event_calc_duration
					(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0)),
					pEvent->m_nPosition,
					dblServerTime, //server time
	// parent fields
					(bSelf?pEvent->m_uid:pParent->m_uid),
					(bSelf?(pEvent->m_pszEncodedKey?pEvent->m_pszEncodedKey:""):(pParent->m_pszEncodedKey?pParent->m_pszEncodedKey:"")),
					(bSelf?(pEvent->m_pszEncodedRecKey?pEvent->m_pszEncodedRecKey:""):(pParent->m_pszEncodedRecKey?pParent->m_pszEncodedRecKey:"")),
					(bSelf?(pEvent->m_pszEncodedID?pEvent->m_pszEncodedID:""):(pParent->m_pszEncodedID?pParent->m_pszEncodedID:"")),
					(bSelf?(pEvent->m_event.m_ucSegment==0xff?-1:pEvent->m_event.m_ucSegment):(pParent?(pParent->m_event.m_ucSegment==0xff?-1:pParent->m_event.m_ucSegment):-1)),
					(bSelf?(pEvent->m_pszEncodedTitle?pEvent->m_pszEncodedTitle:""):(pParent->m_pszEncodedTitle?pParent->m_pszEncodedTitle:"")),
					(bSelf?(pEvent->m_event.m_usType):(pParent?pParent->m_event.m_usType:-1)),
					(bSelf?(pEvent->m_event.m_usStatus|((pEvent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT))?(pEvent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT)):0)|(((pEvent->m_event.m_usDevice==0x0000)&&(!(pEvent->IsNoDeviceType())))?SENTINEL_EVENT_NODEVICE:((pEvent->m_event.m_usDevice&0x00ff)?SENTINEL_EVENT_CUED:0))):(pParent?(pParent->m_event.m_usStatus|((pParent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT))?(pParent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT)):0)|((pParent->m_event.m_usDevice==0x0000)?SENTINEL_EVENT_NODEVICE:((pParent->m_event.m_usDevice&0x00ff)?SENTINEL_EVENT_CUED:0))):-1)),
					(bSelf?(pEvent->m_event.m_ulControl):(pParent?pParent->m_event.m_ulControl:-1)),
					(bSelf?(pEvent->m_dblTime):(pParent?pParent->m_dblTime:-1.0)),
					(bSelf?(pEvent->m_event.m_ulDurationMS):(pParent?pParent->m_event.m_ulDurationMS:-1)),
					(bSelf?(pEvent->m_dblCalcTime):(pParent?pParent->m_dblCalcTime:-1.0)),
					(bSelf?(pEvent->m_nCalcDur):(pParent?pParent->m_nCalcDur:-1)),
					(bSelf?((((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0))):(pParent?(pParent->m_dblCalcTime + (double)pParent->m_nCalcDur/1000.0):-1.0)),
					(bSelf?(pEvent->m_nPosition):(pParent?pParent->m_nPosition:-1))
					);
			}
			else
			{// parent was null - have to put in zee placeholders
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
itemid, \
event_key, \
server_name, \
server_list, \
list_id, \
event_id, \
event_clip, \
event_segment, \
event_title, \
%s\
event_type, \
event_status, \
event_time_mode, \
event_start, \
event_duration, \
event_calc_start, \
event_calc_duration, \
event_calc_end, \
event_position, \
event_last_update, \
parent_uid, \
parent_key, \
parent_id, \
parent_clip, \
parent_segment, \
parent_title, \
parent_type, \
parent_status, \
parent_time_mode, \
parent_start, \
parent_duration, \
parent_calc_start, \
parent_calc_duration, \
parent_calc_end, \
parent_position) VALUES \
(%d, '%s', '%s', %d, %d, '%s', '%s', %d, '%s', %s%s%s%d, %d, %d, %.3f, %d, %.3f, %d, %.3f, %d, %.3f, \
-1, '', '', '', -1, '', -1, -1, -1, -1.000, -1, -1.000, -1, -1.000, -1)",
					g_psentinel->m_settings.m_pszLiveEvents,  // table name
					(pEvent->m_pszEncodedData?"event_data, ":""), //event_data (exists)

	// event fields
					pEvent->m_uid,
					(pEvent->m_pszEncodedKey?pEvent->m_pszEncodedKey:""),
					m_pszServerName, //server_name
					m_nHarrisListID, // server_list - actual 1-based harris list number
					m_nChannelID, // list_id - internal lookup channel number
					(pEvent->m_pszEncodedRecKey?pEvent->m_pszEncodedRecKey:(pEvent->m_event.m_pszReconcileKey?pEvent->m_event.m_pszReconcileKey:"")), //event_id
					(pEvent->m_pszEncodedID?pEvent->m_pszEncodedID:(pEvent->m_event.m_pszID?pEvent->m_event.m_pszID:"")), //event_clip  (must not be null)
					(pEvent->m_event.m_ucSegment==0xff?-1:pEvent->m_event.m_ucSegment),
					(pEvent->m_pszEncodedTitle?pEvent->m_pszEncodedTitle:""), //event_title
					(pEvent->m_pszEncodedData?"'":""), //event_data (exists)
					(pEvent->m_pszEncodedData?pEvent->m_pszEncodedData:""), //event_data  
					(pEvent->m_pszEncodedData?"', ":""), //event_data (exists)
					pEvent->m_event.m_usType, //event_type
					pEvent->m_event.m_usStatus|((pEvent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT))?(pEvent->m_nFlags&(SENTINEL_EVENT_TIMEMASK|SENTINEL_EVENT_LOOKOUT)):0)|(((pEvent->m_event.m_usDevice==0x0000)&&(!(pEvent->IsNoDeviceType())))?SENTINEL_EVENT_NODEVICE:((pEvent->m_event.m_usDevice&0x00ff)?SENTINEL_EVENT_CUED:0)) , //event_status
					pEvent->m_event.m_ulControl, //event_time_mode
					pEvent->m_dblTime, //event_start
					pEvent->m_event.m_ulDurationMS, //event_duration
					pEvent->m_dblCalcTime, //event_calc_start
					pEvent->m_nCalcDur, //event_calc_duration
					(((pEvent->m_event.m_usType&0xff)<SECCOMMENT)?(pEvent->m_dblCalcTime + (double)pEvent->m_nCalcDur/1000.0):(pParent?(pParent->m_dblCalcTime + (double)(pParent->m_nCalcDur)/1000.0):0)),
					pEvent->m_nPosition,
					dblServerTime //server time
					);
			}
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updating %d [%s] built string %d ms", m_pszDesc, index, pEvent->m_event.m_pszID, clock()-nClock); // Sleep(50); //(Dispatch message)
									
if(g_psentinel->m_settings.m_bDebugInsertSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "SentinelChannelThread event insert A: %s", szSQL);  //Sleep(250); //(Dispatch message)
//g_psentinel->m_msgr.DM(MSG_ICONUSER1, NULL, "Sentinel:debug", "before insert -> Event %d", n);//  Sleep(50); //(Dispatch message)

if(!g_psentinel->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_psentinel->m_data.m_critSQL);
			if((!m_bKillChannelThread)&&(!g_bKillThread))
			{
//if(g_psentinel->m_settings.m_bDebugSQL) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, m_pszSource, "SentinelChannelThread insert: %s", szSQL); // Sleep(50); //(Dispatch message)
				if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
				{
					//**MSG
	g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:debug", "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
				}
				else
				{
					pEvent->m_nFlags |= SENTINEL_EVENT_INSERTED;
					nRV = ADC_SUCCESS;
				}
			}
//g_psentinel->m_msgr.DM(MSG_ICONUSER1, NULL, "Sentinel:debug", "after insert -> Event %d", n);//  Sleep(50); //(Dispatch message)
if(!g_psentinel->m_settings.m_bMultiConnectSQL) LeaveCriticalSection(&g_psentinel->m_data.m_critSQL);
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updated %d [%s] %d ms", m_pszDesc, index, pEvent->m_event.m_pszID, clock()-nClock); // Sleep(50); //(Dispatch message)




//AfxMessageBox("updated");

		}


			if(
				  (g_psentinel->m_settings.m_bUseControlModules)
				&&(g_psentinel->m_data.m_bControlModuleServiceThreadStarted)
				&&(
						(g_psentinel->m_settings.m_nCtrlModLookahead<0)
					||((g_psentinel->m_settings.m_nCtrlModLookahead>0)&&(pEvent->m_nPosition<=g_psentinel->m_settings.m_nCtrlModLookahead))
				  )
				)
			{

				if(g_psentinel->m_settings.m_nCtrlModMaxQueue>0) 
				{
					EnterCriticalSection(&g_psentinel->m_data.m_critControlModuleService);
					int nChanges = g_psentinel->m_data.m_nNumChanges;
					LeaveCriticalSection(&g_psentinel->m_data.m_critControlModuleService);

					if(nChanges>=g_psentinel->m_settings.m_nCtrlModMaxQueue)
					{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "discarding change on [%s], queue is full (%d items)", pEvent->m_event.m_pszID, nChanges); //  Sleep(250);//(Dispatch message)

						return nRV;
					}
				}

				CBufferUtil  bu;
#pragma message(alert "here, send a change to send xml to control modules for events")

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "creating event change, update [%s]", pEvent->m_event.m_pszID); //  Sleep(250);//(Dispatch message)

		CSentinelChange* pNewChange =  new CSentinelChange;
		if(pNewChange)
		{
			pNewChange->m_pObj = (void*) new CSentinelEventObject;
			if(pNewChange->m_pObj)
			{
				pNewChange->m_nType = DLLCMD_AITEM;

				CSentinelEventObject* pChgEvent = ((CSentinelEventObject*)(pNewChange->m_pObj));

				*pChgEvent = *(pEvent);
				pChgEvent->m_pszEncodedKey = NULL;
				pChgEvent->m_pszEncodedID = NULL;
				pChgEvent->m_pszEncodedTitle = NULL;
				pChgEvent->m_pszEncodedRecKey = NULL;
				pChgEvent->m_pszEncodedData = NULL;
				pChgEvent->m_event.m_pszID = NULL;
				pChgEvent->m_event.m_pszReconcileKey = NULL;
				pChgEvent->m_event.m_pszTitle = NULL;
				pChgEvent->m_event.m_pszData = NULL;
				pChgEvent->m_event.m_pContextData = NULL;

				pNewChange->m_nChannelID = m_nChannelID;
				pNewChange->m_nHarrisListID = m_nHarrisListID;

				pNewChange->m_pszServerName =  bu.XMLEncode(m_pszServerName);
//				pNewChange->m_pszDesc = bu.XMLEncode(pChannel->m_pszDesc);

				pNewChange->m_pchXMLkey =  bu.XMLEncode(pEvent->m_pszEncodedKey);
				pNewChange->m_pchXMLrec =  bu.XMLEncode(pEvent->m_event.m_pszReconcileKey);
				pNewChange->m_pchXMLclip =  bu.XMLEncode(pEvent->m_event.m_pszID);
				pNewChange->m_pchXMLtitle =  bu.XMLEncode(pEvent->m_event.m_pszTitle);
				pNewChange->m_pchXMLdata =  bu.XMLEncode(pEvent->m_event.m_pszData);

				if(pEvent->m_pParent)
				{
					pNewChange->m_nParentUID = ((CSentinelEventObject*)(pEvent->m_pParent))->m_uid;
				}
				else
				{
					pNewChange->m_nParentUID = -1;
				}


				if(
						(!g_bKillThread) // not kill thread because service module is already shut down.
					&&(_beginthread(SentinelPublishChangeThread, 0, (void*)(pNewChange))==-1)
					)
				{
					//error.

					//**MSG
					g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start publish change thread."); //(Dispatch message)

					try{ delete (pNewChange); } catch(...){}
				}
				
			}
			else
			{
				try{ delete (pNewChange); } catch(...){}
			}
		}


			}

	}
	return nRV;
}



//////////////////////////////////////////////////////////////////////
// CSentinelData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSentinelData::CSentinelData()
{
	m_socketRemote = NULL;
	m_bInRemoteCommand = false;
	m_bSocketMonitorStarted = false;
	m_bMonitorSocket = false;
	m_nRemoteClock = 0;

	InitializeCriticalSection(&m_critText);
	InitializeCriticalSection(&m_critSQL);
	InitializeCriticalSection(&m_critUID);
	InitializeCriticalSection(&m_critDebug);
	InitializeCriticalSection(&m_critChange);
	InitializeCriticalSection(&m_critWorkingSet);
	InitializeCriticalSection(&m_critChannels);
	InitializeCriticalSection(&m_critConns);
	InitializeCriticalSection(&m_critExternalModules);
	InitializeCriticalSection(&m_critControlModuleService);
	InitializeCriticalSection(&m_critAsRun);
	InitializeCriticalSection(&m_critMessaging);
	InitializeCriticalSection(&m_critRemoteChannels);
	InitializeCriticalSection(&m_critVirtualChannels);
	InitializeCriticalSection(&m_critRemoteSocket);
	

	// messaging...
	m_bNetworkMessagingInitialized=false;

	m_bTrafficKillWatch=true;
	m_bTrafficWatchWarningSent=false;
	m_bTrafficWatchInitialized = false;
	m_bTrafficWatchStarted = false;
	m_bCheckModsWarningSent = false;
	m_bCheckMsgsWarningSent = false;
	m_bCheckAsRunWarningSent = false;

	_ftime( &m_timebAutoPurge ); // the last time autopurge was run
	_ftime( &m_timeTrafficWatch );

	m_ppConnObj = NULL;
	m_nNumConnectionObjects = 0;
	m_ppChannelObj = NULL;
	m_nNumChannelObjects = 0;
	m_ppRemoteChannelObj = NULL;
	m_nNumRemoteChannelObjects=0;
	m_ppVirtualChannelObj = NULL;
	m_nNumVirtualChannelObjects = 0;


	m_ppControlObj = NULL;
	m_nNumControlObjects = 0;
	m_ppChanges=NULL;
	m_nNumChanges=0;
	m_nNumChangesArray=0;
	m_nChangesArrayIndex=0;


	m_ppWorkingSet=NULL;
	m_nNumSetIDs=0;
	m_ppWorkingSetChanges=NULL;
	m_nNumSetChanges=0;
	m_nNumSetChangesArray=0;
	m_bWorkingSetThreadStarted = false;
	m_bWorkingSetThreadSuppress = false;
	m_bControlModuleServiceThreadStarted = false;
	m_bKillControlModuleServiceThread = true;

	m_nLastUid=1;

	m_nMaxLicensedChannels=-1;

	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebTick );
	m_pszHost = NULL;			// the name of the host
	m_pszCortexHost = NULL;			// the name of the cortex host
	m_pszCompleteHost = NULL;	// the complete name of the host
	m_usCortexCommandPort = SENTINEL_PORT_CMD;
	m_usCortexStatusPort = SENTINEL_PORT_STATUS;

	m_nSettingsMod = -1;
	m_nChannelsMod = -1;
	m_nConnectionsMod = -1;
	m_nControllersMod = -1;
	m_nRemoteChannelsMod = -1;
	m_nVirtualChannelsMod = -1;

	m_nLastSettingsMod = -1;
	m_nLastChannelsMod = -1;
	m_nLastConnectionsMod = -1;
	m_nLastTrafficFileMod = 1;
	m_nLastTrafficEventsMod = 1;
	m_nLastControllersMod= -1;
	m_nLastRemoteChannelsMod = -1;
	m_nLastVirtualChannelsMod = -1;

	m_pdb = NULL;
	m_pdbConn = NULL;
	m_bQuietKill = false;
	m_bProcessSuspended = false;

	InitializeCriticalSection(&m_critIncrement);

}

CSentinelData::~CSentinelData()
{
	m_bMonitorSocket=false;
	while(m_bSocketMonitorStarted) Sleep(10);

	if(m_socketRemote)
	{
		m_net.CloseConnection(m_socketRemote);
		m_socketRemote = NULL;
	}

	m_bTrafficKillWatch=true;
	while(m_bTrafficWatchStarted) Sleep(1);

	if(m_ppConnObj)
	{
		int i=0;
		while(i<m_nNumConnectionObjects)
		{
			if(m_ppConnObj[i]) delete m_ppConnObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppConnObj; // delete array of pointers to objects, must use new to allocate
	}
	
	EnterCriticalSection(&m_critChannels);

	if(m_ppChannelObj)
	{
		int i=0;
		while(i<m_nNumChannelObjects)
		{
			if(m_ppChannelObj[i]) delete m_ppChannelObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppChannelObj; // delete array of pointers to objects, must use new to allocate
	}
	LeaveCriticalSection(&m_critChannels);

	EnterCriticalSection(&m_critRemoteChannels);

	if(m_ppRemoteChannelObj)
	{
		int i=0;
		while(i<m_nNumRemoteChannelObjects)
		{
			if(m_ppRemoteChannelObj[i]) delete m_ppRemoteChannelObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppRemoteChannelObj; // delete array of pointers to objects, must use new to allocate
	}
	LeaveCriticalSection(&m_critRemoteChannels);

	EnterCriticalSection(&m_critVirtualChannels);

	if(m_ppVirtualChannelObj)
	{
		int i=0;
		while(i<m_nNumVirtualChannelObjects)
		{
			if(m_ppVirtualChannelObj[i]) delete m_ppVirtualChannelObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppVirtualChannelObj; // delete array of pointers to objects, must use new to allocate
	}
	LeaveCriticalSection(&m_critVirtualChannels);

	EnterCriticalSection(&m_critExternalModules);

	if(m_ppControlObj)
	{
		int i=0;
		while(i<m_nNumControlObjects)
		{
			if(m_ppControlObj[i]) delete m_ppControlObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppControlObj; // delete array of pointers to objects, must use new to allocate
	}
	LeaveCriticalSection(&m_critExternalModules);

	EnterCriticalSection(&m_critWorkingSet);
	if(m_ppWorkingSet)
	{
		int i=0;
		while(i<m_nNumSetIDs)
		{
			if(m_ppWorkingSet[i])
			{
				if(m_ppWorkingSet[i]->pszKey) free(m_ppWorkingSet[i]->pszKey);
				delete m_ppWorkingSet[i]; // delete objects, must use new to allocate
			}
			i++;
		}
		delete [] m_ppWorkingSet; // delete array of pointers to objects, must use new to allocate
	}
	if(m_ppWorkingSetChanges)
	{
		int i=0;
		while(i<m_nNumSetChanges)
		{
			if(m_ppWorkingSetChanges[i])
			{
				if(m_ppWorkingSetChanges[i]->pszKey) free(m_ppWorkingSetChanges[i]->pszKey);
				delete m_ppWorkingSetChanges[i]; // delete objects, must use new to allocate
			}
			i++;
		}
		delete [] m_ppWorkingSetChanges; // delete array of pointers to objects, must use new to allocate
	}

	m_ppWorkingSetChanges=NULL;
	m_nNumSetChanges=0;

	m_ppWorkingSet=NULL;
	m_nNumSetIDs=0;

	LeaveCriticalSection(&m_critWorkingSet);

	EnterCriticalSection(&m_critControlModuleService);

	if(m_ppChanges)
	{	
		int i=m_nChangesArrayIndex;
		int j=0;
		while((j<m_nNumChanges)&&(i<m_nNumChangesArray))
		{
			if(m_ppChanges[i]){ try{ delete m_ppChanges[i]; } catch(...){}} // delete objects, must use new to allocate
			i++;
			j++;
		}
		try{delete [] m_ppChanges; } catch(...){}// delete array of pointers to objects, must use new to allocate
	}

	m_ppChanges = NULL;

	m_nNumChanges=0;
	m_nNumChangesArray=0;
	m_nChangesArrayIndex=0;

	LeaveCriticalSection(&m_critControlModuleService);


	if(m_pszHost) free(m_pszHost);	// must use malloc to allocate
	if(m_pszCompleteHost) free(m_pszCompleteHost);	// must use malloc to allocate
	if(m_pszCortexHost) free(m_pszCortexHost);	// must use malloc to allocate

	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critIncrement);
	DeleteCriticalSection(&m_critSQL);
	DeleteCriticalSection(&m_critUID);
	DeleteCriticalSection(&m_critDebug);
	DeleteCriticalSection(&m_critChange);
	DeleteCriticalSection(&m_critWorkingSet);
	DeleteCriticalSection(&m_critChannels);
	DeleteCriticalSection(&m_critConns);
	DeleteCriticalSection(&m_critExternalModules);
	DeleteCriticalSection(&m_critControlModuleService);
	DeleteCriticalSection(&m_critAsRun);
	DeleteCriticalSection(&m_critMessaging);
	DeleteCriticalSection(&m_critRemoteChannels);
	DeleteCriticalSection(&m_critVirtualChannels);
	DeleteCriticalSection(&m_critRemoteSocket);


}

char* CSentinelData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CSentinelData::SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError)
{
	int nRV = CX_ERROR;
	bool bError=false;

	if(!bOverwriteExistingError)
	{
		if(
			  ((m_ulFlags&SENTINEL_ICON_MASK) == SENTINEL_STATUS_ERROR)
			||((m_ulFlags&SENTINEL_STATUS_CMDSVR_MASK) == SENTINEL_STATUS_CMDSVR_ERROR)
			||((m_ulFlags&SENTINEL_STATUS_STATUSSVR_MASK) ==  SENTINEL_STATUS_STATUSSVR_ERROR)
			||((m_ulFlags&SENTINEL_STATUS_THREAD_MASK) == SENTINEL_STATUS_THREAD_ERROR)
			||((m_ulFlags&SENTINEL_STATUS_FAIL_MASK) == SENTINEL_STATUS_FAIL_DB)
			)	bError=true;
	}

	if (ulStatus&SENTINEL_ICON_MASK)
	{
		m_ulFlags &= ~SENTINEL_ICON_MASK;
		m_ulFlags |= (ulStatus&SENTINEL_ICON_MASK);
	}
	if (ulStatus&SENTINEL_STATUS_CMDSVR_MASK)
	{
		m_ulFlags &= ~SENTINEL_STATUS_CMDSVR_MASK;
		m_ulFlags |= (ulStatus&SENTINEL_STATUS_CMDSVR_MASK);
	}
	if (ulStatus&SENTINEL_STATUS_STATUSSVR_MASK)
	{
		m_ulFlags &= ~SENTINEL_STATUS_STATUSSVR_MASK;
		m_ulFlags |= (ulStatus&SENTINEL_STATUS_STATUSSVR_MASK);
	}
	if (ulStatus&SENTINEL_STATUS_THREAD_MASK)
	{
		m_ulFlags &= ~SENTINEL_STATUS_THREAD_MASK;
		m_ulFlags |= (ulStatus&SENTINEL_STATUS_THREAD_MASK);
	}
	if (ulStatus&SENTINEL_STATUS_FAIL_MASK)
	{
		m_ulFlags &= ~SENTINEL_STATUS_FAIL_MASK;
		m_ulFlags |= (ulStatus&SENTINEL_STATUS_FAIL_MASK);
	}

	if(bError)
	{
		m_ulFlags &= ~SENTINEL_ICON_MASK;
		m_ulFlags |= SENTINEL_STATUS_ERROR;
	}

	if((pszText)&&(!bError))
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
//				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
//			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_psentinel->m_settings.m_pszIconPath)&&(strlen(g_psentinel->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_psentinel->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_psentinel->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_psentinel->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_psentinel->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_psentinel->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}

/*int	CSentinelData::SetStatusText(char* pszText, unsigned long ulStatus)
{
	int nRV = CX_ERROR;
	if(pszText)
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_psentinel->m_settings.m_pszIconPath)&&(strlen(g_psentinel->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_psentinel->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_psentinel->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_psentinel->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_psentinel->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_psentinel->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}
	return nRV;
}
*/
// utility
int	CSentinelData::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszCompleteHost) free(m_pszCompleteHost);
						m_pszCompleteHost=pch;

						pch = (char*) malloc(nValue+1);
						if(pch!=NULL)
						{
							if(m_pszHost) free(m_pszHost);
							m_pszHost=pch;

							strcpy(m_pszHost, m_pszCompleteHost);
							pch = strchr(m_pszHost, '.');
							if(pch)
							{
								*pch = 0;
							}

							return SENTINEL_SUCCESS;
						}
					}
				}
			}
		}
	}
	return SENTINEL_ERROR;
}

int CSentinelData::SetLastUID(char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)
		&&(g_psentinel)&&(g_psentinel->m_settings.m_pszExchange)&&(strlen(g_psentinel->m_settings.m_pszExchange)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

	EnterCriticalSection(&g_psentinel->m_data.m_critUID);
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s set mod = %d WHERE criterion = 'UID'",
			g_psentinel->m_settings.m_pszExchange,
			g_psentinel->m_data.m_nLastUid
			);
	LeaveCriticalSection(&g_psentinel->m_data.m_critUID);
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return SENTINEL_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return SENTINEL_ERROR;
}

int CSentinelData::GetLastUID(char* pszInfo)
{
	if((g_psentinel)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		if(g_psentinel->m_settings.m_bUseTraffic)
		{

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT case when maxID >= exchangeUID then maxID else exchangeUID end as nextID from \
(select case when eventItemId >= trafficItemId then eventItemId + 1 else trafficItemId + 1 end as maxID, exchangeUID from (select (select max(itemid) \
from %s.dbo.%s)as eventItemId, (select max(itemid) from %s.dbo.%s) as trafficItemId, (select mod \
from %s.dbo.%s where criterion = 'UID') as exchangeUID) as valueTable) as maxItemID", 
				((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
				((g_psentinel->m_settings.m_pszLiveEvents)&&(strlen(g_psentinel->m_settings.m_pszLiveEvents)))?g_psentinel->m_settings.m_pszLiveEvents:"Events",
				((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
				((g_psentinel->m_settings.m_pszTraffic)&&(strlen(g_psentinel->m_settings.m_pszTraffic)))?g_psentinel->m_settings.m_pszTraffic:"Traffic_Events",
				((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
				((g_psentinel->m_settings.m_pszExchange)&&(strlen(g_psentinel->m_settings.m_pszExchange)))?g_psentinel->m_settings.m_pszExchange:"Exchange"
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT case when eventItemId >= exchangeUID then eventItemId + 1 else exchangeUID end as nextID from \
(select (select max(itemid) from %s.dbo.%s)as eventItemId, (select mod from %s.dbo.%s where criterion = 'UID') as exchangeUID) as maxItemID", 
				((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
				((g_psentinel->m_settings.m_pszLiveEvents)&&(strlen(g_psentinel->m_settings.m_pszLiveEvents)))?g_psentinel->m_settings.m_pszLiveEvents:"Events",
				((g_psentinel->m_settings.m_pszDatabase)&&(strlen(g_psentinel->m_settings.m_pszDatabase)))?g_psentinel->m_settings.m_pszDatabase:"Sentinel",
				((g_psentinel->m_settings.m_pszExchange)&&(strlen(g_psentinel->m_settings.m_pszExchange)))?g_psentinel->m_settings.m_pszExchange:"Exchange"
				);
		}

//		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT mod FROM %s WHERE criterion = 'UID'", 
//			((g_psentinel->m_settings.m_pszExchange)&&(strlen(g_psentinel->m_settings.m_pszExchange)))?g_psentinel->m_settings.m_pszExchange:"Exchange");

		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = SENTINEL_ERROR;
//			int nIndex = 0;
			if (!prs->IsEOF())
			{
				CString szMod;
				try
				{
//					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
//					prs->GetFieldValue("mod", szMod);//HARDCODE
					prs->GetFieldValue("nextID", szMod);//HARDCODE

					nReturn = atoi(szMod);
				}
				catch( ... )
				{
				}

//				nIndex++;
//				prs->MoveNext();
			}
			prs->Close();

			delete prs;
			LeaveCriticalSection(&m_critSQL);
			return nReturn;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return SENTINEL_ERROR;
}


int CSentinelData::GetNewUID()
{
	int nRV;
	EnterCriticalSection(&g_psentinel->m_data.m_critUID);
	nRV = 	g_psentinel->m_data.m_nLastUid;
	if(g_psentinel->m_data.m_nLastUid>=INT_MAX)
		g_psentinel->m_data.m_nLastUid=1; // skip zero on the rollaround
	else
		g_psentinel->m_data.m_nLastUid++;
	LeaveCriticalSection(&g_psentinel->m_data.m_critUID);
	return nRV;
}


int CSentinelData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_psentinel)&&(g_psentinel->m_settings.m_pszExchange)&&(strlen(g_psentinel->m_settings.m_pszExchange)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s set mod = \
(SELECT case when max(mod) is null then 1 when max(mod) >= %d \
then 1 else max(mod) + 1 end from %s WHERE criterion = '%s') WHERE criterion = '%s'",
			g_psentinel->m_settings.m_pszExchange,
			SENTINEL_DB_MOD_MAX,
			g_psentinel->m_settings.m_pszExchange,
			szTemp, szTemp		
			);
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return SENTINEL_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return SENTINEL_ERROR;
}

int CSentinelData::CheckMessages(char* pszInfo)
{
	if((g_psentinel)&&(m_pdbConn)&&(m_pdb)
		&&(g_psentinel->m_settings.m_pszMessages)&&(strlen(g_psentinel->m_settings.m_pszMessages)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_psentinel->m_settings.m_pszMessages)&&(strlen(g_psentinel->m_settings.m_pszMessages)))?g_psentinel->m_settings.m_pszMessages:"Messages",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_psentinel->m_settings.m_nAutoPurgeMessageDays*86400) // local time....
			);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return SENTINEL_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return SENTINEL_ERROR;
}

int CSentinelData::CheckAsRun(char* pszInfo)
{
	if((g_psentinel)&&(m_pdbConn)&&(m_pdb)
		&&(g_psentinel->m_settings.m_pszAsRun)&&(strlen(g_psentinel->m_settings.m_pszAsRun)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_psentinel->m_settings.m_pszAsRun)&&(strlen(g_psentinel->m_settings.m_pszAsRun)))?g_psentinel->m_settings.m_pszAsRun:"AsRun_Log",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_psentinel->m_settings.m_nAutoPurgeAsRunDays*86400) // local time....
			);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return SENTINEL_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return SENTINEL_ERROR;
}




int CSentinelData::CheckDatabaseMods(char* pszInfo)
{
	if((g_psentinel)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_psentinel->m_settings.m_pszExchange)&&(strlen(g_psentinel->m_settings.m_pszExchange)))?g_psentinel->m_settings.m_pszExchange:"Exchange");

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "checkmods SQL %s", szSQL);  //(Dispatch message)

		EnterCriticalSection(&m_critSQL);
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "retrieve checkmods SQL %s", szSQL);  //(Dispatch message)
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "retrieved checkmods%s: %s", (prs==NULL)?" (NULL)":"", pszInfo);  //(Dispatch message)
		if(prs)
		{
			int nReturn = SENTINEL_SUCCESS;
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				CString szCriterion;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
				}

//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:mod_check", "criterion: %s mod:%s  %d searching for [%s]", szCriterion, szMod,
//											 g_psentinel->m_settings.m_bUseControlModules, g_psentinel->m_settings.m_pszExternalModules); // Sleep(50); //(Dispatch message)

				if((g_psentinel->m_settings.m_pszSettings)&&(strlen(g_psentinel->m_settings.m_pszSettings)))
				{
					szTemp.Format("DBT_%s",g_psentinel->m_settings.m_pszSettings);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nSettingsMod = nReturn;
					}
				}

				if(
						(g_psentinel->m_settings.m_bUseControlModules)
					&&(g_psentinel->m_settings.m_pszExternalModules)
					&&(strlen(g_psentinel->m_settings.m_pszExternalModules))
					)
				{

					szTemp.Format("DBT_%s",g_psentinel->m_settings.m_pszExternalModules);
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:mod_check", "%d checking [%s] against [%s]", 
//											 g_psentinel->m_settings.m_bUseControlModules, g_psentinel->m_settings.m_pszExternalModules,szTemp); // Sleep(50); //(Dispatch message)
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nControllersMod = nReturn;
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:mod_check", "assigned [%s] %d %d", szTemp,m_nControllersMod, nReturn ); // Sleep(50); //(Dispatch message)
					}
				}

				if((g_psentinel->m_settings.m_pszConnections)&&(strlen(g_psentinel->m_settings.m_pszConnections)))
				{
					szTemp.Format("DBT_%s",g_psentinel->m_settings.m_pszConnections);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nConnectionsMod = nReturn;
					}
				}

				if((g_psentinel->m_settings.m_pszChannels)&&(strlen(g_psentinel->m_settings.m_pszChannels)))
				{
					szTemp.Format("DBT_%s",g_psentinel->m_settings.m_pszChannels);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nChannelsMod = nReturn;
					}
				}

				if((g_psentinel->m_settings.m_pszRemoteChannels)&&(strlen(g_psentinel->m_settings.m_pszRemoteChannels)))
				{
					szTemp.Format("DBT_%s",g_psentinel->m_settings.m_pszRemoteChannels);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nLastRemoteChannelsMod = nReturn;
					}
				}

				if((g_psentinel->m_settings.m_pszVirtualMappingView)&&(strlen(g_psentinel->m_settings.m_pszVirtualMappingView)))
				{
					szTemp.Format("DBT_%s",g_psentinel->m_settings.m_pszVirtualMappingView);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nLastVirtualChannelsMod = nReturn;
					}
				}

				// get the suspend

				szTemp.Format("Suspend");
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					nReturn = atoi(szMod);
					if(nReturn>0)
					{
						if(!m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Sentinel is suspended.");  
							g_psentinel->m_data.SetStatusText(errorstring, CX_STATUS_UNKNOWN);
							g_psentinel->m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:suspend", "*** Sentinel has been suspended. ***"); // Sleep(50); //(Dispatch message)
							g_psentinel->SendMsg(CX_SENDMSG_INFO, "Sentinel:suspend", "Sentinel has been suspended");
							m_bProcessSuspended = true;
						}
					}
					else
					{
						if(m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Sentinel is running.");  
							g_psentinel->m_data.SetStatusText(errorstring, CX_STATUS_OK);
							g_psentinel->m_msgr.DM(MSG_ICONNONE, NULL, "Sentinel:resume", "*** Sentinel has been resumed. ***");//  Sleep(50); //(Dispatch message)
							g_psentinel->SendMsg(CX_SENDMSG_INFO, "Sentinel:resume", "Sentinel has been resumed.");
							m_bProcessSuspended = false;

							g_psentinel->m_data.GetConnections();  // re-initializes connection threads if necessary.
						}
					}
				}

				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			delete prs;
			LeaveCriticalSection(&m_critSQL);
			return nReturn;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return SENTINEL_ERROR;
}

int CSentinelData::SetThreads(char* pszInfo)
{
	if(g_psentinel==NULL) return SENTINEL_ERROR;
	int nReturn = SENTINEL_SUCCESS;

//	g_dir.m_pWatchThread = SentinelTrafficWatchThread;

//AfxMessageBox("setting this up");
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_TRAFFIC) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "watch: %d %d %d",(g_psentinel),(g_psentinel->m_settings.m_bUseTraffic),(g_psentinel->m_data.m_bTrafficWatchStarted));  //(Dispatch message)

			if((g_psentinel->m_settings.m_bUseTraffic)&&(!(g_psentinel->m_data.m_bTrafficWatchStarted))&&(!g_bKillThread))
			{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "we really arent watching yet in settings, beginthread SentinelTrafficWatchThread");  //(Dispatch message)
				g_psentinel->m_data.m_bTrafficKillWatch = false;
				if(_beginthread(SentinelTrafficWatchThread, 0, (void*)NULL)==-1)
				{
					//error.

					//**MSG
					g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start traffic folder watch thread."); //(Dispatch message)

				}
				else
				{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "we arent watching yet in settings");  //(Dispatch message)
					while((!g_bKillThread)&&(!g_psentinel->m_data.m_bTrafficWatchStarted))
					{

//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%d %d %d",(!g_bKillThread),(!g_psentinel->m_data.m_bTrafficWatchStarted), clock());  //(Dispatch message)
//CString foo; foo.Format("%d %d %d",(!g_bKillThread),(!g_psentinel->m_data.m_bTrafficWatchStarted), clock()); AfxMessageBox(foo);
						Sleep(1);
					}
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "we are watching");  //(Dispatch message)
				}
			}
			if((!g_psentinel->m_settings.m_bUseTraffic)||(g_bKillThread))
			{
				g_psentinel->m_data.m_bTrafficKillWatch = true;
				while((!g_bKillThread)&&(g_psentinel->m_data.m_bTrafficWatchStarted))
				{
					Sleep(1);
				}
			}
//AfxMessageBox("out");

if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_WORKINGSET) g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "working set: %d %d %d",(g_psentinel),(g_psentinel->m_settings.m_bUseWorkingSet),(g_psentinel->m_data.m_bWorkingSetThreadStarted));  //(Dispatch message)
			if((g_psentinel->m_settings.m_bUseWorkingSet)&&(!g_psentinel->m_data.m_bWorkingSetThreadStarted)&&(!g_bKillThread))
			{
				if(_beginthread(SentinelWorkingSetMaintainThread, 0, (void*)NULL)==-1)
				{
					//error.

					//**MSG
					g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start working set maintenance thread."); //(Dispatch message)

				}
				else
				{
					while((!g_bKillThread)&&(!g_psentinel->m_data.m_bWorkingSetThreadStarted))
					{
						Sleep(1);
					}
				}
			}


			if((g_psentinel->m_settings.m_bUseControlModules)&&(!g_bKillThread))
			{
				if(!g_psentinel->m_data.m_bControlModuleServiceThreadStarted)
				{
					g_psentinel->m_data.m_bKillControlModuleServiceThread =false;

					if(_beginthread(SentinelControlModuleServiceThread, 0, (void*)NULL)==-1)
					{
						//error.

						//**MSG
						g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "Could not start control module service thread."); //(Dispatch message)

					}
					else
					{
						while((!g_bKillThread)&&(!g_psentinel->m_data.m_bControlModuleServiceThreadStarted))
						{
							Sleep(1);
						}
					}

				}
			}
			else
			{
				g_psentinel->m_data.m_bKillControlModuleServiceThread =true;
			}


	return nReturn;
}

int CSentinelData::GetRemoteChannels(char* pszInfo)
{
	if((g_psentinel)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY server, listid", 
			((g_psentinel->m_settings.m_pszRemoteChannels)&&(strlen(g_psentinel->m_settings.m_pszRemoteChannels)))?g_psentinel->m_settings.m_pszRemoteChannels:"RemoteChannels");
		
		EnterCriticalSection(&m_critSQL);
		EnterCriticalSection(&m_critRemoteChannels);

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = SENTINEL_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szServer = "";
				CString szDesc = "";
				CString szTemp = "";
				unsigned long ulFlags;   // various flags
				int nListID = -1;
				int nChannelID = -1;
				int nTemp;
				bool bFound = false;
				bool bFlagsFound = false;
				try
				{
					prs->GetFieldValue("ID", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nChannelID = nTemp;
					}
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bFlagsFound = true;
						ulFlags = atol(szTemp);
					}
					prs->GetFieldValue("description", szDesc);//HARDCODE
					prs->GetFieldValue("server", szServer);//HARDCODE
					szServer.TrimLeft(); szServer.TrimRight();
					// **** if servername is >16 chars, need to truncate and flag error somewhere.  message?

					prs->GetFieldValue("listid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nListID = nTemp;
					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				if((g_psentinel->m_data.m_ppRemoteChannelObj)&&(g_psentinel->m_data.m_nNumRemoteChannelObjects))
				{
					nTemp=0;
					while(nTemp<g_psentinel->m_data.m_nNumRemoteChannelObjects)
					{
						if((g_psentinel->m_data.m_ppRemoteChannelObj[nTemp])&&(g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_pszServerName))
						{
							if(
									(szServer.GetLength()>0)
								&&(szServer.CompareNoCase(g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_pszServerName)==0)
								&&(nListID==g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_nHarrisListID)
								)
							{
								bFound = true;
								// override with the new changes:
								if(nChannelID>=0) g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_nChannelID = nChannelID;

								if(
										(szDesc.GetLength()>0)
									&&((g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_pszDesc==NULL)||(szDesc.CompareNoCase(g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_pszDesc)))
									)
								{
									if(g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_pszDesc) free(g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_pszDesc);

									g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_pszDesc) sprintf(g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_pszDesc, szDesc);
								}

								if((bFlagsFound)&&(ulFlags&SENTINEL_FLAG_ENABLED)&&(!((g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_ulFlags)&SENTINEL_FLAG_ENABLED)))
								{

// ****  do something here to check out if the list is able to be activated, that is, if it's on a connected connection

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating remote list: %s...", 
										g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_pszDesc?g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_pszDesc:g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_pszServerName);  
									g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:remote_destination_change", errorstring); // Sleep(20);  //(Dispatch message)

// **** if it's able to be activated, set the status to connected.
//XX									g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_ulStatus = SENTINEL_STATUS_CONN;
								}
								

								if((bFlagsFound)&&(!(ulFlags&SENTINEL_FLAG_ENABLED))&&((g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_ulFlags)&SENTINEL_FLAG_ENABLED))
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating remote list: %s...", 
										g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_pszDesc?g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_pszDesc:g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_pszServerName);  
									g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:remote_destination_change", errorstring); // Sleep(20);     //(Dispatch message)

//XX									g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_ulStatus = SENTINEL_STATUS_NOTCON;
								}

								// and set up the flags
								if(bFlagsFound) g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_ulFlags = ulFlags|SENTINEL_FLAG_FOUND;
								else g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]->m_ulFlags |= SENTINEL_FLAG_FOUND;
							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szServer.GetLength()>0)&&(nListID>0)) // have to add.
				{

					CSentinelChannelObject* pscho = new CSentinelChannelObject;
					if(pscho)
					{
						CSentinelChannelObject** ppObj = new CSentinelChannelObject*[g_psentinel->m_data.m_nNumRemoteChannelObjects+1];
						if(ppObj)
						{
							int o=0;
							if((g_psentinel->m_data.m_ppRemoteChannelObj)&&(g_psentinel->m_data.m_nNumRemoteChannelObjects>0))
							{
								while(o<g_psentinel->m_data.m_nNumRemoteChannelObjects)
								{
									ppObj[o] = g_psentinel->m_data.m_ppRemoteChannelObj[o];
									o++;
								}
								try{delete [] g_psentinel->m_data.m_ppRemoteChannelObj;} catch(...){}

							}
							ppObj[g_psentinel->m_data.m_nNumRemoteChannelObjects] = pscho;
							g_psentinel->m_data.m_ppRemoteChannelObj = ppObj;
							g_psentinel->m_data.m_nNumRemoteChannelObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szServer.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, szServer);

							ppObj[o]->m_nHarrisListID = nListID;

							if(nChannelID>=0) ppObj[o]->m_nChannelID = nChannelID;

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, szDesc);
							}
//							ppObj[o]->m_usType = not used;
							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|SENTINEL_FLAG_FOUND;
							else ppObj[o]->m_ulFlags |= SENTINEL_FLAG_FOUND;
								

// ****  do something here to check out if the list is able to be activated, that is, if it's on a connected connection
							if((ppObj[o]->m_ulFlags)&SENTINEL_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating remote list: %s...", 
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName);  
								g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:remote_destination_change", errorstring);//    Sleep(20);   //(Dispatch message)

// **** if it's able to be activated, set the status to connected.
//XX								ppObj[o]->m_ulStatus = SENTINEL_STATUS_CONN;
							}

						}
						else
						{
							try{delete pscho;} catch(...){}
						}
					}
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();
			LeaveCriticalSection(&m_critSQL);

			try{delete prs;} catch(...){}

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<g_psentinel->m_data.m_nNumRemoteChannelObjects)
			{
				if((g_psentinel->m_data.m_ppRemoteChannelObj)&&(g_psentinel->m_data.m_ppRemoteChannelObj[nIndex]))
				{
					if((g_psentinel->m_data.m_ppRemoteChannelObj[nIndex]->m_ulFlags)&SENTINEL_FLAG_FOUND)
					{
						(g_psentinel->m_data.m_ppRemoteChannelObj[nIndex]->m_ulFlags) &= ~SENTINEL_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(g_psentinel->m_data.m_ppRemoteChannelObj[nIndex])
						{
							if((g_psentinel->m_data.m_ppRemoteChannelObj[nIndex]->m_ulFlags)&SENTINEL_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating remote list: %s...", 
									g_psentinel->m_data.m_ppRemoteChannelObj[nIndex]->m_pszDesc?g_psentinel->m_data.m_ppRemoteChannelObj[nIndex]->m_pszDesc:g_psentinel->m_data.m_ppRemoteChannelObj[nIndex]->m_pszServerName);  
								g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:remote_destination_remove", errorstring);  //   Sleep(20);  //(Dispatch message)
//XX								g_psentinel->m_data.m_ppRemoteChannelObj[nIndex]->m_ulStatus = SENTINEL_STATUS_NOTCON;
							}

							g_psentinel->m_data.m_ppRemoteChannelObj[nIndex]->m_bKillChannelThread = true;
							while(g_psentinel->m_data.m_ppRemoteChannelObj[nIndex]->m_bChannelThreadStarted) Sleep(1);

							try{delete g_psentinel->m_data.m_ppRemoteChannelObj[nIndex];} catch(...){}
							g_psentinel->m_data.m_nNumRemoteChannelObjects--;

							int nTemp=nIndex;
							while(nTemp<g_psentinel->m_data.m_nNumRemoteChannelObjects)
							{
								g_psentinel->m_data.m_ppRemoteChannelObj[nTemp]=g_psentinel->m_data.m_ppRemoteChannelObj[nTemp+1];
								nTemp++;
							}
							g_psentinel->m_data.m_ppRemoteChannelObj[nTemp] = NULL;
						} else nIndex++;
					}
				}
				else
					nIndex++;
			}

//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "GetRemoteChannels returning");  Sleep(250); //(Dispatch message)
			LeaveCriticalSection(&m_critRemoteChannels);
			return nReturn;
		}
		LeaveCriticalSection(&m_critRemoteChannels);
		LeaveCriticalSection(&m_critSQL);

	}
	return SENTINEL_ERROR;
}

int CSentinelData::GetVirtualChannels(char* pszInfo)
{
	if((g_psentinel)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, publicID, description, active, channelID, local_channel FROM %s WHERE (active_channel <> 0) ORDER BY ID", 
			((g_psentinel->m_settings.m_pszVirtualMappingView)&&(strlen(g_psentinel->m_settings.m_pszVirtualMappingView)))?g_psentinel->m_settings.m_pszVirtualMappingView:"VirtualMappingView");
		

//SELECT     ID, publicID, description, active, virtualID, channelID, local_channel, active_channel, status
//FROM         VirtualMappingView
//WHERE     (active_channel = 1)

		EnterCriticalSection(&m_critSQL);
		EnterCriticalSection(&m_critVirtualChannels);

		CSentinelVirtualChannelObject** ppVirtObj = NULL;
		int nNumVirts = 0;


		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = SENTINEL_ERROR;
			int nIndex = 0;
			bool bError=false;
			while (!prs->IsEOF())
			{
				CString szDesc = "";
				CString szTemp = "";
				unsigned long ulFlags=0;   // various flags
				unsigned short usType=0;
				int nID = -1;
				int nChannelID = -1;
				int nMapID = -1;     // actually the 
				int nTemp;
				try
				{
					prs->GetFieldValue("ID", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nID = nTemp;
					}
					prs->GetFieldValue("publicID", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nChannelID = nTemp;
					}
					
					prs->GetFieldValue("description", szDesc);//HARDCODE

					prs->GetFieldValue("active", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulFlags = atol(szTemp);
					}

					prs->GetFieldValue("channelID", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nMapID = nTemp;
					}
					prs->GetFieldValue("local_channel", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						usType = (unsigned short) atol(szTemp);
					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					bError=true;
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
					bError=true;
				}



				CSentinelVirtualChannelObject* pscho = new CSentinelVirtualChannelObject;
				if(pscho)
				{
					CSentinelVirtualChannelObject** ppObj = new CSentinelVirtualChannelObject*[g_psentinel->m_data.m_nNumVirtualChannelObjects+1];
					if(ppObj)
					{
						int o=0;

						if((ppVirtObj)&&(nNumVirts>0))
						{
							while(o<nNumVirts)
							{
								ppObj[o] = ppVirtObj[o];
								o++;
							}
							try{delete [] ppVirtObj;} catch(...){}

						}
						ppObj[nNumVirts] = pscho;
						ppVirtObj = ppObj;
						nNumVirts++;

						ppObj[o]->m_ulFlags = ulFlags;
						ppObj[o]->m_usType = usType;
						ppObj[o]->m_nID = nID;
						ppObj[o]->m_nChannelID = nChannelID;
						ppObj[o]->m_nMapID = nMapID;

						if(szDesc.GetLength()>0)
						{
							ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
							if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, szDesc);
						}
						
// ****  do something here to check out if the list is able to be activated, that is, if it's on a connected connection
						if((ppObj[o]->m_ulFlags)&SENTINEL_FLAG_ENABLED)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating virtual list: %s, ID %d, mapping to %s %d", 
								ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:"(N/A)",
								ppObj[o]->m_nChannelID, 
								(ppObj[o]->m_usType == SENTINEL_TYPE_REMOTE)?"remote":"local",
								ppObj[o]->m_nMapID
								);  
							g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:virtual_destinations", errorstring);//    Sleep(20);   //(Dispatch message)

// **** if it's able to be activated, set the status to connected.
//XX								ppObj[o]->m_ulStatus = SENTINEL_STATUS_CONN;
						}

					}
					else
					{
						bError=true;

						try{delete pscho;} catch(...){}
					}
				}  //else error...
				else 					
					bError=true;

				

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();
			LeaveCriticalSection(&m_critSQL);

			try{delete prs;} catch(...){}

			CSentinelVirtualChannelObject** ppTempVirtObj=NULL;
			int nTempNumVirts =0;
			if(!bError)
			{
				ppTempVirtObj = g_psentinel->m_data.m_ppVirtualChannelObj;
				nTempNumVirts = g_psentinel->m_data.m_nNumVirtualChannelObjects;
				g_psentinel->m_data.m_ppVirtualChannelObj = ppVirtObj;
				g_psentinel->m_data.m_nNumVirtualChannelObjects =  nNumVirts;
			}

//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "GetVirtualChannels returning");  Sleep(250); //(Dispatch message)
			LeaveCriticalSection(&m_critVirtualChannels);

			if(!bError)
			{
				if(ppTempVirtObj)
				{
					int i=0;
					while(i<nTempNumVirts)
					{
						if(ppTempVirtObj[i]) delete ppTempVirtObj[i]; // delete objects, must use new to allocate
						i++;
					}
					delete [] ppTempVirtObj; // delete array of pointers to objects, must use new to allocate
				}
			}

			return nReturn;
		}
		LeaveCriticalSection(&m_critVirtualChannels);
		LeaveCriticalSection(&m_critSQL);

	}
	return SENTINEL_ERROR;
}


int CSentinelData::GetChannels(char* pszInfo)
{
	if((g_psentinel)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY server, listid", 
			((g_psentinel->m_settings.m_pszChannels)&&(strlen(g_psentinel->m_settings.m_pszChannels)))?g_psentinel->m_settings.m_pszChannels:"Channels");
		
		EnterCriticalSection(&m_critSQL);
		EnterCriticalSection(&m_critChannels);

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = SENTINEL_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szServer = "";
				CString szDesc = "";
				CString szTemp = "";
				unsigned long ulFlags;   // various flags
				int nListID = -1;
				int nChannelID = -1;
				int nTemp;
				bool bFound = false;
				bool bFlagsFound = false;
				try
				{
					prs->GetFieldValue("ID", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nChannelID = nTemp;
					}
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bFlagsFound = true;
						ulFlags = atol(szTemp);
					}
					prs->GetFieldValue("description", szDesc);//HARDCODE
					prs->GetFieldValue("server", szServer);//HARDCODE
					szServer.TrimLeft(); szServer.TrimRight();
					// **** if servername is >16 chars, need to truncate and flag error somewhere.  message?

					prs->GetFieldValue("listid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nListID = nTemp;
					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				if((g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_nNumChannelObjects))
				{
					nTemp=0;
					while(nTemp<g_psentinel->m_data.m_nNumChannelObjects)
					{
						if((g_psentinel->m_data.m_ppChannelObj[nTemp])&&(g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszServerName))
						{
							if(
									(szServer.GetLength()>0)
								&&(szServer.CompareNoCase(g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszServerName)==0)
								&&(nListID==g_psentinel->m_data.m_ppChannelObj[nTemp]->m_nHarrisListID)
								)
							{
								bFound = true;
								// override with the new changes:
								if(nChannelID>=0) g_psentinel->m_data.m_ppChannelObj[nTemp]->m_nChannelID = nChannelID;

								if(
										(szDesc.GetLength()>0)
									&&((g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszDesc==NULL)||(szDesc.CompareNoCase(g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszDesc)))
									)
								{
									if(g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszDesc) free(g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszDesc);

									g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszDesc) sprintf(g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszDesc, szDesc);
								}

								if((bFlagsFound)&&(ulFlags&SENTINEL_FLAG_ENABLED)&&(!((g_psentinel->m_data.m_ppChannelObj[nTemp]->m_ulFlags)&SENTINEL_FLAG_ENABLED)))
								{

// ****  do something here to check out if the list is able to be activated, that is, if it's on a connected connection

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating list: %s...", 
										g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszDesc?g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszDesc:g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszServerName);  
									g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:destination_change", errorstring); // Sleep(20);  //(Dispatch message)

// **** if it's able to be activated, set the status to connected.
//XX									g_psentinel->m_data.m_ppChannelObj[nTemp]->m_ulStatus = SENTINEL_STATUS_CONN;
								}
								

								if((bFlagsFound)&&(!(ulFlags&SENTINEL_FLAG_ENABLED))&&((g_psentinel->m_data.m_ppChannelObj[nTemp]->m_ulFlags)&SENTINEL_FLAG_ENABLED))
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating list: %s...", 
										g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszDesc?g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszDesc:g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszServerName);  
									g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:destination_change", errorstring); // Sleep(20);     //(Dispatch message)

//XX									g_psentinel->m_data.m_ppChannelObj[nTemp]->m_ulStatus = SENTINEL_STATUS_NOTCON;
								}

								// and set up the flags
								if(bFlagsFound) g_psentinel->m_data.m_ppChannelObj[nTemp]->m_ulFlags = ulFlags|SENTINEL_FLAG_FOUND;
								else g_psentinel->m_data.m_ppChannelObj[nTemp]->m_ulFlags |= SENTINEL_FLAG_FOUND;
							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szServer.GetLength()>0)&&(nListID>0)) // have to add.
				{

					CSentinelChannelObject* pscho = new CSentinelChannelObject;
					if(pscho)
					{
						CSentinelChannelObject** ppObj = new CSentinelChannelObject*[g_psentinel->m_data.m_nNumChannelObjects+1];
						if(ppObj)
						{
							int o=0;
							if((g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_nNumChannelObjects>0))
							{
								while(o<g_psentinel->m_data.m_nNumChannelObjects)
								{
									ppObj[o] = g_psentinel->m_data.m_ppChannelObj[o];
									o++;
								}
								try{delete [] g_psentinel->m_data.m_ppChannelObj;} catch(...){}

							}
							ppObj[g_psentinel->m_data.m_nNumChannelObjects] = pscho;
							g_psentinel->m_data.m_ppChannelObj = ppObj;
							g_psentinel->m_data.m_nNumChannelObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szServer.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, szServer);

							ppObj[o]->m_nHarrisListID = nListID;

							if(nChannelID>=0) ppObj[o]->m_nChannelID = nChannelID;

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, szDesc);
							}
//							ppObj[o]->m_usType = not used;
							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|SENTINEL_FLAG_FOUND;
							else ppObj[o]->m_ulFlags |= SENTINEL_FLAG_FOUND;
								

// ****  do something here to check out if the list is able to be activated, that is, if it's on a connected connection
							if((ppObj[o]->m_ulFlags)&SENTINEL_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating list: %s...", 
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName);  
								g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:destination_change", errorstring);//    Sleep(20);   //(Dispatch message)

// **** if it's able to be activated, set the status to connected.
//XX								ppObj[o]->m_ulStatus = SENTINEL_STATUS_CONN;
							}

						}
						else
						{
							try{delete pscho;} catch(...){}
						}
					}
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();
			LeaveCriticalSection(&m_critSQL);

			try{delete prs;} catch(...){}

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<g_psentinel->m_data.m_nNumChannelObjects)
			{
				if((g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_ppChannelObj[nIndex]))
				{
					if((g_psentinel->m_data.m_ppChannelObj[nIndex]->m_ulFlags)&SENTINEL_FLAG_FOUND)
					{
						(g_psentinel->m_data.m_ppChannelObj[nIndex]->m_ulFlags) &= ~SENTINEL_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(g_psentinel->m_data.m_ppChannelObj[nIndex])
						{
							if((g_psentinel->m_data.m_ppChannelObj[nIndex]->m_ulFlags)&SENTINEL_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating list: %s...", 
									g_psentinel->m_data.m_ppChannelObj[nIndex]->m_pszDesc?g_psentinel->m_data.m_ppChannelObj[nIndex]->m_pszDesc:g_psentinel->m_data.m_ppChannelObj[nIndex]->m_pszServerName);  
								g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:destination_remove", errorstring);  //   Sleep(20);  //(Dispatch message)
//XX								g_psentinel->m_data.m_ppChannelObj[nIndex]->m_ulStatus = SENTINEL_STATUS_NOTCON;
							}

							g_psentinel->m_data.m_ppChannelObj[nIndex]->m_bKillChannelThread = true;
							while(g_psentinel->m_data.m_ppChannelObj[nIndex]->m_bChannelThreadStarted) Sleep(1);

							try{delete g_psentinel->m_data.m_ppChannelObj[nIndex];} catch(...){}
							g_psentinel->m_data.m_nNumChannelObjects--;

							int nTemp=nIndex;
							while(nTemp<g_psentinel->m_data.m_nNumChannelObjects)
							{
								g_psentinel->m_data.m_ppChannelObj[nTemp]=g_psentinel->m_data.m_ppChannelObj[nTemp+1];
								nTemp++;
							}
							g_psentinel->m_data.m_ppChannelObj[nTemp] = NULL;
						} else nIndex++;
					}
				}
				else
					nIndex++;
			}

//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "GetChannels returning");  Sleep(250); //(Dispatch message)
			LeaveCriticalSection(&m_critChannels);
			return nReturn;
		}
		LeaveCriticalSection(&m_critChannels);
		LeaveCriticalSection(&m_critSQL);

	}
	return SENTINEL_ERROR;
}

int CSentinelData::ReturnConnectionIndex(char* pchServer)
{
	EnterCriticalSection(&m_critConns);
	if((g_psentinel)&&(pchServer)&&(strlen(pchServer)>0)
		&&(g_psentinel->m_data.m_ppConnObj)&&(g_psentinel->m_data.m_nNumConnectionObjects))
	{
		int nTemp=0;
		while(nTemp<g_psentinel->m_data.m_nNumConnectionObjects)
		{
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "c2");  Sleep(250); //(Dispatch message)
			if((g_psentinel->m_data.m_ppConnObj[nTemp])&&(g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszServerName))
			{
				if(strcmp(g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszServerName, pchServer)==0)
				{
					LeaveCriticalSection(&m_critConns);
					return nTemp;
				}
			}
			nTemp++;
		}
	}
	LeaveCriticalSection(&m_critConns);
	return SENTINEL_ERROR;
}

int CSentinelData::ReturnChannelIndex(char* pchServer, int nList)
{
	EnterCriticalSection(&m_critChannels);
	if((g_psentinel)&&(pchServer)&&(strlen(pchServer)>0)&&(nList>0)&&(nList<=MAXSYSLISTS)
		&&(g_psentinel->m_data.m_ppChannelObj)&&(g_psentinel->m_data.m_nNumChannelObjects))
	{
		int nTemp=0;
		while(nTemp<g_psentinel->m_data.m_nNumChannelObjects)
		{
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "c2");  Sleep(250); //(Dispatch message)
			if((g_psentinel->m_data.m_ppChannelObj[nTemp])&&(g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszServerName))
			{
				if(
					  (strcmp(g_psentinel->m_data.m_ppChannelObj[nTemp]->m_pszServerName, pchServer)==0)
					&&(nList == g_psentinel->m_data.m_ppChannelObj[nTemp]->m_nHarrisListID)
					)
				{
					LeaveCriticalSection(&m_critChannels);
					return nTemp;
				}

			}
			nTemp++;
		}
	}
	LeaveCriticalSection(&m_critChannels);
	return SENTINEL_ERROR;
}


int CSentinelData::GetConnections(char* pszInfo)
{
	if((g_psentinel)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY server",  //HARDCODE
			((g_psentinel->m_settings.m_pszConnections)&&(strlen(g_psentinel->m_settings.m_pszConnections)))?g_psentinel->m_settings.m_pszConnections:"Connections");
//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "GetConnections");  Sleep(250); //(Dispatch message)

		EnterCriticalSection(&m_critSQL);
		EnterCriticalSection(&m_critConns);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = SENTINEL_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szHost="";
				CString szClient="";
				CString szDesc="";
				CString szTemp;
				unsigned long ulFlags;   // various flags
			//	unsigned short usType;
				int nTemp = -1;
				bool bFlagsFound = false;
				bool bFound = false;
				try
				{
					prs->GetFieldValue("server", szHost);//HARDCODE
					szHost.TrimLeft(); szHost.TrimRight();
					prs->GetFieldValue("description", szDesc);//HARDCODE
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulFlags = atol(szTemp);
						bFlagsFound  =true;
					}
					prs->GetFieldValue("client", szClient);//HARDCODE
					szClient.TrimLeft(); szClient.TrimRight();
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				if((g_psentinel->m_data.m_ppConnObj)&&(g_psentinel->m_data.m_nNumConnectionObjects))
				{
					nTemp=0;
					while(nTemp<g_psentinel->m_data.m_nNumConnectionObjects)
					{
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "c2");  Sleep(250); //(Dispatch message)
						if((g_psentinel->m_data.m_ppConnObj[nTemp])&&(g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszServerName))
						{
							if((szHost.GetLength()>0)&&(szHost.CompareNoCase(g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszServerName)==0))
							{
								bFound = true;
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "c3");  Sleep(250); //(Dispatch message)

								if(
										((bFlagsFound)&&(!(ulFlags&SENTINEL_FLAG_ENABLED))&&((g_psentinel->m_data.m_ppConnObj[nTemp]->m_ulFlags)&SENTINEL_FLAG_ENABLED))
									||(
												(szClient.GetLength()>0)&&((g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszClientName==NULL)||(szClient.CompareNoCase(g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszClientName))) // different client name
											&&(
													((bFlagsFound)&&(ulFlags&SENTINEL_FLAG_ENABLED))
												||((!bFlagsFound)&&((g_psentinel->m_data.m_ppConnObj[nTemp]->m_ulFlags)&SENTINEL_FLAG_ENABLED))
												)
										)
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", 
										g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszServerName);  
									g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:destination_change", errorstring); // Sleep(100);  //(Dispatch message)
//									g_psentinel->m_data.m_ppConnObj[nTemp]->m_pDlg->OnDisconnect();
									//**** disconnect

//									g_adc.DisconnectServer(g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszServerName);
									g_psentinel->m_data.m_ppConnObj[nTemp]->m_bKillConnThread = true;  // let it finish on its own..
									//**** should check return value....
	//								while((g_psentinel->m_data.m_ppConnObj[nTemp])&&(g_psentinel->m_data.m_ppConnObj[nTemp]->m_bConnThreadStarted)) Sleep(100);
						//			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s is disconnected.", 
						//				g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszServerName);  
						//			g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:destination_change", errorstring);  Sleep(100);  //(Dispatch message)

//									g_psentinel->m_data.m_ppConnObj[nTemp]->m_ulStatus = SENTINEL_STATUS_NOTCON;
								}

								if(
										((bFlagsFound)&&(ulFlags&SENTINEL_FLAG_ENABLED)&&((!((g_psentinel->m_data.m_ppConnObj[nTemp]->m_ulFlags)&SENTINEL_FLAG_ENABLED))||(!g_psentinel->m_data.m_ppConnObj[nTemp]->m_bConnThreadStarted)))
									||(
												(szClient.GetLength()>0)&&((g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszClientName==NULL)||(szClient.CompareNoCase(g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszClientName))) // different client name
											&&(
													((bFlagsFound)&&(ulFlags&SENTINEL_FLAG_ENABLED))
												||((!bFlagsFound)&&((g_psentinel->m_data.m_ppConnObj[nTemp]->m_ulFlags)&SENTINEL_FLAG_ENABLED))
												)
										)
									)
								{
									

// cant connect again until the last thread has finished
									g_psentinel->m_data.m_ppConnObj[nTemp]->m_bKillConnThread = true;
									while((g_psentinel->m_data.m_ppConnObj[nTemp])&&(g_psentinel->m_data.m_ppConnObj[nTemp]->m_bConnThreadStarted)) Sleep(100);

									if(!((g_psentinel->m_settings.m_bDisconnectOnSuspend)&&(g_psentinel->m_data.m_bProcessSuspended)))
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s to %s...", 
											g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszClientName?g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszClientName:"Sentinel",  
											g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszServerName);  
										g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:destination_change", errorstring); //  Sleep(100); //(Dispatch message)
	//									g_psentinel->m_data.m_ppConnObj[nTemp]->m_pDlg->OnButtonConnect();
										//**** if connect set following status
	//XX									g_psentinel->m_data.m_ppConnObj[nTemp]->m_ulStatus = SENTINEL_STATUS_NOTCON;  // let thread set this

										g_psentinel->m_data.m_ppConnObj[nTemp]->m_bKillConnThread = false;
	EnterCriticalSection(&g_psentinel->m_data.m_ppConnObj[nTemp]->m_critAPI);
										g_psentinel->m_data.m_ppConnObj[nTemp]->m_pAPIConn = NULL;
	LeaveCriticalSection(&g_psentinel->m_data.m_ppConnObj[nTemp]->m_critAPI);
										if(_beginthread(SentinelConnectionThread, 0, (void*)g_psentinel->m_data.m_ppConnObj[nTemp])==-1)
										{
											//error.

											//**MSG
								
											
										}
										else
										{
		//									while(!g_psentinel->m_data.m_ppConnObj[nTemp]->m_bConnThreadStarted) Sleep(100);
										}
									}

									//**** should check return value....

								}
								
								g_psentinel->m_data.m_ppConnObj[nTemp]->m_dblServerTimeIntervalMS = (double)g_psentinel->m_settings.m_ulServerStatusDBIntervalMS;
								g_psentinel->m_data.m_ppConnObj[nTemp]->m_bListStatusToDB = g_psentinel->m_settings.m_bListStatusToDB;

								if(
										(szDesc.GetLength()>0)
									&&((g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszDesc==NULL)||(szDesc.CompareNoCase(g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszDesc)))
									)
								{
									if(g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszDesc) free(g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszDesc);

									g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszDesc) sprintf(g_psentinel->m_data.m_ppConnObj[nTemp]->m_pszDesc, "%s", szDesc);
								}

								if(bFlagsFound) g_psentinel->m_data.m_ppConnObj[nTemp]->m_ulFlags = ulFlags|SENTINEL_FLAG_FOUND;
								else g_psentinel->m_data.m_ppConnObj[nTemp]->m_ulFlags |= SENTINEL_FLAG_FOUND;

							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szHost.GetLength()>0)) // have to add.
				{
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "adding %s", szHost);  Sleep(250); //(Dispatch message)
					CSentinelConnectionObject* pscno = new CSentinelConnectionObject;
					if(pscno)
					{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CSentinelConnectionObject** ppObj = new CSentinelConnectionObject*[g_psentinel->m_data.m_nNumConnectionObjects+1];
						if(ppObj)
						{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((g_psentinel->m_data.m_ppConnObj)&&(g_psentinel->m_data.m_nNumConnectionObjects>0))
							{
								while(o<g_psentinel->m_data.m_nNumConnectionObjects)
								{
									ppObj[o] = g_psentinel->m_data.m_ppConnObj[o];
									o++;
								}
								try{delete [] g_psentinel->m_data.m_ppConnObj;} catch(...){}

							}
							ppObj[o] = pscno;
							g_psentinel->m_data.m_ppConnObj = ppObj;
							g_psentinel->m_data.m_nNumConnectionObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szHost.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, "%s", szHost);

							if(szClient.GetLength()<=0)
							{
								szClient = "Sentinel";
							}

							if(szClient.GetLength()>0)
							{
								ppObj[o]->m_pszClientName = (char*)malloc(szClient.GetLength()+1); 
								if(ppObj[o]->m_pszClientName) sprintf(ppObj[o]->m_pszClientName, "%s", szClient);
							}

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}
//							ppObj[o]->m_usType = not used;
							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|SENTINEL_FLAG_FOUND;
							else ppObj[o]->m_ulFlags |= SENTINEL_FLAG_FOUND;
								
							ppObj[o]->m_dblServerTimeIntervalMS = (double)g_psentinel->m_settings.m_ulServerStatusDBIntervalMS;
							ppObj[o]->m_bListStatusToDB = g_psentinel->m_settings.m_bListStatusToDB;


							if((ppObj[o]->m_ulFlags)&SENTINEL_FLAG_ENABLED)
							{
								if(!((g_psentinel->m_settings.m_bDisconnectOnSuspend)&&(g_psentinel->m_data.m_bProcessSuspended)))
								{


									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s to %s...", 
										((ppObj[o]->m_pszClientName)&&(strlen(ppObj[o]->m_pszClientName)))?ppObj[o]->m_pszClientName:"Sentinel",  
										ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName);  
									g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:destination_init", errorstring);  //Sleep(100);  //(Dispatch message)

									//**** if connect 

									// create a connection object in g_adc.
									// associate that with the new CSentinelConnectionObject;
									// start the thread.
									ppObj[o]->m_bKillConnThread = false;
	EnterCriticalSection(&ppObj[o]->m_critAPI);
									ppObj[o]->m_pAPIConn = NULL;
	LeaveCriticalSection(&ppObj[o]->m_critAPI);
	//									g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "beginning thread for %s", szHost);  Sleep(250); //(Dispatch message)
	//XX								ppObj[o]->m_ulStatus = SENTINEL_STATUS_NOTCON;  // let thread set this
									if(_beginthread(SentinelConnectionThread, 0, (void*)ppObj[o])==-1)
									{
										//error.

										//**MSG
	//									g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "problem adding %s", szHost);  Sleep(250); //(Dispatch message)

							
										
									}
								//**** should check return value....
								}

							}
						}
						else
						{
							try{delete pscno;} catch(...){}
						}
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			try{delete prs;} catch(...){}
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)
			LeaveCriticalSection(&m_critSQL);

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<g_psentinel->m_data.m_nNumConnectionObjects)
			{
				if((g_psentinel->m_data.m_ppConnObj)&&(g_psentinel->m_data.m_ppConnObj[nIndex]))
				{
					if((g_psentinel->m_data.m_ppConnObj[nIndex]->m_ulFlags)&SENTINEL_FLAG_FOUND)
					{
						(g_psentinel->m_data.m_ppConnObj[nIndex]->m_ulFlags) &= ~SENTINEL_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(g_psentinel->m_data.m_ppConnObj[nIndex])
						{
							if((g_psentinel->m_data.m_ppConnObj[nIndex]->m_ulFlags)&SENTINEL_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", 
									g_psentinel->m_data.m_ppConnObj[nIndex]->m_pszDesc?g_psentinel->m_data.m_ppConnObj[nIndex]->m_pszDesc:g_psentinel->m_data.m_ppConnObj[nIndex]->m_pszServerName);  
								g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:destination_remove", errorstring); //  Sleep(100); //(Dispatch message)
//									g_psentinel->m_data.m_ppConnObj[nTemp]->m_pDlg->OnDisconnect();

								//**** disconnect
							}

							g_psentinel->m_data.m_ppConnObj[nIndex]->m_bKillConnThread = true;
							// this has to wait because we are deleting.
							while((g_psentinel->m_data.m_ppConnObj[nIndex])&&(g_psentinel->m_data.m_ppConnObj[nIndex]->m_bConnThreadStarted)) Sleep(100);

							if((g_psentinel->m_data.m_ppConnObj[nIndex]->m_ulFlags)&SENTINEL_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s is disconnected.", 
									g_psentinel->m_data.m_ppConnObj[nIndex]->m_pszDesc?g_psentinel->m_data.m_ppConnObj[nIndex]->m_pszDesc:g_psentinel->m_data.m_ppConnObj[nIndex]->m_pszServerName);  
								g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:destination_remove", errorstring); //  Sleep(100);  //(Dispatch message)

								//g_adc.DisconnectServer(g_psentinel->m_data.m_ppConnObj[nIndex]->m_pszServerName);
								//**** should check return value....

//								g_psentinel->m_data.m_ppConnObj[nIndex]->m_ulStatus = SENTINEL_STATUS_NOTCON;
							}

							try{delete g_psentinel->m_data.m_ppConnObj[nIndex];} catch(...){}
							g_psentinel->m_data.m_nNumConnectionObjects--;

							int nTemp=nIndex;
							while(nTemp<g_psentinel->m_data.m_nNumConnectionObjects)
							{
								g_psentinel->m_data.m_ppConnObj[nTemp]=g_psentinel->m_data.m_ppConnObj[nTemp+1];
								nTemp++;
							}
							g_psentinel->m_data.m_ppConnObj[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			LeaveCriticalSection(&m_critConns);
			return nReturn;
		}
		LeaveCriticalSection(&m_critConns);
		LeaveCriticalSection(&m_critSQL);

	}
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return SENTINEL_ERROR;
}


int CSentinelData::GetControlModules(char* pszInfo)
{
	if((g_psentinel)&&(m_pdbConn)&&(m_pdb))
	{
		HRESULT hRes;
		try
		{  

#if _WIN32_WINNT >= 0x0400
			hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
			hRes = CoInitialize(NULL);
#endif
		}
		catch(...)
		{
			if(g_psentinel) g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:GetControlModules", "Exception in CoInitialize");  //(Dispatch message)
		}


		IXMLDOMDocumentPtr pDoc(__uuidof(DOMDocument));
		HRESULT hr = pDoc->put_async(VARIANT_FALSE);
										
		// Remove document safety options
		IObjectSafety* pSafety=NULL;
		DWORD dwSupported, dwEnabled;

		if ( SUCCEEDED(pDoc->QueryInterface(IID_IObjectSafety, (void**)&pSafety)))
		{
			pSafety->GetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, &dwSupported, &dwEnabled );
			pSafety->SetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, dwSupported, 0 );
		}

		CCortexUtil util;  // cortex utility functions.

		CCortexMessage msg; // for the char* CCortexMessage::XMLTextNodeValue(IXMLDOMNodePtr pNode) function.
/*		
		
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY server",  //HARDCODE
			((g_psentinel->m_settings.m_pszChannelInfo)&&(strlen(g_psentinel->m_settings.m_pszChannelInfo)))?g_psentinel->m_settings.m_pszChannelInfo:"ChannelInfo");
//		g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "GetConnections");  Sleep(250); //(Dispatch message)
*/

		char szSQL[DB_SQLSTRING_MAXLEN];
		char dberrorstring[DB_ERRORSTRING_LEN];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, name, description, dll_name, active, flags, type, status, timeout, message, channelids, code FROM %s WHERE type=%d ORDER by name",
				(g_psentinel->m_settings.m_pszExternalModules?g_psentinel->m_settings.m_pszExternalModules:"Controllers"),
				DLL_TYPE_CONTROL
				);
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "obtaining control with [%s]", szSQL); //  Sleep(250);//(Dispatch message)

		CCortexPlugIn tempControlObj;

	EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, dberrorstring);
		if(prs)
		{
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "B entering extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
	EnterCriticalSection(&m_critExternalModules);
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "B entered extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = SENTINEL_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
/*
				CString szHost="";
				CString szClient="";
				CString szDesc="";
				CString szTemp;
				unsigned long ulFlags;   // various flags
			//	unsigned short usType;
				bool bFlagsFound = false;
*/

				int nTemp = -1;
				CString szName = "";
				bool bFound = false;

				try
				{
					CString szTemp;
					prs->GetFieldValue("ID", szTemp);
					tempControlObj.m_nID = atoi(szTemp);
					
					prs->GetFieldValue("name", szTemp); // 256
					if(tempControlObj.m_pszName) { try{ free(tempControlObj.m_pszName); } catch(...){}}
					tempControlObj.m_pszName = (char*)malloc(szTemp.GetLength()+1);
					if( tempControlObj.m_pszName ) sprintf(tempControlObj.m_pszName, "%s", szTemp);

					prs->GetFieldValue("description", szTemp); //256
					if(tempControlObj.m_pszDesc) { try{ free(tempControlObj.m_pszDesc); } catch(...){}}
					tempControlObj.m_pszDesc = (char*)malloc(szTemp.GetLength()+1);
					if( tempControlObj.m_pszDesc ) sprintf(tempControlObj.m_pszDesc, "%s", szTemp);

					prs->GetFieldValue("dll_name", szName); //256
					if(tempControlObj.m_pszModule) { try{ free(tempControlObj.m_pszModule); } catch(...){}}
					tempControlObj.m_pszModule = (char*)malloc(szName.GetLength()+1);
					if( tempControlObj.m_pszModule ) sprintf(tempControlObj.m_pszModule, "%s", szName);
//						AfxMessageBox(szTemp);

					prs->GetFieldValue("active", szTemp);
					tempControlObj.m_ulFlags = ((atol(szTemp)>0)?SENTINEL_FLAG_ENABLED:SENTINEL_FLAG_DISABLED);
//						AfxMessageBox(szTemp);

					prs->GetFieldValue("flags", szTemp);
					tempControlObj.m_ulFlags |= atol(szTemp);
//						AfxMessageBox(szTemp);

					prs->GetFieldValue("type", szTemp);
					tempControlObj.m_usType = (unsigned short)atol(szTemp);
//						AfxMessageBox(szTemp);

					prs->GetFieldValue("status", szTemp);
					tempControlObj.m_ulStatus = (unsigned short)atol(szTemp);
//						AfxMessageBox(szTemp);

					prs->GetFieldValue("timeout", szTemp);
					tempControlObj.m_nDeinitTimeoutMS = (unsigned short)atoi(szTemp);
//						AfxMessageBox(szTemp);

					prs->GetFieldValue("message", szTemp); //256
//						AfxMessageBox(szTemp);
					if(tempControlObj.m_pszStatus) { try{ free(tempControlObj.m_pszStatus); } catch(...){}}
					tempControlObj.m_pszStatus = (char*)malloc(szTemp.GetLength()+1);
					if( tempControlObj.m_pszStatus ) sprintf(tempControlObj.m_pszStatus, "%s", szTemp);

					prs->GetFieldValue("channelids", szTemp); //4096
//						AfxMessageBox(szTemp);

					if(tempControlObj.m_pszChannelIds) { try{ free(tempControlObj.m_pszChannelIds); } catch(...){}}
					tempControlObj.m_pszChannelIds = (char*)malloc(szTemp.GetLength()+1);
					if( tempControlObj.m_pszChannelIds ) sprintf(tempControlObj.m_pszChannelIds, "%s", szTemp);

//					AfxMessageBox(tempControlObj.m_pszChannelIds);

					prs->GetFieldValue("code", szTemp); //4096
					if(tempControlObj.m_key.m_pszLicenseString) { try{ free(tempControlObj.m_key.m_pszLicenseString); } catch(...){}}
					tempControlObj.m_key.m_pszLicenseString = (char*)malloc(szTemp.GetLength()+1);
					if( tempControlObj.m_key.m_pszLicenseString ) sprintf(tempControlObj.m_key.m_pszLicenseString, "%s", szTemp);
				}
				catch( ... )
				{
				}

				if((m_ppControlObj)&&(m_nNumControlObjects))
				{
					nTemp=0;
					while(nTemp<m_nNumControlObjects)
					{
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "c2");  Sleep(250); //(Dispatch message)
						if(m_ppControlObj[nTemp])
						{
							if(
								// OK check this on module name, because otherwise, have to reload etc.
								  (szName.GetLength()>0)
								&&(szName.CompareNoCase(m_ppControlObj[nTemp]->m_pszModule)==0)

//									m_ppControlObj[nTemp] == tempControlObj.m_nID
								)
							{

								m_ppControlObj[nTemp]->m_usType =	tempControlObj.m_usType;

								m_ppControlObj[nTemp]->m_nID = tempControlObj.m_nID;

								m_ppControlObj[nTemp]->m_pszModule = tempControlObj.m_pszModule;
								tempControlObj.m_pszModule = NULL;
							
								if(
										(
											(m_ppControlObj[nTemp]->m_pszName)
										&&(tempControlObj.m_pszName)
										&&(strcmp(m_ppControlObj[nTemp]->m_pszName,tempControlObj.m_pszName))
										)
									||((m_ppControlObj[nTemp]->m_pszName==NULL)!=(tempControlObj.m_pszName==NULL))
									)
								{
									if(m_ppControlObj[nTemp]->m_pszName)
									{ 
										try{ free(m_ppControlObj[nTemp]->m_pszName); } catch(...){} 
										m_ppControlObj[nTemp]->m_pszName = NULL;
									}

									m_ppControlObj[nTemp]->m_pszName = tempControlObj.m_pszName;
									tempControlObj.m_pszName = NULL;
								}
								
								if(
										(
											(m_ppControlObj[nTemp]->m_pszDesc)
										&&(tempControlObj.m_pszDesc)
										&&(strcmp(m_ppControlObj[nTemp]->m_pszDesc,tempControlObj.m_pszDesc))
										)
									||((m_ppControlObj[nTemp]->m_pszDesc==NULL)!=(tempControlObj.m_pszDesc==NULL))
									)
								{
									if(m_ppControlObj[nTemp]->m_pszDesc)
									{ 
										try{ free(m_ppControlObj[nTemp]->m_pszDesc); } catch(...){} 
										m_ppControlObj[nTemp]->m_pszDesc = NULL;
									}

									m_ppControlObj[nTemp]->m_pszDesc = tempControlObj.m_pszDesc;
									tempControlObj.m_pszDesc = NULL;
								}
								
								if(
										(
											(m_ppControlObj[nTemp]->m_pszStatus)
										&&(tempControlObj.m_pszStatus)
										&&(strcmp(m_ppControlObj[nTemp]->m_pszStatus,tempControlObj.m_pszStatus))
										)
									||((m_ppControlObj[nTemp]->m_pszStatus==NULL)!=(tempControlObj.m_pszStatus==NULL))
									)
								{
									if(m_ppControlObj[nTemp]->m_pszStatus)
									{ 
										try{ free(m_ppControlObj[nTemp]->m_pszStatus); } catch(...){} 
										m_ppControlObj[nTemp]->m_pszStatus = NULL;
									}

									m_ppControlObj[nTemp]->m_pszStatus = tempControlObj.m_pszStatus;
									tempControlObj.m_pszStatus = NULL;
								}

								if(
										(
											(m_ppControlObj[nTemp]->m_pszChannelIds)
										&&(tempControlObj.m_pszChannelIds)
										&&(strcmp(m_ppControlObj[nTemp]->m_pszChannelIds,tempControlObj.m_pszChannelIds))
										)
									||((m_ppControlObj[nTemp]->m_pszChannelIds==NULL)!=(tempControlObj.m_pszChannelIds==NULL))
									)
								{
									if(m_ppControlObj[nTemp]->m_pszChannelIds)
									{ 
										try{ free(m_ppControlObj[nTemp]->m_pszChannelIds); } catch(...){} 
										m_ppControlObj[nTemp]->m_pszChannelIds = NULL;
									}

									m_ppControlObj[nTemp]->m_pszChannelIds = tempControlObj.m_pszChannelIds;
									tempControlObj.m_pszChannelIds = NULL;
//					AfxMessageBox(tempControlObj.m_pszChannelIds);

									m_ppControlObj[nTemp]->m_ulFlags|=SENTINEL_FLAG_INIT; // re-init since new channels may have been added.
								}

								if(
										(
											(m_ppControlObj[nTemp]->m_key.m_pszLicenseString)
										&&(tempControlObj.m_key.m_pszLicenseString)
										&&(strcmp(m_ppControlObj[nTemp]->m_key.m_pszLicenseString,tempControlObj.m_key.m_pszLicenseString))
										)
									||((m_ppControlObj[nTemp]->m_key.m_pszLicenseString==NULL)!=(tempControlObj.m_key.m_pszLicenseString==NULL))
									)
								{
									if(m_ppControlObj[nTemp]->m_key.m_pszLicenseString)
									{ 
										try{ free(m_ppControlObj[nTemp]->m_key.m_pszLicenseString); } catch(...){} 
										m_ppControlObj[nTemp]->m_key.m_pszLicenseString = NULL;
									}

									m_ppControlObj[nTemp]->m_key.m_pszLicenseString = tempControlObj.m_key.m_pszLicenseString;
									tempControlObj.m_key.m_pszLicenseString = NULL;

									m_ppControlObj[nTemp]->m_key.InterpretKey();
								}

								m_ppControlObj[nTemp]->m_nDeinitTimeoutMS = tempControlObj.m_nDeinitTimeoutMS;

								m_ppControlObj[nTemp]->m_ulStatus = tempControlObj.m_ulStatus;
								
								if(m_ppControlObj[nTemp]->m_ulFlags&SENTINEL_FLAG_INIT) // has not yet been initialized, do not overwrite this flag
								{
									m_ppControlObj[nTemp]->m_ulFlags = tempControlObj.m_ulFlags|SENTINEL_FLAG_INIT;
								}
								else
								{
									m_ppControlObj[nTemp]->m_ulFlags = tempControlObj.m_ulFlags;
								}

							
								bFound = true;
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "c3");  Sleep(250); //(Dispatch message)
//								m_ppControlObj[nTemp]->m_bKillAutomationThread = false;
//								m_ppControlObj[nTemp]->m_pTabulator = g_psentinel;
//								m_ppControlObj[nTemp]->m_pEndpoint = pEndObj;

//								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning analysis for %s (%s)", 
//													m_ppControlObj[nTemp]->m_pszName,m_ppControlObj[nTemp]->m_pszDesc );
//								g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:destination_change", errorstring);    //(Dispatch message)


								if((m_ppControlObj[nTemp]->m_hinstDLL==NULL)&&(m_ppControlObj[nTemp]->m_ulFlags&SENTINEL_FLAG_ENABLED))
								{
									m_ppControlObj[nTemp]->m_hinstDLL = AfxLoadLibrary(m_ppControlObj[nTemp]->m_pszModule);

									if(m_ppControlObj[nTemp]->m_hinstDLL==NULL)
									{
										int err = GetLastError();
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d loading module %s.  Cannot begin %s (%s)", 
															err, m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName,m_ppControlObj[nTemp]->m_pszDesc );
										g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_change", errorstring);    //(Dispatch message)
									}
									else
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning analysis for %s (%s)", 
															m_ppControlObj[nTemp]->m_pszName,m_ppControlObj[nTemp]->m_pszDesc );
										g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_change", errorstring);    //(Dispatch message)
									}
								

									// only do the following on load

									if((m_ppControlObj[nTemp]->m_lpfnDllCtrl==NULL)&&(m_ppControlObj[nTemp]->m_hinstDLL))
									{
										m_ppControlObj[nTemp]->m_lpfnDllCtrl = (LPFNDLLCTRL)GetProcAddress((m_ppControlObj[nTemp]->m_hinstDLL), DLLFUNC_NAME);
										if(m_ppControlObj[nTemp]->m_lpfnDllCtrl==NULL)
										{
											int err = GetLastError();
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d getting process address in module %s.  Cannot begin %s (%s)", 
																err, m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName,m_ppControlObj[nTemp]->m_pszDesc );
											g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_change", errorstring);    //(Dispatch message)
										}
									}

									if(m_ppControlObj[nTemp]->m_lpfnDllCtrl)
									{

										m_ppControlObj[nTemp]->m_ulFlags|=SENTINEL_FLAG_INIT;

/*

#define DLL_FLAG_DISPATCH     0x00010000 // allowed to get the dispatcher callback
#define DLL_FLAG_MSG			    0x00020000 // allowed to get the UI message callback
#define DLL_FLAG_ASRUN        0x00040000 // allowed to get the UI asrun callback
#define DLL_FLAG_STATUS		    0x00080000 // allowed to get the UI set status callback

#define DLLCMD_INIT						0x0010 // A signal to the DLL that it should run any internal initialization routines. The DLL may want to wait for settings to be set before proceeding to context-sensitive routines.
#define DLLCMD_SET_DISPATCH		0x0011 // Sets a callback function address so that the DLL may send messages to the calling application's message dispatch procedure, which writes log lines, sends emails out, etc depending on severity level and other criteria.
#define DLLCMD_SET_MSG				0x0012 // Sets a callback function address so that the DLL may send messages to the calling application's web user interface for messages.
#define DLLCMD_SET_ASRUN			0x0013 // Sets a callback function address so that the DLL may send messages to the calling application's web user interface for as-run items.
#define DLLCMD_GET_SETTINGS		0x0014 // Obtain a list of settings that the DLL would like to make sure exists in the user interface, for use inside the DLL.
#define DLLCMD_SET_SETTINGS		0x0015 // Set a list of settings with values from the web interface (database).
#define DLLCMD_SET_STATUS			0x0016 // Sets a callback function address so that the DLL may send status updates to the calling application's web user interface for module status.

#define DLLCMD_FREE_BUFFER		0x00ff // A call to free a buffer.  This will be called to free buffers that have been allocated by the calls specified above. Note that this is a different call from the call to free an object, regardless of whether the DLL uses the same allocation/deallocation functions in the call.  That command is given here for completeness, but is not used in initialization
#define DLLCMD_FREE_OBJECT		0x00fe // A call to free an object.
									
	*/

										char* pch = (char*)malloc(64);
										if(pch)
										{
											sprintf(pch, "%d", m_ppControlObj[nTemp]->m_nID);
										}
										char* pchOld = pch; 

										int nCode = m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_INIT);
										if(nCode<SENTINEL_SUCCESS)
										{
											/// do something, report?
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d initializing module %s: %s (%s)", nCode,
																m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName, m_ppControlObj[nTemp]->m_pszDesc );
											g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_change", errorstring);    //(Dispatch message)
										}
										else
										{
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Module %s: %s (%s) initialized with [%s] code %d", 
												m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName, m_ppControlObj[nTemp]->m_pszDesc, (((pch)&&(pch!=pchOld))?pch:""), nCode );
											g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_change", errorstring);    //(Dispatch message)
										}

										if((pch)&&(pch!=pchOld))
										{
											try{ m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
											catch(...) {}
											pch = NULL;
										}

										if(pchOld)
										{
											try{ free(pchOld); } catch(...){}
										}

										if(m_ppControlObj[nTemp]->m_ulFlags&DLL_FLAG_DISPATCH)
										{
	//typedef void (__stdcall* LPFNDM)( unsigned long, char*, char*, char* );
	//typedef int (__stdcall* LPFNMSG)( int, char*, char* );
	//typedef int (__stdcall* LPFNAR)( int, char*, char*, char*, int);
	// Generic callbacks for DLLs to send status message to module info interface.
	//typedef int (__stdcall* LPFNSTATUS)( int, int, char*);


											nCode = m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(MessageDispatch), DLLCMD_SET_DISPATCH);
											if(nCode<SENTINEL_SUCCESS)
											{
												/// do something, report?
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d setting message dispatcher pointer in module %s: %s (%s)", nCode,
																	m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName,m_ppControlObj[nTemp]->m_pszDesc );
												g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_change", errorstring);    //(Dispatch message)
											}
										}
										if(m_ppControlObj[nTemp]->m_ulFlags&DLL_FLAG_MSG)
										{
											nCode = m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(SendMsg), DLLCMD_SET_MSG);
											if(nCode<SENTINEL_SUCCESS)
											{
												/// do something, report?
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d setting UI message callback pointer in module %s: %s (%s)", nCode,
																	m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName,m_ppControlObj[nTemp]->m_pszDesc );
												g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_change", errorstring);    //(Dispatch message)
											}
										}
										if(m_ppControlObj[nTemp]->m_ulFlags&DLL_FLAG_ASRUN)
										{
											nCode = m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(SendAsRunMsg), DLLCMD_SET_ASRUN);
											if(nCode<SENTINEL_SUCCESS)
											{
												/// do something, report?
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d setting UI as run callback pointer in module %s: %s (%s)", nCode,
																	m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName,m_ppControlObj[nTemp]->m_pszDesc );
												g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_change", errorstring);    //(Dispatch message)
											}
										}
										if(m_ppControlObj[nTemp]->m_ulFlags&DLL_FLAG_STATUS)
										{
											nCode = m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(SetExternalModuleStatus), DLLCMD_SET_STATUS);
											if(nCode<SENTINEL_SUCCESS)
											{
												/// do something, report?
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d setting UI module status callback pointer in module %s: %s (%s)", nCode,
																	m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName,m_ppControlObj[nTemp]->m_pszDesc );
												g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_change", errorstring);    //(Dispatch message)
											}
										}

										CCortexDBSetting** ppSettings = NULL;
										int nNumSettings = 0 ;


										pch = NULL;
										nCode = m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_GET_SETTINGS);
										if(nCode<SENTINEL_SUCCESS)
										{
											/// do something, report?
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d getting module settings from module %s: %s (%s)", nCode,
																m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName,m_ppControlObj[nTemp]->m_pszDesc );
											g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_change", errorstring);    //(Dispatch message)
										}
										else
										{
											// process these settings
/* // XML like this is expected:
<setting>
	<category>network</category>
	<category_display>Network Settings</category_display>
	<parameter>port</parameter>
	<parameter_display>TCPIP port</parameter_display>
	<value>12345</value>
	<desc>The TCPIP port for the main communication	listener</desc>
	<pattern>Integer</pattern>
	<min_len>null</min_len>
	<max_len>null</max_len>
	<min_value>1024</min_value>
	<max_value>65535</max_value>
	<sysonly>true</sysonly>
</setting>
<setting>
  ....
</setting>
*/

									// load this into and XML object and parse it out,
									// records are unique on category and parameter togther
									// do not overwrite VALUES on items that are already in the db.



		//									need a top level node:
											char* pchXML = (char*)malloc(strlen(pch)+strlen("<xml></xml>")+1);
											if(pchXML)
											{
												sprintf(pchXML, "<xml>%s</xml>", pch);

												pDoc->put_async(VARIANT_FALSE);
												hr = pDoc->loadXML((char*)pchXML);
												if(hr!=VARIANT_TRUE)
												{
													g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:GetControlModules", "XML parse error: [%s]", pchXML );
												}
												else
												{
				if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
					g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:GetControlModules", "begin xml parse");  //(Dispatch message)
													IXMLDOMNodeListPtr pChildNodes = pDoc->GetchildNodes();

		/*
		#define DLL_SETTING_XML_CATEGORY						0
		#define DLL_SETTING_XML_CATEGORY_DISPLAY		1
		#define DLL_SETTING_XML_PARAM								2
		#define DLL_SETTING_XML_PARAM_DISPLAY				3
		#define DLL_SETTING_XML_VALUE								4
		#define DLL_SETTING_XML_DESC								5
		#define DLL_SETTING_XML_PATTERN							6
		#define DLL_SETTING_XML_MIN_LEN							7
		#define DLL_SETTING_XML_MAX_LEN							8
		#define DLL_SETTING_XML_MIN_VAL							9
		#define DLL_SETTING_XML_MAX_VAL							10
		#define DLL_SETTING_XML_SYSONLY							11

		#define DLL_SETTING_XML_NODECOUNT						12
		*/
		//											IXMLDOMNodePtr pNodes[DLL_SETTING_XML_NODECOUNT];
		//											int n=DLL_SETTING_XML_CATEGORY;
		//											while(n<DLL_SETTING_XML_NODECOUNT){ pNodes[n]=NULL; n++; }

													if (pChildNodes != NULL)
													{
														IXMLDOMNodePtr pChild;
														while(pChild = pChildNodes->nextNode())
														{
															DOMNodeType nodeType;
															pChild->get_nodeType(&nodeType);

															if(nodeType == NODE_ELEMENT)
															{
																char tag[MAX_PATH]; 
																//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
																util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

												//				AfxMessageBox(tag);

																if(strcmp("xml", tag)==0)  // this one is guaranteed, basically!
																{

																	pChildNodes = pChild->GetchildNodes();
																	if (pChildNodes != NULL)
																	{

																		while(pChild = pChildNodes->nextNode()) // loop thru the settings.
																		{
																			pChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

																		//		AfxMessageBox(tag);

																				if(strcmp("setting", tag)==0)
																				{
																					IXMLDOMNodeListPtr pSettingChildNodes = pChild->GetchildNodes();
																					if (pSettingChildNodes != NULL)
																					{
																						//new node with children!

																						CCortexDBSetting** ppNewSettings = new CCortexDBSetting*[nNumSettings+1];
																						if(ppNewSettings)
																						{
																							if(ppSettings)
																							{
																								int s=0;
																								while(s<nNumSettings)
																								{
																									ppNewSettings[s] = ppSettings[s];
																									s++;
																								}

																								try{ delete [] ppSettings; }catch(...){}
																							}
																							ppSettings = ppNewSettings;


																							CCortexDBSetting* pSettingObj = new CCortexDBSetting;
																							if(pSettingObj)
																							{
																								ppSettings[nNumSettings] = pSettingObj;
																								pSettingObj->m_n_sortorder = nNumSettings+1;

																								while(pChild = pSettingChildNodes->nextNode()) // loop thru the settings.
																								{
																									pChild->get_nodeType(&nodeType);

																									if(nodeType == NODE_ELEMENT)
																									{
																										//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
																										util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

																								//		AfxMessageBox(tag);

																										if(strcmp("category", tag)==0)
																										{
																											pSettingObj->m_psz_category_vc64 = msg.XMLTextNodeValue(pChild);
																											if(pSettingObj->m_psz_category_vc64)
																											{
																												if(strlen(pSettingObj->m_psz_category_vc64)>64) pSettingObj->m_psz_category_vc64[64]=0;
																											}
																										}
																										else
																										if(strcmp("category_display", tag)==0)
																										{
																											pSettingObj->m_psz_category_display_name_vc64 = msg.XMLTextNodeValue(pChild);
																											if(pSettingObj->m_psz_category_display_name_vc64)
																											{
																												if(strlen(pSettingObj->m_psz_category_display_name_vc64)>64) pSettingObj->m_psz_category_display_name_vc64[64]=0;
																											}
																										}
																										else
																										if(strcmp("parameter", tag)==0)
																										{
																											pSettingObj->m_psz_parameter_vc64 = msg.XMLTextNodeValue(pChild);
																											if(pSettingObj->m_psz_parameter_vc64)
																											{
																												if(strlen(pSettingObj->m_psz_parameter_vc64)>64) pSettingObj->m_psz_parameter_vc64[64]=0;
																											}
																										}
																										else
																										if(strcmp("parameter_display", tag)==0)
																										{
																											pSettingObj->m_psz_parameter_display_name_vc64 = msg.XMLTextNodeValue(pChild);
																											if(pSettingObj->m_psz_parameter_display_name_vc64)
																											{
																												if(strlen(pSettingObj->m_psz_parameter_display_name_vc64)>64) pSettingObj->m_psz_parameter_display_name_vc64[64]=0;
																											}
																										}
																										else
																										if(strcmp("value", tag)==0)
																										{
																											pSettingObj->m_psz_value_vc256 = msg.XMLTextNodeValue(pChild);
																											if(pSettingObj->m_psz_value_vc256)
																											{
																												if(strlen(pSettingObj->m_psz_value_vc256)>256) pSettingObj->m_psz_value_vc256[256]=0;
																											}
																										}
																										else
																										if(strcmp("desc", tag)==0)
																										{
																											pSettingObj->m_psz_description_vc256 = msg.XMLTextNodeValue(pChild);
																											if(pSettingObj->m_psz_description_vc256)
																											{
																												if(strlen(pSettingObj->m_psz_description_vc256)>256) pSettingObj->m_psz_description_vc256[256]=0;
																											}
																										}
																										else
																										if(strcmp("pattern", tag)==0)
																										{
																											pSettingObj->m_psz_pattern_vc64 = msg.XMLTextNodeValue(pChild);
																											if(pSettingObj->m_psz_pattern_vc64)
																											{
																												if(strlen(pSettingObj->m_psz_pattern_vc64)>64) pSettingObj->m_psz_pattern_vc64[64]=0;
																											}
																										}
																										else
																										if(strcmp("min_len", tag)==0)
																										{
																											char* pchSetting = msg.XMLTextNodeValue(pChild);
																											if(pchSetting)
																											{
																												if(stricmp(pchSetting, "null")!=0)
																												{
																													pSettingObj->m_n_min_len = atoi(pchSetting);
																												}
																												else
																												{
																													pSettingObj->m_n_min_len = -1;
																												}
																												try{ free(pchSetting); }catch(...){}
																											}
																										}
																										else
																										if(strcmp("max_len", tag)==0)
																										{
																											char* pchSetting = msg.XMLTextNodeValue(pChild);
																											if(pchSetting)
																											{
																												if(stricmp(pchSetting, "null")!=0)
																												{
																													pSettingObj->m_n_max_len = atoi(pchSetting);
																												}
																												else
																												{
																													pSettingObj->m_n_max_len =-1;
																												}
																												try{ free(pchSetting); }catch(...){}
																											}
																										}
																										else
																										if(strcmp("min_value", tag)==0)
																										{
																											pSettingObj->m_psz_min_value_vc64 = msg.XMLTextNodeValue(pChild);
																											if(pSettingObj->m_psz_min_value_vc64)
																											{
																												if(strlen(pSettingObj->m_psz_min_value_vc64)>64) pSettingObj->m_psz_min_value_vc64[64]=0;
																											}
																										}
																										else
																										if(strcmp("max_value", tag)==0)
																										{
																											pSettingObj->m_psz_max_value_vc64 = msg.XMLTextNodeValue(pChild);
																											if(pSettingObj->m_psz_max_value_vc64)
																											{
																												if(strlen(pSettingObj->m_psz_max_value_vc64)>64) pSettingObj->m_psz_max_value_vc64[64]=0;
																											}
																										}
																										else
																										if(strcmp("sysonly", tag)==0)
																										{
																											char* pchSetting = msg.XMLTextNodeValue(pChild);
																											if(pchSetting)
																											{
																												if((stricmp(pchSetting, "true")==0)||(strcmp(pchSetting, "1")==0))
																												{
																													pSettingObj->m_b_sys_only = true;
																												}
																												try{ free(pchSetting); }catch(...){}
																											}
																										}
																									}
																								}
																								nNumSettings++;
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}

												try{ free(pchXML); } catch(...){}
											}
											else
											{
												// no settings, that is fine.
											}
										}

										if(pch)
										{
											try{ m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
											catch(...) {}
											pch = NULL;
										}



								//here, swap settings out, add to the database if not there, etc.

										if((ppSettings)&&(nNumSettings>0))
										{
											// need a new connection to retrieve while in the retrieve.
											CDBUtil db;
											CDBconn* pdbConn = db.CreateNewConnection(g_psentinel->m_settings.m_pszDSN, g_psentinel->m_settings.m_pszUser, g_psentinel->m_settings.m_pszPW);
											if(pdbConn)
											{
												if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
												{
													g_psentinel->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Sentinel:database_connect(get_control_modules)", errorstring);  //(Dispatch message)

												}
												else
												{
													int d=0;
													while(d<nNumSettings)
													{
														if((ppSettings[d])&&(ppSettings[d]->m_psz_category_vc64)&&(ppSettings[d]->m_psz_parameter_vc64))  //category and param must exist
														{

		/*
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT category_display_name, value, parameter_display_name, min_len, \
		max_len, pattern, min_value, max_value, description, sys_only  FROM %s WHERE category = '�%d�%s' AND parameter = '%s'",
						(g_psentinel->m_settings.m_pszSettings?g_psentinel->m_settings.m_pszSettings:"Settings"),
						ppObj[o]->m_nID, 
						ppSettings[d]->m_psz_category_vc64, 
						ppSettings[d]->m_psz_parameter_vc64
						);
		*/
															_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT value FROM %s WHERE category = '�%d�%s' AND parameter = '%s'",
																(g_psentinel->m_settings.m_pszSettings?g_psentinel->m_settings.m_pszSettings:"Settings"),
																m_ppControlObj[nTemp]->m_nID, 
																ppSettings[d]->m_psz_category_vc64, 
																ppSettings[d]->m_psz_parameter_vc64
																);
															ppSettings[d]->ulFlags = 0;

		//AfxMessageBox("retrieving stuff");

															CRecordset* prsset = db.Retrieve(pdbConn, szSQL, dberrorstring);
															if(prsset)
															{
																if(!prsset->IsEOF())
																{
																	ppSettings[d]->ulFlags = 1; //exists

																	try
																	{
																		CString szTemp;
		/*
																		prsset->GetFieldValue("category_display_name", szTemp);
																		if(
																				(szTemp.GetLength())
																			&&(ppSettings[d]->m_psz_category_display_name_vc64)
																			&&(szTemp.Compare(ppSettings[d]->m_psz_category_display_name_vc64)
																			)
																		{
																			ppSettings[d]->ulFlags |= 2;  //modify
																		}
		*/

																		// just do value for now.

																		prsset->GetFieldValue("value", szTemp);
																		if(
																				((ppSettings[d]->m_psz_value_vc256==NULL)&&(szTemp.GetLength()))
																			||((ppSettings[d]->m_psz_value_vc256)&&(szTemp.Compare(ppSettings[d]->m_psz_value_vc256)))
																			)
																		{
																			if(ppSettings[d]->m_psz_value_vc256)
																			{
																				try{ free(ppSettings[d]->m_psz_value_vc256); } catch(...){}
																			}
																			ppSettings[d]->m_psz_value_vc256 = (char*)malloc(szTemp.GetLength()+1);
																			if(ppSettings[d]->m_psz_value_vc256)
																			{
																				sprintf(ppSettings[d]->m_psz_value_vc256, "%s", szTemp);
																			}
																		}

																	}
																	catch(...)
																	{
																	}														
																}

																prsset->Close();
																try{ delete prsset; } catch(...){}
															}



															if(ppSettings[d]->ulFlags==0)
															{
																// didn't exist in db, must add it

																CCortexDBSetting dbs;
																dbs.m_psz_category_vc64 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_category_vc64, 64);
																dbs.m_psz_category_display_name_vc64 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_category_display_name_vc64, 64);
																dbs.m_psz_parameter_vc64 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_parameter_vc64, 64);
																dbs.m_psz_value_vc256 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_value_vc256, 256);
																dbs.m_psz_parameter_display_name_vc64 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_parameter_display_name_vc64, 64);
																dbs.m_psz_pattern_vc64 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_pattern_vc64, 64);
																dbs.m_psz_min_value_vc64 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_min_value_vc64, 64);
																dbs.m_psz_max_value_vc64 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_max_value_vc64, 64);
																dbs.m_psz_description_vc256 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_description_vc256, 256);
																char min_len[MAX_PATH];
																char max_len[MAX_PATH];
																if(ppSettings[d]->m_n_min_len==-1)
																{
																	strcpy(min_len, "NULL");
																}
																else
																{
																	sprintf(min_len, "%d", ppSettings[d]->m_n_min_len);
																}
																if(ppSettings[d]->m_n_max_len==-1)
																{
																	strcpy(max_len, "NULL");
																}
																else
																{
																	sprintf(max_len, "%d", ppSettings[d]->m_n_max_len);
																}
		//AfxMessageBox("about to format");

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
category, category_display_name, parameter, value, parameter_display_name, min_len, max_len, pattern, min_value, max_value, sortorder, description, sys_only) \
VALUES ('�%d�%s', %s%s%s, '%s', %s%s%s, %s%s%s, %s, %s, %s%s%s, %s%s%s, %s%s%s, %d, %s%s%s, %d)",
						(g_psentinel->m_settings.m_pszSettings?g_psentinel->m_settings.m_pszSettings:"Settings"),

						m_ppControlObj[nTemp]->m_nID, (dbs.m_psz_category_vc64?dbs.m_psz_category_vc64:""), 

						(dbs.m_psz_category_display_name_vc64?"'":""),
						(dbs.m_psz_category_display_name_vc64?dbs.m_psz_category_display_name_vc64:"NULL"), 
						(dbs.m_psz_category_display_name_vc64?"'":""),
						
						(dbs.m_psz_parameter_vc64?dbs.m_psz_parameter_vc64:""),
						
						(dbs.m_psz_value_vc256?"'":""),
						(dbs.m_psz_value_vc256?dbs.m_psz_value_vc256:"NULL"), 
						(dbs.m_psz_value_vc256?"'":""),

						(dbs.m_psz_parameter_display_name_vc64?"'":""),
						(dbs.m_psz_parameter_display_name_vc64?dbs.m_psz_parameter_display_name_vc64:"NULL"), 
						(dbs.m_psz_parameter_display_name_vc64?"'":""),

						min_len, 
						max_len,

						(dbs.m_psz_pattern_vc64?"'":""),
						(dbs.m_psz_pattern_vc64?dbs.m_psz_pattern_vc64:"NULL"), 
						(dbs.m_psz_pattern_vc64?"'":""),

						(dbs.m_psz_min_value_vc64?"'":""),
						(dbs.m_psz_min_value_vc64?dbs.m_psz_min_value_vc64:"NULL"), 
						(dbs.m_psz_min_value_vc64?"'":""),

						(dbs.m_psz_max_value_vc64?"'":""),
						(dbs.m_psz_max_value_vc64?dbs.m_psz_max_value_vc64:"NULL"), 
						(dbs.m_psz_max_value_vc64?"'":""),

						(m_ppControlObj[nTemp]->m_nID*1000)+d+1,

						(dbs.m_psz_description_vc256?"'":""),
						(dbs.m_psz_description_vc256?dbs.m_psz_description_vc256:"NULL"), 
						(dbs.m_psz_description_vc256?"'":""),

						(ppSettings[d]->m_b_sys_only?1:0)
						);
		//AfxMessageBox(szSQL);

																if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
																{
																	//**MSG
														g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:database_connect(get_control_modules)", errorstring); // Sleep(50); //(Dispatch message)
																}

															}

														}


														d++;
													}

												}


												db.RemoveConnection(pdbConn);

											}

										}

//AfxMessageBox("done with getting settings");

										//pch = /// TODO make up the settings buffer
/*
								Consists of XML of this nature:
<setting>
	<category>network</category>
	<parameter>port</parameter>
	<value>12345</value>
</setting>
<setting>
	...
</setting>
*/
										if((ppSettings)&&(nNumSettings>0))
										{
											pch = NULL;  // just for safety

											int d=0;
											int nLen = 0;
											while(d<nNumSettings)
											{
												strcpy(szSQL, "");
												if(ppSettings[d])
												{
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN, "<setting><category>%s</category><parameter>%s</parameter><value>%s</value></setting>", 
														ppSettings[d]->m_psz_category_vc64?ppSettings[d]->m_psz_category_vc64:"", 
														ppSettings[d]->m_psz_parameter_vc64?ppSettings[d]->m_psz_parameter_vc64:"", 
														ppSettings[d]->m_psz_value_vc256?ppSettings[d]->m_psz_value_vc256:"");
												
												}
												
												int nRec = strlen(szSQL);
												if(nRec)
												{
													char* pchNew = (char*)malloc(nLen + nRec + 1);
													if(pchNew)
													{
														if(pch)
														{
															memcpy(pchNew, pch, nLen);
															try {free(pch);} catch(...){}
														}
														memcpy(pchNew+nLen, szSQL, nRec);
														nLen+=nRec;
														*(pchNew+nLen) = 0;

														pch = pchNew;
													}
												}
												d++;
											}

											nCode = m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_SET_SETTINGS);
											if(nCode<SENTINEL_SUCCESS)
											{
												/// do something, report?
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d setting module settings in module %s: %s (%s)", nCode,
																	m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName,m_ppControlObj[nTemp]->m_pszDesc );
												g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_change", errorstring);    //(Dispatch message)
											}

											if(pch)
											{
												try{ free(pch); }
												catch(...) {}
											}

											// done with the settings, so delete:

											while(d<nNumSettings)
											{
												if(ppSettings[d])
												{
													try{ delete ppSettings[d]; } catch(...){}
												}
												d++;
											}
											try{ delete [] ppSettings; } catch(...){}

										}

										nCode = m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(NULL), DLLCMD_RELEASE);
										if(nCode<SENTINEL_SUCCESS)
										{
											/// do something, report?
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d setting module settings in module %s: %s (%s)", nCode,
																m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName,m_ppControlObj[nTemp]->m_pszDesc );
											g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_change", errorstring);    //(Dispatch message)
										}

									}


/*
								if(m_ppControlObj[nTemp]->m_bAutomationThreadStarted == false)
								{

									//**** if connect set following status
									if(_beginthread(TabulatorAutomationThread, 0, (void*)m_ppControlObj[nTemp])==-1)
									{
										//error.

										//**MSG
									}
									//**** should check return value....
								}
*/


/*
								Sleep(30);
								if(m_ppControlObj[nTemp]->m_bNearAnalysisThreadStarted == false)
								{
									if(_beginthread(TabulatorNearAnalysisThread, 0, (void*)m_ppControlObj[nTemp])==-1)
									{
										//error.

										//**MSG
									}
									//**** should check return value....
								}
*/
							//	Sleep(30);
/*
								if(m_ppControlObj[nTemp]->m_bTriggerThreadStarted == false)
								{
									if(_beginthread(TabulatorTriggerThread, 0, (void*)m_ppControlObj[nTemp])==-1)
									{
										//error.

										//**MSG
									}
									//**** should check return value....
								}
								Sleep(30);

*/
								}
								else
								if((m_ppControlObj[nTemp]->m_hinstDLL!=NULL)&&(!(m_ppControlObj[nTemp]->m_ulFlags&SENTINEL_FLAG_ENABLED)))
								{
									// have to unload etc.
									
									if(m_ppControlObj[nTemp]->m_hinstDLL)
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending control for %s (%s)", 
																	m_ppControlObj[nTemp]->m_pszName,m_ppControlObj[nTemp]->m_pszDesc );  
										g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
			//									m_ppControlObj[nTemp]->m_pDlg->OnDisconnect();

										if(m_ppControlObj[nTemp]->m_lpfnDllCtrl)
										{
										//**** disconnect
			//							m_ppControlObj[nTemp]->m_bKillAutomationThread = true;

			/*
			#define DLLCMD_DEINIT					0x0020 // A signal to the DLL that it should run any internal de-initialization routines. After the DEINIT command is received the DLL should prepare to finalize any messaging back to the calling application.
			#define DLLCMD_MSGEND					0x0021 // A signal to the DLL that it should not send any further messages through any messaging callbacks, including as-run.
			#define DLLCMD_QUERY					0x0022 // A query to the DLL to ask it if it has finished deinitialization.  This command will repeat until a succesful return, or a de-init timeout has elapsed.
			#define DLLCMD_DLLEND					0x002f // A signal to the DLL that it is about to be unloaded.
			*/
											char* pch = (char*)malloc(64);
											if(pch)
											{
												sprintf(pch, "%d", m_ppControlObj[nTemp]->m_nID);
											}
											char* pchOld = pch; 

											int nCode = m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_DEINIT);
											if(nCode<SENTINEL_SUCCESS)
											{
												/// do something, report?
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d de-initializing module %s: %s (%s)", nCode,
																	m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName, m_ppControlObj[nTemp]->m_pszDesc );
												g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
											}
											else
											{
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Module %s: %s (%s) de-initialized with [%s] code %d", 
													m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName, m_ppControlObj[nTemp]->m_pszDesc, (((pch)&&(pch!=pchOld))?pch:""), nCode );
												g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
											}

											if((pch)&&(pch!=pchOld))
											{
												try{ m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
												catch(...) {}
												pch = NULL;
												if(pchOld)
												{
													try{ free(pchOld); }
													catch(...) {}
													pchOld = NULL;
												}
											}
											if(pch)
											{
												try{ free(pch); }
												catch(...) {}
												pch = NULL;
											}

											pch = (char*)malloc(64);
											if(pch)
											{
												sprintf(pch, "%d", m_ppControlObj[nTemp]->m_nID);
											}
											pchOld = pch; 

											nCode = m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_MSGEND);
											if(nCode<SENTINEL_SUCCESS)
											{
												/// do something, report?
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d commanding message end in module %s: %s (%s)", nCode,
																	m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName, m_ppControlObj[nTemp]->m_pszDesc );
												g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
											}
											else
											{
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Module %s: %s (%s) ended messaging with [%s] code %d", 
													m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName, m_ppControlObj[nTemp]->m_pszDesc, (((pch)&&(pch!=pchOld))?pch:""), nCode );
												g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
											}

											if((pch)&&(pch!=pchOld))
											{
												try{ m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
												catch(...) {}
												pch = NULL;
												if(pchOld)
												{
													try{ free(pchOld); }
													catch(...) {}
													pchOld = NULL;
												}
											}
											if(pch)
											{
												try{ free(pch); }
												catch(...) {}
												pch = NULL;
											}

											bool bReleased = false;
											_timeb tmChk;
											_ftime(&tmChk);

											if(m_ppControlObj[nTemp]->m_nDeinitTimeoutMS>0)
											{
												tmChk.time += m_ppControlObj[nTemp]->m_nDeinitTimeoutMS/1000;
												tmChk.millitm += m_ppControlObj[nTemp]->m_nDeinitTimeoutMS%1000;
												while(tmChk.millitm>999)
												{
													tmChk.millitm-=1000;
													tmChk.time++;
												}
											}
											else
											{
												tmChk.time++;
											}

											_timeb tmNow;
											_ftime(&tmNow);

											do
											{

												pch = (char*)malloc(64);
												if(pch)
												{
													sprintf(pch, "%d", m_ppControlObj[nTemp]->m_nID);
												}
												pchOld = pch; 

												int nCode = m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_QUERY);
												if(nCode<SENTINEL_SUCCESS)
												{
													/// do something, report?
													_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d querying control end in module %s: %s (%s)", nCode,
																		m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName, m_ppControlObj[nTemp]->m_pszDesc );
													g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
												}
												else
												{
													bReleased = true;
													_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Module %s: %s (%s) query end succeeded with [%s] code %d", 
														m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName, m_ppControlObj[nTemp]->m_pszDesc, (((pch)&&(pch!=pchOld))?pch:""), nCode );
													g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
												}

												if((pch)&&(pch!=pchOld))
												{
													try{ m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
													catch(...) {}
													pch = NULL;
													if(pchOld)
													{
														try{ free(pchOld); }
														catch(...) {}
														pchOld = NULL;
													}
												}
												if(pch)
												{
													try{ free(pch); }
													catch(...) {}
													pch = NULL;
												}

												_timeb tmSleep;
												_ftime(&tmSleep);

												tmSleep.time +=	g_psentinel->m_settings.m_ulModuleEndQueryIntervalMS/1000;
												tmSleep.millitm += (unsigned short)(g_psentinel->m_settings.m_ulModuleEndQueryIntervalMS%1000);
												while(tmSleep.millitm>999)
												{
													tmSleep.millitm-=1000;
													tmSleep.time++;
												}

												while (
																(!bReleased)
															&&(
																	(tmNow.time<tmSleep.time)
																||((tmNow.time==tmSleep.time)&&(tmNow.millitm<tmSleep.millitm))
																)
//															&&(!g_bKillThread) //don't do this on query end
															)
												{												
													Sleep(1);
													_ftime(&tmNow);
												}

											}	while (
																(!bReleased)
															&&(
																	(tmNow.time<tmChk.time)
																||((tmNow.time==tmChk.time)&&(tmNow.millitm<tmChk.millitm))
																)
															);

											//g_adc.DisconnectServer(m_ppControlObj[nTemp]->m_pszServerName);
											//**** should check return value....


											pch = (char*)malloc(64);
											if(pch)
											{
												sprintf(pch, "%d", m_ppControlObj[nTemp]->m_nID);
											}
											pchOld = pch; 

											nCode = m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_DLLEND);
											if(nCode<SENTINEL_SUCCESS)
											{
												/// do something, report?
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d commanding control end in module %s: %s (%s)", nCode,
																	m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName, m_ppControlObj[nTemp]->m_pszDesc );
												g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
											}
											else
											{
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Module %s: %s (%s) ended control with [%s] code %d", 
													m_ppControlObj[nTemp]->m_pszModule, m_ppControlObj[nTemp]->m_pszName, m_ppControlObj[nTemp]->m_pszDesc, (((pch)&&(pch!=pchOld))?pch:""), nCode );
												g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
											}

											if((pch)&&(pch!=pchOld))
											{
												try{ m_ppControlObj[nTemp]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
												catch(...) {}
												pch = NULL;
												if(pchOld)
												{
													try{ free(pchOld); }
													catch(...) {}
													pchOld = NULL;
												}
											}
											if(pch)
											{
												try{ free(pch); }
												catch(...) {}
												pch = NULL;
											}
										}

										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ended control for %s (%s)", 
																m_ppControlObj[nTemp]->m_pszName,m_ppControlObj[nTemp]->m_pszDesc );  
										g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
										

										try
										{
//											AfxFreeLibrary(m_ppControlObj[nTemp]->m_hinstDLL);  // crashes!  not sure why - lets try again.
										}
										catch(...)
										{
										}

										m_ppControlObj[nTemp]->m_hinstDLL = NULL;
										m_ppControlObj[nTemp]->m_lpfnDllCtrl = NULL;

										Sleep(100); // let it settle

										// OK HERE, but not in the main thread shutdown, it means that someone has removed a module completely.
										// so we remove its settings from the DB
									}

								}
								m_ppControlObj[nTemp]->m_ulFlags |= SENTINEL_FLAG_FOUND;

							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szName.GetLength()>0)) // have to add.
				{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "adding control module: %s: %s (%s)", tempControlObj.m_pszModule,tempControlObj.m_pszName,tempControlObj.m_pszDesc); //  Sleep(250);//(Dispatch message)
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "adding %s", szHost);  Sleep(250); //(Dispatch message)
					CCortexPlugIn* pscno = new CCortexPlugIn;
					if(pscno)
					{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CCortexPlugIn** ppObj = new CCortexPlugIn*[m_nNumControlObjects+1];
						if(ppObj)
						{
//g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((m_ppControlObj)&&(m_nNumControlObjects>0))
							{
								while(o<m_nNumControlObjects)
								{
									ppObj[o] = m_ppControlObj[o];
									o++;
								}
								try{delete [] m_ppControlObj;} catch(...){}

							}
							ppObj[o] = pscno;
							m_ppControlObj = ppObj;
							m_nNumControlObjects++;

							ppObj[o]->m_nID = tempControlObj.m_nID;

							ppObj[o]->m_pszModule = tempControlObj.m_pszModule;
							tempControlObj.m_pszModule = NULL;
						
							ppObj[o]->m_pszName = tempControlObj.m_pszName;
							tempControlObj.m_pszName = NULL;
							
							ppObj[o]->m_pszDesc = tempControlObj.m_pszDesc;
							tempControlObj.m_pszDesc = NULL;

							ppObj[o]->m_pszStatus = tempControlObj.m_pszStatus;
							tempControlObj.m_pszStatus = NULL;

							ppObj[o]->m_pszChannelIds = tempControlObj.m_pszChannelIds;
							tempControlObj.m_pszChannelIds = NULL;
//					AfxMessageBox(ppObj[o]->m_pszChannelIds);

							ppObj[o]->m_key.m_pszLicenseString = tempControlObj.m_key.m_pszLicenseString;
							tempControlObj.m_key.m_pszLicenseString = NULL;

							ppObj[o]->m_nDeinitTimeoutMS = tempControlObj.m_nDeinitTimeoutMS;

							ppObj[o]->m_ulStatus = tempControlObj.m_ulStatus;

							ppObj[o]->m_ulFlags = tempControlObj.m_ulFlags;

							ppObj[o]->m_usType =	tempControlObj.m_usType;

							ppObj[o]->m_key.InterpretKey();

							if((ppObj[o]->m_hinstDLL==NULL)&&(ppObj[o]->m_ulFlags&SENTINEL_FLAG_ENABLED))
							{
								ppObj[o]->m_hinstDLL = AfxLoadLibrary(ppObj[o]->m_pszModule);

								if(ppObj[o]->m_hinstDLL==NULL)
								{
									int err = GetLastError();
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d loading module %s.  Cannot begin %s (%s)", 
														err, ppObj[o]->m_pszModule, ppObj[o]->m_pszName, ppObj[o]->m_pszDesc );
									g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_add", errorstring);    //(Dispatch message)
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning analysis for %s (%s)", 
														ppObj[o]->m_pszName, ppObj[o]->m_pszDesc );
									g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_add", errorstring);    //(Dispatch message)
								}
							}

							if((ppObj[o]->m_lpfnDllCtrl==NULL)&&(ppObj[o]->m_hinstDLL))
							{
								ppObj[o]->m_lpfnDllCtrl = (LPFNDLLCTRL)GetProcAddress((ppObj[o]->m_hinstDLL), DLLFUNC_NAME);
								if(ppObj[o]->m_lpfnDllCtrl==NULL)
								{
									int err = GetLastError();
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d getting process address in module %s.  Cannot begin %s (%s)", 
														err, ppObj[o]->m_pszModule, ppObj[o]->m_pszName, ppObj[o]->m_pszDesc );
									g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_add", errorstring);    //(Dispatch message)
								}
							}

							if(ppObj[o]->m_lpfnDllCtrl)
							{

								ppObj[o]->m_ulFlags|=SENTINEL_FLAG_INIT;
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:control_module", "setting control module init 0x%08x", ppObj[o]->m_ulFlags); //  Sleep(250);//(Dispatch message)

/*

#define DLL_FLAG_DISPATCH     0x00010000 // allowed to get the dispatcher callback
#define DLL_FLAG_MSG			    0x00020000 // allowed to get the UI message callback
#define DLL_FLAG_ASRUN        0x00040000 // allowed to get the UI asrun callback
#define DLL_FLAG_STATUS		    0x00080000 // allowed to get the UI set status callback

#define DLLCMD_INIT						0x0010 // A signal to the DLL that it should run any internal initialization routines. The DLL may want to wait for settings to be set before proceeding to context-sensitive routines.
#define DLLCMD_SET_DISPATCH		0x0011 // Sets a callback function address so that the DLL may send messages to the calling application's message dispatch procedure, which writes log lines, sends emails out, etc depending on severity level and other criteria.
#define DLLCMD_SET_MSG				0x0012 // Sets a callback function address so that the DLL may send messages to the calling application's web user interface for messages.
#define DLLCMD_SET_ASRUN			0x0013 // Sets a callback function address so that the DLL may send messages to the calling application's web user interface for as-run items.
#define DLLCMD_GET_SETTINGS		0x0014 // Obtain a list of settings that the DLL would like to make sure exists in the user interface, for use inside the DLL.
#define DLLCMD_SET_SETTINGS		0x0015 // Set a list of settings with values from the web interface (database).
#define DLLCMD_SET_STATUS			0x0016 // Sets a callback function address so that the DLL may send status updates to the calling application's web user interface for module status.

#define DLLCMD_FREE_BUFFER		0x00ff // A call to free a buffer.  This will be called to free buffers that have been allocated by the calls specified above. Note that this is a different call from the call to free an object, regardless of whether the DLL uses the same allocation/deallocation functions in the call.  That command is given here for completeness, but is not used in initialization
#define DLLCMD_FREE_OBJECT		0x00fe // A call to free an object.
								
*/

								char* pch = (char*)malloc(64);
								if(pch)
								{
									sprintf(pch, "%d", ppObj[o]->m_nID);
								}
								char* pchOld = pch; 

								int nCode = ppObj[o]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_INIT);
								if(nCode<SENTINEL_SUCCESS)
								{
									/// do something, report?
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d initializing module %s: %s (%s)", nCode,
														ppObj[o]->m_pszModule, ppObj[o]->m_pszName, ppObj[o]->m_pszDesc );
									g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_add", errorstring);    //(Dispatch message)
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Module %s: %s (%s) initialized with [%s] code %d", 
										ppObj[o]->m_pszModule, ppObj[o]->m_pszName, ppObj[o]->m_pszDesc, (((pch)&&(pch!=pchOld))?pch:""), nCode );
									g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_add", errorstring);    //(Dispatch message)
								}

								if((pch)&&(pch!=pchOld))
								{
									try{ ppObj[o]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
									catch(...) {}
									pch = NULL;
								}

								if(pchOld)
								{
									try{ free(pchOld); } catch(...){}
								}

								if(ppObj[o]->m_ulFlags&DLL_FLAG_DISPATCH)
								{
//typedef void (__stdcall* LPFNDM)( unsigned long, char*, char*, char* );
//typedef int (__stdcall* LPFNMSG)( int, char*, char* );
//typedef int (__stdcall* LPFNAR)( int, char*, char*, char*, int);
// Generic callbacks for DLLs to send status message to module info interface.
//typedef int (__stdcall* LPFNSTATUS)( int, int, char*);


									nCode = ppObj[o]->m_lpfnDllCtrl((void**)(MessageDispatch), DLLCMD_SET_DISPATCH);
//AfxMessageBox("returned from DLLCMD_SET_DISPATCH");
									if(nCode<SENTINEL_SUCCESS)
									{
										/// do something, report?
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d setting message dispatcher pointer in module %s: %s (%s)", nCode,
															ppObj[o]->m_pszModule, ppObj[o]->m_pszName, ppObj[o]->m_pszDesc );
										g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_add", errorstring);    //(Dispatch message)
									}
								}
								if(ppObj[o]->m_ulFlags&DLL_FLAG_MSG)
								{
									nCode = ppObj[o]->m_lpfnDllCtrl((void**)(SendMsg), DLLCMD_SET_MSG);
									if(nCode<SENTINEL_SUCCESS)
									{
										/// do something, report?
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d setting UI message callback pointer in module %s: %s (%s)", nCode,
															ppObj[o]->m_pszModule, ppObj[o]->m_pszName, ppObj[o]->m_pszDesc );
										g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_add", errorstring);    //(Dispatch message)
									}
								}
								if(ppObj[o]->m_ulFlags&DLL_FLAG_ASRUN)
								{
									nCode = ppObj[o]->m_lpfnDllCtrl((void**)(SendAsRunMsg), DLLCMD_SET_ASRUN);
									if(nCode<SENTINEL_SUCCESS)
									{
										/// do something, report?
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d setting UI as run callback pointer in module %s: %s (%s)", nCode,
															ppObj[o]->m_pszModule, ppObj[o]->m_pszName, ppObj[o]->m_pszDesc );
										g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_add", errorstring);    //(Dispatch message)
									}
								}
								if(ppObj[o]->m_ulFlags&DLL_FLAG_STATUS)
								{
									nCode = ppObj[o]->m_lpfnDllCtrl((void**)(SetExternalModuleStatus), DLLCMD_SET_STATUS);
									if(nCode<SENTINEL_SUCCESS)
									{
										/// do something, report?
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d setting UI module status callback pointer in module %s: %s (%s)", nCode,
															ppObj[o]->m_pszModule, ppObj[o]->m_pszName, ppObj[o]->m_pszDesc );
										g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_add", errorstring);    //(Dispatch message)
									}
								}


								CCortexDBSetting** ppSettings = NULL;
								int nNumSettings = 0 ;


								pch = NULL;
								nCode = ppObj[o]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_GET_SETTINGS);
								if(nCode<SENTINEL_SUCCESS)
								{
									/// do something, report?
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d getting module settings from module %s: %s (%s)", nCode,
														ppObj[o]->m_pszModule, ppObj[o]->m_pszName, ppObj[o]->m_pszDesc );
									g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_add", errorstring);    //(Dispatch message)
								}
								else if((pch)&&(strlen(pch))) // has to have content...
								{
									// process these settings

/* // XML like this is expected:
<setting>
	<category>network</category>
	<category_display>Network Settings</category_display>
	<parameter>port</parameter>
	<parameter_display>TCPIP port</parameter_display>
	<value>12345</value>
	<desc>The TCPIP port for the main communication	listener</desc>
	<pattern>Integer</pattern>
	<min_len>null</min_len>
	<max_len>null</max_len>
	<min_value>1024</min_value>
	<max_value>65535</max_value>
	<sysonly>true</sysonly>
</setting>
<setting>
  ....
</setting>
*/

									// load this into and XML object and parse it out,
									// records are unique on category and parameter togther
									// do not overwrite VALUES on items that are already in the db.



//									need a top level node:
									char* pchXML = (char*)malloc(strlen(pch)+strlen("<xml></xml>")+1);
									if(pchXML)
									{
										sprintf(pchXML, "<xml>%s</xml>", pch);

										pDoc->put_async(VARIANT_FALSE);
										hr = pDoc->loadXML((char*)pchXML);
										if(hr!=VARIANT_TRUE)
										{
											g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:GetControlModules", "XML parse error: [%s]", pchXML );
										}
										else
										{
		if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_COMM) 
			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:GetControlModules", "begin xml parse");  //(Dispatch message)
											IXMLDOMNodeListPtr pChildNodes = pDoc->GetchildNodes();
//AfxMessageBox(pchXML);

/*
#define DLL_SETTING_XML_CATEGORY						0
#define DLL_SETTING_XML_CATEGORY_DISPLAY		1
#define DLL_SETTING_XML_PARAM								2
#define DLL_SETTING_XML_PARAM_DISPLAY				3
#define DLL_SETTING_XML_VALUE								4
#define DLL_SETTING_XML_DESC								5
#define DLL_SETTING_XML_PATTERN							6
#define DLL_SETTING_XML_MIN_LEN							7
#define DLL_SETTING_XML_MAX_LEN							8
#define DLL_SETTING_XML_MIN_VAL							9
#define DLL_SETTING_XML_MAX_VAL							10
#define DLL_SETTING_XML_SYSONLY							11

#define DLL_SETTING_XML_NODECOUNT						12
*/
//											IXMLDOMNodePtr pNodes[DLL_SETTING_XML_NODECOUNT];
//											int n=DLL_SETTING_XML_CATEGORY;
//											while(n<DLL_SETTING_XML_NODECOUNT){ pNodes[n]=NULL; n++; }

											if (pChildNodes != NULL)
											{
												IXMLDOMNodePtr pChild;
												while(pChild = pChildNodes->nextNode())
												{
													DOMNodeType nodeType;
													pChild->get_nodeType(&nodeType);

													if(nodeType == NODE_ELEMENT)
													{
														char tag[MAX_PATH]; 
														//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
														util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

//AfxMessageBox(tag);

														if(strcmp("xml", tag)==0)  // this one is guaranteed, basically!
														{

															pChildNodes = pChild->GetchildNodes();
															if (pChildNodes != NULL)
															{

																while(pChild = pChildNodes->nextNode()) // loop thru the settings.
																{
//AfxMessageBox("node");

																	pChild->get_nodeType(&nodeType);

																	if(nodeType == NODE_ELEMENT)
																	{
																		//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
																		util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));
//AfxMessageBox(tag);

																		if(strcmp("setting", tag)==0)
																		{
																			IXMLDOMNodeListPtr pSettingChildNodes = pChild->GetchildNodes();
																			if (pSettingChildNodes != NULL)
																			{
																				//new node with children!

																				CCortexDBSetting** ppNewSettings = new CCortexDBSetting*[nNumSettings+1];
																				if(ppNewSettings)
																				{
																					if(ppSettings)
																					{
																						int s=0;
																						while(s<nNumSettings)
																						{
																							ppNewSettings[s] = ppSettings[s];
																							s++;
																						}

																						try{ delete [] ppSettings; }catch(...){}
																					}
																					ppSettings = ppNewSettings;


																					CCortexDBSetting* pSettingObj = new CCortexDBSetting;
																					if(pSettingObj)
																					{
																						ppSettings[nNumSettings] = pSettingObj;
																						pSettingObj->m_n_sortorder = nNumSettings+1;

																						while(pChild = pSettingChildNodes->nextNode()) // loop thru the settings.
																						{
																							pChild->get_nodeType(&nodeType);

																							if(nodeType == NODE_ELEMENT)
																							{
																								//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
																								util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));
//AfxMessageBox(tag);

																								if(strcmp("category", tag)==0)
																								{
																									pSettingObj->m_psz_category_vc64 = msg.XMLTextNodeValue(pChild);
																									if(pSettingObj->m_psz_category_vc64)
																									{
																										if(strlen(pSettingObj->m_psz_category_vc64)>64) pSettingObj->m_psz_category_vc64[64]=0;
																									}
																								}
																								else
																								if(strcmp("category_display", tag)==0)
																								{
																									pSettingObj->m_psz_category_display_name_vc64 = msg.XMLTextNodeValue(pChild);
																									if(pSettingObj->m_psz_category_display_name_vc64)
																									{
																										if(strlen(pSettingObj->m_psz_category_display_name_vc64)>64) pSettingObj->m_psz_category_display_name_vc64[64]=0;
																									}
																								}
																								else
																								if(strcmp("parameter", tag)==0)
																								{
																									pSettingObj->m_psz_parameter_vc64 = msg.XMLTextNodeValue(pChild);
																									if(pSettingObj->m_psz_parameter_vc64)
																									{
																										if(strlen(pSettingObj->m_psz_parameter_vc64)>64) pSettingObj->m_psz_parameter_vc64[64]=0;
																									}
																								}
																								else
																								if(strcmp("parameter_display", tag)==0)
																								{
																									pSettingObj->m_psz_parameter_display_name_vc64 = msg.XMLTextNodeValue(pChild);
																									if(pSettingObj->m_psz_parameter_display_name_vc64)
																									{
																										if(strlen(pSettingObj->m_psz_parameter_display_name_vc64)>64) pSettingObj->m_psz_parameter_display_name_vc64[64]=0;
																									}
																								}
																								else
																								if(strcmp("value", tag)==0)
																								{
																									pSettingObj->m_psz_value_vc256 = msg.XMLTextNodeValue(pChild);
																									if(pSettingObj->m_psz_value_vc256)
																									{
																										if(strlen(pSettingObj->m_psz_value_vc256)>256) pSettingObj->m_psz_value_vc256[256]=0;
																									}
																								}
																								else
																								if(strcmp("desc", tag)==0)
																								{
																									pSettingObj->m_psz_description_vc256 = msg.XMLTextNodeValue(pChild);
																									if(pSettingObj->m_psz_description_vc256)
																									{
																										if(strlen(pSettingObj->m_psz_description_vc256)>256) pSettingObj->m_psz_description_vc256[256]=0;
																									}
																								}
																								else
																								if(strcmp("pattern", tag)==0)
																								{
																									pSettingObj->m_psz_pattern_vc64 = msg.XMLTextNodeValue(pChild);
																									if(pSettingObj->m_psz_pattern_vc64)
																									{
																										if(strlen(pSettingObj->m_psz_pattern_vc64)>64) pSettingObj->m_psz_pattern_vc64[64]=0;
																									}
																								}
																								else
																								if(strcmp("min_len", tag)==0)
																								{
																									char* pchSetting = msg.XMLTextNodeValue(pChild);
																									if(pchSetting)
																									{
																										if(stricmp(pchSetting, "null")!=0)
																										{
																											pSettingObj->m_n_min_len = atoi(pchSetting);
																										}
																										else
																										{
																											pSettingObj->m_n_min_len = -1;
																										}
																										try{ free(pchSetting); }catch(...){}
																									}
																								}
																								else
																								if(strcmp("max_len", tag)==0)
																								{
																									char* pchSetting = msg.XMLTextNodeValue(pChild);
																									if(pchSetting)
																									{
																										if(stricmp(pchSetting, "null")!=0)
																										{
																											pSettingObj->m_n_max_len = atoi(pchSetting);
																										}
																										else
																										{
																											pSettingObj->m_n_max_len = -1;
																										}
																										try{ free(pchSetting); }catch(...){}
																									}
																								}
																								else
																								if(strcmp("min_value", tag)==0)
																								{
																									pSettingObj->m_psz_min_value_vc64 = msg.XMLTextNodeValue(pChild);
																									if(pSettingObj->m_psz_min_value_vc64)
																									{
																										if(strlen(pSettingObj->m_psz_min_value_vc64)>64) pSettingObj->m_psz_min_value_vc64[64]=0;
																									}
																								}
																								else
																								if(strcmp("max_value", tag)==0)
																								{
																									pSettingObj->m_psz_max_value_vc64 = msg.XMLTextNodeValue(pChild);
																									if(pSettingObj->m_psz_max_value_vc64)
																									{
																										if(strlen(pSettingObj->m_psz_max_value_vc64)>64) pSettingObj->m_psz_max_value_vc64[64]=0;
																									}
																								}
																								else
																								if(strcmp("sysonly", tag)==0)
																								{
																									char* pchSetting = msg.XMLTextNodeValue(pChild);
																									if(pchSetting)
																									{
																										if((stricmp(pchSetting, "true")==0)||(strcmp(pchSetting, "1")==0))
																										{
																											pSettingObj->m_b_sys_only = true;
																										}
																										try{ free(pchSetting); }catch(...){}
																									}
																								}
																							}
																						}
																						nNumSettings++;
//AfxMessageBox("setting was parsed");
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
//AfxMessageBox("out");
										try{ free(pchXML); } catch(...){}
									}
									else
									{
										// no settings, that is fine.
									}

								}
//AfxMessageBox("calling get settings free");

								if(pch)
								{
									try{ ppObj[o]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
									catch(...) {}
									pch = NULL;
								}

//AfxMessageBox("called get settings free");

								//here, swap settings out, add to the database if not there, etc.

								if((ppSettings)&&(nNumSettings>0))
								{
									// need a new connection to retrieve while in the retrieve.
									CDBUtil db;
									CDBconn* pdbConn = db.CreateNewConnection(g_psentinel->m_settings.m_pszDSN, g_psentinel->m_settings.m_pszUser, g_psentinel->m_settings.m_pszPW);
									if(pdbConn)
									{
										if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
										{
											g_psentinel->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Sentinel:database_connect(get_control_modules)", errorstring);  //(Dispatch message)

										}
										else
										{
											int d=0;
											while(d<nNumSettings)
											{
												if((ppSettings[d])&&(ppSettings[d]->m_psz_category_vc64)&&(ppSettings[d]->m_psz_parameter_vc64))  //category and param must exist
												{

/*
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT category_display_name, value, parameter_display_name, min_len, \
max_len, pattern, min_value, max_value, description, sys_only  FROM %s WHERE category = '�%d�%s' AND parameter = '%s'",
				(g_psentinel->m_settings.m_pszSettings?g_psentinel->m_settings.m_pszSettings:"Settings"),
				ppObj[o]->m_nID, 
				ppSettings[d]->m_psz_category_vc64, 
				ppSettings[d]->m_psz_parameter_vc64
				);
*/
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT value FROM %s WHERE category = '�%d�%s' AND parameter = '%s'",
														(g_psentinel->m_settings.m_pszSettings?g_psentinel->m_settings.m_pszSettings:"Settings"),
														ppObj[o]->m_nID, 
														ppSettings[d]->m_psz_category_vc64, 
														ppSettings[d]->m_psz_parameter_vc64
														);
													ppSettings[d]->ulFlags = 0;

//AfxMessageBox("retrieving stuff");

													CRecordset* prsset = db.Retrieve(pdbConn, szSQL, dberrorstring);
													if(prsset)
													{
														if(!prsset->IsEOF())
														{
															ppSettings[d]->ulFlags = 1; //exists

															try
															{
																CString szTemp;
/*
																prsset->GetFieldValue("category_display_name", szTemp);
																if(
																	  (szTemp.GetLength())
																	&&(ppSettings[d]->m_psz_category_display_name_vc64)
																	&&(szTemp.Compare(ppSettings[d]->m_psz_category_display_name_vc64)
																	)
																{
																	ppSettings[d]->ulFlags |= 2;  //modify
																}
*/

																// just do value for now.

																prsset->GetFieldValue("value", szTemp);
																if(
																		((ppSettings[d]->m_psz_value_vc256==NULL)&&(szTemp.GetLength()))
																	||((ppSettings[d]->m_psz_value_vc256)&&(szTemp.Compare(ppSettings[d]->m_psz_value_vc256)))
																	)
																{
																	if(ppSettings[d]->m_psz_value_vc256)
																	{
																		try{ free(ppSettings[d]->m_psz_value_vc256); } catch(...){}
																	}
																	ppSettings[d]->m_psz_value_vc256 = (char*)malloc(szTemp.GetLength()+1);
																	if(ppSettings[d]->m_psz_value_vc256)
																	{
																		sprintf(ppSettings[d]->m_psz_value_vc256, "%s", szTemp);
																	}
																}

															}
															catch(...)
															{
															}														
														}

														prsset->Close();
														try{ delete prsset; } catch(...){}
													}



													if(ppSettings[d]->ulFlags==0)
													{
														// didn't exist in db, must add it

														CCortexDBSetting dbs;
														dbs.m_psz_category_vc64 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_category_vc64, 64);
														dbs.m_psz_category_display_name_vc64 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_category_display_name_vc64, 64);
														dbs.m_psz_parameter_vc64 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_parameter_vc64, 64);
														dbs.m_psz_value_vc256 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_value_vc256, 256);
														dbs.m_psz_parameter_display_name_vc64 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_parameter_display_name_vc64, 64);
														dbs.m_psz_pattern_vc64 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_pattern_vc64, 64);
														dbs.m_psz_min_value_vc64 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_min_value_vc64, 64);
														dbs.m_psz_max_value_vc64 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_max_value_vc64, 64);
														dbs.m_psz_description_vc256 = db.LimitEncodeQuotes(ppSettings[d]->m_psz_description_vc256, 256);
														char min_len[MAX_PATH];
														char max_len[MAX_PATH];
														if(ppSettings[d]->m_n_min_len==-1)
														{
															strcpy(min_len, "NULL");
														}
														else
														{
															sprintf(min_len, "%d", ppSettings[d]->m_n_min_len);
														}
														if(ppSettings[d]->m_n_max_len==-1)
														{
															strcpy(max_len, "NULL");
														}
														else
														{
															sprintf(max_len, "%d", ppSettings[d]->m_n_max_len);
														}
//AfxMessageBox("about to format");

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
category, category_display_name, parameter, value, parameter_display_name, min_len, max_len, pattern, min_value, max_value, sortorder, description, sys_only) \
VALUES ('�%d�%s', %s%s%s, '%s', %s%s%s, %s%s%s, %s, %s, %s%s%s, %s%s%s, %s%s%s, %d, %s%s%s, %d)",
				(g_psentinel->m_settings.m_pszSettings?g_psentinel->m_settings.m_pszSettings:"Settings"),

				ppObj[o]->m_nID, (dbs.m_psz_category_vc64?dbs.m_psz_category_vc64:""), 

				(dbs.m_psz_category_display_name_vc64?"'":""),
				(dbs.m_psz_category_display_name_vc64?dbs.m_psz_category_display_name_vc64:"NULL"), 
				(dbs.m_psz_category_display_name_vc64?"'":""),
				
				(dbs.m_psz_parameter_vc64?dbs.m_psz_parameter_vc64:""),
				
				(dbs.m_psz_value_vc256?"'":""),
				(dbs.m_psz_value_vc256?dbs.m_psz_value_vc256:"NULL"), 
				(dbs.m_psz_value_vc256?"'":""),

				(dbs.m_psz_parameter_display_name_vc64?"'":""),
				(dbs.m_psz_parameter_display_name_vc64?dbs.m_psz_parameter_display_name_vc64:"NULL"), 
				(dbs.m_psz_parameter_display_name_vc64?"'":""),

				min_len, 
				max_len,

				(dbs.m_psz_pattern_vc64?"'":""),
				(dbs.m_psz_pattern_vc64?dbs.m_psz_pattern_vc64:"NULL"), 
				(dbs.m_psz_pattern_vc64?"'":""),

				(dbs.m_psz_min_value_vc64?"'":""),
				(dbs.m_psz_min_value_vc64?dbs.m_psz_min_value_vc64:"NULL"), 
				(dbs.m_psz_min_value_vc64?"'":""),

				(dbs.m_psz_max_value_vc64?"'":""),
				(dbs.m_psz_max_value_vc64?dbs.m_psz_max_value_vc64:"NULL"), 
				(dbs.m_psz_max_value_vc64?"'":""),

				(ppObj[o]->m_nID*1000)+d+1,

				(dbs.m_psz_description_vc256?"'":""),
				(dbs.m_psz_description_vc256?dbs.m_psz_description_vc256:"NULL"), 
				(dbs.m_psz_description_vc256?"'":""),

				(ppSettings[d]->m_b_sys_only?1:0)
				);
//AfxMessageBox(szSQL);

														if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
														{
															//**MSG
												g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:database_connect(get_control_modules)", errorstring); // Sleep(50); //(Dispatch message)
														}

													}

												}


												d++;
											}

										}


										db.RemoveConnection(pdbConn);

									}

								}

//AfxMessageBox("done with getting settings");

								//pch = /// TODO make up the settings buffer

/*
								Consists of XML of this nature:
<setting>
	<category>network</category>
	<parameter>port</parameter>
	<value>12345</value>
</setting>
<setting>
	...
</setting>
*/

								if((ppSettings)&&(nNumSettings>0))
								{
									pch = NULL;  // just for safety

									int d=0;
									int nLen = 0;
									while(d<nNumSettings)
									{
										strcpy(szSQL, "");
										if(ppSettings[d])
										{
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN, "<setting><category>%s</category><parameter>%s</parameter><value>%s</value></setting>", 
												ppSettings[d]->m_psz_category_vc64?ppSettings[d]->m_psz_category_vc64:"", 
												ppSettings[d]->m_psz_parameter_vc64?ppSettings[d]->m_psz_parameter_vc64:"", 
												ppSettings[d]->m_psz_value_vc256?ppSettings[d]->m_psz_value_vc256:"");
										
										}
										
										int nRec = strlen(szSQL);
										if(nRec)
										{
											char* pchNew = (char*)malloc(nLen + nRec + 1);
											if(pchNew)
											{
												if(pch)
												{
													memcpy(pchNew, pch, nLen);
													try {free(pch);} catch(...){}
												}
												memcpy(pchNew+nLen, szSQL, nRec);
												nLen+=nRec;
												*(pchNew+nLen) = 0;

												pch = pchNew;
											}
										}
										d++;
									}


									nCode = ppObj[o]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_SET_SETTINGS);
									if(nCode<SENTINEL_SUCCESS)
									{
										/// do something, report?
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d setting module settings in module %s: %s (%s)", nCode,
															ppObj[o]->m_pszModule, ppObj[o]->m_pszName, ppObj[o]->m_pszDesc );
										g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_add", errorstring);    //(Dispatch message)
									}

									if(pch)
									{
										try{ free(pch); }
										catch(...) {}
									}

									// done with the settings, so delete:

									while(d<nNumSettings)
									{
										if(ppSettings[d])
										{
											try{ delete ppSettings[d]; } catch(...){}
										}
										d++;
									}
									try{ delete [] ppSettings; } catch(...){}

								}

								nCode = ppObj[o]->m_lpfnDllCtrl((void**)(NULL), DLLCMD_RELEASE);
								if(nCode<SENTINEL_SUCCESS)
								{
									/// do something, report?
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d setting module settings in module %s: %s (%s)", nCode,
														ppObj[o]->m_pszModule, ppObj[o]->m_pszName, ppObj[o]->m_pszDesc );
									g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_add", errorstring);    //(Dispatch message)
								}

							

							
/*
							if(ppObj[o]->m_bAutomationThreadStarted == false)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning analysis for %s (%s)", 
													m_ppControlObj[nIndex]->m_pszName,m_ppControlObj[nIndex]->m_pszDesc );
								g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:destination_change", errorstring);    //(Dispatch message)

								//**** if connect set following status
								if(_beginthread(TabulatorAutomationThread, 0, (void*)ppObj[o])==-1)
								{
									//error.

									//**MSG
								}
								//**** should check return value....
							}

/*
							Sleep(30);
							if(ppObj[o]->m_bNearAnalysisThreadStarted == false)
							{
								if(_beginthread(TabulatorNearAnalysisThread, 0, (void*)ppObj[o])==-1)
								{
									//error.

									//**MSG
								}
								//**** should check return value....
							}

							Sleep(30);
							if(ppObj[o]->m_bTriggerThreadStarted == false)
							{
								if(_beginthread(TabulatorTriggerThread, 0, (void*)ppObj[o])==-1)
								{
									//error.

									//**MSG
								}
								//**** should check return value....
							}
*/
		//					Sleep(30);
							}
							ppObj[o]->m_ulFlags |= SENTINEL_FLAG_FOUND;
							
						}
						else
						{
							try{delete pscno;} catch(...){}
						}
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			try{ delete prs;} catch(...){}
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)
	LeaveCriticalSection(&m_critSQL);

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<m_nNumControlObjects)
			{
				if((m_ppControlObj)&&(m_ppControlObj[nIndex]))
				{
					if((m_ppControlObj[nIndex]->m_ulFlags)&SENTINEL_FLAG_FOUND)
					{
						(m_ppControlObj[nIndex]->m_ulFlags) &= ~SENTINEL_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if((m_ppControlObj[nIndex])&&(m_ppControlObj[nIndex]->m_hinstDLL))
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending control for %s (%s)", 
														m_ppControlObj[nIndex]->m_pszName,m_ppControlObj[nIndex]->m_pszDesc );  
							g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
//									m_ppControlObj[nTemp]->m_pDlg->OnDisconnect();

							if(m_ppControlObj[nIndex]->m_lpfnDllCtrl)
							{
							//**** disconnect
//							m_ppControlObj[nIndex]->m_bKillAutomationThread = true;

/*
#define DLLCMD_DEINIT					0x0020 // A signal to the DLL that it should run any internal de-initialization routines. After the DEINIT command is received the DLL should prepare to finalize any messaging back to the calling application.
#define DLLCMD_MSGEND					0x0021 // A signal to the DLL that it should not send any further messages through any messaging callbacks, including as-run.
#define DLLCMD_QUERY					0x0022 // A query to the DLL to ask it if it has finished deinitialization.  This command will repeat until a succesful return, or a de-init timeout has elapsed.
#define DLLCMD_DLLEND					0x002f // A signal to the DLL that it is about to be unloaded.
*/
								char* pch = (char*)malloc(64);
								if(pch)
								{
									sprintf(pch, "%d", m_ppControlObj[nIndex]->m_nID);
								}
								char* pchOld = pch; 

								int nCode = m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_DEINIT);
								if(nCode<SENTINEL_SUCCESS)
								{
									/// do something, report?
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d de-initializing module %s: %s (%s)", nCode,
														m_ppControlObj[nIndex]->m_pszModule, m_ppControlObj[nIndex]->m_pszName, m_ppControlObj[nIndex]->m_pszDesc );
									g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Module %s: %s (%s) de-initialized with [%s] code %d", 
										m_ppControlObj[nIndex]->m_pszModule, m_ppControlObj[nIndex]->m_pszName, m_ppControlObj[nIndex]->m_pszDesc, (((pch)&&(pch!=pchOld))?pch:""), nCode );
									g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
								}

								if((pch)&&(pch!=pchOld))
								{
									try{ m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
									catch(...) {}
									pch = NULL;
									if(pchOld)
									{
										try{ free(pchOld); }
										catch(...) {}
										pchOld = NULL;
									}
								}
								if(pch)
								{
									try{ free(pch); }
									catch(...) {}
									pch = NULL;
								}

								pch = (char*)malloc(64);
								if(pch)
								{
									sprintf(pch, "%d", m_ppControlObj[nIndex]->m_nID);
								}
								pchOld = pch; 

								nCode = m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_MSGEND);
								if(nCode<SENTINEL_SUCCESS)
								{
									/// do something, report?
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d commanding message end in module %s: %s (%s)", nCode,
														m_ppControlObj[nIndex]->m_pszModule, m_ppControlObj[nIndex]->m_pszName, m_ppControlObj[nIndex]->m_pszDesc );
									g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Module %s: %s (%s) ended messaging with [%s] code %d", 
										m_ppControlObj[nIndex]->m_pszModule, m_ppControlObj[nIndex]->m_pszName, m_ppControlObj[nIndex]->m_pszDesc, (((pch)&&(pch!=pchOld))?pch:""), nCode );
									g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
								}

								if((pch)&&(pch!=pchOld))
								{
									try{ m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
									catch(...) {}
									pch = NULL;
									if(pchOld)
									{
										try{ free(pchOld); }
										catch(...) {}
										pchOld = NULL;
									}
								}
								if(pch)
								{
									try{ free(pch); }
									catch(...) {}
									pch = NULL;
								}

								bool bReleased = false;
								_timeb tmChk;
								_ftime(&tmChk);

								if(m_ppControlObj[nIndex]->m_nDeinitTimeoutMS>0)
								{
									tmChk.time += m_ppControlObj[nIndex]->m_nDeinitTimeoutMS/1000;
									tmChk.millitm += m_ppControlObj[nIndex]->m_nDeinitTimeoutMS%1000;
									while(tmChk.millitm>999)
									{
										tmChk.millitm-=1000;
										tmChk.time++;
									}
								}
								else
								{
									tmChk.time++;
								}

								_timeb tmNow;
								_ftime(&tmNow);

								do
								{

									pch = (char*)malloc(64);
									if(pch)
									{
										sprintf(pch, "%d", m_ppControlObj[nIndex]->m_nID);
									}
									pchOld = pch; 

									int nCode = m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_QUERY);
									if(nCode<SENTINEL_SUCCESS)
									{
										/// do something, report?
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d querying control end in module %s: %s (%s)", nCode,
															m_ppControlObj[nIndex]->m_pszModule, m_ppControlObj[nIndex]->m_pszName, m_ppControlObj[nIndex]->m_pszDesc );
										g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
									}
									else
									{
										bReleased = true;
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Module %s: %s (%s) query end succeeded with [%s] code %d", 
											m_ppControlObj[nIndex]->m_pszModule, m_ppControlObj[nIndex]->m_pszName, m_ppControlObj[nIndex]->m_pszDesc, (((pch)&&(pch!=pchOld))?pch:""), nCode );
										g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
									}

									if((pch)&&(pch!=pchOld))
									{
										try{ m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
										catch(...) {}
										pch = NULL;
										if(pchOld)
										{
											try{ free(pchOld); }
											catch(...) {}
											pchOld = NULL;
										}
									}
									if(pch)
									{
										try{ free(pch); }
										catch(...) {}
										pch = NULL;
									}

									_timeb tmSleep;
									_ftime(&tmSleep);

									tmSleep.time +=	g_psentinel->m_settings.m_ulModuleEndQueryIntervalMS/1000;
									tmSleep.millitm +=  (unsigned short)(g_psentinel->m_settings.m_ulModuleEndQueryIntervalMS%1000);
									while(tmSleep.millitm>999)
									{
										tmSleep.millitm-=1000;
										tmSleep.time++;
									}

									while (
													(!bReleased)
												&&(
														(tmNow.time<tmSleep.time)
													||((tmNow.time==tmSleep.time)&&(tmNow.millitm<tmSleep.millitm))
													)
//															&&(!g_bKillThread) //don't do this on query end
												)
									{												
										Sleep(1);
										_ftime(&tmNow);
									}

								}	while (
													(!bReleased)
												&&(
														(tmNow.time<tmChk.time)
													||((tmNow.time==tmChk.time)&&(tmNow.millitm<tmChk.millitm))
													)
												);

								//g_adc.DisconnectServer(m_ppControlObj[nIndex]->m_pszServerName);
								//**** should check return value....


								pch = (char*)malloc(64);
								if(pch)
								{
									sprintf(pch, "%d", m_ppControlObj[nIndex]->m_nID);
								}
								pchOld = pch; 

								nCode = m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_DLLEND);
								if(nCode<SENTINEL_SUCCESS)
								{
									/// do something, report?
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d commanding control end in module %s: %s (%s)", nCode,
														m_ppControlObj[nIndex]->m_pszModule, m_ppControlObj[nIndex]->m_pszName, m_ppControlObj[nIndex]->m_pszDesc );
									g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Module %s: %s (%s) ended control with [%s] code %d", 
										m_ppControlObj[nIndex]->m_pszModule, m_ppControlObj[nIndex]->m_pszName, m_ppControlObj[nIndex]->m_pszDesc, (((pch)&&(pch!=pchOld))?pch:""), nCode );
									g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
								}

								if((pch)&&(pch!=pchOld))
								{
									try{ m_ppControlObj[nIndex]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_FREE_BUFFER); }
									catch(...) {}
									pch = NULL;
									if(pchOld)
									{
										try{ free(pchOld); }
										catch(...) {}
										pchOld = NULL;
									}
								}
								if(pch)
								{
									try{ free(pch); }
									catch(...) {}
									pch = NULL;
								}
							}

							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ended control for %s (%s)", 
													m_ppControlObj[nIndex]->m_pszName,m_ppControlObj[nIndex]->m_pszDesc );  
							g_psentinel->m_msgr.DM(MSG_ICONINFO, NULL, "Sentinel:module_remove", errorstring);    //(Dispatch message)
							

							try
							{
//								AfxFreeLibrary(m_ppControlObj[nIndex]->m_hinstDLL);  // crashes!  not sure why - lets try again.
							}
							catch(...)
							{
							}

							m_ppControlObj[nIndex]->m_hinstDLL = NULL;
							m_ppControlObj[nIndex]->m_lpfnDllCtrl = NULL;

							Sleep(100); // let it settle

							// OK HERE, but not in the mail thread shutdown, it means that someone has removed a module completely.
							// so we remove its settings from the DB


							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE category LIKE '�%d�%%'",
									g_psentinel->m_settings.m_pszSettings?g_psentinel->m_settings.m_pszSettings:"Settings",  // table name
									m_ppControlObj[nIndex]->m_nID
								);

							EnterCriticalSection(&m_critSQL);
							if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
							{
								//**MSG
					g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:GetControlModules", "SQL error: %s", dberrorstring); // Sleep(50); //(Dispatch message)
							}
							LeaveCriticalSection(&m_critSQL);

							try
							{
								delete m_ppControlObj[nIndex];
							}
							catch(...)
							{
							}

							m_nNumControlObjects--;

							int nTemp=nIndex;
							while(nTemp<m_nNumControlObjects)
							{
								m_ppControlObj[nTemp]=m_ppControlObj[nTemp+1];
								nTemp++;
							}
							m_ppControlObj[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "B leaving extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)
	LeaveCriticalSection(&m_critExternalModules);
//g_psentinel->m_msgr.DM(MSG_ICONSTOP, NULL, "Sentinel:process_check", "B left extmod crit %d", clock() ); // Sleep(50); //(Dispatch message)

//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "leaving GetControlModules 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
		else
		{
	LeaveCriticalSection(&m_critSQL);

			g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:GetControlModules", "SQL error on retrieve: %s", dberrorstring); // Sleep(50); //(Dispatch message)
			if(pszInfo)
			{
				try{ strcpy( pszInfo, dberrorstring); } catch(...){}
			}
		}
		if(pSafety) pSafety->Release();
		try
		{  
			CoUninitialize(); //XML
		}
		catch(...)
		{
			if(g_psentinel) g_psentinel->m_msgr.DM(MSG_ICONERROR, NULL, "Sentinel:GetControlModules", "Exception in CoUninitialize");  //(Dispatch message)
		}

	}
//			g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return SENTINEL_ERROR;
}


int CSentinelData::CheckPluginOnConn(char* pszPlugInChannelIds, char* pszConnServerName)
{
if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:CheckPluginOnConn", "checking plugin on conn with [%s] [%s] %d %d", pszPlugInChannelIds, pszConnServerName, m_ppChannelObj, m_nNumChannelObjects); //  Sleep(250);//(Dispatch message)
	if((pszPlugInChannelIds)&&(pszConnServerName)&&(m_ppChannelObj)&&(m_nNumChannelObjects))
	{
		unsigned long ulBufferLength = strlen(pszPlugInChannelIds); // really, there has to be data
		if(pszPlugInChannelIds)
		{
			CSafeBufferUtil sbu;
			char* pch = sbu.Token(pszPlugInChannelIds, ulBufferLength, " ,.:-|/\\;+");  // expect commas but really can be anything non numerical.  let's just use some punctuation.
			while(pch)
			{
				int nChannelID = atoi(pch);
				if(nChannelID>0)
				{
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:CheckPluginOnConn", "checking channel %d", nChannelID); //  Sleep(250);//(Dispatch message)

					int n=0;
	EnterCriticalSection(&m_critChannels);
					while(n<m_nNumChannelObjects)
					{
						if(m_ppChannelObj[n])
						{
							if((m_ppChannelObj[n]->m_nChannelID==nChannelID)&&(m_ppChannelObj[n]->m_pszServerName))
							{
//if(g_psentinel->m_settings.m_ulDebug&SENTINEL_DEBUG_CONTROL)	
//	g_psentinel->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:CheckPluginOnConn", "checking [%s] vs [%s]", pszConnServerName, m_ppChannelObj[n]->m_pszServerName); //  Sleep(250);//(Dispatch message)

								if(strcmp(pszConnServerName, m_ppChannelObj[n]->m_pszServerName)==0)
								{
	LeaveCriticalSection(&m_critChannels);

									return SENTINEL_SUCCESS;
								}
							}

						}
						n++;
					}
	LeaveCriticalSection(&m_critChannels);


				}
				else
				{
					pch = sbu.Token(NULL, NULL, " ,.:-|/\\;+");
				}
			}
		}

	}
	return SENTINEL_ERROR;
}