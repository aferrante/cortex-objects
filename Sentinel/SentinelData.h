// SentinelData.h: interface for the CSentinelData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SENTINELDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
#define AFX_SENTINELDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <winsock2.h>
#include <sys/timeb.h>
#include "../../Common/TXT/BufferUtil.h" 
#include "../../Common/LAN/NetUtil.h" 
#include "../../Common/MFC/ODBC/DBUtil.h"
#include "../../Common/API/Harris/ADC.h"
#include "../../Common/KEY/LicenseKey.h"




// Generic callbacks for DLLs to send things to the modules Message and As-Run tables.
//typedef int (__stdcall* LPFNMSG)( int, char*, char* );
//typedef int (__stdcall* LPFNAR)( int, char*, char*, char*, int);
// Generic callbacks for DLLs to send status message to module info interface.
//typedef int (__stdcall* LPFNSTATUS)( int, int, char*);


void   __stdcall MessageDispatch( unsigned long ulFlags, char* pszDestination, char* pszCaller, char* pszMessage );
int		 __stdcall SendMsg( int nType, char* pszSender, char* pszMessage );
int		 __stdcall SendAsRunMsg( int nType, char* pszSender, char* pszMessage, char* pszEventID, int nChannelID );
int		 __stdcall SetExternalModuleStatus( int nModuleID, int nType, char* pszStatus );


typedef struct workingset
{
	BOOL bAdd;  // c.f. delete
	int nRefCount;
	char* pszKey;
} workingset;



class CSentinelUtil  
{
public:
	CSentinelUtil();
	virtual ~CSentinelUtil();

	unsigned long DateFromName(char* pszFilename);
	int ChannelFromName(char* pszFilename, char** ppszServer=NULL, int* pnList=NULL, void** ppChannel=NULL);
	char* PreprocessDateSpecifiers(char* pszFormatString, long lDate);
	unsigned long FileTimeToUnixTime(FILETIME ft, BOOL bLocal);

	// these imported from CADC, but made local so a local uitl object can be made per thread and not call the single global gadc.
	unsigned long	ConvertHMSFToMilliseconds(unsigned char ucHours, unsigned char ucMinutes, unsigned char ucSeconds, unsigned char ucFrames, unsigned char ucFrameBasis=0x29, unsigned long ulFlags=ADC_NORMAL);  // pass in the systemfrx from the system data
	void	ConvertMillisecondsToHMSF(unsigned long ulMilliseconds, unsigned char* pucHours, unsigned char* pucMinutes, unsigned char* pucSeconds, unsigned char* pucFrames, unsigned char ucFrameBasis=0x29, unsigned long ulFlags=ADC_NORMAL);// pass in the systemfrx from the system data
};




// these are data reference objects, pulled from the database.
// actual connection, list, and event objects are in the CADC object
class CSentinelConnectionObject  
{
public:
	CSentinelConnectionObject();
	virtual ~CSentinelConnectionObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	char* m_pszServerName;
	char* m_pszClientName;
	char* m_pszDesc;

	CAConnection* m_pAPIConn;  // pointer to the associated API connection.
	CRITICAL_SECTION m_critAPI; 

	// db
	double m_dblLastServerTimeMS;
	double m_dblServerTimeIntervalMS;
	double m_dblUpdateTime;
	bool m_bListStatusToDB;

	// control
	bool m_bKillConnThread;
	bool m_bConnThreadStarted;
};


class CSentinelEventObject  
{
public:
	CSentinelEventObject();
	virtual ~CSentinelEventObject();

	int m_nFlags;  // inserted in db, processed, etc.
	int	m_nDiffers; 

	CAEvent m_event; // the ADC event values.
	int m_uid;  // the unique Event ID
	int m_nPrecedingUid;  // the unique Event ID of the preceding event
	int m_nPosition;  // the Harris list position
	int m_nPreviousPosition;  // the previous Harris list position
	int m_nRefCount;  // the reference count, if a secondary;
	double m_dblTime;
	double m_dblCalcTime;
	int	   m_nCalcDur;
	char* m_pszEncodedKey;
	char* m_pszEncodedID;
	char* m_pszEncodedTitle;
	char* m_pszEncodedRecKey;
	char* m_pszEncodedData;

	// parent info stored in parent obj.
	void*  m_pParent;
	double m_dblUpdateTime;

	int  IsIdentical(CSentinelEventObject* pEvent);  // is exactly identical
	bool IsMatch(CSentinelEventObject* pEvent, double dblTimeToleranceMS, double dblTimeOffset, double* pdblTimeOffset, int nMatchMode=SENTINEL_MATCH_TIME, char* pszSource=NULL);      // matches on time, id, etc...
	bool IsNoDeviceType(); 
};




class CSentinelTrafficObject  
{
public:
	CSentinelTrafficObject();
	virtual ~CSentinelTrafficObject();

	CAList m_List; // the ADC events in a list.
	char* m_pszFileName;  // full file path
	CSentinelEventObject** m_ppevents;
	int m_nNumEvents;
	unsigned long m_ulDate; //unixtime
	int m_nID;  // unique ID

	// channel info stored in channel obj.
	void*  m_pChannel;

	CDBUtil m_db; // for encode quotes utility

	int AssembleKey(int index, char* pszFormat, BYTE bcdFrameRate);
//	int UpdateEventSQL(int index, CDBUtil* pdb,	CDBconn* pdbConn, double dblServerTime); // not used, done inline
	int FindTrafficUid(int uid);  //returns index
	int InsertTrafficEvent(CSentinelEventObject* pevent, int index=-1);  // -1 = add to end.
	int DeleteTrafficEvent(int index);  // removes from array,
	int DeleteAllTrafficEvents();  // removes all from array, and array itself
};




class CSentinelChannelObject  
{
public:
	CSentinelChannelObject();
	virtual ~CSentinelChannelObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	int m_nChannelID;  // the unique Channel ID within Sentinel setup. (assigned externally)
	int m_nHarrisListID;  // the 1-based List # on the associated ADC-100 server

	char* m_pszServerName;
	char* m_pszDesc;
	char m_pszSource[MAX_MESSAGE_LENGTH];  // message source 

	CAConnection** m_ppAPIConn;  // address of the pointer to the associated API connection.
	CRITICAL_SECTION* m_pcritAPI;  // pointer to critsec object for connection

// control
//	bool* m_pbKillConnThread;
	bool m_bKillChannelThread;
	bool m_bChannelThreadStarted;

// status
	bool m_bChannelThreadGettingEvents;
	bool m_bChannelThreadPublishingTempEvents;
	bool m_bChannelThreadPublishingEvents;

// operations
	CSentinelEventObject** m_ppevents;
	int m_nNumEvents;

	CSentinelTrafficObject** m_pptraffic;
	int m_nNumTraffic;

	int m_nLastFound;
	double m_dblUpdateTime;

	int FindUid(int uid);  //returns index
	int FindEvent(CSentinelEventObject* pEvent, double dblTimeToleranceMS, double dblTimeOffset, double* pdblTimeOffset, int nMatchMode);  //returns index, uses IsMatch
	int InsertEvent(CSentinelEventObject* pevent, int index);  // -1 = add to end.
	int DeleteEvent(int index);  // removes from array
	int FindEventByPosition(int nPosition);  //returns index, uses IsMatch

	int UpdateEventSQL(int index, CDBUtil* pdb,	CDBconn* pdbConn, double dblServerTime); 
	int AssembleKey(int index, char* pszFormat, BYTE bcdFrameRate);
	int SetAllUnprocessed();
	int ShiftPositions(int nIndex, int nDelta);

	CDBUtil m_db; // for encode quotes utility

	// for traffic lists
	int FindTrafficObjectByDate(unsigned long ulDate);
	int FindTrafficObjectByID(int nID);
	int FindTrafficObjectByPath(char* pchFilePath);
	int InsertTrafficObject(CSentinelTrafficObject* pObj, int index=-1);  // -1 = add to end.
	int DeleteTrafficObject(int index);
};


class CSentinelVirtualChannelObject  
{
public:
	CSentinelVirtualChannelObject();
	virtual ~CSentinelVirtualChannelObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	int m_nID;  // the unique internal ID within Sentinel setup.
	int m_nChannelID;  // the unique Channel ID that is editable by the user
	int m_nMapID;  // the channel ID (remote or local, as determined by m_usType) that this virtual channel maps to.

	char* m_pszDesc; // description of this virtual channel
};

class CSentinelChange  
{
public:
	CSentinelChange();
	virtual ~CSentinelChange();

	void* m_pObj;
	int m_nType;
	int m_nChannelID;
	int m_nHarrisListID;
	int m_nParentUID;

	char* m_pchXMLkey;
	char* m_pchXMLrec;
	char* m_pchXMLclip;
	char* m_pchXMLtitle;
	char* m_pchXMLdata;

	char* m_pszServerName;
	char* m_pszClientName;
	char* m_pszDesc;

	tlistdata  m_list;
	systemstatus m_sys;
	TConnectionStatus m_status;

	_timeb m_timestamp;
};

class CSentinelData  
{
public:
	CSentinelData();
	virtual ~CSentinelData();


	SOCKET m_socketRemote;
	bool m_bInRemoteCommand;
	bool m_bSocketMonitorStarted;
	bool m_bMonitorSocket;
	int m_nRemoteClock;

	// util object
	CBufferUtil m_bu;
	CNetUtil m_net;

  // hostname.  have to get it, store it here
	char*	m_pszHost;	// the name of the host
	char*	m_pszCompleteHost;	// the complete name of the host

	char*	m_pszCortexHost;	// the name of the cortex host
	unsigned short m_usCortexCommandPort;
	unsigned short m_usCortexStatusPort;

	CSentinelConnectionObject** m_ppConnObj;
	int m_nNumConnectionObjects;

	CSentinelChannelObject** m_ppChannelObj;
	int m_nNumChannelObjects;

	CSentinelChannelObject** m_ppRemoteChannelObj;
	int m_nNumRemoteChannelObjects;

	CSentinelVirtualChannelObject** m_ppVirtualChannelObj;
	int m_nNumVirtualChannelObjects;

	CCortexPlugIn** m_ppControlObj;
	int m_nNumControlObjects;

	workingset** m_ppWorkingSet;
	int m_nNumSetIDs;
	workingset** m_ppWorkingSetChanges;
	int m_nNumSetChanges;
	int m_nNumSetChangesArray;

	CSentinelChange** m_ppChanges;
	int m_nNumChanges;
	int m_nNumChangesArray;
	int m_nChangesArrayIndex;

	bool m_bWorkingSetThreadStarted;
	bool m_bWorkingSetThreadSuppress;

	bool m_bControlModuleServiceThreadStarted;
	bool m_bKillControlModuleServiceThread;


	_timeb m_timebAutoPurge; // the last time autopurge was run
	_timeb m_timebTick; // the last time check inside the thread
	_timeb m_timebLastStatus; // the time of the last status given to cortex
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for cortex global status (each obj has their own as well)

	int m_nSettingsMod;
	int m_nChannelsMod;
	int m_nConnectionsMod;
	int m_nControllersMod;
	int m_nRemoteChannelsMod;
	int m_nVirtualChannelsMod;

	int m_nLastSettingsMod;
	int m_nLastChannelsMod;
	int m_nLastConnectionsMod;
	int m_nLastControllersMod;
	int m_nLastRemoteChannelsMod;
	int m_nLastVirtualChannelsMod;

	bool m_bNetworkMessagingInitialized;
	bool m_bCheckModsWarningSent;
	bool m_bCheckMsgsWarningSent;
	bool m_bCheckAsRunWarningSent;
	int m_nLastTrafficFileMod;
	int m_nLastTrafficEventsMod;

	int m_nLastUid;

	bool m_bProcessSuspended;

	char* GetStatusText(unsigned long* pulStatus);  // allocates mem, must free after use
	int		SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError = false);
	int		GetHost();

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;
	int CheckMessages(char* pszInfo=NULL);
	int CheckAsRun(char* pszInfo=NULL);
	int CheckDatabaseMods(char* pszInfo=NULL);
	int IncrementDatabaseMods(char* pszTableName, char* pszInfo=NULL);
	int GetConnections(char* pszInfo=NULL);
	int GetChannels(char* pszInfo=NULL);
	int GetControlModules(char* pszInfo=NULL);
	int SetThreads(char* pszInfo=NULL);
	int ReturnConnectionIndex(char* pchServer);
	int ReturnChannelIndex(char* pchServer, int nList);
	int SetLastUID(char* pszInfo=NULL);
	int GetLastUID(char* pszInfo=NULL);
	int GetNewUID();
	int CheckPluginOnConn(char* pszPlugInChannelIds, char* pszConnServerName);
	int GetRemoteChannels(char* pszInfo=NULL);
	int GetVirtualChannels(char* pszInfo=NULL);

	CLicenseKey m_key;
	int m_nMaxLicensedChannels;

	bool m_bQuietKill;
	CRITICAL_SECTION m_critIncrement; 
	CRITICAL_SECTION m_critUID; 
	CRITICAL_SECTION m_critSQL;
	CRITICAL_SECTION m_critDebug;
	CRITICAL_SECTION m_critChange;
	CRITICAL_SECTION m_critWorkingSet;
	CRITICAL_SECTION m_critChannels;  // exclude access to channels array
	CRITICAL_SECTION m_critConns;
	CRITICAL_SECTION m_critExternalModules;
	CRITICAL_SECTION m_critControlModuleService;
	CRITICAL_SECTION m_critRemoteChannels;  // exclude access to remote channels array
	CRITICAL_SECTION m_critVirtualChannels;  // exclude access to remote channels array
	CRITICAL_SECTION m_critRemoteSocket;  

	CRITICAL_SECTION m_critAsRun;
	CRITICAL_SECTION m_critMessaging;

	// traffic
	bool m_bTrafficKillWatch;
	bool m_bTrafficWatchStarted;
	bool m_bTrafficWatchInitialized;
	bool m_bTrafficWatchWarningSent;
	_timeb m_timeTrafficWatch;

private:
	CRITICAL_SECTION m_critText;
	char* m_pszStatus;	// parseable string

//	unsigned long m_ulReqCount;
//	unsigned long m_ulSvcCount;
};

#endif // !defined(AFX_SENTINELDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
