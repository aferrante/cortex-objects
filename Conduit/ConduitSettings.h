// ConduitSettings.h: interface for the CConduitSettings class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CONDUITSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
#define AFX_CONDUITSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ConduitDefines.h"
#include "../../Common/MFC/ODBC/DBUtil.h"

class CConduitSettings  
{
public:
	CConduitSettings();
	virtual ~CConduitSettings();

	int Settings(bool bRead);
	int GetFromDatabase(char* pszInfo=NULL);  //only get.  Interface publishes
	char* ProcessString(char* pszString, bool bFreeIncomingString);


	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;

	char* m_pszName;  // familiar name of this instance (and therefore the name of the Conduit database)
	char* m_pszType;  // familiar name of the type.
	char* m_pszProject;  // familiar name of the project name.
	unsigned long m_ulMainMode;

	// ports
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;

	// The following flags determine what settings are loaded. 
	// The specifics do not need to be stored in cortex settings, they are pushed to the various objects.

	// messaging for Conduit
	bool m_bUseLog;			// write a log file
	bool m_bUseEmail;		// send an email on failures (or commanded from remotes)
	bool m_bUseNetwork; // send a message to a remote host
	bool m_bReportSuccessfulOperation;
	bool m_bLogTransfers;
	char* m_pszFileSpec;
	char* m_pszMailSpec;
	char* m_pszProcessedFileSpec;
	char* m_pszProcessedMailSpec;
	bool m_bUseXMLClientLog;			// write a log file
	bool m_bLogNetworkErrors; // enables messaging into network object (logs socket errors, etc).
	bool m_bMillisecondMessaging;			// use millisecond resolution for messages and asrun

	int m_nNumberOfStatusLines;


	int m_nAutoPurgeMessageDays;
	int m_nAutoPurgeAsRunDays;
	int m_nAutoPurgeInterval;

	int m_nThreadDwellMS;
	bool m_bDebugLists;  // if true, dumps the temp and events databases
	int  m_nDebugListTopCount;  //if positive, just prints out the top n recored in the events tables.
	bool m_bDebugSQL;  // if true, prints out the SQL calls, all but event inserts
	bool m_bDebugInsertSQL;  // if true, prints out the event insert SQL calls
	unsigned long m_ulDebug;  // prints out debug statements that & with this.
	char* m_pszDebugOrder; // the order by clause for debug


	// automation
	int m_nTriggerAdvanceMS; // number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)
	int m_nAnalyzeRulesDwellMS; // number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once.
	int m_nAnalyzeParameterDwellMS; // number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once.
	int m_nAnalyzeTimingDwellMS; // max number of milliseconds to allow timing not be re-run
	bool m_bUseLocalClock;  // use the computer's clock, not the transmitted server time
	bool m_bUseUTC; // use UTC time, not local/DST time.

	// file handling

	// database stuff
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;
	char* m_pszDatabase;
	char* m_pszDBServer;

	bool  m_bPeriodicQueries;
	char* m_pszPeriodicQueries;  // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.
	bool  m_bPeriodicShell;
	char* m_pszPeriodicShellCmds;  // a pipe-delimited string indicating intervals and shell commands (ShellExecute)
	bool  m_bPeriodicHttp;
	char* m_pszPeriodicHttp;  // a pipe-delimited string indicating intervals and HTTP gets to run...  results stored in m_pszPeriodicHttpResult
	char* m_pszPeriodicHttpResult;


//	char* m_pszSettings;  // the Settings table name
//	char* m_pszExchange;  // the Exchange table name
//	char* m_pszMessages;  // the Messages table name
//	char* m_pszQueue;			// the Queue table name
//	char* m_pszDownloaders;  // the Downloader table name 
//	char* m_pszEvents;  // the Events view name 
//	char* m_pszAsRun;  // the As-run table name
//	char* m_pszCommandQueue;			// the Command Queue table name
	

// other settings
	unsigned long m_ulModsIntervalMS;  // interval on which to check database mods

	char* m_pszLicense;  // the License Key
	char* m_pszIconPath;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

};

#endif // !defined(AFX_CONDUITSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
