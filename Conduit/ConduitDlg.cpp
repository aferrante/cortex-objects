// ConduitDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Conduit.h"
#include "ConduitDlg.h"
#include "ConduitMain.h"
#include "ConduitHandler.h"
#include <process.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CConduitApp theApp;
extern CConduitMain* g_pconduit;
extern bool g_bThreadStarted;
extern bool g_bKillStatus;
void ShutdownThread(void* pvArgs);


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About
// CAbout requires MFC.

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
	m_hcurLink = NULL;

}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CAboutDlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
  OnLogo(nFlags, point);

	CDialog::OnLButtonUp(nFlags, point);
}

BOOL CAboutDlg::OnInitDialog() 
{
  CDialog::OnInitDialog();

  
  CFont* pFont;
	CFont* pSetFont;
	CFont newFont;
  LOGFONT lfLogFont;
//  pFont = GetDlgItem(IDC_STATIC_URL)->GetFont();
  pFont = GetFont();
  COLORREF color;
  CDC* pdc;

  // set font to underline
	pSetFont =pFont;

  if(pFont->GetLogFont(&lfLogFont))
	{
		lfLogFont.lfUnderline = (BYTE) TRUE;
		if(newFont.CreateFontIndirect(&lfLogFont))
			pSetFont = &newFont;
	}

  // set color to blue...
  pdc = GetDlgItem(IDC_STATIC_URL)->GetDC( );
  color=RGB(0,0,255);
	if(pdc!=NULL)
	{
		CBmpUtil bmpu;
		HDC hdc= pdc->GetSafeHdc();
		((CStatic*)GetDlgItem(IDC_STATIC_URL))->SetBitmap(
			bmpu.TextOutBitmap(
				hdc, 
				"http://www.VideoDesignSoftware.com", 
				pSetFont, 
				color, 
				GetSysColor(COLOR_3DFACE), 
				0
				)
			);
		//center window
		CRect rcCtrl, rcWnd;
		GetClientRect(&rcWnd);
		GetDlgItem(IDC_STATIC_URL)->GetWindowRect(&rcCtrl);
		ScreenToClient(&rcCtrl);
		GetDlgItem(IDC_STATIC_URL)->SetWindowPos(NULL, (rcWnd.Width()-rcCtrl.Width())/2, rcCtrl.top ,0,0,SWP_NOSIZE|SWP_NOZORDER);

	}
	else
	{
		GetDlgItem(IDC_STATICTEXT_URL)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATICTEXT_URL)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_URL)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_URL)->EnableWindow(FALSE);
	}

  CString szTemp;
  szTemp.Format("Build date: %s %s", __DATE__, __TIME__);
  GetDlgItem(IDC_STATIC_BUILD)->SetWindowText(szTemp);

#ifdef CONDUIT_CURRENT_VERSION
  szTemp.Format("Conduit Application\nVersion %s", CONDUIT_CURRENT_VERSION);
  GetDlgItem(IDC_STATICTEXT_TITLE)->SetWindowText(szTemp);
	szTemp.Format("About Conduit %s", CONDUIT_CURRENT_VERSION);
	SetWindowText(szTemp);
#endif

	m_hcurLink = AfxGetApp()->LoadCursor(IDC_CURSOR_LINK);

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void CAboutDlg::OnLogo(UINT nFlags, CPoint point)
{
	CRect rcLogo;
  GetDlgItem(IDC_STATIC_URL)->GetWindowRect(&rcLogo);
  ScreenToClient(&rcLogo);
	if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
		(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
	{
    // launch browser
    HINSTANCE hi;
    hi=ShellExecute((HWND) NULL, "open", "http://www.VideoDesignSoftware.com", NULL, NULL, SW_HIDE);
  }
	else
	{
		GetDlgItem(IDC_STATICTEXT_URL)->GetWindowRect(&rcLogo);
		ScreenToClient(&rcLogo);
		if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
			(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
		{
			// launch browser
			HINSTANCE hi;
			hi=ShellExecute((HWND) NULL, "open", "http://www.VideoDesignSoftware.com", NULL, NULL, SW_HIDE);
		}
		else
		{
			GetDlgItem(IDC_STATIC_LOGO)->GetWindowRect(&rcLogo);
			ScreenToClient(&rcLogo);
			if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
				(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
			{
				// launch browser
				HINSTANCE hi;
				hi=ShellExecute((HWND) NULL, "open", "http://www.VideoDesignSoftware.com", NULL, NULL, SW_HIDE);
			}
		}
	}
}


void CAboutDlg::OnMouseMove(UINT nFlags, CPoint point) 
{
	HCURSOR hCur = GetCursor();
	CRect rcLogo;
  GetDlgItem(IDC_STATIC_URL)->GetWindowRect(&rcLogo);
  ScreenToClient(&rcLogo);
	if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
		(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
	{
		if(m_hcurLink!=hCur) SetCursor(m_hcurLink);
  }
	else
	{
		GetDlgItem(IDC_STATICTEXT_URL)->GetWindowRect(&rcLogo);
		ScreenToClient(&rcLogo);
		if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
			(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
		{
			if(m_hcurLink!=hCur) SetCursor(m_hcurLink);
		}
		else
		{
			GetDlgItem(IDC_STATIC_LOGO)->GetWindowRect(&rcLogo);
			ScreenToClient(&rcLogo);
			if ((point.x>rcLogo.left)&&(point.x<rcLogo.right)&&
				(point.y>rcLogo.top)&&(point.y<rcLogo.bottom))  // point must be within the rect
			{
				if(m_hcurLink!=hCur) SetCursor(m_hcurLink);
			}
			else
			{
				// just set it back to the Arrow
			}
		}
	}
	
	CDialog::OnMouseMove(nFlags, point);
}


/////////////////////////////////////////////////////////////////////////////
// CConduitDlg dialog


CConduitDlg::CConduitDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConduitDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CConduitDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
//	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_hIcon = (HICON)LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(IDR_MAINFRAME), IMAGE_ICON, 16, 16, LR_LOADMAP3DCOLORS );
	m_bVis=FALSE;
	m_bNewSizeInit=TRUE;
	m_nDxEye = 1;
	m_hbmp[BMP_PROG] = NULL;
	m_bStatusMode = false;
	_ftime( &m_timebTick );
	m_cr = CLR_INVALID;
	m_ulConduitFlags = 0xffffffff;
	m_sizeDlgMin = CSize(CONDUITDLG_MINSIZEX, CONDUITDLG_MINSIZEY);

}


void CConduitDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConduitDlg)
	DDX_Control(pDX, IDC_TREE1, m_tree);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CConduitDlg, CDialog)
	//{{AFX_MSG_MAP(CConduitDlg)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_BUTTON_SETTINGS, OnButtonSettings)
	ON_WM_GETMINMAXINFO()
	ON_WM_QUERYDRAGICON()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_WM_SYSCOMMAND()
	ON_WM_TIMER()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST1, OnDblclkList1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CConduitDlg message handlers

void CConduitDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	ShowWindow(SW_HIDE);
	// CDialog::OnCancel();
}

void CConduitDlg::OnOK() 
{
//	CDialog::OnOK();
}

void CConduitDlg::SetProgressColor(COLORREF cr)
{
	CWnd* pwnd = GetDlgItem(IDC_STATIC_PROGBAR);
	if((pwnd)&&(m_cr!=cr))
	{
//		CPaintDC dc(pwnd);
//		dc.SetBkColor( cr );
		CRect rcProg;
		pwnd->GetClientRect(&rcProg);
//		dc.FillSolidRect( rcProg, cr );
		
		if(m_hbmp[BMP_PROG]!=NULL) DeleteObject(m_hbmp[BMP_PROG]);
		m_hbmp[BMP_PROG] = m_bmpu.ColorBitmap(GetDC()->GetSafeHdc(), rcProg.Width(), rcProg.Height(), cr);
		((CStatic*)pwnd)->SetBitmap(m_hbmp[BMP_PROG]);
		m_cr = cr;
	}
}

void CConduitDlg::SetProgress(unsigned long ulFlags)
{
//#define CXDLG_CLEAR		0x00000000
//#define CXDLG_WAITING 0x10000000
	m_ulFlags = ulFlags;

	if(ulFlags&CXDLG_WAITING)
	{
		// start the timer to get the "waiting" look.
		if(ulFlags&0xffff)
			SetTimer(1, (ulFlags&0xffff), NULL);
		else
			SetTimer(1, 30, NULL);
		CWnd* pwnd = GetDlgItem(IDC_STATIC_PROGBAR);
		if(pwnd)
		{
			pwnd->ShowWindow(SW_SHOW);
			CRect rcFull;
			CRect rcProg;
			GetDlgItem(IDC_STATIC_STATUSTEXT)->GetWindowRect(&rcFull);
			ScreenToClient(&rcFull);
			pwnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);
			pwnd->SetWindowPos(
				&wndTop,
				rcFull.left, rcProg.top,
				1, rcProg.Height(),
				SWP_NOZORDER);
		}
	}
	else
	if(ulFlags&CXDLG_EVILEYE)  //like waiting, only no timer, needs actual calls from an outside thread to show that its working!
	{
		// kill the timer if it's on, to get the "evil eye" look.
		KillTimer(1);

		CWnd* pwnd = GetDlgItem(IDC_STATIC_PROGBAR);
		if(pwnd)
		{
			//width is expressed as a percentage.
			int nWidth;
			if(((ulFlags&0x7f)>0)&&((ulFlags&0x7f)<100))
				nWidth = (ulFlags&0x7f);
			else
				nWidth = 10;

			pwnd->ShowWindow(SW_SHOW);
			CRect rcFull;
			CRect rcProg;
			GetDlgItem(IDC_STATIC_STATUSTEXT)->GetWindowRect(&rcFull);
			ScreenToClient(&rcFull);
			pwnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);

			//recalc width to pixels;
			nWidth = (rcFull.Width()*nWidth/100);


			if(rcProg.Width() != nWidth)//starting.
			{
				if((rcProg.left+nWidth)>= rcFull.right)
				{
					rcProg.left = rcFull.right-nWidth-1;
				}
				if( rcProg.left <= rcFull.left)
				{
					rcProg.left = rcFull.left+1;
				}
				pwnd->SetWindowPos(
					&wndTop,
					rcProg.left, rcProg.top,
					nWidth, rcProg.Height(),
					SWP_NOZORDER);
				//speed is expressed as a pixel value.
				if((((ulFlags&0xff00)>>8)>0)&&((int)((ulFlags&0xff00)>>8)<nWidth))  // max one full step per call
					m_nDxEye = ((ulFlags&0xff00)>>8);
				else
					m_nDxEye = 1;
			}
			else
			{
				if((rcProg.right+m_nDxEye>rcFull.right)||(rcProg.left+m_nDxEye<rcFull.left))
				{
					m_nDxEye=-m_nDxEye;
				}
				pwnd->SetWindowPos(
					&wndTop,
					rcProg.left+m_nDxEye, rcProg.top,
					nWidth, rcProg.Height(),
					SWP_NOZORDER|SWP_NOSIZE);
			}
		}
	}
	else 
	if(ulFlags==CXDLG_CLEAR)
	{
		// kill the timer and clear
		KillTimer(1);
		GetDlgItem(IDC_STATIC_PROGBAR)->ShowWindow(SW_HIDE);
	}
	else
	if((ulFlags&0x7F)<=100)
	{// set a percentage
		// do the progress look.
		CWnd* pwnd = GetDlgItem(IDC_STATIC_PROGBAR);
		if(pwnd)
		{
			pwnd->ShowWindow(SW_SHOW);
			CRect rcFull;
			CRect rcProg;
			GetDlgItem(IDC_STATIC_STATUSTEXT)->GetWindowRect(&rcFull);
			ScreenToClient(&rcFull);
			pwnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);
			// right edge moves to the right
			pwnd->SetWindowPos(
				&wndTop,
				rcFull.left, rcProg.top,
				rcFull.Width()*(ulFlags&0x7F)/100, rcProg.Height(),
				SWP_NOZORDER);
		}
	}
	else
	if(((ulFlags&0xff)>100)&&((ulFlags&0xff)<200)) // range 1 to 99. described by 101 to 199.
	{// set a percentage
		// do the left side progress look.
		CWnd* pwnd = GetDlgItem(IDC_STATIC_PROGBAR);
		if(pwnd)
		{
			pwnd->ShowWindow(SW_SHOW);
			CRect rcFull;
			CRect rcProg;
			GetDlgItem(IDC_STATIC_STATUSTEXT)->GetWindowRect(&rcFull);
			ScreenToClient(&rcFull);
			pwnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);
			// left edge moves to the right
			pwnd->SetWindowPos(
				&wndTop,
				rcFull.left+(rcFull.Width()*((ulFlags&0xff)-100)/100), rcProg.top,
				rcFull.Width()-(rcFull.Width()*((ulFlags&0xff)-100)/100), rcProg.Height(),  //width done this way because of int truncate
				SWP_NOZORDER);
		}
	}
}

void CConduitDlg::ShellExecuteSettings()
{
	if((theApp.m_pszSettingsURL!=NULL)&&(strlen(theApp.m_pszSettingsURL)>0))
	{
		//shell execute a browser to the Conduit settings URL, shich can be found in 
		//one of the ini files. it gets initialized once the webserver is up.
		// launch browser
		HINSTANCE hi;
		hi=ShellExecute((HWND) NULL, "open", theApp.m_pszSettingsURL, NULL, NULL, SW_SHOW);
	}
	else
	{
		AfxMessageBox("The settings interface has not yet been initialized.");
	}
}

void CConduitDlg::OnButtonSettings() 
{
	ShellExecuteSettings();
//	g_settings.DoModal();
}

BOOL CConduitDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
//	g_settings.Settings(READ);

	m_lce.SubclassDlgItem(IDC_LIST1, this);
	m_lce.SetHeight(16);
	m_lce.SetImages(IDB_BITMAP_STDICONS, 16, RGB(254,254,254)); 

	CRect rc; 
	m_lce.GetWindowRect(&rc);
	m_lce.InsertColumn(0, "", LVCFMT_LEFT, rc.Width()-16, 0 );

/*
for the tree
	CImageList* pci = new CImageList;
//	pci->Create( IDB_BITMAP_DIR, 16, 32, RGB(255,255,255) );
	
	pci->Create(16, 16, ILC_COLOR8, 2, 4);  // have to use ILC_COLOR8 for 256 color images

	CBitmap bitmap;
  bitmap.LoadBitmap(IDB_BITMAP_DIR);
  pci->Add(&bitmap, (COLORREF)0x00ff00);  // theres no green...
  bitmap.DeleteObject();

	m_tree.SetImageList( pci, TVSIL_NORMAL );

*/
//	int nRegisterCode=0;

	CString szParams=_T("");//, szReport=_T("");

	// main Dlg Title
	szParams.Format("Conduit powered by VDS");
	SetWindowText(szParams);

/*
	// set up default MB title
//	szParams.Format("Conduit");
	nRegisterCode = g_md.RegisterDestination(MD_DEST_UI_MB, MD_DESTTYPE_UI_MB,	szParams, (void*)(NULL));
	if (nRegisterCode != MD_DESTINATION_REGISTERED) {szReport.Format("failed to register MB Title\n code: 0x%02x", nRegisterCode); AfxMessageBox(szReport);}

	// set up logging list control
	szParams.Format("50000|T-iM-F-S-D"); // if this changes, modify dest in OnButtonPause should match
	nRegisterCode = g_md.RegisterDestination(MD_DEST_UI_LIST1, MD_DESTTYPE_UI_LISTCTRLEX,	szParams, (void*)(&m_lce));
	if (nRegisterCode != MD_DESTINATION_REGISTERED) {szReport.Format("failed to register ListCtrlEx\n code: 0x%02x", nRegisterCode); AfxMessageBox(szReport);}
*/
	// set up logfile
//	szParams.Format("Conduit|YM");
//	nRegisterCode = m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, "log", char* pszParams, char* pszInfo);
//	if (nRegisterCode != MSG_SUCCESS) {szReport.Format("failed to register log file\n code: %d", nRegisterCode); AfxMessageBox(szReport);}

/*
	// set up e-mail group 1 -> make this general alert
	szParams.Format("%s|Conduit|%s|Default subject",g_settings.m_szEmailTo,g_settings.m_szEmailIP);
	nRegisterCode = g_md.RegisterDestination(MD_DEST_EMAIL_GP1, MD_DESTTYPE_EMAIL, szParams);
	if (nRegisterCode != MD_DESTINATION_REGISTERED) {szReport.Format("failed to register e-mail1\n code: 0x%02x", nRegisterCode); AfxMessageBox(szReport);}

	// set up e-mail group 2 -> make this just VDI
	szParams.Format("kenji@VideoDesignSoftware.com,gregg@VideoDesignSoftware.com|Conduit|%s|Default Subj",g_settings.m_szEmailIP);
	nRegisterCode = g_md.RegisterDestination(MD_DEST_EMAIL_GP2, MD_DESTTYPE_EMAIL, szParams);
	if (nRegisterCode != MD_DESTINATION_REGISTERED) {szReport.Format("failed to register e-mail2\n code: 0x%02x", nRegisterCode); AfxMessageBox(szReport);}
	
	
	szParams.Format("-------------- Conduit start --------------");
	CString szLine = _T("");
	for(int l=0; l<szParams.GetLength(); l++) szLine+="-"; // make it pretty!
	
	g_md.DM(0,MD_FMT_UI_ICONINFO,0,
		"%s\n%s|OnInitDialog",
		szLine,szParams
		); //(Dispatch message)
// 
*/

	// Load all bitmaps, store handles
	CBitmap bmp;

  bmp.LoadBitmap(IDB_SETTINGS);
	m_hbmp[BMP_SETTINGS] = m_bmpu.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmpu.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
/*
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BITMAP_PAUSELIST);
	m_hbmp[BMP_PAUSE] = m_bmpu.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmpu.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BITMAP_PLAYLIST);
	m_hbmp[BMP_PLAY] = m_bmpu.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmpu.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
*/
	bmp.DeleteObject();

	// uncomment below to set up buttons
	((CButton*)GetDlgItem(IDC_BUTTON_SETTINGS))->SetBitmap(m_hbmp[BMP_SETTINGS]);
//	((CButton*)GetDlgItem(IDC_BUTTON_PAUSE))->SetBitmap(m_hbmp[BMP_PAUSE]);

	GetDlgItem(IDC_STATIC_PROGBAR)->ShowWindow(SW_HIDE);

	// uncomment below for tooltips
	EnableToolTips(TRUE);
	

	//change the progress bar style...
//	GetDlgItem(IDC_STATIC_PROGBAR)->ModifyStyleEx( SS_GRAYRECT, SS_OWNERDRAW );  //interesting look, because cant modify cstatic, apparently.
	SetProgressColor(GetSysColor(COLOR_ACTIVECAPTION));
	SetTimer(606, 3000, NULL); // three second status interval.
	SetTimer(2, 5000, NULL); // five second status interval.
	SetTimer(3, 10, NULL); // evil eye update int
	SetStatus();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CConduitDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}
void CConduitDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CConduitDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CConduitDlg::OnGetMinMaxInfo( MINMAXINFO FAR* lpMMI ) 
{
	lpMMI->ptMinTrackSize.x=m_sizeDlgMin.cx;
	lpMMI->ptMinTrackSize.y=m_sizeDlgMin.cy;
}

void CConduitDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	int nCtrlID;

	int dx,dy;
	CRect rcList;
	dx=m_rcDlg.right-cx;
	dy=m_rcDlg.bottom-cy;

	if(m_bVis)
	{
		CWnd* pWnd;

		pWnd=GetDlgItem(IDC_STATIC_STATUSTEXT);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_STSTATUS].left,  // goes with right edge
			m_rcCtrl[ID_STSTATUS].top-dy,  // goes with bottom edge
			m_rcCtrl[ID_STSTATUS].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_STSTATUS].Height(), 
			SWP_NOZORDER
			);
/*
		pWnd=GetDlgItem(IDC_STATIC_PROG);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_STPROG].left,  // goes with right edge
			m_rcCtrl[ID_STPROG].top-dy,  // goes with bottom edge
			m_rcCtrl[ID_STPROG].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_STPROG].Height(), 
			SWP_NOZORDER
			);
*/

		pWnd=GetDlgItem(IDC_STATIC_PROGBAR);
		if(pWnd!=NULL)
		{
			CRect rcProg;
			pWnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);
			pWnd->SetWindowPos( 
			&wndTop,
			rcProg.left,  
			m_rcCtrl[ID_STSTATUS].top-dy+(m_rcCtrl[ID_STPROG].top-m_rcCtrl[ID_STSTATUS].top),  // goes with bottom edge
			rcProg.Width(), // leave the width alone, it's a progress bar!
			rcProg.Height(), 
			SWP_NOZORDER
			);
		}

		pWnd=GetDlgItem(IDC_BUTTON_SETTINGS);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
//			m_rcCtrl[ID_BNSETTINGS].left-(dx/2),  // centered
			m_rcCtrl[ID_BNSETTINGS].left-dx,  // goes with right edge
//			m_rcCtrl[ID_BNSETTINGS].top,  // goes with top edge
			m_rcCtrl[ID_BNSETTINGS].top-dy,  // goes with bottom edge
			0, 0, 
			SWP_NOZORDER|SWP_NOSIZE
			);
/*
		pWnd=GetDlgItem(IDC_BUTTON_PAUSE);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
//			m_rcCtrl[ID_BNPAUSE].left-(dx/2),  // centered
			m_rcCtrl[ID_BNPAUSE].left-dx,  // goes with right edge
//			m_rcCtrl[ID_BNPAUSE].top,  // goes with top edge
			m_rcCtrl[ID_BNPAUSE].top-dy,  // goes with bottom edge
			0, 0, 
			SWP_NOZORDER|SWP_NOSIZE
			);
*/
		pWnd=GetDlgItem(IDC_LIST1);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			0, 0, 
			m_rcCtrl[ID_LIST].Width()-dx,  // goes with right edge
			m_rcCtrl[ID_LIST].Height()-dy,  // goes with bottom edge
			SWP_NOZORDER|SWP_NOMOVE
			);

		if(m_lce.GetColumnWidth(0)<(m_rcCtrl[ID_LIST].Width()-dx-16)) // only make this get larger
		{
			m_lce.SetColumnWidth(0, (m_rcCtrl[ID_LIST].Width()-dx-16) ); 
		}



		for (int i=0;i<CONDUITDLG_NUM_MOVING_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_LIST: nCtrlID=IDC_LIST1;break;
			case ID_BNSETTINGS: nCtrlID=IDC_BUTTON_SETTINGS;break;
//			case ID_BNPAUSE: nCtrlID=IDC_BUTTON_PAUSE;break;
			case ID_STSTATUS: nCtrlID=IDC_STATIC_STATUSTEXT;break;
			case ID_STPROG: nCtrlID=IDC_STATIC_PROGBAR;break;
			}
			GetDlgItem(nCtrlID)->Invalidate();
		}
	}		
}

void CConduitDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);

//sizing stuff
	if(m_bNewSizeInit)
	{
		int nCtrlID;

		GetClientRect(&m_rcDlg); 
		CRect rcDlg=m_rcDlg;
		for (int i=0;i<CONDUITDLG_NUM_MOVING_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_LIST: nCtrlID=IDC_LIST1;break;
			case ID_BNSETTINGS: nCtrlID=IDC_BUTTON_SETTINGS;break;
//			case ID_BNPAUSE: nCtrlID=IDC_BUTTON_PAUSE;break;
			case ID_STSTATUS: nCtrlID=IDC_STATIC_STATUSTEXT;break;
			case ID_STPROG: nCtrlID=IDC_STATIC_PROGBAR;break;
			}
			GetDlgItem(nCtrlID)->GetWindowRect(&m_rcCtrl[i]);
			ScreenToClient(&m_rcCtrl[i]);
		}
		CFileUtil fu;
		char pszFilename[MAX_PATH];
		strcpy(pszFilename, "");

		char* pchF=theApp.GetSettingsFilename();
		if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
		
		if(strlen(pszFilename)<=0)  strcpy(pszFilename, CONDUIT_SETTINGS_FILE_DEFAULT);  // cortex settings file
//		AfxMessageBox("s");

		if(fu.GetSettings(pszFilename, false)&FILEUTIL_MALLOC_OK)
		{
			bool bRemember = fu.GetIniInt("Window Settings", "RememberPlacement", 1)?true:false; 
			int l=0, t=0, r=0, b=0, m=0, minx=CONDUITDLG_MINSIZEX, miny=CONDUITDLG_MINSIZEY;
			l = fu.GetIniInt("Window Settings", "Left", -1);  //get from buffer
			t = fu.GetIniInt("Window Settings", "Top", -1);  //get from buffer
			r = fu.GetIniInt("Window Settings", "Right", -1);  //get from buffer
			b = fu.GetIniInt("Window Settings", "Bottom", -1);  //get from buffer
			m = fu.GetIniInt("Window Settings", "Maximize", 0);  //get from buffer
			minx = fu.GetIniInt("Window Settings", "MinSizeX", CONDUITDLG_MINSIZEX);  //get from buffer
			miny = fu.GetIniInt("Window Settings", "MinSizeY", CONDUITDLG_MINSIZEY);  //get from buffer
			fu.SetIniInt("Window Settings", "Left", l);      //write to buffer in case default was taken
			fu.SetIniInt("Window Settings", "Top", t);      //write to buffer in case default was taken
			fu.SetIniInt("Window Settings", "Right", r);      //write to buffer in case default was taken
			fu.SetIniInt("Window Settings", "Bottom", b);      //write to buffer in case default was taken
			fu.SetIniInt("Window Settings", "Maximize", m);      //write to buffer in case default was taken
			fu.SetIniInt("Window Settings", "MinSizeX", minx);  //get from buffer
			fu.SetIniInt("Window Settings", "MinSizeY", miny);  //get from buffer
			fu.SetIniInt("Window Settings", "RememberPlacement", bRemember?1:0); 
			// write the settings file
			fu.SetSettings(CONDUIT_SETTINGS_FILE_DEFAULT, false);

			m_sizeDlgMin.cx=minx;
			m_sizeDlgMin.cy=miny;

			if(bRemember)
			{
				if((l>=0)&&(t>=0)&&(r>=0)&&(b>=0))
				{
					if((r>l)&&(b>t))
					{
						//max position stuff: if it,s off the screen, just make it 0,0
						if (l>=GetSystemMetrics(SM_CXSCREEN)) {r-=l; l=0;}
						if (t>=GetSystemMetrics(SM_CYSCREEN)) {b-=t; t=0;}

						// min size stuff
						if(r-l<CONDUITDLG_MINSIZEX) r=l+CONDUITDLG_MINSIZEX;
						if(b-t<CONDUITDLG_MINSIZEY) b=t+CONDUITDLG_MINSIZEY;

						m_bVis=TRUE;
						SetWindowPos(	&wndTop, l,t,(r-l), (b-t), SWP_NOZORDER);
					}
				}
				if(m>0)
				{
					m_bVis=TRUE;  // have to do this ahead of show max
					ShowWindow(	SW_MAXIMIZE );
				}
			}
		}
	}
	m_bVis=TRUE;
	m_bNewSizeInit=FALSE;
}

BOOL CConduitDlg::Create() 
{
	return CDialog::Create(IDD);
}

BOOL CConduitDlg::OnToolTipNotify( UINT id, NMHDR* pNMHDR, LRESULT* pResult )
{
	TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
	UINT nID =pNMHDR->idFrom;
	if (pTTT->uFlags & TTF_IDISHWND)
	{
		// idFrom is actually the HWND of the tool
		nID = ::GetDlgCtrlID((HWND)nID);
		if(nID)
		{
			switch(nID)
			{
			case IDC_BUTTON_SETTINGS:
				{
					sprintf(pTTT->szText, "Display or edit settings.");
				} break;
			}
			return TRUE;
		}
	}
	return FALSE;
}

void CConduitDlg::OnExit()
{
	KillTimer(2);
	KillTimer(3);
	CFileUtil fu;
	char pszFilename[MAX_PATH];
	strcpy(pszFilename, "");

	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, CONDUIT_SETTINGS_FILE_DEFAULT);  // cortex settings file
//		AfxMessageBox("s");

	if(fu.GetSettings(pszFilename, false)&FILEUTIL_MALLOC_OK)
	{
		//CRect rc;
		//GetWindowRect(&rc);  // cant use this, in case maximized or minimized, have to use:
		WINDOWPLACEMENT wndpl;
		GetWindowPlacement( &wndpl );

		fu.SetIniInt("Window Settings", "Left",		wndpl.rcNormalPosition.left);     //write to buffer
		fu.SetIniInt("Window Settings", "Top",		wndpl.rcNormalPosition.top);      //write to buffer
		fu.SetIniInt("Window Settings", "Right",	wndpl.rcNormalPosition.right);    //write to buffer
		fu.SetIniInt("Window Settings", "Bottom",	wndpl.rcNormalPosition.bottom);   //write to buffer
		fu.SetIniInt("Window Settings", "Maximize",	IsZoomed()?1:0);   //write to buffer
		// write the settings file
		fu.SetSettings(CONDUIT_SETTINGS_FILE_DEFAULT, false);
	}
	while(g_bThreadStarted) Sleep(1);
	CDialog::OnCancel();
}



void CConduitDlg::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent == 1)
	{
		// do the progress look.
		CWnd* pwnd = GetDlgItem(IDC_STATIC_PROGBAR);
		if(pwnd)
		{
			CRect rcFull;
			CRect rcProg;
			GetDlgItem(IDC_STATIC_STATUSTEXT)->GetWindowRect(&rcFull);
			ScreenToClient(&rcFull);
			pwnd->GetWindowRect(&rcProg);
			ScreenToClient(&rcProg);
			// right edge moving to the right
			if(rcProg.left<=rcFull.left)
			{
				if(rcProg.right>=rcFull.right) // too far, so start with the left edge now.
				{
					pwnd->SetWindowPos(
						&wndTop,
						rcFull.left+1, rcProg.top,
						rcFull.Width()-1, rcProg.Height(),
						SWP_NOZORDER);
				}
				else  //ok just make right edge move to the right
				{
					pwnd->SetWindowPos(
						&wndTop,
						rcFull.left, rcProg.top,
						min(rcProg.Width()+1, rcFull.Width()), rcProg.Height(),
						SWP_NOZORDER|SWP_NOMOVE);
				}
			}
			else  // left edge moving to the right, so check
			{
				if(rcProg.left>=rcFull.right)  // made it, so lets start over.
				{
					pwnd->SetWindowPos(
						&wndTop,
						rcFull.left, rcProg.top,
						1, rcProg.Height(),
						SWP_NOZORDER);
				}
				else  //ok just make left edge move to the right
				{
					pwnd->SetWindowPos(
						&wndTop,
						rcFull.right-min(rcProg.Width()-1,rcFull.Width()-1), rcProg.top,
						min(rcProg.Width()-1,rcFull.Width()-1), rcProg.Height(),
						SWP_NOZORDER);
				}
			}
		}
	}
	else
	if(nIDEvent == 606)
	{
		//		GetDlgItem(IDC_STATIC_STATUSTEXT)->SetWindowText("yo");
		if((g_pconduit)&&(g_bThreadStarted)&&(!g_bKillStatus))
		{
			SetStatus();
		}
		KillTimer(606);
	}
	else
	if(nIDEvent == 2)
	{
		//		GetDlgItem(IDC_STATIC_STATUSTEXT)->SetWindowText("yo");
		if((g_pconduit)&&(g_bThreadStarted)&&(!g_bKillStatus))
		{
			SetStatus();
		}
	}
	else
	if(nIDEvent == 3)
	{
		//		GetDlgItem(IDC_STATIC_STATUSTEXT)->SetWindowText("yo yo yo");
		if((g_pconduit)&&(g_bThreadStarted)&&(!g_bKillStatus))
		{
			unsigned long ulFlags = g_pconduit->m_data.m_ulFlags;
			// build a status string for the bottom status
			if((g_pconduit->m_data.m_timebTick.time>m_timebTick.time)&&((ulFlags&CONDUIT_STATUS_THREAD_MASK) != CONDUIT_STATUS_THREAD_ENDED))
			{
				char pszText[256];

				switch(ulFlags&CONDUIT_STATUS_THREAD_MASK)
				{
					case CONDUIT_STATUS_THREAD_RUN: 
					{
						tm* theTime = localtime( &g_pconduit->m_data.m_timebTick.time	);
						strftime( pszText, 30, "%H:%M:%S", theTime );

						sprintf(pszText, "%s  %s:",	pszText, g_pconduit->m_data.m_pszHost?g_pconduit->m_data.m_pszHost:"localhost");
/*
						switch(ulFlags&CONDUIT_STATUS_FILESVR_MASK)
						{
						case CONDUIT_STATUS_FILESVR_RUN://					0x00000200  // file server running
							{
								sprintf(pszText, "%s%d",	pszText, g_pconduit->m_settings.m_usFilePort);
							} break;
						case CONDUIT_STATUS_FILESVR_END://					0x00000300  // file server shutting down
							{
								sprintf(pszText, "%s%d(end)",	pszText, g_pconduit->m_settings.m_usFilePort);
							} break;
						case CONDUIT_STATUS_FILESVR_ERROR://				0x00000400  // file server error
							{
								strcat(pszText, "(error)");
							} break;
						case CONDUIT_STATUS_FILESVR_START://				0x00000100  // starting the file server
						default:
							{
								sprintf(pszText, "%s%d(init)",	pszText, g_pconduit->m_settings.m_usFilePort);
							} break;
						}

						strcat(pszText, ".");

*/
						switch(ulFlags&CONDUIT_STATUS_CMDSVR_MASK)
						{
						case CONDUIT_STATUS_CMDSVR_RUN://					0x00002000  // command server running
							{
								sprintf(pszText, "%s%d",	pszText, g_pconduit->m_settings.m_usCommandPort);
							} break;
						case CONDUIT_STATUS_CMDSVR_END://					0x00003000  // command server shutting down
							{
								sprintf(pszText, "%s%d(end)",	pszText, g_pconduit->m_settings.m_usCommandPort);
							} break;
						case CONDUIT_STATUS_CMDSVR_ERROR://				0x00004000  // command server error
							{
								strcat(pszText, "(error)");
							} break;
						case CONDUIT_STATUS_CMDSVR_START://				0x00001000  // starting the command server
						default:
							{
								sprintf(pszText, "%s%d(init)",	pszText, g_pconduit->m_settings.m_usCommandPort);
							} break;
						}

						switch(ulFlags&CONDUIT_STATUS_STATUSSVR_MASK)
						{
						case CONDUIT_STATUS_STATUSSVR_RUN://					0x00020000  // status server running
							{
								sprintf(pszText, "%s.%d",	pszText, g_pconduit->m_settings.m_usStatusPort);
							} break;
						case CONDUIT_STATUS_STATUSSVR_END://					0x00030000  // status server shutting down
							{
								sprintf(pszText, "%s.%d(end)",	pszText, g_pconduit->m_settings.m_usStatusPort);
							} break;
						case CONDUIT_STATUS_STATUSSVR_ERROR://				0x00040000  // status server error
							{
								strcat(pszText, ".(error)");
							} break;
						case CONDUIT_STATUS_STATUSSVR_START://				0x00010000  // starting the status server
						default:
							{
								sprintf(pszText, "%s.%d(init)",	pszText, g_pconduit->m_settings.m_usStatusPort);
							} break;
						}
					} break;
					case CONDUIT_STATUS_THREAD_SPARK:
					{
						strcpy(pszText, "Conduit is starting registered objects...");
					} break;
					case CONDUIT_STATUS_THREAD_END:
					{
						strcpy(pszText, "Conduit is shutting down...");
					} break;
					case CONDUIT_STATUS_THREAD_ERROR:
					{
						strcpy(pszText, "Conduit has a main thread error!");
					} break; 
					case CONDUIT_STATUS_THREAD_START:
					default:
					{
						strcpy(pszText, "Conduit is initializing...");
					} break;
				}
				GetDlgItem(IDC_STATIC_STATUSTEXT)->SetWindowText(pszText);

			}

			// do something with the evil eye
			if((ulFlags&CONDUIT_STATUS_THREAD_MASK) != CONDUIT_STATUS_THREAD_ENDED)
			{
				switch(ulFlags&CONDUIT_STATUS_THREAD_MASK)
				{
				case CONDUIT_STATUS_THREAD_START:
					{
						if(m_ulConduitFlags!=CONDUIT_STATUS_THREAD_START)
						{
							m_ulConduitFlags=CONDUIT_STATUS_THREAD_START;
							SetProgress(CXDLG_CLEAR);
							SetProgressColor(GetSysColor(COLOR_ACTIVECAPTION));
							SetProgress(CXDLG_WAITING);
						}
					} break;
				case CONDUIT_STATUS_THREAD_SPARK:
					{
						if(m_ulConduitFlags!=CONDUIT_STATUS_THREAD_SPARK)
						{
							m_ulConduitFlags=CONDUIT_STATUS_THREAD_SPARK;
							SetProgress(CXDLG_CLEAR);
							SetProgressColor(RGB(192,192,0));
							SetProgress(CXDLG_WAITING);
						}
					} break;
				case CONDUIT_STATUS_THREAD_RUN:
					{
						if(m_ulConduitFlags!=CONDUIT_STATUS_THREAD_RUN)
						{
							m_ulConduitFlags=CONDUIT_STATUS_THREAD_RUN;
							SetProgress(CXDLG_CLEAR);
							SetProgressColor(RGB(0,192,0));
						}
						if(
								(g_pconduit->m_data.m_timebTick.time>m_timebTick.time)
							||((g_pconduit->m_data.m_timebTick.time==m_timebTick.time)&&(g_pconduit->m_data.m_timebTick.millitm>m_timebTick.millitm))
							)
						{
							SetProgress(CXDLG_EVILEYE);  // default settings.
						}
					} break;
				case CONDUIT_STATUS_THREAD_END:
					{
						if(m_ulConduitFlags!=CONDUIT_STATUS_THREAD_END)
						{
							m_ulConduitFlags=CONDUIT_STATUS_THREAD_END;
							SetProgressColor(RGB(192,0,0));
						}
						if(
								(g_pconduit->m_data.m_timebTick.time>m_timebTick.time)
							||((g_pconduit->m_data.m_timebTick.time==m_timebTick.time)&&(g_pconduit->m_data.m_timebTick.millitm>m_timebTick.millitm))
							)
						{
							SetProgress(CXDLG_EVILEYE);  // default settings.
						}
					} break;
				} //switch
				m_timebTick.time = g_pconduit->m_data.m_timebTick.time;
				m_timebTick.millitm = g_pconduit->m_data.m_timebTick.millitm;
			}
		}
	}
	else
	CDialog::OnTimer(nIDEvent);
}

void CConduitDlg::StatusMessage(char* pszFormat, ...)
{
	if((pszFormat)&&(strlen(pszFormat)))
	{
		_timeb timestamp;
		_ftime( &timestamp );

		char buffer[MAX_MESSAGE_LENGTH];
		tm* theTime = localtime( &timestamp.time	);
		strftime(buffer, 30, "%H:%M:%S.", theTime );
		int nOffest = strlen(buffer);

		sprintf(buffer+nOffest,"%03d   ",timestamp.millitm);
		nOffest = strlen(buffer);
		va_list marker;
		// create the formatted output string
		va_start(marker, pszFormat); // Initialize variable arguments.
		_vsnprintf((char *) (buffer+nOffest), MAX_MESSAGE_LENGTH-1, pszFormat, (va_list) marker);
		va_end( marker );             // Reset variable arguments.

//		m_lce.InsertItem(m_lce.GetItemCount(), buffer);  
		m_lce.InsertItem(0, buffer);  // keep it at top.

	}
}

void CConduitDlg::SetStatus()
{
//return;
	if(g_pconduit==NULL) return;
	unsigned long ulFlags = g_pconduit->m_data.m_ulFlags;
	if((ulFlags&CONDUIT_STATUS_THREAD_MASK) == CONDUIT_STATUS_THREAD_ENDED) return;

	// fill the status display with the current status.
	m_bStatusMode = true;  // now we can handle clicks on status

// we want something like:

//  Watch folder: path
//  Destination folder: path


	int nStatusRows = 10; //max(1,g_pconduit->m_data.m_nNumDownloadObjects);//g_pconduit->m_data.m_nNumConnectionObjects + g_pconduit->m_data.m_nNumChannelObjects;
	int nRows = m_lce.GetItemCount();

	int i=0, n=0;
	CString szText;
	if(!(
			(g_pconduit->m_data.m_key.m_bValid)
		&&(
				(!g_pconduit->m_data.m_key.m_bExpires)
			||((g_pconduit->m_data.m_key.m_bExpires)&&(!g_pconduit->m_data.m_key.m_bExpired))
			||((g_pconduit->m_data.m_key.m_bExpires)&&(g_pconduit->m_data.m_key.m_bExpireForgiveness)&&(g_pconduit->m_data.m_key.m_ulExpiryDate+g_pconduit->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
			)
		&&(
				(!g_pconduit->m_data.m_key.m_bMachineSpecific)
			||((g_pconduit->m_data.m_key.m_bMachineSpecific)&&(g_pconduit->m_data.m_key.m_bValidMAC))
			)
		))
	{
		nStatusRows +=2;

		if(i<nRows)
			m_lce.SetItemText(i,0, "*** INVALID LICENSE KEY ***");
		else
			m_lce.InsertItem( i, "*** INVALID LICENSE KEY ***" ); 
		i++;
		
		if(i<nRows)
			m_lce.SetItemText(i,0, "");
		else
			m_lce.InsertItem( i, "" ); 
		i++; // space
	}

	if(g_pconduit->m_data.m_bProcessSuspended)
	{
		nStatusRows +=2;

		if(i<nRows)
			m_lce.SetItemText(i,0, "[SUSPENDED]");
		else
			m_lce.InsertItem( i, "[SUSPENDED]" ); 
		i++;
		
		if(i<nRows)
			m_lce.SetItemText(i,0, "");
		else
			m_lce.InsertItem( i, "" ); 
		i++; // space
	}

/*

	while(i<nStatusRows)
	{
		CString szNew = "";
		if(i<nRows)
		{
			szText = m_lce.GetItemText(i,0);
		}
		else
		{
			szText = "";
			m_lce.InsertItem( i, "" );
		}

		if(g_pconduit->m_data.m_nNumDownloadObjects>0)
		{

			if(
					(n<g_pconduit->m_data.m_nNumDownloadObjects)
				&&(g_pconduit->m_data.m_ppDownloadObj)
				&&(g_pconduit->m_data.m_ppDownloadObj[n])
				)
			{
				szNew.Format("%s (%s)",
					g_pconduit->m_data.m_ppDownloadObj[n]->m_pszName, 
					g_pconduit->m_data.m_ppDownloadObj[n]->m_pszDesc
					);

				CString szInfo = "";

				if(g_pconduit->m_data.m_ppDownloadObj[n]->m_hinstDLL==NULL)
				{
					szInfo.Format(" ERROR loading module");
					szNew += szInfo;
				}
				else
				if(g_pconduit->m_data.m_ppDownloadObj[n]->m_lpfnDllCtrl==NULL)
				{
					szInfo.Format(" ERROR obtaining process address");
					szNew += szInfo;
				}
				else
				if(g_pconduit->m_data.m_ppDownloadObj[n]->m_pDLLdata)
				{
					int d=0;
					while(d<g_pconduit->m_data.m_ppDownloadObj[n]->m_pDLLdata->nNumThreads)
					{
						CString szInfo = "";
						if(
							  (g_pconduit->m_data.m_ppDownloadObj[n]->m_pDLLdata->thread)
							&&(g_pconduit->m_data.m_ppDownloadObj[n]->m_pDLLdata->thread[d])
							&&(g_pconduit->m_data.m_ppDownloadObj[n]->m_pDLLdata->thread[d]->pszThreadName)
							&&(strlen(g_pconduit->m_data.m_ppDownloadObj[n]->m_pDLLdata->thread[d]->pszThreadName)>0)
							)
						{
							szInfo.Format(" [%s%s%s %02x]", 
								g_pconduit->m_data.m_ppDownloadObj[n]->m_pDLLdata->thread[d]->pszThreadName,
								(((g_pconduit->m_data.m_ppDownloadObj[n]->m_pDLLdata->thread[d]->pszThreadStateText)&&(strlen(g_pconduit->m_data.m_ppDownloadObj[n]->m_pDLLdata->thread[d]->pszThreadStateText)>0))?" ":""),
								g_pconduit->m_data.m_ppDownloadObj[n]->m_pDLLdata->thread[d]->pszThreadStateText,
								g_pconduit->m_data.m_ppDownloadObj[n]->m_pDLLdata->thread[d]->nThreadState
								);

							szNew += szInfo;
						}
		/*
						else
						{
							szNew += " [X]";
						}
		* /
						d++;
					}
				}	//else						szNew += " [NULL]";

			}
			else
			{
				szNew.Format("Unknown module"); 
			}
		}
		else
		{
			szNew.Format("No modules are installed");
		}

		if(szText.Compare(szNew))
		{
			m_lce.SetItemText(i, 0, szNew); 
		}

		n++;
		i++;
	}
*/


	while(nRows>nStatusRows)
	{
		i = m_lce.GetItemCount();
		m_lce.DeleteItem(i-1);
		nRows--;
	}

}


void CConduitDlg::OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
/*
	// TODO: Add your control notification handler code here
	if(g_pconduit->m_data.m_nNumObjects)
	{
		int nRows = m_lce.GetItemCount();
		if(nRows == g_pconduit->m_data.m_nNumObjects)
		{
			nRows = m_lce.GetNextItem( -1,  LVNI_SELECTED );
			if(nRows>=0)
			{
				if((g_pconduit->m_data.m_ppObj)&&(g_pconduit->m_data.m_ppObj[nRows])&&(g_pconduit->m_data.m_ppObj[nRows]->m_pDlg))
				{
					if(g_pconduit->m_data.m_ppObj[nRows]->m_pDlg->IsWindowVisible())
					{
//						AfxMessageBox("hide");
						g_pconduit->m_data.m_ppObj[nRows]->m_pDlg->ShowWindow(SW_HIDE);
					}
					else
					{
//						AfxMessageBox("show");
						g_pconduit->m_data.m_ppObj[nRows]->m_pDlg->ShowWindow(SW_SHOW);
					}
				}
			}
		}
	}

	SetFocus();
*/	
	*pResult = 0;
}

LRESULT CConduitDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	switch(message) 
	{
	case WM_QUERYENDSESSION:
		{
	g_pconduit->m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Conduit", "The query end session message has been received.");  //(Dispatch message)
			// spark an interrupt process.
			if(_beginthread(ShutdownThread, 0, (void*) this)==-1)
			{
				//?
			}
			return FALSE; // stop the process.

		} break;

	case WM_ENDSESSION:
		{
		} break;
	case WM_CLOSE:
		{
		} break;
	}
	
	return CDialog::DefWindowProc(message, wParam, lParam);
}


void ShutdownThread(void* pvArgs)
{
	CConduitDlg* pDlg = (CConduitDlg*)pvArgs;
	if(pDlg==NULL) return;

	if(AfxMessageBox("To end the session, you must shut down Conduit.\r\nDo you wish to proceed?", MB_YESNO)==IDYES)
	{
	g_pconduit->m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Conduit", "Conduit is shutting down due to an end session request.");  //(Dispatch message)
		((CConduitHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

		while(g_bThreadStarted) Sleep(1);


		Sleep(250); // just to settle down other things.

		// ah, let them just shut down again, once cortex is shut down.
//		ExitWindowsEx(EWX_SHUTDOWN|EWX_FORCEIFHUNG, SHTDN_REASON_MAJOR_OTHER|SHTDN_REASON_MINOR_OTHER|SHTDN_REASON_FLAG_PLANNED);
	}
}

BOOL CConduitDlg::PreTranslateMessage(MSG* pMsg) 
{
	if(g_pconduit)
	{
		if (pMsg->message == WM_SYSKEYDOWN)  //means alt
		{
			if(((pMsg->wParam==VK_SHIFT)||(pMsg->wParam==83)||(pMsg->wParam==VK_MENU))&&(GetAsyncKeyState(83)&0x8000)&&(GetAsyncKeyState(VK_SHIFT)&0x8000)&&(GetAsyncKeyState(VK_MENU)&0x8000))
			{
				g_pconduit->m_settings.GetFromDatabase();
			}
		}
		else
		if (pMsg->message == WM_KEYDOWN)
		{
			if(((pMsg->wParam==VK_SHIFT)||(pMsg->wParam==83)||(pMsg->wParam==VK_MENU))&&(GetAsyncKeyState(83)&0x8000)&&(GetAsyncKeyState(VK_SHIFT)&0x8000)&&(GetAsyncKeyState(VK_MENU)&0x8000))
			{
				g_pconduit->m_settings.GetFromDatabase();
			}
		}		
	}

		
	return CDialog::PreTranslateMessage(pMsg);
}
