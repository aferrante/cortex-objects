#if !defined(AFX_CONDUITDLG_H__4CB22BD5_B9CF_42A3_8692_1831BFF8A31D__INCLUDED_)
#define AFX_CONDUITDLG_H__4CB22BD5_B9CF_42A3_8692_1831BFF8A31D__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ConduitDlg.h : header file
//

#define CONDUITDLG_MINSIZEX 200
#define CONDUITDLG_MINSIZEY 150


#define ID_LIST 0
#define ID_BNSETTINGS 1
//#define ID_BNPAUSE 2
#define ID_STSTATUS 2
#define ID_STPROG 3

#define CONDUITDLG_NUM_MOVING_CONTROLS 4

#define BMP_SETTINGS 0
#define BMP_PROG 1
//#define BMP_PAUSE 1
//#define BMP_PLAY 2
#define MAX_BMPS 2

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
public: 
	HCURSOR m_hcurLink;

protected:
	//{{AFX_MSG(CAboutDlg)
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	virtual BOOL OnInitDialog();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//}}AFX_MSG


public:
  void OnLogo(UINT nFlags, CPoint point) ;


	DECLARE_MESSAGE_MAP()
};


#define CXDLG_CLEAR		0x00000000
#define CXDLG_WAITING 0x10000000
#define CXDLG_EVILEYE 0x20000000
#define CXDLG_MASK		0x30000000
#define	CXDLG_PROGCHAR '-'//'�'

/////////////////////////////////////////////////////////////////////////////
// CConduitDlg dialog

class CConduitDlg : public CDialog
{
// Construction
public:
	CConduitDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CConduitDlg)
	enum { IDD = IDD_CONDUIT_DIALOG };
	CTreeCtrl	m_tree;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CConduitDlg)
	public:
	virtual BOOL Create();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

	//  main status list
public:
	CListCtrlEx m_lce;
	bool m_bStatusMode;

protected:	
	CBmpUtil m_bmpu;

	HBITMAP m_hbmp[MAX_BMPS];

	//sizing
	BOOL m_bVis;
	BOOL 	m_bNewSizeInit;
	CRect m_rcDlg;
	CRect m_rcCtrl[CONDUITDLG_NUM_MOVING_CONTROLS];
	CSize m_sizeDlgMin;

	BOOL OnToolTipNotify( UINT id, NMHDR* pNMHDR, LRESULT* pResult );

	_timeb m_timebTick;
	COLORREF m_cr; // current evil eye color

// Implementation
protected:
	HICON m_hIcon;
	int m_nDxEye;
	unsigned long m_ulFlags;
	unsigned long m_ulConduitFlags;

	// Generated message map functions
	//{{AFX_MSG(CConduitDlg)
	afx_msg void OnPaint();
	afx_msg void OnButtonSettings();
	virtual BOOL OnInitDialog();
	afx_msg void OnGetMinMaxInfo( MINMAXINFO FAR* lpMMI );
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void OnExit();
	void ShellExecuteSettings();

	void SetProgress(unsigned long ulFlags);
	void SetProgressColor(COLORREF cr);

	void SetStatus();
	void StatusMessage(char* pszFormat, ...);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONDUITDLG_H__4CB22BD5_B9CF_42A3_8692_1831BFF8A31D__INCLUDED_)
