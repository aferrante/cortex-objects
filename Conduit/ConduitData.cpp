// ConduitData.cpp: implementation of the CConduitData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <process.h>
#include "Conduit.h"
#include "ConduitHandler.h" 
#include "ConduitMain.h" 
#include "ConduitData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CConduitMain* g_pconduit;
extern CConduitApp theApp;
//extern CADC g_adc; 	// the Harris ADC object

extern bool g_bKillThread;

void ConduitAutomationThread(void* pvArgs);

CMessager* g_pmsgr = NULL;

//////////////////////////////////////////////////////////////////////
// CConduitEvent Construction/Destruction
//////////////////////////////////////////////////////////////////////

CConduitEvent::CConduitEvent()
{
	m_ulFlags = CONDUIT_FLAG_DISABLED;
	m_dblTriggerTime = -1.0; 
	m_bAnalyzed = false;
	m_bReAnalyzed = false;
	m_nEventID = -1;	
	m_nAnalyzedTriggerID = -1;
	m_nType = -1;
	m_nDestType = -1;
}

CConduitEvent::~CConduitEvent()
{
}

CConduitEventArray::CConduitEventArray()
{
	m_nEvents = 0; 
	m_ppEvents = NULL;
	InitializeCriticalSection(&m_critEvents);
	m_nEventMod=0;
	m_nLastEventMod=-1;
}

CConduitEventArray::~CConduitEventArray()
{
	EnterCriticalSection(&m_critEvents);
	DeleteEvents();
	LeaveCriticalSection(&m_critEvents);
	DeleteCriticalSection(&m_critEvents);
}

void	CConduitEventArray::DeleteEvents()
{
	if((m_nEvents)&&(m_ppEvents))
	{
		int i=0;
		while(i<m_nEvents)
		{
			if(m_ppEvents[i]) delete m_ppEvents[i];
			m_ppEvents[i] = NULL;
			i++;
		}
		delete [] m_ppEvents;
	}
	m_ppEvents = NULL;
	m_nEvents = 0;
}


//////////////////////////////////////////////////////////////////////
// CConduitConnectionObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*
CConduitConnectionObject::CConduitConnectionObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_pszClientName = NULL;
	m_ulStatus	= CONDUIT_STATUS_UNINIT;
	m_ulFlags = CONDUIT_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
//	m_pAPIConn = NULL;
	m_bKillConnThread = true;
	m_bConnThreadStarted = false;

}

CConduitConnectionObject::~CConduitConnectionObject()
{
	m_bKillConnThread = true;
	while(	m_bConnThreadStarted ) Sleep(1);

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
	if(m_pszClientName) free(m_pszClientName); // must use malloc to allocate
}

*/

//////////////////////////////////////////////////////////////////////
// CConduitDownloadObject Construction/Destruction
//////////////////////////////////////////////////////////////////////



CConduitDownloadObject::CConduitDownloadObject()
{
	m_hinstDLL=NULL; // DLL load...
	m_lpfnDllCtrl=NULL; // pointer to function
	m_pDLLdata=NULL;
	
	m_pszDesc		= NULL;
	m_pszName = NULL;
	m_pszModule = NULL;
	m_ulStatus	= CONDUIT_STATUS_UNINIT;
	m_ulFlags = CONDUIT_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
	m_nID=-1;  // the unique plugin ID within Conduit setup. (assigned externally)

	m_nSettingsMod = 0;
	m_nLastSettingsMod = -1;

}


CConduitDownloadObject::~CConduitDownloadObject()
{
	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_pszModule) free(m_pszModule); // must use malloc to allocate
}


//////////////////////////////////////////////////////////////////////
// CPeriodicQuery Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPeriodicObject::CPeriodicObject()
{
	m_pszObject = NULL;
	m_pszReturnObject = NULL;
	m_ulInterval = 0; // number of seconds between calls
	m_ulOffset=0;   // number of seconds to offset
	m_ulLastSent=0; // unixtime, last sent.
}

CPeriodicObject::~CPeriodicObject()
{
	if(m_pszObject) free(m_pszObject);
	if(m_pszReturnObject) free(m_pszReturnObject);
}





//////////////////////////////////////////////////////////////////////
// CConduitData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CConduitData::CConduitData()
{
	InitializeCriticalSection(&m_critText);
	InitializeCriticalSection(&m_critDownloader);
	InitializeCriticalSection(&m_critSQL);

	InitializeCriticalSection(&m_critQueries);
	InitializeCriticalSection(&m_critShell);
	InitializeCriticalSection(&m_critHttp);

	// messaging...
	m_bNetworkMessagingInitialized=false;

	m_bCheckModsWarningSent = false;

//	m_ppConnObj = NULL;
//	m_nNumConnectionObjects = 0;
//	m_ppDownloadObj = NULL;
//	m_nNumDownloadObjects = 0;

	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebAutoPurge ); // the last time autopurge was run
	_ftime( &m_timebTick );
	m_pszHost = NULL;			// the name of the host
	m_pszCompleteHost = NULL;	// the complete name of the host
	m_pszCortexHost = NULL;			// the name of the cortex host
	m_usCortexCommandPort = CONDUIT_PORT_CMD;
	m_usCortexStatusPort = CONDUIT_PORT_STATUS;
	m_nSettingsMod = -1;
	m_nDownloadMod = -1;
//	m_nConnectionsMod = -1;
	m_nLastSettingsMod = -1;
	m_nLastDownloadMod = -1;
//	m_nLastConnectionsMod = -1;

//	m_nLastDownloader=-1;


	m_nQueueMod =-1;
	m_nLastQueueMod =-1;

	m_pdb = NULL;
	m_pdbConn = NULL;
	m_pdb2 = NULL;
	m_pdb2Conn = NULL;

	m_bQuietKill = false;

	m_bProcessSuspended = false;
//	m_bAutomationThreadStarted=false;
//	m_dblLastAutomationChange= -1.0;

	m_ppPeriodicQuery = NULL;	
	m_nNumQueries = 0;
	m_ulQueriesStart = 0;

	m_ppPeriodicShell = NULL;	
	m_nNumShellCmds = 0;
	m_ulShellStart = 0;

	m_ppPeriodicHttp = NULL;	
	m_nNumHttpCalls = 0;
	m_ulHttpStart = 0;



}

CConduitData::~CConduitData()
{

	if(m_ppPeriodicQuery)
	{
		int i=0;
		while(i<m_nNumQueries)
		{
			if(m_ppPeriodicQuery[i]) delete m_ppPeriodicQuery[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppPeriodicQuery; // delete array of pointers to objects, must use new to allocate
	}
	if(m_ppPeriodicShell)
	{
		int i=0;
		while(i<m_nNumShellCmds)
		{
			if(m_ppPeriodicShell[i]) delete m_ppPeriodicShell[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppPeriodicShell; // delete array of pointers to objects, must use new to allocate
	}
	if(m_ppPeriodicHttp)
	{
		int i=0;
		while(i<m_nNumHttpCalls)
		{
			if(m_ppPeriodicHttp[i]) delete m_ppPeriodicHttp[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppPeriodicHttp; // delete array of pointers to objects, must use new to allocate
	}


/*
	if(m_ppConnObj)
	{
		int i=0;
		while(i<m_nNumConnectionObjects)
		{
			if(m_ppConnObj[i]) delete m_ppConnObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppConnObj; // delete array of pointers to objects, must use new to allocate
	}

	if(m_ppDownloadObj)
	{
		int i=0;
		while(i<m_nNumDownloadObjects)
		{
			if(m_ppDownloadObj[i]) delete m_ppDownloadObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppDownloadObj; // delete array of pointers to objects, must use new to allocate
	}
*/
	if(m_pszHost) free(m_pszHost);	// must use malloc to allocate
	if(m_pszCompleteHost) free(m_pszCompleteHost);	// must use malloc to allocate
	if(m_pszCortexHost) free(m_pszCortexHost);	// must use malloc to allocate

	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critDownloader);
	DeleteCriticalSection(&m_critSQL);

	DeleteCriticalSection(&m_critQueries);
	DeleteCriticalSection(&m_critShell);
	DeleteCriticalSection(&m_critHttp);

}

char* CConduitData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CConduitData::SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError)
{
	int nRV = CX_ERROR;
	bool bError=false;

	if(!bOverwriteExistingError)
	{
		if(
			  ((m_ulFlags&CONDUIT_ICON_MASK) == CONDUIT_STATUS_ERROR)
			||((m_ulFlags&CONDUIT_STATUS_CMDSVR_MASK) == CONDUIT_STATUS_CMDSVR_ERROR)
			||((m_ulFlags&CONDUIT_STATUS_STATUSSVR_MASK) ==  CONDUIT_STATUS_STATUSSVR_ERROR)
			||((m_ulFlags&CONDUIT_STATUS_THREAD_MASK) == CONDUIT_STATUS_THREAD_ERROR)
			||((m_ulFlags&CONDUIT_STATUS_FAIL_MASK) == CONDUIT_STATUS_FAIL_DB)
			)	bError=true;
	}

	if (ulStatus&CONDUIT_ICON_MASK)
	{
		m_ulFlags &= ~CONDUIT_ICON_MASK;
		m_ulFlags |= (ulStatus&CONDUIT_ICON_MASK);
	}
	if (ulStatus&CONDUIT_STATUS_CMDSVR_MASK)
	{
		m_ulFlags &= ~CONDUIT_STATUS_CMDSVR_MASK;
		m_ulFlags |= (ulStatus&CONDUIT_STATUS_CMDSVR_MASK);
	}
	if (ulStatus&CONDUIT_STATUS_STATUSSVR_MASK)
	{
		m_ulFlags &= ~CONDUIT_STATUS_STATUSSVR_MASK;
		m_ulFlags |= (ulStatus&CONDUIT_STATUS_STATUSSVR_MASK);
	}
	if (ulStatus&CONDUIT_STATUS_THREAD_MASK)
	{
		m_ulFlags &= ~CONDUIT_STATUS_THREAD_MASK;
		m_ulFlags |= (ulStatus&CONDUIT_STATUS_THREAD_MASK);
	}
	if (ulStatus&CONDUIT_STATUS_FAIL_MASK)
	{
		m_ulFlags &= ~CONDUIT_STATUS_FAIL_MASK;
		m_ulFlags |= (ulStatus&CONDUIT_STATUS_FAIL_MASK);
	}

	if(bError)
	{
		m_ulFlags &= ~CONDUIT_ICON_MASK;
		m_ulFlags |= CONDUIT_STATUS_ERROR;
	}

	if((pszText)&&(!bError))
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
//				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
//			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_pconduit->m_settings.m_pszIconPath)&&(strlen(g_pconduit->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_pconduit->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_pconduit->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_pconduit->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_pconduit->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_pconduit->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}

// utility
int	CConduitData::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszCompleteHost) free(m_pszCompleteHost);
						m_pszCompleteHost=pch;

						pch = (char*) malloc(nValue+1);
						if(pch!=NULL)
						{
							if(m_pszHost) free(m_pszHost);
							m_pszHost=pch;

							strcpy(m_pszHost, m_pszCompleteHost);
							pch = strchr(m_pszHost, '.');
							if(pch)
							{
								*pch = 0;
							}

							return CONDUIT_SUCCESS;
						}
					}
				}
			}
		}
	}
	return CONDUIT_ERROR;
}



/*
int CConduitData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_pconduit)&&(g_pconduit->m_settings.m_pszExchange)&&(strlen(g_pconduit->m_settings.m_pszExchange)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s set mod = \
(SELECT case when max(mod) is null then 1 when max(mod) >= %d \
then 1 else max(mod) + 1 end from %s WHERE criterion = '%s') WHERE criterion = '%s'",
			g_pconduit->m_settings.m_pszExchange,
			CONDUIT_DB_MOD_MAX,
			g_pconduit->m_settings.m_pszExchange,
			szTemp, szTemp		
			);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			return CONDUIT_SUCCESS;
		}
	}
	return CONDUIT_ERROR;
}


int CConduitData::CheckMessages(char* pszInfo)
{
	if((g_pconduit)&&(m_pdbConn)&&(m_pdb)
		&&(g_pconduit->m_settings.m_pszMessages)&&(strlen(g_pconduit->m_settings.m_pszMessages)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_pconduit->m_settings.m_pszMessages)&&(strlen(g_pconduit->m_settings.m_pszMessages)))?g_pconduit->m_settings.m_pszMessages:"Messages",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_pconduit->m_settings.m_nAutoPurgeMessageDays*86400) // local time....
			);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return CONDUIT_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return CONDUIT_ERROR;
}

int CConduitData::CheckAsRun(char* pszInfo)
{
	if((g_pconduit)&&(m_pdbConn)&&(m_pdb)
		&&(g_pconduit->m_settings.m_pszAsRun)&&(strlen(g_pconduit->m_settings.m_pszAsRun)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_pconduit->m_settings.m_pszAsRun)&&(strlen(g_pconduit->m_settings.m_pszAsRun)))?g_pconduit->m_settings.m_pszAsRun:"AsRun_Log",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_pconduit->m_settings.m_nAutoPurgeAsRunDays*86400) // local time....
			);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return CONDUIT_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return CONDUIT_ERROR;
}





int CConduitData::CheckDatabaseMods(char* pszInfo)
{
	if((g_pconduit)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY criterion DESC, mod ASC",  // puts suspend at the top, and orders timestamped mods with oldest first! 
			((g_pconduit->m_settings.m_pszExchange)&&(strlen(g_pconduit->m_settings.m_pszExchange)))?g_pconduit->m_settings.m_pszExchange:"Exchange");

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			bool bFileChanges = false;
			int nReturn = CONDUIT_ERROR;
			int nIndex = 0;
			while ((!(prs->IsEOF()))&&(!bFileChanges)&&(prs))
			{
				CString szCriterion;
				CString szFlag;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("flag", szFlag);//HARDCODE
					szFlag.TrimLeft(); szFlag.TrimRight();
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
					prs->Close();
					delete prs;
					prs=NULL;
					break;
				}

//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "retrieved %s, %s, %s", szCriterion, szFlag, szMod);    //(Dispatch message)


				if((g_pconduit->m_settings.m_pszSettings)&&(strlen(g_pconduit->m_settings.m_pszSettings)))
				{
					szTemp.Format("DBT_%s",g_pconduit->m_settings.m_pszSettings);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nSettingsMod = nReturn;
					}
				}

				if((g_pconduit->m_settings.m_pszQueue)&&(strlen(g_pconduit->m_settings.m_pszQueue)))
				{
					szTemp.Format("DBT_%s",g_pconduit->m_settings.m_pszQueue);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nQueueMod = nReturn;
					}
				} 
				
				if((g_pconduit->m_settings.m_pszDownloaders)&&(strlen(g_pconduit->m_settings.m_pszDownloaders)))
				{
					szTemp.Format("DBT_%s",g_pconduit->m_settings.m_pszDownloaders);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nDownloadMod = nReturn;
					}
				}
			
				if((g_pconduit->m_settings.m_pszEvents)&&(strlen(g_pconduit->m_settings.m_pszEvents)>0))
				{
					szTemp.Format("DBT_%s",g_pconduit->m_settings.m_pszEvents);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
if(g_pconduit->m_settings.m_ulDebug&CONDUIT_DEBUG_EXCHANGE)	
	g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "DBT_%s has %s",g_pconduit->m_settings.m_pszEvents, szMod); 
						nReturn = atoi(szMod);
						if(nReturn>0)
						{
	EnterCriticalSection(&m_eventarray.m_critEvents);
							m_eventarray.m_nEventMod = nReturn;
	LeaveCriticalSection(&m_eventarray.m_critEvents);
						}
					}
//					else
//					{
//if(g_pconduit->m_settings.m_ulDebug&CONDUIT_DEBUG_EXCHANGE)	
//	g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "criterion was %s",szTemp); 
//					}
				}

				if((m_ppDownloadObj)&&(m_nNumDownloadObjects>0))
				{
					int x=0;
					while(x<m_nNumDownloadObjects)
					{
						if((m_ppDownloadObj[x])&&(m_ppDownloadObj[x]->m_pszName))
						{
							szTemp.Format("DBT_%s_%d_Settings",m_ppDownloadObj[x]->m_pszName, m_ppDownloadObj[x]->m_nID);
							if(szCriterion.CompareNoCase(szTemp)==0)
							{
//add logging here
								
								nReturn = atoi(szMod);
								if(nReturn>0) m_ppDownloadObj[x]->m_nSettingsMod = nReturn;
								if(m_ppDownloadObj[x]->m_nSettingsMod != m_ppDownloadObj[x]->m_nLastSettingsMod)
								{
	//								if((m_ppDownloadObj[x]->m_pDLLdata==NULL)&&(m_ppDownloadObj[x]->m_lpfnDllCtrl)) // BUG
									if((m_ppDownloadObj[x]!=NULL)&&(m_ppDownloadObj[x]->m_lpfnDllCtrl))
									{
										int nCode = m_ppDownloadObj[x]->m_lpfnDllCtrl((void**)(NULL), DLLCMD_GETDBSETTINGS);
										if(nCode<CLIENT_SUCCESS)
										{
											/// do something, report?
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error sending database settings change to module %s: %s (%s)", 
																m_ppDownloadObj[x]->m_pszModule, m_ppDownloadObj[x]->m_pszName,m_ppDownloadObj[x]->m_pszDesc );
											g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:module_settings", errorstring);    //(Dispatch message)
										}
										else
										{
											m_ppDownloadObj[x]->m_nLastSettingsMod = m_ppDownloadObj[x]->m_nSettingsMod;
										}
									}
								}
							}
						}
						x++;
					}
				}
			
				// get the suspend

				szTemp.Format("Suspend");
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					nReturn = atoi(szMod);
					if(nReturn>0)
					{
						if(!m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Conduit is suspended.");  
							g_pconduit->m_data.SetStatusText(errorstring, CX_STATUS_UNKNOWN);
							g_pconduit->m_msgr.DM(MSG_ICONNONE, NULL, "Conduit:suspend", "*** Conduit has been suspended. ***"); // Sleep(50); //(Dispatch message)
							g_pconduit->SendMsg(CX_SENDMSG_INFO, "Conduit:suspend", "Conduit has been suspended");


							// have to send all the plugins a global suspend call
							if((m_ppDownloadObj)&&(m_nNumDownloadObjects>0))
							{
								int x=0;
								while(x<m_nNumDownloadObjects)
								{
									if(m_ppDownloadObj[x])
									{
										szTemp.Format("suspend");
										if((m_ppDownloadObj[x]->m_pDLLdata==NULL)&&(m_ppDownloadObj[x]->m_lpfnDllCtrl))
										{
											int nCode = m_ppDownloadObj[x]->m_lpfnDllCtrl((void**)(szTemp.GetBuffer(0)), DLLCMD_SETGLOBALSTATE);
											szTemp.ReleaseBuffer();
											if(nCode<CLIENT_SUCCESS)
											{
												/// do something, report?
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error setting global state in module %s: %s (%s)", 
																	m_ppDownloadObj[x]->m_pszModule, m_ppDownloadObj[x]->m_pszName,m_ppDownloadObj[x]->m_pszDesc );
												g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:module_suspend", errorstring);    //(Dispatch message)
											}
										}
									}
									x++;
								}
							}
	
							m_bProcessSuspended = true;
						}
					}
					else
					{
						if(m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Conduit is running.");  
							g_pconduit->m_data.SetStatusText(errorstring, CX_STATUS_OK);
							g_pconduit->m_msgr.DM(MSG_ICONNONE, NULL, "Conduit:resume", "*** Conduit has been resumed. ***");//  Sleep(50); //(Dispatch message)
							g_pconduit->SendMsg(CX_SENDMSG_INFO, "Conduit:resume", "Conduit has been resumed.");

							// have to send all the plugins a global resume call
							if((m_ppDownloadObj)&&(m_nNumDownloadObjects>0))
							{
								int x=0;
								while(x<m_nNumDownloadObjects)
								{
									if(m_ppDownloadObj[x])
									{
										szTemp.Format("resume");
										if((m_ppDownloadObj[x]->m_pDLLdata==NULL)&&(m_ppDownloadObj[x]->m_lpfnDllCtrl))
										{
											int nCode = m_ppDownloadObj[x]->m_lpfnDllCtrl((void**)(szTemp.GetBuffer(0)), DLLCMD_SETGLOBALSTATE);
											szTemp.ReleaseBuffer();
											if(nCode<CLIENT_SUCCESS)
											{
												/// do something, report?
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error setting global state in module %s: %s (%s)", 
																	m_ppDownloadObj[x]->m_pszModule, m_ppDownloadObj[x]->m_pszName,m_ppDownloadObj[x]->m_pszDesc );
												g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:module_resume", errorstring);    //(Dispatch message)
											}
										}
									}
									x++;
								}
							}

							m_bProcessSuspended = false;
						}
					}
				}

				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			delete prs;
			prs = NULL;
			return nReturn;
		}
	}
	return CONDUIT_ERROR;
}




/*
int CConduitData::GetChannels(char* pszInfo)
{
	if((g_pconduit)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_pconduit->m_settings.m_pszChannels)&&(strlen(g_pconduit->m_settings.m_pszChannels)))?g_pconduit->m_settings.m_pszChannels:"Channels");

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = CONDUIT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szServer = "";
				CString szDesc = "";
				CString szTemp = "";
				unsigned long ulFlags;   // various flags
				int nListID = -1;
				int nChannelID = -1;
				int nTemp;
				bool bFound = false;
				bool bFlagsFound = false;
				try
				{
					prs->GetFieldValue("ID", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nChannelID = nTemp;
					}
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bFlagsFound = true;
						ulFlags = atol(szTemp);
					}
					prs->GetFieldValue("description", szDesc);//HARDCODE
					prs->GetFieldValue("server", szServer);//HARDCODE
					szServer.TrimLeft(); szServer.TrimRight();
					// **** if servername is >16 chars, need to truncate and flag error somewhere.  message?

					prs->GetFieldValue("listid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nListID = nTemp;
					}
				}
				catch( ... )
				{
				}

				if((g_pconduit->m_data.m_ppDownloadObj)&&(g_pconduit->m_data.m_nNumDownloadObjects))
				{
					nTemp=0;
					while(nTemp<g_pconduit->m_data.m_nNumDownloadObjects)
					{
						if(g_pconduit->m_data.m_ppDownloadObj[nTemp])
						{
							if(
									(szServer.GetLength()>0)
								&&(szServer.CompareNoCase(g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_pszServerName)==0)
								&&(nListID==g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_nHarrisListID)
								)
							{
								bFound = true;
								// override with the new changes:
								if(nChannelID>=0) g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_nChannelID = nChannelID;

								if(
										(szDesc.GetLength()>0)
									&&(szDesc.CompareNoCase(g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_pszDesc))
									)
								{
									if(g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_pszDesc) free(g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_pszDesc);

									g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_pszDesc) sprintf(g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_pszDesc, szDesc);
								}

								if((bFlagsFound)&&(ulFlags&CONDUIT_FLAG_ENABLED)&&(!((g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_ulFlags)&CONDUIT_FLAG_ENABLED)))
								{

// ****  do something here to check out if the list is able to be activated, that is, if it's on a connected connection

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating list: %s...", 
										g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_pszDesc?g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_pszDesc:g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_pszServerName);  
									g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:destination_change", errorstring);    //(Dispatch message)

// **** if it's able to be activated, set the status to connected.
									g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_ulStatus = CONDUIT_STATUS_CONN;
								}
								

								if((bFlagsFound)&&(!(ulFlags&CONDUIT_FLAG_ENABLED))&&((g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_ulFlags)&CONDUIT_FLAG_ENABLED))
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating list: %s...", 
										g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_pszDesc?g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_pszDesc:g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_pszServerName);  
									g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:destination_change", errorstring);    //(Dispatch message)

									g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_ulStatus = CONDUIT_STATUS_NOTCON;
								}

								// and set up the flags
								if(bFlagsFound) g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_ulFlags = ulFlags|CONDUIT_FLAG_FOUND;
								else g_pconduit->m_data.m_ppDownloadObj[nTemp]->m_ulFlags |= CONDUIT_FLAG_FOUND;
							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szServer.GetLength()>0)&&(nListID>0)) // have to add.
				{

					CConduitDownloadObject* pscho = new CConduitDownloadObject;
					if(pscho)
					{
						CConduitDownloadObject** ppObj = new CConduitDownloadObject*[g_pconduit->m_data.m_nNumDownloadObjects+1];
						if(ppObj)
						{
							int o=0;
							if((g_pconduit->m_data.m_ppDownloadObj)&&(g_pconduit->m_data.m_nNumDownloadObjects>0))
							{
								while(o<g_pconduit->m_data.m_nNumDownloadObjects)
								{
									ppObj[o] = g_pconduit->m_data.m_ppDownloadObj[o];
									o++;
								}
								delete [] g_pconduit->m_data.m_ppDownloadObj;

							}
							ppObj[g_pconduit->m_data.m_nNumDownloadObjects] = pscho;
							g_pconduit->m_data.m_ppDownloadObj = ppObj;
							g_pconduit->m_data.m_nNumDownloadObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szServer.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, szServer);

							ppObj[o]->m_nHarrisListID = nListID;

							if(nChannelID>=0) ppObj[o]->m_nChannelID = nChannelID;

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, szDesc);
							}
//							ppObj[o]->m_usType = not used;
							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|CONDUIT_FLAG_FOUND;
							else ppObj[o]->m_ulFlags |= CONDUIT_FLAG_FOUND;
								

// ****  do something here to check out if the list is able to be activated, that is, if it's on a connected connection
							if((ppObj[o]->m_ulFlags)&CONDUIT_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating list: %s...", 
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName);  
								g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:destination_change", errorstring);    //(Dispatch message)

// **** if it's able to be activated, set the status to connected.
								ppObj[o]->m_ulStatus = CONDUIT_STATUS_CONN;
							}

						}
						else
							delete pscho;
					}
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			prs = NULL;

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<g_pconduit->m_data.m_nNumDownloadObjects)
			{
				if((g_pconduit->m_data.m_ppDownloadObj)&&(g_pconduit->m_data.m_ppDownloadObj[nIndex]))
				{
					if((g_pconduit->m_data.m_ppDownloadObj[nIndex]->m_ulFlags)&CONDUIT_FLAG_FOUND)
					{
						(g_pconduit->m_data.m_ppDownloadObj[nIndex]->m_ulFlags) &= ~CONDUIT_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(g_pconduit->m_data.m_ppDownloadObj[nIndex])
						{
							if((g_pconduit->m_data.m_ppDownloadObj[nIndex]->m_ulFlags)&CONDUIT_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating list: %s...", 
									g_pconduit->m_data.m_ppDownloadObj[nIndex]->m_pszDesc?g_pconduit->m_data.m_ppDownloadObj[nIndex]->m_pszDesc:g_pconduit->m_data.m_ppDownloadObj[nIndex]->m_pszServerName);  
								g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:destination_remove", errorstring);    //(Dispatch message)
								g_pconduit->m_data.m_ppDownloadObj[nIndex]->m_ulStatus = CONDUIT_STATUS_NOTCON;
							}

							delete g_pconduit->m_data.m_ppDownloadObj[nIndex];
							g_pconduit->m_data.m_nNumDownloadObjects--;

							int nTemp=nIndex;
							while(nTemp<g_pconduit->m_data.m_nNumDownloadObjects)
							{
								g_pconduit->m_data.m_ppDownloadObj[nTemp]=g_pconduit->m_data.m_ppDownloadObj[nTemp+1];
								nTemp++;
							}
							g_pconduit->m_data.m_ppDownloadObj[nTemp] = NULL;
						} else nIndex++;
					}
				}
				else
					nIndex++;
			}

			return nReturn;
		}
	}
	return CONDUIT_ERROR;
}

int CConduitData::GetConnections(char* pszInfo)
{
	if((g_pconduit)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY server",  //HARDCODE
			((g_pconduit->m_settings.m_pszConnections)&&(strlen(g_pconduit->m_settings.m_pszConnections)))?g_pconduit->m_settings.m_pszConnections:"Connections");
//		g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "GetConnections");  Sleep(250); //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = CONDUIT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szHost="";
				CString szClient="";
				CString szDesc="";
				CString szTemp;
				unsigned long ulFlags;   // various flags
			//	unsigned short usType;
				int nTemp = -1;
				bool bFlagsFound = false;
				bool bFound = false;
				try
				{
					prs->GetFieldValue("server", szHost);//HARDCODE
					szHost.TrimLeft(); szHost.TrimRight();
					prs->GetFieldValue("description", szDesc);//HARDCODE
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulFlags = atol(szTemp);
					}
					prs->GetFieldValue("client", szClient);//HARDCODE
					szClient.TrimLeft(); szClient.TrimRight();
				}
				catch( ... )
				{
				}

				if((g_pconduit->m_data.m_ppConnObj)&&(g_pconduit->m_data.m_nNumConnectionObjects))
				{
					nTemp=0;
					while(nTemp<g_pconduit->m_data.m_nNumConnectionObjects)
					{
//			g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "c2");  Sleep(250); //(Dispatch message)
						if(g_pconduit->m_data.m_ppConnObj[nTemp])
						{
							if((szHost.GetLength()>0)&&(szHost.CompareNoCase(g_pconduit->m_data.m_ppConnObj[nTemp]->m_pszServerName)==0))
							{
								bFound = true;
//			g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "c3");  Sleep(250); //(Dispatch message)

								if(
										((bFlagsFound)&&(!(ulFlags&CONDUIT_FLAG_ENABLED))&&((g_pconduit->m_data.m_ppConnObj[nTemp]->m_ulFlags)&CONDUIT_FLAG_ENABLED))
									||(
												(szClient.GetLength()>0)&&(szClient.CompareNoCase(g_pconduit->m_data.m_ppConnObj[nTemp]->m_pszClientName)) // different client name
											&&(
													((bFlagsFound)&&(ulFlags&CONDUIT_FLAG_ENABLED))
												||((!bFlagsFound)&&((g_pconduit->m_data.m_ppConnObj[nTemp]->m_ulFlags)&CONDUIT_FLAG_ENABLED))
												)
										)
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", 
										g_pconduit->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_pconduit->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_pconduit->m_data.m_ppConnObj[nTemp]->m_pszServerName);  
									g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:destination_change", errorstring);    //(Dispatch message)
//									g_pconduit->m_data.m_ppConnObj[nTemp]->m_pDlg->OnDisconnect();
									//**** disconnect

//									g_adc.DisconnectServer(g_pconduit->m_data.m_ppConnObj[nTemp]->m_pszServerName);
									g_pconduit->m_data.m_ppConnObj[nTemp]->m_bKillConnThread = true;
									//**** should check return value....

									g_pconduit->m_data.m_ppConnObj[nTemp]->m_ulStatus = CONDUIT_STATUS_NOTCON;
								}

								if(
										((bFlagsFound)&&(ulFlags&CONDUIT_FLAG_ENABLED)&&(!((g_pconduit->m_data.m_ppConnObj[nTemp]->m_ulFlags)&CONDUIT_FLAG_ENABLED)))
									||(
												(szClient.GetLength()>0)&&(szClient.CompareNoCase(g_pconduit->m_data.m_ppConnObj[nTemp]->m_pszClientName)) // different client name
											&&(
													((bFlagsFound)&&(ulFlags&CONDUIT_FLAG_ENABLED))
												||((!bFlagsFound)&&((g_pconduit->m_data.m_ppConnObj[nTemp]->m_ulFlags)&CONDUIT_FLAG_ENABLED))
												)
										)
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s to %s...", 
										g_pconduit->m_data.m_ppConnObj[nTemp]->m_pszClientName?g_pconduit->m_data.m_ppConnObj[nTemp]->m_pszClientName:"Conduit",  
										g_pconduit->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_pconduit->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_pconduit->m_data.m_ppConnObj[nTemp]->m_pszServerName);  
									g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:destination_change", errorstring);    //(Dispatch message)
//									g_pconduit->m_data.m_ppConnObj[nTemp]->m_pDlg->OnButtonConnect();
									//**** if connect set following status

									g_pconduit->m_data.m_ppConnObj[nTemp]->m_bKillConnThread = false;
									g_pconduit->m_data.m_ppConnObj[nTemp]->m_pAPIConn = NULL;
									if(_beginthread(ConduitConnectionThread, 0, (void*)g_pconduit->m_data.m_ppConnObj[nTemp])==-1)
									{
										//error.

										//**MSG
							
										
									}
									//**** should check return value....

									g_pconduit->m_data.m_ppConnObj[nTemp]->m_ulStatus = CONDUIT_STATUS_CONN;
								}

								if(bFlagsFound) g_pconduit->m_data.m_ppConnObj[nTemp]->m_ulFlags = ulFlags|CONDUIT_FLAG_FOUND;
								else g_pconduit->m_data.m_ppConnObj[nTemp]->m_ulFlags |= CONDUIT_FLAG_FOUND;

							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szHost.GetLength()>0)) // have to add.
				{
//			g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "adding %s", szHost);  Sleep(250); //(Dispatch message)
					CConduitConnectionObject* pscno = new CConduitConnectionObject;
					if(pscno)
					{
//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CConduitConnectionObject** ppObj = new CConduitConnectionObject*[g_pconduit->m_data.m_nNumConnectionObjects+1];
						if(ppObj)
						{
//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((g_pconduit->m_data.m_ppConnObj)&&(g_pconduit->m_data.m_nNumConnectionObjects>0))
							{
								while(o<g_pconduit->m_data.m_nNumConnectionObjects)
								{
									ppObj[o] = g_pconduit->m_data.m_ppConnObj[o];
									o++;
								}
								delete [] g_pconduit->m_data.m_ppConnObj;

							}
							ppObj[o] = pscno;
							g_pconduit->m_data.m_ppConnObj = ppObj;
							g_pconduit->m_data.m_nNumConnectionObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szHost.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, "%s", szHost);

							if(szClient.GetLength()<=0)
							{
								szClient = "Conduit";
							}

							if(szClient.GetLength()>0)
							{
								ppObj[o]->m_pszClientName = (char*)malloc(szClient.GetLength()+1); 
								if(ppObj[o]->m_pszClientName) sprintf(ppObj[o]->m_pszClientName, "%s", szClient);
							}

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}
//							ppObj[o]->m_usType = not used;
							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|CONDUIT_FLAG_FOUND;
							else ppObj[o]->m_ulFlags |= CONDUIT_FLAG_FOUND;
								

							if((ppObj[o]->m_ulFlags)&CONDUIT_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s to %s...", 
									((ppObj[o]->m_pszClientName)&&(strlen(ppObj[o]->m_pszClientName)))?ppObj[o]->m_pszClientName:"Conduit",  
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName);  
								g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:destination_change", errorstring);    //(Dispatch message)

								//**** if connect 

								// create a connection object in g_adc.
								// associate that with the new CConduitConnectionObject;
								// start the thread.
								ppObj[o]->m_bKillConnThread = false;
								ppObj[o]->m_pAPIConn = NULL;
									g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "beginning thread for %s", szHost);  Sleep(250); //(Dispatch message)
								if(_beginthread(ConduitConnectionThread, 0, (void*)ppObj[o])==-1)
								{
									//error.

									//**MSG
									g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "problem adding %s", szHost);  Sleep(250); //(Dispatch message)

						
									
								}
							//**** should check return value....

								ppObj[o]->m_ulStatus = CONDUIT_STATUS_CONN;
							}
						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			prs = NULL;
//			g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<g_pconduit->m_data.m_nNumConnectionObjects)
			{
				if((g_pconduit->m_data.m_ppConnObj)&&(g_pconduit->m_data.m_ppConnObj[nIndex]))
				{
					if((g_pconduit->m_data.m_ppConnObj[nIndex]->m_ulFlags)&CONDUIT_FLAG_FOUND)
					{
						(g_pconduit->m_data.m_ppConnObj[nIndex]->m_ulFlags) &= ~CONDUIT_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(g_pconduit->m_data.m_ppConnObj[nIndex])
						{
							if((g_pconduit->m_data.m_ppConnObj[nIndex]->m_ulFlags)&CONDUIT_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", 
									g_pconduit->m_data.m_ppConnObj[nIndex]->m_pszDesc?g_pconduit->m_data.m_ppConnObj[nIndex]->m_pszDesc:g_pconduit->m_data.m_ppConnObj[nIndex]->m_pszServerName);  
								g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:destination_remove", errorstring);    //(Dispatch message)
//									g_pconduit->m_data.m_ppConnObj[nTemp]->m_pDlg->OnDisconnect();

								//**** disconnect

								g_pconduit->m_data.m_ppConnObj[nIndex]->m_bKillConnThread = false;
								while(g_pconduit->m_data.m_ppConnObj[nIndex]->m_bConnThreadStarted) Sleep(1);

								//g_adc.DisconnectServer(g_pconduit->m_data.m_ppConnObj[nIndex]->m_pszServerName);
								//**** should check return value....

								g_pconduit->m_data.m_ppConnObj[nIndex]->m_ulStatus = CONDUIT_STATUS_NOTCON;
							}

							delete g_pconduit->m_data.m_ppConnObj[nIndex];
							g_pconduit->m_data.m_nNumConnectionObjects--;

							int nTemp=nIndex;
							while(nTemp<g_pconduit->m_data.m_nNumConnectionObjects)
							{
								g_pconduit->m_data.m_ppConnObj[nTemp]=g_pconduit->m_data.m_ppConnObj[nTemp+1];
								nTemp++;
							}
							g_pconduit->m_data.m_ppConnObj[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

//			g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
	}
//			g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)
	return CONDUIT_ERROR;
}

*/

/*

int CConduitData::GetQueue(char* pszInfo)
{
	// following removed to deal with wait mode below.
/*
	if(g_miranda.m_bTransferring)
	{
		// something in progress, must wait.
		return CONDUIT_ERROR;
	}
* /
	
	if((g_pconduit)&&(m_pdbConn)&&(m_pdb))
	{

		// let's just get the top (oldest timestamp) item in the queue, and deal with it.

		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE action <> 128 ORDER BY timestamp ASC", //HARDCODE
			((g_pconduit->m_settings.m_pszQueue)&&(strlen(g_pconduit->m_settings.m_pszQueue)))?g_pconduit->m_settings.m_pszQueue:"Queue");

//create table Queue (itemid int identity(1,1) NOT NULL, filename_source varchar(256), filename_dest varchar(256), 
//action int, host varchar(64), timestamp float, username varchar(32));

		CString szFilenameSource = "";
		CString szFilenameDest = "";
		CString szServer = "";
		CString szUsername = "";
		CString szTemp = "";
		int nItemID = -1;
		bool bItemFound = false;
		double dblTimestamp=-57;   // timestamp
		int nAction = -1;
		int nTemp;

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = CONDUIT_ERROR;
			int nIndex = 0;
//			while ((!prs->IsEOF()))
			if ((!prs->IsEOF()))  // just do the one record, if there is one
			{
				try
				{
					prs->GetFieldValue("itemid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nItemID = atoi(szTemp);
						bItemFound = true;
					}
					prs->GetFieldValue("filename_source", szFilenameSource);//HARDCODE
					szFilenameSource.TrimLeft(); szFilenameSource.TrimRight();
					prs->GetFieldValue("filename_dest", szFilenameDest);//HARDCODE
					szFilenameDest.TrimLeft(); szFilenameDest.TrimRight();
					prs->GetFieldValue("action", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nAction = nTemp;
					}
					prs->GetFieldValue("host", szServer);//HARDCODE
					szServer.TrimLeft(); szServer.TrimRight();

					prs->GetFieldValue("timestamp", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						dblTimestamp = atof(szTemp);
					}
					prs->GetFieldValue("username", szUsername);//HARDCODE
					szUsername.TrimLeft(); szUsername.TrimRight();
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
* /
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();

				} 
				catch( ... )
				{
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			prs = NULL;

//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "got %d things from queue", nIndex);   Sleep(250);//(Dispatch message)

			if((nReturn>0)&&(bItemFound))
			{
				// process the request, then remove it from the queue
//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "got item %d from queue", nItemID);   Sleep(250);//(Dispatch message)


				// send remove SQL
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE itemid = %d",  //HARDCODE
					((g_pconduit->m_settings.m_pszQueue)&&(strlen(g_pconduit->m_settings.m_pszQueue)))?g_pconduit->m_settings.m_pszQueue:"Queue",
					nItemID
					);

/*
Conduit:
Queue action IDs
1  delete file from store
2  delete metadata record
3  delete file from store AND delete metadata record
4  move file (will update path in metadata record)
8  archive - moves file to some path, updates metadata record with archive info (date, etc)
19 purge (removes all files and metadata records for files that have non-registered filetypes)
34 clean (removes all metadata records where the file doesn't exist - not archived, and with an ingest date [means, dont delete a placeholder metadata record in anticipation of a new file coming in] ).

Queue table:
create table Queue (itemid int identity(1,1) NOT NULL, filename_source varchar(256), filename_dest varchar(256), 
action int, host varchar(64),timestamp int, username varchar(50));
* /

				if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
				{
					//**MSG
g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

				}
			}

			return nReturn;
		}
	}
	return CONDUIT_ERROR;
}


int CConduitData::FindDownloader(char* pszName, int nStartIndex)
{
	EnterCriticalSection(&m_critDownloader);
	if((pszName)&&(m_ppDownloadObj)&&(m_nNumDownloadObjects))
	{
		if((nStartIndex<0)||(nStartIndex>=m_nNumDownloadObjects)) nStartIndex=0;
		int n=nStartIndex;
		BOOL bDone = false; 
		BOOL bFirst = true; 
		while(!bDone)
		{
			while(((n<m_nNumDownloadObjects)&&(bFirst))||((n<nStartIndex)&&(!bFirst)))
			{
				if(m_ppDownloadObj[n])
				{
					if((m_ppDownloadObj[n]->m_pszName)&&(strcmp(m_ppDownloadObj[n]->m_pszName, pszName)==0)) 
					{
	LeaveCriticalSection(&m_critDownloader);
						return n;
					} 
				}
				n++;
			}
			if((nStartIndex>0)&&(bFirst))
			{
				n=0;
			}
			else
			{
				bDone = true;
			}
			bFirst = false; 
		}
	}
	LeaveCriticalSection(&m_critDownloader);
	return CONDUIT_ERROR;
}

int CConduitData::GetEvents(char* pszInfo)
{
	if((g_pconduit)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		int nTemp = -1;

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT \
conduit_EventID, Source, Scene, Graphic_Action_Type, Action_Name, Event_ActionID, ActionTriggerOffset, Action_Value, event_name, layer \
FROM %s ORDER by conduit_EventID, ActionTriggerOffset",
				(g_pconduit->m_settings.m_pszEvents?g_pconduit->m_settings.m_pszEvents:"TriggerInfo")
				);
if(g_pconduit->m_settings.m_ulDebug&CONDUIT_DEBUG_EVENTS)	
	g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "obtaining events with [%s]", szSQL); //  Sleep(250);//(Dispatch message)

		CConduitEvent tempEventObj;

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
	EnterCriticalSection(&(m_eventarray.m_critEvents));
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = CONDUIT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
/*
				CString szHost="";
				CString szClient="";
				CString szDesc="";
				CString szTemp;
				unsigned long ulFlags;   // various flags
			//	unsigned short usType;
				bool bFlagsFound = false;
 * /

				CString szName = "";
				bool bFound = false;

				try
				{
// conduit_EventID, Source, Scene, Graphic_Action_Type, Action_Name, Event_ActionID, ActionTriggerOffset, Action_Value, event_name, layer					
/*
class CConduitEvent  
{
public:
	CConduitEvent();
	virtual ~CConduitEvent();

	double m_dblTriggerTime; 
	bool m_bAnalyzed;
	bool m_bReAnalyzed;
	int  m_nAnalyzedTriggerID;
	int  m_nEventID;
	int  m_nDestType;
	
	int m_nType;
	CString m_szName;
	CString m_szHost;
	CString m_szSource;
	CString m_szScene;
	CString m_szIdentifier;
	CString m_szValue;
	CString m_szParamDependencies;
	CString m_szDestModule;
	CString m_szLayer;
};
* /
					CString szTemp;

					prs->GetFieldValue("conduit_EventID", szTemp);
					tempEventObj.m_nEventID = -1;
					tempEventObj.m_nEventID = atoi(szTemp);
					
					prs->GetFieldValue("Source", szTemp);
					tempEventObj.m_szSource = szTemp;

					prs->GetFieldValue("Scene", szTemp);
					tempEventObj.m_szScene = szTemp;

					prs->GetFieldValue("Graphic_Action_Type", szTemp);
					tempEventObj.m_nType = (unsigned short)atoi(szTemp);

					prs->GetFieldValue("Action_Name", szTemp);
					tempEventObj.m_szIdentifier = szTemp;

					prs->GetFieldValue("Event_ActionID", szTemp);
					tempEventObj.m_nAnalyzedTriggerID = -1;
					if(szTemp.GetLength())	tempEventObj.m_nAnalyzedTriggerID = atol(szTemp);

					prs->GetFieldValue("ActionTriggerOffset", szTemp);
					tempEventObj.m_dblTriggerTime = atof(szTemp);

					prs->GetFieldValue("Action_Value", szTemp);
					tempEventObj.m_szValue = szTemp;

					prs->GetFieldValue("event_name", szTemp);
					tempEventObj.m_szName = szTemp;

					prs->GetFieldValue("layer", szTemp);
					tempEventObj.m_szLayer = szTemp;

				}
				catch( ... )
				{
				}

				if((m_eventarray.m_ppEvents)&&(m_eventarray.m_nEvents))
				{
					nTemp=0;
					while(nTemp<m_eventarray.m_nEvents)
					{
//			g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "c2");  Sleep(250); //(Dispatch message)
						if(m_eventarray.m_ppEvents[nTemp])
						{
							if(tempEventObj.m_nAnalyzedTriggerID == m_eventarray.m_ppEvents[nTemp]->m_nAnalyzedTriggerID)
							{
							
								bFound = true;

								*(m_eventarray.m_ppEvents[nTemp]) = tempEventObj;
								m_eventarray.m_ppEvents[nTemp]->m_ulFlags = CONDUIT_FLAG_FOUND;

							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(tempEventObj.m_nAnalyzedTriggerID>0)) // have to add.
				{
if(g_pconduit->m_settings.m_ulDebug&CONDUIT_DEBUG_EVENTS)	
	g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:event_add", "adding event: %d: %s:%s)", tempEventObj.m_nAnalyzedTriggerID,tempEventObj.m_szName,tempEventObj.m_szIdentifier); //  Sleep(250);//(Dispatch message)

					CConduitEvent* pscno = new CConduitEvent;
					if(pscno)
					{
//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CConduitEvent** ppObj = new CConduitEvent*[m_eventarray.m_nEvents+1];
						if(ppObj)
						{
//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((m_eventarray.m_ppEvents)&&(m_eventarray.m_nEvents>0))
							{
								while(o<m_eventarray.m_nEvents)
								{
									ppObj[o] = m_eventarray.m_ppEvents[o];
									o++;
								}
								delete [] m_eventarray.m_ppEvents;

							}
							ppObj[o] = pscno;
							m_eventarray.m_ppEvents = ppObj;
							m_eventarray.m_nEvents++;

							*(ppObj[o]) = tempEventObj;


							ppObj[o]->m_ulFlags = CONDUIT_FLAG_FOUND;
							
						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//			g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			nIndex = 0;

			while(nIndex<m_eventarray.m_nEvents)
			{
				if((m_eventarray.m_ppEvents)&&(m_eventarray.m_ppEvents[nIndex]))
				{
					if((m_eventarray.m_ppEvents[nIndex]->m_ulFlags)&CONDUIT_FLAG_FOUND)
					{
						(m_eventarray.m_ppEvents[nIndex]->m_ulFlags) &= ~CONDUIT_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(m_eventarray.m_ppEvents[nIndex])
						{

if(g_pconduit->m_settings.m_ulDebug&CONDUIT_DEBUG_EVENTS)	
	g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:event_remove", "removing event: %d: %s:%s)", m_eventarray.m_ppEvents[nIndex]->m_nAnalyzedTriggerID,m_eventarray.m_ppEvents[nIndex]->m_szName,m_eventarray.m_ppEvents[nIndex]->m_szIdentifier); //  Sleep(250);//(Dispatch message)
							

							delete m_eventarray.m_ppEvents[nIndex];
							m_eventarray.m_nEvents--;

							int nTemp=nIndex;
							while(nTemp<m_eventarray.m_nEvents)
							{
								m_eventarray.m_ppEvents[nTemp]=m_eventarray.m_ppEvents[nTemp+1];
								nTemp++;
							}
							m_eventarray.m_ppEvents[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

			// now sort the events
			nIndex = 0;
			nTemp=0;
			CConduitEvent* pSwap;

			while(nIndex<m_eventarray.m_nEvents-1)
			{
				if((m_eventarray.m_ppEvents)&&(m_eventarray.m_ppEvents[nIndex]))
				{
					nTemp = nIndex+1;
					while(nTemp<m_eventarray.m_nEvents-1)
					{
						if((m_eventarray.m_ppEvents[nTemp])&&(m_eventarray.m_ppEvents[nTemp]->m_nEventID<m_eventarray.m_ppEvents[nIndex]->m_nEventID))
						{
							// swap;
							pSwap = m_eventarray.m_ppEvents[nTemp];
							m_eventarray.m_ppEvents[nTemp] = m_eventarray.m_ppEvents[nIndex];
							m_eventarray.m_ppEvents[nIndex] = pSwap;
						}
						nTemp++;
					}
				}
				nIndex++;
			}

			nIndex = 0;
			nTemp=0;

			while(nIndex<m_eventarray.m_nEvents-1)
			{
				if((m_eventarray.m_ppEvents)&&(m_eventarray.m_ppEvents[nIndex]))
				{
					nTemp = nIndex+1;
					while(nTemp<m_eventarray.m_nEvents-1)
					{
						if((m_eventarray.m_ppEvents[nTemp])&&(m_eventarray.m_ppEvents[nTemp]->m_nEventID!=m_eventarray.m_ppEvents[nIndex]->m_nEventID))
						{
							break;// no match, move to next event
						}
						else
						{
							if((m_eventarray.m_ppEvents[nTemp])&&(m_eventarray.m_ppEvents[nTemp]->m_dblTriggerTime<m_eventarray.m_ppEvents[nIndex]->m_dblTriggerTime))
							{
								// swap;
								pSwap = m_eventarray.m_ppEvents[nTemp];
								m_eventarray.m_ppEvents[nTemp] = m_eventarray.m_ppEvents[nIndex];
								m_eventarray.m_ppEvents[nIndex] = pSwap;
							}
						}

						nTemp++;
					}
				}

				nIndex++;
			}

	LeaveCriticalSection(&m_eventarray.m_critEvents);

//			g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
	}
	return CONDUIT_ERROR;
}

int CConduitData::GetDownloaders(char* pszInfo)
{
	if((g_pconduit)&&(m_pdbConn)&&(m_pdb))
	{

		 g_pmsgr = &g_pconduit->m_msgr;
/*		
		
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY server",  //HARDCODE
			((g_pconduit->m_settings.m_pszChannelInfo)&&(strlen(g_pconduit->m_settings.m_pszChannelInfo)))?g_pconduit->m_settings.m_pszChannelInfo:"ChannelInfo");
//		g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "GetConnections");  Sleep(250); //(Dispatch message)
* /

		char szSQL[DB_SQLSTRING_MAXLEN];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, name, description, dll_name, active, type FROM %s ORDER by name",
				(g_pconduit->m_settings.m_pszDownloaders?g_pconduit->m_settings.m_pszDownloaders:"Downloaders")
				);
if(g_pconduit->m_settings.m_ulDebug&CONDUIT_DEBUG_DATAFEEDS)	
	g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "obtaining downloaders with [%s]", szSQL); //  Sleep(250);//(Dispatch message)

		CConduitDownloadObject tempDownloaderObj;

	EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
	EnterCriticalSection(&m_critDownloader);
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = CONDUIT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
/*
				CString szHost="";
				CString szClient="";
				CString szDesc="";
				CString szTemp;
				unsigned long ulFlags;   // various flags
			//	unsigned short usType;
				bool bFlagsFound = false;
* /

				int nTemp = -1;
				CString szName = "";
				bool bFound = false;

				try
				{
					CString szTemp;
					prs->GetFieldValue("ID", szTemp);
					tempDownloaderObj.m_nID = atoi(szTemp);
					
					prs->GetFieldValue("name", szTemp);
					tempDownloaderObj.m_pszName = (char*)malloc(szTemp.GetLength()+1);
					if( tempDownloaderObj.m_pszName ) sprintf(tempDownloaderObj.m_pszName, "%s", szTemp);

					prs->GetFieldValue("description", szTemp);
					tempDownloaderObj.m_pszDesc = (char*)malloc(szTemp.GetLength()+1);
					if( tempDownloaderObj.m_pszDesc ) sprintf(tempDownloaderObj.m_pszDesc, "%s", szTemp);

					prs->GetFieldValue("dll_name", szName);
					tempDownloaderObj.m_pszModule = (char*)malloc(szName.GetLength()+1);
					if( tempDownloaderObj.m_pszModule ) sprintf(tempDownloaderObj.m_pszModule, "%s", szName);

					prs->GetFieldValue("active", szTemp);
					tempDownloaderObj.m_ulFlags = atol(szTemp);

					prs->GetFieldValue("type", szTemp);
					tempDownloaderObj.m_usType = (unsigned short)atol(szTemp);
				}
				catch( ... )
				{
				}

				if((m_ppDownloadObj)&&(m_nNumDownloadObjects))
				{
					nTemp=0;
					while(nTemp<m_nNumDownloadObjects)
					{
//			g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "c2");  Sleep(250); //(Dispatch message)
						if(m_ppDownloadObj[nTemp])
						{
							if(
								  (szName.GetLength()>0)
								&&(szName.CompareNoCase(m_ppDownloadObj[nTemp]->m_pszModule)==0)
								)
							{
							
								bFound = true;
//			g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "c3");  Sleep(250); //(Dispatch message)
//								m_ppDownloadObj[nTemp]->m_bKillAutomationThread = false;
//								m_ppDownloadObj[nTemp]->m_pConduit = g_pconduit;
//								m_ppDownloadObj[nTemp]->m_pEndpoint = pEndObj;

//								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning analysis for  %s (%s)", 
//													m_ppDownloadObj[nTemp]->m_pszName,m_ppDownloadObj[nTemp]->m_pszDesc );
//								g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:destination_change", errorstring);    //(Dispatch message)


								if(m_ppDownloadObj[nTemp]->m_hinstDLL==NULL)
								{
									m_ppDownloadObj[nTemp]->m_hinstDLL = AfxLoadLibrary(m_ppDownloadObj[nTemp]->m_pszModule);

									if(m_ppDownloadObj[nTemp]->m_hinstDLL==NULL)
									{
										int err = GetLastError();
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d loading module %s.  Cannot begin %s (%s)", 
															err, m_ppDownloadObj[nTemp]->m_pszModule, m_ppDownloadObj[nTemp]->m_pszName,m_ppDownloadObj[nTemp]->m_pszDesc );
										g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:module_change", errorstring);    //(Dispatch message)
									}
									else
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning analysis for  %s (%s)", 
															m_ppDownloadObj[nTemp]->m_pszName,m_ppDownloadObj[nTemp]->m_pszDesc );
										g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:module_change", errorstring);    //(Dispatch message)
									}
								}

								if((m_ppDownloadObj[nTemp]->m_lpfnDllCtrl==NULL)&&(m_ppDownloadObj[nTemp]->m_hinstDLL))
								{
									m_ppDownloadObj[nTemp]->m_lpfnDllCtrl = (LPFNDLLCTRL)GetProcAddress((m_ppDownloadObj[nTemp]->m_hinstDLL), DLLFUNC_NAME);
									if(m_ppDownloadObj[nTemp]->m_lpfnDllCtrl==NULL)
									{
										int err = GetLastError();
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d getting process address in module %s.  Cannot begin %s (%s)", 
															err, m_ppDownloadObj[nTemp]->m_pszModule, m_ppDownloadObj[nTemp]->m_pszName,m_ppDownloadObj[nTemp]->m_pszDesc );
										g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:module_change", errorstring);    //(Dispatch message)
									}
								}

								if((m_ppDownloadObj[nTemp]->m_pDLLdata==NULL)&&(m_ppDownloadObj[nTemp]->m_lpfnDllCtrl))
								{
									int nCode = m_ppDownloadObj[nTemp]->m_lpfnDllCtrl((void**)(&m_ppDownloadObj[nTemp]->m_pDLLdata), DLLCMD_GETDATAOBJ);
									if(nCode<CLIENT_SUCCESS)
									{
										m_ppDownloadObj[nTemp]->m_pDLLdata = NULL;
									}
									else
									{
										nCode = m_ppDownloadObj[nTemp]->m_lpfnDllCtrl((void**)(&m_eventarray), DLLCMD_SETEVENTS);
										if(nCode<CLIENT_SUCCESS)
										{
											/// do something, report?
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error setting event array pointer in module %s: %s (%s)", 
																m_ppDownloadObj[nTemp]->m_pszModule, m_ppDownloadObj[nTemp]->m_pszName,m_ppDownloadObj[nTemp]->m_pszDesc );
											g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:module_change", errorstring);    //(Dispatch message)
										}
	//									else // must not be else, we need the messager
										{
											nCode = m_ppDownloadObj[nTemp]->m_lpfnDllCtrl((void**)(&g_pmsgr), DLLCMD_SETDISPATCHER);
											if(nCode<CLIENT_SUCCESS)
											{
												/// do something, report?
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error setting message dispatcher pointer in module %s: %s (%s)", 
																	m_ppDownloadObj[nTemp]->m_pszModule, m_ppDownloadObj[nTemp]->m_pszName,m_ppDownloadObj[nTemp]->m_pszDesc );
												g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:module_change", errorstring);    //(Dispatch message)
											}
										}
									}

								}


/*
								if(m_ppDownloadObj[nTemp]->m_bAutomationThreadStarted == false)
								{

									//**** if connect set following status
									if(_beginthread(ConduitAutomationThread, 0, (void*)m_ppDownloadObj[nTemp])==-1)
									{
										//error.

										//**MSG
									}
									//**** should check return value....
								}
* /


/*
								Sleep(30);
								if(m_ppDownloadObj[nTemp]->m_bNearAnalysisThreadStarted == false)
								{
									if(_beginthread(ConduitNearAnalysisThread, 0, (void*)m_ppDownloadObj[nTemp])==-1)
									{
										//error.

										//**MSG
									}
									//**** should check return value....
								}
* /
							//	Sleep(30);
/*
								if(m_ppDownloadObj[nTemp]->m_bTriggerThreadStarted == false)
								{
									if(_beginthread(ConduitTriggerThread, 0, (void*)m_ppDownloadObj[nTemp])==-1)
									{
										//error.

										//**MSG
									}
									//**** should check return value....
								}
								Sleep(30);

* /

								m_ppDownloadObj[nTemp]->m_ulFlags = CONDUIT_FLAG_FOUND;

							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szName.GetLength()>0)) // have to add.
				{
if(g_pconduit->m_settings.m_ulDebug&CONDUIT_DEBUG_DATAFEEDS)	
	g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "adding downloader: %s: %s (%s)", tempDownloaderObj.m_pszModule,tempDownloaderObj.m_pszName,tempDownloaderObj.m_pszDesc); //  Sleep(250);//(Dispatch message)
//			g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "adding %s", szHost);  Sleep(250); //(Dispatch message)
					CConduitDownloadObject* pscno = new CConduitDownloadObject;
					if(pscno)
					{
//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CConduitDownloadObject** ppObj = new CConduitDownloadObject*[m_nNumDownloadObjects+1];
						if(ppObj)
						{
//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((m_ppDownloadObj)&&(m_nNumDownloadObjects>0))
							{
								while(o<m_nNumDownloadObjects)
								{
									ppObj[o] = m_ppDownloadObj[o];
									o++;
								}
								delete [] m_ppDownloadObj;

							}
							ppObj[o] = pscno;
							m_ppDownloadObj = ppObj;
							m_nNumDownloadObjects++;

							ppObj[o]->m_nID = tempDownloaderObj.m_nID;

							ppObj[o]->m_pszModule = tempDownloaderObj.m_pszModule;
							tempDownloaderObj.m_pszModule = NULL;
						
							ppObj[o]->m_pszName = tempDownloaderObj.m_pszName;
							tempDownloaderObj.m_pszName = NULL;
							
							ppObj[o]->m_pszDesc = tempDownloaderObj.m_pszDesc;
							tempDownloaderObj.m_pszDesc = NULL;

							if(ppObj[o]->m_hinstDLL==NULL)
							{
								ppObj[o]->m_hinstDLL = AfxLoadLibrary(ppObj[o]->m_pszModule);

								if(ppObj[o]->m_hinstDLL==NULL)
								{
									int err = GetLastError();
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d loading module %s.  Cannot begin %s (%s)", 
														err, ppObj[o]->m_pszModule, ppObj[o]->m_pszName,ppObj[o]->m_pszDesc );
									g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:module_add", errorstring);    //(Dispatch message)
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning analysis for  %s (%s)", 
														ppObj[o]->m_pszName,ppObj[o]->m_pszDesc );
									g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:module_add", errorstring);    //(Dispatch message)
								}
							}

							if((ppObj[o]->m_lpfnDllCtrl==NULL)&&(ppObj[o]->m_hinstDLL))
							{
								ppObj[o]->m_lpfnDllCtrl = (LPFNDLLCTRL)GetProcAddress((ppObj[o]->m_hinstDLL), DLLFUNC_NAME);
								if(ppObj[o]->m_lpfnDllCtrl==NULL)
								{
									int err = GetLastError();
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d getting process address in module %s.  Cannot begin %s (%s)", 
														err, ppObj[o]->m_pszModule, ppObj[o]->m_pszName,ppObj[o]->m_pszDesc );
									g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:module_add", errorstring);    //(Dispatch message)
								}
							}

							if((ppObj[o]->m_pDLLdata==NULL)&&(ppObj[o]->m_lpfnDllCtrl))
							{
								int nCode = ppObj[o]->m_lpfnDllCtrl((void**)(&ppObj[o]->m_pDLLdata), DLLCMD_GETDATAOBJ);
								if(nCode<CLIENT_SUCCESS)
								{
									ppObj[o]->m_pDLLdata = NULL;
								}
								else
								{

									nCode = ppObj[o]->m_lpfnDllCtrl((void**)(&m_eventarray), DLLCMD_SETEVENTS);
									if(nCode<CLIENT_SUCCESS)
									{
										/// do something, report?
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error setting event array pointer in module %s: %s (%s)", 
															ppObj[o]->m_pszModule, ppObj[o]->m_pszName,ppObj[o]->m_pszDesc );
										g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:module_add", errorstring);    //(Dispatch message)
									}
	//								else // must not be else, we need the messager
									{
										nCode = ppObj[o]->m_lpfnDllCtrl((void**)(&g_pmsgr), DLLCMD_SETDISPATCHER);
										if(nCode<CLIENT_SUCCESS)
										{
											/// do something, report?
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error setting message dispatcher pointer in module %s: %s (%s)", 
																ppObj[o]->m_pszModule, ppObj[o]->m_pszName,ppObj[o]->m_pszDesc );
											g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:module_add", errorstring);    //(Dispatch message)
										}
									}


								}
							}
/*
							if(ppObj[o]->m_lpfnDllCtrl)
							{
								char* pch = (char*)malloc(strlen("Server_Refresh_Setting")+1);
								if(pch)
								{
									strcpy(pch, "Server_Refresh_Setting");
									ppObj[o]->m_lpfnDllCtrl((void**)(&pch), DLLCMD_CALLFUNC);
									free(pch);
								}
							}
* /
							ppObj[o]->m_usType =	tempDownloaderObj.m_usType;

							
/*
							if(ppObj[o]->m_bAutomationThreadStarted == false)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning analysis for  %s (%s)", 
													ppObj[o]->m_pszName,ppObj[o]->m_pszDesc );
								g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:destination_change", errorstring);    //(Dispatch message)

								//**** if connect set following status
								if(_beginthread(ConduitAutomationThread, 0, (void*)ppObj[o])==-1)
								{
									//error.

									//**MSG
								}
								//**** should check return value....
							}

/*
							Sleep(30);
							if(ppObj[o]->m_bNearAnalysisThreadStarted == false)
							{
								if(_beginthread(ConduitNearAnalysisThread, 0, (void*)ppObj[o])==-1)
								{
									//error.

									//**MSG
								}
								//**** should check return value....
							}

							Sleep(30);
							if(ppObj[o]->m_bTriggerThreadStarted == false)
							{
								if(_beginthread(ConduitTriggerThread, 0, (void*)ppObj[o])==-1)
								{
									//error.

									//**MSG
								}
								//**** should check return value....
							}
* /
		//					Sleep(30);

							ppObj[o]->m_ulFlags = CONDUIT_FLAG_FOUND;
							
						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//			g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)
	LeaveCriticalSection(&m_critSQL);

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<m_nNumDownloadObjects)
			{
				if((m_ppDownloadObj)&&(m_ppDownloadObj[nIndex]))
				{
					if((m_ppDownloadObj[nIndex]->m_ulFlags)&CONDUIT_FLAG_FOUND)
					{
						(m_ppDownloadObj[nIndex]->m_ulFlags) &= ~CONDUIT_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(m_ppDownloadObj[nIndex])
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending data aggregation for %s (%s)", 
														m_ppDownloadObj[nIndex]->m_pszName,m_ppDownloadObj[nIndex]->m_pszDesc );  
							g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:module_remove", errorstring);    //(Dispatch message)
//									m_ppDownloadObj[nTemp]->m_pDlg->OnDisconnect();

							//**** disconnect
//							m_ppDownloadObj[nIndex]->m_bKillAutomationThread = true;

							int ixi = 0;
							while (
											(m_ppDownloadObj[nIndex]->m_pDLLdata!=NULL)
										&&(m_ppDownloadObj[nIndex]->m_pDLLdata->nNumThreads>0)
										&&(m_ppDownloadObj[nIndex]->m_pDLLdata->thread!=NULL)
										&&(ixi<500) // half a second max, this stalls the get thread.
										)
							{
								int nth=0;

								int nNumThreadsActive=0;
								EnterCriticalSection(&m_ppDownloadObj[nIndex]->m_pDLLdata->m_crit);
								while(nth<m_ppDownloadObj[nIndex]->m_pDLLdata->nNumThreads)
								{
									if(
										  (m_ppDownloadObj[nIndex]->m_pDLLdata->thread!=NULL)
										&&(m_ppDownloadObj[nIndex]->m_pDLLdata->thread[nth]!=NULL)
										) 
										m_ppDownloadObj[nIndex]->m_pDLLdata->thread[nth]->bKillThread=true;
									nth++;
								}

								nth=0;
								while(nth<m_ppDownloadObj[nIndex]->m_pDLLdata->nNumThreads)
								{
									if(
										  (m_ppDownloadObj[nIndex]->m_pDLLdata->thread!=NULL)
										&&(m_ppDownloadObj[nIndex]->m_pDLLdata->thread[nth]!=NULL)
										)
									{
										if(m_ppDownloadObj[nIndex]->m_pDLLdata->thread[nth]->bThreadStarted)
										{ nNumThreadsActive++; break; } 
									}
									else
									{ break;
									}
									nth++;
								}
								LeaveCriticalSection(&m_ppDownloadObj[nIndex]->m_pDLLdata->m_crit);

								if(nNumThreadsActive<=0) ixi=500;
								ixi++;
								Sleep(1);
							}
							//g_adc.DisconnectServer(m_ppDownloadObj[nIndex]->m_pszServerName);
							//**** should check return value....

							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ended data aggregation for %s (%s)", 
													m_ppDownloadObj[nIndex]->m_pszName,m_ppDownloadObj[nIndex]->m_pszDesc );  
							g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "Conduit:module_remove", errorstring);    //(Dispatch message)
							

							try
							{
								AfxFreeLibrary(m_ppDownloadObj[nIndex]->m_hinstDLL);  // crashes!  not sure why
							}
							catch(...){}

							delete m_ppDownloadObj[nIndex];
							m_nNumDownloadObjects--;

							int nTemp=nIndex;
							while(nTemp<m_nNumDownloadObjects)
							{
								m_ppDownloadObj[nTemp]=m_ppDownloadObj[nTemp+1];
								nTemp++;
							}
							m_ppDownloadObj[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
	LeaveCriticalSection(&m_critDownloader);
			}

//			g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
		else
		{
	LeaveCriticalSection(&m_critSQL);
		}

	}
//			g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return CONDUIT_ERROR;
}
*/

#ifdef THISISNOTDEFINED

void ConduitAutomationThread(void* pvArgs)
{
	CConduitDownloadObject* pChannelObj = (CConduitDownloadObject*) pvArgs;
	if(pChannelObj==NULL) return;

	pChannelObj->m_bAutomationThreadStarted=true;
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Beginning automation thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);

	g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "ConduitAutomationThread", errorstring);    //(Dispatch message)

	strcpy(errorstring, "");
	CDBUtil db;


	CDBconn* pdbConn = db.CreateNewConnection(g_pconduit->m_settings.m_pszDSN, g_pconduit->m_settings.m_pszUser, g_pconduit->m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			g_pconduit->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Conduit:automation_database_connect", errorstring);  //(Dispatch message)
			pdbConn = g_pconduit->m_data.m_pdbConn;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", g_pconduit->m_settings.m_pszDSN, g_pconduit->m_settings.m_pszUser, g_pconduit->m_settings.m_pszPW); 
		g_pconduit->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Conduit:automation_database_init", errorstring);  //(Dispatch message)
		pdbConn = g_pconduit->m_data.m_pdbConn;

		//**MSG
	}



	CConduitDownloadObject tempDownloaderObj;
	bool bChangesPending = false;
	_timeb timebPending;

	while((!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
	{
		// check automation list for changes (mod)
		bool bChanges = false;

//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "%d %d %d %d %d.%03d", g_pconduit->m_data.m_nIndexAutomationEndpoint, g_pconduit->m_settings.m_nNumEndpointsInstalled, g_pconduit->m_settings.m_ppEndpointObject, g_pconduit->m_settings.m_ppEndpointObject[g_pconduit->m_data.m_nIndexAutomationEndpoint], g_pconduit->m_data.m_timebTick.time, g_pconduit->m_data.m_timebTick.millitm);   Sleep(50);//(Dispatch message)

		if(
			  (!g_pconduit->m_data.m_bProcessSuspended)
			&&(g_pconduit->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!g_pconduit->m_data.m_key.m_bExpires)
				||((g_pconduit->m_data.m_key.m_bExpires)&&(!g_pconduit->m_data.m_key.m_bExpired))
				||((g_pconduit->m_data.m_key.m_bExpires)&&(g_pconduit->m_data.m_key.m_bExpireForgiveness)&&(g_pconduit->m_data.m_key.m_ulExpiryDate+g_pconduit->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!g_pconduit->m_data.m_key.m_bMachineSpecific)
				||((g_pconduit->m_data.m_key.m_bMachineSpecific)&&(g_pconduit->m_data.m_key.m_bValidMAC))
				)

			)
		{
//			CConduitEndpointObject* pAutoObj = g_pconduit->m_settings.m_ppEndpointObject[g_pconduit->m_data.m_nIndexAutomationEndpoint];

	_ftime(&pChannelObj->m_timebAutomationTick); // the last time check inside the thread
			// check the channel info view for 
			tempDownloaderObj.m_nListChanged = -1;

			if((g_pconduit->m_settings.m_pszChannelInfo)&&(strlen(g_pconduit->m_settings.m_pszChannelInfo)))
			{

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT server_time, server_status, server_basis, server_changed, server_last_update, \
list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update FROM %s WHERE server = '%s' AND listid = %d",
						(g_pconduit->m_settings.m_pszChannelInfo?g_pconduit->m_settings.m_pszChannelInfo:"ChannelInfo"),

						pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum
						);


				CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
				if(prs != NULL)
				{
//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "� not null");  Sleep(50);//(Dispatch message)
					if((!prs->IsEOF())&&(!g_bKillThread)&&(!pChannelObj->m_bKillAutomationThread))
					{
//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "� NEOF");  Sleep(50);//(Dispatch message)
						try
						{
							CString szTemp;
							prs->GetFieldValue("server_time", szTemp);
							tempDownloaderObj.m_dblServertime = atof(szTemp);

							prs->GetFieldValue("server_status", szTemp);
							tempDownloaderObj.m_nServerStatus = atoi(szTemp);

							prs->GetFieldValue("server_basis", szTemp);
							tempDownloaderObj.m_nServerBasis = atoi(szTemp);

							prs->GetFieldValue("server_changed", szTemp);
							tempDownloaderObj.m_nServerChanged = atoi(szTemp);

							prs->GetFieldValue("server_last_update", szTemp);
							tempDownloaderObj.m_dblServerLastUpdate = atof(szTemp);

							prs->GetFieldValue("list_state", szTemp);
							tempDownloaderObj.m_nListState = atoi(szTemp);

							prs->GetFieldValue("list_changed", szTemp);
							tempDownloaderObj.m_nListChanged = atoi(szTemp);

							prs->GetFieldValue("list_display", szTemp);
							tempDownloaderObj.m_nListDisplay = atoi(szTemp);

							prs->GetFieldValue("list_syschange", szTemp);
							tempDownloaderObj.m_nListSysChange = atoi(szTemp);

							prs->GetFieldValue("list_count", szTemp);
							tempDownloaderObj.m_nListCount = atoi(szTemp);

							prs->GetFieldValue("list_lookahead", szTemp);
							tempDownloaderObj.m_nListLookahead = atoi(szTemp);

							prs->GetFieldValue("list_last_update", szTemp);
							tempDownloaderObj.m_dblListLastUpdate = atof(szTemp);

						}
						catch(...)
						{
						}
					}
					delete prs;
				}

			}
	
			_ftime( &pChannelObj->m_timebAutomationTick );  // we're still alive.

			if(( tempDownloaderObj.m_nListChanged > 0 )&&(tempDownloaderObj.m_nListDisplay > 0 ))
			{
				if(
					  (tempDownloaderObj.m_nListChanged != pChannelObj->m_nListChanged)
					||(tempDownloaderObj.m_nListDisplay != pChannelObj->m_nListDisplay)
					||(tempDownloaderObj.m_nServerChanged != pChannelObj->m_nServerChanged)
					||(tempDownloaderObj.m_nListState != pChannelObj->m_nListState)
					)
				{
					pChannelObj->m_dblServertime  = tempDownloaderObj.m_dblServertime;
					pChannelObj->m_dblServerLastUpdate  = tempDownloaderObj.m_dblServerLastUpdate;
					pChannelObj->m_nServerStatus  = tempDownloaderObj.m_nServerStatus;
					pChannelObj->m_nServerBasis  = tempDownloaderObj.m_nServerBasis;
					pChannelObj->m_nServerChanged  = tempDownloaderObj.m_nServerChanged;

					pChannelObj->m_dblListLastUpdate  = tempDownloaderObj.m_dblListLastUpdate;
					pChannelObj->m_nListState  = tempDownloaderObj.m_nListState;
					pChannelObj->m_nListChanged  = tempDownloaderObj.m_nListChanged;
					pChannelObj->m_nListDisplay  = tempDownloaderObj.m_nListDisplay;
					pChannelObj->m_nListSysChange  = tempDownloaderObj.m_nListSysChange;
					pChannelObj->m_nListCount  = tempDownloaderObj.m_nListCount;
					pChannelObj->m_nListLookahead  = tempDownloaderObj.m_nListLookahead;
					
					bChanges = true;

					bChangesPending = true;
					_ftime(&timebPending);
					timebPending.time += g_pconduit->m_settings.m_nAnalyzeParameterDwellMS/1000;
					timebPending.millitm += g_pconduit->m_settings.m_nAnalyzeParameterDwellMS%1000;
					if(timebPending.millitm>999)
					{
						timebPending.time++;
						timebPending.millitm -= 1000;
					}
				}

			}

			// dwell changes - another change will reset the time, suppressing the previous change
			if(
				  (bChangesPending)
				&&(
				    (g_pconduit->m_data.m_timebTick.time>timebPending.time)
					||(
					    (g_pconduit->m_data.m_timebTick.time==timebPending.time)
						&&(g_pconduit->m_data.m_timebTick.millitm>timebPending.millitm)
						)
					)
				)
			{
				bChangesPending = false;
/*
				if(pChannelObj->m_nAnalysisCounter >= INT_MAX) pChannelObj->m_nAnalysisCounter = 0;
				else		pChannelObj->m_nAnalysisCounter++;
*/
//following moved to immediate from delayed.... 
				if(pChannelObj->m_nAnalysisCounter >= INT_MAX) pChannelObj->m_nAnalysisCounter = 0;
				else	pChannelObj->m_nAnalysisCounter++;

//				pChannelObj->m_bNearEventsChanged = true;
				// do some delayed stuff here.
			}


			if(bChanges)
			{
//				pChannelObj->m_bTriggerEventsChanged = true;
//				pChannelObj->m_bFarEventsChanged = true;


//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "Automation changes exist %d", pAutoObj->m_nModLiveEvents);  // Sleep(50);//(Dispatch message)
				// g_pconduit->m_data.m_nEventLastMax = 0; // start at the beginning....
// actually dont reset this.
				bChanges = false;
				bool bSQLError = false;
				// if changes
				// {
				//   Close the m_prsEvents recordset and delete it, set it back to NULL.

//				g_pconduit->m_data.ReleaseRecordSet();

				_ftime( &pChannelObj->m_timebAutomationTick); 

					//update the servertime.
//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "Automation svr atof? %f", timeval);  Sleep(50);//(Dispatch message)
						
				if(g_pconduit->m_settings.m_bUseLocalClock)
				{
					if(g_pconduit->m_settings.m_bUseUTC)
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
								((double)(pChannelObj->m_timebAutomationTick.time))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + g_pconduit->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}
					else
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
								((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
							+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + g_pconduit->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}
				}
				else
				{
					if(pChannelObj->m_dblServerLastUpdate>0.0)
					{
						/*
						pChannelObj->m_dblAutomationTimeDiffMS = 
							(((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
							+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm) 
							- pChannelObj->m_dblServerLastUpdate;
						*/
						pChannelObj->m_dblAutomationTimeDiffMS = 
							((double)pChannelObj->m_timebAutomationTick.time)*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm
							- pChannelObj->m_dblServerLastUpdate;
					// DEMO only, need to handle midnight.
//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "Automation svr time in unix? %f", timeval);  Sleep(50);//(Dispatch message)
//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "Automation svr time diffMS? %d", nDiff);  Sleep(50);//(Dispatch message)
						pChannelObj->m_dblAutomationTimeEstimate = (pChannelObj->m_dblServertime + pChannelObj->m_dblAutomationTimeDiffMS 
							+ (double)g_pconduit->m_settings.m_nTriggerAdvanceMS)/1000.0;
//										g_pconduit->m_data.m_dblAutomationTimeEstimate = (double)timebTempTime.time;
//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "Automation estimate time? %f", g_pconduit->m_data.m_dblAutomationTimeEstimate);  Sleep(50);//(Dispatch message)
					}
				}
				// else automation type unknown, so nothing
			} // automation changes exist

/////////////////////////////////////////////////////////
			else  // no automation changes
			{

				_ftime( &pChannelObj->m_timebAutomationTick); 
					// just update the time based on estimate
				if(g_pconduit->m_settings.m_bUseLocalClock)
				{
					if(g_pconduit->m_settings.m_bUseUTC)
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
								((double)(pChannelObj->m_timebAutomationTick.time))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + g_pconduit->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}
					else
					{
						pChannelObj->m_dblAutomationTimeEstimate = 
								((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
							+ (pChannelObj->m_timebAutomationTick.dstflag?3600:0)))
							+ ((double)(pChannelObj->m_timebAutomationTick.millitm + g_pconduit->m_settings.m_nTriggerAdvanceMS))/1000.0;
					}
				}
				else
				{
					if(pChannelObj->m_dblServerLastUpdate>0.0)
					{
						/*
						pChannelObj->m_dblAutomationTimeDiffMS = (((double)(pChannelObj->m_timebAutomationTick.time - (pChannelObj->m_timebAutomationTick.timezone*60) 
							+(pChannelObj->m_timebAutomationTick.dstflag?3600:0)))*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm) 
							- pChannelObj->m_dblServerLastUpdate;
							*/

						pChannelObj->m_dblAutomationTimeDiffMS = 
							((double)pChannelObj->m_timebAutomationTick.time)*1000.0 + (double)pChannelObj->m_timebAutomationTick.millitm
							- pChannelObj->m_dblServerLastUpdate;

						pChannelObj->m_dblAutomationTimeEstimate = (pChannelObj->m_dblServertime + pChannelObj->m_dblAutomationTimeDiffMS 
							+ (double)g_pconduit->m_settings.m_nTriggerAdvanceMS)/1000.0;
					}
				}

/*
				g_pconduit->m_data.m_dblAutomationTimeEstimate = g_pconduit->m_data.m_dblHarrisTime + ((double)(g_pconduit->m_data.m_nHarrisTimeDiffMS)/1000.0) + (double)g_pconduit->m_settings.m_nTriggerAdvanceMS/1000.0
					+ ((double)timebTempTime.time - (double)g_pconduit->m_data.m_timebHarrisCheckTimeLocal.time)
					+ ((double)timebTempTime.millitm/1000.0 - (double)g_pconduit->m_data.m_timebHarrisCheckTimeLocal.millitm/1000.0); // the difference beween now and last check
//										g_pconduit->m_data.m_dblAutomationTimeEstimate = (double)timebTempTime.time;
//g_pconduit->m_msgr.DM(MSG_ICONHAND, NULL, "Conduit:debug", "Automation estimate time now %f", g_pconduit->m_data.m_dblAutomationTimeEstimate);  Sleep(50);//(Dispatch message)
*/

			}


		} // if license etc


		Sleep(1); // dont peg processor
	} // while((!g_bKillThread)&&(!pAobj->m_bKillAutomationThread))

	Sleep(30);
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Ending automation thread for %s:%d", pChannelObj->m_pszServerName, pChannelObj->m_nHarrisListNum);
	g_pconduit->m_msgr.DM(MSG_ICONINFO, NULL, "ConduitAutomationThread", errorstring);    //(Dispatch message)
	pChannelObj->m_bAutomationThreadStarted=false;

  Sleep(50);//(Dispatch message)
	_endthread();
	Sleep(150);

}


#endif



