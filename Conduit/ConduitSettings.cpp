// ConduitSettings.cpp: implementation of the CConduitSettings.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "ConduitDefines.h"
#include "ConduitMain.h" 
#include "ConduitSettings.h"
#include "../../Common/FILE/DirUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CDirUtil	g_dir;				// watch folder utilities.
extern CConduitMain* g_pconduit;
extern CConduitApp theApp;


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CConduitSettings::CConduitSettings()
{
	m_pdb = NULL;
	m_pdbConn = NULL;

	m_pszName = NULL;
	m_pszType = NULL;  // familiar name of the type.
	m_pszProject = NULL;  // familiar name of the project name.
	m_ulMainMode = CONDUIT_MODE_DEFAULT;

	// ports
	m_usCommandPort	= CONDUIT_PORT_CMD;
	m_usStatusPort	= CONDUIT_PORT_STATUS;

	m_nThreadDwellMS = 1000;

	m_nNumberOfStatusLines = 1000;


	m_nAutoPurgeMessageDays = 30; // default
	m_nAutoPurgeAsRunDays = 30; // default
	m_nAutoPurgeInterval = 300;  // 5 minutes should be enough.

	// messaging for Conduit
	m_bUseLog = true;			// write a log file
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host
	m_bReportSuccessfulOperation = false;
	m_bLogTransfers=false;
	m_pszFileSpec = NULL;
	m_pszMailSpec = NULL;
	m_pszProcessedFileSpec = NULL;
	m_pszProcessedMailSpec = NULL;
	m_bUseXMLClientLog = false;
	m_bMillisecondMessaging=true;			// use millisecond resolution for messages and asrun

	// DSN params
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;
	m_pszDatabase = NULL;
	m_pszDBServer = NULL;

//	m_pszSettings = NULL;  // the Settings table name
//	m_pszExchange = NULL;  // the Exchange table name
//	m_pszMessages = NULL;  // the Messages table name
//	m_pszQueue = NULL; // the Queue table name
//	m_pszDownloaders = NULL;
//	m_pszEvents = NULL;  // the Events view name 
//	m_pszAsRun = NULL;  // the As-run table name
//	m_pszCommandQueue = NULL;			// the Command Queue table name


	// file handling

// other settings
	m_ulModsIntervalMS = 3000;

	m_pszLicense=NULL;  // the License Key
	m_pszIconPath=NULL;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

	m_bDebugLists= false;  // if true, dumps the temp and events databases
	m_nDebugListTopCount=-1;  //if positive, just prints out the top n recored in the events tables.
	m_bDebugSQL=false;  // if true, prints out the SQL calls
	m_bDebugInsertSQL=false;  // if true, prints out the event insert SQL calls
	m_ulDebug = 0;
	m_pszDebugOrder = NULL;

	m_nTriggerAdvanceMS          = 0; // number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)
	m_nAnalyzeRulesDwellMS = 1000; // number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once.
	m_nAnalyzeParameterDwellMS = 2500; // number of milliseconds analysis is delayed after a change (prevents hammering analysis when a bunch of list changes happen all at once.
	m_nAnalyzeTimingDwellMS = 30000;
	m_bUseLocalClock = false;
	m_bUseUTC = false;

	m_pszPeriodicQueries = NULL;
	m_bPeriodicQueries = false;
	m_bPeriodicShell=false;
	m_pszPeriodicShellCmds = NULL;  // a pipe-delimited string indicating intervals and shell commands (ShellExecute)
	m_bPeriodicHttp = false;
	m_pszPeriodicHttp = NULL;  // a pipe-delimited string indicating intervals and HTTP gets to run...  results stored in m_pszPeriodicHttpResult
	m_pszPeriodicHttpResult = NULL;

}

CConduitSettings::~CConduitSettings()
{
	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_pszType) free(m_pszType); // must use malloc to allocate
	if(m_pszProject) free(m_pszProject); // must use malloc to allocate
	if(m_pszDSN) free(m_pszDSN); // must use malloc to allocate
	if(m_pszUser) free(m_pszUser); // must use malloc to allocate
	if(m_pszPW) free(m_pszPW); // must use malloc to allocate
	if(m_pszDatabase) free(m_pszDatabase); // must use malloc to allocate
//	if(m_pszEvents) free(m_pszEvents); // must use malloc to allocate

//	if(m_pszSettings) free(m_pszSettings); // must use malloc to allocate
//	if(m_pszExchange) free(m_pszExchange); // must use malloc to allocate
//	if(m_pszMessages) free(m_pszMessages); // must use malloc to allocate
//	if(m_pszQueue) free(m_pszQueue); // must use malloc to allocate
//	if(m_pszDownloaders) free(m_pszDownloaders); // must use malloc to allocate
//	if(m_pszAsRun) free(m_pszAsRun);
//	if(m_pszCommandQueue) free(m_pszCommandQueue);
	

	if(m_pszLicense) free(m_pszLicense); // must use malloc to allocate
	if(m_pszIconPath) free(m_pszIconPath); // must use malloc to allocate	
	if(m_pszFileSpec) free(m_pszFileSpec); // must use malloc to allocate	
	if(m_pszMailSpec) free(m_pszMailSpec); // must use malloc to allocate	
	if(m_pszProcessedFileSpec) free(m_pszProcessedFileSpec); // must use malloc to allocate	
	if(m_pszProcessedMailSpec) free(m_pszProcessedMailSpec); // must use malloc to allocate	

	if(m_pszDebugOrder) free(m_pszDebugOrder); // must use malloc to allocate

}

int CConduitSettings::Settings(bool bRead)
{
	char pszFilename[MAX_PATH];
	strcpy(pszFilename, "");

	char* pszParams = NULL;


//		AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, CONDUIT_SETTINGS_FILE_DEFAULT);  // cortex settings file
	CFileUtil file;
	// get settings.
	file.GetSettings(pszFilename, false); 

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
		// load up the values on the settings object
			m_pszName = file.GetIniString("Main", "Name", "Conduit", m_pszName);
			m_pszType = file.GetIniString("Main", "Type", "Conduit", m_pszType);
			m_pszProject = file.GetIniString("Main", "Project", "VDS", m_pszProject);
			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);

			m_nThreadDwellMS = file.GetIniInt("Main", "ThreadDwellMS", 1000);

			m_nAutoPurgeMessageDays = file.GetIniInt("AutoPurge", "AutoPurgeMessageDays", 30);
			m_nAutoPurgeAsRunDays = file.GetIniInt("AutoPurge", "AutoPurgeAsRunDays", 30);
			m_nAutoPurgeInterval  = file.GetIniInt("AutoPurge", "AutoPurgeInterval", 300);  // 5 minutes should be enough.


			m_bDebugLists = file.GetIniInt("Database", "DebugLists", 0)?true:false;  // if true, dumps the temp and events databases
			m_nDebugListTopCount = file.GetIniInt("Database", "DebugListTopCount", -1);  //if positive, just prints out the top n recored in the events tables.
			m_bDebugSQL = file.GetIniInt("Database", "DebugSQL", 0)?true:false;  // if true, prints out the SQL calls
			m_bDebugInsertSQL = file.GetIniInt("Database", "DebugInsertSQL", 0)?true:false;  // if true, prints out the event insert SQL calls
			m_ulDebug = file.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.
			m_pszDebugOrder = file.GetIniString("Database", "DebugOrder", "(case when parent_position < 0 then ((event_position)/1000.0) else (parent_calc_start + (event_position-parent_position)/1000.0) end)", m_pszDebugOrder); // the order by clause for debug

			m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", CONDUIT_PORT_CMD);
			m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", CONDUIT_PORT_STATUS);

			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);

			g_pconduit->m_data.m_key.m_bValid=true;

/*
			// recompile license key params
			if(g_pconduit->m_data.m_key.m_pszLicenseString) free(g_pconduit->m_data.m_key.m_pszLicenseString);
			g_pconduit->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(m_pszLicense)+1);
			if(g_pconduit->m_data.m_key.m_pszLicenseString)
			sprintf(g_pconduit->m_data.m_key.m_pszLicenseString, "%s", m_pszLicense);

			g_pconduit->m_data.m_key.InterpretKey();

			char errorstring[MAX_MESSAGE_LENGTH];
			if(g_pconduit->m_data.m_key.m_bValid)
			{
				unsigned long i=0;
				while(i<g_pconduit->m_data.m_key.m_ulNumParams)
				{
					if((g_pconduit->m_data.m_key.m_ppszParams)
						&&(g_pconduit->m_data.m_key.m_ppszValues)
						&&(g_pconduit->m_data.m_key.m_ppszParams[i])
						&&(g_pconduit->m_data.m_key.m_ppszValues[i]))
					{
						if(stricmp(g_pconduit->m_data.m_key.m_ppszParams[i], "max")==0)
						{
							//g_pconduit->m_data.m_nMaxLicensedChannels = atoi(g_pconduit->m_data.m_key.m_ppszValues[i]);
						}
					}
					i++;
				}
			
				if(
						(
							(!g_pconduit->m_data.m_key.m_bExpires)
						||((g_pconduit->m_data.m_key.m_bExpires)&&(!g_pconduit->m_data.m_key.m_bExpired))
						||((g_pconduit->m_data.m_key.m_bExpires)&&(g_pconduit->m_data.m_key.m_bExpireForgiveness)&&(g_pconduit->m_data.m_key.m_ulExpiryDate+g_pconduit->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!g_pconduit->m_data.m_key.m_bMachineSpecific)
						||((g_pconduit->m_data.m_key.m_bMachineSpecific)&&(g_pconduit->m_data.m_key.m_bValidMAC))
						)
					)
				{
					// this overrides stuff so let's not let it do that.

										// let's just not override this in a green condition.  
										// comment it out.
//					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//					g_pconduit->m_data.SetStatusText(errorstring, CONDUIT_STATUS_OK);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
					g_pconduit->m_data.SetStatusText(errorstring, CONDUIT_STATUS_ERROR);
				}
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				g_pconduit->m_data.SetStatusText(errorstring, CONDUIT_STATUS_ERROR);
			}

*/


			m_pszIconPath = file.GetIniString("FileServer", "IconPath", "C:\\Inetpub\\wwwroot\\Cortex\\conduit\\images\\", m_pszIconPath);

			m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
			m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;
			m_bUseXMLClientLog = file.GetIniInt("Messager", "UseXMLClientLog", 0)?true:false;
			m_bMillisecondMessaging = file.GetIniInt("Messager", "MillisecondMessaging", 1)?true:false;			// use millisecond resolution for messages and asrun
			m_nNumberOfStatusLines = file.GetIniInt("Messager", "NumberOfStatusLines", 1000);		


			

			m_pszDSN = file.GetIniString("Database", "DSN", m_pszName?m_pszName:"Conduit", m_pszDSN);
			m_pszUser = file.GetIniString("Database", "DBUser", "sa", m_pszUser);
			m_pszPW = file.GetIniString("Database", "DBPassword", "", m_pszPW);
			m_pszDatabase = file.GetIniString("Database", "DBDefault", m_pszName?m_pszName:"Conduit", m_pszDatabase);
			m_pszDBServer = file.GetIniString("Database", "DBServer", (g_pconduit->m_data.m_pszHost?g_pconduit->m_data.m_pszHost:"127.0.0.1"), m_pszDBServer);

			m_bPeriodicQueries = file.GetIniInt("Database", "UsePeriodicQueries", 0)?true:false; 
			m_bPeriodicShell = file.GetIniInt("Database", "UsePeriodicShellCmds", 0)?true:false; 
			m_bPeriodicHttp = file.GetIniInt("Database", "UsePeriodicHttp", 0)?true:false; 


			if(m_bPeriodicQueries)
			{
//			AfxMessageBox("m_bPeriodicQueries was true");
	EnterCriticalSection(&g_pconduit->m_data.m_critQueries);
				if(m_pszPeriodicQueries) free(m_pszPeriodicQueries); m_pszPeriodicQueries=NULL;

				if(g_pconduit->m_data.m_ppPeriodicQuery)
				{
					int q=0;
					while(q<g_pconduit->m_data.m_nNumQueries)
					{
						if(g_pconduit->m_data.m_ppPeriodicQuery[q]) delete g_pconduit->m_data.m_ppPeriodicQuery[q]; // delete objects, must use new to allocate
						q++;
					}
					delete [] g_pconduit->m_data.m_ppPeriodicQuery; // delete array of pointers to objects, must use new to allocate

				}
				g_pconduit->m_data.m_ppPeriodicQuery = NULL;
				g_pconduit->m_data.m_nNumQueries=0;

				m_pszPeriodicQueries = file.GetIniString("Database", "PeriodicQueries", "", m_pszPeriodicQueries);    // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.
//				AfxMessageBox("got m_pszPeriodicQueries"); AfxMessageBox(m_pszPeriodicQueries);

				if(pszParams) free(pszParams); pszParams=NULL;
				if((m_pszPeriodicQueries)&&(strlen(m_pszPeriodicQueries)))	pszParams = (char*)malloc(strlen(m_pszPeriodicQueries)+1);
				if(pszParams)
				{
					strcpy(pszParams, m_pszPeriodicQueries);

//				AfxMessageBox("got pszParams"); AfxMessageBox(pszParams);

					CSafeBufferUtil sbu;
					char* pch = sbu.Token(pszParams, strlen(pszParams), "|");
					while(pch)
					{
						unsigned long ulInterval = atol(pch);
						unsigned long ulOffset=0;   // number of seconds to offset
						pch = strchr(pch, '+');  //search for offset
						if(pch)
						{
							pch++;
							ulOffset=atol(pch);
						}

						pch = sbu.Token(NULL, NULL, "|");

						if((pch)&&(strlen(pch)>0))
						{
							CPeriodicObject* pQuery = new CPeriodicObject;
							if(pQuery)
							{
								pQuery->m_pszObject = (char*)malloc(strlen(pch)+1);
								if(pQuery->m_pszObject)
								{
									strcpy(pQuery->m_pszObject, pch);
//									AfxMessageBox(pQuery->m_pszObject);
									pQuery->m_ulInterval = ulInterval; // number of seconds between calls
									pQuery->m_ulOffset = ulOffset;   // number of seconds to offset

									CPeriodicObject** ppQueries = new CPeriodicObject*[g_pconduit->m_data.m_nNumQueries+1];
									if(ppQueries)
									{
										int npq=0;
										if((g_pconduit->m_data.m_ppPeriodicQuery)&&(g_pconduit->m_data.m_nNumQueries))
										{
											while(npq<g_pconduit->m_data.m_nNumQueries)
											{
												ppQueries[npq] = g_pconduit->m_data.m_ppPeriodicQuery[npq];
												npq++;
											}
											delete [] g_pconduit->m_data.m_ppPeriodicQuery;
										}
										ppQueries[npq] = pQuery;
										g_pconduit->m_data.m_nNumQueries = npq+1;
										g_pconduit->m_data.m_ppPeriodicQuery = ppQueries;
									}
									else
										delete pQuery;
								}
								else 
									delete pQuery;
							}
						}
						pch = sbu.Token(NULL, NULL, "|");
					}
				}
	LeaveCriticalSection(&g_pconduit->m_data.m_critQueries);

			}
//			else	AfxMessageBox("m_bPeriodicQueries was false");

			if(m_bPeriodicShell)
			{
	EnterCriticalSection(&g_pconduit->m_data.m_critShell);
				if(m_pszPeriodicShellCmds) free(m_pszPeriodicShellCmds); m_pszPeriodicShellCmds=NULL;

				if(g_pconduit->m_data.m_ppPeriodicShell)
				{
					int q=0;
					while(q<g_pconduit->m_data.m_nNumShellCmds)
					{
						if(g_pconduit->m_data.m_ppPeriodicShell[q]) delete g_pconduit->m_data.m_ppPeriodicShell[q]; // delete objects, must use new to allocate
						q++;
					}
					delete [] g_pconduit->m_data.m_ppPeriodicShell; // delete array of pointers to objects, must use new to allocate
				}
				g_pconduit->m_data.m_ppPeriodicShell = NULL;
				g_pconduit->m_data.m_nNumShellCmds = 0;

				m_pszPeriodicShellCmds = file.GetIniString("Database", "PeriodicShellCmds", "", m_pszPeriodicShellCmds);    // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.

//				AfxMessageBox(m_pszPeriodicShellCmds);
				if(pszParams) free(pszParams); pszParams=NULL;
				if((m_pszPeriodicShellCmds)&&(strlen(m_pszPeriodicShellCmds)))	pszParams = (char*)malloc(strlen(m_pszPeriodicShellCmds)+1);
				if(pszParams)
				{
					strcpy(pszParams, m_pszPeriodicShellCmds);

//				AfxMessageBox("got m_pszPeriodicShellCmds"); AfxMessageBox(m_pszPeriodicShellCmds);

					CSafeBufferUtil sbu;
					char* pch = sbu.Token(pszParams, strlen(pszParams), "|");
					while(pch)
					{
						unsigned long ulInterval = atol(pch);
						unsigned long ulOffset=0;   // number of seconds to offset
						pch = strchr(pch, '+');  //search for offset
						if(pch)
						{
							pch++;
							ulOffset=atol(pch);
						}

						pch = sbu.Token(NULL, NULL, "|");

						if((pch)&&(strlen(pch)>0))
						{
							CPeriodicObject* pQuery = new CPeriodicObject;
							if(pQuery)
							{
								pQuery->m_pszObject = (char*)malloc(strlen(pch)+1);
								if(pQuery->m_pszObject)
								{
									strcpy(pQuery->m_pszObject, pch);
									pQuery->m_ulInterval = ulInterval; // number of seconds between calls
									pQuery->m_ulOffset = ulOffset;   // number of seconds to offset

									CPeriodicObject** ppQueries = new CPeriodicObject*[g_pconduit->m_data.m_nNumShellCmds+1];
									if(ppQueries)
									{
										int npq=0;
										if((g_pconduit->m_data.m_ppPeriodicShell)&&(g_pconduit->m_data.m_nNumShellCmds))
										{
											while(npq<g_pconduit->m_data.m_nNumShellCmds)
											{
												ppQueries[npq] = g_pconduit->m_data.m_ppPeriodicShell[npq];
												npq++;
											}
											delete [] g_pconduit->m_data.m_ppPeriodicShell;
										}
										ppQueries[npq] = pQuery;
										g_pconduit->m_data.m_nNumShellCmds = npq+1;
										g_pconduit->m_data.m_ppPeriodicShell = ppQueries;
									}
									else delete pQuery;
								}
								else delete pQuery;
							}
						}
						pch = sbu.Token(NULL, NULL, "|");
					}
				}
	LeaveCriticalSection(&g_pconduit->m_data.m_critShell);

			}
			if(m_bPeriodicHttp)
			{
	EnterCriticalSection(&g_pconduit->m_data.m_critHttp);
				if(m_pszPeriodicHttp) free(m_pszPeriodicHttp); m_pszPeriodicHttp=NULL;

				if(g_pconduit->m_data.m_ppPeriodicHttp)
				{
					int q=0;
					while(q<g_pconduit->m_data.m_nNumHttpCalls)
					{
						if(g_pconduit->m_data.m_ppPeriodicHttp[q]) delete g_pconduit->m_data.m_ppPeriodicHttp[q]; // delete objects, must use new to allocate
						q++;
					}
					delete [] g_pconduit->m_data.m_ppPeriodicHttp; // delete array of pointers to objects, must use new to allocate
				}
				g_pconduit->m_data.m_ppPeriodicHttp = NULL;
				g_pconduit->m_data.m_nNumHttpCalls = 0;

				m_pszPeriodicHttp = file.GetIniString("Database", "PeriodicHttp", "", m_pszPeriodicHttp);    // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.
				if(pszParams) free(pszParams); pszParams=NULL;
				if((m_pszPeriodicHttp)&&(strlen(m_pszPeriodicHttp)))	pszParams = (char*)malloc(strlen(m_pszPeriodicHttp)+1);
				if(pszParams)
				{
					strcpy(pszParams, m_pszPeriodicHttp);

//				AfxMessageBox("got m_pszPeriodicHttp"); AfxMessageBox(m_pszPeriodicHttp);

					CSafeBufferUtil sbu;
					char* pch = sbu.Token(pszParams, strlen(pszParams), "|");
					while(pch)
					{
						unsigned long ulInterval = atol(pch);
						unsigned long ulOffset=0;   // number of seconds to offset
						pch = strchr(pch, '+');  //search for offset
						if(pch)
						{
							pch++;
							ulOffset=atol(pch);
						}

						pch = sbu.Token(NULL, NULL, "|");

						if((pch)&&(strlen(pch)>0))
						{
							CPeriodicObject* pQuery = new CPeriodicObject;
							if(pQuery)
							{
								pQuery->m_pszObject = (char*)malloc(strlen(pch)+1);
								if(pQuery->m_pszObject)
								{
									strcpy(pQuery->m_pszObject, pch);
									pQuery->m_ulInterval = ulInterval; // number of seconds between calls
									pQuery->m_ulOffset = ulOffset;   // number of seconds to offset

									CPeriodicObject** ppQueries = new CPeriodicObject*[g_pconduit->m_data.m_nNumHttpCalls+1];
									if(ppQueries)
									{
										int npq=0;
										if((g_pconduit->m_data.m_ppPeriodicHttp)&&(g_pconduit->m_data.m_nNumHttpCalls))
										{
											while(npq<g_pconduit->m_data.m_nNumHttpCalls)
											{
												ppQueries[npq] = g_pconduit->m_data.m_ppPeriodicHttp[npq];
												npq++;
											}
											delete [] g_pconduit->m_data.m_ppPeriodicHttp;
										}
										ppQueries[npq] = pQuery;
										g_pconduit->m_data.m_nNumHttpCalls = npq+1;
										g_pconduit->m_data.m_ppPeriodicHttp = ppQueries;
									}
									else delete pQuery;
								}
								else delete pQuery;
							}
						}
						pch = sbu.Token(NULL, NULL, "|");
					}
				}
	LeaveCriticalSection(&g_pconduit->m_data.m_critHttp);

			}





//			m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings", m_pszSettings);  // the Settings table name
//			m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange", m_pszExchange);  // the Exchange table name
//			m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages", m_pszMessages);  // the Messages table name
//			m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue", m_pszQueue);  // the Queue table name
//			m_pszDownloaders = file.GetIniString("Database", "DownloaderTableName", "DataFeeds", m_pszDownloaders);  // the  downloader table name
//			m_pszEvents = file.GetIniString("Database", "EventViewName", "TriggerInfo", m_pszEvents); // the Events view name 
//			m_pszAsRun=file.GetIniString("Database", "AsRunTableName", "AsRun_Log", m_pszAsRun);  // the As-run table name
//			m_pszCommandQueue = file.GetIniString("Database", "CommandQueueTableName", "Command_Queue", m_pszCommandQueue);  // the Queue table name

	//		m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels");  // the Channels table name
	//		m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Connections");  // the Connections table name
	//		m_pszLiveEvents = file.GetIniString("Database", "LiveEventsTableName", "Events");  // the LiveEvents table name

			m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 3000);  // in milliseconds


			m_nTriggerAdvanceMS = file.GetIniInt("Automation", "TriggerAdvanceMS", 0);// number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)
			m_nAnalyzeRulesDwellMS = file.GetIniInt("Automation", "AnalyzeRulesDwellMS", 1000);
			m_nAnalyzeParameterDwellMS = file.GetIniInt("Automation", "AnalyzeParameterDwellMS", 2500);
			m_nAnalyzeTimingDwellMS = file.GetIniInt("Automation", "AnalyzeTimingDwellMS", 30000);
			m_bUseLocalClock = file.GetIniInt("Automation", "UseLocalClock", 0)?true:false;
			m_bUseUTC = file.GetIniInt("Automation", "UseUTC", 0)?true:false;


			file.SetIniInt("Messager", "NumberOfStatusLines", m_nNumberOfStatusLines);		

			m_pszFileSpec = file.GetIniString("Messager", "LogFileIni", "Logs\\Conduit|YD||1|", m_pszFileSpec);
				// SMTP address[: override port (optional)] | distribution lists | from email |  subject spec | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
			m_pszMailSpec = file.GetIniString("Messager", "SMTPIni", "mail.server.com|f3:recipient@address.com|\"someone\"<from@here.com>|%P %T from %h|%h|C:\\conduitsmtp.txt|1|1|0", m_pszMailSpec);
			if(m_pszFileSpec) 
			{
				if(m_pszProcessedFileSpec)
				{
					try{ free(m_pszProcessedFileSpec); } catch(...) {}
				}
				m_pszProcessedFileSpec = ProcessString(m_pszFileSpec, false);
			}
			
			if(m_pszMailSpec) 
			{
				if(m_pszProcessedMailSpec)
				{
					try{ free(m_pszProcessedMailSpec); } catch(...) {}
				}
				m_pszProcessedMailSpec = ProcessString(m_pszMailSpec, false);
			}

		}
		else //write
		{
			file.SetIniString("Main", "Name", m_pszName);
			file.SetIniString("Main", "Type", m_pszType);
			file.SetIniString("Main", "Project", m_pszProject);
			file.SetIniString("License", "Key", m_pszLicense);

			file.SetIniInt("Main", "ThreadDwellMS", m_nThreadDwellMS);

			file.SetIniInt("Database", "DebugLists", m_bDebugLists?1:0);  // if true, dumps the temp and events databases
			file.SetIniInt("Database", "DebugListTopCount", m_nDebugListTopCount);  //if positive, just prints out the top n recored in the events tables.
			file.SetIniInt("Database", "DebugSQL", m_bDebugSQL?1:0);  // if true, prints out the SQL calls
			file.SetIniInt("Database", "DebugInsertSQL", m_bDebugInsertSQL?1:0);  // if true, prints out the event insert SQL calls
			file.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.
			file.SetIniString("Database", "DebugOrder", m_pszDebugOrder); // the order by clause for debug

			file.SetIniInt("CommandServer", "ListenPort", m_usCommandPort);
			file.SetIniInt("StatusServer", "ListenPort", m_usStatusPort);

			file.SetIniString("FileServer", "IconPath", m_pszIconPath);

			file.SetIniInt("Messager", "UseEmail", m_bUseEmail?1:0);
			file.SetIniInt("Messager", "UseNet", m_bUseNetwork?1:0);
			file.SetIniInt("Messager", "UseLog", m_bUseLog?1:0);
			file.SetIniInt("Messager", "ReportSuccessfulOperation", m_bReportSuccessfulOperation?1:0);
			file.SetIniInt("Messager", "LogTransfers", m_bLogTransfers?1:0);
			file.SetIniInt("Messager", "UseXMLClientLog", m_bUseXMLClientLog?1:0);
			file.SetIniInt("Messager", "MillisecondMessaging", m_bMillisecondMessaging?1:0);		// use millisecond resolution for messages and asrun


			file.SetIniString("Database", "DSN", m_pszDSN);
			file.SetIniString("Database", "DBUser", m_pszUser);
			file.SetIniString("Database", "DBPassword", m_pszPW);
			file.SetIniString("Database", "DBDefault", m_pszDatabase );

//			file.SetIniString("Database", "SettingsTableName", m_pszSettings);  // the Settings table name
//			file.SetIniString("Database", "ExchangeTableName", m_pszExchange);  // the Exchange table name
//			file.SetIniString("Database", "MessagesTableName", m_pszMessages);  // the Messages table name
//			file.SetIniString("Database", "QueueTableName", m_pszQueue);  // the Queue table name
//			file.SetIniString("Database", "DownloaderTableName", m_pszDownloaders);  // the  downloader table name
//			file.SetIniString("Database", "EventViewName", m_pszEvents); // the Events view name 
//			file.SetIniString("Database", "AsRunTableName", m_pszAsRun);  // the As-run table name
//			file.SetIniString("Database", "CommandQueueTableName", m_pszCommandQueue);  // the commandQueue table name

			file.SetIniInt("Database", "ModificationCheckInterval", m_ulModsIntervalMS);  // in milliseconds

			file.SetIniInt("Database", "UsePeriodicQueries", m_bPeriodicQueries?1:0);  
			file.SetIniInt("Database", "UsePeriodicShellCmds", m_bPeriodicShell?1:0);  
			file.SetIniInt("Database", "UsePeriodicHttp", m_bPeriodicHttp?1:0);  


			file.SetIniInt("Automation", "AnalyzeRulesDwellMS",  m_nAnalyzeRulesDwellMS);
			file.SetIniInt("Automation", "AnalyzeParameterDwellMS",  m_nAnalyzeParameterDwellMS);
			file.SetIniInt("Automation", "AnalyzeTimingDwellMS",  m_nAnalyzeTimingDwellMS);

			file.SetIniInt("Automation", "TriggerAdvanceMS", m_nTriggerAdvanceMS);// number of milliseconds before re-analyzed on air time, to send triggers (compensation factor for latency)
			file.SetIniInt("Automation", "UseLocalClock", m_bUseLocalClock?1:0);
			file.SetIniInt("Automation", "UseUTC", m_bUseUTC?1:0);  

			file.SetIniInt("AutoPurge", "AutoPurgeMessageDays", m_nAutoPurgeMessageDays);
			file.SetIniInt("AutoPurge", "AutoPurgeAsRunDays", m_nAutoPurgeAsRunDays);
			file.SetIniInt("AutoPurge", "AutoPurgeInterval", m_nAutoPurgeInterval);  // 5 minutes should be enough.

			file.SetIniString("Messager", "LogFileIni", m_pszFileSpec);
			file.SetIniString("Messager", "SMTPIni", m_pszMailSpec);

			if((m_bPeriodicQueries)&&(m_pszPeriodicQueries))
			{
//				AfxMessageBox("setting m_pszPeriodicQueries"); AfxMessageBox(m_pszPeriodicQueries);
				file.SetIniString("Database", "PeriodicQueries", m_pszPeriodicQueries);    // a pipe-delimited string indicating intervals and stored procedures (or any SQL query). 
// format: "10|sp_backup|60+30|sp_backup2" runs sp_backup every ten seconds starting immediately, and sp_backup2 every minute, starting after 30 seonds.  the query "sp_backup" may be replaced by a full query such as "INSERT INTO table (id, value) VALUES (1, 0)" or any valid SQL string.
			}
			if((m_bPeriodicShell)&&(m_pszPeriodicShellCmds))
			{
//				AfxMessageBox("setting m_pszPeriodicShellCmds"); AfxMessageBox(m_pszPeriodicShellCmds);
				file.SetIniString("Database", "PeriodicShellCmds", m_pszPeriodicShellCmds);    // a pipe-delimited string indicating intervals and cmds
			}
			if((m_bPeriodicHttp)&&(m_pszPeriodicHttp))
			{
//				AfxMessageBox("setting m_pszPeriodicHttp"); AfxMessageBox(m_pszPeriodicHttp);
				file.SetIniString("Database", "PeriodicHttp", m_pszPeriodicHttp);    // a pipe-delimited string indicating intervals and http calls
			}



			file.SetSettings(pszFilename, false);  // have to have correct filename

		}
		return CONDUIT_SUCCESS;
	}
	return CONDUIT_ERROR;
}

int CConduitSettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	return 0; // just get out of here.

/*
	if((m_pszSettings)&&(strlen(m_pszSettings)>0)&&(m_pdbConn)&&(m_pdb)&&(m_pdbConn->m_bConnected)&&(Settings(true)==CONDUIT_SUCCESS))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = CONDUIT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCategory="";
				CString szParameter="";
				CString szValue="";
				CString szTemp="";
				int min, max;
				bool bmin = false, bmax = false;
				try
				{
					prs->GetFieldValue("category", szCategory);  //HARDCODE
					prs->GetFieldValue("parameter", szParameter);  //HARDCODE
					prs->GetFieldValue("value", szValue);  //HARDCODE
					prs->GetFieldValue("min_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						min = atoi(szTemp);
						bmin = true;
					}
					prs->GetFieldValue("max_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						max = atoi(szTemp);
						bmax = true;
					}
				}
				catch( ... )
				{
				}

				int nLength = szValue.GetLength();
				if(szCategory.CompareNoCase("Main")==0)
				{
					if(szParameter.CompareNoCase("Name")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszName)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszName) free(m_pszName);
								m_pszName = pch;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("License")==0)
				{
					if(szParameter.CompareNoCase("Key")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszLicense)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLicense) free(m_pszLicense);
								m_pszLicense = pch;

								// recompile license key params
								if(g_pconduit->m_data.m_key.m_pszLicenseString) free(g_pconduit->m_data.m_key.m_pszLicenseString);
								g_pconduit->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
								if(g_pconduit->m_data.m_key.m_pszLicenseString)
								sprintf(g_pconduit->m_data.m_key.m_pszLicenseString, "%s", szValue);

								g_pconduit->m_data.m_key.InterpretKey();

								char errorstring[MAX_MESSAGE_LENGTH];
								if(g_pconduit->m_data.m_key.m_bValid)
								{
									unsigned long i=0;
									while(i<g_pconduit->m_data.m_key.m_ulNumParams)
									{
										if((g_pconduit->m_data.m_key.m_ppszParams)
											&&(g_pconduit->m_data.m_key.m_ppszValues)
											&&(g_pconduit->m_data.m_key.m_ppszParams[i])
											&&(g_pconduit->m_data.m_key.m_ppszValues[i]))
										{
											if(stricmp(g_pconduit->m_data.m_key.m_ppszParams[i], "max")==0)
											{
//												g_pconduit->m_data.m_nMaxLicensedDevices = atoi(g_pconduit->m_data.m_key.m_ppszValues[i]);
											}
										}
										i++;
									}
								
									if(
											(
												(!g_pconduit->m_data.m_key.m_bExpires)
											||((g_pconduit->m_data.m_key.m_bExpires)&&(!g_pconduit->m_data.m_key.m_bExpired))
											||((g_pconduit->m_data.m_key.m_bExpires)&&(g_pconduit->m_data.m_key.m_bExpireForgiveness)&&(g_pconduit->m_data.m_key.m_ulExpiryDate+g_pconduit->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
											)
										&&(
												(!g_pconduit->m_data.m_key.m_bMachineSpecific)
											||((g_pconduit->m_data.m_key.m_bMachineSpecific)&&(g_pconduit->m_data.m_key.m_bValidMAC))
											)
										)
									{
					// this overrides stuff so let's not let it do that.

										// let's just not override this in a green condition.  
										// comment it out.
//										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//										g_pconduit->m_data.SetStatusText(errorstring, CONDUIT_STATUS_OK);
									}
									else
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
										g_pconduit->m_data.SetStatusText(errorstring, CONDUIT_STATUS_ERROR);
									}
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
									g_pconduit->m_data.SetStatusText(errorstring, CONDUIT_STATUS_ERROR);
								}

							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("CommandServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usCommandPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("StatusServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usStatusPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Messager")==0)
				{
					if(szParameter.CompareNoCase("UseEmail")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseEmail = true;
							else m_bUseEmail = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseNet")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseNetwork = true;
							else m_bUseNetwork = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseLog = true;
							else m_bUseLog = false;
						}
					}
					else
					if(szParameter.CompareNoCase("ReportSuccessfulOperation")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bReportSuccessfulOperation = true;
							else m_bReportSuccessfulOperation = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseXMLClientLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseXMLClientLog = true;
							else m_bUseXMLClientLog = false;
						}
					}
				}
				else
/*
				if(szCategory.CompareNoCase("FileHandling")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("DeleteSourceFileOnTransfer")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bDeleteSourceFileOnTransfer = true;
							else m_bDeleteSourceFileOnTransfer = false;
						}
					}
				}
				else
* /
				if(szCategory.CompareNoCase("Database")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("SettingsTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszSettings)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszSettings) free(m_pszSettings);
								m_pszSettings = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ExchangeTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszExchange)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszExchange) free(m_pszExchange);
								m_pszExchange = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("MessagesTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszMessages)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszMessages) free(m_pszMessages);
								m_pszMessages = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("QueueTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszQueue)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszQueue) free(m_pszQueue);
								m_pszQueue = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("CommandQueueTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszCommandQueue)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszCommandQueue) free(m_pszCommandQueue);
								m_pszCommandQueue = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("AsRunTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszAsRun)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszAsRun) free(m_pszAsRun);
								m_pszAsRun = pch;
							}
						}
					}
/*
					else
					if(szParameter.CompareNoCase("ConnectionsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszConnections) free(m_pszConnections);
								m_pszConnections = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("LiveEventsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLiveEvents) free(m_pszLiveEvents);
								m_pszLiveEvents = pch;
							}
						}
					}
* /
					else
					if(szParameter.CompareNoCase("ModificationCheckInterval")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_ulModsIntervalMS = nLength;
						}
					}
				}
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
					if(szParameter.CompareNoCase("Messages")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeMessageDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
					else
					if(szParameter.CompareNoCase("AsRun_Log")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeAsRunDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
				}
/*
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
				}
* /				


				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			if(pszInfo)
			{
				_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
			}
			delete prs;
			prs = NULL;

			Settings(false); //write
			return CONDUIT_SUCCESS;
		}
	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s%s%s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. ",
				m_pdb?"":"Database pointer was NULL. ",
				m_pdbConn?"":"Connection pointer was NULL. ",
				((m_pdbConn)&&(!m_pdbConn->m_bConnected))?"Database not connected. ":""
				);
		}
	}
	return CONDUIT_ERROR;
	*/
}

char* CConduitSettings::ProcessString(char* pszString, bool bFreeIncomingString)
{
	if(pszString)
	{
		char pszOutput[4096];
		int nInLen=strlen(pszString);
		int nOutLen=0;
		char* pch = pszString;
		char* pchEnd = min((pch+4096),(pszString+nInLen));
		while((*pch!=0)&&(pch<pchEnd))
		{
			if(*pch == '%')
			{
				// we are in a specifier.
				// here is a list of ours:
				// %h  local hostname
		
				pch++;
				if((*pch!=0)&&(pch<pchEnd))
				{
					if(*pch == 'h')
					{
						if((g_pconduit->m_data.m_pszHost)&&(strlen(g_pconduit->m_data.m_pszHost)))
						{
							char* pszTemp = (char*)malloc(strlen(g_pconduit->m_data.m_pszHost)+1);
							if(pszTemp)
							{
								strcpy(pszTemp, g_pconduit->m_data.m_pszHost);
							
								char* pchPlus = pszTemp;
								while((*pchPlus != 0)&&(nOutLen<4096))
								{
									pszOutput[nOutLen]= *pchPlus;
									nOutLen++;
									pchPlus++;
								}
								free(pszTemp);
							}
						}
						pch++;
					}
					else
					{
						// some other specifier, can be anything.
						pszOutput[nOutLen]= *(pch-1); // do add the %, this may get further processed
						nOutLen++;
						pszOutput[nOutLen]= *pch;
						nOutLen++;
						pch++;
					}
				}
				else
				{
					pszOutput[nOutLen]= *(pch-1);
					nOutLen++;
				}
				
			}
			else
			{
				pszOutput[nOutLen]= *pch;
				nOutLen++;
				pch++;
			}
		}

		pszOutput[nOutLen]=0;

		pch = (char*)malloc(strlen(pszOutput)+1);
		if(pch)
		{
			strcpy(pch,pszOutput);
			if(bFreeIncomingString) 
			{
				try {free(pszString);} catch(...){}
			}
		}
		return pch;
	}
	return NULL;
}




