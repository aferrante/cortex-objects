// Barbero.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "BarberoMain.h"
#include "Barbero.h"
#include "BarberoHandler.h"
#include "BarberoDlg.h"
#include <process.h>
//#include "BarberoSettings.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// global control vars for main thread
extern bool g_bKillThread;
extern bool g_bThreadStarted;


// ******************************************
// THINGS you have to do to get this project to compile correctly
// 1) Add to files: bmp.cpp, bmp.h, EmailHandler.cpp/.h, 
//    FileHandler.cpp/.h, ListCtrlEx.cpp/.h, MessageDispatcher.cpp/.h, MDdefines.h,
//    NAHandler.cpp/.h, networking.cpp/.h, sendmail.cpp/.h, TextUtils.cpp/.h,
//    UIHandler.cpp/.h
//    The inlcludes for all these are in "$$root$$.h", and you can remove them if
//    your project does not need them.  However, the default project is built with:
//    an App Class, a Dlg class, a Handler Class, and a Settings class.
//    The App Class is the App, the Dlg is the Dialog which is displayed when the
//    systray icon is hit.  This is the class that is the "main dialog" and has all the
//    Messge dispatching, etc.  Howevr, it is not the application's main window.  The
//    main window is not visible, it is the Handler class.  It is also persistent, and
//    basically handles creation, destruction, the icons and handles the mouse clicks
//    in the systray.  The Settings class handles all the Settings, the dialog is not
//    persistent although the data is.

// ******************************************
// There is a list of commented out includes in the "$$root$$.h" file.  
// Generally, add includes to that file, as all files in the project include that file.


/////////////////////////////////////////////////////////////////////////////
// CBarberoApp

BEGIN_MESSAGE_MAP(CBarberoApp, CWinApp)
	//{{AFX_MSG_MAP(CBarberoApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBarberoApp construction

CBarberoApp::CBarberoApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
//	m_ulFlags = CXWF_DEFAULT;
	m_pszSettingsURL = NULL;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CBarberoApp object

CBarberoApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CBarberoApp initialization

BOOL CBarberoApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	// uncomment the following to initialize RichEdit Controls
//	AfxInitRichEdit();

	// prevent multiple instances
/*
	bool bAllowMultiple = false;
  if (m_lpCmdLine[0] != '\0')
  {
		if( (strstr(m_lpCmdLine, "/c")) || (strstr(m_lpCmdLine, "/C")) )  // clone mode.  Allow another instance (until we check later for existence of a listener)
		bAllowMultiple = true;
  }
*/
//	AfxMessageBox( m_lpCmdLine );

	::CreateMutex(NULL,TRUE,m_pszExeName);
	if((GetLastError()==ERROR_ALREADY_EXISTS))
		//&&(!bAllowMultiple)) 
		return FALSE;  

  
	// uncomment the following to write PID to file (change filename)
  FILE *fp; fp = fopen("barbero.pid", "wt");
  if (fp) { fprintf(fp, "%ld\n", _getpid()); fclose(fp); } 


#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

  //SetRegistryKey("Video Design Interactive");  // can use this for licensing later on, or use a hidden file somewhere

	CBarberoDlg* pDlg = new CBarberoDlg();
	VERIFY(pDlg->Create());
//	pDlg->DoModal();

// handler window for systray
	CBarberoHandler* pWnd = new CBarberoHandler();
	pWnd->m_pMainDlg = pDlg;
	VERIFY(pWnd->Create());
	m_pMainWnd = pWnd;


/////////////////////////////////////////////////////////////////////////
// this was a test
/*
	CXDlg* prDlg = new CXDlg;
	if(prDlg)
	{
		if(!prDlg->Create()) AfxMessageBox("Could not create object");
		else
		{
			x
			prDlg->UpdateData(FALSE); 
			prDlg->SetWindowText("xxx"); 

			prDlg->ShowWindow(SW_SHOW);
		}
	}
	return TRUE;
*/
/////////////////////////////////////////////////////////////////////////

	// here start the main (almost non-MFC) thread.
	// we can feed a pointer to the app into it to give it access to the status windows.
	// all settings are in the thread.
	g_bThreadStarted = false;
	if(_beginthread(BarberoMainThread, 0, (void*) this)<0)
	{
		AfxMessageBox("Could not begin main thread!\nMust exit.");
		return FALSE;
	}

	return TRUE;
}

int CBarberoApp::ExitInstance() 
{
	g_bKillThread=true;
	while(g_bThreadStarted) Sleep(1);
	if(m_pszSettingsURL) free(m_pszSettingsURL);
	return CWinApp::ExitInstance();
}
