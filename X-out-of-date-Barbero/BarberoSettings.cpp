// BarberoSettings.cpp: implementation of the CBarberoSettings.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "BarberoDefines.h"
#include "BarberoSettings.h"
#include "BarberoMain.h" 

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CBarberoMain* g_pbarbero;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBarberoSettings::CBarberoSettings()
{
	m_pdb = NULL;
	m_pdbConn = NULL;

	m_pszName = NULL;
	m_ulMainMode = BARBERO_MODE_DEFAULT;

	// ports
	m_usCommandPort	= BARBERO_PORT_CMD;
	m_usStatusPort	= BARBERO_PORT_STATUS;

	// messaging for Barbero
	m_bUseLog = true;			// write a log file
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host
	m_bReportSuccessfulOperation=false;
	m_bLogTransfers=false;
	m_pszFileSpec = NULL;


	m_bDiReCTInstalled = false;
	// Harris API
//	m_bUseListCount = false; // get all events up until the list count (otherwise just up to the lookahead)

	// DSN params
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;

	m_pszSettings = NULL;  // the Settings table name
	m_pszExchange = NULL;  // the Exchange table name
	m_pszMessages = NULL;  // the Messages table name

	m_pszDestinationMediaTemp = NULL;
	m_pszDestinationMedia = NULL;  // the DestinationMedia table name
	m_pszDestinations = NULL;  // the Destinations table name
	m_pszQueue = NULL; // the Queue table name

	m_ulModsIntervalMS = 1000;

	m_pszLicense=NULL;  // the License Key
	m_pszIconPath=NULL;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.
}

CBarberoSettings::~CBarberoSettings()
{
	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_pszDSN) free(m_pszDSN); // must use malloc to allocate
	if(m_pszUser) free(m_pszUser); // must use malloc to allocate
	if(m_pszPW) free(m_pszPW); // must use malloc to allocate
	if(m_pszSettings) free(m_pszSettings); // must use malloc to allocate
	if(m_pszExchange) free(m_pszExchange); // must use malloc to allocate
	if(m_pszMessages) free(m_pszMessages); // must use malloc to allocate

	if(m_pszDestinationMediaTemp) free(m_pszDestinationMediaTemp); // must use malloc to allocate
	if(m_pszDestinationMedia) free(m_pszDestinationMedia); // must use malloc to allocate
	if(m_pszDestinations) free(m_pszDestinations); // must use malloc to allocate
	if(m_pszQueue) free(m_pszQueue); // must use malloc to allocate
	if(m_pszLicense) free(m_pszLicense); // must use malloc to allocate
	if(m_pszIconPath) free(m_pszIconPath); // must use malloc to allocate	
	if(m_pszFileSpec) free(m_pszFileSpec); // must use malloc to allocate
}


int CBarberoSettings::Settings(bool bRead)
{
//	char pszFilename[MAX_PATH];
	CFileUtil file;
	// get settings.
	file.GetSettings(BARBERO_SETTINGS_FILE_DEFAULT, false); 

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
		// load up the values on the settings object
			m_pszName = file.GetIniString("Main", "Name", "Barbero", m_pszName);
			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);

			// recompile license key params
			if(g_pbarbero->m_data.m_key.m_pszLicenseString) free(g_pbarbero->m_data.m_key.m_pszLicenseString);
			g_pbarbero->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(m_pszLicense)+1);
			if(g_pbarbero->m_data.m_key.m_pszLicenseString)
			sprintf(g_pbarbero->m_data.m_key.m_pszLicenseString, "%s", m_pszLicense);

			g_pbarbero->m_data.m_key.InterpretKey();

			char errorstring[MAX_MESSAGE_LENGTH];
			if(g_pbarbero->m_data.m_key.m_bValid)
			{
				unsigned long i=0;
				while(i<g_pbarbero->m_data.m_key.m_ulNumParams)
				{
					if((g_pbarbero->m_data.m_key.m_ppszParams)
						&&(g_pbarbero->m_data.m_key.m_ppszValues)
						&&(g_pbarbero->m_data.m_key.m_ppszParams[i])
						&&(g_pbarbero->m_data.m_key.m_ppszValues[i]))
					{
						if(stricmp(g_pbarbero->m_data.m_key.m_ppszParams[i], "max")==0)
						{
							g_pbarbero->m_data.m_nMaxLicensedDevices = atoi(g_pbarbero->m_data.m_key.m_ppszValues[i]);
						}
					}
					i++;
				}
			
				if(
						(
							(!g_pbarbero->m_data.m_key.m_bExpires)
						||((g_pbarbero->m_data.m_key.m_bExpires)&&(!g_pbarbero->m_data.m_key.m_bExpired))
						||((g_pbarbero->m_data.m_key.m_bExpires)&&(g_pbarbero->m_data.m_key.m_bExpireForgiveness)&&(g_pbarbero->m_data.m_key.m_ulExpiryDate+g_pbarbero->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!g_pbarbero->m_data.m_key.m_bMachineSpecific)
						||((g_pbarbero->m_data.m_key.m_bMachineSpecific)&&(g_pbarbero->m_data.m_key.m_bValidMAC))
						)
					)
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
					g_pbarbero->m_data.SetStatusText(errorstring, BARBERO_STATUS_OK);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
					g_pbarbero->m_data.SetStatusText(errorstring, BARBERO_STATUS_ERROR);
				}
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				g_pbarbero->m_data.SetStatusText(errorstring, BARBERO_STATUS_ERROR);
			}

			m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", BARBERO_PORT_CMD);
			m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", BARBERO_PORT_STATUS);

			m_pszIconPath = file.GetIniString("FileServer", "IconPath", "", m_pszIconPath);
			m_pszFileSpec = file.GetIniString("Messager", "LogFileIni", "Barbero|YD||1|", m_pszFileSpec);

			m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
			m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;

			m_pszDSN = file.GetIniString("Database", "DSN", (m_pszName?m_pszName:"Barbero"), m_pszDSN);
			m_pszUser = file.GetIniString("Database", "DBUser", "sa", m_pszUser);
			m_pszPW = file.GetIniString("Database", "DBPassword", "", m_pszPW);
			m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings", m_pszSettings);  // the Settings table name
			m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange", m_pszExchange);  // the Exchange table name
			m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages", m_pszMessages);  // the Messages table name

			m_pszDestinationMediaTemp = file.GetIniString("Database", "DestinationMediaTempTableName", "Destinations_Media_Temp", m_pszDestinationMediaTemp);  // the Destinations table name
			m_pszDestinationMedia = file.GetIniString("Database", "DestinationMediaTableName", "Destinations_Media", m_pszDestinationMedia);  // the Destinations table name
			m_pszDestinations = file.GetIniString("Database", "DestinationsTableName", "Destinations", m_pszDestinations);  // the Destinations table name
			m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue", m_pszQueue);  // the Queue table name

			m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
			
		}
		else //write
		{
			file.SetIniString("Main", "Name", m_pszName);
			file.SetIniString("License", "Key", m_pszLicense);

			file.SetIniInt("CommandServer", "ListenPort", m_usCommandPort);
			file.SetIniInt("StatusServer", "ListenPort", m_usStatusPort);
			file.SetIniString("FileServer", "IconPath", m_pszIconPath);
			file.SetIniString("Messager", "LogFileIni", m_pszFileSpec);

			file.SetIniInt("Messager", "UseEmail", m_bUseEmail?1:0);
			file.SetIniInt("Messager", "UseNet", m_bUseNetwork?1:0);
			file.SetIniInt("Messager", "UseLog", m_bUseLog?1:0);
			file.SetIniInt("Messager", "ReportSuccessfulOperation", m_bReportSuccessfulOperation?1:0);
			file.SetIniInt("Messager", "LogTransfers", m_bLogTransfers?1:0);

			file.SetIniString("Database", "DSN", m_pszDSN);
			file.SetIniString("Database", "DBUser", m_pszUser);
			file.SetIniString("Database", "DBPassword", m_pszPW);
			file.SetIniString("Database", "SettingsTableName", m_pszSettings);  // the Settings table name
			file.SetIniString("Database", "ExchangeTableName", m_pszExchange);  // the Exchange table name
			file.SetIniString("Database", "MessagesTableName", m_pszMessages);  // the Messages table name

	//		file.SetIniInt("HarrisAPI", "UseListCount", m_bUseListCount?1:0); // get all events up until the list count (otherwise just up to the lookahead)

			file.SetIniString("Database", "DestinationsTableName", m_pszDestinations);  // the Destinations table name
			file.SetIniString("Database", "QueueTableName", m_pszQueue);  // the Queue table name
			file.SetIniString("Database", "DestinationMediaTableName", m_pszDestinationMedia);  // the DestinationMedia table name
			file.SetIniString("Database", "DestinationMediaTempTableName", m_pszDestinationMediaTemp );  // the Destinations table name

			file.SetIniInt("Database", "ModificationCheckInterval", m_ulModsIntervalMS);  // in milliseconds

			file.SetSettings(BARBERO_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

		}
		return BARBERO_SUCCESS;
	}
	return BARBERO_ERROR;
}



int CBarberoSettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	if((m_pszSettings)&&(strlen(m_pszSettings)>0)&&(m_pdbConn)&&(m_pdb)&&(m_pdbConn->m_bConnected)&&(Settings(true)==BARBERO_SUCCESS))  //read has to succeed
	{
		// get old settings.
/*
		char pszFilename[MAX_PATH];
		strcpy(pszFilename, BARBERO_SETTINGS_FILE_DEFAULT);  // barbero settings file
		CFileUtil file;
		file.GetSettings(pszFilename, false); 
	// load up the values on the settings object
		if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
		{
			g_pbarbero->m_settings.m_pszName = file.GetIniString("Main", "Name", "Barbero");
			g_pbarbero->m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");
			// dont decompile license here, it may be getting reset below.  if it's the same, then no need to re-do it anyway.

			g_pbarbero->m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", BARBERO_PORT_CMD);
			g_pbarbero->m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", BARBERO_PORT_STATUS);

			g_pbarbero->m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");

			g_pbarbero->m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			g_pbarbero->m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			g_pbarbero->m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			g_pbarbero->m_settings.m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
			g_pbarbero->m_settings.m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;

			g_pbarbero->m_settings.m_pszDSN = file.GetIniString("Database", "DSN", g_pbarbero->m_settings.m_pszName?g_pbarbero->m_settings.m_pszName:"Barbero");
			g_pbarbero->m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
			g_pbarbero->m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
			g_pbarbero->m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
			g_pbarbero->m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
			g_pbarbero->m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name

			g_pbarbero->m_settings.m_pszDestinationMediaTemp = file.GetIniString("Database", "DestinationMediaTempTableName", "Destinations_Media_Temp");  // the Destinations table name
			g_pbarbero->m_settings.m_pszDestinationMedia = file.GetIniString("Database", "DestinationMediaTableName", "Destinations_Media");  // the Destinations table name
			g_pbarbero->m_settings.m_pszDestinations = file.GetIniString("Database", "DestinationsTableName", "Destinations");  // the Destinations table name
			g_pbarbero->m_settings.m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue");  // the Queue table name

			g_pbarbero->m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds

		}
*/
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = BARBERO_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCategory="";
				CString szParameter="";
				CString szValue="";
				CString szTemp="";
				int min, max;
				bool bmin = false, bmax = false;
				try
				{
					prs->GetFieldValue("category", szCategory);  //HARDCODE
					prs->GetFieldValue("parameter", szParameter);  //HARDCODE
					prs->GetFieldValue("value", szValue);  //HARDCODE
					prs->GetFieldValue("min_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						min = atoi(szTemp);
						bmin = true;
					}
					prs->GetFieldValue("max_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						max = atoi(szTemp);
						bmax = true;
					}
				}
				catch( ... )
				{
				}

				int nLength = szValue.GetLength();
				if(szCategory.CompareNoCase("Main")==0)
				{
					if(szParameter.CompareNoCase("Name")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszName) free(m_pszName);
								m_pszName = pch;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("License")==0)
				{
					if(szParameter.CompareNoCase("Key")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLicense) free(m_pszLicense);
								m_pszLicense = pch;

								// recompile license key params
								if(g_pbarbero->m_data.m_key.m_pszLicenseString) free(g_pbarbero->m_data.m_key.m_pszLicenseString);
								g_pbarbero->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
								if(g_pbarbero->m_data.m_key.m_pszLicenseString)
								sprintf(g_pbarbero->m_data.m_key.m_pszLicenseString, "%s", szValue);

								g_pbarbero->m_data.m_key.InterpretKey();

								char errorstring[MAX_MESSAGE_LENGTH];
								if(g_pbarbero->m_data.m_key.m_bValid)
								{
									unsigned long i=0;
									while(i<g_pbarbero->m_data.m_key.m_ulNumParams)
									{
										if((g_pbarbero->m_data.m_key.m_ppszParams)
											&&(g_pbarbero->m_data.m_key.m_ppszValues)
											&&(g_pbarbero->m_data.m_key.m_ppszParams[i])
											&&(g_pbarbero->m_data.m_key.m_ppszValues[i]))
										{
											if(stricmp(g_pbarbero->m_data.m_key.m_ppszParams[i], "max")==0)
											{
												g_pbarbero->m_data.m_nMaxLicensedDevices = atoi(g_pbarbero->m_data.m_key.m_ppszValues[i]);
											}
										}
										i++;
									}
								
									if(
											(
												(!g_pbarbero->m_data.m_key.m_bExpires)
											||((g_pbarbero->m_data.m_key.m_bExpires)&&(!g_pbarbero->m_data.m_key.m_bExpired))
											||((g_pbarbero->m_data.m_key.m_bExpires)&&(g_pbarbero->m_data.m_key.m_bExpireForgiveness)&&(g_pbarbero->m_data.m_key.m_ulExpiryDate+g_pbarbero->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
											)
										&&(
												(!g_pbarbero->m_data.m_key.m_bMachineSpecific)
											||((g_pbarbero->m_data.m_key.m_bMachineSpecific)&&(g_pbarbero->m_data.m_key.m_bValidMAC))
											)
										)
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
										g_pbarbero->m_data.SetStatusText(errorstring, BARBERO_STATUS_OK);
									}
									else
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
										g_pbarbero->m_data.SetStatusText(errorstring, BARBERO_STATUS_ERROR);
									}
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
									g_pbarbero->m_data.SetStatusText(errorstring, BARBERO_STATUS_ERROR);
								}

							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("CommandServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usCommandPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("StatusServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usStatusPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Messager")==0)
				{
					if(szParameter.CompareNoCase("UseEmail")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseEmail = true;
							else m_bUseEmail = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseNet")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseNetwork = true;
							else m_bUseNetwork = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseLog = true;
							else m_bUseLog = false;
						}
					}
				}
/*
				else
				if(szCategory.CompareNoCase("HarrisAPI")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("UseListCount")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseListCount = true;
							else m_bUseListCount = false;
						}
					}
				}
*/
				else
				if(szCategory.CompareNoCase("Database")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("SettingsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszSettings) free(m_pszSettings);
								m_pszSettings = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ExchangeTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszExchange) free(m_pszExchange);
								m_pszExchange = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("MessagesTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszMessages) free(m_pszMessages);
								m_pszMessages = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("DestinationMediaTempTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszDestinationMediaTemp) free(m_pszDestinationMediaTemp);
								m_pszDestinationMediaTemp = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("DestinationMediaTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszDestinationMedia) free(m_pszDestinationMedia);
								m_pszDestinationMedia = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("DestinationsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszDestinations) free(m_pszDestinations);
								m_pszDestinations = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("QueueTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszQueue) free(m_pszQueue);
								m_pszQueue = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ModificationCheckInterval")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_ulModsIntervalMS = nLength;
						}
					}
				}


				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			if(pszInfo)
			{
				_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
			}
			delete prs;


			// save changes to CSF
			Settings(false); //write
/*
			if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
			{
				// these explicts arent necessary - uncomment to write out a full file to edit...
				file.SetIniString("Main", "Name", g_pbarbero->m_settings.m_pszName);
				file.SetIniString("License", "Key", g_pbarbero->m_settings.m_pszLicense);

				file.SetIniInt("CommandServer", "ListenPort", g_pbarbero->m_settings.m_usCommandPort);
				file.SetIniInt("StatusServer", "ListenPort", g_pbarbero->m_settings.m_usStatusPort);
				file.SetIniString("FileServer", "IconPath", g_pbarbero->m_settings.m_pszIconPath);

				file.SetIniInt("Messager", "UseEmail", g_pbarbero->m_settings.m_bUseEmail?1:0);
				file.SetIniInt("Messager", "UseNet", g_pbarbero->m_settings.m_bUseNetwork?1:0);
				file.SetIniInt("Messager", "UseLog", g_pbarbero->m_settings.m_bUseLog?1:0);
				file.SetIniInt("Messager", "ReportSuccessfulOperation", g_pbarbero->m_settings.m_bReportSuccessfulOperation?1:0);
				file.SetIniInt("Messager", "LogTransfers", g_pbarbero->m_settings.m_bLogTransfers?1:0);

				file.SetIniString("Database", "DSN", g_pbarbero->m_settings.m_pszDSN);
				file.SetIniString("Database", "DBUser", g_pbarbero->m_settings.m_pszUser);
				file.SetIniString("Database", "DBPassword", g_pbarbero->m_settings.m_pszPW);
				file.SetIniString("Database", "SettingsTableName", g_pbarbero->m_settings.m_pszSettings);  // the Settings table name
				file.SetIniString("Database", "ExchangeTableName", g_pbarbero->m_settings.m_pszExchange);  // the Exchange table name
				file.SetIniString("Database", "MessagesTableName", g_pbarbero->m_settings.m_pszMessages);  // the Messages table name

		//		file.SetIniInt("HarrisAPI", "UseListCount", g_pbarbero->m_settings.m_bUseListCount?1:0); // get all events up until the list count (otherwise just up to the lookahead)

				file.SetIniString("Database", "DestinationsTableName", g_pbarbero->m_settings.m_pszDestinations);  // the Destinations table name
				file.SetIniString("Database", "QueueTableName", g_pbarbero->m_settings.m_pszQueue);  // the Queue table name
				file.SetIniString("Database", "DestinationMediaTableName", g_pbarbero->m_settings.m_pszDestinationMedia);  // the DestinationMedia table name
				file.SetIniString("Database", "DestinationMediaTempTableName", g_pbarbero->m_settings.m_pszDestinationMediaTemp );  // the Destinations table name

				file.SetIniInt("Database", "ModificationCheckInterval", g_pbarbero->m_settings.m_ulModsIntervalMS);  // in milliseconds

				file.SetSettings(BARBERO_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

			}
*/
			return BARBERO_SUCCESS;
		}
	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s%s%s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. ",
				m_pdb?"":"Database pointer was NULL. ",
				m_pdbConn?"":"Connection pointer was NULL. ",
				((m_pdbConn)&&(!m_pdbConn->m_bConnected))?"Database not connected. ":""
				);
		}
	}
	return BARBERO_ERROR;
}

