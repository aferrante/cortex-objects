// BarberoDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(BARBERODEFINES_H_INCLUDED)
#define BARBERODEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define BARBERO_CURRENT_VERSION		"1.2.0.7"


// modes
#define BARBERO_MODE_DEFAULT			0x00000000  // exclusive
#define BARBERO_MODE_LISTENER		0x00000001  // exclusive
#define BARBERO_MODE_CLONE				0x00000002  // exclusive
#define BARBERO_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define BARBERO_MODE_VOLATILE		0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define BARBERO_MODE_MASK				0x0000000f  // 

// default port values.
//#define BARBERO_PORT_FILE				80		
#define BARBERO_PORT_CMD					10680		
#define BARBERO_PORT_STATUS			10681		

#define BARBERO_PORT_INVALID			0	

#define BARBERO_FLAG_DISABLED		0x0000	 // default
#define BARBERO_FLAG_ENABLED			0x0001	
#define BARBERO_FLAG_FOUND				0x1000
#define BARBERO_FLAG_NEW					0x0100
#define BARBERO_FLAG_REFRESH			0x0200

#define BARBERO_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded


// status
#define BARBERO_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define BARBERO_STATUS_UNKNOWN							0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define BARBERO_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define BARBERO_STATUS_ERROR								0x00000020  // error (red icon)
#define BARBERO_STATUS_CONN								0x00000030  // ready (green icon)	
#define BARBERO_STATUS_OK									0x00000030  // ready (green icon)	
#define BARBERO_STATUS_RUN									0x00000040  // in progress, running, owned etc (blue icon);	
#define BARBERO_ICON_MASK									0x00000070  // mask	

#define BARBERO_STATUS_SUSPEND							0x00000080  // suspended	(yellow icon please)

#define BARBERO_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define BARBERO_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define BARBERO_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define BARBERO_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define BARBERO_STATUS_CMDSVR_MASK					0x00007000  // command server mask bits

#define BARBERO_STATUS_STATUSSVR_START			0x00010000  // starting the status server
#define BARBERO_STATUS_STATUSSVR_RUN				0x00020000  // status server running
#define BARBERO_STATUS_STATUSSVR_END				0x00030000  // status server shutting down
#define BARBERO_STATUS_STATUSSVR_ERROR			0x00040000  // status server error
#define BARBERO_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define BARBERO_STATUS_THREAD_START				0x00100000  // starting the main thread
#define BARBERO_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define BARBERO_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define BARBERO_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define BARBERO_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define BARBERO_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define BARBERO_STATUS_THREAD_MASK					0x00f00000  // main thread mask bits

// various failures...
#define BARBERO_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define BARBERO_STATUS_FAIL_DB							0x20000000  // could not get DB
#define BARBERO_STATUS_FAIL_MASK						0xf0000000  // failure code mask bits

//return values
#define BARBERO_SUCCESS   0
#define BARBERO_ERROR	   -1


// default filenames
#define BARBERO_SETTINGS_FILE_DEFAULT	  "barbero.csf"		// csf = cortex settings file



#endif // !defined(BARBERODEFINES_H_INCLUDED)
