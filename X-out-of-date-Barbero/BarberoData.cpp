// BarberoData.cpp: implementation of the CBarberoData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <process.h>
#include "Barbero.h"
#include "BarberoHandler.h" 
#include "BarberoMain.h" 
#include "BarberoData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//extern CIS2Comm g_miranda;  //global miranda object

extern CBarberoMain* g_pbarbero;
extern CBarberoApp theApp;
//extern CADC g_adc; 	// the Harris ADC object

extern void BarberoConnectionThread(void* pvArgs);


//////////////////////////////////////////////////////////////////////
// CBarberoDestinationObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBarberoDestinationObject::CBarberoDestinationObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
//	m_pszClientName = NULL;
	m_ulStatus	= BARBERO_STATUS_UNINIT;
	m_ulFlags = BARBERO_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
//	m_pAPIConn = NULL;

	m_dblDiskKBFree = -1.0;  
	m_dblDiskKBTotal = -1.0; 
	m_dblDiskPercent = 90.0; 
	m_nChannelID = -1;
	m_nItemID = -1;
	
	m_bKillConnThread = true;
	m_bConnThreadStarted = false;

}

CBarberoDestinationObject::~CBarberoDestinationObject()
{
	m_bKillConnThread = true;
//	while(	m_bConnThreadStarted ) Sleep(1);

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
//	if(m_pszClientName) free(m_pszClientName); // must use malloc to allocate
}


/*
//////////////////////////////////////////////////////////////////////
// CBarberoChannelObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBarberoChannelObject::CBarberoChannelObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_ulStatus	= BARBERO_STATUS_UNINIT;
	m_ulFlags = BARBERO_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
	m_nChannelID = -1; // unassigned
	m_nHarrisListID = -1; // unassigned
	m_pbKillConnThread = NULL;
	m_bKillChannelThread = true;
	m_bChannelThreadStarted = false;
	m_pAPIConn = NULL;
}

CBarberoChannelObject::~CBarberoChannelObject()
{
	m_bKillChannelThread = true;
	while(	m_bChannelThreadStarted ) Sleep(1);

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
}
*/



//////////////////////////////////////////////////////////////////////
// CBarberoData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBarberoData::CBarberoData()
{
	InitializeCriticalSection(&m_critText);

	m_ppDestObj = NULL;
	m_nNumDestinationObjects = 0;
//	m_ppChannelObj = NULL;
//	m_nNumChannelObjects = 0;

	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebTick );
	m_pszHost = NULL;			// the name of the host
	m_pszCompleteHost = NULL;	// the complete name of the host
	m_pszCortexHost = NULL;			// the name of the cortex host
	m_usCortexCommandPort = BARBERO_PORT_CMD;
	m_usCortexStatusPort = BARBERO_PORT_STATUS;
	m_nSettingsMod = -1;
//	m_nChannelsMod = -1;
	m_nDestinationsMod = -1;
	m_nQueueMod = -1;
	m_nLastSettingsMod = -1;
//	m_nLastChannelsMod = -1;
	m_nLastDestinationsMod = -1;
	m_nLastQueueMod = -1;
	m_pdb = NULL;
	m_pdbConn = NULL;
	m_bQuietKill = false;
	m_bProcessSuspended = false;
	m_nMaxLicensedDevices = 2;
}

CBarberoData::~CBarberoData()
{
	if(m_ppDestObj)
	{
		int i=0;
		while(i<m_nNumDestinationObjects)
		{
			if(m_ppDestObj[i]) delete m_ppDestObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppDestObj; // delete array of pointers to objects, must use new to allocate
	}
/*
	if(m_ppChannelObj)
	{
		int i=0;
		while(i<m_nNumChannelObjects)
		{
			if(m_ppChannelObj[i]) delete m_ppChannelObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppChannelObj; // delete array of pointers to objects, must use new to allocate
	}
*/
	if(m_pszHost) free(m_pszHost);	// must use malloc to allocate
	if(m_pszCompleteHost) free(m_pszCompleteHost);	// must use malloc to allocate
	if(m_pszCortexHost) free(m_pszCortexHost);	// must use malloc to allocate

	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);
}

char* CBarberoData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CBarberoData::SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError)
{
	int nRV = CX_ERROR;
	bool bError=false;

	if(!bOverwriteExistingError)
	{
		if(
			  ((m_ulFlags&BARBERO_ICON_MASK) == BARBERO_STATUS_ERROR)
			||((m_ulFlags&BARBERO_STATUS_CMDSVR_MASK) == BARBERO_STATUS_CMDSVR_ERROR)
			||((m_ulFlags&BARBERO_STATUS_STATUSSVR_MASK) ==  BARBERO_STATUS_STATUSSVR_ERROR)
			||((m_ulFlags&BARBERO_STATUS_THREAD_MASK) == BARBERO_STATUS_THREAD_ERROR)
			||((m_ulFlags&BARBERO_STATUS_FAIL_MASK) == BARBERO_STATUS_FAIL_DB)
			)	bError=true;
	}

	if (ulStatus&BARBERO_ICON_MASK)
	{
		m_ulFlags &= ~BARBERO_ICON_MASK;
		m_ulFlags |= (ulStatus&BARBERO_ICON_MASK);
	}
	if (ulStatus&BARBERO_STATUS_CMDSVR_MASK)
	{
		m_ulFlags &= ~BARBERO_STATUS_CMDSVR_MASK;
		m_ulFlags |= (ulStatus&BARBERO_STATUS_CMDSVR_MASK);
	}
	if (ulStatus&BARBERO_STATUS_STATUSSVR_MASK)
	{
		m_ulFlags &= ~BARBERO_STATUS_STATUSSVR_MASK;
		m_ulFlags |= (ulStatus&BARBERO_STATUS_STATUSSVR_MASK);
	}
	if (ulStatus&BARBERO_STATUS_THREAD_MASK)
	{
		m_ulFlags &= ~BARBERO_STATUS_THREAD_MASK;
		m_ulFlags |= (ulStatus&BARBERO_STATUS_THREAD_MASK);
	}
	if (ulStatus&BARBERO_STATUS_FAIL_MASK)
	{
		m_ulFlags &= ~BARBERO_STATUS_FAIL_MASK;
		m_ulFlags |= (ulStatus&BARBERO_STATUS_FAIL_MASK);
	}

	if(bError)
	{
		m_ulFlags &= ~BARBERO_ICON_MASK;
		m_ulFlags |= BARBERO_STATUS_ERROR;
	}

	if((pszText)&&(!bError))
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
//				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
//			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_pbarbero->m_settings.m_pszIconPath)&&(strlen(g_pbarbero->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_pbarbero->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_pbarbero->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_pbarbero->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_pbarbero->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_pbarbero->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}

/*
int	CBarberoData::SetStatusText(char* pszText, unsigned long ulStatus)
{
	int nRV = CX_ERROR;
	if(pszText)
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_pbarbero->m_settings.m_pszIconPath)&&(strlen(g_pbarbero->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_pbarbero->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_pbarbero->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_pbarbero->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_pbarbero->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_pbarbero->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}
*/
// utility
int	CBarberoData::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszCompleteHost) free(m_pszCompleteHost);
						m_pszCompleteHost=pch;

						pch = (char*) malloc(nValue+1);
						if(pch!=NULL)
						{
							if(m_pszHost) free(m_pszHost);
							m_pszHost=pch;

							strcpy(m_pszHost, m_pszCompleteHost);
							pch = strchr(m_pszHost, '.');
							if(pch)
							{
								*pch = 0;
							}

							return BARBERO_SUCCESS;
						}
					}
				}
			}
		}
	}
	return BARBERO_ERROR;
}

int CBarberoData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_pbarbero)&&(g_pbarbero->m_settings.m_pszExchange)&&(strlen(g_pbarbero->m_settings.m_pszExchange)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s set mod = \
(SELECT case when max(mod) is null then 1 when max(mod) >= %d \
then 1 else max(mod) + 1 end from %s WHERE criterion = '%s') WHERE criterion = '%s'",
			g_pbarbero->m_settings.m_pszExchange,
			BARBERO_DB_MOD_MAX,
			g_pbarbero->m_settings.m_pszExchange,
			szTemp, szTemp		
			);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			return BARBERO_SUCCESS;
		}
	}
	return BARBERO_ERROR;
}

int CBarberoData::CheckDatabaseMods(char* pszInfo)
{
	if((g_pbarbero)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_pbarbero->m_settings.m_pszExchange)&&(strlen(g_pbarbero->m_settings.m_pszExchange)))?g_pbarbero->m_settings.m_pszExchange:"Exchange");

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = BARBERO_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCriterion;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
				}

				if((g_pbarbero->m_settings.m_pszSettings)&&(strlen(g_pbarbero->m_settings.m_pszSettings)))
				{
					szTemp.Format("DBT_%s",g_pbarbero->m_settings.m_pszSettings);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nSettingsMod = nReturn;
					}
				}

/*
				if((g_pbarbero->m_settings.m_pszChannels)&&(strlen(g_pbarbero->m_settings.m_pszChannels)))
				{
					szTemp.Format("DBT_%s",g_pbarbero->m_settings.m_pszChannels);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nChannelsMod = nReturn;
					}
				}
*/
				if((g_pbarbero->m_settings.m_pszDestinations)&&(strlen(g_pbarbero->m_settings.m_pszDestinations)))
				{
					szTemp.Format("DBT_%s",g_pbarbero->m_settings.m_pszDestinations);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nDestinationsMod = nReturn;
					}
				}

				if((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))
				{
					szTemp.Format("DBT_%s",g_pbarbero->m_settings.m_pszQueue);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nQueueMod = nReturn;
					}
				}
				// get the suspend

				szTemp.Format("Suspend");
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					nReturn = atoi(szMod);
					if(nReturn>0) m_bProcessSuspended = true;
					else m_bProcessSuspended = false;
				}

				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			delete prs;
			return nReturn;
		}
	}
	return BARBERO_ERROR;
}


int CBarberoData::GetQueue(char* pszInfo)
{
	// following removed to deal with wait mode below.
/*
	if(g_miranda.m_bTransferring)
	{
		// something in progress, must wait.
		return BARBERO_ERROR;
	}
*/
	
	if((g_pbarbero)&&(m_pdbConn)&&(m_pdb))
	{

		// let's just get the top (oldest timestamp) item in the queue, and deal with it.

		_ftime( &g_pbarbero->m_data.m_timebTick );

		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s ORDER BY timestamp ASC", //HARDCODE
			((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue");

//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
//action int, host varchar(64), timestamp float, username varchar(32));

		CString szFilenameLocal = "";
		CString szFilenameRemote = "";
		CString szServer = "";
		CString szUsername = "";
		CString szMessage = "";
		CString szTemp = "";
		int nItemID = -1;
		bool bItemFound = false;
		double dblTimestamp=-57;   // timestamp
		int nAction = -1;
		int nTemp;
		int nEventItemID=-1;

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = BARBERO_ERROR;
			int nIndex = 0;
//			while ((!prs->IsEOF()))
			if ((!prs->IsEOF()))  // just do the one record, if there is one
			{
				try
				{
					prs->GetFieldValue("itemid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nItemID = atoi(szTemp);
						bItemFound = true;
					}
					prs->GetFieldValue("filename_local", szFilenameLocal);//HARDCODE
					szFilenameLocal.TrimLeft(); szFilenameLocal.TrimRight();
					prs->GetFieldValue("filename_remote", szFilenameRemote);//HARDCODE
					szFilenameRemote.TrimLeft(); szFilenameRemote.TrimRight();
					prs->GetFieldValue("action", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nAction = nTemp;
					}
					prs->GetFieldValue("host", szServer);//HARDCODE
					szServer.TrimLeft(); szServer.TrimRight();

					prs->GetFieldValue("timestamp", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						dblTimestamp = atof(szTemp);
					}
					prs->GetFieldValue("username", szUsername);//HARDCODE
					szUsername.TrimLeft(); szUsername.TrimRight();
					prs->GetFieldValue("event_itemid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nEventItemID = nTemp;
					}

					prs->GetFieldValue("message", szMessage);//HARDCODE
					
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;

//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "got %d things from queue", nIndex);   Sleep(250);//(Dispatch message)

			if((nReturn>0)&&(bItemFound))
			{
				// process the request, then remove it from the queue
//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "got item %d from queue", nItemID);   Sleep(250);//(Dispatch message)
		_ftime( &g_pbarbero->m_data.m_timebTick );

				if(szServer.GetLength()>0)
				{
					char* pchServer = szServer.GetBuffer(1);
					int nDestIndex = ReturnDestinationIndex(pchServer);
//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "got server %s from queue", pchServer);   Sleep(250);//(Dispatch message)

					if((nDestIndex>=0)&&(m_ppDestObj)&&(m_ppDestObj[nDestIndex])&&(m_ppDestObj[nDestIndex]->m_ulFlags&BARBERO_FLAG_ENABLED))
					{  
						// only do anything if its enabled.
//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "enabled");   Sleep(250);//(Dispatch message)

						int nHostReturn = BARBERO_ERROR;

////////////////MIRANDA to replace
/*

						if((g_miranda.m_pszHost)&&(strlen(g_miranda.m_pszHost))&&(strcmp(g_miranda.m_pszHost,pchServer)==0))
						{
							// no need to do anything.
							nHostReturn = BARBERO_SUCCESS;
						}
						else
						{
							nHostReturn = g_miranda.SetHost(pchServer);
						}

						if(nHostReturn>=BARBERO_SUCCESS)
						{

		_ftime( &g_pbarbero->m_data.m_timebTick );
							nHostReturn = g_miranda.OxSoxPing();
							//ping the host first.
		_ftime( &g_pbarbero->m_data.m_timebTick );
							if(nHostReturn == OX_SUCCESS)
							{
								char pchDirAlias[50];
								int nDirIndex = szFilenameRemote.Find("/");

							// first thing is, get the disk space statistics.

								BOOL bReturn;
								
								if(g_miranda.m_lpfnGetDriveInfo)
								{ 
									if(nDirIndex>0)
									{
										sprintf(pchDirAlias, "%s", szFilenameRemote.Left(nDirIndex));
										if(pchDirAlias[0]!='$') // error
											strcpy(pchDirAlias, "$VIDEO"); // default.
									}
									else
									{
										strcpy(pchDirAlias, "$VIDEO"); // default.
									}

									DiskInfo_ diskinfo;

									try
									{
										bReturn = g_miranda.m_lpfnGetDriveInfo(g_miranda.m_pszHost, pchDirAlias, &diskinfo);
									}
									catch (...)
									{
										bReturn = FALSE;
									}
							//		AfxMessageBox("Y");
									if(bReturn == FALSE)
									{
										//**MSG
						//				g_pbarbero->m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:oxsox_diskinfo", "Could not get disk information for %s partition on %s", pchDirAlias, pchServer );   Sleep(50);//(Dispatch message)
	/*
	g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "m_lpfnGetDriveInfo returned FALSE");   Sleep(50);//(Dispatch message)
										g_pbarbero->m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:oxsox_diskinfo", "Could not get disk information for %s partition", pchDirAlias );   Sleep(50);//(Dispatch message)
	g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "m_lpfnGetDriveInfo returned2 FALSE");   Sleep(50);//(Dispatch message)
* /
										g_pbarbero->m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:oxsox_diskinfo", "Could not get disk information for %s", pchServer );   Sleep(50);//(Dispatch message)
//	g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "m_lpfnGetDriveInfo returned3 FALSE");   Sleep(50);//(Dispatch message)

									}
									else
									{
										if(nDestIndex>=0)
										{
											//its a host in our list!
											if((m_ppDestObj)&&(m_ppDestObj[nDestIndex]))
											{
												m_ppDestObj[nDestIndex]->m_dblDiskKBFree = (double)diskinfo.KBytes_Free;  
												m_ppDestObj[nDestIndex]->m_dblDiskKBTotal = (double)diskinfo.KBytes_Total;  
												// ok, now update DB.  but do not increment counter, that will cause infinite loop.

												if(m_ppDestObj[nDestIndex]->m_nItemID>0) // last known unique item id
												{

	// create table Destinations (destinationid int identity(1,1) NOT NULL, host varchar(64) NOT NULL,
	//				type int, description varchar(256), diskspace_free float, diskspace_total float, diskspace_threshold real,
	//				channelid int, flags int);

													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET diskspace_free = %f, diskspace_total = %f WHERE destinationid = %d",  //HARDCODE
														((g_pbarbero->m_settings.m_pszDestinations)&&(strlen(g_pbarbero->m_settings.m_pszDestinations)))?g_pbarbero->m_settings.m_pszDestinations:"Destinations",
														m_ppDestObj[nDestIndex]->m_dblDiskKBFree,
														m_ppDestObj[nDestIndex]->m_dblDiskKBTotal,
														m_ppDestObj[nDestIndex]->m_nItemID);
													if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
													{
														//**MSG
									//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

													}
												}
											}
										}
									}
	//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "after m_lpfnGetDriveInfo ");   Sleep(50);//(Dispatch message)

								}
								else
								{
									//**MSG
									g_pbarbero->m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:oxsox_diskinfo", "Could not get disk information for %s partition on %s; NULL function address.", pchDirAlias, pchServer );   Sleep(50);//(Dispatch message)
	//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "m_lpfnGetDriveInfo was NULL");   Sleep(50);//(Dispatch message)
								}

	//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "file local %s, file remote %s, action %d", szFilenameLocal, szFilenameRemote, nAction);  Sleep(250); //(Dispatch message)

								char dberrorstring[DB_ERRORSTRING_LEN];
	//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "switching with %d", nAction);   Sleep(50);//(Dispatch message)
								switch(nAction)
								{
								case 64:// check file on Miranda
									{
										char pchFileRemote[MAX_PATH+1];
										_snprintf(pchFileRemote, MAX_PATH, "%s", szFilenameRemote);
	//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "*** checking %s on %s", pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)
										unsigned nSize, nTimestamp;

										int nReturn = g_miranda.OxSoxGetFileStats(pchFileRemote, NULL, &nSize, &nTimestamp);
										if((nReturn<OX_SUCCESS)||(nReturn&OX_NOFILE))
										{
											//**MSG
											g_pbarbero->m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:oxsox_checkfile", 
												"%s not found on %s.", pchFileRemote, pchServer );   Sleep(50);//(Dispatch message)

											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E064:%s not found on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d;",  //HARDCODE
												((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
												pchFileRemote, pchServer, nItemID
												);
										}
										else
										{
											//while(!g_miranda.m_bTransferring) Sleep(1);  //not good, in case error

	//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "%s on %s succeeded", pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)
											// we have queued the xfer , lets update the thing with a status, then exit without removing
											
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I064:%s exists on %s.', filename_local = '%d|%d', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
												((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
												pchFileRemote, pchServer, nSize, nTimestamp, nItemID
												);

						/*
						Queue action IDs
						1 transfer to Miranda
						2 transfer from Miranda
						3 delete from Miranda
						4 refresh device listing
						19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
						32 wait on transfer
						64 check file on Miranda
						128 Give results (no action).

	//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
	//action int, host varchar(64), timestamp float, username varchar(32));
						* /
										}
											
	//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
										{
											//**MSG
							//		g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

										}

										szServer.ReleaseBuffer();
										return BARBERO_ERROR; // we dont want to remove the item at the end of this function
										

									} break;
								case 128:// Give results (no action).
									{
										szServer.ReleaseBuffer();
										return BARBERO_ERROR; // we dont want to remove the item at the end of this function
										// the requestor process will remove this.
									} break;

								case 1:// transfer to Miranda
									{
										if(g_miranda.m_bTransferring) // would like to update queue item with % status.
										{
											// something in progress, must wait. -  just return negative for now
											szServer.ReleaseBuffer();
											return BARBERO_ERROR;
										}

										char pchFileLocal[MAX_PATH+1];
										char pchFileRemote[MAX_PATH+1];
										_snprintf(pchFileLocal, MAX_PATH, "%s", szFilenameLocal);
										_snprintf(pchFileRemote, MAX_PATH, "%s", szFilenameRemote);
	//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "*** transferring %s to %s on %s", pchFileLocal, pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)

										if(g_miranda.OxSoxPutFile(pchFileLocal, pchFileRemote, NULL)<OX_SUCCESS)
										{
											//**MSG
											g_pbarbero->m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:oxsox_putfile", 
												"Could not transfer %s to %s on %s.", pchFileLocal, pchFileRemote, pchServer );   Sleep(50);//(Dispatch message)

											// and give the info if it was external.
											if(nEventItemID>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E001:Could not transfer %s to %s on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
														((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
														pchFileLocal, pchFileRemote, pchServer,
														nItemID
														);
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
				//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
												return BARBERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function
											}
										}
										else
										{
											//while(!g_miranda.m_bTransferring) Sleep(1);  //not good, in case error

											Sleep(100); // let the thread begin transferring.
	//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "*** transfer of %s to %s on %s succeeded", pchFileLocal, pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)
											// we have queued the xfer , lets update the thing with a status, then exit without removing
											
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET action = 32, timestamp = -1.0, message = '1' WHERE itemid = %d",  //HARDCODE
												((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
												nItemID
												);

						/*
						Queue action IDs
						1 transfer to Miranda
						2 transfer from Miranda
						3 delete from Miranda
						4 refresh device listing
						19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
						32 wait on transfer
						64 check file on Miranda
						128 Give results (no action).

	//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
	//action int, host varchar(64), timestamp float, username varchar(32));
						* /

											
	//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
											if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
											{
												//**MSG
	//									g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

											}

											szServer.ReleaseBuffer();
											return BARBERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
											// we also dont want to remove the item at the end of this function
										}
									} break;
								case 2:// transfer from Miranda
									{
										if(g_miranda.m_bTransferring) // would like to update queue item with % status.
										{
											// something in progress, must wait. -  just return negative for now
											szServer.ReleaseBuffer();
											return BARBERO_ERROR;
										}
										char pchFileLocal[MAX_PATH+1];
										char pchFileRemote[MAX_PATH+1];
										_snprintf(pchFileLocal, MAX_PATH, "%s", szFilenameLocal);
										_snprintf(pchFileRemote, MAX_PATH, "%s", szFilenameRemote);

										if(g_miranda.OxSoxGetFile(pchFileLocal, pchFileRemote, NULL/*, OX_REPORT* /)<OX_SUCCESS)
										{
											//**MSG
											g_pbarbero->m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:oxsox_getfile", 
												"Could not transfer %s on %s to %s.", pchFileRemote, pchServer, pchFileLocal );   Sleep(50);//(Dispatch message)

											// and give the info if it was external.
											if(nEventItemID>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E002:Could not transfer %s on %s to %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
														((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
														pchFileRemote, pchServer, pchFileLocal,
														nItemID
														);
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
				//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
												return BARBERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function
											}

										}
										else
										{
											//while(!g_miranda.m_bTransferring) Sleep(1);  //not good, in case error

											Sleep(100); // let the thread begin transferring.
											// we have queued the xfer , lets update the thing with a status, then exit without removing
											
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET action = 32, timestamp = -1.0, message = '2' WHERE itemid = %d",  //HARDCODE
												((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
												nItemID
												);

						/*
						Queue action IDs
						1 transfer to Miranda
						2 transfer from Miranda
						3 delete from Miranda
						4 refresh device listing
						19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
						32 wait on transfer

	//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
	//action int, host varchar(64), timestamp float, username varchar(32));
						* /

											if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
											{
												//**MSG
							//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

											}

											szServer.ReleaseBuffer();
											return BARBERO_ERROR;  // because we dont want to increment counter. // there is no counter.
											// we also dont want to remove the item at the end of this function
										}
									} break;
								case 3:// delete from Miranda
									{
										if(g_miranda.m_bTransferring) // would like to update queue item with % status.
										{
											// something in progress, must wait. -  just return negative for now
											szServer.ReleaseBuffer();
											return BARBERO_ERROR;
										}
										char pchFileRemote[MAX_PATH+1];
										_snprintf(pchFileRemote, MAX_PATH, "%s", szFilenameRemote);
										if(g_miranda.OxSoxDelFile(pchFileRemote, NULL)<OX_SUCCESS)
										{
											//**MSG
											g_pbarbero->m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:oxsox_delfile", 
												"Could not delete %s on %s.", pchFileRemote, pchServer );   Sleep(50);//(Dispatch message)
											if(nEventItemID>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E003:Could not delete %s on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
														((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
														szFilenameRemote,
														pchServer, nItemID
														);
		//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
									//		g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
												return BARBERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function

											}

										}
										else


		//								schedule a refresh
										if((g_pbarbero->m_data.m_ppDestObj[nDestIndex]->m_pszServerName)&&(strlen(g_pbarbero->m_data.m_ppDestObj[nDestIndex]->m_pszServerName)) && ((g_pbarbero->m_data.m_ppDestObj[nDestIndex]->m_ulFlags)&BARBERO_FLAG_ENABLED)  )
										{
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (filename_local, filename_remote, action, host, timestamp, username) \
VALUES ('','%s', 4, '%s', -2.0, 'sys' )", //HARDCODE
											((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
											pchDirAlias, g_pbarbero->m_data.m_ppDestObj[nDestIndex]->m_pszServerName
											);


			//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", szSQL);  Sleep(250); //(Dispatch message)
			//					char errorstring[DB_ERRORSTRING_LEN];
			//								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
											if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
											{
												//**MSG
			//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

											}

											// and give the info if it was external.
											if(nEventItemID>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I003:Finished deleting %s on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
														((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
														szFilenameRemote, 
														pchServer, nItemID
														);
												
		//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
									//		g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
												return BARBERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function

											}
										}


									} break;
								case 32:// wait on transfer
									{

										if(g_miranda.m_bTransferring) // would like to update queue item with % status.
										{
											// something in progress, must wait. -  just return negative for now
											szServer.ReleaseBuffer();
											return BARBERO_ERROR;
										}

										// if we are here, it's because we have finished a transfer!
		//								schedule a refresh
										if((g_pbarbero->m_data.m_ppDestObj[nDestIndex]->m_pszServerName)&&(strlen(g_pbarbero->m_data.m_ppDestObj[nDestIndex]->m_pszServerName)) && ((g_pbarbero->m_data.m_ppDestObj[nDestIndex]->m_ulFlags)&BARBERO_FLAG_ENABLED)  )
										{
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (filename_local, filename_remote, action, host, timestamp, username) \
VALUES ('','%s', 4, '%s', -2.0, 'sys' )", //HARDCODE
											((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
											pchDirAlias, g_pbarbero->m_data.m_ppDestObj[nDestIndex]->m_pszServerName
											);


			//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", szSQL);  Sleep(250); //(Dispatch message)
			//					char errorstring[DB_ERRORSTRING_LEN];
			//								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
											if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
											{
												//**MSG
			//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

											}

											// and give the info if it was external.
											if(nEventItemID>0)
											{
												int nDirection = atoi(szMessage);
	//						case 2:// transfer from Miranda
	//						case 1:// transfer to Miranda
												if(g_miranda.m_bTransferError) //error!.
												{
													g_miranda.m_bTransferError = false; // reset the error so we can move on
													if(nDirection>0)
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E032:Error transferring %s to %s%s%s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
															(nDirection==1)?szFilenameLocal:szFilenameRemote,
															(nDirection==1)?szFilenameRemote:szFilenameLocal,
															(nDirection==1)?" on ":" from ",
															pchServer, nItemID
															);
													}
													else
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E032:Error transferring %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
															szFilenameRemote,
															pchServer, nItemID
															);

													}
												}
												else
												{
													if(nDirection>0)
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I032:Finished transferring %s to %s%s%s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
															(nDirection==1)?szFilenameLocal:szFilenameRemote,
															(nDirection==1)?szFilenameRemote:szFilenameLocal,
															(nDirection==1)?" on ":" from ",
															pchServer, nItemID
															);
													}
													else
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I032:Finished transferring %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
															szFilenameRemote,
															pchServer, nItemID
															);

													}
												}

	//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "Transfer SQL: %s", szSQL);  Sleep(250); //(Dispatch message)
	//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
	//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
												return BARBERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function

											}
										}

									} break;
								case 19:// purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
									{
									}  // not break;  want to go on to refresh device listing.
								case 4:// refresh device listing
									{
	//create table Destinations_Media (host varchar(64), file_name varchar(256), 
	//transfer_date int, partition varchar(16), file_size float, 
	//Direct_local_last_used int, Direct_local_times_used int);

										// have to do this 3 times, possibly.
	//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "case 4");   Sleep(50);//(Dispatch message)
										int nTimes = 3;
										int nDirs = 0;


										if(szFilenameRemote.GetLength()>0)
										{ // explicit dir
											if(szFilenameRemote.Compare("$VIDEO")==0)
											{
												nDirs = 0; nTimes=1;
											}
											else
											if(szFilenameRemote.Compare("$AUDIO")==0)
											{
												nDirs = 1; nTimes=2; // not really 2 times, just have to be nDirs = nTimes-1;
											}
											else
											if(szFilenameRemote.Compare("$FONTS")==0)
											{
												nDirs = 2; nTimes=3; // not really 3 times, just have to be nDirs = nTimes-1;
											}
										}

										for (int nDir=nDirs; nDir<nTimes; nDir++)
										{
		_ftime( &g_pbarbero->m_data.m_timebTick );
											PV3DirEntry_ pv3DirEntry=NULL;
											int nNumEntries = 0;

											switch(nDir)
											{
											default:
											case 0:	strcpy(pchDirAlias, "$VIDEO"); break;
											case 1:	strcpy(pchDirAlias, "$AUDIO"); break;
											case 2:	strcpy(pchDirAlias, "$FONTS"); break;
											}
											
											bool bDelTemp = true;
											if(g_miranda.OxSoxGetDirInfo(pchDirAlias, &pv3DirEntry, &nNumEntries)<OX_SUCCESS)
											{
												//**MSG
												g_pbarbero->m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:oxsox_dirinfo", 
													"Could not get directory information for %s partition on %s.", pchDirAlias, pchServer );   Sleep(50);//(Dispatch message)

												// and give the info if it was external.
												if(nEventItemID>0)
												{
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E004:Could not get directory information for %s partition on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
															pchDirAlias, pchServer,
															nItemID
															);
													if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
													{
														//**MSG
	//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

													}
													szServer.ReleaseBuffer();
													return BARBERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
													// we also dont want to remove the item at the end of this function
												}
												bDelTemp = false;  // forces exit on error
											}
											else
											{
												// success, lets update db.
	//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "OxSoxGetDirInfo returned >= OX_SUCCESS" );   Sleep(50);//(Dispatch message)


	// the old way was m_pszDestinationMedia.  now we merge on temp table m_pszDestinationMediaTemp
												// first remove everything from the db.  clear out temp db
												/*
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
host = '%s' AND \
partition = '%s'",
														((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														pchServer,
														pchDirAlias

													);
												* /
												// first remove everything from the db.  clear out temp db  
												// And we really mean everything.  no where clause this time.
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
														((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp"
													);
												if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
												{
													//**MSG
													bDelTemp = false;

													g_pbarbero->m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:oxsox_dirinfo", 
														"Could not get directory information for %s partition on %s.", pchDirAlias, pchServer );   Sleep(50);//(Dispatch message)

													// and give the info if it was external.
													if(nEventItemID>0)
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E004:Could not get directory information for %s partition on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
																((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
																pchDirAlias, pchServer,
																nItemID
																);
														if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
														{
															//**MSG
		//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

														}
														szServer.ReleaseBuffer();
														return BARBERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
														// we also dont want to remove the item at the end of this function
													}

												}


												int nFile = 0;
												while((nFile<nNumEntries)&&(bDelTemp))  // have to fail out if the temp table wasn't cleared.
												{
			_ftime( &g_pbarbero->m_data.m_timebTick );

													char* pchFilename = m_pdb->EncodeQuotes(pv3DirEntry[nFile].FileName);
	/*
	// don't check for existence, we already removed everything just to be sure.

												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s WHERE \
	host = '%s' AND \
	file_name = '%s' AND \
	partition = '%s';",
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															pchServer,
															pchFilename?pchFilename:pv3DirEntry[nFile].FileName,											
															pchDirAlias

														);

													// get
													CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, dberrorstring);
													int nNumFound = 0;
													int nID = -1;
													if(prs != NULL) 
													{
														if(!prs->IsEOF())
														{  // all we really need to know is that one record has been returned.
				/*
															CString szValue;
															try
															{
																// get id!
																prs->GetFieldValue("itemid", szValue);
																nID = atoi(szValue);
															}
															catch( ... )
															{
															}

				* / // keep this one open

															nNumFound++;
															prs->MoveNext();
														}
														prs->Close();
														delete prs;
													}

													if((nNumFound>0)&&(nID>0))
													{
	//create table Destinations_Media (host varchar(64), file_name varchar(256), 
	//transfer_date int, partition varchar(16), file_size float, 
	//Direct_local_last_used int, Direct_local_times_used int);

														// if found update it with new values
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
	transfer_date = %d, \
	file_size = %f, \
	WHERE host = '%s' AND \
	file_name = '%s' AND \
	partition = '%s';", //HARDCODE
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															pv3DirEntry[nFile].FileTimeStamp,
															(double)pv3DirEntry[nFile].FileSize,
															pchServer,
															pchFilename?pchFilename:pv3DirEntry[nFile].FileName,											
															pchDirAlias
														);

													}
													else
													{
													* /
														// add it.
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
host, \
file_name, \
transfer_date, \
partition, \
file_size) VALUES ('%s', '%s', %d, '%s', %f)",   //HARDCODE
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															pchServer,
															pchFilename?pchFilename:pv3DirEntry[nFile].FileName,											
															pv3DirEntry[nFile].FileTimeStamp,
															pchDirAlias,
															(double)pv3DirEntry[nFile].FileSize
															);

													// }

													if(pchFilename) free(pchFilename);

													if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
													{
														//**MSG
													}

		/*
													pv3DirEntry[nFile].FileName
													pv3DirEntry[nFile].FileSize,
													pv3DirEntry[nFile].FileAttribs,
													pv3DirEntry[nFile].FileTimeStamp
		* /
													nFile++;
												}

												if((pv3DirEntry)&&(g_miranda.m_lpfnFreeOxSoxPtr)) // only do if pointer is not null
												{
													try
													{
														g_miranda.m_lpfnFreeOxSoxPtr(pv3DirEntry);
													}
													catch(...)
													{
													}
												}

												// now everything is in the temp table,
												// update the files with info from the temp table

												if(bDelTemp)
												{

													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
%s.transfer_date = %s.transfer_date, \
%s.file_size = %s.file_size \
FROM %s, %s \
WHERE %s.host = '%s' AND \
%s.file_name = %s.file_name \
AND %s.partition = %s.partition",
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															pchServer,
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp"
															);

	//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "Updating from temp SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
													if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
													{
														//**MSG
	//g_pbarbero->m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:debug", "Updating from temp ERROR: %s", dberrorstring);  Sleep(50); //(Dispatch message)
													}


													// insert the ones that are in the temp list that are not in the main media list.
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (host, file_name, transfer_date, partition, file_size, direct_local_last_used, direct_local_times_used) \
SELECT %s.host, %s.file_name, %s.transfer_date, %s.partition, %s.file_size, 0, 0 FROM \
%s LEFT JOIN (SELECT * FROM %s WHERE host = '%s') AS %s on %s.file_name = %s.file_name and %s.partition = %s.partition WHERE %s.file_name is NULL",
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															pchServer,
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media"
															);

	//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "Updating from temp SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
													if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
													{
														//**MSG
	//g_pbarbero->m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:debug", "Updating from temp ERROR: %s", dberrorstring);  Sleep(50); //(Dispatch message)
													}


													// remove the ones that are no longer in the temp list
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
%s.host = '%s' AND \
%s.partition = '%s' AND \
cast(%s.file_name AS varchar(256)) + ' ' + cast(%s.partition AS varchar(16)) \
NOT IN (SELECT cast(%s.file_name as varchar(256)) + ' ' + cast(%s.partition AS varchar(16)) \
AS fileinfo from %s where host = '%s' and %s.partition = '%s')",
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															pchServer,
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															pchDirAlias,
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pbarbero->m_settings.m_pszDestinationMedia)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMedia)))?g_pbarbero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															pchServer,
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															pchDirAlias
															);


	//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "Delete extras from media SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
													if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
													{
														//**MSG
	//g_pbarbero->m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:debug", "Delete extras from media ERROR: %s", dberrorstring);  Sleep(50); //(Dispatch message)
													}


												}
		// clear the temp table
/*
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
host = '%s' AND \
partition = '%s'",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															pchServer,
															pchDirAlias

														);
														* /

												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
															((g_pbarbero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pbarbero->m_settings.m_pszDestinationMediaTemp)))?g_pbarbero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp"
														);
												if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
												{
													//**MSG
												}

												// and give the info if it was external.
												//if bDelTemp was false, it gave the info already above and returned.  so it must have succeeded.

												if(nEventItemID>0)
												{
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I004:Retrieved directory information for %s partition on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
															pchDirAlias, pchServer,
															nItemID
															);
													if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
													{
														//**MSG
					//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

													}
													szServer.ReleaseBuffer();
													return BARBERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
													// we also dont want to remove the item at the end of this function

												}
											}
										}
									} break;
								}
								_ftime( &g_pbarbero->m_data.m_timebTick );
							}
							else  // could not ping it for some reason.  Have to error out and delete the thing thing
							{

								g_pbarbero->m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:oxsox_ping", 
									"Could not ping %s.  The request cannot be completed.", pchServer );   Sleep(50);//(Dispatch message)

								// and give the info if it was external.
								if(nEventItemID>0)
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E%03d:Could not ping %s.  The request cannot be completed.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
											((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
											nAction, pchServer,
											nItemID
											);
									if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
									{
										//**MSG
	//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

									}
									//return BARBERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
									// we DO want to remove the item at the end of this function
								}
							}

						}
						else
						{
							//**MSG
							g_pbarbero->m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:oxsox_host", "Could not set host to %s", pchServer );   Sleep(50);//(Dispatch message)
							szServer.ReleaseBuffer();
							return BARBERO_ERROR; // we dont want to remove the item at the end of this function

	//	g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "getting channels again");   Sleep(250);//(Dispatch message)
						}
*/
////////////////MIRANDA to replace


					}
					else
					{
						//**MSG

						switch(nAction)
						{
						default:
							{
								g_pbarbero->m_msgr.DM(MSG_ICONINFO, NULL, "Barbero:Queue", "An event was queued for %s, but that destination is unavailable or inactive.  The event will be ignored and purged from the queue.", pchServer );   Sleep(50);//(Dispatch message)
							} break;  
						case 2:// transfer from Miranda
						case 1:// transfer to Miranda
						case 3:// DELETE FROM Miranda
						case 64:// check file on Miranda
							{
								g_pbarbero->m_msgr.DM(MSG_ICONINFO, NULL, "Barbero:Queue", "An event was queued for %s, but that destination is unavailable or inactive.  The event will be ignored and purged from the queue.", pchServer );   Sleep(50);//(Dispatch message)

								if(nEventItemID>0) // means, commanded from external, so give info
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E%03d:%s is unavailable or inactive.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
											((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
											nAction, pchServer,
											nItemID
											);
	//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
									if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
									{
										//**MSG
						//		g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

									}
									szServer.ReleaseBuffer();
									return BARBERO_ERROR; // we dont want to remove the item at the end of this function
								}
							} break;
						}
					}

					szServer.ReleaseBuffer();
				}


				// send remove SQL
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE itemid = %d",  //HARDCODE
					((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
					nItemID
					);

/*
Queue action IDs
1 transfer to Miranda
2 transfer from Miranda
3 delete from Miranda
4 refresh device listing
19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
32 wait on transfer

Queue table:
//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
//action int, host varchar(64), timestamp float, username varchar(32));
*/

				if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
				{
					//**MSG
//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

				}
			}

			return nReturn;
		}
	}
	return BARBERO_ERROR;
}

int CBarberoData::ReturnDestinationIndex(char* pszServerName)
{
	if((pszServerName)&&(strlen(pszServerName))&&(g_pbarbero)&&(g_pbarbero->m_data.m_ppDestObj)&&(g_pbarbero->m_data.m_nNumDestinationObjects))
	{
		int i=0;
		while(i<g_pbarbero->m_data.m_nNumDestinationObjects)
		{
			if((g_pbarbero->m_data.m_ppDestObj[i])&&(g_pbarbero->m_data.m_ppDestObj[i]->m_pszServerName))
			{
				if(strcmp(pszServerName, g_pbarbero->m_data.m_ppDestObj[i]->m_pszServerName)==0) return i;
			}
			i++;
		}
	}
	return BARBERO_ERROR;
}

int CBarberoData::GetDestinations(char* pszInfo)
{
	if((g_pbarbero)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

//select * from destinations as destinations join 
//(select distinct top 2 host from destinations order by host) as top_destinations 
//on destinations.host = top_destinations.host order by flags destinations.desc, destinations.channelid, destinations.host 

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s AS destinations JOIN \
(SELECT DISTINCT TOP %d host FROM %s ORDER BY host) AS top_destinations ON destinations.host = top_destinations.host \
ORDER BY destinations.flags DESC, destinations.channelid, destinations.host",  //HARDCODE
			((g_pbarbero->m_settings.m_pszDestinations)&&(strlen(g_pbarbero->m_settings.m_pszDestinations)))?g_pbarbero->m_settings.m_pszDestinations:"Destinations",
			g_pbarbero->m_data.m_nMaxLicensedDevices,
			((g_pbarbero->m_settings.m_pszDestinations)&&(strlen(g_pbarbero->m_settings.m_pszDestinations)))?g_pbarbero->m_settings.m_pszDestinations:"Destinations"
			);
	
//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "GetDestinations getting %d dests", g_pbarbero->m_data.m_nMaxLicensedDevices);  Sleep(250); //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "Got Destinations");  Sleep(250); //(Dispatch message)

			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = BARBERO_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{

// create table Destinations (destinationid int identity(1,1) NOT NULL, host varchar(64) NOT NULL,
//				type int, description varchar(256), diskspace_free float, diskspace_total float, diskspace_threshold real,
//				channelid int, flags int);

		_ftime( &g_pbarbero->m_data.m_timebTick );

				CString szHost="";
//				CString szClient="";
				CString szDesc="";
				CString szTemp;
//				double dblDiskspaceFree=-1.0;
//				double dblDiskspaceTotal=-1.0;
				double dblDiskspaceThreshold=-1.0;
				unsigned long ulFlags;   // various flags
				unsigned short usType;
				int nChannelid=-1;
				int nTemp = -1;
				int nItemID = -1;
				bool bItemFound = false;
				bool bFlagsFound = false;
				bool bFound = false;
				try
				{
					prs->GetFieldValue("destinationid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bItemFound = true;
						nItemID = atoi(szTemp);
					}
					prs->GetFieldValue("host", szHost);//HARDCODE
					szHost.TrimLeft(); szHost.TrimRight();
					prs->GetFieldValue("type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						usType = (unsigned short)atol(szTemp);
					}
					prs->GetFieldValue("description", szDesc);//HARDCODE
					prs->GetFieldValue("diskspace_threshold", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						dblDiskspaceThreshold = atof(szTemp);
					}
					prs->GetFieldValue("channelid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nChannelid = atoi(szTemp);
					}
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bFlagsFound = true;
						ulFlags = atol(szTemp);
					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}
//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "Got Destination: %s at %s (0x%08x)", szDesc, szHost, ulFlags);  Sleep(250); //(Dispatch message)

				if((g_pbarbero->m_data.m_ppDestObj)&&(g_pbarbero->m_data.m_nNumDestinationObjects))
				{
					nTemp=0;
					while(nTemp<g_pbarbero->m_data.m_nNumDestinationObjects)
					{
//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "c2");  Sleep(250); //(Dispatch message)
						if((g_pbarbero->m_data.m_ppDestObj[nTemp])&&(g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszServerName))
						{
							if((szHost.GetLength()>0)&&(szHost.CompareNoCase(g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszServerName)==0)&&(nItemID==g_pbarbero->m_data.m_ppDestObj[nTemp]->m_nItemID))
							{
								bFound = true;
								bool bRefresh = false;

								if(
										(szDesc.GetLength()>0)
									&&((g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszDesc==NULL)||(szDesc.CompareNoCase(g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszDesc)))
									)
								{
									if(g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszDesc) free(g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszDesc);

									g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszDesc) sprintf(g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszDesc, szDesc);
								}

								if(bItemFound) g_pbarbero->m_data.m_ppDestObj[nTemp]->m_nItemID = nItemID;
								g_pbarbero->m_data.m_ppDestObj[nTemp]->m_usType = usType;

								if(dblDiskspaceThreshold>0.0) g_pbarbero->m_data.m_ppDestObj[nTemp]->m_dblDiskPercent = dblDiskspaceThreshold;
//								g_pbarbero->m_data.m_ppDestObj[nTemp]->m_dblDiskKBFree = dblDiskspaceFree;
//								g_pbarbero->m_data.m_ppDestObj[nTemp]->m_dblDiskKBTotal = dblDiskspaceTotal;

//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "c3");  Sleep(250); //(Dispatch message)

								if(
										((bFlagsFound)&&(!(ulFlags&BARBERO_FLAG_ENABLED))&&((g_pbarbero->m_data.m_ppDestObj[nTemp]->m_ulFlags)&BARBERO_FLAG_ENABLED))
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
										g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszDesc?g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszDesc:g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszServerName);  
									g_pbarbero->m_msgr.DM(MSG_ICONINFO, NULL, "Barbero:destination_change", errorstring);  Sleep(20);  //(Dispatch message)
									g_pbarbero->m_data.m_ppDestObj[nTemp]->m_bKillConnThread = true;
									g_pbarbero->m_data.m_ppDestObj[nTemp]->m_ulStatus = BARBERO_STATUS_NOTCON;
								}

								if(
										((bFlagsFound)&&(ulFlags&BARBERO_FLAG_ENABLED)&&(!((g_pbarbero->m_data.m_ppDestObj[nTemp]->m_ulFlags)&BARBERO_FLAG_ENABLED)))
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating %s...", 
//										g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszClientName?g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszClientName:"Barbero",  
										g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszDesc?g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszDesc:g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pszServerName);  
									g_pbarbero->m_msgr.DM(MSG_ICONINFO, NULL, "Barbero:destination_change", errorstring);   Sleep(20);  //(Dispatch message)
//									g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pDlg->OnButtonConnect();
									//**** if connect set following status


									bRefresh = true;
//									g_pbarbero->m_data.m_ppDestObj[nTemp]->m_bKillConnThread = false;
	//								if(_beginthread(BarberoConnectionThread, 0, (void*)g_pbarbero->m_data.m_ppDestObj[nTemp])<0)
	//								{
										//error.

										//**MSG
							
										
		//							}
									//**** should check return value....

									g_pbarbero->m_data.m_ppDestObj[nTemp]->m_ulStatus = BARBERO_STATUS_CONN;
								}

								if(bFlagsFound) g_pbarbero->m_data.m_ppDestObj[nTemp]->m_ulFlags = ulFlags|BARBERO_FLAG_FOUND;
								else g_pbarbero->m_data.m_ppDestObj[nTemp]->m_ulFlags |= BARBERO_FLAG_FOUND;

								if(bRefresh)g_pbarbero->m_data.m_ppDestObj[nTemp]->m_ulFlags |= BARBERO_FLAG_REFRESH;

							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szHost.GetLength()>0)) // have to add.
				{
//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "adding %s", szHost);  Sleep(250); //(Dispatch message)
					CBarberoDestinationObject* pscno = new CBarberoDestinationObject;
					if(pscno)
					{
//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CBarberoDestinationObject** ppObj = new CBarberoDestinationObject*[g_pbarbero->m_data.m_nNumDestinationObjects+1];
						if(ppObj)
						{
//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((g_pbarbero->m_data.m_ppDestObj)&&(g_pbarbero->m_data.m_nNumDestinationObjects>0))
							{
								while(o<g_pbarbero->m_data.m_nNumDestinationObjects)
								{
									ppObj[o] = g_pbarbero->m_data.m_ppDestObj[o];
									o++;
								}
								delete [] g_pbarbero->m_data.m_ppDestObj;

							}
							ppObj[o] = pscno;
							g_pbarbero->m_data.m_ppDestObj = ppObj;
							g_pbarbero->m_data.m_nNumDestinationObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szHost.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, "%s", szHost);

/*							
							if(szClient.GetLength()<=0)
							{
								szClient = "Barbero";
							}

							if(szClient.GetLength()>0)
							{
								ppObj[o]->m_pszClientName = (char*)malloc(szClient.GetLength()+1); 
								if(ppObj[o]->m_pszClientName) sprintf(ppObj[o]->m_pszClientName, "%s", szClient);
							}
*/
							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}
							if(bItemFound) ppObj[o]->m_nItemID = nItemID;
							ppObj[o]->m_usType = usType;

							if(dblDiskspaceThreshold>0.0) ppObj[o]->m_dblDiskPercent = dblDiskspaceThreshold;
//							ppObj[o]->m_dblDiskKBFree = dblDiskspaceFree;
//							ppObj[o]->m_dblDiskKBTotal = dblDiskspaceTotal;

//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "adding flags: %s (0x%08x)", szHost, ulFlags);  Sleep(250); //(Dispatch message)

							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|BARBERO_FLAG_FOUND|BARBERO_FLAG_NEW;
							else ppObj[o]->m_ulFlags |= BARBERO_FLAG_FOUND|BARBERO_FLAG_NEW;
								
//g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", " added flags: %s (0x%08x) %d", szHost, ppObj[o]->m_ulFlags, ((ppObj[o]->m_ulFlags)&BARBERO_FLAG_ENABLED));  Sleep(250); //(Dispatch message)

							if((ppObj[o]->m_ulFlags)&BARBERO_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating %s...", 
										ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName);  
								g_pbarbero->m_msgr.DM(MSG_ICONINFO, NULL, "Barbero:destination_change", errorstring);   Sleep(20);  //(Dispatch message)

								//**** if connect 

								// create a connection object in g_adc.
								// associate that with the new CBarberoConnectionObject;
								// start the thread.
//								ppObj[o]->m_bKillConnThread = false;

								ppObj[o]->m_ulStatus = BARBERO_STATUS_CONN;
							}

						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			// and insert media list refresh into the queue for all new destinatons.
			nIndex = 0;
			while(nIndex<g_pbarbero->m_data.m_nNumDestinationObjects)
			{
		_ftime( &g_pbarbero->m_data.m_timebTick );
				if((g_pbarbero->m_data.m_ppDestObj)&&(g_pbarbero->m_data.m_ppDestObj[nIndex]))
				{

					if((g_pbarbero->m_data.m_ppDestObj[nIndex]->m_ulFlags)&BARBERO_FLAG_FOUND)
					{
						(g_pbarbero->m_data.m_ppDestObj[nIndex]->m_ulFlags) &= ~BARBERO_FLAG_FOUND;

						if((g_pbarbero->m_data.m_ppDestObj[nIndex]->m_ulFlags)&(BARBERO_FLAG_NEW|BARBERO_FLAG_REFRESH))
						{
							(g_pbarbero->m_data.m_ppDestObj[nIndex]->m_ulFlags) &= ~(BARBERO_FLAG_REFRESH|BARBERO_FLAG_NEW);
							// refresh media.
// insert an immediate destinations media refresh into the queue.
/*
Queue action IDs
1 transfer to Miranda
2 transfer from Miranda
3 delete from Miranda
4 refresh device listing
19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)

Queue table:
//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
//action int, host varchar(64), timestamp float, username varchar(32));
*/

							if((g_pbarbero->m_data.m_ppDestObj[nIndex]->m_pszServerName)&&(strlen(g_pbarbero->m_data.m_ppDestObj[nIndex]->m_pszServerName)) && ((g_pbarbero->m_data.m_ppDestObj[nIndex]->m_ulFlags)&BARBERO_FLAG_ENABLED)  )
							{
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (filename_local, filename_remote, action, host, timestamp, username) \
VALUES ('','', 4, '%s', 0, 'sys' )", //HARDCODE
								((g_pbarbero->m_settings.m_pszQueue)&&(strlen(g_pbarbero->m_settings.m_pszQueue)))?g_pbarbero->m_settings.m_pszQueue:"Queue",
								g_pbarbero->m_data.m_ppDestObj[nIndex]->m_pszServerName
								);


//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", szSQL);  Sleep(250); //(Dispatch message)
//					char errorstring[DB_ERRORSTRING_LEN];
//								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
								{
									//**MSG
//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

								}

// deal with the diskspace info 
							// actually, do this on any transaction from queue.

							}
						}
					
						nIndex++;
					}
					else
					{
						if(g_pbarbero->m_data.m_ppDestObj[nIndex])
						{
							if((g_pbarbero->m_data.m_ppDestObj[nIndex]->m_ulFlags)&BARBERO_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
									g_pbarbero->m_data.m_ppDestObj[nIndex]->m_pszDesc?g_pbarbero->m_data.m_ppDestObj[nIndex]->m_pszDesc:g_pbarbero->m_data.m_ppDestObj[nIndex]->m_pszServerName);  
								g_pbarbero->m_msgr.DM(MSG_ICONINFO, NULL, "Barbero:destination_remove", errorstring);   Sleep(20);  //(Dispatch message)
//									g_pbarbero->m_data.m_ppDestObj[nTemp]->m_pDlg->OnDisconnect();

								//**** disconnect

								g_pbarbero->m_data.m_ppDestObj[nIndex]->m_bKillConnThread = false;
//								while(g_pbarbero->m_data.m_ppDestObj[nIndex]->m_bConnThreadStarted) Sleep(1);

								//g_adc.DisconnectServer(g_pbarbero->m_data.m_ppDestObj[nIndex]->m_pszServerName);
								//**** should check return value....

								g_pbarbero->m_data.m_ppDestObj[nIndex]->m_ulStatus = BARBERO_STATUS_NOTCON;
							}

							delete g_pbarbero->m_data.m_ppDestObj[nIndex];
							g_pbarbero->m_data.m_nNumDestinationObjects--;

							int nTemp=nIndex;
							while(nTemp<g_pbarbero->m_data.m_nNumDestinationObjects)
							{
								g_pbarbero->m_data.m_ppDestObj[nTemp]=g_pbarbero->m_data.m_ppDestObj[nTemp+1];
								nTemp++;
							}
							g_pbarbero->m_data.m_ppDestObj[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
	}
//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return BARBERO_ERROR;
}

