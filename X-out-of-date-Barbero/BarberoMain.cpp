// BarberoMain.cpp: implementation of the CBarberoMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "Barbero.h"  // just included to have access to windowing environment
#include "BarberoDlg.h"  // just included to have access to windowing environment
#include "BarberoHandler.h"  // just included to have access to windowing environment

#include "BarberoMain.h"
#include <process.h>
#include "../../Common/TXT/BufferUtil.h"
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"
//#include "../../Common/API/Miranda/IS2Comm.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;
CBarberoMain* g_pbarbero=NULL;
//CADC g_adc;
//CIS2Comm g_miranda;  // global miranda object

extern CMessager* g_pmsgr;  // from Messager.cpp
extern CBarberoApp theApp;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBarberoMain::CBarberoMain()
{
}

CBarberoMain::~CBarberoMain()
{
}

/*

char*	CBarberoMain::BarberoTranslate(CHTTPHeader* pHeader, char* pszBuffer)			// apply barbero scripting language
{
	return pszBuffer;
}

int		CBarberoMain::InterpretDirective(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the directive, such as a char buffer.
	return BARBERO_SUCCESS;
}
*/

SOCKET*	CBarberoMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // barbero initiates a request to an object server
{
	CNetData* pdata = new CNetData;

	pdata->m_ucType = ucType;      // defined type - indicates which protocol to use, structure of data

	pdata->m_ucCmd = ucCmd;       // the command byte
	pdata->m_ucSubCmd = ucSubCmd;       // the subcommand byte

	pdata->m_pucData = pucData;
	pdata->m_ulDataLen = ulDataLen;

	SOCKET* ps = NULL; 
	m_net.SendData(pdata, pchHost, usPort, 5000, 0, NET_SND_CMDTOSVR, ps);
//	m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);

	return ps;
}

int		CBarberoMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// barbero replies to an object server after receiving data. (usually ack or nak)
{
	return BARBERO_SUCCESS;
}

int		CBarberoMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// barbero answers a request from an object client
{
	return BARBERO_SUCCESS;
}


void BarberoMainThread(void* pvArgs)
{
	CBarberoApp* pApp = (CBarberoApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment
	//startup.
	g_bThreadStarted = true;

	//create the main objects.

	CBarberoMain barbero;
	CDBUtil db;

	barbero.m_data.m_ulFlags &= ~BARBERO_STATUS_THREAD_MASK;
	barbero.m_data.m_ulFlags |= BARBERO_STATUS_THREAD_START;
	barbero.m_data.m_ulFlags &= ~BARBERO_ICON_MASK;
	barbero.m_data.m_ulFlags |= BARBERO_STATUS_UNINIT;

	barbero.m_data.GetHost();

	g_pbarbero = &barbero;


	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
//	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\Barbero\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}



//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

//		AfxMessageBox( pApp->m_lpCmdLine );

	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// check here for Cortex IP.
			if(strlen(pch))
			{
//		AfxMessageBox( pch );
				if(barbero.m_data.m_bu.CountChar(pch, strlen(pch), ':')>=1)  // must be of form "hostname:cmdport" where hostname can be name or ip
				{
					barbero.m_data.m_pszCortexHost = (char*)malloc(strlen(pch)+1);
					if(barbero.m_data.m_pszCortexHost)
					{
						strcpy(barbero.m_data.m_pszCortexHost, pch);
//		AfxMessageBox( barbero.m_data.m_pszCortexHost );

						char* pchd = strchr(barbero.m_data.m_pszCortexHost, ':');
						if(pchd)
						{
							*pchd = 0;  // null term the host name;
							pchd++;
//		AfxMessageBox( barbero.m_data.m_pszCortexHost );

							char* pchd2 = strchr(pchd, ':');
							if(pchd2)
							{
								*pchd2 = 0;  // null term the cmd port;
								pchd2++;
								if(strlen(pchd2)) barbero.m_data.m_usCortexStatusPort = atoi(pchd2);
							}

							if(strlen(pchd)) barbero.m_data.m_usCortexCommandPort = atoi(pchd);
						}
					}

				}
			}
			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }

	// get settings.
	barbero.m_settings.Settings(true); //read
/*
	char pszFilename[MAX_PATH];

	strcpy(pszFilename, BARBERO_SETTINGS_FILE_DEFAULT);  // barbero settings file

	CFileUtil file;
	file.GetSettings(pszFilename, false); 
	char* pszParams = NULL;
// load up the values on the settings object
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		barbero.m_settings.m_pszName = file.GetIniString("Main", "Name", "Barbero");
		barbero.m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");

		// compile license key params
		if(g_pbarbero->m_data.m_key.m_pszLicenseString) free(g_pbarbero->m_data.m_key.m_pszLicenseString);
		g_pbarbero->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(barbero.m_settings.m_pszLicense)+1);
		if(g_pbarbero->m_data.m_key.m_pszLicenseString)
			sprintf(g_pbarbero->m_data.m_key.m_pszLicenseString, "%s", barbero.m_settings.m_pszLicense);

		g_pbarbero->m_data.m_key.InterpretKey();

		if(g_pbarbero->m_data.m_key.m_bValid)
		{
			unsigned long i=0;
			while(i<g_pbarbero->m_data.m_key.m_ulNumParams)
			{
				if((g_pbarbero->m_data.m_key.m_ppszParams)
					&&(g_pbarbero->m_data.m_key.m_ppszValues)
					&&(g_pbarbero->m_data.m_key.m_ppszParams[i])
					&&(g_pbarbero->m_data.m_key.m_ppszValues[i]))
				{
					if(stricmp(g_pbarbero->m_data.m_key.m_ppszParams[i], "max")==0)
					{
						g_pbarbero->m_data.m_nMaxLicensedDevices = atoi(g_pbarbero->m_data.m_key.m_ppszValues[i]);
					}
				}
				i++;
			}

			if(
					(
				    (!barbero.m_data.m_key.m_bExpires)
					||((barbero.m_data.m_key.m_bExpires)&&(!barbero.m_data.m_key.m_bExpired))
					||((barbero.m_data.m_key.m_bExpires)&&(barbero.m_data.m_key.m_bExpireForgiveness)&&(barbero.m_data.m_key.m_ulExpiryDate+barbero.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
				    (!barbero.m_data.m_key.m_bMachineSpecific)
					||((barbero.m_data.m_key.m_bMachineSpecific)&&(barbero.m_data.m_key.m_bValidMAC))
					)
				)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
				barbero.m_data.SetStatusText(errorstring, BARBERO_STATUS_OK);
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				barbero.m_data.SetStatusText(errorstring, BARBERO_STATUS_ERROR);
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
			barbero.m_data.SetStatusText(errorstring, BARBERO_STATUS_ERROR);
		}
/*
		barbero.m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", BARBERO_PORT_CMD);
		barbero.m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", BARBERO_PORT_STATUS);

		barbero.m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");

		barbero.m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
		barbero.m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
		barbero.m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
		barbero.m_settings.m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
		barbero.m_settings.m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;

		barbero.m_settings.m_pszDSN = file.GetIniString("Database", "DSN", barbero.m_settings.m_pszName?barbero.m_settings.m_pszName:"Barbero");
		barbero.m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
		barbero.m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
		barbero.m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
		barbero.m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
		barbero.m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name

		barbero.m_settings.m_pszDestinationMediaTemp = file.GetIniString("Database", "DestinationMediaTempTableName", "Destinations_Media_Temp");  // the Destinations table name
		barbero.m_settings.m_pszDestinationMedia = file.GetIniString("Database", "DestinationMediaTableName", "Destinations_Media");  // the Destinations table name
		barbero.m_settings.m_pszDestinations = file.GetIniString("Database", "DestinationsTableName", "Destinations");  // the Destinations table name
		barbero.m_settings.m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue");  // the Queue table name

		barbero.m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds

		if(pszParams) free(pszParams); pszParams=NULL;
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
	}
*/

	bool bUseLog = false;
	if(barbero.m_settings.m_bUseLog)
	{
		bUseLog = barbero.m_settings.m_bUseLog;

		// for logfiles, we need params, and they must be in this format:
		//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
//		pszParams = file.GetIniString("Messager", "LogFileIni", "Barbero|YD||1|");
		int nRegisterCode=0;

		nRegisterCode = barbero.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, "log", barbero.m_settings.m_pszFileSpec, errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
			barbero.m_data.SetStatusText(errorstring, (BARBERO_STATUS_FAIL_LOG|BARBERO_STATUS_ERROR));
		}

//		if(pszParams) free(pszParams); pszParams=NULL;
	}

	barbero.m_msgr.DM(MSG_ICONINFO, NULL, "Barbero", "--------------------------------------------------\n\
-------------- Barbero %s start --------------", BARBERO_CURRENT_VERSION);  //(Dispatch message)


//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
//	barbero.m_http.InitializeMessaging(&barbero.m_msgr);
	barbero.m_net.InitializeMessaging(&barbero.m_msgr);

	db.InitializeMessaging(&barbero.m_msgr);
//	g_miranda.InitializeMessaging(&barbero.m_msgr);

// TODO: here, get the DB and pull any override settings.
	//****
	CDBconn* pdbConn = db.CreateNewConnection(barbero.m_settings.m_pszDSN, barbero.m_settings.m_pszUser, barbero.m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			barbero.m_data.SetStatusText(errorstring, (BARBERO_STATUS_FAIL_DB|BARBERO_STATUS_ERROR));
			barbero.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Barbero:database_connect", errorstring);  //(Dispatch message)
		}
		else
		{
			barbero.m_settings.m_pdbConn = pdbConn;
			barbero.m_settings.m_pdb = &db;
			barbero.m_data.m_pdbConn = pdbConn;
			barbero.m_data.m_pdb = &db;
			if(barbero.m_settings.GetFromDatabase(errorstring)<BARBERO_SUCCESS)
			{
				barbero.m_data.SetStatusText(errorstring, (BARBERO_STATUS_FAIL_DB|BARBERO_STATUS_ERROR));
				barbero.m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:database_get", errorstring);  //(Dispatch message)
			}
			else
			{


				if(
						(barbero.m_settings.m_pszQueue)
					&&(strlen(barbero.m_settings.m_pszQueue)>0)
					)
				{

					char szSQL[DB_SQLSTRING_MAXLEN];
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
														barbero.m_settings.m_pszQueue  // table name
													);
//			g_pbarbero->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s", szSQL);  Sleep(250); //(Dispatch message)

					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						//**MSG
					}

				}



				if((!barbero.m_settings.m_bUseLog)&&(bUseLog))
				{
					// reset it
					barbero.m_msgr.DM(MSG_ICONINFO, NULL, "Barbero", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

					Sleep(250); // let the message get there.
					barbero.m_msgr.RemoveDestination("log");

				}
			}
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", barbero.m_settings.m_pszDSN, barbero.m_settings.m_pszUser, barbero.m_settings.m_pszPW); 
		barbero.m_data.SetStatusText(errorstring, (BARBERO_STATUS_FAIL_DB|BARBERO_STATUS_ERROR));
		barbero.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Barbero:database_init", errorstring);  //(Dispatch message)

		//**MSG
	}



//init command and status listeners.

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing command server on %d", barbero.m_settings.m_usCommandPort); 
	barbero.m_data.SetStatusText(errorstring, BARBERO_STATUS_CMDSVR_START);
	barbero.m_msgr.DM(MSG_ICONNONE, NULL, "Barbero:command_server_init", errorstring);  //(Dispatch message)

	if(barbero.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = barbero.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "BarberoCommandServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = BarberoCommandHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &barbero;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &barbero.m_net;					// pointer to the object with the Message function.


		if(barbero.m_net.StartServer(pServer, &barbero.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			barbero.m_data.SetStatusText(errorstring, BARBERO_STATUS_CMDSVR_ERROR);
			barbero.m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:command_server_init", errorstring);  //(Dispatch message)
			barbero.SendMsg(CX_SENDMSG_ERROR, "Barbero:command_server_init", errorstring);
		}
		else
		{
//AfxMessageBox("Q1");
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Command server listening on %d", barbero.m_settings.m_usCommandPort);
			barbero.m_data.SetStatusText(errorstring, BARBERO_STATUS_CMDSVR_RUN);
			barbero.m_msgr.DM(MSG_ICONNONE, NULL, "Barbero:command_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing status server on %d", barbero.m_settings.m_usStatusPort); 
	barbero.m_data.SetStatusText(errorstring, BARBERO_STATUS_STATUSSVR_START);
	barbero.m_msgr.DM(MSG_ICONNONE, NULL, "Barbero:status_server_init", errorstring);  //(Dispatch message)

	if(barbero.m_settings.m_usStatusPort>0)
	{
//AfxMessageBox("Q1");
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = barbero.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "BarberoStatusServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = BarberoStatusHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &barbero;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &barbero.m_net;					// pointer to the object with the Message function.

		if(barbero.m_net.StartServer(pServer, &barbero.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			barbero.m_data.SetStatusText(errorstring, BARBERO_STATUS_STATUSSVR_ERROR);
			barbero.m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:status_server_init", errorstring);  //(Dispatch message)
			barbero.SendMsg(CX_SENDMSG_ERROR, "Barbero:status_server_init", errorstring);
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Status server listening on %d", barbero.m_settings.m_usStatusPort);
			barbero.m_data.SetStatusText(errorstring, BARBERO_STATUS_STATUSSVR_RUN);
			barbero.m_msgr.DM(MSG_ICONNONE, NULL, "Barbero:status_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

//AfxMessageBox("QQ");

	
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Barbero is registering destination devices...");
	barbero.m_msgr.DM(MSG_ICONNONE, NULL, "Barbero:destination_init", errorstring);  //(Dispatch message)

	barbero.m_data.SetStatusText(errorstring, BARBERO_STATUS_THREAD_SPARK);

	// now connect all the registered servers
	// this is the list of destinations in the db that get connected


	if(!(barbero.m_settings.m_ulMainMode&BARBERO_MODE_CLONE))
	{
		// get connections from database - set up Harris API connections
		barbero.m_data.GetDestinations();
//	barbero.m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "X2");  Sleep(250);//(Dispatch message)
//		barbero.m_data.GetChannels();
//	barbero.m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "X3");  Sleep(250);//(Dispatch message)
	}
//	barbero.m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "X4");  Sleep(250);//(Dispatch message)
	
	if((barbero.m_data.m_ulFlags&BARBERO_ICON_MASK) != BARBERO_STATUS_ERROR)
	{
		barbero.m_data.m_ulFlags &= ~BARBERO_ICON_MASK;
		barbero.m_data.m_ulFlags |= BARBERO_STATUS_OK;  // green - we want run to be blue when something in progress
	}


// initialize cortex (make this not persistent for now)
	CNetData* pdata = new CNetData;
	if(pdata)
	{

		pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA;      // defined type - indicates which protocol to use, structure of data

		pdata->m_ucCmd = CX_REQ_HELLO;       // the command byte
		pdata->m_ucSubCmd = 0;       // the subcommand byte

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
		if(pch)
		{
			sprintf(pch, "%s:%d:%d:%d:%s", 
				barbero.m_data.m_pszHost,
				barbero.m_settings.m_usCommandPort,
				barbero.m_settings.m_usStatusPort,
				CX_TYPE_RESOURCE,
				barbero.m_settings.m_pszName?barbero.m_settings.m_pszName:"Barbero"
				);

			pdata->m_pucData =  (unsigned char*) pch;
			pdata->m_ulDataLen = strlen(pch);
		}

		SOCKET s = NULL; 
//AfxMessageBox("sending");
//AfxMessageBox( barbero.m_data.m_pszCortexHost );
//AfxMessageBox( (char*)pdata->m_pucData );
		if(barbero.m_net.SendData(pdata, barbero.m_data.m_pszCortexHost, barbero.m_data.m_usCortexCommandPort, 5000, 0, NET_SND_CMDTOSVR, &s)>=NET_SUCCESS)
		{
			//send ack
//			AfxMessageBox("ack");
			barbero.m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);
		}
		else
		{
//			AfxMessageBox("could not send");
		}
		barbero.m_net.CloseConnection(s);

		delete pdata;

	}


	// try to initialize the oxsox object.

/*
	int nReturn = g_miranda.OxSoxLoad();
	if(nReturn<0)
	{
		barbero.m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:oxsox_init", "Error: OxSoxLoad returned %d", nReturn);  //(Dispatch message)
		barbero.SendMsg(CX_SENDMSG_ERROR, "Barbero:oxsox_init", "Error: OxSoxLoad returned %d", nReturn);
	}	
	else
	{
		barbero.m_msgr.DM(MSG_ICONNONE, NULL, "Barbero:oxsox_init", "Success: OxSoxLoad returned %07x", nReturn);  //(Dispatch message)
	}

	if(g_miranda.m_lpfnInitWinsock)
	{ 
		try
		{
			nReturn = g_miranda.m_lpfnInitWinsock();
			g_miranda.m_bDLLWinsockInit = true; // InitWinsock always returns true
		}
		catch(...)
		{
			barbero.m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:oxsox_init", "Error: exception calling InitWinsock");  //(Dispatch message)
			barbero.SendMsg(CX_SENDMSG_ERROR, "Barbero:oxsox_init", "Error: exception calling InitWinsock");
		}
	}
	else
	{
		barbero.m_msgr.DM(MSG_ICONERROR, NULL, "Barbero:oxsox_init", "Error: NULL function address for InitWinsock");  //(Dispatch message)
		barbero.SendMsg(CX_SENDMSG_ERROR, "Barbero:oxsox_init", "Error: NULL function address for InitWinsock");
	}

*/
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Barbero main thread running.");  
	barbero.m_data.SetStatusText(errorstring, BARBERO_STATUS_THREAD_RUN);
//	barbero.m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", errorstring);  Sleep(250);//(Dispatch message)
	barbero.SendMsg(CX_SENDMSG_INFO, "Barbero:init", "Barbero %s main thread running.", BARBERO_CURRENT_VERSION);

	_timeb timebCheckMods;
	_ftime( &timebCheckMods );
///AfxMessageBox("xxxxx");
	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &barbero.m_data.m_timebTick );
//	barbero.m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "%d.%03d", barbero.m_data.m_timebTick.time, barbero.m_data.m_timebTick.millitm);   Sleep(250);//(Dispatch message)

// monitor changes in exchange db and react.
		if(
					(barbero.m_data.m_timebTick.time > timebCheckMods.time )
				||((barbero.m_data.m_timebTick.time == timebCheckMods.time)&&(barbero.m_data.m_timebTick.millitm >= timebCheckMods.millitm))
				&&(!g_bKillThread)
				&&(!barbero.m_data.m_bProcessSuspended)
/*
// cant do the following here.  need to allow the call to barbero.m_settings.GetFromDatabase()
// with a new license key!
				&&(barbero.m_data.m_key.m_bValid)  // must have a valid license
				&&(
				    (!barbero.m_data.m_key.m_bExpires)
					||((barbero.m_data.m_key.m_bExpires)&&(!barbero.m_data.m_key.m_bExpired))
					||((barbero.m_data.m_key.m_bExpires)&&(barbero.m_data.m_key.m_bExpireForgiveness)&&(barbero.m_data.m_key.m_ulExpiryDate+barbero.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
				    (!barbero.m_data.m_key.m_bMachineSpecific)
					||((barbero.m_data.m_key.m_bMachineSpecific)&&(barbero.m_data.m_key.m_bValidMAC))
					)
*/
			)
		{
//	barbero.m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "time to check");  Sleep(250);//(Dispatch message)
			timebCheckMods.time = barbero.m_data.m_timebTick.time + barbero.m_settings.m_ulModsIntervalMS/1000; 
			timebCheckMods.millitm = barbero.m_data.m_timebTick.millitm + (unsigned short)(barbero.m_settings.m_ulModsIntervalMS%1000); // fractional second updates
			if(timebCheckMods.millitm>999)
			{
				timebCheckMods.time++;
				timebCheckMods.millitm%=1000;
			}
//	barbero.m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "checking db connected");   Sleep(250);//(Dispatch message)
			if((pdbConn)&&(pdbConn->m_bConnected))
			{
//AfxMessageBox("x");
//	barbero.m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "checkmods");  //(Dispatch message)
				barbero.m_data.CheckDatabaseMods();
				if(barbero.m_data.m_nSettingsMod != barbero.m_data.m_nLastSettingsMod)
				{
					if(barbero.m_settings.GetFromDatabase()>=BARBERO_SUCCESS)
					{
						barbero.m_data.m_nLastSettingsMod = barbero.m_data.m_nSettingsMod;
					}
				}
				if(barbero.m_data.m_nDestinationsMod != barbero.m_data.m_nLastDestinationsMod)
				{
//			barbero.m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "getting destinations again");   Sleep(250);//(Dispatch message)
					if( 
							(barbero.m_data.m_key.m_bValid)  // must have a valid license
						&&(
								(!barbero.m_data.m_key.m_bExpires)
							||((barbero.m_data.m_key.m_bExpires)&&(!barbero.m_data.m_key.m_bExpired))
							||((barbero.m_data.m_key.m_bExpires)&&(barbero.m_data.m_key.m_bExpireForgiveness)&&(barbero.m_data.m_key.m_ulExpiryDate+barbero.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
							)
						&&(
								(!barbero.m_data.m_key.m_bMachineSpecific)
							||((barbero.m_data.m_key.m_bMachineSpecific)&&(barbero.m_data.m_key.m_bValidMAC))
							)
						)
					{
						if(barbero.m_data.GetDestinations()>=BARBERO_SUCCESS)
						{
							barbero.m_data.m_nLastDestinationsMod = barbero.m_data.m_nDestinationsMod;
						}
					}
				}
			}
		}
//AfxMessageBox("zoinks");

		
		// following section is outside the mods loop, the Queue is really the main loop.
// following line commented out, because we want to check the Queue all the time, for the presence of anything at all to deal with.
//				if(barbero.m_data.m_nQueueMod != barbero.m_data.m_nLastQueueMod)
		if(
				(!barbero.m_data.m_bProcessSuspended)	
			&&(barbero.m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!barbero.m_data.m_key.m_bExpires)
				||((barbero.m_data.m_key.m_bExpires)&&(!barbero.m_data.m_key.m_bExpired))
				||((barbero.m_data.m_key.m_bExpires)&&(barbero.m_data.m_key.m_bExpireForgiveness)&&(barbero.m_data.m_key.m_ulExpiryDate+barbero.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!barbero.m_data.m_key.m_bMachineSpecific)
				||((barbero.m_data.m_key.m_bMachineSpecific)&&(barbero.m_data.m_key.m_bValidMAC))
				)
			)
		{
//			barbero.m_msgr.DM(MSG_ICONHAND, NULL, "Barbero:debug", "getting channels again");   Sleep(250);//(Dispatch message)
			if(barbero.m_data.GetQueue()>=BARBERO_SUCCESS)
			{
				barbero.m_data.m_nLastQueueMod = barbero.m_data.m_nQueueMod;
			}
		}
		
		
		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();

		Sleep(1); 
//		Sleep(1000);  // stalls window procedure
	}

	barbero.m_data.m_ulFlags &= ~BARBERO_STATUS_THREAD_MASK;
	barbero.m_data.m_ulFlags |= BARBERO_STATUS_THREAD_END;

	barbero.m_msgr.DM(MSG_ICONINFO, NULL, "Barbero:uninit", "Barbero is shutting down.");  //(Dispatch message)
	barbero.SendMsg(CX_SENDMSG_INFO, "Barbero:uninit", "Barbero %s is shutting down.", BARBERO_CURRENT_VERSION);

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Barbero is shutting down.");  
	barbero.m_data.m_ulFlags &= ~CX_ICON_MASK;
	barbero.m_data.m_ulFlags |= CX_STATUS_UNKNOWN;
	barbero.m_data.SetStatusText(errorstring, barbero.m_data.m_ulFlags);

/*
	// here's a hard coded one for now
	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no need to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = barbero.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		barbero.m_net.OpenConnection(barbero.m_http.m_pszHost, 10888, &s); // branding hardcode
		barbero.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		barbero.m_net.CloseConnection(s); // branding hardcode
	}
*/

//	m_pDlg->SetProgress(BARBERODLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
//	barbero.m_data.m_ulFlags &= ~BARBERO_STATUS_FILESVR_MASK;
//	barbero.m_data.m_ulFlags |= BARBERO_STATUS_FILESVR_END;
//	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down file server....");  
//	barbero.m_data.SetStatusText(errorstring, barbero.m_data.m_ulFlags);
//	_ftime( &barbero.m_data.m_timebTick );
//	barbero.m_http.EndServer();
	_ftime( &barbero.m_data.m_timebTick );
//	AfxMessageBox("shutting down command server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down command server....");  
	barbero.m_msgr.DM(MSG_ICONNONE, NULL, "Barbero:command_server_uninit", errorstring);  //(Dispatch message)
	barbero.m_data.SetStatusText(errorstring, BARBERO_STATUS_CMDSVR_END);
	_ftime( &barbero.m_data.m_timebTick );
	barbero.m_net.StopServer(barbero.m_settings.m_usCommandPort, 5000, errorstring);
	_ftime( &barbero.m_data.m_timebTick );
//	AfxMessageBox("shutting down status server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down status server....");  
	barbero.m_msgr.DM(MSG_ICONNONE, NULL, "Barbero:status_server_uninit", errorstring);  //(Dispatch message)
	barbero.m_data.SetStatusText(errorstring, BARBERO_STATUS_STATUSSVR_END);
	_ftime( &barbero.m_data.m_timebTick );
	barbero.m_net.StopServer(barbero.m_settings.m_usStatusPort, 5000, errorstring);
	_ftime( &barbero.m_data.m_timebTick );
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Barbero is exiting.");  
	barbero.m_msgr.DM(MSG_ICONNONE, NULL, "Barbero:uninit", errorstring);  //(Dispatch message)
	barbero.m_data.SetStatusText(errorstring, barbero.m_data.m_ulFlags);



	// save settings.  // dont save them here.  save them on any changes in the main command loop.
	barbero.m_settings.Settings(false); //write
/*

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		// these explicts arent necessary - uncomment to write out a full file to edit...
		file.SetIniString("Main", "Name", barbero.m_settings.m_pszName);
		file.SetIniString("License", "Key", barbero.m_settings.m_pszLicense);

		file.SetIniInt("CommandServer", "ListenPort", barbero.m_settings.m_usCommandPort);
		file.SetIniInt("StatusServer", "ListenPort", barbero.m_settings.m_usStatusPort);
		file.SetIniString("FileServer", "IconPath", barbero.m_settings.m_pszIconPath);

		file.SetIniInt("Messager", "UseEmail", barbero.m_settings.m_bUseEmail?1:0);
		file.SetIniInt("Messager", "UseNet", barbero.m_settings.m_bUseNetwork?1:0);
		file.SetIniInt("Messager", "UseLog", barbero.m_settings.m_bUseLog?1:0);
		file.SetIniInt("Messager", "ReportSuccessfulOperation", barbero.m_settings.m_bReportSuccessfulOperation?1:0);
		file.SetIniInt("Messager", "LogTransfers", barbero.m_settings.m_bLogTransfers?1:0);

		file.SetIniString("Database", "DSN", barbero.m_settings.m_pszDSN);
		file.SetIniString("Database", "DBUser", barbero.m_settings.m_pszUser);
		file.SetIniString("Database", "DBPassword", barbero.m_settings.m_pszPW);
		file.SetIniString("Database", "SettingsTableName", barbero.m_settings.m_pszSettings);  // the Settings table name
		file.SetIniString("Database", "ExchangeTableName", barbero.m_settings.m_pszExchange);  // the Exchange table name
		file.SetIniString("Database", "MessagesTableName", barbero.m_settings.m_pszMessages);  // the Messages table name

//		file.SetIniInt("HarrisAPI", "UseListCount", barbero.m_settings.m_bUseListCount?1:0); // get all events up until the list count (otherwise just up to the lookahead)

		file.SetIniString("Database", "DestinationsTableName", barbero.m_settings.m_pszDestinations);  // the Destinations table name
		file.SetIniString("Database", "QueueTableName", barbero.m_settings.m_pszQueue);  // the Queue table name
		file.SetIniString("Database", "DestinationMediaTableName", barbero.m_settings.m_pszDestinationMedia);  // the DestinationMedia table name
		file.SetIniString("Database", "DestinationMediaTempTableName", barbero.m_settings.m_pszDestinationMediaTemp );  // the Destinations table name

		file.SetIniInt("Database", "ModificationCheckInterval", barbero.m_settings.m_ulModsIntervalMS);  // in milliseconds

		file.SetSettings(BARBERO_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

	}
*/
	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "shutting down");
	barbero.m_data.m_ulFlags &= ~BARBERO_ICON_MASK;
	barbero.m_data.m_ulFlags |= BARBERO_STATUS_UNINIT;
	barbero.m_data.SetStatusText(errorstring, barbero.m_data.m_ulFlags);

	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Barbero is exiting");
	barbero.m_data.m_ulFlags &= ~CX_ICON_MASK;
	barbero.m_data.m_ulFlags |= CX_STATUS_UNINIT;
	barbero.m_data.SetStatusText(errorstring, barbero.m_data.m_ulFlags);

	//exiting
	barbero.m_msgr.DM(MSG_ICONINFO, NULL, "Barbero", "-------------- Barbero %s exit ---------------\n\
--------------------------------------------------\n", BARBERO_CURRENT_VERSION);  //(Dispatch message)
///	m_pDlg->SetProgress(BARBERODLG_CLEAR); // no point

	_ftime( &barbero.m_data.m_timebTick );
	barbero.m_data.m_ulFlags &= ~BARBERO_STATUS_THREAD_MASK;
	barbero.m_data.m_ulFlags |= BARBERO_STATUS_THREAD_ENDED;

	g_pbarbero = NULL;
	db.RemoveConnection(pdbConn);

	Sleep(500); // small delay at end
	g_bThreadStarted = false;
	Sleep(300); // another small delay at end
	_endthread();
}

/*
void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((g_pmsgr)&&(g_pmsgr->m_lpfnDM))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		CMessagingObject msg;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = msg.EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = msg.EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = msg.EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch;
		pch = (char*) malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
* /
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if(pszEncodedMsg) 
			{
				ulBufferLen = strlen(pszEncodedMsg);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedCaller)
			{
				ulBufferLen = strlen(pszEncodedCaller);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedDest)
			{
				ulBufferLen = strlen(pszEncodedDest);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			g_pmsgr->m_lpfnDM(pch, ulFlags);
			free(pch);
		}
	}
}
*/


/*
// this thread is based on void HTTP10HandlerThread(void* pvArgs) from
// HTTP10.cpp.  It makes a special webserver with security, cgi parsing, and dynamic results
void BarberoHTTPThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return;}

	CBarberoMain* pBarbero = (CBarberoMain*)(pClient->m_lpObject) ;  // pointer to the global object.

// HTTP service by def is a non-persistent service, so even if the Client type is
// set to persist, we ignore that directive in this handler thread

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;


		CHTTP10* phttp = (CHTTP10*)(&(pBarbero->m_http)) ;  // pointer to the global object.
		if(phttp == NULL)// cant do anything!
		{
			shutdown(pClient->m_socket, SD_BOTH);
			closesocket(pClient->m_socket);
			(*(pClient->m_pulConnections))--;
			_endthread(); return;
		}

		CHTTP10 http(phttp->m_pszRoot);  // local object for utility functions ONLY.
		// this allows the local to clone the root setting of the global, 
		// in order to do correct parsing in its own thread

		// the following are just pointers to the global obj.  
		// must NULL these out before object gets destroyed, 
		// so that global resources not freed!   !!!!!!!  important !!!!!
		http.m_pszHost = phttp->m_pszHost;
		http.m_ppszErrors = phttp->m_ppszMime;
		http.m_ppszMime = phttp->m_ppszMime;   
		http.m_ulStatus = phttp->m_ulStatus;   // important for redirect and error
		
		unsigned char* pch;// for use with GetLine().
		unsigned long ulBufLen;// for use with GetLine().
		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		unsigned short usCode = 0;

		// get the raw buffer and process it yourself
		nReturn = http.m_net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_EOLN|EOLN_HTTP, pszStatus);
		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). 
			//was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
		{
			//error.
			if(pClient->m_lpMsgObj)
			{
				_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error receiving data.  %s", pszStatus);
				((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
			}

			usCode = 400;  //  we TRY to send this....
			if(pch!=NULL)
			{
				free(pch);  // must free the incoming data buffer 
				pch = NULL;
			}
			// create a return message buffer.
			pch = (unsigned char*)http.ErrorBuffer(NULL, usCode);   // 400 bad request
			ulBufLen = strlen((char*)pch); // valid from an error page.

			// send malformed request answer if possible  - have to tell the client that the request was not received in good order.
			nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
			if(nReturn<NET_SUCCESS)
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
				}
			}
		}
		else  // successful reception of data.
		{
			// process the data here...
			// in this sample the buffer is loaded into
			// pch, with ulBufLen as length

/*
			FILE* fp = fopen("rec.txt", "ab");
			if (fp)
			{
				fwrite(pch, 1, ulBufLen, fp );
				fflush(fp);
				fclose(fp);
			}
* /
			// parse buffer, format reply.
			CHTTPHeader header;

			if (http.ParseHeader(pch, ulBufLen, &header) < HTTP_SUCCESS)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("header not parsed\r\n\r\n", 1, strlen("header not parsed\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}
* /
				
				if(pch!=NULL) 
				{
					free(pch);  // must free the incoming data buffer 
					pch = NULL;
				}
				// create a return message buffer.
				usCode = 400;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}
			else
			{
				// here we have enough info to check security if desired, and if not, return a 401;
				// else continue....
/*
					FILE* fp = fopen("rec.txt", "ab");
					if (fp)
					{// lets write out the whole header so we can examine its full contents. including args.
						char bufferin[10000];
						sprintf(bufferin,
"header successfully parsed\r\n\
method: %d\r\n\
version: %s\r\n\
URL: %s\r\n\
Path: %s\r\n\
mimetype: %s\r\n\
user: %s\r\n\
password: %s\r\n\
num args: %ld\r\n",
	header.m_ucMethod,  // enumerated values
	header.m_pszVersion==NULL?"(null)":header.m_pszVersion,
	header.m_pszURL==NULL?"(null)":header.m_pszURL,
	header.m_pszPath==NULL?"(null)":header.m_pszPath,
	header.m_pszMimeType==NULL?"(null)":header.m_pszMimeType,
	header.m_pszUser==NULL?"(null)":header.m_pszUser,
	header.m_pszPassword==NULL?"(null)":header.m_pszPassword,
	header.m_ulNumArgs

);
		
	unsigned long k=0;
	while(k<header.m_ulNumArgs)
	{
		if((header.m_ppszArgNames)&&(header.m_ppszArgValues))
		{
			strcat(bufferin, "  ");
			strcat(bufferin, header.m_ppszArgNames[k]==NULL?"(null)":header.m_ppszArgNames[k]);
			strcat(bufferin, "=");
			strcat(bufferin, header.m_ppszArgValues[k]==NULL?"(null)":header.m_ppszArgValues[k]);
			strcat(bufferin, "\r\n");
		}
	k++;
	}

	strcat(bufferin, "headers:\r\n");
	strcat(bufferin, header.m_pszHeaders);
	strcat(bufferin, "\r\n");


			//		fwrite("header successfully parsed\r\n", 1, strlen("header successfully parsed\r\n"), fp );
						fwrite(bufferin, 1, strlen(bufferin), fp );
						fflush(fp);
						fclose(fp);
					}

* /


				if((phttp->m_pSecure)&&((header.m_pszUser==NULL)||(header.m_pszPassword==NULL)))
				{
					usCode = 401;
					pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
					ulBufLen = strlen((char*)pch); // valid from an error page.
					// buffer successfully parsed.
				}
				else 
				{
					if(pch!=NULL)
					{
						free(pch);  // must free the incoming data buffer 
						pch = NULL;
					}

					// create a return message buffer.
					switch(header.m_ucMethod)
					{
					case HTTP_GET://			1  // HTTP/1.0
					case HTTP_HEAD://			2  // HTTP/1.0
						{
							if(
									(strnicmp(header.m_pszPath, http.m_pszRoot, strlen(http.m_pszRoot)) == 0 )  // else not a part of the served file tree
								)
							{
								if(phttp->m_pSecure)
								{
									// have to check the main object for permission.
									// we are going to go by URL (asset level)
									// Loci are going to just be convenient names for the admins to set up.

									if((phttp->m_pSecure->CheckSecure(header.m_pszUser, header.m_pszPassword, NULL, header.m_pszURL))!=SECURE_SUCCESS)
									{
										usCode = 401;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
										ulBufLen = strlen((char*)pch); // valid from an error page.
										break; // get out of the switch from here.
									}
									// else it flows thru....
								}

								DWORD dwAttrib;

								dwAttrib = GetFileAttributes(header.m_pszPath);

								// check for directory (redirect nec.)
								if ( (dwAttrib!=0xffffffff)&&(dwAttrib&FILE_ATTRIBUTE_DIRECTORY) )  // if error, it may be because no file exists, so check in the next part.
								{
									usCode = 301;  // not necessary but why not.
									pch = (unsigned char*)http.RedirectBuffer(&header);
									ulBufLen = strlen((char*)pch); // valid from an redirect page.
								}
								else
								{
									// before we try to open the file, lets spend the time now (not while the file is open)
									// to figure out how long the static text respone buffer will be.
									CBufferUtil bu;
									// notice the sample "Server: BarberoServer/3.0.3.1" header included.  Pick the appname and ver, probably best
									unsigned long ulBufferLen = strlen("HTTP/1.0 200 OK\r\nServer: BarberoServer/\r\nContent-Length: \r\n\r\n");
									ulBufferLen += strlen(BARBERO_CURRENT_VERSION);

									if((header.m_pszMimeType)&&(strlen(header.m_pszMimeType)>0))
									{
										ulBufferLen += (strlen("Content-Type: \r\n") + strlen(header.m_pszMimeType)); // only add if known!
									}
									else // empty!
									{
										// dont free the "permanent resource", just null the pointer
										header.m_pszMimeType=NULL;
									}

									FILE* pFile = NULL;

									pFile = fopen(header.m_pszPath, "rb");
									if (pFile == NULL)  // file not openable, or not found.
									{
										usCode = 404;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 404 not found
										ulBufLen = strlen((char*)pch); // valid from an error page.
									}
									else
									{
										// this part just assembles the file into a buffer.
										// this may be modified to do dynamic html things by using the 
										// pClient->m_lpObject that refers back to some parser....

										usCode = 200; // OK  (!)
										// determine file size
										fseek(pFile, 0, SEEK_END);
										unsigned long ulFileLen = ftell(pFile);


										// here we have to parse out any directives.  if this is a template file,
										// we may have a variable length.
									//	x


										if(header.m_ucMethod == HTTP_GET)
										{
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += (strlen(length)+ulFileLen);

											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you truncations at zeroed bytes

											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: BarberoServer/%s", BARBERO_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
											
											fclose(pFile);
										}
										else
										if(header.m_ucMethod == HTTP_HEAD)
										{
											fclose(pFile);  // we have all we need already.
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += strlen(length);
											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											//  create the header
											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: BarberoServer/%s", BARBERO_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												// its identical except for the body.  next line must stay commented out, its just there for ref.
												//fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
										}
									}
								}
							}
							else  // not part of the server service.
							{
								usCode = 403;
								pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 403 forbidden ...
								ulBufLen = strlen((char*)pch); // valid from an error page.
							}

						} break;
					case HTTP_POST://			3  // HTTP/1.0
						{
							usCode = 501;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 501 not implemented ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_PUT://			4  // HTTP/1.1
					case HTTP_DELETE://		5  // HTTP/1.1
					case HTTP_OPTIONS://  6  // HTTP/1.1
					case HTTP_TRACE://	  7  // HTTP/1.1
						{
							usCode = 505;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 505 http version not supported ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_UNDEF://		0  // internal
					default:
						{
							usCode = 400;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					}
				}
			}

			// send a reply, in pch., with strlen pch as length

			if(pch == NULL)  // had a problem forming the request, try to send a last ditch error code.
			{
				usCode = 500;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 500 internal server error
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}

			if(pch != NULL)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("sending:\r\n", 1, strlen("sending:\r\n"), fp );
					fwrite(pch, 1, ulBufLen, fp );
					fwrite("\r\n\r\n", 1, strlen("\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}

* /
				nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
				if(nReturn<NET_SUCCESS)
				{
					//error.
					if(pClient->m_lpMsgObj)
					{
						_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
						((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
					}
				}
			}
		}

//		char foo[34]; sprintf(foo, "code: %d", usCode);	AfxMessageBox(foo);

		http.m_pszHost = NULL;		// but do NOT free resource
		http.m_ppszErrors = NULL; // but do NOT free resource
		http.m_ppszMime = NULL;   // but do NOT free resource

		if(pch!=NULL) free(pch);  // must free the data buffer 

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}
*/


void BarberoCommandHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure in Protocol 1  //SECURE with PROTOCOL2 later
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszStatus);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Barbero:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Barbero:CommandHandlerThread");
						}
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{
//AfxMessageBox("receiving");

					// parse the command:

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					switch(data.m_ucCmd)
					{
					case CX_CMD_GETINFO: // not actually supported yet.  everything thru csf files for now.
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case CX_CMD_BYE:
						{
	g_pbarbero->m_msgr.DM(MSG_ICONINFO, NULL, "Barbero", "The Global Kill command has been received.");  //(Dispatch message)

							bSetGlobalKill = true;
							g_pbarbero->m_data.m_bQuietKill = true;
							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					default:
						{
							//just ack
							data.m_ucCmd = NET_CMD_ACK;

		//					char repbuf[64];
							if(data.m_pucData!=NULL)
							{
		/*
								_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

								for(unsigned long q=0; q<data.m_ulDataLen; q++)
								{
									if(strlen(repbuf)<63)
										strncat(repbuf, (char*)(data.m_pucData+q), 1);
								}
								if(strlen(repbuf)<63)
									strcat(repbuf, "]");
								data.m_ulDataLen = strlen(repbuf);
								repbuf[data.m_ulDataLen] = 0;  // just in case
		*/
								free(data.m_pucData);  //destroy the buffer;
							}
		/*
							else
							{
								_snprintf(repbuf, 63, "data was NULL");
								data.m_ulDataLen = strlen(repbuf); 
							}

							data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
							if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
							else 
							{
								data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
								data.m_ulDataLen = 0;
							}
		*/
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					}

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Barbero:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Barbero:CommandHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.
	if (bSetGlobalKill)	//g_bKillThread = true;
	((CBarberoHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}

void BarberoStatusHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszStatus);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Barbero:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Barbero:CommandHandlerThread");
						}
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Barbero:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Barbero:CommandHandlerThread");
							free(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}


int CBarberoMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)&&(strlen(m_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			m_settings.m_pszMessages,
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			m_settings.m_pszMessages
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return BARBERO_SUCCESS;
		}
	}
	return BARBERO_ERROR;
}


