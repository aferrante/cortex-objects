// BarberoData.h: interface for the CBarberoData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BARBERODATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
#define AFX_BARBERODATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <winsock2.h>
#include <sys/timeb.h>
#include "../../Common/TXT/BufferUtil.h" 
#include "../../Common/LAN/NetUtil.h" 
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"
//#include "../../Common/API/Miranda/IS2Comm.h"
#include "../../Common/KEY/LicenseKey.h"

// these are data reference objects, pulled from the database.
// actual connection, list, and event objects are in the CADC object
class CBarberoDestinationObject  
{
public:
	CBarberoDestinationObject();
	virtual ~CBarberoDestinationObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	char* m_pszServerName;
//	char* m_pszClientName;
	char* m_pszDesc;

//	CAConnection* m_pAPIConn;  // pointer to the associated API connection.


	double m_dblDiskKBFree;  
	double m_dblDiskKBTotal; 
	double m_dblDiskPercent; 
	int m_nChannelID;

	int m_nItemID;

	// control
	// no threads but can leave the vars.
	bool m_bKillConnThread;
	bool m_bConnThreadStarted;
};

/*
class CBarberoChannelObject  
{
public:
	CBarberoChannelObject();
	virtual ~CBarberoChannelObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	int m_nChannelID;  // the unique Channel ID within Barbero setup. (assigned externally)
	int m_nHarrisListID;  // the 1-based List # on the associated ADC-100 server

	char* m_pszServerName;
	char* m_pszDesc;

	CAConnection* m_pAPIConn;  // pointer to the associated API connection.

// control
	bool* m_pbKillConnThread;
	bool m_bKillChannelThread;
	bool m_bChannelThreadStarted;
};

*/

class CBarberoData  
{
public:
	CBarberoData();
	virtual ~CBarberoData();

	// util object
	CBufferUtil m_bu;

  // hostname.  have to get it, store it here
	char*	m_pszHost;	// the name of the host
	char*	m_pszCompleteHost;	// the complete name of the host
	char*	m_pszCortexHost;	// the name of the cortex host
	unsigned short m_usCortexCommandPort;
	unsigned short m_usCortexStatusPort;

	CBarberoDestinationObject** m_ppDestObj;
	int m_nNumDestinationObjects;

//	CBarberoChannelObject** m_ppChannelObj;
//	int m_nNumChannelObjects;

	_timeb m_timebTick; // the last time check inside the thread
	_timeb m_timebLastStatus; // the time of the last status given to cortex
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for cortex global status (each obj has their own as well)

	int m_nSettingsMod;
//	int m_nChannelsMod;
	int m_nDestinationsMod;
	int m_nQueueMod;
	int m_nLastSettingsMod;
//	int m_nLastChannelsMod;
	int m_nLastDestinationsMod;
	int m_nLastQueueMod;
	bool m_bProcessSuspended;
	int m_nMaxLicensedDevices;

	char* GetStatusText(unsigned long* pulStatus);  // allocates mem, must free after use
	int		SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError = false);
	int		GetHost();

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;
	int CheckDatabaseMods(char* pszInfo=NULL);
	int IncrementDatabaseMods(char* pszTableName, char* pszInfo=NULL);
	int GetDestinations(char* pszInfo=NULL);
	int GetQueue(char* pszInfo=NULL);
	int ReturnDestinationIndex(char* pszServerName);

	CLicenseKey m_key;

	bool m_bQuietKill;

	CRITICAL_SECTION m_critText;

private:
	char* m_pszStatus;	// parseable string

//	unsigned long m_ulReqCount;
//	unsigned long m_ulSvcCount;
};

#endif // !defined(AFX_BARBERODATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
