// ProsperoSettings.h: interface for the CProsperoSettings class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROSPEROSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
#define AFX_PROSPEROSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ProsperoDefines.h"
#include "../../Common/MFC/ODBC/DBUtil.h"

class CProsperoSettings  
{
public:
	CProsperoSettings();
	virtual ~CProsperoSettings();

	int GetFromDatabase(char* pszInfo=NULL);  //only get.  Interface publishes
	int Settings(bool bRead);
	char* ProcessString(char* pszString, bool bFreeIncomingString);

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;

	char* m_pszName;  // familiar name of this instance (and therefore the name of the Prospero database)
	char* m_pszType;  // familiar name of the type.
	char* m_pszProject;  // familiar name of the project name.
	unsigned long m_ulMainMode;

	// ports
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;

	// The following flags determine what settings are loaded. 
	// The specifics do not need to be stored in cortex settings, they are pushed to the various objects.

	// messaging for Prospero
	bool m_bUseLog;			// write a log file
	bool m_bUseEmail;		// send an email on failures (or commanded from remotes)
	bool m_bUseNetwork; // send a message to a remote host
	bool m_bReportSuccessfulOperation;
	bool m_bLogTransfers;
	char* m_pszFileSpec;
	char* m_pszMailSpec;
	char* m_pszProcessedFileSpec;
	char* m_pszProcessedMailSpec;
	bool m_bUseXMLClientLog;			// write a log file
	bool m_bLogNetworkErrors; // enables messaging into network object (logs socket errors, etc).
	bool m_bMillisecondMessaging;			// use millisecond resolution for messages and asrun
//	bool m_bUseMessagingForMiranda;


	int m_nThreadDwellMS;
	bool m_bDebugLists;  // if true, dumps the temp and events databases
	int  m_nDebugListTopCount;  //if positive, just prints out the top n recored in the events tables.
	bool m_bDebugSQL;  // if true, prints out the SQL calls, all but event inserts
	bool m_bDebugInsertSQL;  // if true, prints out the event insert SQL calls
	unsigned long m_ulDebug;  // prints out debug statements that & with this.
	char* m_pszDebugOrder; // the order by clause for debug

	int m_nAutoPurgeMessageDays;
//	int m_nAutoPurgeAsRunDays;
	int m_nAutoPurgeInterval;

	int m_nMaxDeleteItems;  // max number of items to delete in a single SQL statement

	int m_nPutDelayMS;  // re-prioritization delay after a successful put command
	int m_nGetDelayMS;  // re-prioritization delay after a successful get command
	int m_nErrorDelayMS;  // re-prioritization delay after an error
	int m_nErrorRetries;  // number of retries after oxsox error to return error to queue
	int m_nDiskCheckMinIntervalMS;  // minimum time in between disk checks
	int m_nCommmandQueueIntervalMS;  // interval in the command thread.  if Queue is used, set this to large value to not compete with db query load

	// Harris API
//	bool m_bUseListCount; // get all events up until the list count (otherwise just up to the lookahead)

	bool m_bDiReCTInstalled; // the DiReCT module is installed.


	// database stuff
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;

	char* m_pszSettings;  // the Settings table name
	char* m_pszExchange;  // the Exchange table name
	char* m_pszMessages;  // the Messages table name

	char* m_pszDestinationMediaTemp;  // the DestinationMediaTemp table name
	char* m_pszDestinationMedia;  // the DestinationMedia table name
	char* m_pszDestinations;  // the Destinations table name
	char* m_pszQueue;  // the Queue table name
	char* m_pszCommandQueue;  // the CommandQueue table name

	unsigned long m_ulModsIntervalMS;  // interval on which to check database mods
	unsigned long m_ulFileRefreshInterval;  // interval on which to mandatorily refresh the file listing in seconds.

	char* m_pszLicense;  // the License Key
	char* m_pszIconPath;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.


	// hive stuff
	int m_nNumberOfHiveObjects;
	char* m_pszDLLFileName;
	bool m_bHiveReloadLibraries; // if true (default), will reload the DLLs at staggered intervals (based on a fraction of the reload interval)
	int m_nHiveReloadInterval; // in seconds, defaults to 1 day.  If there are 7 objects in the hive, each gets reloaded once per day, each 1/7 of a day after another in sequence, until they are all reset within 1 day.
	int m_nHiveReloadCheckInterval; // in seconds, interval on which to check if the reload interval has elapsed.

};

#endif // !defined(AFX_PROSPEROSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
