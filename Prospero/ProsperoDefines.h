// ProsperoDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(PROSPERODEFINES_H_INCLUDED)
#define PROSPERODEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define PROSPERO_CURRENT_VERSION		"1.2.0.13"


// modes
#define PROSPERO_MODE_DEFAULT			0x00000000  // exclusive
#define PROSPERO_MODE_LISTENER		0x00000001  // exclusive
#define PROSPERO_MODE_CLONE				0x00000002  // exclusive
#define PROSPERO_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define PROSPERO_MODE_VOLATILE		0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define PROSPERO_MODE_MASK				0x0000000f  // 

// default port values.
//#define PROSPERO_PORT_FILE				80		
#define PROSPERO_PORT_CMD					10680		
#define PROSPERO_PORT_STATUS			10681		

#define PROSPERO_PORT_INVALID			0	

#define PROSPERO_FLAG_DISABLED		0x0000	 // default
#define PROSPERO_FLAG_ENABLED			0x0001	
#define PROSPERO_FLAG_FOUND				0x1000
#define PROSPERO_FLAG_NEW					0x0100
#define PROSPERO_FLAG_REFRESH			0x0200

#define PROSPERO_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded

// event status
#define PROSPERO_EVENT_NEW									0x00000000  // no flags yet
//#define PROSPERO_EVENT_ACQUIRED							0x00000001  // the m_event member has been assigned
#define PROSPERO_EVENT_INSERTED							0x00000002  // inserted into DB
#define PROSPERO_EVENT_PROCESSED						0x00000004  // processed since last status change
#define PROSPERO_EVENT_ALLOCATED						0x00000008  // new memory object
#define PROSPERO_EVENT_DELETING							0x00000010  // deleting out of DB 
#define PROSPERO_EVENT_DELETED							0x00000020  // deleted out of DB 


// status
#define PROSPERO_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define PROSPERO_STATUS_UNKNOWN							0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define PROSPERO_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define PROSPERO_STATUS_ERROR								0x00000020  // error (red icon)
#define PROSPERO_STATUS_CONN								0x00000030  // ready (green icon)	
#define PROSPERO_STATUS_OK									0x00000030  // ready (green icon)	
#define PROSPERO_STATUS_RUN									0x00000040  // in progress, running, owned etc (blue icon);	
#define PROSPERO_ICON_MASK									0x00000070  // mask	

#define PROSPERO_STATUS_SUSPEND							0x00000080  // suspended	(yellow icon please)

#define PROSPERO_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define PROSPERO_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define PROSPERO_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define PROSPERO_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define PROSPERO_STATUS_CMDSVR_MASK					0x00007000  // command server mask bits

#define PROSPERO_STATUS_STATUSSVR_START			0x00010000  // starting the status server
#define PROSPERO_STATUS_STATUSSVR_RUN				0x00020000  // status server running
#define PROSPERO_STATUS_STATUSSVR_END				0x00030000  // status server shutting down
#define PROSPERO_STATUS_STATUSSVR_ERROR			0x00040000  // status server error
#define PROSPERO_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define PROSPERO_STATUS_THREAD_START				0x00100000  // starting the main thread
#define PROSPERO_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define PROSPERO_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define PROSPERO_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define PROSPERO_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define PROSPERO_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define PROSPERO_STATUS_THREAD_MASK					0x00f00000  // main thread mask bits

// various failures...
#define PROSPERO_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define PROSPERO_STATUS_FAIL_DB							0x20000000  // could not get DB
#define PROSPERO_STATUS_FAIL_MASK						0xf0000000  // failure code mask bits

//return values
#define PROSPERO_SUCCESS   0
#define PROSPERO_ERROR	   -1


//partition defines.
#define PROSPERO_PART_NONE   -1
#define PROSPERO_PART_VIDEO   0
#define PROSPERO_PART_AUDIO	  1
#define PROSPERO_PART_FONTS	  2


// default filenames
#define PROSPERO_SETTINGS_FILE_SETTINGS	  "prospero.csr"		// csr = cortex settings redirect
#define PROSPERO_SETTINGS_FILE_DEFAULT	  "prospero.csf"		// csf = cortex settings file

// debug defines
#define PROSPERO_DEBUG_QUEUE				0x00000001
#define PROSPERO_DEBUG_INSERT				0x00000002
#define PROSPERO_DEBUG_INSERTTIME	  0x00000004
#define PROSPERO_DEBUG_TIME					0x00000008
#define PROSPERO_DEBUG_HIVE					0x00000010
#define PROSPERO_DEBUG_COMM					0x00000200

#define PROSPERO_DEBUG_MSG_NET			0x00001000
#define PROSPERO_DEBUG_MSG_DB				0x00002000
#define PROSPERO_DEBUG_MSG_IS2			0x00100000



#endif // !defined(PROSPERODEFINES_H_INCLUDED)
