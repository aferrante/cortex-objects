// ProsperoData.cpp: implementation of the CProsperoData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <process.h>
#include "Prospero.h"
#include "ProsperoHandler.h" 
#include "ProsperoMain.h" 
#include "ProsperoData.h"
#include <sys/stat.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CIS2Comm g_miranda;  //global miranda object
extern CIS2Hive g_mirandaHive; // global Miranda hive object

extern bool g_bKillThread;
extern bool g_bThreadStarted;

extern CProsperoMain* g_pprospero;
extern CProsperoApp theApp;
//extern CADC g_adc; 	// the Harris ADC object

//extern void ProsperoConnectionThread(void* pvArgs);
extern void ProsperoAsynchConnThread(void* pvArgs);

//////////////////////////////////////////////////////////////////////
// CProsperoDestinationMediaObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProsperoDestinationMediaObject::CProsperoDestinationMediaObject()
{
	m_pszFileName	 = NULL;
	m_pszEncodedFileName = NULL;
	m_nPartition = PROSPERO_PART_NONE;
	m_nFlags = PROSPERO_EVENT_NEW;
	m_nFileDate=-1;  
	m_dblFileSize=-1.0;

	m_nDirectLastUsed=-1;
	m_nDirectTimesUsed=-1;  
}

CProsperoDestinationMediaObject::~CProsperoDestinationMediaObject()
{
	if(m_pszFileName) free(m_pszFileName); // must use malloc to allocate
	if(m_pszEncodedFileName) free(m_pszEncodedFileName); // must use malloc to allocate
}


//////////////////////////////////////////////////////////////////////
// CProsperoDestinationObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProsperoDestinationObject::CProsperoDestinationObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
//	m_pszClientName = NULL;
	m_ulStatus	= PROSPERO_STATUS_UNINIT;
	m_ulFlags = PROSPERO_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
//	m_pAPIConn = NULL;

	m_dblDiskKBFree = -1.0;  
	m_dblDiskKBTotal = -1.0; 
	m_dblDiskPercent = 90.0; 
	m_nChannelID = -1;
	m_nItemID = -1;
	m_nLastDiskCheck = 0;
	
	m_bKillConnThread = true;
	m_bConnThreadStarted = false;

	m_ppmedia = NULL;
	m_nNumFiles =0;
	m_nLastFound = -1;
	m_timebLastFileRefresh.time = 0; // the time of the last file listing 
}

CProsperoDestinationObject::~CProsperoDestinationObject()
{
	m_bKillConnThread = true;
//	while(	m_bConnThreadStarted ) Sleep(1);
	int i=0;
	if(m_ppmedia)
	{
		while(i<m_nNumFiles)
		{
			if(m_ppmedia[i]) delete m_ppmedia[i];
			i++;
		}
		delete [] m_ppmedia;
	}

	m_ppmedia = NULL;


	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
//	if(m_pszClientName) free(m_pszClientName); // must use malloc to allocate
}


int CProsperoDestinationObject::FindMedia(CProsperoDestinationMediaObject* pMedia)  //returns index
{
	if((pMedia)&&(m_ppmedia)&&(m_nNumFiles>0))
	{
		int i=m_nLastFound+1, q=0;
		while(q<m_nNumFiles)
		{
			if(i>=m_nNumFiles) i=0;
			if(
				  (m_ppmedia[i])
				&&(m_ppmedia[i]->m_nPartition==pMedia->m_nPartition)
				&&(
					  ((m_ppmedia[i]->m_pszFileName)&&(pMedia->m_pszFileName)&&(stricmp(m_ppmedia[i]->m_pszFileName, pMedia->m_pszFileName)==0))
					||((m_ppmedia[i]->m_pszFileName==NULL)&&(pMedia->m_pszFileName==NULL))
					)
				)
			{
				m_nLastFound = i;
				return i;
			}
			else
			{
			 i++;
			}
			q++;
		}
	}
	return PROSPERO_ERROR;
}


int CProsperoDestinationObject::InsertMedia(CProsperoDestinationMediaObject* pMedia, int index)  // -1 = add to end.
{
	if(pMedia)
	{
		CProsperoDestinationMediaObject** ppObj = new CProsperoDestinationMediaObject*[m_nNumFiles+1];
		if(ppObj)
		{
			CProsperoDestinationMediaObject** ppDelObj = m_ppmedia;
			int o=0;
			if((m_ppmedia)&&(m_nNumFiles>0))
			{
				if((index<0)||(index>=m_nNumFiles))
				{
					while(o<m_nNumFiles)
					{
						ppObj[o] = m_ppmedia[o];
						o++;
					}
					ppObj[m_nNumFiles] = pMedia;
					index = m_nNumFiles;
				}
				else
				{
					while(o<index)
					{
						ppObj[o] = m_ppmedia[o];
						o++;
					}
					ppObj[o] = pMedia;
					while(o<m_nNumFiles)
					{
						ppObj[o+1] = m_ppmedia[o];
						o++;
					}
				}
			}
			else
			{
				ppObj[m_nNumFiles] = pMedia;  // just add the one
				index = m_nNumFiles;
			}

			m_ppmedia = ppObj;
			if(ppDelObj) delete [] ppDelObj;
			m_nNumFiles++;
			return index;
		}
	}

	return PROSPERO_ERROR;
}

int CProsperoDestinationObject::DeleteMedia(int index)  // removes from array
{
	if((index>=0)&&(index<m_nNumFiles)&&(m_ppmedia))
	{
		CProsperoDestinationMediaObject** ppDelObj = m_ppmedia;
		if(m_nNumFiles>1)
		{
			CProsperoDestinationMediaObject** ppObj = new CProsperoDestinationMediaObject*[m_nNumFiles-1];
			if(ppObj)
			{
				CProsperoDestinationMediaObject* pDelEventObj = NULL;
				m_nNumFiles--;
				int o=0;
				while(o<index)
				{
					ppObj[o] = m_ppmedia[o];
					o++;
				}
				pDelEventObj = m_ppmedia[o];
				while(o<m_nNumFiles)
				{
					ppObj[o] = m_ppmedia[o+1];
					o++;
				}

				m_ppmedia = ppObj;
				if(ppDelObj) delete [] ppDelObj;
				if(pDelEventObj) delete pDelEventObj;
				return PROSPERO_SUCCESS;
			}
		}
		else
		{
			m_nNumFiles = 0;
			m_ppmedia = NULL;
			if(ppDelObj) delete [] ppDelObj;
			return PROSPERO_SUCCESS;
		}
	}
	return PROSPERO_ERROR;
}

int CProsperoDestinationObject::UpdateMediaSQL(int index, CDBUtil* pdb,	CDBconn* pdbConn, char* pszPartition)
{
	int nRV = PROSPERO_ERROR;

	if(
		  (index>=0)
		&&(index<m_nNumFiles)
		&&(m_ppmedia)
		&&(m_ppmedia[index])
		&&(pdb)
		&&(pdbConn)
		&&(m_pszServerName)
		&&(pszPartition)
		)
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char dberrorstring[DB_ERRORSTRING_LEN];

// change all of following to build string based on only what has changed
//		int nClock = clock();
//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "%s ProsperoChannelThread event: Updating %d [%s]", m_pszDesc, index, pEvent->m_event.m_pszID); // Sleep(50); //(Dispatch message)


		if(m_ppmedia[index]->m_nFlags&PROSPERO_EVENT_INSERTED)
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET transfer_date = %d, file_size = %f WHERE host = '%s' AND file_name = '%s' AND partition = '%s'", 
				((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
				m_ppmedia[index]->m_nFileDate,
				m_ppmedia[index]->m_dblFileSize,
				m_pszServerName,
				m_ppmedia[index]->m_pszEncodedFileName,											
				pszPartition
				);  

//			EnterCriticalSection(&g_pprospero->m_data.m_critSQL);
			if((!g_bKillThread)&&(g_bThreadStarted))
			{
if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_INSERT) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Update entry for %s:%s:  SQL: %s",	m_pszServerName, pszPartition, szSQL);  // Sleep(250);//(Dispatch message)
//if(g_pprospero->m_settings.m_bDebugSQL) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, m_pszSource, "ProsperoChannelThread insert: %s", szSQL); // Sleep(50); //(Dispatch message)
				if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
				{
					//**MSG
g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:debug", "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
				}
				else
				{
					nRV = PROSPERO_SUCCESS;
				}
			}
//g_pprospero->m_msgr.DM(MSG_ICONUSER1, NULL, "Prospero:debug", "after insert -> Event %d", n);//  Sleep(50); //(Dispatch message)
//			LeaveCriticalSection(&g_pprospero->m_data.m_critSQL);
		}
		else if(m_ppmedia[index]->m_pszEncodedFileName)
		{
			// if direct is installed and there are stats, then we should update them, it menas a refresh was done.
			if(
				  (g_pprospero->m_settings.m_bDiReCTInstalled)
				&&(m_ppmedia[index]->m_nDirectTimesUsed>=0) // if this is less than zero, it's because we didnt get statistics, its just a listing update.
				)
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
host, \
file_name, \
transfer_date, \
partition, \
file_size, \
Direct_local_last_used, \
Direct_local_times_used) VALUES ('%s', '%s', %d, '%s', %f, %d, %d)",   //HARDCODE
					((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
					m_pszServerName,
					m_ppmedia[index]->m_pszEncodedFileName,											
					m_ppmedia[index]->m_nFileDate,
					pszPartition,
					m_ppmedia[index]->m_dblFileSize,
					m_ppmedia[index]->m_nDirectLastUsed,
					m_ppmedia[index]->m_nDirectTimesUsed
					);

			}
			else // ignore the direct status fields
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
host, \
file_name, \
transfer_date, \
partition, \
file_size) VALUES ('%s', '%s', %d, '%s', %f)",   //HARDCODE
					((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
					m_pszServerName,
					m_ppmedia[index]->m_pszEncodedFileName,											
					m_ppmedia[index]->m_nFileDate,
					pszPartition,
					m_ppmedia[index]->m_dblFileSize
					);
			
			}
			//			EnterCriticalSection(&g_pprospero->m_data.m_critSQL);
			if((!g_bKillThread)&&(g_bThreadStarted))
			{
//if(g_pprospero->m_settings.m_bDebugSQL) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, m_pszSource, "ProsperoChannelThread insert: %s", szSQL); // Sleep(50); //(Dispatch message)
if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_INSERT) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Insert entry for %s:%s:  SQL: %s",	m_pszServerName, pszPartition, szSQL);  // Sleep(250);//(Dispatch message)
				if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
				{
					//**MSG
g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:debug", "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
				}
				else
				{
					m_ppmedia[index]->m_nFlags |= PROSPERO_EVENT_INSERTED;
					nRV = PROSPERO_SUCCESS;
				}
			}
//g_pprospero->m_msgr.DM(MSG_ICONUSER1, NULL, "Prospero:debug", "after insert -> Event %d", n);//  Sleep(50); //(Dispatch message)
//			LeaveCriticalSection(&g_pprospero->m_data.m_critSQL);
//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "%s ProsperoChannelThread event: Updated %d [%s] %d ms", m_pszDesc, index, pEvent->m_event.m_pszID, clock()-nClock); // Sleep(50); //(Dispatch message)

//AfxMessageBox("updated");

		}
		// else cant update anyway

	}
	return nRV;
}

int CProsperoDestinationObject::SetAllUnprocessed()
{
	if((m_ppmedia)&&(m_nNumFiles>0))
	{
		int i=0;
		while(i<m_nNumFiles)
		{
			if(m_ppmedia[i]) m_ppmedia[i]->m_nFlags &= ~PROSPERO_EVENT_PROCESSED;
			i++;
		}
	}
	return PROSPERO_SUCCESS;
}

int CProsperoDestinationObject::SetAllUninserted(int nPartition)
{
	if((m_ppmedia)&&(m_nNumFiles>0))
	{
		int i=0;
		while(i<m_nNumFiles)
		{
			if((m_ppmedia[i])&&(m_ppmedia[i]->m_nPartition==nPartition)) m_ppmedia[i]->m_nFlags &= ~PROSPERO_EVENT_INSERTED;
			i++;
		}
	}
	return PROSPERO_SUCCESS;
}

int CProsperoDestinationObject::RemoveAllEvents(int nPartition)
{
	if((m_ppmedia)&&(m_nNumFiles>0))
	{
		int i=0;
		while(i<m_nNumFiles)
		{
			if((m_ppmedia[i])&&(m_ppmedia[i]->m_nPartition==nPartition))
			{
				DeleteMedia(i);
			}
			else
			{
				i++;
			}
		}
	}
	return PROSPERO_SUCCESS;
}



/*
//////////////////////////////////////////////////////////////////////
// CProsperoChannelObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProsperoChannelObject::CProsperoChannelObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_ulStatus	= PROSPERO_STATUS_UNINIT;
	m_ulFlags = PROSPERO_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
	m_nChannelID = -1; // unassigned
	m_nHarrisListID = -1; // unassigned
	m_pbKillConnThread = NULL;
	m_bKillChannelThread = true;
	m_bChannelThreadStarted = false;
	m_pAPIConn = NULL;
}

CProsperoChannelObject::~CProsperoChannelObject()
{
	m_bKillChannelThread = true;
	while(	m_bChannelThreadStarted ) Sleep(1);

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
}
*/



//////////////////////////////////////////////////////////////////////
// CProsperoData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProsperoData::CProsperoData()
{
	InitializeCriticalSection(&m_critText);

	m_ppDestObj = NULL;
	m_nNumDestinationObjects = 0;
//	m_ppChannelObj = NULL;
//	m_nNumChannelObjects = 0;

		// messaging...
	m_bNetworkMessagingInitialized=false;

	m_bCommandQueueThreadStarted = false;

	m_bCheckModsWarningSent = false;
	m_bCheckMsgsWarningSent = false;
//	m_bCheckAsRunWarningSent = false;

	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebAutoPurge ); // the last time autopurge was run
	_ftime( &m_timebTick );
	m_pszHost = NULL;			// the name of the host
	m_pszCompleteHost = NULL;	// the complete name of the host
	m_pszCortexHost = NULL;			// the name of the cortex host
	m_usCortexCommandPort = PROSPERO_PORT_CMD;
	m_usCortexStatusPort = PROSPERO_PORT_STATUS;
	m_nSettingsMod = -1;
//	m_nChannelsMod = -1;
	m_nDestinationsMod = -1;
	m_nQueueMod = -1;
	m_nLastSettingsMod = -1;
//	m_nLastChannelsMod = -1;
	m_nLastDestinationsMod = -1;
	m_nLastQueueMod = -1;
	m_pdb = NULL;
	m_pdbConn = NULL;
//	m_pdb2 = NULL;
//	m_pdb2Conn = NULL;

	m_bQuietKill = false;
	m_bProcessSuspended = false;
	m_nMaxLicensedDevices = 2;

	_ftime(&m_timebCommandQueueTick);

}

CProsperoData::~CProsperoData()
{
	if(m_ppDestObj)
	{
		int i=0;
		while(i<m_nNumDestinationObjects)
		{
			if(m_ppDestObj[i]) delete m_ppDestObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppDestObj; // delete array of pointers to objects, must use new to allocate
	}
/*
	if(m_ppChannelObj)
	{
		int i=0;
		while(i<m_nNumChannelObjects)
		{
			if(m_ppChannelObj[i]) delete m_ppChannelObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppChannelObj; // delete array of pointers to objects, must use new to allocate
	}
*/
	if(m_pszHost) free(m_pszHost);	// must use malloc to allocate
	if(m_pszCompleteHost) free(m_pszCompleteHost);	// must use malloc to allocate
	if(m_pszCortexHost) free(m_pszCortexHost);	// must use malloc to allocate

	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);
}

char* CProsperoData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CProsperoData::SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError)
{
	int nRV = CX_ERROR;
	bool bError=false;

	if(!bOverwriteExistingError)
	{
		if(
			  ((m_ulFlags&PROSPERO_ICON_MASK) == PROSPERO_STATUS_ERROR)
			||((m_ulFlags&PROSPERO_STATUS_CMDSVR_MASK) == PROSPERO_STATUS_CMDSVR_ERROR)
			||((m_ulFlags&PROSPERO_STATUS_STATUSSVR_MASK) ==  PROSPERO_STATUS_STATUSSVR_ERROR)
			||((m_ulFlags&PROSPERO_STATUS_THREAD_MASK) == PROSPERO_STATUS_THREAD_ERROR)
			||((m_ulFlags&PROSPERO_STATUS_FAIL_MASK) == PROSPERO_STATUS_FAIL_DB)
			)	bError=true;
	}

	if (ulStatus&PROSPERO_ICON_MASK)
	{
		m_ulFlags &= ~PROSPERO_ICON_MASK;
		m_ulFlags |= (ulStatus&PROSPERO_ICON_MASK);
	}
	if (ulStatus&PROSPERO_STATUS_CMDSVR_MASK)
	{
		m_ulFlags &= ~PROSPERO_STATUS_CMDSVR_MASK;
		m_ulFlags |= (ulStatus&PROSPERO_STATUS_CMDSVR_MASK);
	}
	if (ulStatus&PROSPERO_STATUS_STATUSSVR_MASK)
	{
		m_ulFlags &= ~PROSPERO_STATUS_STATUSSVR_MASK;
		m_ulFlags |= (ulStatus&PROSPERO_STATUS_STATUSSVR_MASK);
	}
	if (ulStatus&PROSPERO_STATUS_THREAD_MASK)
	{
		m_ulFlags &= ~PROSPERO_STATUS_THREAD_MASK;
		m_ulFlags |= (ulStatus&PROSPERO_STATUS_THREAD_MASK);
	}
	if (ulStatus&PROSPERO_STATUS_FAIL_MASK)
	{
		m_ulFlags &= ~PROSPERO_STATUS_FAIL_MASK;
		m_ulFlags |= (ulStatus&PROSPERO_STATUS_FAIL_MASK);
	}

	if(bError)
	{
		m_ulFlags &= ~PROSPERO_ICON_MASK;
		m_ulFlags |= PROSPERO_STATUS_ERROR;
	}

	if((pszText)&&(!bError))
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
//				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
//			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_pprospero->m_settings.m_pszIconPath)&&(strlen(g_pprospero->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_pprospero->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_pprospero->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_pprospero->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_pprospero->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_pprospero->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}

/*
int	CProsperoData::SetStatusText(char* pszText, unsigned long ulStatus)
{
	int nRV = CX_ERROR;
	if(pszText)
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_pprospero->m_settings.m_pszIconPath)&&(strlen(g_pprospero->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_pprospero->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_pprospero->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_pprospero->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_pprospero->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_pprospero->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}
*/
// utility
int	CProsperoData::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszCompleteHost) free(m_pszCompleteHost);
						m_pszCompleteHost=pch;

						pch = (char*) malloc(nValue+1);
						if(pch!=NULL)
						{
							if(m_pszHost) free(m_pszHost);
							m_pszHost=pch;

							strcpy(m_pszHost, m_pszCompleteHost);
							pch = strchr(m_pszHost, '.');
							if(pch)
							{
								*pch = 0;
							}

							return PROSPERO_SUCCESS;
						}
					}
				}
			}
		}
	}
	return PROSPERO_ERROR;
}

int CProsperoData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_pprospero)&&(g_pprospero->m_settings.m_pszExchange)&&(strlen(g_pprospero->m_settings.m_pszExchange)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s set mod = \
(SELECT case when max(mod) is null then 1 when max(mod) >= %d \
then 1 else max(mod) + 1 end from %s WHERE criterion = '%s') WHERE criterion = '%s'",
			g_pprospero->m_settings.m_pszExchange,
			PROSPERO_DB_MOD_MAX,
			g_pprospero->m_settings.m_pszExchange,
			szTemp, szTemp		
			);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			return PROSPERO_SUCCESS;
		}
	}
	return PROSPERO_ERROR;
}

int CProsperoData::CheckMessages(char* pszInfo)
{
	if((g_pprospero)&&(m_pdbConn)&&(m_pdb)
		&&(g_pprospero->m_settings.m_pszMessages)&&(strlen(g_pprospero->m_settings.m_pszMessages)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_pprospero->m_settings.m_pszMessages)&&(strlen(g_pprospero->m_settings.m_pszMessages)))?g_pprospero->m_settings.m_pszMessages:"Messages",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_pprospero->m_settings.m_nAutoPurgeMessageDays*86400) // local time....
			);

//		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
//			LeaveCriticalSection(&m_critSQL);
			return PROSPERO_SUCCESS;
		}
//		LeaveCriticalSection(&m_critSQL);
	}
	return PROSPERO_ERROR;
}
/*
int CProsperoData::CheckAsRun(char* pszInfo)
{
	if((g_pprospero)&&(m_pdbConn)&&(m_pdb)
		&&(g_pprospero->m_settings.m_pszAsRun)&&(strlen(g_pprospero->m_settings.m_pszAsRun)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_pprospero->m_settings.m_pszAsRun)&&(strlen(g_pprospero->m_settings.m_pszAsRun)))?g_pprospero->m_settings.m_pszAsRun:"AsRun_Log",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_pprospero->m_settings.m_nAutoPurgeAsRunDays*86400) // local time....
			);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return PROSPERO_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return PROSPERO_ERROR;
}
*/

int CProsperoData::CheckDatabaseMods(char* pszInfo)
{
	if((g_pprospero)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_pprospero->m_settings.m_pszExchange)&&(strlen(g_pprospero->m_settings.m_pszExchange)))?g_pprospero->m_settings.m_pszExchange:"Exchange");

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = PROSPERO_SUCCESS;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCriterion;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
				}

				if((g_pprospero->m_settings.m_pszSettings)&&(strlen(g_pprospero->m_settings.m_pszSettings)))
				{
					szTemp.Format("DBT_%s",g_pprospero->m_settings.m_pszSettings);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nSettingsMod = nReturn;
					}
				}

/*
				if((g_pprospero->m_settings.m_pszChannels)&&(strlen(g_pprospero->m_settings.m_pszChannels)))
				{
					szTemp.Format("DBT_%s",g_pprospero->m_settings.m_pszChannels);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nChannelsMod = nReturn;
					}
				}
*/
				if((g_pprospero->m_settings.m_pszDestinations)&&(strlen(g_pprospero->m_settings.m_pszDestinations)))
				{
					szTemp.Format("DBT_%s",g_pprospero->m_settings.m_pszDestinations);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nDestinationsMod = nReturn;
					}
				}

				if((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))
				{
					szTemp.Format("DBT_%s",g_pprospero->m_settings.m_pszQueue);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nQueueMod = nReturn;
					}
				}
				// get the suspend

				szTemp.Format("Suspend");
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					nReturn = atoi(szMod);
					if(nReturn>0)
					{
						if(!m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Prospero is suspended.");  
							g_pprospero->m_data.SetStatusText(errorstring, CX_STATUS_UNKNOWN);
							g_pprospero->m_msgr.DM(MSG_ICONNONE, NULL, "Prospero:suspend", "*** Prospero has been suspended. ***"); // Sleep(50); //(Dispatch message)
							g_pprospero->SendMsg(CX_SENDMSG_INFO, "Prospero:suspend", "Prospero has been suspended");
							m_bProcessSuspended = true;
						}
					}
					else
					{
						if(m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Prospero is running.");  
							g_pprospero->m_data.SetStatusText(errorstring, CX_STATUS_OK);
							g_pprospero->m_msgr.DM(MSG_ICONNONE, NULL, "Prospero:resume", "*** Prospero has been resumed. ***");//  Sleep(50); //(Dispatch message)
							g_pprospero->SendMsg(CX_SENDMSG_INFO, "Prospero:resume", "Prospero has been resumed.");
							m_bProcessSuspended = false;
						}
					}
				}

				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			delete prs;
			return nReturn;
		}
	}
	return PROSPERO_ERROR;
}


int CProsperoData::GetQueue(char* pszInfo)
{
	// following removed to deal with wait mode below.
/*
	if(g_miranda.m_bTransferring)
	{
		// something in progress, must wait.
		return PROSPERO_ERROR;
	}
*/
	
	if((g_pprospero)&&(m_pdbConn)&&(m_pdb))
	{

		// let's just get the top (oldest timestamp) item in the queue, and deal with it.

		_ftime( &g_pprospero->m_data.m_timebTick );

		char szSQL[DB_SQLSTRING_MAXLEN];
		char szSQLTemp[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE action <> 128 ORDER BY timestamp ASC", //HARDCODE
			((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue");

//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
//action int, host varchar(64), timestamp float, username varchar(32));

		CString szFilenameLocal = "";
		CString szFilenameRemote = "";
		CString szServer = "";
		CString szUsername = "";
		CString szMessage = "";
		CString szTemp = "";
		int nItemID = -1;
		bool bItemFound = false;
		double dblTimestamp=-57.0;   // timestamp
		int nAction = -1;
		int nTemp;
		int nRetries=0;
		int nEventItemID=-1;
		int nRequiredHiveIndex=-1;
		int nDirection = -1;

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = PROSPERO_ERROR;
			int nIndex = 0;
//			while ((!prs->IsEOF()))
			if ((!prs->IsEOF()))  // just do the one record, if there is one
			{
				try
				{
					prs->GetFieldValue("itemid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nItemID = atoi(szTemp);
						bItemFound = true;
					}
					prs->GetFieldValue("filename_local", szFilenameLocal);//HARDCODE
					szFilenameLocal.TrimLeft(); szFilenameLocal.TrimRight();
					prs->GetFieldValue("filename_remote", szFilenameRemote);//HARDCODE
					szFilenameRemote.TrimLeft(); szFilenameRemote.TrimRight();
					prs->GetFieldValue("action", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nAction = nTemp;
					}
					prs->GetFieldValue("host", szServer);//HARDCODE
					szServer.TrimLeft(); szServer.TrimRight();

					prs->GetFieldValue("timestamp", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						dblTimestamp = atof(szTemp);
					}
					prs->GetFieldValue("username", szUsername);//HARDCODE
					szUsername.TrimLeft(); szUsername.TrimRight();
					prs->GetFieldValue("event_itemid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nEventItemID = nTemp;
					}

					prs->GetFieldValue("message", szMessage);//HARDCODE
					if(szMessage.GetAt(0)=='R')
					{
						nRetries = atoi(szMessage.Mid(1));
						if(nAction==32)
						{
							// now extract it from the retries.
							int nPipe =  szMessage.Find("|");
							if(nPipe>=0)
							{
								nRequiredHiveIndex = atoi(szMessage.Mid(nPipe+1));
								nDirection = ((int)(atof(szMessage.Mid(nPipe+1))*10.0))%10;
							}
							else // not sure, this is a prob if it happens.
							{
		g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:debug", "Error: required hive index not found: %s", szMessage);  // Sleep(250);//(Dispatch message)

							}
						}

					}
					else
					if(nAction==32)
					{
						nRequiredHiveIndex = atoi(szMessage);

						nDirection = ((int)(atof(szMessage)*10.0))%10;

					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;

	

if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_QUEUE)
{
	if(nIndex==1)
		g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Got item from queue: [item=%d, action=%d, local=%s, remote=%s, host=%s, user=%s, message=%s, time=%.3f]",nItemID, nAction, szFilenameLocal, szFilenameRemote, szServer, szUsername, szMessage, dblTimestamp);  // Sleep(250);//(Dispatch message)
	else
		g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Got %d things from queue", nIndex);  // Sleep(250);//(Dispatch message)
}

			if((nReturn>0)&&(bItemFound))
			{
				// process the request, then remove it from the queue
//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "got item %d from queue", nItemID);   Sleep(250);//(Dispatch message)
		_ftime( &g_pprospero->m_data.m_timebTick );

				if(szServer.GetLength()>0)
				{
					char* pchServer = szServer.GetBuffer(1);
					int nDestIndex = ReturnDestinationIndex(pchServer);
//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "got server %s from queue", pchServer);   Sleep(250);//(Dispatch message)

					if((nDestIndex>=0)&&(m_ppDestObj)&&(m_ppDestObj[nDestIndex])&&(m_ppDestObj[nDestIndex]->m_ulFlags&PROSPERO_FLAG_ENABLED))
					{  
						// only do anything if its enabled.
//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "enabled");   Sleep(250);//(Dispatch message)

						int nHostReturn = PROSPERO_ERROR;
						int nHiveIndex =-1;

						CIS2Comm* pMirandaObject = &g_miranda;

						if(g_pprospero->m_settings.m_nNumberOfHiveObjects > 0)
						{
							EnterCriticalSection(&g_mirandaHive.m_critHive);

							switch(nAction)
							{
							case 32: // wait on transfer
								{
									nHiveIndex  = g_mirandaHive.AccessHive(nRequiredHiveIndex);  // need a specific hive object to check on transfer.
								} break;
							case 1:  // transfer to
							case 2:  // transfer from
								{
									// don;t set up more transfers if we already have one going on on a particular host.
									if( g_mirandaHive.CheckHostInHive(pchServer) > OX_ERROR)
									{
										// report 
if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_HIVE)
{
	g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:CheckHostInHive", "(%d) A transfer was already in progress on %s", nAction, pchServer);  // Sleep(250);//(Dispatch message)
}

										
									}
									else
									{
										nHiveIndex  = g_mirandaHive.AccessHive();
									}
								} break;
							default: // all else.
								{
									nHiveIndex  = g_mirandaHive.AccessHive();
								} break;
							}
							
							if(nHiveIndex>OX_ERROR)
							{	
								pMirandaObject = g_mirandaHive.m_ppIs2Comm[nHiveIndex];
								LeaveCriticalSection(&g_mirandaHive.m_critHive);

if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_HIVE)
		g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:AccessHive", "Returned object %d at address 0x%08x", nHiveIndex, pMirandaObject);  // Sleep(250);//(Dispatch message)

							}
							else
							{
									// something in progress, must wait. -  just return negative for now
								szServer.ReleaseBuffer();
								if(g_pprospero->m_settings.m_nErrorDelayMS>0)
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET timestamp = %.3f WHERE itemid = %d",  //HARDCODE
										((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
//												statbuffer.st_mtime,
//												statbuffer.st_size,
										dblTimestamp+(((double)(g_pprospero->m_settings.m_nErrorDelayMS))/1000.0),
										nItemID
										);
								}
								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
								{
									//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL while reprioritizing command: %s", errorstring); // Sleep(250); //(Dispatch message)

								}
								LeaveCriticalSection(&g_mirandaHive.m_critHive);


if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_HIVE)
{
	if(nAction==32)
		g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:AccessHive", "Hive object %d not available", nRequiredHiveIndex);  // Sleep(250);//(Dispatch message)
	else
		g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:AccessHive", "No available hive object");  // Sleep(250);//(Dispatch message)
}
								return PROSPERO_ERROR;
								
							}

						}

						if((pMirandaObject->m_pszHost)&&(strlen(pMirandaObject->m_pszHost))&&(strcmp(pMirandaObject->m_pszHost,pchServer)==0))
						{
							// no need to do anything.
							nHostReturn = PROSPERO_SUCCESS;
						}
						else
						{
							nHostReturn = pMirandaObject->SetHost(pchServer);
						}

						if(nHostReturn>=PROSPERO_SUCCESS)
						{

		_ftime( &g_pprospero->m_data.m_timebTick );
							try
							{
								nHostReturn = pMirandaObject->OxSoxPing();
							}
							catch(...)
							{
								nHostReturn = PROSPERO_ERROR;
g_pprospero->m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Prospero:oxsox_ping", "Exception pinging %s. Code %d, index %d", pchServer, GetLastError(), nHiveIndex );  // Sleep(50);//(Dispatch message)
							}
							//ping the host first.
		_ftime( &g_pprospero->m_data.m_timebTick );
							if(nHostReturn == OX_SUCCESS)
							{
								char pchDirAlias[50];
								char pchRemoteFilenameNoPartition[MAX_PATH]; // without 
								strcpy(pchDirAlias, "$VIDEO");
								strcpy(pchRemoteFilenameNoPartition, "");

								int nDirIndex = szFilenameRemote.Find("/");
							// first thing is, get the disk space statistics.
								if(nDirIndex>0)
								{
									_snprintf(pchRemoteFilenameNoPartition, MAX_PATH, "%s", szFilenameRemote.Mid(nDirIndex+1));
									_snprintf(pchDirAlias, 50, "%s", szFilenameRemote.Left(nDirIndex));
									if(pchDirAlias[0]!='$') // error
										strcpy(pchDirAlias, "$VIDEO"); // default.
								}
								else
								{
									strcpy(pchDirAlias, "$VIDEO"); // default.
								}

								BOOL bReturn;

								if((nDestIndex>=0)&&(m_ppDestObj)&&(m_ppDestObj[nDestIndex]))
								{
									if(
											( 
												(g_pprospero->m_settings.m_nDiskCheckMinIntervalMS>0)
											&&(m_ppDestObj[nDestIndex]->m_nLastDiskCheck + (g_pprospero->m_settings.m_nDiskCheckMinIntervalMS/1000) < g_pprospero->m_data.m_timebTick.time )
											)
										||(g_pprospero->m_settings.m_nDiskCheckMinIntervalMS<=0)  // then just go ahead
										)
									{
								
										if(pMirandaObject->m_lpfnGetDriveInfo)
										{ 

											DiskInfo_ diskinfo;

											try
											{
												bReturn = pMirandaObject->m_lpfnGetDriveInfo(pMirandaObject->m_pszHost, pchDirAlias, &diskinfo);
											}
											catch (...)
											{
												bReturn = FALSE;
		g_pprospero->m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Prospero:oxsox_diskinfo", "Exception getting disk information for %s; item %d (%d), code %d", pchServer, nItemID, nEventItemID, GetLastError() );  // Sleep(50);//(Dispatch message)
											}
									//		AfxMessageBox("Y");
											if(bReturn == FALSE)
											{
												//**MSG
								//				g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_diskinfo", "Could not get disk information for %s partition on %s", pchDirAlias, pchServer ); //  Sleep(50);//(Dispatch message)
			/*
			g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "m_lpfnGetDriveInfo returned FALSE");   Sleep(50);//(Dispatch message)
												g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_diskinfo", "Could not get disk information for %s partition", pchDirAlias );  // Sleep(50);//(Dispatch message)
			g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "m_lpfnGetDriveInfo returned2 FALSE");   Sleep(50);//(Dispatch message)
		*/

		g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_diskinfo", "Could not get disk information for %s; item %d (%d)", pchServer, nItemID, nEventItemID );  // Sleep(50);//(Dispatch message)
		//	g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "m_lpfnGetDriveInfo returned3 FALSE");   Sleep(50);//(Dispatch message)

											}
											else
											{
													m_ppDestObj[nDestIndex]->m_nLastDiskCheck = g_pprospero->m_data.m_timebTick.time;
		//										if(nDestIndex>=0)
		//										{
													//its a host in our list!
		//											if((m_ppDestObj)&&(m_ppDestObj[nDestIndex]))
		//											{
														m_ppDestObj[nDestIndex]->m_dblDiskKBFree = (double)diskinfo.KBytes_Free;  
														m_ppDestObj[nDestIndex]->m_dblDiskKBTotal = (double)diskinfo.KBytes_Total;  
														// ok, now update DB.  but do not increment counter, that will cause infinite loop.

														if(m_ppDestObj[nDestIndex]->m_nItemID>0) // last known unique item id
														{

			// create table Destinations (destinationid int identity(1,1) NOT NULL, host varchar(64) NOT NULL,
			//				type int, description varchar(256), diskspace_free float, diskspace_total float, diskspace_threshold real,
			//				channelid int, flags int);

															_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET diskspace_free = %f, diskspace_total = %f WHERE destinationid = %d",  //HARDCODE
																((g_pprospero->m_settings.m_pszDestinations)&&(strlen(g_pprospero->m_settings.m_pszDestinations)))?g_pprospero->m_settings.m_pszDestinations:"Destinations",
																m_ppDestObj[nDestIndex]->m_dblDiskKBFree,
																m_ppDestObj[nDestIndex]->m_dblDiskKBTotal,
																m_ppDestObj[nDestIndex]->m_nItemID);
															if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
															{
																//**MSG
		g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

															}
														}
		//											}
		//										}
											}
			//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "after m_lpfnGetDriveInfo ");  // Sleep(50);//(Dispatch message)

										}
										else
										{
											//**MSG
		g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_diskinfo", "Could not get disk information for %s partition on %s; NULL function address.", pchDirAlias, pchServer ); //  Sleep(50);//(Dispatch message)
			//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "m_lpfnGetDriveInfo was NULL"); //  Sleep(50);//(Dispatch message)
										}
									}
								}
	//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "file local %s, file remote %s, action %d", szFilenameLocal, szFilenameRemote, nAction);  Sleep(250); //(Dispatch message)

								char dberrorstring[DB_ERRORSTRING_LEN];
	//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "switching with %d", nAction); //  Sleep(50);//(Dispatch message)
								switch(nAction)
								{
								case 64:// check file on Miranda
									{
										char pchFileRemote[MAX_PATH+1];
										_snprintf(pchFileRemote, MAX_PATH, "%s", szFilenameRemote);
	//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "*** checking %s on %s", pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)
										unsigned nSize, nTimestamp;

										int nReturn = OX_ERROR;
										
										try
										{
											nReturn = pMirandaObject->OxSoxGetFileStats(pchFileRemote, NULL, &nSize, &nTimestamp);
										}
										catch(...)
										{
											nReturn = OX_EXCEPTION;

g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_checkfile", 
												"Exception in GetFileStats for %s on %s; Object %d", pchFileRemote, pchServer, nHiveIndex); //  Sleep(50);//(Dispatch message)

										}

										if((nReturn<OX_SUCCESS)||(nReturn&(OX_NOFILE|OX_EXCEPTION)))
										{

											if((g_pprospero->m_settings.m_nErrorRetries>0)&&(nRetries<g_pprospero->m_settings.m_nErrorRetries))  
											{
												nRetries++;
g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_checkfile", 
												"%s not found on %s, item %d (%d). code %d, object %d.  Retrying %d of %d", pchFileRemote, pchServer, nItemID, nEventItemID, nReturn, nHiveIndex, nRetries, g_pprospero->m_settings.m_nErrorRetries); //  Sleep(50);//(Dispatch message)


												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'R%d', timestamp = %.3f WHERE itemid = %d",  //HARDCODE
														((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
														nRetries, dblTimestamp+(((double)(g_pprospero->m_settings.m_nErrorDelayMS))/1000.0),
														nItemID
														);

											}
											else
											{ // final failure

											//**MSG
g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_checkfile", 
												"%s not found on %s. code %d, object %d", pchFileRemote, pchServer, nReturn, nHiveIndex ); //  Sleep(50);//(Dispatch message)

											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E064:%s not found on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d;",  //HARDCODE
												((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
												pchFileRemote, pchServer, nItemID
												);
											}
										}
										else
										{
											//while(!pMirandaObject->m_bTransferring) Sleep(1);  //not good, in case error

if(g_pprospero->m_settings.m_bReportSuccessfulOperation) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "File check for %s on %s succeeded", pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)
											// we have queued the xfer , lets update the thing with a status, then exit without removing
											
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I064:%s exists on %s.', filename_local = '%d|%d', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
												((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
												pchFileRemote, pchServer, nSize, nTimestamp, nItemID
												);

						/*
						Queue action IDs
						1 transfer to Miranda
						2 transfer from Miranda
						3 delete from Miranda
						4 refresh device listing
						19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
						32 wait on transfer
						64 check file on Miranda
						128 Give results (no action).

	//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
	//action int, host varchar(64), timestamp float, username varchar(32));
						*/
										}
											
										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
										{
											//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

										}

										szServer.ReleaseBuffer();
										return PROSPERO_ERROR; // we dont want to remove the item at the end of this function
										

									} break;
								case 128:// Give results (no action).
									{
										szServer.ReleaseBuffer();
										return PROSPERO_ERROR; // we dont want to remove the item at the end of this function
										// the requestor process will remove this.
									} break;

								case 1:// transfer to Miranda
									{
										if(pMirandaObject->m_bTransferring) // would like to update queue item with % status.
										{
											// something in progress, must wait. -  just return negative for now
											szServer.ReleaseBuffer();
											if(g_pprospero->m_settings.m_nErrorDelayMS>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET timestamp = %.3f WHERE itemid = %d",  //HARDCODE
													((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
	//												statbuffer.st_mtime,
	//												statbuffer.st_size,
													dblTimestamp+(((double)(g_pprospero->m_settings.m_nErrorDelayMS))/1000.0),
													nItemID
													);
												
if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_QUEUE)
{
		g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Reprioritzing (%d ms) transfer to %s: %s",
			g_pprospero->m_settings.m_nErrorDelayMS, pchServer, szSQL);  // Sleep(250);//(Dispatch message)
}

												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
												{
													//**MSG
				g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL while reprioritizing command: %s", errorstring); // Sleep(250); //(Dispatch message)

												}
											}

											return PROSPERO_ERROR;
										}

										char pchFileLocal[MAX_PATH+1];
										char pchFileRemote[MAX_PATH+1];
										_snprintf(pchFileLocal, MAX_PATH, "%s", szFilenameLocal);
										_snprintf(pchFileRemote, MAX_PATH, "%s", szFilenameRemote);
	//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "*** transferring %s to %s on %s", pchFileLocal, pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)


										int nTransferError = 0;
										struct _stat statbuffer;
										if ( _stat( pchFileLocal, &statbuffer ) == -1 )
										{ // just check that the file is there first, before attempting to call the dll or communicate remote.
											nTransferError = 1;
										}
										else
										{
											int nReturn = OX_ERROR;

											try
											{
												nReturn = pMirandaObject->OxSoxPutFile(pchFileLocal, pchFileRemote, NULL);
											}
											catch(...)
											{
												nReturn = OX_ERROR;

	g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_putfile", 
													"Exception in PutFile for %s->%s on %s; Object %d", pchFileLocal, pchFileRemote, pchServer, nHiveIndex); //  Sleep(50);//(Dispatch message)

											}

											if(nReturn<OX_SUCCESS)
											{
												nTransferError = 2;
											}
										}


										if(nTransferError>0)
										{
											//**MSG
g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_putfile", 
												"Could not transfer %s to %s on %s.  %s", pchFileLocal, pchFileRemote, pchServer,((nTransferError==1)?"The file does not exist.":"The oxsox transfer failed.") ); //  Sleep(50);//(Dispatch message)

											// and give the info if it was external.
											if(nEventItemID>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E001:Could not transfer %s to %s on %s.  %s', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
														((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
														pchFileLocal, pchFileRemote, pchServer,
														((nTransferError==1)?"The file does not exist.":"The oxsox transfer failed."),
														nItemID
														);
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
												{
													//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring);//  Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
												return PROSPERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function
											}
										}
										else
										{
											//while(!pMirandaObject->m_bTransferring) Sleep(1);  //not good, in case error

											int nClock = clock()+100;
											while((!pMirandaObject->m_bTransferring)&&(nClock<clock())) // instead of just sleep so we can escape quickly if need to
											{
												Sleep(1);  //not good, in case error
											}
									//		Sleep(100); // let the thread begin transferring.
	//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "*** transfer of %s to %s on %s succeeded", pchFileLocal, pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)
											// we have queued the xfer , lets update the thing with a status, then exit without removing
											
											if(g_pprospero->m_settings.m_nPutDelayMS>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET action = 32, timestamp = %.3f, message = '%d.1' WHERE itemid = %d",  //HARDCODE
													((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
	//												statbuffer.st_mtime,
	//												statbuffer.st_size,
													dblTimestamp+(((double)(g_pprospero->m_settings.m_nPutDelayMS))/1000.0),
													nHiveIndex, nItemID
													);
											}
											else  // otherwise it is immediate, goes to top of queue
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET action = 32, timestamp = -1.0, message = '%d.1' WHERE itemid = %d",  //HARDCODE
													((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
	//												statbuffer.st_mtime,
	//												statbuffer.st_size,
													nHiveIndex, nItemID
													);
											}

						/*
						Queue action IDs
						1 transfer to Miranda
						2 transfer from Miranda
						3 delete from Miranda
						4 refresh device listing
						19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
						32 wait on transfer
						64 check file on Miranda
						128 Give results (no action).

	//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
	//action int, host varchar(64), timestamp float, username varchar(32));
						*/

											
											if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
//											if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
											{
												//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring);  //Sleep(250); //(Dispatch message)

											}

											szServer.ReleaseBuffer();
											return PROSPERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
											// we also dont want to remove the item at the end of this function
										}
									} break;
								case 2:// transfer from Miranda
									{
										if(pMirandaObject->m_bTransferring) // would like to update queue item with % status.
										{
											// something in progress, must wait. -  just return negative for now
											szServer.ReleaseBuffer();
											if(g_pprospero->m_settings.m_nErrorDelayMS>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET timestamp = %.3f WHERE itemid = %d",  //HARDCODE
													((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
	//												statbuffer.st_mtime,
	//												statbuffer.st_size,
													dblTimestamp+(((double)(g_pprospero->m_settings.m_nErrorDelayMS))/1000.0),
													nItemID
													);
											
if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_QUEUE)
{
		g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Reprioritzing (%d ms) transfer from %s: %s",
			g_pprospero->m_settings.m_nErrorDelayMS, pchServer, szSQL);  // Sleep(250);//(Dispatch message)
}

												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
												{
													//**MSG
				g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL while reprioritizing command: %s", errorstring); // Sleep(250); //(Dispatch message)

												}
											}
											return PROSPERO_ERROR;
										}
										char pchFileLocal[MAX_PATH+1];
										char pchFileRemote[MAX_PATH+1];
										_snprintf(pchFileLocal, MAX_PATH, "%s", szFilenameLocal);
										_snprintf(pchFileRemote, MAX_PATH, "%s", szFilenameRemote);

										int nReturn = OX_ERROR;
										
										try
										{
											nReturn = pMirandaObject->OxSoxGetFile(pchFileLocal, pchFileRemote, NULL/*, OX_REPORT*/);
										}
										catch(...)
										{
											nReturn = OX_ERROR;

g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_getfile", 
												"Exception in GetFile for %s<-%s on %s; Object %d", pchFileLocal, pchFileRemote, pchServer, nHiveIndex); //  Sleep(50);//(Dispatch message)

										}

										if(nReturn<OX_SUCCESS)
										{
											//**MSG
g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_getfile", 
												"Could not transfer %s on %s to %s.", pchFileRemote, pchServer, pchFileLocal ); //  Sleep(50);//(Dispatch message)

											// and give the info if it was external.
											if(nEventItemID>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E002:Could not transfer %s on %s to %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
														((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
														pchFileRemote, pchServer, pchFileLocal,
														nItemID
														);
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
												{
													//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
												return PROSPERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function
											}

										}
										else
										{
											int nClock = clock()+100;
											while((!pMirandaObject->m_bTransferring)&&(nClock<clock())) // instead of just sleep so we can escape quickly if need to
											{
												Sleep(1);  //not good, in case error
											}

//											Sleep(100); // let the thread begin transferring.
											// we have queued the xfer , lets update the thing with a status, then exit without removing
											if(g_pprospero->m_settings.m_nGetDelayMS>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET action = 32, timestamp = %.3f, message = '%d.2' WHERE itemid = %d",  //HARDCODE
													((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
	//												statbuffer.st_mtime,
	//												statbuffer.st_size,
													dblTimestamp+(((double)(g_pprospero->m_settings.m_nGetDelayMS))/1000.0),
													nHiveIndex, nItemID
													);
											}
											else  // otherwise it is immediate, goes to top of queue
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET action = 32, timestamp = -1.0, message = '%d.2' WHERE itemid = %d",  //HARDCODE
													((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
													nHiveIndex, nItemID
													);
											}

						/*
						Queue action IDs
						1 transfer to Miranda
						2 transfer from Miranda
						3 delete from Miranda
						4 refresh device listing
						19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
						32 wait on transfer

	//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
	//action int, host varchar(64), timestamp float, username varchar(32));
						*/

											if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
											{
												//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring);//  Sleep(250); //(Dispatch message)

											}

											szServer.ReleaseBuffer();
											return PROSPERO_ERROR;  // because we dont want to increment counter. // there is no counter.
											// we also dont want to remove the item at the end of this function
										}
									} break;
								case 3:// delete from Miranda
									{
										if(pMirandaObject->m_bTransferring) // would like to update queue item with % status.
										{
											// something in progress, must wait. -  just return negative for now
											szServer.ReleaseBuffer();

											if(g_pprospero->m_settings.m_nErrorDelayMS>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET timestamp = %.3f WHERE itemid = %d",  //HARDCODE
													((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
	//												statbuffer.st_mtime,
	//												statbuffer.st_size,
													dblTimestamp+(((double)(g_pprospero->m_settings.m_nErrorDelayMS))/1000.0),
													nItemID
													);

if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_QUEUE)
{
		g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Reprioritzing (%d ms) deletion on %s: %s",
			g_pprospero->m_settings.m_nErrorDelayMS, pchServer, szSQL);  // Sleep(250);//(Dispatch message)
}

												
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
												{
													//**MSG
				g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL while reprioritizing command: %s", errorstring); // Sleep(250); //(Dispatch message)

												}
											}


											return PROSPERO_ERROR;
										}
										char pchFileRemote[MAX_PATH+1];
										_snprintf(pchFileRemote, MAX_PATH, "%s", szFilenameRemote);

										int nReturn = OX_ERROR;
										
										try
										{
											nReturn = pMirandaObject->OxSoxDelFile(pchFileRemote, NULL);
										}
										catch(...)
										{
											nReturn = OX_ERROR;

g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_checkfile", 
												"Exception in DelFile for %s on %s; Object %d", pchFileRemote, pchServer, nHiveIndex); //  Sleep(50);//(Dispatch message)

										}


										if(nReturn<OX_SUCCESS)
										{
											//**MSG
											g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_delfile", 
												"Could not delete %s on %s.", pchFileRemote, pchServer ); //  Sleep(50);//(Dispatch message)
											if(nEventItemID>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E003:Could not delete %s on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
														((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
														szFilenameRemote,
														pchServer, nItemID
														);
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
		//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring);//  Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
												return PROSPERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function

											}

										}
										else


		//								schedule a refresh
										if(
												(g_pprospero->m_data.m_ppDestObj[nDestIndex]->m_pszServerName)
												&&(strlen(g_pprospero->m_data.m_ppDestObj[nDestIndex]->m_pszServerName)) 
												&& ((g_pprospero->m_data.m_ppDestObj[nDestIndex]->m_ulFlags)&PROSPERO_FLAG_ENABLED)  
												)
										{
											if((g_pprospero->m_data.m_ppDestObj[nDestIndex]->m_timebLastFileRefresh.time
														+(int)g_pprospero->m_settings.m_ulFileRefreshInterval) < g_pprospero->m_data.m_timebTick.time)
											{

												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (filename_local, filename_remote, action, host, timestamp, username) \
	VALUES ('','%s', 4, '%s', -2.0, 'sys' )", //HARDCODE
												((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
												pchDirAlias, g_pprospero->m_data.m_ppDestObj[nDestIndex]->m_pszServerName
												);


				//			g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", szSQL);  Sleep(250); //(Dispatch message)
				//					char errorstring[DB_ERRORSTRING_LEN];
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
				//								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
	g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

												}
											}
											else // update just the array. (dont need to do it if we are scheduling the refresh...)
											{
											// if it was a transfer to, we need to add it to the internal array.
												if(szFilenameRemote.GetLength()>0)
												{

													CProsperoDestinationMediaObject NewMedia;

													NewMedia.m_pszFileName = NULL;
													if(strcmp(pchDirAlias,"$VIDEO")==0)
													{
														NewMedia.m_nPartition = PROSPERO_PART_VIDEO;
														NewMedia.m_pszFileName = pchRemoteFilenameNoPartition;
													}
													else
													if(strcmp(pchDirAlias,"$AUDIO")==0)
													{
														NewMedia.m_nPartition = PROSPERO_PART_AUDIO;
														NewMedia.m_pszFileName = pchRemoteFilenameNoPartition;
													}
													else
													if(strcmp(pchDirAlias,"$FONTS")==0)
													{
														NewMedia.m_nPartition = PROSPERO_PART_FONTS;
														NewMedia.m_pszFileName = pchRemoteFilenameNoPartition;
													}

													if(NewMedia.m_pszFileName) // its on a partition we care about
													{
														m_ppDestObj[nDestIndex]->m_nLastFound=-1;

														int nTestEventIndex = m_ppDestObj[nDestIndex]->FindMedia(&NewMedia);

														if(nTestEventIndex>=0)
														{
														// found it so lets delete
															_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE host = '%s' AND partition = '%s' AND file_name = '%s'",
																g_pprospero->m_settings.m_pszDestinationMedia,
																m_ppDestObj[nDestIndex]->m_pszServerName, //server_name
																pchDirAlias, // directory alias
																m_ppDestObj[nDestIndex]->m_ppmedia[nTestEventIndex]->m_pszEncodedFileName
															);
if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_INSERT) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Delete for %s:%s.  SQL: %s",	pchServer,pchDirAlias,szSQL);  // Sleep(250);//(Dispatch message)
//				if(g_pprospero->m_settings.m_bDebugSQL) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread delete: %s", szSQL); // Sleep(50); //(Dispatch message)
//				if(!g_pprospero->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_pprospero->m_data.m_critSQL);
															if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
															{
													//**MSG
g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:delete_media", "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
															}

															m_ppDestObj[nDestIndex]->DeleteMedia(nTestEventIndex);
														}
													} // on a partition we care
													NewMedia.m_pszFileName = NULL;
													NewMedia.m_pszEncodedFileName = NULL;

												}
											}

											// and give the info if it was external.
											if(nEventItemID>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I003:Finished deleting %s on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
														((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
														szFilenameRemote, 
														pchServer, nItemID
														);
												
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
												{
													//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
												return PROSPERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function

											}
										}


									} break;
								case 32:// wait on transfer
									{

if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_QUEUE)
{
		g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Waiting (%d) on transfer (%d) with %s.",
			pMirandaObject->m_bTransferring, nDirection, pchServer);  // Sleep(250);//(Dispatch message)
}
										if(pMirandaObject->m_bTransferring) // would like to update queue item with % status.
										{
											// something in progress, must wait. -  just return negative for now
											szServer.ReleaseBuffer();

											// actually, let's move it back down in priority again.

											

											if(nDirection==1) // put
											{
												if(g_pprospero->m_settings.m_nPutDelayMS>0)
												{

													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET timestamp = %.3f WHERE itemid = %d",  //HARDCODE
														((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
		//												statbuffer.st_mtime,
		//												statbuffer.st_size,
														dblTimestamp+(((double)(g_pprospero->m_settings.m_nPutDelayMS))/1000.0),
														nItemID
														);
if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_QUEUE)
{
		g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Reprioritzing transfer (%d ms) to %s: %s",
			g_pprospero->m_settings.m_nPutDelayMS, pchServer, szSQL);  // Sleep(250);//(Dispatch message)
}
													
													if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
													{
														//**MSG
					g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL while reprioritizing command: %s", errorstring); // Sleep(250); //(Dispatch message)

													}
												}

											}
											else
											if(nDirection==2) // get
											{
												if(g_pprospero->m_settings.m_nGetDelayMS>0)
												{
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET timestamp = %.3f WHERE itemid = %d",  //HARDCODE
														((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
		//												statbuffer.st_mtime,
		//												statbuffer.st_size,
														dblTimestamp+(((double)(g_pprospero->m_settings.m_nGetDelayMS))/1000.0),
														nItemID
														);
													
if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_QUEUE)
{
		g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Reprioritzing transfer (%d ms) from %s: %s",
			g_pprospero->m_settings.m_nGetDelayMS, pchServer, szSQL);  // Sleep(250);//(Dispatch message)
}
													if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
													{
														//**MSG
					g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL while reprioritizing command: %s", errorstring); // Sleep(250); //(Dispatch message)

													}
												}


											}


											return PROSPERO_ERROR;
										}


										// if we are here, it's because we have finished a transfer!  (which could have ended in transfer error)
										// OK, not necessarily now with the hive.  We could be in a new instance that is not transferring.

										// how can we tell.... ?
										// OK now if its a transfer, we require getting the right hive memner that initiated the transfer, so this is now true again.


		//								schedule a refresh
										if((g_pprospero->m_data.m_ppDestObj[nDestIndex]->m_pszServerName)&&(strlen(g_pprospero->m_data.m_ppDestObj[nDestIndex]->m_pszServerName)) && ((g_pprospero->m_data.m_ppDestObj[nDestIndex]->m_ulFlags)&PROSPERO_FLAG_ENABLED)  )
										{

//											int nDirection = atoi(szMessage);

											if((g_pprospero->m_data.m_ppDestObj[nDestIndex]->m_timebLastFileRefresh.time
														+(int)g_pprospero->m_settings.m_ulFileRefreshInterval) < g_pprospero->m_data.m_timebTick.time)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (filename_local, filename_remote, action, host, timestamp, username) \
	VALUES ('','%s', 4, '%s', -2.0, 'sys' )", //HARDCODE
												((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
												pchDirAlias, g_pprospero->m_data.m_ppDestObj[nDestIndex]->m_pszServerName
												);


				//			g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", szSQL);  Sleep(250); //(Dispatch message)
				//					char errorstring[DB_ERRORSTRING_LEN];
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
				//								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring); /// Sleep(250); //(Dispatch message)

												}
											}
											else // update just the array. (dont need to do it if we are scheduling the refresh...)
											{
											// if it was a transfer to, we need to add it to the internal array.
												if((nDirection==1)&&(szFilenameRemote.GetLength()>0)&&(!pMirandaObject->m_bTransferError))  // if error just report it and get out.
												{
														// found it, else cannot add so skip
													CProsperoDestinationMediaObject NewMedia;

													NewMedia.m_pszFileName = NULL;
													if(strcmp(pchDirAlias,"$VIDEO")==0)
													{
														NewMedia.m_nPartition = PROSPERO_PART_VIDEO;
														NewMedia.m_pszFileName = pchRemoteFilenameNoPartition;
													}
													else
													if(strcmp(pchDirAlias,"$AUDIO")==0)
													{
														NewMedia.m_nPartition = PROSPERO_PART_AUDIO;
														NewMedia.m_pszFileName = pchRemoteFilenameNoPartition;
													}
													else
													if(strcmp(pchDirAlias,"$FONTS")==0)
													{
														NewMedia.m_nPartition = PROSPERO_PART_FONTS;
														NewMedia.m_pszFileName = pchRemoteFilenameNoPartition;
													}

													if(NewMedia.m_pszFileName) // it's on a partition we care about
													{
														unsigned nSize, nTimestamp;

														char pchFileRemote[MAX_PATH+1];
														_snprintf(pchFileRemote, MAX_PATH, "%s", szFilenameRemote);

														int nCheckReturn = OX_ERROR;
															
														try
														{
															nCheckReturn = pMirandaObject->OxSoxGetFileStats(pchFileRemote, NULL, &nSize, &nTimestamp);
														}
														catch(...)
														{

															nCheckReturn = OX_ERROR;

g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_checkfilestats", 
												"Exception in GetFileStats for %s on %s; Object %d", pchFileRemote, pchServer, nHiveIndex); //  Sleep(50);//(Dispatch message)

														}

														if(!((nCheckReturn<OX_SUCCESS)||(nCheckReturn&(OX_NOFILE|OX_EXCEPTION))))
														{

															NewMedia.m_dblFileSize = (double)nSize;
															NewMedia.m_nFileDate = nTimestamp;


															m_ppDestObj[nDestIndex]->m_nLastFound=-1;

															bool bUpdateError = false;
															int nTestEventIndex = m_ppDestObj[nDestIndex]->FindMedia(&NewMedia);

															if(nTestEventIndex>=0)
															{
																// exists.
//																m_ppDestObj[nDestIndex]->m_ppmedia[nTestEventIndex]->m_nFlags|= PROSPERO_EVENT_PROCESSED;

																if(
																		(m_ppDestObj[nDestIndex]->m_ppmedia[nTestEventIndex]->m_dblFileSize != NewMedia.m_dblFileSize)
																	||(m_ppDestObj[nDestIndex]->m_ppmedia[nTestEventIndex]->m_nFileDate != NewMedia.m_nFileDate)
																	)
																{
		//if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_INSERT) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Update entry for %s:%s",	pchServer,pchDirAlias);  // Sleep(250);//(Dispatch message)
					//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updating %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID); // Sleep(50); //(Dispatch message)
																	if(m_ppDestObj[nDestIndex]->UpdateMediaSQL(nTestEventIndex, m_pdb,	m_pdbConn, pchDirAlias)<PROSPERO_SUCCESS)
																			bUpdateError = true; 
					//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updated %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID); // Sleep(50); //(Dispatch message)
																	//nUpdates++;
																}
															}
															else
															{
																// have to add.
																CProsperoDestinationMediaObject* pObj = new CProsperoDestinationMediaObject;
																if(pObj)
																{
																	pObj->m_dblFileSize = NewMedia.m_dblFileSize;
																	pObj->m_nFileDate = NewMedia.m_nFileDate;
																	pObj->m_nPartition = NewMedia.m_nPartition;
																	if(NewMedia.m_pszFileName)
																	{
																		pObj->m_pszFileName = (char*)malloc(strlen(NewMedia.m_pszFileName)+1); 
																		if(pObj->m_pszFileName) strcpy(pObj->m_pszFileName, NewMedia.m_pszFileName);

																		pObj->m_pszEncodedFileName = m_pdb->EncodeQuotes(NewMedia.m_pszFileName);
																		
																	} 
																	else
																	{
																		pObj->m_pszEncodedFileName = NULL;
																		pObj->m_pszFileName = NULL;
																	}

																	//insert
																	nTestEventIndex = m_ppDestObj[nDestIndex]->InsertMedia(pObj, -1);
																	if(nTestEventIndex>=0)
																	{	
																		pObj->m_nFlags|= PROSPERO_EVENT_ALLOCATED;

		//if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_INSERT) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Insert entry for %s:%s",	pchServer,pchDirAlias);  // Sleep(250);//(Dispatch message)
					//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updating %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  //Sleep(50); //(Dispatch message)
																		if(m_ppDestObj[nDestIndex]->UpdateMediaSQL(nTestEventIndex, m_pdb,	m_pdbConn, pchDirAlias)<PROSPERO_SUCCESS)
																			bUpdateError = true;
					//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updated %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  //Sleep(50); //(Dispatch message)
														//				nUpdates++;

																	}
																	else //error
																	{
																		delete pObj;
																	}

																}

															} // else added

														} // didnt find on remote device
														else
														{
g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_checkputfile", 
												"%s not found on %s. code %d, object %d", pchFileRemote, pchServer, nCheckReturn, nHiveIndex ); //  Sleep(50);//(Dispatch message)

															pMirandaObject->m_bTransferError = true; // just set this here.
														}
													} // on a partition we care
													NewMedia.m_pszFileName = NULL;
													NewMedia.m_pszEncodedFileName = NULL;
												}
											}

											// and give the info if it was external.
											if(nEventItemID>0)
											{
	//						case 2:// transfer from Miranda
	//						case 1:// transfer to Miranda
												if(pMirandaObject->m_bTransferError) //error!.
												{
													pMirandaObject->m_bTransferError = false; // reset the error so we can move on
													if(nDirection>0)
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E032:Error transferring %s to %s%s%s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
															(nDirection==1)?szFilenameLocal:szFilenameRemote,
															(nDirection==1)?szFilenameRemote:szFilenameLocal,
															(nDirection==1)?" on ":" from ",
															pchServer, nItemID
															);
													}
													else
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E032:Error transferring %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
															szFilenameRemote,
															pchServer, nItemID
															);

													}
												}
												else
												{
													if(nDirection>0)
													{

														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I032:Finished transferring %s to %s%s%s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
															(nDirection==1)?szFilenameLocal:szFilenameRemote,
															(nDirection==1)?szFilenameRemote:szFilenameLocal,
															(nDirection==1)?" on ":" from ",
															pchServer, nItemID
															);
													}
													else
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I032:Finished transferring %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
															szFilenameRemote,
															pchServer, nItemID
															);

													}
												}

	//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Transfer SQL: %s", szSQL);  Sleep(250); //(Dispatch message)
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
	//											if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
												return PROSPERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function

											}
										}

									} break;
								case 19:// purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
									{
									}  // not break;  want to go on to refresh device listing.
								case 4:// refresh device listing
									{
	//create table Destinations_Media (host varchar(64), file_name varchar(256), 
	//transfer_date int, partition varchar(16), file_size float, 
	//Direct_local_last_used int, Direct_local_times_used int);
										// have to do this 3 times, possibly.
if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_INSERT) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Device listing requested for %s", pchServer);  // Sleep(250);//(Dispatch message)
	//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "case 4");   Sleep(50);//(Dispatch message)
										int nTimes = 3;
										int nDirs = 0;


										if(szFilenameRemote.GetLength()>0)
										{ // explicit dir
											if(szFilenameRemote.Compare("$VIDEO")==0)
											{
												nDirs = PROSPERO_PART_VIDEO; nTimes=1;
											}
											else
											if(szFilenameRemote.Compare("$AUDIO")==0)
											{
												nDirs = PROSPERO_PART_AUDIO; nTimes=2; // not really 2 times, just have to be nDirs = nTimes-1;
											}
											else
											if(szFilenameRemote.Compare("$FONTS")==0)
											{
												nDirs = PROSPERO_PART_FONTS; nTimes=3; // not really 3 times, just have to be nDirs = nTimes-1;
											}
										}

										for (int nDir=nDirs; nDir<nTimes; nDir++)
										{
											int nClock = clock();

		_ftime( &g_pprospero->m_data.m_timebTick );
											PV3DirEntry_ pv3DirEntry=NULL;
											int nNumEntries = 0;

											switch(nDir)
											{
											default:
											case PROSPERO_PART_VIDEO:	strcpy(pchDirAlias, "$VIDEO"); break;
											case PROSPERO_PART_AUDIO:	strcpy(pchDirAlias, "$AUDIO"); break;
											case PROSPERO_PART_FONTS:	strcpy(pchDirAlias, "$FONTS"); break;
											}
											
											bool bUpdateError = false;
											bool bDelTemp = true;
											if(pMirandaObject->OxSoxGetDirInfo(pchDirAlias, &pv3DirEntry, &nNumEntries)<OX_SUCCESS)
											{
												//**MSG
												g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_dirinfo", 
													"Could not get directory information for %s partition on %s.", pchDirAlias, pchServer );  // Sleep(50);//(Dispatch message)

												// and give the info if it was external.
												if(nEventItemID>0)
												{
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E004:Could not get directory information for %s partition on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
															pchDirAlias, pchServer,
															nItemID
															);
													if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
													{
														//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring);  //Sleep(250); //(Dispatch message)

													}
													szServer.ReleaseBuffer();
													return PROSPERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
													// we also dont want to remove the item at the end of this function
												}
												bDelTemp = false;  // forces exit on error
											}
											else
											{
if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_INSERTTIME) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Device listing retrieved from %s:%s completed in %d ms", pchServer,pchDirAlias, clock()-nClock);  // Sleep(250);//(Dispatch message)

												m_ppDestObj[nDestIndex]->SetAllUnprocessed();
												m_ppDestObj[nDestIndex]->m_nLastFound=-1; //reset this to the top;

												int nFile = 0;
												if(szFilenameLocal.Compare("REFRESH")==0) 
												{
													if(m_pdb)
													{
														if(g_pprospero->m_settings.m_bDiReCTInstalled)
														{
														// have to actually pull all the ones from the db before deleting them... in order to preserve their statistics for direct!

															// first delete all things on this host and partition in the event array
															m_ppDestObj[nDestIndex]->RemoveAllEvents(nDir);
															// then retrieve the things existing in the db, on that host and partition. add these to the array

															// this is much faster than looping thru the array each time to find and replace the info
															_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
																"SELECT file_name, Direct_local_last_used, Direct_local_times_used FROM %s WHERE host = '%s' AND partition = '%s'", //HARDCODE
																((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
																pchServer, pchDirAlias);

															prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
															if(prs)
															{
																
																while (!prs->IsEOF())
																{
																	try
																	{
																		CProsperoDestinationMediaObject* pObj = new CProsperoDestinationMediaObject;

																		// have to add.
																		if(pObj)
																		{
																			CString szTemp;
																			prs->GetFieldValue("file_name", szTemp);//HARDCODE
																			szTemp.TrimLeft(); szTemp.TrimRight();

																			int nLen = szTemp.GetLength();
																			if(nLen)
																			{
																				pObj->m_pszFileName = (char*)malloc(nLen+1); 
																				if(pObj->m_pszFileName) sprintf(pObj->m_pszFileName, "%s", szTemp);

																				pObj->m_pszEncodedFileName = m_pdb->EncodeQuotes(pObj->m_pszFileName);
																			} 
																			else
																			{
																				pObj->m_pszEncodedFileName = NULL;
																				pObj->m_pszFileName = NULL;
																			}

																			prs->GetFieldValue("Direct_local_last_used", szTemp);//HARDCODE
																			pObj->m_nDirectLastUsed = atol(szTemp);
																			prs->GetFieldValue("Direct_local_times_used", szTemp);//HARDCODE
																			pObj->m_nDirectTimesUsed = atol(szTemp);

																			pObj->m_nPartition = nDir;

																			// see if it exists already


																			int nFind = m_ppDestObj[nDestIndex]->FindMedia(pObj);
																			if(nFind>=0)
																			{
																				// already there!
																				// use biggest statistics
																				if(pObj->m_nDirectLastUsed > m_ppDestObj[nDestIndex]->m_ppmedia[nFind]->m_nDirectLastUsed)
																				{
																					m_ppDestObj[nDestIndex]->m_ppmedia[nFind]->m_nDirectLastUsed =  pObj->m_nDirectLastUsed;
																				}
																				if(pObj->m_nDirectTimesUsed > m_ppDestObj[nDestIndex]->m_ppmedia[nFind]->m_nDirectTimesUsed)
																				{
																					m_ppDestObj[nDestIndex]->m_ppmedia[nFind]->m_nDirectTimesUsed =  pObj->m_nDirectTimesUsed;
																				}
																				//then delete
																				try{delete pObj;} catch(...){}
																			}
																			else
																			{
																				//insert
																				m_ppDestObj[nDestIndex]->InsertMedia(pObj, -1);
																			}
																		}
																	}
																	catch (...)
																	{
																	}
																	prs->MoveNext();
																}
																delete prs;
																prs = NULL;
															}
														}
														//else // direct not installed, we can just blow it away.

														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE host = '%s' AND partition = '%s'",
																(((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media"),
																pchServer, pchDirAlias
															);
														if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
														{
															//**MSG
g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:destination_refresh", "Could not clear tables for %s:%s. Error: %s", pchServer,pchDirAlias,errorstring ); //  Sleep(50);//(Dispatch message)
														}
														else
														{
															m_ppDestObj[nDestIndex]->SetAllUninserted(nDir);
														}
													}
												}

												// success, lets update db.
if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_INSERT) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Device listing returned for %s:%s, returned %d entries", pchServer,pchDirAlias,nNumEntries);  // Sleep(250);//(Dispatch message)
	//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "OxSoxGetDirInfo returned >= OX_SUCCESS" );   Sleep(50);//(Dispatch message)

												nFile = 0;
												CProsperoDestinationMediaObject NewMedia;

												NewMedia.m_nPartition = nDir;//pchDirAlias;

												while(nFile<nNumEntries)  // have to fail out if the temp table wasn't cleared.
												{
			_ftime( &g_pprospero->m_data.m_timebTick );

													NewMedia.m_pszFileName = pv3DirEntry[nFile].FileName;
													NewMedia.m_dblFileSize = (double)pv3DirEntry[nFile].FileSize;
													NewMedia.m_nFileDate = (int)pv3DirEntry[nFile].FileTimeStamp;

													if((strcmp(NewMedia.m_pszFileName, "."))&&(strcmp(NewMedia.m_pszFileName, "..")))  // not a directory
													{
														int nTestEventIndex = m_ppDestObj[nDestIndex]->FindMedia(&NewMedia);
//problem with duplicate records here
														if(nTestEventIndex>=0)
														{
															// exists.
															m_ppDestObj[nDestIndex]->m_ppmedia[nTestEventIndex]->m_nFlags|= PROSPERO_EVENT_PROCESSED;

															if(
																	(m_ppDestObj[nDestIndex]->m_ppmedia[nTestEventIndex]->m_dblFileSize != NewMedia.m_dblFileSize)
																||(m_ppDestObj[nDestIndex]->m_ppmedia[nTestEventIndex]->m_nFileDate != NewMedia.m_nFileDate)
																||(!(m_ppDestObj[nDestIndex]->m_ppmedia[nTestEventIndex]->m_nFlags&PROSPERO_EVENT_INSERTED))  // on a refresh
																)
															{
	//if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_INSERT) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Update entry for %s:%s",	pchServer,pchDirAlias);  // Sleep(250);//(Dispatch message)
				//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updating %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID); // Sleep(50); //(Dispatch message)
																if(m_ppDestObj[nDestIndex]->UpdateMediaSQL(nTestEventIndex, m_pdb,	m_pdbConn, pchDirAlias)<PROSPERO_SUCCESS)
																		bUpdateError = true; 
				//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updated %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID); // Sleep(50); //(Dispatch message)
																//nUpdates++;
															}
														}
														else
														{
															// have to add.
															CProsperoDestinationMediaObject* pObj = new CProsperoDestinationMediaObject;
															if(pObj)
															{
																pObj->m_dblFileSize = NewMedia.m_dblFileSize;
																pObj->m_nFileDate = NewMedia.m_nFileDate;
																pObj->m_nPartition = NewMedia.m_nPartition;
																if(NewMedia.m_pszFileName)
																{
																	pObj->m_pszFileName = (char*)malloc(strlen(NewMedia.m_pszFileName)+1); 
																	if(pObj->m_pszFileName) strcpy(pObj->m_pszFileName, NewMedia.m_pszFileName);

																	pObj->m_pszEncodedFileName = m_pdb->EncodeQuotes(pv3DirEntry[nFile].FileName);
																	
																} 
																else
																{
																	pObj->m_pszEncodedFileName = NULL;
																	pObj->m_pszFileName = NULL;
																}

																//insert
																nTestEventIndex = m_ppDestObj[nDestIndex]->InsertMedia(pObj, -1);
																if(nTestEventIndex>=0)
																{	
																	pObj->m_nFlags|= (PROSPERO_EVENT_PROCESSED|PROSPERO_EVENT_ALLOCATED);

	//if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_INSERT) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Insert entry for %s:%s",	pchServer,pchDirAlias);  // Sleep(250);//(Dispatch message)
				//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updating %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  //Sleep(50); //(Dispatch message)
																	if(m_ppDestObj[nDestIndex]->UpdateMediaSQL(nTestEventIndex, m_pdb,	m_pdbConn, pchDirAlias)<PROSPERO_SUCCESS)
																		bUpdateError = true;
				//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s SentinelChannelThread event: Updated %d [%s]", pChannel->m_pszDesc, n, pList->m_ppEvents[n]->m_pszID);  //Sleep(50); //(Dispatch message)
													//				nUpdates++;

																}
																else //error
																{
																	delete pObj;
																}
															}

														} // else added
													} // else just increment

													nFile++;
												}
												NewMedia.m_pszEncodedFileName = NULL;
												NewMedia.m_pszFileName = NULL;


												// have to go thru and delete and remove all that we have not processed.
												int nNumToDelete=0;

												if(m_ppDestObj[nDestIndex]->m_ppmedia)
												{
													int n=0;
			//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "Deleting events of %d events", pChannel->m_nNumEvents);//  Sleep(50); //(Dispatch message)
													while(n<m_ppDestObj[nDestIndex]->m_nNumFiles)
													{
			//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "Checking processed %s:  %d", m_ppDestObj[nDestIndex]->m_ppmedia[n]->m_pszEncodedID, (m_ppDestObj[nDestIndex]->m_ppmedia[n]->m_nFlags&SENTINEL_EVENT_PROCESSED));
														if(
																(m_ppDestObj[nDestIndex]->m_ppmedia[n])
															&&(!(m_ppDestObj[nDestIndex]->m_ppmedia[n]->m_nFlags&PROSPERO_EVENT_PROCESSED)) 
															&&(m_ppDestObj[nDestIndex]->m_ppmedia[n]->m_nPartition == nDir)
															)
														{
//																if(!(m_ppDestObj[nDestIndex]->m_ppmedia[n]->m_nFlags&PROSPERO_EVENT_DELETED))
															{
																if(nNumToDelete==0)
																{
																	_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE host = '%s' AND partition = '%s' AND (file_name = '%s'",
																		g_pprospero->m_settings.m_pszDestinationMedia,
																		m_ppDestObj[nDestIndex]->m_pszServerName, //server_name
																		pchDirAlias, // directory alias
																		m_ppDestObj[nDestIndex]->m_ppmedia[n]->m_pszEncodedFileName
																	);
																}
																else
																{
																	_snprintf(szSQLTemp, DB_SQLSTRING_MAXLEN-1, "%s OR file_name = '%s'", szSQL, m_ppDestObj[nDestIndex]->m_ppmedia[n]->m_pszEncodedFileName);
																	strcpy(szSQL, szSQLTemp);
																}
																nNumToDelete++;

																if((nNumToDelete>=g_pprospero->m_settings.m_nMaxDeleteItems)||(n>=m_ppDestObj[nDestIndex]->m_nNumFiles-1))
																{
																	//do it.
																	strcat(szSQL, ")");
if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_INSERT) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Delete for %s:%s.  SQL: %s",	pchServer,pchDirAlias,szSQL);  // Sleep(250);//(Dispatch message)
//				if(g_pprospero->m_settings.m_bDebugSQL) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread delete: %s", szSQL); // Sleep(50); //(Dispatch message)
//				if(!g_pprospero->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_pprospero->m_data.m_critSQL);
																	if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
																	{
															//**MSG
g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:delete_media", "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
																	}
//				if(!g_pprospero->m_settings.m_bMultiConnectSQL) LeaveCriticalSection(&g_pprospero->m_data.m_critSQL);

																	nNumToDelete=0;
																}
															}

															if(m_ppDestObj[nDestIndex]->DeleteMedia(n)<PROSPERO_SUCCESS)
															{
																 //error
															}
															else n--;  // rewind one.
														}
														n++;
													}

												}

												if(nNumToDelete>0)
												{
													//finish it.
													strcat(szSQL, ")");
if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_INSERT) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Delete for %s:%s.  SQL: %s",	pchServer,pchDirAlias,szSQL);  // Sleep(250);//(Dispatch message)
//				if(g_pprospero->m_settings.m_bDebugSQL) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, pChannel->m_pszSource, "SentinelChannelThread delete: %s", szSQL); // Sleep(50); //(Dispatch message)
//				if(!g_pprospero->m_settings.m_bMultiConnectSQL) EnterCriticalSection(&g_pprospero->m_data.m_critSQL);
													if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
													{
												//**MSG
g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:delete_media", "SQL error: %s", dberrorstring);//  Sleep(50); //(Dispatch message)
													}
//				if(!g_pprospero->m_settings.m_bMultiConnectSQL) LeaveCriticalSection(&g_pprospero->m_data.m_critSQL);

													nNumToDelete=0;
												}



												if((pv3DirEntry)&&(pMirandaObject->m_lpfnFreeOxSoxPtr)) // only do if pointer is not null
												{
													try
													{
														pMirandaObject->m_lpfnFreeOxSoxPtr(pv3DirEntry);
													}
													catch(...)
													{
													}
												}

												// and give the info if it was external.
												//if bDelTemp was false, it gave the info already above and returned.  so it must have succeeded.

												if((nEventItemID>0)&&(nDir == nTimes-1)) // have to wait for the last one to return successfully.
												{
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I004:Retrieved directory information for %s partition on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
															pchDirAlias, pchServer,
															nItemID
															);
													if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
													{
														//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", dberrorstring); // Sleep(250); //(Dispatch message)

													}
													szServer.ReleaseBuffer();

													return PROSPERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
													// we also dont want to remove the item at the end of this function

												}
											}  // end of the else

if(g_pprospero->m_settings.m_ulDebug&PROSPERO_DEBUG_INSERTTIME) g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Device listing request for %s:%s completed in %d ms", pchServer,pchDirAlias, clock()-nClock);  // Sleep(250);//(Dispatch message)

										}  // for loop on partition.
										if((nTimes - nDirs) >= 3) // did a full refresh.
										{
											_ftime(&m_ppDestObj[nDestIndex]->m_timebLastFileRefresh);
										}
									} break;
								}
								_ftime( &g_pprospero->m_data.m_timebTick );
							}
							else  // could not ping it for some reason.  Have to error out and delete the thing thing
							{
								// if we could not ping, we want to reset it until our retries are up, before declaring an error.
								if((g_pprospero->m_settings.m_nErrorRetries>0)&&(nRetries<g_pprospero->m_settings.m_nErrorRetries))  // we want to ALWAYS retry this particular one.  but yes, do de-piroritize it.
								{
									nRetries++;
									g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_ping", 
										"Could not ping %s.  The request cannot be completed for item %d (%d). code %d.  Retrying %d of %d", 
										pchServer, nItemID, nEventItemID, nHostReturn, nRetries, g_pprospero->m_settings.m_nErrorRetries); //  Sleep(50);//(Dispatch message)

									if(nAction == 32) // waiting on transfer has some special stuff in message field, can't jsut overwrite it.
									{
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'R%d|%d.%d', timestamp = %.3f WHERE itemid = %d",  //HARDCODE
												((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
												nRetries, nRequiredHiveIndex, nDirection,
												dblTimestamp+(((double)(g_pprospero->m_settings.m_nErrorDelayMS))/1000.0),
												nItemID
												);
									}
									else
									{
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'R%d', timestamp = %.3f WHERE itemid = %d",  //HARDCODE
												((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
												nRetries, dblTimestamp+(((double)(g_pprospero->m_settings.m_nErrorDelayMS))/1000.0),
												nItemID
												);
									}
									if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
									{
										//**MSG
	g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL while reprioritizing command: %s", errorstring); // Sleep(250); //(Dispatch message)

									}
									else
									{
										return PROSPERO_ERROR; 
										// we DO NOT want to remove the item at the end of this function, since we are retrying
									}

								}
								else
								{
								g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_ping", 
									"Could not ping %s.  The request cannot be completed for item %d (%d). code %d.  Failure.", pchServer, nItemID, nEventItemID, nHostReturn ); //  Sleep(50);//(Dispatch message)
								}


								// and give the info if it was external.
								if(nEventItemID>0)
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E%03d:Could not ping %s.  The request cannot be completed.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
											((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
											nAction, pchServer,
											nItemID
											);
									if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
									{
										//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

									}
									
									return PROSPERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
									// we DO want to remove the item at the end of this   
									//  why?  I tink not, if we are waiting for something to return, we wil wait forever unless we get this request.
								}

							}
						}
						else
						{
							//**MSG
							g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_host", "Could not set host to %s for item %d (%d), code %d)", pchServer, nItemID, nEventItemID, nHostReturn ); //  Sleep(50);//(Dispatch message)
							szServer.ReleaseBuffer();


							// if we could not set the host, we want to reset it in priority until our retries are up.

//							if(g_pprospero->m_settings.m_nErrorRetries>0)  // we want to ALWAYS retry this particular one.  but yes, do de-prioritize it.
							{
								nRetries++;

								if(nAction == 32) // waiting on transfer has some special stuff in message field, can't jsut overwrite it.
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'R%d|%d.%d', timestamp = %.3f WHERE itemid = %d",  //HARDCODE
											((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
											nRetries, nRequiredHiveIndex, nDirection,
											dblTimestamp+(((double)(g_pprospero->m_settings.m_nErrorDelayMS))/1000.0),
											nItemID
											);
								}
								else
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'R%d', timestamp = %.3f WHERE itemid = %d",  //HARDCODE
											((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
											nRetries, dblTimestamp+(((double)(g_pprospero->m_settings.m_nErrorDelayMS))/1000.0),
											nItemID
											);
								}

								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
								{
									//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL while reprioritizing command: %s", errorstring); // Sleep(250); //(Dispatch message)

								}
							}

							return PROSPERO_ERROR; // we dont want to remove the item at the end of this function, because we want to retry.

	//	g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "getting channels again");   Sleep(250);//(Dispatch message)
						}
					}
					else
					{
						//**MSG

						switch(nAction)
						{
						default:
							{
								g_pprospero->m_msgr.DM(MSG_ICONINFO, NULL, "Prospero:Queue", "An event was queued for %s, but that destination is unavailable or inactive.  The event will be ignored and purged from the queue.", pchServer );  // Sleep(50);//(Dispatch message)
							} break;  
						case 2:// transfer from Miranda
						case 1:// transfer to Miranda
						case 3:// DELETE FROM Miranda
						case 64:// check file on Miranda
							{
								g_pprospero->m_msgr.DM(MSG_ICONINFO, NULL, "Prospero:Queue", "An event was queued for %s, but that destination is unavailable or inactive.  The event will be ignored and purged from the queue.", pchServer );  // Sleep(50);//(Dispatch message)

								if(nEventItemID>0) // means, commanded from external, so give info
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E%03d:%s is unavailable or inactive.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
											((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
											nAction, pchServer,
											nItemID
											);
									if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
	//								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
									{
										//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

									}
									szServer.ReleaseBuffer();
									return PROSPERO_ERROR; // we dont want to remove the item at the end of this function
								}
							} break;
						}
					}

					szServer.ReleaseBuffer();
				}


				// send remove SQL
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE itemid = %d",  //HARDCODE
					((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
					nItemID
					);

/*
Queue action IDs
1 transfer to Miranda
2 transfer from Miranda
3 delete from Miranda
4 refresh device listing
19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
32 wait on transfer

Queue table:
//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
//action int, host varchar(64), timestamp float, username varchar(32));
*/

				if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
				{
					//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

				}
			}

			return nReturn;
		}
	}
	return PROSPERO_ERROR;
}

int CProsperoData::ReturnDestinationIndex(char* pszServerName)
{
	if((pszServerName)&&(strlen(pszServerName))&&(g_pprospero)&&(g_pprospero->m_data.m_ppDestObj)&&(g_pprospero->m_data.m_nNumDestinationObjects))
	{
		int i=0;
		while(i<g_pprospero->m_data.m_nNumDestinationObjects)
		{
			if((g_pprospero->m_data.m_ppDestObj[i])&&(g_pprospero->m_data.m_ppDestObj[i]->m_pszServerName))
			{
				if(strcmp(pszServerName, g_pprospero->m_data.m_ppDestObj[i]->m_pszServerName)==0) return i;
			}
			i++;
		}
	}
	return PROSPERO_ERROR;
}

int CProsperoData::GetDestinations(char* pszInfo)
{
	if((g_pprospero)&&(m_pdbConn)&&(m_pdb)&&(!g_bKillThread))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

//select * from destinations as destinations join 
//(select distinct top 2 host from destinations order by host) as top_destinations 
//on destinations.host = top_destinations.host order by flags destinations.desc, destinations.channelid, destinations.host 

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s AS destinations JOIN \
(SELECT DISTINCT TOP %d host FROM %s ORDER BY host) AS top_destinations ON destinations.host = top_destinations.host \
ORDER BY destinations.flags DESC, destinations.channelid, destinations.host",  //HARDCODE
			((g_pprospero->m_settings.m_pszDestinations)&&(strlen(g_pprospero->m_settings.m_pszDestinations)))?g_pprospero->m_settings.m_pszDestinations:"Destinations",
			g_pprospero->m_data.m_nMaxLicensedDevices,
			((g_pprospero->m_settings.m_pszDestinations)&&(strlen(g_pprospero->m_settings.m_pszDestinations)))?g_pprospero->m_settings.m_pszDestinations:"Destinations"
			);
	
//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "GetDestinations getting %d dests", g_pprospero->m_data.m_nMaxLicensedDevices);  Sleep(250); //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Got Destinations");  Sleep(250); //(Dispatch message)

			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = PROSPERO_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF())&&(!g_bKillThread))
			{

// create table Destinations (destinationid int identity(1,1) NOT NULL, host varchar(64) NOT NULL,
//				type int, description varchar(256), diskspace_free float, diskspace_total float, diskspace_threshold real,
//				channelid int, flags int);

		_ftime( &g_pprospero->m_data.m_timebTick );

				CString szHost="";
//				CString szClient="";
				CString szDesc="";
				CString szTemp;
//				double dblDiskspaceFree=-1.0;
//				double dblDiskspaceTotal=-1.0;
				double dblDiskspaceThreshold=-1.0;
				unsigned long ulFlags;   // various flags
				unsigned short usType;
				int nChannelid=-1;
				int nTemp = -1;
				int nItemID = -1;
				bool bItemFound = false;
				bool bFlagsFound = false;
				bool bFound = false;
				try
				{
					prs->GetFieldValue("destinationid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bItemFound = true;
						nItemID = atoi(szTemp);
					}
					prs->GetFieldValue("host", szHost);//HARDCODE
					szHost.TrimLeft(); szHost.TrimRight();
					prs->GetFieldValue("type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						usType = (unsigned short)atol(szTemp);
					}
					prs->GetFieldValue("description", szDesc);//HARDCODE
					prs->GetFieldValue("diskspace_threshold", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						dblDiskspaceThreshold = atof(szTemp);
					}
					prs->GetFieldValue("channelid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nChannelid = atoi(szTemp);
					}
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bFlagsFound = true;
						ulFlags = atol(szTemp);
					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}
//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Got Destination: %s at %s (0x%08x)", szDesc, szHost, ulFlags);  Sleep(250); //(Dispatch message)

				if((g_pprospero->m_data.m_ppDestObj)&&(g_pprospero->m_data.m_nNumDestinationObjects)&&(!g_bKillThread))
				{
					nTemp=0;
					while((nTemp<g_pprospero->m_data.m_nNumDestinationObjects)&&(!g_bKillThread))
					{
//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "c2");  Sleep(250); //(Dispatch message)
						if((g_pprospero->m_data.m_ppDestObj[nTemp])&&(g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszServerName))
						{
							if((szHost.GetLength()>0)&&(szHost.CompareNoCase(g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszServerName)==0)&&(nItemID==g_pprospero->m_data.m_ppDestObj[nTemp]->m_nItemID))
							{
								bFound = true;
								bool bRefresh = false;

								if(
										(szDesc.GetLength()>0)
									&&((g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszDesc==NULL)||(szDesc.CompareNoCase(g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszDesc)))
									)
								{
									if(g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszDesc) free(g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszDesc);

									g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszDesc) sprintf(g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszDesc, szDesc);
								}

								if(bItemFound) g_pprospero->m_data.m_ppDestObj[nTemp]->m_nItemID = nItemID;
								g_pprospero->m_data.m_ppDestObj[nTemp]->m_usType = usType;

								if(dblDiskspaceThreshold>0.0) g_pprospero->m_data.m_ppDestObj[nTemp]->m_dblDiskPercent = dblDiskspaceThreshold;
//								g_pprospero->m_data.m_ppDestObj[nTemp]->m_dblDiskKBFree = dblDiskspaceFree;
//								g_pprospero->m_data.m_ppDestObj[nTemp]->m_dblDiskKBTotal = dblDiskspaceTotal;

//			g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "c3");  Sleep(250); //(Dispatch message)

								if(
										((bFlagsFound)&&(!(ulFlags&PROSPERO_FLAG_ENABLED))&&((g_pprospero->m_data.m_ppDestObj[nTemp]->m_ulFlags)&PROSPERO_FLAG_ENABLED))
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
										g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszDesc?g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszDesc:g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszServerName);  
									g_pprospero->m_msgr.DM(MSG_ICONINFO, NULL, "Prospero:destination_change", errorstring);  //Sleep(20);  //(Dispatch message)
									g_pprospero->m_data.m_ppDestObj[nTemp]->m_bKillConnThread = true;
									g_pprospero->m_data.m_ppDestObj[nTemp]->m_ulStatus = PROSPERO_STATUS_NOTCON;
									if(g_pprospero->m_data.m_ppDestObj[nTemp]->m_is2.m_socket != NULL)
									{
										g_pprospero->m_data.m_ppDestObj[nTemp]->m_is2.CloseConnection(); 
									}

								}

								if(
										((bFlagsFound)&&(ulFlags&PROSPERO_FLAG_ENABLED)&&(!((g_pprospero->m_data.m_ppDestObj[nTemp]->m_ulFlags)&PROSPERO_FLAG_ENABLED)))
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating %s...", 
//										g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszClientName?g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszClientName:"Prospero",  
										g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszDesc?g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszDesc:g_pprospero->m_data.m_ppDestObj[nTemp]->m_pszServerName);  
									g_pprospero->m_msgr.DM(MSG_ICONINFO, NULL, "Prospero:destination_change", errorstring);  // Sleep(20);  //(Dispatch message)
//									g_pprospero->m_data.m_ppDestObj[nTemp]->m_pDlg->OnButtonConnect();
									//**** if connect set following status



									if((g_pprospero->m_data.m_ppDestObj[nTemp]->m_is2.m_socket == NULL)&&(!g_pprospero->m_data.m_ppDestObj[nTemp]->m_bConnThreadStarted))
									{
										g_pprospero->m_data.m_ppDestObj[nTemp]->m_ulStatus |= PROSPERO_STATUS_CONN;
										if(_beginthread(ProsperoAsynchConnThread, 0, (void*)g_pprospero->m_data.m_ppDestObj[nTemp])==-1)
										{
										//error.

										//**MSG
										}
										else
										{
											int nWait = 0;
											while((g_pprospero->m_data.m_ppDestObj[nTemp]->m_ulStatus&PROSPERO_STATUS_CONN)&&(nWait<500))  // 0.5 seconds?
											{
												nWait++;
												Sleep(1);
											}
										}
									}



									bRefresh = true;
//									g_pprospero->m_data.m_ppDestObj[nTemp]->m_bKillConnThread = false;
	//								if(_beginthread(ProsperoConnectionThread, 0, (void*)g_pprospero->m_data.m_ppDestObj[nTemp])==-1)
	//								{
										//error.

										//**MSG
							
										
		//							}
									//**** should check return value....

								//	g_pprospero->m_data.m_ppDestObj[nTemp]->m_ulStatus = PROSPERO_STATUS_CONN;
								}

								if(bFlagsFound) g_pprospero->m_data.m_ppDestObj[nTemp]->m_ulFlags = ulFlags|PROSPERO_FLAG_FOUND;
								else g_pprospero->m_data.m_ppDestObj[nTemp]->m_ulFlags |= PROSPERO_FLAG_FOUND;

								if(bRefresh)g_pprospero->m_data.m_ppDestObj[nTemp]->m_ulFlags |= PROSPERO_FLAG_REFRESH;

							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szHost.GetLength()>0)&&(!g_bKillThread)) // have to add.
				{
//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "adding %s", szHost);  Sleep(250); //(Dispatch message)
					CProsperoDestinationObject* pscno = new CProsperoDestinationObject;
					if(pscno)
					{
//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CProsperoDestinationObject** ppObj = new CProsperoDestinationObject*[g_pprospero->m_data.m_nNumDestinationObjects+1];
						if(ppObj)
						{
//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((g_pprospero->m_data.m_ppDestObj)&&(g_pprospero->m_data.m_nNumDestinationObjects>0))
							{
								while(o<g_pprospero->m_data.m_nNumDestinationObjects)
								{
									ppObj[o] = g_pprospero->m_data.m_ppDestObj[o];
									o++;
								}
								delete [] g_pprospero->m_data.m_ppDestObj;

							}
							ppObj[o] = pscno;
							g_pprospero->m_data.m_ppDestObj = ppObj;
							g_pprospero->m_data.m_nNumDestinationObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szHost.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, "%s", szHost);

/*							
							if(szClient.GetLength()<=0)
							{
								szClient = "Prospero";
							}

							if(szClient.GetLength()>0)
							{
								ppObj[o]->m_pszClientName = (char*)malloc(szClient.GetLength()+1); 
								if(ppObj[o]->m_pszClientName) sprintf(ppObj[o]->m_pszClientName, "%s", szClient);
							}
*/
							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}
							if(bItemFound) ppObj[o]->m_nItemID = nItemID;
							ppObj[o]->m_usType = usType;

							if(dblDiskspaceThreshold>0.0) ppObj[o]->m_dblDiskPercent = dblDiskspaceThreshold;
//							ppObj[o]->m_dblDiskKBFree = dblDiskspaceFree;
//							ppObj[o]->m_dblDiskKBTotal = dblDiskspaceTotal;

//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "adding flags: %s (0x%08x)", szHost, ulFlags);  Sleep(250); //(Dispatch message)

							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|PROSPERO_FLAG_FOUND|PROSPERO_FLAG_NEW;
							else ppObj[o]->m_ulFlags |= PROSPERO_FLAG_FOUND|PROSPERO_FLAG_NEW;
								
//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", " added flags: %s (0x%08x) %d", szHost, ppObj[o]->m_ulFlags, ((ppObj[o]->m_ulFlags)&PROSPERO_FLAG_ENABLED));  Sleep(250); //(Dispatch message)

							if((ppObj[o]->m_ulFlags)&PROSPERO_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating %s...", 
										ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName);  
								g_pprospero->m_msgr.DM(MSG_ICONINFO, NULL, "Prospero:destination_change", errorstring);  // Sleep(20);  //(Dispatch message)

								//**** if connect 
								if((ppObj[o]->m_is2.m_socket == NULL)&&(!ppObj[o]->m_bConnThreadStarted))
								{
									ppObj[o]->m_ulStatus |= PROSPERO_STATUS_CONN;
									if(_beginthread(ProsperoAsynchConnThread, 0, (void*)ppObj[o])==-1)
									{
									//error.

									//**MSG
									}
									else
									{
										int nWait = 0;
										while((ppObj[o]->m_ulStatus&PROSPERO_STATUS_CONN)&&(nWait<500))  // 0.5 seconds?
										{
											nWait++;
											Sleep(1);
										}
									}
								}

								// create a connection object in g_adc.
								// associate that with the new CProsperoConnectionObject;
								// start the thread.
//								ppObj[o]->m_bKillConnThread = false;

//								ppObj[o]->m_ulStatus = PROSPERO_STATUS_CONN;
							}

						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//			g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			// and insert media list refresh into the queue for all new destinatons.
			nIndex = 0;
			while((nIndex<g_pprospero->m_data.m_nNumDestinationObjects)&&(!g_bKillThread))  // exit will kill these
			{
		_ftime( &g_pprospero->m_data.m_timebTick );
				if((g_pprospero->m_data.m_ppDestObj)&&(g_pprospero->m_data.m_ppDestObj[nIndex]))
				{

					if((g_pprospero->m_data.m_ppDestObj[nIndex]->m_ulFlags)&PROSPERO_FLAG_FOUND)
					{
						(g_pprospero->m_data.m_ppDestObj[nIndex]->m_ulFlags) &= ~PROSPERO_FLAG_FOUND;

						if((g_pprospero->m_data.m_ppDestObj[nIndex]->m_ulFlags)&(PROSPERO_FLAG_NEW|PROSPERO_FLAG_REFRESH))
						{
							(g_pprospero->m_data.m_ppDestObj[nIndex]->m_ulFlags) &= ~(PROSPERO_FLAG_REFRESH|PROSPERO_FLAG_NEW);
							// refresh media.
// insert an immediate destinations media refresh into the queue.
/*
Queue action IDs
1 transfer to Miranda
2 transfer from Miranda
3 delete from Miranda
4 refresh device listing
19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)

Queue table:
//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
//action int, host varchar(64), timestamp float, username varchar(32));
*/

							if((g_pprospero->m_data.m_ppDestObj[nIndex]->m_pszServerName)&&(strlen(g_pprospero->m_data.m_ppDestObj[nIndex]->m_pszServerName)) && ((g_pprospero->m_data.m_ppDestObj[nIndex]->m_ulFlags)&PROSPERO_FLAG_ENABLED)  )
							{
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (filename_local, filename_remote, action, host, timestamp, username) \
VALUES ('REFRESH','', 4, '%s', 0, 'sys' )", //HARDCODE
								((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
								g_pprospero->m_data.m_ppDestObj[nIndex]->m_pszServerName
								);


//			g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", szSQL);  Sleep(250); //(Dispatch message)
//					char errorstring[DB_ERRORSTRING_LEN];
								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
//								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
								{
									//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

								}

// deal with the diskspace info 
							// actually, do this on any transaction from queue.

							}
						}
					
						nIndex++;
					}
					else
					{
						if(g_pprospero->m_data.m_ppDestObj[nIndex])
						{
							if((g_pprospero->m_data.m_ppDestObj[nIndex]->m_ulFlags)&PROSPERO_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
									g_pprospero->m_data.m_ppDestObj[nIndex]->m_pszDesc?g_pprospero->m_data.m_ppDestObj[nIndex]->m_pszDesc:g_pprospero->m_data.m_ppDestObj[nIndex]->m_pszServerName);  
								g_pprospero->m_msgr.DM(MSG_ICONINFO, NULL, "Prospero:destination_remove", errorstring); //  Sleep(20);  //(Dispatch message)
//									g_pprospero->m_data.m_ppDestObj[nTemp]->m_pDlg->OnDisconnect();

								//**** disconnect

								g_pprospero->m_data.m_ppDestObj[nIndex]->m_bKillConnThread = false;
//								while(g_pprospero->m_data.m_ppDestObj[nIndex]->m_bConnThreadStarted) Sleep(1);

								//g_adc.DisconnectServer(g_pprospero->m_data.m_ppDestObj[nIndex]->m_pszServerName);
								//**** should check return value....

								g_pprospero->m_data.m_ppDestObj[nIndex]->m_ulStatus = PROSPERO_STATUS_NOTCON;
								if(g_pprospero->m_data.m_ppDestObj[nIndex]->m_is2.m_socket != NULL)
								{
									g_pprospero->m_data.m_ppDestObj[nIndex]->m_is2.CloseConnection(); 
								}

							}

							delete g_pprospero->m_data.m_ppDestObj[nIndex];
							g_pprospero->m_data.m_nNumDestinationObjects--;

							int nTemp=nIndex;
							while(nTemp<g_pprospero->m_data.m_nNumDestinationObjects)
							{
								g_pprospero->m_data.m_ppDestObj[nTemp]=g_pprospero->m_data.m_ppDestObj[nTemp+1];
								nTemp++;
							}
							g_pprospero->m_data.m_ppDestObj[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

//			g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
	}
//			g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return PROSPERO_ERROR;
}





#ifdef OLD_TEMP_METHOD


											else
											{

												// success, lets update db.
	//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "OxSoxGetDirInfo returned >= OX_SUCCESS" );   Sleep(50);//(Dispatch message)


	// the old way was m_pszDestinationMedia.  now we merge on temp table m_pszDestinationMediaTemp
												// first remove everything from the db.  clear out temp db
												/*
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
host = '%s' AND \
partition = '%s'",
														((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														pchServer,
														pchDirAlias

													);
												*/
												// first remove everything from the db.  clear out temp db  
												// And we really mean everything.  no where clause this time.
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
														((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp"
													);
												if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
												{
													//**MSG
													bDelTemp = false;

													g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:oxsox_dirinfo", 
														"Could not get directory information for %s partition on %s.", pchDirAlias, pchServer ); //  Sleep(50);//(Dispatch message)

													// and give the info if it was external.
													if(nEventItemID>0)
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E004:Could not get directory information for %s partition on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
																((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
																pchDirAlias, pchServer,
																nItemID
																);
														if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
														{
															//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

														}
														szServer.ReleaseBuffer();
														return PROSPERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
														// we also dont want to remove the item at the end of this function
													}

												}


												int nFile = 0;
												while((nFile<nNumEntries)&&(bDelTemp))  // have to fail out if the temp table wasn't cleared.
												{
			_ftime( &g_pprospero->m_data.m_timebTick );

													char* pchFilename = m_pdb->EncodeQuotes(pv3DirEntry[nFile].FileName);
	/*
	// don't check for existence, we already removed everything just to be sure.

												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s WHERE \
	host = '%s' AND \
	file_name = '%s' AND \
	partition = '%s';",
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															pchServer,
															pchFilename?pchFilename:pv3DirEntry[nFile].FileName,											
															pchDirAlias

														);

													// get
													CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, dberrorstring);
													int nNumFound = 0;
													int nID = -1;
													if(prs != NULL) 
													{
														if(!prs->IsEOF())
														{  // all we really need to know is that one record has been returned.
				/*
															CString szValue;
															try
															{
																// get id!
																prs->GetFieldValue("itemid", szValue);
																nID = atoi(szValue);
															}
															catch( ... )
															{
															}

				* /
															nNumFound++;
															prs->MoveNext();
														}
														prs->Close();
														delete prs;
													}

													if((nNumFound>0)&&(nID>0))
													{
	//create table Destinations_Media (host varchar(64), file_name varchar(256), 
	//transfer_date int, partition varchar(16), file_size float, 
	//Direct_local_last_used int, Direct_local_times_used int);

														// if found update it with new values
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
	transfer_date = %d, \
	file_size = %f, \
	WHERE host = '%s' AND \
	file_name = '%s' AND \
	partition = '%s';", //HARDCODE
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															pv3DirEntry[nFile].FileTimeStamp,
															(double)pv3DirEntry[nFile].FileSize,
															pchServer,
															pchFilename?pchFilename:pv3DirEntry[nFile].FileName,											
															pchDirAlias
														);

													}
													else
													{
													*/
														// add it.
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
host, \
file_name, \
transfer_date, \
partition, \
file_size) VALUES ('%s', '%s', %d, '%s', %f)",   //HARDCODE
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															pchServer,
															pchFilename?pchFilename:pv3DirEntry[nFile].FileName,											
															pv3DirEntry[nFile].FileTimeStamp,
															pchDirAlias,
															(double)pv3DirEntry[nFile].FileSize
															);

													// }

													if(pchFilename) free(pchFilename);

													if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
													{
														//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", dberrorstring); // Sleep(250); //(Dispatch message)
													}

		/*
													pv3DirEntry[nFile].FileName
													pv3DirEntry[nFile].FileSize,
													pv3DirEntry[nFile].FileAttribs,
													pv3DirEntry[nFile].FileTimeStamp
		*/
													nFile++;
												}

												if((pv3DirEntry)&&(pMirandaObject->m_lpfnFreeOxSoxPtr)) // only do if pointer is not null
												{
													try
													{
														pMirandaObject->m_lpfnFreeOxSoxPtr(pv3DirEntry);
													}
													catch(...)
													{
													}
												}

												// now everything is in the temp table,
												// update the files with info from the temp table

												if(bDelTemp)
												{

													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
%s.transfer_date = %s.transfer_date, \
%s.file_size = %s.file_size \
FROM %s, %s \
WHERE %s.host = '%s' AND \
%s.file_name = %s.file_name \
AND %s.partition = %s.partition",
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															pchServer,
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp"
															);

	//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Updating from temp SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
													if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
													{
														//**MSG
g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:debug", "Updating from temp ERROR: %s", dberrorstring); // Sleep(50); //(Dispatch message)
													}


													// insert the ones that are in the temp list that are not in the main media list.
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (host, file_name, transfer_date, partition, file_size, direct_local_last_used, direct_local_times_used) \
SELECT %s.host, %s.file_name, %s.transfer_date, %s.partition, %s.file_size, 0, 0 FROM \
%s LEFT JOIN (SELECT * FROM %s WHERE host = '%s') AS %s on %s.file_name = %s.file_name and %s.partition = %s.partition WHERE %s.file_name is NULL",
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															pchServer,
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media"
															);

	//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Updating from temp SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
													if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
													{
														//**MSG
g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:debug", "Updating from temp ERROR: %s", dberrorstring); // Sleep(50); //(Dispatch message)
													}


													// remove the ones that are no longer in the temp list
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
%s.host = '%s' AND \
%s.partition = '%s' AND \
cast(%s.file_name AS varchar(256)) + ' ' + cast(%s.partition AS varchar(16)) \
NOT IN (SELECT cast(%s.file_name as varchar(256)) + ' ' + cast(%s.partition AS varchar(16)) \
AS fileinfo from %s where host = '%s' and %s.partition = '%s')",
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															pchServer,
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															pchDirAlias,
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pprospero->m_settings.m_pszDestinationMedia)&&(strlen(g_pprospero->m_settings.m_pszDestinationMedia)))?g_pprospero->m_settings.m_pszDestinationMedia:"Destinations_Media",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															pchServer,
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															pchDirAlias
															);


	//g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "Delete extras from media SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
													if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
													{
														//**MSG
g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:debug", "Delete extras from media ERROR: %s", dberrorstring); // Sleep(50); //(Dispatch message)
													}


												}
		// clear the temp table
/*
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
host = '%s' AND \
partition = '%s'",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															pchServer,
															pchDirAlias

														);
														*/

												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
															((g_pprospero->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_pprospero->m_settings.m_pszDestinationMediaTemp)))?g_pprospero->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp"
														);
												if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
												{
													//**MSG
g_pprospero->m_msgr.DM(MSG_ICONERROR, NULL, "Prospero:debug", "Delete extras from media ERROR: %s", dberrorstring); // Sleep(50); //(Dispatch message)
												}

												// and give the info if it was external.
												//if bDelTemp was false, it gave the info already above and returned.  so it must have succeeded.

												if(nEventItemID>0)
												{
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I004:Retrieved directory information for %s partition on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_pprospero->m_settings.m_pszQueue)&&(strlen(g_pprospero->m_settings.m_pszQueue)))?g_pprospero->m_settings.m_pszQueue:"Queue",
															pchDirAlias, pchServer,
															nItemID
															);
													if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
													{
														//**MSG
g_pprospero->m_msgr.DM(MSG_ICONHAND, NULL, "Prospero:debug", "ERROR executing SQL: %s", dberrorstring); // Sleep(250); //(Dispatch message)

													}
													szServer.ReleaseBuffer();
													return PROSPERO_ERROR;  // because we dont want to increment counter.  // there is no counter.
													// we also dont want to remove the item at the end of this function

												}
											}  // end of the else
#endif //OLD TEMP METHOD



