// ProsperoSettings.cpp: implementation of the CProsperoSettings.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "ProsperoDefines.h"
#include "ProsperoSettings.h"
#include "ProsperoMain.h" 

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CProsperoMain* g_pprospero;
extern CProsperoApp theApp;


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProsperoSettings::CProsperoSettings()
{
	m_pdb = NULL;
	m_pdbConn = NULL;

	m_nAutoPurgeMessageDays = 30; // default
//	m_nAutoPurgeAsRunDays = 30; // default
	m_nAutoPurgeInterval = 300;  // 5 minutes should be enough.

	m_pszName = NULL;
	m_pszType = NULL;  // familiar name of the type.
	m_pszProject = NULL;  // familiar name of the project name.
	m_ulMainMode = PROSPERO_MODE_DEFAULT;

	// ports
	m_usCommandPort	= PROSPERO_PORT_CMD;
	m_usStatusPort	= PROSPERO_PORT_STATUS;

	// messaging for Prospero
	m_bUseLog = true;			// write a log file
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host
	m_bReportSuccessfulOperation=false;
	m_bLogTransfers=false;
	m_pszFileSpec = NULL;
	m_pszMailSpec = NULL;
	m_pszProcessedFileSpec = NULL;
	m_pszProcessedMailSpec = NULL;
	m_bUseXMLClientLog = false;
	m_bMillisecondMessaging=true;			// use millisecond resolution for messages and asrun
//	m_bUseMessagingForMiranda = false;

	m_nThreadDwellMS = 1000;
	m_bDebugLists= false;  // if true, dumps the temp and events databases
	m_nDebugListTopCount=-1;  //if positive, just prints out the top n recored in the events tables.
	m_bDebugSQL=false;  // if true, prints out the SQL calls
	m_bDebugInsertSQL=false;  // if true, prints out the event insert SQL calls
	m_ulDebug = 0;
	m_pszDebugOrder = NULL;

	m_bDiReCTInstalled = false;
	// Harris API
//	m_bUseListCount = false; // get all events up until the list count (otherwise just up to the lookahead)

	m_nPutDelayMS=10000;  // re-prioritization delay after a successful put command
	m_nGetDelayMS=10000;  // re-prioritization delay after a successful get command
	m_nErrorDelayMS=10000;  // re-prioritization delay after an error
	m_nErrorRetries=2;  // number of retries after oxsox error to return error to queue
	m_nDiskCheckMinIntervalMS=60000;  // minimum time in between disk checks
	m_nCommmandQueueIntervalMS=10000;  // interval in the command thread.  if Queue is used, set this to large value to not compete with db query load


	// DSN params
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;

	m_pszSettings = NULL;  // the Settings table name
	m_pszExchange = NULL;  // the Exchange table name
	m_pszMessages = NULL;  // the Messages table name

	m_pszDestinationMediaTemp = NULL;
	m_pszDestinationMedia = NULL;  // the DestinationMedia table name
	m_pszDestinations = NULL;  // the Destinations table name
	m_pszQueue = NULL; // the Queue table name
	m_pszCommandQueue = NULL;  // the CommandQueue table name
	m_nMaxDeleteItems = 10;  // max number of items to delete in a single SQL statement

	m_ulModsIntervalMS = 1000;
	m_ulFileRefreshInterval = 3600;  // interval on which to mandatorily refresh the file listing in seconds.

	m_pszLicense=NULL;  // the License Key
	m_pszIconPath=NULL;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

	m_nNumberOfHiveObjects=0;
	m_pszDLLFileName=NULL;
	m_bHiveReloadLibraries=true; // if true (default), will reload the DLLs at staggered intervals (based on a fraction of the reload interval)
	m_nHiveReloadInterval=86400; // in seconds, defaults to 1 day.  If there are 7 objects in the hive, each gets reloaded once per day, each 1/7 of a day after another in sequence, until they are all reset within 1 day.
	m_nHiveReloadCheckInterval=30; // in seconds, interval on which to check if the reload interval has elapsed.

}

CProsperoSettings::~CProsperoSettings()
{
	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_pszType) free(m_pszType); // must use malloc to allocate
	if(m_pszProject) free(m_pszProject); // must use malloc to allocate
	if(m_pszDSN) free(m_pszDSN); // must use malloc to allocate
	if(m_pszUser) free(m_pszUser); // must use malloc to allocate
	if(m_pszPW) free(m_pszPW); // must use malloc to allocate
	if(m_pszSettings) free(m_pszSettings); // must use malloc to allocate
	if(m_pszExchange) free(m_pszExchange); // must use malloc to allocate
	if(m_pszMessages) free(m_pszMessages); // must use malloc to allocate

	if(m_pszDestinationMediaTemp) free(m_pszDestinationMediaTemp); // must use malloc to allocate
	if(m_pszDestinationMedia) free(m_pszDestinationMedia); // must use malloc to allocate
	if(m_pszDestinations) free(m_pszDestinations); // must use malloc to allocate
	if(m_pszQueue) free(m_pszQueue); // must use malloc to allocate
	if(m_pszCommandQueue) free(m_pszCommandQueue); // must use malloc to allocate
	if(m_pszLicense) free(m_pszLicense); // must use malloc to allocate
	if(m_pszIconPath) free(m_pszIconPath); // must use malloc to allocate	
	if(m_pszFileSpec) free(m_pszFileSpec); // must use malloc to allocate	
	if(m_pszMailSpec) free(m_pszMailSpec); // must use malloc to allocate	
	if(m_pszProcessedFileSpec) free(m_pszProcessedFileSpec); // must use malloc to allocate	
	if(m_pszProcessedMailSpec) free(m_pszProcessedMailSpec); // must use malloc to allocate	

	if(m_pszDebugOrder) free(m_pszDebugOrder); // must use malloc to allocate
}


int CProsperoSettings::Settings(bool bRead)
{
	char pszFilename[MAX_PATH];
	strcpy(pszFilename, "");
//		AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, PROSPERO_SETTINGS_FILE_DEFAULT);  // cortex settings file
	CFileUtil file;
	// get settings.
	file.GetSettings(pszFilename, false); 

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
		// load up the values on the settings object
			m_pszName = file.GetIniString("Main", "Name", "Prospero", m_pszName);
			m_pszType = file.GetIniString("Main", "Type", "Prospero", m_pszType);
			m_pszProject = file.GetIniString("Main", "Project", "VDS", m_pszProject);
			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);

			m_nThreadDwellMS = file.GetIniInt("Main", "ThreadDwellMS", 1000);

			m_bDebugLists = file.GetIniInt("Database", "DebugLists", 0)?true:false;  // if true, dumps the temp and events databases
			m_nDebugListTopCount = file.GetIniInt("Database", "DebugListTopCount", -1);  //if positive, just prints out the top n recored in the events tables.
			m_bDebugSQL = file.GetIniInt("Database", "DebugSQL", 0)?true:false;  // if true, prints out the SQL calls
			m_bDebugInsertSQL = file.GetIniInt("Database", "DebugInsertSQL", 0)?true:false;  // if true, prints out the event insert SQL calls
			m_ulDebug = file.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.
			m_pszDebugOrder = file.GetIniString("Database", "DebugOrder", "(case when parent_position < 0 then ((event_position)/1000.0) else (parent_calc_start + (event_position-parent_position)/1000.0) end)", m_pszDebugOrder); // the order by clause for debug

			// recompile license key params
			if(g_pprospero->m_data.m_key.m_pszLicenseString) free(g_pprospero->m_data.m_key.m_pszLicenseString);
			g_pprospero->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(m_pszLicense)+1);
			if(g_pprospero->m_data.m_key.m_pszLicenseString)
			sprintf(g_pprospero->m_data.m_key.m_pszLicenseString, "%s", m_pszLicense);

			g_pprospero->m_data.m_key.InterpretKey();

			char errorstring[MAX_MESSAGE_LENGTH];
			if(g_pprospero->m_data.m_key.m_bValid)
			{
				unsigned long i=0;
				while(i<g_pprospero->m_data.m_key.m_ulNumParams)
				{
					if((g_pprospero->m_data.m_key.m_ppszParams)
						&&(g_pprospero->m_data.m_key.m_ppszValues)
						&&(g_pprospero->m_data.m_key.m_ppszParams[i])
						&&(g_pprospero->m_data.m_key.m_ppszValues[i]))
					{
						if(stricmp(g_pprospero->m_data.m_key.m_ppszParams[i], "max")==0)
						{
							g_pprospero->m_data.m_nMaxLicensedDevices = atoi(g_pprospero->m_data.m_key.m_ppszValues[i]);
						}
					}
					i++;
				}
			
				if(
						(
							(!g_pprospero->m_data.m_key.m_bExpires)
						||((g_pprospero->m_data.m_key.m_bExpires)&&(!g_pprospero->m_data.m_key.m_bExpired))
						||((g_pprospero->m_data.m_key.m_bExpires)&&(g_pprospero->m_data.m_key.m_bExpireForgiveness)&&(g_pprospero->m_data.m_key.m_ulExpiryDate+g_pprospero->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!g_pprospero->m_data.m_key.m_bMachineSpecific)
						||((g_pprospero->m_data.m_key.m_bMachineSpecific)&&(g_pprospero->m_data.m_key.m_bValidMAC))
						)
					)
				{
					// this overrides stuff so let's not let it do that.

										// let's just not override this in a green condition.  
										// comment it out.
//					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//					g_pprospero->m_data.SetStatusText(errorstring, PROSPERO_STATUS_OK);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
					g_pprospero->m_data.SetStatusText(errorstring, PROSPERO_STATUS_ERROR);
				}
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				g_pprospero->m_data.SetStatusText(errorstring, PROSPERO_STATUS_ERROR);
			}

			m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", PROSPERO_PORT_CMD);
			m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", PROSPERO_PORT_STATUS);

			m_pszIconPath = file.GetIniString("FileServer", "IconPath", "C:\\Inetpub\\wwwroot\\Cortex\\prospero\\images\\", m_pszIconPath);
			m_bUseXMLClientLog = file.GetIniInt("Messager", "UseXMLClientLog", 0)?true:false;
			m_bMillisecondMessaging = file.GetIniInt("Messager", "MillisecondMessaging", 1)?true:false;			// use millisecond resolution for messages and asrun

			m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
			m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;
//			m_bUseMessagingForMiranda = file.GetIniInt("Messager", "UseMessagingForMiranda", 0)?true:false;


			m_pszDSN = file.GetIniString("Database", "DSN", (m_pszName?m_pszName:"Prospero"), m_pszDSN);
			m_pszUser = file.GetIniString("Database", "DBUser", "sa", m_pszUser);
			m_pszPW = file.GetIniString("Database", "DBPassword", "", m_pszPW);
			m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings", m_pszSettings);  // the Settings table name
			m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange", m_pszExchange);  // the Exchange table name
			m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages", m_pszMessages);  // the Messages table name

			m_nAutoPurgeMessageDays = file.GetIniInt("AutoPurge", "AutoPurgeMessageDays", 30);
	//		m_nAutoPurgeAsRunDays = file.GetIniInt("AutoPurge", "AutoPurgeAsRunDays", 30);
			m_nAutoPurgeInterval  = file.GetIniInt("AutoPurge", "AutoPurgeInterval", 300);  // 5 minutes should be enough.

			m_nPutDelayMS = file.GetIniInt("Queue", "PutDelayMS", 10000);  // re-prioritization delay after a successful put command
			m_nGetDelayMS = file.GetIniInt("Queue", "GetDelayMS", 10000);  // re-prioritization delay after a successful get command
			m_nErrorDelayMS = file.GetIniInt("Queue", "ErrorDelayMS", 10000);  // re-prioritization delay after an error
			m_nErrorRetries = file.GetIniInt("Queue", "ErrorRetries", 2);  // number of retries after oxsox error to return error to queue
			m_nDiskCheckMinIntervalMS = file.GetIniInt("Queue", "DiskCheckMinIntervalMS", 60000);  // minimum time in between disk checks
			m_nCommmandQueueIntervalMS = file.GetIniInt("Queue", "CommmandQueueIntervalMS", 10000);  // interval in the command thread.  if Queue is used, set this to large value to not compete with db query load


			m_pszDestinationMediaTemp = file.GetIniString("Database", "DestinationMediaTempTableName", "Destinations_Media_Temp", m_pszDestinationMediaTemp);  // the Destinations table name
			m_pszDestinationMedia = file.GetIniString("Database", "DestinationMediaTableName", "Destinations_Media", m_pszDestinationMedia);  // the Destinations table name
			m_pszDestinations = file.GetIniString("Database", "DestinationsTableName", "Destinations", m_pszDestinations);  // the Destinations table name
			m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue", m_pszQueue);  // the Queue table name
			m_pszCommandQueue = file.GetIniString("Database", "CommandQueueTableName", "Command_Queue", m_pszCommandQueue);  // the CommandQueue table name
	 
			m_ulFileRefreshInterval = file.GetIniInt("Database", "FileRefreshInterval", 3600);  // interval on which to mandatorily refresh the file listing in seconds.

			m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds
			m_nMaxDeleteItems = file.GetIniInt("Database", "MaxDeleteItems", 10);  // max number of items to delete in a single SQL statement
			
			m_pszFileSpec = file.GetIniString("Messager", "LogFileIni", "Logs\\Prospero|YD||1|", m_pszFileSpec);
				// SMTP address[: override port (optional)] | distribution lists | from email |  subject spec | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
			m_pszMailSpec = file.GetIniString("Messager", "SMTPIni", "mail.server.com|f3:recipient@address.com|\"someone\"<from@here.com>|%P %T from %h|%h|C:\\prosperosmtp.txt|1|1|0", m_pszMailSpec);
			if(m_pszFileSpec) 
			{
				if(m_pszProcessedFileSpec)
				{
					try{ free(m_pszProcessedFileSpec); } catch(...) {}
				}
				m_pszProcessedFileSpec = ProcessString(m_pszFileSpec, false);
			}
			
			if(m_pszMailSpec) 
			{
				if(m_pszProcessedMailSpec)
				{
					try{ free(m_pszProcessedMailSpec); } catch(...) {}
				}
				m_pszProcessedMailSpec = ProcessString(m_pszMailSpec, false);
			}
			
			m_nNumberOfHiveObjects = file.GetIniInt("Hive", "NumberOfObjects", 0);
			m_pszDLLFileName = file.GetIniString("Hive", "DLLFileName", "OxSox2.dll", m_pszDLLFileName);
			m_bHiveReloadLibraries = file.GetIniInt("Hive", "ReloadLibraries", 1)?true:false; // if true (default), will reload the DLLs at staggered intervals (based on a fraction of the reload interval)
			m_nHiveReloadInterval = file.GetIniInt("Hive", "ReloadInterval", 86400); // in seconds, defaults to 1 day.  If there are 7 objects in the hive, each gets reloaded once per day, each 1/7 of a day after another in sequence, until they are all reset within 1 day.
			m_nHiveReloadCheckInterval = file.GetIniInt("Hive", "ReloadCheckInterval", 30); // in seconds, interval on which to check if the reload interval has elapsed.

		}
		else //write
		{
			file.SetIniString("Main", "Name", m_pszName);
			file.SetIniString("Main", "Type", m_pszType);
			file.SetIniString("Main", "Project", m_pszProject);
			file.SetIniString("License", "Key", m_pszLicense);

			file.SetIniInt("Main", "ThreadDwellMS", m_nThreadDwellMS);

			file.SetIniInt("Database", "DebugLists", m_bDebugLists?1:0);  // if true, dumps the temp and events databases
			file.SetIniInt("Database", "DebugListTopCount", m_nDebugListTopCount);  //if positive, just prints out the top n recored in the events tables.
			file.SetIniInt("Database", "DebugSQL", m_bDebugSQL?1:0);  // if true, prints out the SQL calls
			file.SetIniInt("Database", "DebugInsertSQL", m_bDebugInsertSQL?1:0);  // if true, prints out the event insert SQL calls
			file.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.
			file.SetIniString("Database", "DebugOrder", m_pszDebugOrder); // the order by clause for debug

			file.SetIniInt("CommandServer", "ListenPort", m_usCommandPort);
			file.SetIniInt("StatusServer", "ListenPort", m_usStatusPort);
			file.SetIniString("FileServer", "IconPath", m_pszIconPath);

			file.SetIniInt("Messager", "UseEmail", m_bUseEmail?1:0);
			file.SetIniInt("Messager", "UseNet", m_bUseNetwork?1:0);
			file.SetIniInt("Messager", "UseLog", m_bUseLog?1:0);
			file.SetIniInt("Messager", "ReportSuccessfulOperation", m_bReportSuccessfulOperation?1:0);
			file.SetIniInt("Messager", "LogTransfers", m_bLogTransfers?1:0);
			file.SetIniInt("Messager", "UseXMLClientLog", m_bUseXMLClientLog?1:0);
			file.SetIniInt("Messager", "MillisecondMessaging", m_bMillisecondMessaging?1:0);		// use millisecond resolution for messages and asrun
//			file.SetIniInt("Messager", "UseMessagingForMiranda", m_bUseMessagingForMiranda?1:0);

			file.SetIniString("Database", "DSN", m_pszDSN);
			file.SetIniString("Database", "DBUser", m_pszUser);
			file.SetIniString("Database", "DBPassword", m_pszPW);
			file.SetIniString("Database", "SettingsTableName", m_pszSettings);  // the Settings table name
			file.SetIniString("Database", "ExchangeTableName", m_pszExchange);  // the Exchange table name
			file.SetIniString("Database", "MessagesTableName", m_pszMessages);  // the Messages table name

	//		file.SetIniInt("HarrisAPI", "UseListCount", m_bUseListCount?1:0); // get all events up until the list count (otherwise just up to the lookahead)

			file.SetIniString("Database", "DestinationsTableName", m_pszDestinations);  // the Destinations table name
			file.SetIniString("Database", "QueueTableName", m_pszQueue);  // the Queue table name
			file.SetIniString("Database", "CommandQueueTableName", m_pszCommandQueue);  // the CommandQueue table name
			file.SetIniString("Database", "DestinationMediaTableName", m_pszDestinationMedia);  // the DestinationMedia table name
			file.SetIniString("Database", "DestinationMediaTempTableName", m_pszDestinationMediaTemp );  // the Destinations table name

			file.SetIniInt("Database", "ModificationCheckInterval", m_ulModsIntervalMS);  // in milliseconds
			file.SetIniInt("Database", "FileRefreshInterval", m_ulFileRefreshInterval);  // interval on which to mandatorily refresh the file listing in seconds.
			file.SetIniInt("Database", "MaxDeleteItems", m_nMaxDeleteItems);  // max number of items to delete in a single SQL statement

			file.SetIniInt("Queue", "PutDelayMS", m_nPutDelayMS);  // re-prioritization delay after a successful put command
			file.SetIniInt("Queue", "GetDelayMS", m_nGetDelayMS);  // re-prioritization delay after a successful get command
			file.SetIniInt("Queue", "ErrorDelayMS", m_nErrorDelayMS);  // re-prioritization delay after an error
			file.SetIniInt("Queue", "ErrorRetries", m_nErrorRetries);  // number of retries after oxsox error to return error to queue
			file.SetIniInt("Queue", "DiskCheckMinIntervalMS", m_nDiskCheckMinIntervalMS);  // minimum time in between disk checks
			file.SetIniInt("Queue", "CommmandQueueIntervalMS", m_nCommmandQueueIntervalMS, "if only Queue is used, set this to large, if telnet responses needed, set to -1");  // interval in the command thread.  if Queue is used, set this to large value to not compete with db query load


			file.SetIniInt("AutoPurge", "AutoPurgeMessageDays", m_nAutoPurgeMessageDays);
//			file.SetIniInt("AutoPurge", "AutoPurgeAsRunDays", m_nAutoPurgeAsRunDays);
			file.SetIniInt("AutoPurge", "AutoPurgeInterval", m_nAutoPurgeInterval);  // 5 minutes should be enough.

			file.SetIniString("Messager", "LogFileIni", m_pszFileSpec);
			file.SetIniString("Messager", "SMTPIni", m_pszMailSpec);

			file.SetIniInt("Hive", "NumberOfObjects", m_nNumberOfHiveObjects);
			file.SetIniString("Hive", "DLLFileName", m_pszDLLFileName);
			file.SetIniInt("Hive", "ReloadLibraries", m_bHiveReloadLibraries?1:0); // if true (default), will reload the DLLs at staggered intervals (based on a fraction of the reload interval)
			file.SetIniInt("Hive", "ReloadInterval", m_nHiveReloadInterval); // in seconds, defaults to 1 day.  If there are 7 objects in the hive, each gets reloaded once per day, each 1/7 of a day after another in sequence, until they are all reset within 1 day.
			file.SetIniInt("Hive", "ReloadCheckInterval", m_nHiveReloadCheckInterval); // in seconds, interval on which to check if the reload interval has elapsed.


			file.SetSettings(pszFilename, false);  // have to have correct filename

		}
		return PROSPERO_SUCCESS;
	}
	return PROSPERO_ERROR;
}



int CProsperoSettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	if((m_pszSettings)&&(strlen(m_pszSettings)>0)&&(m_pdbConn)&&(m_pdb)&&(m_pdbConn->m_bConnected)&&(Settings(true)==PROSPERO_SUCCESS))  //read has to succeed
	{
		// get old settings.
/*
		char pszFilename[MAX_PATH];
		strcpy(pszFilename, PROSPERO_SETTINGS_FILE_DEFAULT);  // prospero settings file
		CFileUtil file;
		file.GetSettings(pszFilename, false); 
	// load up the values on the settings object
		if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
		{
			g_pprospero->m_settings.m_pszName = file.GetIniString("Main", "Name", "Prospero");
			g_pprospero->m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");
			// dont decompile license here, it may be getting reset below.  if it's the same, then no need to re-do it anyway.

			g_pprospero->m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", PROSPERO_PORT_CMD);
			g_pprospero->m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", PROSPERO_PORT_STATUS);

			g_pprospero->m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");

			g_pprospero->m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			g_pprospero->m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			g_pprospero->m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			g_pprospero->m_settings.m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
			g_pprospero->m_settings.m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;

			g_pprospero->m_settings.m_pszDSN = file.GetIniString("Database", "DSN", g_pprospero->m_settings.m_pszName?g_pprospero->m_settings.m_pszName:"Prospero");
			g_pprospero->m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
			g_pprospero->m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
			g_pprospero->m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
			g_pprospero->m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
			g_pprospero->m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name

			g_pprospero->m_settings.m_pszDestinationMediaTemp = file.GetIniString("Database", "DestinationMediaTempTableName", "Destinations_Media_Temp");  // the Destinations table name
			g_pprospero->m_settings.m_pszDestinationMedia = file.GetIniString("Database", "DestinationMediaTableName", "Destinations_Media");  // the Destinations table name
			g_pprospero->m_settings.m_pszDestinations = file.GetIniString("Database", "DestinationsTableName", "Destinations");  // the Destinations table name
			g_pprospero->m_settings.m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue");  // the Queue table name

			g_pprospero->m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds

		}
*/
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = PROSPERO_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCategory="";
				CString szParameter="";
				CString szValue="";
				CString szTemp="";
				int min, max;
				bool bmin = false, bmax = false;
				try
				{
					prs->GetFieldValue("category", szCategory);  //HARDCODE
					prs->GetFieldValue("parameter", szParameter);  //HARDCODE
					prs->GetFieldValue("value", szValue);  //HARDCODE
					prs->GetFieldValue("min_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						min = atoi(szTemp);
						bmin = true;
					}
					prs->GetFieldValue("max_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						max = atoi(szTemp);
						bmax = true;
					}
				}
				catch( ... )
				{
				}

				int nLength = szValue.GetLength();
				if(szCategory.CompareNoCase("Main")==0)
				{
					if(szParameter.CompareNoCase("Name")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszName) free(m_pszName);
								m_pszName = pch;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("License")==0)
				{
					if(szParameter.CompareNoCase("Key")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLicense) free(m_pszLicense);
								m_pszLicense = pch;

								// recompile license key params
								if(g_pprospero->m_data.m_key.m_pszLicenseString) free(g_pprospero->m_data.m_key.m_pszLicenseString);
								g_pprospero->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
								if(g_pprospero->m_data.m_key.m_pszLicenseString)
								sprintf(g_pprospero->m_data.m_key.m_pszLicenseString, "%s", szValue);

								g_pprospero->m_data.m_key.InterpretKey();

								char errorstring[MAX_MESSAGE_LENGTH];
								if(g_pprospero->m_data.m_key.m_bValid)
								{
									unsigned long i=0;
									while(i<g_pprospero->m_data.m_key.m_ulNumParams)
									{
										if((g_pprospero->m_data.m_key.m_ppszParams)
											&&(g_pprospero->m_data.m_key.m_ppszValues)
											&&(g_pprospero->m_data.m_key.m_ppszParams[i])
											&&(g_pprospero->m_data.m_key.m_ppszValues[i]))
										{
											if(stricmp(g_pprospero->m_data.m_key.m_ppszParams[i], "max")==0)
											{
												g_pprospero->m_data.m_nMaxLicensedDevices = atoi(g_pprospero->m_data.m_key.m_ppszValues[i]);
											}
										}
										i++;
									}
								
									if(
											(
												(!g_pprospero->m_data.m_key.m_bExpires)
											||((g_pprospero->m_data.m_key.m_bExpires)&&(!g_pprospero->m_data.m_key.m_bExpired))
											||((g_pprospero->m_data.m_key.m_bExpires)&&(g_pprospero->m_data.m_key.m_bExpireForgiveness)&&(g_pprospero->m_data.m_key.m_ulExpiryDate+g_pprospero->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
											)
										&&(
												(!g_pprospero->m_data.m_key.m_bMachineSpecific)
											||((g_pprospero->m_data.m_key.m_bMachineSpecific)&&(g_pprospero->m_data.m_key.m_bValidMAC))
											)
										)
									{
					// this overrides stuff so let's not let it do that.

										// let's just not override this in a green condition.  
										// comment it out.
//										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//										g_pprospero->m_data.SetStatusText(errorstring, PROSPERO_STATUS_OK);
									}
									else
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
										g_pprospero->m_data.SetStatusText(errorstring, PROSPERO_STATUS_ERROR);
									}
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
									g_pprospero->m_data.SetStatusText(errorstring, PROSPERO_STATUS_ERROR);
								}

							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("CommandServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usCommandPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("StatusServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usStatusPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Messager")==0)
				{
					if(szParameter.CompareNoCase("UseEmail")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseEmail = true;
							else m_bUseEmail = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseNet")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseNetwork = true;
							else m_bUseNetwork = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseLog = true;
							else m_bUseLog = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseXMLClientLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseXMLClientLog = true;
							else m_bUseXMLClientLog = false;
						}
					}
				}
/*
				else
				if(szCategory.CompareNoCase("HarrisAPI")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("UseListCount")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseListCount = true;
							else m_bUseListCount = false;
						}
					}
				}
*/
				else
				if(szCategory.CompareNoCase("Database")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("SettingsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszSettings) free(m_pszSettings);
								m_pszSettings = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ExchangeTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszExchange) free(m_pszExchange);
								m_pszExchange = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("MessagesTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszMessages) free(m_pszMessages);
								m_pszMessages = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("DestinationMediaTempTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszDestinationMediaTemp) free(m_pszDestinationMediaTemp);
								m_pszDestinationMediaTemp = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("DestinationMediaTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszDestinationMedia) free(m_pszDestinationMedia);
								m_pszDestinationMedia = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("DestinationsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszDestinations) free(m_pszDestinations);
								m_pszDestinations = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("QueueTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszQueue) free(m_pszQueue);
								m_pszQueue = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ModificationCheckInterval")==0)
					{
						if(nLength>0)
						{
							nLength = atol(szValue);
							if(nLength>0) m_ulModsIntervalMS = nLength;
						}
					}
					else
					if(szParameter.CompareNoCase("FileRefreshInterval")==0)
					{
						if(nLength>0)
						{
							nLength = atol(szValue);
							if(nLength>0) m_ulFileRefreshInterval = nLength;
						}
					}
				}
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
					if(szParameter.CompareNoCase("Messages")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeMessageDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
				}
/*
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
					if(szParameter.CompareNoCase("AsRun_Log")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeAsRunDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
				}
				

*/
				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			if(pszInfo)
			{
				_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
			}
			delete prs;


			// save changes to CSF
			Settings(false); //write
/*
			if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
			{
				// these explicts arent necessary - uncomment to write out a full file to edit...
				file.SetIniString("Main", "Name", g_pprospero->m_settings.m_pszName);
				file.SetIniString("License", "Key", g_pprospero->m_settings.m_pszLicense);

				file.SetIniInt("CommandServer", "ListenPort", g_pprospero->m_settings.m_usCommandPort);
				file.SetIniInt("StatusServer", "ListenPort", g_pprospero->m_settings.m_usStatusPort);
				file.SetIniString("FileServer", "IconPath", g_pprospero->m_settings.m_pszIconPath);

				file.SetIniInt("Messager", "UseEmail", g_pprospero->m_settings.m_bUseEmail?1:0);
				file.SetIniInt("Messager", "UseNet", g_pprospero->m_settings.m_bUseNetwork?1:0);
				file.SetIniInt("Messager", "UseLog", g_pprospero->m_settings.m_bUseLog?1:0);
				file.SetIniInt("Messager", "ReportSuccessfulOperation", g_pprospero->m_settings.m_bReportSuccessfulOperation?1:0);
				file.SetIniInt("Messager", "LogTransfers", g_pprospero->m_settings.m_bLogTransfers?1:0);

				file.SetIniString("Database", "DSN", g_pprospero->m_settings.m_pszDSN);
				file.SetIniString("Database", "DBUser", g_pprospero->m_settings.m_pszUser);
				file.SetIniString("Database", "DBPassword", g_pprospero->m_settings.m_pszPW);
				file.SetIniString("Database", "SettingsTableName", g_pprospero->m_settings.m_pszSettings);  // the Settings table name
				file.SetIniString("Database", "ExchangeTableName", g_pprospero->m_settings.m_pszExchange);  // the Exchange table name
				file.SetIniString("Database", "MessagesTableName", g_pprospero->m_settings.m_pszMessages);  // the Messages table name

		//		file.SetIniInt("HarrisAPI", "UseListCount", g_pprospero->m_settings.m_bUseListCount?1:0); // get all events up until the list count (otherwise just up to the lookahead)

				file.SetIniString("Database", "DestinationsTableName", g_pprospero->m_settings.m_pszDestinations);  // the Destinations table name
				file.SetIniString("Database", "QueueTableName", g_pprospero->m_settings.m_pszQueue);  // the Queue table name
				file.SetIniString("Database", "DestinationMediaTableName", g_pprospero->m_settings.m_pszDestinationMedia);  // the DestinationMedia table name
				file.SetIniString("Database", "DestinationMediaTempTableName", g_pprospero->m_settings.m_pszDestinationMediaTemp );  // the Destinations table name

				file.SetIniInt("Database", "ModificationCheckInterval", g_pprospero->m_settings.m_ulModsIntervalMS);  // in milliseconds

				file.SetSettings(PROSPERO_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

			}
*/
			return PROSPERO_SUCCESS;
		}
	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s%s%s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. ",
				m_pdb?"":"Database pointer was NULL. ",
				m_pdbConn?"":"Connection pointer was NULL. ",
				((m_pdbConn)&&(!m_pdbConn->m_bConnected))?"Database not connected. ":""
				);
		}
	}
	return PROSPERO_ERROR;
}


char* CProsperoSettings::ProcessString(char* pszString, bool bFreeIncomingString)
{
	if(pszString)
	{
		char pszOutput[4096];
		int nInLen=strlen(pszString);
		int nOutLen=0;
		char* pch = pszString;
		char* pchEnd = min((pch+4096),(pszString+nInLen));
		while((*pch!=0)&&(pch<pchEnd))
		{
			if(*pch == '%')
			{
				// we are in a specifier.
				// here is a list of ours:
				// %h  local hostname
		
				pch++;
				if((*pch!=0)&&(pch<pchEnd))
				{
					if(*pch == 'h')
					{
						if((g_pprospero->m_data.m_pszHost)&&(strlen(g_pprospero->m_data.m_pszHost)))
						{
							char* pszTemp = (char*)malloc(strlen(g_pprospero->m_data.m_pszHost)+1);
							if(pszTemp)
							{
								strcpy(pszTemp, g_pprospero->m_data.m_pszHost);
							
								char* pchPlus = pszTemp;
								while((*pchPlus != 0)&&(nOutLen<4096))
								{
									pszOutput[nOutLen]= *pchPlus;
									nOutLen++;
									pchPlus++;
								}
								free(pszTemp);
							}
						}
						pch++;
					}
					else
					{
						// some other specifier, can be anything.
						pszOutput[nOutLen]= *(pch-1); // do add the %, this may get further processed
						nOutLen++;
						pszOutput[nOutLen]= *pch;
						nOutLen++;
						pch++;
					}
				}
				else
				{
					pszOutput[nOutLen]= *(pch-1);
					nOutLen++;
				}
				
			}
			else
			{
				pszOutput[nOutLen]= *pch;
				nOutLen++;
				pch++;
			}
		}

		pszOutput[nOutLen]=0;

		pch = (char*)malloc(strlen(pszOutput)+1);
		if(pch)
		{
			strcpy(pch,pszOutput);
			if(bFreeIncomingString) 
			{
				try {free(pszString);} catch(...){}
			}
		}
		return pch;
	}
	return NULL;
}




