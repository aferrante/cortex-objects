// HeliosData.h: interface for the CHeliosData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HELIOSDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
#define AFX_HELIOSDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <winsock2.h>
#include <sys/timeb.h>
#include "../../Common/TXT/BufferUtil.h" 
#include "../../Common/LAN/NetUtil.h" 
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"
#include "../../Common/API/Omnibus/OmniComm.h"
#include "../../Common/KEY/LicenseKey.h"

// these are data reference objects, pulled from the database.
// actual connection, list, and event objects are in the CADC object
class CHeliosConnectionObject  
{
public:
	CHeliosConnectionObject();
	virtual ~CHeliosConnectionObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	unsigned short m_usPort;  // the port on which to communicate with Omnibus.

	char* m_pszServerName;  // of the colosss adaptor
	char* m_pszDesc;

	CCAConn* m_pCAConn;  // pointer to the associated colossus adaptor connection.

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;

	// control
//	bool m_bKillConnThread;
//	bool m_bConnThreadStarted;
};


class CHeliosChannelObject  
{
public:
	CHeliosChannelObject();
	virtual ~CHeliosChannelObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	int m_nChannelID;  // the unique Channel ID within Helios setup, same as Omnibus channel number

	char* m_pszDesc;

// control
//	bool* m_pbKillConnThread;
//	bool m_bKillChannelThread;
//	bool m_bChannelThreadStarted;
};



class CHeliosData  
{
public:
	CHeliosData();
	virtual ~CHeliosData();

	// util object
	CBufferUtil m_bu;

  // hostname.  have to get it, store it here
	char*	m_pszHost;	// the name of the host
	char*	m_pszCompleteHost;	// the complete name of the host
	char*	m_pszCortexHost;	// the name of the cortex host
	unsigned short m_usCortexCommandPort;
	unsigned short m_usCortexStatusPort;

	bool m_bNetworkMessagingInitialized;
	bool m_bCheckModsWarningSent;
	bool m_bCheckMsgsWarningSent;
//	bool m_bCheckAsRunWarningSent;

	CHeliosConnectionObject** m_ppConnObj;
	int m_nNumConnectionObjects;

	CHeliosChannelObject** m_ppChannelObj;
	int m_nNumChannelObjects;

	_timeb m_timebAutoPurge; // the last time autopurge was run
	_timeb m_timebTick; // the last time check inside the thread
	_timeb m_timebLastStatus; // the time of the last status given to cortex
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for cortex global status (each obj has their own as well)

	int m_nSettingsMod;
	int m_nChannelsMod;
	int m_nConnectionsMod;
	int m_nLastSettingsMod;
	int m_nLastChannelsMod;
	int m_nLastConnectionsMod;

	char* GetStatusText(unsigned long* pulStatus);  // allocates mem, must free after use
	int		SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError = false);
	int		GetHost();

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;
	CDBUtil* m_pdb2;
	CDBconn* m_pdb2Conn;
	int CheckMessages(char* pszInfo=NULL);
//	int CheckAsRun(char* pszInfo=NULL);
	int CheckDatabaseMods(char* pszInfo=NULL);
	int IncrementDatabaseMods(char* pszTableName, char* pszInfo=NULL);
	int GetConnections(char* pszInfo=NULL);
	int GetChannels(char* pszInfo=NULL);
//	int IsChannelActive(int nChannelID=-1);  // moved to omnicomm
	// dont need IsConnectionActive because non acitve connections cut the comm thread.

	CLicenseKey m_key;
	int m_nMaxLicensedChannels;

	bool m_bProcessSuspended;
	bool m_bQuietKill;
	CRITICAL_SECTION m_critChannels; 
	CRITICAL_SECTION m_critIncrement; 
	CRITICAL_SECTION m_critSQL;

private:
	CRITICAL_SECTION m_critText;
	char* m_pszStatus;	// parseable string

//	unsigned long m_ulReqCount;
//	unsigned long m_ulSvcCount;
};

#endif // !defined(AFX_HELIOSDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
