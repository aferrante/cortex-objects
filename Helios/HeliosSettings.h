// HeliosSettings.h: interface for the CHeliosSettings class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HELIOSSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
#define AFX_HELIOSSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "HeliosDefines.h"
#include "../../Common/MFC/ODBC/DBUtil.h"

class CHeliosSettings  
{
public:
	CHeliosSettings();
	virtual ~CHeliosSettings();

	int GetFromDatabase(char* pszInfo=NULL);  //only get.  Interface publishes
	int Settings(bool bRead);
	char* ProcessString(char* pszString, bool bFreeIncomingString);

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;

	char* m_pszName;  // familiar name of this instance (and therefore the name of the Helios database)
	char* m_pszType;  // familiar name of the type.
	char* m_pszProject;  // familiar name of the project name.
	unsigned long m_ulMainMode;

	// ports
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;

	// The following flags determine what settings are loaded. 
	// The specifics do not need to be stored in cortex settings, they are pushed to the various objects.

	// messaging for Helios
	bool m_bUseLog;			// write a log file
	bool m_bUseEmail;		// send an email on failures (or commanded from remotes)
	bool m_bUseNetwork; // send a message to a remote host
	bool m_bLogNetworkErrors; // enables messaging into network object (logs socket errors, etc).
	char* m_pszFileSpec;
	char* m_pszMailSpec;
	char* m_pszProcessedFileSpec;
	char* m_pszProcessedMailSpec;
	bool m_bSendMsg; // send a message to the UI
	bool m_bListStatusInExchange; // old way, in exchange table.
	bool m_bStatusInTables; // new way, in Connections (server status) and Channels (list status) table.
	bool m_bMillisecondMessaging;			// use millisecond resolution for messages and asrun

	int m_nThreadDwellMS;
	int m_nInitStatusDwellMS;
	bool m_bDebugLists;  // if true, dumps the temp and events databases
	int  m_nDebugListTopCount;  //if positive, just prints out the top n recored in the events tables.
	bool m_bDebugSQL;  // if true, prints out the SQL calls, all but event inserts
	bool m_bDebugInsertSQL;  // if true, prints out the event insert SQL calls
	unsigned long m_ulDebug;  // prints out debug statements that & with this.
	char* m_pszDebugOrder; // the order by clause for debug


	int m_nAutoPurgeMessageDays;
//	int m_nAutoPurgeAsRunDays;
	int m_nAutoPurgeInterval;


	// database stuff
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;

	char* m_pszSettings;  // the Settings table name
	char* m_pszExchange;  // the Exchange table name
	char* m_pszMessages;  // the Messages table name
	char* m_pszAsRun;  // the As run table name
	char* m_pszControlModules;  // the control modules table name - DLLs that get commanded by changes in Sentinel
	char* m_pszInterpreterModules;  // the interpreter modules table name - DLLs that accept automation data from an external source

	char* m_pszChannels;  // the Channels table name
	char* m_pszConnections;  // the Connections table name
	char* m_pszLiveEvents;  // the LiveEvents table name

	bool m_bClearEventsInit;  // if true, clears channel based event table on init
	bool m_bClearEventsExit;  // if true, clears channel based event table on exit
	bool m_bUseActive;	// use active chanel monitoring
	bool m_bCompareEventsConnect;  // if true, compares events received on a new connect, removes any not found in the get
	bool m_bClearEventsConnect;  // if true, clears channel based event table on connection

	//OmnibusXML
	char* m_pszOmnibusDataFields;  // comma separated list of data fields
	char* m_pszDebugLog;
	char* m_pszCommLog;
	bool  m_bWriteXML;
	bool  m_bUseITXInterface;  // otherwise, Colossus
	bool  m_bUseSystemClock;  // use the system clock for servertime.

	bool m_bMultiThreadChannelProcessing;			// dispatch XML to channel based processing (will interleave events in time so that one channel does not block top events of others)

	int m_nMessageEventCountLimit;  // parse only so many events per message, discard the rest.

	int m_nLookaheadTimeLimitSec;  // Seconds from "now" to not process adds and updates. discard the rest.  Deletions are not filtered.
	int m_nLookbehindTimeLimitSec;  // Seconds before "now" to not process adds and updates. discard the rest.  Deletions are not filtered.

	int m_nAsyncDatabaseLookaheadSec;  // immediately commit to the DB any events that have an on air time of "now" plus this number of seconds, or prior. otherwise commit events as they come into the window.
	int m_nAsyncDatabaseCheckIntervalMS;  // only check for uncommitted events every so often.
	int m_nMinimumListUpdateIntervalMS;  // prevent database updates on list status faster than this interval
	int m_nMinimumServerUpdateIntervalMS;  // prevent database updates on server status faster than this interval
	bool m_bPreventServerTimeFromEvents;			// ignore the current time reference within events.


	// Omnibus old event purge
	unsigned long m_ulCheckInterval;	// number of seconds between checks (0 turns off)
	unsigned long m_ulExpiryPeriod;	// when the event's end time (start plus duration) is more than this number of seconds older than the current server time, it is purged.
	unsigned long m_ulConnTimeout;	// when the connection has received no update for number of seconds, the connection is restarted 
	unsigned long m_ulConnectionRetries;		// how many times to try to re-establish ULONG_MAX-1 is 4085 years with 30 second interval!
	unsigned long m_ulConnectionInterval;	// how long to wait in seconds before retrying
	unsigned long m_ulInterConnectionIntervalMS;  // interval to wait in between establishment of connections in GetConnections
	unsigned long m_ulInterEventIntervalMS;   	// time to stall between committing each event to the database
	unsigned long m_ulThreadDelayMS;   	// time to stall between starting the connection thread and getting events

	unsigned long m_ulModsIntervalMS;  // interval on which to check database mods

	bool m_bUseXMLClientLog;			// write a log file

	char* m_pszLicense;  // the License Key
	char* m_pszOEMcodes;  // the possible OEM string
	char* m_pszIconPath;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.
};

#endif // !defined(AFX_HELIOSSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
