// HeliosDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(HELIOSDEFINES_H_INCLUDED)
#define HELIOSDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define HELIOS_CURRENT_VERSION		"2.1.1.18"

// modes
#define HELIOS_MODE_DEFAULT			0x00000000  // exclusive
#define HELIOS_MODE_LISTENER		0x00000001  // exclusive
#define HELIOS_MODE_CLONE				0x00000002  // exclusive
#define HELIOS_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define HELIOS_MODE_VOLATILE		0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define HELIOS_MODE_MASK				0x0000000f  // 

// default port values.
//#define HELIOS_PORT_FILE				80		
#define HELIOS_PORT_CMD					10672		
#define HELIOS_PORT_STATUS			10673		

#define HELIOS_PORT_INVALID			0	

#define HELIOS_FLAG_DISABLED		0x0000	 // default
#define HELIOS_FLAG_ENABLED			0x0001	
#define HELIOS_FLAG_FOUND				0x1000
#define HELIOS_FLAG_NTSC				0x0000
#define HELIOS_FLAG_NTSCDF			0x0010
#define HELIOS_FLAG_PAL					0x0020
#define HELIOS_FLAG_ITX					0x0100   // iTX, not Colossus

#define HELIOS_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded


// status
#define HELIOS_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define HELIOS_STATUS_UNKNOWN							0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define HELIOS_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define HELIOS_STATUS_ERROR								0x00000020  // error (red icon)
#define HELIOS_STATUS_CONN								0x00000030  // ready (green icon)	
#define HELIOS_STATUS_OK									0x00000030  // ready (green icon)	
#define HELIOS_STATUS_RUN									0x00000040  // in progress, running, owned etc (blue icon);	
#define HELIOS_ICON_MASK									0x00000070  // mask	

#define HELIOS_STATUS_SUSPEND							0x00000080  // suspended	(yellow icon please)

#define HELIOS_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define HELIOS_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define HELIOS_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define HELIOS_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define HELIOS_STATUS_CMDSVR_MASK					0x00007000  // command server mask bits

#define HELIOS_STATUS_STATUSSVR_START			0x00010000  // starting the status server
#define HELIOS_STATUS_STATUSSVR_RUN				0x00020000  // status server running
#define HELIOS_STATUS_STATUSSVR_END				0x00030000  // status server shutting down
#define HELIOS_STATUS_STATUSSVR_ERROR			0x00040000  // status server error
#define HELIOS_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define HELIOS_STATUS_THREAD_START				0x00100000  // starting the main thread
#define HELIOS_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define HELIOS_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define HELIOS_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define HELIOS_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define HELIOS_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define HELIOS_STATUS_THREAD_MASK					0x00f00000  // main thread mask bits

// various failures...
#define HELIOS_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define HELIOS_STATUS_FAIL_DB							0x20000000  // could not get DB
#define HELIOS_STATUS_FAIL_MASK						0xf0000000  // failure code mask bits

//return values
#define HELIOS_SUCCESS   0
#define HELIOS_ERROR	   -1


// default filenames
#define HELIOS_SETTINGS_FILE_SETTINGS	  "helios.csr"		// csr = cortex settings redirect
#define HELIOS_SETTINGS_FILE_DEFAULT	  "helios.csf"		// csf = cortex settings file

// debug defines
#define HELIOS_DEBUG_CONN						0x00000001
#define HELIOS_DEBUG_TIMING					0x00000002
#define HELIOS_DEBUG_PROCESS				0x00000004
#define HELIOS_DEBUG_SQL						0x00000008
#define HELIOS_DEBUG_COMM						0x00000200
#define HELIOS_DEBUG_COMMLOG				0x00000400
#define HELIOS_DEBUG_EVENT_PROCESS	0x00000800
#define HELIOS_DEBUG_EVENT_ID				0x00001000



#endif // !defined(HELIOSDEFINES_H_INCLUDED)
