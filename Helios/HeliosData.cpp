// HeliosData.cpp: implementation of the CHeliosData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <process.h>
#include "Helios.h"
#include "HeliosHandler.h" 
#include "HeliosMain.h" 
#include "HeliosData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CHeliosMain* g_phelios;
extern CHeliosApp theApp;
//extern CADC g_adc; 	// the Harris ADC object
extern COmniComm g_omni;
extern CCAActives g_actives;

extern bool g_bThreadStarted;
extern bool g_bKillStatus;


//extern void HeliosConnectionThread(void* pvArgs);

void SendMessageToHelios(int nType, char* pszSender, char* pszError)
{
	if(g_phelios)
	{
//		EnterCriticalSection(&(g_phelios->m_critSendMsg));  // do these outside
		g_phelios->SendMsg(nType, pszSender, pszError);
//		LeaveCriticalSection(&(g_phelios->m_critSendMsg));  // do these outside
	}
}



//////////////////////////////////////////////////////////////////////
// CHeliosConnectionObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHeliosConnectionObject::CHeliosConnectionObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_usPort = 10540;  // the port on which to communicate with Omnibus.
	m_ulStatus	= HELIOS_STATUS_UNINIT;
	m_ulFlags = HELIOS_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
	m_pCAConn = NULL;
//	m_bKillConnThread = true;
//	m_bConnThreadStarted = false;

	m_pdb=NULL;
	m_pdbConn=NULL;
}

CHeliosConnectionObject::~CHeliosConnectionObject()
{
//	m_bKillConnThread = true;
//	while(	m_bConnThreadStarted ) Sleep(1);

	try
	{
		if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
		if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
	}
	catch(...)
	{
	}
}



//////////////////////////////////////////////////////////////////////
// CHeliosChannelObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHeliosChannelObject::CHeliosChannelObject()
{
	m_pszDesc		= NULL;
	m_ulStatus	= HELIOS_STATUS_UNINIT;
	m_ulFlags = HELIOS_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
	m_nChannelID = -1; // unassigned
//	m_pbKillConnThread = NULL;
//	m_bKillChannelThread = true;
//	m_bChannelThreadStarted = false;
//	m_pAPIConn = NULL;
}

CHeliosChannelObject::~CHeliosChannelObject()
{
	try
	{
		if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
//	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
	}
	catch(...)
	{
	}
}




//////////////////////////////////////////////////////////////////////
// CHeliosData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHeliosData::CHeliosData()
{
	InitializeCriticalSection(&m_critText);
	InitializeCriticalSection(&m_critSQL);
	InitializeCriticalSection(&m_critChannels);
	
	// messaging...
	m_bNetworkMessagingInitialized=false;

	m_bCheckModsWarningSent = false;

	m_nMaxLicensedChannels=-1;

	m_ppConnObj = NULL;
	m_nNumConnectionObjects = 0;
	m_ppChannelObj = NULL;
	m_nNumChannelObjects = 0;
	m_bProcessSuspended=false;


	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebAutoPurge ); // the last time autopurge was run
	_ftime( &m_timebTick );
	m_pszHost = NULL;			// the name of the host
	m_pszCompleteHost = NULL;	// the complete name of the host
	m_pszCortexHost = NULL;			// the name of the cortex host
	m_usCortexCommandPort = HELIOS_PORT_CMD;
	m_usCortexStatusPort = HELIOS_PORT_STATUS;
	m_nSettingsMod = -1;
	m_nChannelsMod = -1;
	m_nConnectionsMod = -1;
	m_nLastSettingsMod = -1;
	m_nLastChannelsMod = -1;
	m_nLastConnectionsMod = -1;
	m_pdb = NULL;
	m_pdbConn = NULL;
	m_pdb2 = NULL;
	m_pdb2Conn = NULL;
	m_bQuietKill = false;

	InitializeCriticalSection(&m_critIncrement);

}

CHeliosData::~CHeliosData()
{
	if(m_ppConnObj)
	{
		int i=0;
		while(i<m_nNumConnectionObjects)
		{
			try
			{
				if(m_ppConnObj[i]) delete m_ppConnObj[i]; // delete objects, must use new to allocate
			}	catch(...) {}
			m_ppConnObj[i] = NULL;
			i++;
		}
		delete [] m_ppConnObj; // delete array of pointers to objects, must use new to allocate
	}

	if(m_ppChannelObj)
	{
		int i=0;
		while(i<m_nNumChannelObjects)
		{
			try
			{
				if(m_ppChannelObj[i]) delete m_ppChannelObj[i]; // delete objects, must use new to allocate
			}	catch(...) {}
			m_ppChannelObj[i] = NULL;
			i++;
		}
		try
		{
			delete [] m_ppChannelObj; // delete array of pointers to objects, must use new to allocate
		}	catch(...) {}
	}

	if(m_pszHost) free(m_pszHost);	// must use malloc to allocate
	if(m_pszCompleteHost) free(m_pszCompleteHost);	// must use malloc to allocate
	if(m_pszCortexHost) free(m_pszCortexHost);	// must use malloc to allocate

	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critIncrement);
	DeleteCriticalSection(&m_critSQL);
	DeleteCriticalSection(&m_critChannels);
}


char* CHeliosData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CHeliosData::SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError)
{
	int nRV = CX_ERROR;
	bool bError=false;

	if(!bOverwriteExistingError)
	{
		if(
			  ((m_ulFlags&HELIOS_ICON_MASK) == HELIOS_STATUS_ERROR)
			||((m_ulFlags&HELIOS_STATUS_CMDSVR_MASK) == HELIOS_STATUS_CMDSVR_ERROR)
			||((m_ulFlags&HELIOS_STATUS_STATUSSVR_MASK) ==  HELIOS_STATUS_STATUSSVR_ERROR)
			||((m_ulFlags&HELIOS_STATUS_THREAD_MASK) == HELIOS_STATUS_THREAD_ERROR)
			||((m_ulFlags&HELIOS_STATUS_FAIL_MASK) == HELIOS_STATUS_FAIL_DB)
			)	bError=true;
	}

	if (ulStatus&HELIOS_ICON_MASK)
	{
		m_ulFlags &= ~HELIOS_ICON_MASK;
		m_ulFlags |= (ulStatus&HELIOS_ICON_MASK);
	}
	if (ulStatus&HELIOS_STATUS_CMDSVR_MASK)
	{
		m_ulFlags &= ~HELIOS_STATUS_CMDSVR_MASK;
		m_ulFlags |= (ulStatus&HELIOS_STATUS_CMDSVR_MASK);
	}
	if (ulStatus&HELIOS_STATUS_STATUSSVR_MASK)
	{
		m_ulFlags &= ~HELIOS_STATUS_STATUSSVR_MASK;
		m_ulFlags |= (ulStatus&HELIOS_STATUS_STATUSSVR_MASK);
	}
	if (ulStatus&HELIOS_STATUS_THREAD_MASK)
	{
		m_ulFlags &= ~HELIOS_STATUS_THREAD_MASK;
		m_ulFlags |= (ulStatus&HELIOS_STATUS_THREAD_MASK);
	}
	if (ulStatus&HELIOS_STATUS_FAIL_MASK)
	{
		m_ulFlags &= ~HELIOS_STATUS_FAIL_MASK;
		m_ulFlags |= (ulStatus&HELIOS_STATUS_FAIL_MASK);
	}

	if(bError)
	{
		m_ulFlags &= ~HELIOS_ICON_MASK;
		m_ulFlags |= HELIOS_STATUS_ERROR;
	}

	if((pszText)&&(!bError))
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
//				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
//			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_phelios)&&(g_phelios->m_settings.m_pszIconPath)&&(strlen(g_phelios->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_phelios->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_phelios->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_phelios->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_phelios->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_phelios->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}

// utility
int	CHeliosData::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszCompleteHost) free(m_pszCompleteHost);
						m_pszCompleteHost=pch;

						pch = (char*) malloc(nValue+1);
						if(pch!=NULL)
						{
							if(m_pszHost) free(m_pszHost);
							m_pszHost=pch;

							strcpy(m_pszHost, m_pszCompleteHost);
							pch = strchr(m_pszHost, '.');
							if(pch)
							{
								*pch = 0;
							}

							return HELIOS_SUCCESS;
						}
					}
				}
			}
		}
	}
	return HELIOS_ERROR;
}

int CHeliosData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_phelios)&&(g_phelios->m_settings.m_pszExchange)&&(strlen(g_phelios->m_settings.m_pszExchange)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s set mod = \
(SELECT case when max(mod) is null then 1 when max(mod) >= %d \
then 1 else max(mod) + 1 end from %s WHERE criterion = '%s') WHERE criterion = '%s'",
			g_phelios->m_settings.m_pszExchange,
			HELIOS_DB_MOD_MAX,
			g_phelios->m_settings.m_pszExchange,
			szTemp, szTemp		
			);
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return HELIOS_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return HELIOS_ERROR;
}


int CHeliosData::CheckMessages(char* pszInfo)
{
	if((g_phelios)&&(m_pdbConn)&&(m_pdb)
		&&(g_phelios->m_settings.m_pszMessages)&&(strlen(g_phelios->m_settings.m_pszMessages)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_phelios->m_settings.m_pszMessages)&&(strlen(g_phelios->m_settings.m_pszMessages)))?g_phelios->m_settings.m_pszMessages:"Messages",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_phelios->m_settings.m_nAutoPurgeMessageDays*86400) // local time....
			);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return HELIOS_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return HELIOS_ERROR;
}
/*
int CHeliosData::CheckAsRun(char* pszInfo)
{
	if((g_phelios)&&(m_pdbConn)&&(m_pdb)
		&&(g_phelios->m_settings.m_pszAsRun)&&(strlen(g_phelios->m_settings.m_pszAsRun)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_phelios->m_settings.m_pszAsRun)&&(strlen(g_phelios->m_settings.m_pszAsRun)))?g_phelios->m_settings.m_pszAsRun:"AsRun_Log",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_phelios->m_settings.m_nAutoPurgeAsRunDays*86400) // local time....
			);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return HELIOS_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return HELIOS_ERROR;
}
*/

int CHeliosData::CheckDatabaseMods(char* pszInfo)
{
	if((g_phelios)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_phelios->m_settings.m_pszExchange)&&(strlen(g_phelios->m_settings.m_pszExchange)))?g_phelios->m_settings.m_pszExchange:"Exchange");

		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = HELIOS_SUCCESS;
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				CString szCriterion;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
				}

				if((g_phelios->m_settings.m_pszSettings)&&(strlen(g_phelios->m_settings.m_pszSettings)))
				{
					szTemp.Format("DBT_%s",g_phelios->m_settings.m_pszSettings);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nSettingsMod = nReturn;
					}
				}

				if((g_phelios->m_settings.m_pszChannels)&&(strlen(g_phelios->m_settings.m_pszChannels)))
				{
					szTemp.Format("DBT_%s",g_phelios->m_settings.m_pszChannels);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nChannelsMod = nReturn;
					}
				}

				if((g_phelios->m_settings.m_pszConnections)&&(strlen(g_phelios->m_settings.m_pszConnections)))
				{
					szTemp.Format("DBT_%s",g_phelios->m_settings.m_pszConnections);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nConnectionsMod = nReturn;
					}
				}


				// get the suspend

				szTemp.Format("Suspend");
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					nReturn = atoi(szMod);
					if(nReturn>0)
					{
						if(!m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Helios is suspended.");  
							g_phelios->m_data.SetStatusText(errorstring, CX_STATUS_UNKNOWN);
							g_phelios->m_msgr.DM(MSG_ICONNONE, NULL, "Helios:suspend", "*** Helios has been suspended. ***"); // Sleep(50); //(Dispatch message)
							g_phelios->SendMsg(CX_SENDMSG_INFO, "Helios:suspend", "Helios has been suspended");
							m_bProcessSuspended = true;
						}
					}
					else
					{
						if(m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Helios is running.");  
							g_phelios->m_data.SetStatusText(errorstring, CX_STATUS_OK);
							g_phelios->m_msgr.DM(MSG_ICONNONE, NULL, "Helios:resume", "*** Helios has been resumed. ***");//  Sleep(50); //(Dispatch message)
							g_phelios->SendMsg(CX_SENDMSG_INFO, "Helios:resume", "Helios has been resumed.");
							m_bProcessSuspended = false;
						}
					}
				}

				nIndex++;
				prs->MoveNext();
			}
			prs->Close();


			try
			{
				delete prs;
			}	catch(...) {}

			LeaveCriticalSection(&m_critSQL);
			return nReturn;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return HELIOS_ERROR;
}

int CHeliosData::GetChannels(char* pszInfo)
{
	if((g_phelios)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, flags, description FROM %s", 
			((g_phelios->m_settings.m_pszChannels)&&(strlen(g_phelios->m_settings.m_pszChannels)))?g_phelios->m_settings.m_pszChannels:"Channels");

		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = HELIOS_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
//				CString szServer = "";
				CString szDesc = "";
				CString szTemp = "";
				unsigned long ulFlags;   // various flags
//				int nListID = -1;
				int nChannelID = -1;
				int nTemp;
				bool bFound = false;
				bool bFlagsFound = false;
				try
				{
					prs->GetFieldValue("ID", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nChannelID = nTemp;
					}
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bFlagsFound = true;
						ulFlags = atol(szTemp);
					}
					prs->GetFieldValue("description", szDesc);//HARDCODE
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				if((g_phelios->m_data.m_ppChannelObj)&&(g_phelios->m_data.m_nNumChannelObjects))
				{
					nTemp=0;
					while(nTemp<g_phelios->m_data.m_nNumChannelObjects)
					{
						if((g_phelios->m_data.m_ppChannelObj[nTemp]))//&&(g_phelios->m_data.m_ppChannelObj[nTemp]->m_pszServerName))
						{
							if(
								  (nChannelID==g_phelios->m_data.m_ppChannelObj[nTemp]->m_nChannelID)
								)
							{
								bFound = true;
								// override with the new changes:
								if(
										(szDesc.GetLength()>0)
									&&((g_phelios->m_data.m_ppChannelObj[nTemp]->m_pszDesc==NULL)||(szDesc.CompareNoCase(g_phelios->m_data.m_ppChannelObj[nTemp]->m_pszDesc)))
									)
								{
									if(g_phelios->m_data.m_ppChannelObj[nTemp]->m_pszDesc) free(g_phelios->m_data.m_ppChannelObj[nTemp]->m_pszDesc);

									g_phelios->m_data.m_ppChannelObj[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(g_phelios->m_data.m_ppChannelObj[nTemp]->m_pszDesc) sprintf(g_phelios->m_data.m_ppChannelObj[nTemp]->m_pszDesc, szDesc);
								}

								if((bFlagsFound)&&(ulFlags&HELIOS_FLAG_ENABLED)&&(!((g_phelios->m_data.m_ppChannelObj[nTemp]->m_ulFlags)&HELIOS_FLAG_ENABLED)))
								{

// ****  do something here to check out if the list is able to be activated, that is, if it's on a connected connection

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating list %d%s%s...", 
										g_phelios->m_data.m_ppChannelObj[nTemp]->m_nChannelID,  
										g_phelios->m_data.m_ppChannelObj[nTemp]->m_pszDesc?": ":"", 
										g_phelios->m_data.m_ppChannelObj[nTemp]->m_pszDesc?g_phelios->m_data.m_ppChannelObj[nTemp]->m_pszDesc:"");  
									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:channel_change", errorstring); // Sleep(20);  //(Dispatch message)

									if(g_actives.m_bUseActive)
									{
										EnterCriticalSection(&g_actives.m_critActive);
										g_actives.AddActiveChannel(nChannelID);
										LeaveCriticalSection(&g_actives.m_critActive);
									}

									if((g_phelios->m_settings.m_bClearEventsInit)&&(m_pdb2)&&(m_pdb2Conn))
									{
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
															"DELETE FROM %s WHERE %s = %d", 
															g_phelios->m_settings.m_pszLiveEvents,
															OMNI_DB_LISTID_NAME, nChannelID
															);

										if(m_pdb2->ExecuteSQL(m_pdb2Conn, szSQL, errorstring)<DB_SUCCESS)
										{
											// msg?
											g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:delete_change_init", "SQL error %s", errorstring);  //  Sleep(20);   //(Dispatch message)
										}

									}
// **** if it's able to be activated, set the status to connected.
									g_phelios->m_data.m_ppChannelObj[nTemp]->m_ulStatus = HELIOS_STATUS_CONN;

								}
								

								if((bFlagsFound)&&(!(ulFlags&HELIOS_FLAG_ENABLED))&&((g_phelios->m_data.m_ppChannelObj[nTemp]->m_ulFlags)&HELIOS_FLAG_ENABLED))
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating list %d%s%s...", 
										g_phelios->m_data.m_ppChannelObj[nTemp]->m_nChannelID,  
										g_phelios->m_data.m_ppChannelObj[nTemp]->m_pszDesc?": ":"", 
										g_phelios->m_data.m_ppChannelObj[nTemp]->m_pszDesc?g_phelios->m_data.m_ppChannelObj[nTemp]->m_pszDesc:"");  
									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:channel_change", errorstring); // Sleep(20);     //(Dispatch message)

//g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:deactivate", "X0");    Sleep(100);   //(Dispatch message)
									if(g_actives.m_bUseActive)
									{
										EnterCriticalSection(&g_actives.m_critActive);
//g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:deactivate", "X0a");    Sleep(100);   //(Dispatch message)
										g_actives.RemoveActiveChannel(nChannelID);
//g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:deactivate", "X0b");    Sleep(100);   //(Dispatch message)
										LeaveCriticalSection(&g_actives.m_critActive);
									}
//g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:deactivate", "X0c");    Sleep(100);   //(Dispatch message)

									g_phelios->m_data.m_ppChannelObj[nTemp]->m_ulStatus = HELIOS_STATUS_NOTCON;

/*
g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:deactivate", "X1");    Sleep(100);   //(Dispatch message)
g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:deactivate", "X1a");    Sleep(100);   //(Dispatch message)
g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:deactivate", "X1b");    Sleep(100);   //(Dispatch message)
g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:deactivate", "X1c");    Sleep(100);   //(Dispatch message)
*/
									// find the list object and shut down the list parsing
									int nServer=0;
									bool bListFound = false;
									while((!bListFound)&&(g_phelios->m_data.m_nNumConnectionObjects>nServer))
									{ 
//g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:deactivate", "S%d",nServer);    Sleep(100);   //(Dispatch message)


										if(
												(g_phelios->m_data.m_nNumConnectionObjects>0)
											&&(g_phelios->m_data.m_ppConnObj)
											&&(g_phelios->m_data.m_ppConnObj[nServer])
											&&(g_phelios->m_data.m_ppConnObj[nServer]->m_pCAConn)
											)
										{
								EnterCriticalSection (&g_phelios->m_data.m_ppConnObj[nServer]->m_pCAConn->m_crit);

											int nConnLists = g_phelios->m_data.m_ppConnObj[nServer]->m_pCAConn->GetListCount();
											if(nConnLists>0)
											{
												int nChannelFound = 0;
												while((!bListFound)&&(nChannelFound<g_phelios->m_data.m_nNumChannelObjects))
												{
													int nListFound = 0;
													while((g_phelios)&&(nListFound<nConnLists)&&(!bListFound))
													{
														CCAList* pList = g_phelios->m_data.m_ppConnObj[nServer]->m_pCAConn->GetList(nListFound);
														if(
																(g_phelios->m_data.m_ppChannelObj[nChannelFound])
															&&(pList)
															&&(g_phelios->m_data.m_ppChannelObj[nChannelFound]->m_nChannelID == pList->m_usListID)
															)
														{
															bListFound = true;
															pList->m_bXMLQueueKill=true;
															pList->m_ulFlags &= ~OMNI_FLAGS_ACTIVE;

															// while( pList->m_bXMLQueueStarted) Sleep(1);  // don't wait for it, no need.

															break;
														}
														nListFound++;
													}
													nChannelFound++;
												}
											}
								LeaveCriticalSection (&g_phelios->m_data.m_ppConnObj[nServer]->m_pCAConn->m_crit);

										}
										nServer++;
									}
//g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:deactivate", "X2");    Sleep(100);   //(Dispatch message)

									if((g_phelios->m_settings.m_bClearEventsExit)&&(m_pdb2)&&(m_pdb2Conn))
									{
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
															"DELETE FROM %s WHERE %s = %d", 
															g_phelios->m_settings.m_pszLiveEvents,
															OMNI_DB_LISTID_NAME, nChannelID
															);

										if(m_pdb2->ExecuteSQL(m_pdb2Conn, szSQL, errorstring)<DB_SUCCESS)
										{
											// msg?
											g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:delete_exit", "SQL error %s", errorstring);  //  Sleep(20);   //(Dispatch message)
										}

									}
//g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:deactivate", "X3");    Sleep(100);   //(Dispatch message)


								}

								// and set up the flags
								if(bFlagsFound) g_phelios->m_data.m_ppChannelObj[nTemp]->m_ulFlags = ulFlags|HELIOS_FLAG_FOUND;
								else g_phelios->m_data.m_ppChannelObj[nTemp]->m_ulFlags |= HELIOS_FLAG_FOUND;
							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(nChannelID>0)) // have to add.
				{

					CHeliosChannelObject* pscho = new CHeliosChannelObject;
					if(pscho)
					{
						CHeliosChannelObject** ppObj = new CHeliosChannelObject*[g_phelios->m_data.m_nNumChannelObjects+1];
						if(ppObj)
						{
							int o=0;
							if((g_phelios->m_data.m_ppChannelObj)&&(g_phelios->m_data.m_nNumChannelObjects>0))
							{
								while(o<g_phelios->m_data.m_nNumChannelObjects)
								{
									ppObj[o] = g_phelios->m_data.m_ppChannelObj[o];
									o++;
								}
								try
								{
									delete [] g_phelios->m_data.m_ppChannelObj;
								}	catch(...) {}
								g_phelios->m_data.m_ppChannelObj = NULL;

							}
							ppObj[g_phelios->m_data.m_nNumChannelObjects] = pscho;
							g_phelios->m_data.m_ppChannelObj = ppObj;
							g_phelios->m_data.m_nNumChannelObjects++;

//							ppObj[o]->m_pszServerName = (char*)malloc(szServer.GetLength()+1); 
//							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, szServer);

//							ppObj[o]->m_nHarrisListID = nListID;

//							if(nChannelID>=0) 
							ppObj[o]->m_nChannelID = nChannelID;

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, szDesc);
							}
//							ppObj[o]->m_usType = not used;
							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|HELIOS_FLAG_FOUND;
							else ppObj[o]->m_ulFlags |= HELIOS_FLAG_FOUND;
							
							if((g_phelios->m_settings.m_bClearEventsInit)&&(m_pdb2)&&(m_pdb2Conn))
							{
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
													"DELETE FROM %s WHERE %s = %d", 
													g_phelios->m_settings.m_pszLiveEvents,
													OMNI_DB_LISTID_NAME, nChannelID
													);

								if(m_pdb2->ExecuteSQL(m_pdb2Conn, szSQL, errorstring)<DB_SUCCESS)
								{
									// msg?
									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:delete_init", "SQL error %s", errorstring);  //  Sleep(20);   //(Dispatch message)
								}

							}

// ****  do something here to check out if the list is able to be activated, that is, if it's on a connected connection
							if((ppObj[o]->m_ulFlags)&HELIOS_FLAG_ENABLED)
							{
								if(g_actives.m_bUseActive)
								{
									EnterCriticalSection(&g_actives.m_critActive);
									g_actives.AddActiveChannel(nChannelID);
									LeaveCriticalSection(&g_actives.m_critActive);
								}

								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating list %d%s%s...", 
									ppObj[o]->m_nChannelID,  
									ppObj[o]->m_pszDesc?": ":"", 
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:"");  
								g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:channel_init", errorstring);  //  Sleep(20);   //(Dispatch message)

// **** if it's able to be activated, set the status to connected.
								ppObj[o]->m_ulStatus = HELIOS_STATUS_CONN;
							}

						}
						else
						{
							try
							{
								delete pscho;
							}	catch(...) {}
						}
					}
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			try
			{
				delete prs;
			}	catch(...) {}

			LeaveCriticalSection(&m_critSQL);

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<g_phelios->m_data.m_nNumChannelObjects)
			{
				if((g_phelios->m_data.m_ppChannelObj)&&(g_phelios->m_data.m_ppChannelObj[nIndex]))
				{
					if((g_phelios->m_data.m_ppChannelObj[nIndex]->m_ulFlags)&HELIOS_FLAG_FOUND)
					{
						(g_phelios->m_data.m_ppChannelObj[nIndex]->m_ulFlags) &= ~HELIOS_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(g_phelios->m_data.m_ppChannelObj[nIndex])
						{
							if((g_phelios->m_data.m_ppChannelObj[nIndex]->m_ulFlags)&HELIOS_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating list %d%s%s...", 
									g_phelios->m_data.m_ppChannelObj[nIndex]->m_nChannelID,  
									g_phelios->m_data.m_ppChannelObj[nIndex]->m_pszDesc?": ":"", 
									g_phelios->m_data.m_ppChannelObj[nIndex]->m_pszDesc?g_phelios->m_data.m_ppChannelObj[nIndex]->m_pszDesc:""); 
								g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:destination_remove", errorstring);   //  Sleep(20);  //(Dispatch message)
								g_phelios->m_data.m_ppChannelObj[nIndex]->m_ulStatus = HELIOS_STATUS_NOTCON;

							}

							if(g_actives.m_bUseActive)
							{
								EnterCriticalSection(&g_actives.m_critActive);
								g_actives.RemoveActiveChannel(g_phelios->m_data.m_ppChannelObj[nIndex]->m_nChannelID);
								LeaveCriticalSection(&g_actives.m_critActive);
							}

//							g_phelios->m_data.m_ppChannelObj[nIndex]->m_bKillChannelThread = true;
//							while(g_phelios->m_data.m_ppChannelObj[nIndex]->m_bChannelThreadStarted) Sleep(1);

							// find the list object and shut down the list parsing
							int nServer=0;
							bool bListFound = false;
							while((!bListFound)&&(g_phelios->m_data.m_nNumConnectionObjects>nServer))
							{ 

								if(
										(g_phelios->m_data.m_nNumConnectionObjects>0)
									&&(g_phelios->m_data.m_ppConnObj)
									&&(g_phelios->m_data.m_ppConnObj[nServer])
									&&(g_phelios->m_data.m_ppConnObj[nServer]->m_pCAConn)
									)
								{
						EnterCriticalSection (&g_phelios->m_data.m_ppConnObj[nServer]->m_pCAConn->m_crit);

									int nConnLists = g_phelios->m_data.m_ppConnObj[nServer]->m_pCAConn->GetListCount();
									if(nConnLists>0)
									{
										int nChannelFound = 0;
										while((!bListFound)&&(nChannelFound<g_phelios->m_data.m_nNumChannelObjects))
										{
											int nListFound = 0;
											while((g_phelios)&&(nListFound<nConnLists)&&(!bListFound))
											{
												CCAList* pList = g_phelios->m_data.m_ppConnObj[nServer]->m_pCAConn->GetList(nListFound);
												if(
														(g_phelios->m_data.m_ppChannelObj[nChannelFound])
													&&(pList)
													&&(g_phelios->m_data.m_ppChannelObj[nChannelFound]->m_nChannelID == pList->m_usListID)
													)
												{
													bListFound = true;
													pList->m_bXMLQueueKill=true;
													pList->m_ulFlags &= ~OMNI_FLAGS_ACTIVE;

													// while( pList->m_bXMLQueueStarted) Sleep(1);  // don't wait for it, no need.

													break;
												}
												nListFound++;
											}
											nChannelFound++;
										}
									}
						LeaveCriticalSection (&g_phelios->m_data.m_ppConnObj[nServer]->m_pCAConn->m_crit);

								}
								nServer++;
							}

							if((g_phelios->m_settings.m_bClearEventsExit)&&(m_pdb2)&&(m_pdb2Conn))
							{
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
													"DELETE FROM %s WHERE %s = %d", 
													g_phelios->m_settings.m_pszLiveEvents,
													OMNI_DB_LISTID_NAME, g_phelios->m_data.m_ppChannelObj[nIndex]->m_nChannelID
													);

								if(m_pdb2->ExecuteSQL(m_pdb2Conn, szSQL, errorstring)<DB_SUCCESS)
								{
									// msg?
									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:delete_exit", "SQL error %s", errorstring);  //  Sleep(20);   //(Dispatch message)
								}

							}

							delete g_phelios->m_data.m_ppChannelObj[nIndex];
							g_phelios->m_data.m_ppChannelObj[nIndex] = NULL;
							g_phelios->m_data.m_nNumChannelObjects--;

							int nTemp=nIndex;
							while(nTemp<g_phelios->m_data.m_nNumChannelObjects)
							{
								g_phelios->m_data.m_ppChannelObj[nTemp]=g_phelios->m_data.m_ppChannelObj[nTemp+1];
								nTemp++;
							}
							g_phelios->m_data.m_ppChannelObj[nTemp] = NULL;
						} else nIndex++;
					}
				}
				else
					nIndex++;
			}

//		g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "GetChannels returning");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
		LeaveCriticalSection(&m_critSQL);

	}
	return HELIOS_ERROR;
}

int CHeliosData::GetConnections(char* pszInfo)
{
	if((g_phelios)&&(m_pdb2Conn)&&(m_pdb2))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT conn_ip, description, flags, conn_port, server_basis FROM %s ORDER BY conn_ip",  //HARDCODE
			((g_phelios->m_settings.m_pszConnections)&&(strlen(g_phelios->m_settings.m_pszConnections)))?g_phelios->m_settings.m_pszConnections:"Adaptors");
//g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "GetConnections");  Sleep(250); //(Dispatch message)

		char error[DB_SQLSTRING_MAXLEN];
//		EnterCriticalSection(&m_critSQL);
		CRecordset* prs = m_pdb2->Retrieve(m_pdb2Conn, szSQL, error);
//g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "GetConnections %s", error);  Sleep(250); //(Dispatch message)

		
//		CRecordset* prs = m_pdb2->Retrieve(m_pdb2Conn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = HELIOS_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
//g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "GetConnections %d", nIndex);  Sleep(250); //(Dispatch message)
				CString szHost="";
//				CString szClient="";
				CString szDesc="";
				CString szTemp;
				unsigned long ulFlags = 0;   // various flags
				unsigned short usPort=10540;
				int nTemp = -1;
				bool bFlagsFound = false;
				bool bFound = false;
				try
				{
					prs->GetFieldValue("conn_ip", szHost);//HARDCODE
					szHost.TrimLeft(); szHost.TrimRight();
//g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "GetConnections %s", szHost);  Sleep(250); //(Dispatch message)
					prs->GetFieldValue("description", szDesc);//HARDCODE
//g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "GetConnections %s", szDesc);  Sleep(250); //(Dispatch message)
					prs->GetFieldValue("flags", szTemp);//HARDCODE
//g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "GetConnections e %s", szTemp);  Sleep(250); //(Dispatch message)
					if(szTemp.GetLength())
					{
						ulFlags = atol(szTemp);
						bFlagsFound  =true;
					}
					prs->GetFieldValue("conn_port", szTemp);//HARDCODE
//g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "GetConnections p %s", szTemp);  Sleep(250); //(Dispatch message)
					if(szTemp.GetLength())
					{
						nTemp = atol(szTemp);
						if (nTemp!=0) usPort =nTemp;
					}
					prs->GetFieldValue("server_basis", szTemp);//HARDCODE
//g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "GetConnections vs %s", szTemp);  Sleep(250); //(Dispatch message)
					if(szTemp.GetLength())
					{
						nTemp = atol(szTemp);
						if (nTemp!=0) ulFlags |= nTemp;
					}

//					prs->GetFieldValue("client", szClient);//HARDCODE
//					szClient.TrimLeft(); szClient.TrimRight();
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

//			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "c1");  Sleep(250); //(Dispatch message)
				if((g_phelios->m_data.m_ppConnObj)&&(g_phelios->m_data.m_nNumConnectionObjects))
				{
					nTemp=0;
					while(nTemp<g_phelios->m_data.m_nNumConnectionObjects)
					{
//			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "c2");  Sleep(250); //(Dispatch message)
						if((g_phelios->m_data.m_ppConnObj[nTemp])&&(g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName))
						{
							if((szHost.GetLength()>0)&&(szHost.CompareNoCase(g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName)==0))
							{
								bFound = true;
//			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "c3");  Sleep(250); //(Dispatch message)

								if(
										((bFlagsFound)&&(!(ulFlags&HELIOS_FLAG_ENABLED))&&((g_phelios->m_data.m_ppConnObj[nTemp]->m_ulFlags)&HELIOS_FLAG_ENABLED))
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s%s%s...", 
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName,
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?" on ":"",
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName:""
										);  
									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", errorstring); // Sleep(100);  //(Dispatch message)
//									g_phelios->m_data.m_ppConnObj[nTemp]->m_pDlg->OnDisconnect();
									//**** disconnect

//									g_adc.DisconnectServer(g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName);
//									g_phelios->m_data.m_ppConnObj[nTemp]->m_bKillConnThread = true;  // let it finish on its own..

									int nDisTime=g_omni.DisconnectServer(g_phelios->m_data.m_ppConnObj[nTemp]->m_pCAConn, 5000);
									if( nDisTime >= OMNI_SUCCESS)
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pCAConn = NULL;

if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_TIMING)
{
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnected %s%s%s in %d milliseconds (5000ms timeout)", 
		g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName,
		g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?" on ":"",
		g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName:"",
		nDisTime
		);  
	g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", errorstring); // Sleep(100);  //(Dispatch message)
}
									//**** should check return value....
	//								while((g_phelios->m_data.m_ppConnObj[nTemp])&&(g_phelios->m_data.m_ppConnObj[nTemp]->m_bConnThreadStarted)) Sleep(100);
						//			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s is disconnected.", 
						//				g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName);  
						//			g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:destination_change", errorstring);  Sleep(100);  //(Dispatch message)

									g_phelios->m_data.m_ppConnObj[nTemp]->m_ulStatus = HELIOS_STATUS_NOTCON;
								}

								if(
										(szDesc.GetLength()>0)
									&&((g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc==NULL)||(szDesc.CompareNoCase(g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc)))
									)
								{
									if(g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc) free(g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc);

									g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc) sprintf(g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc, "%s", szDesc);
								}


								if(
										((bFlagsFound)&&(ulFlags&HELIOS_FLAG_ENABLED)&&(!((g_phelios->m_data.m_ppConnObj[nTemp]->m_ulFlags)&HELIOS_FLAG_ENABLED)))
									||( (usPort!=g_phelios->m_data.m_ppConnObj[nTemp]->m_usPort)
											&&(
													((bFlagsFound)&&(ulFlags&HELIOS_FLAG_ENABLED))
												||((!bFlagsFound)&&((g_phelios->m_data.m_ppConnObj[nTemp]->m_ulFlags)&HELIOS_FLAG_ENABLED))
												)
										)
									)
								{

// cant connect again until the last thread has finished
									if((g_phelios->m_data.m_ppConnObj[nTemp])&&(g_phelios->m_data.m_ppConnObj[nTemp]->m_pCAConn)&&(g_phelios->m_data.m_ppConnObj[nTemp]->m_pCAConn->m_bThreadStarted))
									{
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", "d1");   Sleep(200); //(Dispatch message)

										int nDisTime=g_omni.DisconnectServer(g_phelios->m_data.m_ppConnObj[nTemp]->m_pCAConn, 5000);
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", "d2");   Sleep(200); //(Dispatch message)
										if( nDisTime >= OMNI_SUCCESS)
											g_phelios->m_data.m_ppConnObj[nTemp]->m_pCAConn = NULL;
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_TIMING)
{
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnected %s%s%s in %d milliseconds (5000ms timeout)", 
		g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName,
		g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?" on ":"",
		g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName:"",
		nDisTime
		);  
	g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", errorstring);  Sleep(100);  //(Dispatch message)
}

/*
										int nClock = clock()+500;
										while((g_phelios->m_data.m_ppConnObj[nTemp])&&(g_phelios->m_data.m_ppConnObj[nTemp]->m_pCAConn)&&(g_phelios->m_data.m_ppConnObj[nTemp]->m_pCAConn->m_bThreadStarted)&&(clock()<nClock))
										{
											Sleep(10);
										}
*/
									}

									g_phelios->m_data.m_ppConnObj[nTemp]->m_ulStatus = HELIOS_STATUS_NOTCON;

									g_phelios->m_data.m_ppConnObj[nTemp]->m_usPort = usPort;

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s%s%s...", 
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName,
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?" on ":"",
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName:""
										);  
									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", errorstring); //  Sleep(100); //(Dispatch message)

/*
g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:debug", "Connecting with %s, %d, %s, %s, %d, %d, %s, %s" ,
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName, 
										g_phelios->m_data.m_ppConnObj[nTemp]->m_usPort, 
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc, 
										g_phelios->m_settings.m_pszOmnibusDataFields, 
										g_phelios->m_data.m_pdb2, 
										g_phelios->m_data.m_pdb2Conn, 
										g_phelios->m_settings.m_pszLiveEvents,
										g_phelios->m_settings.m_pszExchange
										 );   Sleep(100); //(Dispatch message)
*/

								//	g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", "q");   Sleep(100); //(Dispatch message)

									if(g_phelios->m_data.m_ppConnObj[nTemp]->m_pdb)
									{
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", "a");   Sleep(200); //(Dispatch message)
										if(g_phelios->m_data.m_ppConnObj[nTemp]->m_pdbConn)
										{
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", "b");   Sleep(100); //(Dispatch message)
											g_phelios->m_data.m_ppConnObj[nTemp]->m_pdb->RemoveConnection(g_phelios->m_data.m_ppConnObj[nTemp]->m_pdbConn);
										}
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", "1");   Sleep(100); //(Dispatch message)
										try{	delete  g_phelios->m_data.m_ppConnObj[nTemp]->m_pdb; } catch(...){}
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pdb = NULL;
									}

//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", "s");   Sleep(100); //(Dispatch message)
									if(g_phelios->m_data.m_ppConnObj[nTemp]->m_pCAConn)
									{
										if(g_phelios->m_data.m_ppConnObj[nTemp]->m_pCAConn->m_pcritSQL)
										{
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", "2");   Sleep(100); //(Dispatch message)
											try{ delete g_phelios->m_data.m_ppConnObj[nTemp]->m_pCAConn->m_pcritSQL; } catch(...){}
										}
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", "3");   Sleep(100); //(Dispatch message)
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pCAConn->m_pcritSQL =  NULL;
									}
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", "4");   Sleep(100); //(Dispatch message)/

//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", "5");   Sleep(100); //(Dispatch message)



									g_phelios->m_data.m_ppConnObj[nTemp]->m_pdb = new CDBUtil;
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", "6");   Sleep(100); //(Dispatch message)
									if(g_phelios->m_data.m_ppConnObj[nTemp]->m_pdb)
									{
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_change", "7");   Sleep(100); //(Dispatch message)

									g_phelios->m_data.m_ppConnObj[nTemp]->m_pdbConn 
										= g_phelios->m_data.m_ppConnObj[nTemp]->m_pdb->CreateNewConnection(g_phelios->m_settings.m_pszDSN, g_phelios->m_settings.m_pszUser, g_phelios->m_settings.m_pszPW);
	if(g_phelios->m_data.m_ppConnObj[nTemp]->m_pdbConn)
	{
		if(g_phelios->m_data.m_ppConnObj[nTemp]->m_pdb->ConnectDatabase(g_phelios->m_data.m_ppConnObj[nTemp]->m_pdbConn, errorstring)<DB_SUCCESS)
		{
			g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_debug", "Failed to connect to database: %s", errorstring); //  Sleep(100); //(Dispatch message)
		}
		else
		{
			g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_debug", "Connected to database for %s%s%s",
												g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName,
												g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?" on ":"",
												g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName:""
				); //  Sleep(100); //(Dispatch message)
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create secondary connection for %s:%s:%s", g_phelios->m_settings.m_pszDSN, g_phelios->m_settings.m_pszUser, g_phelios->m_settings.m_pszPW); 
		g_phelios->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Helios:database2_init", errorstring);  //(Dispatch message)
		//**MSG
	}

									}
									CRITICAL_SECTION* pMsgCrit = new CRITICAL_SECTION;
									if(pMsgCrit)
									{
										InitializeCriticalSection(pMsgCrit);
									}

// with SQL
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONN)
{
	g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_debug", "Entering ConnectServer on change"); //  Sleep(100); //(Dispatch message)
}

									Sleep(250); // quarter second delay just to stagger conn threads.
									CCAConn* pConn = g_omni.ConnectServer(
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName, 
										g_phelios->m_data.m_ppConnObj[nTemp]->m_usPort, 
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc, 
										g_phelios->m_settings.m_pszOmnibusDataFields, 
//										g_phelios->m_data.m_pdb, 
//										g_phelios->m_data.m_pdbConn, 
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pdb, 
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pdbConn, 
										g_phelios->m_settings.m_pszLiveEvents,
										(g_phelios->m_settings.m_bListStatusInExchange?g_phelios->m_settings.m_pszExchange:NULL),
										(g_phelios->m_settings.m_bStatusInTables?g_phelios->m_settings.m_pszConnections:NULL),
										(g_phelios->m_settings.m_bStatusInTables?g_phelios->m_settings.m_pszChannels:NULL),
										pMsgCrit, // going to do this so that each connection has its own critsec.
//										&(g_phelios->m_data.m_critSQL),
										(void*)SendMessageToHelios,
										&(g_phelios->m_critSendMsg)
										);

if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONN)
{
	g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_debug", "Left ConnectServer on change with %d", pConn); // Sleep(100); //(Dispatch message)
}
/*
// without SQL
									CCAConn* pConn = g_omni.ConnectServer(
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pszServerName, 
										g_phelios->m_data.m_ppConnObj[nTemp]->m_usPort, 
										g_phelios->m_data.m_ppConnObj[nTemp]->m_pszDesc, 
										g_phelios->m_settings.m_pszOmnibusDataFields, 
										NULL, 
										NULL, 
										NULL,
										NULL,
										NULL
										);
*/
									if(pConn)
									{
//									g_phelios->m_data.m_ppConnObj[nTemp]->m_pDlg->OnButtonConnect();
									//**** if connect set following status
										g_phelios->m_data.m_ppConnObj[nTemp]->m_ulStatus = HELIOS_STATUS_CONN; 


										if(ulFlags&HELIOS_FLAG_PAL)	pConn->SetFrameBasis(OMNI_PAL);
										else if(ulFlags&HELIOS_FLAG_NTSCDF)	pConn->SetFrameBasis(OMNI_NTSCNDF);
										else pConn->SetFrameBasis(OMNI_NTSC);

if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONN)
{
	g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_debug", "Setting comm [%s] and debug [%s]", 
		g_phelios->m_settings.m_pszCommLog?g_phelios->m_settings.m_pszCommLog:"NULL",
		g_phelios->m_settings.m_pszDebugLog?g_phelios->m_settings.m_pszDebugLog:"NULL"); // Sleep(100); //(Dispatch message)
}
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "q1");   Sleep(200); //(Dispatch message)

										if((g_phelios->m_settings.m_pszDebugLog)&&(strlen(g_phelios->m_settings.m_pszDebugLog)>0))
											pConn->SetDebugName(g_phelios->m_settings.m_pszDebugLog);
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "q2");   Sleep(200); //(Dispatch message)
										if((g_phelios->m_settings.m_pszCommLog)&&(strlen(g_phelios->m_settings.m_pszCommLog)>0))
											pConn->SetCommLogName(g_phelios->m_settings.m_pszCommLog);
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "q3");   Sleep(200); //(Dispatch message)
										pConn->m_bWriteXMLtoFile = g_phelios->m_settings.m_bWriteXML;
										pConn->m_ulCheckInterval = g_phelios->m_settings.m_ulCheckInterval;
										pConn->m_ulExpiryPeriod = g_phelios->m_settings.m_ulExpiryPeriod;
										pConn->m_ulConnTimeout = g_phelios->m_settings.m_ulConnTimeout;
										pConn->m_nMessageEventCountLimit = g_phelios->m_settings.m_nMessageEventCountLimit;

										pConn->m_nLookaheadTimeLimitSec = g_phelios->m_settings.m_nLookaheadTimeLimitSec;  // Seconds from "now" to not process adds and updates. discard the rest.  Deletions are not filtered.
										pConn->m_nLookbehindTimeLimitSec = g_phelios->m_settings.m_nLookbehindTimeLimitSec;  // Seconds before "now" to not process adds and updates. discard the rest.  Deletions are not filtered.

										pConn->m_nAsyncDatabaseLookaheadSec = g_phelios->m_settings.m_nAsyncDatabaseLookaheadSec;  // immediately commit to the DB any events that have an on air time of "now" plus this number of seconds, or prior. otherwise commit events as they come into the window.
										pConn->m_nAsyncDatabaseCheckIntervalMS = g_phelios->m_settings.m_nAsyncDatabaseCheckIntervalMS;  // only check for uncommitted events every so often.
										pConn->m_nMinimumListUpdateIntervalMS = g_phelios->m_settings.m_nMinimumListUpdateIntervalMS;  // prevent database updates on list status faster than this interval
										pConn->m_nMinimumServerUpdateIntervalMS = g_phelios->m_settings.m_nMinimumServerUpdateIntervalMS;  // prevent database updates on server status faster than this interval
										pConn->m_bPreventServerTimeFromEvents = g_phelios->m_settings.m_bPreventServerTimeFromEvents;			// ignore the current time reference within events.


									}
									else
									{
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONN)
{
	g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_debug", "pConn was NULL"); // Sleep(100); //(Dispatch message)
}
									}
									g_phelios->m_data.m_ppConnObj[nTemp]->m_pCAConn = pConn;

								}

								g_phelios->m_data.m_ppConnObj[nTemp]->m_usPort = usPort;

								if(bFlagsFound) g_phelios->m_data.m_ppConnObj[nTemp]->m_ulFlags = ulFlags|HELIOS_FLAG_FOUND;
								else g_phelios->m_data.m_ppConnObj[nTemp]->m_ulFlags |= HELIOS_FLAG_FOUND;

								g_phelios->m_data.m_ppConnObj[nTemp]->m_ulFlags |= ((g_phelios->m_settings.m_bUseITXInterface)?OMNI_ITX:0) ;
								g_phelios->m_data.m_ppConnObj[nTemp]->m_ulFlags |= ((g_phelios->m_settings.m_bUseSystemClock)?OMNI_SYSTIME:0) ;
								g_phelios->m_data.m_ppConnObj[nTemp]->m_ulFlags |= ((g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_COMMLOG)?OMNI_DEBUG:0) ;
								g_phelios->m_data.m_ppConnObj[nTemp]->m_ulFlags |= ((g_phelios->m_settings.m_bMultiThreadChannelProcessing)?OMNI_MULTITHREAD:0) ;
								g_phelios->m_data.m_ppConnObj[nTemp]->m_ulFlags |= ((g_phelios->m_settings.m_bClearEventsConnect)?OMNI_CLEARONCONNECT:0) ;
								g_phelios->m_data.m_ppConnObj[nTemp]->m_ulFlags |= ((g_phelios->m_settings.m_bCompareEventsConnect)?OMNI_COMPAREONCONNECT:0) ;
								g_phelios->m_data.m_ppConnObj[nTemp]->m_ulFlags |= ((g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_EVENT_PROCESS)?OMNI_DEBUG_PROCESS:0) ;




							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szHost.GetLength()>0)) // have to add.
				{
//g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "adding %s", szHost);  Sleep(250); //(Dispatch message)
					CHeliosConnectionObject* pscno = new CHeliosConnectionObject;
					if(pscno)
					{
//g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CHeliosConnectionObject** ppObj = new CHeliosConnectionObject*[g_phelios->m_data.m_nNumConnectionObjects+1];
						if(ppObj)
						{
//g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((g_phelios->m_data.m_ppConnObj)&&(g_phelios->m_data.m_nNumConnectionObjects>0))
							{
								while(o<g_phelios->m_data.m_nNumConnectionObjects)
								{
									ppObj[o] = g_phelios->m_data.m_ppConnObj[o];
									o++;
								}
								try
								{
									delete [] g_phelios->m_data.m_ppConnObj;
								}	catch(...) {}
							}
							ppObj[o] = pscno;
							g_phelios->m_data.m_ppConnObj = ppObj;
							g_phelios->m_data.m_nNumConnectionObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szHost.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, "%s", szHost);

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}
//							ppObj[o]->m_usType = not used;
							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|HELIOS_FLAG_FOUND;
							else ppObj[o]->m_ulFlags |= HELIOS_FLAG_FOUND;
								
							ppObj[o]->m_usPort = usPort;

							if((ppObj[o]->m_ulFlags)&HELIOS_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s%s%s...", 
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName,
									ppObj[o]->m_pszDesc?" on ":"",
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszServerName:""
									); 
								
								g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_init", errorstring);//   Sleep(100); //(Dispatch message)

									if(ppObj[o]->m_pdb)
									{
//								g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_init", "2");   Sleep(100); //(Dispatch message)
										try{	delete  ppObj[o]->m_pdb; } catch(...){}
//								g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_init", "3");   Sleep(100); //(Dispatch message)
										ppObj[o]->m_pdb = NULL;
									}
//								g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_init", "4");   Sleep(100); //(Dispatch message)
									ppObj[o]->m_pdb = new CDBUtil;
									if(ppObj[o]->m_pdb)
									{

//								g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_init", "5");   Sleep(100); //(Dispatch message)
										ppObj[o]->m_pdbConn 
											= ppObj[o]->m_pdb->CreateNewConnection(g_phelios->m_settings.m_pszDSN, g_phelios->m_settings.m_pszUser, g_phelios->m_settings.m_pszPW);
//								g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_init", "6");  Sleep(100); //(Dispatch message)
	if(ppObj[o]->m_pdbConn)
	{
		if(ppObj[o]->m_pdb->ConnectDatabase(ppObj[o]->m_pdbConn, errorstring)<DB_SUCCESS)
		{
			g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_debug", "Failed to connect to database: %s", errorstring); //  Sleep(100); //(Dispatch message)
		}
		else
		{
			g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_debug", "Connected to database for %s%s%s",
												ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName,
												ppObj[o]->m_pszDesc?" on ":"",
												ppObj[o]->m_pszDesc?ppObj[o]->m_pszServerName:""
				); //  Sleep(100); //(Dispatch message)
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create secondary connection for %s:%s:%s", g_phelios->m_settings.m_pszDSN, g_phelios->m_settings.m_pszUser, g_phelios->m_settings.m_pszPW); 
		g_phelios->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Helios:database2_init", errorstring);  //(Dispatch message)
		//**MSG
	}
//								g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_init", "7");   Sleep(100); //(Dispatch message)

									}
									CRITICAL_SECTION* pMsgCrit = new CRITICAL_SECTION;
									if(pMsgCrit)
									{
//								g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_init", "8");  Sleep(100); //(Dispatch message)
										InitializeCriticalSection(pMsgCrit);
									}

// with SQL
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONN)
{
	g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_debug", "Entering ConnectServer on init");  Sleep(100); //(Dispatch message)
}
								Sleep(250); // quarter second delay just to stagger conn threads.

								CCAConn* pConn = g_omni.ConnectServer(
									ppObj[o]->m_pszServerName, 
									ppObj[o]->m_usPort, 
									ppObj[o]->m_pszDesc, 
									g_phelios->m_settings.m_pszOmnibusDataFields, 
//										g_phelios->m_data.m_pdb, 
//										g_phelios->m_data.m_pdbConn, 
									ppObj[o]->m_pdb, 
									ppObj[o]->m_pdbConn, 
									g_phelios->m_settings.m_pszLiveEvents,
									(g_phelios->m_settings.m_bListStatusInExchange?g_phelios->m_settings.m_pszExchange:NULL),
									(g_phelios->m_settings.m_bStatusInTables?g_phelios->m_settings.m_pszConnections:NULL),
									(g_phelios->m_settings.m_bStatusInTables?g_phelios->m_settings.m_pszChannels:NULL),
									pMsgCrit, // going to do this so that each connection has its own critsec.
//									&(g_phelios->m_data.m_critSQL),
									(void*)SendMessageToHelios,
									&(g_phelios->m_critSendMsg)
									);
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONN)
{
	g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_debug", "Left ConnectServer on init with %d", pConn);  Sleep(100); //(Dispatch message)

}
/*
// without SQL
								CCAConn* pConn = g_omni.ConnectServer(
									ppObj[o]->m_pszServerName, 
									ppObj[o]->m_usPort, 
									ppObj[o]->m_pszDesc, 
									g_phelios->m_settings.m_pszOmnibusDataFields, 
									NULL, 
									NULL, 
									NULL,
									NULL,
									NULL
									);
*/
								if(pConn)
								{
//									ppObj[o]->m_pDlg->OnButtonConnect();
								//**** if connect set following status
									ppObj[o]->m_ulStatus = HELIOS_STATUS_CONN; 

									if(ppObj[o]->m_ulFlags&HELIOS_FLAG_PAL)	pConn->SetFrameBasis(OMNI_PAL);
									else if(ppObj[o]->m_ulFlags&HELIOS_FLAG_NTSCDF)	pConn->SetFrameBasis(OMNI_NTSCNDF);
									else pConn->SetFrameBasis(OMNI_NTSC);

if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONN)
{
	g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_debug", "Setting comm [%s] and debug [%s]", 
		g_phelios->m_settings.m_pszCommLog?g_phelios->m_settings.m_pszCommLog:"NULL",
		g_phelios->m_settings.m_pszDebugLog?g_phelios->m_settings.m_pszDebugLog:"NULL"); // Sleep(100); //(Dispatch message)
}

//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "qo1");   Sleep(200); //(Dispatch message)
									if((g_phelios->m_settings.m_pszDebugLog)&&(strlen(g_phelios->m_settings.m_pszDebugLog)>0))
										pConn->SetDebugName(g_phelios->m_settings.m_pszDebugLog);
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "qo2");   Sleep(200); //(Dispatch message)
									if((g_phelios->m_settings.m_pszCommLog)&&(strlen(g_phelios->m_settings.m_pszCommLog)>0))
										pConn->SetCommLogName(g_phelios->m_settings.m_pszCommLog);
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "qo3");   Sleep(200); //(Dispatch message)
									pConn->m_bWriteXMLtoFile = g_phelios->m_settings.m_bWriteXML;
									pConn->m_ulCheckInterval = g_phelios->m_settings.m_ulCheckInterval;
									pConn->m_ulExpiryPeriod = g_phelios->m_settings.m_ulExpiryPeriod;
									pConn->m_ulConnTimeout = g_phelios->m_settings.m_ulConnTimeout;
									pConn->m_nMessageEventCountLimit = g_phelios->m_settings.m_nMessageEventCountLimit;

									pConn->m_nLookaheadTimeLimitSec = g_phelios->m_settings.m_nLookaheadTimeLimitSec;  // Seconds from "now" to not process adds and updates. discard the rest.  Deletions are not filtered.
									pConn->m_nLookbehindTimeLimitSec = g_phelios->m_settings.m_nLookbehindTimeLimitSec;  // Seconds before "now" to not process adds and updates. discard the rest.  Deletions are not filtered.

									pConn->m_nAsyncDatabaseLookaheadSec = g_phelios->m_settings.m_nAsyncDatabaseLookaheadSec;  // immediately commit to the DB any events that have an on air time of "now" plus this number of seconds, or prior. otherwise commit events as they come into the window.
									pConn->m_nAsyncDatabaseCheckIntervalMS = g_phelios->m_settings.m_nAsyncDatabaseCheckIntervalMS;  // only check for uncommitted events every so often.
									pConn->m_nMinimumListUpdateIntervalMS = g_phelios->m_settings.m_nMinimumListUpdateIntervalMS;  // prevent database updates on list status faster than this interval
									pConn->m_nMinimumServerUpdateIntervalMS = g_phelios->m_settings.m_nMinimumServerUpdateIntervalMS;  // prevent database updates on server status faster than this interval
									pConn->m_bPreventServerTimeFromEvents = g_phelios->m_settings.m_bPreventServerTimeFromEvents;			// ignore the current time reference within events.


									pConn->m_ulFlags |= ((g_phelios->m_settings.m_bUseITXInterface)?OMNI_ITX:0) ;
									pConn->m_ulFlags |= ((g_phelios->m_settings.m_bUseSystemClock)?OMNI_SYSTIME:0) ;
									pConn->m_ulFlags |= ((g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_COMMLOG)?OMNI_DEBUG:0) ;
									pConn->m_ulFlags |= ((g_phelios->m_settings.m_bMultiThreadChannelProcessing)?OMNI_MULTITHREAD:0) ;
									pConn->m_ulFlags |= ((g_phelios->m_settings.m_bClearEventsConnect)?OMNI_CLEARONCONNECT:0) ;
									pConn->m_ulFlags |= ((g_phelios->m_settings.m_bCompareEventsConnect)?OMNI_COMPAREONCONNECT:0) ;
									pConn->m_ulFlags |= ((g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_EVENT_PROCESS)?OMNI_DEBUG_PROCESS:0) ;
								}
								else
								{
if(g_phelios->m_settings.m_ulDebug&HELIOS_DEBUG_CONN)
{
g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:conn_debug", "pConn was NULL"); // Sleep(100); //(Dispatch message)
}
								}
								ppObj[o]->m_pCAConn = pConn;
								
							}
						}
						else
						{
							try
							{
								delete pscno;
							}	catch(...) {}
						}
					}
				}

//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "w");   Sleep(200); //(Dispatch message)

				nIndex++;
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "w1");   Sleep(200); //(Dispatch message)
				prs->MoveNext();
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "w2");   Sleep(200); //(Dispatch message)

				if((!prs->IsEOF())&&(g_phelios->m_settings.m_ulInterConnectionIntervalMS>0))
				{
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "w3");   Sleep(200); //(Dispatch message)
	//				_ftime( &g_phelios->m_data.m_timebTick );

					_timeb timeNext;
					_timeb timeTick;
					_ftime(&timeTick);


//					timeNext.time = g_phelios->m_data.m_timebTick.time + g_phelios->m_settings.m_ulInterConnectionIntervalMS/1000;
	//				timeNext.millitm = g_phelios->m_data.m_timebTick.millitm + ((unsigned short)g_phelios->m_settings.m_ulInterConnectionIntervalMS)%1000;
					timeNext.time = timeTick.time + g_phelios->m_settings.m_ulInterConnectionIntervalMS/1000;
					timeNext.millitm = timeTick.millitm + ((unsigned short)g_phelios->m_settings.m_ulInterConnectionIntervalMS)%1000;

					while(timeNext.millitm>999)
					{
						timeNext.time++;
						timeNext.millitm-=1000;
					}
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "w4");   Sleep(200); //(Dispatch message)

					while((g_phelios)&&(!g_bKillStatus)&&(g_bThreadStarted) )
					{
						_ftime( &timeTick );

						if(
								(timeTick.time>timeNext.time)
							||((timeTick.time==timeNext.time)&&(timeTick.millitm>timeNext.millitm))
							) break;
					}

//								Sleep	(g_phelios->m_settings.m_ulInterConnectionIntervalMS);
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "wx");   Sleep(200); //(Dispatch message)
					
				}
			}

			nReturn = nIndex;
			prs->Close();
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "ws1");   Sleep(200); //(Dispatch message)

			try
			{
				delete prs;
			}	catch(...) {}
//									g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios", "ws2");   Sleep(200); //(Dispatch message)

// 			LeaveCriticalSection(&m_critSQL);

//			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<g_phelios->m_data.m_nNumConnectionObjects)
			{
				if((g_phelios->m_data.m_ppConnObj)&&(g_phelios->m_data.m_ppConnObj[nIndex]))
				{
					if((g_phelios->m_data.m_ppConnObj[nIndex]->m_ulFlags)&HELIOS_FLAG_FOUND)
					{
						(g_phelios->m_data.m_ppConnObj[nIndex]->m_ulFlags) &= ~HELIOS_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(g_phelios->m_data.m_ppConnObj[nIndex])
						{
							if((g_phelios->m_data.m_ppConnObj[nIndex]->m_ulFlags)&HELIOS_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s%s%s...", 
									g_phelios->m_data.m_ppConnObj[nIndex]->m_pszDesc?g_phelios->m_data.m_ppConnObj[nIndex]->m_pszDesc:g_phelios->m_data.m_ppConnObj[nIndex]->m_pszServerName,
									g_phelios->m_data.m_ppConnObj[nIndex]->m_pszDesc?" on ":"",
									g_phelios->m_data.m_ppConnObj[nIndex]->m_pszDesc?g_phelios->m_data.m_ppConnObj[nIndex]->m_pszServerName:""
									);  
							}

//							g_phelios->m_data.m_ppConnObj[nIndex]->m_bKillConnThread = true;
							// this has to wait because we are deleting.
//							while((g_phelios->m_data.m_ppConnObj[nIndex])&&(g_phelios->m_data.m_ppConnObj[nIndex]->m_bConnThreadStarted)) Sleep(100);

							if((g_phelios->m_data.m_ppConnObj[nIndex]->m_ulFlags)&HELIOS_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "%s is disconnected.", 
									g_phelios->m_data.m_ppConnObj[nIndex]->m_pszDesc?g_phelios->m_data.m_ppConnObj[nIndex]->m_pszDesc:g_phelios->m_data.m_ppConnObj[nIndex]->m_pszServerName);  
								g_phelios->m_msgr.DM(MSG_ICONINFO, NULL, "Helios:destination_remove", errorstring);  // Sleep(100);  //(Dispatch message)

								//g_adc.DisconnectServer(g_phelios->m_data.m_ppConnObj[nIndex]->m_pszServerName);
								//**** should check return value....

//								g_phelios->m_data.m_ppConnObj[nIndex]->m_ulStatus = HELIOS_STATUS_NOTCON;
							}

							// disconnect SQL

							if(g_phelios->m_data.m_ppConnObj[nIndex]->m_pdb)
							{
								if(g_phelios->m_data.m_ppConnObj[nIndex]->m_pdbConn)
								{
									g_phelios->m_data.m_ppConnObj[nIndex]->m_pdb->RemoveConnection(g_phelios->m_data.m_ppConnObj[nIndex]->m_pdbConn);
								}
								try{ delete g_phelios->m_data.m_ppConnObj[nIndex]->m_pdb; } catch(...){}
								g_phelios->m_data.m_ppConnObj[nIndex]->m_pdb = NULL;
							}
							try
							{
								delete g_phelios->m_data.m_ppConnObj[nIndex];
							}	catch(...) {}

							if(g_phelios->m_data.m_ppConnObj[nIndex]->m_pCAConn)
							{
								if(g_phelios->m_data.m_ppConnObj[nIndex]->m_pCAConn->m_pcritSQL)
								{
									try{ delete g_phelios->m_data.m_ppConnObj[nIndex]->m_pCAConn->m_pcritSQL; } catch(...){}
								}
								g_phelios->m_data.m_ppConnObj[nIndex]->m_pCAConn->m_pcritSQL =  NULL;
							}

							g_phelios->m_data.m_ppConnObj[nIndex] = NULL;
							g_phelios->m_data.m_nNumConnectionObjects--;

							int nTemp=nIndex;
							while(nTemp<g_phelios->m_data.m_nNumConnectionObjects)
							{
								g_phelios->m_data.m_ppConnObj[nTemp]=g_phelios->m_data.m_ppConnObj[nTemp+1];
								nTemp++;
							}
							g_phelios->m_data.m_ppConnObj[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "leaving GetConnections with %d", nIndex);  Sleep(250); //(Dispatch message)
			return nReturn;
		}
//	else g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "GetConnections was NULL");  Sleep(250); //(Dispatch message)
//		LeaveCriticalSection(&m_critSQL);

	}
//			g_phelios->m_msgr.DM(MSG_ICONHAND, NULL, "Helios:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return HELIOS_ERROR;
}

