// ArchivistData.h: interface for the CArchivistData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARCHIVISTDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
#define AFX_ARCHIVISTDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <winsock2.h>
#include <sys/timeb.h>
#include "../../Common/TXT/BufferUtil.h" 
#include "../../Common/LAN/NetUtil.h" 
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"
#include "../../Common/KEY/LicenseKey.h"
#include "../../Common/FILE/DirUtil.h"


// these are data reference objects, pulled from the database.
// actual connection, list, and event objects are in the CADC object
/*

class CArchivistConnectionObject  
{
public:
	CArchivistConnectionObject();
	virtual ~CArchivistConnectionObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	char* m_pszServerName;
	char* m_pszClientName;
	char* m_pszDesc;

//	CAConnection* m_pAPIConn;  // pointer to the associated API connection.

	// control
	bool m_bKillConnThread;
	bool m_bConnThreadStarted;
};


class CArchivistChannelObject  
{
public:
	CArchivistChannelObject();
	virtual ~CArchivistChannelObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	int m_nChannelID;  // the unique Channel ID within Archivist setup. (assigned externally)
	int m_nHarrisListID;  // the 1-based List # on the associated ADC-100 server

	char* m_pszServerName;
	char* m_pszDesc;

//	CAConnection* m_pAPIConn;  // pointer to the associated API connection.

// control
	bool* m_pbKillConnThread;
	bool m_bKillChannelThread;
	bool m_bChannelThreadStarted;
};

*/

class CArchivistFileTypeObject  
{
public:
	CArchivistFileTypeObject();
	virtual ~CArchivistFileTypeObject();

	char* m_pszExt;
	char* m_pszDesc;
	int m_nID;
	unsigned long m_ulFlags;   // various flags
};


class CArchivistData  
{
public:
	CArchivistData();
	virtual ~CArchivistData();

	// util object
	CBufferUtil m_bu;

  // hostname.  have to get it, store it here
	char*	m_pszHost;	// the name of the host
	char*	m_pszCompleteHost;	// the complete name of the host

	char*	m_pszCortexHost;	// the name of the cortex host
	unsigned short m_usCortexCommandPort;
	unsigned short m_usCortexStatusPort;

	bool m_bNetworkMessagingInitialized;
	bool m_bCheckModsWarningSent;
	bool m_bCheckMsgsWarningSent;
//	bool m_bCheckAsRunWarningSent;
	_timeb m_timebAutoPurge; // the last time autopurge was run

	CArchivistFileTypeObject** m_ppFileTypes;
	int    m_nNumFileTypes;

/*
	CArchivistConnectionObject** m_ppConnObj;
	int m_nNumConnectionObjects;

	CArchivistChannelObject** m_ppChannelObj;
	int m_nNumChannelObjects;
*/
	_timeb m_timebTick; // the last time check inside the thread
	_timeb m_timebLastStatus; // the time of the last status given to cortex
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for cortex global status (each obj has their own as well)

	int m_nSettingsMod;
	int m_nMetadataMod;
	int m_nFileTypesMod;
//	int m_nChannelsMod;
//	int m_nConnectionsMod;
	int m_nLastSettingsMod;
	int m_nLastMetadataMod;
	int m_nLastFileTypesMod;
//	int m_nLastChannelsMod;
//	int m_nLastConnectionsMod;

	int m_nQueueMod;
	int m_nLastQueueMod;

	_timeb m_watchtime;


	ULARGE_INTEGER m_uliBytesAvail;
	ULARGE_INTEGER m_uliBytesTotal;
	ULARGE_INTEGER m_uliBytesFree;

	double m_dblAvail;
	double m_dblTotal;
	double m_dblFree;

	bool m_bDiskWarningSent;
	bool m_bDiskWarning;
	bool m_bDiskDeletionSent;
	bool m_bDiskDeletion;
	bool m_bWatchfolderInitialized;
	bool m_bWatchfolderWarningSent;

	bool m_bBackupDiskWarningSent;
	bool m_bBackupDiskWarning;
	bool m_bBackupDiskDeletionSent;
	bool m_bBackupDiskDeletion;

	bool m_bProcessSuspended;

	char* GetStatusText(unsigned long* pulStatus);  // allocates mem, must free after use
	int		SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError = false);
	int		GetHost();

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;
	CDBUtil* m_pdb2;
	CDBconn* m_pdb2Conn;
	CDBUtil* m_pdbBackup;
	CDBconn* m_pdbBackupConn;
	int CheckMessages(char* pszInfo=NULL);
//	int CheckAsRun(char* pszInfo=NULL);
	int CheckDatabaseMods(char* pszInfo=NULL);
	int CheckMetadataMods(char* pszInfo=NULL);
	int IncrementDatabaseMods(char* pszTableName, char* pszInfo=NULL);
//	int GetConnections(char* pszInfo=NULL);
//	int GetChannels(char* pszInfo=NULL);

	int PushBackupRecord(char* pszFilename, char* pszInfo=NULL);
	int ExecuteBackupQuery(char* pszQuery, char* pszInfo=NULL);

	int GetFileTypes(char* pszInfo=NULL);
	int GetFileTypeIndex(char* pszFileExt);

	int GetQueue(char* pszInfo=NULL);

	int InitPending();
	int SetPending(CDirEntry* pDirChange, bool bBackupOnly=false);
	int GetPending(CDirEntry** ppDirChange, int* pnRetries, bool* pbBackupOnly);
	int DelPending(int nID);
	int ModPending(int nID);

	CLicenseKey m_key;

	bool m_bQuietKill;

private:
	CRITICAL_SECTION m_critText;
	char* m_pszStatus;	// parseable string

//	unsigned long m_ulReqCount;
//	unsigned long m_ulSvcCount;
};

#endif // !defined(AFX_ARCHIVISTDATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
