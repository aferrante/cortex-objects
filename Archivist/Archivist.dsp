# Microsoft Developer Studio Project File - Name="Archivist" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Archivist - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Archivist.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Archivist.mak" CFG="Archivist - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Archivist - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Archivist - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Archivist - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Fr /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 C:\Source\Common\API\Harris\APILIB32.lib rpcrt4.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "Archivist - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 C:\Source\Common\API\Harris\APILIB32.lib rpcrt4.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Archivist - Win32 Release"
# Name "Archivist - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Archivist.cpp
# End Source File
# Begin Source File

SOURCE=.\Archivist.rc
# End Source File
# Begin Source File

SOURCE=.\ArchivistData.cpp

!IF  "$(CFG)" == "Archivist - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "Archivist - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ArchivistDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ArchivistHandler.cpp
# End Source File
# Begin Source File

SOURCE=.\ArchivistMain.cpp
# End Source File
# Begin Source File

SOURCE=.\ArchivistSettings.cpp

!IF  "$(CFG)" == "Archivist - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "Archivist - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Common\TXT\BufferUtil.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\..\Common\IMG\BMP\CBmpUtil_MFC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Cortex\3.0.4.2\CortexShared.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Common\MFC\ODBC\DBUtil.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Common\FILE\DirUtil.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Common\TXT\FileUtil.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\..\Common\KEY\LicenseKey.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Common\MFC\ListCtrlEx\ListCtrlEx.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Common\TXT\LogUtil.cpp

!IF  "$(CFG)" == "Archivist - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "Archivist - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Common\MSG\Messager.cpp

!IF  "$(CFG)" == "Archivist - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "Archivist - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Common\MSG\MessagingObject.cpp

!IF  "$(CFG)" == "Archivist - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "Archivist - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Common\LAN\NetUtil.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Common\SMTP\smtp.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Common\SMTP\SMTPUtil.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\Archivist.h
# End Source File
# Begin Source File

SOURCE=.\ArchivistData.h
# End Source File
# Begin Source File

SOURCE=.\ArchivistDefines.h
# End Source File
# Begin Source File

SOURCE=.\ArchivistDlg.h
# End Source File
# Begin Source File

SOURCE=.\ArchivistHandler.h
# End Source File
# Begin Source File

SOURCE=.\ArchivistMain.h
# End Source File
# Begin Source File

SOURCE=.\ArchivistSettings.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\TXT\BufferUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\IMG\BMP\CBmpUtil_MFC.h
# End Source File
# Begin Source File

SOURCE=..\..\Cortex\3.0.4.2\CortexShared.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\MFC\ODBC\DBUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\FILE\DirUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\TXT\FileUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\KEY\LicenseKey.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\MFC\ListCtrlEx\ListCtrlEx.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\TXT\LogUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\MSG\Messager.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\MSG\MessagingObject.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\MSG\msg.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\LAN\NetDefines.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\LAN\NetUtil.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\SMTP\smtp.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\SMTP\SMTPUtil.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\..\Common\API\Harris\SYSDEFS.H
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\Archivist.ico
# End Source File
# Begin Source File

SOURCE=.\res\Archivist.rc2
# End Source File
# Begin Source File

SOURCE=.\res\cursor1.cur
# End Source File
# Begin Source File

SOURCE=.\res\down.bmp
# End Source File
# Begin Source File

SOURCE=.\res\favicon.ico
# End Source File
# Begin Source File

SOURCE=.\res\hpbvdsw.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00002.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00003.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00004.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00005.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00006.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00007.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00008.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00009.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00010.ico
# End Source File
# Begin Source File

SOURCE=".\res\icon-cxb.ico"
# End Source File
# Begin Source File

SOURCE=".\res\icon-cxg.ico"
# End Source File
# Begin Source File

SOURCE=".\res\icon-cxr.ico"
# End Source File
# Begin Source File

SOURCE=".\res\icon-cxy.ico"
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon2.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon_com.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon_cxb.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon_cxg.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon_cxr.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon_cxy.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon_qua.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_cxr.ico
# End Source File
# Begin Source File

SOURCE=.\res\jewelIcon.ico
# End Source File
# Begin Source File

SOURCE=.\res\orad.ico
# End Source File
# Begin Source File

SOURCE=.\res\pauselist.bmp
# End Source File
# Begin Source File

SOURCE=.\res\playlist.bmp
# End Source File
# Begin Source File

SOURCE=.\res\settings.bmp
# End Source File
# Begin Source File

SOURCE=".\res\Status-Blu.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Status-Grn.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Status-Red.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Status-Yel.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\stdicons.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Titling_Branding_Banner_Harris.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Titling_Branding_Banner_VDS.bmp
# End Source File
# Begin Source File

SOURCE=.\res\up.bmp
# End Source File
# Begin Source File

SOURCE=.\res\vdi.bmp
# End Source File
# Begin Source File

SOURCE=.\res\vdi.ico
# End Source File
# Begin Source File

SOURCE=.\res\VDS_logo.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\todo.txt
# End Source File
# End Target
# End Project
