// ArchivistSettings.cpp: implementation of the CArchivistSettings.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "ArchivistDefines.h"
#include "ArchivistMain.h" 
#include "ArchivistSettings.h"
#include "../../Common/FILE/DirUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CDirUtil	g_dir;				// watch folder utilities.
extern CArchivistMain* g_parchivist;
extern CArchivistApp theApp;


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CArchivistSettings::CArchivistSettings()
{
	m_pdb = NULL;
	m_pdbConn = NULL;

	m_pszName = NULL;
	m_pszType = NULL;  // familiar name of the type.
	m_pszProject = NULL;  // familiar name of the project name.
	m_ulMainMode = ARCHIVIST_MODE_DEFAULT;

	// ports
	m_usCommandPort	= ARCHIVIST_PORT_CMD;
	m_usStatusPort	= ARCHIVIST_PORT_STATUS;

	m_nThreadDwellMS = 1000;

	m_nAutoPurgeMessageDays = 30; // default
//	m_nAutoPurgeAsRunDays = 30; // default
	m_nAutoPurgeInterval = 300;  // 5 minutes should be enough.

	// messaging for Archivist
	m_bUseLog = true;			// write a log file
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host
	m_bReportSuccessfulOperation = false;
	m_bLogTransfers=false;
	m_bUseDeletionSync=false; 
	m_bUseBackupDeletionSync=false; 
	m_pszFileSpec = NULL;
	m_pszMailSpec = NULL;
	m_pszProcessedFileSpec = NULL;
	m_pszProcessedMailSpec = NULL;
	m_bUseXMLClientLog = false;
	m_bMillisecondMessaging=true;			// use millisecond resolution for messages and asrun

	// DSN params
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;
	m_pszDatabase = NULL;

	m_pszBackupServer = NULL;
	m_pszBackupDatabase = NULL;
	m_pszBackupDSN = NULL;
	m_pszBackupUser = NULL;
	m_pszBackupPW = NULL;

	m_pszSettings = NULL;  // the Settings table name
	m_pszExchange = NULL;  // the Exchange table name
	m_pszMessages = NULL;  // the Messages table name
	m_pszMetadata = NULL;  // the Metadata table name
	m_pszFileTypes = NULL;  // the FileTypes table name
	m_pszQueue = NULL; // the Queue table name
	m_pszPending = NULL;		// the Pending events table name

	// file handling
	m_bFilterFileTypes = false;
	m_bDeleteSourceFileOnTransfer = false;
	m_bUseBackupDestination = false;
	m_bUseBackupDatabase = false;
	m_nWatchfolderInterval = 60000;
	m_nMaxFilesPerCycle = -1;

	m_dblDeletePercentageDiskFull = 98.0;
	m_dblWarnPercentageDiskFull = 80.0;

// other settings
	m_pszWatchFolderPath = NULL;  // the Watchfolder path
	m_pszDestinationFolderPath = NULL;  // the Destination path
	m_pszBackupDestinationFolderPath = NULL;  // the Backup Destination path
	m_pszArchiveDestinationFolderPath=NULL;
	m_pszBackupDestinationFolderLocalPath = NULL;

	m_ulModsIntervalMS = 3000;

	m_bDirectInstalled = false;
	m_bAutoDeleteOldest = true;
	m_bOverwriteFiles = true;
	m_nPendingRetries = 100;

	m_pszLicense=NULL;  // the License Key
	m_pszIconPath=NULL;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

	m_bDebugLists= false;  // if true, dumps the temp and events databases
	m_nDebugListTopCount=-1;  //if positive, just prints out the top n recored in the events tables.
	m_bDebugSQL=false;  // if true, prints out the SQL calls
	m_bDebugInsertSQL=false;  // if true, prints out the event insert SQL calls
	m_ulDebug = 0;
	m_pszDebugOrder = NULL;


}

CArchivistSettings::~CArchivistSettings()
{
	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_pszType) free(m_pszType); // must use malloc to allocate
	if(m_pszProject) free(m_pszProject); // must use malloc to allocate
	if(m_pszDSN) free(m_pszDSN); // must use malloc to allocate
	if(m_pszUser) free(m_pszUser); // must use malloc to allocate
	if(m_pszPW) free(m_pszPW); // must use malloc to allocate
	if(m_pszDatabase) free(m_pszDatabase); // must use malloc to allocate

	if(m_pszBackupServer) free(m_pszBackupServer); // must use malloc to allocate
	if(m_pszBackupDatabase) free(m_pszBackupDatabase); // must use malloc to allocate
	if(m_pszBackupDSN) free(m_pszBackupDSN); // must use malloc to allocate
	if(m_pszBackupUser) free(m_pszBackupUser); // must use malloc to allocate
	if(m_pszBackupPW) free(m_pszBackupPW); // must use malloc to allocate
	if(m_pszSettings) free(m_pszSettings); // must use malloc to allocate
	if(m_pszExchange) free(m_pszExchange); // must use malloc to allocate
	if(m_pszMessages) free(m_pszMessages); // must use malloc to allocate
	if(m_pszMetadata) free(m_pszMetadata); // must use malloc to allocate
	if(m_pszFileTypes) free(m_pszFileTypes); // must use malloc to allocate
	if(m_pszQueue) free(m_pszQueue); // must use malloc to allocate
	if(m_pszPending) free(m_pszPending); // must use malloc to allocate


	if(m_pszWatchFolderPath) free(m_pszWatchFolderPath); // must use malloc to allocate
	if(m_pszDestinationFolderPath) free(m_pszDestinationFolderPath); // must use malloc to allocate
	if(m_pszBackupDestinationFolderPath) free(m_pszBackupDestinationFolderPath); // must use malloc to allocate
	if(m_pszArchiveDestinationFolderPath) free(m_pszArchiveDestinationFolderPath); // must use malloc to allocate
	if(m_pszBackupDestinationFolderLocalPath) free(m_pszBackupDestinationFolderLocalPath); // must use malloc to allocate

	if(m_pszLicense) free(m_pszLicense); // must use malloc to allocate
	if(m_pszIconPath) free(m_pszIconPath); // must use malloc to allocate	
	if(m_pszFileSpec) free(m_pszFileSpec); // must use malloc to allocate	
	if(m_pszMailSpec) free(m_pszMailSpec); // must use malloc to allocate	
	if(m_pszProcessedFileSpec) free(m_pszProcessedFileSpec); // must use malloc to allocate	
	if(m_pszProcessedMailSpec) free(m_pszProcessedMailSpec); // must use malloc to allocate	

	if(m_pszDebugOrder) free(m_pszDebugOrder); // must use malloc to allocate

}

int CArchivistSettings::Settings(bool bRead)
{
	char pszFilename[MAX_PATH];
	strcpy(pszFilename, "");
//		AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, ARCHIVIST_SETTINGS_FILE_DEFAULT);  // cortex settings file
	CFileUtil file;
	// get settings.
	file.GetSettings(pszFilename, false); 

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
		// load up the values on the settings object
			m_pszName = file.GetIniString("Main", "Name", "Archivist", m_pszName);
			m_pszType = file.GetIniString("Main", "Type", "Archivist", m_pszType);
			m_pszProject = file.GetIniString("Main", "Project", "VDS", m_pszProject);
			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);

			m_nThreadDwellMS = file.GetIniInt("Main", "ThreadDwellMS", 1000);

			m_bDebugLists = file.GetIniInt("Database", "DebugLists", 0)?true:false;  // if true, dumps the temp and events databases
			m_nDebugListTopCount = file.GetIniInt("Database", "DebugListTopCount", -1);  //if positive, just prints out the top n recored in the events tables.
			m_bDebugSQL = file.GetIniInt("Database", "DebugSQL", 0)?true:false;  // if true, prints out the SQL calls
			m_bDebugInsertSQL = file.GetIniInt("Database", "DebugInsertSQL", 0)?true:false;  // if true, prints out the event insert SQL calls
			m_ulDebug = file.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.
			m_pszDebugOrder = file.GetIniString("Database", "DebugOrder", "(case when parent_position < 0 then ((event_position)/1000.0) else (parent_calc_start + (event_position-parent_position)/1000.0) end)", m_pszDebugOrder); // the order by clause for debug

			m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", ARCHIVIST_PORT_CMD);
			m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", ARCHIVIST_PORT_STATUS);

			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);

			// recompile license key params
			if(g_parchivist->m_data.m_key.m_pszLicenseString) free(g_parchivist->m_data.m_key.m_pszLicenseString);
			g_parchivist->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(m_pszLicense)+1);
			if(g_parchivist->m_data.m_key.m_pszLicenseString)
			sprintf(g_parchivist->m_data.m_key.m_pszLicenseString, "%s", m_pszLicense);

			g_parchivist->m_data.m_key.InterpretKey();

			char errorstring[MAX_MESSAGE_LENGTH];
			if(g_parchivist->m_data.m_key.m_bValid)
			{
				unsigned long i=0;
				while(i<g_parchivist->m_data.m_key.m_ulNumParams)
				{
					if((g_parchivist->m_data.m_key.m_ppszParams)
						&&(g_parchivist->m_data.m_key.m_ppszValues)
						&&(g_parchivist->m_data.m_key.m_ppszParams[i])
						&&(g_parchivist->m_data.m_key.m_ppszValues[i]))
					{
						if(stricmp(g_parchivist->m_data.m_key.m_ppszParams[i], "max")==0)
						{
							//g_parchivist->m_data.m_nMaxLicensedChannels = atoi(g_parchivist->m_data.m_key.m_ppszValues[i]);
						}
					}
					i++;
				}
			
				if(
						(
							(!g_parchivist->m_data.m_key.m_bExpires)
						||((g_parchivist->m_data.m_key.m_bExpires)&&(!g_parchivist->m_data.m_key.m_bExpired))
						||((g_parchivist->m_data.m_key.m_bExpires)&&(g_parchivist->m_data.m_key.m_bExpireForgiveness)&&(g_parchivist->m_data.m_key.m_ulExpiryDate+g_parchivist->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!g_parchivist->m_data.m_key.m_bMachineSpecific)
						||((g_parchivist->m_data.m_key.m_bMachineSpecific)&&(g_parchivist->m_data.m_key.m_bValidMAC))
						)
					)
				{
					// this overrides stuff so let's not let it do that.

										// let's just not override this in a green condition.  
										// comment it out.
//					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//					g_parchivist->m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_OK);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
					g_parchivist->m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_ERROR);
				}
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				g_parchivist->m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_ERROR);
			}


			m_pszIconPath = file.GetIniString("FileServer", "IconPath", "C:\\Inetpub\\wwwroot\\Cortex\\archivist\\images\\", m_pszIconPath);

			m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
			m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;
			m_bUseXMLClientLog = file.GetIniInt("Messager", "UseXMLClientLog", 0)?true:false;
			m_bMillisecondMessaging = file.GetIniInt("Messager", "MillisecondMessaging", 1)?true:false;			// use millisecond resolution for messages and asrun

			m_nAutoPurgeMessageDays = file.GetIniInt("AutoPurge", "AutoPurgeMessageDays", 30);
	//		m_nAutoPurgeAsRunDays = file.GetIniInt("AutoPurge", "AutoPurgeAsRunDays", 30);
			m_nAutoPurgeInterval  = file.GetIniInt("AutoPurge", "AutoPurgeInterval", 300);  // 5 minutes should be enough.

			m_bUseDeletionSync = file.GetIniInt("FileHandling", "UseDeletionSync", 0)?true:false; //
			m_bUseBackupDeletionSync = file.GetIniInt("FileHandling", "UseBackupDeletionSync", 0)?true:false; //
			m_bDeleteSourceFileOnTransfer = file.GetIniInt("FileHandling", "DeleteSourceFileOnTransfer", 0)?true:false; //
			m_bUseBackupDestination = file.GetIniInt("FileHandling", "UseBackupDestination", 0)?true:false; // push to backup in addition to dest
			m_bUseBackupDatabase = file.GetIniInt("FileHandling", "UseBackupDatabase", 0)?true:false; // push to backup in addition to dest
			m_bFilterFileTypes = file.GetIniInt("FileHandling", "FilterFileTypes", 0)?true:false; // only transfer the ones that are in the list.
			m_nWatchfolderInterval = file.GetIniInt("FileHandling", "WatchfolderInterval", 60000); // check dir for changes every this many ms
			m_pszWatchFolderPath = file.GetIniString("FileHandling", "WatchFolderPath", "W:\\", m_pszWatchFolderPath);  
			m_pszDestinationFolderPath = file.GetIniString("FileHandling", "DestinationFolderPath", "D:\\Media\\", m_pszDestinationFolderPath);  
			m_pszBackupDestinationFolderPath = file.GetIniString("FileHandling", "BackupDestinationFolderPath", "Q:\\Media\\", m_pszBackupDestinationFolderPath);  
			m_pszBackupDestinationFolderLocalPath = file.GetIniString("FileHandling", "BackupDestinationFolderLocalPath", "D:\\Media\\", m_pszBackupDestinationFolderLocalPath);  
			m_pszArchiveDestinationFolderPath = file.GetIniString("FileHandling", "ArchiveDestinationFolderPath", "X:\\Media\\", m_pszArchiveDestinationFolderPath);  
			m_bOverwriteFiles = file.GetIniInt("FileHandling", "OverwriteFiles", 1)?true:false;
			m_bAutoDeleteOldest = file.GetIniInt("FileHandling", "AutoDeleteOldest", 1)?true:false;
			m_nPendingRetries = file.GetIniInt("FileHandling", "PendingRetries", 100);
			m_nMaxFilesPerCycle = file.GetIniInt("FileHandling", "MaxFilesPerCycle", -1);

			m_bDirectInstalled = file.GetIniInt("InstalledModules", "DirectInstalled", 0)?true:false;

			double dblValue;
			char* pszParams = file.GetIniString("FileHandling", "DeletePercentageDiskFull", "98.0"); 
			if(pszParams)
			{
				if(strlen(pszParams)>0)
				{
					dblValue = atof(pszParams);
					if((dblValue>0.1)&&(dblValue<99.9)) m_dblDeletePercentageDiskFull = dblValue;
				}
				free(pszParams); pszParams=NULL;
			}
			pszParams = file.GetIniString("FileHandling", "WarnPercentageDiskFull", "80.0"); 
			if(pszParams)
			{
				if(strlen(pszParams)>0)
				{
					dblValue = atof(pszParams);
					if((dblValue>0.1)&&(dblValue<99.9)) m_dblWarnPercentageDiskFull = dblValue;
				}
				free(pszParams); pszParams=NULL;
			}

			m_pszDSN = file.GetIniString("Database", "DSN", m_pszName?m_pszName:"Archivist", m_pszDSN);
			m_pszUser = file.GetIniString("Database", "DBUser", "sa", m_pszUser);
			m_pszPW = file.GetIniString("Database", "DBPassword", "", m_pszPW);
			m_pszDatabase = file.GetIniString("Database", "DBDefault", m_pszName?m_pszName:"Archivist", m_pszDatabase);

			m_pszBackupServer = file.GetIniString("Database", "BackupDBServer", "", m_pszBackupServer);
			m_pszBackupDatabase = file.GetIniString("Database", "BackupDBDefault", (m_pszName?m_pszName:"Archivist"), m_pszBackupDatabase);  // for pushing to backup db thru primary DSN conn
			m_pszBackupDSN = file.GetIniString("Database", "BackupDSN", "ArchivistBackup", m_pszBackupDSN);
			m_pszBackupUser = file.GetIniString("Database", "BackupDBUser", "sa", m_pszBackupUser);
			m_pszBackupPW = file.GetIniString("Database", "BackupDBPassword", "", m_pszBackupPW);
			m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings", m_pszSettings);  // the Settings table name
			m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange", m_pszExchange);  // the Exchange table name
			m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages", m_pszMessages);  // the Messages table name
			m_pszMetadata = file.GetIniString("Database", "MetadataTableName", "File_Metadata", m_pszMetadata);  // the Metadata table name
			m_pszFileTypes = file.GetIniString("Database", "FileTypesTableName", "FileTypes", m_pszFileTypes);  // the File Types table name
			m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue", m_pszQueue);  // the Queue table name
			m_pszPending = file.GetIniString("Database", "PendingTableName", "Pending", m_pszPending);  // the Pending table name

	//		m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels");  // the Channels table name
	//		m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Connections");  // the Connections table name
	//		m_pszLiveEvents = file.GetIniString("Database", "LiveEventsTableName", "Events");  // the LiveEvents table name

			m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 3000);  // in milliseconds

			m_pszFileSpec = file.GetIniString("Messager", "LogFileIni", "Logs\\Archivist|YD||1|", m_pszFileSpec);
				// SMTP address[: override port (optional)] | distribution lists | from email |  subject spec | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
			m_pszMailSpec = file.GetIniString("Messager", "SMTPIni", "mail.server.com|f3:recipient@address.com|\"someone\"<from@here.com>|%P %T from %h|%h|C:\\archivistsmtp.txt|1|1|0", m_pszMailSpec);
			if(m_pszFileSpec) 
			{
				if(m_pszProcessedFileSpec)
				{
					try{ free(m_pszProcessedFileSpec); } catch(...) {}
				}
				m_pszProcessedFileSpec = ProcessString(m_pszFileSpec, false);
			}
			
			if(m_pszMailSpec) 
			{
				if(m_pszProcessedMailSpec)
				{
					try{ free(m_pszProcessedMailSpec); } catch(...) {}
				}
				m_pszProcessedMailSpec = ProcessString(m_pszMailSpec, false);
			}

			if(pszParams) free(pszParams); pszParams=NULL;			
		}
		else //write
		{
			file.SetIniString("Main", "Name", m_pszName);
			file.SetIniString("Main", "Type", m_pszType);
			file.SetIniString("Main", "Project", m_pszProject);
			file.SetIniString("License", "Key", m_pszLicense);

			file.SetIniInt("Main", "ThreadDwellMS", m_nThreadDwellMS);

			file.SetIniInt("Database", "DebugLists", m_bDebugLists?1:0);  // if true, dumps the temp and events databases
			file.SetIniInt("Database", "DebugListTopCount", m_nDebugListTopCount);  //if positive, just prints out the top n recored in the events tables.
			file.SetIniInt("Database", "DebugSQL", m_bDebugSQL?1:0);  // if true, prints out the SQL calls
			file.SetIniInt("Database", "DebugInsertSQL", m_bDebugInsertSQL?1:0);  // if true, prints out the event insert SQL calls
			file.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.
			file.SetIniString("Database", "DebugOrder", m_pszDebugOrder); // the order by clause for debug

			file.SetIniInt("CommandServer", "ListenPort", m_usCommandPort);
			file.SetIniInt("StatusServer", "ListenPort", m_usStatusPort);

			file.SetIniString("FileServer", "IconPath", m_pszIconPath);

			file.SetIniInt("Messager", "UseEmail", m_bUseEmail?1:0);
			file.SetIniInt("Messager", "UseNet", m_bUseNetwork?1:0);
			file.SetIniInt("Messager", "UseLog", m_bUseLog?1:0);
			file.SetIniInt("Messager", "ReportSuccessfulOperation", m_bReportSuccessfulOperation?1:0);
			file.SetIniInt("Messager", "LogTransfers", m_bLogTransfers?1:0);
			file.SetIniInt("Messager", "UseXMLClientLog", m_bUseXMLClientLog?true:false);
			file.SetIniInt("Messager", "MillisecondMessaging", m_bMillisecondMessaging?true:false);		// use millisecond resolution for messages and asrun

			file.SetIniInt("FileHandling", "UseDeletionSync", m_bUseDeletionSync?1:0); //
			file.SetIniInt("FileHandling", "UseBackupDeletionSync", m_bUseBackupDeletionSync?1:0); //
			file.SetIniInt("FileHandling", "DeleteSourceFileOnTransfer", m_bDeleteSourceFileOnTransfer?1:0); 
			file.SetIniInt("FileHandling", "UseBackupDestination", m_bUseBackupDestination?1:0);
			file.SetIniInt("FileHandling", "UseBackupDatabase", m_bUseBackupDatabase?1:0);
			file.SetIniInt("FileHandling", "FilterFileTypes", m_bFilterFileTypes?1:0);
			file.SetIniInt("FileHandling", "WatchfolderInterval", m_nWatchfolderInterval); // check dir for changes every this many ms

			char pszValue[64];
			sprintf(pszValue, "%f", m_dblDeletePercentageDiskFull);
			file.SetIniString("FileHandling", "DeletePercentageDiskFull", pszValue); 
			sprintf(pszValue, "%f", m_dblWarnPercentageDiskFull);
			file.SetIniString("FileHandling", "WarnPercentageDiskFull", pszValue); 
			file.SetIniString("FileHandling", "WatchFolderPath", m_pszWatchFolderPath);  
			file.SetIniString("FileHandling", "DestinationFolderPath", m_pszDestinationFolderPath);  
			file.SetIniString("FileHandling", "BackupDestinationFolderPath", m_pszBackupDestinationFolderPath );  
			file.SetIniString("FileHandling", "BackupDestinationFolderLocalPath", m_pszBackupDestinationFolderLocalPath);  
			file.SetIniString("FileHandling", "ArchiveDestinationFolderPath", m_pszArchiveDestinationFolderPath);  
			file.SetIniInt("FileHandling", "OverwriteFiles", m_bOverwriteFiles?1:0);
			file.SetIniInt("FileHandling", "AutoDeleteOldest", m_bAutoDeleteOldest?1:0);
			file.SetIniInt("InstalledModules", "DirectInstalled", m_bDirectInstalled?1:0);
			file.SetIniInt("FileHandling", "PendingRetries", m_nPendingRetries);
			file.SetIniInt("FileHandling", "MaxFilesPerCycle", 	m_nMaxFilesPerCycle);



			file.SetIniString("Database", "DSN", m_pszDSN);
			file.SetIniString("Database", "DBUser", m_pszUser);
			file.SetIniString("Database", "DBPassword", m_pszPW);
			file.SetIniString("Database", "DBDefault", m_pszDatabase );

			file.SetIniString("Database", "BackupDBServer", m_pszBackupServer);
			file.SetIniString("Database", "BackupDBDefault", m_pszBackupDatabase ); // for pushing to backup db thru primary DSN conn

			file.SetIniString("Database", "BackupDSN", m_pszBackupDSN);
			file.SetIniString("Database", "BackupDBUser", m_pszBackupUser);
			file.SetIniString("Database", "BackupDBPassword", m_pszBackupPW);
			file.SetIniString("Database", "SettingsTableName", m_pszSettings);  // the Settings table name
			file.SetIniString("Database", "ExchangeTableName", m_pszExchange);  // the Exchange table name
			file.SetIniString("Database", "MessagesTableName", m_pszMessages);  // the Messages table name
			file.SetIniString("Database", "MetadataTableName", m_pszMetadata);  // the Messages table name
			file.SetIniString("Database", "FileTypesTableName", m_pszFileTypes);  // the File Types table name
			file.SetIniString("Database", "QueueTableName", m_pszQueue);  // the Queue table name
			file.SetIniString("Database", "PendingTableName", m_pszPending);  // the Pending table name

			file.SetIniInt("Database", "ModificationCheckInterval", m_ulModsIntervalMS);  // in milliseconds

			file.SetIniInt("AutoPurge", "AutoPurgeMessageDays", m_nAutoPurgeMessageDays);
//			file.SetIniInt("AutoPurge", "AutoPurgeAsRunDays", m_nAutoPurgeAsRunDays);
			file.SetIniInt("AutoPurge", "AutoPurgeInterval", m_nAutoPurgeInterval);  // 5 minutes should be enough.

			file.SetIniString("Messager", "LogFileIni", m_pszFileSpec);
			file.SetIniString("Messager", "SMTPIni", m_pszMailSpec);

			file.SetSettings(pszFilename, false);  // have to have correct filename

		}
		return ARCHIVIST_SUCCESS;
	}
	return ARCHIVIST_ERROR;
}

int CArchivistSettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	if((m_pszSettings)&&(strlen(m_pszSettings)>0)&&(m_pdbConn)&&(m_pdb)&&(m_pdbConn->m_bConnected)&&(Settings(true)==ARCHIVIST_SUCCESS))
	{

/*
		// get settings.
		char pszFilename[MAX_PATH];
		char errorstring[MAX_MESSAGE_LENGTH];


		strcpy(pszFilename, ARCHIVIST_SETTINGS_FILE_DEFAULT);  // archivist settings file

		CFileUtil file;
		file.GetSettings(pszFilename, false); 
		char* pszParams = NULL;
	// load up the values on the settings object
		if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
		{
			g_parchivist->m_settings.m_pszName = file.GetIniString("Main", "Name", "Archivist");
			g_parchivist->m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", ARCHIVIST_PORT_CMD);
			g_parchivist->m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", ARCHIVIST_PORT_STATUS);

			g_parchivist->m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");

			// compile license key params
			if(g_parchivist->m_data.m_key.m_pszLicenseString) free(g_parchivist->m_data.m_key.m_pszLicenseString);
			g_parchivist->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(g_parchivist->m_settings.m_pszLicense)+1);
			if(g_parchivist->m_data.m_key.m_pszLicenseString)
			sprintf(g_parchivist->m_data.m_key.m_pszLicenseString, "%s", g_parchivist->m_settings.m_pszLicense);

			g_parchivist->m_data.m_key.InterpretKey();

			if(g_parchivist->m_data.m_key.m_bValid)
			{
				unsigned long i=0;
				while(i<g_parchivist->m_data.m_key.m_ulNumParams)
				{
					if((g_parchivist->m_data.m_key.m_ppszParams)
						&&(g_parchivist->m_data.m_key.m_ppszValues)
						&&(g_parchivist->m_data.m_key.m_ppszParams[i])
						&&(g_parchivist->m_data.m_key.m_ppszValues[i]))
					{
						if(stricmp(g_parchivist->m_data.m_key.m_ppszParams[i], "max")==0)
						{
	//						g_parchivist->m_data.m_nMaxLicensedDevices = atoi(g_parchivist->m_data.m_key.m_ppszValues[i]);
						}
					}
					i++;
				}

				if(
						(
							(!g_parchivist->m_data.m_key.m_bExpires)
						||((g_parchivist->m_data.m_key.m_bExpires)&&(!g_parchivist->m_data.m_key.m_bExpired))
						||((g_parchivist->m_data.m_key.m_bExpires)&&(g_parchivist->m_data.m_key.m_bExpireForgiveness)&&(g_parchivist->m_data.m_key.m_ulExpiryDate+g_parchivist->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!g_parchivist->m_data.m_key.m_bMachineSpecific)
						||((g_parchivist->m_data.m_key.m_bMachineSpecific)&&(g_parchivist->m_data.m_key.m_bValidMAC))
						)
					)
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
					g_parchivist->m_data.m_ulFlags &= ~ARCHIVIST_ICON_MASK;
					g_parchivist->m_data.m_ulFlags |= ARCHIVIST_STATUS_OK;
					g_parchivist->m_data.SetStatusText(errorstring, g_parchivist->m_data.m_ulFlags);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
					g_parchivist->m_data.m_ulFlags &= ~ARCHIVIST_ICON_MASK;
					g_parchivist->m_data.m_ulFlags |= ARCHIVIST_STATUS_ERROR;
					g_parchivist->m_data.SetStatusText(errorstring, g_parchivist->m_data.m_ulFlags);
				}
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				g_parchivist->m_data.m_ulFlags &= ~ARCHIVIST_ICON_MASK;
				g_parchivist->m_data.m_ulFlags |= ARCHIVIST_STATUS_ERROR;
				g_parchivist->m_data.SetStatusText(errorstring, g_parchivist->m_data.m_ulFlags);
			}

			g_parchivist->m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");


			g_parchivist->m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			g_parchivist->m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			g_parchivist->m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			g_parchivist->m_settings.m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
			g_parchivist->m_settings.m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;

			g_parchivist->m_settings.m_bUseDeletionSync = file.GetIniInt("FileHandling", "UseDeletionSync", 0)?true:false; //
			g_parchivist->m_settings.m_bUseBackupDeletionSync = file.GetIniInt("FileHandling", "UseBackupDeletionSync", 0)?true:false; //
			g_parchivist->m_settings.m_bDeleteSourceFileOnTransfer = file.GetIniInt("FileHandling", "DeleteSourceFileOnTransfer", 0)?true:false; //
			g_parchivist->m_settings.m_bUseBackupDestination = file.GetIniInt("FileHandling", "UseBackupDestination", 0)?true:false; // push to backup in addition to dest
			g_parchivist->m_settings.m_bUseBackupDatabase = file.GetIniInt("FileHandling", "UseBackupDatabase", 0)?true:false; // push to backup in addition to dest
			g_parchivist->m_settings.m_bFilterFileTypes = file.GetIniInt("FileHandling", "FilterFileTypes", 0)?true:false; // only transfer the ones that are in the list.
			g_parchivist->m_settings.m_nWatchfolderInterval = file.GetIniInt("FileHandling", "WatchfolderInterval", 60000); // check dir for changes every this many ms
			g_parchivist->m_settings.m_pszWatchFolderPath = file.GetIniString("FileHandling", "WatchFolderPath", "W:\\");  
			g_parchivist->m_settings.m_pszDestinationFolderPath = file.GetIniString("FileHandling", "DestinationFolderPath", "D:\\Media\\");  
			g_parchivist->m_settings.m_pszBackupDestinationFolderPath = file.GetIniString("FileHandling", "BackupDestinationFolderPath", "Q:\\Media\\");  
			g_parchivist->m_settings.m_pszBackupDestinationFolderLocalPath = file.GetIniString("FileHandling", "BackupDestinationFolderLocalPath", "D:\\Media\\");  
			g_parchivist->m_settings.m_pszArchiveDestinationFolderPath = file.GetIniString("FileHandling", "ArchiveDestinationFolderPath", "X:\\Media\\");  
			g_parchivist->m_settings.m_bOverwriteFiles = file.GetIniInt("FileHandling", "OverwriteFiles", 1)?true:false;
			g_parchivist->m_settings.m_bAutoDeleteOldest = file.GetIniInt("FileHandling", "AutoDeleteOldest", 1)?true:false;
			g_parchivist->m_settings.m_nPendingRetries = file.GetIniInt("FileHandling", "PendingRetries", 100);


			g_parchivist->m_settings.m_bDirectInstalled = file.GetIniInt("InstalledModules", "DirectInstalled", 0)?true:false;

			double dblValue;
			pszParams = file.GetIniString("FileHandling", "DeletePercentageDiskFull", "98.0"); 
			if(pszParams)
			{
				if(strlen(pszParams)>0)
				{
					dblValue = atof(pszParams);
					if((dblValue>0.1)&&(dblValue<99.9)) g_parchivist->m_settings.m_dblDeletePercentageDiskFull = dblValue;
				}
				free(pszParams); pszParams=NULL;
			}
			pszParams = file.GetIniString("FileHandling", "WarnPercentageDiskFull", "80.0"); 
			if(pszParams)
			{
				if(strlen(pszParams)>0)
				{
					dblValue = atof(pszParams);
					if((dblValue>0.1)&&(dblValue<99.9)) g_parchivist->m_settings.m_dblWarnPercentageDiskFull = dblValue;
				}
				free(pszParams); pszParams=NULL;
			}

			g_parchivist->m_settings.m_pszDSN = file.GetIniString("Database", "DSN", g_parchivist->m_settings.m_pszName?g_parchivist->m_settings.m_pszName:"Archivist");
			g_parchivist->m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
			g_parchivist->m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
			g_parchivist->m_settings.m_pszDatabase = file.GetIniString("Database", "DBDefault", g_parchivist->m_settings.m_pszName?g_parchivist->m_settings.m_pszName:"Archivist");

			g_parchivist->m_settings.m_pszBackupServer = file.GetIniString("Database", "BackupDBServer", "");
			g_parchivist->m_settings.m_pszBackupDatabase = file.GetIniString("Database", "BackupDBDefault", g_parchivist->m_settings.m_pszName?g_parchivist->m_settings.m_pszName:"Archivist");  // for pushing to backup db thru primary DSN conn
			g_parchivist->m_settings.m_pszBackupDSN = file.GetIniString("Database", "BackupDSN", "ArchivistBackup");
			g_parchivist->m_settings.m_pszBackupUser = file.GetIniString("Database", "BackupDBUser", "sa");
			g_parchivist->m_settings.m_pszBackupPW = file.GetIniString("Database", "BackupDBPassword", "");
			g_parchivist->m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
			g_parchivist->m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
			g_parchivist->m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name
			g_parchivist->m_settings.m_pszMetadata = file.GetIniString("Database", "MetadataTableName", "File_Metadata");  // the Metadata table name
			g_parchivist->m_settings.m_pszFileTypes = file.GetIniString("Database", "FileTypesTableName", "FileTypes");  // the File Types table name
			g_parchivist->m_settings.m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue");  // the Queue table name
			g_parchivist->m_settings.m_pszPending = file.GetIniString("Database", "PendingTableName", "Pending");  // the Pending table name

	//		g_parchivist->m_settings.m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels");  // the Channels table name
	//		g_parchivist->m_settings.m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Connections");  // the Connections table name
	//		g_parchivist->m_settings.m_pszLiveEvents = file.GetIniString("Database", "LiveEventsTableName", "Events");  // the LiveEvents table name

			g_parchivist->m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 3000);  // in milliseconds

			if(pszParams) free(pszParams); pszParams=NULL;

		}
*/
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = ARCHIVIST_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCategory="";
				CString szParameter="";
				CString szValue="";
				CString szTemp="";
				int min, max;
				bool bmin = false, bmax = false;
				try
				{
					prs->GetFieldValue("category", szCategory);  //HARDCODE
					prs->GetFieldValue("parameter", szParameter);  //HARDCODE
					prs->GetFieldValue("value", szValue);  //HARDCODE
					prs->GetFieldValue("min_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						min = atoi(szTemp);
						bmin = true;
					}
					prs->GetFieldValue("max_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						max = atoi(szTemp);
						bmax = true;
					}
				}
				catch( ... )
				{
				}

				int nLength = szValue.GetLength();
				if(szCategory.CompareNoCase("Main")==0)
				{
					if(szParameter.CompareNoCase("Name")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszName) free(m_pszName);
								m_pszName = pch;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("License")==0)
				{
					if(szParameter.CompareNoCase("Key")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLicense) free(m_pszLicense);
								m_pszLicense = pch;

								// recompile license key params
								if(g_parchivist->m_data.m_key.m_pszLicenseString) free(g_parchivist->m_data.m_key.m_pszLicenseString);
								g_parchivist->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
								if(g_parchivist->m_data.m_key.m_pszLicenseString)
								sprintf(g_parchivist->m_data.m_key.m_pszLicenseString, "%s", szValue);

								g_parchivist->m_data.m_key.InterpretKey();

								char errorstring[MAX_MESSAGE_LENGTH];
								if(g_parchivist->m_data.m_key.m_bValid)
								{
									unsigned long i=0;
									while(i<g_parchivist->m_data.m_key.m_ulNumParams)
									{
										if((g_parchivist->m_data.m_key.m_ppszParams)
											&&(g_parchivist->m_data.m_key.m_ppszValues)
											&&(g_parchivist->m_data.m_key.m_ppszParams[i])
											&&(g_parchivist->m_data.m_key.m_ppszValues[i]))
										{
											if(stricmp(g_parchivist->m_data.m_key.m_ppszParams[i], "max")==0)
											{
//												g_parchivist->m_data.m_nMaxLicensedDevices = atoi(g_parchivist->m_data.m_key.m_ppszValues[i]);
											}
										}
										i++;
									}
								
									if(
											(
												(!g_parchivist->m_data.m_key.m_bExpires)
											||((g_parchivist->m_data.m_key.m_bExpires)&&(!g_parchivist->m_data.m_key.m_bExpired))
											||((g_parchivist->m_data.m_key.m_bExpires)&&(g_parchivist->m_data.m_key.m_bExpireForgiveness)&&(g_parchivist->m_data.m_key.m_ulExpiryDate+g_parchivist->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
											)
										&&(
												(!g_parchivist->m_data.m_key.m_bMachineSpecific)
											||((g_parchivist->m_data.m_key.m_bMachineSpecific)&&(g_parchivist->m_data.m_key.m_bValidMAC))
											)
										)
									{
					// this overrides stuff so let's not let it do that.

										// let's just not override this in a green condition.  
										// comment it out.
//										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//										g_parchivist->m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_OK);
									}
									else
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
										g_parchivist->m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_ERROR);
									}
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
									g_parchivist->m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_ERROR);
								}

							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("CommandServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usCommandPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("StatusServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usStatusPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Messager")==0)
				{
					if(szParameter.CompareNoCase("UseEmail")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseEmail = true;
							else m_bUseEmail = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseNet")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseNetwork = true;
							else m_bUseNetwork = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseLog = true;
							else m_bUseLog = false;
						}
					}
					else
					if(szParameter.CompareNoCase("ReportSuccessfulOperation")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bReportSuccessfulOperation = true;
							else m_bReportSuccessfulOperation = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseXMLClientLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseXMLClientLog = true;
							else m_bUseXMLClientLog = false;
						}
					}
				}
				else
				if(szCategory.CompareNoCase("FileHandling")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("DeleteSourceFileOnTransfer")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bDeleteSourceFileOnTransfer = true;
							else m_bDeleteSourceFileOnTransfer = false;
						}
					}
					else
					if(szParameter.CompareNoCase("PendingRetries")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>=0) m_nPendingRetries = nLength;
						}
					}
					else
					if(szParameter.CompareNoCase("MaxFilesPerCycle")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							m_nMaxFilesPerCycle = nLength;
						}
					}
					else
					if(szParameter.CompareNoCase("AutoDeleteOldest")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bAutoDeleteOldest = true;
							else m_bAutoDeleteOldest = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseDeletionSync")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseDeletionSync = true;
							else m_bUseDeletionSync = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseBackupDeletionSync")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseBackupDeletionSync = true;
							else m_bUseBackupDeletionSync = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseBackupDestination")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseBackupDestination = true;
							else m_bUseBackupDestination = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseBackupDatabase")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseBackupDatabase = true;
							else m_bUseBackupDatabase = false;
						}
					}
					else  
					if(szParameter.CompareNoCase("FilterFileTypes")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bFilterFileTypes = true;
							else m_bFilterFileTypes = false;
						}
					}
					else
					if(szParameter.CompareNoCase("WatchfolderInterval")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_nWatchfolderInterval = nLength;
						}
					}
					else
					if(szParameter.CompareNoCase("DeletePercentageDiskFull")==0)
					{
						if(nLength>0)
						{
							double dblValue = atof(szValue);
							if((dblValue>0.1)&&(dblValue<99.9)) m_dblDeletePercentageDiskFull = dblValue;
						}
					}
					else
					if(szParameter.CompareNoCase("WarnPercentageDiskFull")==0)
					{
						if(nLength>0)
						{
							double dblValue = atof(szValue);
							if((dblValue>0.1)&&(dblValue<99.9)) m_dblWarnPercentageDiskFull = dblValue;
						}
					}
					else
					if(szParameter.CompareNoCase("WatchFolderPath")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);

								bool bBeginNewWatch = false;

								if(m_pszWatchFolderPath)
								{
									if(stricmp(m_pszWatchFolderPath, pch)) bBeginNewWatch=true;
								}
								else	bBeginNewWatch = true;
								

								if(bBeginNewWatch)
								{
									g_parchivist->m_data.m_bWatchfolderWarningSent = false;
									char errorstring[MAX_MESSAGE_LENGTH];

									int nReturn = g_dir.EndWatch();
									g_parchivist->SendMsg(CX_SENDMSG_INFO, "Archivist:EndWatch", "EndWatch: ending current watchfolder process on %s.", g_parchivist->m_settings.m_pszWatchFolderPath?g_parchivist->m_settings.m_pszWatchFolderPath:"(null)"); 

//									while(g_dir.m_ulChangeThreads>0) Sleep(100);
									int nEnd = g_parchivist->m_data.m_timebTick.time+ ARCHIVIST_THREAD_WAIT_MAX;
									while((g_dir.m_ulChangeThreads>0)&&(g_parchivist->m_data.m_timebTick.time<nEnd))
									{
										_ftime( &g_parchivist->m_data.m_timebTick );
										Sleep(100);
									}

									Sleep(500); // let the thread end

									if(m_pszWatchFolderPath) free(m_pszWatchFolderPath);
									m_pszWatchFolderPath = pch;

									nReturn = g_dir.SetWatchFolder(g_parchivist->m_settings.m_pszWatchFolderPath);
									if(nReturn<DIRUTIL_SUCCESS)
									{
										// error
										//TODO report
_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error setting watchfolder.");  
g_parchivist->m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_ERROR);

										if(!g_parchivist->m_data.m_bWatchfolderWarningSent)
										{
											g_parchivist->SendMsg(CX_SENDMSG_ERROR, "Archivist:SetWatchFolder", "SetWatchFolder returned %d with %s", nReturn, g_parchivist->m_settings.m_pszWatchFolderPath?g_parchivist->m_settings.m_pszWatchFolderPath:"(null)");
											g_parchivist->m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:SetWatchFolder", "SetWatchFolder returned %d with %s", nReturn, g_parchivist->m_settings.m_pszWatchFolderPath?g_parchivist->m_settings.m_pszWatchFolderPath:"(null)"); //  Sleep(100);//(Dispatch message)
					//					g_parchivist->m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:SetWatchFolder", "SetWatchFolder returned %d with %s", nReturn, g_parchivist->m_settings.m_pszWatchFolderPath?g_parchivist->m_settings.m_pszWatchFolderPath:"(null)");  //(Dispatch message)
										}
										g_parchivist->m_data.m_bWatchfolderWarningSent = true;
									}
									else
									{
										g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:SetWatchFolder", "SetWatchFolder succeeded with %s", g_parchivist->m_settings.m_pszWatchFolderPath?g_parchivist->m_settings.m_pszWatchFolderPath:"(null)");  // Sleep(100);//(Dispatch message)

										nReturn = g_dir.BeginWatch( (unsigned long)g_parchivist->m_settings.m_nWatchfolderInterval, true);
										if(nReturn<DIRUTIL_SUCCESS)
										{
											// error
											//TODO report
_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error beginning watchfolder process.");  
g_parchivist->m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_ERROR);

											if(!g_parchivist->m_data.m_bWatchfolderWarningSent)
											{
												g_parchivist->SendMsg(CX_SENDMSG_ERROR, "Archivist:BeginWatch", "BeginWatch returned %d;  could not begin watchfolder thread on %s.", nReturn, g_parchivist->m_settings.m_pszWatchFolderPath?g_parchivist->m_settings.m_pszWatchFolderPath:"(null)");
												g_parchivist->m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BeginWatch", "BeginWatch returned %d;  could not begin watchfolder thread on %s.", nReturn, g_parchivist->m_settings.m_pszWatchFolderPath?g_parchivist->m_settings.m_pszWatchFolderPath:"(null)"); //  Sleep(100);//(Dispatch message)
						//						g_parchivist->m_msgr.DM(MSG_ICONERROR, NULL, "Direct:BeginWatch", "BeginWatch returned %d;  could not begin watchfolder thread on %s.", nReturn, g_parchivist->m_settings.m_pszWatchFolderPath?g_parchivist->m_settings.m_pszWatchFolderPath:"(null)");  //(Dispatch message)
											}
											g_parchivist->m_data.m_bWatchfolderWarningSent = true;
										}
										else
										{
											g_parchivist->SendMsg(CX_SENDMSG_INFO, "Archivist:BeginWatch", "BeginWatch: began watch on %s.", g_parchivist->m_settings.m_pszWatchFolderPath?g_parchivist->m_settings.m_pszWatchFolderPath:"(null)"); 
											g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:SetWatchFolder", "SetWatchFolder: began watch on %s.", g_parchivist->m_settings.m_pszWatchFolderPath?g_parchivist->m_settings.m_pszWatchFolderPath:"(null)"); //  Sleep(100);//(Dispatch message)
					//						g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, "Direct:SetWatchFolder", "SetWatchFolder: began watch on %s", g_parchivist->m_settings.m_pszWatchFolderPath);  //(Dispatch message)

											g_parchivist->m_data.m_bWatchfolderWarningSent = false; //resets
_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "watching folder...");  
g_parchivist->m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_OK);

											// subtract half the interval so that the watch folder checks for changes, staggered with us checking the results in the else below.
											g_parchivist->m_data.m_watchtime.time -= g_parchivist->m_settings.m_nWatchfolderInterval/2000; // second resolution is fine.
					/*						watchtime.millitm += (unsigned short)((g_parchivist->m_settings.m_ulWatchIntervalMS%1000)/2); // fractional second updates
											if(watchtime.millitm>999)
											{
												watchtime.time++;
												watchtime.millitm%=1000;
											}
					*/
										}
									}
								}
								else
								{
									if(m_pszWatchFolderPath) free(m_pszWatchFolderPath);
									m_pszWatchFolderPath = pch;

								}
							}
						}
					}
					else
					if(szParameter.CompareNoCase("DestinationFolderPath")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszDestinationFolderPath) free(m_pszDestinationFolderPath);
								m_pszDestinationFolderPath = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("BackupDestinationFolderPath")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszBackupDestinationFolderPath) free(m_pszBackupDestinationFolderPath);
								m_pszBackupDestinationFolderPath = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("BackupDestinationFolderLocalPath")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszBackupDestinationFolderLocalPath) free(m_pszBackupDestinationFolderLocalPath);
								m_pszBackupDestinationFolderLocalPath = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ArchiveDestinationFolderPath")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszArchiveDestinationFolderPath) free(m_pszArchiveDestinationFolderPath);
								m_pszArchiveDestinationFolderPath = pch;
							}
						}
					}
					else 
					if(szParameter.CompareNoCase("OverwriteFiles")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bOverwriteFiles = true;
							else m_bOverwriteFiles = false;
						}
					}

				}
				else
				if(szCategory.CompareNoCase("Database")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("SettingsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszSettings) free(m_pszSettings);
								m_pszSettings = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ExchangeTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszExchange) free(m_pszExchange);
								m_pszExchange = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("MessagesTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszMessages) free(m_pszMessages);
								m_pszMessages = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("MetadataTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszMetadata) free(m_pszMetadata);
								m_pszMetadata = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("FileTypesTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszFileTypes) free(m_pszFileTypes);
								m_pszFileTypes = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("QueueTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszQueue) free(m_pszQueue);
								m_pszQueue = pch;
							}
						}
					}
/*
					else
					if(szParameter.CompareNoCase("ConnectionsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszConnections) free(m_pszConnections);
								m_pszConnections = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("LiveEventsTableName")==0)
					{
						if(nLength>0)
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLiveEvents) free(m_pszLiveEvents);
								m_pszLiveEvents = pch;
							}
						}
					}
*/
					else
					if(szParameter.CompareNoCase("ModificationCheckInterval")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_ulModsIntervalMS = nLength;
						}
					}
				}
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
					if(szParameter.CompareNoCase("Messages")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeMessageDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
				}
/*
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
					if(szParameter.CompareNoCase("AsRun_Log")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeAsRunDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
				}
*/				


				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			if(pszInfo)
			{
				_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
			}
			delete prs;
			prs = NULL;

			Settings(false); //write
/*
			if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
			{
				// these explicts arent necessary - uncomment to write out a full file to edit...
				file.SetIniString("Main", "Name", g_parchivist->m_settings.m_pszName);
				file.SetIniInt("CommandServer", "ListenPort", g_parchivist->m_settings.m_usCommandPort);
				file.SetIniInt("StatusServer", "ListenPort", g_parchivist->m_settings.m_usStatusPort);
				file.SetIniString("License", "Key", g_parchivist->m_settings.m_pszLicense);

				file.SetIniString("FileServer", "IconPath", g_parchivist->m_settings.m_pszIconPath);

				file.SetIniInt("Messager", "UseEmail", g_parchivist->m_settings.m_bUseEmail?1:0);
				file.SetIniInt("Messager", "UseNet", g_parchivist->m_settings.m_bUseNetwork?1:0);
				file.SetIniInt("Messager", "UseLog", g_parchivist->m_settings.m_bUseLog?1:0);
				file.SetIniInt("Messager", "ReportSuccessfulOperation", g_parchivist->m_settings.m_bReportSuccessfulOperation?1:0);
				file.SetIniInt("Messager", "LogTransfers", g_parchivist->m_settings.m_bLogTransfers?1:0);

				file.SetIniInt("FileHandling", "UseDeletionSync", g_parchivist->m_settings.m_bUseDeletionSync?1:0); //
				file.SetIniInt("FileHandling", "UseBackupDeletionSync", g_parchivist->m_settings.m_bUseBackupDeletionSync?1:0); //
				file.SetIniInt("FileHandling", "DeleteSourceFileOnTransfer", g_parchivist->m_settings.m_bDeleteSourceFileOnTransfer?1:0); 
				file.SetIniInt("FileHandling", "UseBackupDestination", g_parchivist->m_settings.m_bUseBackupDestination?1:0);
				file.SetIniInt("FileHandling", "UseBackupDatabase", g_parchivist->m_settings.m_bUseBackupDatabase?1:0);
				file.SetIniInt("FileHandling", "FilterFileTypes", g_parchivist->m_settings.m_bFilterFileTypes?1:0);
				file.SetIniInt("FileHandling", "WatchfolderInterval", g_parchivist->m_settings.m_nWatchfolderInterval); // check dir for changes every this many ms
				sprintf(errorstring, "%f", g_parchivist->m_settings.m_dblDeletePercentageDiskFull);
				file.SetIniString("FileHandling", "DeletePercentageDiskFull", errorstring); 
				sprintf(errorstring, "%f", g_parchivist->m_settings.m_dblWarnPercentageDiskFull);
				file.SetIniString("FileHandling", "WarnPercentageDiskFull", errorstring); 
				file.SetIniString("FileHandling", "WatchFolderPath", g_parchivist->m_settings.m_pszWatchFolderPath);  
				file.SetIniString("FileHandling", "DestinationFolderPath", g_parchivist->m_settings.m_pszDestinationFolderPath);  
				file.SetIniString("FileHandling", "BackupDestinationFolderPath", g_parchivist->m_settings.m_pszBackupDestinationFolderPath );  
				file.SetIniString("FileHandling", "BackupDestinationFolderLocalPath", g_parchivist->m_settings.m_pszBackupDestinationFolderLocalPath);  
				file.SetIniString("FileHandling", "ArchiveDestinationFolderPath", g_parchivist->m_settings.m_pszArchiveDestinationFolderPath);  
				file.SetIniInt("FileHandling", "OverwriteFiles", g_parchivist->m_settings.m_bOverwriteFiles?1:0);
				file.SetIniInt("FileHandling", "AutoDeleteOldest", g_parchivist->m_settings.m_bAutoDeleteOldest?1:0);
				file.SetIniInt("InstalledModules", "DirectInstalled", g_parchivist->m_settings.m_bDirectInstalled?1:0);
				file.SetIniInt("FileHandling", "PendingRetries", g_parchivist->m_settings.m_nPendingRetries);


				file.SetIniString("Database", "DSN", g_parchivist->m_settings.m_pszDSN);
				file.SetIniString("Database", "DBUser", g_parchivist->m_settings.m_pszUser);
				file.SetIniString("Database", "DBPassword", g_parchivist->m_settings.m_pszPW);
				file.SetIniString("Database", "DBDefault", g_parchivist->m_settings.m_pszDatabase );

				file.SetIniString("Database", "BackupDBServer", g_parchivist->m_settings.m_pszBackupServer);
				file.SetIniString("Database", "BackupDBDefault", g_parchivist->m_settings.m_pszBackupDatabase ); // for pushing to backup db thru primary DSN conn

				file.SetIniString("Database", "BackupDSN", g_parchivist->m_settings.m_pszBackupDSN);
				file.SetIniString("Database", "BackupDBUser", g_parchivist->m_settings.m_pszBackupUser);
				file.SetIniString("Database", "BackupDBPassword", g_parchivist->m_settings.m_pszBackupPW);
				file.SetIniString("Database", "SettingsTableName", g_parchivist->m_settings.m_pszSettings);  // the Settings table name
				file.SetIniString("Database", "ExchangeTableName", g_parchivist->m_settings.m_pszExchange);  // the Exchange table name
				file.SetIniString("Database", "MessagesTableName", g_parchivist->m_settings.m_pszMessages);  // the Messages table name
				file.SetIniString("Database", "MetadataTableName", g_parchivist->m_settings.m_pszMetadata);  // the Messages table name
				file.SetIniString("Database", "FileTypesTableName", g_parchivist->m_settings.m_pszFileTypes);  // the File Types table name
				file.SetIniString("Database", "QueueTableName", g_parchivist->m_settings.m_pszQueue);  // the Queue table name
				file.SetIniString("Database", "PendingTableName", g_parchivist->m_settings.m_pszPending);  // the Pending table name

				file.SetIniInt("Database", "ModificationCheckInterval", g_parchivist->m_settings.m_ulModsIntervalMS);  // in milliseconds


		//		file.SetIniInt("HarrisAPI", "UseListCount", g_parchivist->m_settings.m_bUseListCount?1:0); // get all events up until the list count (otherwise just up to the lookahead)

		//		file.SetIniString("Database", "ChannelsTableName", g_parchivist->m_settings.m_pszChannels);  // the Channels table name
		//		file.SetIniString("Database", "ConnectionsTableName", g_parchivist->m_settings.m_pszConnections);  // the Connections table name
		//		file.SetIniString("Database", "LiveEventsTableName", g_parchivist->m_settings.m_pszLiveEvents);  // the LiveEvents table name


				file.SetSettings(ARCHIVIST_SETTINGS_FILE_DEFAULT, false);  // have to have correct filename

			}
*/

			return ARCHIVIST_SUCCESS;
		}
	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s%s%s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. ",
				m_pdb?"":"Database pointer was NULL. ",
				m_pdbConn?"":"Connection pointer was NULL. ",
				((m_pdbConn)&&(!m_pdbConn->m_bConnected))?"Database not connected. ":""
				);
		}
	}
	return ARCHIVIST_ERROR;
}

char* CArchivistSettings::ProcessString(char* pszString, bool bFreeIncomingString)
{
	if(pszString)
	{
		char pszOutput[4096];
		int nInLen=strlen(pszString);
		int nOutLen=0;
		char* pch = pszString;
		char* pchEnd = min((pch+4096),(pszString+nInLen));
		while((*pch!=0)&&(pch<pchEnd))
		{
			if(*pch == '%')
			{
				// we are in a specifier.
				// here is a list of ours:
				// %h  local hostname
		
				pch++;
				if((*pch!=0)&&(pch<pchEnd))
				{
					if(*pch == 'h')
					{
						if((g_parchivist->m_data.m_pszHost)&&(strlen(g_parchivist->m_data.m_pszHost)))
						{
							char* pszTemp = (char*)malloc(strlen(g_parchivist->m_data.m_pszHost)+1);
							if(pszTemp)
							{
								strcpy(pszTemp, g_parchivist->m_data.m_pszHost);
							
								char* pchPlus = pszTemp;
								while((*pchPlus != 0)&&(nOutLen<4096))
								{
									pszOutput[nOutLen]= *pchPlus;
									nOutLen++;
									pchPlus++;
								}
								free(pszTemp);
							}
						}
						pch++;
					}
					else
					{
						// some other specifier, can be anything.
						pszOutput[nOutLen]= *(pch-1); // do add the %, this may get further processed
						nOutLen++;
						pszOutput[nOutLen]= *pch;
						nOutLen++;
						pch++;
					}
				}
				else
				{
					pszOutput[nOutLen]= *(pch-1);
					nOutLen++;
				}
				
			}
			else
			{
				pszOutput[nOutLen]= *pch;
				nOutLen++;
				pch++;
			}
		}

		pszOutput[nOutLen]=0;

		pch = (char*)malloc(strlen(pszOutput)+1);
		if(pch)
		{
			strcpy(pch,pszOutput);
			if(bFreeIncomingString) 
			{
				try {free(pszString);} catch(...){}
			}
		}
		return pch;
	}
	return NULL;
}


