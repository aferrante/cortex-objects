// ArchivistData.cpp: implementation of the CArchivistData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <process.h>
#include "Archivist.h"
#include "ArchivistHandler.h" 
#include "ArchivistMain.h" 
#include "ArchivistData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CArchivistMain* g_parchivist;
extern CArchivistApp theApp;
//extern CADC g_adc; 	// the Harris ADC object

extern bool g_bKillThread;

//////////////////////////////////////////////////////////////////////
// CArchivistConnectionObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*
CArchivistConnectionObject::CArchivistConnectionObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_pszClientName = NULL;
	m_ulStatus	= ARCHIVIST_STATUS_UNINIT;
	m_ulFlags = ARCHIVIST_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
//	m_pAPIConn = NULL;
	m_bKillConnThread = true;
	m_bConnThreadStarted = false;

}

CArchivistConnectionObject::~CArchivistConnectionObject()
{
	m_bKillConnThread = true;
	while(	m_bConnThreadStarted ) Sleep(1);

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
	if(m_pszClientName) free(m_pszClientName); // must use malloc to allocate
}

*/

//////////////////////////////////////////////////////////////////////
// CArchivistChannelObject Construction/Destruction
//////////////////////////////////////////////////////////////////////


/*
CArchivistChannelObject::CArchivistChannelObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_ulStatus	= ARCHIVIST_STATUS_UNINIT;
	m_ulFlags = ARCHIVIST_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
	m_nChannelID = -1; // unassigned
	m_nHarrisListID = -1; // unassigned
	m_pbKillConnThread = NULL;
	m_bKillChannelThread = true;
	m_bChannelThreadStarted = false;
	m_pAPIConn = NULL;
}

CArchivistChannelObject::~CArchivistChannelObject()
{
	m_bKillChannelThread = true;
	while(	m_bChannelThreadStarted ) Sleep(1);

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
}


*/

CArchivistFileTypeObject::CArchivistFileTypeObject()
{
	m_ulFlags = ARCHIVIST_FLAG_DISABLED;
	m_pszExt = NULL;
	m_pszDesc = NULL;
	m_nID = -1;
}

CArchivistFileTypeObject::~CArchivistFileTypeObject()
{
	if(m_pszExt) free(m_pszExt); // must use malloc to allocate
	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
}


//////////////////////////////////////////////////////////////////////
// CArchivistData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CArchivistData::CArchivistData()
{
	InitializeCriticalSection(&m_critText);

	// messaging...
	m_bNetworkMessagingInitialized=false;

	m_bCheckModsWarningSent = false;
//	m_ppConnObj = NULL;
//	m_nNumConnectionObjects = 0;
//	m_ppChannelObj = NULL;
//	m_nNumChannelObjects = 0;

	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebAutoPurge ); // the last time autopurge was run
	_ftime( &m_timebTick );
	m_pszHost = NULL;			// the name of the host
	m_pszCompleteHost = NULL;	// the complete name of the host
	m_pszCortexHost = NULL;			// the name of the cortex host
	m_usCortexCommandPort = ARCHIVIST_PORT_CMD;
	m_usCortexStatusPort = ARCHIVIST_PORT_STATUS;
	m_nSettingsMod = -1;
	m_nMetadataMod = -1;
	m_nFileTypesMod = -1;
//	m_nChannelsMod = -1;
//	m_nConnectionsMod = -1;
	m_nLastSettingsMod = -1;
	m_nLastMetadataMod = -1;
	m_nLastFileTypesMod = -1;
//	m_nLastChannelsMod = -1;
//	m_nLastConnectionsMod = -1;

	m_nQueueMod =-1;
	m_nLastQueueMod =-1;

	m_pdb = NULL;
	m_pdbConn = NULL;
	m_pdb2 = NULL;
	m_pdb2Conn = NULL;
	m_pdbBackup = NULL;
	m_pdbBackupConn = NULL;


	m_bQuietKill = false;
	m_ppFileTypes = NULL;
	m_nNumFileTypes=0;


	m_uliBytesAvail.LowPart = 0; //init!  
	m_uliBytesAvail.HighPart = 0; //init!  
	m_uliBytesTotal.LowPart = 0; //init!  
	m_uliBytesTotal.HighPart = 0; //init!  
	m_uliBytesFree.LowPart = 0; //init!  
	m_uliBytesFree.HighPart = 0; //init!  

	m_dblAvail = -1.0;
	m_dblTotal = -1.0;
	m_dblFree = -1.0;

	m_bDiskWarningSent = false;
	m_bDiskWarning = false;
	m_bDiskDeletionSent = false;
	m_bDiskDeletion = false;
	m_bWatchfolderWarningSent = false;

	m_bBackupDiskWarningSent = false;
	m_bBackupDiskWarning = false;
	m_bBackupDiskDeletionSent = false;
	m_bBackupDiskDeletion = false;

	m_bWatchfolderInitialized=false;

	m_bProcessSuspended = false;

	_ftime( &m_watchtime );

}

CArchivistData::~CArchivistData()
{

	if(m_ppFileTypes)
	{
		int i=0;
		while(i<m_nNumFileTypes)
		{
			if(m_ppFileTypes[i]) delete m_ppFileTypes[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppFileTypes; // delete array of pointers to objects, must use new to allocate
	}

/*
	if(m_ppConnObj)
	{
		int i=0;
		while(i<m_nNumConnectionObjects)
		{
			if(m_ppConnObj[i]) delete m_ppConnObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppConnObj; // delete array of pointers to objects, must use new to allocate
	}

	if(m_ppChannelObj)
	{
		int i=0;
		while(i<m_nNumChannelObjects)
		{
			if(m_ppChannelObj[i]) delete m_ppChannelObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppChannelObj; // delete array of pointers to objects, must use new to allocate
	}
*/
	if(m_pszHost) free(m_pszHost);	// must use malloc to allocate
	if(m_pszCompleteHost) free(m_pszCompleteHost);	// must use malloc to allocate
	if(m_pszCortexHost) free(m_pszCortexHost);	// must use malloc to allocate

	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);
}

char* CArchivistData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CArchivistData::SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError)
{
	int nRV = CX_ERROR;
	bool bError=false;

	if(!bOverwriteExistingError)
	{
		if(
			  ((m_ulFlags&ARCHIVIST_ICON_MASK) == ARCHIVIST_STATUS_ERROR)
			||((m_ulFlags&ARCHIVIST_STATUS_CMDSVR_MASK) == ARCHIVIST_STATUS_CMDSVR_ERROR)
			||((m_ulFlags&ARCHIVIST_STATUS_STATUSSVR_MASK) ==  ARCHIVIST_STATUS_STATUSSVR_ERROR)
			||((m_ulFlags&ARCHIVIST_STATUS_THREAD_MASK) == ARCHIVIST_STATUS_THREAD_ERROR)
			||((m_ulFlags&ARCHIVIST_STATUS_FAIL_MASK) == ARCHIVIST_STATUS_FAIL_DB)
			)	bError=true;
	}

	if (ulStatus&ARCHIVIST_ICON_MASK)
	{
		m_ulFlags &= ~ARCHIVIST_ICON_MASK;
		m_ulFlags |= (ulStatus&ARCHIVIST_ICON_MASK);
	}
	if (ulStatus&ARCHIVIST_STATUS_CMDSVR_MASK)
	{
		m_ulFlags &= ~ARCHIVIST_STATUS_CMDSVR_MASK;
		m_ulFlags |= (ulStatus&ARCHIVIST_STATUS_CMDSVR_MASK);
	}
	if (ulStatus&ARCHIVIST_STATUS_STATUSSVR_MASK)
	{
		m_ulFlags &= ~ARCHIVIST_STATUS_STATUSSVR_MASK;
		m_ulFlags |= (ulStatus&ARCHIVIST_STATUS_STATUSSVR_MASK);
	}
	if (ulStatus&ARCHIVIST_STATUS_THREAD_MASK)
	{
		m_ulFlags &= ~ARCHIVIST_STATUS_THREAD_MASK;
		m_ulFlags |= (ulStatus&ARCHIVIST_STATUS_THREAD_MASK);
	}
	if (ulStatus&ARCHIVIST_STATUS_FAIL_MASK)
	{
		m_ulFlags &= ~ARCHIVIST_STATUS_FAIL_MASK;
		m_ulFlags |= (ulStatus&ARCHIVIST_STATUS_FAIL_MASK);
	}

	if(bError)
	{
		m_ulFlags &= ~ARCHIVIST_ICON_MASK;
		m_ulFlags |= ARCHIVIST_STATUS_ERROR;
	}

	if((pszText)&&(!bError))
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
//				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
//			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_parchivist->m_settings.m_pszIconPath)&&(strlen(g_parchivist->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_parchivist->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_parchivist->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_parchivist->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_parchivist->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_parchivist->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}
/*
int	CArchivistData::SetStatusText(char* pszText, unsigned long ulStatus)
{
	int nRV = CX_ERROR;
	if(pszText)
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_parchivist->m_settings.m_pszIconPath)&&(strlen(g_parchivist->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_parchivist->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_parchivist->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_parchivist->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_parchivist->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_parchivist->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}
	return nRV;
}
*/
// utility
int	CArchivistData::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszCompleteHost) free(m_pszCompleteHost);
						m_pszCompleteHost=pch;

						pch = (char*) malloc(nValue+1);
						if(pch!=NULL)
						{
							if(m_pszHost) free(m_pszHost);
							m_pszHost=pch;

							strcpy(m_pszHost, m_pszCompleteHost);
							pch = strchr(m_pszHost, '.');
							if(pch)
							{
								*pch = 0;
							}

							return ARCHIVIST_SUCCESS;
						}
					}
				}
			}
		}
	}
	return ARCHIVIST_ERROR;
}

int CArchivistData::PushBackupRecord(char* pszFilename, char* pszInfo)
{
	int nReturn = ARCHIVIST_ERROR;
	if(m_pdb2)
	{
		char errorstring[MAX_MESSAGE_LENGTH];
		CSafeBufferUtil sbu;
		bool bDelete = false;

		char* pchEncodedFile = NULL; 
		char* pchEncodedPath = NULL; 
		char* pchEncodedBackupPath = NULL; 

		//pszFilename either contains a |filename, or a filepath|filename pair
		char* pchField = sbu.Token(pszFilename, strlen(pszFilename), "|", MODE_SINGLEDELIM);
		if((pchField)&&(strlen(pchField)))
		{
			pchEncodedPath = m_pdb2->EncodeQuotes(pchField);

			//m_pszWatchFolderPath
			// have to assmeble the backup local file path..

			char path_buffer[MAX_PATH+1];
			if(strlen(pchField)>strlen(g_parchivist->m_settings.m_pszDestinationFolderPath))
			{
				pchEncodedBackupPath = pchField + strlen(g_parchivist->m_settings.m_pszDestinationFolderPath);
				sprintf(path_buffer, "%s%s", g_parchivist->m_settings.m_pszBackupDestinationFolderLocalPath, pchEncodedBackupPath);
			}
			else  // must be in the root watch dir
			{
				sprintf(path_buffer, "%s", g_parchivist->m_settings.m_pszBackupDestinationFolderLocalPath);
			}

			pchEncodedBackupPath = NULL;

			// remove trailing slashes, if any
			while(
							(strlen(path_buffer)>0)
						&&(
								(path_buffer[strlen(path_buffer)-1]=='\\')
							||(path_buffer[strlen(path_buffer)-1]=='/')
							)
						)
			{
				path_buffer[strlen(path_buffer)-1] = 0;
			}

			pchEncodedBackupPath = m_pdb2->EncodeQuotes(path_buffer);
		}

		pchField = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
		if((pchField)&&(strlen(pchField)))
		{
			pchEncodedFile = m_pdb2->EncodeQuotes(pchField);
		}

		if(pchField)
		{
			pchField = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);

			if((pchField)&&(strcmp(pchField, "delete")==0))
			{
				bDelete = true;
			}
		}


		if(
			  (pchEncodedFile)
			&&(strlen(pchEncodedFile)>0)
			&&(g_parchivist->m_settings.m_pszBackupServer)  // need servername for backup server, we are going thru primary conn.
			&&(strlen(g_parchivist->m_settings.m_pszBackupServer)>0)
			)  // must have at least a file
		{
			char szSQL[DB_SQLSTRING_MAXLEN];


			// we have to delete the file on the backup db

			if((pchEncodedBackupPath)&&(strlen(pchEncodedBackupPath)>0))
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM [%s].%s.dbo.%s WHERE \
sys_filename = '%s' AND \
sys_filepath = '%s'",
					g_parchivist->m_settings.m_pszBackupServer,
					g_parchivist->m_settings.m_pszBackupDatabase?g_parchivist->m_settings.m_pszBackupDatabase:"Archivist",
					g_parchivist->m_settings.m_pszMetadata?g_parchivist->m_settings.m_pszMetadata:"File_Metadata",
					pchEncodedFile,
					pchEncodedBackupPath
					);
			}
			else
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM [%s].%s.dbo.%s WHERE \
sys_filename = '%s'",
					g_parchivist->m_settings.m_pszBackupServer,
					g_parchivist->m_settings.m_pszBackupDatabase?g_parchivist->m_settings.m_pszBackupDatabase:"Archivist",
					g_parchivist->m_settings.m_pszMetadata?g_parchivist->m_settings.m_pszMetadata:"File_Metadata",
					pchEncodedFile
					);
			}
//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "SQL %s", szSQL);    //(Dispatch message)
			if(m_pdb2->ExecuteSQL(m_pdb2Conn, szSQL, errorstring)>=DB_SUCCESS)
			{
				nReturn = ARCHIVIST_SUCCESS;
			}
			else g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "SQL error: %s", errorstring);    //(Dispatch message)


			if(!bDelete)
			{
			// we then insert the one
//insert into [10.0.0.222].archivist.dbo.file_metadata select * from archivist.dbo.file_metadata where sys_filename = 'R603.oxt' and sys_filepath = 'D:\_DestinationFolder';
				if((pchEncodedPath)&&(strlen(pchEncodedPath)>0))
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO [%s].%s.dbo.%s SELECT * FROM %s.dbo.%s WHERE \
sys_filename = '%s' AND \
sys_filepath = '%s'",
						g_parchivist->m_settings.m_pszBackupServer,
						g_parchivist->m_settings.m_pszBackupDatabase?g_parchivist->m_settings.m_pszBackupDatabase:"Archivist",
						g_parchivist->m_settings.m_pszMetadata?g_parchivist->m_settings.m_pszMetadata:"File_Metadata",
						g_parchivist->m_settings.m_pszDatabase?g_parchivist->m_settings.m_pszDatabase:"Archivist",
						g_parchivist->m_settings.m_pszMetadata?g_parchivist->m_settings.m_pszMetadata:"File_Metadata",
						pchEncodedFile,
						pchEncodedPath
						);
				}
				else
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO [%s].%s.dbo.%s SELECT * FROM %s.dbo.%s WHERE \
sys_filename = '%s'",
						g_parchivist->m_settings.m_pszBackupServer,
						g_parchivist->m_settings.m_pszBackupDatabase?g_parchivist->m_settings.m_pszBackupDatabase:"Archivist",
						g_parchivist->m_settings.m_pszMetadata?g_parchivist->m_settings.m_pszMetadata:"File_Metadata",
						g_parchivist->m_settings.m_pszDatabase?g_parchivist->m_settings.m_pszDatabase:"Archivist",
						g_parchivist->m_settings.m_pszMetadata?g_parchivist->m_settings.m_pszMetadata:"File_Metadata",
						pchEncodedFile
						);
				}
				if(m_pdb2->ExecuteSQL(m_pdb2Conn, szSQL, errorstring)>=DB_SUCCESS)
				{
					nReturn = ARCHIVIST_SUCCESS;
				}
				else g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "SQL error: %s", errorstring);    //(Dispatch message)

				// we then update the one with the proper path value.
				if(
						(pchEncodedPath)
					&&(strlen(pchEncodedPath)>0)
					&&(pchEncodedBackupPath)  // need servername for backup server, we are going thru primary conn.
					&&(strlen(pchEncodedBackupPath)>0)
					&&(strcmp(pchEncodedPath, pchEncodedBackupPath))  // only do if different.
					)  // must have at least a file
				{
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE [%s].%s.dbo.%s SET \
sys_filepath = '%s' WHERE \
sys_filename = '%s' AND \
sys_filepath = '%s'",
						g_parchivist->m_settings.m_pszBackupServer,
						g_parchivist->m_settings.m_pszBackupDatabase?g_parchivist->m_settings.m_pszBackupDatabase:"Archivist",
						g_parchivist->m_settings.m_pszMetadata?g_parchivist->m_settings.m_pszMetadata:"File_Metadata",
						pchEncodedBackupPath,
						pchEncodedFile,
						pchEncodedPath
						);
//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "SQL %s", szSQL);    //(Dispatch message)
					if(m_pdb2->ExecuteSQL(m_pdb2Conn, szSQL, errorstring)>=DB_SUCCESS)
					{
						nReturn = ARCHIVIST_SUCCESS;
					}
					else g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "SQL error: %s", errorstring);    //(Dispatch message)
				}
			}
		}

		if(pchEncodedFile) free(pchEncodedFile);
		if(pchEncodedPath) free(pchEncodedPath);
		if(pchEncodedBackupPath) free(pchEncodedBackupPath);

	}

	return nReturn;
}

int CArchivistData::ExecuteBackupQuery(char* pszQuery, char* pszInfo)
{
	int nReturn = ARCHIVIST_ERROR;
	if(
//		  (m_pdb2)
			(m_pdbBackup)
		&&(m_pdbBackupConn)
//		&&(g_parchivist->m_settings.m_pszBackupDSN)&&(strlen(g_parchivist->m_settings.m_pszBackupDSN)>0)
//		&&(g_parchivist->m_settings.m_pszBackupUser)&&(strlen(g_parchivist->m_settings.m_pszBackupUser)>0)
//		&&(g_parchivist->m_settings.m_pszBackupPW)  // could be blank
		)
	{
		//  using a transient connection to DB.  (dont just keep open).
/*
		CDBconn* pdbBackupConn = m_pdb2->CreateNewConnection(
			g_parchivist->m_settings.m_pszBackupDSN, 
			g_parchivist->m_settings.m_pszBackupUser, 
			g_parchivist->m_settings.m_pszBackupPW);
//		g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "backup metadata after connect"); Sleep(100); //(Dispatch message)
*/
//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "SQL %s", pszQuery);    //(Dispatch message)
		char errorstring[MAX_MESSAGE_LENGTH];
//		if(m_pdb->ExecuteSQL(pdbBackupConn, pszQuery, pszInfo)>=DB_SUCCESS)
		if(m_pdbBackup->ExecuteSQL(m_pdbBackupConn, pszQuery, errorstring)>=DB_SUCCESS)
		{
			nReturn = ARCHIVIST_SUCCESS;
		}
		else g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "SQL error: %s", errorstring);    //(Dispatch message)
	}
	return nReturn;
}


int CArchivistData::IncrementDatabaseMods(char* pszTableName, char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb)&&(pszTableName)&&(strlen(pszTableName))
		&&(g_parchivist)&&(g_parchivist->m_settings.m_pszExchange)&&(strlen(g_parchivist->m_settings.m_pszExchange)))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s set mod = \
(SELECT case when max(mod) is null then 1 when max(mod) >= %d \
then 1 else max(mod) + 1 end from %s WHERE criterion = '%s') WHERE criterion = '%s'",
			g_parchivist->m_settings.m_pszExchange,
			ARCHIVIST_DB_MOD_MAX,
			g_parchivist->m_settings.m_pszExchange,
			szTemp, szTemp		
			);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			return ARCHIVIST_SUCCESS;
		}
	}
	return ARCHIVIST_ERROR;
}


int CArchivistData::CheckMessages(char* pszInfo)
{
	if((g_parchivist)&&(m_pdbConn)&&(m_pdb)
		&&(g_parchivist->m_settings.m_pszMessages)&&(strlen(g_parchivist->m_settings.m_pszMessages)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_parchivist->m_settings.m_pszMessages)&&(strlen(g_parchivist->m_settings.m_pszMessages)))?g_parchivist->m_settings.m_pszMessages:"Messages",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_parchivist->m_settings.m_nAutoPurgeMessageDays*86400) // local time....
			);

//		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
//			LeaveCriticalSection(&m_critSQL);
			return ARCHIVIST_SUCCESS;
		}
//		LeaveCriticalSection(&m_critSQL);
	}
	return ARCHIVIST_ERROR;
}
/*
int CArchivistData::CheckAsRun(char* pszInfo)
{
	if((g_parchivist)&&(m_pdbConn)&&(m_pdb)
		&&(g_parchivist->m_settings.m_pszAsRun)&&(strlen(g_parchivist->m_settings.m_pszAsRun)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_parchivist->m_settings.m_pszAsRun)&&(strlen(g_parchivist->m_settings.m_pszAsRun)))?g_parchivist->m_settings.m_pszAsRun:"AsRun_Log",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_parchivist->m_settings.m_nAutoPurgeAsRunDays*86400) // local time....
			);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return ARCHIVIST_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return ARCHIVIST_ERROR;
}
*/


int CArchivistData::CheckDatabaseMods(char* pszInfo)
{
	if((g_parchivist)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY criterion DESC, mod ASC",  // puts suspend at the top, and orders timestamped mods with oldest first! 
			((g_parchivist->m_settings.m_pszExchange)&&(strlen(g_parchivist->m_settings.m_pszExchange)))?g_parchivist->m_settings.m_pszExchange:"Exchange");

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			bool bFileChanges = false;
			int nReturn = ARCHIVIST_SUCCESS;
			int nIndex = 0;
			while ((!(prs->IsEOF()))&&(!bFileChanges)&&(prs))
			{
				CString szCriterion;
				CString szFlag;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("flag", szFlag);//HARDCODE
					szFlag.TrimLeft(); szFlag.TrimRight();
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
					prs->Close();
					delete prs;
					prs=NULL;
					break;
				}

//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "retrieved %s, %s, %s", szCriterion, szFlag, szMod);    //(Dispatch message)


				if((g_parchivist->m_settings.m_pszSettings)&&(strlen(g_parchivist->m_settings.m_pszSettings)))
				{
					szTemp.Format("DBT_%s",g_parchivist->m_settings.m_pszSettings);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nSettingsMod = nReturn;
					}
				}


				if((g_parchivist->m_settings.m_pszFileTypes)&&(strlen(g_parchivist->m_settings.m_pszFileTypes)))
				{
					szTemp.Format("DBT_%s",g_parchivist->m_settings.m_pszFileTypes);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nFileTypesMod = nReturn;
					}
				}

				if((g_parchivist->m_settings.m_pszQueue)&&(strlen(g_parchivist->m_settings.m_pszQueue)))
				{
					szTemp.Format("DBT_%s",g_parchivist->m_settings.m_pszQueue);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nQueueMod = nReturn;
					}
				} 
				
				if((g_parchivist->m_settings.m_pszMetadata)&&(strlen(g_parchivist->m_settings.m_pszMetadata)))
				{
					szTemp.Format("DBT_%s",g_parchivist->m_settings.m_pszMetadata);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						if(!bFileChanges)
						{
							if(szFlag.GetLength()>0)
							{
								bFileChanges = true;
								m_nMetadataMod = -1;
							}
							else
							{
								nReturn = atoi(szMod);
								if(nReturn>0) m_nMetadataMod = nReturn;
							}
						}
					}
				} 
				
				// get the suspend

				szTemp.Format("Suspend");
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					nReturn = atoi(szMod);
					if(nReturn>0)
					{
						if(!m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Archivist is suspended.");  
							g_parchivist->m_data.SetStatusText(errorstring, CX_STATUS_UNKNOWN);
							g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:suspend", "*** Archivist has been suspended. ***"); // Sleep(50); //(Dispatch message)
							g_parchivist->SendMsg(CX_SENDMSG_INFO, "Archivist:suspend", "Archivist has been suspended");
							m_bProcessSuspended = true;
						}
					}
					else
					{
						if(m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Archivist is running.");  
							g_parchivist->m_data.SetStatusText(errorstring, CX_STATUS_OK);
							g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:resume", "*** Archivist has been resumed. ***");//  Sleep(50); //(Dispatch message)
							g_parchivist->SendMsg(CX_SENDMSG_INFO, "Archivist:resume", "Archivist has been resumed.");
							m_bProcessSuspended = false;
						}
					}
				}

				szTemp.Format("Execute_Backup");
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					bFileChanges = true;
					m_nMetadataMod = -1;
				}


				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			delete prs;
			prs = NULL;
			return nReturn;
		}
	}
	return ARCHIVIST_ERROR;
}



int CArchivistData::CheckMetadataMods(char* pszInfo)
{
	if((g_parchivist)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH]; 
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY criterion DESC, mod ASC",  // puts suspend at the top, and orders timestamped mods with oldest first! 
			((g_parchivist->m_settings.m_pszExchange)&&(strlen(g_parchivist->m_settings.m_pszExchange)))?g_parchivist->m_settings.m_pszExchange:"Exchange");

		bool bFileFound = false;
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			int nReturn = ARCHIVIST_ERROR;
			int nIndex = 0;
			while ((!(prs->IsEOF()))&&(prs)&&(!g_bKillThread))
			{
				if((g_parchivist->m_settings.m_nMaxFilesPerCycle>0)&&(nIndex>g_parchivist->m_settings.m_nMaxFilesPerCycle)) break;
				CString szCriterion;
				CString szFlag;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("flag", szFlag);//HARDCODE
					szFlag.TrimLeft(); szFlag.TrimRight();
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
					prs->Close();
					delete prs;
					prs=NULL;
					break;
				}

//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "retrieved %s, %s, %s", szCriterion, szFlag, szMod);    //(Dispatch message)
				
/*
				// get the suspend  // may as well check for this here anyway.

				szTemp.Format("Suspend");
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					nReturn = atoi(szMod);
					if(nReturn>0)
					{
						if(!m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Archivist is suspended.");  
							g_parchivist->m_data.SetStatusText(errorstring, CX_STATUS_UNKNOWN);
							g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:suspend", "*** Archivist has been suspended. ***"); // Sleep(50); //(Dispatch message)
							g_parchivist->SendMsg(CX_SENDMSG_INFO, "Archivist:suspend", "Archivist has been suspended");
							m_bProcessSuspended = true;
						}
					}
					else
					{
						if(m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Archivist is running.");  
							g_parchivist->m_data.SetStatusText(errorstring, CX_STATUS_OK);
							g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:resume", "*** Archivist has been resumed. ***");//  Sleep(50); //(Dispatch message)
							g_parchivist->SendMsg(CX_SENDMSG_INFO, "Archivist:resume", "Archivist has been resumed.");
							m_bProcessSuspended = false;
						}
					}
				}
*/

// deal with pushing the backup data on any single file that has changed.

				// get any backup execution requests.
				szTemp.Format("Execute_Backup");
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					// ok we are not doing the following, instead we created a secondary connection to manage this.
//					prs->Close(); // have to do this in order for this following delete to work.
//					prs = NULL;

					bFileFound = true;

					// this means there is a query in flag, to send to the backup.

					if(g_parchivist->m_settings.m_bUseBackupDatabase)
					{
						ExecuteBackupQuery(szFlag.GetBuffer(1), pszInfo);  // now uses secondary connection
						szFlag.ReleaseBuffer();
					}


					char* pszEncodedQuery = m_pdb2->EncodeQuotes(szFlag);

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE criterion = '%s' AND flag = '%s' and mod = %s",  // puts suspend at the top! 
								((g_parchivist->m_settings.m_pszExchange)&&(strlen(g_parchivist->m_settings.m_pszExchange)))?g_parchivist->m_settings.m_pszExchange:"Exchange",
								szCriterion,
								pszEncodedQuery?pszEncodedQuery:szFlag,
								szMod
							);
					if(pszEncodedQuery) free(pszEncodedQuery);

					//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "SQL %s", szSQL);    //(Dispatch message)
//					if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
					if(m_pdb2)
					{
						if(m_pdb2->ExecuteSQL(m_pdb2Conn, szSQL, errorstring)<DB_SUCCESS)
						{
g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "SQL error: %s", errorstring);    //(Dispatch message)
						}
					}
					else
					{
g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "no secondary connection");    //(Dispatch message)
					}
					
//					nReturn = ARCHIVIST_FILE_EXISTS; // removed this 
//					break;                           // and this. so we can process many records at once, insead of one per call (because we can never catch up)
				}

				if((g_parchivist->m_settings.m_pszMetadata)&&(strlen(g_parchivist->m_settings.m_pszMetadata)))
				{
					szTemp.Format("DBT_%s",g_parchivist->m_settings.m_pszMetadata);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						// ok we are not doing the following, instead we created a secondary connection to manage this.
						//prs->Close(); // have to do this in order for this following delete to work.
						//prs = NULL;
						nReturn = atoi(szMod);

						if(szFlag.GetLength()>0)
						{
							bFileFound = true;
							// a file was put there by the interface.
							// if backup database, have to push it.

							if(g_parchivist->m_settings.m_bUseBackupDatabase)
							{
								PushBackupRecord(szFlag.GetBuffer(1), pszInfo);
								szFlag.ReleaseBuffer();
							}
							char* pszEncodedFile = m_pdb2->EncodeQuotes(szFlag);

							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE criterion = '%s' AND flag = '%s' and mod = %s",  // puts suspend at the top! 
								((g_parchivist->m_settings.m_pszExchange)&&(strlen(g_parchivist->m_settings.m_pszExchange)))?g_parchivist->m_settings.m_pszExchange:"Exchange",
								szCriterion,
								pszEncodedFile?pszEncodedFile:szFlag,
								szMod
							);

							if(pszEncodedFile) free(pszEncodedFile);

//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "SQL %s", szSQL);    //(Dispatch message)
							//if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
							if(m_pdb2)
							{
								if(m_pdb2->ExecuteSQL(m_pdb2Conn, szSQL, errorstring)<DB_SUCCESS)
								{
g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:remove backup directive", "SQL error: %s", pszInfo);    //(Dispatch message)
								}
							}
							else
							{
g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "no secondary connection");    //(Dispatch message)
							}
							
//						nReturn = ARCHIVIST_FILE_EXISTS; // removed this 
//						break;                           // and this. so we can process many records at once, insead of one per call (because we can never catch up)

						}
						else
						{
						// else its a generic mod, dont remove.  (I dont think there are generic mods)
							if(nReturn>0) m_nMetadataMod = nReturn;
						}
					}
				}

				nIndex++;
				if(prs) prs->MoveNext();
				else break;  /// out of the while.
			}
			if(prs) 
			{
				prs->Close();
				delete prs;
				prs = NULL;
			}
			if(!bFileFound)
				return ARCHIVIST_SUCCESS;
			else
				return nReturn;
		}
	}
	return ARCHIVIST_ERROR;
}

int CArchivistData::GetFileTypes(char* pszInfo)
{
	if((g_parchivist)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_parchivist->m_settings.m_pszFileTypes)&&(strlen(g_parchivist->m_settings.m_pszFileTypes)))?g_parchivist->m_settings.m_pszFileTypes:"FileTypes");

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = ARCHIVIST_ERROR;
			int nIndex = 0;
			int nFileTypeIndex = 0;
			while ((!prs->IsEOF()))
			{
//create table FileTypes (criterion varchar(32) NOT NULL, flag varchar(256) NOT NULL, mod int NOT NULL);
				CString szExt = "";
				CString szDesc = "";
				CString szTemp = "";
				int nID=-1;
				try
				{
					prs->GetFieldValue("criterion", szExt);//HARDCODE
					szExt.TrimLeft(); szExt.TrimRight();
					prs->GetFieldValue("flag", szDesc);//HARDCODE
					szDesc.TrimLeft(); szDesc.TrimRight();
					prs->GetFieldValue("mod", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nID = atoi(szTemp);
					}
				}
				catch( ... )
				{
				}

				nFileTypeIndex = GetFileTypeIndex(szExt.GetBuffer(1));
				szExt.ReleaseBuffer();

				if((nFileTypeIndex<0)&&(szExt.GetLength())) // have to add.
				{

					CArchivistFileTypeObject* pscho = new CArchivistFileTypeObject;
					if(pscho)
					{
						CArchivistFileTypeObject** ppObj = new CArchivistFileTypeObject*[g_parchivist->m_data.m_nNumFileTypes+1];
						if(ppObj)
						{
							int o=0;
							if((g_parchivist->m_data.m_ppFileTypes)&&(g_parchivist->m_data.m_nNumFileTypes>0))
							{
								while(o<g_parchivist->m_data.m_nNumFileTypes)
								{
									ppObj[o] = g_parchivist->m_data.m_ppFileTypes[o];
									o++;
								}
								delete [] g_parchivist->m_data.m_ppFileTypes;

							}
							ppObj[g_parchivist->m_data.m_nNumFileTypes] = pscho;
							g_parchivist->m_data.m_ppFileTypes = ppObj;
							g_parchivist->m_data.m_nNumFileTypes++;

							ppObj[o]->m_pszExt = (char*)malloc(szExt.GetLength()+1); 
							if(ppObj[o]->m_pszExt) sprintf(ppObj[o]->m_pszExt, "%s", szExt);

							if(szDesc.GetLength())
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}

							if(nID>=0) ppObj[o]->m_nID = nID;

							ppObj[o]->m_ulFlags = ARCHIVIST_FLAG_FOUND;
						}
						else
							delete pscho;
					}
				}
				else
				{
					// it's there;
					g_parchivist->m_data.m_ppFileTypes[nFileTypeIndex]->m_ulFlags = ARCHIVIST_FLAG_FOUND;
					if(szDesc.GetLength())
					{
						if(g_parchivist->m_data.m_ppFileTypes[nFileTypeIndex]->m_pszDesc) free(g_parchivist->m_data.m_ppFileTypes[nFileTypeIndex]->m_pszDesc);
						g_parchivist->m_data.m_ppFileTypes[nFileTypeIndex]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
						if(g_parchivist->m_data.m_ppFileTypes[nFileTypeIndex]->m_pszDesc) sprintf(g_parchivist->m_data.m_ppFileTypes[nFileTypeIndex]->m_pszDesc, "%s", szDesc);
					}
					if(nID>=0) g_parchivist->m_data.m_ppFileTypes[nFileTypeIndex]->m_nID = nID;
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			prs = NULL;

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<g_parchivist->m_data.m_nNumFileTypes)
			{
				if((g_parchivist->m_data.m_ppFileTypes)&&(g_parchivist->m_data.m_ppFileTypes[nIndex]))
				{
					if((g_parchivist->m_data.m_ppFileTypes[nIndex]->m_ulFlags)&ARCHIVIST_FLAG_FOUND)
					{
						(g_parchivist->m_data.m_ppFileTypes[nIndex]->m_ulFlags) &= ~ARCHIVIST_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(g_parchivist->m_data.m_ppFileTypes[nIndex])
						{
							delete g_parchivist->m_data.m_ppFileTypes[nIndex];
							g_parchivist->m_data.m_nNumFileTypes--;

							int nTemp=nIndex;
							while(nTemp<g_parchivist->m_data.m_nNumFileTypes)
							{
								g_parchivist->m_data.m_ppFileTypes[nTemp]=g_parchivist->m_data.m_ppFileTypes[nTemp+1];
								nTemp++;
							}
							g_parchivist->m_data.m_ppFileTypes[nTemp] = NULL;
						} else nIndex++;
					}
				}
				else
					nIndex++;
			}

			return nReturn;
		}
	}
	return ARCHIVIST_ERROR;
}




/*
int CArchivistData::GetChannels(char* pszInfo)
{
	if((g_parchivist)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_parchivist->m_settings.m_pszChannels)&&(strlen(g_parchivist->m_settings.m_pszChannels)))?g_parchivist->m_settings.m_pszChannels:"Channels");

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = ARCHIVIST_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szServer = "";
				CString szDesc = "";
				CString szTemp = "";
				unsigned long ulFlags;   // various flags
				int nListID = -1;
				int nChannelID = -1;
				int nTemp;
				bool bFound = false;
				bool bFlagsFound = false;
				try
				{
					prs->GetFieldValue("ID", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nChannelID = nTemp;
					}
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bFlagsFound = true;
						ulFlags = atol(szTemp);
					}
					prs->GetFieldValue("description", szDesc);//HARDCODE
					prs->GetFieldValue("server", szServer);//HARDCODE
					szServer.TrimLeft(); szServer.TrimRight();
					// **** if servername is >16 chars, need to truncate and flag error somewhere.  message?

					prs->GetFieldValue("listid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nListID = nTemp;
					}
				}
				catch( ... )
				{
				}

				if((g_parchivist->m_data.m_ppChannelObj)&&(g_parchivist->m_data.m_nNumChannelObjects))
				{
					nTemp=0;
					while(nTemp<g_parchivist->m_data.m_nNumChannelObjects)
					{
						if(g_parchivist->m_data.m_ppChannelObj[nTemp])
						{
							if(
									(szServer.GetLength()>0)
								&&(szServer.CompareNoCase(g_parchivist->m_data.m_ppChannelObj[nTemp]->m_pszServerName)==0)
								&&(nListID==g_parchivist->m_data.m_ppChannelObj[nTemp]->m_nHarrisListID)
								)
							{
								bFound = true;
								// override with the new changes:
								if(nChannelID>=0) g_parchivist->m_data.m_ppChannelObj[nTemp]->m_nChannelID = nChannelID;

								if(
										(szDesc.GetLength()>0)
									&&(szDesc.CompareNoCase(g_parchivist->m_data.m_ppChannelObj[nTemp]->m_pszDesc))
									)
								{
									if(g_parchivist->m_data.m_ppChannelObj[nTemp]->m_pszDesc) free(g_parchivist->m_data.m_ppChannelObj[nTemp]->m_pszDesc);

									g_parchivist->m_data.m_ppChannelObj[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(g_parchivist->m_data.m_ppChannelObj[nTemp]->m_pszDesc) sprintf(g_parchivist->m_data.m_ppChannelObj[nTemp]->m_pszDesc, szDesc);
								}

								if((bFlagsFound)&&(ulFlags&ARCHIVIST_FLAG_ENABLED)&&(!((g_parchivist->m_data.m_ppChannelObj[nTemp]->m_ulFlags)&ARCHIVIST_FLAG_ENABLED)))
								{

// ****  do something here to check out if the list is able to be activated, that is, if it's on a connected connection

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating list: %s...", 
										g_parchivist->m_data.m_ppChannelObj[nTemp]->m_pszDesc?g_parchivist->m_data.m_ppChannelObj[nTemp]->m_pszDesc:g_parchivist->m_data.m_ppChannelObj[nTemp]->m_pszServerName);  
									g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:destination_change", errorstring);    //(Dispatch message)

// **** if it's able to be activated, set the status to connected.
									g_parchivist->m_data.m_ppChannelObj[nTemp]->m_ulStatus = ARCHIVIST_STATUS_CONN;
								}
								

								if((bFlagsFound)&&(!(ulFlags&ARCHIVIST_FLAG_ENABLED))&&((g_parchivist->m_data.m_ppChannelObj[nTemp]->m_ulFlags)&ARCHIVIST_FLAG_ENABLED))
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating list: %s...", 
										g_parchivist->m_data.m_ppChannelObj[nTemp]->m_pszDesc?g_parchivist->m_data.m_ppChannelObj[nTemp]->m_pszDesc:g_parchivist->m_data.m_ppChannelObj[nTemp]->m_pszServerName);  
									g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:destination_change", errorstring);    //(Dispatch message)

									g_parchivist->m_data.m_ppChannelObj[nTemp]->m_ulStatus = ARCHIVIST_STATUS_NOTCON;
								}

								// and set up the flags
								if(bFlagsFound) g_parchivist->m_data.m_ppChannelObj[nTemp]->m_ulFlags = ulFlags|ARCHIVIST_FLAG_FOUND;
								else g_parchivist->m_data.m_ppChannelObj[nTemp]->m_ulFlags |= ARCHIVIST_FLAG_FOUND;
							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szServer.GetLength()>0)&&(nListID>0)) // have to add.
				{

					CArchivistChannelObject* pscho = new CArchivistChannelObject;
					if(pscho)
					{
						CArchivistChannelObject** ppObj = new CArchivistChannelObject*[g_parchivist->m_data.m_nNumChannelObjects+1];
						if(ppObj)
						{
							int o=0;
							if((g_parchivist->m_data.m_ppChannelObj)&&(g_parchivist->m_data.m_nNumChannelObjects>0))
							{
								while(o<g_parchivist->m_data.m_nNumChannelObjects)
								{
									ppObj[o] = g_parchivist->m_data.m_ppChannelObj[o];
									o++;
								}
								delete [] g_parchivist->m_data.m_ppChannelObj;

							}
							ppObj[g_parchivist->m_data.m_nNumChannelObjects] = pscho;
							g_parchivist->m_data.m_ppChannelObj = ppObj;
							g_parchivist->m_data.m_nNumChannelObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szServer.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, szServer);

							ppObj[o]->m_nHarrisListID = nListID;

							if(nChannelID>=0) ppObj[o]->m_nChannelID = nChannelID;

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, szDesc);
							}
//							ppObj[o]->m_usType = not used;
							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|ARCHIVIST_FLAG_FOUND;
							else ppObj[o]->m_ulFlags |= ARCHIVIST_FLAG_FOUND;
								

// ****  do something here to check out if the list is able to be activated, that is, if it's on a connected connection
							if((ppObj[o]->m_ulFlags)&ARCHIVIST_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating list: %s...", 
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName);  
								g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:destination_change", errorstring);    //(Dispatch message)

// **** if it's able to be activated, set the status to connected.
								ppObj[o]->m_ulStatus = ARCHIVIST_STATUS_CONN;
							}

						}
						else
							delete pscho;
					}
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			prs = NULL;

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<g_parchivist->m_data.m_nNumChannelObjects)
			{
				if((g_parchivist->m_data.m_ppChannelObj)&&(g_parchivist->m_data.m_ppChannelObj[nIndex]))
				{
					if((g_parchivist->m_data.m_ppChannelObj[nIndex]->m_ulFlags)&ARCHIVIST_FLAG_FOUND)
					{
						(g_parchivist->m_data.m_ppChannelObj[nIndex]->m_ulFlags) &= ~ARCHIVIST_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(g_parchivist->m_data.m_ppChannelObj[nIndex])
						{
							if((g_parchivist->m_data.m_ppChannelObj[nIndex]->m_ulFlags)&ARCHIVIST_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating list: %s...", 
									g_parchivist->m_data.m_ppChannelObj[nIndex]->m_pszDesc?g_parchivist->m_data.m_ppChannelObj[nIndex]->m_pszDesc:g_parchivist->m_data.m_ppChannelObj[nIndex]->m_pszServerName);  
								g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:destination_remove", errorstring);    //(Dispatch message)
								g_parchivist->m_data.m_ppChannelObj[nIndex]->m_ulStatus = ARCHIVIST_STATUS_NOTCON;
							}

							delete g_parchivist->m_data.m_ppChannelObj[nIndex];
							g_parchivist->m_data.m_nNumChannelObjects--;

							int nTemp=nIndex;
							while(nTemp<g_parchivist->m_data.m_nNumChannelObjects)
							{
								g_parchivist->m_data.m_ppChannelObj[nTemp]=g_parchivist->m_data.m_ppChannelObj[nTemp+1];
								nTemp++;
							}
							g_parchivist->m_data.m_ppChannelObj[nTemp] = NULL;
						} else nIndex++;
					}
				}
				else
					nIndex++;
			}

			return nReturn;
		}
	}
	return ARCHIVIST_ERROR;
}

int CArchivistData::GetConnections(char* pszInfo)
{
	if((g_parchivist)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s ORDER BY server",  //HARDCODE
			((g_parchivist->m_settings.m_pszConnections)&&(strlen(g_parchivist->m_settings.m_pszConnections)))?g_parchivist->m_settings.m_pszConnections:"Connections");
//		g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "GetConnections");  Sleep(250); //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = ARCHIVIST_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szHost="";
				CString szClient="";
				CString szDesc="";
				CString szTemp;
				unsigned long ulFlags;   // various flags
			//	unsigned short usType;
				int nTemp = -1;
				bool bFlagsFound = false;
				bool bFound = false;
				try
				{
					prs->GetFieldValue("server", szHost);//HARDCODE
					szHost.TrimLeft(); szHost.TrimRight();
					prs->GetFieldValue("description", szDesc);//HARDCODE
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulFlags = atol(szTemp);
					}
					prs->GetFieldValue("client", szClient);//HARDCODE
					szClient.TrimLeft(); szClient.TrimRight();
				}
				catch( ... )
				{
				}

				if((g_parchivist->m_data.m_ppConnObj)&&(g_parchivist->m_data.m_nNumConnectionObjects))
				{
					nTemp=0;
					while(nTemp<g_parchivist->m_data.m_nNumConnectionObjects)
					{
//			g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "c2");  Sleep(250); //(Dispatch message)
						if(g_parchivist->m_data.m_ppConnObj[nTemp])
						{
							if((szHost.GetLength()>0)&&(szHost.CompareNoCase(g_parchivist->m_data.m_ppConnObj[nTemp]->m_pszServerName)==0))
							{
								bFound = true;
//			g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "c3");  Sleep(250); //(Dispatch message)

								if(
										((bFlagsFound)&&(!(ulFlags&ARCHIVIST_FLAG_ENABLED))&&((g_parchivist->m_data.m_ppConnObj[nTemp]->m_ulFlags)&ARCHIVIST_FLAG_ENABLED))
									||(
												(szClient.GetLength()>0)&&(szClient.CompareNoCase(g_parchivist->m_data.m_ppConnObj[nTemp]->m_pszClientName)) // different client name
											&&(
													((bFlagsFound)&&(ulFlags&ARCHIVIST_FLAG_ENABLED))
												||((!bFlagsFound)&&((g_parchivist->m_data.m_ppConnObj[nTemp]->m_ulFlags)&ARCHIVIST_FLAG_ENABLED))
												)
										)
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", 
										g_parchivist->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_parchivist->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_parchivist->m_data.m_ppConnObj[nTemp]->m_pszServerName);  
									g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:destination_change", errorstring);    //(Dispatch message)
//									g_parchivist->m_data.m_ppConnObj[nTemp]->m_pDlg->OnDisconnect();
									//**** disconnect

//									g_adc.DisconnectServer(g_parchivist->m_data.m_ppConnObj[nTemp]->m_pszServerName);
									g_parchivist->m_data.m_ppConnObj[nTemp]->m_bKillConnThread = true;
									//**** should check return value....

									g_parchivist->m_data.m_ppConnObj[nTemp]->m_ulStatus = ARCHIVIST_STATUS_NOTCON;
								}

								if(
										((bFlagsFound)&&(ulFlags&ARCHIVIST_FLAG_ENABLED)&&(!((g_parchivist->m_data.m_ppConnObj[nTemp]->m_ulFlags)&ARCHIVIST_FLAG_ENABLED)))
									||(
												(szClient.GetLength()>0)&&(szClient.CompareNoCase(g_parchivist->m_data.m_ppConnObj[nTemp]->m_pszClientName)) // different client name
											&&(
													((bFlagsFound)&&(ulFlags&ARCHIVIST_FLAG_ENABLED))
												||((!bFlagsFound)&&((g_parchivist->m_data.m_ppConnObj[nTemp]->m_ulFlags)&ARCHIVIST_FLAG_ENABLED))
												)
										)
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s to %s...", 
										g_parchivist->m_data.m_ppConnObj[nTemp]->m_pszClientName?g_parchivist->m_data.m_ppConnObj[nTemp]->m_pszClientName:"Archivist",  
										g_parchivist->m_data.m_ppConnObj[nTemp]->m_pszDesc?g_parchivist->m_data.m_ppConnObj[nTemp]->m_pszDesc:g_parchivist->m_data.m_ppConnObj[nTemp]->m_pszServerName);  
									g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:destination_change", errorstring);    //(Dispatch message)
//									g_parchivist->m_data.m_ppConnObj[nTemp]->m_pDlg->OnButtonConnect();
									//**** if connect set following status

									g_parchivist->m_data.m_ppConnObj[nTemp]->m_bKillConnThread = false;
									g_parchivist->m_data.m_ppConnObj[nTemp]->m_pAPIConn = NULL;
									if(_beginthread(ArchivistConnectionThread, 0, (void*)g_parchivist->m_data.m_ppConnObj[nTemp])==-1)
									{
										//error.

										//**MSG
							
										
									}
									//**** should check return value....

									g_parchivist->m_data.m_ppConnObj[nTemp]->m_ulStatus = ARCHIVIST_STATUS_CONN;
								}

								if(bFlagsFound) g_parchivist->m_data.m_ppConnObj[nTemp]->m_ulFlags = ulFlags|ARCHIVIST_FLAG_FOUND;
								else g_parchivist->m_data.m_ppConnObj[nTemp]->m_ulFlags |= ARCHIVIST_FLAG_FOUND;

							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szHost.GetLength()>0)) // have to add.
				{
//			g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "adding %s", szHost);  Sleep(250); //(Dispatch message)
					CArchivistConnectionObject* pscno = new CArchivistConnectionObject;
					if(pscno)
					{
//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CArchivistConnectionObject** ppObj = new CArchivistConnectionObject*[g_parchivist->m_data.m_nNumConnectionObjects+1];
						if(ppObj)
						{
//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((g_parchivist->m_data.m_ppConnObj)&&(g_parchivist->m_data.m_nNumConnectionObjects>0))
							{
								while(o<g_parchivist->m_data.m_nNumConnectionObjects)
								{
									ppObj[o] = g_parchivist->m_data.m_ppConnObj[o];
									o++;
								}
								delete [] g_parchivist->m_data.m_ppConnObj;

							}
							ppObj[o] = pscno;
							g_parchivist->m_data.m_ppConnObj = ppObj;
							g_parchivist->m_data.m_nNumConnectionObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szHost.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, "%s", szHost);

							if(szClient.GetLength()<=0)
							{
								szClient = "Archivist";
							}

							if(szClient.GetLength()>0)
							{
								ppObj[o]->m_pszClientName = (char*)malloc(szClient.GetLength()+1); 
								if(ppObj[o]->m_pszClientName) sprintf(ppObj[o]->m_pszClientName, "%s", szClient);
							}

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}
//							ppObj[o]->m_usType = not used;
							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|ARCHIVIST_FLAG_FOUND;
							else ppObj[o]->m_ulFlags |= ARCHIVIST_FLAG_FOUND;
								

							if((ppObj[o]->m_ulFlags)&ARCHIVIST_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connecting %s to %s...", 
									((ppObj[o]->m_pszClientName)&&(strlen(ppObj[o]->m_pszClientName)))?ppObj[o]->m_pszClientName:"Archivist",  
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName);  
								g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:destination_change", errorstring);    //(Dispatch message)

								//**** if connect 

								// create a connection object in g_adc.
								// associate that with the new CArchivistConnectionObject;
								// start the thread.
								ppObj[o]->m_bKillConnThread = false;
								ppObj[o]->m_pAPIConn = NULL;
									g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "beginning thread for %s", szHost);  Sleep(250); //(Dispatch message)
								if(_beginthread(ArchivistConnectionThread, 0, (void*)ppObj[o])==-1)
								{
									//error.

									//**MSG
									g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "problem adding %s", szHost);  Sleep(250); //(Dispatch message)

						
									
								}
							//**** should check return value....

								ppObj[o]->m_ulStatus = ARCHIVIST_STATUS_CONN;
							}
						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			prs = NULL;
//			g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			nIndex = 0;
			while(nIndex<g_parchivist->m_data.m_nNumConnectionObjects)
			{
				if((g_parchivist->m_data.m_ppConnObj)&&(g_parchivist->m_data.m_ppConnObj[nIndex]))
				{
					if((g_parchivist->m_data.m_ppConnObj[nIndex]->m_ulFlags)&ARCHIVIST_FLAG_FOUND)
					{
						(g_parchivist->m_data.m_ppConnObj[nIndex]->m_ulFlags) &= ~ARCHIVIST_FLAG_FOUND;
						nIndex++;
					}
					else
					{
						if(g_parchivist->m_data.m_ppConnObj[nIndex])
						{
							if((g_parchivist->m_data.m_ppConnObj[nIndex]->m_ulFlags)&ARCHIVIST_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", 
									g_parchivist->m_data.m_ppConnObj[nIndex]->m_pszDesc?g_parchivist->m_data.m_ppConnObj[nIndex]->m_pszDesc:g_parchivist->m_data.m_ppConnObj[nIndex]->m_pszServerName);  
								g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:destination_remove", errorstring);    //(Dispatch message)
//									g_parchivist->m_data.m_ppConnObj[nTemp]->m_pDlg->OnDisconnect();

								//**** disconnect

								g_parchivist->m_data.m_ppConnObj[nIndex]->m_bKillConnThread = false;
								while(g_parchivist->m_data.m_ppConnObj[nIndex]->m_bConnThreadStarted) Sleep(1);

								//g_adc.DisconnectServer(g_parchivist->m_data.m_ppConnObj[nIndex]->m_pszServerName);
								//**** should check return value....

								g_parchivist->m_data.m_ppConnObj[nIndex]->m_ulStatus = ARCHIVIST_STATUS_NOTCON;
							}

							delete g_parchivist->m_data.m_ppConnObj[nIndex];
							g_parchivist->m_data.m_nNumConnectionObjects--;

							int nTemp=nIndex;
							while(nTemp<g_parchivist->m_data.m_nNumConnectionObjects)
							{
								g_parchivist->m_data.m_ppConnObj[nTemp]=g_parchivist->m_data.m_ppConnObj[nTemp+1];
								nTemp++;
							}
							g_parchivist->m_data.m_ppConnObj[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

//			g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
	}
//			g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)
	return ARCHIVIST_ERROR;
}

*/

int CArchivistData::GetFileTypeIndex(char* pszFileExt)
{

	if((pszFileExt)&&(m_ppFileTypes)&&(m_nNumFileTypes>0))
	{
		int i=0;
		while(i<m_nNumFileTypes)
		{
			if((m_ppFileTypes[i])&&(m_ppFileTypes[i]->m_pszExt)&&(strlen(m_ppFileTypes[i]->m_pszExt)))
			{
//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "conparing %s, %s ", m_ppFileTypes[i]->m_pszExt, pszFileExt); Sleep(100); //(Dispatch message)

				if( stricmp(m_ppFileTypes[i]->m_pszExt, pszFileExt)==0) return i;
			}
			i++;
		}
	}

	return ARCHIVIST_ERROR;
}

int CArchivistData::GetQueue(char* pszInfo)
{
	// following removed to deal with wait mode below.
/*
	if(g_miranda.m_bTransferring)
	{
		// something in progress, must wait.
		return ARCHIVIST_ERROR;
	}
*/
	
	if((g_parchivist)&&(m_pdbConn)&&(m_pdb))
	{

		// let's just get the top (oldest timestamp) item in the queue, and deal with it.

		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE action <> 128 ORDER BY timestamp ASC", //HARDCODE
			((g_parchivist->m_settings.m_pszQueue)&&(strlen(g_parchivist->m_settings.m_pszQueue)))?g_parchivist->m_settings.m_pszQueue:"Queue");

//create table Queue (itemid int identity(1,1) NOT NULL, filename_source varchar(256), filename_dest varchar(256), 
//action int, host varchar(64), timestamp float, username varchar(32));

		CString szFilenameSource = "";
		CString szFilenameDest = "";
		CString szServer = "";
		CString szUsername = "";
		CString szTemp = "";
		int nItemID = -1;
		bool bItemFound = false;
		double dblTimestamp=-57;   // timestamp
		int nAction = -1;
		int nTemp;

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = ARCHIVIST_ERROR;
			int nIndex = 0;
//			while ((!prs->IsEOF()))
			if ((!prs->IsEOF()))  // just do the one record, if there is one
			{
				try
				{
					prs->GetFieldValue("itemid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nItemID = atoi(szTemp);
						bItemFound = true;
					}
					prs->GetFieldValue("filename_source", szFilenameSource);//HARDCODE
					szFilenameSource.TrimLeft(); szFilenameSource.TrimRight();
					prs->GetFieldValue("filename_dest", szFilenameDest);//HARDCODE
					szFilenameDest.TrimLeft(); szFilenameDest.TrimRight();
					prs->GetFieldValue("action", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nAction = nTemp;
					}
					prs->GetFieldValue("host", szServer);//HARDCODE
					szServer.TrimLeft(); szServer.TrimRight();

					prs->GetFieldValue("timestamp", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						dblTimestamp = atof(szTemp);
					}
					prs->GetFieldValue("username", szUsername);//HARDCODE
					szUsername.TrimLeft(); szUsername.TrimRight();
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();

				} 
				catch( ... )
				{
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			prs = NULL;

//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "got %d things from queue", nIndex);   Sleep(250);//(Dispatch message)

			if((nReturn>0)&&(bItemFound))
			{
				// process the request, then remove it from the queue
//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "got item %d from queue", nItemID);   Sleep(250);//(Dispatch message)


				///// process..... queue

/*
Archivist:
Queue action IDs
1  delete file from store
2  delete metadata record
3  delete file from store AND delete metadata record
4  move file (will update path in metadata record)
8  archive - moves file to some path, updates metadata record with archive info (date, etc)
19 purge (removes all files and metadata records for files that have non-registered filetypes)
34 clean (removes all metadata records where the file doesn't exist - not archived, and with an ingest date [means, dont delete a placeholder metadata record in anticipation of a new file coming in] ).

Queue table:
create table Queue (itemid int identity(1,1) NOT NULL, filename_source varchar(256), filename_dest varchar(256), action int, host varchar(64),timestamp int, username varchar(50));

we may not use host at all - but we might when we implement archiving, so I just left it in there for now.  anyway it then matches miranda.  we dont have to use it.

I don't know if I mentioned it, but I need DBT_File_Metadata incrementer working in the Exchange table.  I know I didn't mention that I need a slightly different function with it though.  When a change occurs, I want you to do two things.  In addition to incrementing mod, I want you to put the contents of filename of the record you are changing (in File_Metadata), into flags (in Exchange).  But I dont want you to update the existing DBT_File_Metadata, I just want you to add another. so like:

Exchange:

criterion        	flag     	mod
DBT_File_Metadata	file1.wav	1
DBT_File_Metadata	file2.wav	2
DBT_File_Metadata	file3.oxa	3

etc.

That way I can react to each change without having to download the whole table (obviously for archivist, the table will be large, so tthis matters more than with the live sawp kind of data in the other modules)

if its a different kind of change, like a delete all or something, just leave flag field blank, and I will refresh the whole table.
*/


				char errorstring[DB_ERRORSTRING_LEN];
				if(nAction&1)//  delete file from store
				{
					if(DeleteFile(szFilenameSource))
					{
 						if(g_parchivist->m_settings.m_bLogTransfers) g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:DeleteFile", "Deleted file: %s", szFilenameSource); // Sleep(10);//(Dispatch message)
						if(
							  (g_parchivist->m_settings.m_bUseBackupDestination)
							&&(g_parchivist->m_settings.m_pszDestinationFolderPath)
							&&(strlen(g_parchivist->m_settings.m_pszDestinationFolderPath))
							&&(g_parchivist->m_settings.m_pszBackupDestinationFolderPath)
							&&(strlen(g_parchivist->m_settings.m_pszBackupDestinationFolderPath))
							&&(strcmp(g_parchivist->m_settings.m_pszDestinationFolderPath, g_parchivist->m_settings.m_pszBackupDestinationFolderPath))
							)
						{
							char path_buffer[MAX_PATH+1];

							// get backup path.
							char* pchRoot = szFilenameSource.GetBuffer(1) + strlen(g_parchivist->m_settings.m_pszDestinationFolderPath);
							sprintf(path_buffer, "%s%s", g_parchivist->m_settings.m_pszBackupDestinationFolderPath, pchRoot);
							szFilenameSource.ReleaseBuffer();
							if(DeleteFile(path_buffer))
							{
								if(g_parchivist->m_settings.m_bLogTransfers) g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:BackupDeleteFile", "Deleted file: %s", path_buffer);  // Sleep(10);//(Dispatch message)
							}
							else
							{
 								g_parchivist->m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BackupDeleteFileError", "Couldn't delete file: %s (%d)", path_buffer, GetLastError());  // Sleep(10);//(Dispatch message)
							}
						}
					}
					else
					{
 						g_parchivist->m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DeleteFileError", "Couldn't delete file: %s (%d)", szFilenameSource, GetLastError()); //  Sleep(10);//(Dispatch message)
					}
				}
				if(nAction&2)//  delete metadata record
				{

					int nSlash = szFilenameSource.ReverseFind('\\');
					if(nSlash<0)
					{
						nSlash = szFilenameSource.ReverseFind('/');
					}

					CString szFilename;
					CString szFilepath;
					if(nSlash<0)
					{
						szFilename = szFilenameSource;
						szFilepath = "";
					}
					else
					{
						szFilename = szFilenameSource.Mid(nSlash+1);
						szFilepath = szFilenameSource.Left(nSlash);
					}

					char* pchFilename = m_pdb->EncodeQuotes(szFilename);
					char* pchFilepath = m_pdb->EncodeQuotes(szFilepath);
					
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE sys_filename = '%s' AND sys_filepath = '%s'",  //HARDCODE
						((g_parchivist->m_settings.m_pszMetadata)&&(strlen(g_parchivist->m_settings.m_pszMetadata)))?g_parchivist->m_settings.m_pszMetadata:"File_Metadata",
						pchFilename?pchFilename:szFilename,
						pchFilepath?pchFilepath:szFilepath
						);
					if(pchFilename) free(pchFilename);
					if(pchFilepath) free(pchFilepath);

///				g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "Executing SQL: %s", szSQL);  Sleep(100); //(Dispatch message)

					if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						//**MSG
g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ERROR executing SQL: %s", errorstring); // Sleep(100); //(Dispatch message)

					}
					else
					{
						if(g_parchivist->m_settings.m_bUseBackupDatabase)
						{
							char path_buffer[MAX_PATH+1];
							_snprintf(path_buffer, MAX_PATH, "%s|%s|delete",
								szFilepath,
								szFilename
								);

							PushBackupRecord(path_buffer, errorstring);
						}
					}

				}
//3  delete file from store AND delete metadata record
				if(nAction&4)//  move file (will update path in metadata record)
				{
					// use copy and delete instead of move... just in case one fails we still have the file...
					if(CopyFile(szFilenameSource, szFilenameDest, TRUE))  // true = fail if exists.
					{
						DeleteFile(szFilenameSource);
 						if(g_parchivist->m_settings.m_bLogTransfers) g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:MoveFile", "Moved file: %s to %s", szFilenameSource, szFilenameDest); // Sleep(100);//(Dispatch message)
						// now deal wiht metadata.
						CString szFilename;
						CString szFilepath;
						CString szFilename2;
						CString szFilepath2;
						int nSlash = szFilenameSource.ReverseFind('\\');
						if(nSlash<0)
						{
							szFilename = szFilenameSource;
							szFilepath = "";
						}
						else
						{
							szFilename = szFilenameSource.Mid(nSlash+1);
							szFilepath = szFilenameSource.Left(nSlash);
						}
						nSlash = szFilenameDest.ReverseFind('\\');
						if(nSlash<0)
						{
							szFilename2 = szFilenameDest;
							szFilepath2 = "";
						}
						else
						{
							szFilename2 = szFilenameDest.Mid(nSlash+1);
							szFilepath2 = szFilenameDest.Left(nSlash);
						}

						char* pchFilename = m_pdb->EncodeQuotes(szFilename);
						char* pchFilepath = m_pdb->EncodeQuotes(szFilepath);
						char* pchFilename2 = m_pdb->EncodeQuotes(szFilename2);
						char* pchFilepath2 = m_pdb->EncodeQuotes(szFilepath2);
						
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
sys_filename = '%s', sys_filepath = '%s' \
WHERE sys_filename = '%s' AND sys_filepath = '%s'",  //HARDCODE
							((g_parchivist->m_settings.m_pszMetadata)&&(strlen(g_parchivist->m_settings.m_pszMetadata)))?g_parchivist->m_settings.m_pszMetadata:"File_Metadata",
							pchFilename2?pchFilename2:szFilename2,
							pchFilepath2?pchFilepath2:szFilepath2,
							pchFilename?pchFilename:szFilename,
							pchFilepath?pchFilepath:szFilepath
							);
						if(pchFilename) free(pchFilename);
						if(pchFilepath) free(pchFilepath);
						if(pchFilename2) free(pchFilename2);
						if(pchFilepath2) free(pchFilepath2);

	///				g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "Executing SQL: %s", szSQL);  Sleep(100); //(Dispatch message)
						if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
							//**MSG
g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ERROR executing SQL: %s", errorstring); // Sleep(100); //(Dispatch message)
						}
						else
						{
							if(g_parchivist->m_settings.m_bUseBackupDatabase)
							{
								char path_buffer[MAX_PATH+1];
								_snprintf(path_buffer, MAX_PATH, "%s|%s",
									szFilepath2.GetBuffer(1),
									szFilename2.GetBuffer(1)
									);

								szFilename2.ReleaseBuffer();
								szFilepath2.ReleaseBuffer();

								PushBackupRecord(path_buffer, errorstring);
							}
						}
					}
					else  // file exists, overwrite
					{
						if(g_parchivist->m_settings.m_bOverwriteFiles)
						{
							// use copy and delete instead of move... just in case one fails we still have the file...
							if(CopyFile(szFilenameSource, szFilenameDest, FALSE))  // true = fail if exists.
							{
								DeleteFile(szFilenameSource);
 								if(g_parchivist->m_settings.m_bLogTransfers) g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:MoveFile", "Moved file: %s to %s", szFilenameSource, szFilenameDest); // Sleep(100);//(Dispatch message)
								// now deal wiht metadata.
								CString szFilename;
								CString szFilepath;
								CString szFilename2;
								CString szFilepath2;
								int nSlash = szFilenameSource.ReverseFind('\\');
								if(nSlash<0)
								{
									szFilename = szFilenameSource;
									szFilepath = "";
								}
								else
								{
									szFilename = szFilenameSource.Mid(nSlash+1);
									szFilepath = szFilenameSource.Left(nSlash);
								}
								nSlash = szFilenameDest.ReverseFind('\\');
								if(nSlash<0)
								{
									szFilename2 = szFilenameDest;
									szFilepath2 = "";
								}
								else
								{
									szFilename2 = szFilenameDest.Mid(nSlash+1);
									szFilepath2 = szFilenameDest.Left(nSlash);
								}

								char* pchFilename = m_pdb->EncodeQuotes(szFilename);
								char* pchFilepath = m_pdb->EncodeQuotes(szFilepath);
								char* pchFilename2 = m_pdb->EncodeQuotes(szFilename2);
								char* pchFilepath2 = m_pdb->EncodeQuotes(szFilepath2);
								
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
sys_filename = '%s', sys_filepath = '%s' \
WHERE sys_filename = '%s' AND sys_filepath = '%s'",  //HARDCODE
									((g_parchivist->m_settings.m_pszMetadata)&&(strlen(g_parchivist->m_settings.m_pszMetadata)))?g_parchivist->m_settings.m_pszMetadata:"File_Metadata",
									pchFilename2?pchFilename2:szFilename2,
									pchFilepath2?pchFilepath2:szFilepath2,
									pchFilename?pchFilename:szFilename,
									pchFilepath?pchFilepath:szFilepath
									);
								if(pchFilename) free(pchFilename);
								if(pchFilepath) free(pchFilepath);
								if(pchFilename2) free(pchFilename2);
								if(pchFilepath2) free(pchFilepath2);

			///				g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "Executing SQL: %s", szSQL);  Sleep(100); //(Dispatch message)
								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
								{
									//**MSG
g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ERROR executing SQL: %s", errorstring); // Sleep(100); //(Dispatch message)
								}
								else
								{
									if(g_parchivist->m_settings.m_bUseBackupDatabase)
									{
										char path_buffer[MAX_PATH+1];
										_snprintf(path_buffer, MAX_PATH, "%s|%s",
											szFilepath2.GetBuffer(1),
											szFilename2.GetBuffer(1)
											);

										szFilename2.ReleaseBuffer();
										szFilepath2.ReleaseBuffer();

										PushBackupRecord(path_buffer, errorstring);
									}
								}

							}
							else
							{
 								g_parchivist->m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:MoveFileError", "Could not move file: %s to %s. (%d)", szFilenameSource, szFilenameDest, GetLastError()); // Sleep(100);//(Dispatch message)
							}
						}
						else
						{
 							g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:MoveFileError", "Could not move file: %s to %s.\nThe file already exists at the destination and will not be overwritten.", szFilenameSource, szFilenameDest); // Sleep(100);//(Dispatch message)
						}
					}
				}
				if(nAction&8)//  archive - moves file to some path, updates metadata record with archive info (date, etc)
				{
					// use copy and delete instead of move... just in case one fails we still have the file...

					//we assemble filename dest from source.

					if(szFilenameSource.Find(g_parchivist->m_settings.m_pszDestinationFolderPath)==0)
					{
						szFilenameDest.Format("%s%s",g_parchivist->m_settings.m_pszArchiveDestinationFolderPath, szFilenameSource.Mid(strlen(g_parchivist->m_settings.m_pszDestinationFolderPath)));
					}
					else
					{
						int nSlash = szFilenameSource.ReverseFind('\\');
						if(nSlash<0) nSlash = szFilenameSource.ReverseFind('/');
						if(nSlash<0)
							szFilenameDest.Format("%s%s", g_parchivist->m_settings.m_pszArchiveDestinationFolderPath, szFilenameSource);
						else
							szFilenameDest.Format("%s%s", g_parchivist->m_settings.m_pszArchiveDestinationFolderPath, szFilenameSource.Mid(nSlash));
					}

					if(CopyFile(szFilenameSource, szFilenameDest, TRUE))  // true = fail if exists.
					{
						DeleteFile(szFilenameSource);
 						if(g_parchivist->m_settings.m_bLogTransfers)  g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:ArchiveFile", "Archive file: %s moved to %s", szFilenameSource, szFilenameDest); // Sleep(100);//(Dispatch message)
						// now deal wiht metadata.
						CString szFilename;
						CString szFilepath;
						CString szFilename2;
						CString szFilepath2;
						int nSlash = szFilenameSource.ReverseFind('\\');
						if(nSlash<0)
						{
							szFilename = szFilenameSource;
							szFilepath = "";
						}
						else
						{
							szFilename = szFilenameSource.Mid(nSlash+1);
							szFilepath = szFilenameSource.Left(nSlash);
						}
						nSlash = szFilenameDest.ReverseFind('\\');
						if(nSlash<0)
						{
							szFilename2 = szFilenameDest;
							szFilepath2 = "";
						}
						else
						{
							szFilename2 = szFilenameDest.Mid(nSlash+1);
							szFilepath2 = szFilenameDest.Left(nSlash);
						}

						char* pchFilename = m_pdb->EncodeQuotes(szFilename);
						char* pchFilepath = m_pdb->EncodeQuotes(szFilepath);
						char* pchFilename2 = m_pdb->EncodeQuotes(szFilename2);
						char* pchFilepath2 = m_pdb->EncodeQuotes(szFilepath2);
						
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
sys_filename = '%s', sys_filepath = '%s', sys_file_flags = %d \
WHERE sys_filename = '%s' AND sys_filepath = '%s'",  //HARDCODE
							((g_parchivist->m_settings.m_pszMetadata)&&(strlen(g_parchivist->m_settings.m_pszMetadata)))?g_parchivist->m_settings.m_pszMetadata:"File_Metadata",
							pchFilename2?pchFilename2:szFilename2,
							pchFilepath2?pchFilepath2:szFilepath2,
							ARCHIVIST_FILEFLAG_ARCHIVED,
							pchFilename?pchFilename:szFilename,
							pchFilepath?pchFilepath:szFilepath
							);
						if(pchFilename) free(pchFilename);
						if(pchFilepath) free(pchFilepath);
						if(pchFilename2) free(pchFilename2);
						if(pchFilepath2) free(pchFilepath2);

	///				g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "Executing SQL: %s", szSQL);  Sleep(100); //(Dispatch message)
						if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
							//**MSG
g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ERROR executing SQL: %s", errorstring); // Sleep(100); //(Dispatch message)
						}
						else
						{
							if(g_parchivist->m_settings.m_bUseBackupDatabase)
							{
								char path_buffer[MAX_PATH+1];
								_snprintf(path_buffer, MAX_PATH, "%s|%s",
									szFilepath2.GetBuffer(1),
									szFilename2.GetBuffer(1)
									);

								szFilename2.ReleaseBuffer();
								szFilepath2.ReleaseBuffer();

								PushBackupRecord(path_buffer, errorstring);
							}
						}

					}
					else  // file exists, overwrite
					{
						if(g_parchivist->m_settings.m_bOverwriteFiles)
						{
							// use copy and delete instead of move... just in case one fails we still have the file...
							if(CopyFile(szFilenameSource, szFilenameDest, FALSE))  // true = fail if exists.
							{
								DeleteFile(szFilenameSource);
 								if(g_parchivist->m_settings.m_bLogTransfers) g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:ArchiveFile", "Archive file: %s moved to %s", szFilenameSource, szFilenameDest);  //Sleep(100);//(Dispatch message)
								// now deal wiht metadata.
								CString szFilename;
								CString szFilepath;
								CString szFilename2;
								CString szFilepath2;
								int nSlash = szFilenameSource.ReverseFind('\\');
								if(nSlash<0)
								{
									szFilename = szFilenameSource;
									szFilepath = "";
								}
								else
								{
									szFilename = szFilenameSource.Mid(nSlash+1);
									szFilepath = szFilenameSource.Left(nSlash);
								}
								nSlash = szFilenameDest.ReverseFind('\\');
								if(nSlash<0)
								{
									szFilename2 = szFilenameDest;
									szFilepath2 = "";
								}
								else
								{
									szFilename2 = szFilenameDest.Mid(nSlash+1);
									szFilepath2 = szFilenameDest.Left(nSlash);
								}

								char* pchFilename = m_pdb->EncodeQuotes(szFilename);
								char* pchFilepath = m_pdb->EncodeQuotes(szFilepath);
								char* pchFilename2 = m_pdb->EncodeQuotes(szFilename2);
								char* pchFilepath2 = m_pdb->EncodeQuotes(szFilepath2);
								
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
sys_filename = '%s', sys_filepath = '%s', sys_file_flags = %d \
WHERE sys_filename = '%s' AND sys_filepath = '%s'",  //HARDCODE
									((g_parchivist->m_settings.m_pszMetadata)&&(strlen(g_parchivist->m_settings.m_pszMetadata)))?g_parchivist->m_settings.m_pszMetadata:"File_Metadata",
									pchFilename2?pchFilename2:szFilename2,
									pchFilepath2?pchFilepath2:szFilepath2,
									ARCHIVIST_FILEFLAG_ARCHIVED,
									pchFilename?pchFilename:szFilename,
									pchFilepath?pchFilepath:szFilepath
									);
								if(pchFilename) free(pchFilename);
								if(pchFilepath) free(pchFilepath);
								if(pchFilename2) free(pchFilename2);
								if(pchFilepath2) free(pchFilepath2);

			///				g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "Executing SQL: %s", szSQL);  Sleep(100); //(Dispatch message)
								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
								{
									//**MSG
g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ERROR executing SQL: %s", errorstring);//  Sleep(100); //(Dispatch message)
								}
								else
								{
									if(g_parchivist->m_settings.m_bUseBackupDatabase)
									{
										char path_buffer[MAX_PATH+1];
										_snprintf(path_buffer, MAX_PATH, "%s|%s",
											szFilepath2.GetBuffer(1),
											szFilename2.GetBuffer(1)
											);

										szFilename2.ReleaseBuffer();
										szFilepath2.ReleaseBuffer();

										PushBackupRecord(path_buffer, errorstring);
									}
								}

							}
							else
							{
 								g_parchivist->m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:ArchiveFileError", "Could not archive file: %s to %s. (%d)", szFilenameSource, szFilenameDest, GetLastError()); // Sleep(100);//(Dispatch message)
							}
						}
						else
						{
 							g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:ArchiveFileError", "Could not archive file: %s to %s.\nThe file already exists at the destination and will not be overwritten.", szFilenameSource, szFilenameDest); // Sleep(100);//(Dispatch message)
						}
					}

				}
				if(nAction&16)// purge (removes all files and metadata records for files that have non-registered filetypes)
				{
					if((m_ppFileTypes)&&(m_nNumFileTypes>0))
					{
						CString szTemp;
						CString szQuery; 
						szQuery.Format("SELECT * FROM %s WHERE sys_file_flags <> %d ",
							((g_parchivist->m_settings.m_pszMetadata)&&(strlen(g_parchivist->m_settings.m_pszMetadata)))?g_parchivist->m_settings.m_pszMetadata:"File_Metadata",
							ARCHIVIST_FILEFLAG_ARCHIVED
							);

						int nFile = 0;
						while(nFile<m_nNumFileTypes)
						{
							if(m_ppFileTypes[nFile])
							{
								char* pchEncodedExt = m_pdb->EncodeQuotes(m_ppFileTypes[nFile]->m_pszExt);
								if((pchEncodedExt)&&(strlen(pchEncodedExt)))
								{
									if(nFile>0)
									{
										szTemp.Format(" AND sys_filename NOT LIKE '%%.%s'", pchEncodedExt);
									}
									else
									{
										szTemp.Format("sys_filename NOT LIKE '%%.%s'", pchEncodedExt);
									}

									szQuery += szTemp;
								}
								if (pchEncodedExt) free(pchEncodedExt);

							}
							nFile++;
						}
						CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szQuery.GetBuffer(1), errorstring);
						szQuery.ReleaseBuffer();

						CString szFilename;
						CString szFilepath;

						while((prs != NULL)&&(!prs->IsEOF()))
						{
							try
							{
								// get id!
								prs->GetFieldValue("sys_filename", szFilename);//HARDCODE
								szFilename.TrimLeft(); szFilename.TrimRight();
								prs->GetFieldValue("sys_filepath", szFilepath);//HARDCODE
								szFilepath.TrimLeft(); szFilepath.TrimRight();
								
								szTemp.Format("%s\\%s", szFilepath, szFilename);
								if(DeleteFile(szTemp))
								{
 									g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:PurgeFile", "Deleted file: %s", szTemp); // Sleep(50);//(Dispatch message)

									if(
											(g_parchivist->m_settings.m_bUseBackupDestination)
										&&(g_parchivist->m_settings.m_pszDestinationFolderPath)
										&&(strlen(g_parchivist->m_settings.m_pszDestinationFolderPath))
										&&(g_parchivist->m_settings.m_pszBackupDestinationFolderPath)
										&&(strlen(g_parchivist->m_settings.m_pszBackupDestinationFolderPath))
										)
									{
										char path_buffer[MAX_PATH+1];

										// get backup path.
										char* pchRoot = szTemp.GetBuffer(1) + strlen(g_parchivist->m_settings.m_pszDestinationFolderPath);
										sprintf(path_buffer, "%s%s", g_parchivist->m_settings.m_pszBackupDestinationFolderPath, pchRoot);
										szTemp.ReleaseBuffer();
										if(DeleteFile(path_buffer))
										{
											if(g_parchivist->m_settings.m_bLogTransfers) g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:BackupPurgeFile", "Deleted file: %s", path_buffer);   //Sleep(10);//(Dispatch message)
										}
										else
										{
 											g_parchivist->m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BackupPurgeFileError", "Couldn't delete file: %s (%d)", path_buffer, GetLastError());   //Sleep(10);//(Dispatch message)
										}
									}
								}
								else
								{
 									g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:PurgeFileError", "Couldn't delete file: %s (%d)", szTemp, GetLastError()); //  Sleep(50);//(Dispatch message)
								}

							}
							catch(CException *e)// CDBException *e, CMemoryException *m)  
							{
								if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
		/*
									if(m_pmsgr)
									{
										char errorstring[DB_ERRORSTRING_LEN];
										_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
										Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
									}
		*/
								}
								else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
								{
									// The error code is in e->m_nRetCode
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
		//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
								}
								else 
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
		//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
								}
								e->Delete();

							} 
							catch( ... )
							{
							}

							prs->MoveNext();
						}
						prs->Close();
						delete prs;
						prs = NULL;
					}
				}
				if(nAction&32)// clean (removes all metadata records where the file doesn't exist - not archived, and with an ingest date [means, dont delete a placeholder metadata record in anticipation of a new file coming in] ).
				{
 					g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:PurgeData", "Purging metadata for nonexistent files..."); // Sleep(50);//(Dispatch message)
					CString szTemp;
					CString szQuery; 
					szQuery.Format("SELECT * FROM %s WHERE sys_ingest_date IS NOT NULL AND sys_ingest_date > 0 AND sys_file_flags <> %d",
						((g_parchivist->m_settings.m_pszMetadata)&&(strlen(g_parchivist->m_settings.m_pszMetadata)))?g_parchivist->m_settings.m_pszMetadata:"File_Metadata",
						ARCHIVIST_FILEFLAG_ARCHIVED
						);

					CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szQuery.GetBuffer(1), errorstring);
					szQuery.ReleaseBuffer();

					CString szFilename;
					CString szFilepath;

					while((prs != NULL)&&(!prs->IsEOF()))
					{
						try
						{
							// get id!
							prs->GetFieldValue("sys_filename", szFilename);//HARDCODE
							szFilename.TrimLeft(); szFilename.TrimRight();
							prs->GetFieldValue("sys_filepath", szFilepath);//HARDCODE
							szFilepath.TrimLeft(); szFilepath.TrimRight();

							szTemp.Format("%s\\%s", szFilepath, szFilename);
							CFileFind cff;
							if(!cff.FindFile(szTemp))  // file wasnt there
							{
								char* pchFilename = m_pdb->EncodeQuotes(szFilename);
								char* pchFilepath = m_pdb->EncodeQuotes(szFilepath);
				
								szQuery.Format("DELETE FROM %s WHERE sys_filename = '%s' AND sys_filepath = '%s'",
									((g_parchivist->m_settings.m_pszMetadata)&&(strlen(g_parchivist->m_settings.m_pszMetadata)))?g_parchivist->m_settings.m_pszMetadata:"File_Metadata",
									pchFilename,
									pchFilepath);

								if(pchFilename) free(pchFilename);
								if(pchFilepath) free(pchFilepath);

								m_pdb->ExecuteSQL(m_pdbConn, szQuery.GetBuffer(1), errorstring);
								szQuery.ReleaseBuffer();

								if(g_parchivist->m_settings.m_bUseBackupDatabase)
								{
									char path_buffer[MAX_PATH+1];
									_snprintf(path_buffer, MAX_PATH, "%s|%s|delete",
										szFilepath.GetBuffer(1),
										szFilename.GetBuffer(1)
										);

									szFilename.ReleaseBuffer();
									szFilepath.ReleaseBuffer();

									PushBackupRecord(path_buffer, errorstring);
								}

							}
							cff.Close();
						}
						catch(CException *e)// CDBException *e, CMemoryException *m)  
						{
							if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
							{
								if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
	/*
								if(m_pmsgr)
								{
									char errorstring[DB_ERRORSTRING_LEN];
									_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
									Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
								}
	*/
							}
							else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
							{
								// The error code is in e->m_nRetCode
								if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
	//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
							}
							else 
							{
								if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
	//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
							}
							e->Delete();
						} 
						catch( ... )
						{
						}

						prs->MoveNext();
					}
					prs->Close();
					delete prs;
					prs = NULL;
				}



				// send remove SQL
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE itemid = %d",  //HARDCODE
					((g_parchivist->m_settings.m_pszQueue)&&(strlen(g_parchivist->m_settings.m_pszQueue)))?g_parchivist->m_settings.m_pszQueue:"Queue",
					nItemID
					);

/*
Archivist:
Queue action IDs
1  delete file from store
2  delete metadata record
3  delete file from store AND delete metadata record
4  move file (will update path in metadata record)
8  archive - moves file to some path, updates metadata record with archive info (date, etc)
19 purge (removes all files and metadata records for files that have non-registered filetypes)
34 clean (removes all metadata records where the file doesn't exist - not archived, and with an ingest date [means, dont delete a placeholder metadata record in anticipation of a new file coming in] ).

Queue table:
create table Queue (itemid int identity(1,1) NOT NULL, filename_source varchar(256), filename_dest varchar(256), 
action int, host varchar(64),timestamp int, username varchar(50));
*/

				if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
				{
					//**MSG
g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

				}
			}

			return nReturn;
		}
	}
	return ARCHIVIST_ERROR;
}

int CArchivistData::InitPending()
{
	if(
		  (m_pdb)
		&&(m_pdbConn)
		&&(g_parchivist)
		&&(g_parchivist->m_settings.m_pszPending)
		&&(strlen(g_parchivist->m_settings.m_pszPending))
		)
	{
		m_pdb->AddTable(m_pdbConn, g_parchivist->m_settings.m_pszPending);
		if(m_pdb->TableExists(m_pdbConn, g_parchivist->m_settings.m_pszPending)==DB_EXISTS)
		{
			// get the schema
			m_pdb->GetTableInfo(m_pdbConn, g_parchivist->m_settings.m_pszPending);
			return ARCHIVIST_SUCCESS;
		}
		else
		{
			char szSQL[DB_SQLSTRING_MAXLEN];

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 

"CREATE TABLE [%s] (\"id\" int identity(1,1) NOT NULL, \
\"file_path\" varchar(256) NOT NULL, \
\"file_name\" varchar(256), \
\"file_DOSname\" varchar(256), \
\"file_flags\" bigint NULL, \
\"file_attribs\" bigint NULL, \
\"file_size_high\" bigint NULL, \
\"file_size_low\" bigint NULL, \
\"file_time_high\" bigint NULL, \
\"file_time_low\" bigint NULL, \
\"entry_timestamp\" bigint NOT NULL, \
\"entry_retries\" int NOT NULL, \
\"entry_backup\" int NOT NULL)",
						g_parchivist->m_settings.m_pszPending?g_parchivist->m_settings.m_pszPending:"Pending"
					);

			if(m_pdb->ExecuteSQL(m_pdbConn, szSQL)>=DB_SUCCESS)
			{
				// get the schema
				m_pdb->GetTableInfo(m_pdbConn, g_parchivist->m_settings.m_pszPending);
				return ARCHIVIST_SUCCESS;
			}
		}
	}
	return ARCHIVIST_ERROR;
}


int CArchivistData::SetPending(CDirEntry* pDirChange, bool bBackupOnly)
{
	if((pDirChange)&&(g_parchivist)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1,
			
"INSERT INTO [%s] (file_path, file_name, file_DOSname, file_flags, file_attribs, file_size_high, file_size_low, file_time_high, file_time_low, entry_timestamp, entry_retries, entry_backup) VALUES \
('%s', '%s', '%s', %ld, %ld, %ld, %ld, %ld, %ld, %ld, 0, %d)",
				g_parchivist->m_settings.m_pszPending?g_parchivist->m_settings.m_pszPending:"Pending",

				pDirChange->m_szFullPath?pDirChange->m_szFullPath:"",  // with no filename
				pDirChange->m_szFilename?pDirChange->m_szFilename:"",  // long filename
				pDirChange->m_szDOSFilename?pDirChange->m_szDOSFilename:"",  //8.3 format
				pDirChange->m_ulFlags,					// type
				pDirChange->m_ulFileAttribs,		// attribs returned by GetFileAttributes
				pDirChange->m_ulFileSizeHigh,				// filesize		DWORD    nFileSizeHigh; 
				pDirChange->m_ulFileSizeLow,				// filesize		DWORD    nFileSizeLow;
				pDirChange->m_ftFileModified.dwHighDateTime,			// unixtime, seconds resolution, last modified
				pDirChange->m_ftFileModified.dwLowDateTime,			// unixtime, seconds resolution, last modified
				(unsigned long)(m_timebTick.time - (m_timebTick.timezone*60) +(m_timebTick.dstflag?3600:0)),
				bBackupOnly?1:0
				);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL)>=DB_SUCCESS)
		{
			return ARCHIVIST_SUCCESS;
		}
	}

	return ARCHIVIST_ERROR;
}

int CArchivistData::GetPending(CDirEntry** ppDirChange, int* pnRetries, bool* pbBackupOnly)
{
	int nRV = ARCHIVIST_ERROR;
	if((ppDirChange)&&(g_parchivist)&&(m_pdbConn)&&(m_pdb))
	{
		*ppDirChange = NULL;
		// let's just get the top (oldest timestamp) item in the table, and deal with it.
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM [%s] WHERE entry_timestamp < %ld ORDER BY entry_timestamp ASC", //HARDCODE
			((g_parchivist->m_settings.m_pszPending)&&(strlen(g_parchivist->m_settings.m_pszPending)))?g_parchivist->m_settings.m_pszPending:"Pending",
			(unsigned long)(m_timebTick.time - (m_timebTick.timezone*60) +(m_timebTick.dstflag?3600:0)) - (g_parchivist->m_settings.m_nWatchfolderInterval/1000)
			);

//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%s", szSQL);  //(Dispatch message)

		int nItemID = -1;
		CString szTemp;

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL);
		if(prs)
		{
//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "got prs");  //(Dispatch message)
			if (!prs->IsEOF())
			{
//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "have item");  //(Dispatch message)
				CDirEntry* pde = new CDirEntry;

				if(pde)
				{
					try
					{
						prs->GetFieldValue("id", szTemp);  //HARDCODE
						nRV = atoi(szTemp);

						prs->GetFieldValue("file_path", szTemp);  //HARDCODE
						pde->m_szFullPath =      (char*)malloc(szTemp.GetLength()+1);     // no filename
						if(pde->m_szFullPath)    sprintf(pde->m_szFullPath, "%s", szTemp );
						
						prs->GetFieldValue("file_name", szTemp);  //HARDCODE
						pde->m_szFilename =      (char*)malloc(szTemp.GetLength()+1);     //  long filename
						if(pde->m_szFilename)    sprintf(pde->m_szFilename, "%s", szTemp );
						
						prs->GetFieldValue("file_DOSname", szTemp);  //HARDCODE
						pde->m_szDOSFilename =      (char*)malloc(szTemp.GetLength()+1);     // 8.3 format
						if(pde->m_szDOSFilename)    sprintf(pde->m_szDOSFilename, "%s", szTemp );
						
						prs->GetFieldValue("file_flags", szTemp);  //HARDCODE
						pde->m_ulFlags          = atol(szTemp);					    // type

						prs->GetFieldValue("file_attribs", szTemp);  //HARDCODE
						pde->m_ulFileAttribs    = atol(szTemp);		    // attribs returned by GetFileAttributes

						prs->GetFieldValue("file_size_high", szTemp);  //HARDCODE
						pde->m_ulFileSizeHigh   = atol(szTemp);				// filesize		DWORD    nFileSizeHigh; 
						
						prs->GetFieldValue("file_size_low", szTemp);  //HARDCODE
						pde->m_ulFileSizeLow    = atol(szTemp);				// filesize		DWORD    nFileSizeLow;

						prs->GetFieldValue("file_time_high", szTemp);  //HARDCODE
						pde->m_ftFileModified.dwHighDateTime  = atol(szTemp);			// unixtime, seconds resolution, last modified

						prs->GetFieldValue("file_time_low", szTemp);  //HARDCODE
						pde->m_ftFileModified.dwLowDateTime  = atol(szTemp);			// unixtime, seconds resolution, last modified
						
						prs->GetFieldValue("entry_timestamp", szTemp);  //HARDCODE
						pde->m_ulEntryTimestamp = atol(szTemp);			// unixtime, seconds resolution, last modified

//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here");  //(Dispatch message)
						if(pnRetries)
						{
							prs->GetFieldValue("entry_retries", szTemp);  //HARDCODE
							*pnRetries = atol(szTemp);			// unixtime, seconds resolution, last modified
						}
						if(pbBackupOnly)
						{
							prs->GetFieldValue("entry_backup", szTemp);  //HARDCODE
							*pbBackupOnly = (atol(szTemp)>0)?true:false;			// unixtime, seconds resolution, last modified
						}
						*ppDirChange = pde;
//g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here2");  //(Dispatch message)
					}
					catch (...)
					{
					}
				}
			}
			prs->Close();
			delete prs;
			prs = NULL;
		}
	}
	return nRV;
}

int CArchivistData::DelPending(int nID)
{
	if((nID>=0)&&(g_parchivist)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM [%s] WHERE id = %d",
				g_parchivist->m_settings.m_pszPending?g_parchivist->m_settings.m_pszPending:"Pending",
				nID
				);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL)>=DB_SUCCESS)
		{
			return ARCHIVIST_SUCCESS;
		}
	}
	return ARCHIVIST_ERROR;
}

int CArchivistData::ModPending(int nID)
{
	if((nID>=0)&&(g_parchivist)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE [%s] SET entry_timestamp = %ld, entry_retries = \
(SELECT case when max(entry_retries) is null then 1 when max(entry_retries) >= %d \
then 1 else max(entry_retries) + 1 end from %s WHERE id = %d) WHERE id = %d",
				g_parchivist->m_settings.m_pszPending?g_parchivist->m_settings.m_pszPending:"Pending",
				(unsigned long)(m_timebTick.time - (m_timebTick.timezone*60) +(m_timebTick.dstflag?3600:0)),
				INT_MAX,
				g_parchivist->m_settings.m_pszPending?g_parchivist->m_settings.m_pszPending:"Pending",
				nID,
				nID
				);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL)>=DB_SUCCESS)
		{
			return ARCHIVIST_SUCCESS;
		}
	}
	return ARCHIVIST_ERROR;
}
