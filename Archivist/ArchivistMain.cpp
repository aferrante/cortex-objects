// ArchivistMain.cpp: implementation of the CArchivistMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "Archivist.h"  // just included to have access to windowing environment
#include "ArchivistDlg.h"  // just included to have access to windowing environment
#include "ArchivistHandler.h"  // just included to have access to windowing environment

#include "ArchivistMain.h"
#include <process.h>
#include <direct.h>
#include <errno.h>
#include "../../Common/TXT/BufferUtil.h"
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"
#include "../../Common/FILE/DirUtil.h"
#include <sys/timeb.h>
#include <time.h>
#include <direct.h>
#include <objsafe.h>
#include <atlbase.h>

// have to add rpcrt4.lib to the linker for UUID stuff

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bThreadStarted=false;
bool g_bKillStatus=false;
CArchivistMain* g_parchivist=NULL;
//CADC g_adc;
CDirUtil	g_dir;				// watch folder utilities.


extern CMessager* g_pmsgr;  // from Messager.cpp
extern CArchivistApp theApp;



//void ArchivistConnectionThread(void* pvArgs);
//void ArchivistListThread(void* pvArgs);


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CArchivistMain::CArchivistMain()
{
}

CArchivistMain::~CArchivistMain()
{
}

int CArchivistMain::DoAutoDeletion( bool bBackup, char* pszInfo )
{
	// need to go out to the database and file cataloged files and delete by parameter:
	// prioritize and keep most recent last used date, most times used, smallest
	// delete least used, oldest, biggest until enough space

	//TODO

	bool bDeletionsDone = false;

	// use metadata to figure out which is the statistical best file to delete.
 	if((m_data.m_pdb)&&(m_data.m_pdbConn)
		&&(m_settings.m_pszMetadata)&&(strlen(m_settings.m_pszMetadata)))
	{
		char errorstring[MAX_MESSAGE_LENGTH];
		char szSQL[DB_SQLSTRING_MAXLEN];
//		CDBconn* pdbBackupConn = NULL;

		// first go for the ones with an expiry date!
		_timeb timestamp;
		_ftime( &timestamp );
		_ftime( &m_data.m_timebTick );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT sys_filename, sys_filepath FROM %s \
WHERE sys_file_flags <> %d AND sys_file_flags <> %d AND sys_ingest_date IS NOT NULL AND sys_ingest_date > 0 \
AND sys_expires_after IS NOT NULL AND sys_expires_after < %d \
ORDER BY sys_file_size DESC, sys_last_modified_on ASC", 
			m_settings.m_pszMetadata,
			ARCHIVIST_FILEFLAG_ARCHIVED,
			ARCHIVIST_FILEFLAG_DELETED,
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)) // local time....
			);

		CRecordset* prs = m_data.m_pdb->Retrieve(m_data.m_pdbConn, szSQL, pszInfo);
		if(prs != NULL) 
		{
			while((!prs->IsEOF())&&(!bDeletionsDone))
			{
				CString szFilename;
				CString szFilepath;
				try
				{
					prs->GetFieldValue("sys_filename", szFilename);
					szFilename.TrimLeft(); szFilename.TrimRight();
					prs->GetFieldValue("sys_filepath", szFilepath);
					szFilepath.TrimLeft(); szFilepath.TrimRight();
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
					}
					e->Delete();
				} 
				catch( ... )
				{
				}

				CString szFilenameTarget;
				bool bDeletionSuccess = false;
				_ftime( &m_data.m_timebTick );
				if(bBackup)
				{
					if(szFilepath.Find(g_parchivist->m_settings.m_pszDestinationFolderPath)==0)
					{
						szFilenameTarget.Format("%s%s%s%s",
							g_parchivist->m_settings.m_pszBackupDestinationFolderPath, 
							szFilepath.Mid(strlen(g_parchivist->m_settings.m_pszDestinationFolderPath)),
							(szFilepath.Mid(strlen(g_parchivist->m_settings.m_pszDestinationFolderPath)).GetLength()>0)?"\\":"",
							szFilename);
					}
					else
					{
						szFilenameTarget.Format("%s%s", g_parchivist->m_settings.m_pszBackupDestinationFolderPath, szFilename);
					}
					if(DeleteFile(szFilenameTarget))
					{
						m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:BackupDiskSpaceDelete", "Deleted %s for backup disk space.", szFilenameTarget); // Sleep(50); //(Dispatch message)
						SendMsg(CX_SENDMSG_INFO, "Archivist:BackupDiskSpaceDelete", "Deleted %s for backup disk space.", szFilenameTarget);
						bDeletionSuccess = true;
					}
					else
					{
						m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BackupDiskSpaceDeleteError", "Could not delete %s on the backup disk. (%d)", szFilenameTarget, GetLastError());//  Sleep(50); //(Dispatch message)
						SendMsg(CX_SENDMSG_ERROR, "Archivist:BackupDiskSpaceDeleteError", "Could not delete %s on the backup disk. (%d)", szFilenameTarget, GetLastError());
					}
				}
				else
				{ // update main record.
					szFilenameTarget.Format("%s\\%s", szFilepath, szFilename); // no trailing \ on filepath, so need to have one
					if(DeleteFile(szFilenameTarget))
					{
						char* pchFilename = m_data.m_pdb->EncodeQuotes(szFilename);
						char* pchFilepath = m_data.m_pdb->EncodeQuotes(szFilepath);
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET sys_file_flags = %d, sys_last_modified_on = %d , sys_last_modified_by = 'sys' \
WHERE sys_filename = '%s', sys_filepath = '%s'",
							m_settings.m_pszMetadata,
							ARCHIVIST_FILEFLAG_DELETED,
							m_data.m_timebTick.time,
							pchFilename?pchFilename:szFilename,
							pchFilepath?pchFilepath:szFilepath
						);
						if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ERROR executing SQL: %s", errorstring); // Sleep(100); //(Dispatch message)
						}
						m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:DiskSpaceDelete", "Deleted %s for disk space.", szFilenameTarget); // Sleep(50); //(Dispatch message)
						SendMsg(CX_SENDMSG_INFO, "Archivist:DiskSpaceDelete", "Deleted %s for disk space.", szFilenameTarget);
						bDeletionSuccess = true;

						if(
								(m_settings.m_bUseBackupDatabase)
//							&&(m_settings.m_pszBackupDSN)
//							&&(strlen(m_settings.m_pszBackupDSN)>0) // has to be valid
//							&&(m_settings.m_pszBackupUser)
//							&&(m_settings.m_pszBackupPW)
//							&&(strcmp(m_settings.m_pszDSN, m_settings.m_pszBackupDSN)!=0)  // different DSN
							)
						{
							//  using a transient connection to DB.  (dont just keep open).
/*
							if(pdbBackupConn==NULL)
							{
								pdbBackupConn = m_data.m_pdb->CreateNewConnection(
									m_settings.m_pszBackupDSN, 
									m_settings.m_pszBackupUser, 
									m_settings.m_pszBackupPW);
								if(pdbBackupConn)
								{
									m_data.m_pdb->ConnectDatabase(pdbBackupConn, pszInfo);
								}
							}
*/
							if(m_data.m_pdbBackup)
//							if((pdbBackupConn)&&(pdbBackupConn->m_bConnected))
							{
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET sys_file_flags = %d, sys_last_modified_on = %d, sys_last_modified_by = 'sys' \
WHERE sys_filename = '%s', sys_filepath = '%s'",
									m_settings.m_pszMetadata,
									ARCHIVIST_FILEFLAG_DELETED,
									m_data.m_timebTick.time,
									pchFilename?pchFilename:szFilename,
									pchFilepath?pchFilepath:szFilepath
								);
	//							if(m_data.m_pdb->ExecuteSQL(pdbBackupConn, szSQL, pszInfo)<DB_SUCCESS)
								if(m_data.m_pdbBackup->ExecuteSQL(m_data.m_pdbBackupConn, szSQL, errorstring)<DB_SUCCESS)
								{
									g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ERROR executing SQL: %s", errorstring); // Sleep(100); //(Dispatch message)
								}
							}
						}
						if(pchFilename) free(pchFilename);
						if(pchFilepath) free(pchFilepath);
					}
					else
					{
						m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DiskSpaceDeleteError", "Could not delete %s. (%d)", szFilenameTarget, GetLastError()); //Sleep(50); //(Dispatch message)
						SendMsg(CX_SENDMSG_ERROR, "Archivist:DiskSpaceDeleteError", "Could not delete %s. (%d)", szFilenameTarget, GetLastError());
					}
				}

				if(bDeletionSuccess)
				{
					// check disk space to set bDeletionsDone
					double dblDiskPercent;

					// we had a prob with this before.
					BOOL bSuccess = FALSE;
					// only check the disk space once every 5 seconds.
					_ftime(&m_data.m_timebTick);

													
					bSuccess = GetDiskFreeSpaceEx( bBackup?m_settings.m_pszBackupDestinationFolderPath:m_settings.m_pszDestinationFolderPath, 
							&m_data.m_uliBytesAvail, 
							&m_data.m_uliBytesTotal,
							&m_data.m_uliBytesFree
 						); 
																	
					if(bSuccess) // make this zero to remove the disk check
					{
						m_data.m_dblAvail = ((double)(m_data.m_uliBytesAvail.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)m_data.m_uliBytesAvail.LowPart;
						m_data.m_dblTotal = ((double)(m_data.m_uliBytesTotal.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)m_data.m_uliBytesTotal.LowPart;
						m_data.m_dblFree	= ((double)(m_data.m_uliBytesFree.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)m_data.m_uliBytesFree.LowPart;
					}

					dblDiskPercent = ((m_data.m_dblTotal-m_data.m_dblAvail)/(m_data.m_dblTotal/100.0));  // == used space

					if(dblDiskPercent<m_settings.m_dblDeletePercentageDiskFull) bDeletionsDone = true;
				}
			}  // while


			prs->Close();
			delete prs;
			prs = NULL;
		}

		if((!bDeletionsDone)&&(m_settings.m_bDirectInstalled))
		{
			// tried the expired ones, now have to try direct usage stats if installed.
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT sys_filename, sys_filepath FROM %s \
WHERE sys_file_flags <> %d AND sys_file_flags <> %d AND sys_ingest_date IS NOT NULL AND sys_ingest_date > 0 \
AND direct_times_used IS NOT NULL AND direct_times_used > 0 \
ORDER BY direct_last_used ASC, direct_times_used ASC, sys_file_size DESC, sys_last_modified_on ASC", 
				m_settings.m_pszMetadata,
				ARCHIVIST_FILEFLAG_ARCHIVED,
				ARCHIVIST_FILEFLAG_DELETED
				);

			CRecordset* prs = m_data.m_pdb->Retrieve(m_data.m_pdbConn, szSQL, pszInfo);
			if(prs != NULL) 
			{
				while((!prs->IsEOF())&&(!bDeletionsDone))
				{
					CString szFilename;
					CString szFilepath;
					try
					{
						prs->GetFieldValue("sys_filename", szFilename);
						szFilename.TrimLeft(); szFilename.TrimRight();
						prs->GetFieldValue("sys_filepath", szFilepath);
						szFilepath.TrimLeft(); szFilepath.TrimRight();
					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
	/*
							if(m_pmsgr)
							{
								char errorstring[DB_ERRORSTRING_LEN];
								_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
								Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
							}
	*/
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							// The error code is in e->m_nRetCode
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
	//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
						}
						else 
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
	//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
						}
						e->Delete();
					} 
					catch( ... )
					{
					}

					CString szFilenameTarget;
					bool bDeletionSuccess = false;
					_ftime( &m_data.m_timebTick );
					if(bBackup)
					{
						if(szFilepath.Find(g_parchivist->m_settings.m_pszDestinationFolderPath)==0)
						{
							szFilenameTarget.Format("%s%s%s%s",
								g_parchivist->m_settings.m_pszBackupDestinationFolderPath, 
								szFilepath.Mid(strlen(g_parchivist->m_settings.m_pszDestinationFolderPath)),
								(szFilepath.Mid(strlen(g_parchivist->m_settings.m_pszDestinationFolderPath)).GetLength()>0)?"\\":"",
								szFilename);
						}
						else
						{
							szFilenameTarget.Format("%s%s", g_parchivist->m_settings.m_pszBackupDestinationFolderPath, szFilename);
						}
						if(DeleteFile(szFilenameTarget))
						{
							m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:BackupDiskSpaceDelete", "Deleted %s for backup disk space.", szFilenameTarget); // Sleep(50); //(Dispatch message)
							SendMsg(CX_SENDMSG_INFO, "Archivist:BackupDiskSpaceDelete", "Deleted %s for backup disk space.", szFilenameTarget);
							bDeletionSuccess = true;
						}
						else
						{
							m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BackupDiskSpaceDeleteError", "Could not delete %s on the backup disk. (%d)", szFilenameTarget, GetLastError()); // Sleep(50); //(Dispatch message)
							SendMsg(CX_SENDMSG_ERROR, "Archivist:BackupDiskSpaceDeleteError", "Could not delete %s on the backup disk. (%d)", szFilenameTarget, GetLastError());
						}
					}
					else
					{ // update main record.
						szFilenameTarget.Format("%s\\%s", szFilepath, szFilename); // no trailing \ on filepath, so need to have one
						if(DeleteFile(szFilenameTarget))
						{
							char* pchFilename = m_data.m_pdb->EncodeQuotes(szFilename);
							char* pchFilepath = m_data.m_pdb->EncodeQuotes(szFilepath);
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET sys_file_flags = %d, sys_last_modified_on = %d, sys_last_modified_by = 'sys' \
WHERE sys_filename = '%s', sys_filepath = '%s'",
								m_settings.m_pszMetadata,
								ARCHIVIST_FILEFLAG_DELETED,
								m_data.m_timebTick.time,
								pchFilename?pchFilename:szFilename,
								pchFilepath?pchFilepath:szFilepath
							);
							if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
							{
g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ERROR executing SQL: %s", errorstring); // Sleep(100); //(Dispatch message)
							}
							m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:DiskSpaceDelete", "Deleted %s for disk space.", szFilenameTarget);//  Sleep(50); //(Dispatch message)
							SendMsg(CX_SENDMSG_INFO, "Archivist:DiskSpaceDelete", "Deleted %s for disk space.", szFilenameTarget);
							bDeletionSuccess = true;

							if(
									(m_settings.m_bUseBackupDatabase)
//								&&(m_settings.m_pszBackupDSN)
//								&&(strlen(m_settings.m_pszBackupDSN)>0) // has to be valid
//								&&(m_settings.m_pszBackupUser)
//								&&(m_settings.m_pszBackupPW)
//								&&(strcmp(m_settings.m_pszDSN, m_settings.m_pszBackupDSN)!=0)  // different DSN
								)
							{
								//  using a transient connection to DB.  (dont just keep open).
/*
								if(pdbBackupConn==NULL)
								{
									pdbBackupConn = m_data.m_pdb->CreateNewConnection(
										m_settings.m_pszBackupDSN, 
										m_settings.m_pszBackupUser, 
										m_settings.m_pszBackupPW);
									if(pdbBackupConn)
									{
										m_data.m_pdb->ConnectDatabase(pdbBackupConn, pszInfo);
									}
								}
*/
								if(m_data.m_pdbBackup)
//								if((pdbBackupConn)&&(pdbBackupConn->m_bConnected))
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET sys_file_flags = %d, sys_last_modified_on = %d, sys_last_modified_by = 'sys' \
WHERE sys_filename = '%s', sys_filepath = '%s'",
										m_settings.m_pszMetadata,
										ARCHIVIST_FILEFLAG_DELETED,
										m_data.m_timebTick.time,
										pchFilename?pchFilename:szFilename,
										pchFilepath?pchFilepath:szFilepath
									);
		//							if(m_data.m_pdb->ExecuteSQL(pdbBackupConn, szSQL, pszInfo)<DB_SUCCESS)
									if(m_data.m_pdbBackup->ExecuteSQL(m_data.m_pdbBackupConn, szSQL, errorstring)<DB_SUCCESS)
									{
g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ERROR executing SQL: %s", errorstring); // Sleep(100); //(Dispatch message)
									}
								}
							}
							if(pchFilename) free(pchFilename);
							if(pchFilepath) free(pchFilepath);
						}
						else
						{
							m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DiskSpaceDeleteError", "Could not delete %s. (%d)", szFilenameTarget, GetLastError()); //Sleep(50); //(Dispatch message)
							SendMsg(CX_SENDMSG_ERROR, "Archivist:DiskSpaceDeleteError", "Could not delete %s. (%d)", szFilenameTarget, GetLastError());
						}
					}

					if(bDeletionSuccess)
					{
						// check disk space to set bDeletionsDone
						double dblDiskPercent;

						// we had a prob with this before.
						BOOL bSuccess = FALSE;
						// only check the disk space once every 5 seconds.
						_ftime(&m_data.m_timebTick);

														
						bSuccess = GetDiskFreeSpaceEx( bBackup?m_settings.m_pszBackupDestinationFolderPath:m_settings.m_pszDestinationFolderPath, 
								&m_data.m_uliBytesAvail, 
								&m_data.m_uliBytesTotal,
								&m_data.m_uliBytesFree
 							); 
																		
						if(bSuccess) // make this zero to remove the disk check
						{
							m_data.m_dblAvail = ((double)(m_data.m_uliBytesAvail.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)m_data.m_uliBytesAvail.LowPart;
							m_data.m_dblTotal = ((double)(m_data.m_uliBytesTotal.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)m_data.m_uliBytesTotal.LowPart;
							m_data.m_dblFree	= ((double)(m_data.m_uliBytesFree.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)m_data.m_uliBytesFree.LowPart;
						}

						dblDiskPercent = ((m_data.m_dblTotal-m_data.m_dblAvail)/(m_data.m_dblTotal/100.0));  // == used space

						if(dblDiskPercent<m_settings.m_dblDeletePercentageDiskFull) bDeletionsDone = true;
					}
				}  // while


				prs->Close();
				delete prs;
				prs = NULL;
			}
		}

		if((!bDeletionsDone)&&(m_settings.m_bAutoDeleteOldest))
		{
				// tried the expired ones, and possibly direct stats, now have to try oldest timestamp.
				//DANGER!

			// OK we are actually using sys_last_modified_on and not sys_file_timestamp. so, less dangerous
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT sys_filename, sys_filepath FROM %s \
WHERE sys_file_flags <> %d AND sys_file_flags <> %d AND sys_ingest_date IS NOT NULL AND sys_ingest_date > 0 \
AND sys_last_modified_on IS NOT NULL \
ORDER BY sys_last_modified_on ASC, sys_file_size DESC", 
				m_settings.m_pszMetadata,
				ARCHIVIST_FILEFLAG_ARCHIVED,
				ARCHIVIST_FILEFLAG_DELETED
				);

			CRecordset* prs = m_data.m_pdb->Retrieve(m_data.m_pdbConn, szSQL, pszInfo);
			if(prs != NULL) 
			{
				while((!prs->IsEOF())&&(!bDeletionsDone))
				{
					CString szFilename;
					CString szFilepath;
					try
					{
						prs->GetFieldValue("sys_filename", szFilename);
						szFilename.TrimLeft(); szFilename.TrimRight();
						prs->GetFieldValue("sys_filepath", szFilepath);
						szFilepath.TrimLeft(); szFilepath.TrimRight();
					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
	/*
							if(m_pmsgr)
							{
								char errorstring[DB_ERRORSTRING_LEN];
								_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
								Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
							}
	*/
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							// The error code is in e->m_nRetCode
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory");
	//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
						}
						else 
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
	//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
						}
						e->Delete();
					} 
					catch( ... )
					{
					}

					CString szFilenameTarget;
					bool bDeletionSuccess = false;
					_ftime( &m_data.m_timebTick );
					if(bBackup)
					{
						if(szFilepath.Find(g_parchivist->m_settings.m_pszDestinationFolderPath)==0)
						{
							szFilenameTarget.Format("%s%s%s%s",
								g_parchivist->m_settings.m_pszBackupDestinationFolderPath, 
								szFilepath.Mid(strlen(g_parchivist->m_settings.m_pszDestinationFolderPath)),
								(szFilepath.Mid(strlen(g_parchivist->m_settings.m_pszDestinationFolderPath)).GetLength()>0)?"\\":"",
								szFilename);
						}
						else
						{
							szFilenameTarget.Format("%s%s", g_parchivist->m_settings.m_pszBackupDestinationFolderPath, szFilename);
						}
						if(DeleteFile(szFilenameTarget))
						{
							m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:BackupDiskSpaceDelete", "Deleted %s for backup disk space.", szFilenameTarget);  //Sleep(50); //(Dispatch message)
							SendMsg(CX_SENDMSG_INFO, "Archivist:BackupDiskSpaceDelete", "Deleted %s for backup disk space.", szFilenameTarget);
							bDeletionSuccess = true;
						}
						else
						{
							m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BackupDiskSpaceDeleteError", "Could not delete %s on the backup disk. (%d)", szFilenameTarget, GetLastError()); // Sleep(50); //(Dispatch message)
							SendMsg(CX_SENDMSG_ERROR, "Archivist:BackupDiskSpaceDeleteError", "Could not delete %s on the backup disk. (%d)", szFilenameTarget, GetLastError());
						}
					}
					else
					{ // update main record.
						szFilenameTarget.Format("%s\\%s", szFilepath, szFilename); // no trailing \ on filepath, so need to have one
						if(DeleteFile(szFilenameTarget))
						{
							char* pchFilename = m_data.m_pdb->EncodeQuotes(szFilename);
							char* pchFilepath = m_data.m_pdb->EncodeQuotes(szFilepath);
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET sys_file_flags = %d, sys_last_modified_on = %d, sys_last_modified_by = 'sys' \
WHERE sys_filename = '%s', sys_filepath = '%s'",
								m_settings.m_pszMetadata,
								ARCHIVIST_FILEFLAG_DELETED,
								m_data.m_timebTick.time,
								pchFilename?pchFilename:szFilename,
								pchFilepath?pchFilepath:szFilepath
							);
							if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
							{
g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ERROR executing SQL: %s", errorstring); // Sleep(100); //(Dispatch message)
							}
							m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:DiskSpaceDelete", "Deleted %s for disk space.", szFilenameTarget); // Sleep(50); //(Dispatch message)
							SendMsg(CX_SENDMSG_INFO, "Archivist:DiskSpaceDelete", "Deleted %s for disk space.", szFilenameTarget);
							bDeletionSuccess = true;

							if(
									(m_settings.m_bUseBackupDatabase)
//								&&(m_settings.m_pszBackupDSN)
//								&&(strlen(m_settings.m_pszBackupDSN)>0) // has to be valid
//								&&(m_settings.m_pszBackupUser)
//								&&(m_settings.m_pszBackupPW)
//								&&(strcmp(m_settings.m_pszDSN, m_settings.m_pszBackupDSN)!=0)  // different DSN
								)
							{
								//  using a transient connection to DB.  (dont just keep open).
/*
								if(pdbBackupConn==NULL)
								{
									pdbBackupConn = m_data.m_pdb->CreateNewConnection(
										m_settings.m_pszBackupDSN, 
										m_settings.m_pszBackupUser, 
										m_settings.m_pszBackupPW);
									if(pdbBackupConn)
									{
										m_data.m_pdb->ConnectDatabase(pdbBackupConn, pszInfo);
									}
								}
*/
								if(m_data.m_pdbBackup)
//								if((pdbBackupConn)&&(pdbBackupConn->m_bConnected))
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET sys_file_flags = %d, sys_last_modified_on = %d, sys_last_modified_by = 'sys' \
WHERE sys_filename = '%s', sys_filepath = '%s'",
										m_settings.m_pszMetadata,
										ARCHIVIST_FILEFLAG_DELETED,
										m_data.m_timebTick.time,
										pchFilename?pchFilename:szFilename,
										pchFilepath?pchFilepath:szFilepath
									);
		//							if(m_data.m_pdb->ExecuteSQL(pdbBackupConn, szSQL, pszInfo)<DB_SUCCESS)
									if(m_data.m_pdbBackup->ExecuteSQL(m_data.m_pdbBackupConn, szSQL, errorstring)<DB_SUCCESS)
									{
g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ERROR executing SQL: %s", errorstring); // Sleep(100); //(Dispatch message)
									}
								}
							}
							if(pchFilename) free(pchFilename);
							if(pchFilepath) free(pchFilepath);

						}
						else
						{
							m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DiskSpaceDeleteError", "Could not delete %s. (%d)", szFilenameTarget, GetLastError()); //Sleep(50); //(Dispatch message)
							SendMsg(CX_SENDMSG_ERROR, "Archivist:DiskSpaceDeleteError", "Could not delete %s. (%d)", szFilenameTarget, GetLastError());
						}
					}

					if(bDeletionSuccess)
					{
						// check disk space to set bDeletionsDone
						double dblDiskPercent;

						// we had a prob with this before.
						BOOL bSuccess = FALSE;
						// only check the disk space once every 5 seconds.
						_ftime(&m_data.m_timebTick);

														
						bSuccess = GetDiskFreeSpaceEx( bBackup?m_settings.m_pszBackupDestinationFolderPath:m_settings.m_pszDestinationFolderPath, 
								&m_data.m_uliBytesAvail, 
								&m_data.m_uliBytesTotal,
								&m_data.m_uliBytesFree
 							); 
																		
						if(bSuccess) // make this zero to remove the disk check
						{
							m_data.m_dblAvail = ((double)(m_data.m_uliBytesAvail.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)m_data.m_uliBytesAvail.LowPart;
							m_data.m_dblTotal = ((double)(m_data.m_uliBytesTotal.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)m_data.m_uliBytesTotal.LowPart;
							m_data.m_dblFree	= ((double)(m_data.m_uliBytesFree.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)m_data.m_uliBytesFree.LowPart;
						}

						dblDiskPercent = ((m_data.m_dblTotal-m_data.m_dblAvail)/(m_data.m_dblTotal/100.0));  // == used space

						if(dblDiskPercent<m_settings.m_dblDeletePercentageDiskFull) bDeletionsDone = true;
					}
				}  // while


				prs->Close();
				delete prs;
				prs = NULL;
			}
		}

//		if((pdbBackupConn)&&(pdbBackupConn->m_bConnected))
//			m_data.m_pdb->DisconnectDatabase(pdbBackupConn);

//		if(pdbBackupConn) delete pdbBackupConn;


		if(bDeletionsDone)
		{
			m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:DiskSpaceDelete", "File deletions have restored the disk utilization to below the auto-deletion level (%.02f%% disk full).\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
							 m_settings.m_dblDeletePercentageDiskFull,
							 m_data.m_dblAvail, 
							 m_data.m_dblAvail/(m_data.m_dblTotal/100.0), 
							 m_data.m_dblFree, 
							 m_data.m_dblFree/(m_data.m_dblTotal/100.0), 
							 m_data.m_dblTotal
							 ); // Sleep(50); //(Dispatch message)
			SendMsg(CX_SENDMSG_INFO, "Archivist:DiskSpaceDelete", "File deletions have restored the disk utilization to below the auto-deletion level (%.02f%% disk full).\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
							 m_settings.m_dblDeletePercentageDiskFull,
							 m_data.m_dblAvail, 
							 m_data.m_dblAvail/(m_data.m_dblTotal/100.0), 
							 m_data.m_dblFree, 
							 m_data.m_dblFree/(m_data.m_dblTotal/100.0), 
							 m_data.m_dblTotal
							 );  //(Dispatch message)
			return ARCHIVIST_SUCCESS;
		}
		else
		{
			m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DiskSpaceDeleteError", "File deletions were unsuccessful. (%d)\nUnable to restore the disk utilization to below the auto-deletion level (%.02f%% disk full).\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
							 GetLastError(),
							 m_settings.m_dblDeletePercentageDiskFull,
							 m_data.m_dblAvail, 
							 m_data.m_dblAvail/(m_data.m_dblTotal/100.0), 
							 m_data.m_dblFree, 
							 m_data.m_dblFree/(m_data.m_dblTotal/100.0), 
							 m_data.m_dblTotal
							 ); // Sleep(50); //(Dispatch message)
			SendMsg(CX_SENDMSG_ERROR, "Archivist:DiskSpaceDeleteError", "File deletions were unsuccessful. (%d)\nUnable to restore the disk utilization to below the auto-deletion level (%.02f%% disk full).\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
							 GetLastError(),
							 m_settings.m_dblDeletePercentageDiskFull,
							 m_data.m_dblAvail, 
							 m_data.m_dblAvail/(m_data.m_dblTotal/100.0), 
							 m_data.m_dblFree, 
							 m_data.m_dblFree/(m_data.m_dblTotal/100.0), 
							 m_data.m_dblTotal
							 );  //(Dispatch message)
		}
	}
	else
	{
		m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DiskSpaceDeleteError", "Could not perform auto-deletion. (%d)", GetLastError());// Sleep(50); //(Dispatch message)
		SendMsg(CX_SENDMSG_ERROR, "Archivist:DiskSpaceDeleteError", "Could not perform auto-deletion. (%d)", GetLastError());
	}

	return ARCHIVIST_ERROR;
}


/*

char*	CArchivistMain::ArchivistTranslate(CHTTPHeader* pHeader, char* pszBuffer)			// apply archivist scripting language
{
	return pszBuffer;
}

int		CArchivistMain::InterpretDirective(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the directive, such as a char buffer.
	return ARCHIVIST_SUCCESS;
}
*/

SOCKET*	CArchivistMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // archivist initiates a request to an object server
{
	CNetData* pdata = new CNetData;

	pdata->m_ucType = ucType;      // defined type - indicates which protocol to use, structure of data

	pdata->m_ucCmd = ucCmd;       // the command byte
	pdata->m_ucSubCmd = ucSubCmd;       // the subcommand byte

	pdata->m_pucData = pucData;
	pdata->m_ulDataLen = ulDataLen;

	SOCKET* ps = NULL; 
	m_net.SendData(pdata, pchHost, usPort, 5000, 0, NET_SND_CMDTOSVR, ps);
//	m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);

	return ps;
}

int		CArchivistMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// archivist replies to an object server after receiving data. (usually ack or nak)
{
	return ARCHIVIST_SUCCESS;
}

int		CArchivistMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// archivist answers a request from an object client
{
	return ARCHIVIST_SUCCESS;
}


void ArchivistMainThread(void* pvArgs)
{
	CArchivistApp* pApp = (CArchivistApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment
	//startup.
	g_bThreadStarted = true;

	//create the main objects.

	CArchivistMain archivist;
	CDBUtil db;
	CDBUtil db2;
	CDBUtil dbbackup;

	archivist.m_data.m_ulFlags &= ~ARCHIVIST_STATUS_THREAD_MASK;
	archivist.m_data.m_ulFlags |= ARCHIVIST_STATUS_THREAD_START;
	archivist.m_data.m_ulFlags &= ~ARCHIVIST_ICON_MASK;
	archivist.m_data.m_ulFlags |= ARCHIVIST_STATUS_UNINIT;

	archivist.m_data.GetHost();

	g_parchivist = &archivist;


	char szSQL[DB_SQLSTRING_MAXLEN];
	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
//	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\Archivist\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}



//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

//		AfxMessageBox( pApp->m_lpCmdLine );

	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// check here for Cortex IP.
			if(strlen(pch))
			{
//		AfxMessageBox( pch );
				if(archivist.m_data.m_bu.CountChar(pch, strlen(pch), ':')>=1)  // must be of form "hostname:cmdport" where hostname can be name or ip
				{
					archivist.m_data.m_pszCortexHost = (char*)malloc(strlen(pch)+1);
					if(archivist.m_data.m_pszCortexHost)
					{
						strcpy(archivist.m_data.m_pszCortexHost, pch);
//		AfxMessageBox( archivist.m_data.m_pszCortexHost );

						char* pchd = strchr(archivist.m_data.m_pszCortexHost, ':');
						if(pchd)
						{
							*pchd = 0;  // null term the host name;
							pchd++;
//		AfxMessageBox( archivist.m_data.m_pszCortexHost );

							char* pchd2 = strchr(pchd, ':');
							if(pchd2)
							{
								*pchd2 = 0;  // null term the cmd port;
								pchd2++;
								if(strlen(pchd2)) archivist.m_data.m_usCortexStatusPort = atoi(pchd2);
							}

							if(strlen(pchd)) archivist.m_data.m_usCortexCommandPort = atoi(pchd);
						}
					}

				}
			}
			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }

	// get settings.
	archivist.m_settings.Settings(true); //read
/*
	char pszFilename[MAX_PATH];

	strcpy(pszFilename, ARCHIVIST_SETTINGS_FILE_DEFAULT);  // archivist settings file

	CFileUtil file;
	file.GetSettings(pszFilename, false); 
	char* pszParams = NULL;
// load up the values on the settings object
	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		archivist.m_settings.m_pszName = file.GetIniString("Main", "Name", "Archivist");
		archivist.m_settings.m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", ARCHIVIST_PORT_CMD);
		archivist.m_settings.m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", ARCHIVIST_PORT_STATUS);

		archivist.m_settings.m_pszLicense = file.GetIniString("License", "Key", "invalid_key");

		// compile license key params
		if(g_parchivist->m_data.m_key.m_pszLicenseString) free(g_parchivist->m_data.m_key.m_pszLicenseString);
		g_parchivist->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(archivist.m_settings.m_pszLicense)+1);
		if(g_parchivist->m_data.m_key.m_pszLicenseString)
		sprintf(g_parchivist->m_data.m_key.m_pszLicenseString, "%s", archivist.m_settings.m_pszLicense);

		g_parchivist->m_data.m_key.InterpretKey();

		if(g_parchivist->m_data.m_key.m_bValid)
		{
			unsigned long i=0;
			while(i<g_parchivist->m_data.m_key.m_ulNumParams)
			{
				if((g_parchivist->m_data.m_key.m_ppszParams)
					&&(g_parchivist->m_data.m_key.m_ppszValues)
					&&(g_parchivist->m_data.m_key.m_ppszParams[i])
					&&(g_parchivist->m_data.m_key.m_ppszValues[i]))
				{
					if(stricmp(g_parchivist->m_data.m_key.m_ppszParams[i], "max")==0)
					{
//						g_parchivist->m_data.m_nMaxLicensedDevices = atoi(g_parchivist->m_data.m_key.m_ppszValues[i]);
					}
				}
				i++;
			}

			if(
					(
				    (!archivist.m_data.m_key.m_bExpires)
					||((archivist.m_data.m_key.m_bExpires)&&(!archivist.m_data.m_key.m_bExpired))
					||((archivist.m_data.m_key.m_bExpires)&&(archivist.m_data.m_key.m_bExpireForgiveness)&&(archivist.m_data.m_key.m_ulExpiryDate+archivist.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
				    (!archivist.m_data.m_key.m_bMachineSpecific)
					||((archivist.m_data.m_key.m_bMachineSpecific)&&(archivist.m_data.m_key.m_bValidMAC))
					)
				)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
				archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_OK);
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_ERROR);
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
			archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_ERROR);
		}

/*
		archivist.m_settings.m_pszIconPath = file.GetIniString("FileServer", "IconPath", "");


		archivist.m_settings.m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
		archivist.m_settings.m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
		archivist.m_settings.m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
		archivist.m_settings.m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
		archivist.m_settings.m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;

		archivist.m_settings.m_bUseDeletionSync = file.GetIniInt("FileHandling", "UseDeletionSync", 0)?true:false; //
		archivist.m_settings.m_bUseBackupDeletionSync = file.GetIniInt("FileHandling", "UseBackupDeletionSync", 0)?true:false; //
		archivist.m_settings.m_bDeleteSourceFileOnTransfer = file.GetIniInt("FileHandling", "DeleteSourceFileOnTransfer", 0)?true:false; //
		archivist.m_settings.m_bUseBackupDestination = file.GetIniInt("FileHandling", "UseBackupDestination", 0)?true:false; // push to backup in addition to dest
		archivist.m_settings.m_bUseBackupDatabase = file.GetIniInt("FileHandling", "UseBackupDatabase", 0)?true:false; // push to backup in addition to dest
		archivist.m_settings.m_bFilterFileTypes = file.GetIniInt("FileHandling", "FilterFileTypes", 0)?true:false; // only transfer the ones that are in the list.
		archivist.m_settings.m_nWatchfolderInterval = file.GetIniInt("FileHandling", "WatchfolderInterval", 60000); // check dir for changes every this many ms
		archivist.m_settings.m_pszWatchFolderPath = file.GetIniString("FileHandling", "WatchFolderPath", "W:\\");  
		archivist.m_settings.m_pszDestinationFolderPath = file.GetIniString("FileHandling", "DestinationFolderPath", "D:\\Media\\");  
		archivist.m_settings.m_pszBackupDestinationFolderPath = file.GetIniString("FileHandling", "BackupDestinationFolderPath", "Q:\\Media\\");  
		archivist.m_settings.m_pszBackupDestinationFolderLocalPath = file.GetIniString("FileHandling", "BackupDestinationFolderLocalPath", "D:\\Media\\");  
		archivist.m_settings.m_pszArchiveDestinationFolderPath = file.GetIniString("FileHandling", "ArchiveDestinationFolderPath", "X:\\Media\\");  
		archivist.m_settings.m_bOverwriteFiles = file.GetIniInt("FileHandling", "OverwriteFiles", 1)?true:false;
		archivist.m_settings.m_bAutoDeleteOldest = file.GetIniInt("FileHandling", "AutoDeleteOldest", 1)?true:false;
		archivist.m_settings.m_nPendingRetries = file.GetIniInt("FileHandling", "PendingRetries", 100);


		archivist.m_settings.m_bDirectInstalled = file.GetIniInt("InstalledModules", "DirectInstalled", 0)?true:false;

		double dblValue;
		pszParams = file.GetIniString("FileHandling", "DeletePercentageDiskFull", "98.0"); 
		if(pszParams)
		{
			if(strlen(pszParams)>0)
			{
				dblValue = atof(pszParams);
				if((dblValue>0.1)&&(dblValue<99.9)) archivist.m_settings.m_dblDeletePercentageDiskFull = dblValue;
			}
			free(pszParams); pszParams=NULL;
		}
		pszParams = file.GetIniString("FileHandling", "WarnPercentageDiskFull", "80.0"); 
		if(pszParams)
		{
			if(strlen(pszParams)>0)
			{
				dblValue = atof(pszParams);
				if((dblValue>0.1)&&(dblValue<99.9)) archivist.m_settings.m_dblWarnPercentageDiskFull = dblValue;
			}
			free(pszParams); pszParams=NULL;
		}

		archivist.m_settings.m_pszBackupServer = file.GetIniString("Database", "BackupDBServer", "");
		archivist.m_settings.m_pszBackupDatabase = file.GetIniString("Database", "BackupDBDefault", archivist.m_settings.m_pszName?archivist.m_settings.m_pszName:"Archivist");  // for pushing to backup db thru primary DSN conn

		archivist.m_settings.m_pszDSN = file.GetIniString("Database", "DSN", archivist.m_settings.m_pszName?archivist.m_settings.m_pszName:"Archivist");
		archivist.m_settings.m_pszUser = file.GetIniString("Database", "DBUser", "sa");
		archivist.m_settings.m_pszPW = file.GetIniString("Database", "DBPassword", "");
		archivist.m_settings.m_pszDatabase = file.GetIniString("Database", "DBDefault", archivist.m_settings.m_pszName?archivist.m_settings.m_pszName:"Archivist");
		
		archivist.m_settings.m_pszBackupDSN = file.GetIniString("Database", "BackupDSN", "ArchivistBackup");
		archivist.m_settings.m_pszBackupUser = file.GetIniString("Database", "BackupDBUser", "sa");
		archivist.m_settings.m_pszBackupPW = file.GetIniString("Database", "BackupDBPassword", "");
		archivist.m_settings.m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings");  // the Settings table name
		archivist.m_settings.m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange");  // the Exchange table name
		archivist.m_settings.m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages");  // the Messages table name
		archivist.m_settings.m_pszMetadata = file.GetIniString("Database", "MetadataTableName", "File_Metadata");  // the Metadata table name
		archivist.m_settings.m_pszFileTypes = file.GetIniString("Database", "FileTypesTableName", "FileTypes");  // the File Types table name
		archivist.m_settings.m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue");  // the Queue table name
		archivist.m_settings.m_pszPending = file.GetIniString("Database", "PendingTableName", "Pending");  // the Pending table name

//		archivist.m_settings.m_pszChannels = file.GetIniString("Database", "ChannelsTableName", "Channels");  // the Channels table name
//		archivist.m_settings.m_pszConnections = file.GetIniString("Database", "ConnectionsTableName", "Connections");  // the Connections table name
//		archivist.m_settings.m_pszLiveEvents = file.GetIniString("Database", "LiveEventsTableName", "Events");  // the LiveEvents table name

		archivist.m_settings.m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds

		if(pszParams) free(pszParams); pszParams=NULL;
	}
	else 
	{
		//report failure
		// have to save up errors in error buffer until we have somewhere to send them.
	}
*/

	bool bUseLog = false;
	bool bUseEmail = false;
	bool bLogNetworkErrors = false;

	if(archivist.m_settings.m_bUseLog)
	{
		bUseLog = archivist.m_settings.m_bUseLog;

		// for logfiles, we need params, and they must be in this format:
		//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
//		pszParams = file.GetIniString("Messager", "LogFileIni", "Archivist|YD||1|");
		int nRegisterCode=0;

		nRegisterCode = archivist.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
			"log", 
			archivist.m_settings.m_pszProcessedFileSpec?archivist.m_settings.m_pszProcessedFileSpec:archivist.m_settings.m_pszFileSpec, 
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
			archivist.m_data.SetStatusText(errorstring, (ARCHIVIST_STATUS_FAIL_LOG|ARCHIVIST_STATUS_ERROR));
		}

//		if(pszParams) free(pszParams); pszParams=NULL;
	}

	archivist.m_msgr.DM(MSG_ICONINFO, NULL, "Archivist", "--------------------------------------------------\n\
-------------- Archivist %s start --------------", ARCHIVIST_CURRENT_VERSION);  //(Dispatch message)


	if(archivist.m_settings.m_bUseEmail)
	{
		bUseEmail = archivist.m_settings.m_bUseEmail;
				// for smtp, we need params, and they must be in this format:
				// SMTP address[: override port (optional)] | distribution lists | from email | subject spec | localhostname | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
				// note on distribution lists:
				// we want to give the option to map message type and severity to different address lists.
				// the distribution lists field format is this:
				// ff:x@x.com,q@q.com;0x:B@b.com
				// the : delimits the type and severity flag specifier from the email address list for that set. 
				// the email addresses are delimited by comma
				// the sets are delimited by ;
				// the delimiters are not allowed in addresses, as described here: http://en.wikipedia.org/wiki/E-mail_address
				// on the type and severity specifiers, it is like this:
				// the subfield is a 2 character hex code, sort of.
				// severity is the first char, possible values are 0,1,2,3,4, for normal, low, medium, high, critical
				// additionally, f means all
				// type is the second char, possible values are 0-8, for the follwing:
				// #define MSG_ICONNONE								0x00000000  // default, nothing
				// #define MSG_ICONQUESTION						0x00000001  // ? icon
				// #define MSG_ICONEXCLAMATION				0x00000002  // ! icon
				// #define MSG_ICONERROR							0x00000003  // X icon
				// #define MSG_ICONSTOP								0x00000004  // stop sign icon
				// #define MSG_ICONHAND								0x00000005  // hand icon
				// #define MSG_ICONINFO								0x00000006  // (i) icon 
				// #define MSG_ICONUSER1							0x00000007  
				// #define MSG_ICONUSER2							0x00000008  
				// ff means send all messages to the address list
				// 00 means send all messages with severity normal and no "icon" to the address list
				// 03 means send all messages with severity normal and error type to the address list
				// 43 means send all messages with severity critical and error type to the address list
				// you can set up different address lists with the same addresses - one email per dispatched message will be sent in the case of overlap.

		int nRegisterCode=0;

		// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
		nRegisterCode = archivist.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
			"email", 
			archivist.m_settings.m_pszProcessedMailSpec?archivist.m_settings.m_pszProcessedMailSpec:archivist.m_settings.m_pszMailSpec,
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
			archivist.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
			archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:smtp_init", errorstring);  //(Dispatch message)
		}
	}
	


//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
//	archivist.m_http.InitializeMessaging(&archivist.m_msgr);
//	archivist.m_net.InitializeMessaging(&archivist.m_msgr);
	if(archivist.m_settings.m_bLogNetworkErrors) 
	{
		bLogNetworkErrors = archivist.m_settings.m_bLogNetworkErrors;
		if(archivist.m_net.InitializeMessaging(&archivist.m_msgr)==0)
		{
			archivist.m_data.m_bNetworkMessagingInitialized=true;
		}
	}

// TODO: here, get the DB and pull any override settings.
	//****
	CDBconn* pdbConn = db.CreateNewConnection(archivist.m_settings.m_pszDSN, archivist.m_settings.m_pszUser, archivist.m_settings.m_pszPW);
	archivist.m_settings.m_pdbConn = pdbConn;
	archivist.m_settings.m_pdb = &db;
	archivist.m_data.m_pdbConn = pdbConn;
	archivist.m_data.m_pdb = &db;
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			archivist.m_data.SetStatusText(errorstring, (ARCHIVIST_STATUS_FAIL_DB|ARCHIVIST_STATUS_ERROR));
			archivist.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Archivist:database_connect", errorstring);  //(Dispatch message)
		}
		else
		{
			if(archivist.m_settings.GetFromDatabase(errorstring)<ARCHIVIST_SUCCESS)
			{
				archivist.m_data.SetStatusText(errorstring, (ARCHIVIST_STATUS_FAIL_DB|ARCHIVIST_STATUS_ERROR));
				archivist.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Archivist:database_get", errorstring);  //(Dispatch message)
			}
			else
			{
				archivist.m_data.m_nLastSettingsMod = archivist.m_data.m_nSettingsMod;
				archivist.m_data.InitPending(); // set up the pending table
				if((!archivist.m_settings.m_bUseEmail)&&(bUseEmail))
				{
					bUseEmail = false;
					// reset it
					archivist.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "Shutting down email functions.");  //(Dispatch message)

//						Sleep(250); // let the message get there.
					archivist.m_msgr.RemoveDestination("email");

				}
				if((!archivist.m_settings.m_bLogNetworkErrors)&&(bLogNetworkErrors))
				{
					// reset it
					archivist.m_msgr.DM(MSG_ICONINFO, NULL, "Cortex", "Shutting down network logging.");  //(Dispatch message)
					if(archivist.m_data.m_bNetworkMessagingInitialized)
					{
						archivist.m_net.UninitializeMessaging();  // void return
						archivist.m_data.m_bNetworkMessagingInitialized=false;
					}

				}
				if((!archivist.m_settings.m_bUseLog)&&(bUseLog))
				{
					// reset it
					archivist.m_msgr.DM(MSG_ICONINFO, NULL, "Archivist", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

					Sleep(250); // let the message get there.
					archivist.m_msgr.RemoveDestination("log");

				}
			}
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", archivist.m_settings.m_pszDSN, archivist.m_settings.m_pszUser, archivist.m_settings.m_pszPW); 
		archivist.m_data.SetStatusText(errorstring, (ARCHIVIST_STATUS_FAIL_DB|ARCHIVIST_STATUS_ERROR));
		archivist.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Archivist:database_init", errorstring);  //(Dispatch message)

		//**MSG
	}

	// create a secondary connection
	CDBconn* pdb2Conn = db2.CreateNewConnection(archivist.m_settings.m_pszDSN, archivist.m_settings.m_pszUser, archivist.m_settings.m_pszPW);
	archivist.m_data.m_pdb2Conn = pdb2Conn;
	archivist.m_data.m_pdb2 = &db2;
	if(pdb2Conn)
	{
		if(db2.ConnectDatabase(pdb2Conn, errorstring)<DB_SUCCESS)
		{
			archivist.m_data.SetStatusText(errorstring, (ARCHIVIST_STATUS_FAIL_DB|ARCHIVIST_STATUS_ERROR));
			archivist.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Archivist:database2_connect", errorstring);  //(Dispatch message)
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create secondary connection for %s:%s:%s", archivist.m_settings.m_pszDSN, archivist.m_settings.m_pszUser, archivist.m_settings.m_pszPW); 

		archivist.m_data.SetStatusText(errorstring, (ARCHIVIST_STATUS_FAIL_DB|ARCHIVIST_STATUS_ERROR));
		archivist.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Archivist:database2_init", errorstring);  //(Dispatch message)

		//**MSG
	}


	// create a backup connection
	CDBconn* pdbBackupConn = dbbackup.CreateNewConnection(archivist.m_settings.m_pszBackupDSN, archivist.m_settings.m_pszBackupUser, archivist.m_settings.m_pszBackupPW);
	archivist.m_data.m_pdbBackup = &dbbackup;
	archivist.m_data.m_pdbBackupConn = pdbBackupConn;
	if(pdbBackupConn)
	{
		if(dbbackup.ConnectDatabase(pdbBackupConn, errorstring)<DB_SUCCESS)
		{
			archivist.m_data.SetStatusText(errorstring, (ARCHIVIST_STATUS_FAIL_DB|ARCHIVIST_STATUS_ERROR));
			archivist.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Archivist:database_backup_connect", errorstring);  //(Dispatch message)
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create backup connection for %s:%s:%s", archivist.m_settings.m_pszBackupDSN, archivist.m_settings.m_pszBackupUser, archivist.m_settings.m_pszBackupPW); 

		archivist.m_data.SetStatusText(errorstring, (ARCHIVIST_STATUS_FAIL_DB|ARCHIVIST_STATUS_ERROR));
		archivist.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Archivist:database_backup_init", errorstring);  //(Dispatch message)

		//**MSG
	}



//init command and status listeners.

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing command server on %d", archivist.m_settings.m_usCommandPort); 
	archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_CMDSVR_START);
	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:command_server_init", errorstring);  //(Dispatch message)

	if(archivist.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = archivist.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "ArchivistCommandServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = ArchivistCommandHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &archivist;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &archivist.m_net;					// pointer to the object with the Message function.


		if(archivist.m_net.StartServer(pServer, &archivist.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_CMDSVR_ERROR);
			archivist.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Archivist:command_server_init", errorstring);  //(Dispatch message)
			archivist.SendMsg(CX_SENDMSG_ERROR, "Archivist:command_server_init", errorstring);
		}
		else
		{
//AfxMessageBox("Q1");
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Command server listening on %d", archivist.m_settings.m_usCommandPort);
			archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_CMDSVR_RUN);
			archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:command_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing XML server on %d", archivist.m_settings.m_usStatusPort); 
	archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_STATUSSVR_START);
	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:xml_server_init", errorstring);  //(Dispatch message)

	if(archivist.m_settings.m_usStatusPort>0)
	{
//AfxMessageBox("Q1");
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = archivist.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "ArchivistXMLServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = ArchivistXMLHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &archivist;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &archivist.m_net;					// pointer to the object with the Message function.

		if(archivist.m_net.StartServer(pServer, &archivist.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_STATUSSVR_ERROR);
			archivist.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Archivist:xml_server_init", errorstring);  //(Dispatch message)
			archivist.SendMsg(CX_SENDMSG_ERROR, "Archivist:xml_server_init", errorstring);
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML server listening on %d", archivist.m_settings.m_usStatusPort);
			archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_STATUSSVR_RUN);
			archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:xml_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

//AfxMessageBox("QQ");

	
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Archivist is initializing watchfolder...");
	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:watchfolder_init", errorstring);  //(Dispatch message)
	archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_THREAD_SPARK);

	// now connect all the registered servers
	// this is the list of servers in the db that get connected


	if(!(archivist.m_settings.m_ulMainMode&ARCHIVIST_MODE_CLONE))
	{
		archivist.m_data.GetFileTypes();
//		archivist.m_data.GetConnections();
//	archivist.m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "X2");  Sleep(250);//(Dispatch message)
//		archivist.m_data.GetChannels();
//	archivist.m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "X3");  Sleep(250);//(Dispatch message)
	}
//	archivist.m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "X4");  Sleep(250);//(Dispatch message)
	
	if((archivist.m_data.m_ulFlags&ARCHIVIST_ICON_MASK) != ARCHIVIST_STATUS_ERROR)
	{
		archivist.m_data.m_ulFlags &= ~ARCHIVIST_ICON_MASK;
		archivist.m_data.m_ulFlags |= ARCHIVIST_STATUS_OK;  // green - we want run to be blue when something in progress
	}


// initialize cortex (make this not persistent for now)
	if((archivist.m_data.m_pszCortexHost)&&(strlen(archivist.m_data.m_pszCortexHost)))
	{
		CNetData* pdata = new CNetData;
		if(pdata)
		{

			pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA;      // defined type - indicates which protocol to use, structure of data

			pdata->m_ucCmd = CX_REQ_HELLO;       // the command byte
			pdata->m_ucSubCmd = 0;       // the subcommand byte

			char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
			if(pch)
			{
				sprintf(pch, "%s:%d:%d:%d:%d:%s", 
					archivist.m_data.m_pszHost,
					archivist.m_settings.m_usCommandPort,
					archivist.m_settings.m_usStatusPort,
					CX_TYPE_PROCESS,
					theApp.m_nPID,
					archivist.m_settings.m_pszName?archivist.m_settings.m_pszName:"Archivist"
					);

				pdata->m_pucData =  (unsigned char*) pch;
				pdata->m_ulDataLen = strlen(pch);
			}

			SOCKET s = NULL; 
	//AfxMessageBox("sending");
	//AfxMessageBox( archivist.m_data.m_pszCortexHost );
	//AfxMessageBox( (char*)pdata->m_pucData );
			if(archivist.m_net.SendData(pdata, archivist.m_data.m_pszCortexHost, archivist.m_data.m_usCortexCommandPort, 5000, 0, NET_SND_CMDTOSVR, &s)>=NET_SUCCESS)
			{
				//send ack
	//			AfxMessageBox("ack");
				archivist.m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);
			}
			else
			{
	//			AfxMessageBox("could not send");
			}
			archivist.m_net.CloseConnection(s);

			delete pdata;

		}
	}

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Archivist main thread running.");  
	archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_THREAD_RUN);
//	archivist.m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", errorstring);  Sleep(250);//(Dispatch message)
	archivist.SendMsg(CX_SENDMSG_INFO, "Archivist:init", "Archivist %s main thread running.", ARCHIVIST_CURRENT_VERSION);

	_timeb timebCheckMods;
	_ftime( &timebCheckMods );

	_ftime( &archivist.m_data.m_watchtime );

///AfxMessageBox("xxxxx");

	char pszPath[MAX_PATH+2]; // just a string for temp paths

	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &archivist.m_data.m_timebTick );
//	archivist.m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "%d.%03d", archivist.m_data.m_timebTick.time, archivist.m_data.m_timebTick.millitm);   Sleep(250);//(Dispatch message)

// monitor changes in exchange db and react.
		if(
					(archivist.m_data.m_timebTick.time > timebCheckMods.time )
				||((archivist.m_data.m_timebTick.time == timebCheckMods.time)&&(archivist.m_data.m_timebTick.millitm >= timebCheckMods.millitm))
				&&(!g_bKillThread)
//				&&(!archivist.m_data.m_bProcessSuspended)
			)
		{
//	archivist.m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "time to check");  Sleep(250);//(Dispatch message)
			timebCheckMods.time = archivist.m_data.m_timebTick.time + archivist.m_settings.m_ulModsIntervalMS/1000; 
			timebCheckMods.millitm = archivist.m_data.m_timebTick.millitm + (unsigned short)(archivist.m_settings.m_ulModsIntervalMS%1000); // fractional second updates
			if(timebCheckMods.millitm>999)
			{
				timebCheckMods.time++;
				timebCheckMods.millitm%=1000;
			}
if(archivist.m_settings.m_ulDebug&ARCHIVIST_DEBUG_EXCHANGE)	
	archivist.m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "checking db connected"); //  Sleep(250);//(Dispatch message)
			if(pdbConn)//&&(pdbConn->m_bConnected))
			{
//AfxMessageBox("x");
if(archivist.m_settings.m_ulDebug&ARCHIVIST_DEBUG_EXCHANGE)	
	archivist.m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "checkmods");  //(Dispatch message)

				strcpy(errorstring, "");

				if(archivist.m_data.CheckDatabaseMods(errorstring)==ARCHIVIST_ERROR)
				{
					if(!archivist.m_data.m_bCheckModsWarningSent)
					{
						archivist.m_msgr.DM(MSG_ICONERROR, NULL, "CheckDatabaseMods", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
						archivist.m_data.m_bCheckModsWarningSent=true;
					}
				}
				else
				{
					if(archivist.m_data.m_bCheckModsWarningSent)
					{
						archivist.m_msgr.DM(MSG_ICONINFO, NULL, "CheckDatabaseMods", "Resumed checking."); // Sleep(50); //(Dispatch message)
					}
					archivist.m_data.m_bCheckModsWarningSent=false;
				}

				if(archivist.m_data.m_timebTick.time > archivist.m_data.m_timebAutoPurge.time + archivist.m_settings.m_nAutoPurgeInterval)
				{
					_ftime(&archivist.m_data.m_timebAutoPurge);
					if(archivist.m_settings.m_nAutoPurgeMessageDays>0)
					{
						if(archivist.m_data.CheckMessages(errorstring)==ARCHIVIST_ERROR)
						{
							if(!archivist.m_data.m_bCheckMsgsWarningSent)
							{
								archivist.m_msgr.DM(MSG_ICONERROR, NULL, "CheckMessages", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								archivist.m_data.m_bCheckMsgsWarningSent=true;
							}
						}
						else
						if(archivist.m_data.m_bCheckMsgsWarningSent)
						{
							archivist.m_msgr.DM(MSG_ICONINFO, NULL, "CheckMessages", "Resumed checking messages for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						archivist.m_data.m_bCheckMsgsWarningSent=false;
					}

	/*
					if(archivist.m_settings.m_nAutoPurgeAsRunDays>0)
					{
						if(archivist.m_data.CheckAsRun(errorstring)==ARCHIVIST_ERROR)
						{
							if(!archivist.m_data.m_bCheckAsRunWarningSent)
							{
								archivist.m_msgr.DM(MSG_ICONERROR, NULL, "CheckAsRun", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								archivist.m_data.m_bCheckAsRunWarningSent=true;
							}
						}
						else
						if(archivist.m_data.m_bCheckMsgsWarningSent)
						{
							archivist.m_msgr.DM(MSG_ICONINFO, NULL, "CheckAsRun", "Resumed checking as-run for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						archivist.m_data.m_bCheckAsRunWarningSent=false;
					}

	*/
				}
				
				if(archivist.m_data.m_nSettingsMod != archivist.m_data.m_nLastSettingsMod)
				{
if(archivist.m_settings.m_ulDebug&ARCHIVIST_DEBUG_EXCHANGE)	
	archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "GetFromDatabase");  //(Dispatch message)
					if(archivist.m_settings.GetFromDatabase()>=ARCHIVIST_SUCCESS)
					{
						archivist.m_data.m_nLastSettingsMod = archivist.m_data.m_nSettingsMod;




						// check for stuff to change

						// network messaging
						if(archivist.m_settings.m_bLogNetworkErrors) 
						{
							if(!archivist.m_data.m_bNetworkMessagingInitialized)
							{
								if(archivist.m_net.InitializeMessaging(&archivist.m_msgr)==0)
								{
									archivist.m_data.m_bNetworkMessagingInitialized=true;
								}
							}
						}
						else
						{
							if(archivist.m_data.m_bNetworkMessagingInitialized)
							{
								archivist.m_net.UninitializeMessaging();  // void return
								archivist.m_data.m_bNetworkMessagingInitialized=false;
							}
						}

						// logging and email messaging:

						if(!archivist.m_settings.m_bUseEmail)
						{
							if(bUseEmail)
							{
								bUseEmail = false;
								// reset it
								archivist.m_msgr.DM(MSG_ICONINFO, NULL, "Archivist", "Shutting down email functions.");  //(Dispatch message)

		//						Sleep(250); // let the message get there.
								archivist.m_msgr.RemoveDestination("email");
							}
						}
						else
						{
							if(!bUseEmail)
							{
								bUseEmail = true;
								int nRegisterCode=0;

								// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
								nRegisterCode = archivist.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
									"email", 
									archivist.m_settings.m_pszProcessedMailSpec?archivist.m_settings.m_pszProcessedMailSpec:archivist.m_settings.m_pszMailSpec,
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
									archivist.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
									archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:smtp_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=archivist.m_msgr.GetDestIndex("email");
								if(nIndex>=0)
								{
									if((archivist.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(archivist.m_settings.m_pszProcessedMailSpec?archivist.m_settings.m_pszProcessedMailSpec:archivist.m_settings.m_pszMailSpec))
									{
										if(strcmp(archivist.m_msgr.m_ppDest[nIndex]->m_pszParams, (archivist.m_settings.m_pszProcessedMailSpec?archivist.m_settings.m_pszProcessedMailSpec:archivist.m_settings.m_pszMailSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = archivist.m_msgr.ModifyDestination(
												"email", 
												archivist.m_settings.m_pszProcessedMailSpec?archivist.m_settings.m_pszProcessedMailSpec:archivist.m_settings.m_pszMailSpec,
												MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify SMTP object!\n code: %d", nRegisterCode); 
												//archivist.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:smtp_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}

						if(!archivist.m_settings.m_bUseLog)
						{
							if(bUseLog)
							{
								bUseLog = false;
								// reset it
								archivist.m_msgr.DM(MSG_ICONINFO, NULL, "Archivist", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

								Sleep(250); // let the message get there.
								archivist.m_msgr.RemoveDestination("log");
							}
						}
						else
						{
							if(!bUseLog)
							{
								bUseLog = true;
								int nRegisterCode=0;

								nRegisterCode = archivist.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
									"log", 
									archivist.m_settings.m_pszProcessedFileSpec?archivist.m_settings.m_pszProcessedFileSpec:archivist.m_settings.m_pszFileSpec, 
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
									archivist.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_LOG|CX_STATUS_ERROR));
									archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:log_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=archivist.m_msgr.GetDestIndex("log");
								if(nIndex>=0)
								{
									if((archivist.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(archivist.m_settings.m_pszProcessedFileSpec?archivist.m_settings.m_pszProcessedFileSpec:archivist.m_settings.m_pszFileSpec))
									{
										if(strcmp(archivist.m_msgr.m_ppDest[nIndex]->m_pszParams, (archivist.m_settings.m_pszProcessedFileSpec?archivist.m_settings.m_pszProcessedFileSpec:archivist.m_settings.m_pszFileSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = archivist.m_msgr.ModifyDestination(
												"log", 
												archivist.m_settings.m_pszProcessedFileSpec?archivist.m_settings.m_pszProcessedFileSpec:archivist.m_settings.m_pszFileSpec,
												MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify log object!\n code: %d", nRegisterCode); 
												//archivist.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:log_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}









					}
				}
				if(archivist.m_data.m_nFileTypesMod != archivist.m_data.m_nLastFileTypesMod)
				{
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "GetFileTypes");  //(Dispatch message)
					if(archivist.m_data.GetFileTypes()>=ARCHIVIST_SUCCESS)
					{
						archivist.m_data.m_nLastFileTypesMod = archivist.m_data.m_nFileTypesMod;
					}
				}
/*
				if(archivist.m_data.m_nMetadataMod != archivist.m_data.m_nLastMetadataMod)
				{
//					if(archivist.m_settings.GetFromDatabase()>=ARCHIVIST_SUCCESS)
					{
						archivist.m_data.m_nLastMetadataMod = archivist.m_data.m_nMetadataMod;
					}
				}

/*
				if(archivist.m_data.m_nConnectionsMod != archivist.m_data.m_nLastConnectionsMod)
				{
//			archivist.m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "getting connections again");   Sleep(250);//(Dispatch message)
					if(archivist.m_data.GetConnections()>=ARCHIVIST_SUCCESS)
					{
						archivist.m_data.m_nLastConnectionsMod = archivist.m_data.m_nConnectionsMod;
					}
				}
				if(archivist.m_data.m_nChannelsMod != archivist.m_data.m_nLastChannelsMod)
				{
//			archivist.m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "getting channels again");   Sleep(250);//(Dispatch message)
					if(archivist.m_data.GetChannels()>=ARCHIVIST_SUCCESS)
					{
						archivist.m_data.m_nLastChannelsMod = archivist.m_data.m_nChannelsMod;
					}
				}
*/
			}

		}


		// following section is outside the mods loop, the Queue is really the main loop.

// following line commented out, because we want to check the Queue all the time, for the presence of anything at all to deal with.
//				if(archivist.m_data.m_nQueueMod != archivist.m_data.m_nLastQueueMod)
		if(
				(!archivist.m_data.m_bProcessSuspended)		
			&&(!g_bKillThread)
			&&(archivist.m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!archivist.m_data.m_key.m_bExpires)
				||((archivist.m_data.m_key.m_bExpires)&&(!archivist.m_data.m_key.m_bExpired))
				||((archivist.m_data.m_key.m_bExpires)&&(archivist.m_data.m_key.m_bExpireForgiveness)&&(archivist.m_data.m_key.m_ulExpiryDate+archivist.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!archivist.m_data.m_key.m_bMachineSpecific)
				||((archivist.m_data.m_key.m_bMachineSpecific)&&(archivist.m_data.m_key.m_bValidMAC))
				)
			)
		{
//			archivist.m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "getting channels again");   Sleep(250);//(Dispatch message)
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "GetQueue");  //(Dispatch message)
			if((!g_bKillThread)&&(archivist.m_data.GetQueue()>=ARCHIVIST_SUCCESS))
			{
				archivist.m_data.m_nLastQueueMod = archivist.m_data.m_nQueueMod;
			}

//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "CheckMetadataMods");  //(Dispatch message)
			if((!g_bKillThread)&&(archivist.m_data.m_nMetadataMod<0))
			{
				if(archivist.m_data.CheckMetadataMods()>=ARCHIVIST_SUCCESS)  // this is effectively a queue too...
					archivist.m_data.m_nMetadataMod = 1;
			}
		}

//AfxMessageBox("zoinks");
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "PeekMessage");  //(Dispatch message)
		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "PumpMessage");  //(Dispatch message)


		if(
			  (archivist.m_settings.m_pszWatchFolderPath)
			&&(!g_bKillThread)
			&&(archivist.m_settings.m_pszDestinationFolderPath)
			&&(!archivist.m_data.m_bProcessSuspended)
			&&(archivist.m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!archivist.m_data.m_key.m_bExpires)
				||((archivist.m_data.m_key.m_bExpires)&&(!archivist.m_data.m_key.m_bExpired))
				||((archivist.m_data.m_key.m_bExpires)&&(archivist.m_data.m_key.m_bExpireForgiveness)&&(archivist.m_data.m_key.m_ulExpiryDate+archivist.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!archivist.m_data.m_key.m_bMachineSpecific)
				||((archivist.m_data.m_key.m_bMachineSpecific)&&(archivist.m_data.m_key.m_bValidMAC))
				)
			)
		{
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "Watch");  //(Dispatch message)

			if(!g_dir.m_bWatchFolder)
			{

				g_dir.m_ptimebIncrementor = &archivist.m_data.m_timebTick;

//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "we arent watching yet");  //(Dispatch message)
				int nReturn = g_dir.SetWatchFolder(archivist.m_settings.m_pszWatchFolderPath);
				if(nReturn<DIRUTIL_SUCCESS)
				{
					// error
_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error setting watchfolder.");  
archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_ERROR);

					if(!archivist.m_data.m_bWatchfolderWarningSent)
					{
						archivist.SendMsg(CX_SENDMSG_ERROR, "Archivist:SetWatchFolder", "SetWatchFolder returned %d with %s", nReturn, archivist.m_settings.m_pszWatchFolderPath?archivist.m_settings.m_pszWatchFolderPath:"(null)");
						archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:SetWatchFolder", "SetWatchFolder returned %d with %s", nReturn, archivist.m_settings.m_pszWatchFolderPath?archivist.m_settings.m_pszWatchFolderPath:"(null)");  // Sleep(100);//(Dispatch message)
//					archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:SetWatchFolder", "SetWatchFolder returned %d with %s", nReturn, archivist.m_settings.m_pszWatchFolderPath?archivist.m_settings.m_pszWatchFolderPath:"(null)");  //(Dispatch message)
					}
					archivist.m_data.m_bWatchfolderWarningSent = true;
				}
				else
				{
//					archivist.SendMsg(1, "Archivist:SetWatchFolder", "SetWatchFolder succeeded with %s", archivist.m_settings.m_pszWatchFolderPath?archivist.m_settings.m_pszWatchFolderPath:"(null)"); 
					archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:SetWatchFolder", "SetWatchFolder succeeded with %s", archivist.m_settings.m_pszWatchFolderPath?archivist.m_settings.m_pszWatchFolderPath:"(null)"); //  Sleep(100);//(Dispatch message)

					if(!g_bKillThread)
					{
						nReturn = g_dir.BeginWatch( (unsigned long)archivist.m_settings.m_nWatchfolderInterval, true);
						if(nReturn<DIRUTIL_SUCCESS)
						{
							// error
							//TODO report
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error beginning watchfolder process.");  
	archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_ERROR);

							if(!archivist.m_data.m_bWatchfolderWarningSent)
							{
								archivist.SendMsg(CX_SENDMSG_ERROR, "Archivist:BeginWatch", "BeginWatch returned %d;  could not begin watchfolder thread on %s.", nReturn, archivist.m_settings.m_pszWatchFolderPath?archivist.m_settings.m_pszWatchFolderPath:"(null)");
								archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BeginWatch", "BeginWatch returned %d;  could not begin watchfolder thread on %s.", nReturn, archivist.m_settings.m_pszWatchFolderPath?archivist.m_settings.m_pszWatchFolderPath:"(null)"); //  Sleep(100);//(Dispatch message)
		//						archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Direct:BeginWatch", "BeginWatch returned %d;  could not begin watchfolder thread on %s.", nReturn, archivist.m_settings.m_pszWatchFolderPath?archivist.m_settings.m_pszWatchFolderPath:"(null)");  //(Dispatch message)
							}
							archivist.m_data.m_bWatchfolderWarningSent = true;
						}
						else
						{
							archivist.m_data.m_bWatchfolderWarningSent = false;

							archivist.SendMsg(CX_SENDMSG_INFO, "Archivist:BeginWatch", "BeginWatch: began watch on %s.", archivist.m_settings.m_pszWatchFolderPath?archivist.m_settings.m_pszWatchFolderPath:"(null)"); 
							archivist.m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:BeginWatch", "BeginWatch: began watch on %s.", archivist.m_settings.m_pszWatchFolderPath?archivist.m_settings.m_pszWatchFolderPath:"(null)");  // Sleep(100);//(Dispatch message)
	//						archivist.m_msgr.DM(MSG_ICONINFO, NULL, "Direct:SetWatchFolder", "SetWatchFolder: began watch on %s", archivist.m_settings.m_pszWatchFolderPath);  //(Dispatch message)

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "watching folder...");  
	archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_OK);

							// subtract half the interval so that the watch folder checks for changes, staggered with us checking the results in the else below.
							archivist.m_data.m_watchtime.time -= archivist.m_settings.m_nWatchfolderInterval/2000; // second resolution is fine.
	/*						watchtime.millitm += (unsigned short)((archivist.m_settings.m_ulWatchIntervalMS%1000)/2); // fractional second updates
							if(watchtime.millitm>999)
							{
								watchtime.time++;
								watchtime.millitm%=1000;
							}
	*/
						}
					}
				}
			}
			else // we are watching!
			if(
					(g_dir.m_bInitialized)// but are we initialized
				&&(!g_dir.m_bAnalyzing) // and we arent currently analyzing the cahnges.
				&&(!g_bKillThread)&&(!archivist.m_data.m_bProcessSuspended)
				)
			{
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "we are watching");  //(Dispatch message)

				int nPendingID = -1;
				int nPendingRetries = -1;
				bool bPendingBackupOnly = false;
				CDirEntry* pDirChange = NULL;

				int nWatchChanges = g_dir.QueryChanges();
				int nPendingChanges = 0;
				if(nWatchChanges<=0)
				{
					// check the pending queue
					nPendingID = archivist.m_data.GetPending(&pDirChange, &nPendingRetries, &bPendingBackupOnly);
					if(nPendingID>=0)
					{
						// we have one.
						if(pDirChange) // we really have one
						{
							nPendingChanges=1;
						}
						else
						{
							// just to be sure
							nPendingID = -1;
							nPendingRetries = -1;
							bPendingBackupOnly = false;
						}
					}
				}

				if((nWatchChanges>0)||(nPendingChanges>0))
				{
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%d watchfolder, %d pending changes found", nWatchChanges, nPendingChanges);  //(Dispatch message)

					// if there's an addition, grab the file path in the inbox, and create
					// the same folder if necessary, copy the file there, delete out of the inbox
					// (dont do a move, in case theres a prob transferring)
					// once the file is on the file server, update the metadata in the database
					int numchanges = 0; // debug only
					_timeb diskchecktime;
					_ftime(&diskchecktime);
					diskchecktime.time-=6;  // init so it goes.

					// live changes we will process all in this inner while loop, but pendings we will 
					// do once per big loop (so we can check for more live changes in between)
					while(
						     (!g_bKillThread)&&(!archivist.m_data.m_bProcessSuspended)
								 &&
								 (
									 (
										 (nWatchChanges>0)
									 &&(g_dir.GetDirChange(&pDirChange)>0)
									 &&(pDirChange)
									 )
									 ||
									 (
										 (nPendingChanges>0)
									 &&(pDirChange)
									 )
								 )
							 )

					{
						nPendingChanges=0;  // reset this
						numchanges++;  // debug only
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "file: %s (%d changes this round)", pDirChange->m_szFilename, numchanges);  Sleep(100);
						_ftime( &archivist.m_data.m_timebTick );
						switch(pDirChange->m_ulFlags&(~WATCH_CHK)) // ignore the check flag
						{
						case WATCH_INIT://		0x00000000  // was there at beginning	
						case WATCH_ADD: //		0x00000001  // was added	
						case WATCH_CHG: //		0x00000003  // was chg	 // need to update the metadata if changed.
							{
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "ADD or INIT: %s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);  //(Dispatch message)
								// if theres a new thing, we have to do one or more things.
								// if it's a new directory, we have to add it in the storage area to mimic
								// if it's a new file, we have to copy it to the storage area.
								// but first we have to check the storage area to see if it's there already,
								// if so, flag it.
								// if not, we copy it.
								// we leave it there so that the backup can also copy it.
								// we check DB later to make sure we have it both places before deleting.
								if((pDirChange->m_szFilename)&&(pDirChange->m_szFullPath))
								{
									if((strcmp(pDirChange->m_szFilename, "."))&&(strcmp(pDirChange->m_szFilename, ".."))) // only if its a real file or dir
									{
										if(pDirChange->m_ulFileAttribs&FILE_ATTRIBUTE_DIRECTORY)
										{
											if(stricmp(archivist.m_settings.m_pszWatchFolderPath, archivist.m_settings.m_pszDestinationFolderPath))  // only do if different
											{
												// mkdir is fine if dir is there already
												sprintf(pszPath, "%s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);
												char* pchRoot = NULL;
												if(strnicmp(pszPath, archivist.m_settings.m_pszWatchFolderPath, strlen(archivist.m_settings.m_pszWatchFolderPath))==0)
												{
													pchRoot = pszPath + strlen(archivist.m_settings.m_pszWatchFolderPath);
													char path_buffer[MAX_PATH+1];
													char dir_buffer[MAX_PATH+1];
													sprintf(path_buffer, "%s%s", archivist.m_settings.m_pszDestinationFolderPath, pchRoot);

													//_mkdir(pszPath);
													pchRoot = strstr(path_buffer, ":\\");  //drive delim.
													int nBegin=0;
													if(pchRoot) nBegin = pchRoot-path_buffer+2;

													// have to make dir.
													// if dir already exists, no problem, _mkdir just returns and we continue
													// so, we can always just call mkdir

//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "before mkdir with %s", path_buffer);  //(Dispatch message)
													strcpy(dir_buffer, path_buffer);

													for (int i=nBegin; i<(int)strlen(path_buffer); i++)
													{
														if((path_buffer[i]=='/')||(path_buffer[i]=='\\'))
														{
															dir_buffer[i] = 0;
															if(strlen(dir_buffer)>0)
															{
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "mkdir with %s", dir_buffer);  //(Dispatch message)

																_mkdir(dir_buffer);
															}

															dir_buffer[i] = '\\';
														}
														else
														if(i==(int)strlen(path_buffer)-1)
														{
															if(strlen(dir_buffer)>0)
															{
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "mkdir with %s", dir_buffer);  //(Dispatch message)

																_mkdir(dir_buffer);
															}
														}
														else
															dir_buffer[i] = path_buffer[i];
													}


												} // correct base folder
											} // watch and destination are not the same

											if( (archivist.m_settings.m_bUseBackupDestination)
												&&(archivist.m_settings.m_pszBackupDestinationFolderPath)
												&&(strlen(archivist.m_settings.m_pszBackupDestinationFolderPath))
												&&(stricmp(archivist.m_settings.m_pszBackupDestinationFolderPath, archivist.m_settings.m_pszDestinationFolderPath))
												&&(stricmp(archivist.m_settings.m_pszWatchFolderPath, archivist.m_settings.m_pszBackupDestinationFolderPath))  // only do if different
												)
											{
												// mkdir is fine if dir is there already
												sprintf(pszPath, "%s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);
												char* pchRoot = NULL;
												if(strnicmp(pszPath, archivist.m_settings.m_pszWatchFolderPath, strlen(archivist.m_settings.m_pszWatchFolderPath))==0)
												{
													pchRoot = pszPath + strlen(archivist.m_settings.m_pszWatchFolderPath);
													char path_buffer[MAX_PATH+1];
													char dir_buffer[MAX_PATH+1];
													sprintf(path_buffer, "%s%s", archivist.m_settings.m_pszBackupDestinationFolderPath, pchRoot);

													//_mkdir(pszPath);
													pchRoot = strstr(path_buffer, ":\\");  //drive delim.
													int nBegin=0;
													if(pchRoot) nBegin = pchRoot-path_buffer+2;

													// have to make dir.
													// if dir already exists, no problem, _mkdir just returns and we continue
													// so, we can always just call mkdir

//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "before mkdir with %s", path_buffer);  //(Dispatch message)
													strcpy(dir_buffer, path_buffer);

													for (int i=nBegin; i<(int)strlen(path_buffer); i++)
													{
														if((path_buffer[i]=='/')||(path_buffer[i]=='\\'))
														{
															dir_buffer[i] = 0;
															if(strlen(dir_buffer)>0)
															{
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "mkdir with %s", dir_buffer);  //(Dispatch message)

																_mkdir(dir_buffer);
															}

															dir_buffer[i] = '\\';
														}
														else
														if(i==(int)strlen(path_buffer)-1)
														{
															if(strlen(dir_buffer)>0)
															{
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "mkdir with %s", dir_buffer);  //(Dispatch message)

																_mkdir(dir_buffer);
															}
														}
														else
															dir_buffer[i] = path_buffer[i];
													}


												} // correct base folder
											} // watch and destination are not the same
										}  // dir
										else  // not a dir
										{
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "not a dir");  //(Dispatch message)
//Sleep(100);
											// if it's a new file, we have to copy it to the storage area.
											// but first we have to check the storage area to see if it's there already,
											// if so, flag it.
											// if not, we copy it.
											// we leave it there so that the backup can also copy it. not true: separate synch copies it over both places
											// we check DB later to make sure we have it both places before deleting. not true.


											bool bTransferFile = false;

											// check the filename
											if(archivist.m_settings.m_bFilterFileTypes)
											{
												sprintf(pszPath, "%s", pDirChange->m_szFilename);
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "file is %s", pszPath); Sleep(100); //(Dispatch message)

												char* pchDot = strrchr(pszPath, '.');
												if(pchDot)
												{
													pchDot++;
													char extension[MAX_PATH];
													sprintf(extension, "%s", pchDot);

													int nFileTypeIndex = archivist.m_data.GetFileTypeIndex(extension);
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "extension is %s, %d ", extension, nFileTypeIndex); Sleep(100); //(Dispatch message)
													if(nFileTypeIndex>=0) bTransferFile = true;
												}
											}
											else
											{
												bTransferFile = true;
											}


											if(bTransferFile)
											{
												bool bPrimaryCopyOK = false;
												bool bBackupCopyOK = false;
												bool bPrimaryPathSame = false;
												bool bBackupPathSame = false;
												// first transfer the file to the actual destination.
												sprintf(pszPath, "%s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "path is %s", pszPath); Sleep(100); //(Dispatch message)
												char* pchRoot = NULL;
												if(strnicmp(pszPath, archivist.m_settings.m_pszWatchFolderPath, strlen(archivist.m_settings.m_pszWatchFolderPath))==0)
												{
													pchRoot = pszPath + strlen(archivist.m_settings.m_pszWatchFolderPath);
													char path_buffer[MAX_PATH+1];
													sprintf(path_buffer, "%s%s", archivist.m_settings.m_pszDestinationFolderPath, pchRoot);

													if(!bPendingBackupOnly)
													{

														// have to copy file, but check space first.
														double dblDiskPercent;

														// we had a prob with this before.
														BOOL bSuccess = FALSE;
														// only check the disk space once every 5 seconds.
														_ftime(&archivist.m_data.m_timebTick);

		//												if( timestamp.time > diskchecktime.time ) 
														{
	//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "about to call disk space %d>%d", (unsigned long)archivist.m_data.m_timebTick.time, (unsigned long)diskchecktime.time);  //(Dispatch message)
															
															bSuccess = GetDiskFreeSpaceEx( archivist.m_settings.m_pszDestinationFolderPath, 
																																	&archivist.m_data.m_uliBytesAvail, 
																																	&archivist.m_data.m_uliBytesTotal,
																																	&archivist.m_data.m_uliBytesFree
 																																); 

														}																	
		//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "called disk space");  //(Dispatch message)
														if(bSuccess) // make this zero to remove the disk check
														{
															diskchecktime.time = (archivist.m_data.m_timebTick.time+5);
		//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "assigned %d = %d",  diskchecktime.time, time(NULL)+5);  //(Dispatch message)

															archivist.m_data.m_dblAvail = ((double)(archivist.m_data.m_uliBytesAvail.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)archivist.m_data.m_uliBytesAvail.LowPart;
															archivist.m_data.m_dblTotal = ((double)(archivist.m_data.m_uliBytesTotal.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)archivist.m_data.m_uliBytesTotal.LowPart;
															archivist.m_data.m_dblFree	= ((double)(archivist.m_data.m_uliBytesFree.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)archivist.m_data.m_uliBytesFree.LowPart;
														}

														dblDiskPercent = ((archivist.m_data.m_dblTotal-archivist.m_data.m_dblAvail)/(archivist.m_data.m_dblTotal/100.0));  // == used space

												/*		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:DiskSpaceCheck", "Disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
																		 archivist.m_data.m_dblAvail, 
																		 archivist.m_data.m_dblAvail/(archivist.m_data.m_dblTotal/100.0), 
																		 archivist.m_data.m_dblFree, 
																		 archivist.m_data.m_dblFree/(archivist.m_data.m_dblTotal/100.0), 
																		 archivist.m_data.m_dblTotal
																		 );  //(Dispatch message)
		*/
		//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here"); Sleep(100); //(Dispatch message)

														if(dblDiskPercent>archivist.m_settings.m_dblDeletePercentageDiskFull)
														{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disk near full!");  
		archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_NOTCON);
		//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here2"); Sleep(100); //(Dispatch message)
															if(!archivist.m_data.m_bDiskDeletionSent)
															{
																archivist.m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Archivist:DiskSpaceCheck", "Disk utilization has reached the auto-deletion level (%.02f%% available free space).  The system must delete older files to continue to function.\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
																				 (100.0-archivist.m_settings.m_dblDeletePercentageDiskFull), 
																				 archivist.m_data.m_dblAvail, 
																				 archivist.m_data.m_dblAvail/(archivist.m_data.m_dblTotal/100.0), 
																				 archivist.m_data.m_dblFree, 
																				 archivist.m_data.m_dblFree/(archivist.m_data.m_dblTotal/100.0), 
																				 archivist.m_data.m_dblTotal
																				 );  //(Dispatch message)
																archivist.SendMsg(CX_SENDMSG_INFO, "Archivist:DiskSpaceCheck", "Disk utilization has reached the auto-deletion level (%.02f%% available free space).  The system must delete older files to continue to function.\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
																				 (100.0-archivist.m_settings.m_dblDeletePercentageDiskFull),
																				 archivist.m_data.m_dblAvail, 
																				 archivist.m_data.m_dblAvail/(archivist.m_data.m_dblTotal/100.0), 
																				 archivist.m_data.m_dblFree, 
																				 archivist.m_data.m_dblFree/(archivist.m_data.m_dblTotal/100.0), 
																				 archivist.m_data.m_dblTotal
																				 );  //(Dispatch message)
							//									Sleep(50);
																archivist.m_data.m_bDiskDeletionSent = true;
															}

															// do auto deletes!
															archivist.DoAutoDeletion();

														}
														else
														{
															archivist.m_data.m_bDiskDeletionSent = false;  // always reset if the threshold comes back down
		//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here22"); Sleep(100); //(Dispatch message)

															if(dblDiskPercent>archivist.m_settings.m_dblWarnPercentageDiskFull)
															{
												
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disk space warning!");  
		archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_NOTCON);
						
		//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here56"); Sleep(100); //(Dispatch message)
																archivist.m_data.m_bDiskWarning = true;
																if(!archivist.m_data.m_bDiskWarningSent)
																{
		//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here x"); Sleep(100); //(Dispatch message)
																	//TODO:  send an actual report out
																	archivist.m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Archivist:DiskSpaceCheck", "Disk utilization has exceeded the warning level (%.02f%% available free space).  The system must delete older files to continue to function.  Auto-deletion will commence when the available disk space drops below %.02f%%.\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
																					 (100.0-archivist.m_settings.m_dblWarnPercentageDiskFull), 
																					 (100.0-archivist.m_settings.m_dblDeletePercentageDiskFull), 
																					 archivist.m_data.m_dblAvail, 
																					 archivist.m_data.m_dblAvail/(archivist.m_data.m_dblTotal/100.0), 
																					 archivist.m_data.m_dblFree, 
																					 archivist.m_data.m_dblFree/(archivist.m_data.m_dblTotal/100.0), 
																					 archivist.m_data.m_dblTotal
																					 );  //(Dispatch message)
		//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here x1"); Sleep(100); //(Dispatch message)

																	archivist.SendMsg(CX_SENDMSG_INFO, "Archivist:DiskSpaceCheck", "Disk utilization has exceeded the warning level (%.02f%% available free space).  The system must delete older files to continue to function.  Auto-deletion will commence when the available disk space drops below %.02f%%.\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
																					 (100.0-archivist.m_settings.m_dblWarnPercentageDiskFull), 
																					 (100.0-archivist.m_settings.m_dblDeletePercentageDiskFull), 
																					 archivist.m_data.m_dblAvail, 
																					 archivist.m_data.m_dblAvail/(archivist.m_data.m_dblTotal/100.0), 
																					 archivist.m_data.m_dblFree, 
																					 archivist.m_data.m_dblFree/(archivist.m_data.m_dblTotal/100.0), 
																					 archivist.m_data.m_dblTotal
																					 );
		//															Sleep(100);
		//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here x2"); Sleep(100); //(Dispatch message)
																	archivist.m_data.m_bDiskWarningSent = true;
																}
															}
															else
															{
																archivist.m_data.m_bDiskWarningSent = false;  // always reset if the threshold comes back down
																archivist.m_data.m_bDiskWarning = false;
		if(!archivist.m_data.m_bBackupDiskWarning)  // dont overwrite if theres a different prob
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "watching folder...");  
			archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_OK);
		}
															}
														}
														 
		// archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here arg"); Sleep(100); //(Dispatch message)

		/*
						if()

			m_bDiskWarningSent = false;
			m_bDiskWarning = false;
			m_bDiskDeletionSent = false;
			m_bDiskDeletion = false;
		*/
													}

													if(stricmp(pszPath, path_buffer))  // only do if different
													{
														if(!bPendingBackupOnly)
														{
		//													archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Copying file: %s to %s", pszPath, path_buffer);  //(Dispatch message)
										EnterCriticalSection(&g_dir.m_critFileOp);


										// have to check if the target exists and if so set its attribs to not ready only so we can overwrite if nec.
															DWORD dwfa = GetFileAttributes(path_buffer);
															if(dwfa!=0xFFFFFFFF) // then it succeeded, the file was there, etc.
															{
																dwfa&= ~FILE_ATTRIBUTE_READONLY;
																SetFileAttributes(path_buffer, dwfa);
															}

															if(CopyFile( pszPath, path_buffer, FALSE)) // flag for operation if file exists FALSE = overwrite.
															{
																if(archivist.m_settings.m_bLogTransfers) archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:Watchfolder", "Copied file: %s to %s", pszPath, path_buffer);  //(Dispatch message)
		//Sleep(500);  // let the disk flush.
																// update DB.
																// TODO db stuff
																bPrimaryCopyOK = true;
																//db stuff later..
																if(archivist.m_settings.m_bReportSuccessfulOperation)
																	archivist.SendMsg(CX_SENDMSG_INFO, "Archivist:Watchfolder", "Copied file: %s to %s", pszPath, path_buffer); 

	//Sleep(200);
																archivist.m_data.DelPending(nPendingID); // remove from pending list

															}
															else
															{ 
																int nErrorCode = GetLastError();

																if((nErrorCode == ERROR_ACCESS_DENIED )||(nErrorCode == ERROR_SHARING_VIOLATION)||(nErrorCode == ERROR_LOCK_VIOLATION))
																{
																	// busy for the moment.
																	if(nPendingID>=0)
																	{
																		if(nPendingRetries<archivist.m_settings.m_nPendingRetries)
																		{
																			archivist.m_data.ModPending(nPendingID);
																		}
																		else
																		{
																			archivist.m_data.DelPending(nPendingID);
 																			archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:Watchfolder", "Couldn't copy file: %s to %s (%d) after %d tries", pszPath, path_buffer, nErrorCode, nPendingRetries);  //(Dispatch message)
																			archivist.SendMsg(CX_SENDMSG_ERROR, "Archivist:Watchfolder", "Couldn't copy file: %s to %s (%d) after %d tries", pszPath, path_buffer, nErrorCode, nPendingRetries); 
																		}
																	}
																	else  // this is not a pending retry, so set it.
																	{
																		if(nPendingRetries<0) // it was a busy watchfolder item, or get pending failed.
																		{
																			archivist.m_data.SetPending(pDirChange);
 																			archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:Watchfolder", "Couldn't copy file: %s to %s (%d), retrying...", pszPath, path_buffer, nErrorCode);  //(Dispatch message)
																			archivist.SendMsg(CX_SENDMSG_ERROR, "Archivist:Watchfolder", "Couldn't copy file: %s to %s (%d), retrying...", pszPath, path_buffer, nErrorCode); 
																		}
																		else //final, because there is no ID
																		{
 																			archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:Watchfolder", "Couldn't copy file: %s to %s (%d)", pszPath, path_buffer, nErrorCode);  //(Dispatch message)
																			archivist.SendMsg(CX_SENDMSG_ERROR, "Archivist:Watchfolder", "Couldn't copy file: %s to %s (%d)", pszPath, path_buffer, nErrorCode); 
																		}
																	}
																}
																else  // its a "real" error
																{
 																	archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:Watchfolder", "Couldn't copy file: %s to %s (%d)", pszPath, path_buffer, nErrorCode);  //(Dispatch message)
																	archivist.SendMsg(CX_SENDMSG_ERROR, "Archivist:Watchfolder", "Couldn't copy file: %s to %s (%d)", pszPath, path_buffer, nErrorCode); 
																	if(nPendingID>=0) archivist.m_data.DelPending(nPendingID);

																}
															}
										
		/*
										
															if(MoveFileEx( pszPath, path_buffer, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING|MOVEFILE_WRITE_THROUGH)) // flag for operation if file exists FALSE = overwrite.
															{
																archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Moved file: %s to %s", pszPath, path_buffer);  //(Dispatch message)
															}
															else
 																archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Couldn't move file: %s (%d)", pszPath, GetLastError());  //(Dispatch message)
		*/								
										
										LeaveCriticalSection(&g_dir.m_critFileOp);
	//														archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:Watchfolder", "finished copying file: %s to %s", pszPath, path_buffer);  //(Dispatch message)
		//Sleep(200);
														}
														else
														{
															bPrimaryCopyOK = true;  // fake.  it means it was ok in the past
														}
													}
													else
													{
														if(archivist.m_settings.m_bLogTransfers) archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:Watchfolder", "Found file: %s", pszPath);  //(Dispatch message)
														bPrimaryCopyOK = true;
														bPrimaryPathSame = true;
														if(archivist.m_settings.m_bReportSuccessfulOperation)
															archivist.SendMsg(CX_SENDMSG_INFO, "Archivist:Watchfolder", "Found file: %s", pszPath);
													}

													if((bPrimaryCopyOK)&&(!bPendingBackupOnly))
													{
														// deal with metadata.

														// get the new path.
														if(strlen(pDirChange->m_szFullPath)>strlen(archivist.m_settings.m_pszWatchFolderPath))
														{
															pchRoot = pDirChange->m_szFullPath + strlen(archivist.m_settings.m_pszWatchFolderPath);
															sprintf(path_buffer, "%s%s", archivist.m_settings.m_pszDestinationFolderPath, pchRoot);
														}
														else  // must be in the root watch dir
														{
															sprintf(path_buffer, "%s", archivist.m_settings.m_pszDestinationFolderPath);
														}

														// remove trailing slashes, if any
														while(
																	  (strlen(path_buffer)>0)
																	&&(
																			(path_buffer[strlen(path_buffer)-1]=='\\')
																		||(path_buffer[strlen(path_buffer)-1]=='/')
																		)
																	)
														{
															path_buffer[strlen(path_buffer)-1] = 0;
														}


	//	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "finished copying file: %s to %s", pszPath, path_buffer);  //(Dispatch message)
	//Sleep(200);

		if(pdbConn)
		{
			CDBRecord* pdbrData=NULL;
			unsigned long ulNumRecords=0;

//			int nMetadataTableIndex = direct.m_db.GetTableIndex(archivist.m_data.m_pdbConnPrimary, archivist.m_settings.m_pszTableMeta);
//			if(nMetadataTableIndex>=0)
			{
				char* pchEncodedFilename = db.EncodeQuotes(pDirChange->m_szFilename);
				char* pchEncodedPath = db.EncodeQuotes(path_buffer);


//create table File_Metadata (sys_filename varchar(256) NOT NULL, sys_filepath varchar(256), 
//sys_linked_file varchar(256), sys_description varchar(256), sys_operator varchar(64), sys_type int, 
//sys_duration int, sys_valid_from int, sys_expires_after int, sys_ingest_date int, direct_last_used int, 
//direct_times_used int, sys_file_flags int, sys_file_size float, sys_created_on int, sys_created_by varchar(32),  
//sys_last_modified_on int, sys_last_modified_by varchar(32));

//#define ARCHIVIST_FILEFLAG_RECORDONLY			0x00000000
//#define ARCHIVIST_FILEFLAG_FILEXFER				0x00000001
//#define ARCHIVIST_FILEFLAG_FILECOPIED			0x00000002


				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE sys_filename = '%s' AND sys_filepath = '%s'", 
					archivist.m_settings.m_pszMetadata?archivist.m_settings.m_pszMetadata:"File_Metadata",
					pchEncodedFilename?pchEncodedFilename:pDirChange->m_szFilename,
					pchEncodedPath?pchEncodedPath:path_buffer
					);
//		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "Executing %s", szSQL); Sleep(100); //(Dispatch message)
//	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Executing %s", szSQL);  //(Dispatch message)
	//Sleep(200);

				//Sleep(500);
				bool bFoundRecord = false;
				CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
				if(prs != NULL) 
				{
//					Sleep(100); // a little delay to not peg the DB server.
					if(!prs->IsEOF())
					{
						prs->Close();  // close before next sql statement
						delete prs;
						prs = NULL;

						bFoundRecord = true;
						// just update flags
//		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "found one!"); Sleep(100); //(Dispatch message)

					}
					if(prs)
					{
						prs->Close();  // close before next sql statement
						delete prs;
						prs = NULL;
					}
				}

				char* pchDesc = NULL;
				if(bFoundRecord)
				{
					// determine filetype from extension.
					int nType = -1; // unknown!
					char* pchExt = strrchr(pDirChange->m_szFilename, '.');
					if((pchExt)&&(strlen(pchExt)>1))
					{
						pchExt++;
					}	else	pchExt = NULL;


					if(pchExt)
					{
						nType = archivist.m_data.GetFileTypeIndex(pchExt);
						if(nType>=0) pchDesc = 	db.EncodeQuotes(archivist.m_data.m_ppFileTypes[nType]->m_pszDesc);
					}

					_ftime( &archivist.m_data.m_timebTick );

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"UPDATE %s SET sys_file_flags = %d, sys_description='%s', sys_operator = 'sys', sys_type = %d, sys_ingest_date = %d, sys_file_size = %f, sys_file_timestamp = %d, sys_last_modified_on = %d, sys_last_modified_by = 'sys' WHERE sys_filename = '%s' AND sys_filepath = '%s'", 
						archivist.m_settings.m_pszMetadata?archivist.m_settings.m_pszMetadata:"File_Metadata",
						ARCHIVIST_FILEFLAG_FILECOPIED,
//						pchEncodedPath?pchEncodedPath:path_buffer,//pDirChange->m_szFullPath, // in case the path got changed.
						(nType>=0)?(pchDesc?pchDesc:"unknown file type"):"unknown file type",
						(nType>=0)?(archivist.m_data.m_ppFileTypes[nType]->m_nID):-1,
						(unsigned long)(archivist.m_data.m_timebTick.time - (archivist.m_data.m_timebTick.timezone*60) +(archivist.m_data.m_timebTick.dstflag?3600:0)),
						(double)(pDirChange->m_ulFileSizeHigh)*(0xffffffff) + (double)(pDirChange->m_ulFileSizeLow),
						CTime(pDirChange->m_ftFileModified).GetTime(),
						(unsigned long)(archivist.m_data.m_timebTick.time - (archivist.m_data.m_timebTick.timezone*60) +(archivist.m_data.m_timebTick.dstflag?3600:0)),
						pchEncodedFilename?pchEncodedFilename:pDirChange->m_szFilename,
						pchEncodedPath?pchEncodedPath:path_buffer //pDirChange->m_szFullPath, // in case the path got changed.
						);
//		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "Executing %s", szSQL); Sleep(100); //(Dispatch message)
//	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Executing %s", szSQL);  //(Dispatch message)
//Sleep(200);
					if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						if(archivist.m_settings.m_bLogTransfers) archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:UpdateMetadata", "%s", errorstring);  //(Dispatch message)
						_timeb timestamp;
						_ftime( &timestamp ); 
						timestamp.time+=5;

						while(
										(archivist.m_data.m_timebTick.time < timestamp.time )
									&&(!g_bKillThread)
									&&(!archivist.m_data.m_bProcessSuspended)
									)
						{

							_ftime( &archivist.m_data.m_timebTick ); 
							MSG msg;
							while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
							AfxGetApp()->PumpMessage();
							Sleep(1);
						}

						if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{ // give up
							archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:UpdateMetadata", "%s", errorstring);  //(Dispatch message)
						}
					}
				}
				else
				{ // add new!
//create table File_Metadata (sys_filename varchar(256) NOT NULL, sys_filepath varchar(256), 
//sys_linked_file varchar(256), sys_description varchar(256), sys_operator varchar(64), sys_type int, 
//sys_duration int, sys_valid_from int, sys_expires_after int, sys_ingest_date int, direct_last_used int, 
//direct_times_used int, sys_file_flags int, sys_file_size float, sys_created_on int, sys_created_by varchar(32),  
//sys_last_modified_on int, sys_last_modified_by varchar(32));

//#define ARCHIVIST_FILEFLAG_RECORDONLY			0x00000000
//#define ARCHIVIST_FILEFLAG_FILEXFER				0x00000001
//#define ARCHIVIST_FILEFLAG_FILECOPIED			0x00000002
					// determine filetype from extension.
					int nType = -1; // unknown!
					char* pchExt = strrchr(pDirChange->m_szFilename, '.');
					if((pchExt)&&(strlen(pchExt)>1))
					{
						pchExt++;
					}	else	pchExt = NULL;


					if(pchExt)
					{
						nType = archivist.m_data.GetFileTypeIndex(pchExt);
						if(nType>=0) pchDesc = 	db.EncodeQuotes(archivist.m_data.m_ppFileTypes[nType]->m_pszDesc);
					}
					
					
					_ftime( &archivist.m_data.m_timebTick );

					// have to insert a default system record. - just the fields we care about.
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"INSERT INTO %s (sys_filename, sys_filepath, sys_description, sys_operator, sys_type, sys_ingest_date, sys_file_flags, sys_file_size, sys_file_timestamp, sys_created_on, sys_created_by, sys_last_modified_on, sys_last_modified_by) VALUES \
('%s', '%s', '%s', 'sys', %d, %d, %d, %f, %d, %d, 'sys', %d, 'sys')", 
						archivist.m_settings.m_pszMetadata?archivist.m_settings.m_pszMetadata:"File_Metadata",
						pchEncodedFilename?pchEncodedFilename:pDirChange->m_szFilename, 
						pchEncodedPath?pchEncodedPath:path_buffer,//pDirChange->m_szFullPath, // in case the path got changed.
						(nType>=0)?(pchDesc?pchDesc:"unknown file type"):"unknown file type",
						(nType>=0)?(archivist.m_data.m_ppFileTypes[nType]->m_nID):-1,
						(unsigned long)(archivist.m_data.m_timebTick.time - (archivist.m_data.m_timebTick.timezone*60) +(archivist.m_data.m_timebTick.dstflag?3600:0)),
						ARCHIVIST_FILEFLAG_FILECOPIED,
						(double)(pDirChange->m_ulFileSizeHigh)*(0xffffffff) + (double)(pDirChange->m_ulFileSizeLow),
						CTime(pDirChange->m_ftFileModified).GetTime(),
						(unsigned long)(archivist.m_data.m_timebTick.time - (archivist.m_data.m_timebTick.timezone*60) +(archivist.m_data.m_timebTick.dstflag?3600:0)),
						(unsigned long)(archivist.m_data.m_timebTick.time - (archivist.m_data.m_timebTick.timezone*60) +(archivist.m_data.m_timebTick.dstflag?3600:0))
						);

/*
	unsigned long m_ulFileSizeHigh;				// filesize		DWORD    nFileSizeHigh; 
	unsigned long m_ulFileSizeLow;				// filesize		DWORD    nFileSizeLow;
	unsigned long m_ulFileAttribs;		// attribs returned by GetFileAttributes
	FILETIME m_ftFileModified;			// unixtime, seconds resolution, last modified
*/
//		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "Executing %s", szSQL); Sleep(100); //(Dispatch message)
	//	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Executing %s", szSQL);  //(Dispatch message)
	//Sleep(200);
					if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						if(archivist.m_settings.m_bLogTransfers) archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:InsertMetadata", "%s", errorstring);  //(Dispatch message)
						_timeb timestamp;
						_ftime( &timestamp ); 
						timestamp.time+=5;

						while(
										(archivist.m_data.m_timebTick.time < timestamp.time )
									&&(!g_bKillThread)
									&&(!archivist.m_data.m_bProcessSuspended)
									)
						{

							_ftime( &archivist.m_data.m_timebTick ); 
							MSG msg;
							while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
							AfxGetApp()->PumpMessage();
							Sleep(1);
						}

						if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{ // give up
							archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:InsertMetadata", "%s", errorstring);  //(Dispatch message)
						}
					}
	//				Sleep(50); // a little delay to not peg the DB server.
				}
				
				if(pchDesc) free(pchDesc);
				if(pchEncodedFilename) free(pchEncodedFilename);
				if(pchEncodedPath) free(pchEncodedPath);
			}
		} //if(archivist.m_data.m_pdbConnPrimary)

													} //if(bCopyOK)

//archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:debug", "checking chg: %d %d %x (%d)", archivist.m_settings.m_bDirectInstalled, bPrimaryCopyOK, pDirChange->m_ulFlags, ((pDirChange->m_ulFlags&WATCH_CHG)==WATCH_CHG));  //(Dispatch message)
													if((archivist.m_settings.m_bDirectInstalled)&&(bPrimaryCopyOK)&&((pDirChange->m_ulFlags&WATCH_CHG)==WATCH_CHG))
													{
//archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:debug", "updating direct");  //(Dispatch message)
														// the file has changed, so we have to signal direct to propagate it
														// insert into the exchange table
														if(pdbConn)
														{
															char path_buffer[MAX_PATH+1];

															char* pchRoot = NULL;
															if((strnicmp(pszPath, archivist.m_settings.m_pszWatchFolderPath, strlen(archivist.m_settings.m_pszWatchFolderPath))==0))
															{
																pchRoot = pszPath + strlen(archivist.m_settings.m_pszWatchFolderPath);
																sprintf(path_buffer, "%s%s", archivist.m_settings.m_pszDestinationFolderPath, pchRoot);
															}
															else
															{
																strcpy(path_buffer, pszPath);
															}

															pchRoot = db.EncodeQuotes(path_buffer);

															//criterion varchar(32) NOT NULL, flag varchar(256) NOT NULL, mod
															_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO [%s] (criterion, flag, mod) VALUES ('DiReCT_Update', '%s', %d)",
																archivist.m_settings.m_pszExchange,
																pchRoot?pchRoot:path_buffer,
																archivist.m_data.m_timebTick.time
																);

//archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:debug", "sending SQL: %s", szSQL);  //(Dispatch message)
															if(pchRoot) free(pchRoot);
															if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
															{
 																archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DiReCTUpdate", "Error updating DiReCT with file %s: %s", path_buffer, errorstring);  //(Dispatch message)
																archivist.SendMsg(CX_SENDMSG_ERROR, "Archivist:DiReCTUpdate", "Error updating DiReCT with file %s: %s", path_buffer, errorstring); 
															}
														}

													}

 												} // correct base folder


												if(!archivist.m_settings.m_bUseBackupDestination) bBackupCopyOK=true;  // for deleting source at end

												//then transfer from watchfolder to backup.
												if( (archivist.m_settings.m_bUseBackupDestination)
													&&(archivist.m_settings.m_pszBackupDestinationFolderPath)
													&&(strlen(archivist.m_settings.m_pszBackupDestinationFolderPath))
													&&(stricmp(archivist.m_settings.m_pszBackupDestinationFolderPath, archivist.m_settings.m_pszDestinationFolderPath))
													)
												{
													// transfer the file
													sprintf(pszPath, "%s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);
	//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "path is %s", pszPath); Sleep(100); //(Dispatch message)
													char* pchRoot = NULL;
													pchRoot = pszPath + strlen(archivist.m_settings.m_pszWatchFolderPath);
													char path_buffer[MAX_PATH+1];
// dont transfer from storage, transfer from  original watchfolder.  we just copied to storage so may not have flushed buffers yet, etc
//													sprintf(path_buffer, "%s%s", archivist.m_settings.m_pszDestinationFolderPath, pchRoot);
//													sprintf(pszPath, "%s", path_buffer);
													sprintf(path_buffer, "%s%s", archivist.m_settings.m_pszBackupDestinationFolderPath, pchRoot);

													// have to copy file, but check space first.
													double dblDiskPercent;

													// we had a prob with this before.
													BOOL bSuccess = FALSE;
													// only check the disk space once every 5 seconds.
													_ftime(&archivist.m_data.m_timebTick);

	//												if( timestamp.time > diskchecktime.time ) 
													{
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "about to call disk space %d>%d", (unsigned long)archivist.m_data.m_timebTick.time, (unsigned long)diskchecktime.time);  //(Dispatch message)
														
														bSuccess = GetDiskFreeSpaceEx( archivist.m_settings.m_pszBackupDestinationFolderPath, 
																																&archivist.m_data.m_uliBytesAvail, 
																																&archivist.m_data.m_uliBytesTotal,
																																&archivist.m_data.m_uliBytesFree
 																															); 

													}																	
	//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "called disk space");  //(Dispatch message)
													if(bSuccess) // make this zero to remove the disk check
													{
														diskchecktime.time = (archivist.m_data.m_timebTick.time+5);
	//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "assigned %d = %d",  diskchecktime.time, time(NULL)+5);  //(Dispatch message)

														archivist.m_data.m_dblAvail = ((double)(archivist.m_data.m_uliBytesAvail.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)archivist.m_data.m_uliBytesAvail.LowPart;
														archivist.m_data.m_dblTotal = ((double)(archivist.m_data.m_uliBytesTotal.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)archivist.m_data.m_uliBytesTotal.LowPart;
														archivist.m_data.m_dblFree	= ((double)(archivist.m_data.m_uliBytesFree.HighPart))*(((double)(ULONG_MAX))+1.0) + (double)archivist.m_data.m_uliBytesFree.LowPart;
													}

													dblDiskPercent = ((archivist.m_data.m_dblTotal-archivist.m_data.m_dblAvail)/(archivist.m_data.m_dblTotal/100.0));  // == used space

											/*		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:Backup DiskSpaceCheck", "Disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
																	 archivist.m_data.m_dblAvail, 
																	 archivist.m_data.m_dblAvail/(archivist.m_data.m_dblTotal/100.0), 
																	 archivist.m_data.m_dblFree, 
																	 archivist.m_data.m_dblFree/(archivist.m_data.m_dblTotal/100.0), 
																	 archivist.m_data.m_dblTotal
																	 );  //(Dispatch message)
	*/
	//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here"); Sleep(100); //(Dispatch message)

													if(dblDiskPercent>archivist.m_settings.m_dblDeletePercentageDiskFull)
													{
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Backup disk near full!");  
	archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_NOTCON);
	//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here2"); Sleep(100); //(Dispatch message)
														if(!archivist.m_data.m_bBackupDiskDeletionSent)
														{
															//TODO:  send an actual report out
															archivist.m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Archivist:BackupDiskSpaceCheck", "Backup disk utilization has reached the auto-deletion level (%.02f%% available free space).  The system must delete older files to continue to function.\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
																			 (100.0-archivist.m_settings.m_dblDeletePercentageDiskFull), 
																			 archivist.m_data.m_dblAvail, 
																			 archivist.m_data.m_dblAvail/(archivist.m_data.m_dblTotal/100.0), 
																			 archivist.m_data.m_dblFree, 
																			 archivist.m_data.m_dblFree/(archivist.m_data.m_dblTotal/100.0), 
																			 archivist.m_data.m_dblTotal
																			 );  //(Dispatch message)
															archivist.SendMsg(CX_SENDMSG_INFO, "Archivist:BackupDiskSpaceCheck", "Backup disk utilization has reached the auto-deletion level (%.02f%% available free space).  The system must delete older files to continue to function.\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
																			 (100.0-archivist.m_settings.m_dblDeletePercentageDiskFull),
																			 archivist.m_data.m_dblAvail, 
																			 archivist.m_data.m_dblAvail/(archivist.m_data.m_dblTotal/100.0), 
																			 archivist.m_data.m_dblFree, 
																			 archivist.m_data.m_dblFree/(archivist.m_data.m_dblTotal/100.0), 
																			 archivist.m_data.m_dblTotal
																			 );  //(Dispatch message)
												//			Sleep(100);
															archivist.m_data.m_bBackupDiskDeletionSent = true;
														}

														// do auto deletes!
														archivist.DoAutoDeletion(true);

													}
													else
													{
														archivist.m_data.m_bBackupDiskDeletionSent = false;  // always reset if the threshold comes back down
	//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here22"); Sleep(100); //(Dispatch message)

														if(dblDiskPercent>archivist.m_settings.m_dblWarnPercentageDiskFull)
														{
											
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Backup disk space warning!");  
	archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_NOTCON);
					
	//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here56"); Sleep(100); //(Dispatch message)
															archivist.m_data.m_bBackupDiskWarning = true;
															if(!archivist.m_data.m_bBackupDiskWarningSent)
															{
	//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here x"); Sleep(100); //(Dispatch message)
																//TODO:  send an actual report out
																archivist.m_msgr.DM(MSG_ICONEXCLAMATION, NULL, "Archivist:BackupDiskSpaceCheck", "Backup disk utilization has exceeded the warning level (%.02f%% available free space).  The system must delete older files to continue to function.  Auto-deletion will commence when the available disk space drops below %.02f%%.\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
																				 (100.0-archivist.m_settings.m_dblWarnPercentageDiskFull), 
																				 (100.0-archivist.m_settings.m_dblDeletePercentageDiskFull), 
																				 archivist.m_data.m_dblAvail, 
																				 archivist.m_data.m_dblAvail/(archivist.m_data.m_dblTotal/100.0), 
																				 archivist.m_data.m_dblFree, 
																				 archivist.m_data.m_dblFree/(archivist.m_data.m_dblTotal/100.0), 
																				 archivist.m_data.m_dblTotal
																				 );  //(Dispatch message)
	//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here x1"); Sleep(100); //(Dispatch message)

																archivist.SendMsg(CX_SENDMSG_INFO, "Archivist:BackupDiskSpaceCheck", "Backup disk utilization has exceeded the warning level (%.02f%% available free space).  The system must delete older files to continue to function.  Auto-deletion will commence when the available disk space drops below %.02f%%.\nCurrent disk space statistics:\nBytes available: %.0lf (%.02f%%)\nBytes free: %.0lf (%.02f%%)\nBytes Total: %.0lf", 
																				 (100.0-archivist.m_settings.m_dblWarnPercentageDiskFull), 
																				 (100.0-archivist.m_settings.m_dblDeletePercentageDiskFull), 
																				 archivist.m_data.m_dblAvail, 
																				 archivist.m_data.m_dblAvail/(archivist.m_data.m_dblTotal/100.0), 
																				 archivist.m_data.m_dblFree, 
																				 archivist.m_data.m_dblFree/(archivist.m_data.m_dblTotal/100.0), 
																				 archivist.m_data.m_dblTotal
																				 );
	//															Sleep(100);
	//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here x2"); Sleep(100); //(Dispatch message)
																archivist.m_data.m_bBackupDiskWarningSent = true;
															}
														}
														else
														{
															archivist.m_data.m_bBackupDiskWarningSent = false;  // always reset if the threshold comes back down
															archivist.m_data.m_bBackupDiskWarning = false;
		if(!archivist.m_data.m_bDiskWarning)  // dont overwrite if theres a different prob
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "watching folder...");  
			archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_OK);
		}
														}
													}
													 
	// archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "here arg"); Sleep(100); //(Dispatch message)

	/*
					if()

		m_bDiskWarningSent = false;
		m_bDiskWarning = false;
		m_bDiskDeletionSent = false;
		m_bDiskDeletion = false;
	*/
													if(stricmp(pszPath, path_buffer))  // only do if different
													{
	//													archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "copying file: %s to %s", pszPath, path_buffer);  //(Dispatch message)
									EnterCriticalSection(&g_dir.m_critFileOp);


									// have to check if the target exists and if so set its attribs to not ready only so we can overwrite if nec.
														DWORD dwfa = GetFileAttributes(path_buffer);
														if(dwfa!=0xFFFFFFFF) // then it succeeded, the file was there, etc.
														{
															dwfa&= ~FILE_ATTRIBUTE_READONLY;
															SetFileAttributes(path_buffer, dwfa);
														}

														if(CopyFile( pszPath, path_buffer, FALSE)) // flag for operation if file exists FALSE = overwrite.
														{
															if(archivist.m_settings.m_bLogTransfers) archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:BackupCopy", "Copied file: %s to %s", pszPath, path_buffer);  //(Dispatch message)
	//Sleep(500);  // let the disk flush.
															// update DB.
															// TODO db stuff
															bBackupCopyOK = true;
															//db stuff later..
															if(archivist.m_settings.m_bReportSuccessfulOperation)
																archivist.SendMsg(CX_SENDMSG_INFO, "Archivist:BackupCopy", "Copied file: %s to %s", pszPath, path_buffer); 

															if(bPendingBackupOnly) archivist.m_data.DelPending(nPendingID); // remove from pending list

														}
														else
														{
															int nErrorCode = GetLastError();

															if((nErrorCode == ERROR_ACCESS_DENIED )||(nErrorCode == ERROR_SHARING_VIOLATION)||(nErrorCode == ERROR_LOCK_VIOLATION))
															{
																// busy for the moment.
																if((nPendingID>=0)&&(bPendingBackupOnly))
																{
																	if(nPendingRetries<archivist.m_settings.m_nPendingRetries)
																	{
																		archivist.m_data.ModPending(nPendingID); // remove from pending list
																	}
																	else
																	{
																		archivist.m_data.DelPending(nPendingID);
 																		archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BackupCopy", "Couldn't copy file: %s to %s (%d) after %d tries", pszPath, path_buffer, nErrorCode, nPendingRetries);  //(Dispatch message)
																		archivist.SendMsg(CX_SENDMSG_ERROR, "Archivist:BackupCopy", "Couldn't copy file: %s to %s (%d) after %d tries", pszPath, path_buffer, nErrorCode, nPendingRetries); 
																	}
																}
																else // its either the original pending item, or a live item
																{
																	if(nPendingID>=0) //its the original pending item
																	{
																		if(archivist.m_settings.m_nPendingRetries>0)
																		{
																			if(bPrimaryCopyOK) // then the original was deleted
																			{
																				archivist.m_data.SetPending(pDirChange, true);  // sets a backup only one.
																			}
	//																		else
	//																		{
	//																			 the original is still there, doesnt need extra mod.
	//																		}
 																			archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BackupCopy", "Couldn't copy file: %s to %s (%d), retrying...", pszPath, path_buffer, nErrorCode);  //(Dispatch message)
																			archivist.SendMsg(CX_SENDMSG_ERROR, "Archivist:BackupCopy", "Couldn't copy file: %s to %s (%d), retrying...", pszPath, path_buffer, nErrorCode); 
																		}
																		else //final
																		{
 																			archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BackupCopy", "Couldn't copy file: %s to %s (%d)", pszPath, path_buffer, nErrorCode);  //(Dispatch message)
																			archivist.SendMsg(CX_SENDMSG_ERROR, "Archivist:BackupCopy", "Couldn't copy file: %s to %s (%d)", pszPath, path_buffer, nErrorCode); 
																		}
																	}
																	else  //its a live item
																	{
																		if(bPrimaryCopyOK) // then the original was deleted
																		{
																			archivist.m_data.SetPending(pDirChange, true);  // sets a backup only one.
																		}
//																		else
//																		{
//																			 the original live item was set to pending, dont need
//																		}
 																		archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BackupCopy", "Couldn't copy file: %s to %s (%d), retrying...", pszPath, path_buffer, nErrorCode);  //(Dispatch message)
																		archivist.SendMsg(CX_SENDMSG_ERROR, "Archivist:BackupCopy", "Couldn't copy file: %s to %s (%d), retrying...", pszPath, path_buffer, nErrorCode); 
																	}
																}
															}
															else  // its a "real" error
															{
 																archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BackupCopy", "Couldn't copy file: %s to %s (%d)", pszPath, path_buffer, nErrorCode);  //(Dispatch message)
																archivist.SendMsg(CX_SENDMSG_ERROR, "Archivist:BackupCopy", "Couldn't copy file: %s to %s (%d)", pszPath, path_buffer, nErrorCode); 
																if((nPendingID>=0)&&(bPendingBackupOnly)) archivist.m_data.DelPending(nPendingID);
															}
														}
									
	/*
									
														if(MoveFileEx( pszPath, path_buffer, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING|MOVEFILE_WRITE_THROUGH)) // flag for operation if file exists FALSE = overwrite.
														{
															archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Moved file: %s to %s", pszPath, path_buffer);  //(Dispatch message)
														}
														else
 															archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Couldn't move file: %s (%d)", pszPath, GetLastError());  //(Dispatch message)
	*/								
									
									LeaveCriticalSection(&g_dir.m_critFileOp);
//														archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:Watchfolder", "finished copying file: %s to %s", pszPath, path_buffer);  //(Dispatch message)
	//Sleep(200);
													}
													else
													{
														bBackupCopyOK = true;
														bBackupPathSame = true;
													}


/*
		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "backup copy OK, %d, %s, %d, %s, %s, %d",
			
													(archivist.m_settings.m_bUseBackupDatabase),
													(archivist.m_settings.m_pszBackupDSN),
													(strlen(archivist.m_settings.m_pszBackupDSN)>0), // has to be valid
													(archivist.m_settings.m_pszBackupUser),
													(archivist.m_settings.m_pszBackupPW),
													(strcmp(archivist.m_settings.m_pszDSN, archivist.m_settings.m_pszBackupDSN)!=0)  // different DSN
			
			
			); Sleep(100); //(Dispatch message)

*/
													if(
														  (bBackupCopyOK)
														&&(archivist.m_settings.m_bUseBackupDatabase)
														&&(archivist.m_settings.m_pszBackupDSN)
														&&(strlen(archivist.m_settings.m_pszBackupDSN)>0) // has to be valid
														&&(archivist.m_settings.m_pszBackupUser)
														&&(archivist.m_settings.m_pszBackupPW)
														&&(strcmp(archivist.m_settings.m_pszDSN, archivist.m_settings.m_pszBackupDSN)!=0)  // different DSN
														)
													{
														// deal with metadata.
//		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "backup metadata"); Sleep(100); //(Dispatch message)

/////////   **********************************************
////  The following is backup push of metdata.
////  Our file copy is a mapped drive.
														// get the new path.
														if(strlen(pDirChange->m_szFullPath)>strlen(archivist.m_settings.m_pszWatchFolderPath))
														{
															pchRoot = pDirChange->m_szFullPath + strlen(archivist.m_settings.m_pszWatchFolderPath);
															sprintf(path_buffer, "%s%s", archivist.m_settings.m_pszBackupDestinationFolderLocalPath, pchRoot);
														}
														else  // must be in the root watch dir
														{
															sprintf(path_buffer, "%s", archivist.m_settings.m_pszBackupDestinationFolderLocalPath);
														}
														
														// remove trailing slashes, if any
														while(
																	  (strlen(path_buffer)>0)
																	&&(
																			(path_buffer[strlen(path_buffer)-1]=='\\')
																		||(path_buffer[strlen(path_buffer)-1]=='/')
																		)
																	)
														{
															path_buffer[strlen(path_buffer)-1] = 0;
														}

	//	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "finished copying file: %s to %s", pszPath, path_buffer);  //(Dispatch message)
	//Sleep(200);
//		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "backup metadata connecting"); Sleep(100); //(Dispatch message)

		//  using a transient connection to DB.  (dont just keep open).
/*
		CDBconn* pdbBackupConn = db.CreateNewConnection(
			archivist.m_settings.m_pszBackupDSN, 
			archivist.m_settings.m_pszBackupUser, 
			archivist.m_settings.m_pszBackupPW);
//		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "backup metadata after connect"); Sleep(100); //(Dispatch message)
*/
//		if((pdbBackupConn)&&(db.ConnectDatabase(pdbBackupConn, errorstring)>=DB_SUCCESS))
		if(archivist.m_data.m_pdbBackup)
		{
			CDBRecord* pdbrData=NULL;
			unsigned long ulNumRecords=0;
//		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "backup metadata connected"); Sleep(100); //(Dispatch message)

//			int nMetadataTableIndex = direct.m_db.GetTableIndex(archivist.m_data.m_pdbConnPrimary, archivist.m_settings.m_pszTableMeta);
//			if(nMetadataTableIndex>=0)
			{
				char* pchEncodedFilename = archivist.m_data.m_pdbBackup->EncodeQuotes(pDirChange->m_szFilename);
				char* pchEncodedPath = archivist.m_data.m_pdbBackup->EncodeQuotes(path_buffer);


//create table File_Metadata (sys_filename varchar(256) NOT NULL, sys_filepath varchar(256), 
//sys_linked_file varchar(256), sys_description varchar(256), sys_operator varchar(64), sys_type int, 
//sys_duration int, sys_valid_from int, sys_expires_after int, sys_ingest_date int, direct_last_used int, 
//direct_times_used int, sys_file_flags int, sys_file_size float, sys_created_on int, sys_created_by varchar(32),  
//sys_last_modified_on int, sys_last_modified_by varchar(32));

//#define ARCHIVIST_FILEFLAG_RECORDONLY			0x00000000
//#define ARCHIVIST_FILEFLAG_FILEXFER				0x00000001
//#define ARCHIVIST_FILEFLAG_FILECOPIED			0x00000002


				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE sys_filename = '%s' AND sys_filepath = '%s'", 
					archivist.m_settings.m_pszMetadata?archivist.m_settings.m_pszMetadata:"File_Metadata",
					pchEncodedFilename?pchEncodedFilename:pDirChange->m_szFilename,
					pchEncodedPath?pchEncodedPath:path_buffer
					);
//		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "Executing %s", szSQL); Sleep(100); //(Dispatch message)
//	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Executing %s", szSQL);  //(Dispatch message)
	//Sleep(200);

///		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "backup metadata about to retrieve"); Sleep(100); //(Dispatch message)
				//Sleep(500);
				bool bFoundRecord = false;
				CRecordset* prs = archivist.m_data.m_pdbBackup->Retrieve(archivist.m_data.m_pdbBackupConn, szSQL, errorstring);
				if(prs != NULL) 
				{
//					Sleep(100); // a little delay to not peg the DB server.
					if(!prs->IsEOF())
					{
						prs->Close();  // close before next sql statement
						delete prs;
						prs = NULL;

						bFoundRecord = true;
						// just update flags
//		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "found one!"); Sleep(100); //(Dispatch message)

					}
					if(prs)
					{
						prs->Close();  // close before next sql statement
						delete prs;
						prs = NULL;
					}
				}

				char* pchDesc = NULL;

				if(bFoundRecord)
				{
//		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "backup metadata found"); Sleep(100); //(Dispatch message)
					// determine filetype from extension.
					int nType = -1; // unknown!
					char* pchExt = strrchr(pDirChange->m_szFilename, '.');
					if((pchExt)&&(strlen(pchExt)>1))
					{
						pchExt++;
					}	else	pchExt = NULL;


					if(pchExt)
					{
						nType = archivist.m_data.GetFileTypeIndex(pchExt);
						if(nType>=0) pchDesc = 	archivist.m_data.m_pdbBackup->EncodeQuotes(archivist.m_data.m_ppFileTypes[nType]->m_pszDesc);
					}

					_ftime( &archivist.m_data.m_timebTick );

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"UPDATE %s SET sys_file_flags = %d, sys_description='%s', sys_operator = 'sys', sys_type = %d, sys_ingest_date = %d, sys_file_size = %f, sys_file_timestamp = %d, sys_last_modified_on = %d, sys_last_modified_by = 'sys' WHERE sys_filename = '%s' AND sys_filepath = '%s'", 
						archivist.m_settings.m_pszMetadata?archivist.m_settings.m_pszMetadata:"File_Metadata",
						ARCHIVIST_FILEFLAG_FILECOPIED,
//						pchEncodedPath?pchEncodedPath:path_buffer,//pDirChange->m_szFullPath, // in case the path got changed.
						(nType>=0)?(pchDesc?pchDesc:"unknown file type"):"unknown file type",
						(nType>=0)?(archivist.m_data.m_ppFileTypes[nType]->m_nID):-1,
						(unsigned long)(archivist.m_data.m_timebTick.time - (archivist.m_data.m_timebTick.timezone*60) +(archivist.m_data.m_timebTick.dstflag?3600:0)),
						(double)(pDirChange->m_ulFileSizeHigh)*(0xffffffff) + (double)(pDirChange->m_ulFileSizeLow),
						CTime(pDirChange->m_ftFileModified).GetTime(),
						(unsigned long)(archivist.m_data.m_timebTick.time - (archivist.m_data.m_timebTick.timezone*60) +(archivist.m_data.m_timebTick.dstflag?3600:0)),
						pchEncodedFilename?pchEncodedFilename:pDirChange->m_szFilename,
						pchEncodedPath?pchEncodedPath:path_buffer //pDirChange->m_szFullPath, // in case the path got changed.
						);
//archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "Executing %s", szSQL); Sleep(100); //(Dispatch message)
//	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Executing %s", szSQL);  //(Dispatch message)
//Sleep(200);
					if(archivist.m_data.m_pdbBackup->ExecuteSQL(archivist.m_data.m_pdbBackupConn, szSQL, errorstring)<DB_SUCCESS)
					{
						if(archivist.m_settings.m_bLogTransfers) archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:UpdateBackupMetadata", "%s", errorstring);  //(Dispatch message)
						_timeb timestamp;
						_ftime( &timestamp ); 
						timestamp.time+=5;

						while(
										(archivist.m_data.m_timebTick.time < timestamp.time )
									&&(!g_bKillThread)
									&&(!archivist.m_data.m_bProcessSuspended)
									)
						{

							_ftime( &archivist.m_data.m_timebTick ); 
							MSG msg;
							while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
							AfxGetApp()->PumpMessage();
							Sleep(1);
						}

						if(archivist.m_data.m_pdbBackup->ExecuteSQL(archivist.m_data.m_pdbBackupConn, szSQL, errorstring)<DB_SUCCESS)
						{ // give up
							archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:UpdateBackupMetadata", "%s", errorstring);  //(Dispatch message)
						}
					}
	//					Sleep(50); // a little delay to not peg the DB server.
				}
				else
				{ // add new!
//create table File_Metadata (sys_filename varchar(256) NOT NULL, sys_filepath varchar(256), 
//sys_linked_file varchar(256), sys_description varchar(256), sys_operator varchar(64), sys_type int, 
//sys_duration int, sys_valid_from int, sys_expires_after int, sys_ingest_date int, direct_last_used int, 
//direct_times_used int, sys_file_flags int, sys_file_size float, sys_created_on int, sys_created_by varchar(32),  
//sys_last_modified_on int, sys_last_modified_by varchar(32));

//#define ARCHIVIST_FILEFLAG_RECORDONLY			0x00000000
//#define ARCHIVIST_FILEFLAG_FILEXFER				0x00000001
//#define ARCHIVIST_FILEFLAG_FILECOPIED			0x00000002
					// determine filetype from extension.
					int nType = -1; // unknown!
					char* pchExt = strrchr(pDirChange->m_szFilename, '.');
					if((pchExt)&&(strlen(pchExt)>1))
					{
						pchExt++;
					}	else	pchExt = NULL;


					if(pchExt)
					{
						nType = archivist.m_data.GetFileTypeIndex(pchExt);
						if(nType>=0) pchDesc = 	archivist.m_data.m_pdbBackup->EncodeQuotes(archivist.m_data.m_ppFileTypes[nType]->m_pszDesc);
					}
					
					
					_ftime( &archivist.m_data.m_timebTick );

					// have to insert a default system record. - just the fields we care about.
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"INSERT INTO %s (sys_filename, sys_filepath, sys_description, sys_operator, sys_type, sys_ingest_date, sys_file_flags, sys_file_size, sys_file_timestamp, sys_created_on, sys_created_by, sys_last_modified_on, sys_last_modified_by) VALUES \
('%s', '%s', '%s', 'sys', %d, %d, %d, %f, %d, %d, 'sys', %d, 'sys')", 
						archivist.m_settings.m_pszMetadata?archivist.m_settings.m_pszMetadata:"File_Metadata",
						pchEncodedFilename?pchEncodedFilename:pDirChange->m_szFilename, 
						pchEncodedPath?pchEncodedPath:path_buffer,//pDirChange->m_szFullPath, // in case the path got changed.
						(nType>=0)?(pchDesc?pchDesc:"unknown file type"):"unknown file type",
						(nType>=0)?(archivist.m_data.m_ppFileTypes[nType]->m_nID):-1,
						(unsigned long)(archivist.m_data.m_timebTick.time - (archivist.m_data.m_timebTick.timezone*60) +(archivist.m_data.m_timebTick.dstflag?3600:0)),
						ARCHIVIST_FILEFLAG_FILECOPIED,
						(double)(pDirChange->m_ulFileSizeHigh)*(0xffffffff) + (double)(pDirChange->m_ulFileSizeLow),
						CTime(pDirChange->m_ftFileModified).GetTime(),
						(unsigned long)(archivist.m_data.m_timebTick.time - (archivist.m_data.m_timebTick.timezone*60) +(archivist.m_data.m_timebTick.dstflag?3600:0)),
						(unsigned long)(archivist.m_data.m_timebTick.time - (archivist.m_data.m_timebTick.timezone*60) +(archivist.m_data.m_timebTick.dstflag?3600:0))
						);


/*
	unsigned long m_ulFileSizeHigh;				// filesize		DWORD    nFileSizeHigh; 
	unsigned long m_ulFileSizeLow;				// filesize		DWORD    nFileSizeLow;
	unsigned long m_ulFileAttribs;		// attribs returned by GetFileAttributes
	FILETIME m_ftFileModified;			// unixtime, seconds resolution, last modified
*/
//archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "Executing %s", szSQL); Sleep(100); //(Dispatch message)
	//	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:Watchfolder", "Executing %s", szSQL);  //(Dispatch message)
	//Sleep(200);
					if(archivist.m_data.m_pdbBackup->ExecuteSQL(archivist.m_data.m_pdbBackupConn, szSQL, errorstring)<DB_SUCCESS)
					{
						if(archivist.m_settings.m_bLogTransfers) archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:InsertBackupMetadata", "%s", errorstring);  //(Dispatch message)
						_timeb timestamp;
						_ftime( &timestamp ); 
						timestamp.time+=5;

						while(
										(archivist.m_data.m_timebTick.time < timestamp.time )
									&&(!g_bKillThread)
									&&(!archivist.m_data.m_bProcessSuspended)
									)
						{

							_ftime( &archivist.m_data.m_timebTick ); 
							MSG msg;
							while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
							AfxGetApp()->PumpMessage();
							Sleep(1);
						}

						if(archivist.m_data.m_pdbBackup->ExecuteSQL(archivist.m_data.m_pdbBackupConn, szSQL, errorstring)<DB_SUCCESS)
						{ // give up
							archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:InsertBackupMetadata", "%s", errorstring);  //(Dispatch message)
						}
					}
	//				Sleep(50); // a little delay to not peg the DB server.
				}
				
				if(pchDesc) free(pchDesc);
				if(pchEncodedFilename) free(pchEncodedFilename);
				if(pchEncodedPath) free(pchEncodedPath);
			}
//			db.DisconnectDatabase(pdbBackupConn, errorstring);
		} //if(archivist.m_data.m_pdbConnBackup)
//		db.RemoveConnection(pdbBackupConn);


													} //if(bCopyOK) on the backup.
												}

												if(archivist.m_settings.m_bDeleteSourceFileOnTransfer)
												{
													if(!(pDirChange->m_ulFileAttribs&FILE_ATTRIBUTE_READONLY))  // because delete will fail on files that are read only
													{
													// we are just deleting immediately...
//																	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:Watchfolder", "deleting file: %s", pszPath);  //(Dispatch message)
//Sleep(200);
							
//														_unlink(pszPath); // try this!  nope, didnt work.
//archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:debug", "%d %d %d %d deleting file: %s", bPrimaryCopyOK, bBackupCopyOK,(!bPrimaryPathSame),(!bBackupPathSame),pszPath);  //(Dispatch message)

														if((bPrimaryCopyOK)&&(bBackupCopyOK)&&(!bPrimaryPathSame)&&(!bBackupPathSame))
														{
															if(DeleteFile(pszPath))
															{
 																if(archivist.m_settings.m_bLogTransfers) archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:Watchfolder", "Deleted file: %s", pszPath);  //(Dispatch message)
															}
															else
															{
 																archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:Watchfolder", "Couldn't delete file: %s (%d)", pszPath, GetLastError());  //(Dispatch message)
															}
														}
													}
												}//if(archivist.m_settings.m_bDeleteSourceFileOnTransfer)
											}  // valid to transfer
										}  // not a dir
									} // valid filename
								}// non-null params

							} break;
						case WATCH_DEL: //		0x00000002  // was del
							{
								// a file or dir was deleted in the watchfolder. (manually)
								// if a file, we need to update metadata
								// but only if:
								// WE did not delete it, because m_bDeleteSourceFileOnTransfer was true
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "DEL: %s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);  //(Dispatch message)

								if(
										(!archivist.m_settings.m_bDeleteSourceFileOnTransfer)
									&&(archivist.m_settings.m_bUseDeletionSync)
									)
								{
									if((pDirChange->m_szFilename)&&(pDirChange->m_szFullPath))
									{
										if((strcmp(pDirChange->m_szFilename, "."))&&(strcmp(pDirChange->m_szFilename, ".."))) // only if its a real file or dir
										{
											if(pDirChange->m_ulFileAttribs&FILE_ATTRIBUTE_DIRECTORY)  // its a dir
											{
												if(nPendingID>=0)
												{
													bool bDirPendingDeleted = false;
													if(
														  (stricmp(archivist.m_settings.m_pszWatchFolderPath, archivist.m_settings.m_pszDestinationFolderPath))  // only do if different
														&&(!bPendingBackupOnly)
														)
													{
														sprintf(pszPath, "%s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);
														char* pchRoot = NULL;
														if(strnicmp(pszPath, archivist.m_settings.m_pszWatchFolderPath, strlen(archivist.m_settings.m_pszWatchFolderPath))==0)
														{
															pchRoot = pszPath + strlen(archivist.m_settings.m_pszWatchFolderPath);
															char path_buffer[MAX_PATH+1];
															sprintf(path_buffer, "%s%s", archivist.m_settings.m_pszDestinationFolderPath, pchRoot);
															if(rmdir(path_buffer)==0) 
															{
																if(g_parchivist->m_settings.m_bLogTransfers) g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:DeleteDir", "Deleted directory: %s", path_buffer);  // Sleep(10);//(Dispatch message)
																archivist.m_data.DelPending(nPendingID);
																bDirPendingDeleted = true;
															}
															else
															{ 
																int nErrorCode = errno;

 																if( nErrorCode == ENOTEMPTY )
																{
																	// busy for the moment.
																	if(nPendingRetries<archivist.m_settings.m_nPendingRetries)
																	{
																		//archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DeleteDirError", "Couldn't delete directory: %s (%d) retrying", path_buffer, nErrorCode); //  Sleep(10);//(Dispatch message)
																		archivist.m_data.ModPending(nPendingID);
																	}
																	else
																	{
																		archivist.m_data.DelPending(nPendingID);
																		bDirPendingDeleted = true;
																		archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DeleteDirError", "Couldn't delete directory: %s (%d)", path_buffer, nErrorCode);  // Sleep(10);//(Dispatch message)
																	}
																}
																else  // its a "real" error
																{
																	archivist.m_data.DelPending(nPendingID);
																	bDirPendingDeleted = true;
																	archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DeleteDirError", "Couldn't delete directory: %s (%d)", path_buffer, nErrorCode);  // Sleep(10);//(Dispatch message)
																}
															}
														}
													}
													


													// ok, here, we want to match the backup.

													if(
															(archivist.m_settings.m_bUseBackupDeletionSync)
														&&(archivist.m_settings.m_pszWatchFolderPath)
														&&(strlen(archivist.m_settings.m_pszWatchFolderPath))
														&&(archivist.m_settings.m_pszBackupDestinationFolderPath)
														&&(strlen(archivist.m_settings.m_pszBackupDestinationFolderPath))
														&&(strcmp(archivist.m_settings.m_pszWatchFolderPath, archivist.m_settings.m_pszBackupDestinationFolderPath))
														)
													{
														sprintf(pszPath, "%s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);
														char* pchRoot = NULL;
														if(strnicmp(pszPath, archivist.m_settings.m_pszWatchFolderPath, strlen(archivist.m_settings.m_pszWatchFolderPath))==0)
														{
															pchRoot = pszPath + strlen(archivist.m_settings.m_pszWatchFolderPath);
															char path_buffer[MAX_PATH+1];
															sprintf(path_buffer, "%s%s", archivist.m_settings.m_pszBackupDestinationFolderPath, pchRoot);
															if(_rmdir(path_buffer)==0)
															{
																if(g_parchivist->m_settings.m_bLogTransfers) g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:DeleteBackupDir", "Deleted directory: %s", path_buffer);  // Sleep(10);//(Dispatch message)
																if(!bDirPendingDeleted) archivist.m_data.DelPending(nPendingID);
															}
															else
															{ 
																if(bDirPendingDeleted) archivist.m_data.SetPending(pDirChange, true); // add the backup one in, but only if the other one is gone
																int nErrorCode = errno;

																if(nErrorCode == ENOTEMPTY )
																{
																	// busy for the moment.
																	if(nPendingRetries<archivist.m_settings.m_nPendingRetries)
																	{
																		if(!bDirPendingDeleted) archivist.m_data.ModPending(nPendingID);
																	}
																	else
																	{
																		if(!bDirPendingDeleted) archivist.m_data.DelPending(nPendingID);
																		archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DeleteBackupDirError", "Couldn't delete directory: %s (%d)", path_buffer, nErrorCode);  // Sleep(10);//(Dispatch message)
																	}
																}
																else  // its a "real" error
																{
																	if(!bDirPendingDeleted) archivist.m_data.DelPending(nPendingID);
																	archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DeleteBackupDirError", "Couldn't delete directory: %s (%d)", path_buffer, nErrorCode); //  Sleep(10);//(Dispatch message)
																}
															}
														}

													}
												}
												else
												{
													// let's just put it in the pending queue, we prob have to delete its files first anyway
													archivist.m_data.SetPending(pDirChange);
												}
											}
											else // its a file.
											{
												bool bDirPendingDeleted = false;

												char path_buffer[MAX_PATH+1];
												char* pchRoot = NULL;
												if(
													  (stricmp(archivist.m_settings.m_pszWatchFolderPath, archivist.m_settings.m_pszDestinationFolderPath))  // only do if different.
													&&(!bPendingBackupOnly)
													)

												{
													// get dest path.
													sprintf(pszPath, "%s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);
													pchRoot = pszPath + strlen(g_parchivist->m_settings.m_pszWatchFolderPath);
													sprintf(path_buffer, "%s%s", g_parchivist->m_settings.m_pszDestinationFolderPath, pchRoot);

													DWORD dwfa = GetFileAttributes(path_buffer);
													if(dwfa!=0xFFFFFFFF) // then it succeeded, the file was there, etc.
													{
														dwfa&= ~FILE_ATTRIBUTE_READONLY;
														SetFileAttributes(path_buffer, dwfa);
													

														if(DeleteFile(path_buffer))
														{
															if(g_parchivist->m_settings.m_bLogTransfers)
																archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:DeleteFile", "Deleted file: %s", path_buffer);  // Sleep(10);//(Dispatch message)
															if(nPendingID>=0)
															{
																archivist.m_data.DelPending(nPendingID);
																bDirPendingDeleted = true;
															}
														}
														else
														{
															int nErrorCode = GetLastError();

															if((nErrorCode == ERROR_ACCESS_DENIED )||(nErrorCode == ERROR_SHARING_VIOLATION)||(nErrorCode == ERROR_LOCK_VIOLATION))
															{
																// busy for the moment.
																if(nPendingID>=0)
																{
																	if(nPendingRetries<archivist.m_settings.m_nPendingRetries)
																	{
																		archivist.m_data.ModPending(nPendingID);
																	}
																	else
																	{
																		archivist.m_data.DelPending(nPendingID);
																		bDirPendingDeleted = true;
 																		archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DeleteFileError", "Couldn't delete file: %s (%d) after %d tries", path_buffer, nErrorCode, nPendingRetries); //  Sleep(10);//(Dispatch message)
																	}
																}
																else
																{
																	if(archivist.m_settings.m_nPendingRetries>0)
																	{
																		archivist.m_data.SetPending(pDirChange);
 																		archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DeleteFileError", "Couldn't delete file: %s (%d), retrying...", path_buffer, nErrorCode);  // Sleep(10);//(Dispatch message)
																	}
																	else //final
																	{
 																		archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DeleteFileError", "Couldn't delete file: %s (%d)", path_buffer, nErrorCode); //  Sleep(10);//(Dispatch message)
																	}
																}
															}
															else  // its a "real" error
															{
																archivist.m_data.DelPending(nPendingID);
																bDirPendingDeleted = true;
 																archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:DeleteFileError", "Couldn't delete file: %s (%d)", path_buffer, nErrorCode);  // Sleep(10);//(Dispatch message)
															}
														}
													}
													// else it wasnt there so ignore it

												}

												pchRoot = NULL;
												if(strlen(pDirChange->m_szFullPath)>strlen(archivist.m_settings.m_pszWatchFolderPath))
												{
													pchRoot = pDirChange->m_szFullPath + strlen(archivist.m_settings.m_pszWatchFolderPath);
													sprintf(path_buffer, "%s%s", archivist.m_settings.m_pszDestinationFolderPath, pchRoot);
												}
												else  // must be in the root watch dir
												{
													sprintf(path_buffer, "%s", archivist.m_settings.m_pszDestinationFolderPath);
												}
												
												// remove trailing slashes, if any
												while(
																(strlen(path_buffer)>0)
															&&(
																	(path_buffer[strlen(path_buffer)-1]=='\\')
																||(path_buffer[strlen(path_buffer)-1]=='/')
																)
															)
												{
													path_buffer[strlen(path_buffer)-1] = 0;
												}
											
												//update the metadata.

		if((pdbConn)&&!(bPendingBackupOnly))
		{
			if(strlen(pDirChange->m_szFullPath)>strlen(archivist.m_settings.m_pszWatchFolderPath))
			{
				pchRoot = pDirChange->m_szFullPath + strlen(archivist.m_settings.m_pszWatchFolderPath);
				sprintf(path_buffer, "%s%s", archivist.m_settings.m_pszDestinationFolderPath, pchRoot);
			}
			else  // must be in the root watch dir
			{
				sprintf(path_buffer, "%s", archivist.m_settings.m_pszDestinationFolderPath);
			}
			
			// remove trailing slashes, if any
			while(
							(strlen(path_buffer)>0)
						&&(
								(path_buffer[strlen(path_buffer)-1]=='\\')
							||(path_buffer[strlen(path_buffer)-1]=='/')
							)
						)
			{
				path_buffer[strlen(path_buffer)-1] = 0;
			}


			char* pchEncodedFilename = db.EncodeQuotes(pDirChange->m_szFilename);
			char* pchEncodedPath = db.EncodeQuotes(path_buffer);

					// removed sys_ingest_date = 0; could be =NULL. but just left it
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
				"UPDATE %s SET sys_file_flags = %d, sys_last_modified_on = %d, sys_last_modified_by = 'sys' WHERE sys_filename = '%s' AND sys_filepath = '%s'", 
				archivist.m_settings.m_pszMetadata?archivist.m_settings.m_pszMetadata:"File_Metadata",
				ARCHIVIST_FILEFLAG_DELETED,
				(unsigned long)(archivist.m_data.m_timebTick.time - (archivist.m_data.m_timebTick.timezone*60) +(archivist.m_data.m_timebTick.dstflag?3600:0)),
				pchEncodedFilename?pchEncodedFilename:pDirChange->m_szFilename,
				pchEncodedPath?pchEncodedPath:path_buffer //pDirChange->m_szFullPath, // in case the path got changed.
				);

			if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
			{
				if(archivist.m_settings.m_bLogTransfers) archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:UpdateMetadata(Delete)", "%s", errorstring);  //(Dispatch message)
				_timeb timestamp;
				_ftime( &timestamp ); 
				timestamp.time+=5;

				while(
								(archivist.m_data.m_timebTick.time < timestamp.time )
							&&(!g_bKillThread)
							&&(!archivist.m_data.m_bProcessSuspended)
							)
				{

					_ftime( &archivist.m_data.m_timebTick ); 
					MSG msg;
					while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
					AfxGetApp()->PumpMessage();
					Sleep(1);
				}

				if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
				{ // give up
					archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:UpdateMetadata(Delete2)", "%s", errorstring);  //(Dispatch message)
				}
			}

			if(pchEncodedFilename) free(pchEncodedFilename);
			if(pchEncodedPath) free(pchEncodedPath);

			// ok, here, we want to match the backup.

			if(
					(archivist.m_settings.m_bUseBackupDeletionSync)
				&&(archivist.m_settings.m_pszWatchFolderPath)
				&&(strlen(archivist.m_settings.m_pszWatchFolderPath))
				&&(archivist.m_settings.m_pszBackupDestinationFolderPath)
				&&(strlen(archivist.m_settings.m_pszBackupDestinationFolderPath))
				&&(archivist.m_settings.m_pszBackupDestinationFolderLocalPath)
				&&(strlen(archivist.m_settings.m_pszBackupDestinationFolderLocalPath))
				&&(strcmp(archivist.m_settings.m_pszWatchFolderPath, archivist.m_settings.m_pszBackupDestinationFolderPath))
				)
			{

				// get backup path.
				sprintf(pszPath, "%s\\%s", pDirChange->m_szFullPath, pDirChange->m_szFilename);
				pchRoot = pszPath + strlen(g_parchivist->m_settings.m_pszWatchFolderPath);
				sprintf(path_buffer, "%s%s", g_parchivist->m_settings.m_pszBackupDestinationFolderPath, pchRoot);

				DWORD dwfa = GetFileAttributes(path_buffer);
				if(dwfa!=0xFFFFFFFF) // then it succeeded, the file was there, etc.
				{
					dwfa&= ~FILE_ATTRIBUTE_READONLY;
					SetFileAttributes(path_buffer, dwfa);
				
					if(DeleteFile(path_buffer))
					{
						if(g_parchivist->m_settings.m_bLogTransfers) g_parchivist->m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:BackupDeleteFile", "Deleted file: %s", path_buffer);  // Sleep(10);//(Dispatch message)
						if(nPendingID>=0)
						{
							if(!bDirPendingDeleted) archivist.m_data.DelPending(nPendingID);
						}
					}
					else
					{
						if(nPendingID>=0)
						{
							if(bDirPendingDeleted) archivist.m_data.SetPending(pDirChange, true); 
						}
						else
						{
							archivist.m_data.SetPending(pDirChange, true); // add the backup one in, but only if the other one is gone
						}
						int nErrorCode = GetLastError();

						if((nErrorCode == ERROR_ACCESS_DENIED )||(nErrorCode == ERROR_SHARING_VIOLATION)||(nErrorCode == ERROR_LOCK_VIOLATION))
						{
							// busy for the moment.
							if(nPendingID>=0)
							{
								if(nPendingRetries<archivist.m_settings.m_nPendingRetries)
								{
									archivist.m_data.ModPending(nPendingID);
								}
								else
								{
									archivist.m_data.DelPending(nPendingID);
 									archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BackupDeleteFileError", "Couldn't delete file: %s (%d) after %d tries", path_buffer, nErrorCode, nPendingRetries);  // Sleep(10);//(Dispatch message)
								}
							}
							else
							{
								if(archivist.m_settings.m_nPendingRetries>0)
								{
									archivist.m_data.SetPending(pDirChange, true);
 									archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BackupDeleteFileError", "Couldn't delete file: %s (%d), retrying...", path_buffer, nErrorCode);  // Sleep(10);//(Dispatch message)
								}
								else //final
								{
 									archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BackupDeleteFileError", "Couldn't delete file: %s (%d)", path_buffer, nErrorCode);  // Sleep(10);//(Dispatch message)
								}
							}
						}
						else  // its a "real" error
						{
							if(!bDirPendingDeleted) archivist.m_data.DelPending(nPendingID);
							archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:BackupDeleteFileError", "Couldn't delete file: %s (%d)", path_buffer, nErrorCode); //  Sleep(10);//(Dispatch message)
						}
					}
				}// else it wasnt there so ignore it
			}
			// then set up the backup db
			if(
					(archivist.m_settings.m_bUseBackupDatabase)
				&&(archivist.m_settings.m_pszBackupDSN)
				&&(strlen(archivist.m_settings.m_pszBackupDSN)>0) // has to be valid
				&&(archivist.m_settings.m_pszBackupUser)
				&&(archivist.m_settings.m_pszBackupPW)
				&&(strcmp(archivist.m_settings.m_pszDSN, archivist.m_settings.m_pszBackupDSN)!=0)  // different DSN
				)
			{
														// deal with metadata.
/////////   **********************************************
////  The following is backup push of metdata.
////  Our file copy is a mapped drive.
				// get the new path.
				if(strlen(pDirChange->m_szFullPath)>strlen(archivist.m_settings.m_pszWatchFolderPath))
				{
					pchRoot = pDirChange->m_szFullPath + strlen(archivist.m_settings.m_pszWatchFolderPath);
					sprintf(path_buffer, "%s%s", archivist.m_settings.m_pszBackupDestinationFolderLocalPath, pchRoot);
				}
				else  // must be in the root watch dir
				{
					sprintf(path_buffer, "%s", archivist.m_settings.m_pszBackupDestinationFolderLocalPath);
				}
				
				// remove trailing slashes, if any
				while(
								(strlen(path_buffer)>0)
							&&(
									(path_buffer[strlen(path_buffer)-1]=='\\')
								||(path_buffer[strlen(path_buffer)-1]=='/')
								)
							)
				{
					path_buffer[strlen(path_buffer)-1] = 0;
				}

				//  using a transient connection to DB.  (dont just keep open).
/*
				CDBconn* pdbBackupConn = db.CreateNewConnection(
					archivist.m_settings.m_pszBackupDSN, 
					archivist.m_settings.m_pszBackupUser, 
					archivist.m_settings.m_pszBackupPW);
		//		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "backup metadata after connect"); Sleep(100); //(Dispatch message)
*/

//				if((pdbBackupConn)&&(db.ConnectDatabase(pdbBackupConn, errorstring)>=DB_SUCCESS))
				if(archivist.m_data.m_pdbBackup)
				{
		//		archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Direct:debug", "backup metadata connected"); Sleep(100); //(Dispatch message)
					pchEncodedFilename = archivist.m_data.m_pdbBackup->EncodeQuotes(pDirChange->m_szFilename);
					pchEncodedPath = archivist.m_data.m_pdbBackup->EncodeQuotes(path_buffer);

					// removed sys_ingest_date = 0; could be =NULL. but just left it
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 
						"UPDATE %s SET sys_file_flags = %d, sys_last_modified_on = %d, sys_last_modified_by = 'sys' WHERE sys_filename = '%s' AND sys_filepath = '%s'", 
						archivist.m_settings.m_pszMetadata?archivist.m_settings.m_pszMetadata:"File_Metadata",
						ARCHIVIST_FILEFLAG_DELETED,
						(unsigned long)(archivist.m_data.m_timebTick.time - (archivist.m_data.m_timebTick.timezone*60) +(archivist.m_data.m_timebTick.dstflag?3600:0)),
						pchEncodedFilename?pchEncodedFilename:pDirChange->m_szFilename,
						pchEncodedPath?pchEncodedPath:path_buffer //pDirChange->m_szFullPath, // in case the path got changed.
						);

					if(archivist.m_data.m_pdbBackup->ExecuteSQL(archivist.m_data.m_pdbBackupConn, szSQL, errorstring)<DB_SUCCESS)
					{
						if(archivist.m_settings.m_bLogTransfers) archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:UpdateBackupMetadata(Delete)", "%s", errorstring);  //(Dispatch message)
						_timeb timestamp;
						_ftime( &timestamp ); 
						timestamp.time+=5;

						while(
										(archivist.m_data.m_timebTick.time < timestamp.time )
									&&(!g_bKillThread)
									&&(!archivist.m_data.m_bProcessSuspended)
									)
						{

							_ftime( &archivist.m_data.m_timebTick ); 
							MSG msg;
							while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
							AfxGetApp()->PumpMessage();
							Sleep(1);
						}

						if(archivist.m_data.m_pdbBackup->ExecuteSQL(archivist.m_data.m_pdbBackupConn, szSQL, errorstring)<DB_SUCCESS)
						{ // give up
							archivist.m_msgr.DM(MSG_ICONERROR, NULL, "Archivist:UpdateBackupMetadata(Delete2)", "%s", errorstring);  //(Dispatch message)
						}
					}

					if(pchEncodedFilename) free(pchEncodedFilename);
					if(pchEncodedPath) free(pchEncodedPath);

//					db.DisconnectDatabase(pdbBackupConn, errorstring);
				} //if(archivist.m_data.m_pdbConnBackup)
//				db.RemoveConnection(pdbBackupConn);
			}
		}
											}
										}
									}
								}
							}
							break;
						default: 
							break;  // unknown
						}


/*
			// debug file
						FILE* fp;
						fp = fopen("_removedlog.log", "at");
						if(fp)
						{
							fprintf(fp, "%s\\%s %s %d %d%d %d%db 0x%08x\r\n", 	
								pDirChange->m_szFullPath?pDirChange->m_szFullPath:"(null)",
								pDirChange->m_szFilename?pDirChange->m_szFilename:"(null)",
								foo,
								pDirChange->m_ulEntryTimestamp,			// unixtime, seconds resolution, last modified
								pDirChange->m_ftFileModified.dwHighDateTime,			// unixtime, seconds resolution, last modified
								pDirChange->m_ftFileModified.dwLowDateTime,			// unixtime, seconds resolution, last modified
								pDirChange->m_ulFileSizeHigh,				// filesize		DWORD    nFileSizeHigh; 
								pDirChange->m_ulFileSizeLow,				// filesize		DWORD    nFileSizeLow;
								pDirChange->m_ulFileAttribs    // attribs returned by GetFileAttributes
								);
							fflush(fp);
							fclose(fp);
						}


						Message("%s\\%s %s %d %d%d %d%db 0x%08x", 	
							pDirChange->m_szFullPath?pDirChange->m_szFullPath:"(null)",
							pDirChange->m_szFilename?pDirChange->m_szFilename:"(null)",
							foo,
							pDirChange->m_ulEntryTimestamp,			// unixtime, seconds resolution, last modified
							pDirChange->m_ftFileModified.dwHighDateTime,			// unixtime, seconds resolution, last modified
							pDirChange->m_ftFileModified.dwLowDateTime,			// unixtime, seconds resolution, last modified
							pDirChange->m_ulFileSizeHigh,				// filesize		DWORD    nFileSizeHigh; 
							pDirChange->m_ulFileSizeLow,				// filesize		DWORD    nFileSizeLow;
							pDirChange->m_ulFileAttribs    // attribs returned by GetFileAttributes
							);
	*/
//	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "***debug***", "about to delete pDirChange");  //(Dispatch message)
//Sleep(200);
						if(pDirChange) delete(pDirChange);
//	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "***debug***", "deleted pDirChange");  //(Dispatch message)
//Sleep(200);
//	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "***debug***", "really deleted pDirChange");  //(Dispatch message)
//Sleep(200);


					} // while changes
//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "%d changes registered", numchanges);  //(Dispatch message)
//Sleep(100);

				}  // there are changes
				archivist.m_data.m_bWatchfolderInitialized = true;
			}  // we are watching
		}  // watchfolder process  and not suspended
		else // could be suspended.  have to end watch.
		{
			if(g_dir.m_bWatchFolder)
			{
				g_dir.EndWatch();
				int nEnd = archivist.m_data.m_timebTick.time+ ARCHIVIST_THREAD_WAIT_MAX;
				while((g_dir.m_ulChangeThreads>0)&&(archivist.m_data.m_timebTick.time<nEnd))
				{
					_ftime( &archivist.m_data.m_timebTick );
					Sleep(100);
				}
				archivist.m_data.m_bWatchfolderInitialized = false;
			}
		}


//archivist.m_msgr.DM(MSG_ICONHAND, NULL, "***debug***", "end loop");  //(Dispatch message)
//Sleep(200);

		Sleep(1);  //just to not peg processor if nothing is happening.

		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();

		Sleep(1);  //just to not peg processor if nothing is happening.
//		Sleep(1000);  // stalls window procedure
	}

	archivist.m_data.m_ulFlags &= ~ARCHIVIST_STATUS_THREAD_MASK;
	archivist.m_data.m_ulFlags |= ARCHIVIST_STATUS_THREAD_END;

	archivist.m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:uninit", "Archivist is shutting down.");  //(Dispatch message)
	archivist.SendMsg(CX_SENDMSG_INFO, "Archivist:uninit", "Archivist %s is shutting down.", ARCHIVIST_CURRENT_VERSION);

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Archivist is shutting down.");  
	archivist.m_data.m_ulFlags &= ~CX_ICON_MASK;
	archivist.m_data.m_ulFlags |= CX_STATUS_UNKNOWN;
	archivist.m_data.SetStatusText(errorstring, archivist.m_data.m_ulFlags);


	g_dir.EndWatch();
	int nEnd = archivist.m_data.m_timebTick.time+ ARCHIVIST_THREAD_WAIT_MAX;
	while((g_dir.m_ulChangeThreads>0)&&(archivist.m_data.m_timebTick.time<nEnd))
	{
		_ftime( &archivist.m_data.m_timebTick );
		Sleep(10);
		if(archivist.m_settings.m_ulDebug&ARCHIVIST_DEBUG_THREADS) archivist.m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:uninit", "Archivist has %d directory threads remaining.", g_dir.m_ulChangeThreads);  //(Dispatch message)
	}

// shut down all the running objects;
/*
	if(archivist.m_data.m_ppConnObj)
	{
		int i=0;
		while(i<archivist.m_data.m_nNumConnectionObjects)
		{
			if(archivist.m_data.m_ppConnObj[i])
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnecting %s...", archivist.m_data.m_ppConnObj[i]->m_pszDesc?archivist.m_data.m_ppConnObj[i]->m_pszDesc:archivist.m_data.m_ppConnObj[i]->m_pszServerName);  

// **** disconnect servers
				archivist.m_data.SetStatusText(errorstring, archivist.m_data.m_ulFlags);
				archivist.m_msgr.DM(MSG_ICONINFO, NULL, "Archivist:connection_uninit", errorstring);    //(Dispatch message)

				delete archivist.m_data.m_ppConnObj[i];
				archivist.m_data.m_ppConnObj[i] = NULL;
			}
			i++;
		}
		delete [] archivist.m_data.m_ppConnObj;
	}
	if(archivist.m_data.m_ppChannelObj)
	{
		int i=0;
		while(i<archivist.m_data.m_nNumChannelObjects)
		{
			if(archivist.m_data.m_ppChannelObj[i])
			{
				delete archivist.m_data.m_ppChannelObj[i];
				archivist.m_data.m_ppChannelObj[i] = NULL;
			}
			i++;
		}
		delete [] archivist.m_data.m_ppChannelObj;
	}
*/

/*
	// here's a hard coded one for now
	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no need to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = archivist.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		archivist.m_net.OpenConnection(archivist.m_http.m_pszHost, 10888, &s); // branding hardcode
		archivist.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		archivist.m_net.CloseConnection(s); // branding hardcode
	}
*/

//	m_pDlg->SetProgress(ARCHIVISTDLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
//	archivist.m_data.m_ulFlags &= ~ARCHIVIST_STATUS_FILESVR_MASK;
//	archivist.m_data.m_ulFlags |= ARCHIVIST_STATUS_FILESVR_END;
//	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down file server....");  
//	archivist.m_data.SetStatusText(errorstring, archivist.m_data.m_ulFlags);
//	_ftime( &archivist.m_data.m_timebTick );
//	archivist.m_http.EndServer();
	_ftime( &archivist.m_data.m_timebTick );
//	AfxMessageBox("shutting down command server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down command server....");  
	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:command_server_uninit", errorstring);  //(Dispatch message)
	archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_CMDSVR_END);
	_ftime( &archivist.m_data.m_timebTick );
	archivist.m_net.StopServer(archivist.m_settings.m_usCommandPort, 5000, errorstring);
	_ftime( &archivist.m_data.m_timebTick );
//	AfxMessageBox("shutting down status server.");
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down XML server....");  
	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:xml_server_uninit", errorstring);  //(Dispatch message)
	archivist.m_data.SetStatusText(errorstring, ARCHIVIST_STATUS_STATUSSVR_END);
	_ftime( &archivist.m_data.m_timebTick );
	archivist.m_net.StopServer(archivist.m_settings.m_usStatusPort, 5000, errorstring);
	_ftime( &archivist.m_data.m_timebTick );
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Archivist is exiting.");  
	archivist.m_msgr.DM(MSG_ICONNONE, NULL, "Archivist:uninit", errorstring);  //(Dispatch message)
	archivist.m_data.SetStatusText(errorstring, archivist.m_data.m_ulFlags);

	g_bKillStatus = true;


	// save settings.  // dont save them here.  save them on any changes in the main command loop.

	archivist.m_settings.Settings(false); //write

	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Archivist is exiting");
	archivist.m_data.m_ulFlags &= ~CX_ICON_MASK;
	archivist.m_data.m_ulFlags |= CX_STATUS_UNINIT;
	archivist.m_data.SetStatusText(errorstring, archivist.m_data.m_ulFlags);


	//exiting
	archivist.m_msgr.DM(MSG_ICONINFO, NULL, "Archivist", "-------------- Archivist %s exit ---------------\n\
--------------------------------------------------\n", ARCHIVIST_CURRENT_VERSION);  //(Dispatch message)
///	m_pDlg->SetProgress(ARCHIVISTDLG_CLEAR); // no point

	_ftime( &archivist.m_data.m_timebTick );
	archivist.m_data.m_ulFlags &= ~ARCHIVIST_STATUS_THREAD_MASK;
	archivist.m_data.m_ulFlags |= ARCHIVIST_STATUS_THREAD_ENDED;
	Sleep(250); // let the message get there.
	archivist.m_msgr.RemoveDestination("log");
	archivist.m_msgr.RemoveDestination("email");

	archivist.m_data.m_pdb = NULL;
	archivist.m_data.m_pdb2 = NULL;
	archivist.m_data.m_pdbBackup = NULL;
	if(pdbConn)
	{	
		db.DisconnectDatabase(pdbConn, errorstring);
		delete(pdbConn);
		pdbConn = NULL;
		archivist.m_data.m_pdbConn = NULL;
	}
	if(pdb2Conn)
	{	
		db2.DisconnectDatabase(pdb2Conn, errorstring);
		delete(pdb2Conn);
		pdb2Conn = NULL;
		archivist.m_data.m_pdb2Conn = NULL;
	}
	if(pdbBackupConn)
	{	
		db.DisconnectDatabase(pdbBackupConn, errorstring);
		delete(pdbBackupConn);
		pdbBackupConn = NULL;
		archivist.m_data.m_pdbBackupConn = NULL;
	}
	
	int nClock = clock() + 300; // small delay at end
	while(clock()<nClock)	{_ftime( &archivist.m_data.m_timebTick );}
	g_bThreadStarted = false;
	nClock = clock() + archivist.m_settings.m_nThreadDwellMS; // another small delay at end
	while(clock()<nClock)	{_ftime( &archivist.m_data.m_timebTick );}
	g_parchivist = NULL;
	Sleep(300); //one more small delay at end
	_endthread();
}

/*
void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((g_pmsgr)&&(g_pmsgr->m_lpfnDM))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		CMessagingObject msg;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = msg.EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = msg.EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = msg.EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch;
		pch = (char*) malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
* /
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if(pszEncodedMsg) 
			{
				ulBufferLen = strlen(pszEncodedMsg);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedCaller)
			{
				ulBufferLen = strlen(pszEncodedCaller);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedDest)
			{
				ulBufferLen = strlen(pszEncodedDest);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			g_pmsgr->m_lpfnDM(pch, ulFlags);
			free(pch);
		}
	}
}
*/


/*
// this thread is based on void HTTP10HandlerThread(void* pvArgs) from
// HTTP10.cpp.  It makes a special webserver with security, cgi parsing, and dynamic results
void ArchivistHTTPThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return;}

	CArchivistMain* pArchivist = (CArchivistMain*)(pClient->m_lpObject) ;  // pointer to the global object.

// HTTP service by def is a non-persistent service, so even if the Client type is
// set to persist, we ignore that directive in this handler thread

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;


		CHTTP10* phttp = (CHTTP10*)(&(pArchivist->m_http)) ;  // pointer to the global object.
		if(phttp == NULL)// cant do anything!
		{
			shutdown(pClient->m_socket, SD_BOTH);
			closesocket(pClient->m_socket);
			(*(pClient->m_pulConnections))--;
			_endthread(); return;
		}

		CHTTP10 http(phttp->m_pszRoot);  // local object for utility functions ONLY.
		// this allows the local to clone the root setting of the global, 
		// in order to do correct parsing in its own thread

		// the following are just pointers to the global obj.  
		// must NULL these out before object gets destroyed, 
		// so that global resources not freed!   !!!!!!!  important !!!!!
		http.m_pszHost = phttp->m_pszHost;
		http.m_ppszErrors = phttp->m_ppszMime;
		http.m_ppszMime = phttp->m_ppszMime;   
		http.m_ulStatus = phttp->m_ulStatus;   // important for redirect and error
		
		unsigned char* pch;// for use with GetLine().
		unsigned long ulBufLen;// for use with GetLine().
		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		unsigned short usCode = 0;

		// get the raw buffer and process it yourself
		nReturn = http.m_net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_EOLN|EOLN_HTTP, pszStatus);
		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). 
			//was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
		{
			//error.
			if(pClient->m_lpMsgObj)
			{
				_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error receiving data.  %s", pszStatus);
				((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
			}

			usCode = 400;  //  we TRY to send this....
			if(pch!=NULL)
			{
				free(pch);  // must free the incoming data buffer 
				pch = NULL;
			}
			// create a return message buffer.
			pch = (unsigned char*)http.ErrorBuffer(NULL, usCode);   // 400 bad request
			ulBufLen = strlen((char*)pch); // valid from an error page.

			// send malformed request answer if possible  - have to tell the client that the request was not received in good order.
			nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
			if(nReturn<NET_SUCCESS)
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
				}
			}
		}
		else  // successful reception of data.
		{
			// process the data here...
			// in this sample the buffer is loaded into
			// pch, with ulBufLen as length

/*
			FILE* fp = fopen("rec.txt", "ab");
			if (fp)
			{
				fwrite(pch, 1, ulBufLen, fp );
				fflush(fp);
				fclose(fp);
			}
* /
			// parse buffer, format reply.
			CHTTPHeader header;

			if (http.ParseHeader(pch, ulBufLen, &header) < HTTP_SUCCESS)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("header not parsed\r\n\r\n", 1, strlen("header not parsed\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}
* /
				
				if(pch!=NULL) 
				{
					free(pch);  // must free the incoming data buffer 
					pch = NULL;
				}
				// create a return message buffer.
				usCode = 400;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}
			else
			{
				// here we have enough info to check security if desired, and if not, return a 401;
				// else continue....
/*
					FILE* fp = fopen("rec.txt", "ab");
					if (fp)
					{// lets write out the whole header so we can examine its full contents. including args.
						char bufferin[10000];
						sprintf(bufferin,
"header successfully parsed\r\n\
method: %d\r\n\
version: %s\r\n\
URL: %s\r\n\
Path: %s\r\n\
mimetype: %s\r\n\
user: %s\r\n\
password: %s\r\n\
num args: %ld\r\n",
	header.m_ucMethod,  // enumerated values
	header.m_pszVersion==NULL?"(null)":header.m_pszVersion,
	header.m_pszURL==NULL?"(null)":header.m_pszURL,
	header.m_pszPath==NULL?"(null)":header.m_pszPath,
	header.m_pszMimeType==NULL?"(null)":header.m_pszMimeType,
	header.m_pszUser==NULL?"(null)":header.m_pszUser,
	header.m_pszPassword==NULL?"(null)":header.m_pszPassword,
	header.m_ulNumArgs

);
		
	unsigned long k=0;
	while(k<header.m_ulNumArgs)
	{
		if((header.m_ppszArgNames)&&(header.m_ppszArgValues))
		{
			strcat(bufferin, "  ");
			strcat(bufferin, header.m_ppszArgNames[k]==NULL?"(null)":header.m_ppszArgNames[k]);
			strcat(bufferin, "=");
			strcat(bufferin, header.m_ppszArgValues[k]==NULL?"(null)":header.m_ppszArgValues[k]);
			strcat(bufferin, "\r\n");
		}
	k++;
	}

	strcat(bufferin, "headers:\r\n");
	strcat(bufferin, header.m_pszHeaders);
	strcat(bufferin, "\r\n");


			//		fwrite("header successfully parsed\r\n", 1, strlen("header successfully parsed\r\n"), fp );
						fwrite(bufferin, 1, strlen(bufferin), fp );
						fflush(fp);
						fclose(fp);
					}

* /


				if((phttp->m_pSecure)&&((header.m_pszUser==NULL)||(header.m_pszPassword==NULL)))
				{
					usCode = 401;
					pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
					ulBufLen = strlen((char*)pch); // valid from an error page.
					// buffer successfully parsed.
				}
				else 
				{
					if(pch!=NULL)
					{
						free(pch);  // must free the incoming data buffer 
						pch = NULL;
					}

					// create a return message buffer.
					switch(header.m_ucMethod)
					{
					case HTTP_GET://			1  // HTTP/1.0
					case HTTP_HEAD://			2  // HTTP/1.0
						{
							if(
									(strnicmp(header.m_pszPath, http.m_pszRoot, strlen(http.m_pszRoot)) == 0 )  // else not a part of the served file tree
								)
							{
								if(phttp->m_pSecure)
								{
									// have to check the main object for permission.
									// we are going to go by URL (asset level)
									// Loci are going to just be convenient names for the admins to set up.

									if((phttp->m_pSecure->CheckSecure(header.m_pszUser, header.m_pszPassword, NULL, header.m_pszURL))!=SECURE_SUCCESS)
									{
										usCode = 401;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
										ulBufLen = strlen((char*)pch); // valid from an error page.
										break; // get out of the switch from here.
									}
									// else it flows thru....
								}

								DWORD dwAttrib;

								dwAttrib = GetFileAttributes(header.m_pszPath);

								// check for directory (redirect nec.)
								if ( (dwAttrib!=0xffffffff)&&(dwAttrib&FILE_ATTRIBUTE_DIRECTORY) )  // if error, it may be because no file exists, so check in the next part.
								{
									usCode = 301;  // not necessary but why not.
									pch = (unsigned char*)http.RedirectBuffer(&header);
									ulBufLen = strlen((char*)pch); // valid from an redirect page.
								}
								else
								{
									// before we try to open the file, lets spend the time now (not while the file is open)
									// to figure out how long the static text respone buffer will be.
									CBufferUtil bu;
									// notice the sample "Server: ArchivistServer/3.0.3.1" header included.  Pick the appname and ver, probably best
									unsigned long ulBufferLen = strlen("HTTP/1.0 200 OK\r\nServer: ArchivistServer/\r\nContent-Length: \r\n\r\n");
									ulBufferLen += strlen(ARCHIVIST_CURRENT_VERSION);

									if((header.m_pszMimeType)&&(strlen(header.m_pszMimeType)>0))
									{
										ulBufferLen += (strlen("Content-Type: \r\n") + strlen(header.m_pszMimeType)); // only add if known!
									}
									else // empty!
									{
										// dont free the "permanent resource", just null the pointer
										header.m_pszMimeType=NULL;
									}

									FILE* pFile = NULL;

									pFile = fopen(header.m_pszPath, "rb");
									if (pFile == NULL)  // file not openable, or not found.
									{
										usCode = 404;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 404 not found
										ulBufLen = strlen((char*)pch); // valid from an error page.
									}
									else
									{
										// this part just assembles the file into a buffer.
										// this may be modified to do dynamic html things by using the 
										// pClient->m_lpObject that refers back to some parser....

										usCode = 200; // OK  (!)
										// determine file size
										fseek(pFile, 0, SEEK_END);
										unsigned long ulFileLen = ftell(pFile);


										// here we have to parse out any directives.  if this is a template file,
										// we may have a variable length.
									//	x


										if(header.m_ucMethod == HTTP_GET)
										{
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += (strlen(length)+ulFileLen);

											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you truncations at zeroed bytes

											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: ArchivistServer/%s", ARCHIVIST_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
											
											fclose(pFile);
										}
										else
										if(header.m_ucMethod == HTTP_HEAD)
										{
											fclose(pFile);  // we have all we need already.
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += strlen(length);
											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											//  create the header
											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: ArchivistServer/%s", ARCHIVIST_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												// its identical except for the body.  next line must stay commented out, its just there for ref.
												//fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
										}
									}
								}
							}
							else  // not part of the server service.
							{
								usCode = 403;
								pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 403 forbidden ...
								ulBufLen = strlen((char*)pch); // valid from an error page.
							}

						} break;
					case HTTP_POST://			3  // HTTP/1.0
						{
							usCode = 501;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 501 not implemented ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_PUT://			4  // HTTP/1.1
					case HTTP_DELETE://		5  // HTTP/1.1
					case HTTP_OPTIONS://  6  // HTTP/1.1
					case HTTP_TRACE://	  7  // HTTP/1.1
						{
							usCode = 505;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 505 http version not supported ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_UNDEF://		0  // internal
					default:
						{
							usCode = 400;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					}
				}
			}

			// send a reply, in pch., with strlen pch as length

			if(pch == NULL)  // had a problem forming the request, try to send a last ditch error code.
			{
				usCode = 500;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 500 internal server error
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}

			if(pch != NULL)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("sending:\r\n", 1, strlen("sending:\r\n"), fp );
					fwrite(pch, 1, ulBufLen, fp );
					fwrite("\r\n\r\n", 1, strlen("\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}

* /
				nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
				if(nReturn<NET_SUCCESS)
				{
					//error.
					if(pClient->m_lpMsgObj)
					{
						_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
						((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
					}
				}
			}
		}

//		char foo[34]; sprintf(foo, "code: %d", usCode);	AfxMessageBox(foo);

		http.m_pszHost = NULL;		// but do NOT free resource
		http.m_ppszErrors = NULL; // but do NOT free resource
		http.m_ppszMime = NULL;   // but do NOT free resource

		if(pch!=NULL) free(pch);  // must free the data buffer 

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}
*/


void ArchivistCommandHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure in Protocol 1  //SECURE with PROTOCOL2 later
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Archivist:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Archivist:CommandHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{
//AfxMessageBox("receiving");

					// parse the command:

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					switch(data.m_ucCmd)
					{
					case CX_CMD_GETINFO: // not actually supported yet.  everything thru csf files for now.
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case CX_CMD_GETSTATUS: //	0x99 // gets status info
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							unsigned long ulStatus;
							// assemble if there is 

							char* pch = g_parchivist->m_data.GetStatusText(&ulStatus);  // allocates mem, must free after use
							if(pch)
							{
								data.m_ucSubCmd = (unsigned char)(ARCHIVIST_ICON_MASK&ulStatus);
								data.m_pucData=(unsigned char*)pch;
								data.m_ulDataLen = strlen(pch);
							}
							else
							{
								data.m_ucCmd = NET_CMD_NAK;
								data.m_ucSubCmd = NET_CMD_NAK;
							}

							data.m_ucType = NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					case CX_CMD_BYE:
						{
	g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, "Archivist", "The Global Kill command has been received.");  //(Dispatch message)

							bSetGlobalKill = true;
							g_parchivist->m_data.m_bQuietKill = true;
							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					default:
						{
							//just ack
							data.m_ucCmd = NET_CMD_ACK;

		//					char repbuf[64];
							if(data.m_pucData!=NULL)
							{
		/*
								_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

								for(unsigned long q=0; q<data.m_ulDataLen; q++)
								{
									if(strlen(repbuf)<63)
										strncat(repbuf, (char*)(data.m_pucData+q), 1);
								}
								if(strlen(repbuf)<63)
									strcat(repbuf, "]");
								data.m_ulDataLen = strlen(repbuf);
								repbuf[data.m_ulDataLen] = 0;  // just in case
		*/
								free(data.m_pucData);  //destroy the buffer;
							}
		/*
							else
							{
								_snprintf(repbuf, 63, "data was NULL");
								data.m_ulDataLen = strlen(repbuf); 
							}

							data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
							if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
							else 
							{
								data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
								data.m_ulDataLen = 0;
							}
		*/
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					}

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Archivist:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Archivist:CommandHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.
	if (bSetGlobalKill)	//g_bKillThread = true;
	((CArchivistHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}

void ArchivistStatusHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Archivist:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Archivist:CommandHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Archivist:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Archivist:CommandHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}


int CArchivistMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_data.m_pdb)&&(m_data.m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)&&(strlen(m_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_data.m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_data.m_pdb->EncodeQuotes(pszSender);

		if(m_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime) \
VALUES ('%s', '%s', %d, %d.%03d)", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				m_settings.m_pszMessages
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		if(m_data.m_pdb->ExecuteSQL(m_data.m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return ARCHIVIST_SUCCESS;
		}
	}
	return ARCHIVIST_ERROR;
}

/*
void ArchivistConnectionThread(void* pvArgs)
{
			g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ArchivistConnectionThread");  Sleep(250); //(Dispatch message)
	CArchivistConnectionObject* pConn = (CArchivistConnectionObject*) pvArgs;
	if(pConn==NULL) 
	{
		//**MSG
		return;
	}
	pConn->m_bConnThreadStarted = true;
	unsigned long ulTick = GetTickCount();
	unsigned long ulElapsedTick = GetTickCount();
			g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ArchivistConnectionThread2");  Sleep(250); //(Dispatch message)
	while(!pConn->m_bKillConnThread)
	{

		if(	pConn->m_pAPIConn )
		{
			g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "getting status");  Sleep(250); //(Dispatch message)
			// we can monitor the connection and get status
			EnterCriticalSection(&g_adc.m_crit);
			g_adc.GetStatusData(pConn->m_pAPIConn);
			LeaveCriticalSection(&g_adc.m_crit);
			ulTick = GetTickCount();

			if(pConn->m_pAPIConn->m_Status > 0)  // connected
			{
				// here we need to monitor list states and threads
				int nChannels = 0;

				while(nChannels<g_parchivist->m_data.m_nNumChannelObjects)
				{
					if((g_parchivist->m_data.m_ppChannelObj)&&(g_parchivist->m_data.m_ppChannelObj[nChannels]))
					{
						if(strcmp(g_parchivist->m_data.m_ppChannelObj[nChannels]->m_pszServerName, pConn->m_pszServerName)==0) // we are on the right adc conn.
						{
							//if the list is active, check to see if there is a thread going.
							if(g_parchivist->m_data.m_ppChannelObj[nChannels]->m_ulFlags&ARCHIVIST_FLAG_ENABLED)
							{
								if(
									  (g_parchivist->m_data.m_ppChannelObj[nChannels]->m_nHarrisListID<0)
									||(g_parchivist->m_data.m_ppChannelObj[nChannels]->m_nHarrisListID>=MAXSYSLISTS)
									)
								{
									// error!
									g_parchivist->m_data.m_ppChannelObj[nChannels]->m_ulFlags &= ~ARCHIVIST_FLAG_ENABLED;  //disable

								}
								else
								if( !g_parchivist->m_data.m_ppChannelObj[nChannels]->m_bChannelThreadStarted)
								{ // start the thread!
									g_parchivist->m_data.m_ppChannelObj[nChannels]->m_bKillChannelThread=false;
									g_parchivist->m_data.m_ppChannelObj[nChannels]->m_pAPIConn=pConn->m_pAPIConn;
									g_parchivist->m_data.m_ppChannelObj[nChannels]->m_pbKillConnThread=&(pConn->m_bKillConnThread);

			g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "beginning list thread");  Sleep(250); //(Dispatch message)
									if(_beginthread(ArchivistListThread, 0, (void*)g_parchivist->m_data.m_ppChannelObj[nChannels])==-1)
									{
										//error.
							
			g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "problem beginning list thread");  Sleep(250); //(Dispatch message)
										//**MSG
									}
								}
							}
							else  // disabled
							{
								if( g_parchivist->m_data.m_ppChannelObj[nChannels]->m_bChannelThreadStarted)
								{
									// end the thread.
			g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ending list thread");  Sleep(250); //(Dispatch message)
									g_parchivist->m_data.m_ppChannelObj[nChannels]->m_bKillChannelThread=true;
								}
							}
						}
					}
					nChannels++;
				}
			}
			else
			{
				g_adc.DisconnectServer(pConn->m_pAPIConn);  // just removes from adc list.
				pConn->m_ulStatus = ARCHIVIST_STATUS_ERROR;  // due to disconnection.  resets on connect
				pConn->m_pAPIConn = NULL; // break out.
			}

			if(pConn->m_pAPIConn)
			{
				ulElapsedTick = GetTickCount();
				if(ulElapsedTick<ulTick) //wrapped
				{
					Sleep(((pConn->m_pAPIConn->m_SysData.systemfrx) == 0x24)?40:33);
				}
				else
				{
					ulElapsedTick -= ulTick;
					Sleep(((pConn->m_pAPIConn->m_SysData.systemfrx) == 0x24)?40:33 - ulElapsedTick);
				}
			}
			//else break out and try to reconnect.
		}
		else
		{
			if(pConn->m_ulFlags&ARCHIVIST_FLAG_ENABLED) // active.
			{
				if((pConn->m_pszClientName)&&(strlen(pConn->m_pszClientName)<=0)) 
				{
					free(pConn->m_pszClientName);
					pConn->m_pszClientName = NULL;
				}
				if(!pConn->m_pszClientName)
				{
					pConn->m_pszClientName = (char*)malloc(strlen("Archivist")+1);
					if(pConn->m_pszClientName) strcpy(pConn->m_pszClientName, "Archivist");
				}
					
				CAConnection* pAPIConn = g_adc.ConnectServer(pConn->m_pszServerName, pConn->m_pszClientName);
				if(pAPIConn)
				{
					pConn->m_pAPIConn = pAPIConn; // connected.
					pConn->m_ulStatus = ARCHIVIST_STATUS_CONN;
				}
			} // else don't connect, just 
			else
			{
				pConn->m_ulStatus = ARCHIVIST_STATUS_NOTCON;
			}

			Sleep(2000);// wait two seconds after a connection attempt;
		}
			
	}

	g_adc.DisconnectServer(pConn->m_pszServerName);

	pConn->m_bConnThreadStarted = false;
	_endthread();
}

void ArchivistListThread(void* pvArgs)
{
	g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ArchivistChannelThread");  Sleep(250); //(Dispatch message)
	CArchivistChannelObject* pChannel = (CArchivistChannelObject*) pvArgs;
	if(pChannel==NULL)
	{
		//**MSG
		return;
	}
	bool* pbKillConnThread = pChannel->m_pbKillConnThread;
	if(pbKillConnThread==NULL)  // the address, not what it points to
	{
		//**MSG
		return;
	}
	if(	pChannel->m_pAPIConn ) // need this to do anything.  // could get it by looping through g_adc.... later.
	{
		//**MSG
		return;
	}
	pChannel->m_bChannelThreadStarted = true;


	// lets set up some convenience vars;
	int nZeroList = pChannel->m_nHarrisListID-1;


	if((nZeroList<0)||(nZeroList>=MAXSYSLISTS)) // error on list number.
	{
		//**MSG
		return;
	}

	tlistdata* plistdata = &(pChannel->m_pAPIConn->m_ListData[nZeroList]);
	TConnectionStatus* pstatus =  &(pChannel->m_pAPIConn->m_Status);
	CAList** ppList = &(pChannel->m_pAPIConn->m_pList[nZeroList]);
	CAList*  pList = pChannel->m_pAPIConn->m_pList[nZeroList];  // volatile, always reassign
	CDBUtil* pdb = g_parchivist->m_data.m_pdb;
	CDBconn* pdbConn = g_parchivist->m_data.m_pdbConn;

	pChannel->m_pAPIConn->m_bListActive[nZeroList] = true;
//	unsigned long ulTick = GetTickCount();
//	unsigned long ulElapsedTick = GetTickCount();
			g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, "Archivist:debug", "ArchivistChannelThread2");  Sleep(250); //(Dispatch message)

	tlistdata listdata;
	tlistdata lastlistdata;  // for comparison

  lastlistdata.listchanged=0;				// incremented whenever list size changes
  lastlistdata.listdisplay=0;				// incremented whenever an event changes
  lastlistdata.listsyschange=0;			// incremented when list related values are changed. 
  lastlistdata.listcount=0;					// number of events in list 
  lastlistdata.liststate=0;
			
	while((!pChannel->m_bKillChannelThread)&&((*pbKillConnThread)!=true))
	{
//		EnterCriticalSection(&g_adc.m_crit);
//		g_adc.GetStatusData(pConn->m_pAPIConn);
//		LeaveCriticalSection(&g_adc.m_crit);
		if(*pstatus > 0)  // have to be connected.
		{
			// snapshot the live list data
			EnterCriticalSection(&g_adc.m_crit);
			memcpy(&listdata, plistdata, sizeof(tlistdata));
			LeaveCriticalSection(&g_adc.m_crit);

			if((listdata.listchanged!=lastlistdata.listchanged)||(listdata.listdisplay!=lastlistdata.listdisplay)) // just do the event chage one as well for now.
			{
				// the list has changed, need to get all the events again  (will take care of any event changes as well)
				int nNumEvents = g_adc.GetEvents(pChannel->m_pAPIConn,
					pChannel->m_nHarrisListID, 
					0, //start at the top.
					g_parchivist->m_settings.m_bUseListCount?listdata.listcount:listdata.lookahead);

				EnterCriticalSection(&(pList->m_crit));  // lock the list out
				pList = (*ppList);  // volatile, must reassign
				if((nNumEvents>=ADC_SUCCESS)&&(pList))
				{
					// deal with database.
					if((pdb!=NULL)&&(pdbConn!=NULL)&&(g_parchivist->m_settings.m_pszLiveEvents)&&(strlen(g_parchivist->m_settings.m_pszLiveEvents)>0))
					{
						char dberrorstring[DB_ERRORSTRING_LEN];
						char szSQL[DB_SQLSTRING_MAXLEN];

						double dblLastPrimaryEventTime = -1.0;
						unsigned short usLastPrimaryOnAirJulianDate = 0xffff;
						unsigned long ulLastPrimaryOnAirTimeMS = 0xffffffff;
						double dblServerTime = (double)pChannel->m_pAPIConn->m_usRefJulianDate - 25567.0   // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
										+ (double)pChannel->m_pAPIConn->m_ulRefTimeMS/1000.0;

						int n=0;
						while(n<nNumEvents)
						{

							if((pList->m_ppEvents)&&(pList->m_ppEvents[n])&&(pList->m_ppEvents[n]->m_pszID)&&(strlen(pList->m_ppEvents[n]->m_pszID)))
							{

								double dblEventTime = 0.0; 
								unsigned short usOnAirJulianDate = pList->m_ppEvents[n]->m_usOnAirJulianDate;

								if(usOnAirJulianDate == 0xffff)
								{
									_timeb timestamp;
									_ftime(&timestamp);

									usOnAirJulianDate = pChannel->m_pAPIConn->m_usRefJulianDate;  // this is a guess.

									dblEventTime = (double)usOnAirJulianDate - 25567.0   // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
										+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
								}
								else
								{
									dblEventTime = (double)pList->m_ppEvents[n]->m_usOnAirJulianDate - 25567.0   // offset between jan 1 1900 and jan 1 1970 (70 years = 25567 days)
										+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
								}
									  

								unsigned short usType = pList->m_ppEvents[n]->m_usType;

								if(usType&SECONDARYEVENT)
								{
									if(ulLastPrimaryOnAirTimeMS>0.0)
									{
										dblEventTime = dblLastPrimaryEventTime 
											+ (double)pList->m_ppEvents[n]->m_ulOnAirTimeMS/1000.0;
										usOnAirJulianDate = usLastPrimaryOnAirJulianDate;
										if(pList->m_ppEvents[n]->m_ulOnAirTimeMS + ulLastPrimaryOnAirTimeMS >= 86400000 )
										{
											//wrapped days.
											usOnAirJulianDate++;
										}
									}
								}
								else
								{
									ulLastPrimaryOnAirTimeMS = pList->m_ppEvents[n]->m_ulOnAirTimeMS;
									dblLastPrimaryEventTime = dblEventTime;
									usLastPrimaryOnAirJulianDate = usOnAirJulianDate;
								}

//						select event with server, list, start time +/- tolerance, and clip ID
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s WHERE \
server_name = '%s' AND \
server_list = %d AND \
event_clip = '%s' AND \
event_start >= %f AND \
event_start <= %f;",
										g_parchivist->m_settings.m_pszLiveEvents,
										pChannel->m_pAPIConn->m_pszServerName,
										pChannel->m_nHarrisListID,
										pList->m_ppEvents[n]->m_pszID,
										dblEventTime-0.3, dblEventTime+0.3
									);

								// get
								CRecordset* prs = pdb->Retrieve(pdbConn, szSQL, dberrorstring);
								int nNumFound = 0;
								int nID = -1;
								if(prs != NULL) 
								{
									if(!prs->IsEOF())
									{
										// get id!
										CString szValue;
										prs->GetFieldValue("(itemid", szValue);
										nID = atoi(szValue);
										nNumFound++;
										prs->MoveNext();
									}
									prs->Close();
									delete prs;
								}

								if((nNumFound>0)&&(nID>0))
								{
									// update it.
									// if found update it with new values, and a found flag (app_data_aux int will be the found flag 1=found)
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
server_name = '%s', \
server_list = %d, \
list_id = %d, \
event_id = '%s', \
event_clip = '%s', \
event_title = '%s', \
%s%s%s\
event_type = %d, \
event_status = %d, \
event_time_mode = %d, \
event_start = %f, \
event_duration = %d, \
event_last_update = %f, \
app_data_aux = 1 WHERE itemid = %d;",  // 1 is the found flag.
										g_parchivist->m_settings.m_pszLiveEvents,  // table name
										pChannel->m_pAPIConn->m_pszServerName, //server_name
										pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
										pChannel->m_nChannelID, // list_id - internal lookup channel number
										pList->m_ppEvents[n]->m_pszReconcileKey?pList->m_ppEvents[n]->m_pszReconcileKey:"", //event_id
										pList->m_ppEvents[n]->m_pszID, //event_clip  (must not be null)
										pList->m_ppEvents[n]->m_pszTitle?pList->m_ppEvents[n]->m_pszTitle:"", //event_title
										pList->m_ppEvents[n]->m_pszData?"event_data = '":"", //event_data (exists)
										pList->m_ppEvents[n]->m_pszData?pList->m_ppEvents[n]->m_pszData:"", //event_data  
										pList->m_ppEvents[n]->m_pszData?"', ":"", //event_data (exists)
										pList->m_ppEvents[n]->m_usType, //event_type
										pList->m_ppEvents[n]->m_usStatus, //event_status
										pList->m_ppEvents[n]->m_usControl, //event_time_mode
										dblEventTime, //event_start
										pList->m_ppEvents[n]->m_ulDurationMS, //event_duration
										dblServerTime, //event_last_update
										nID // the unique ID of the thing
									);
// app_data = , // no app data for the above, let it be NULL or whatever it is.


								}
								else
								{
									// add it.
									// if not found, insert one with a found flag
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
server_name, \
server_list, \
list_id, \
event_id, \
event_clip, \
event_title, \
%s\
event_type, \
event_status, \
event_time_mode, \
event_start, \
event_duration, \
event_last_update, \
app_data_aux) VALUES ('%s', %d, %d, '%s', '%s', '%s', %s%s%s%d, %d, %d, %f, %d, %f, 1);",  // 1 is the found flag.
										g_parchivist->m_settings.m_pszLiveEvents,  // table name
										pList->m_ppEvents[n]->m_pszData?"event_data, ":"", //event_data (exists)
										pChannel->m_pAPIConn->m_pszServerName, //server_name
										pChannel->m_nHarrisListID, // server_list - actual 1-based harris list number
										pChannel->m_nChannelID, // list_id - internal lookup channel number
										pList->m_ppEvents[n]->m_pszReconcileKey?pList->m_ppEvents[n]->m_pszReconcileKey:"", //event_id
										pList->m_ppEvents[n]->m_pszID, //event_clip  (must not be null)
										pList->m_ppEvents[n]->m_pszTitle?pList->m_ppEvents[n]->m_pszTitle:"", //event_title
										pList->m_ppEvents[n]->m_pszData?"'":"", //event_data (exists)
										pList->m_ppEvents[n]->m_pszData?pList->m_ppEvents[n]->m_pszData:"", //event_data  
										pList->m_ppEvents[n]->m_pszData?"', ":"", //event_data (exists)
										pList->m_ppEvents[n]->m_usType, //event_type
										pList->m_ppEvents[n]->m_usStatus, //event_status
										pList->m_ppEvents[n]->m_usControl, //event_time_mode
										dblEventTime, //event_start
										pList->m_ppEvents[n]->m_ulDurationMS, //event_duration
										dblServerTime //event_last_update
										);

								}

								if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
								{
									//**MSG
								}
							}
							n++;
						}

						///////////////////////
						// remove found flags.

//					remove all with no found flag.

						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
server_name = '%s' AND \
server_list = %d AND \
app_data_aux < 1;",
										g_parchivist->m_settings.m_pszLiveEvents,  // table name
										pChannel->m_pAPIConn->m_pszServerName, //server_name
										pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
									);
						if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
							//**MSG
						}
//					remove all found flags for next go-around.
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
app_data_aux = 0 WHERE \
server_name = '%s' AND \
server_list = %d;",
										g_parchivist->m_settings.m_pszLiveEvents,  // table name
										pChannel->m_pAPIConn->m_pszServerName, //server_name
										pChannel->m_nHarrisListID // server_list - actual 1-based harris list number
									);

						if (pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
						{
							//**MSG
						}

					
						// increment exchange mod counter. for events table.
						if (g_parchivist->m_data.IncrementDatabaseMods(g_parchivist->m_settings.m_pszLiveEvents, dberrorstring)<ARCHIVIST_SUCCESS)
						{
							//**MSG
						}

					}
					// record last values if get was successful.
					memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
				}
				LeaveCriticalSection(&(pList->m_crit));
			}
/*			else
			if(listdata.listdisplay!=lastlistdata.listdisplay)
			{
				// an event has changed, need to find it and just deal with that one.
				// record last values
				memcpy(&lastlistdata, &listdata, sizeof(tlistdata));
			}
* /
		}

	}
	pChannel->m_pAPIConn->m_bListActive[nZeroList] = false;
	pChannel->m_bChannelThreadStarted = false;
	_endthread();
}
*/


void ArchivistXMLHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;
	
	char szArchivistSource[MAX_PATH]; 
	strcpy(szArchivistSource, "ArchivistXMLHandler");

	CBufferUtil bu;

	HRESULT hRes;
	try
	{  

#if _WIN32_WINNT >= 0x0400
		hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		hRes = CoInitialize(NULL);
#endif
	}
	catch(...)
	{
		if(g_parchivist) g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, szArchivistSource, "Exception in CoInitialize");  //(Dispatch message)
	}


	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		char dberrorstring[DB_ERRORSTRING_LEN];
//		char xmlerrorstring[MAX_MESSAGE_LENGTH];
		char errorstring[MAX_MESSAGE_LENGTH];
//		char szSQL[DB_SQLSTRING_MAXLEN];
		strcpy(dberrorstring, "");
		strcpy(errorstring, "");

//		char element[MAX_MESSAGE_LENGTH];

//		AfxMessageBox(szArchivistSource);

		CNetUtil net(false); // local object for utility functions.
		CCortexUtil util;  // cortex utility functions.

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		bool bCloseCommand = false;  // make these not persistent unless a persist command comes in.
		bool bPersist = false;  // make these not persistent unless a persist command comes in.
		int nPeriodic = -1;  
		int nTimed = -1;  

		char* pchBuffer = NULL;
		unsigned long ulBufferLen = 0;
		char* pch = NULL;
		char* pchXML = NULL;
		char* pchXMLStream = NULL;
		unsigned long ulAccumulatedBufferLen = 0;
		unsigned long ulRetry = 0;

		unsigned long ulConnLastMessage = 0;
		unsigned long ulConnTimeout = 0;
		
		char filename[MAX_PATH];
		char lastrxfilename[MAX_PATH];
		char lasttxfilename[MAX_PATH];
		int nRxDupes=0;
		int nTxDupes=0;

		FILE* fp = NULL;

		_timeb timestamp;
		_timeb timeactive;
		_timeb timeperiodic;

		_ftime(&timeactive);
		_ftime(&timeperiodic);


	// 	(*(pClient->m_pulConnections))++;//already exists

		IXMLDOMDocumentPtr pDoc(__uuidof(DOMDocument));  // let's have only one object per connection
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread D"); Sleep(100); //(Dispatch message)
		HRESULT hr;//= pDoc.CreateInstance(__uuidof(DOMDocument));
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread E");  //(Dispatch message)
		hr = pDoc->put_async(VARIANT_FALSE);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread F");  //(Dispatch message)
										
		// Remove document safety options
		IObjectSafety* pSafety=NULL;
		DWORD dwSupported, dwEnabled;
			

		if ( SUCCEEDED(pDoc->QueryInterface(IID_IObjectSafety, (void**)&pSafety)))
		{
			pSafety->GetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, &dwSupported, &dwEnabled );
			pSafety->SetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, dwSupported, 0 );
		}

		// initialize random seed
		srand ( time(NULL) );

		CCortexMessage msg;

		// allocate all the static ones
//		char* m_pchResponse[CX_XML_BUFFER_COUNT] = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};

//		AfxMessageBox("formatting");

		msg.FormatContent(CX_XML_BUFFER_CORTEXBEGIN, MAX_MESSAGE_LENGTH, "<cortex src=\"%s\" version=\"%s\">", 
			(g_parchivist->m_settings.m_pszName?g_parchivist->m_settings.m_pszName:"Archivist"), 
			CX_XML_VERSION
			);


		msg.SetContent(CX_XML_BUFFER_CORTEXEND, "</cortex>");

		msg.SetContent(CX_XML_BUFFER_FLAGSEND, "</flags>");

		msg.SetContent(CX_XML_BUFFER_DATAEND, "</data>");

		//  ack and nak are the same length so we can allocate it, just replace contents later
		msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

//		AfxMessageBox("set");



		if (FAILED(hRes))
		{
			sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established, but CoInitialize failed.  Unable to process XML.", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,
				pClient->m_socket);

			if(g_parchivist)
			{
				g_parchivist->m_msgr.DM(MSG_ICONERROR, NULL, szArchivistSource, errorstring);  //(Dispatch message)
				g_parchivist->SendMsg(CX_SENDMSG_ERROR, szArchivistSource, errorstring);
			}

			goto cleanup;
		}

		
		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);
		if(g_parchivist)
		{
			g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, szArchivistSource, errorstring);  //(Dispatch message)
			g_parchivist->SendMsg(CX_SENDMSG_INFO, szArchivistSource, errorstring);
		}



		while ( 
									(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
//								&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
								&&(pClient->m_socket!=NULL)
								&&(!bCloseCommand)
					)
		{


			pchBuffer = NULL;
			ulBufferLen = 0;

			
			int nReturnCode = net.GetLine((unsigned char**)&pchBuffer, &ulBufferLen, pClient->m_socket, NET_RCV_ONCE, errorstring);
			if(nReturnCode == NET_SUCCESS)
			{
				ulRetry = 0;
				//process any received XML.
				// have to keep accumulating until we find a </cortex> tag.
				if((pchBuffer)&&(ulBufferLen)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
				{
					int nLen = 0;
					if(pchXMLStream) nLen = ulAccumulatedBufferLen;
					pch = (char*) malloc(nLen+ulBufferLen+1);  //term 0
					if(pch)
					{
						char* pchEnd = NULL;
						char* pchNext = NULL;
						if(pchXMLStream)  // we have an old buffer.
						{
							memcpy(pch, pchXMLStream, ulAccumulatedBufferLen);
							memcpy(pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen);
							free(pchXMLStream); 
							pchXMLStream = pch;  // reassign!
							ulAccumulatedBufferLen += ulBufferLen;
							*(pch+ulAccumulatedBufferLen) = 0;  // null term
						}
						else
						{
							// this is new.
							// first we have to skip all chars that are not a '<'
							// pchEnd = strchr(pchBuffer, '<');  // have to find first tag, we are only interested in XML.
							// used to use strchr but if there are leading zeros in the buffer, we never get past them

							pchEnd = pchBuffer;
							while((*pchEnd!='<')&&(pchEnd<pchBuffer+ulBufferLen)) pchEnd++;

							if(pchEnd<pchBuffer+ulBufferLen)
							{
								strcpy(pch, pchEnd);
								ulAccumulatedBufferLen += (ulBufferLen-(pchEnd-pchBuffer));
								pchXMLStream = pch;  // reassign!
								*(pch+ulAccumulatedBufferLen) = 0;  // null term
							}
							else  // not found!
							{
								free(pch);
								pchXMLStream = NULL;
							}
						}

						if(pchBuffer) free(pchBuffer);
						pchBuffer = NULL;
						if(pchXMLStream)
						{

							pchEnd = strstr(pchXMLStream, "</cortex>");
							while((pchEnd)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
							{
							// found a token.
								pchEnd+=strlen("</cortex>");

								pchNext = pchEnd;
								while((*pchNext!='<')&&(pchNext<pchEnd+strlen(pchEnd))) pchNext++;

								if(pchNext<pchEnd+strlen(pchEnd))
								{
									// we found a remainder.
									nLen = strlen(pchNext);
									pch = (char*) malloc(nLen+1);  //term 0
									if(pch)
									{
										memcpy(pch, pchNext, nLen);
										*(pch+nLen) = 0;
									}
								}
								else pch = NULL;


								pchXML = pchXMLStream; // just use it.
								*pchEnd = 0; //null terminate it

								pchXMLStream = pch;  // take the rest of the stream.
								if(pchXMLStream) ulAccumulatedBufferLen = strlen(pchXMLStream);
								else ulAccumulatedBufferLen=0;

								// now have to deal with XML found in pchXML.
								//debug file write
								if((g_parchivist)&&(g_parchivist->m_settings.m_bUseXMLClientLog))
								{
//				AfxMessageBox("logging");
if(g_parchivist->m_settings.m_ulDebug&ARCHIVIST_DEBUG_COMM) 
	g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, szArchivistSource, "begin log receive");  //(Dispatch message)

									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_parchivist->m_settings.m_pszName?g_parchivist->m_settings.m_pszName:"Archivist"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lastrxfilename, filename)==0)
									{
										nRxDupes++;
									}
									else
									{
										nRxDupes=0;
										strcpy(lastrxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_RX.xml",nRxDupes);

//	AfxMessageBox(filename);

									fp = fopen(filename, "wb");
									if(fp)
									{
//											fwrite(filename, 1, strlen(filename),fp);
										fwrite(pchXML, 1, strlen(pchXML),fp);
										fflush(fp);
										fclose(fp);
										fp = NULL;
									}
if(g_parchivist->m_settings.m_ulDebug&ARCHIVIST_DEBUG_COMM) 
	g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, szArchivistSource, "end log receive");  //(Dispatch message)

								}

								// do stuff here.

								_ftime(&timeactive);  // reset the inactivity timer.


								// first attempt to extract the rxid.
								char* pchRXID = NULL;
								int nIDlen = 0;

								char* pchFind = strstr(pchXML, "<rx rxid=\"");

								if(pchFind)
								{
									pchFind+=strlen("<rx rxid=\"");
									char* pchTerminate = pchFind;
									while(pchTerminate<pchEnd)
									{
										if(
											  (*pchTerminate == '\"')
											||(*pchTerminate == '>')
											||((pchTerminate<pchEnd)&&(*pchTerminate == '/')&&(*(pchTerminate+1) == '>'))
											)
										{
											break;
										}
										else
										{
											pchTerminate+=1;
										}
									}

									nIDlen = pchTerminate-pchFind;
									if(nIDlen>0)
									{
										pchRXID = (char*)malloc(nIDlen+1);
									}
									if(pchRXID)
									{
										memset(pchRXID, 0, nIDlen+1);
										memcpy(pchRXID, pchFind, nIDlen);

										pchTerminate = bu.XMLEncode(pchRXID); // just in case
										if(pchTerminate)
										{
											try{free(pchRXID);} catch(...){}
											pchRXID = pchTerminate;
										}
									}
								}


							//create a response UUID
								UUID uuid;
								UuidCreate(&uuid); // could check response code to be RPC_S_OK ?

					//			CoCreateGuid(&uuid);

								unsigned char* pucUUID = NULL;
								UuidToString(&uuid, &pucUUID);
								// call this later: RpcStringFree(&pucUUID);

								
								bool bResponseSent = false;  // means, sent a response already.  Otherwise have to look at the failure code and deal at the end

								msg.m_nTxStep = CX_XML_BUFFER_CORTEXBEGIN;

								//OK, let's assemble the tx element.

								if(pchRXID)
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUID);
								}


								// then, parse the XML
								msg.m_nFailure=CX_XML_ERR_SUCCESS;
								pDoc->put_async(VARIANT_FALSE);
			//				AfxMessageBox((char*)pchXML);
								hr = pDoc->loadXML((char*)pchXML);
								if(hr!=VARIANT_TRUE)
								{
								//					AfxMessageBox("bad load");
									bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML parse error: [%s]", pchXML );
									g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, szArchivistSource, errorstring);
									//let's try to send a nak.

									msg.m_nFailure=CX_XML_ERR_XMLPARSE;
									msg.m_nType = CX_XML_TYPE_UNK;
									msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
									msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML parse error %d</msg>", msg.m_nFailure);
									msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");
								}
								else
								{
	//								AfxMessageBox("yirgacheff");

if(g_parchivist->m_settings.m_ulDebug&ARCHIVIST_DEBUG_COMM) 
	g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, szArchivistSource, "begin xml parse");  //(Dispatch message)
									IXMLDOMNodeListPtr pChildNodes = pDoc->GetchildNodes();

									IXMLDOMNodePtr pNodes[CX_XML_NODECOUNT];
									int n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }

									msg.m_nType = CX_XML_TYPE_UNK;
									msg.SetType("unknown");

									bool bCommandHandled = false;
									if (pChildNodes != NULL)
									{
										IXMLDOMNodePtr pChild;
										while(pChild = pChildNodes->nextNode())
										{
											DOMNodeType nodeType;
											pChild->get_nodeType(&nodeType);

											if(nodeType == NODE_ELEMENT)
											{
												char tag[MAX_PATH]; 

												util.ConvertWideToT(tag, MAX_PATH, &pChild->GetnodeName());

											//	AfxMessageBox(tag);

												if(strcmp("cortex", tag)==0)
												{
													pNodes[CX_XML_NODE_CORTEX] = pChild;
													// now get subs to cortex

													pChildNodes = pChild->GetchildNodes();
													if (pChildNodes != NULL)
													{
														while(pChild = pChildNodes->nextNode())
														{
															pChild->get_nodeType(&nodeType);

															if(nodeType == NODE_ELEMENT)
															{
																util.ConvertWideToT(tag, MAX_PATH, &pChild->GetnodeName());

														//		AfxMessageBox(tag);

																if(strcmp("rx", tag)==0)
																{
																	pNodes[CX_XML_NODE_RX] = pChild;
																	// now get the "real" rxid;

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
																	//	AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
//																			strncpy(tag, W2T(), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));

								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("rxid", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
//																				strncpy(tag, W2T(), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));

																		//		AfxMessageBox("replace rxid.");

																				if(pchRXID)
																				{
																					try{free(pchRXID);} catch(...){}
																					pchRXID = bu.XMLEncode(tag); // just in case
																					if(pchRXID)
																					{
// reassemble tx element
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
																					}
																				}

																			}
																			else
																			if(strcmp("part", tag)==0)
																			{
																				//  multi part message
																			}
																		}
																	}

																}
																else
																if(strcmp("cmd", tag)==0)
																{
																	pNodes[CX_XML_NODE_CMD] = pChild;
																	pNodes[CX_XML_NODE_OPTIONS] = NULL;
																	pNodes[CX_XML_NODE_DATA] = NULL;

																	// now get attribs and then get subs, data and options

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
								//										AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
//																			strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));

								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("type", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
//																				strncpy(tag, W2T(), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));

																				msg.SetType(tag);

//																																		AfxMessageBox(tag);

																				if(strcmp("ack", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_ACK;
																				}
																				else
																				if(strcmp("nak", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_NAK;
																				}
																				else
																				if(strcmp("persist", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PERSIST;
																					bPersist = true;
																				}
																				else
																				if(strcmp("quit", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_QUIT;
																					bPersist = false;
																				}
																				else
																				if(strcmp("status", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_STATUS;
																				}
																				else
																				if(strcmp("bye", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_BYE;
																				}
																				else
																				if(strcmp("get_version", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_GETVERSION;
																				}
																				else
																				if(strcmp("ping", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PING;
																				}
/*
																				else

																					

/////////////////////////////////////////////////////////////////////////////////////////////////
////                                    begin Archivist specific commands
//#define SENTINEL_XML_TYPE_GETCONN				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
//#define SENTINEL_XML_TYPE_GETLIST				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
//#define SENTINEL_XML_TYPE_GETEVENT			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
																				if(strcmp("get_conn", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETCONN;
																				}
																				else
																				if(strcmp("get_list", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETLIST;
																				}
																				else
																				if(strcmp("get_event", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETEVENT;
																				}
																					
////                                    end Archivist specific commands
/////////////////////////////////////////////////////////////////////////////////////////////////
*/

																				
																			}
																			else
																			if(strcmp("user", tag)==0)
																			{
																			}
																			else
																			if(strcmp("password", tag)==0)
																			{
																			}
																		}
																	}


																	IXMLDOMNodeListPtr pCmdChildNodes = pChild->GetchildNodes();
																	if (pCmdChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pCmdChild;
																		while(pCmdChild = pCmdChildNodes->nextNode())
																		{
																			pCmdChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				util.ConvertWideToT(tag, MAX_PATH, &(pCmdChild->GetnodeName()));

																				if(strcmp("options", tag)==0)
																				{
																					pNodes[CX_XML_NODE_OPTIONS] = pCmdChild;
																				}
																				else
																				if(strcmp("data", tag)==0)
																				{
																					pNodes[CX_XML_NODE_DATA] = pCmdChild;
																				}
																			}
																		}
																	}
																}
															}
														}
														// by here, all the necessary stuff should be filled out.

//														AfxMessageBox("here");
												

														switch(msg.m_nType)
														{
														case CX_XML_TYPE_ACK://						1	// 4.1 ack Acknowledges a message with success
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the ack

																
															} break; //case CX_XML_TYPE_ACK
														case CX_XML_TYPE_NAK://						2	// 4.2 nak Acknowledges a message with failure, or cancels
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the nak

															} break; //case CX_XML_TYPE_NAK
														case CX_XML_TYPE_PERSIST://				3	// 4.3 persist Requests a persistent connection
															{
																//persistence already done, above
																// set timeout and hearbeat intervals
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"persist\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																if(pNodes[CX_XML_NODE_OPTIONS])
																{

																	// have to return flags
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																	IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																	if (pOptChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pOptChild;
																		while(pOptChild = pOptChildNodes->nextNode())
																		{
																			pOptChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				util.ConvertWideToT(tag, MAX_PATH, &(pOptChild->GetnodeName()));

																				if(strcmp("timeout", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						nTimed = atoi(pchTmp);

																						_snprintf(tag, MAX_PATH, "<timeout>%d</timeout>", nTimed);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;
																					}
																				}
																				else
																				if(strcmp("heartbeat", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						strncpy(tag, pchTmp, MAX_PATH);

																						if(strcmp("yes", tag)==0)
																						{
																							nPeriodic = 30;
																						}
																						else
																						if(strcmp("no", tag)==0)
																						{
																							nPeriodic = -1;
																						}
																						else
																						{
																							nPeriodic = atoi(tag);
																						}

																						_snprintf(tag, MAX_PATH, "<heartbeat>%d</heartbeat>", nPeriodic);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );


																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;

																					}
																				}
																			}
																		}
																	}
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																}
																
															} break; //case CX_XML_TYPE_PERSIST
														case CX_XML_TYPE_QUIT://					4	// 4.4 quit Disconnects a persistent connection
															{
																//already done, above
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"quit\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																
															} break; //case CX_XML_TYPE_QUIT
														case CX_XML_TYPE_STATUS://				5	// 4.5 status Obtains status information regarding the module
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"status\" code=\"0\">");

																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																char* pchStatus = g_parchivist->m_data.GetStatusText(&ulDataLen);
																if(pchStatus)
																{
																	msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<status><code>0x%08x</code><text>%s</text></status>",
																		ulDataLen,
																		pchStatus	
																		);
																}
																else
																{
																	msg.SetContent(CX_XML_BUFFER_DATA, 
																		"<status><code>0xffffffff</code><text>unavailable</text></status>"
																		);

																}

																if(pchStatus){try { free(pchStatus); } catch(...){}  pchStatus=NULL;}

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


															} break; //case CX_XML_TYPE_STATUS
														case CX_XML_TYPE_BYE://						6	// 4.6 bye Causes the module to shut down
															{

g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, "Archivist", "The Global Kill XML command has been received.");  //(Dispatch message)

																bSetGlobalKill = true;
																g_parchivist->m_data.m_bQuietKill = true;
																bCloseCommand = true;

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"bye\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_GETVERSION://		7	// 4.7 get_version gets the version of the module as well as the build date
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_version\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<version>%s</version><build>%s %s</build>",
																		CX_CURRENT_VERSION,
																		 __DATE__,
																		 __TIME__	
																		);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_PING://					8	// 4.8 ping just sends an ack to indicate it is alive
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"ping\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE


////////////////////////////////////////////////////////
// begin Archivist specific XML commands
/*
														case SENTINEL_XML_TYPE_GETCONN://				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_conn\" code=\"%d\">", g_parchivist->m_data.m_nNumConnectionObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_parchivist->m_data.m_critConns);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_parchivist->m_data.m_ppConnObj)&&(g_parchivist->m_data.m_nNumConnectionObjects))
																{

																	//let's see if we have any options.

																	char* pchServer = NULL;  //server name search
																	int nActive =-1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{

																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("active", tag)==0)
																					{
																				//		AfxMessageBox("active");
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}
																					//		AfxMessageBox("active 2");

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																				//			AfxMessageBox(tag);

																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_parchivist->m_data.m_nNumConnectionObjects)
																	{

																		if(g_parchivist->m_data.m_ppConnObj[m])
																		{
																			bool bInclude = true;
																			CArchivistConnectionObject* pObj = g_parchivist->m_data.m_ppConnObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																	//		AfxMessageBox("here");
																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																	//			AfxMessageBox("here 2");
																			if(bInclude)
																			{
																				if(pObj->m_pAPIConn)
																				{
																					if((pObj->m_pAPIConn->m_SysData.systemfrx==0x29)
																						&&(pObj->m_pAPIConn->m_SysData.systemdf))
																					{
																						strcpy(tag, "29.97");
																					}
																					else
																					{
																						sprintf(tag, "%02x",pObj->m_pAPIConn->m_SysData.systemfrx);
																					}
																				}

																				_ftime(&timestamp);
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<aconn>\
<config active=\"%d\">\
<server>%s</server>\
<client>%s</client>\
<port>N/A</port>\
<basis>%s</basis>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<server_time>%.03f</server_time>\
<server_status>%d</server_status>\
<server_lists>%d</server_lists>\
<server_changed>%d</server_changed>\
<server_last_update>%.03f</server_last_update>\
<message>%s</message>\
</status>\
</aconn>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pObj->m_pszServerName,
																					pObj->m_pszClientName,
																					(pObj->m_pAPIConn?tag:"N/A"),
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(pObj->m_dblLastServerTimeMS/1000.0),
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? -1 : (pObj->m_pAPIConn?pObj->m_pAPIConn->m_Status:-1)),
																					(pObj->m_pAPIConn?pObj->m_pAPIConn->m_SysData.systemlistcount:-1),
																					(pObj->m_pAPIConn?pObj->m_pAPIConn->m_SysData.syschanged:-1),
																					pObj->m_dblUpdateTime,
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? (((pObj->m_ulStatus&SENTINEL_ICON_MASK) == SENTINEL_STATUS_ERROR)?"connection error":"not connected"):"")

																																										
																				);
																					
																			

																				msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}

																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;

																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_parchivist->m_data.m_critConns);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);


															} break; //case SENTINEL_XML_TYPE_GETCONN

														case SENTINEL_XML_TYPE_GETLIST://				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_list\" code=\"%d\">", g_parchivist->m_data.m_nNumChannelObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																
EnterCriticalSection(&g_parchivist->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_parchivist->m_data.m_ppChannelObj)&&(g_parchivist->m_data.m_nNumChannelObjects))
																{

																	//let's see if we have any options.

																	char* pchServer = NULL;  //server name search
																	int nActive =-1;
																	int nID=-1;
																	int nListNum = -1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{

																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("id", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nID = atoi(pchTmp);
																								if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("list_number", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nListNum = atoi(pchTmp);
																								if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("active", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_parchivist->m_data.m_nNumChannelObjects)
																	{

																		if(g_parchivist->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CArchivistChannelObject* pObj = g_parchivist->m_data.m_ppChannelObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																			if((nID>0)&&(pObj->m_nChannelID != nID))  bInclude = false;
																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				_ftime(&timestamp);

																				tlistdata* ptlist = NULL;
																				if(pObj->m_pAPIConn)
																				{
																					ptlist = &(pObj->m_pAPIConn->m_ListData[pObj->m_nHarrisListID-1]);
																				}
																				
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<alist>\
<config active=\"%d\" id=\"%d\">\
<server>%s</server>\
<list_number>%d</list_number>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<list_state>%d</list_state>\
<list_changed>%d</list_changed>\
<list_display>%d</list_display>\
<list_syschange>%d</list_syschange>\
<list_count>%d</list_count>\
<list_lookahead>%d</list_lookahead>\
<list_last_update>%.3f</list_last_update>\
</status>\
</alist>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pObj->m_nChannelID,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(ptlist?ptlist->liststate:-1),
																					(ptlist?ptlist->listchanged:-1),
																					(ptlist?ptlist->listdisplay:-1),
																					(ptlist?ptlist->listsyschange:-1),
																					(ptlist?ptlist->listcount:-1),
																					(ptlist?ptlist->lookahead:-1),
																					pObj->m_dblUpdateTime
																																										
																				);
																					
																			

																				msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_LIST_MAXPAYLOAD, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}

																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;

																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_parchivist->m_data.m_critChannels);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case SENTINEL_XML_TYPE_GETLIST
														case SENTINEL_XML_TYPE_GETEVENT://			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
															{

																msg.ClearContent(CX_XML_BUFFER_MSG);
																int nGlobalCount =0;
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_parchivist->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_parchivist->m_data.m_ppChannelObj)&&(g_parchivist->m_data.m_nNumChannelObjects))
																{

																	//let's see if we have any options.

//																	char* pchServer = NULL;  //server name search
//																	int nActive =-1;
																	int nID=-1;
																	int nLimitNum = -1;
																	int nDone = -1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{
																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("id", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nID = atoi(pchTmp);
																								if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("limit", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nLimitNum = atoi(pchTmp);
																								if(nLimitNum <=0) nLimitNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<limit>%d</limit>", nLimitNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("done", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nDone = 0;
																								else if(strcmp("1", pchTmp)==0) nDone = 1;
																								else if(strcmp("yes", pchTmp)==0) nDone = 1;
																								else if(strcmp("no", pchTmp)==0) nDone = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<done>%d</done>", nDone);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}

																					/*
																					else
																					if(strcmp("list_number", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nListNum = atoi(pchTmp);
																								if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("active", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																					* /
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_parchivist->m_data.m_nNumChannelObjects)
																	{

																		if(g_parchivist->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CArchivistChannelObject* pObj = g_parchivist->m_data.m_ppChannelObj[m];

/*
																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

* /																		if(
																				//	(nID>0)&&  //commenting this out makes it mandatory!
																					(pObj->m_nChannelID != nID)
																				)  
																				bInclude = false;

//																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				// found a channel that works, now must loop thru the events, filtering if nec.

																				// need to allocate a big buffer and deal with directly, rather than allocating new for every event.
																			
/* // was this, which worked but slowly.  below is new
																				if(pObj->m_ppevents)
																				{
																					 
																					int q = 0;
																					int nCount = 0;
																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;
																						unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));

																						if(pObj->m_ppevents[q])
																						{
																							CArchivistEventObject* pEObj = pObj->m_ppevents[q];

																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;
																							if(bInclude)
																							{

																								_ftime(&timestamp);

																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
																								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																									"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_usControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CArchivistEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																								);
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																								msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );

																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
* /




																				char* pchPayload = (char*)malloc(SENTINEL_XML_EVENT_MAXPAYLOAD);
																				char* pchAppend = pchPayload;

																				// re-use errorstring, why not.
																		//		int nTimes[10] = {0,0,0,0,0,0,0,0,0,0};
																				int nClock = clock();
if(g_parchivist->m_settings.m_ulDebug&ARCHIVIST_DEBUG_COMM) 
	g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, szArchivistSource, "start event payload assembly");  //(Dispatch message)

																				if((pObj->m_ppevents)&&(pchPayload))
																				{
																					unsigned long ulBufferLen = SENTINEL_XML_EVENT_MAXPAYLOAD;
																					char* pchBufferEnd = (pchPayload+SENTINEL_XML_EVENT_MAXPAYLOAD);
																					int nEventLen = 0;
																					unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));
																					int q = 0;
																					int nCount = 0;
																					int nBuffers = 1;
																					int nTempOffset;
																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;

																						if(pObj->m_ppevents[q])
																						{
																					//		nTimes[0] += (clock() - nClock); nClock = clock(); 

																							CArchivistEventObject* pEObj = pObj->m_ppevents[q];
																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;

																				//			nTimes[1] += (clock() - nClock); nClock = clock(); 
																							if(bInclude)
																							{

																							//	_ftime(&timestamp);
																					//		nTimes[2] += (clock() - nClock); nClock = clock(); 

																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
																					//		nTimes[3] += (clock() - nClock); nClock = clock(); 
																								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, 
																									"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_usControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CArchivistEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																								);
																					//		nTimes[4] += (clock() - nClock); nClock = clock(); 
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																						//	nTimes[5] += (clock() - nClock); nClock = clock(); 
																								nEventLen = strlen(errorstring)+1;//+1 for term zero

																								if(pchAppend+nEventLen>pchBufferEnd)  // if must be changed to while if SENTINEL_XML_EVENT_MAXPAYLOAD defined < MAX_MESSAGE_LENGTH (buffer vs errorstring)
																								{
																									// need to realloc
																									nBuffers++;
																									ulBufferLen = SENTINEL_XML_EVENT_MAXPAYLOAD*nBuffers;
																									char* pchBufferNew = (char*)malloc(ulBufferLen);
																									if(pchBufferNew)
																									{
																										nTempOffset = pchAppend+1 - pchPayload;
																										memcpy(pchBufferNew, pchPayload, nTempOffset);
																										try {free(pchPayload);} catch(...){}
																										pchPayload = pchBufferNew;
																										pchBufferEnd = (pchPayload+ulBufferLen);
																										pchAppend = pchPayload+nTempOffset-1;																									

																									}
																								}
																								

																								// append
																								memcpy(pchAppend, errorstring, nEventLen);
																								pchAppend += nEventLen-1; // append over the term 0 next time.


																						//	nTimes[6] += (clock() - nClock); nClock = clock(); 


/*
																								msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
* /
																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
																				msg.ClearContent(CX_XML_BUFFER_DATA);
																				if(pchPayload)
																				{
																					msg.m_pchResponse[CX_XML_BUFFER_DATA] = pchPayload;		
																					ulDataLen = pchAppend - pchPayload;
																				}
if(g_parchivist->m_settings.m_ulDebug&ARCHIVIST_DEBUG_COMM) 
	g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, szArchivistSource, "end event payload assembly");  //(Dispatch message)

																			}

																		}
																		m++;
																	}

/*																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;
* /
																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_parchivist->m_data.m_critChannels);

/* // not needed now, we tally the buffer as we go
																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																* /
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_event\" code=\"%d\">", nGlobalCount);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case SENTINEL_XML_TYPE_GETEVENT

// end Archivist specific XML commands
////////////////////////////////////////////////////////
*/
														case CX_XML_TYPE_UNK://						0 // unknown
														default:
															{
																sprintf(errorstring, "Unsupported XML command: [%s]", (msg.m_pchType?msg.m_pchType:"") );
																g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, szArchivistSource, errorstring);
																//let's try to send a nak.

																msg.m_nType = CX_XML_TYPE_UNK;
																msg.m_nFailure=CX_XML_ERR_BADCMD;
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"%s\" code=\"%d\">", (msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Unsupported XML command [%s] (error %d)</msg>",(msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

															} break; //case CX_XML_TYPE_UNK://						0 // unknown
														}
															
														
														if(!bPersist) bCloseCommand = true;

													}
													else // no child nodes to cortex - can't identify command.
													{
														bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
														sprintf(errorstring, "Cortex XML format error: [%s]", pchXML );
														g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, szArchivistSource, errorstring);
														//let's try to send a nak.

														msg.m_nType = CX_XML_TYPE_UNK;
														msg.m_nFailure=CX_XML_ERR_NOCMD;
														msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
														msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Cortex XML format error %d</msg>", msg.m_nFailure);
														msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

													}
												}
											}
										}
									}
									else
									{
										// no child nodes, can't do nuttin.
										bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
										sprintf(errorstring, "XML format error: [%s]", pchXML );
										g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, szArchivistSource, errorstring);
										//let's try to send a nak.

										msg.m_nType = CX_XML_TYPE_UNK;
										msg.m_nFailure=CX_XML_ERR_NOCHILD;
										msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
										msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML format error %d</msg>", msg.m_nFailure);
										msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

									}
if(g_parchivist->m_settings.m_ulDebug&ARCHIVIST_DEBUG_COMM) 
	g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, szArchivistSource, "end xml parse");  //(Dispatch message)

									n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }
								}

								if(!bResponseSent)
								{
if(g_parchivist->m_settings.m_ulDebug&ARCHIVIST_DEBUG_COMM) 
	g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, szArchivistSource, "begin send response");  //(Dispatch message)

								// send it
		_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_parchivist)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending response message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_parchivist->m_msgr.DM(MSG_ICONERROR, NULL, szArchivistSource, errorstring);  //(Dispatch message)
				g_parchivist->SendMsg(CX_SENDMSG_ERROR, szArchivistSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
if(g_parchivist->m_settings.m_ulDebug&ARCHIVIST_DEBUG_COMM) 
	g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, szArchivistSource, "end send response");  //(Dispatch message)

								}

								// log it

								//debug file write
								if((g_parchivist)&&(g_parchivist->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
								{
if(g_parchivist->m_settings.m_ulDebug&ARCHIVIST_DEBUG_COMM) 
	g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, szArchivistSource, "begin log response");  //(Dispatch message)
									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_parchivist->m_settings.m_pszName?g_parchivist->m_settings.m_pszName:"Archivist"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lasttxfilename, filename)==0)
									{
										nTxDupes++;
									}
									else
									{
										nTxDupes=0;
										strcpy(lasttxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

									fp = fopen(filename, "wb");
									if(fp)
									{
										int b=CX_XML_BUFFER_CORTEXBEGIN;
										while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
										{
//											fwrite(filename, 1, strlen(filename),fp);
											if(msg.m_pchResponse[b])
											{
												if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

												fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
												fflush(fp);
											}
											b++;

										}
										fclose(fp);
										fp = NULL;
									}
if(g_parchivist->m_settings.m_ulDebug&ARCHIVIST_DEBUG_COMM) 
	g_parchivist->m_msgr.DM(MSG_ICONHAND, NULL, szArchivistSource, "end log response");  //(Dispatch message)
								}

//AfxMessageBox("001");
								int b=CX_XML_BUFFER_TX;
								while(b<CX_XML_BUFFER_DATAEND)
								{
									if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
									{
										try { free(msg.m_pchResponse[b]); } catch(...){}
										msg.m_pchResponse[b] = NULL;
									}
									b++;
								}

//AfxMessageBox("002");
								if(pucUUID)
								{
									try { RpcStringFree(&pucUUID); } catch(...){}
								}
								pucUUID = NULL;

//AfxMessageBox("003");
								if(pchXML) 
								{
									try { free(pchXML); } catch(...){}
								}
								pchXML = NULL;
//AfxMessageBox("004");

								if(pchRXID) 
								{
									try { free(pchRXID); } catch(...){}
								}
								pchRXID = NULL;

//AfxMessageBox("005");
								if (pchXMLStream) 
									pchEnd = strstr(pchXMLStream, "</cortex>");
								else pchEnd = NULL;
//AfxMessageBox("006");
							}  // while </cortex> exists.
						}//if(pchXMLStream)
						// dont do a free(pch);  
					}  // else out of mem, so just skip
					else
					{
						// out of mem.. .log error?
					}

				} //if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))

			} // if getline succeeds.
			else
			{
				// here, we either have timed out because theres no data, or, the connection has been lost.
				// if we lost the conn, no need to re-establish, we are a client handler, let the client reconnect if they want.
				if(nReturnCode == NET_ERROR_CONN)		// connection lost
				{
					ulRetry++;

					if(g_parchivist)
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH, "Lost connection from %d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno );

						g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, szArchivistSource, errorstring);  //(Dispatch message)
						g_parchivist->SendMsg(CX_SENDMSG_INFO, szArchivistSource, errorstring);
					}

					break;  // break out of while loop, closes connection, starts again
				}
				else
				{
//									g_parchivist->m_msgr.DM(MSG_ICONERROR, NULL, szArchivistSource, "check %d", clock());  //(Dispatch message)

					// we prob just havent gotten a message because no data.  
					// so, check to see if its time for publication service
									// no, do this in the stall loop


				}

			} // end of else from if(nReturnCode == NET_SUCCESS)

			// and lets check if we've timed out on the connection, no matter what the success was
			_ftime( &timestamp );
			if(
				  (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				&&(ulConnTimeout>0)
				&&(ulConnLastMessage>0)
				&&((ulConnLastMessage + ulConnTimeout) < (unsigned long)timestamp.time )
				)
			{
				// we timed out, need to disconnect
				ulRetry++;


				sprintf(errorstring, "Connection from %d.%d.%d.%d timed out.", 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno);

				if(g_parchivist)
				{
					g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, szArchivistSource, errorstring);  //(Dispatch message)
					g_parchivist->SendMsg(CX_SENDMSG_INFO, szArchivistSource, errorstring);
				}

				if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;} //free this memory before breaking out of the while.

				break;  // break out of while loop, closes connection, starts again
			}

			if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;}
			
			if(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) Sleep(1);  // dont peg processor




/*
			// following line fills out the data structure
//			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);
			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Archivist:XMLHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Archivist:XMLHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Archivist:XMLHandlerThread");
						}
					}
				}
			}
*/
			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Archivist:XMLHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
//						g_parchivist->m_msgr.DM(MSG_ICONERROR, NULL, szArchivistSource, "check %d", clock());  //(Dispatch message)


						if(nPeriodic>0)
						{
							_ftime( &timestamp );
							if((timeperiodic.time + nPeriodic)< timestamp.time)
							{
								// send asynchronous message

								//create a response UUID
								UUID uuida;
								UuidCreate(&uuida); // could check response code to be RPC_S_OK ?

								unsigned char* pucUUIDa = NULL;
								UuidToString(&uuida, &pucUUIDa);

								msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUIDa);

								if(pucUUIDa)
								{
									try { RpcStringFree(&pucUUIDa); } catch(...){}
								}
								pucUUIDa = NULL;


								msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"system\" code=\"0\">");

								msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
								msg.ClearContent(CX_XML_BUFFER_FLAGS);

								unsigned long ulDataLen = 0;
								char* pchStatus = g_parchivist->m_data.GetStatusText(&ulDataLen);
								if(pchStatus)
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0x%08x</code><text>%s</text></status><time>%d.%03d</time></system>",
										ulDataLen,
										pchStatus,
										timestamp.time, timestamp.millitm
										);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0xffffffff</code><text>unavailable</text></status><time>%d.%03d</time></system>",
										timestamp.time, timestamp.millitm
										);

								}

								if(pchStatus){try { free(pchStatus); } catch(...){} pchStatus=NULL;}

								ulDataLen = 0;
								if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
								{
									ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
								}
								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
									ulDataLen,
									net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
									);

								msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>%s status message</msg>", 
									(g_parchivist->m_settings.m_pszName?g_parchivist->m_settings.m_pszName:"Archivist"));

								msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


								// now, send and log.
	////////////////////////////////////////////// code copied from above.  if modified, must be 2 places.  sorry...
									// send it
			_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_parchivist)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending system message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_parchivist->m_msgr.DM(MSG_ICONERROR, NULL, szArchivistSource, errorstring);  //(Dispatch message)
				g_parchivist->SendMsg(CX_SENDMSG_ERROR, szArchivistSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
								
	//AfxMessageBox("oop");
									// log it

									//debug file write
									if((g_parchivist)&&(g_parchivist->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
									{
										strcpy(filename, "Logs");
										_mkdir(filename);  // if exists already np
										strcat(filename, "\\");
										sprintf(errorstring, "%d.%d.%d.%d", 
											pClient->m_si.sin_addr.s_net, 
											pClient->m_si.sin_addr.s_host, 
											pClient->m_si.sin_addr.s_lh, 
											pClient->m_si.sin_addr.s_impno);

										strcat(filename, errorstring);

										_mkdir(filename);  // if exists already np

		//								_ftime( &timestamp );  // use timestamp we just had..

										tm* theTime = localtime( &timestamp.time	);

										sprintf(errorstring, "\\%s_", 
											(g_parchivist->m_settings.m_pszName?g_parchivist->m_settings.m_pszName:"Archivist"));

										strcat(filename, errorstring);

										char filenametemp[MAX_PATH];
										strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

										strcat(filename, filenametemp);

										int nOffset = strlen(filename);
										sprintf(filename+nOffset,"%03d",timestamp.millitm);

										if (strcmp(lasttxfilename, filename)==0)
										{
											nTxDupes++;
										}
										else
										{
											nTxDupes=0;
											strcpy(lasttxfilename, filename);
										}
										nOffset = strlen(filename);
										sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

										fp = fopen(filename, "wb");
										if(fp)
										{
											int b=CX_XML_BUFFER_CORTEXBEGIN;
											while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
											{
	//											fwrite(filename, 1, strlen(filename),fp);
												if(msg.m_pchResponse[b])
												{
													if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

													fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
													fflush(fp);
												}
												b++;

											}
											fclose(fp);
											fp = NULL;
										}
									}


									b=CX_XML_BUFFER_TX;
									while(b<CX_XML_BUFFER_DATAEND)
									{
										if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
										{
											try { free(msg.m_pchResponse[b]); } catch(...){}
											msg.m_pchResponse[b] = NULL;
										}
										b++;
									}
	/////////////////////////////////////////////
							}

						}

						//and check timeout
						if(nTimed>0)
						{
							_ftime( &timestamp );
							if((timeactive.time + nTimed) < timestamp.time)
							{
								//kill it

								bCloseCommand = true;
							}
							
						}


						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} 

cleanup:
//		AfxMessageBox("cleanup");
//AfxMessageBox("010");

//		pDoc->Release();  // destructor calls this so don't do it here. 
		if(pSafety) pSafety->Release();

//AfxMessageBox("011");

// cleanup:  //was here but want to uninit com etc
//AfxMessageBox("012");

		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d closed.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);

//AfxMessageBox("013");
		if(g_parchivist)
		{
			g_parchivist->m_msgr.DM(MSG_ICONINFO, NULL, szArchivistSource, errorstring);  //(Dispatch message)
			g_parchivist->SendMsg(CX_SENDMSG_INFO, szArchivistSource, errorstring);
		}

//AfxMessageBox("014");

		if(pch!=NULL) { try{free(pch);} catch(...) {} } // must free the data buffer 

//AfxMessageBox("015");

		shutdown(pClient->m_socket, SD_BOTH);
//AfxMessageBox("016");
		closesocket(pClient->m_socket);
//AfxMessageBox("017");
		(*(pClient->m_pulConnections))--;
//AfxMessageBox("018");

	}
//		AfxMessageBox("end cleanup");
//AfxMessageBox("019");

	try
	{  
		CoUninitialize(); //XML
	}
	catch(...)
	{
		if(g_parchivist) g_parchivist->m_msgr.DM(MSG_ICONERROR, NULL, szArchivistSource, "Exception in CoUninitialize");  //(Dispatch message)
	}

//AfxMessageBox("020");

	try
	{
		delete pClient; // was created with new in the thread that spawned this one.
	}	catch(...) {}

//AfxMessageBox("021");
	if (bSetGlobalKill)	//g_bKillThread = true;
		((CArchivistHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}


