// ArchivistSettings.h: interface for the CArchivistSettings class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARCHIVISTSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
#define AFX_ARCHIVISTSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ArchivistDefines.h"
#include "../../Common/MFC/ODBC/DBUtil.h"

class CArchivistSettings  
{
public:
	CArchivistSettings();
	virtual ~CArchivistSettings();

	int Settings(bool bRead);
	int GetFromDatabase(char* pszInfo=NULL);  //only get.  Interface publishes
	char* ProcessString(char* pszString, bool bFreeIncomingString);

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;

	char* m_pszName;  // familiar name of this instance (and therefore the name of the Archivist database)
	char* m_pszType;  // familiar name of the type.
	char* m_pszProject;  // familiar name of the project name.
	unsigned long m_ulMainMode;

	// ports
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;

	// The following flags determine what settings are loaded. 
	// The specifics do not need to be stored in cortex settings, they are pushed to the various objects.

	// messaging for Archivist
	bool m_bUseLog;			// write a log file
	bool m_bUseEmail;		// send an email on failures (or commanded from remotes)
	bool m_bUseNetwork; // send a message to a remote host
	bool m_bReportSuccessfulOperation;
	bool m_bLogTransfers;
	char* m_pszFileSpec;
	char* m_pszMailSpec;
	char* m_pszProcessedFileSpec;
	char* m_pszProcessedMailSpec;
	bool m_bUseXMLClientLog;			// write a log file
	bool m_bLogNetworkErrors; // enables messaging into network object (logs socket errors, etc).
	bool m_bMillisecondMessaging;			// use millisecond resolution for messages and asrun

	int m_nThreadDwellMS;
	bool m_bDebugLists;  // if true, dumps the temp and events databases
	int  m_nDebugListTopCount;  //if positive, just prints out the top n recored in the events tables.
	bool m_bDebugSQL;  // if true, prints out the SQL calls, all but event inserts
	bool m_bDebugInsertSQL;  // if true, prints out the event insert SQL calls
	unsigned long m_ulDebug;  // prints out debug statements that & with this.
	char* m_pszDebugOrder; // the order by clause for debug

	int m_nAutoPurgeMessageDays;
//	int m_nAutoPurgeAsRunDays;
	int m_nAutoPurgeInterval;


	// file handling
	bool m_bDeleteSourceFileOnTransfer; 
	bool m_bUseBackupDestination; 
	bool m_bUseDeletionSync; 
	bool m_bUseBackupDeletionSync; 
	bool m_bUseBackupDatabase; 
	bool m_bFilterFileTypes;
	int  m_nWatchfolderInterval;
	double  m_dblDeletePercentageDiskFull;
	double  m_dblWarnPercentageDiskFull;

	// database stuff
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;
	char* m_pszDatabase;
	char* m_pszBackupServer;  // for pushing to backup db thru primary DSN conn
	char* m_pszBackupDatabase;  // for pushing to backup db thru primary DSN conn
	char* m_pszBackupDSN;
	char* m_pszBackupUser;
	char* m_pszBackupPW;

	char* m_pszSettings;  // the Settings table name
	char* m_pszExchange;  // the Exchange table name
	char* m_pszMessages;  // the Messages table name
	char* m_pszMetadata;  // the Metadata table name
	char* m_pszFileTypes; // the FileTypes table name
	char* m_pszQueue;			// the Queue table name
	char* m_pszPending;		// the Pending events table name

// other settings
	char* m_pszWatchFolderPath;  // the Watchfolder path
	char* m_pszDestinationFolderPath;  // the Destination path
	char* m_pszBackupDestinationFolderPath;  // the Backup destination path
	char* m_pszArchiveDestinationFolderPath;  // the Archive path
	char* m_pszBackupDestinationFolderLocalPath;  // the Backup destination path on the backup server.

	bool m_bDirectInstalled;
	bool m_bOverwriteFiles;  // for archiving and general moves.  Watchfolder always overwrites.
	bool m_bAutoDeleteOldest;

	int m_nPendingRetries;
	int m_nMaxFilesPerCycle;

	unsigned long m_ulModsIntervalMS;  // interval on which to check database mods

	char* m_pszLicense;  // the License Key
	char* m_pszIconPath;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.

};

#endif // !defined(AFX_ARCHIVISTSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
