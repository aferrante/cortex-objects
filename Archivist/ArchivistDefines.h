// ArchivistDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(ARCHIVISTDEFINES_H_INCLUDED)
#define ARCHIVISTDEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define ARCHIVIST_CURRENT_VERSION		"1.0.2.14"


// modes
#define ARCHIVIST_MODE_DEFAULT			0x00000000  // exclusive
#define ARCHIVIST_MODE_LISTENER			0x00000001  // exclusive
#define ARCHIVIST_MODE_CLONE				0x00000002  // exclusive
#define ARCHIVIST_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define ARCHIVIST_MODE_VOLATILE			0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define ARCHIVIST_MODE_MASK					0x0000000f  // 

// default port values.
//#define ARCHIVIST_PORT_FILE				80		
#define ARCHIVIST_PORT_CMD					20660		
#define ARCHIVIST_PORT_STATUS				20661		

#define ARCHIVIST_PORT_INVALID			0	

#define ARCHIVIST_FLAG_DISABLED			0x0000	 // default
#define ARCHIVIST_FLAG_ENABLED			0x0001	
#define ARCHIVIST_FLAG_FOUND				0x1000

#define ARCHIVIST_THREAD_WAIT_MAX		30 //seconds

#define ARCHIVIST_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded


#define ARCHIVIST_FILEFLAG_RECORDONLY			0x00000000
#define ARCHIVIST_FILEFLAG_FILEXFER				0x00000001
#define ARCHIVIST_FILEFLAG_FILECOPIED			0x00000002
#define ARCHIVIST_FILEFLAG_ARCHIVED				0x00000004
#define ARCHIVIST_FILEFLAG_DELETED				0x00000008


// status
#define ARCHIVIST_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define ARCHIVIST_STATUS_UNKNOWN						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define ARCHIVIST_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define ARCHIVIST_STATUS_ERROR							0x00000020  // error (red icon)
#define ARCHIVIST_STATUS_CONN								0x00000030  // ready (green icon)	
#define ARCHIVIST_STATUS_OK									0x00000030  // ready (green icon)	
#define ARCHIVIST_STATUS_RUN								0x00000040  // in progress, running, owned etc (blue icon);	
#define ARCHIVIST_ICON_MASK									0x00000070  // mask	

#define ARCHIVIST_STATUS_SUSPEND						0x00000080  // suspended	(yellow icon please)

#define ARCHIVIST_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define ARCHIVIST_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define ARCHIVIST_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define ARCHIVIST_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define ARCHIVIST_STATUS_CMDSVR_MASK				0x00007000  // command server mask bits

#define ARCHIVIST_STATUS_STATUSSVR_START		0x00010000  // starting the status server
#define ARCHIVIST_STATUS_STATUSSVR_RUN			0x00020000  // status server running
#define ARCHIVIST_STATUS_STATUSSVR_END			0x00030000  // status server shutting down
#define ARCHIVIST_STATUS_STATUSSVR_ERROR		0x00040000  // status server error
#define ARCHIVIST_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define ARCHIVIST_STATUS_THREAD_START				0x00100000  // starting the main thread
#define ARCHIVIST_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define ARCHIVIST_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define ARCHIVIST_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define ARCHIVIST_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define ARCHIVIST_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define ARCHIVIST_STATUS_THREAD_MASK				0x00f00000  // main thread mask bits

// various failures...
#define ARCHIVIST_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define ARCHIVIST_STATUS_FAIL_DB						0x20000000  // could not get DB
#define ARCHIVIST_STATUS_FAIL_MASK					0xf0000000  // failure code mask bits

//return values
#define ARCHIVIST_SUCCESS   0
#define ARCHIVIST_ERROR	   -1
#define ARCHIVIST_FILE_EXISTS  -2


// default filenames
#define ARCHIVIST_SETTINGS_FILE_SETTINGS	  "archivist.csr"		// csr = cortex settings redirect
#define ARCHIVIST_SETTINGS_FILE_DEFAULT	  "archivist.csf"		// csf = cortex settings file

// debug defines
#define ARCHIVIST_DEBUG_TIMING					0x00000001
#define ARCHIVIST_DEBUG_EXCHANGE				0x00000002
#define ARCHIVIST_DEBUG_CONNECT					0x00000004
#define ARCHIVIST_DEBUG_CONNECTSTATUS		0x00000008
#define ARCHIVIST_DEBUG_THREADS					0x00000010
#define ARCHIVIST_DEBUG_COMM						0x00000200



#endif // !defined(ARCHIVISTDEFINES_H_INCLUDED)
