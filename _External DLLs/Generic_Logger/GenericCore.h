// GenericCore.h: interface for the CGenericCore class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GENERICCORE_H__D426F8FF_BC44_4257_869A_044999574215__INCLUDED_)
#define AFX_GENERICCORE_H__D426F8FF_BC44_4257_869A_044999574215__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "..\..\..\Common\TXT\BufferUtil.h"

class CGenericCore  
{
public:
	CGenericCore();
	virtual ~CGenericCore();

	CBufferUtil m_bu;

	BOOL IsPointInWnd(CPoint point, CWnd* pWnd);
	BOOL IsPointInRect(CPoint point, CRect rcClientRect);
  CString CALEncodeBrackets(CString szString);

};

#endif // !defined(AFX_GENERICCORE_H__D426F8FF_BC44_4257_869A_044999574215__INCLUDED_)
