// GenericData.h: interface for the CGenericData and CGenericMessage classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GENERICDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_)
#define AFX_GENERICDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "..\..\..\Common\LAN\NetUtil.h"
//#include "..\..\..\Cortex Objects\Tabulator\TabulatorDefines.h"
//#include "..\..\..\Cortex Objects\Tabulator\TabulatorData.h"
#include "..\..\..\Common\MFC\ODBC\DBUtil.h"


#define SENDMSG_SENDER_MAXLEN 64
#define SENDMSG_EVENT_MAXLEN 64
#define SENDMSG_MESSAGE_MAXLEN 512

class CGenericData  
{
public:
	CGenericData();
	virtual ~CGenericData();

	// remove pipes
	CString Encode(CString szText);
	// put back pipes
	CString Decode(CString szText);

	CString XMLTextNodeValue(IXMLDOMNodePtr pNode);

	// do following 2 thru callbacks
//	int SendMsg(int nType, char* pszSender, char* pszMessage, ...);
//	int SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...);

	bool m_bDialogStarted;
	SOCKET m_socket;
//	CRITICAL_SECTION m_critClientSocket;

	CNetUtil m_net;
	CDBUtil m_dbu;

	CDBUtil*  m_pdb;
	CDBconn* m_pdbConn;


	CRITICAL_SECTION m_critTransact;
//	bool m_bTransact;

	char* m_pszInstanceID;

	LPFNDM			m_lpfnDispatchMsg;
	LPFNMSG			m_lpfnMsg;
	LPFNAR			m_lpfnAsRunMsg;
	LPFNSTATUS	m_lpfnStatusMsg;

	int m_nQuery;



};


#endif // !defined(AFX_GENERICDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_)
