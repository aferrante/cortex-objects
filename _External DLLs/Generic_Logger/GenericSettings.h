#if !defined(AFX_GENERICSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_)
#define AFX_GENERICSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GenericSettings.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGenericSettings dialog

class CGenericSettings : public CDialog
{
// Construction
public:
	CGenericSettings(CWnd* pParent = NULL);   // standard constructor
	virtual ~CGenericSettings();   // standard destructor

// Dialog Data
	//{{AFX_DATA(CGenericSettings)
	enum { IDD = IDD_DIALOG_SETTINGS };
	//}}AFX_DATA

	CRITICAL_SECTION m_crit;

		// database stuff
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;

	//exposed
	char* m_pszAppName;
	char* m_pszInternalAppName;
	char* m_pszAboutText;
	char* m_pszSettingsText;

	char* m_pszFileSpec;

	int Settings(bool bRead);

	unsigned long m_ulDebug;  // prints out debug statements that & with this.

	bool m_bOutputToFile;
	char* m_pszOutputFileSpec;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGenericSettings)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGenericSettings)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GENERICSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_)
