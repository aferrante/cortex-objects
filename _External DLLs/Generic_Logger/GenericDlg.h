#if !defined(AFX_GENERICDLG_H__F935B8A6_4FC2_4AB9_A958_9A2C93601BBC__INCLUDED_)
#define AFX_GENERICDLG_H__F935B8A6_4FC2_4AB9_A958_9A2C93601BBC__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GenericDlg.h : header file
//
#define ID_BNTKR		 0
#define ID_STHWIPE	 1
#define ID_STVWIPE	 2
#define ID_STHWIPE2	 3
#define ID_REFEED		 4
#define ID_RETICK		 5
#define ID_REPREV		 6
#define ID_BNCOMMIT  7
#define ID_BNCOMMIT2 8
#define ID_STCLIPRC  9
#define ID_LCEFEED	 10
#define ID_LCEDEFLT  11
#define ID_LCESTACK  12
#define ID_BNCYCLE	 13
#define ID_INVF			 14


#define DLG_NUM_MOVING_CONTROLS 15


#define ID_BNFDEL		 0
#define ID_BNFUN		 1
#define ID_BNFGD		 2
#define ID_BNFBD		 3
#define ID_BNFGT		 4
#define ID_BNFCM		 5
#define ID_BNFPL		 6
#define ID_BNFPLV		 7
#define ID_BNFCH		 8
#define ID_BNFCHV		 9
#define ID_BNFTM		 10
#define ID_BNFTMV		 11

#define DLG_NUM_STATIC_CONTROLS 12



#define INS				0
#define REM				1
#define RELOAD		2
#define BAD				3
#define UNMARKED	4
#define GOOD			5
#define GREAT			6
#define COMMITTED	7
#define PLAYED		8
#define LENGTH		9
#define AGE				10

#include "..\..\..\Common\MFC\ListCtrlEx\ListCtrlEx.h"
#include "..\..\..\Common\MFC\EditEx\EditEx.h"
#include "..\..\..\Common\IMG\BMP\CBmpUtil_MFC.h"
#include "..\..\..\Common\TXT\BufferUtil.h"


/////////////////////////////////////////////////////////////////////////////
// CGenericDlg dialog

class CGenericDlg : public CDialog
{
// Construction
public:
	CGenericDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGenericDlg)
	enum { IDD = IDD_DIALOG_MAIN };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGenericDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

public:
	BOOL 	m_bNewSizeInit;


// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGenericDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GENERICDLG_H__F935B8A6_4FC2_4AB9_A958_9A2C93601BBC__INCLUDED_)
