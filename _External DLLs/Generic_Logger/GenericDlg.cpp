// GenericDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Generic.h"
#include "GenericDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CGenericApp			theApp;
extern CGenericCore			g_core;
extern CGenericData			g_data;
extern CGenericDlg*			g_pdlg;
extern CGenericSettings	g_settings;
extern CMessager*					g_ptabmsgr;



/////////////////////////////////////////////////////////////////////////////
// CGenericDlg dialog


CGenericDlg::CGenericDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGenericDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGenericDlg)
	//}}AFX_DATA_INIT

}


void CGenericDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGenericDlg)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGenericDlg, CDialog)
	//{{AFX_MSG_MAP(CGenericDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CGenericDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(IDD, pParentWnd);
}

