; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
ClassCount=4
Class1=CGenericApp
LastClass=CGenericDlg
NewFileInclude2=#include "Generic.h"
ResourceCount=4
NewFileInclude1=#include "stdafx.h"
Resource1=IDD_DIALOG_TESTFEED
Class2=CGenericSettings
LastTemplate=CDialog
Class3=CGenericDlg
Resource2=IDD_DIALOG_MAIN
Class4=CGenericTestFeed
Resource3=IDD_DIALOG_SETTINGS
Resource4=IDR_MENU1

[CLS:CGenericApp]
Type=0
HeaderFile=Generic.h
ImplementationFile=Generic.cpp
Filter=N
LastObject=CGenericApp
BaseClass=CWinApp
VirtualFilter=AC

[DLG:IDD_DIALOG_MAIN]
Type=1
Class=CGenericDlg
ControlCount=0

[DLG:IDD_DIALOG_SETTINGS]
Type=1
Class=CGenericSettings
ControlCount=2
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816

[CLS:CGenericSettings]
Type=0
HeaderFile=GenericSettings.h
ImplementationFile=GenericSettings.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_CHECK_PLAYONCE
VirtualFilter=dWC

[CLS:CGenericDlg]
Type=0
HeaderFile=GenericDlg.h
ImplementationFile=GenericDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_CHECK_CYCLE

[DLG:IDD_DIALOG_TESTFEED]
Type=1
Class=CGenericTestFeed
ControlCount=9
Control1=IDOK,button,1073807360
Control2=IDCANCEL,button,1073807360
Control3=IDC_EDIT_FEEDURL,edit,1350631552
Control4=IDC_STATIC,static,1342308352
Control5=IDC_RICHEDIT_STATUS,RICHEDIT,1353779396
Control6=IDC_BUTTON_START,button,1342242816
Control7=IDC_CHECK_FILE,button,1342242819
Control8=IDC_STATIC_TIME,static,1342308352
Control9=IDC_CHECK_TIME,button,1342242819

[CLS:CGenericTestFeed]
Type=0
HeaderFile=GenericTestFeed.h
ImplementationFile=GenericTestFeed.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CGenericTestFeed

[MNU:IDR_MENU1]
Type=1
Class=?
Command1=ID_CMD_TOGGLEACTIVE
Command2=ID_CMD_CLEARFEED
Command3=ID_CMD_CLEARDEFAULT
Command4=ID_CMD_CLEARTICKER
Command5=ID_CMD_REFRESH
Command6=ID_CMD_SETTINGSDLG
CommandCount=6

