// Generic.h : main header file for the GENERIC DLL
//

#if !defined(AFX_GENERIC_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
#define AFX_GENERIC_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#import "msxml3.dll" named_guids 
using namespace MSXML2;

//#include "../../../Client/ClientDefines.h"
#include "../../../Common/MSG/Messager.h"
#include "../../../Common/TXT/FileUtil.h"
#include "..\..\..\Cortex\3.0.4.4\CortexDefines.h"
#include "..\..\..\Cortex\3.0.4.4\CortexShared.h"

#include "..\..\..\Common\MFC\Inet\Inet.h"

#define REMOVE_CLIENTSERVER  // for web-based UI only


#include "GenericCore.h"
#include "GenericData.h"
#include "GenericDlg.h"
#include "GenericSettings.h"

#define SETTINGS_FILENAME "generic.ini"

#define VERSION_STRING "1.0.0.1"
#define GENERIC_DATA 0
#define GENERIC_TICKER 1
#define GENERIC_UI 2
#define GENERIC_THREADS 3

/////////////////////////////////////////////////////////////////////////////
// CGenericApp
// See Generic.cpp for the implementation of this class
//

class CGenericApp : public CWinApp
{
public:
	CGenericApp();
	int  DLLCtrl(void** ppvoid, UINT nType);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGenericApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	char m_szSettingsFilename[MAX_PATH];

	//{{AFX_MSG(CGenericApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GENERIC_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
