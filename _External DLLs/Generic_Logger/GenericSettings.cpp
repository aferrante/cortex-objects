// GenericSettings.cpp : implementation file
//

#include "stdafx.h"
#include "Generic.h"
#include "GenericSettings.h"
#include "GenericTestFeed.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


extern CGenericData			g_data;
extern CGenericSettings	g_settings;
extern CMessager*			g_pmsgr;
extern CGenericApp				theApp;

/////////////////////////////////////////////////////////////////////////////
// CGenericSettings dialog


CGenericSettings::CGenericSettings(CWnd* pParent /*=NULL*/)
	: CDialog(CGenericSettings::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGenericSettings)
	//}}AFX_DATA_INIT

	m_ulDebug = 0;
	m_pszAppName=NULL;
	m_pszInternalAppName=NULL;
	m_pszAboutText=NULL;
	m_pszSettingsText=NULL;
	m_pszFileSpec = NULL;

	m_bOutputToFile=false;
	m_pszOutputFileSpec=NULL;

	InitializeCriticalSection(&m_crit);
}

CGenericSettings::~CGenericSettings()
{
	EnterCriticalSection(&m_crit);
	if(m_pszAppName) free(m_pszAppName);
	if(m_pszInternalAppName) free(m_pszInternalAppName);
	if(m_pszAboutText) free(m_pszAboutText);
	if(m_pszSettingsText) free(m_pszSettingsText);

	if(m_pszDSN) free(m_pszDSN);
	if(m_pszUser) free(m_pszUser);
	if(m_pszPW) free(m_pszPW);

	if(m_pszFileSpec) free(m_pszFileSpec); // must use malloc to allocate	
	if(m_pszOutputFileSpec) free(m_pszOutputFileSpec); // must use malloc to allocate	

	LeaveCriticalSection(&m_crit);
	DeleteCriticalSection(&m_crit);


}


void CGenericSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGenericSettings)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGenericSettings, CDialog)
	//{{AFX_MSG_MAP(CGenericSettings)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGenericSettings message handlers


int CGenericSettings::Settings(bool bRead)
{
	CFileUtil fu;
	if(fu.GetSettings(theApp.m_szSettingsFilename, false)&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
			char* pch;
			pch = fu.GetIniString("Global", "AppName", "Generic");
			if(pch)
			{
				if(m_pszAppName) free(m_pszAppName);
				m_pszAppName = pch;
			}

			pch = fu.GetIniString("Global", "InternalAppName", "Generic");
			if(pch)
			{
				if(m_pszInternalAppName) free(m_pszInternalAppName);
				m_pszInternalAppName = pch;
			}

			pch = (char*)malloc(strlen(m_pszAppName)+strlen(VERSION_STRING)+strlen(", version ")+1);
			if(pch)
			{
				if(m_pszAboutText) free(m_pszAboutText);
				m_pszAboutText = pch;
				sprintf(m_pszAboutText, "%s, version %s", m_pszAppName, VERSION_STRING);
			}

			pch = fu.GetIniString("Global", "SettingsText", "Generic settings...");
			if(pch)
			{
				if(m_pszSettingsText) free(m_pszSettingsText);
				m_pszSettingsText = pch;
			}

			m_ulDebug = fu.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.
			
			m_pszDSN = fu.GetIniString("Database", "DSN", "Tabulator", m_pszDSN);
			m_pszUser = fu.GetIniString("Database", "DBUser", "sa", m_pszUser);
			m_pszPW = fu.GetIniString("Database", "DBPassword", "", m_pszPW);

			m_pszFileSpec = fu.GetIniString("Messager", "LogFileIni", "Logs\\Generic|YD||1|", m_pszFileSpec);

			m_bOutputToFile = fu.GetIniInt("Output", "OutputToFile", 1)?true:false;
			m_pszOutputFileSpec= fu.GetIniString("Output", "OutputFileIni", "C:\\output.xml", m_pszOutputFileSpec);

//			int GetIniInt(char* pszSection, char* pszEntry, int nDefaultValue);
		}
		else // write
		{
			if(m_pszAppName) fu.SetIniString("Global", "AppName", m_pszAppName);
			if(m_pszAppName) fu.SetIniString("Global", "InternalAppName", m_pszInternalAppName);
//			if(m_pszAboutText) fu.SetIniString("Global", "AboutText", m_pszAboutText);  // dont save
			if(m_pszSettingsText) fu.SetIniString("Global", "SettingsText", m_pszSettingsText);
//			int SetIniInt(char* pszSection, char* pszEntry, int nValue, char* pszComment=NULL);

			fu.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.

			fu.SetIniString("Database", "DSN", m_pszDSN);
			fu.SetIniString("Database", "DBUser", m_pszUser);
			fu.SetIniString("Database", "DBPassword", m_pszPW);

			fu.SetIniString("Messager", "LogFileIni", m_pszFileSpec);
			fu.SetIniInt("Output", "OutputToFile", m_bOutputToFile?1:0);
			fu.SetIniString("Output", "OutputFileIni", m_pszOutputFileSpec);

			if(!(fu.SetSettings(theApp.m_szSettingsFilename, false)&FILEUTIL_MALLOC_OK)) return CX_ERROR;
		}

		return CX_SUCCESS;
	}
//	else AfxMessageBox("failed!");
	return CX_ERROR;
}




