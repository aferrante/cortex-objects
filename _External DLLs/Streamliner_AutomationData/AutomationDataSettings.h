#if !defined(AFX_AutomationDataSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_)
#define AFX_AutomationDataSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// AutomationDataSettings.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAutomationDataSettings 

class CAutomationDataSettings //: public CDialog
{
// Construction
public:
	CAutomationDataSettings(/*CWnd* pParent = NULL*/);   // standard constructor
	virtual ~CAutomationDataSettings();   // standard destructor
/*
// Dialog Data
	//{{AFX_DATA(CAutomationDataSettings)
	enum { IDD = IDD_DIALOG_SETTINGS };
	BOOL	m_bAutoconnect;
	BOOL	m_bTickerPlayOnce;
	CString	m_szHost;
	CString	m_szBaseURL;
	int		m_nBatch;
	int		m_nPort;
	int		m_nPollIntervalMS;
	CString	m_szProgramID;
	int		m_nRequestIntervalMS;
	int		m_nTickerPort;
	CString	m_szTickerHost;
	BOOL	m_bSmut_ReplaceCurses;
	int		m_nDoneCount;
	//}}AFX_DATA

	// internal
	bool m_bHasDlg;
	bool m_bIsServer;
	bool m_bObtainedFromServer;
	bool m_bFromDialog;

	int m_nMaxMessages;

	// UI
	COLORREF m_crCommitted;
*/
	CRITICAL_SECTION m_crit;

		// database stuff
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;



//	bool m_bUseFeed; // use the data download thread
//	char* m_pszFeed;  // the Feed table name
//	char* m_pszTicker;  // the Ticker table name
	char* m_pszMessages;  // the Messages table name
	char* m_pszAsRun;  // the As-run table name
	char* m_pszSettings;  // the Settings table name
	char* m_pszAutomationDatas;  // the AutomationDatas table name

	bool m_bMillisecondMessaging;			// use millisecond resolution for messages and asrun

// tables with data
	char* m_pszAutoModule;  // as in sentinel
	char* m_pszAutoLists;   // as in channels
	char* m_pszAutoEvents;   // as in event lists
	char* m_pszReplacementRules;
	char* m_pszDescriptionPattern;
	char* m_pszClassificationRules;
	char* m_pszButtonsTableName;  // for determining state

	char* m_pszTabulatorModule;
	char* m_pszTabulatorHost;  // the Tabulator host IP or name
	int m_nTabulatorPort;  // the Tabulator host port

	char* m_pszBlockedIDToken;
	char* m_pszBlockedDescToken;

	char* m_pszUnknownIDToken;
	char* m_pszUnknownDescToken;

	char* m_pszColDelimToken;
	char* m_pszClassTestFormat;

	char* m_pszTimerStartToken;
	char* m_pszTimerToken;
	char* m_pszTimerTable;


	bool m_bReadableEncodedAsRun;	

	bool m_bShowDiagnosticWindow;	

//	char* m_pszDestinationHost;  // do we want to direct drive the host ever?  or always shoot to Libretto or other module.? (still need the host either way but need to know what drive mode)

//	need module info, libretto command queue etc.
	char* m_pszModule;  // the module name (Libretto)
	char* m_pszQueue;  // the module's queue table name (Queue of Libretto.dbo.Queue)

	// need to be able to send 5 events.

/*
// the Replace Ad tabulator event.
//  Libretto: Replace Ad Message message

// the Replace Content tabulator event.
//  Libretto: Replace Content Message message


//  BLOCK GPO w Timer message
//  Tabulator:Timer:Timer_Start|Timer1|<timer duration in real ms>|<time code to start counting from BCD int>      // Start Timer1
//  Tabulator:GTP32:GPO_Set|3|1             // SET USP GPO 3 ON
//  Tabulator:GTP32:GPO_Set|3|0             // SET USP GPO 3 OFF "un-press" it - no effect


// the PASS tabulator event:
//  Stop Timer1
//  Tabulator:GTP32:GPO_Set|2|1             // SET USP GPO 2 ON
//  Libretto: Rejoin Main message
//  Tabulator:GTP32:GPO_Set|2|0             // SET USP GPO 2 OFF "un-press" it - no effect


// the BLOCK only tabulator event.
//  Libretto: Event notification message
//  Tabulator:GTP32:GPO_Set|3|1             // SET USP GPO 3 ON
//  Tabulator:GTP32:GPO_Set|3|0             // SET USP GPO 3 OFF "un-press" it - no effect
*/

	char* m_pszReplaceAdEvent;  // the event identifier of the replace ad event
	char* m_pszReplaceContentEvent;  // the event identifier of the replace content evnet
	char* m_pszEventNotificationEvent;  // the event identifier of the "BLOCK only" event
	char* m_pszRejoinMainEvent;  // the event identifier of the PASS tabulator event (includes Rejoin Main)
	char* m_pszTimerEvent;  // the event identifier of the BLOCK GPO w Timer event

	char* m_pszDrivenHost;  // IP address of the VANC inserter for libretto to route properly


	int m_nTriggerBuffer; // number of items to hold in a buffer for triggering

	int m_nProgramStreamOffset;
	int m_nTimerExpiryManualDelayMS;
	int m_nTimerExpiryAutoDelayMS;
	int m_nTimerStartOffsetMS;
	int m_nTimerDurationAdjustMS;

	int m_nAutomationAnalysisLookahead;  // number of events to parse.
	char* m_pszAutoServer;  // as in MAIN-DS
	int m_nAutoServerListNumber;   // as in 1
	bool m_bAutomationLookahead;			// value imported from Harris
	int m_nAutoListGetDelayMS;   // delay in between incrementer change and a list event get.

	int m_nAutomationLoggingLookahead;  // number of events to log from top.


/*
	char* m_pszInEvent;  // the event identifier of the transition in (Server_Ticker_In) command
	char* m_pszOutEvent;  // the event identifier of the transition out (Server_Ticker_Out) command
	char* m_pszTextEvent;  // the event identifier of the text message event
*/

//	char* m_pszAsRunFilename; 
//	char* m_pszAsRunDestination;

/*
	char* m_pszTickerMessagePreamble; 

	char* m_pszTickerBackplateName;   // for external graphics engine use.
	char* m_pszTickerLogoName;    // for external graphics engine use.
	int m_nTickerBackplateID; 
	int m_nTickerLogoID; 
	int m_nTickerTextID; 

	int m_nTickerEngineType; 
	int m_nTriggerBuffer; // number of items to hold in a buffer for triggering
	int m_nDefaultDwellTime;

	char* m_pszMessageToken; 
	char* m_pszAux1Token; 
	char* m_pszAux2Token; 

	bool m_bAutoPurgeExpired;
	bool m_bAutoPurgeExceeded;
	int m_nAutoPurgeExpiredAfterMins;
	int m_nAutoPurgeExceededAfterMins;
	bool m_bAutostartTicker;
	bool m_bAutostartData;
	bool m_bUseLocalTime;  // or use unixtime

	int m_nGraphicsHostType;

	//Harris Icon specific
	int m_nLayoutSalvoFormat; // 0= 4 digit numerical, 1=8 digit numerical, 2 = strings  
*/
	//exposed
	char* m_pszAppName;
	char* m_pszInternalAppName;
	char* m_pszAboutText;
	char* m_pszSettingsText;

	int GetFromDatabase(char* pszInfo=NULL);  //only get.  Interface publishes

	int Settings(bool bRead);

//	CString RemoteSettingsToString();
//	int StringToRemoteSettings(CString szSettings);

	unsigned long m_ulDebug;  // prints out debug statements that & with this.


	bool m_bUseTimeCode;			// use adrienne timecode
	int m_nFrameRate;   // for time code addresses NTSC=30, PAL=25
	bool m_bDF;  // Drop frame




/*
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAutomationDataSettings)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAutomationDataSettings)
	afx_msg void OnButtonTestfeed();
	virtual void OnOK();
	afx_msg void OnButtonConnect();
	virtual BOOL OnInitDialog();
	afx_msg void OnCheckPlayonce();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

*/
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AutomationDataSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_)
