// AutomationDataData.h: interface for the CAutomationDataData and CAutomationDataMessage classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AutomationDataDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_)
#define AFX_AutomationDataDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../../../Common/LAN/NetUtil.h"
#include "../../../Cortex Objects/Tabulator/TabulatorDefines.h"
//#include "../../../Cortex Objects/Tabulator/TabulatorData.h"
#include "../../../Common/MFC/ODBC/DBUtil.h"
#include "../../../Common/TIME/TimeConvert.h"
#include "../../../Common/API/SCTE/SCTE104Util.h"
#include "../../../Common/TXT/BufferUtil.h"


#define SENDMSG_SENDER_MAXLEN 64
#define SENDMSG_MESSAGE_MAXLEN 512


// same as CNucleusEvent definition, currently.

class CTabulatorEvent  
{
public:
	CTabulatorEvent();
	virtual ~CTabulatorEvent();

	unsigned long m_ulFlags;
	double m_dblTriggerTime; 
	bool m_bAnalyzed;
	bool m_bReAnalyzed;
	int  m_nAnalyzedTriggerID;
	int  m_nEventID;
	int  m_nDestType;
	
	int m_nType;
	CString m_szName;
	CString m_szHost;
	CString m_szSource;
	CString m_szScene;
	CString m_szIdentifier;
	CString m_szValue;
	CString m_szParamDependencies;
	CString m_szDestModule;
	CString m_szLayer;
};



class CTabulatorEventArray  
{
public:
	CTabulatorEventArray();
	virtual ~CTabulatorEventArray();

	void	DeleteEvents();

	int m_nEvents;
	CTabulatorEvent** m_ppEvents;
	CRITICAL_SECTION m_critEvents;
	int m_nEventMod;
	int m_nLastEventMod;

};

class CAutomationList  
{
public:
	CAutomationList();
	virtual ~CAutomationList();

	int m_nState;
	int m_nChange;
	int m_nDisplay;
	int m_nSysChange;
	int m_nCount; 
	int m_nLookahead;
	double m_dblLastUpdate;
};


class CAutomationEvent  
{
public:
	CAutomationEvent();
	virtual ~CAutomationEvent();

 	int m_nID;
	unsigned short m_usType;
	char* m_pszID;
	char* m_pszTitle;
	char* m_pszReconcileKey;
	unsigned char  m_ucSegment;
	unsigned long  m_ulOnAirTimeMS;					// ideal milliseconds as appears in the list
	unsigned long  m_ulOnAirLocalUnixDate;  //in seconds, offset from January 1, 1900
	unsigned long  m_ulDurationMS;          // ideal milliseconds as appears in the list
	unsigned long  m_ulSOMMS;               // ideal milliseconds as appears in the list
	unsigned short m_usStatus;
//	unsigned short m_usControl; // was this
	unsigned long m_ulControl;  // made it this, storing revent->extrathree (which is extended event control in recfm80) in the upper word

	// potential output variables.
	unsigned long m_ulClassification;
	char* m_pszOutputString; // like:
	// Replace Ad BLOCK (for advertisments that are the first in the pod)
	// PASS (for not-ads that do not have replacements)
	// ReplaceContent <ID1> <ID2> BLOCK (for not-ads that do not have replacements)
	// PENDING (for items not analyzed yet)
	// Timer suppressed (for items suppressed by the timer - either type)
};





class CReplacementRule  
{
public:
	CReplacementRule();
	virtual ~CReplacementRule();

//create table ReplacementRules (ID int identity(1,1), programID varchar(16), sequence int, replacementID varchar(16), duration int, valid_date decimal(20,3), expire_date decimal(20,3))  
 	int m_nID;
	char* m_pszProgramID;

 	int m_nSequence;
	char* m_pszReplacementID;
	int m_nDuration; // in ideal MS
	int m_nDurationInFrames; // in frames
	int m_nOutputDuration; //  in ideal MS

	double m_dblValidDate;
	double m_dblExpireDate;

	int m_nOffset;
};


class CDescriptionPattern  
{
public:
	CDescriptionPattern();
	virtual ~CDescriptionPattern();
  
//create table DescriptionPattern (ID int identity(1,1), programID varchar(16), pattern varchar(128), valid_date decimal(20,3), expire_date decimal(20,3))  
 	int m_nID;
	char* m_pszProgramID;
	char* m_pszPattern;

	double m_dblValidDate;
	double m_dblExpireDate;
};


class CClassificationRule
{
public:
	CClassificationRule();
	virtual ~CClassificationRule();

	//create table ClassificationRules (ID int identity(1,1), pattern varchar(128), col_name varchar(64), event_type int)  
	int m_nID;
	char* m_pszPattern;
	char* m_pszColName;
	int m_nEventType; // long from is 2, advertisement probably 1, none =0
};


class CMessageOverrideValues
{
public:
	CMessageOverrideValues();
	virtual ~CMessageOverrideValues();

// following char arrays have an extra byte for terminating zero
// the times should all be in "real" millseconds.
// event notification portion of stream messages
	unsigned long m_ulEN_EventStart;						// The current LTC time address, plus the Program Stream Offset. for manual event 
	unsigned long m_ulEN_EventDur;							// The time address 00:00:00;00.  for manual event
	char m_pszEN_ProgramID[17];						      	// The value "TV+:UNKNOWN".  for manual event
	char m_pszEN_ProgramDescription[33];						// Blank.  for manual event

// context sensitive overrides, ad content, ad replacement
	unsigned long m_ulRepl_EventStart;						// not used for ad replacement, offset for content replacement, The current LTC time address, plus the Program Stream Offset if not available
	unsigned long m_ulRepl_EventDur;							// duration for ad replacement, length for content replacement 00:00:00;00 if not available.
	char m_pszRepl_ProgramID[17];						      	// The value "TV+:UNKNOWN".  if not available
	char m_pszRepl_ProgramDescription[33];						// not used for content replacement Blank.  if not available

// if there is an original ID from a table, to do lookups on...
	char m_pszOriginal_ProgramID[17];			
	unsigned long m_ulActualTimerDurationMS;						// for the duration, we have a required duration in real milliseconds and we have a start tc in drop frame non-compensated, BCD encoded TC . have to convert the m_ulEN_EventStart value.

	int m_nReplacementType; // just record the replacement type here to make it easier to parse it internally
	int m_nAutomationClass; // just record the automation type here to make it easier to parse it internally
};


class CSyncVariables
{
public:
	CSyncVariables();
	virtual ~CSyncVariables();

	// use these values to sync or defeat possible simultaneous automation events
	int m_nLastTimerStart;
	int m_nLastTimerDuration;
	bool m_bLastTimerEventStartAligned; // if a timer was set such that it ends exactly when an automation event begins, set this to true to defeat incoming automation event.
	int m_nLastTimerEventType;
	bool m_bTimerActionPending;


	// "squirrel away" these values for re-use
	int m_nLastENEventStart;
	int m_nLastENEventDur;
};


class COutputVariables
{
public:
	COutputVariables();
	virtual ~COutputVariables();

	bool m_b_list_is_playing;  // automation list is playing a primary event.

	//NOTE!  three different meanings of the word "current" - be aware!

	// here, current means in-progess, OR most recently past.
	char* m_psz_current_long_form_ID; // = Harris ID of last primary event classified as long-form.
	char* m_psz_current_long_form_description;// = Harris Title of last primary event classified as long-form.
	int m_n_current_long_form_start; // = Harris on air time of last primary event classified as long-form. in ideal MS
	int m_n_current_long_form_duration;//  = Harris duration of last primary event classified as long-form. in ideal MS

	// here, next means after the "current", as defined above
	int m_n_next_long_form_start; // = Harris on air time of next primary event classified as long-form, or if no such event exists in the playlist, the starting time address of the Next Long-Form Program is the ending time address of the last primary event in the playlist plus one frame, in ideal ms

	// here current means the in progress one, as we will not have messages for upcoming or past ones.
	int m_n_current_ad_pod_start; // =  Harris on air time of last ad pod in ideal ms
	int m_n_current_ad_pod_duration;// =  Assembled Harris duration of last ad pod in real ms

	// here, current means the currently playing primary event.
	int m_n_current_primary_class; 
	int m_n_current_primary_start; // in ideal MS; 
	int m_n_current_primary_duration;  //in ideal MS 
	char* m_psz_current_primary_ID; // = Harris ID of current
	char* m_psz_current_primary_description;// = Harris Title of current
};



class CAutomationDataData  
{
public:
	CAutomationDataData();
	virtual ~CAutomationDataData();

	// remove pipes
	CString Encode(CString szText);
	// put back pipes
	CString Decode(CString szText);

	int InitDLLData(DLLthread_t* pData);

	CString XMLTextNodeValue(IXMLDOMNodePtr pNode);

	int SendMsg(int nType, char* pszSender, char* pszMessage, ...);
//	int SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...);
	int SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...);


//	SOCKET m_socket;
//	CRITICAL_SECTION m_critClientSocket;

	CNetUtil m_net;
	CDBUtil m_dbu;
	CSCTE104Util m_scte104;  //global just so the message number is global (not that it matters - it gets reassigned at Libretto).
	CTimeConvert m_timeconv; //time converison utility
	CBufferUtil m_bu;

	SOCKET m_socketTabulator;
	BOOL m_bInCommand;


//	CRITICAL_SECTION m_critMsg;
	CRITICAL_SECTION m_critSQL;
	CRITICAL_SECTION m_critOutputSQL;

	CTabulatorEventArray* m_pEvents;
//	CTabulatorEventArray* m_pInEvents;
//	CTabulatorEventArray* m_pOutEvents;
//	CTabulatorEventArray* m_pTextEvents;
	CRITICAL_SECTION m_critEventsSettings;
/*
//just here for the descriptions
	m_bReplaceAdEventChanged=false;  // the event identifier of the replace ad has changed
	m_bReplaceContentEventChanged=false;  // the event identifier of the replace content has changed
	m_bEventNotificationEventChanged=false;  // the event identifier of the "BLOCK only" event has changed
	m_bRejoinMainEventChanged=false;  // the event identifier of the PASS tabulator event (includes Rejoin Main) has changed
	m_bTimerEventChanged=false;  // the event identifier of the BLOCK GPO w Timer event has changed
*/
	CTabulatorEventArray* m_pReplaceAdEvents;
	CTabulatorEventArray* m_pReplaceContentEvents;
	CTabulatorEventArray* m_pEventNotificationEvents;
	CTabulatorEventArray* m_pRejoinMainEvents;
	CTabulatorEventArray* m_pTimerEvents;

	CSCTE104MultiMessage* m_pscte104MsgEventNotification;
	CSCTE104MultiMessage* m_pscte104MsgReplaceAd;
	CSCTE104MultiMessage* m_pscte104MsgReplaceContent;
	CSCTE104MultiMessage* m_pscte104MsgRejoinMain;
	CSCTE104MultiMessage* m_pscte104MsgCurrentTime;


	int InitializeEventNotificationObject(CSCTE104MultiMessage* pscte104MsgEventNotification);
	int InitializeReplaceAdObject();
	int InitializeReplaceContentObject();
	int InitializeRejoinMainObject();
	int InitializeCurrentTime();

	int UpdateEventNotificationObject(CSCTE104MultiMessage* pscte104Msg, char* pszID, char* pszDesc, unsigned long ulStartTC, unsigned long ulDurationMS);
	int UpdateReplaceAdObject(CSCTE104MultiMessage* pscte104Msg, char* pszID, char* pszDesc, unsigned long ulDurationMS);
	int UpdateReplaceContentObject(CSCTE104MultiMessage* pscte104Msg, char* pszID, unsigned long ulOffsetMS, unsigned long ulDurationMS);

	int TestPattern(char* pszValue, char* pszPattern);
	int CheckTimer();
	int CheckTimerExpiryAlreadyProcessedAutomationEvent();
	int CheckState();

	int PlayEvent(CTabulatorEventArray** ppEvent, CMessageOverrideValues* pOverrides=NULL, char* pszInfo=NULL);
	CRITICAL_SECTION m_critPlayEvent;  // need to enforce one at a time.


	int RefreshTabulatorEvents(	CTabulatorEventArray* m_pEvents, char* pszToken );

	int GetReplacementRules();
	int GetDescriptionPatterns();
	int GetClassificationRules();
	int GetAutomationEvents();
	int GetAutomationListStatus();
	int GetClassification(CAutomationEvent* pEvent);

	int ReportAutomationEvents();

	int Replace(bool bCurrent, bool bTimer=false);
	int Replace_Manual(CMessageOverrideValues* pOverrides);
	int Enter_Pass(bool bCurrent, bool bTimer=false);
	int Enter_Block();
	int Query_Ready();
	int CountCurrentReplacementRules(double dblTime); // use system time

	int IdealAddTimeAndDuration(int nStart, int nDuration);

	int ClearOutputVariables(bool bExcludeLongForm=false);
	
	bool m_bInitalEventGetSucceeded;

	int m_nLastEventMod;

	bool m_bReplaceAdEventChanged;  // the event identifier of the replace ad has changed
	bool m_bReplaceContentEventChanged;  // the event identifier of the replace content has changed
	bool m_bEventNotificationEventChanged;  // the event identifier of the "BLOCK only" event has changed
	bool m_bRejoinMainEventChanged;  // the event identifier of the PASS tabulator event (includes Rejoin Main) has changed
	bool m_bTimerEventChanged;  // the event identifier of the BLOCK GPO w Timer event has changed


	CDBUtil*  m_pdb;
	CDBconn* m_pdbConn;

	CDBUtil*  m_pdbOutput;
	CDBconn* m_pdbOutputConn;

//	CRITICAL_SECTION m_critTransact;

	CRITICAL_SECTION m_critTimeCode;
	unsigned long m_ulTimeCode;
	unsigned char m_ucTimeCodeBits;
	unsigned long m_ulTimeCodeElapsed;
	bool m_bTimeCodeThreadStarted;
	bool m_bTimeCodeThreadKill;

	int m_nLastTime;
	int m_nLastMillitm;
	int m_nTimeIncr;
	int m_nMillitmIncr;


//	int GetAutomationDatas(char* pszInfo=NULL);
//	int FindAutomationData(char* pszAutomationDataName);

	CReplacementRule** m_ppReplacementRules;
	int m_nReplacementRules;
	int m_nReplacementRulesArraySize;

	CDescriptionPattern** m_ppDescriptionPatterns;
	int m_nDescriptionPatterns;
	int m_nDescriptionPatternsArraySize;

	CClassificationRule** m_ppClassificationRules;
	int m_nClassificationRules;
	int m_nClassificationRulesArraySize;

	CRITICAL_SECTION m_critTableData;

	CAutomationList m_AutoList;

	CAutomationEvent** m_ppAutoEvents;
	int m_nAutoEvents;
	int m_nAutoEventsArraySize;

	COutputVariables m_Output;
	CRITICAL_SECTION m_critOutputVariables;

	CSyncVariables m_Sync;
	CRITICAL_SECTION m_critSyncVariables;

	CRITICAL_SECTION m_critAutomationEvents;

	bool m_bAutoAnalysisThreadStarted;
	bool m_bAutoAnalysisThreadKill;


};




#endif // !defined(AFX_AutomationDataDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_)
