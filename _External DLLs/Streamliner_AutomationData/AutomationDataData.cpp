// AutomationDataData.cpp: implementation of the CAutomationDataData and CAutomationDataMessage classes.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AutomationData.h"
#include "AutomationDataCore.h"
#include "AutomationDataData.h"
#include "AutomationDataSettings.h"
#include <atlbase.h>
#include <process.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CAutomationDataCore	g_core;
extern CAutomationDataData	g_data;
extern CAutomationDataSettings	g_settings;
extern CMessager*			g_ptabmsgr;
extern DLLdata_t       g_dlldata;

#define SENDMSG_EVENT_MAXLEN 64
#define SENDMSG_SENDER_MAXLEN 64
#define SENDMSG_MESSAGE_MAXLEN 512




//////////////////////////////////////////////////////////////////////
// CTabulatorEvent Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTabulatorEvent::CTabulatorEvent()
{
	m_ulFlags = TABULATOR_FLAG_DISABLED;
	m_dblTriggerTime = -1.0; 
	m_bAnalyzed = false;
	m_bReAnalyzed = false;
	m_nEventID = -1;	
	m_nAnalyzedTriggerID = -1;
	m_nType = -1;
	m_nDestType = -1;
}

CTabulatorEvent::~CTabulatorEvent()
{
}

CTabulatorEventArray::CTabulatorEventArray()
{
	m_nEvents = 0; 
	m_ppEvents = NULL;
	InitializeCriticalSection(&m_critEvents);
	m_nEventMod=0;
	m_nLastEventMod=-1;
}

CTabulatorEventArray::~CTabulatorEventArray()
{
	EnterCriticalSection(&m_critEvents);
	DeleteEvents();
	LeaveCriticalSection(&m_critEvents);
	DeleteCriticalSection(&m_critEvents);
}

void	CTabulatorEventArray::DeleteEvents()
{
	if((m_nEvents)&&(m_ppEvents))
	{
		int i=0;
		while(i<m_nEvents)
		{
			if(m_ppEvents[i]) delete m_ppEvents[i];
			m_ppEvents[i] = NULL;
			i++;
		}
		delete [] m_ppEvents;
	}
	m_ppEvents = NULL;
	m_nEvents = 0;
}


CAutomationList::CAutomationList()
{
	m_nState=-1;
	m_nChange=-1;
	m_nDisplay=-1;
	m_nSysChange=-1;
	m_nCount=-1;
	m_nLookahead=-1;
	m_dblLastUpdate = -1.0;
}

CAutomationList::~CAutomationList()
{
}


CAutomationEvent::CAutomationEvent()
{
 	m_nID = -1;
	m_usType = 0xffff;
	m_pszID = NULL;
	m_pszTitle = NULL;
	m_pszReconcileKey = NULL;
	m_ucSegment = 0xff;
	m_ulOnAirTimeMS = 0xffffffff;					// ideal milliseconds as appears in the list
	m_ulOnAirLocalUnixDate = 0xffffffff;  //in seconds, offset from January 1, 1900
	m_ulDurationMS = 0xffffffff;          // ideal milliseconds as appears in the list
	m_ulSOMMS = 0xffffffff;               // ideal milliseconds as appears in the list
	m_usStatus = 0xffff;
//	unsigned short m_usControl; // was this
	m_ulControl = 0xffffffff;  // made it this, storing revent->extrathree (which is extended event control in recfm80) in the upper word

	// potential output variables.
	m_ulClassification = AUTODATA_CLASS_TYPE_NONE;
	m_pszOutputString = NULL; // like:
	// Replace Ad BLOCK (for advertisments that are the first in the pod)
	// PASS (for not-ads that do not have replacements)
	// ReplaceContent <ID1> <ID2> BLOCK (for not-ads that do not have replacements)
	// PENDING (for items not analyzed yet)
	// Timer suppressed (for items suppressed by the timer - either type)
}


CAutomationEvent::~CAutomationEvent()
{
	if(m_pszID) free(m_pszID);
	if(m_pszTitle) free(m_pszTitle);
	if(m_pszReconcileKey) free(m_pszReconcileKey);
	if(m_pszOutputString) free(m_pszOutputString);
}



CSyncVariables::CSyncVariables()
{
	// use these values to sync or defeat possible simultaneous automation events
	m_nLastTimerStart=-1;
	m_nLastTimerDuration=-1;
	m_bLastTimerEventStartAligned=false; // if a timer was set such that it ends exactly when an automation event begins, set this to true to defeat incoming automation event.
	m_nLastTimerEventType = AUTODATA_MANUAL_TYPE_UNKNOWN;
	
	m_bTimerActionPending=false;



	// "squirrel away" these values for re-use
	m_nLastENEventStart=-1;
	m_nLastENEventDur=-1;
}

CSyncVariables::~CSyncVariables()
{
}



COutputVariables::COutputVariables()
{
	m_b_list_is_playing = false;  // automation list is playing a primary event.

	//NOTE!  three different meaings of the word "current" - be aware!

	// here, current means in-progess, OR most recently past.
	m_psz_current_long_form_ID=NULL; // = Harris ID of last primary event classified as long-form.
	m_psz_current_long_form_description=NULL;// = Harris Title of last primary event classified as long-form.
	m_n_current_long_form_start=AUTODATA_INVALID_TIMEADDRESS; // = Harris on air time of last primary event classified as long-form. in ideal MS
	m_n_current_long_form_duration=AUTODATA_INVALID_TIMEADDRESS;//  = Harris duration of last primary event classified as long-form. in ideal MS

	// here, next means after the "current", as defined above
	m_n_next_long_form_start=AUTODATA_INVALID_TIMEADDRESS; // = Harris on air time of next primary event classified as long-form, or if no such event exists in the playlist, the starting time address of the Next Long-Form Program is the ending time address of the last primary event in the playlist plus one frame, in ideal ms

	// here current means the in progress one, as we will not have messages for upcoming or past ones.
	m_n_current_ad_pod_start=AUTODATA_INVALID_TIMEADDRESS; // =  Harris on air time of last ad pod in ideal ms
	m_n_current_ad_pod_duration=AUTODATA_INVALID_TIMEADDRESS;// =  Assembled Harris duration of last ad pod  in real ms

	// here, current means the currently playing primary event.
	m_n_current_primary_class=AUTODATA_CLASS_TYPE_NONE; 
	m_n_current_primary_start=AUTODATA_INVALID_TIMEADDRESS; // in ideal MS; 
	m_n_current_primary_duration=AUTODATA_INVALID_TIMEADDRESS;  //in ideal MS 
	m_psz_current_primary_ID=NULL; // = Harris ID of current
	m_psz_current_primary_description=NULL;// = Harris Title of current
}

COutputVariables:: ~COutputVariables()
{
	if(m_psz_current_long_form_ID) free(m_psz_current_long_form_ID);
	if(m_psz_current_long_form_description) free(m_psz_current_long_form_description);
	if(m_psz_current_primary_ID) free(m_psz_current_primary_ID);
	if(m_psz_current_primary_description) free(m_psz_current_primary_description);
}





CReplacementRule::CReplacementRule()
{
	m_nID=-1;
	m_pszProgramID=NULL;
	m_pszReplacementID=NULL;

 	m_nSequence=-1;
	m_nDuration=-1; // in ideal MS, invalid to start
	m_nDurationInFrames = -1; // invalid
	m_nOutputDuration = 0; // nuttin

	m_dblValidDate = 0.0;
	m_dblExpireDate = 0.0;

	m_nOffset = 0; // time address 0
}

CReplacementRule::~CReplacementRule()
{
	if(m_pszReplacementID) free(m_pszReplacementID);
	if(m_pszProgramID) free(m_pszProgramID);
}


CDescriptionPattern::CDescriptionPattern()
{
	m_nID=-1;
	m_pszProgramID=NULL;
	m_pszPattern=NULL;

	m_dblValidDate = 0.0;
	m_dblExpireDate = 0.0;
}

CDescriptionPattern::~CDescriptionPattern()
{
	if(m_pszPattern) free(m_pszPattern);
	if(m_pszProgramID) free(m_pszProgramID);
}

CClassificationRule::CClassificationRule()
{
	m_nID=-1;
	m_pszPattern=NULL;
	m_pszColName=NULL;
	m_nEventType=AUTODATA_CLASS_TYPE_NONE;
}

CClassificationRule::~CClassificationRule()
{
	if(m_pszPattern) free(m_pszPattern);
	if(m_pszColName) free(m_pszColName);
}



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/*
CAutomationDataMessage::CAutomationDataMessage()
{
	m_szMessage="";
	m_szRevisedMessage="";
	m_nTimestamp =-1;
	m_nLastUpdate =-1;
	m_nID = -1;
	m_nStatus = AutomationData_MSG_STATUS_NONE;
	m_nType = AutomationData_MSG_TYPE_NONE;
	m_nPlayed = 0;
	m_dblIndex = -0.1;
}

CAutomationDataMessage::~CAutomationDataMessage()
{
}


CString CAutomationDataMessage::SQLUpdate(bool bTicker, double dblSeekIndex)
{
  CString szSQL="";
	if(bTicker)
	{
	char* szMsg = g_data.m_dbu.EncodeQuotes(m_szMessage);
	if(m_nStatus&AutomationData_MSG_STATUS_INSERTED)
	{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "upd item (%d [%s])", m_nID, m_szMessage); //(Dispatch message)
		szSQL.Format("UPDATE %s SET \
[text] = '%s', \
[sort_order] = %.4f, \
[timestamp] = %d, \
[updated] = %d, \
[status] = %d, \
[type] = %d, \
[played] = %d WHERE [id] = %d AND [sort_order] >= %.4f AND [sort_order] <= %.4f", 
					g_settings.m_pszTicker, 
					(szMsg?szMsg:m_szMessage),
					m_dblIndex,
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|AutomationData_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed,
					m_nID,
					dblSeekIndex-0.0000009,
					dblSeekIndex+0.0000009
					);  // table name
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "upd item (%d [%s]) SQL %s", m_nID, m_szMessage, szSQL);//(Dispatch message)
	}
	else
	{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "ins item (%d [%s])", m_nID, m_szMessage); //(Dispatch message)
		szSQL.Format("INSERT INTO %s (\
[id], \
[text], \
[sort_order], \
[timestamp], \
[updated], \
[status], \
[type], \
[played]) VALUES \
(%d, '%s', %.4f, %d, %d, %d, %d, %d)",
					g_settings.m_pszTicker,  // table name
					m_nID,
					(szMsg?szMsg:m_szMessage),
					m_dblIndex,
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|AutomationData_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "ins item (%d [%s]) SQL %s", m_nID, m_szMessage, szSQL); //(Dispatch message)
	}
	if(szMsg) free(szMsg);
	}
	else
	{
	char* szMsg = g_data.m_dbu.EncodeQuotes(m_szMessage);
	char* szRevMsg = g_data.m_dbu.EncodeQuotes(m_szRevisedMessage);
	if(m_nStatus&AutomationData_MSG_STATUS_INSERTED)
	{
		szSQL.Format("UPDATE %s SET \
[text] = '%s', \
[revision] = '%s', \
[timestamp] = %d, \
[updated] = %d, \
[status] = %d, \
[type] = %d, \
[played] = %d WHERE [id] = %d", g_settings.m_pszFeed, 
					(szMsg?szMsg:m_szMessage),
					(szRevMsg?szRevMsg:m_szRevisedMessage),
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|AutomationData_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed,
					m_nID);  // table name
	}
	else
	{

		szSQL.Format("INSERT INTO %s (\
[id], \
[text], \
[revision], \
[timestamp], \
[updated], \
[status], \
[type], \
[played]) VALUES \
(%d, '%s', '%s', %d, %d, %d, %d, %d)",
					g_settings.m_pszFeed,  // table name
					m_nID,
					(szMsg?szMsg:m_szMessage),
					(szRevMsg?szRevMsg:m_szRevisedMessage),
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|AutomationData_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed);
	}

	if(szMsg) free(szMsg);
	if(szRevMsg) free(szRevMsg);
	}

	return szSQL;
}

*/

/*
int CAutomationDataItem::PublishItem(unsigned long ulFlags)
{
	if(( g_data.m_pdb )&&(	g_data.m_pdbConn))
	{

		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		if(ulFlags&AUTODATA_UPDATE_COUNTONLY)
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET count = %d WHERE uid = %d",  //HARDCODE
					((g_settings.m_pszAutomationData)&&(strlen(g_settings.m_pszAutomationData)))?g_settings.m_pszAutomationData:"AutomationData",
					m_nCount,
					m_nItemID
					);
		}
		else
		{			
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d, duration = %d, start_tc = %d, stop_tc = %d, \
count = %d, paused_duration = %d, paused_count = %d, last_started = %.03f, last_stopped = %.03f WHERE uid = %d",  //HARDCODE
					((g_settings.m_pszAutomationData)&&(strlen(g_settings.m_pszAutomationData)))?g_settings.m_pszAutomationData:"AutomationData",
					m_ulStatus,

					m_ulDuration,
					((m_ulStartTC==0xffffffff)?-1:m_ulStartTC),
					((m_ulStopTC==0xffffffff)?-1:m_ulStopTC),

					m_nCount,
					m_ulPausedDuration,
					m_ulTimesPaused,
					
					(((double)(m_timeLastStarted.time)) + ((double)(m_timeLastStarted.millitm))/1000.0),
					(((double)(m_timeLastStopped.time)) + ((double)(m_timeLastStopped.millitm))/1000.0),

					m_nItemID
					);
		}
//			Sleep(1); // dont peg processor
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_SQL)) 
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:update", "SQL: %s", szSQL); // Sleep(250); //(Dispatch message)

EnterCriticalSection(&g_data.m_critSQL);
		if(g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//**MSG
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "AutomationData:update", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

		}
LeaveCriticalSection(&g_data.m_critSQL);

	}
	return CLIENT_ERROR;
}
*/


CMessageOverrideValues::CMessageOverrideValues()
{
// event notification portion of stream messages
	m_ulEN_EventStart=AUTODATA_INVALID_TIMEADDRESS;						// The current LTC time address, plus the Program Stream Offset. for manual event 
	m_ulEN_EventDur=AUTODATA_INVALID_TIMEADDRESS;							// The time address 00:00:00;00.  for manual event
	memset(m_pszEN_ProgramID, 0, 17);						      	// The value "TV+:UNKNOWN".  for manual event
	memset(m_pszEN_ProgramDescription, 0, 33);						// Blank.  for manual event

// context sensitive overrides, ad content, ad replacement
	m_ulRepl_EventStart=AUTODATA_INVALID_TIMEADDRESS;						// not used for ad replacement, offset for content replacement, The current LTC time address, plus the Program Stream Offset if not available
	m_ulRepl_EventDur=AUTODATA_INVALID_TIMEADDRESS;							// pod duration for ad replacement, length for content replacement 00:00:00;00 if not available.
	memset(m_pszRepl_ProgramID, 0, 17);						      	// The value "TV+:UNKNOWN".  if not available
	memset(m_pszRepl_ProgramDescription, 0, 33);						// not used for content replacement Blank.  if not available

// if there is an original ID from a table, to do lookups on...
	memset(m_pszOriginal_ProgramID, 0, 17);
	m_ulActualTimerDurationMS = AUTODATA_INVALID_TIMEADDRESS;

	m_nReplacementType = AUTODATA_MANUAL_TYPE_UNKNOWN; // just record the replacement type here to make it easier to parse it internally
	m_nAutomationClass = AUTODATA_CLASS_TYPE_NONE; // just record the automation type here to make it easier to parse it internally


}

CMessageOverrideValues::~CMessageOverrideValues()
{
}










//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAutomationDataData::CAutomationDataData()
{
	m_bInitalEventGetSucceeded = false;
	m_socketTabulator = NULL;
	m_bInCommand = FALSE;

	m_bAutoAnalysisThreadStarted=false;
	m_bAutoAnalysisThreadKill=true;


	m_nLastTime = -1;
	m_nLastMillitm = -1;
	m_nTimeIncr = 0;
	m_nMillitmIncr = 0;




	m_pEvents = NULL;
//	m_pInEvents = new CTabulatorEventArray;
//	m_pOutEvents = new CTabulatorEventArray;
//	m_pTextEvents = new CTabulatorEventArray;

//	m_socket=NULL;

	m_pdb=NULL;
	m_pdbConn=NULL;
	m_pdbOutput=NULL;
	m_pdbOutputConn=NULL;
	InitializeCriticalSection(&m_critSQL);
	InitializeCriticalSection(&m_critOutputSQL);
	

	InitializeCriticalSection(&m_critTimeCode);
	m_bTimeCodeThreadStarted = false;
	m_bTimeCodeThreadKill = true;

	m_ucTimeCodeBits = 0xff; //invalid value
	m_ulTimeCode = 0xffffffff; //invalid value
	m_ulTimeCodeElapsed = 0;

	InitializeCriticalSection(&m_critEventsSettings);

	m_nLastEventMod = -3;

	m_bReplaceAdEventChanged=false;  // the event identifier of the replace ad has changed
	m_bReplaceContentEventChanged=false;  // the event identifier of the replace content has changed
	m_bEventNotificationEventChanged=false;  // the event identifier of the "BLOCK only" event has changed
	m_bRejoinMainEventChanged=false;  // the event identifier of the PASS tabulator event (includes Rejoin Main) has changed
	m_bTimerEventChanged=false;  // the event identifier of the BLOCK GPO w Timer event has changed

	m_pReplaceAdEvents = new CTabulatorEventArray;
	m_pReplaceContentEvents = new CTabulatorEventArray;
	m_pEventNotificationEvents = new CTabulatorEventArray;
	m_pRejoinMainEvents = new CTabulatorEventArray;
	m_pTimerEvents = new CTabulatorEventArray;


	m_pscte104MsgEventNotification = NULL;
	m_pscte104MsgReplaceAd         = NULL;
	m_pscte104MsgReplaceContent    = NULL;
	m_pscte104MsgRejoinMain        = NULL;
	m_pscte104MsgCurrentTime       = NULL;

//	m_ppAutomationData=NULL;
//	m_nAutomationData=0;


	m_ppReplacementRules=NULL;
	m_nReplacementRules=-1;  //uninit

	m_ppDescriptionPatterns=NULL;
	m_nDescriptionPatterns=-1;  //uninit

	m_ppClassificationRules=NULL;
	m_nClassificationRules=-1;  //uninit

	m_nReplacementRulesArraySize = 0;
	m_nDescriptionPatternsArraySize = 0;
	m_nClassificationRulesArraySize = 0;


	InitializeCriticalSection(&m_critTableData);

	InitializeCriticalSection(&m_critOutputVariables);
	InitializeCriticalSection(&m_critSyncVariables);
	
	InitializeCriticalSection(&m_critAutomationEvents);
	InitializeCriticalSection(&m_critPlayEvent);  // need to enforce one at a time.


}

CAutomationDataData::~CAutomationDataData()
{
	m_bTimeCodeThreadKill = true;
//	DeleteCriticalSection(&m_critClientSocket);
	DeleteCriticalSection(&m_critSQL);
	DeleteCriticalSection(&m_critOutputSQL);
//	if(	m_pInEvents) delete  m_pInEvents;
//	if(	m_pOutEvents) delete  m_pOutEvents;
//	if(	m_pTextEvents) delete  m_pTextEvents;
	DeleteCriticalSection(&m_critTimeCode);
	DeleteCriticalSection(&m_critEventsSettings);
	if(	m_pReplaceAdEvents) delete  m_pReplaceAdEvents;
	if(	m_pReplaceContentEvents) delete  m_pReplaceContentEvents;
	if(	m_pEventNotificationEvents) delete  m_pEventNotificationEvents;
	if(	m_pRejoinMainEvents) delete  m_pRejoinMainEvents;
	if(	m_pTimerEvents) delete  m_pTimerEvents;

	if(m_pscte104MsgEventNotification) delete  m_pscte104MsgEventNotification;
	if(m_pscte104MsgReplaceAd) delete  m_pscte104MsgReplaceAd;
	if(m_pscte104MsgReplaceContent) delete  m_pscte104MsgReplaceContent;
	if(m_pscte104MsgRejoinMain) delete  m_pscte104MsgRejoinMain;
	if(m_pscte104MsgCurrentTime) delete  m_pscte104MsgCurrentTime;


	EnterCriticalSection(&m_critTableData);
	if((m_ppReplacementRules)&&(m_nReplacementRules>0))
	{
		int i=0;
		while(i<m_nReplacementRules)
		{
			if(m_ppReplacementRules[i])
			{
				delete m_ppReplacementRules[i];
			}
			i++;
		}
		delete [] m_ppReplacementRules;
	}
	m_ppReplacementRules=NULL;
	m_nReplacementRules=0; 

	if((m_ppDescriptionPatterns)&&(m_nDescriptionPatterns>0))
	{
		int i=0;
		while(i<m_nDescriptionPatterns)
		{
			if(m_ppDescriptionPatterns[i])
			{
				delete m_ppDescriptionPatterns[i];
			}
			i++;
		}
		delete [] m_ppDescriptionPatterns;
	}
	m_ppDescriptionPatterns=NULL;
	m_nDescriptionPatterns=0; 

	if((m_ppClassificationRules)&&(m_nClassificationRules>0))
	{
		int i=0;
		while(i<m_nClassificationRules)
		{
			if(m_ppClassificationRules[i])
			{
				delete m_ppClassificationRules[i];
			}
			i++;
		}
		delete [] m_ppClassificationRules;
	}
	m_ppClassificationRules=NULL;
	m_nClassificationRules=0; 

	LeaveCriticalSection(&m_critTableData);

	DeleteCriticalSection(&m_critTableData);
	DeleteCriticalSection(&m_critOutputVariables);
	DeleteCriticalSection(&m_critSyncVariables);
	DeleteCriticalSection(&m_critAutomationEvents);
	DeleteCriticalSection(&m_critPlayEvent);  // need to enforce one at a time.
}

// remove pipes
CString CAutomationDataData::Encode(CString szText)
{
  CString szOut = "";
  int i;
  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '|')
      szOut += "%:";
    else if (szText[i] == '%')
      szOut += "%%";
    else if ((szText[i] != '\n')&&(szText[i] != '\r'))  //strip returns while we are at it.
       szOut += (char) szText[i];
  }
  return szOut;
}

// put back pipes
CString CAutomationDataData::Decode(CString szText)
{
  CString szOut = "";
  int i;

  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '%')
    {
      i++; if (i>=szText.GetLength()) break;
      if (szText[i] == '%')
      {
        szOut += "%";
      }
      else if (szText[i] == ':')
      {
        szOut += "|";
      }
    }
    else szOut += (char) szText[i];
  }
  return szOut;
}


CString CAutomationDataData::XMLTextNodeValue(IXMLDOMNodePtr pNode)
{
	CString szText = _T("");
	if(pNode)
	{
		IXMLDOMNodeListPtr pValues = pNode->GetchildNodes();
		if (pValues != NULL)
		{
			IXMLDOMNodePtr pValue;
			while(pValue = pValues->nextNode())
			{
				DOMNodeType valnodeType;
				pValue->get_nodeType(&valnodeType);

				if(valnodeType == NODE_TEXT)
				{
USES_CONVERSION;
					szText = W2T(_bstr_t(pValue->GetnodeValue()));
					szText.TrimLeft();
					szText.TrimRight();
					return szText;
				}
			}
		}
	}
	return szText;
}



int CAutomationDataData::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszMessages)&&(strlen(g_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		if(g_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime) \
VALUES ('%s', '%s', %d, %d.%03d)", // HARDCODE
				g_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				g_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				g_settings.m_pszMessages
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
//			IncrementDatabaseMods(g_settings.m_pszMessages, dberrorstring);
		LeaveCriticalSection(&m_critSQL);
			return TABULATOR_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return TABULATOR_ERROR;
}
/*
int CAutomationDataData::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszMessages)&&(strlen(g_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			g_settings.m_pszMessages,
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			g_settings.m_pszMessages
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
//			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return TABULATOR_SUCCESS;
		}
	}
	return TABULATOR_ERROR;
}
*/

int CAutomationDataData::SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...)
{
 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszAsRun)&&(strlen(g_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		if((pszEventID)&&(strlen(pszEventID)>SENDMSG_EVENT_MAXLEN)) pszEventID[SENDMSG_EVENT_MAXLEN-1]=0;
		
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);
		char* pchEvent = m_pdb->EncodeQuotes(pszEventID);
		if(g_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, channelid, eventid) \
VALUES ('%s', '%s', %d, %d.%03d, %d, '%s')", // HARDCODE
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm,
				nChannelID,
				pchEvent?pchEvent:""
				);
		}
		else
		{

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log")
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
		if(pchEvent) free(pchEvent);

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
//			IncrementDatabaseMods(g_settings.m_pszAsRun, dberrorstring);
			return TABULATOR_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);

	}
	return TABULATOR_ERROR;
}

/*
int CAutomationDataData::SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszAsRun)&&(strlen(g_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log")
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "AutomationData_Asrun", "SQL %s", szSQL) ;// Sleep(100); //(Dispatch message)

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
//			IncrementDatabaseMods(g_settings.m_pszAsRun, dberrorstring);
			return TABULATOR_SUCCESS;
		}
		else
		{
if(g_ptabmsgr)
{
	char szSource[MAX_PATH]; sprintf(szSource, "%s_Asrun", g_settings.m_pszInternalAppName);
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "SQL error %s", dberrorstring) ;// Sleep(100); //(Dispatch message)
}
		}
		LeaveCriticalSection(&m_critSQL);

	}
	else
		{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "AutomationData_Asrun", "null conditions");// Sleep(100); //(Dispatch message)
		}
	return TABULATOR_ERROR;
}
*/

int CAutomationDataData::InitializeEventNotificationObject(CSCTE104MultiMessage* pscte104MsgEventNotification)
{
	if(pscte104MsgEventNotification)
	{
		// everything but current time gets event notification as OP1
		pscte104MsgEventNotification->m_ppscte104Ops[0]->OpId1High = 	0x81; //		opID [high order byte]						0x8150 user defined operation
		pscte104MsgEventNotification->m_ppscte104Ops[0]->OpId1Low = 0x50;		//		opID [low order byte]
		pscte104MsgEventNotification->m_ppscte104Ops[0]->OpDataLengthHigh = 0x00;			// xx		data_length [high order byte]			
		pscte104MsgEventNotification->m_ppscte104Ops[0]->OpDataLengthLow  = 0x47;			// xx		data_length [low order byte]			

		pscte104MsgEventNotification->m_ppscte104Ops[0]->proprietary_data_ptr = new unsigned char[0x47];
		if(pscte104MsgEventNotification->m_ppscte104Ops[0]->proprietary_data_ptr)
		{
			memset(pscte104MsgEventNotification->m_ppscte104Ops[0]->proprietary_data_ptr, 0x00, 0x47); // nulls

#pragma message(alert "fix this hardcoded token")

			pscte104MsgEventNotification->m_ppscte104Ops[0]->proprietary_data_ptr[0] = 'V';
			pscte104MsgEventNotification->m_ppscte104Ops[0]->proprietary_data_ptr[1] = 'D';
			pscte104MsgEventNotification->m_ppscte104Ops[0]->proprietary_data_ptr[2] = 'S';
			pscte104MsgEventNotification->m_ppscte104Ops[0]->proprietary_data_ptr[3] = ' ';
			pscte104MsgEventNotification->m_ppscte104Ops[0]->proprietary_data_ptr[4] = 0x01;  // event notification command
/*
			char buffer[12];
			_timeb timestamp;
			_ftime(&timestamp);
			tm* theTime = localtime( &timestamp.time	);
			strftime( buffer, 12, "%Y-%m-%d", theTime );

			memcpy(&pscte104MsgEventNotification->m_ppscte104Ops[0]->proprietary_data_ptr[5], buffer, 10);
		
//					pscte104MsgEventNotification->m_scte104Hdr.TimeStamp.time_type = 0x02;
//					pscte104MsgEventNotification->m_scte104Hdr.TimeStamp.time_ptr = new unsigned char[4]; // NO, this was already created with CreateMultiMessageObject.
			if(pscte104MsgEventNotification->m_scte104Hdr.TimeStamp.time_ptr)
			{
				unsigned long ulTimeCode;
				// get the current TC.
				if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
				{
					EnterCriticalSection(&g_data.m_critTimeCode);
					ulTimeCode = g_data.m_ulTimeCode;
		//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
					LeaveCriticalSection(&g_data.m_critTimeCode);
				}
				else
				{
					// use system time
					int nNowMS = ((timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))%86400)*1000 + timestamp.millitm;
					m_timeconv.MStoBCDHMSF(nNowMS, &ulTimeCode, (g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate));
				}

				char chDigit = (char)((ulTimeCode&0xff000000)>>24);
        *(buffer) = chDigit%16 + chDigit/16;
				chDigit = (char)((ulTimeCode&0x00ff0000)>>16);
        *(buffer+1) = chDigit%16 + chDigit/16; 
				chDigit = (char)((ulTimeCode&0x0000ff00)>>8);
        *(buffer+2) = chDigit%16 + chDigit/16;
				chDigit = (char)((ulTimeCode&0x000000ff));
        *(buffer+3) = chDigit%16 + chDigit/16;

				memcpy(pscte104MsgEventNotification->m_scte104Hdr.TimeStamp.time_ptr, buffer, 4);

			}

*/
			memset(&pscte104MsgEventNotification->m_ppscte104Ops[0]->proprietary_data_ptr[23], 0x20, 48); // spaces
		}
		return pscte104MsgEventNotification->UpdateDataLengths();
	}
	return TABULATOR_ERROR;
}

int CAutomationDataData::InitializeReplaceAdObject()
{
	if(m_pscte104MsgReplaceAd)
	{
		InitializeEventNotificationObject(m_pscte104MsgReplaceAd);

		m_pscte104MsgReplaceAd->m_ppscte104Ops[1]->OpId1High = 	0x81; //		opID [high order byte]						0x8150 user defined operation
		m_pscte104MsgReplaceAd->m_ppscte104Ops[1]->OpId1Low = 0x50;		//		opID [low order byte]
		m_pscte104MsgReplaceAd->m_ppscte104Ops[1]->OpDataLengthHigh = 0x00;			// xx		data_length [high order byte]			
		m_pscte104MsgReplaceAd->m_ppscte104Ops[1]->OpDataLengthLow  = 0x39;			// xx		data_length [low order byte]			

		m_pscte104MsgReplaceAd->m_ppscte104Ops[1]->proprietary_data_ptr = new unsigned char[0x39];
		if(m_pscte104MsgReplaceAd->m_ppscte104Ops[1]->proprietary_data_ptr)
		{
			memset(m_pscte104MsgReplaceAd->m_ppscte104Ops[1]->proprietary_data_ptr, 0x00, 0x39); // nulls


#pragma message(alert "fix this hardcoded token")

			m_pscte104MsgReplaceAd->m_ppscte104Ops[1]->proprietary_data_ptr[0] = 'V';
			m_pscte104MsgReplaceAd->m_ppscte104Ops[1]->proprietary_data_ptr[1] = 'D';
			m_pscte104MsgReplaceAd->m_ppscte104Ops[1]->proprietary_data_ptr[2] = 'S';
			m_pscte104MsgReplaceAd->m_ppscte104Ops[1]->proprietary_data_ptr[3] = ' ';
			m_pscte104MsgReplaceAd->m_ppscte104Ops[1]->proprietary_data_ptr[4] = 0x02;  // replace ad command

			memset(&m_pscte104MsgReplaceAd->m_ppscte104Ops[1]->proprietary_data_ptr[9], 0x20, 48); // spaces
		}

		return m_pscte104MsgReplaceAd->UpdateDataLengths();
	}
	return TABULATOR_ERROR;
}

int CAutomationDataData::InitializeReplaceContentObject()
{
	if(m_pscte104MsgReplaceContent)
	{
		InitializeEventNotificationObject(m_pscte104MsgReplaceContent);

		m_pscte104MsgReplaceContent->m_ppscte104Ops[1]->OpId1High = 	0x81; //		opID [high order byte]						0x8150 user defined operation
		m_pscte104MsgReplaceContent->m_ppscte104Ops[1]->OpId1Low = 0x50;		//		opID [low order byte]
		m_pscte104MsgReplaceContent->m_ppscte104Ops[1]->OpDataLengthHigh = 0x00;			// xx		data_length [high order byte]			
		m_pscte104MsgReplaceContent->m_ppscte104Ops[1]->OpDataLengthLow  = 0x1d;			// xx		data_length [low order byte]			

		m_pscte104MsgReplaceContent->m_ppscte104Ops[1]->proprietary_data_ptr = new unsigned char[0x1d];
		if(m_pscte104MsgReplaceContent->m_ppscte104Ops[1]->proprietary_data_ptr)
		{
			memset(m_pscte104MsgReplaceContent->m_ppscte104Ops[1]->proprietary_data_ptr, 0x00, 0x1d); // nulls

#pragma message(alert "fix this hardcoded token")

			m_pscte104MsgReplaceContent->m_ppscte104Ops[1]->proprietary_data_ptr[0] = 'V';
			m_pscte104MsgReplaceContent->m_ppscte104Ops[1]->proprietary_data_ptr[1] = 'D';
			m_pscte104MsgReplaceContent->m_ppscte104Ops[1]->proprietary_data_ptr[2] = 'S';
			m_pscte104MsgReplaceContent->m_ppscte104Ops[1]->proprietary_data_ptr[3] = ' ';
			m_pscte104MsgReplaceContent->m_ppscte104Ops[1]->proprietary_data_ptr[4] = 0x03;  // replace content

			memset(&m_pscte104MsgReplaceContent->m_ppscte104Ops[1]->proprietary_data_ptr[13], 0x20, 16); // spaces
		}
		return m_pscte104MsgReplaceContent->UpdateDataLengths();
	}
	return TABULATOR_ERROR;
}

int CAutomationDataData::InitializeRejoinMainObject()
{
	if(m_pscte104MsgRejoinMain)
	{
		InitializeEventNotificationObject(m_pscte104MsgRejoinMain);
		m_pscte104MsgRejoinMain->m_ppscte104Ops[1]->OpId1High = 	0x81; //		opID [high order byte]						0x8150 user defined operation
		m_pscte104MsgRejoinMain->m_ppscte104Ops[1]->OpId1Low = 0x50;		//		opID [low order byte]
		m_pscte104MsgRejoinMain->m_ppscte104Ops[1]->OpDataLengthHigh = 0x00;			// xx		data_length [high order byte]			
		m_pscte104MsgRejoinMain->m_ppscte104Ops[1]->OpDataLengthLow  = 0x05;			// xx		data_length [low order byte]			

		m_pscte104MsgRejoinMain->m_ppscte104Ops[1]->proprietary_data_ptr = new unsigned char[0x05];
		if(m_pscte104MsgRejoinMain->m_ppscte104Ops[1]->proprietary_data_ptr)
		{
			memset(m_pscte104MsgRejoinMain->m_ppscte104Ops[1]->proprietary_data_ptr, 0x00, 0x05); // nulls

#pragma message(alert "fix this hardcoded token")

			m_pscte104MsgRejoinMain->m_ppscte104Ops[1]->proprietary_data_ptr[0] = 'V';
			m_pscte104MsgRejoinMain->m_ppscte104Ops[1]->proprietary_data_ptr[1] = 'D';
			m_pscte104MsgRejoinMain->m_ppscte104Ops[1]->proprietary_data_ptr[2] = 'S';
			m_pscte104MsgRejoinMain->m_ppscte104Ops[1]->proprietary_data_ptr[3] = ' ';
			m_pscte104MsgRejoinMain->m_ppscte104Ops[1]->proprietary_data_ptr[4] = 0x04;  // rejoin main
		}
		return m_pscte104MsgRejoinMain->UpdateDataLengths();
	}
	return TABULATOR_ERROR;
}

int CAutomationDataData::InitializeCurrentTime()
{
	if(m_pscte104MsgCurrentTime)
	{
		m_pscte104MsgCurrentTime->m_ppscte104Ops[0]->OpId1High = 	0x81; //		opID [high order byte]						0x8150 user defined operation
		m_pscte104MsgCurrentTime->m_ppscte104Ops[0]->OpId1Low = 0x50;		//		opID [low order byte]
		m_pscte104MsgCurrentTime->m_ppscte104Ops[0]->OpDataLengthHigh = 0x00;			// xx		data_length [high order byte]			
		m_pscte104MsgCurrentTime->m_ppscte104Ops[0]->OpDataLengthLow  = 0x05;			// xx		data_length [low order byte]			

		m_pscte104MsgCurrentTime->m_ppscte104Ops[0]->proprietary_data_ptr = new unsigned char[0x05];
		if(m_pscte104MsgCurrentTime->m_ppscte104Ops[0]->proprietary_data_ptr)
		{

#pragma message(alert "fix this hardcoded token")

			m_pscte104MsgCurrentTime->m_ppscte104Ops[0]->proprietary_data_ptr[0] = 'V';
			m_pscte104MsgCurrentTime->m_ppscte104Ops[0]->proprietary_data_ptr[1] = 'D';
			m_pscte104MsgCurrentTime->m_ppscte104Ops[0]->proprietary_data_ptr[2] = 'S';
			m_pscte104MsgCurrentTime->m_ppscte104Ops[0]->proprietary_data_ptr[3] = ' ';
			m_pscte104MsgCurrentTime->m_ppscte104Ops[0]->proprietary_data_ptr[4] = 0x05; // current time command
		}
		return m_pscte104MsgCurrentTime->UpdateDataLengths();
	}
	return TABULATOR_ERROR;
}


int CAutomationDataData::ClearOutputVariables(bool bExcludeLongForm)
{
	m_Output.m_b_list_is_playing = false;  // automation list is playing a primary event.

	//NOTE!  three different meaings of the word "current" - be aware!

	if(!bExcludeLongForm)
	{
		// here, current means in-progess, OR most recently past.
		if(m_Output.m_psz_current_long_form_ID) free(m_Output.m_psz_current_long_form_ID);
		m_Output.m_psz_current_long_form_ID=NULL; // = Harris ID of last primary event classified as long-form.
		if(m_Output.m_psz_current_long_form_description) free(m_Output.m_psz_current_long_form_description);
		m_Output.m_psz_current_long_form_description=NULL;// = Harris Title of last primary event classified as long-form.
		m_Output.m_n_current_long_form_start=AUTODATA_INVALID_TIMEADDRESS; // = Harris on air time of last primary event classified as long-form. in ideal MS
		m_Output.m_n_current_long_form_duration=AUTODATA_INVALID_TIMEADDRESS;//  = Harris duration of last primary event classified as long-form. in ideal MS
	}
	// here, next means after the "current", as defined above
	m_Output.m_n_next_long_form_start=AUTODATA_INVALID_TIMEADDRESS; // = Harris on air time of next primary event classified as long-form, or if no such event exists in the playlist, the starting time address of the Next Long-Form Program is the ending time address of the last primary event in the playlist plus one frame, in ideal ms

	// here current means the in progress one, as we will not have messages for upcoming or past ones.
	m_Output.m_n_current_ad_pod_start=AUTODATA_INVALID_TIMEADDRESS; // =  Harris on air time of last ad pod in ideal ms
	m_Output.m_n_current_ad_pod_duration=AUTODATA_INVALID_TIMEADDRESS;// =  Assembled Harris duration of last ad pod  in ideal ms

	// here, current means the currently playing primary event.
	m_Output.m_n_current_primary_class=AUTODATA_CLASS_TYPE_NONE; 

	m_Output.m_n_current_primary_start=AUTODATA_INVALID_TIMEADDRESS; // in ideal MS; 
	m_Output.m_n_current_primary_duration=AUTODATA_INVALID_TIMEADDRESS;  //in ideal MS 

	if(m_Output.m_psz_current_primary_ID) free(m_Output.m_psz_current_primary_ID);
	m_Output.m_psz_current_primary_ID=NULL; 
	if(m_Output.m_psz_current_primary_description) free(m_Output.m_psz_current_primary_description);
	m_Output.m_psz_current_primary_description=NULL;


	return TABULATOR_SUCCESS;

}

int CAutomationDataData::GetClassification(CAutomationEvent* pEvent)
{
	if(pEvent)
	{

if(g_settings.m_ulDebug&(AUTODATA_DEBUG_CLASSRULES)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetClassification", "Checking event [%s][%s] against %d patterns", 
		pEvent->m_pszID?pEvent->m_pszID:"(null)", 
		pEvent->m_pszTitle?pEvent->m_pszTitle:"(null)", 
		m_nClassificationRules); // Sleep(250); //(Dispatch message)
}

		if((m_ppClassificationRules)&&(m_nClassificationRules>0))
		{
			int i=0;
			while(i<m_nClassificationRules)
			{
				if(m_ppClassificationRules[i])
				{

					if(
						  (m_ppClassificationRules[i]->m_pszPattern)
						&&(strlen(m_ppClassificationRules[i]->m_pszPattern)>0)
						&&(m_ppClassificationRules[i]->m_pszColName)
						&&(strlen(m_ppClassificationRules[i]->m_pszColName)>0)
						)
					{

if(g_settings.m_ulDebug&(AUTODATA_DEBUG_CLASSRULES)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetClassification", "Checking event [%s][%s] against pattern #%d: %s in %s", 
		pEvent->m_pszID?pEvent->m_pszID:"(null)", 
		pEvent->m_pszTitle?pEvent->m_pszTitle:"(null)", 
		i, m_ppClassificationRules[i]->m_pszPattern, m_ppClassificationRules[i]->m_pszColName); // Sleep(250); //(Dispatch message)
}


/*
	[event_id] [varchar](32) // reconcile key
	[event_clip] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[event_title] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
*/
						if(stricmp(m_ppClassificationRules[i]->m_pszColName, "event_clip")==0)
						{
							if((pEvent->m_pszID)&&(strlen(pEvent->m_pszID)>0))
							{
								if(TestPattern(pEvent->m_pszID, m_ppClassificationRules[i]->m_pszPattern) == AUTODATA_PATTERN_MATCH)
								{
									return m_ppClassificationRules[i]->m_nEventType;
								}
							}
						}
						else
						if(stricmp(m_ppClassificationRules[i]->m_pszColName, "event_title")==0)
						{
							if((pEvent->m_pszTitle)&&(strlen(pEvent->m_pszTitle)>0))
							{
								if(TestPattern(pEvent->m_pszTitle, m_ppClassificationRules[i]->m_pszPattern) == AUTODATA_PATTERN_MATCH)
								{
									return m_ppClassificationRules[i]->m_nEventType;
								}
							}
						}
					}
				}
				i++;
			}
		}
	}
	return AUTODATA_CLASS_TYPE_NONE;
}

int CAutomationDataData::ReportAutomationEvents()
{

	if((m_ppAutoEvents)&&(m_nAutoEvents)&&(g_ptabmsgr))
	{
		// match to preserve previously calc'd output vars
		int nLastFound = -1;
		unsigned long ulTCoat;
		unsigned long ulTCdur;
		CTimeConvert tcv;

		unsigned long ulTimeCode;
		if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
		{
			EnterCriticalSection(&g_data.m_critTimeCode);
			ulTimeCode = g_data.m_ulTimeCode;
//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
			LeaveCriticalSection(&g_data.m_critTimeCode);
		}
		else
		{
			// use system time
			_timeb timestamp;
			_ftime(&timestamp);
			int nTC = ((timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))%86400)*1000 + timestamp.millitm;
			ulTimeCode = tcv.MStoBCDHMSF(nTC, &ulTimeCode, g_settings.m_nFrameRate);
		}



		
		g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:ReportAutomationEvents", "************************ %d events, logging %d at TC %08x", m_nAutoEvents, (g_settings.m_nAutomationLoggingLookahead>0)?g_settings.m_nAutomationLoggingLookahead:m_nAutoEvents, ulTimeCode);
		int i=0;
		while (
						(i<m_nAutoEvents)
					&&(
							(g_settings.m_nAutomationLoggingLookahead<=0)
						||(
								(g_settings.m_nAutomationLoggingLookahead>0)
							&&(i<g_settings.m_nAutomationLoggingLookahead)
							)
						)
					)
		{
			if(m_ppAutoEvents[i])
			{
				tcv.MStoBCDHMSF(m_ppAutoEvents[i]->m_ulOnAirTimeMS, &ulTCoat,(double)g_settings.m_nFrameRate); // using ideal
				tcv.MStoBCDHMSF(m_ppAutoEvents[i]->m_ulDurationMS, &ulTCdur,(double)g_settings.m_nFrameRate); // using ideal

				g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:ReportAutomationEvents", "%04d oat:%08x dur:%08x status:%04x type:%04x [%s][%s]", 
					i,
					ulTCoat,ulTCdur, 
					m_ppAutoEvents[i]->m_usStatus,
					m_ppAutoEvents[i]->m_usType,
					m_ppAutoEvents[i]->m_pszID?m_ppAutoEvents[i]->m_pszID:"(null)",
					m_ppAutoEvents[i]->m_pszTitle?m_ppAutoEvents[i]->m_pszTitle:"(null)"
					);//  Sleep(250); //(Dispatch message)

			}
			i++;
		}
		g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:ReportAutomationEvents", "************************");

		return i;
	}
	else
	{
		if(g_ptabmsgr)
			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:ReportAutomationEvents", "There are no automation events.");

		return 0;
	}
}



int CAutomationDataData::GetAutomationEvents()
{
	if((m_pdbConn)&&(m_pdb)&&(g_settings.m_pszAutoServer)&&(strlen(g_settings.m_pszAutoServer)>0)&&(g_settings.m_nAutoServerListNumber>0))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		if(g_settings.m_nAutomationAnalysisLookahead>0)
		{
			if(g_settings.m_bAutomationLookahead)
			{
				sprintf(errorstring, " AND event_position < %d", (min(g_settings.m_nAutomationAnalysisLookahead, g_data.m_AutoList.m_nLookahead))  );
			}
			else
			{
				sprintf(errorstring, " AND event_position < %d", (g_settings.m_nAutomationAnalysisLookahead)  );
			}
		}
		else
		{
			if(g_settings.m_bAutomationLookahead)
			{
				sprintf(errorstring, " AND event_position < %d", (g_data.m_AutoList.m_nLookahead)  );
			}
			else
			{
				sprintf (errorstring, "");
			}
		}


		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT itemid, event_id, event_clip, event_segment, event_title, \
event_type, event_status, event_time_mode, event_start, event_duration \
FROM %s.dbo.%s WHERE server_name = '%s' AND server_list = %d AND (event_type & 128 = 0)%s \
ORDER BY event_position", // if we get ads first, those are easier to actuate, so let's prioritize them
			((g_settings.m_pszAutoModule)&&(strlen(g_settings.m_pszAutoModule)))?g_settings.m_pszAutoModule:"Sentinel",
			((g_settings.m_pszAutoEvents)&&(strlen(g_settings.m_pszAutoEvents)))?g_settings.m_pszAutoEvents:"Events",
			g_settings.m_pszAutoServer, g_settings.m_nAutoServerListNumber,
			errorstring
			); 
		strcpy (errorstring, "");

/*

 	int m_nID;
	unsigned short m_usType;
	char* m_pszID;
	char* m_pszTitle;
	char* m_pszReconcileKey;
	unsigned char  m_ucSegment;
	unsigned long  m_ulOnAirTimeMS;					// ideal milliseconds as appears in the list
	unsigned long  m_ulOnAirLocalUnixDate;  //in seconds, offset from January 1, 1900
	unsigned long  m_ulDurationMS;          // ideal milliseconds as appears in the list
	unsigned long  m_ulSOMMS;               // ideal milliseconds as appears in the list
	unsigned short m_usStatus;
//	unsigned short m_usControl; // was this
	unsigned long m_ulControl;  // made it this, storing revent->extrathree (which is extended event control in recfm80) in the upper word

	// potential output variables.
	unsigned long m_ulClassification;
	char* m_pszOutputString; // like:


USE [sentinel]
GO
/ ****** Object:  Table [dbo].[Events]    Script Date: 12/19/2011 00:39:06 ****** /
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events](
	[itemid] [int] NOT NULL,
	[event_key] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[server_name] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[server_list] [int] NULL,
	[list_id] [int] NULL,
	[event_id] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[event_clip] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[event_segment] [int] NULL,
	[event_title] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[event_data] [varchar](4096) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[event_type] [int] NULL,
	[event_status] [int] NULL,
	[event_time_mode] [int] NULL,
	[event_start] [decimal](20, 3) NULL,
	[event_duration] [int] NULL,
	[event_calc_start] [decimal](20, 3) NULL,
	[event_calc_duration] [int] NULL,
	[event_calc_end] [decimal](20, 3) NULL,
	[event_position] [int] NULL,
	[event_last_update] [decimal](20, 3) NULL,
	[parent_uid] [int] NULL,
	[parent_key] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[parent_id] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[parent_clip] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[parent_segment] [int] NULL,
	[parent_title] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[parent_type] [int] NULL,
	[parent_status] [int] NULL,
	[parent_time_mode] [int] NULL,
	[parent_start] [decimal](20, 3) NULL,
	[parent_duration] [int] NULL,
	[parent_calc_start] [decimal](20, 3) NULL,
	[parent_calc_duration] [int] NULL,
	[parent_calc_end] [decimal](20, 3) NULL,
	[parent_position] [int] NULL,
	[app_data] [varchar](512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[app_data_aux] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF

*/
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_AUTOEVENTS)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetAutomationEvents", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "GetAutomationEvents");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.

			// first let's preserve the existing list.
			CAutomationEvent** ppTemp  = NULL;
			int nNumEvents = m_nAutoEvents;
			if(m_nAutoEvents>0)
			{
				ppTemp = new CAutomationEvent*[m_nAutoEvents];
				if(ppTemp)
				{
					memcpy(ppTemp, m_ppAutoEvents, sizeof(CAutomationEvent*)*m_nAutoEvents);
					memset(m_ppAutoEvents, 0, sizeof(CAutomationEvent*)*m_nAutoEventsArraySize);  // do the whole array
					m_nAutoEvents = 0;
				}
				else
				{
					nNumEvents=0;
				}
			}

			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nAutoEventsArraySize) // have to expand
				{
					CAutomationEvent** ppNew = new CAutomationEvent*[m_nAutoEventsArraySize + AUTODATA_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nAutoEventsArraySize += AUTODATA_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(CAutomationEvent*)*m_nAutoEventsArraySize);
						if(m_ppAutoEvents)
						{
							memcpy(ppNew, m_ppAutoEvents, sizeof(CAutomationEvent*)*nIndex); // it's an array of pointers
						}
						m_ppAutoEvents = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppAutoEvents==NULL)
				{
					m_nAutoEventsArraySize = 0;
					m_nAutoEvents=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nAutoEvents)
				{
					m_ppAutoEvents[nIndex] = new CAutomationEvent;  // guaranteed that there will be array space, above.
				}

				if(m_ppAutoEvents[nIndex])
				{
					CString szTemp;
// SELECT itemid, event_id, event_clip, event_segment, event_title,
// event_type, event_status, event_time_mode,	event_start, event_duration
					
					prs->GetFieldValue("itemid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("event_id", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						if(m_ppAutoEvents[nIndex]->m_pszReconcileKey != NULL)
						{
							free(m_ppAutoEvents[nIndex]->m_pszReconcileKey);
						}
						m_ppAutoEvents[nIndex]->m_pszReconcileKey = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppAutoEvents[nIndex]->m_pszReconcileKey)
						{
							strcpy(m_ppAutoEvents[nIndex]->m_pszReconcileKey, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("event_clip", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						if(m_ppAutoEvents[nIndex]->m_pszID != NULL)
						{
							free(m_ppAutoEvents[nIndex]->m_pszID);
						}
						m_ppAutoEvents[nIndex]->m_pszID = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppAutoEvents[nIndex]->m_pszID)
						{
							strcpy(m_ppAutoEvents[nIndex]->m_pszID, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("event_segment", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_ucSegment = (unsigned char)atol(szTemp);
					}
					prs->GetFieldValue("event_title", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						if(m_ppAutoEvents[nIndex]->m_pszTitle != NULL)
						{
							free(m_ppAutoEvents[nIndex]->m_pszTitle);
						}
						m_ppAutoEvents[nIndex]->m_pszTitle = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppAutoEvents[nIndex]->m_pszTitle)
						{
							strcpy(m_ppAutoEvents[nIndex]->m_pszTitle, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("event_type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_usType = (unsigned short)atol(szTemp);
					}
					prs->GetFieldValue("event_status", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_usStatus = (unsigned short)atol(szTemp);
					}
					prs->GetFieldValue("event_time_mode", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_ulControl = (unsigned short)atol(szTemp);
					}
					double dblOnAir;
					prs->GetFieldValue("event_start", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						dblOnAir = atof(szTemp);

if(g_settings.m_ulDebug&(AUTODATA_DEBUG_AUTOEVENTS)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetAutomationEvents", "obtained %.3f", dblOnAir);//  Sleep(250); //(Dispatch message)
}

						m_ppAutoEvents[nIndex]->m_ulOnAirLocalUnixDate = ((unsigned long)(int)(dblOnAir));

						dblOnAir -= (double)(int)(dblOnAir);

if(g_settings.m_ulDebug&(AUTODATA_DEBUG_AUTOEVENTS)) 
{
	// this is unbelievable.  0.166 -> 166.000 when *1000.0, but then when casted to unsigned long, reverts to 165!  shady floating points!
// 2011-Dec-30 07:31:43.769 [H ] calc 0.166 166.000 165 - AutomationData:GetAutomationEvents - 
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetAutomationEvents", "calc %.3f %.3f %d %d", dblOnAir, (dblOnAir)*1000.0, (unsigned long)((dblOnAir)*1000.0), m_ppAutoEvents[nIndex]->m_ulOnAirLocalUnixDate);//  Sleep(250); //(Dispatch message)/
}


//						m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS = (unsigned long)((dblOnAir)*1000.0 +.999999999) + (m_ppAutoEvents[nIndex]->m_ulOnAirLocalUnixDate%86400)*1000; // ideal not real
						m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS = (unsigned long)(((dblOnAir)*1000.0) +.1) + (m_ppAutoEvents[nIndex]->m_ulOnAirLocalUnixDate%86400)*1000; // ideal not real

						// OK had to add the .0001 in first just to tip the balance.  then we have nice correct integer values.
						// then instead of using the 0.99999999 as explained, just going to do the same wasy as I did duration

						if(m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS%10) m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS++;  // these are ideal values.  so will either end in 0, 3, or 6.  if not 0, increment up by one ms.

						
						// the 0.99999999 comes from the fact that Sentinel stores the ideal ms in increments of 33 ms, like:
						// .000, .033, .066, .100, .133 etc.  which means that they are rounded.
						// So something that has a frame of 01 would get 033 milliseconds, not 033.333333 ms
						// this truncs to 33 and so we get frame number 00.
						// by adding .99999 we up this to 34.66666666 which truncs to 34 and we are all set.
						// similar for 66.6666 -> 67.  BUT, 100ms -> 100.999999999 ms -> 100.  so we are good with round values.

						if((m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS == 0)&&(!(m_ppAutoEvents[nIndex]->m_ulControl&0x10))) // this could be an error, attempt to correct it.
						{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetAutomationEvents", "Detected time address of 00:00:00.00" );//  Sleep(250); //(Dispatch message)/

							if(nIndex>0)
							{
								if(m_ppAutoEvents[nIndex-1])
								{
									if((m_ppAutoEvents[nIndex-1]->m_ulOnAirTimeMS  + m_ppAutoEvents[nIndex-1]->m_ulDurationMS)%86400000)
									{
										m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS  =  m_ppAutoEvents[nIndex-1]->m_ulOnAirTimeMS  + m_ppAutoEvents[nIndex-1]->m_ulDurationMS; 
										m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS %= 86400000; // in case rolled over
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetAutomationEvents", "Re-assigned start time of event from 0 to %d", m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS );//  Sleep(250); //(Dispatch message)/
									}
								}
							}
						}

					}
					prs->GetFieldValue("event_duration", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_ulDurationMS = atol(szTemp); // neg durations ever? no, guess not.
						if(m_ppAutoEvents[nIndex]->m_ulDurationMS%10) m_ppAutoEvents[nIndex]->m_ulDurationMS++;  // these are ideal values.  so will either end in 0, 3, or 6.  if not 0, increment up by one ms.
					}
				
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_AUTOEVENTS)) 
{
	unsigned long ulTCoat;
	unsigned long ulTCdur;
	CTimeConvert tcv;
	tcv.MStoBCDHMSF(m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS, &ulTCoat,(double)g_settings.m_nFrameRate); // using ideal
	tcv.MStoBCDHMSF(m_ppAutoEvents[nIndex]->m_ulDurationMS, &ulTCdur,(double)g_settings.m_nFrameRate); // using ideal

	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetAutomationEvents", "%02d[%s][%s] oat %08x %d dur %08x %d", nIndex,
		m_ppAutoEvents[nIndex]->m_pszID?m_ppAutoEvents[nIndex]->m_pszID:"(null)",
		m_ppAutoEvents[nIndex]->m_pszTitle?m_ppAutoEvents[nIndex]->m_pszTitle:"(null)",
		ulTCoat, m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS,
		ulTCdur, m_ppAutoEvents[nIndex]->m_ulDurationMS
		);//  Sleep(250); //(Dispatch message)
}
				}
				nIndex++;
				prs->MoveNext();
			}


			if(nIndex<m_nAutoEvents)
			{
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_AUTOEVENTS)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetAutomationEvents", "Deleting overflow %d>%d", m_nAutoEvents, nIndex);//  Sleep(250); //(Dispatch message)
}
				int i=nIndex;
				while(i<m_nAutoEvents)
				{
					if(m_ppAutoEvents[i]) delete m_ppAutoEvents[i];
					m_ppAutoEvents[i] = NULL;
					i++;
				}
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_AUTOEVENTS)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetAutomationEvents", "Deleted overflow, events now %d", nIndex); // Sleep(250); //(Dispatch message)
}
			}

			m_nAutoEvents = nIndex;

			if((m_ppAutoEvents)&&(m_nAutoEvents)&&(ppTemp != NULL)&&(nNumEvents>0))
			{
				// match to preserve previously calc'd output vars
				int nLastFound = -1;
				
				int i=0;
				while(i<m_nAutoEvents)
				{
					if(m_ppAutoEvents[i])
					{
						int nCount=0;
						int j = nLastFound+1;
						int nEndIndex = nNumEvents-1;

						while(nCount < nNumEvents) // max search them all
						{
							if(j>nEndIndex) j=0;
							if(ppTemp[j])
							{
								if(ppTemp[j]->m_nID == m_ppAutoEvents[i]->m_nID)
								{
									// match!
									m_ppAutoEvents[i]->m_pszOutputString  = ppTemp[j]->m_pszOutputString;
									ppTemp[j]->m_pszOutputString = NULL; //prevents freeing

									m_ppAutoEvents[i]->m_ulClassification = ppTemp[j]->m_ulClassification;

									nLastFound = j;

									break;
								}
							}
							j++;							
							nCount++; 
						}
						if(nCount >= nNumEvents) // didnt find.
							nLastFound = -1;
					}
					i++;
				}
				
		//		nNumEvents = m_nAutoEvents;  // this is just wrong.
			}

			if((ppTemp != NULL)&&(nNumEvents>0))
			{
				int i=0;
				while(i<nNumEvents)
				{
					if(ppTemp[i]) delete ppTemp[i];
					i++;
				}
				delete [] ppTemp;
			}

			m_bInitalEventGetSucceeded = true;

			prs->Close();
			delete prs;

			return nIndex;
		}
	}
	return TABULATOR_ERROR;
}

int CAutomationDataData::GetAutomationListStatus()
{
	if((m_pdbConn)&&(m_pdb)&&(g_settings.m_pszAutoServer)&&(strlen(g_settings.m_pszAutoServer)>0)&&(g_settings.m_nAutoServerListNumber>0))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update \
FROM %s.dbo.%s WHERE server = '%s' AND listid = %d AND (flags & 1 = 1)",  // has to be active
			((g_settings.m_pszAutoModule)&&(strlen(g_settings.m_pszAutoModule)))?g_settings.m_pszAutoModule:"Sentinel",
			((g_settings.m_pszAutoLists)&&(strlen(g_settings.m_pszAutoLists)))?g_settings.m_pszAutoLists:"Channels",
			g_settings.m_pszAutoServer, g_settings.m_nAutoServerListNumber

			); 

		/*
class CAutomationList  
{
public:
	CAutomationList();
	virtual ~CAutomationList();

	int m_nState;
	int m_nChange;
	int m_nDisplay;
	int m_nSysChange;
	int m_nCount; 
	int m_nLookahead;
	double m_dblLastUpdate;
};

USE [sentinel]
GO
/ ****** Object:  Table [dbo].[Channels]    Script Date: 12/19/2011 00:55:03 ****** /
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Channels](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[flags] [int] NOT NULL,
	[description] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[server] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[listid] [int] NULL,
	[list_state] [int] NULL,
	[list_changed] [int] NULL,
	[list_display] [int] NULL,
	[list_syschange] [int] NULL,
	[list_count] [int] NULL,
	[list_lookahead] [int] NULL,
	[list_last_update] [decimal](20, 3) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF

*/


if(g_settings.m_ulDebug&(AUTODATA_DEBUG_AUTOLIST)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetAutomationListStatus", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "GetAutomationListStatus");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		int nIndex = 0;
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			if (!prs->IsEOF())
			{
				nIndex++;
				CString szTemp;

// SELECT list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update
				
				prs->GetFieldValue("list_state", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nState = atoi(szTemp);
				}
				prs->GetFieldValue("list_changed", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nChange = atoi(szTemp);
				}
				prs->GetFieldValue("list_display", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nDisplay = atoi(szTemp);
				}
				prs->GetFieldValue("list_syschange", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nSysChange = atoi(szTemp);
				}
				prs->GetFieldValue("list_count", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nCount = atoi(szTemp);
				}
				prs->GetFieldValue("list_lookahead", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nLookahead = atoi(szTemp);
				}
				prs->GetFieldValue("list_last_update", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_dblLastUpdate = atof(szTemp);
				}


				if((m_AutoList.m_nState<0)||(m_AutoList.m_nChange<0))	m_bInitalEventGetSucceeded = true; // nothing was connected or active so OK, call that initialized.


				prs->Close();
				delete prs;
				return TABULATOR_SUCCESS;
			
			}
			else
			{
				m_bInitalEventGetSucceeded = true; // nothing was active so OK, call that initialized.
			}

			prs->Close();
			delete prs;
		}

		if(nIndex==0)
		{
			// no records, either no automation or whatever...
			m_AutoList.m_nState=-1;
			m_AutoList.m_nChange=-1;
			m_AutoList.m_nDisplay=-1;
			m_AutoList.m_nSysChange=-1;
			m_AutoList.m_nCount=-1;
			m_AutoList.m_nLookahead=-1;
			m_AutoList.m_dblLastUpdate = -1.0;
		}
	}
	return TABULATOR_ERROR;
}



int CAutomationDataData::GetReplacementRules()
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		// unfortunately, I cannot just get the ones within the valid dates, since time progresses, and I only get this table when it changes.
		// I have to weed out the ones as I go....
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, programID, sequence, replacementID, duration, valid_date, expire_date FROM %s ORDER BY sequence", 
			((g_settings.m_pszReplacementRules)&&(strlen(g_settings.m_pszReplacementRules)))?g_settings.m_pszReplacementRules:"ReplacementRules"
			); 

//create table ReplacementRules (ID int identity(1,1), programID varchar(16), sequence int, replacementID varchar(16), duration int, valid_date decimal(20,3), expire_date decimal(20,3))  
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetReplacementRules", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "GetReplacementRules");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nReplacementRulesArraySize) // have to expand
				{
					CReplacementRule** ppNew = new CReplacementRule*[m_nReplacementRulesArraySize + AUTODATA_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nReplacementRulesArraySize += AUTODATA_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(CReplacementRule*)*m_nReplacementRulesArraySize);
						if(m_ppReplacementRules)
						{
							memcpy(ppNew, m_ppReplacementRules, sizeof(CReplacementRule*)*nIndex); // it's an array of pointers
						}
						m_ppReplacementRules = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppReplacementRules==NULL)
				{
					m_nReplacementRulesArraySize = 0;
					m_nReplacementRules=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nReplacementRules)
				{
					m_ppReplacementRules[nIndex] = new CReplacementRule;  // guaranteed that there will be array space, above.
				}

				if(m_ppReplacementRules[nIndex])
				{
// removed all length validation.
//if something is "blank" then let it be - it has to overwrite any old data.

					CString szTemp;
	//SELECT ID, programID, sequence, replacementID, duration, valid_date, expire_date
					prs->GetFieldValue("ID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppReplacementRules[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("programID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						if(m_ppReplacementRules[nIndex]->m_pszProgramID != NULL)
						{
							free(m_ppReplacementRules[nIndex]->m_pszProgramID);
						}
						m_ppReplacementRules[nIndex]->m_pszProgramID = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppReplacementRules[nIndex]->m_pszProgramID)
						{
							strcpy(m_ppReplacementRules[nIndex]->m_pszProgramID, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("sequence", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppReplacementRules[nIndex]->m_nSequence = atoi(szTemp);
					}
					prs->GetFieldValue("replacementID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						if(m_ppReplacementRules[nIndex]->m_pszReplacementID != NULL)
						{
							free(m_ppReplacementRules[nIndex]->m_pszReplacementID);
						}
						m_ppReplacementRules[nIndex]->m_pszReplacementID = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppReplacementRules[nIndex]->m_pszReplacementID)
						{
							strcpy(m_ppReplacementRules[nIndex]->m_pszReplacementID, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("duration", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppReplacementRules[nIndex]->m_nDuration = atoi(szTemp);

						// following line is so that we do not lose a frame due to rounding down 33.3333 to 33 and 66.6666 to 66.
						if(m_ppReplacementRules[nIndex]->m_nDuration%10) m_ppReplacementRules[nIndex]->m_nDuration++;


						if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
						{
//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp

							if(g_data.m_ucTimeCodeBits&0x80) // drop frame...
							{
								m_ppReplacementRules[nIndex]->m_nDurationInFrames = g_data.m_timeconv.MStoF(g_data.m_timeconv.NDFMStoDFMS( m_ppReplacementRules[nIndex]->m_nDuration ), 29.97) ;
							}
							else
							{
								m_ppReplacementRules[nIndex]->m_nDurationInFrames = g_data.m_timeconv.MStoF( m_ppReplacementRules[nIndex]->m_nDuration , ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) ;
							}
						}
						else  // system time
						{
							if(g_settings.m_bDF) // drop frame...
							{
								m_ppReplacementRules[nIndex]->m_nDurationInFrames = g_data.m_timeconv.MStoF(g_data.m_timeconv.NDFMStoDFMS( m_ppReplacementRules[nIndex]->m_nDuration ), 29.97) ;
							}
							else
							{
								m_ppReplacementRules[nIndex]->m_nDurationInFrames = g_data.m_timeconv.MStoF( m_ppReplacementRules[nIndex]->m_nDuration , g_settings.m_nFrameRate) ;
							}
						}



					}
					prs->GetFieldValue("valid_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppReplacementRules[nIndex]->m_dblValidDate = atof(szTemp);
					}
					prs->GetFieldValue("expire_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppReplacementRules[nIndex]->m_dblExpireDate = atof(szTemp);
					}
				}


if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "AutomationData:debug", "RR%03d ID %s, seq %d, ReplID %s, Dur %d, %.3f-%.3f", 
	nIndex, 
	m_ppReplacementRules[nIndex]->m_pszProgramID,
	m_ppReplacementRules[nIndex]->m_nSequence,
	m_ppReplacementRules[nIndex]->m_pszReplacementID,
	m_ppReplacementRules[nIndex]->m_nDuration, // in ideal MS
	m_ppReplacementRules[nIndex]->m_dblValidDate,
	m_ppReplacementRules[nIndex]->m_dblExpireDate
	
	);

				nIndex++;
				prs->MoveNext();
			}

			if(nIndex<m_nReplacementRules)
			{
				int i=nIndex;
				while(i<m_nReplacementRules)
				{
					if(m_ppReplacementRules[i]) delete m_ppReplacementRules[i];
					m_ppReplacementRules[i] = NULL;
					i++;
				}
			}
			m_nReplacementRules = nIndex;

			prs->Close();
			delete prs;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "AutomationData:debug", "RR: %d", m_nReplacementRules	);



			return nIndex;
		}
	}
	return TABULATOR_ERROR;
}

int CAutomationDataData::GetDescriptionPatterns()
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		// unfortunately, I cannot just get the ones within the valid dates, since time progresses, and I only get this table when it changes.
		// I have to weed out the ones as I go....
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, programID, pattern, valid_date, expire_date FROM %s ORDER BY ID", 
			((g_settings.m_pszDescriptionPattern)&&(strlen(g_settings.m_pszDescriptionPattern)))?g_settings.m_pszDescriptionPattern:"DescriptionPattern"
			); 

//create table DescriptionPattern (ID int identity(1,1), programID varchar(16), pattern varchar(128), valid_date decimal(20,3), expire_date decimal(20,3))  

if(g_settings.m_ulDebug&(AUTODATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetDescriptionPatterns", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "GetDescriptionPatterns");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nDescriptionPatternsArraySize) // have to expand
				{
					CDescriptionPattern** ppNew = new CDescriptionPattern*[m_nDescriptionPatternsArraySize + AUTODATA_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nDescriptionPatternsArraySize += AUTODATA_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(CDescriptionPattern*)*m_nDescriptionPatternsArraySize);
						if(m_ppDescriptionPatterns)
						{
							memcpy(ppNew, m_ppDescriptionPatterns, sizeof(CDescriptionPattern*)*nIndex); // it's an array of pointers
						}
						m_ppDescriptionPatterns = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppDescriptionPatterns==NULL)
				{
					m_nDescriptionPatternsArraySize = 0;
					m_nDescriptionPatterns=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nDescriptionPatterns)
				{
					m_ppDescriptionPatterns[nIndex] = new CDescriptionPattern;  // guaranteed that there will be array space, above.
				}

				if(m_ppDescriptionPatterns[nIndex])
				{
					CString szTemp;
// removed all length validation. EXCEPT for pattern - that you need
//if something is "blank" then let it be - it has to overwrite any old data.
	//SELECT ID, programID, pattern, valid_date, expire_date
					prs->GetFieldValue("ID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppDescriptionPatterns[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("programID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						if(m_ppDescriptionPatterns[nIndex]->m_pszProgramID != NULL)
						{
							free(m_ppDescriptionPatterns[nIndex]->m_pszProgramID);
						}
						m_ppDescriptionPatterns[nIndex]->m_pszProgramID = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppDescriptionPatterns[nIndex]->m_pszProgramID)
						{
							strcpy(m_ppDescriptionPatterns[nIndex]->m_pszProgramID, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("pattern", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						if(m_ppDescriptionPatterns[nIndex]->m_pszPattern != NULL)
						{
							free(m_ppDescriptionPatterns[nIndex]->m_pszPattern);
						}
						m_ppDescriptionPatterns[nIndex]->m_pszPattern = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppDescriptionPatterns[nIndex]->m_pszPattern)
						{
							strcpy(m_ppDescriptionPatterns[nIndex]->m_pszPattern, szTemp.GetBuffer(0));
						}
					}
					else
					{
						if(m_ppDescriptionPatterns[nIndex]->m_pszPattern != NULL)
						{
							free(m_ppDescriptionPatterns[nIndex]->m_pszPattern);
						}
						m_ppDescriptionPatterns[nIndex]->m_pszPattern = NULL;
					}
					prs->GetFieldValue("valid_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppDescriptionPatterns[nIndex]->m_dblValidDate = atof(szTemp);
					}
					prs->GetFieldValue("expire_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppDescriptionPatterns[nIndex]->m_dblExpireDate = atof(szTemp);
					}
				}

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_DESCPATTERNS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "AutomationData:debug", "DP%03d ID %s, Pattern %s, %.3f-%.3f", 
	nIndex, 
	m_ppDescriptionPatterns[nIndex]->m_pszProgramID,
	m_ppDescriptionPatterns[nIndex]->m_pszPattern,
	m_ppDescriptionPatterns[nIndex]->m_dblValidDate,
	m_ppDescriptionPatterns[nIndex]->m_dblExpireDate
	
	);


				nIndex++;
				prs->MoveNext();
			}

			if(nIndex<m_nDescriptionPatterns)
			{
				int i=nIndex;
				while(i<m_nDescriptionPatterns)
				{
					if(m_ppDescriptionPatterns[i]) delete m_ppDescriptionPatterns[i];
					m_ppDescriptionPatterns[i] = NULL;
					i++;
				}
			}

			m_nDescriptionPatterns = nIndex;

			prs->Close();
			delete prs;


if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_DESCPATTERNS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "AutomationData:debug", "DP: %d", m_nDescriptionPatterns	);



			return nIndex;
		}
	}
	return TABULATOR_ERROR;
}


int CAutomationDataData::GetClassificationRules()
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, pattern, col_name, event_type FROM %s ORDER BY event_type", // if we get ads first, those are easier to actuate, so let's prioritize them
			((g_settings.m_pszClassificationRules)&&(strlen(g_settings.m_pszClassificationRules)))?g_settings.m_pszClassificationRules:"ClassificationRules"
			); 

//create table ClassificationRules (ID int identity(1,1), pattern varchar(128), col_name varchar(64), event_type int)  

if(g_settings.m_ulDebug&(AUTODATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetClassificationRules", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "GetClassificationRules");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nClassificationRulesArraySize) // have to expand
				{
					CClassificationRule** ppNew = new CClassificationRule*[m_nClassificationRulesArraySize + AUTODATA_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nClassificationRulesArraySize += AUTODATA_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(CClassificationRule*)*m_nClassificationRulesArraySize);
						if(m_ppClassificationRules)
						{
							memcpy(ppNew, m_ppClassificationRules, sizeof(CClassificationRule*)*nIndex); // it's an array of pointers
						}
						m_ppClassificationRules = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppClassificationRules==NULL)
				{
					m_nClassificationRulesArraySize = 0;
					m_nClassificationRules=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nClassificationRules)
				{
					m_ppClassificationRules[nIndex] = new CClassificationRule;  // guaranteed that there will be array space, above.
				}

				if(m_ppClassificationRules[nIndex])
				{
					CString szTemp;
// removed all length validation. EXCEPT for pattern - that you need
//if something is "blank" then let it be - it has to overwrite any old data.
	//SELECT ID, pattern, col_name, event_type 
					prs->GetFieldValue("ID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppClassificationRules[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("pattern", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						if(m_ppClassificationRules[nIndex]->m_pszPattern != NULL)
						{
							free(m_ppClassificationRules[nIndex]->m_pszPattern);
						}
						m_ppClassificationRules[nIndex]->m_pszPattern = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppClassificationRules[nIndex]->m_pszPattern)
						{
							strcpy(m_ppClassificationRules[nIndex]->m_pszPattern, szTemp.GetBuffer(0));
						}
					}
					else
					{
						if(m_ppClassificationRules[nIndex]->m_pszPattern != NULL)
						{
							free(m_ppClassificationRules[nIndex]->m_pszPattern);
						}
						m_ppClassificationRules[nIndex]->m_pszPattern = NULL;
					}

					prs->GetFieldValue("col_name", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						if(m_ppClassificationRules[nIndex]->m_pszColName != NULL)
						{
							free(m_ppClassificationRules[nIndex]->m_pszColName);
						}
						m_ppClassificationRules[nIndex]->m_pszColName = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppClassificationRules[nIndex]->m_pszColName)
						{
							strcpy(m_ppClassificationRules[nIndex]->m_pszColName, szTemp.GetBuffer(0));
						}
					}
					else
					{
						if(m_ppClassificationRules[nIndex]->m_pszColName != NULL)
						{
							free(m_ppClassificationRules[nIndex]->m_pszColName);
						}
						m_ppClassificationRules[nIndex]->m_pszColName = NULL;
					}
					prs->GetFieldValue("event_type", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppClassificationRules[nIndex]->m_nEventType = atoi(szTemp);
					}
				}

if(g_settings.m_ulDebug&(AUTODATA_DEBUG_CLASSRULES)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetClassificationRules", "CR%d [%s][%s] %d", 
		nIndex,
		m_ppClassificationRules[nIndex]->m_pszColName?m_ppClassificationRules[nIndex]->m_pszColName:"(null)",
		m_ppClassificationRules[nIndex]->m_pszPattern?m_ppClassificationRules[nIndex]->m_pszPattern:"(null)",
		m_ppClassificationRules[nIndex]->m_nEventType
		); // Sleep(250); //(Dispatch message)
}


				nIndex++;
				prs->MoveNext();
			}


			if(nIndex<m_nClassificationRules)
			{
				int i=nIndex;
				while(i<m_nClassificationRules)
				{
					if(m_ppClassificationRules[i]) delete m_ppClassificationRules[i];
					m_ppClassificationRules[i] = NULL;
					i++;
				}
			}

			m_nClassificationRules = nIndex;

			prs->Close();
			delete prs;

			return nIndex;
		}
	}
	return TABULATOR_ERROR;
}


int CAutomationDataData::CountCurrentReplacementRules(double dblTime)// use system time
{
	if(m_ppReplacementRules)
	{
		int nCount=0;
		int i=0;
		while(i<m_nReplacementRules)
		{
			if(m_ppReplacementRules[i])
			{

				if(
					  (dblTime < m_ppReplacementRules[i]->m_dblExpireDate)  // not <= because = it's expired now
					&&(dblTime >= m_ppReplacementRules[i]->m_dblValidDate)  //  is >= because = means it's valid now
					)
				{
					nCount++;
				}
			}

			i++;
		}
		return nCount;
	}
	return 0;
}


int CAutomationDataData::Query_Ready()
{
	if(m_bInitalEventGetSucceeded) return TABULATOR_SUCCESS;
	return TABULATOR_ERROR;
}


int CAutomationDataData::Enter_Pass(bool bCurrent, bool bTimer)
{

/*
PASS Control State
On entering (or re-entering) the PASS Control State, Promotor shall:

1. Stop the Event Masking Timer.
2. Extinguish Button 1 ("AUTO") on the Basic Operator Panel.		// handled by the GTP 32 setup
3. Illuminate Button 2 ("PASS") on the Basic Operator Panel.		// handled by the GTP 32 setup
4. Extinguish Button 3 ("BLOCK") on the Basic Operator Panel.		// handled by the GTP 32 setup
5. Open the Downstream Keyer Control GPO.												// handled by the GTP 32 setup

6. Generate and insert a Rejoin Main Message, thereby returning the TV+ audience to the Program Stream.
*/
												// the PASS tabulator event:
												//  Stop Timer1
												//  Open USP GPO 3 OFF
												//  Rejoin Main message

//all of this is triggered with a single token:

// AutomationData|Enter_Pass

	if(m_pRejoinMainEvents)
	{
		CMessageOverrideValues* pOverrides = new CMessageOverrideValues;
		char errorstring[MAX_MESSAGE_LENGTH];

		if(pOverrides)
		{
/*
			pOverrides->m_ulEN_EventStart;						// The current LTC time address, plus the Program Stream Offset. for manual event 
			pOverrides->m_ulEN_EventDur;							// The time address 00:00:00;00.  for manual event
			pOverrides->m_pszEN_ProgramID[17];						      	// The value "TV+:UNKNOWN".  for manual event
			pOverrides->m_pszEN_ProgramDescription[33];						// Blank.  for manual event

	
	>There are three cases where Stream Messages are generated on entering (or re-entering) a Control State: page 17, lines 24-25 (AUTO); page 18, lines 8-9 (PASS); and page 18, lines 13-15 (BLOCK).
	>In these cases, you may follow the requirements, on page 12, lines 16-21, for Stream Messages generated while processing Manual Events.

On entering the PASS Control State, the Rejoin Main Message is sent unconditionally. The EventStart field of this Rejoin Main Message should contain
 the current LTC time address, plus the Program Stream Offset. 
 The EventDur, ProgramID, and ProgramDescription fields of this Rejoin Main Message may contain either the values given on page 12, lines 19, 20, and 21, respectively, 
 or information from the Selected Automation System, if it is available, whichever you prefer.	
		
// FOR ease, we are going to "prefer" the static values.   NO, can't do.  just use current stuff if available.

	EventStart: The current LTC time address, plus the Program Stream Offset.
	EventDur: The time address 00:00:00;00.
	ProgramID: The value "TV+:UNKNOWN".
	ProgramDescription: Blank.

*/



			EnterCriticalSection(&g_data.m_critOutputVariables);
			if(m_Output.m_b_list_is_playing)
			{
				if((m_Output.m_psz_current_primary_ID)&&(strlen(m_Output.m_psz_current_primary_ID)))
				{
					strncpy(pOverrides->m_pszEN_ProgramID, m_Output.m_psz_current_primary_ID, 16);
				}
				else
				if((g_settings.m_pszUnknownIDToken)&&(strlen(g_settings.m_pszUnknownIDToken)))
				{
					strncpy(pOverrides->m_pszEN_ProgramID, g_settings.m_pszUnknownIDToken, 16);
				}
				else
				{
					strcpy(pOverrides->m_pszEN_ProgramID, "TV+:UNKNOWN");
				}

				if((m_Output.m_psz_current_primary_description)&&(strlen(m_Output.m_psz_current_primary_description)))
				{
					strncpy(pOverrides->m_pszEN_ProgramDescription, m_Output.m_psz_current_primary_description, 32);
				}
				else
				if((g_settings.m_pszBlockedDescToken)&&(strlen(g_settings.m_pszBlockedDescToken)))
				{
					strncpy(pOverrides->m_pszEN_ProgramDescription, g_settings.m_pszBlockedDescToken, 32);
				}
				// else nothing, it initializes to blank,

				if(bTimer)
				{

					EnterCriticalSection(&g_data.m_critSyncVariables);
					pOverrides->m_nReplacementType = g_data.m_Sync.m_nLastTimerEventType;
					pOverrides->m_ulEN_EventStart = (unsigned long)IdealAddTimeAndDuration(g_data.m_Sync.m_nLastTimerStart, g_data.m_Sync.m_nLastTimerDuration); // this can wrap midnight 
					// - we add program stream offset and go from there, make the correction later.
					// Actually, that is a problem.  If the timer was set yesterday, and now we have a +24 hour thing, when the timer expires it will actually 
					// advance one day from today - making the thing "tomorrow" - not good.

					// so let's just back it up. if necessary - if it ends up just before midnight, and we add a program stream offset kicks us back over, that is correct.
					if(pOverrides->m_ulEN_EventStart>86399999)
					{
						pOverrides->m_ulEN_EventStart -= 86400000;
					}
					
					g_data.m_Sync.m_nLastENEventStart = pOverrides->m_ulEN_EventStart;

					pOverrides->m_ulEN_EventDur = (unsigned long)(g_data.m_Output.m_n_current_primary_duration - ((int)pOverrides->m_ulEN_EventStart - g_data.m_Output.m_n_current_primary_start)); // in ideal MS; 
					g_data.m_Sync.m_nLastENEventDur = (int) pOverrides->m_ulEN_EventDur;


if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_TIMER)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Timer of type %02x expired with %d, %d; RM EN %d, %d (%d, %d)", 
	 pOverrides->m_nReplacementType,
	 g_data.m_Sync.m_nLastTimerStart, g_data.m_Sync.m_nLastTimerDuration,
	 pOverrides->m_ulEN_EventStart,
	 pOverrides->m_ulEN_EventDur,
	 g_data.m_Output.m_n_current_primary_start,
	 g_data.m_Output.m_n_current_primary_duration
	 );


					LeaveCriticalSection(&g_data.m_critSyncVariables);
				}
				else
				{
					if(bCurrent)
					{
						// have to grab the TC here, and subtract dur etc.

						if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
						{
							unsigned long ulTimeCode;
							EnterCriticalSection(&g_data.m_critTimeCode);
							ulTimeCode = g_data.m_ulTimeCode;
				//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
							LeaveCriticalSection(&g_data.m_critTimeCode);

							pOverrides->m_ulEN_EventStart = m_timeconv.BCDHMSFtoMS(ulTimeCode, ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)); // make it into ideal ms.

							
			if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_TIMECODE)) 
			 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:CurrentTime", "TC %d", pOverrides->m_ulEN_EventStart); // Sleep(250); //(Dispatch message)


						}
						else
						{
							_timeb timestamp;
							_ftime(&timestamp);

							// use system time
							pOverrides->m_ulEN_EventStart = ((timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))%86400)*1000 + timestamp.millitm;
			if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_TIMECODE)) 
			 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:CurrentTime", "TC sys %d", pOverrides->m_ulEN_EventStart); // Sleep(250); //(Dispatch message)
						}

						// this prob needs ideal subtraction compensation, but maybe not.
										
						pOverrides->m_ulEN_EventDur = (unsigned long)(g_data.m_Output.m_n_current_primary_duration - ((int)pOverrides->m_ulEN_EventStart - g_data.m_Output.m_n_current_primary_start)); // in ideal MS; 
					}
					else
					{
						pOverrides->m_ulEN_EventStart = m_Output.m_n_current_primary_start;						// The current LTC time address, plus the Program Stream Offset. for manual event 
						pOverrides->m_ulEN_EventDur = m_Output.m_n_current_primary_duration;							// The time address 00:00:00;00.  for manual event
					}
				}

				LeaveCriticalSection(&g_data.m_critOutputVariables);
			}
			else
			{
				LeaveCriticalSection(&g_data.m_critOutputVariables);

				if((g_settings.m_pszUnknownIDToken)&&(strlen(g_settings.m_pszUnknownIDToken)))
				{
					strncpy(pOverrides->m_pszEN_ProgramID, g_settings.m_pszUnknownIDToken, 16);
				}
				else
				{
					strcpy(pOverrides->m_pszEN_ProgramID, "TV+:UNKNOWN");
				}

				if((g_settings.m_pszBlockedDescToken)&&(strlen(g_settings.m_pszBlockedDescToken)))
				{
					strncpy(pOverrides->m_pszEN_ProgramDescription, g_settings.m_pszBlockedDescToken, 32);
				}
				// else nothing, it initializes to blank,

				if(bTimer)
				{

					EnterCriticalSection(&g_data.m_critSyncVariables);
					pOverrides->m_nReplacementType = g_data.m_Sync.m_nLastTimerEventType;

					pOverrides->m_ulEN_EventStart = (unsigned long)IdealAddTimeAndDuration(g_data.m_Sync.m_nLastTimerStart, g_data.m_Sync.m_nLastTimerDuration); // this can wrap midnight 
					// - we add program stream offset and go from there, make the correction later.
					// Actually, that is a problem.  If the timer was set yesterday, and now we have a +24 hour thing, when the timer expires it will actually 
					// advance one day from today - making the thing "tomorrow" - not good.

					// so let's just back it up. if necessary - if it ends up just before midnight, and we add a program stream offset kicks us back over, that is correct.
					if(pOverrides->m_ulEN_EventStart>86399999)
					{
						pOverrides->m_ulEN_EventStart -= 86400000;
					}



//					g_data.m_Sync.m_nLastENEventStart = pOverrides->m_ulEN_EventStart;  // don't use this, it was a manual event

					pOverrides->m_ulEN_EventDur = 0;

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_TIMER)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Timer of type %02x expired with %d, %d; RM EN %d, %d", 
	 pOverrides->m_nReplacementType,
	 g_data.m_Sync.m_nLastTimerStart, 
	 g_data.m_Sync.m_nLastTimerDuration,
	 pOverrides->m_ulEN_EventStart,
	 pOverrides->m_ulEN_EventDur
	 );


					LeaveCriticalSection(&g_data.m_critSyncVariables);

				}
				else
				{
					pOverrides->m_ulEN_EventStart = AUTODATA_USECURRENT_TIMEADDRESS;						// The current LTC time address, plus the Program Stream Offset. for manual event 
					pOverrides->m_ulEN_EventDur = 0;							// The time address 00:00:00;00.  for manual event
				}
			}
			int n= PlayEvent(&m_pRejoinMainEvents, pOverrides, errorstring);
			if(n<CLIENT_SUCCESS)
			{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "AutomationData:Enter_Pass", "Error: %s", errorstring ); // Sleep(250); //(Dispatch message)
			}

			delete pOverrides;
			return n;

		}
	}
	return TABULATOR_ERROR;
}


int CAutomationDataData::Enter_Block()
{
												// do the re-enter BLOCK control mode event associated with this button.
												//TODO
/*
BLOCK Control State
On entering (or re-entering) the BLOCK Control State, Promotor shall:
1. Extinguish Button 1 ("AUTO") on the Basic Operator Panel.   // handled by the GTP 32 setup
2. Generate and insert an Event Notification Message having the value "TV+:BLOCKED" in the ProgramID field, thereby initiating an indefinite replacement operation at upLynk.
3. Extinguish Button 2 ("PASS") on the Basic Operator Panel.   // handled by the GTP 32 setup
4. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.  // handled by the GTP 32 setup
5. Close the Downstream Keyer Control GPO.										 // handled by the GTP 32 setup
*/
	
												// the BLOCK tabulator event.
												//  Event notification message
												//  Close USP GPO 3 ON


//all of this is triggered with a single token:

// AutomationData|Enter_Block

	// uses m_pEventNotificationEvents

	if(m_pEventNotificationEvents)
	{
		CMessageOverrideValues* pOverrides = new CMessageOverrideValues;
		char errorstring[MAX_MESSAGE_LENGTH];

		if(pOverrides)
		{
/*
			pOverrides->m_ulEN_EventStart;						// The current LTC time address, plus the Program Stream Offset. for manual event 
			pOverrides->m_ulEN_EventDur;							// The time address 00:00:00;00.  for manual event
			pOverrides->m_pszEN_ProgramID[17];						      	// The value "TV+:UNKNOWN".  for manual event
			pOverrides->m_pszEN_ProgramDescription[33];						// Blank.  for manual event

	
	>There are three cases where Stream Messages are generated on entering (or re-entering) a Control State: page 17, lines 24-25 (AUTO); page 18, lines 8-9 (PASS); and page 18, lines 13-15 (BLOCK).
	>In these cases, you may follow the requirements, on page 12, lines 16-21, for Stream Messages generated while processing Manual Events.
	EventStart: The current LTC time address, plus the Program Stream Offset.
	EventDur: The time address 00:00:00;00.
	ProgramID: The value "TV+:BLOCKED".
	ProgramDescription: Blank.

*/

			if((g_settings.m_pszBlockedIDToken)&&(strlen(g_settings.m_pszBlockedIDToken)))
			{
				strncpy(pOverrides->m_pszEN_ProgramID, g_settings.m_pszBlockedIDToken, 16);
			}
			else
			{
				strcpy(pOverrides->m_pszEN_ProgramID, "TV+:BLOCKED");
			}

			if((g_settings.m_pszBlockedDescToken)&&(strlen(g_settings.m_pszBlockedDescToken)))
			{
				strncpy(pOverrides->m_pszEN_ProgramDescription, g_settings.m_pszBlockedDescToken, 32);
			}
			// else nothing, it initializes to blank,

			pOverrides->m_ulEN_EventStart = AUTODATA_USECURRENT_TIMEADDRESS;						// The current LTC time address, plus the Program Stream Offset. for manual event 
			pOverrides->m_ulEN_EventDur = 0;							// The time address 00:00:00;00.  for manual event

			int n= PlayEvent(&m_pEventNotificationEvents, pOverrides, errorstring);
			if(n<CLIENT_SUCCESS)
			{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "AutomationData:Enter_Block", "Error: %s", errorstring ); // Sleep(250); //(Dispatch message)
			}

			delete pOverrides;
			return n;

		}
	}
	return TABULATOR_ERROR;
}


int CAutomationDataData::Replace_Manual(CMessageOverrideValues* pOverrides)
{
	// uses m_pEventNotificationEvents

	if(m_pTimerEvents)
	{
//		CMessageOverrideValues* pOverrides = new CMessageOverrideValues;
		char errorstring[MAX_MESSAGE_LENGTH];

		int n = TABULATOR_ERROR;
		if(pOverrides)
		{
/*
			pOverrides->m_ulEN_EventStart;						// The current LTC time address, plus the Program Stream Offset. for manual event 
			pOverrides->m_ulEN_EventDur;							// The time address 00:00:00;00.  for manual event
			pOverrides->m_pszEN_ProgramID[17];						      	// The value "TV+:UNKNOWN".  for manual event
			pOverrides->m_pszEN_ProgramDescription[33];						// Blank.  for manual event

	

To process a Manual Event, given an invoking GPO number, Promotor shall follow
these steps:
1. If the Event Masking Timer is counting down  // THIS IS DONE OUTSIDE THIS FUNCTION CALL

	
		or Promotor is not in the AUTO
Control State, stop processing of this Manual Event.


2. Retrieve the entry, if any, in the Manual Event Table having a GPO Number equal to the invoking GPO number.
If there is no such entry, stop processing of this Manual Event.

	// we are going to do step 2 first, since there is no consequence.
	// then send over the info and let the automation_data.dll cancel the event if step 1 evaluates.


3. If the Event Type in the Manual Event Table entry is Ad Replacement:
  a. Generate and insert a Replace Ad Message, populated as follows:
   - PodDur: The Duration in the Manual Event Table entry.
   - LongFormProgramID: The value "TV+:UNKNOWN"
   - LongFormProgramDescription: Blank.
  b. Start the Event Masking Timer with the Duration in the Manual Event Table entry.
  c. Extinguish Button 2 ("PASS") on the Basic Operator Panel.
  d. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.
  e. Close the Downstream Keyer Control GPO.
  f. Stop processing of this Manual Event.

												// the Replace Ad tabulator event.
												//  Replace Ad Message message

												//  BLOCK GPO w Timer message
												//  Start Timer1
												//  Close USP GPO 3 ON
											*/

			if(pOverrides->m_nReplacementType == AUTODATA_MANUAL_TYPE_ADREPL)
			{
				if(m_pReplaceAdEvents)
				{
					n= PlayEvent(&m_pReplaceAdEvents, pOverrides, errorstring);
				}
				else
				{
					strcpy(errorstring, "Replace Ad events did not exist");
				}
				if(n<CLIENT_SUCCESS)
				{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "AutomationData:Replace_Manual", "Error (Replace ad): %s", errorstring ); // Sleep(250); //(Dispatch message)
				}
				else
				{
					n= PlayEvent(&m_pTimerEvents, pOverrides, errorstring);
					if(n<CLIENT_SUCCESS)
					{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "AutomationData:Replace_Manual", "Error (Timer): %s", errorstring ); // Sleep(250); //(Dispatch message)
					}
				}
			}
			else
			if(pOverrides->m_nReplacementType == AUTODATA_MANUAL_TYPE_CONTENTREPL)
			{

				char pszOriginal_ProgramID[17];			
				memset(pszOriginal_ProgramID, 0, 17);
				strcpy(pszOriginal_ProgramID,  pOverrides->m_pszOriginal_ProgramID );	



/*
4. If the Event Type in the Manual Event Table entry is Program Replacement:
  a. Retrieve the entries in the Replacement Rules Table having an Original Program ID equal to the Program ID in the Retrieved Manual Event Table entry.
  b. If there are no such entries, stop processing of this Manual Event.

	*/
				int nCount=0;
	EnterCriticalSection(&g_data.m_critTableData);

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "AutomationData:Replace_Manual", "Sorting RR %08x, %d", 
	g_data.m_ppReplacementRules, g_data.m_nReplacementRules
	);

				if((g_data.m_ppReplacementRules)&&(g_data.m_nReplacementRules>0))
				{
					// cocktail sort.

					int nEnd = m_nReplacementRules-1;
					int nBegin = 0;
					int ix=0;
					BOOL bSwitch = TRUE;
					CReplacementRule* pRepRule=NULL;
					while(bSwitch == TRUE)
					{
						bSwitch = FALSE;
						for(ix = nBegin; ix < nEnd; ix++)
						{
							if((g_data.m_ppReplacementRules[ix])&&(g_data.m_ppReplacementRules[ix+1]))
							{
								if(g_data.m_ppReplacementRules[ix]->m_nSequence > g_data.m_ppReplacementRules[ix+1]->m_nSequence)
								{
									pRepRule = g_data.m_ppReplacementRules[ix];
									g_data.m_ppReplacementRules[ix] = g_data.m_ppReplacementRules[ix+1];
									g_data.m_ppReplacementRules[ix+1]=pRepRule;
									bSwitch = TRUE;
								}
							}
						}
						nEnd--;
						for(ix = nEnd; ix > nBegin; ix--)
						{
							if((g_data.m_ppReplacementRules[ix])&&(g_data.m_ppReplacementRules[ix-1]))
							{
								if(g_data.m_ppReplacementRules[ix]->m_nSequence < g_data.m_ppReplacementRules[ix-1]->m_nSequence)
								{
									pRepRule = g_data.m_ppReplacementRules[ix];
									g_data.m_ppReplacementRules[ix] = g_data.m_ppReplacementRules[ix-1];
									g_data.m_ppReplacementRules[ix-1]=pRepRule;
									bSwitch = TRUE;
								}
							}
						}
						nBegin++;
					}


if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
{
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "AutomationData:Replace_Manual", "Sorted RR %08x, %d", 
	g_data.m_ppReplacementRules, g_data.m_nReplacementRules
	);

	int i=0;
	while(i<g_data.m_nReplacementRules)
	{
		if(g_data.m_ppReplacementRules[i])
		{
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "AutomationData:debug", "RR%03d ID %s, seq %d, ReplID %s, Dur %d, %.3f-%.3f", 
	i, 
	g_data.m_ppReplacementRules[i]->m_pszProgramID,
	g_data.m_ppReplacementRules[i]->m_nSequence,
	g_data.m_ppReplacementRules[i]->m_pszReplacementID,
	g_data.m_ppReplacementRules[i]->m_nDuration, // in ideal MS
	g_data.m_ppReplacementRules[i]->m_dblValidDate,
	g_data.m_ppReplacementRules[i]->m_dblExpireDate
	
	);


		}
		i++;
	}
}


/*
	c. For each retrieved Replacement Rules Table entry, sorted by Replacement Sequence, from lowest to highest:
	{
		i. Generate and insert a Replace Content Message, populated as follows:
		 - ReplLength: The Replacement Duration in the Replacement Rules Table entry.
		 - Offset: The time address 00:00:00;00.
		 - ReplProgramID: The Replacement Program ID in the Replacement Rules Table entry.

												// the Replace Content tabulator event.
												//  Replace Content Message message

	 ii. Add the Replacement Duration in the Replacement Rules Table entry to the Total Replacement Duration.
	}
	*/
					double dblTime;
					_timeb timeNow;
					_ftime(&timeNow);
					// the timer duration is given in actual millseconds.
					pOverrides->m_ulActualTimerDurationMS = 0;
					pOverrides->m_ulRepl_EventStart = 0x00000000; // not used for ad replacement, offset for content replacement

					dblTime = ((double)(timeNow.time - (timeNow.timezone*60)+(timeNow.dstflag?3600:0))) + ((double)(timeNow.millitm))/1000.0; // local time
					int i=0;
					while(i<g_data.m_nReplacementRules)
					{
						if(g_data.m_ppReplacementRules[i])
						{

							if(
									(dblTime < g_data.m_ppReplacementRules[i]->m_dblExpireDate)  // not <= because = it's expired now
								&&(dblTime >= g_data.m_ppReplacementRules[i]->m_dblValidDate)  //  is >= because = means it's valid now
								)
							{
								if(g_data.m_ppReplacementRules[i]->m_pszProgramID)
								{
									if(strcmp(g_data.m_ppReplacementRules[i]->m_pszProgramID, pszOriginal_ProgramID)==0)
									{
										if((g_data.m_ppReplacementRules[i]->m_pszReplacementID)&&(strlen(g_data.m_ppReplacementRules[i]->m_pszReplacementID)>0))
										{
	//  a. Retrieve the entries in the Replacement Rules Table having an Original Program ID equal to the Program ID in the Retrieved Manual Event Table entry.


						//					pOverrides->m_ulActualTimerDurationMS += g_data.m_timeconv.NDFMStoDFMS( g_data.m_ppReplacementRules[i]->m_nDuration );
											if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
											{
//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp

												if(g_data.m_ucTimeCodeBits&0x80) // drop frame...
												{
													pOverrides->m_ulActualTimerDurationMS += g_data.m_timeconv.NDFMStoDFMS( g_data.m_ppReplacementRules[i]->m_nDuration );
												}
												else
												{
													pOverrides->m_ulActualTimerDurationMS += g_data.m_ppReplacementRules[i]->m_nDuration;
												}
											}
											else  // system time
											{
												if(g_settings.m_bDF) // drop frame...
												{
													pOverrides->m_ulActualTimerDurationMS += g_data.m_timeconv.NDFMStoDFMS( g_data.m_ppReplacementRules[i]->m_nDuration );
												}
												else
												{
													pOverrides->m_ulActualTimerDurationMS += g_data.m_ppReplacementRules[i]->m_nDuration;
												}
											}



											pOverrides->m_ulRepl_EventDur = g_data.m_ppReplacementRules[i]->m_nDuration;		// IDEaL is correct					// duration for ad replacement, length for content replacement 00:00:00;00 if not available.
											memset(pOverrides->m_pszRepl_ProgramID, 0, 17);
											strcpy(pOverrides->m_pszRepl_ProgramID,  g_data.m_ppReplacementRules[i]->m_pszReplacementID	);	

											if(m_pReplaceContentEvents)
											{
												n= PlayEvent(&m_pReplaceContentEvents, pOverrides, errorstring);
											}
											else
											{
												strcpy(errorstring, "Replace Content events did not exist");
											}
											if(n<CLIENT_SUCCESS)
											{
				g_data.SendMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, "Error (Replace Content): ID %s: %s", g_data.m_ppReplacementRules[i]->m_pszReplacementID, errorstring);
											}

											nCount++;
										}
										else
										{
									if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "AutomationData:Replace_Manual", "Error (Replace Content): ID %s: %s", g_data.m_ppReplacementRules[i]->m_pszReplacementID, errorstring ); // Sleep(250); //(Dispatch message)
				g_data.SendMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, "A replacement was discarded because the ID was blank.");
										}
									}
								}
							}
						}

						i++;
					}
				}
	LeaveCriticalSection(&g_data.m_critTableData);
				


/*

5. Start the Event Masking Timer with the Total Replacement Duration.

												//  BLOCK GPO w Timer message
												//  Start Timer1
												//  Close USP GPO 3 ON

6. Extinguish Button 2 ("PASS") on the Basic Operator Panel.
7. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.
8. Close the Downstream Keyer Control GPO.
9. Stop processing of this Manual Event.
*/
				if(nCount>0)
				{
				// set the timer duration
					n= PlayEvent(&m_pTimerEvents, pOverrides, errorstring);
					if(n<CLIENT_SUCCESS)
					{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "AutomationData:Replace_Manual", "Error (Timer): %s", errorstring ); // Sleep(250); //(Dispatch message)
					}

				}
				else
				{
					n= nCount;  // just return success
				}
				

//all of this is triggered with a single token:

//AutomationData|Replace_Manual|<manual type>|<manual replacement data (=Pod dur if type is ad, original ID if type is content)>  // the actual replace command, results in the commands being sent.to Libretto, uses the manual event procedure


			}   // content replacement

//			delete pOverrides;
			return n;

		}
	}
	return TABULATOR_ERROR;	
}

int CAutomationDataData::Replace(bool bCurrent, bool bTimer)
{
	int n =TABULATOR_ERROR;

	char szSource[64];

	if(bCurrent) strncpy(szSource, "AutomationData:Replace_Current", 63);
	else  strncpy(szSource, "AutomationData:Replace", 63);

/*
AUTO Control State
On entering (or re-entering) the AUTO Control State, Promotor shall:
1. Illuminate Button 1 ("AUTO") on the Basic Operator Panel.		// handled by the GTP 32 setup

	*********************** LOOK AT THIS ****************
2. If there is a Selected Automation System and it is currently executing a primary event, process an Automation Event with the in-progress primary event, 
   substituting for purposes of this processing:
			a. The current LTC time address in place of the starting time address of the in-progress primary event, and
			b. The remaining duration of the in-progress primary event in place of the duration of the in-progress primary event.
*******************************************************************

Processing Automation Events
To process an Automation Event, given a primary event, Promotor shall follow these steps:
1. (In Phase Two only:) If the Event Masking Timer is counting down, go to Step 7 of this procedure.
   (Before Phase Two:) If the Event Masking Timer is counting down, stop processing of this Automation Event.  ALREADY CHECKED OUTSIDE

	// we can send it over to the automation_data.dll for cancellation or processing.

	// IGNORE< we are SETTING the AUTO STATE.
2. If Promotor is not in the AUTO Control State, stop processing of this Automation Event.
	// For the purposes of the button press here, we can assemble the event and send it over to the automation_data.dll for cancellation or processing

3. If the primary event is classified as Advertisement:
     a. Compute the Pod Duration, which shall be the sum of the duration of the current primary event and the durations of each of the
        immediately following primary events, if any, in the playlist that are also classified as Advertisement.
        (Note: The ad pod ends with the first event that is not classified as Advertisement, and the durations of this event and following events shall not be added to the Pod Duration.)
     b. Generate and insert a Replace Ad Message, populated as follows:
        - PodDur: The Pod Duration.
        - LongFormProgramID: The Program ID of the Current Long-Form Program.
        - LongFormProgramDescription: The Program Description of the Current Long-Form Program.
     c. Start the Event Masking Timer with the Pod Duration.
     d. Extinguish Button 2 ("PASS") on the Basic Operator Panel.
     e. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.
     f. Close the Downstream Keyer Control GPO.
     g. Stop processing of this Automation Event.


												// the Replace Ad tabulator event.
												//  Replace Ad Message message

												//  BLOCK GPO w Timer message
												//  Start Timer1
												//  SET USP GPO 3 ON
												//  SET USP GPO 3 OFF "un-press" it - no effect


*/

if(g_settings.m_ulDebug&(AUTODATA_DEBUG_STATE)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:Replace", "entering m_critOutputVariables"); // Sleep(250); //(Dispatch message)
}

EnterCriticalSection(&g_data.m_critOutputVariables);
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_STATE)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:Replace", "entered m_critOutputVariables"); // Sleep(250); //(Dispatch message)
}
	if(!g_data.m_Output.m_b_list_is_playing)
	{
LeaveCriticalSection(&g_data.m_critOutputVariables);
		return Enter_Pass(bCurrent, bTimer);  // if list is not playing jst do the otherwise case.
	}
	else  // have to check the timer!
	{
		int nTimer=-1;
		if(!bTimer) nTimer = g_data.CheckTimer();

		if((bTimer)||((!bTimer)&&(nTimer==0))) // if it's a timer expiry, just go ahead. but if not, have to check the timer.
		{
			if(g_data.m_Output.m_n_current_primary_class & AUTODATA_CLASS_TYPE_AD)  // used & but could be ==.  just safer in case of other flags
			{
				if(m_pTimerEvents)
				{

					CMessageOverrideValues* pOverrides = new CMessageOverrideValues;
					char errorstring[MAX_MESSAGE_LENGTH];

					if(pOverrides)
					{
						if((g_data.m_Output.m_psz_current_primary_ID)&&(strlen(g_data.m_Output.m_psz_current_primary_ID)))
						{
							strncpy(pOverrides->m_pszEN_ProgramID, g_data.m_Output.m_psz_current_primary_ID, 16);
						}
						else
						{
							strcpy(pOverrides->m_pszEN_ProgramID, "TV+:UNKNOWN");
						}

						if((g_data.m_Output.m_psz_current_primary_description)&&(strlen(g_data.m_Output.m_psz_current_primary_description)))
						{
							strncpy(pOverrides->m_pszEN_ProgramDescription, g_data.m_Output.m_psz_current_primary_description, 32);
						}
						// else nothing, it initializes to blank,

						if(bCurrent)
						{
							if(bTimer)
							{
								// use "expected values"
				EnterCriticalSection(&g_data.m_critSyncVariables);

								pOverrides->m_ulEN_EventStart = (unsigned long)IdealAddTimeAndDuration(g_data.m_Sync.m_nLastTimerStart, g_data.m_Sync.m_nLastTimerDuration); // this can wrap midnight 
								// - we add program stream offset and go from there, make the correction later.
								// Actually, that is a problem.  If the timer was set yesterday, and now we have a +24 hour thing, when the timer expires it will actually 
								// advance one day from today - making the thing "tomorrow" - not good.

								// so let's just back it up. if necessary - if it ends up just before midnight, and we add a program stream offset kicks us back over, that is correct.
								if(pOverrides->m_ulEN_EventStart>86399999)
								{
									pOverrides->m_ulEN_EventStart -= 86400000;
								}



								g_data.m_Sync.m_nLastENEventStart = (int)pOverrides->m_ulEN_EventStart;

								pOverrides->m_nReplacementType = AUTODATA_AUTO_TYPE_ADREPL;

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_TIMER)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Timer of type %02x expired with %d, %d; AR EN %d", 
	 pOverrides->m_nReplacementType,
	 g_data.m_Sync.m_nLastTimerStart, g_data.m_Sync.m_nLastTimerDuration,
	 pOverrides->m_ulEN_EventStart);

				LeaveCriticalSection(&g_data.m_critSyncVariables);

							}
							else
							{
								_timeb timeNow;
								_ftime(&timeNow);

								if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
								{
									unsigned long ulTimeCode;
									EnterCriticalSection(&g_data.m_critTimeCode);
									ulTimeCode = g_data.m_ulTimeCode;
						//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
									LeaveCriticalSection(&g_data.m_critTimeCode);

									pOverrides->m_ulEN_EventStart = m_timeconv.BCDHMSFtoMS(ulTimeCode, ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)); // make it into ideal ms.
								}
								else
								{
									// use system time
									pOverrides->m_ulEN_EventStart = ((timeNow.time - (timeNow.timezone*60) +(timeNow.dstflag?3600:0))%86400)*1000 + timeNow.millitm;
								}
								pOverrides->m_nReplacementType = AUTODATA_MANUAL_TYPE_ADREPL;  // current and not timer means an actual button was pressed, re-entering auto.
							}

							pOverrides->m_ulEN_EventDur = (unsigned long)(g_data.m_Output.m_n_current_primary_duration - ((int)pOverrides->m_ulEN_EventStart - g_data.m_Output.m_n_current_primary_start)); // in ideal MS; 

							if(bTimer)
							{
				EnterCriticalSection(&g_data.m_critSyncVariables);
								g_data.m_Sync.m_nLastENEventDur = (int) pOverrides->m_ulEN_EventDur;
				LeaveCriticalSection(&g_data.m_critSyncVariables);
							}

							if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
							{

								if(g_data.m_ucTimeCodeBits&0x80) // drop frame...
								{
									pOverrides->m_ulRepl_EventDur = g_data.m_Output.m_n_current_ad_pod_duration // this is real ms
										- g_data.m_timeconv.NDFMStoDFMS(pOverrides->m_ulEN_EventStart - g_data.m_Output.m_n_current_ad_pod_start); // in ideal MS; 
								}
								else
								{
									pOverrides->m_ulRepl_EventDur = g_data.m_Output.m_n_current_ad_pod_duration // this is real ms
										- (pOverrides->m_ulEN_EventStart - g_data.m_Output.m_n_current_ad_pod_start); // in ideal MS; 
								}
							}
							else
							{
								if(g_settings.m_bDF)
								{
									pOverrides->m_ulRepl_EventDur = g_data.m_Output.m_n_current_ad_pod_duration // this is real ms
										- g_data.m_timeconv.NDFMStoDFMS(pOverrides->m_ulEN_EventStart - g_data.m_Output.m_n_current_ad_pod_start); // in ideal MS; 
								}
								else
								{
									pOverrides->m_ulRepl_EventDur = g_data.m_Output.m_n_current_ad_pod_duration // this is real ms
										- (pOverrides->m_ulEN_EventStart - g_data.m_Output.m_n_current_ad_pod_start); // in ideal MS; 
								}
							}
						}
						else
						{
							pOverrides->m_ulEN_EventStart = g_data.m_Output.m_n_current_primary_start;
							pOverrides->m_ulEN_EventDur = g_data.m_Output.m_n_current_primary_duration;
							pOverrides->m_ulRepl_EventDur = g_data.m_Output.m_n_current_ad_pod_duration; // real ms
							pOverrides->m_nReplacementType = AUTODATA_AUTO_TYPE_ADREPL;
						}

						pOverrides->m_ulActualTimerDurationMS = pOverrides->m_ulRepl_EventDur;
		/*
			 a. Compute the Pod Duration, which shall be the sum of the duration of the current primary event and the durations of each of the
					immediately following primary events, if any, in the playlist that are also classified as Advertisement.
					(Note: The ad pod ends with the first event that is not classified as Advertisement, and the durations of this event and following events shall not be added to the Pod Duration.)
			 b. Generate and insert a Replace Ad Message, populated as follows:
					- PodDur: The Pod Duration.
					- LongFormProgramID: The Program ID of the Current Long-Form Program.
					- LongFormProgramDescription: The Program Description of the Current Long-Form Program.

		*/

		/*  // can this case even exist?  if there is an ad playing, then we have an ad pod.
						if(
								(g_data.m_Output.m_n_current_ad_pod_duration == AUTODATA_INVALID_TIMEADDRESS)
							||(g_data.m_Output.m_n_current_long_form_duration == AUTODATA_INVALID_TIMEADDRESS)
						{

						}
		*/
						strncpy(pOverrides->m_pszRepl_ProgramID, ((g_data.m_Output.m_psz_current_long_form_ID)&&(strlen(g_data.m_Output.m_psz_current_long_form_ID)))?g_data.m_Output.m_psz_current_long_form_ID:"TV+:UNKNOWN", 16); 
						strncpy(pOverrides->m_pszRepl_ProgramDescription, ((g_data.m_Output.m_psz_current_long_form_description)&&(strlen(g_data.m_Output.m_psz_current_long_form_description)))?g_data.m_Output.m_psz_current_long_form_description:"", 32); 
		LeaveCriticalSection(&g_data.m_critOutputVariables);


						if(m_pReplaceAdEvents)
						{
							n= PlayEvent(&m_pReplaceAdEvents, pOverrides, errorstring);
						}
						else
						{
							strcpy(errorstring, "Replace Ad events did not exist");
						}
						if(n<CLIENT_SUCCESS)
						{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szSource, "Error (Replace ad): %s", errorstring ); // Sleep(250); //(Dispatch message)
						}
						else
						{
							n= PlayEvent(&m_pTimerEvents, pOverrides, errorstring);
							if(n<CLIENT_SUCCESS)
							{
					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szSource, "Error (Timer): %s", errorstring ); // Sleep(250); //(Dispatch message)
							}
						}

						delete pOverrides;
						return n;
					}
					else
					{
		LeaveCriticalSection(&g_data.m_critOutputVariables);
					}
					
				} // have events
				else
				{
		LeaveCriticalSection(&g_data.m_critOutputVariables);

				}

			} // type was ad
			else
			{ // type is not ad



		/*
		else do the complex thing:

		4. Retrieve the entries in the Replacement Rules Table having an Original Program ID equal to the Program ID of the primary event.

		*/		

				int nNumEntries=0;
				int nArraySize=0;
				CReplacementRule** ppRRules = NULL;
				double dblTime;
				_timeb timeNow;
				_ftime(&timeNow);
				dblTime = ((double)(timeNow.time - (timeNow.timezone*60)+(timeNow.dstflag?3600:0))) + ((double)(timeNow.millitm))/1000.0; // local time
			EnterCriticalSection(&g_data.m_critTableData);

				if((g_data.m_ppReplacementRules)&&(g_data.m_nReplacementRules>0))
				{
					ppRRules = new CReplacementRule*[g_data.m_nReplacementRules];

					if(ppRRules)
					{
						nArraySize = g_data.m_nReplacementRules;
						int i=0;
						while(i<g_data.m_nReplacementRules)
						{
							if((g_data.m_ppReplacementRules[i])&&(g_data.m_ppReplacementRules[i]->m_pszProgramID))
							{
								if(
										(dblTime < g_data.m_ppReplacementRules[i]->m_dblExpireDate)  // not <= because = it's expired now
									&&(dblTime >= g_data.m_ppReplacementRules[i]->m_dblValidDate)  //  is >= because = means it's valid now
									)
								{
									if(g_data.m_Output.m_psz_current_primary_ID)
									{
										if(strcmp(g_data.m_ppReplacementRules[i]->m_pszProgramID, g_data.m_Output.m_psz_current_primary_ID)==0)
										{
											//match!
											ppRRules[nNumEntries++] = g_data.m_ppReplacementRules[i];
										}
									}
								}
							}
							i++;
						}
					}
				}




		/*
		5. If no entries were retrieved from the Replacement Rules Table in the previous step, evaluate entries in the Description Pattern Table, and for
			 each entry in the Description Pattern Table having a Matching Pattern satisfied by the Program Description of the primary event, retrieve the
			 entries from the Replacement Rules Table having an Original Program ID equal to the Program ID in the Description Pattern Table entry.
		*/

				if(nNumEntries<=0)
				{
					if(
							(g_data.m_ppReplacementRules)&&(g_data.m_nReplacementRules>0)        // have to have replacement rules
						&&(g_data.m_ppDescriptionPatterns)&&(g_data.m_nDescriptionPatterns>0)  // have to have description patterns
						)
					{
						int i=0;
						while(i<g_data.m_nDescriptionPatterns)
						{
							if((g_data.m_ppDescriptionPatterns[i])&&(g_data.m_ppDescriptionPatterns[i]->m_pszPattern)&&(strlen(g_data.m_ppDescriptionPatterns[i]->m_pszPattern)>0))
							{
								if(
										(dblTime < g_data.m_ppDescriptionPatterns[i]->m_dblExpireDate)  // not <= because = it's expired now
									&&(dblTime >= g_data.m_ppDescriptionPatterns[i]->m_dblValidDate)  //  is >= because = means it's valid now
									)
								{
									if(g_data.m_Output.m_psz_current_primary_description)
									{
										if(g_data.TestPattern(g_data.m_Output.m_psz_current_primary_description, g_data.m_ppDescriptionPatterns[i]->m_pszPattern) == AUTODATA_PATTERN_MATCH)
										{
											// retrieve the entries from the Replacement Rules Table having an Original Program ID equal to the Program ID in the Description Pattern Table entry.
											if(g_data.m_ppDescriptionPatterns[i]->m_pszProgramID)
											{
												int j=0;
												while(j<g_data.m_nReplacementRules)
												{
													if((g_data.m_ppReplacementRules[j])&&(g_data.m_ppReplacementRules[j]->m_pszProgramID))
													{
														if(
																(dblTime < g_data.m_ppReplacementRules[j]->m_dblExpireDate)  // not <= because = it's expired now
															&&(dblTime >= g_data.m_ppReplacementRules[j]->m_dblValidDate)  //  is >= because = means it's valid now
															)
														{
															if(strcmp(g_data.m_ppReplacementRules[j]->m_pszProgramID, g_data.m_ppDescriptionPatterns[i]->m_pszProgramID)==0)
															{
																//match!

																if(ppRRules)
																{
																	if(nNumEntries<nArraySize)
																	{
																		ppRRules[nNumEntries++] = g_data.m_ppReplacementRules[j];
																	}
																	else
																	{
																		CReplacementRule** ppTemp = new CReplacementRule*[nArraySize+AUTODATA_DATA_ARRAY_INCREMENT];

																		if(ppTemp)
																		{
																			nArraySize += AUTODATA_DATA_ARRAY_INCREMENT;
																			memcpy(ppTemp, ppRRules, nNumEntries*sizeof(CReplacementRule*));
																			delete [] ppRRules;
																			ppRRules = ppTemp;

																			ppRRules[nNumEntries++] = g_data.m_ppReplacementRules[j];
																		}
																	}
																}
																else
																{
																	ppRRules = new CReplacementRule*[g_data.m_nReplacementRules];

																	if(ppRRules)
																	{
																		nArraySize = g_data.m_nReplacementRules;
																		ppRRules[nNumEntries++] = g_data.m_ppReplacementRules[j];
																	}
																}
															}
														}
													}
													j++;
												}
											}
										}
									}
								}
							}
							i++;
						}
					}
				}


		/*

		6. If one or more entries were retrieved from the Replacement Rules Table in either of the two previous steps:

		*/	
				
				if((nNumEntries>0)&&(ppRRules))
				{


		/*
				 a. Create an Effective Replacement Rules List, which shall contain the retrieved Replacement Rules Table entries, sorted by Replacement
						Sequence, from lowest to highest. Each entry in the Effective Replacement Rules List shall also have an Offset, which initially shall be the time address 00:00:00;00.
						(Note: The Effective Replacement Rules List need not persist after processing of the Automation Event is complete.)
		*/

				// OK, first sort them.

					// cocktail sort.

					int nEnd = nNumEntries-1;
					int nBegin = 0;
					int ix=0;
					BOOL bSwitch = TRUE;
					CReplacementRule* pRepRule=NULL;
					while(bSwitch == TRUE)
					{
						bSwitch = FALSE;
						for(ix = nBegin; ix < nEnd; ix++)
						{
							if((ppRRules[ix])&&(ppRRules[ix+1]))
							{
								if(ppRRules[ix]->m_nSequence > ppRRules[ix+1]->m_nSequence)
								{
									pRepRule = ppRRules[ix];
									ppRRules[ix] = ppRRules[ix+1];
									ppRRules[ix+1]=pRepRule;
									bSwitch = TRUE;
								}
							}
						}
						nEnd--;
						for(ix = nEnd; ix > nBegin; ix--)
						{
							if((ppRRules[ix])&&(ppRRules[ix-1]))
							{
								if(ppRRules[ix]->m_nSequence < ppRRules[ix-1]->m_nSequence)
								{
									pRepRule = ppRRules[ix];
									ppRRules[ix] = ppRRules[ix-1];
									ppRRules[ix-1]=pRepRule;
									bSwitch = TRUE;
								}
							}
						}
						nBegin++;
					}

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Computing Trial Duration");

					ix=0;
					int nTrialDuration = 0; //let's make this "real" ms // NO - now it is number of frames.
					while(ix < nNumEntries)
					{
						if(ppRRules[ix])
						{
							ppRRules[ix]->m_nOffset = 0; //Each entry in the Effective Replacement Rules List shall also have an Offset, which initially shall be the time address 00:00:00;00.

							// also number of frames.

							// initialize the output duration
							ppRRules[ix]->m_nOutputDuration = ppRRules[ix]->m_nDuration;

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Computing Trial Duration: Replacement index %d, id [%s], adding dur %d (%d fr) to %d fr", 
							 ix, 
							 ppRRules[ix]->m_pszReplacementID?ppRRules[ix]->m_pszReplacementID:"(null)",
							 ppRRules[ix]->m_nDuration, ppRRules[ix]->m_nDurationInFrames, nTrialDuration
							 );

							nTrialDuration += ppRRules[ix]->m_nDurationInFrames;  // minus the zero offsets

						}
						ix++;
					}
if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Computed Trial Duration %d frames", nTrialDuration);

		/*
				 b. Compute the Required Replacement Duration, which shall be the starting time address of the Next Long-Form Program less the starting time address of the current primary event
				 // BUT, replacing starting time address of the current primary event
			*********************** LOOK AT THIS ****************
		2. If there is a Selected Automation System and it is currently executing a primary event, process an Automation Event with the in-progress primary event, 
			 substituting for purposes of this processing:
					a. The current LTC time address in place of the starting time address of the in-progress primary event, and
					b. The remaining duration of the in-progress primary event in place of the duration of the in-progress primary event.
		*******************************************************************

		*/


					CMessageOverrideValues* pOverrides = new CMessageOverrideValues;
					char errorstring[MAX_MESSAGE_LENGTH];

					if(pOverrides)
					{
						if((g_data.m_Output.m_psz_current_primary_ID)&&(strlen(g_data.m_Output.m_psz_current_primary_ID)))
						{
							strncpy(pOverrides->m_pszEN_ProgramID, g_data.m_Output.m_psz_current_primary_ID, 16);
						}
						else
						{
							strcpy(pOverrides->m_pszEN_ProgramID, "TV+:UNKNOWN");
						}

						if((g_data.m_Output.m_psz_current_primary_description)&&(strlen(g_data.m_Output.m_psz_current_primary_description)))
						{
							strncpy(pOverrides->m_pszEN_ProgramDescription, g_data.m_Output.m_psz_current_primary_description, 32);
						}
						// else nothing, it initializes to blank,

						if(bCurrent)
						{

							if(bTimer)
							{
								// use "expected values"
				EnterCriticalSection(&g_data.m_critSyncVariables);

								pOverrides->m_ulEN_EventStart = (unsigned long)IdealAddTimeAndDuration(g_data.m_Sync.m_nLastTimerStart, g_data.m_Sync.m_nLastTimerDuration); // this can wrap midnight 
								// - we add program stream offset and go from there, make the correction later.
								// Actually, that is a problem.  If the timer was set yesterday, and now we have a +24 hour thing, when the timer expires it will actually 
								// advance one day from today - making the thing "tomorrow" - not good.

								// so let's just back it up. if necessary - if it ends up just before midnight, and we add a program stream offset kicks us back over, that is correct.
								if(pOverrides->m_ulEN_EventStart>86399999)
								{
									pOverrides->m_ulEN_EventStart -= 86400000;
								}



								g_data.m_Sync.m_nLastENEventStart = (int)pOverrides->m_ulEN_EventStart;
								pOverrides->m_nReplacementType = AUTODATA_AUTO_TYPE_CONTENTREPL;

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_TIMER)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Timer of type %02x expired with %d, %d; CR EN %d", 
	 pOverrides->m_nReplacementType,
	 g_data.m_Sync.m_nLastTimerStart, g_data.m_Sync.m_nLastTimerDuration,
	 pOverrides->m_ulEN_EventStart);
				LeaveCriticalSection(&g_data.m_critSyncVariables);
							}
							else
							{
				//					pOverrides->m_ulEN_EventStart = AUTODATA_USECURRENT_TIMEADDRESS;						// The current LTC time address, plus the Program Stream Offset. for manual event 
								if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
								{
									unsigned long ulTimeCode;
									EnterCriticalSection(&g_data.m_critTimeCode);
									ulTimeCode = g_data.m_ulTimeCode;
						//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
									LeaveCriticalSection(&g_data.m_critTimeCode);

									pOverrides->m_ulEN_EventStart = m_timeconv.BCDHMSFtoMS(ulTimeCode, ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)); // make it into ideal ms.

								}
								else
								{
									// use system time
									pOverrides->m_ulEN_EventStart = ((timeNow.time - (timeNow.timezone*60) +(timeNow.dstflag?3600:0))%86400)*1000 + timeNow.millitm;
								}
								pOverrides->m_nReplacementType = AUTODATA_MANUAL_TYPE_CONTENTREPL;   // if current and not timer, it means an actual button was pressed, re-entering auto - let's defeat any delays.

							}

							pOverrides->m_ulEN_EventDur = (unsigned long)(g_data.m_Output.m_n_current_primary_duration - ((int)pOverrides->m_ulEN_EventStart - g_data.m_Output.m_n_current_primary_start)); // in ideal MS; 
							if(bTimer)
							{
				EnterCriticalSection(&g_data.m_critSyncVariables);
								g_data.m_Sync.m_nLastENEventDur = (int)pOverrides->m_ulEN_EventDur;
				LeaveCriticalSection(&g_data.m_critSyncVariables);
							}
							
						}
						else
						{
							pOverrides->m_ulEN_EventStart = g_data.m_Output.m_n_current_primary_start;
							pOverrides->m_ulEN_EventDur = g_data.m_Output.m_n_current_primary_duration;
							pOverrides->m_nReplacementType = AUTODATA_AUTO_TYPE_CONTENTREPL;
						}
						
if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Computing Replacement Duration %d - %d", g_data.m_Output.m_n_next_long_form_start, pOverrides->m_ulEN_EventStart);


//						int nReplacementDuration = g_data.m_Output.m_n_next_long_form_start - pOverrides->m_ulEN_EventStart; //ideal MS.  and changed back to ideal
//						int nReplacementDuration;// changed to real ms = g_data.m_Output.m_n_next_long_form_start - pOverrides->m_ulEN_EventStart; //ideal MS.

						int nReplacementDuration;// now changed to frames

						if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
						{
//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp

							if(g_data.m_ucTimeCodeBits&0x80) // drop frame...
							{
								nReplacementDuration = g_data.m_timeconv.MStoF(g_data.m_timeconv.NDFMStoDFMS( g_data.m_Output.m_n_next_long_form_start ), 29.97) 
									- g_data.m_timeconv.MStoF(g_data.m_timeconv.NDFMStoDFMS( pOverrides->m_ulEN_EventStart ), 29.97);

								while(nReplacementDuration<0) // it means we had a midnight rollover
								{
									nReplacementDuration += 2589408;  // this will compensate. number of frames in a drop frame "day"
								}

							}
							else
							{
								nReplacementDuration = g_data.m_timeconv.MStoF( g_data.m_Output.m_n_next_long_form_start , ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) 
									- g_data.m_timeconv.MStoF( pOverrides->m_ulEN_EventStart , ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0));

								while(nReplacementDuration<0) // it means we had a midnight rollover
								{
									nReplacementDuration += g_data.m_timeconv.MStoF( 86400000 , ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0));  // this will compensate. number of frames in a day
								}


							}
						}
						else  // system time
						{
							if(g_settings.m_bDF) // drop frame...
							{
								nReplacementDuration = g_data.m_timeconv.MStoF(g_data.m_timeconv.NDFMStoDFMS( g_data.m_Output.m_n_next_long_form_start ), 29.97) 
									- g_data.m_timeconv.MStoF(g_data.m_timeconv.NDFMStoDFMS( pOverrides->m_ulEN_EventStart ), 29.97);

								while(nReplacementDuration<0) // it means we had a midnight rollover
								{
									nReplacementDuration += 2589408;  // this will compensate. number of frames in a drop frame "day"
								}

							}
							else
							{
								nReplacementDuration = g_data.m_timeconv.MStoF( g_data.m_Output.m_n_next_long_form_start , g_settings.m_nFrameRate) 
									- g_data.m_timeconv.MStoF( pOverrides->m_ulEN_EventStart , g_settings.m_nFrameRate);

								while(nReplacementDuration<0) // it means we had a midnight rollover
								{
									nReplacementDuration += g_data.m_timeconv.MStoF( 86400000 , g_settings.m_nFrameRate);  // this will compensate. number of frames in a day
								}
							}
						}

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Computed Replacement Duration %d", nReplacementDuration);


if(g_settings.m_ulDebug&(AUTODATA_DEBUG_STATE)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:Replace", "leaving m_critOutputVariables"); // Sleep(250); //(Dispatch message)
}
		LeaveCriticalSection(&g_data.m_critOutputVariables);

if(g_settings.m_ulDebug&(AUTODATA_DEBUG_STATE)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:Replace", "left m_critOutputVariables"); // Sleep(250); //(Dispatch message)
}
		/*
				 c. Compute the Trial Total Replacement Duration, which shall be the sum of the Replacement Durations of the entries in the Effective Replacement Rules List, 
						less the sum of the Offsets of the entries in the Effective Replacement Rules List.

						DONE ABOVE.

						

				 d. If the Trial Total Replacement Duration is less than the Required Replacement Duration, increase the Replacement Duration of the last entry
						in the Effective Replacement Rules List by the difference between the Required Replacement Duration and the Trial Total Replacement Duration.
		*/

						if(nTrialDuration<nReplacementDuration)
						{
if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Trial duration %d frames was less than Replacement Duration %d frames, extending last item by %d frames",
	 nTrialDuration, nReplacementDuration, nReplacementDuration-nTrialDuration);

// what it was.
//							ppRRules[nNumEntries-1]->m_nDuration += (nReplacementDuration-nTrialDuration);  // all ideal ms. // NO, now frames.

// but now, need to add ideal ms to it based on frames

							int nDurAdjust;

							if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
							{
	//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp

								if(g_data.m_ucTimeCodeBits&0x80) // drop frame...
								{
									nDurAdjust = g_data.m_timeconv.DFMStoNDFMS(g_data.m_timeconv.FtoMS(nReplacementDuration-nTrialDuration, 29.97));
								}
								else
								{
									nDurAdjust = g_data.m_timeconv.FtoMS(nReplacementDuration-nTrialDuration, ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0));
								}
							}
							else  // system time
							{
								if(g_settings.m_bDF) // drop frame...
								{
									nDurAdjust = g_data.m_timeconv.DFMStoNDFMS(g_data.m_timeconv.FtoMS(nReplacementDuration-nTrialDuration, 29.97));
								}
								else
								{
									nDurAdjust = g_data.m_timeconv.FtoMS(nReplacementDuration-nTrialDuration, g_settings.m_nFrameRate);
								}
							}

							ppRRules[nNumEntries-1]->m_nOutputDuration = ppRRules[nNumEntries-1]->m_nDuration + nDurAdjust;

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Duration of last item %d [%s] was %d ms, extended by %d ms, now %d ms",
	 nNumEntries-1,
	 ppRRules[nNumEntries-1]->m_pszReplacementID?ppRRules[nNumEntries-1]->m_pszReplacementID:"(null)",
	 ppRRules[nNumEntries-1]->m_nDuration, nDurAdjust, ppRRules[nNumEntries-1]->m_nOutputDuration
	 );
						}
						else
						{

		/*
				 e. If the Trial Total Replacement Duration is greater than the Required Replacement Duration, repeat the following steps 
						until the Trial Total Replacement Duration is equal to the Required Replacement Duration:
							i. Set the Offset of the first entry in the Effective Replacement Rules List to the difference between the Trial Total Replacement Duration and the Required Replacement Duration.
							ii. If the Offset of the first entry in the Effective Replacement Rules List is greater than or equal to the Replacement Duration of the list entry, delete the entry from the list.
							iii. Recompute the Trial Total Replacement Duration.
		*/
if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Trial duration %d was not less than Replacement Duration %d - iterating...", nTrialDuration,nReplacementDuration);


							while((nTrialDuration!=nReplacementDuration)&&(nNumEntries>0))
							{

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Trial duration %d, Replacement Duration %d, NumEntries %d", nTrialDuration, nReplacementDuration, nNumEntries);

								ix=0;

		 //         i. Set the Offset of the first entry in the Effective Replacement Rules List to 
						//     the difference between the Trial Total Replacement Duration and the Required Replacement Duration.

								if(ppRRules[ix])
								{
									ppRRules[ix]->m_nOffset = (nTrialDuration - nReplacementDuration);

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Replacement index %d, Offset %d, id [%s]", ix, ppRRules[ix]->m_nOffset, ppRRules[ix]->m_pszReplacementID?ppRRules[ix]->m_pszReplacementID:"(null)");

		 //         ii. If the Offset of the first entry in the Effective Replacement Rules List is greater than or equal to 
						//     the Replacement Duration of the list entry, delete the entry from the list.

									if(ppRRules[ix]->m_nOffset >= ppRRules[ix]->m_nDurationInFrames)
									{

										while(ix < nNumEntries-1)
										{
											ppRRules[ix] = ppRRules[ix+1];
											ix++;
										}
										ppRRules[ix] = NULL;
										nNumEntries--;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "   --- deleted entry, number of entries now %d", nNumEntries);
									}
								}
							// iii. Recompute the Trial Total Replacement Duration.
//				 c. Compute the Trial Total Replacement Duration, which shall be the sum of the Replacement Durations of the entries in the Effective Replacement Rules List, 
//						less the sum of the Offsets of the entries in the Effective Replacement Rules List.

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Recomputing trial total replacement duration");

								nTrialDuration = 0;
								ix=0;
								while(ix < nNumEntries)
								{
									if(ppRRules[ix])
									{
										nTrialDuration += ppRRules[ix]->m_nDurationInFrames - ppRRules[ix]->m_nOffset; 
if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "adding %d to trial total replacement duration, now %d", ppRRules[ix]->m_nDurationInFrames - ppRRules[ix]->m_nOffset, nTrialDuration);

									}
									ix++;
								}

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Recomputed trial total replacement duration: %d", nTrialDuration);

							}
						}

		//   f. If there are no entries remaining in the Effective Replacement Rules List, stop processing of this Automation Event.

						if((nNumEntries>0)&&(ppRRules))
						{

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "RR %d entries to be processed", nNumEntries);

		/*
						 g. For each entry in the Effective Replacement Rules List:
						 {
									i. Generate and insert a Replace Content Message, populated as follows:
										 - ReplProgramID: The Replacement Program ID in the list entry.
										 - ReplLength: The Replacement Duration in the list entry.
										 - Offset: The Offset in the list entry.

																// the Replace Content tabulator event.
																//  Replace Content Message message

									ii. Add to the Actual Total Replacement Duration the Replacement Duration in the list entry.
						 }






		NTC has identified a substantive error in the Automation Event processing procedure in the Functional Requirements document, version 1.5.2.

		Step 6.h (page 23, lines 13-14) states, "Start the Event Masking Timer with the Actual Total Replacement Duration."  To correct the error, 
		this step is revised to state instead, "Start the Event Masking Timer with the Required Replacement Duration."

		This change makes the duration used to set the Event Masking Timer (in step 6.h) the same as the total length of the replaced portion of the 
		Program Stream (computed according to the Stream Specification, version 1.3, page 9, line 30 through page 10, line 9).

		As a result of this change, the Actual Total Replacement Duration computed in Step 6.g.ii (page 23, lines 11-12) is no longer used.  This 
		step should be deleted, and step 6.g.i (page 23, lines 4-10) should be retained and re-numbered as step 6.g.



		*/


							pOverrides->m_ulActualTimerDurationMS = 0;
							int i=0;
							int nCount=0;
							while(i<nNumEntries)
							{
								if(ppRRules[i])
								{

									if((ppRRules[i]->m_pszReplacementID)&&(strlen(ppRRules[i]->m_pszReplacementID)>0))
									{
		//								pOverrides->m_ulActualTimerDurationMS += g_data.m_timeconv.NDFMStoDFMS( ppRRules[i]->m_nDuration ); // removed due to "substantive error"

										pOverrides->m_ulRepl_EventDur = ppRRules[i]->m_nOutputDuration;		// IDEaL is correct					// duration for ad replacement, length for content replacement 00:00:00;00 if not available.

//										pOverrides->m_ulRepl_EventStart = ppRRules[i]->m_nOffset;		// IDEaL is correct	

										if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
										{
				//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp

											if(g_data.m_ucTimeCodeBits&0x80) // drop frame...
											{
												pOverrides->m_ulRepl_EventStart = g_data.m_timeconv.DFMStoNDFMS(  g_data.m_timeconv.FtoMS(ppRRules[i]->m_nOffset, 29.97) );
											}
											else
											{
												pOverrides->m_ulRepl_EventStart = g_data.m_timeconv.FtoMS(ppRRules[i]->m_nOffset, ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0));
											}
										}
										else  // system time
										{
											if(g_settings.m_bDF) // drop frame...
											{
												pOverrides->m_ulRepl_EventStart = g_data.m_timeconv.DFMStoNDFMS(  g_data.m_timeconv.FtoMS(ppRRules[i]->m_nOffset, 29.97) );
											}
											else
											{
												pOverrides->m_ulRepl_EventStart = g_data.m_timeconv.FtoMS(ppRRules[i]->m_nOffset, g_settings.m_nFrameRate);
											}
										}

										memset(pOverrides->m_pszRepl_ProgramID, 0, 17);
										strcpy(pOverrides->m_pszRepl_ProgramID,  ppRRules[i]->m_pszReplacementID	);	

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "RR%02d : %d %d", i, pOverrides->m_ulRepl_EventDur, pOverrides->m_ulRepl_EventStart);


										if(m_pReplaceContentEvents)
										{
											n= PlayEvent(&m_pReplaceContentEvents, pOverrides, errorstring);
										}
										else
										{
											strcpy(errorstring, "Replace Content events did not exist");
										}
										if(n<CLIENT_SUCCESS)
										{
								if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szSource, "Error (Replace Content): ID %s: %s", ppRRules[i]->m_pszReplacementID, errorstring ); // Sleep(250); //(Dispatch message)
			g_data.SendMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, "Error (Replace Content): ID %s: %s", ppRRules[i]->m_pszReplacementID, errorstring);
										}

										nCount++;
									}
									else
									{
			g_data.SendMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, "A replacement was discarded because the ID was blank.");
									}

								}
								i++;
							}

//							pOverrides->m_ulActualTimerDurationMS = g_data.m_timeconv.NDFMStoDFMS(nReplacementDuration); // added due to "substantive error"
							if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
							{
//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp

								if(g_data.m_ucTimeCodeBits&0x80) // drop frame...
								{
									pOverrides->m_ulActualTimerDurationMS = g_data.m_timeconv.FtoMS(nReplacementDuration, 29.97);
								}
								else
								{
									pOverrides->m_ulActualTimerDurationMS = g_data.m_timeconv.FtoMS(nReplacementDuration, ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0));
								}
							}
							else  // system time
							{
								if(g_settings.m_bDF) // drop frame...
								{
									pOverrides->m_ulActualTimerDurationMS = g_data.m_timeconv.FtoMS(nReplacementDuration, 29.97);
								}
								else
								{
									pOverrides->m_ulActualTimerDurationMS = g_data.m_timeconv.FtoMS(nReplacementDuration, g_settings.m_nFrameRate);
								}
							}


			LeaveCriticalSection(&g_data.m_critTableData);

		/*
				 h. Start the Event Masking Timer with the Actual Total Replacement Duration.
				 i. Extinguish Button 2 ("PASS") on the Basic Operator Panel.
				 j. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.
				 k. Close the Downstream Keyer Control GPO.
				 l. Stop processing of this Automation Event.


														//  BLOCK GPO w Timer message
														//  Start Timer1
														//  SET USP GPO 3 ON
														//  SET USP GPO 3 OFF "un-press" it - no effect

		*/
							if(nCount>0)
							{
							// set the timer duration
								n= PlayEvent(&m_pTimerEvents, pOverrides, errorstring);
								if(n<CLIENT_SUCCESS)
								{
						if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szSource, "Error (Timer): %s", errorstring ); // Sleep(250); //(Dispatch message)
								}

							}
							else
							{
								n= nCount;  // just return success
							}
							
						}
						else
						{

			LeaveCriticalSection(&g_data.m_critTableData);


							// processed OK, but nothing left.
							// log it.

			g_data.SendMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, "Content replacement produced no valid effective replacements.");
if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_REPLRULES)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Content replacement produced no valid effective replacements.");
							n=0;

						}

						delete pOverrides;
/*
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_STATE)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:Replace", "leaving 2 m_critOutputVariables"); // Sleep(250); //(Dispatch message)
}
		LeaveCriticalSection(&g_data.m_critOutputVariables);
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_STATE)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:Replace", "left 2 m_critOutputVariables"); // Sleep(250); //(Dispatch message)
}
*/
						return n;

					}
					else
					{
		LeaveCriticalSection(&g_data.m_critOutputVariables);

							if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szSource, "Error: override value object could not be allocated." ); // Sleep(250); //(Dispatch message)
					}

				}
				else
				{

			LeaveCriticalSection(&g_data.m_critTableData);
		LeaveCriticalSection(&g_data.m_critOutputVariables);

		/*

		7. In Phase Two only, if the primary event, evaluated using the Network Source Identifiers, is associated with a network source:
				 a. If the Network Replacement Operations List (updated as Stream Message Events are processed) indicates that a network-managed ad replacement operation, content replacement operation, or indefinite replacement operation is in progress:
							i. Generate and insert Stream Messages as required to carry out this replacement operation, provided that the effective duration of a network-managed ad replacement operation, other than an indefinite replacement operation, shall be limited to the starting time address of the Next Long-Form Program less the starting time address of the current primary event.
							ii. If the network-managed replacement operation is an ad replacement operation or content replacement operation, start the Event Masking Timer with the effective duration of the replacement operation.
							iii. Extinguish Button 2 ("PASS") on the Basic Operator Panel.
							iv. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.
							v. Close the Downstream Keyer Control GPO.
				 b. Otherwise:
							i. Stop the Event Masking Timer.
							ii. Illuminate Button 2 ("PASS") on the Basic Operator Panel.
							iii. Extinguish Button 3 ("BLOCK") on the Basic Operator Panel.
							iv. Open the Downstream Keyer Control GPO.
							v. Generate and insert a Rejoin Main Message, thereby returning the TV+ audience to the Program Stream.
				 c. Stop processing of this Automation Event.



			
		Otherwise:
					a. Stop the Event Masking Timer.
					b. Illuminate Button 2 ("PASS") on the Basic Operator Panel.		// NOT handled by the GTP 32 setup
					c. Extinguish Button 3 ("BLOCK") on the Basic Operator Panel.		// NOT handled by the GTP 32 setup
					d. Open the Downstream Keyer Control GPO.												// NOT handled by the GTP 32 setup
					e. Generate and insert a Rejoin Main Message, thereby returning the TV+ audience to the Program Stream.
		*/

														// This means, send the automation DLL a command of type AUTO and have it do all this logic
														// in the first instance it does the big complicated thing.
														// in the otherwise, it does the same thing as it does for PASS
														// so these tabluator events needed.

														// the AUTO tabulator events:

														// the Replace Ad tabulator event.
														//  Replace Ad Message message

														// the Replace Content tabulator event.
														//  Replace Content Message message

														//  BLOCK GPO w Timer message
														//  Start Timer1
														//  SET USP GPO 3 ON
														//  SET USP GPO 3 OFF "un-press" it - no effect



														// the otherwise event:

														// the PASS tabulator event:
														//  Stop Timer1
														//  SET USP GPO 2 ON
														//  Rejoin Main message
														//  SET USP GPO 2 OFF "un-press" it - no effect

		// just do the otherwise event for now.


					return Enter_Pass(bCurrent, bTimer);  // I think I can just do this!
				}
			}
		}
		else
		{
	LeaveCriticalSection(&g_data.m_critOutputVariables);
			if(nTimer == AUTODATA_TIMER_PENDING)
			{
g_data.SendMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, "An event was discontinued because a timer action was pending");
			}
			else
			{
			// log that manual event cannot proceed because timer is counting.
			//TODO
g_data.SendMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, "An event was discontinued because the timer was counting with % ms left.", nTimer);
			}
		}

	} // checked the timer
	return TABULATOR_ERROR;
}


int CAutomationDataData::CheckState()
{
//#define AUTODATA_STATE_AUTO							0x01
//#define AUTODATA_STATE_PASS							0x02
//#define AUTODATA_STATE_BLOCK						0x03
//#define AUTODATA_STATE_ERROR						0x10  // ORABLE....  // have to decide what to do in case of...

// select case when (select case when ((status & 1) = 1) then 1 else 0 end as AutoStatus from tabulator.dbo.Buttons where button_type = 'auto') = 1 then 'AUTO' 
// else case when (select case when ((status & 1) = 1) then 1 else 0 end as PassStatus from tabulator.dbo.Buttons where button_type = 'pass') = 1 then 'PASS' 
// else case when (select case when ((status & 1) = 1) then 1 else 0 end as BlockStatus from tabulator.dbo.Buttons where button_type = 'block') = 1 then 'BLOCK'
// else 'ERROR' end end end as GlobalStatus;   

	if((m_pdbOutput)&&(m_pdbOutputConn))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		// have to encode quotes.
//		char* pchEncodedValue   = m_pdbOutput->EncodeQuotes(g_settings.m_pszTimerToken);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select case when (select case when ((status & 1) = 1) then 1 else 0 end as AutoStatus \
from %s where button_type = 'auto') = 1 then 'AUTO' else case when (select case when ((status & 1) = 1) then 1 else 0 end as PassStatus \
from %s where button_type = 'pass') = 1 then 'PASS' else case when (select case when ((status & 1) = 1) then 1 else 0 end as BlockStatus \
from %s where button_type = 'block') = 1 then 'BLOCK' else 'ERROR' end end end as GlobalStatus",
						((g_settings.m_pszButtonsTableName)&&(strlen(g_settings.m_pszButtonsTableName)))?g_settings.m_pszButtonsTableName:"Buttons",
						((g_settings.m_pszButtonsTableName)&&(strlen(g_settings.m_pszButtonsTableName)))?g_settings.m_pszButtonsTableName:"Buttons",
						((g_settings.m_pszButtonsTableName)&&(strlen(g_settings.m_pszButtonsTableName)))?g_settings.m_pszButtonsTableName:"Buttons"
				);
		
//		if(pchEncodedValue) free(pchEncodedValue);

if(g_settings.m_ulDebug&(AUTODATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:CheckState", "%s", szSQL); // Sleep(250); //(Dispatch message)
}

		EnterCriticalSection(&m_critOutputSQL);
		CRecordset* prs = m_pdbOutput->Retrieve(m_pdbOutputConn, szSQL, errorstring);
		if(prs)
		{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "Got Destinations");  Sleep(250); //(Dispatch message)
			if(!prs->IsEOF())
			{
				CString szTemp;
				prs->GetFieldValue("GlobalStatus", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_STATE)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:CheckState", "returned %s", szTemp); // Sleep(250); //(Dispatch message)
}

					int nReturn = AUTODATA_STATE_ERROR;

					if(szTemp.CompareNoCase("AUTO")==0) 
					{
						nReturn = AUTODATA_STATE_AUTO;
					}
					else
					if(szTemp.CompareNoCase("PASS")==0) 
					{
						nReturn = AUTODATA_STATE_PASS;
					}
					else
					if(szTemp.CompareNoCase("BLOCK")==0) 
					{
						nReturn = AUTODATA_STATE_BLOCK;
					}
					else

					prs->Close();
					delete prs;

					LeaveCriticalSection(&m_critOutputSQL);

					return nReturn;
				}
			}
			prs->Close();
			delete prs;
		}
		else
		{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "AutomationData:CheckState", "ERROR: %s", errorstring); // Sleep(250); //(Dispatch message)
		}
		LeaveCriticalSection(&m_critOutputSQL);
	}
	return TABULATOR_ERROR;	
  
// this return means the call was not succesful, but a 
//return with AUTODATA_STATE_ERROR means the call was successful but the state was not able to be determined due to other error (like disconnection from the GTP 32).
}


int CAutomationDataData::CheckTimerExpiryAlreadyProcessedAutomationEvent()
{
//	bool m_b_list_is_playing;  // automation list is playing a primary event.

	// if list is not playing we arent going to get an automation event, so we don't have to check it.
	EnterCriticalSection(&g_data.m_critSyncVariables);
	if(
		  (g_data.m_Sync.m_nLastENEventStart >= 0 ) // has to be initialized
		&&(g_data.m_Sync.m_nLastENEventStart == g_data.m_Output.m_n_current_primary_start)
		&&(g_data.m_Sync.m_nLastENEventDur == g_data.m_Output.m_n_current_primary_duration)
		)
	{
		LeaveCriticalSection(&g_data.m_critSyncVariables);
		return AUTODATA_TIMER_EXPIRY_DONE;
	}
	LeaveCriticalSection(&g_data.m_critSyncVariables);
	return AUTODATA_TIMER_EXPIRY_NOT_DONE;
}

int CAutomationDataData::CheckTimer()
{
	if(m_Sync.m_bTimerActionPending) return AUTODATA_TIMER_PENDING;

	if((m_pdbOutput)&&(m_pdbOutputConn))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		// have to encode quotes.
		char* pchEncodedValue   = m_pdbOutput->EncodeQuotes(g_settings.m_pszTimerToken);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select [count] from %s where [name] like '%s'",
						((g_settings.m_pszTimerTable)&&(strlen(g_settings.m_pszTimerTable)))?g_settings.m_pszTimerTable:"Timers",
						(pchEncodedValue?pchEncodedValue:g_settings.m_pszTimerToken)
				);
		
		if(pchEncodedValue) free(pchEncodedValue);
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:CheckTimer", "%s", szSQL); // Sleep(250); //(Dispatch message)
}

		EnterCriticalSection(&m_critOutputSQL);
		CRecordset* prs = m_pdbOutput->Retrieve(m_pdbOutputConn, szSQL, errorstring);
		if(prs)
		{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "Got Destinations");  Sleep(250); //(Dispatch message)
			if(!prs->IsEOF())
			{
				CString szTemp;
				prs->GetFieldValue("count", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_TIMER)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:CheckTimer", "returned %s", szTemp); // Sleep(250); //(Dispatch message)
}

					int nReturn = atoi(szTemp);
					prs->Close();
					delete prs;

					LeaveCriticalSection(&m_critOutputSQL);

					return nReturn;
				}
			}
			prs->Close();
			delete prs;
		}
		else
		{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "AutomationData:CheckTimer", "ERROR: %s", errorstring); // Sleep(250); //(Dispatch message)
		}
		LeaveCriticalSection(&m_critOutputSQL);
	}
	return TABULATOR_ERROR;

}

int CAutomationDataData::IdealAddTimeAndDuration(int nStart, int nDuration)
{

	if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
	{
//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp

		if(g_data.m_ucTimeCodeBits&0x80) // drop frame...
		{
	return m_timeconv.DFMStoNDFMS( m_timeconv.NDFMStoDFMS(nStart) + m_timeconv.NDFMStoDFMS(nDuration) );
		}
		else
		{
	return nStart + nDuration;
		}

	}
	else  // system time
	{
		if(g_settings.m_bDF) // drop frame...
		{
	return m_timeconv.DFMStoNDFMS( m_timeconv.NDFMStoDFMS(nStart) + m_timeconv.NDFMStoDFMS(nDuration) );
		}
		else
		{
	return nStart + nDuration;
		}
	}

}


int CAutomationDataData::TestPattern(char* pszValue, char* pszPattern)
{
	if((pszValue)&&(pszPattern)&&(strlen(pszPattern)>0)&&(m_pdbOutput)&&(m_pdbOutputConn))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		// have to encode quotes.
		char* pchEncodedValue   = m_pdbOutput->EncodeQuotes(pszValue);
		char* pchEncodedPattern = m_pdbOutput->EncodeQuotes(pszPattern);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select case when '%s' like '%s' then 'YES' else 'NO' end as col1",
						(pchEncodedValue?pchEncodedValue:pszValue),
						(pchEncodedPattern?pchEncodedPattern:pszPattern)
				);
		
		if(pchEncodedValue) free(pchEncodedValue);
		if(pchEncodedPattern) free(pchEncodedPattern);


if(g_settings.m_ulDebug&(AUTODATA_DEBUG_CLASSRULES|AUTODATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:TestPattern", "Checking event %s against %s with %s", pszValue, pszPattern, szSQL); // Sleep(250); //(Dispatch message)
}

		EnterCriticalSection(&m_critOutputSQL);
		CRecordset* prs = m_pdbOutput->Retrieve(m_pdbOutputConn, szSQL, errorstring);
		if(prs)
		{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "Got Destinations");  Sleep(250); //(Dispatch message)
			if(!prs->IsEOF())
			{
				CString szTemp;
				prs->GetFieldValue("col1", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					int nReturn = AUTODATA_PATTERN_NO_MATCH;
					if(szTemp.CompareNoCase("YES")==0)
					{
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_CLASSRULES)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:TestPattern", "Query returned YES"); // Sleep(250); //(Dispatch message)
}

						nReturn = AUTODATA_PATTERN_MATCH;
						
					}
					prs->Close();
					delete prs;

					LeaveCriticalSection(&m_critOutputSQL);
					
					return nReturn;
				}
			}
			prs->Close();
			delete prs;
		}
		else
		{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "AutomationData:TestPattern", "ERROR: %s", errorstring); // Sleep(250); //(Dispatch message)
		}
		LeaveCriticalSection(&m_critOutputSQL);
	}
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_CLASSRULES)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:TestPattern", "ERROR"); // Sleep(250); //(Dispatch message)
}
	return AUTODATA_PATTERN_ERROR;
}




int CAutomationDataData::UpdateEventNotificationObject(CSCTE104MultiMessage* pscte104Msg, char* pszID, char* pszDesc, unsigned long ulStartTC, unsigned long ulDurationMS)
{
	
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_TRIGGER)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:UpdateEventNotificationObject", "[%s][%s] %d %d",
		pszID?pszID:"(null)",
		pszDesc?pszDesc:"(null)",
		ulStartTC, 
		ulDurationMS
		); // Sleep(250); //(Dispatch message)
}

	if((pscte104Msg)&&(pscte104Msg->m_ppscte104Ops)&&(pscte104Msg->m_scte104Hdr.NumberOfOps>0))
	{
		// first update the time and start time code.
		// since this is an event notification, it must be "today".
		// since we cannot get that from time code, we have to get that from the system clock.
		char buffer[12];
		_timeb timestamp;
		_ftime(&timestamp);
		tm* theTime = localtime( &timestamp.time	);
		strftime( buffer, 12, "%Y-%m-%d", theTime );  //ISO 8601 program start date (yyyy-mm-dd)

		memcpy(&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[5], buffer, 10);

		// then, we can put in the event start time code.
		// this was fed to us as an ideal number of milliseconds of the day (not real df compensated)

		if((ulStartTC == AUTODATA_USECURRENT_TIMEADDRESS)||(ulStartTC == AUTODATA_INVALID_TIMEADDRESS))
		{
			// just get "now"
			// get the current TC.
			if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
			{
				unsigned long ulTimeCode;
				EnterCriticalSection(&g_data.m_critTimeCode);
				ulTimeCode = g_data.m_ulTimeCode;
	//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
				LeaveCriticalSection(&g_data.m_critTimeCode);

				ulStartTC = m_timeconv.BCDHMSFtoMS(ulTimeCode, ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)); // make it into ideal ms.

				
if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_TIMECODE)) 
 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "AutomationData:***** **", "TC %d", ulStartTC); // Sleep(250); //(Dispatch message)


			}
			else
			{
				// use system time
				ulStartTC = ((timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))%86400)*1000 + timestamp.millitm;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_TIMECODE)) 
 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "AutomationData:***** **", "TC sys %d", ulStartTC); // Sleep(250); //(Dispatch message)
			}
		}

		if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
		{
			if(g_settings.m_nProgramStreamOffset>0)
			{
				unsigned long ulOffset = ((g_settings.m_nProgramStreamOffset * 1000)/((g_data.m_ucTimeCodeBits&0x02)?25:30));
				if(ulOffset%10)  ulOffset++; // so we don't lose a frame with integer truncation of things like 33.33333

				ulStartTC = g_data.IdealAddTimeAndDuration(ulStartTC, ulOffset);

		// 	ulStartTC += ulOffset; // was this but need IdealAddTimeAndDuration.
			}
/*
			int h,m,s,f;

			m_timeconv.MStoHMSF(ulStartTC, 
				&h, 
				&m, 
				&s, 
				&f, 
				((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0) // using ideal
//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp
				);

if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "AutomationData:***** **", "TC %d int convert %02x%02x%02x%02x", 
															ulStartTC,h,m,s,f
															); // Sleep(250); //(Dispatch message)
*/
			m_timeconv.MStoHMSF(ulStartTC, 
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[15], 
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[16], 
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[17], 
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[18], 
				((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0) // using ideal
//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp
				);
if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_TIMECODE)) 
 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "AutomationData:***** **", "TC %d convert %02x%02x%02x%02x", 
															ulStartTC,
				pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[15], 
				pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[16], 
				pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[17], 
				pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[18] 
															); // Sleep(250); //(Dispatch message)
		}
		else
		{
			if(g_settings.m_nProgramStreamOffset>0)
			{
				unsigned long ulOffset = ((g_settings.m_nProgramStreamOffset * 1000)/((g_data.m_ucTimeCodeBits&0x02)?25:30));
				if(ulOffset%10)  ulOffset++; // so we don't lose a frame with integer truncation of things like 33.33333

				ulStartTC = g_data.IdealAddTimeAndDuration(ulStartTC, ulOffset);

		// 	ulStartTC += ulOffset; // was this but need IdealAddTimeAndDuration.
			}



			m_timeconv.MStoHMSF(ulStartTC,
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[15], 
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[16], 
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[17], 
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[18], 
				(double)g_settings.m_nFrameRate // using ideal
//				(g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate) // this is for df comp
				);
if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_TIMECODE)) 
 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "AutomationData:***** **", "TC %d sys convert %02x%02x%02x%02x", 
															ulStartTC,
				pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[15], 
				pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[16], 
				pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[17], 
				pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[18] 
															); // Sleep(250); //(Dispatch message)
		}

		// this is due to the requirement that time addresses be mod 24.  as well as this:
/*
> please do the modulus arithmetic as though the sequence of time addresses is:
>
>  . . .  23:59:59;28  23:59:59;29  00:00:00;00  00:00:00;01  00:00:00;02  . . .
>
> with no allowances for discontinuities. 	
*/
//		so, no problem there.
		// the code right now, within a minute of midnight, does this.


		if(pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[15]>23)
		{
			// we have wrapped midnight.
			// need to mod out the hours but also incement day
			pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[15]%=24; 

			timestamp.time += 86400;
			theTime = localtime( &timestamp.time);
			strftime( buffer, 12, "%Y-%m-%d", theTime );  //ISO 8601 program start date (yyyy-mm-dd)

			memcpy(&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[5], buffer, 10);
		}

		// then, we can put in the event duration.
		// this was fed to us as an ideal number of milliseconds (not real df compensated)
		if(ulDurationMS == AUTODATA_INVALID_TIMEADDRESS)
		{
			ulDurationMS = 0;
		}

		if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
		{
			m_timeconv.MStoHMSF(ulDurationMS, 
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[19], 
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[20], 
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[21], 
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[22], 
				((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0) // using ideal
//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp
				);
		}
		else
		{
			m_timeconv.
				MStoHMSF(ulDurationMS,
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[19], 
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[20], 
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[21], 
				&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[22], 
				(double)g_settings.m_nFrameRate // using ideal
//				(g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate) // this is for df comp
				);
		}

		memset(&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[23], 0x20, 48); // spaces

		//now do the ID and desc
		if(pszID)
		{
			int n=strlen(pszID);
			if(n>0)
			{
				if(n>16) n=16;
				memcpy(&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[23], pszID, n);
			}
		} 
		
		if(pszDesc)
		{
			int n=strlen(pszDesc);
			if(n>0)
			{
				if(n>32) n=32;
				memcpy(&pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr[39], pszDesc, n);
			}
		} 	

	}
	return TABULATOR_ERROR;
}


////////////////////////////////////////////////////
// NOTE ***** THIS uses real drop frame compensated milliseconds, since we are assmebling actual
// events into a pod duration
int CAutomationDataData::UpdateReplaceAdObject(CSCTE104MultiMessage* pscte104Msg, char* pszID, char* pszDesc, unsigned long ulDurationMS)
{
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_TRIGGER)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:UpdateReplaceAdObject", "[%s][%s] %d",
		pszID?pszID:"(null)",
		pszDesc?pszDesc:"(null)",
		ulDurationMS
		); // Sleep(250); //(Dispatch message)
}
	if((pscte104Msg)&&(pscte104Msg->m_ppscte104Ops)&&(pscte104Msg->m_scte104Hdr.NumberOfOps>1))
	{
		if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
		{
			m_timeconv.MStoHMSF(ulDurationMS, 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[5], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[6], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[7], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[8], 
//				((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0) // using ideal
				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp
				);
		}
		else
		{
			m_timeconv.
				MStoHMSF(ulDurationMS,
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[5], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[6], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[7], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[8], 
//				(double)g_settings.m_nFrameRate // using ideal
				(g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate) // this is for df comp
				);
		}
		memset(&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[9], 0x20, 48); // spaces

		//now do the ID and desc
		if(pszID)
		{
			int n=strlen(pszID);
			if(n>0)
			{
				if(n>16) n=16;
				memcpy(&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[9], pszID, n);
			}
		} 
		
		if(pszDesc)
		{
			int n=strlen(pszDesc);
			if(n>0)
			{
				if(n>32) n=32;
				memcpy(&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[25], pszDesc, n);
			}
		} 

	}
	return TABULATOR_ERROR;
}


int CAutomationDataData::UpdateReplaceContentObject(CSCTE104MultiMessage* pscte104Msg, char* pszID, unsigned long ulOffsetMS, unsigned long ulDurationMS)
{
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_TRIGGER)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:UpdateReplaceContentObject", "[%s] %d %d",
		pszID?pszID:"(null)",
		ulOffsetMS, 
		ulDurationMS
		); // Sleep(250); //(Dispatch message)
}
	if((pscte104Msg)&&(pscte104Msg->m_ppscte104Ops)&&(pscte104Msg->m_scte104Hdr.NumberOfOps>1))
	{

		// first, the "Replacement Length" which is the duration
		if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
		{
			m_timeconv.MStoHMSF(ulDurationMS, 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[5], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[6], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[7], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[8], 
				((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0) // using ideal
//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp
				);
		}
		else
		{
			m_timeconv.MStoHMSF(ulDurationMS,
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[5], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[6], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[7], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[8], 
				(double)g_settings.m_nFrameRate // using ideal
//				(g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate) // this is for df comp
				);
		}

// then the offset
		// this was fed to us as an ideal number of milliseconds (not real df compensated)

		if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
		{
			m_timeconv.MStoHMSF(ulOffsetMS, 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[9], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[10], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[11], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[12], 
				((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0) // using ideal
//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp
				);
		}
		else
		{
			m_timeconv.MStoHMSF(ulOffsetMS,
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[9], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[10], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[11], 
				&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[12], 
				(double)g_settings.m_nFrameRate // using ideal
//				(g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate) // this is for df comp
				);
		}

		memset(&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[13], 0x20, 16); // spaces

		//now do the ID and desc
		if(pszID)
		{
			int n=strlen(pszID);
			if(n>0)
			{
				if(n>16) n=16;
				memcpy(&pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[13], pszID, n);
			}
		} 

	}
	return TABULATOR_ERROR;
}






int CAutomationDataData::RefreshTabulatorEvents( CTabulatorEventArray* pEvents, char* pszToken )
{
	if((pEvents)&&(pszToken))
	{
		EnterCriticalSection(&(pEvents->m_critEvents));

		pEvents->DeleteEvents();
		int e=0;
		int n=0;
		bool bFound=false;
		bool bAllocated=false;
		while(e<g_data.m_pEvents->m_nEvents)
		{
			if(g_data.m_pEvents->m_ppEvents[e]->m_szName.Compare(pszToken)==0)
			{
				bFound=true;
				if(g_data.m_pEvents->m_nLastEventMod!=pEvents->m_nLastEventMod)
				{
					pEvents->m_nLastEventMod = g_data.m_pEvents->m_nLastEventMod;
				}
			}
			else
			{
				if(bFound) break; // just stop it there. - must be contiguous
			}

			if(bFound)
			{
				if(!bAllocated)
				{
					pEvents->m_ppEvents = new CTabulatorEvent*[g_settings.m_nTriggerBuffer];
					bAllocated = true; // check for errors someday
				}
				pEvents->m_ppEvents[pEvents->m_nEvents] = new CTabulatorEvent;
				if(pEvents->m_ppEvents[pEvents->m_nEvents])
				{
					n++;
					*(pEvents->m_ppEvents[pEvents->m_nEvents]) = *(g_data.m_pEvents->m_ppEvents[e]);
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szName = g_data.m_pEvents->m_ppEvents[e]->m_szName;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szHost = g_data.m_pEvents->m_ppEvents[e]->m_szHost;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szSource = g_data.m_pEvents->m_ppEvents[e]->m_szSource;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szScene = g_data.m_pEvents->m_ppEvents[e]->m_szScene;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szIdentifier = g_data.m_pEvents->m_ppEvents[e]->m_szIdentifier;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szValue = g_data.m_pEvents->m_ppEvents[e]->m_szValue;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szParamDependencies = g_data.m_pEvents->m_ppEvents[e]->m_szParamDependencies;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szDestModule = g_data.m_pEvents->m_ppEvents[e]->m_szDestModule;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szLayer = g_data.m_pEvents->m_ppEvents[e]->m_szLayer;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_EVENTS)) 
g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName, "in event %02d: %s, %.3f: %s:%s:%s",
		 pEvents->m_nEvents,
		 pEvents->m_ppEvents[pEvents->m_nEvents]->m_szName,
		 pEvents->m_ppEvents[pEvents->m_nEvents]->m_dblTriggerTime,
		 pEvents->m_ppEvents[pEvents->m_nEvents]->m_szScene,
		 pEvents->m_ppEvents[pEvents->m_nEvents]->m_szIdentifier,
		 pEvents->m_ppEvents[pEvents->m_nEvents]->m_szValue
		 );// Sleep(100); //(Dispatch message)
					pEvents->m_nEvents++;
				}
			}

			e++;
		}
		LeaveCriticalSection(&(pEvents->m_critEvents));


		return n;
	}
	return TABULATOR_ERROR;
}



int CAutomationDataData::InitDLLData(DLLthread_t* pData)
{
	if(pData)
	{
		InitializeCriticalSection(&pData->m_crit);
		EnterCriticalSection(&pData->m_crit);
		pData->bKillThread=true;
		pData->bThreadStarted=false;
		pData->nThreadState=0;  //generic
		pData->nThreadControl=0;  //generic
		pData->m_dblLastUpdate=-1.0;
		pData->pszThreadName=NULL;
		strcpy(pData->pszThreadStateText, "");//256 chars max

	// URLs may use format specifiers
		pData->pszBaseURL=NULL;
		pData->pszInfoURL=NULL;
		pData->pszConfirmURL=NULL;

		pData->pszSeriesParamName=NULL;    //  %S
		pData->pszProgramParamName=NULL;   //  %P
		pData->pszEpisodeParamName=NULL;   //  %E

		pData->pszSeriesID=NULL;    //  %s
		pData->pszProgramID=NULL;   //  %p
		pData->pszEpisodeID=NULL;   //  %e

		// some generic parameters
		pData->pszToDateParamName=NULL;				//  %t
		pData->pszFromDateParamName=NULL;			//  %f
		pData->pszNowParamName=NULL;					//  %n
		pData->pszBatchParamName=NULL;				//  %b
		pData->pszFromIDParamName=NULL;				//  %m  // start id
		pData->pszGetIDParamName=NULL;				//  %i

		pData->pszLastID=NULL;  // last ID gotten   %l

		pData->nBatchParam=250;
		pData->nShortIntervalMS=0;
		pData->nLongIntervalMS=5000;
		pData->bDirParamStyle=true;  // true is baseurl/paramname/paramvalue/  false is baseurl?paramname=paramvalue

		_ftime(&pData->timebTick); // the time check inside the thread
		_ftime(&pData->timebLastPullTick); // the last time data pulled inside the thread
		pData->nIDrateAvg=-1;
		pData->nIDrateMax=-1;
		pData->nIDrateMin=-1;
		pData->nNumFailures=0;
		pData->nNumSuccess=0;

		pData->pszHost=NULL;
		pData->nPort=80;
		pData->nAuxValue=-1;

		LeaveCriticalSection(&pData->m_crit);
		return CLIENT_SUCCESS;
	}

	return CLIENT_ERROR;
}

/*
int CAutomationDataData::GetAutomationData(char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT uid, name, description, type, status, duration, start_tc, stop_tc, \
count, paused_duration, paused_count, last_started, last_stopped, action_type, action_data FROM %s ORDER BY uid",  //HARDCODE
			((g_settings.m_pszAutomationData)&&(strlen(g_settings.m_pszAutomationData)))?g_settings.m_pszAutomationData:"AutomationData"
			); 
		
/*		

create table AutomationData (uid  int identity(1,1), name varchar(64), description varchar(256), type int, status int, duration int, start_tc int, stop_tc int, \
count int, paused_duration int, paused_count int, last_started decimal(20,3), last_stopped decimal(20,3), action_type int, action_data varchar(2048))  
   


	int m_nItemID;

	unsigned long m_ulType;  // up count, down count
	unsigned long m_ulStatus;

	unsigned long m_ulDuration; // if a downcounter, the total number of milliseconds to count down.  If an upcounter, it should be the maximum count, or zero if none.
	unsigned long m_ulStartTC; // if a scheduled autodata, start from this millisecond offset.
	unsigned long m_ulStopTC; // the time code at finish

	unsigned long m_ulCount; // if a downcounter, the total number of milliseconds left.  If an upcounter, elapsed duration
	unsigned long m_ulPausedDuration; // elapsed milliseconds while paused.
	unsigned long m_ulTimesPaused;
	
	_timeb m_timeLastStarted;
	_timeb m_timeLastStopped;

	char* m_pszName;
	char* m_pszDescription;

	bool m_bKillAutomationData;
	bool m_bAutomationDataStarted;

	// need something here for "action" to do when the autodata finishes.
	unsigned long m_ulActionType;  // up count, down count
	char* m_pszActionData;
	* /

if(g_settings.m_ulDebug&(AUTODATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetAutomationData", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "GetAutomationData");  Sleep(250); //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "Got Destinations");  Sleep(250); //(Dispatch message)

			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = CLIENT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetAutomationData", "index %d", nIndex); // Sleep(250); //(Dispatch message)
}
				CString szName="";
				CString szDesc="";
				CString szActionData="";
				CString szTemp;

				int nItemID = -1;
				bool bItemFound = false;
				bool bFlagsFound = false;
				bool bFound = false;


				unsigned long ulType;  // up count, down count
//				unsigned long ulStatus;

//				unsigned long ulDuration; // if a downcounter, the total number of milliseconds to count down.  If an upcounter, it should be the maximum count, or zero if none.
//				unsigned long ulStartTC; // if a scheduled autodata, start from this millisecond offset.
//				unsigned long ulStopTC; // the time code at finish

//				unsigned long ulCount; // if a downcounter, the total number of milliseconds left.  If an upcounter, elapsed duration
//				unsigned long ulPausedDuration; // elapsed milliseconds while paused.
//				unsigned long ulTimesPaused;
				
//				_timeb timeLastStarted;
//				_timeb timeLastStopped;

				unsigned long ulActionType;  // up count, down count

				unsigned long ulFlags = 0; // used for volatile status


				try
				{
					prs->GetFieldValue("uid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bItemFound = true;
						nItemID = atoi(szTemp);
					}
					prs->GetFieldValue("name", szName);//HARDCODE
					szName.TrimLeft(); szName.TrimRight();

					prs->GetFieldValue("description", szDesc);//HARDCODE
					szDesc.TrimLeft(); szDesc.TrimRight();

					prs->GetFieldValue("type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulType = atol(szTemp);
					}

/*

	// these need to be set on the fly
					prs->GetFieldValue("status", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulStatus = atol(szTemp);
					}

					prs->GetFieldValue("duration", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulDuration = atol(szTemp);
					}


					prs->GetFieldValue("start_tc", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulStartTC = atol(szTemp);
					}

					prs->GetFieldValue("stop_tc", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulStopTC = atol(szTemp);
					}
* /

					prs->GetFieldValue("count", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulCount = atol(szTemp);
					}
					prs->GetFieldValue("paused_duration", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulPausedDuration = atol(szTemp);
					}
					prs->GetFieldValue("paused_count", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulTimesPaused = atol(szTemp);
					}
					prs->GetFieldValue("last_started", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						timeLastStarted.time = atol(szTemp);
						timeLastStarted.millitm = ((unsigned short)((atof(szTemp) - ((double)(timeLastStarted.time))) *1000.0));
					}
					prs->GetFieldValue("last_stopped", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						timeLastStopped.time = atol(szTemp);
						timeLastStopped.millitm = ((unsigned short)((atof(szTemp) - ((double)(timeLastStarted.time))) *1000.0));
					}
* /
					prs->GetFieldValue("action_type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulActionType = atol(szTemp);
					}
					prs->GetFieldValue("action_data", szDesc);//HARDCODE
					szDesc.TrimLeft(); szDesc.TrimRight();


					bFlagsFound = true; // made it thru the whole record
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
* /
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetAutomationData", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL); // Sleep(250); //(Dispatch message)
}
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetAutomationData", "Retrieve: Caught exception: out of memory\n%s", szSQL); // Sleep(250); //(Dispatch message)
}
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
if(g_settings.m_ulDebug&(AUTODATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetAutomationData", "Retrieve: Caught other exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)
}
					}
					e->Delete();
				} 
				catch( ... )
				{
					if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception.\n%s", szSQL);

if(g_settings.m_ulDebug&(AUTODATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:GetAutomationData", "Retrieve: Caught exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)
}

				}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "Got Destination: %s (0x%08x)", szName, ulFlags);  Sleep(250); //(Dispatch message)


//////////////////////////////////////////////////////////////////////////////
// temporarily going to just ignore all this, this is only going to be called once at startup and that is it.
// so this will not get hit


/*
				if(0) //(m_ppAutomationData)&&(m_nAutomationData))
				{
					int nTemp=0;
					while(nTemp<m_nAutomationData)
					{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "c2");  Sleep(250); //(Dispatch message)
						if((m_ppAutomationData[nTemp]))//&&(m_ppAutomationData[nTemp]->m_pszName))
						{
							// changing the follwing to unique on dest id.
				//			if((szName.GetLength()>0)&&(szName.CompareNoCase(m_ppAutomationData[nTemp]->m_pszName)==0))
							if(m_ppAutomationData[nTemp]->m_nItemID == nItemID)
							{
								bFound = true;
								bool bRefresh = false;

								if(
										(szDesc.GetLength()>0)
									&&((m_ppAutomationData[nTemp]->m_pszDesc==NULL)||(szDesc.CompareNoCase(m_ppAutomationData[nTemp]->m_pszDesc)))
									)
								{
									if(m_ppAutomationData[nTemp]->m_pszDesc) free(m_ppAutomationData[nTemp]->m_pszDesc);

									m_ppAutomationData[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(m_ppAutomationData[nTemp]->m_pszDesc) sprintf(m_ppAutomationData[nTemp]->m_pszDesc, szDesc);
								}

								if(
										(szName.GetLength()>0)
									&&((m_ppAutomationData[nTemp]->m_pszName==NULL)||(szName.CompareNoCase(m_ppAutomationData[nTemp]->m_pszName)))
									)
								{
									if(m_ppAutomationData[nTemp]->m_pszName) free(m_ppAutomationData[nTemp]->m_pszName);

									m_ppAutomationData[nTemp]->m_pszName = (char*)malloc(szName.GetLength()+1); 
									if(m_ppAutomationData[nTemp]->m_pszName) sprintf(m_ppAutomationData[nTemp]->m_pszName, szName);
								}

								if(bItemFound) m_ppAutomationData[nTemp]->m_nItemID = nItemID;
								m_ppAutomationData[nTemp]->m_ulType = ulType;

								if(dblDiskspaceThreshold>0.0) m_ppAutomationData[nTemp]->m_dblDiskPercent = dblDiskspaceThreshold;
//								m_ppAutomationData[nTemp]->m_dblDiskKBFree = dblDiskspaceFree;
//								m_ppAutomationData[nTemp]->m_dblDiskKBTotal = dblDiskspaceTotal;

//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "c3");  Sleep(250); //(Dispatch message)

								if(
										((bFlagsFound)&&(!(ulFlags&CLIENT_FLAG_ENABLED))&&((m_ppAutomationData[nTemp]->m_ulFlags)&CLIENT_FLAG_ENABLED))
									)
								{
									m_ppAutomationData[nTemp]->m_bKillMonThread = true;

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
										m_ppAutomationData[nTemp]->m_pszDesc?m_ppAutomationData[nTemp]->m_pszDesc:m_ppAutomationData[nTemp]->m_pszName);  
									g_ptabmsgr->DM(MSG_ICONINFO, NULL, "AutomationData:destination_change", errorstring);  Sleep(20);  //(Dispatch message)
									m_ppAutomationData[nTemp]->m_bKillConnThread = true;
									m_ppAutomationData[nTemp]->m_ulStatus = CLIENT_STATUS_NOTCON;
									if(m_ppAutomationData[nTemp]->m_socket != NULL)
									{
										m_ppAutomationData[nTemp]->m_net.CloseConnection(m_ppAutomationData[nTemp]->m_socket); // re-establish
									}
									m_ppAutomationData[nTemp]->m_socket = NULL;

								}

								if(
										((bFlagsFound)&&(ulFlags&CLIENT_FLAG_ENABLED)&&(!((m_ppAutomationData[nTemp]->m_ulFlags)&CLIENT_FLAG_ENABLED)))
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating %s...", 
//										m_ppAutomationData[nTemp]->m_pszClientName?m_ppAutomationData[nTemp]->m_pszClientName:"Libretto",  
										m_ppAutomationData[nTemp]->m_pszDesc?m_ppAutomationData[nTemp]->m_pszDesc:m_ppAutomationData[nTemp]->m_pszName);  
									g_ptabmsgr->DM(MSG_ICONINFO, NULL, "AutomationData:destination_change", errorstring); //  Sleep(20);  //(Dispatch message)
//									m_ppAutomationData[nTemp]->m_pDlg->OnButtonConnect();
									//**** if connect set following status


									if((m_ppAutomationData[nTemp]->m_socket == NULL)&&(!m_ppAutomationData[nTemp]->m_bConnThreadStarted))
									{
										m_ppAutomationData[nTemp]->m_ulStatus = CLIENT_STATUS_CONN;
										if(_beginthread(LibrettoAsynchConnThread, 0, (void*)(m_ppAutomationData[nTemp]))==-1)
										{
										//error.

										//**MSG
										}
										else
										{
											int nWait = 0;
											while((m_ppAutomationData[nTemp]->m_ulStatus == CLIENT_STATUS_CONN)&&(nWait<1000))  // 0.5 seconds?
											{
												nWait++;
												Sleep(1);
											}
										}
									}

									if(!m_ppAutomationData[nTemp]->m_bMonThreadStarted)
									{
										m_ppAutomationData[nTemp]->m_bKillMonThread = false;
										if(_beginthread(LibrettoConnMonitorThread, 0, (void*)(m_ppAutomationData[nTemp]))==-1)
										{
										//error.
									g_ptabmsgr->DM(MSG_ICONERROR, NULL, "AutomationData:destination_change", "Error starting connection monitor for %s", 
										m_ppAutomationData[nTemp]->m_pszDesc?m_ppAutomationData[nTemp]->m_pszDesc:m_ppAutomationData[nTemp]->m_pszName); //  Sleep(20);  //(Dispatch message)

										//**MSG
										}
									}


									if(!m_ppAutomationData[nTemp]->m_bCommandQueueThreadStarted)
									{
										m_ppAutomationData[nTemp]->m_bKillCommandQueueThread = false;
										if(_beginthread(LibrettoCommandQueueThread, 0, (void*)(m_ppAutomationData[nTemp]))==-1)
										{
										//error.
									g_ptabmsgr->DM(MSG_ICONERROR, NULL, "AutomationData:destination_change", "Error starting command queue for %s", 
										m_ppAutomationData[nTemp]->m_pszDesc?m_ppAutomationData[nTemp]->m_pszDesc:m_ppAutomationData[nTemp]->m_pszName); //  Sleep(20);  //(Dispatch message)

										//**MSG
										}
									}


									

/*
									if(m_ppAutomationData[nTemp]->m_socket == NULL)
									{
										m_ppAutomationData[nTemp]->m_net.OpenConnection(m_ppAutomationData[nTemp]->m_pszName, g_settings.m_nPort, &m_ppAutomationData[nTemp]->m_socket); // re-establish
										char tnbuffer[32]; sprintf(tnbuffer, "\\\\%c%c", 13,10);
										m_ppAutomationData[nTemp]->SendTelnetCommand(tnbuffer, 2);
										unsigned char* pucBuffer = NULL;
										unsigned long ulBufferLen = 256;
										m_ppAutomationData[nTemp]->m_net.GetLine(&pucBuffer, &ulBufferLen, m_ppAutomationData[nTemp]->m_socket);
										if(pucBuffer) free(pucBuffer);
									}
* /
									bRefresh = true;
//									m_ppAutomationData[nTemp]->m_bKillConnThread = false;
	//								if(_beginthread(LibrettoConnectionThread, 0, (void*)m_ppAutomationData[nTemp])<0)
	//								{
										//error.

										//**MSG
							
										
		//							}
									//**** should check return value....

									m_ppAutomationData[nTemp]->m_ulStatus = CLIENT_STATUS_CONN;
								}

								if(bFlagsFound) m_ppAutomationData[nTemp]->m_ulFlags = ulFlags|CLIENT_FLAG_FOUND;
								else m_ppAutomationData[nTemp]->m_ulFlags |= CLIENT_FLAG_FOUND;

								if(bRefresh)m_ppAutomationData[nTemp]->m_ulFlags |= CLIENT_FLAG_REFRESH;

							}
						}
						nTemp++;
					}
				}
// temporarily going to just ignore all this, this is only going to be called once at startup and that is it.
// so this (above) will not get hit
//////////////////////////////////////////////////////////////////////////////

* /



				if((!bFound)&&(szName.GetLength()>0)) // have to add.
				{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "adding %s", szName);  Sleep(250); //(Dispatch message)
					CAutomationDataItem* pscno = new CAutomationDataItem;
					if(pscno)
					{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "new obj for %s", szName);  Sleep(250); //(Dispatch message)
						CAutomationDataItem** ppObj = new CAutomationDataItem*[m_nAutomationData+1];
						if(ppObj)
						{
				
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "new array for %s", szName);  Sleep(250); //(Dispatch message)
							int o=0;
							if((m_ppAutomationData)&&(m_nAutomationData>0))
							{
								while(o<m_nAutomationData)
								{
									ppObj[o] = m_ppAutomationData[o];
									o++;
								}
								delete [] m_ppAutomationData;

							}
							ppObj[o] = pscno;
							m_ppAutomationData = ppObj;
							m_nAutomationData++;

							ppObj[o]->m_pszName = (char*)malloc(szName.GetLength()+1); 
							if(ppObj[o]->m_pszName) sprintf(ppObj[o]->m_pszName, "%s", szName);

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}
							if(bItemFound) ppObj[o]->m_nItemID = nItemID;
							ppObj[o]->m_ulType = ulType;

							if(szActionData.GetLength()>0)
							{
								ppObj[o]->m_pszActionData = (char*)malloc(szActionData.GetLength()+1); 
								if(ppObj[o]->m_pszActionData) sprintf(ppObj[o]->m_pszActionData, "%s", szActionData);
							}
							ppObj[o]->m_ulActionType = ulActionType;



//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", " added flags: %s (0x%08x) %d", szName, ppObj[o]->m_ulFlags, ((ppObj[o]->m_ulFlags)&CLIENT_FLAG_ENABLED));  Sleep(250); //(Dispatch message)


							ppObj[o]->m_ulFlags |= AUTODATA_FLAG_FOUND;
						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			// and insert media list refresh into the queue for all new destinatons.
			nIndex = 0;
			while(nIndex<m_nAutomationData)
			{
				if((m_ppAutomationData)&&(m_ppAutomationData[nIndex]))
				{

					if((m_ppAutomationData[nIndex]->m_ulFlags)&AUTODATA_FLAG_FOUND)
					{
						(m_ppAutomationData[nIndex]->m_ulFlags) &= ~AUTODATA_FLAG_FOUND;

						nIndex++;
					}
					else
					{
						if(m_ppAutomationData[nIndex]->m_bAutomationDataStarted)
						{
							int nWait =  clock()+1500;
							m_ppAutomationData[nIndex]->m_bKillAutomationData = true;

							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
									m_ppAutomationData[nIndex]->m_pszDesc?m_ppAutomationData[nIndex]->m_pszDesc:m_ppAutomationData[nIndex]->m_pszName);  
								if(g_ptabmsgr)  g_ptabmsgr->DM(MSG_ICONINFO, NULL, "AutomationData:remove_autodata", errorstring);  // Sleep(20);  //(Dispatch message)
//									m_ppAutomationData[nTemp]->m_pDlg->OnDisconnect();


							//**** disconnect
//								while(m_ppAutomationData[nIndex]->m_bConnThreadStarted) Sleep(1);

							while(
										 (
										   (m_ppAutomationData[nIndex]->m_bAutomationDataStarted)
										 )
										&&
										 (clock()<nWait)
									 )
							{
								Sleep(1);
							}

							m_nAutomationData--;

							int nTemp=nIndex;
							while(nTemp<m_nAutomationData)
							{
								m_ppAutomationData[nTemp]=m_ppAutomationData[nTemp+1];
								nTemp++;
							}
							m_ppAutomationData[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
	}
//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "AutomationData:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return CLIENT_ERROR;
}


int CAutomationDataData::FindAutomationData(char* pszAutomationDataName)
{
	if((pszAutomationDataName)&&(m_ppAutomationData))
	{
		int nTemp=0;
		while(nTemp<m_nAutomationData)
		{
			if(m_ppAutomationData[nTemp])
			{
				if(m_ppAutomationData[nTemp]->m_pszName)
				{
					if(stricmp(m_ppAutomationData[nTemp]->m_pszName, pszAutomationDataName)==0) return nTemp;
				}
			}
			nTemp++;
		}
	}
	return CLIENT_ERROR;

}
*/


// the way we are doing this, we have to limit the tabulator events to having max of only ONE
// stream message per event.  Because there is one override being sent in....
// at least for now, this is how it needs to be.
int CAutomationDataData::PlayEvent(CTabulatorEventArray** ppEvent, CMessageOverrideValues* pOverrides, char* pszInfo)
{
	CTabulatorEventArray* pEvent = NULL;
	char szTickerSource[MAX_PATH]; sprintf(szTickerSource, "%s_Output", g_settings.m_pszInternalAppName);
	if(ppEvent) pEvent = *ppEvent;
	if((m_pdbOutput)&&(m_pdbOutputConn)&&(pEvent)&&(pEvent->m_ppEvents)&&(pEvent->m_nEvents>0))
	{
		char szCommandSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];
		char TabulatorToken[MAX_PATH];
		int nTabulatorTokenLen=0;
		if((g_settings.m_pszTabulatorModule)&&(strlen(g_settings.m_pszTabulatorModule)))
		{
			_snprintf(TabulatorToken, MAX_PATH, "%s:", g_settings.m_pszTabulatorModule);
		}
		else
		{
			strcpy(TabulatorToken, "Tabulator:");
		}	
		
		nTabulatorTokenLen = strlen(TabulatorToken);
	
		int i=0;
		_timeb timebBeginTick;
		_timeb timebTriggerTick;
		_timeb timebNowTick;
		EnterCriticalSection(&pEvent->m_critEvents);
		_ftime(&timebBeginTick);
		CBufferUtil bu;
//			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "PlayEvent event"); //(Dispatch message)

		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "PlayEvent event %s with %d actions", pEvent->m_ppEvents[0]->m_szScene, pEvent->m_nEvents); //(Dispatch message)

		while((i<pEvent->m_nEvents)&&(g_dlldata.thread)&&(g_dlldata.thread[0])&&(!g_dlldata.thread[0]->bKillThread))
		{
			if((pEvent->m_ppEvents)&&(pEvent->m_ppEvents[i]))
			{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "-X- text event %02d %s %.0f", i, pEvent->m_ppEvents[i]->m_szIdentifier, pEvent->m_ppEvents[i]->m_dblTriggerTime); //(Dispatch message)
				// delay by the offest amount;
				timebTriggerTick.time = timebBeginTick.time+((int)(pEvent->m_ppEvents[i]->m_dblTriggerTime))/1000;
				timebTriggerTick.millitm = (unsigned short)(timebBeginTick.millitm + ((int)(pEvent->m_ppEvents[i]->m_dblTriggerTime))%1000);
				while(timebTriggerTick.millitm>999)
				{
					timebTriggerTick.time++;
					timebTriggerTick.millitm-=1000;
				}
				_ftime(&timebNowTick);

				// assemble the string to send:

				if(pEvent->m_ppEvents[i]->m_szIdentifier.GetLength())
				{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "--- event %02d %s with %s", i, pEvent->m_ppEvents[i]->m_szIdentifier, pEvent->m_ppEvents[i]->m_szValue); //(Dispatch message)

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "pchEncodedLocal: %s", pchEncodedLocal);  Sleep(50);//(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "want to send %s", proj);  Sleep(50);//(Dispatch message)

					CString szII = "";
					CString szReadableII = "";
					bool bTabulatorEvent = false;
					if(pEvent->m_ppEvents[i]->m_nType==0)  //anim
					{


//FIX HARDCODE
#pragma message(alert "fix this hardcode")

						if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Action_CommercialMulti")==0)
						{
							if(
								  (pOverrides) // calculated outside
								&&(m_pscte104MsgReplaceAd)
//								&&(m_pscte104MsgReplaceAd->m_scte104Hdr.NumberOfOps>1)
//								&&(m_pscte104MsgReplaceAd->m_ppscte104Ops)
//								&&(m_pscte104MsgReplaceAd->m_ppscte104Ops[0])
//								&&(m_pscte104MsgReplaceAd->m_ppscte104Ops[1])
								)
							{
								// fill the proprietary data of the event notification part first
								// the time values must ALWAYS be overridden!
								UpdateEventNotificationObject(m_pscte104MsgReplaceAd, 
									(strlen(pOverrides->m_pszEN_ProgramID)>0?pOverrides->m_pszEN_ProgramID:NULL),
									(strlen(pOverrides->m_pszEN_ProgramDescription)>0?pOverrides->m_pszEN_ProgramDescription:NULL),
									pOverrides->m_ulEN_EventStart, 
									pOverrides->m_ulEN_EventDur
									);										
							
							// then fill the proprietary data of the replace ad part
// NOTE ***** THIS uses real drop frame compensated milliseconds, since we are assmebling actual
// events into a pod duration
								UpdateReplaceAdObject(m_pscte104MsgReplaceAd, 
									(strlen(pOverrides->m_pszRepl_ProgramID)>0?pOverrides->m_pszRepl_ProgramID:NULL),
									(strlen(pOverrides->m_pszRepl_ProgramDescription)>0?pOverrides->m_pszRepl_ProgramDescription:NULL),
									pOverrides->m_ulRepl_EventDur
									);
								
								unsigned char* pucOut = m_scte104.ReturnMessageBuffer(m_pscte104MsgReplaceAd);
								if(pucOut)
								{
									// encode it for Libretto
									unsigned long ulLen = (*(pucOut+2))&0x000000ff;
				//Q.Format("buflen %d", ulLen);
				//AfxMessageBox(Q);
									ulLen <<= 8; ulLen += (*(pucOut+3))&0x000000ff;
									char* pchBuffer = (char*)pucOut;


									if(g_settings.m_bReadableEncodedAsRun)
									{
										unsigned long ulBufLen = ulLen;
										char* pchHexBuffer = m_bu.ReadableHex((char*)pucOut, &ulBufLen, MODE_FULLHEX);
										if(pchHexBuffer)
										{
											szReadableII = pchHexBuffer;
											free(pchHexBuffer);
										}
									}

									int nEncodeReturn = m_bu.Base64Encode(&pchBuffer, &ulLen, false);
									if(nEncodeReturn>=RETURN_SUCCESS)
									{
										if(pchBuffer)
										{
											szII.Format("%s", pchBuffer);
											free(pchBuffer);
										}
									}

									delete [] pucOut; // yes, new was used
								}
							}
						}
						else



						if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Action_Program")==0)
						{

//FIX HARDCODE
#pragma message(alert "fix this hardcode")

							if(
								  (pOverrides) // calculated outside
								&&(m_pscte104MsgReplaceContent)
//								&&(m_pscte104MsgReplaceContent->m_scte104Hdr.NumberOfOps>1)
//								&&(m_pscte104MsgReplaceContent->m_ppscte104Ops)
//								&&(m_pscte104MsgReplaceContent->m_ppscte104Ops[0])
//								&&(m_pscte104MsgReplaceContent->m_ppscte104Ops[1])
								)
							{
								// fill the proprietary data of the event notification part first
								// the time values must ALWAYS be overridden!
								UpdateEventNotificationObject(m_pscte104MsgReplaceContent, 
									(strlen(pOverrides->m_pszEN_ProgramID)>0?pOverrides->m_pszEN_ProgramID:NULL),
									(strlen(pOverrides->m_pszEN_ProgramDescription)>0?pOverrides->m_pszEN_ProgramDescription:NULL),
									pOverrides->m_ulEN_EventStart, 
									pOverrides->m_ulEN_EventDur
									);										
							
							// then fill the proprietary data of the replace content part
// NOTE ***** THIS uses ideal milliseconds, 

								UpdateReplaceContentObject(m_pscte104MsgReplaceContent, 
									(strlen(pOverrides->m_pszRepl_ProgramID)>0?pOverrides->m_pszRepl_ProgramID:NULL),
									pOverrides->m_ulRepl_EventStart,
									pOverrides->m_ulRepl_EventDur
									);
								
								unsigned char* pucOut = m_scte104.ReturnMessageBuffer(m_pscte104MsgReplaceContent);
								if(pucOut)
								{
									// encode it for Libretto
									unsigned long ulLen = (*(pucOut+2))&0x000000ff;
				//Q.Format("buflen %d", ulLen);
				//AfxMessageBox(Q);
									ulLen <<= 8; ulLen += (*(pucOut+3))&0x000000ff;
									char* pchBuffer = (char*)pucOut;

									if(g_settings.m_bReadableEncodedAsRun)
									{
										unsigned long ulBufLen = ulLen;
										char* pchHexBuffer = m_bu.ReadableHex((char*)pucOut, &ulBufLen, MODE_FULLHEX);
										if(pchHexBuffer)
										{
											szReadableII = pchHexBuffer;
											free(pchHexBuffer);
										}
									}


									int nEncodeReturn = m_bu.Base64Encode(&pchBuffer, &ulLen, false);
									if(nEncodeReturn>=RETURN_SUCCESS)
									{
										if(pchBuffer)
										{
											szII.Format("%s", pchBuffer);
											free(pchBuffer);
										}
									}

									delete [] pucOut; // yes, new was used
								}
							}

						}
						else
						if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Action_Cancel")==0)
						{
//FIX HARDCODE
#pragma message(alert "fix this hardcode")



							if(
								  (pOverrides) // calculated outside
								&&(m_pscte104MsgRejoinMain)
//								&&(m_pscte104MsgRejoinMain->m_scte104Hdr.NumberOfOps>0)
//								&&(m_pscte104MsgRejoinMain->m_ppscte104Ops)
//								&&(m_pscte104MsgRejoinMain->m_ppscte104Ops[0])
								)
							{
								// fill the proprietary data of the event notification part first
								// the time values must ALWAYS be overridden!
								UpdateEventNotificationObject(m_pscte104MsgRejoinMain, 
									(strlen(pOverrides->m_pszEN_ProgramID)>0?pOverrides->m_pszEN_ProgramID:NULL),
									(strlen(pOverrides->m_pszEN_ProgramDescription)>0?pOverrides->m_pszEN_ProgramDescription:NULL),
									pOverrides->m_ulEN_EventStart, 
									pOverrides->m_ulEN_EventDur
									);										
							
							
								unsigned char* pucOut = m_scte104.ReturnMessageBuffer(m_pscte104MsgRejoinMain);
								if(pucOut)
								{
									// encode it for Libretto
									unsigned long ulLen = (*(pucOut+2))&0x000000ff;
				//Q.Format("buflen %d", ulLen);
				//AfxMessageBox(Q);
									ulLen <<= 8; ulLen += (*(pucOut+3))&0x000000ff;
									char* pchBuffer = (char*)pucOut;

									if(g_settings.m_bReadableEncodedAsRun)
									{
										unsigned long ulBufLen = ulLen;
										char* pchHexBuffer = m_bu.ReadableHex((char*)pucOut, &ulBufLen, MODE_FULLHEX);
										if(pchHexBuffer)
										{
											szReadableII = pchHexBuffer;
											free(pchHexBuffer);
										}
									}


									int nEncodeReturn = m_bu.Base64Encode(&pchBuffer, &ulLen, false);
									if(nEncodeReturn>=RETURN_SUCCESS)
									{
										if(pchBuffer)
										{
											szII.Format("%s", pchBuffer);
											free(pchBuffer);
										}
									}

									delete [] pucOut; // yes, new was used
								}
							}
						}
						else
						if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Stream_Semaphore")==0)
						{

//FIX HARDCODE
#pragma message(alert "fix this hardcode")

							if(
								  (pOverrides) // calculated outside
								&&(m_pscte104MsgEventNotification)
//								&&(m_pscte104MsgEventNotification->m_scte104Hdr.NumberOfOps>0)
//								&&(m_pscte104MsgEventNotification->m_ppscte104Ops)
//								&&(m_pscte104MsgEventNotification->m_ppscte104Ops[0])
								)
							{
								// fill the proprietary data of the event notification part first
								// the time values must ALWAYS be overridden!
								UpdateEventNotificationObject(m_pscte104MsgEventNotification, 
									(strlen(pOverrides->m_pszEN_ProgramID)>0?pOverrides->m_pszEN_ProgramID:NULL),
									(strlen(pOverrides->m_pszEN_ProgramDescription)>0?pOverrides->m_pszEN_ProgramDescription:NULL),
									pOverrides->m_ulEN_EventStart, 
									pOverrides->m_ulEN_EventDur
									);										
							
								unsigned char* pucOut = m_scte104.ReturnMessageBuffer(m_pscte104MsgEventNotification);
								if(pucOut)
								{
									// encode it for Libretto
									unsigned long ulLen = (*(pucOut+2))&0x000000ff;
				//Q.Format("buflen %d", ulLen);
				//AfxMessageBox(Q);
									ulLen <<= 8; ulLen += (*(pucOut+3))&0x000000ff;
									char* pchBuffer = (char*)pucOut;

									if(g_settings.m_bReadableEncodedAsRun)
									{
										unsigned long ulBufLen = ulLen;
										char* pchHexBuffer = m_bu.ReadableHex((char*)pucOut, &ulBufLen, MODE_FULLHEX);
										if(pchHexBuffer)
										{
											szReadableII = pchHexBuffer;
											free(pchHexBuffer);
										}
									}


									int nEncodeReturn = m_bu.Base64Encode(&pchBuffer, &ulLen, false);
									if(nEncodeReturn>=RETURN_SUCCESS)
									{
										if(pchBuffer)
										{
											szII.Format("%s", pchBuffer);
											free(pchBuffer);
										}
									}

									delete [] pucOut; // yes, new was used
								}
							}
						}
						// else not supported!

					} // if(pEvent->m_ppEvents[i]->m_nType==0)  //anim was true
					else  // export
					{

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_TRIGGER)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "--- parse %02d %s with %s", i, pEvent->m_ppEvents[i]->m_szIdentifier, pEvent->m_ppEvents[i]->m_szValue); //(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Type is export, with dest type: %d", pEvent->m_ppEvents[i]->m_nDestType);  Sleep(50);//(Dispatch message)

						// searching for the tabulator token.
						
						if(stricmp(TabulatorToken, pEvent->m_ppEvents[i]->m_szIdentifier.Left(nTabulatorTokenLen).GetBuffer(0))==0)
						{
							bTabulatorEvent = true;  // !


							// CRAP, here I have to insert any parameters that might exist.

							// for now, just check the timer token, it is the only thing that requires parameters (I think)
							if(
									(pEvent->m_ppEvents[i]->m_szValue.CompareNoCase(g_settings.m_pszTimerToken?g_settings.m_pszTimerToken:"") == 0)
								&&(pEvent->m_ppEvents[i]->m_szIdentifier.Mid(nTabulatorTokenLen).CompareNoCase(g_settings.m_pszTimerStartToken?g_settings.m_pszTimerStartToken:"Timer_Start") == 0)
								&&(pOverrides)
								)
							{
								int nRealMS = pOverrides->m_ulActualTimerDurationMS; // real ms. m_timeconv.NDFMStoDFMS(pOverrides->m_ulRepl_EventDur);
								unsigned long ulTimeCode;

								// offset from pOverrides->m_ulEN_EventStart!

								if( 
									  (pOverrides->m_ulEN_EventStart==AUTODATA_USECURRENT_TIMEADDRESS) // do not use timer offset and duration adjustments for these.
									)
								{
									if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
									{
										EnterCriticalSection(&g_data.m_critTimeCode);
										ulTimeCode = g_data.m_ulTimeCode;
							//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
										LeaveCriticalSection(&g_data.m_critTimeCode);

									}
									else
									{
										// use system time
										_timeb timestamp;
										_ftime(&timestamp);
										int nNowMS = ((timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))%86400)*1000 + timestamp.millitm;
										m_timeconv.MStoBCDHMSF(nNowMS, &ulTimeCode, (g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate));
									}

								}
								else // an automation or timer event, here is where we can use offsets and adjustments.
								{
									int nNowMS;// = m_timeconv.NDFMStoDFMS(pOverrides->m_ulEN_EventStart) + g_settings.m_nTimerStartOffsetMS; //ideal MS.

									if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
									{
		//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp

										if(g_data.m_ucTimeCodeBits&0x80) // drop frame...
										{
											nNowMS = m_timeconv.NDFMStoDFMS(pOverrides->m_ulEN_EventStart) + g_settings.m_nTimerStartOffsetMS; //ideal MS.
										}
										else
										{
											nNowMS = pOverrides->m_ulEN_EventStart + g_settings.m_nTimerStartOffsetMS; //ideal MS.
										}

										m_timeconv.MStoBCDHMSF(nNowMS, &ulTimeCode, ((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)));

									}
									else  // system time
									{
										if(g_settings.m_bDF) // drop frame...
										{
											nNowMS = m_timeconv.NDFMStoDFMS(pOverrides->m_ulEN_EventStart) + g_settings.m_nTimerStartOffsetMS; //ideal MS.
										}
										else
										{
											nNowMS = pOverrides->m_ulEN_EventStart + g_settings.m_nTimerStartOffsetMS; //ideal MS.
										}
										m_timeconv.MStoBCDHMSF(nNowMS, &ulTimeCode, (g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate));
									}

									nRealMS += g_settings.m_nTimerDurationAdjustMS;
									 
								}


//				EnterCriticalSection(&g_data.m_critOutputVariables);  // already inside.
				EnterCriticalSection(&g_data.m_critSyncVariables);

								g_data.m_Sync.m_nLastTimerEventType = pOverrides->m_nReplacementType;
								g_data.m_Sync.m_nLastTimerStart = m_timeconv.BCDHMSFtoMS(ulTimeCode, (double)g_settings.m_nFrameRate); // ideal MS.

								if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
								{
	//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp

									if(g_data.m_ucTimeCodeBits&0x80) // drop frame...
									{
										g_data.m_Sync.m_nLastTimerDuration = m_timeconv.DFMStoNDFMS(nRealMS); // ideal MS
									}
									else
									{
										g_data.m_Sync.m_nLastTimerDuration = nRealMS; // ideal MS
									}

								}
								else  // system time
								{
									if(g_settings.m_bDF) // drop frame...
									{
										g_data.m_Sync.m_nLastTimerDuration = m_timeconv.DFMStoNDFMS(nRealMS); // ideal MS
									}
									else
									{
										g_data.m_Sync.m_nLastTimerDuration = nRealMS; // ideal MS
									}
								}


								

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_TIMER)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, g_settings.m_pszInternalAppName, "Timer of type %02x seeded with %d, %d; EN %d, %d",
	 g_data.m_Sync.m_nLastTimerEventType,
	 g_data.m_Sync.m_nLastTimerStart, g_data.m_Sync.m_nLastTimerDuration,
	 pOverrides->m_ulEN_EventStart, pOverrides->m_ulEN_EventDur);
 

								if(g_data.m_Output.m_b_list_is_playing)
								{
									// to check timer alignment with automation events.
									if((pOverrides->m_nReplacementType == AUTODATA_MANUAL_TYPE_ADREPL)||(pOverrides->m_nReplacementType == AUTODATA_AUTO_TYPE_ADREPL))
									{
										// if an ad replace, 
										g_data.m_Sync.m_bLastTimerEventStartAligned // has to be the whole replacement duration, not just the dur of the current event.
											= ((pOverrides->m_ulRepl_EventStart + pOverrides->m_ulRepl_EventDur) == (unsigned long)(g_data.m_Sync.m_nLastTimerStart+g_data.m_Sync.m_nLastTimerDuration));
										// these don't have to be DF converted, they are the same operation, and we are just looking for equality
									}
									else 
									if((pOverrides->m_nReplacementType == AUTODATA_MANUAL_TYPE_CONTENTREPL)||(pOverrides->m_nReplacementType == AUTODATA_AUTO_TYPE_CONTENTREPL))
									{
										// if content replace, it might be the whole duration, but maybe not.
										g_data.m_Sync.m_bLastTimerEventStartAligned // has to be the whole replacement duration, not just the dur of the current event.
											= ((g_data.m_Output.m_n_next_long_form_start) == IdealAddTimeAndDuration(g_data.m_Sync.m_nLastTimerStart, g_data.m_Sync.m_nLastTimerDuration)); // this can wrap midnight - we add program stream offset and go from there, make the correction later.
//							g_data.m_Output.m_n_next_long_form_start - pOverrides->m_ulEN_EventStart; //ideal MS.			
									}
									else // unknown... just use event itself.
									{
										g_data.m_Sync.m_bLastTimerEventStartAligned // has to be the whole replacement duration, not just the dur of the current event.
											= ((pOverrides->m_ulEN_EventStart + pOverrides->m_ulEN_EventDur) == (unsigned long)(g_data.m_Sync.m_nLastTimerStart+g_data.m_Sync.m_nLastTimerDuration));
										// these don't have to be DF converted, they are the same operation, and we are just looking for equality
									}
								}
								else
								{
									g_data.m_Sync.m_bLastTimerEventStartAligned = false;
								}
				LeaveCriticalSection(&g_data.m_critSyncVariables);
//				LeaveCriticalSection(&g_data.m_critOutputVariables);

/*
	// use these values to sync or defeat possible simultaneous automation events
	int m_nLastTimerStart;
	int m_nLastTimerDuration;
	bool m_bLastTimerEventStartAligned; // if a timer was set such that it ends exactly when an automation event begins, set this to true to defeat incoming automation event.

	// "squirrel away" these values for re-use
	int m_nLastENEventStart;
	int m_nLastENEventDur;
*/

								szII.Format("%s|%s|%d|%d", 
										pEvent->m_ppEvents[i]->m_szIdentifier.Mid(nTabulatorTokenLen), 
										pEvent->m_ppEvents[i]->m_szValue,
										nRealMS,
										ulTimeCode
										);


							}
							else
							{

								// just regular assemble
								if(pEvent->m_ppEvents[i]->m_szValue.GetLength()>0)
								{
									szII.Format("%s|%s", 
												pEvent->m_ppEvents[i]->m_szIdentifier.Mid(nTabulatorTokenLen), 
												pEvent->m_ppEvents[i]->m_szValue);
								}
								else
								{
									// could be just the token, no value, then you get no pipe.
									szII.Format("%s", pEvent->m_ppEvents[i]->m_szIdentifier.Mid(nTabulatorTokenLen));
								}
							}
						}	// else nothing, not supported!						
					}//if(pEvent->m_ppEvents[i]->m_nType==0)  //anim was false
					
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "II is now: %s", szII);  Sleep(50);//(Dispatch message)

					int nLen = szII.GetLength();
					if(nLen>0)  //ignore blank stuff, failure
					{
						char eventIDbuffer[256];
						_snprintf(eventIDbuffer, 256, "%s", pEvent->m_ppEvents[i]->m_szName);
						char* pchEncodedLocal = NULL;

						while((	(timebNowTick.time<timebTriggerTick.time)
									||((timebNowTick.time==timebTriggerTick.time)&&(timebNowTick.millitm<timebTriggerTick.millitm))
									)
									&&(g_dlldata.thread[0])&&(!g_dlldata.thread[0]->bKillThread))
						{
							Sleep(1);
							_ftime(&timebNowTick);
						}

						if(bTabulatorEvent)
						{

							if(g_data.m_socketTabulator)
							{
								CNetData data;
								data.m_ucType = (NET_TYPE_PROTOCOL1|NET_TYPE_KEEPOPEN|NET_SND_KEEPOPENLCL); // has data but no subcommand.
								data.m_ucCmd		= TABULATOR_CMD_PLUGINCALL;
								data.m_pucData	= (unsigned char*)malloc(nLen+1);
								if(data.m_pucData)
								{
									memcpy(data.m_pucData, szII.GetBuffer(0), nLen);
									data.m_pucData[nLen]=0;
									data.m_ulDataLen = nLen;
									data.m_ucType |= NET_TYPE_HASDATA;
								}

	g_data.m_bInCommand = TRUE;
								int nReturn = g_data.m_net.SendData(&data, g_data.m_socketTabulator, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, errorstring);
								
								if(nReturn<NET_SUCCESS)
								{
									// log it.
									g_data.m_net.CloseConnection(g_data.m_socketTabulator, errorstring);
									g_data.m_socketTabulator =  NULL;

									g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, eventIDbuffer, g_settings.m_pszInternalAppName, "Error %d sending to %s at %s:%d with: %s", 
										nReturn,
										g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
										g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
										g_settings.m_nTabulatorPort, 
										szII);
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, g_settings.m_pszInternalAppName, "Error %d (%s) sending to %s at %s:%d with: %s", 
						nReturn, errorstring,
						g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
						g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
						g_settings.m_nTabulatorPort, 
						szII);
								}
								else
								{
									g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, eventIDbuffer, g_settings.m_pszInternalAppName, "Successfully executed %s with return data: %02x,%02x %s (%d bytes data)", 
									szII,
									data.m_ucCmd,    // the command byte
									data.m_ucSubCmd,   // the subcommand byte
									(((data.m_pucData)&&(data.m_ulDataLen))?(char*)data.m_pucData:""),
									data.m_ulDataLen
									);

if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_TRIGGER)) 
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, g_settings.m_pszInternalAppName, "Action %s executed successfully with return data: %02x,%02x %s (%d bytes data)", 
								szII,
								data.m_ucCmd,    // the command byte
								data.m_ucSubCmd,   // the subcommand byte
								(((data.m_pucData)&&(data.m_ulDataLen))?(char*)data.m_pucData:""),
								data.m_ulDataLen

							); // Sleep(250); //(Dispatch message)
									// just send an ack
									g_data.m_net.SendData(NULL, g_data.m_socketTabulator, 5000, 0, NET_SND_CLNTACK, errorstring);
								}
	g_data.m_bInCommand = FALSE;

							}
							else
							{
								// can't send!

								g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, eventIDbuffer, g_settings.m_pszInternalAppName, "Error sending to %s at %s:%d with: %s", 
									g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
									g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
									g_settings.m_nTabulatorPort, 
									szII);

if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, g_settings.m_pszInternalAppName, "NULL socket error sending to %s at %s:%d with: %s", 
						g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
						g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
						g_settings.m_nTabulatorPort, 
						szII);
							}

							// send it!
						}
						else
						{
							pchEncodedLocal = m_pdbOutput->EncodeQuotes(szII);

							if((pchEncodedLocal)&&(strlen(pchEncodedLocal)>0))
							{

								if(
									  (g_data.m_nLastTime == timebNowTick.time)
									&&(g_data.m_nLastMillitm == timebNowTick.millitm)
									)
								{
									g_data.m_nMillitmIncr++;
									if(timebNowTick.millitm + g_data.m_nMillitmIncr > 999)
									{
										g_data.m_nTimeIncr++;
										g_data.m_nMillitmIncr -= 1000;
									}
								}
								else
								{
									if( 
										  (timebNowTick.time>g_data.m_nLastTime+g_data.m_nTimeIncr)
										||(
										    (timebNowTick.time==g_data.m_nLastTime+g_data.m_nTimeIncr)
										  &&(timebNowTick.millitm>g_data.m_nLastMillitm+g_data.m_nMillitmIncr)
											)
										)
									{
										// the last tick stuff has not overtaken the new now time, and otherwise would cause an ordering problem, so we can reset to zero
										g_data.m_nMillitmIncr = 0;
										g_data.m_nTimeIncr = 0;
									}
									else
									{
										// lapping did occur, so need to just adjust back
										// knowing that g_data.m_nLastTime and g_data.m_nLastMillitm are going to get rest to being equal to 
										// timebNowTick, we can calc where we can pick up the increment values from.

//										int nSec   = g_data.m_nLastTime + g_data.m_nTimeIncr - timebNowTick.time;
//										int nMilli = 1 + g_data.m_nLastMillitm + g_data.m_nMillitmIncr - timebNowTick.millitm; // don't forget to add one!

										g_data.m_nTimeIncr += (g_data.m_nLastTime - timebNowTick.time);
										g_data.m_nMillitmIncr += (1 + g_data.m_nLastMillitm - timebNowTick.millitm); // don't forget to add one!

										while(timebNowTick.millitm + g_data.m_nMillitmIncr < 0)
										{
											g_data.m_nTimeIncr--;
											g_data.m_nMillitmIncr += 1000;
										}
										while(timebNowTick.millitm + g_data.m_nMillitmIncr > 999)
										{
											g_data.m_nTimeIncr++;
											g_data.m_nMillitmIncr -= 1000;
										}
									}
								}

								_snprintf(szCommandSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(local, remote, action, host, timestamp, username) \
VALUES ('%s', '', %d, '%s', %d.%03d, 'sys')", //HARDCODE
															g_settings.m_pszModule?g_settings.m_pszModule:"Libretto",//"Libretto", //pEndObj->m_pszDBName?pEndObj->m_pszDBName:"Libretto",  // need to get this.
								//							pEndObj->m_pszQueue?pEndObj->m_pszQueue:"Queue",  // ok need to change this too!  argh! its not the endpoint object, that is the automation module....
															g_settings.m_pszQueue?g_settings.m_pszQueue:"Command_Queue",
															pchEncodedLocal?pchEncodedLocal:szII,
															256,
															g_settings.m_pszDrivenHost,
															timebNowTick.time + g_data.m_nTimeIncr,
															timebNowTick.millitm + g_data.m_nMillitmIncr
															);
	//	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Trigger SQL: %s", szCommandSQL); // Sleep(50); //(Dispatch message)
	EnterCriticalSection(&m_critOutputSQL);
								int nSQLReturn = m_pdbOutput->ExecuteSQL(m_pdbOutputConn, szCommandSQL, errorstring);
	LeaveCriticalSection(&m_critOutputSQL);
								if(nSQLReturn<DB_SUCCESS)
								{
							//**MSG
		g_ptabmsgr->DM(MSG_ICONHAND, NULL, szTickerSource, "ERROR %d executing SQL: %s", nSQLReturn, errorstring); // Sleep(50); //(Dispatch message)


									g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, eventIDbuffer, g_settings.m_pszInternalAppName, "Error %s sending data to device at %s: %s", nSQLReturn, g_settings.m_pszDrivenHost, g_settings.m_bReadableEncodedAsRun?szReadableII:szII);
								}
								else
								{
									g_data.m_nLastTime = timebNowTick.time;
									g_data.m_nLastMillitm = timebNowTick.millitm;


									g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, eventIDbuffer, g_settings.m_pszInternalAppName, "Sent data to device at %s: %s", g_settings.m_pszDrivenHost, g_settings.m_bReadableEncodedAsRun?szReadableII:szII);
		
							
		if((g_ptabmsgr)&&(g_settings.m_ulDebug&AUTODATA_DEBUG_TRIGGER)) 
			{
				g_ptabmsgr->DM(MSG_ICONHAND, NULL, szTickerSource, "command %d sent [est time %d.%.03d][%d.%03d + trig %.0f][db %d.%03d]: %s", 
					pEvent->m_ppEvents[i]->m_nEventID,
					timebNowTick.time,
					timebNowTick.millitm,
					timebBeginTick.time,
					timebBeginTick.millitm,
					pEvent->m_ppEvents[i]->m_dblTriggerTime, 
					timebNowTick.time + g_data.m_nTimeIncr,
					timebNowTick.millitm + g_data.m_nMillitmIncr,
					szII); // Sleep(50);//(Dispatch message)
			}
								}
							
							} // if(pchEncodedLocal)&& strlen
							if(pchEncodedLocal) free(pchEncodedLocal);
						} // else from if bTabulator event.
					} // szII had nonzero length 
				}//if(pEvent->m_ppEvents[i]->m_szIdentifier.GetLength())
			}// if((pEvent->m_ppEvents)&&(pEvent->m_ppEvents[i]))
			if(g_dlldata.thread[0]->bKillThread) break;
			i++;
			
		} // while
		LeaveCriticalSection(&pEvent->m_critEvents);
		return CLIENT_SUCCESS;
	}
	else	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "PlayEvent event exited with no action [%d%d%d%d %d]", 
		(m_pdbOutput!=NULL), 
		(m_pdbOutputConn!=NULL),
		(pEvent!=NULL),
		(pEvent->m_ppEvents!=NULL),
		(pEvent->m_nEvents>0)
		); //(Dispatch message)

	return CLIENT_ERROR;
}

