// LibrettoParse.h : main header file for the LIBRETTOPARSE application
//

#if !defined(AFX_LIBRETTOPARSE_H__25EB1E6A_5E43_4BAF_9126_20394371D057__INCLUDED_)
#define AFX_LIBRETTOPARSE_H__25EB1E6A_5E43_4BAF_9126_20394371D057__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CLibrettoParseApp:
// See LibrettoParse.cpp for the implementation of this class
//

class CLibrettoParseApp : public CWinApp
{
public:
	CLibrettoParseApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLibrettoParseApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CLibrettoParseApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LIBRETTOPARSE_H__25EB1E6A_5E43_4BAF_9126_20394371D057__INCLUDED_)
