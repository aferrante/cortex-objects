// StreamData.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "StreamData.h"
#include "StreamDataCore.h"
#include "StreamDataData.h"
#include "StreamDataSettings.h"


#include "../../../Cortex Objects/Tabulator/TabulatorDefines.h"
//#include "../../../Cortex Objects/Tabulator/TabulatorData.h"
#include <process.h>
#include "../../../Common/MFC/ODBC/DBUtil.h"
#include "../../../Common/TXT/BufferUtil.h"
#include <objsafe.h>
#include <atlbase.h>
#include "../../../Cortex/3.0.4.5/CortexShared.h" // included to have xml messaging objects and other shared things


//void ExecuteOpportunitiesThread(void* pvArgs);
void DelayDTMFThread(void* pvArgs);


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

#define StreamDataDLG_MINSIZEX 200
#define StreamDataDLG_MINSIZEY 150

//extern void StreamDataThread(void* pvArgs);

void AutomationAnalysisThread(void* pvArgs);
void MainProcessThread(void* pvArgs);
//void TimerExpiryDelayThread(void* pvArgs);



/////////////////////////////////////////////////////////////////////////////
// CStreamDataApp

BEGIN_MESSAGE_MAP(CStreamDataApp, CWinApp)
	//{{AFX_MSG_MAP(CStreamDataApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStreamDataApp construction

CStreamDataApp::CStreamDataApp()
{
	m_bInitInstanceComplete = false;
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	AfxInitRichEdit();
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CStreamDataApp object

CStreamDataApp				theOppApp;
CStreamDataCore			g_core;
CStreamDataData			g_data;
CStreamDataSettings	g_settings;
CMessager*			g_ptabmsgr = NULL;
DLLdata_t       g_dlldata;







/////////////////////////////////////////////////////////////////////////////
// COutputDlg dialog


COutputDlg::COutputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COutputDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(COutputDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_bVis=FALSE;
	m_bNewSizeInit=TRUE;
}


void COutputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COutputDlg)
	DDX_Control(pDX, IDC_LIST1, m_lc);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COutputDlg, CDialog)
	//{{AFX_MSG_MAP(COutputDlg)
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COutputDlg message handlers

BOOL COutputDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CRect rc; 
	m_lc.GetWindowRect(&rc);
	m_lc.InsertColumn(0, "Output variables", LVCFMT_LEFT, rc.Width()-16, 0 );

	ListView_SetExtendedListViewStyle(m_lc.m_hWnd, LVS_EX_FULLROWSELECT);

	char pszSource[MAX_PATH];
	_snprintf(pszSource, MAX_PATH-1, "%s Information", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
	SetWindowText(pszSource);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL COutputDlg::Create(CWnd* pParentWnd) 
{
/*	if(pParentWnd == NULL)
		AfxMessageBox("creating but NULL!");
	else 
		AfxMessageBox("creating!");
*/
	return CDialog::Create(COutputDlg::IDD, pParentWnd);
}


void COutputDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
//sizing stuff
	if(m_bNewSizeInit)
	{
		int nCtrlID;

		GetClientRect(&m_rcDlg); 
		CRect rcDlg=m_rcDlg;
		for (int i=0;i<1;i++)  
		{
			switch(i)
			{
			case 0: nCtrlID=IDC_LIST1;break;
			}
			GetDlgItem(nCtrlID)->GetWindowRect(&m_rcCtrl[i]);
			ScreenToClient(&m_rcCtrl[i]);
		}
	}
	m_bVis=TRUE;
	m_bNewSizeInit=FALSE;
	
}

void COutputDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	int nCtrlID;

	int dx,dy;
	CRect rcList;
	dx=m_rcDlg.right-cx;
	dy=m_rcDlg.bottom-cy;

	if(m_bVis)
	{
		CWnd* pWnd;

		pWnd=GetDlgItem(IDC_LIST1);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			0, 0, 
			m_rcCtrl[0].Width()-dx,  // goes with right edge
			m_rcCtrl[0].Height()-dy,  // goes with bottom edge
			SWP_NOZORDER|SWP_NOMOVE
			);

		if(m_lc.GetColumnWidth(0)<(m_rcCtrl[0].Width()-dx-16)) // only make this get larger
		{
			m_lc.SetColumnWidth(0, (m_rcCtrl[0].Width()-dx-16) ); 
		}

		for (int i=0;i<1;i++)  
		{
			switch(i)
			{
			case 0: nCtrlID=IDC_LIST1;break;
			}
			GetDlgItem(nCtrlID)->Invalidate();
		}
	}		

	MSG msg;
	while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		AfxGetApp()->PumpMessage();

}

void COutputDlg::PostNcDestroy() 
{
	// TODO: Add your specialized code here and/or call the base class
	delete this;

//	CDialog::PostNcDestroy();
}

void COutputDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	DestroyWindow();
	
//	CDialog::OnCancel();
}

void COutputDlg::OnExit() 
{
	// TODO: Add extra cleanup here
	DestroyWindow();
}

void COutputDlg::UpdateOutputDialog() 
{

	int nStatusRows = 
											1 // time code
										+ 1 // space
										+ 1 // automation list and status info
										+ 1 // space
										+ 13 // output variables, as below
										+ 2  // spaces in between output variables
										;

/*
	m_b_list_is_playing = false;  // automation list is playing a primary event.
	// here, current means the currently playing primary event.
	m_n_current_primary_start=STREAMDATA_INVALID_TIMEADDRESS; // in ideal MS; 
	m_n_current_primary_duration=STREAMDATA_INVALID_TIMEADDRESS;  //in ideal MS 
	m_psz_current_primary_ID=NULL; // = Harris ID of current
	m_psz_current_primary_description=NULL;// = Harris Title of current

	m_n_current_systemtime=0; // in ideal MS; 
	m_n_current_servertime=0; // in ideal MS; 
	m_dbl_last_servertime=-1.0; // in ideal MS since epoch, local time; 
	m_dbl_last_servertime_update=-1.0; // in ideal MS since epoch; 

	m_n_next_start_tagged_event_start = -1; // in ideal MS; -1 means not found
	m_n_next_end_tagged_event_start = -1; // in ideal MS;  -1 means not found
	m_n_next_tagged_interval_duration = 0; // in ideal MS; 
*/

	int nRows = m_lc.GetItemCount();

	int i=0;
	while(i<nStatusRows)
	{
		CString szNew = "";
		CString szText = "";
		if(i<nRows)
		{
			szText = m_lc.GetItemText(i,0);
		}
		else
		{
			szText = "";
			m_lc.InsertItem( i, "" );
		}

		switch(i)
		{
		case 0:// time code
			{
				// assemble data here
				if( g_settings.m_bUseTimeCode )
				{
					EnterCriticalSection(&g_data.m_critTimeCode);
					unsigned long TC = g_data.m_ulTimeCode;
					unsigned char df = g_data.m_ucTimeCodeBits;
					LeaveCriticalSection(&g_data.m_critTimeCode);

					if( TC == 0xffffffff )
					{
						szNew.Format("Time code%s active (no valid time code) op:0x%02x in:0x%02x",
							g_data.m_bTimeCodeThreadStarted?"":" not",
							g_core.m_tc.m_ucBoardOpModeCode,
							g_core.m_tc.m_ucBoardInputsCode
							);

					}
					else
					{
						szNew.Format("Time code%s active: %02x:%02x:%02x%s%02x  op:0x%02x in:0x%02x",
							g_data.m_bTimeCodeThreadStarted?"":" not",
							(unsigned char)((TC&0xff000000)>>24),
							(unsigned char)((TC&0x00ff0000)>>16),
							(unsigned char)((TC&0x0000ff00)>>8),  df&0x80?";":":",
							(unsigned char)(TC&0x000000ff),
							g_core.m_tc.m_ucBoardOpModeCode,
							g_core.m_tc.m_ucBoardInputsCode
							);
					}
//					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "updating  time code with %s ******** %08x %08x", szNew, TC, g_data.m_ulTimeCode);//   Sleep(250);//(Dispatch message)

				}
				else
				{
					double dblTime;
					_timeb timeNow;
					_ftime(&timeNow);

					dblTime = ((double)((timeNow.time - (timeNow.timezone*60)+(timeNow.dstflag?3600:0))%86400))*1000.0 + ((double)(timeNow.millitm)); // local time


					int nHours;
					int nMinutes;
					int nSeconds;
					int nFrames;

					g_data.m_timeconv.MStoHMSF((int)dblTime, &nHours, &nMinutes, &nSeconds, &nFrames, 
						(g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate)
						);

					szNew.Format("Time code not active, system time: %02d:%02d:%02d%s%02d",
						nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames
						);
				}
				

			} break;
		case 2:  // automation list and status info
			{
				CString szLookahead, szStatus;
				szLookahead = "";
				szStatus = "";

/*
int   LISTISPLAYING			= 0x0001;
int   LISTTHREADING			= 0x0002;
int   LISTINFREEZE			= 0x0004;
int   LISTINHOLD				= 0x0008;
int   LISTUPCOUNTING		= 0x0010;
int   LISTRECUEING			= 0x0020;
int   LISTWILLEND				= 0x0040;
int   LISTWILLENDBREAK	= 0x0080;
*/

				if(g_data.m_AutoList.m_nState<0)
				{ szStatus = "not connected"; }
				else if(g_data.m_AutoList.m_nState&0x0010)
				{ szStatus = "UPCOUNT"; }
				else if(g_data.m_AutoList.m_nState&0x0040)
				{ szStatus = "FREEZE"; }
				else if(g_data.m_AutoList.m_nState&0x0008)
				{ szStatus = "HOLD"; }
				else if(g_data.m_AutoList.m_nState&0x0008)
				{ szStatus = "HOLD"; }
				else if(g_data.m_AutoList.m_nState&0x0020)
				{ szStatus = "CUEING"; }
				else if(g_data.m_AutoList.m_nState&0x0001)
				{ szStatus = "PLAYING"; }
				else if(g_data.m_AutoList.m_nState&0x0002)
				{ szStatus = "THREADING"; }
				else
				{ szStatus = "STOPPED"; }

				if(g_data.m_AutoList.m_nState<0)
				{
					szNew.Format("%s:%d no connection",
						g_settings.m_pszAutoServer?g_settings.m_pszAutoServer:"(NULL)",
						g_settings.m_nAutoServerListNumber
						);
				}
				else
				{
					if(g_settings.m_nAutomationAnalysisLookahead>0)
					{
						szLookahead.Format(" local %d", g_settings.m_nAutomationAnalysisLookahead);
					}
				
					szNew.Format("%s:%d %s; count %d %slookahead %d%s",
						g_settings.m_pszAutoServer?g_settings.m_pszAutoServer:"(NULL)",
						g_settings.m_nAutoServerListNumber,
						szStatus,
						g_data.m_AutoList.m_nCount,
						g_settings.m_bAutomationLookahead?"using ":"not using ",
						g_data.m_AutoList.m_nLookahead,
						szLookahead
						);
				}
				
			} break;
		case 4:
			{
				EnterCriticalSection(&g_data.m_critOutputVariables);
				szNew.Format("Automation list is%s in a selected and playing state",  g_data.m_Output.m_b_list_is_playing?"":" not");
				LeaveCriticalSection(&g_data.m_critOutputVariables);
			} break;
/*
		case 6:
			{
				EnterCriticalSection(&g_data.m_critOutputVariables);
				if(g_data.m_Output.m_n_current_long_form_start==STREAMDATA_INVALID_TIMEADDRESS)
				{
					szNew.Format("Current Long Form Start: unavailable");
				}
				else
				{
					int nHours;
					int nMinutes;
					int nSeconds;
					int nFrames;

					g_data.m_timeconv.MStoHMSF(g_data.m_Output.m_n_current_long_form_start, &nHours, &nMinutes, &nSeconds, &nFrames, 
						(/*g_settings.m_bDF?29.97:* /(double)g_settings.m_nFrameRate) // ideal
						);
					szNew.Format("Current Long Form Start: %02d:%02d:%02d%s%02d",  nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames);
				}
				LeaveCriticalSection(&g_data.m_critOutputVariables);

			} break;
		case 7:
			{
				EnterCriticalSection(&g_data.m_critOutputVariables);
				if(g_data.m_Output.m_n_current_long_form_duration==STREAMDATA_INVALID_TIMEADDRESS)
				{
					szNew.Format("Current Long Form Duration: unavailable");
				}
				else
				{
					int nHours;
					int nMinutes;
					int nSeconds;
					int nFrames;

					g_data.m_timeconv.MStoHMSF(g_data.m_Output.m_n_current_long_form_duration, &nHours, &nMinutes, &nSeconds, &nFrames, 
						(/*g_settings.m_bDF?29.97:* /(double)g_settings.m_nFrameRate) // ideal
						);
					szNew.Format("Current Long Form Duration: %02d:%02d:%02d%s%02d",  nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames);
				}
				LeaveCriticalSection(&g_data.m_critOutputVariables);

			} break;
		case 8:
			{
				EnterCriticalSection(&g_data.m_critOutputVariables);
				if(g_data.m_Output.m_psz_current_long_form_ID==NULL)
				{
					szNew.Format("Current Long Form ID: unavailable");
				}
				else
				{
					szNew.Format("Current Long Form ID: %s", g_data.m_Output.m_psz_current_long_form_ID);
				}
				LeaveCriticalSection(&g_data.m_critOutputVariables);

			} break;
		case 9:
			{
				EnterCriticalSection(&g_data.m_critOutputVariables);
				if(g_data.m_Output.m_psz_current_long_form_description==NULL)
				{
					szNew.Format("Current Long Form Description: unavailable");
				}
				else
				{
					szNew.Format("Current Long Form Description: %s", g_data.m_Output.m_psz_current_long_form_description);
				}
				LeaveCriticalSection(&g_data.m_critOutputVariables);

			} break;
		case 10:
			{
				EnterCriticalSection(&g_data.m_critOutputVariables);
				if(g_data.m_Output.m_n_next_long_form_start==STREAMDATA_INVALID_TIMEADDRESS)
				{
					szNew.Format("Next Long Form Start: unavailable");
				}
				else
				{
					int nHours;
					int nMinutes;
					int nSeconds;
					int nFrames;

					g_data.m_timeconv.MStoHMSF(g_data.m_Output.m_n_next_long_form_start, &nHours, &nMinutes, &nSeconds, &nFrames, 
						(/*g_settings.m_bDF?29.97:* /(double)g_settings.m_nFrameRate) // ideal
						);
					szNew.Format("Next Long Form Start: %02d:%02d:%02d%s%02d",  nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames);
				}
				LeaveCriticalSection(&g_data.m_critOutputVariables);

			} break;
		case 12:
			{
				EnterCriticalSection(&g_data.m_critOutputVariables);
				if(g_data.m_Output.m_n_current_ad_pod_start==STREAMDATA_INVALID_TIMEADDRESS)
				{
					szNew.Format("Current Ad Pod Start: unavailable");
				}
				else
				{
					int nHours;
					int nMinutes;
					int nSeconds;
					int nFrames;

					g_data.m_timeconv.MStoHMSF(g_data.m_Output.m_n_current_ad_pod_start, &nHours, &nMinutes, &nSeconds, &nFrames, 
						(/*g_settings.m_bDF?29.97:* /(double)g_settings.m_nFrameRate) // ideal
						);
					szNew.Format("Current Ad Pod Start: %02d:%02d:%02d%s%02d",  nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames);
				}
				LeaveCriticalSection(&g_data.m_critOutputVariables);

			} break;
		case 13:
			{
				EnterCriticalSection(&g_data.m_critOutputVariables);
				if(g_data.m_Output.m_n_current_ad_pod_duration==STREAMDATA_INVALID_TIMEADDRESS)
				{
					szNew.Format("Current Ad Pod Duration: unavailable");
				}
				else
				{
					int nHours;
					int nMinutes;
					int nSeconds;
					int nFrames;

					g_data.m_timeconv.MStoHMSF(g_data.m_Output.m_n_current_ad_pod_duration, &nHours, &nMinutes, &nSeconds, &nFrames, 
						(/*g_settings.m_bDF?29.97:* /(double)g_settings.m_nFrameRate) // ideal
						);
					szNew.Format("Current Ad Pod Duration: %02d:%02d:%02d%s%02d",  nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames);
				}
				LeaveCriticalSection(&g_data.m_critOutputVariables);

			} break;		
		case 15:
			{
				EnterCriticalSection(&g_data.m_critOutputVariables);
				if(!g_data.m_Output.m_b_list_is_playing)
				{
					szNew.Format("Currently playing event classification is unavailable (%d)", g_data.m_Output.m_n_current_primary_class);
				}
				LeaveCriticalSection(&g_data.m_critOutputVariables);

			} break;
*/
		case 16:
			{
				EnterCriticalSection(&g_data.m_critOutputVariables);
				if(!g_data.m_Output.m_b_list_is_playing)
				{
					szNew.Format("Currently playing event start is unavailable");
				}
				else

				if(g_data.m_Output.m_n_current_primary_start==STREAMDATA_INVALID_TIMEADDRESS)
				{
					szNew.Format("Currently playing event start: unavailable");
				}
				else
				{
					int nHours;
					int nMinutes;
					int nSeconds;
					int nFrames;

					g_data.m_timeconv.MStoHMSF(g_data.m_Output.m_n_current_primary_start, &nHours, &nMinutes, &nSeconds, &nFrames, 
						(/*g_settings.m_bDF?29.97:*/(double)g_settings.m_nFrameRate) // ideal
						);
//					szNew.Format("Currently playing event start (%d): %02d:%02d:%02d%s%02d",  g_data.m_Output.m_n_current_primary_start, nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames);
					szNew.Format("Currently playing event start: %02d:%02d:%02d%s%02d",  nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames);
				}
				LeaveCriticalSection(&g_data.m_critOutputVariables);

			} break;
		case 17:
			{
				EnterCriticalSection(&g_data.m_critOutputVariables);
				if(!g_data.m_Output.m_b_list_is_playing)
				{
					szNew.Format("Currently playing event duration is unavailable");
				}
				else

				if(g_data.m_Output.m_n_current_primary_duration==STREAMDATA_INVALID_TIMEADDRESS)
				{
					szNew.Format("Currently playing event duration: unavailable");
				}
				else
				{
					int nHours;
					int nMinutes;
					int nSeconds;
					int nFrames;

					g_data.m_timeconv.MStoHMSF(g_data.m_Output.m_n_current_primary_duration, &nHours, &nMinutes, &nSeconds, &nFrames, 
						(/*g_settings.m_bDF?29.97:*/(double)g_settings.m_nFrameRate) // ideal
						);
//					szNew.Format("Currently playing event duration (%d): %02d:%02d:%02d%s%02d",  g_data.m_Output.m_n_current_primary_duration, nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames);
					szNew.Format("Currently playing event duration: %02d:%02d:%02d%s%02d", nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames);
				}
				LeaveCriticalSection(&g_data.m_critOutputVariables);

			} break;
		case 18:
			{
				EnterCriticalSection(&g_data.m_critOutputVariables);
				if(!g_data.m_Output.m_b_list_is_playing)
				{
					szNew.Format("Currently playing event ID is unavailable");
				}
				else
				if(g_data.m_Output.m_psz_current_primary_ID==NULL)
				{
					szNew.Format("Currently playing event ID: unavailable");
				}
				else
				{
					szNew.Format("Currently playing event ID: %s", g_data.m_Output.m_psz_current_primary_ID);
				}
				LeaveCriticalSection(&g_data.m_critOutputVariables);

			} break;
		case 19:
			{
				EnterCriticalSection(&g_data.m_critOutputVariables);
				if(!g_data.m_Output.m_b_list_is_playing)
				{
					szNew.Format("Currently playing event description is unavailable");
				}
				else
				if(g_data.m_Output.m_psz_current_primary_description==NULL)
				{
					szNew.Format("Currently playing event description: unavailable");
				}
				else
				{
					szNew.Format("Currently playing event description: %s", g_data.m_Output.m_psz_current_primary_description);
				}
				LeaveCriticalSection(&g_data.m_critOutputVariables);

			} break;
			
		default:
			{

/*
bool m_b_list_is_playing;  // automation list is playing a primary event.

//NOTE!  three different meaings of the word "current" - be aware!

// here, current means in-progess, OR most recently past.
char* m_psz_current_long_form_ID; // = Harris ID of last primary event classified as long-form.
char* m_psz_current_long_form_description;// = Harris Title of last primary event classified as long-form.
int m_n_current_long_form_start; // = Harris on air time of last primary event classified as long-form. in ideal MS
int m_n_current_long_form_duration;//  = Harris duration of last primary event classified as long-form. in ideal MS

// here, next means after the "current", as defined above
int m_n_next_long_form_start; // = Harris on air time of next primary event classified as long-form, or if no such event exists in the playlist, the starting time address of the Next Long-Form Program is the ending time address of the last primary event in the playlist plus one frame, in ideal ms

// here current means the in progress one, as we will not have messages for upcoming or past ones.
int m_n_current_ad_pod_start; // =  Harris on air time of last ad pod in ideal ms
int m_n_current_ad_pod_duration;// =  Assembled Harris duration of last ad pod  in real ms

// here, current means the currently playing primary event.
int m_n_current_primary_class; 
int m_n_current_primary_start; // in ideal MS; 
int m_n_current_primary_duration;  //in ideal MS 
char* m_psz_current_primary_ID; // = Harris ID of current
char* m_psz_current_primary_description;// = Harris Title of current
*/

				// nothing.
			} break;
		}

		if(szText.Compare(szNew))
		{
			m_lc.SetItemText(i, 0, szNew); 
		}

		i++;
	}

	while(i<nRows)
	{
		m_lc.DeleteItem(i);
		nRows--;
	}

	MSG msg;
	while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		AfxGetApp()->PumpMessage();

}







int  CStreamDataApp::DLLCtrl(void** ppvoid, UINT nType)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
//	char szSQL[DB_SQLSTRING_MAXLEN];
//	char errorstring[MAX_MESSAGE_LENGTH];

//	char szFeedSource[MAX_PATH]; sprintf(szFeedSource, "%s_DataDownload", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
//	char szTickerSource[MAX_PATH]; sprintf(szTickerSource, "%s_Ticker", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
	int nReturn = CLIENT_SUCCESS;

	char pszSource[MAX_PATH];
	_snprintf(pszSource, MAX_PATH-1, "%s", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");

	switch(nType)
	{
	case DLLCMD_MAINDLG_BEGIN://					0x00
		{
		} break;
	case DLLCMD_MAINDLG_END://						0x01
		{
		} break;
	case DLLCMD_SETTINGSTXT://						0x02
		{
			if(ppvoid) // we should be passing in the address of a char*.
			{
				if((g_settings.m_pszSettingsText)&&(strlen(g_settings.m_pszSettingsText)>0))
				{
					char* pch = (char*)malloc(strlen(g_settings.m_pszSettingsText)+1);
					if(pch)
					{
						strcpy(pch, g_settings.m_pszSettingsText);
						(*ppvoid)= pch;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
				}
				else
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_DOSETTINGSDLG://					0x03
		{
			// takes a NULL pointer.
/*
			if(g_settings.m_bHasDlg)
			{
				g_settings.DoModal();
			}
			else
*/
			{
				// just refresh
			EnterCriticalSection(&g_settings.m_crit);

				if(g_settings.Settings(true)==CLIENT_SUCCESS)
					nReturn = CLIENT_REFRESH;
				else nReturn = CLIENT_ERROR;

			LeaveCriticalSection(&g_settings.m_crit);
			}
		} break;
	case DLLCMD_GETABOUTTXT://						0x04
		{
			if(ppvoid) // we should be passing in the address of a char*.
			{
				if((g_settings.m_pszAboutText)&&(strlen(g_settings.m_pszAboutText)>0))
				{
					char* pch = (char*)malloc(strlen(g_settings.m_pszAboutText)+1);
					if(pch)
					{
						strcpy(pch, g_settings.m_pszAboutText);
						(*ppvoid)= pch;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
				}
				else
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_GETAPPNAME://							0x05
		{
			if(ppvoid) // we should be passing in the address of a char*.
			{
				if((g_settings.m_pszAppName)&&(strlen(g_settings.m_pszAppName)>0))
				{
					char* pch = (char*)malloc(strlen(g_settings.m_pszAppName)+1);
					if(pch)
					{
						strcpy(pch, g_settings.m_pszAppName);
						(*ppvoid)= pch;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
				}
				else
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
			
		} break;
	case DLLCMD_SETDISPATCHER://					0x06
		{
			if (*ppvoid)
			{
				g_ptabmsgr = (CMessager*)*ppvoid;
//				CString szMsg; szMsg.Format("%08x dispatcher",g_pmd);
//				AfxMessageBox(szMsg);
//				AfxMessageBox(AfxGetApp()->m_pszAppName);
//				AfxMessageBox(AfxGetApp()->m_pszExeName);

/*
					if(g_settings.m_bIsServer)
					{

*/
				if(g_settings.m_bEnableTones)
				{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Tones enabled %d",g_settings.m_pszTonesAsRunDestination?1:0);//  Sleep(250); //(Dispatch message)
					if((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))  // allow turning this off with zero length string
					{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Initiating asrun %s", g_settings.m_pszTonesAsRunDestination);//  Sleep(250); //(Dispatch message)
						if(g_ptabmsgr) g_ptabmsgr->AddDestination(
									MSG_DESTTYPE_LOG, 
									g_settings.m_pszTonesAsRunDestination, 
									((g_settings.m_pszTonesAsRunFilename!=NULL)?g_settings.m_pszTonesAsRunFilename:"Logs\\SerialTones\\T|YD01010500|C:\\Cortex\\Logs\\SerialTones\\T%m%d%y|1|1")
									);
					}
				}

				if(g_settings.m_bEnableSCTE104)
				{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "SCTE104 enabled %d",g_settings.m_pszSCTE104AsRunDestination?1:0);//  Sleep(250); //(Dispatch message)
					if((g_settings.m_pszSCTE104AsRunDestination)&&(strlen(g_settings.m_pszSCTE104AsRunDestination)))  // allow turning this off with zero length string
					{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Initiating asrun %s", g_settings.m_pszSCTE104AsRunDestination);//  Sleep(250); //(Dispatch message)
					if(g_ptabmsgr) g_ptabmsgr->AddDestination(
									MSG_DESTTYPE_LOG, 
									g_settings.m_pszSCTE104AsRunDestination, 
									((g_settings.m_pszSCTE104AsRunFilename!=NULL)?g_settings.m_pszSCTE104AsRunFilename:"Logs\\StreamData\\S|YD01010500|C:\\Cortex\\Logs\\StreamData\\S%m%d%y|1|1")
									);
					}
				}


/*
					}
*/

	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Version %s (%s %s) obtained dispatcher", VERSION_STRING, __DATE__, __TIME__);//  Sleep(250); //(Dispatch message)

			}
			else
			{
				nReturn = CLIENT_ERROR;
			}  

		} break;
	case DLLCMD_GETMINX://							0x07
		{
			if(ppvoid) // we should be passing in the address of an int.
			{
				(*ppvoid) = (void*)StreamDataDLG_MINSIZEX;
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_GETMINY://							0x08
		{
			if(ppvoid) // we should be passing in the address of an int.
			{
				(*ppvoid) = (void*)StreamDataDLG_MINSIZEY;
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_GETDATAOBJ://							0x09  // gets a pointer to the main data object
		{
			if(ppvoid) // we should be passing in the address of an data object.
			{
				(*ppvoid) = (void*)(&g_dlldata);
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_CALLFUNC://								0x0a  // call a function....  
		{
//AfxMessageBox("in call func");
			if(ppvoid) // we should be passing in the address of an char buffer.
			{
				char* pch = (char*)(*ppvoid);
				if((pch)&&(strlen(pch)))
				{
					// format is func name|arg|arg|arg  where if arg is a string, it is pipe encoded.
					char* pchDelim = strchr(pch, '|');
					if(pchDelim) *pchDelim = 0; //temporary null term.

		//			AfxMessageBox(pch);
					
if(g_ptabmsgr)
{
	_snprintf(pszSource, MAX_PATH-1, "%s:debug", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
	g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "Received: %s", pch); // Sleep(250); //(Dispatch message)
}
					CString  szData;

					(*ppvoid)= NULL;

					if(strcmp(pch, "Server_Get_Status")==0)
					{
						if(1) // success
						{
								// status return shall be:

							_timeb timebTick;
							_ftime(&timebTick);

							CString  szData;
							szData.Format("%d.%03d", timebTick.time, timebTick.millitm);

							// time|datalastupdate|datastate|time|tickerlastupdate|tickerstate

							//assemble some data, then formulate a response and send

							char* pchReturn =  (char*)malloc(szData.GetLength()+1);
							if(pchReturn)
							{
								sprintf(pchReturn, "%s", szData);
								(*ppvoid) = pchReturn;
							}
						}
						else
						{
							(*ppvoid)= NULL;
							nReturn = CLIENT_ERROR;
						}
					}
					else
					if(strcmp(pch, "Server_Refresh_Setting")==0)
					{
						(*ppvoid)= NULL;
			EnterCriticalSection(&g_settings.m_crit);
						g_settings.Settings(true);						
			LeaveCriticalSection(&g_settings.m_crit);
					}
					else
					if(strcmp(pch, "Server_Refresh_Table")==0)
					{
						// no tables to refresh, just ignore
/*
						nReturn = CLIENT_ERROR;
						if(pchDelim)
						{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker Commit {%s}", pchDelim+1);// Sleep(100); //(Dispatch message)
							CSafeBufferUtil sbu;
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_SINGLEDELIM);
							if((pch)&&(strlen(pch)))
							{
/*
								// table name. // find out which one to refresh
								if(stricmp(pch, "ALL")==0)
								{
	EnterCriticalSection(&g_data.m_critTableData);
//									g_data.GetOpportuneEvents();
//									g_data.GetExclusionRules();
	LeaveCriticalSection(&g_data.m_critTableData);
									nReturn = CLIENT_SUCCESS;
								}
								else
								if((g_settings.m_pszOpportuneEvents)&&(stricmp(pch, g_settings.m_pszOpportuneEvents)==0))
								{
	EnterCriticalSection(&g_data.m_critTableData);
//									g_data.GetOpportuneEvents();
	LeaveCriticalSection(&g_data.m_critTableData);
									nReturn = CLIENT_SUCCESS;
								}
								else
								if((g_settings.m_pszExclusionRules)&&(stricmp(pch, g_settings.m_pszExclusionRules)==0))
								{
	EnterCriticalSection(&g_data.m_critTableData);
//									g_data.GetExclusionRules();
	LeaveCriticalSection(&g_data.m_critTableData);
									nReturn = CLIENT_SUCCESS;
								}
* /
								
							}
						}
				*/
						(*ppvoid)= NULL;
					}
					else
					if(strcmp(pch, "Query_Ready")==0)  // someone wants to know if we are initialized.
					{
						(*ppvoid)= NULL;
						if(g_data.Query_Ready()<TABULATOR_SUCCESS)
						{
							nReturn = CLIENT_ERROR; // otherwise we send 0 = success in the subcommand
						}
					}
					else					
					if(strcmp(pch, "Break_End")==0)  
					{
						_snprintf(pszSource, MAX_PATH-1, "%s:Break_End", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");

						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR; // otherwise we send 0 = success in the subcommand

						if(!g_settings.m_bEnableSCTE104)
						{
							szData.Format("SCTE104 messages are not enabled");
							char* pchReturn =  (char*)malloc(szData.GetLength()+1);
							if(pchReturn)
							{
								sprintf(pchReturn, "%s", szData);
								(*ppvoid) = pchReturn;
							}
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "%s", szData );
	}
						}
						else
						{
							CMessageOverrideValues Overrides;
							Overrides.m_pszDTMF = g_settings.m_pszSCTE104BreakEndEventDTMFsequence;   // default, but can be overridden later
							CSafeBufferUtil sbu;
							char* pchSCTE = NULL;
							if(pchDelim)
							{
								// string coming in is Break_End|DTMFsequenceoptional|prerolloverrideoptional
	//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker Commit {%s}", pchDelim+1);// Sleep(100); //(Dispatch message)
								pchSCTE = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_SINGLEDELIM);
								if(pchSCTE)
								{
									if(strlen(pchSCTE)>0)
									{
										Overrides.m_pszDTMF = pchSCTE;
									}

									// now preroll.
									pchSCTE = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
									if((pchSCTE)&&(strlen(pchSCTE)>0))
									{
										Overrides.m_nPrerollMS = atoi(pchSCTE);
									}
								}// else // use defaults.
							}// else // use defaults.

								// then play the event right now.
							char errorstring[MAX_MESSAGE_LENGTH];
							nReturn = g_data.PlayEvent(&g_data.m_pBreakEndEvents, &Overrides, errorstring);
							if(nReturn<CLIENT_SUCCESS)
							{
								if(g_ptabmsgr)
								{
									g_ptabmsgr->DM(MSG_ICONERROR, NULL, pszSource, "Error: %s", errorstring ); // Sleep(250); //(Dispatch message)
								}

								szData.Format("%s:Break_End Error: %s",  g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", errorstring);
								char* pchReturn =  (char*)malloc(szData.GetLength()+1);
								if(pchReturn)
								{
									sprintf(pchReturn, "%s", szData);
									(*ppvoid) = pchReturn;
								}
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "%s", szData );
	}
							}
						}
					}
					else
					if(strcmp(pch, "Local060")==0)  
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR; // otherwise we send 0 = success in the subcommand
						_snprintf(pszSource, MAX_PATH-1, "%s:Local060", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");

						if(!g_settings.m_bEnableSCTE104)
						{
							szData.Format("SCTE104 messages are not enabled");
							char* pchReturn =  (char*)malloc(szData.GetLength()+1);
							if(pchReturn)
							{
								sprintf(pchReturn, "%s", szData);
								(*ppvoid) = pchReturn;
							}
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "%s", szData );
	}
						}
						else
						{
							CMessageOverrideValues Overrides;

							Overrides.m_pszDTMF = g_settings.m_pszSCTE104Local060EventDTMFsequence;   // default, but can be overridden later
							Overrides.m_nDurationMS = 60000;  //local 060! fixed value, not calculated.
							CSafeBufferUtil sbu;
							char* pchSCTE = NULL;
							if(pchDelim)
							{
								// string coming in is Local060|DTMFsequenceoptional|prerolloverrideoptional|suppressIDincrements
	//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker Commit {%s}", pchDelim+1);// Sleep(100); //(Dispatch message)
								pchSCTE = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_SINGLEDELIM);
								if(pchSCTE)
								{
									if(strlen(pchSCTE)>0)
									{
										Overrides.m_pszDTMF = pchSCTE;
									}
									// now preroll.
									pchSCTE = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
									if(pchSCTE)
									{
										if(strlen(pchSCTE)>0)
										{
											Overrides.m_nPrerollMS = atoi(pchSCTE);
										}
										// now ID increment suppression.
										pchSCTE = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
										if(pchSCTE)
										{
											if(strlen(pchSCTE)>0)
											{
												Overrides.m_uchSource = 0x01; // any value means suppress
											}
										}
									}// else // use defaults.
								}// else // use defaults.
							}// else // use defaults.

								// then play the event right now.
							char errorstring[MAX_MESSAGE_LENGTH];
							nReturn = g_data.PlayEvent(&g_data.m_pLocal060BreakEvents, &Overrides, errorstring);
							if(nReturn<CLIENT_SUCCESS)
							{
								if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, pszSource, "Error: %s", errorstring ); // Sleep(250); //(Dispatch message)

								szData.Format("%s:Local060 Error: %s", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", errorstring);
								char* pchReturn =  (char*)malloc(szData.GetLength()+1);
								if(pchReturn)
								{
									sprintf(pchReturn, "%s", szData);
									(*ppvoid) = pchReturn;
								}
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "%s", szData );
	}
							}
						}
					}
					else
					if(strcmp(pch, "Local120")==0)  
					{
						_snprintf(pszSource, MAX_PATH-1, "%s:Local120", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");

						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR; // otherwise we send 0 = success in the subcommand

						if(!g_settings.m_bEnableSCTE104)
						{
							szData.Format("SCTE104 messages are not enabled");
							char* pchReturn =  (char*)malloc(szData.GetLength()+1);
							if(pchReturn)
							{
								sprintf(pchReturn, "%s", szData);
								(*ppvoid) = pchReturn;
							}
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "%s", szData );
	}
						}
						else
						{
							CMessageOverrideValues Overrides;

							Overrides.m_pszDTMF = g_settings.m_pszSCTE104Local120EventDTMFsequence;   // default, but can be overridden later
							Overrides.m_nDurationMS = 120000;  //local 120! fixed value, not calculated.
							CSafeBufferUtil sbu;
							char* pchSCTE = NULL;
							if(pchDelim)
							{
								// string coming in is Local120|DTMFsequenceoptional|prerolloverrideoptional|suppressIDincrements
	//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker Commit {%s}", pchDelim+1);// Sleep(100); //(Dispatch message)
								pchSCTE = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_SINGLEDELIM);
								if(pchSCTE)
								{
									if(strlen(pchSCTE)>0)
									{
										Overrides.m_pszDTMF = pchSCTE;
									}

									// now preroll.
									pchSCTE = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
									if(pchSCTE)
									{
										if(strlen(pchSCTE)>0)
										{
											Overrides.m_nPrerollMS = atoi(pchSCTE);
										}
										// now ID increment suppression.
										pchSCTE = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
										if(pchSCTE)
										{
											if(strlen(pchSCTE)>0)
											{
												Overrides.m_uchSource = 0x01; // any value means suppress
											}
										}
									}// else // use defaults.
								}// else // use defaults.
							}// else // use defaults.

								// then play the event right now.
							char errorstring[MAX_MESSAGE_LENGTH];
							nReturn = g_data.PlayEvent(&g_data.m_pLocal120BreakEvents, &Overrides, errorstring);
							if(nReturn<CLIENT_SUCCESS)
							{
								if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, pszSource, "Error: %s", errorstring ); // Sleep(250); //(Dispatch message)

								szData.Format("%s:Local120 Error: %s", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", errorstring);
								char* pchReturn =  (char*)malloc(szData.GetLength()+1);
								if(pchReturn)
								{
									sprintf(pchReturn, "%s", szData);
									(*ppvoid) = pchReturn;
								}
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "%s", szData );
	}

							}
						}
					}
					else

					if(strcmp(pch, "National_Break")==0)  
					{
						_snprintf(pszSource, MAX_PATH-1, "%s:National_Break", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");

						// immediately start the delay thread.
						if((g_settings.m_bEnableTones)&&(g_settings.m_bDoNationalBreakTones)) // has to be enabled and set to be coupled w nat brk
						{							
							COutputVariables* pov = new COutputVariables;
							if(pov)
							{
								_timeb now;
								_ftime(&now);

//								pov->m_n_current_primary_start = clock(); // override with a "now" value.  in case critsecs take time, we can compensate
// can't use clock() - differnt cores, the new thread may use a different core with a different clock.... sigh.


								EnterCriticalSection(&g_data.m_critOutputVariables);
								g_data.GetServerTime(true); // calc only!
								*pov = g_data.m_Output;
								LeaveCriticalSection(&g_data.m_critOutputVariables);

								pov->m_psz_current_primary_ID = NULL; // nulled because not used, do not want actual string freed
								pov->m_psz_current_primary_description = NULL;//nulled because not used, do not want actual string freed

								pov->m_n_current_primary_duration = g_settings.m_nNationalBreakTonesDelayMS;  // override the curr pri dur with the delay...

								pov->m_n_current_primary_start = now.time;
								pov->m_n_next_start_tagged_national_event_start = now.millitm; // since not used otherwise, only usingtaged duration for DTMF, not national tagged.

		if(_beginthread(DelayDTMFThread, 0, (void*)(pov))==-1)
		{
		//error.
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, pszSource, "Error starting DTMF delay thread");//   Sleep(250);//(Dispatch message)
		//**MSG
			delete pov;  /// have to free the mem.
		}
							}
							else
							{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, pszSource, "Error allocating DTMF object");//   Sleep(250);//(Dispatch message)
							}
						}


						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR; // otherwise we send 0 = success in the subcommand

						if(!g_settings.m_bEnableSCTE104)
						{
							szData.Format("SCTE104 messages are not enabled");
							char* pchReturn =  (char*)malloc(szData.GetLength()+1);
							if(pchReturn)
							{
								sprintf(pchReturn, "%s", szData);
								(*ppvoid) = pchReturn;
							}
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "%s", szData );
	}

						}
						else
						{

							EnterCriticalSection(&g_data.m_critOutputVariables);
							g_data.GetServerTime(true); // calc only!
							COutputVariables ov = g_data.m_Output;
							LeaveCriticalSection(&g_data.m_critOutputVariables);

							ov.m_psz_current_primary_ID = NULL; // nulled because not used, do not want actual string freed
							ov.m_psz_current_primary_description = NULL;//nulled because not used, do not want actual string freed


							if(
									((g_settings.m_bSCTE104UseDurationAddition?ov.m_n_next_tagged_national_interval_accumulated_duration:ov.m_n_next_tagged_national_interval_subtracted_duration)>0)  // resultant duration
								&&(
								    (g_settings.m_nNationalMinimumDurationMS<=0)
								  ||((g_settings.m_nNationalMinimumDurationMS>0)&&((g_settings.m_bSCTE104UseDurationAddition?ov.m_n_next_tagged_national_interval_accumulated_duration:ov.m_n_next_tagged_national_interval_subtracted_duration)>g_settings.m_nNationalMinimumDurationMS))
									) //(if there is duration left after local durations are subtracted out...
								)
							{
								CMessageOverrideValues Overrides;
								Overrides.m_pszDTMF = g_settings.m_pszSCTE104NationalEventDTMFsequence; //default assignment here.
								Overrides.m_nDurationMS = (g_settings.m_bSCTE104UseDurationAddition?ov.m_n_next_tagged_national_interval_accumulated_duration:ov.m_n_next_tagged_national_interval_subtracted_duration);
								char* pchSCTE = NULL;
								CSafeBufferUtil sbu;
								if(pchDelim)
								{
									// string coming in is Break_End|DTMFsequenceoptional|prerolloverrideoptional
		//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker Commit {%s}", pchDelim+1);// Sleep(100); //(Dispatch message)
									pchSCTE = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_SINGLEDELIM);
									if(pchSCTE)
									{
										if(strlen(pchSCTE)>0)
										{
											Overrides.m_pszDTMF = pchSCTE;
										}

	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "DTMF sequence is %s", Overrides.m_pszDTMF );
	}

										// now preroll.
										pchSCTE = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
										if(pchSCTE)
										{
											if(strlen(pchSCTE)>0)
											{
												Overrides.m_nPrerollMS = atoi(pchSCTE);
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "Preroll is %d (%s)", Overrides.m_nPrerollMS, pchSCTE );
	}
											}

										}
									}// else // use defaults.
								}// else // use defaults.

									// then play the event right now.
								char errorstring[MAX_MESSAGE_LENGTH];
								nReturn = g_data.PlayEvent(&g_data.m_pNationalBreakEvents, &Overrides, errorstring);
								if(nReturn<CLIENT_SUCCESS)
								{
									if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, pszSource, "Error: %s", errorstring ); // Sleep(250); //(Dispatch message)

									szData.Format("%s:National_Break Error: %s", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", errorstring);
									char* pchReturn =  (char*)malloc(szData.GetLength()+1);
									if(pchReturn)
									{
										sprintf(pchReturn, "%s", szData);
										(*ppvoid) = pchReturn;
									}
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "%s", szData );
	}

								}
							}
							else
							{
								// just return success, and a message that it was supressed.
								nReturn = CLIENT_SUCCESS;
								szData.Format("%s: Resultant interval of %d ms was below minimum threshold of %d ms.", pszSource, (g_settings.m_bSCTE104UseDurationAddition?ov.m_n_next_tagged_national_interval_accumulated_duration:ov.m_n_next_tagged_national_interval_subtracted_duration), g_settings.m_nNationalMinimumDurationMS);
								char* pchReturn =  (char*)malloc(szData.GetLength()+1);
								if(pchReturn)
								{
									sprintf(pchReturn, "%s", szData);
									(*ppvoid) = pchReturn;
								}
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "%s", szData );
	}

							}
						}
					}
					else
					if(strcmp(pch, "Test_DTMF")==0)  // someone wants to test sending a dtmf string manually
					{
//AfxMessageBox(pch);
						nReturn = CLIENT_ERROR;
						(*ppvoid)= NULL;

						if(!g_settings.m_bEnableTones)
						{
							szData.Format("DTMF tones are not enabled");
							char* pchReturn =  (char*)malloc(szData.GetLength()+1);
							if(pchReturn)
							{
								sprintf(pchReturn, "%s", szData);
								(*ppvoid) = pchReturn;
							}
						}
						else
						if(pchDelim)
						{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker Commit {%s}", pchDelim+1);// Sleep(100); //(Dispatch message)
							CSafeBufferUtil sbu;
							char* pchDTMF = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_SINGLEDELIM);
							if(pchDTMF)
							{
								char pszToken[MAX_PATH];

								if((g_settings.m_pszDTMFAddress)&&(strlen(g_settings.m_pszDTMFAddress)))
								{
									_snprintf(pszToken, MAX_PATH-1, "%s|%s|%s|%02x%02x%02x%02x", 
										g_settings.m_pszTabulatorPlugInModule?g_settings.m_pszTabulatorPlugInModule:"Videoframe", 
										g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF",
										pchDTMF,
										g_settings.m_ucDTMFAddress[0],
										g_settings.m_ucDTMFAddress[1],
										g_settings.m_ucDTMFAddress[2],
										g_settings.m_ucDTMFAddress[3]
										);
								}
								else
								{
									_snprintf(pszToken, MAX_PATH-1, "%s|%s|%s", 
										g_settings.m_pszTabulatorPlugInModule?g_settings.m_pszTabulatorPlugInModule:"Videoframe", 
										g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF",
										pchDTMF
										);
								}

								unsigned char* pucDataBuffer = (unsigned char*)pszToken;

//AfxMessageBox(pszToken);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Test_DTMF", "Test_DTMF asrun %d", g_settings.m_bAsRunTestDTMF);// Sleep(100); //(Dispatch message)
								char errorstring[DB_ERRORSTRING_LEN];
								nReturn = g_data.SendTabulatorCommand(&pucDataBuffer, "Test_DTMF", g_settings.m_bAsRunTestDTMF?false:true, pchDTMF, NULL, errorstring);

								if(nReturn>CLIENT_ERROR)
								{
									(*ppvoid)= NULL;
								}
								else
								{
									//err msg
									szData.Format("Error %d sending test DTMF command with %s: %s", nReturn, pszToken, errorstring);
									char* pchReturn =  (char*)malloc(szData.GetLength()+1);
									if(pchReturn)
									{
										sprintf(pchReturn, "%s", szData);
										(*ppvoid) = pchReturn;
									}
								}

								if((pucDataBuffer)&&(pucDataBuffer != (unsigned char*)pszToken))
								{
									free(pucDataBuffer);
								}
							}
							else
							{
								//err msg
								szData.Format("No DTMF sequence was available in directive %s", pch);
								char* pchReturn =  (char*)malloc(szData.GetLength()+1);
								if(pchReturn)
								{
									sprintf(pchReturn, "%s", szData);
									(*ppvoid) = pchReturn;
								}
							// asrun.
		if(g_settings.m_bAsRunTestDTMF)
		{
			g_ptabmsgr->DM(MSG_ICONERROR, 
					(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
					"Test_DTMF", "Error: No DTMF sequence was available in directive %s", 
					pch
				);   //(Dispatch message)

		}
							}
						}
						else
						{
							szData.Format("No DTMF sequence was found in directive %s", pch);
							char* pchReturn =  (char*)malloc(szData.GetLength()+1);
							if(pchReturn)
							{
								sprintf(pchReturn, "%s", szData);
								(*ppvoid) = pchReturn;
							}
							// asrun.
		if(g_settings.m_bAsRunTestDTMF)
		{
			g_ptabmsgr->DM(MSG_ICONERROR, 
					(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
					"Test_DTMF", "Error: No DTMF sequence was found in directive %s", 
					pch
				);   //(Dispatch message)

		}
						}

					}
					else
					if(strcmp(pch, "Send_DTMF")==0)  // Promotor wants to send the next dtmf string now.
					{
						nReturn = CLIENT_ERROR;
						if(!g_settings.m_bEnableTones)
						{
							szData.Format("DTMF tones are not enabled");
							char* pchReturn =  (char*)malloc(szData.GetLength()+1);
							if(pchReturn)
							{
								sprintf(pchReturn, "%s", szData);
								(*ppvoid) = pchReturn;
							}
						}
						else
						{
							EnterCriticalSection(&g_data.m_critOutputVariables);
							g_data.GetServerTime(true); // calc only!
							COutputVariables ov = g_data.m_Output;
							LeaveCriticalSection(&g_data.m_critOutputVariables);

							ov.m_psz_current_primary_ID = NULL; // nulled because not used, do not want actual string freed
							ov.m_psz_current_primary_description = NULL;//nulled because not used, do not want actual string freed



							int nDur = (g_settings.m_bTonesUseDurationAddition?ov.m_n_next_tagged_interval_accumulated_duration:ov.m_n_next_tagged_interval_subtracted_duration);

							if(nDur<=0)
							{
								// no tones to send.
								// err msg
								szData.Format("No interval detected; DTMF string cannot be formatted");
								char* pchReturn =  (char*)malloc(szData.GetLength()+1);
								if(pchReturn)
								{
									sprintf(pchReturn, "%s", szData);
									(*ppvoid) = pchReturn;
								}

								// asrun.
	if(g_settings.m_bAsRunDTMF)
	{
				if(ov.m_n_current_servertime>=0)
				{
					int nHours;
					int nMinutes;
					int nSeconds;
					int nFrames;

					g_data.m_timeconv.MStoHMSF(ov.m_n_current_servertime, &nHours, &nMinutes, &nSeconds, &nFrames, 
						(/*g_settings.m_bDF?29.97:*/(double)g_settings.m_nFrameRate) // ideal
						);

					g_ptabmsgr->DM(MSG_ICONERROR, 
							(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
							g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF",
							"Error: No interval detected; DTMF string cannot be formatted at server time %02d:%02d:%02d%s%02d", 
							nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames
						);   //(Dispatch message)
				}
				else
				{
					// no server time
					g_ptabmsgr->DM(MSG_ICONERROR, 
						(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
							g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF", 
							"No interval detected; DTMF string cannot be formatted"
						);   //(Dispatch message)
				}
	}
							}
							else
							{							
								char* pchDTMF = g_data.FormatDTMF(nDur);

								if(pchDTMF)
								{
									char pszToken[MAX_PATH];

									if((g_settings.m_pszDTMFAddress)&&(strlen(g_settings.m_pszDTMFAddress)))
									{
										_snprintf(pszToken, MAX_PATH-1, "%s|%s|%s|%02x%02x%02x%02x", 
											g_settings.m_pszTabulatorPlugInModule?g_settings.m_pszTabulatorPlugInModule:"Videoframe", 
											g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF",
											pchDTMF,
											g_settings.m_ucDTMFAddress[0],
											g_settings.m_ucDTMFAddress[1],
											g_settings.m_ucDTMFAddress[2],
											g_settings.m_ucDTMFAddress[3]
											);
									}
									else
									{
										_snprintf(pszToken, MAX_PATH-1, "%s|%s|%s", 
											g_settings.m_pszTabulatorPlugInModule?g_settings.m_pszTabulatorPlugInModule:"Videoframe", 
											g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF",
											pchDTMF
											);
									}


									unsigned char* pucDataBuffer = (unsigned char*)pszToken;
									char errorstring[DB_ERRORSTRING_LEN];
									nReturn = g_data.SendTabulatorCommand(&pucDataBuffer, g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF", g_settings.m_bAsRunDTMF?false:true, pchDTMF, &ov, errorstring);

									free(pchDTMF);

									if(nReturn>CLIENT_ERROR)
									{
										(*ppvoid)= NULL;
									}
									else
									{
										//err msg
										szData.Format("Error %d sending DTMF command with %s: %s", nReturn, pszToken, errorstring);
										char* pchReturn =  (char*)malloc(szData.GetLength()+1);
										if(pchReturn)
										{
											sprintf(pchReturn, "%s", szData);
											(*ppvoid) = pchReturn;
										}
									}

									if((pucDataBuffer)&&(pucDataBuffer != (unsigned char*)pszToken))
									{
										free(pucDataBuffer);
									}

								}
								else
								{
									//err msg
									szData.Format("No DTMF format available for duration %d", nDur);
									char* pchReturn =  (char*)malloc(szData.GetLength()+1);
									if(pchReturn)
									{
										sprintf(pchReturn, "%s", szData);
										(*ppvoid) = pchReturn;
									}
	if(g_settings.m_bAsRunDTMF)
	{
				if(ov.m_n_current_servertime>=0)
				{
					int nHours;
					int nMinutes;
					int nSeconds;
					int nFrames;

					g_data.m_timeconv.MStoHMSF(ov.m_n_current_servertime, &nHours, &nMinutes, &nSeconds, &nFrames, 
						(/*g_settings.m_bDF?29.97:*/(double)g_settings.m_nFrameRate) // ideal
						);

					g_ptabmsgr->DM(MSG_ICONERROR, 
							(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
							g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF", 
							"Error: No DTMF format available for duration %d at server time %02d:%02d:%02d%s%02d", 
							nDur,
							nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames
						);   //(Dispatch message)
				}
				else
				{
					// no server time
					g_ptabmsgr->DM(MSG_ICONERROR, 
						(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
							g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF", 
							"Error: No DTMF format available for duration %d", 
							nDur
						);   //(Dispatch message)
				}

	}
								}
							}
						}
					}
					else
					{
						szData.Format("Unsupported call [%s]", pch);
						char* pchReturn =  (char*)malloc(szData.GetLength()+1);
						if(pchReturn)
						{
							sprintf(pchReturn, "%s", szData);
							(*ppvoid) = pchReturn;
						}
						nReturn = CLIENT_ERROR;
					}
					
					if(pchDelim) *pchDelim = '|'; //replace pipe.

if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "Handled plugin call"); // Sleep(250); //(Dispatch message)

				}
				else 
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_SETEVENTS://						0x0b
		{
			if(ppvoid)
			{
				g_data.m_pEvents = (CTabulatorEventArray*)ppvoid;
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "Set Tabulator events"); // Sleep(250); //(Dispatch message)

//g_data.m_bCrawlEventChanged=true;  /ST not using Tab events

//AfxMessageBox("X");
			}
		} break;
	case DLLCMD_GETDBSETTINGS://						0x0c
		{
			while(!theOppApp.m_bInitInstanceComplete) Sleep(100);  // have to let the settings be gotten first to init properly.  THEN Tabulator can command it to happen again
			EnterCriticalSection(&g_settings.m_crit);
//	MB("in DLLCMD_GETDBSETTINGS");
			g_settings.GetFromDatabase();
//	MB("returned DLLCMD_GETDBSETTINGS");
			LeaveCriticalSection(&g_settings.m_crit);
		} break;
	case DLLCMD_SETGLOBALSTATE://						0x0d
		{
/*			if(ppvoid)
			{
				char statetoken[MAX_PATH];
				strncpy(statetoken, (char*)ppvoid, MAX_PATH);
				int n=0;
				while(n<g_dlldata.nNumThreads)
				{
					if((g_dlldata.thread)&&(g_dlldata.thread[n]))
					{
						if(stricmp(statetoken, "resume")==0)
						{
							switch(n)
							{
							case StreamData_DATA:// 0
								g_dlldata.thread[StreamData_DATA]->nThreadState &= ~StreamData_DATA_PAUSED; break;
							case StreamData_TICKER:// 1
								g_dlldata.thread[StreamData_TICKER]->nThreadState &= ~StreamData_TICKER_PAUSED; break;
							default:
							case StreamData_UI:// 2
								break;
							}
						}
						else
						if(stricmp(statetoken, "suspend")==0)
						{
							switch(n)
							{
							case StreamData_DATA:// 0
								g_dlldata.thread[StreamData_DATA]->nThreadState |= StreamData_DATA_PAUSED; break;
							case StreamData_TICKER:// 1
								g_dlldata.thread[StreamData_TICKER]->nThreadState |= StreamData_TICKER_PAUSED; break;
							default:
							case StreamData_UI:// 2
								break;
							}
						}
					}
					n++;
				}
			}
			else nReturn = CLIENT_ERROR;
*/

		} break;

		

	default: nReturn = CLIENT_UNKNOWN; break;
	}
	return nReturn;
}

BOOL CStreamDataApp::InitInstance() 
{
#if _WIN32_WINNT >= 0x0400
		HRESULT hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		HRESULT hRes = CoInitialize(NULL);
#endif
		m_bInitInstanceComplete = false;
//	AfxMessageBox(m_pszAppName);
//	AfxMessageBox(m_pszExeName);
//	AfxMessageBox("Foo");
	char pszSource[MAX_PATH];
	_snprintf(pszSource, MAX_PATH-1, "%s", g_settings.m_pszAppName?g_settings.m_pszAppName:"StreamData");

	strcpy(m_szSettingsFilename, SETTINGS_FILENAME);

	if((m_pszAppName)&&(strlen(m_pszAppName)>0)) sprintf(m_szSettingsFilename, "%s.ini", m_pszAppName);
//	MB(m_szSettingsFilename);

	InitializeCriticalSection(&g_dlldata.m_crit);
	EnterCriticalSection(&g_dlldata.m_crit);

	g_dlldata.nNumThreads=0;
	g_dlldata.thread = new DLLthread_t*[1]; // 1 thread, timecode thread
	
	if(g_dlldata.thread)
	{
		g_dlldata.nNumThreads=1;
		g_dlldata.thread[0] = new DLLthread_t;
		g_data.InitDLLData(g_dlldata.thread[0]);
		g_data.InitDLLData(g_dlldata.thread[0]);

		g_dlldata.thread[0]->bKillThread=false;

	}
		
 	LeaveCriticalSection(&g_dlldata.m_crit);

//	g_dlldata.pDlg = g_pdlg; // if there is a dialog....

//	AfxMessageBox("Foo1");
			EnterCriticalSection(&g_settings.m_crit);
//			if(bIsServer)
//			{
//	MB("calling gfdb from init");
	g_settings.GetFromDatabase(); // does Settings(true) first
//	MB("gfdb2");
//			}
//			else
//			{
//	g_settings.Settings(true); // need to get the thread settings...
//			}
			LeaveCriticalSection(&g_settings.m_crit);
	

		// create the db connection...


//		char szSQL[DB_SQLSTRING_MAXLEN];

		_snprintf(pszSource, MAX_PATH-1, "%s_Init:database_connect", g_settings.m_pszAppName?g_settings.m_pszAppName:"StreamData");
		char errorstring[MAX_MESSAGE_LENGTH];
		g_data.m_pdb = new CDBUtil;
		g_data.m_pdbConn = g_data.m_pdb->CreateNewConnection(g_settings.m_pszDSN, g_settings.m_pszUser, g_settings.m_pszPW);
		if(g_data.m_pdbConn)
		{
			if(g_data.m_pdb->ConnectDatabase(g_data.m_pdbConn, errorstring)<DB_SUCCESS)
			{
				if(g_ptabmsgr)
				{
					g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, pszSource, errorstring);  //(Dispatch message)
				}
			}
			else
			{
/*

	// get the autodatas.
				// but first set them all to "reset".

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = '0', duration = '0', start_tc = '0', stop_tc = '0' \
count = '0', paused_duration = '0', paused_count = '0', last_started = '0', last_stopped = '0' WHERE uid > -1",  //HARDCODE
						((g_settings.m_pszStreamData)&&(strlen(g_settings.m_pszStreamData)))?g_settings.m_pszStreamData:"StreamData"
						);
	//			Sleep(1); // dont peg processor
if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_SQL)) 
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "SQL: %s", szSQL); // Sleep(250); //(Dispatch message)
				if(g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
				{
					//**MSG
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

				}

				g_data.GetStreamData();
*/



			}
		}
		else
		{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, pszSource, errorstring);  //(Dispatch message)
		}


		_snprintf(pszSource, MAX_PATH-1, "%s_output_init:database_connect", g_settings.m_pszAppName?g_settings.m_pszAppName:"StreamData");
		g_data.m_pdbOutput = new CDBUtil;
		g_data.m_pdbOutputConn = g_data.m_pdbOutput->CreateNewConnection(g_settings.m_pszDSN, g_settings.m_pszUser, g_settings.m_pszPW);
		if(g_data.m_pdbOutputConn)
		{
			if(g_data.m_pdbOutput->ConnectDatabase(g_data.m_pdbOutputConn, errorstring)<DB_SUCCESS)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, pszSource, errorstring);  //(Dispatch message)
			}
			else
			{

			}
		}
		else
		{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, pszSource, errorstring);  //(Dispatch message)
		}
//	MB("main");



		_snprintf(pszSource, MAX_PATH-1, "%s_automation_init:database_connect", g_settings.m_pszAppName?g_settings.m_pszAppName:"StreamData");
		g_data.m_pdbAutomation = new CDBUtil;
		g_data.m_pdbAutomationConn = g_data.m_pdbAutomation->CreateNewConnection(g_settings.m_pszDSN, g_settings.m_pszUser, g_settings.m_pszPW);
		if(g_data.m_pdbAutomationConn)
		{
			if(g_data.m_pdbAutomation->ConnectDatabase(g_data.m_pdbAutomationConn, errorstring)<DB_SUCCESS)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, pszSource, errorstring);  //(Dispatch message)
			}
			else
			{

			}
		}
		else
		{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, pszSource, errorstring);  //(Dispatch message)
		}



		if(_beginthread(MainProcessThread, 0, (void*)(NULL))==-1)
		{
				AfxMessageBox("no main");

		//error.
if(g_ptabmsgr)
{
			
	_snprintf(pszSource, MAX_PATH-1, "%s_init:debug", g_settings.m_pszAppName?g_settings.m_pszAppName:"StreamData");

	g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "Error starting main process thread");//   Sleep(250);//(Dispatch message)
}
		//**MSG
		}


//	MB("Foo2");
	m_bInitInstanceComplete = true;
	return CWinApp::InitInstance();
}



int CStreamDataApp::ExitInstance() 
{
	g_ptabmsgr = NULL;

	// kill all the autodatas.

/*
	char errorstring[MAX_MESSAGE_LENGTH];
	int nIndex = 0;
	while(nIndex<g_data.m_nStreamData)
	{
		if((g_data.m_ppStreamData)&&(g_data.m_ppStreamData[nIndex]))
		{

			if((g_data.m_ppStreamData[nIndex]->m_ulFlags&STREAMDATA_STATUS_COUNTING)||(g_data.m_ppStreamData[nIndex]->m_bStreamDataStarted))
			{
				int nWait =  clock()+1500;
				g_data.m_ppStreamData[nIndex]->m_bKillStreamData = true;

//				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
//						g_data.m_ppStreamData[nIndex]->m_pszDesc?g_data.m_ppStreamData[nIndex]->m_pszDesc:g_data.m_ppStreamData[nIndex]->m_pszName);  
//					g_ptabmsgr->DM(MSG_ICONINFO, NULL, "StreamData:remove_autodata", errorstring);  // Sleep(20);  //(Dispatch message)

				
				//									g_data.m_ppStreamData[nTemp]->m_pDlg->OnDisconnect();


				//**** disconnect
//								while(g_data.m_ppStreamData[nIndex]->m_bConnThreadStarted) Sleep(1);

				while(
							 (
								 (g_data.m_ppStreamData[nIndex]->m_bStreamDataStarted)
							 )
							&&
							 (clock()<nWait)
						 )
				{
					Sleep(1);
				}

			}
		}
		nIndex++;
	}
	*/
	g_data.m_bTimeCodeThreadKill = true;

	while(g_data.m_bTimeCodeThreadStarted) Sleep(10);

	// can't do the following, get a forever stall
//	if((g_dlldata.thread)&&(g_dlldata.thread[0]))
//	{
//		while(g_dlldata.thread[0]->bThreadStarted ) Sleep(10);
//	}


			EnterCriticalSection(&g_settings.m_crit);
	g_settings.Settings(false);
			LeaveCriticalSection(&g_settings.m_crit);

  CoUninitialize(); //XML
	
	return CWinApp::ExitInstance();
}
/*
void ExecuteOpportunitiesThread(void* pvArgs)
{
	g_data.m_bDoingOpp = true;

	bool bDone = false;  // only do it once per primary event
	while((g_dlldata.thread)&&(g_dlldata.thread[0])&&(!g_dlldata.thread[0]->bKillThread)&&(!g_data.m_bDoingOppCancel)&&(g_data.m_bDoingOpp)&&(g_data.m_Output.m_b_list_is_playing)&&(!bDone))
	{


		//get the time and see if it is time yet

		_timeb timestamp;
		_ftime(&timestamp);
		// use system time
		g_data.m_Output.m_n_current_servertime = ((timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))%86400)*1000 + timestamp.millitm;
		g_data.m_Output.m_n_current_systemtime =g_data.m_Output.m_n_current_servertime;

		if(g_data.m_Output.m_n_current_servertime > g_data.m_Output.m_n_current_primary_start+g_data.m_Output.m_n_current_primary_duration - (g_settings.m_nCrawlDurationMS+g_settings.m_nCrawlPaddingMS))
		{
			bDone=true;  // no time left.
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "ExecuteOpportunitiesThread ending, no time left in current event %d %d %d %d %d",
															g_data.m_Output.m_n_current_servertime,
															g_data.m_Output.m_n_current_primary_start,
															g_data.m_Output.m_n_current_primary_duration,
															g_settings.m_nCrawlDurationMS,
															g_settings.m_nCrawlPaddingMS
															); // Sleep(250); //(Dispatch message)

		}
		else
		if( 
			  (g_data.m_Output.m_n_timeofnext_secondarybegin_or_priend - g_data.m_Output.m_n_timeofcurrent_secondaryend_or_pribegin) > 
				(g_settings.m_nCrawlDurationMS+g_settings.m_nCrawlPaddingMS+g_settings.m_nCrawlPaddingMS)
			)
		{
			if(
				  (g_data.m_Output.m_n_current_servertime>g_data.m_Output.m_n_timeofcurrent_secondaryend_or_pribegin+g_settings.m_nCrawlPaddingMS)
				&&(g_data.m_Output.m_n_current_servertime<g_data.m_Output.m_n_timeofnext_secondarybegin_or_priend-(g_settings.m_nCrawlDurationMS+g_settings.m_nCrawlPaddingMS))
				)
			{
				// there is enough time
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "ExecuteOpportunitiesThread executing item %d", g_data.m_pCrawlEvents);  Sleep(250); //(Dispatch message)
				if(g_data.m_pCrawlEvents)
				{
					g_data.PlayEvent(&g_data.m_pCrawlEvents, NULL);

if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "ExecuteOpportunitiesThread ending, executed item"); // Sleep(250); //(Dispatch message)
				}
				else
				{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "ExecuteOpportunitiesThread ending, no items to execute"); // Sleep(250); //(Dispatch message)
				}
				bDone=true;  // did it once.  enough.
			}
		}
		else
		{
			Sleep(5);
		}

	}
	g_data.m_bDoingOppCancel = false;
	g_data.m_bDoingOpp = false;
}
*/
void CStreamDataApp::MB(char* pszMessage, ...)
{
	if(pszMessage)
	{
		char szMessage[2048];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, 2047, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.

//		char pszSource[MAX_PATH];
//		_snprintf(pszSource, MAX_PATH-1, "%s\r\n%s", m_pszAppName?m_pszAppName:"StreamData",szMessage );

		AfxMessageBox(szMessage);


	}
}

void MainProcessThread(void* pvArgs)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if((g_dlldata.thread)&&(g_dlldata.thread[0]))
	{
		g_dlldata.thread[0]->bThreadStarted = true;
		g_dlldata.thread[0]->nThreadState |= 1; ///started
	}
	Sleep(100); // let Tabulator seed the messenger.

	char errorstring[MAX_MESSAGE_LENGTH];
	bool bSocketReport = false;
	bool bNullEventReport = false;
	CTimeConvert timeconv; //time converison utility


	char szEventSource[MAX_PATH]; sprintf(szEventSource, "%s", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
	char szToken[MAX_PATH]; strcpy(szToken, "");

	bool bClearedOutputVars = false;

	CAutomationList LastAutoList;

	COutputDlg* pOutputDlg = NULL;
	int nLastOutput = 0;

	unsigned long ulTCOV;
	CTimeConvert tcv;

  int   LISTISPLAYING			= 0x0001;
  int   LISTTHREADING			= 0x0002;
  int   LISTINFREEZE			= 0x0004;
  int   LISTINHOLD				= 0x0008;
  int   LISTUPCOUNTING		= 0x0010;
  int   LISTRECUEING			= 0x0020;
  int   LISTWILLEND				= 0x0040;
  int   LISTWILLENDBREAK	= 0x0080;

  int   EVENTDONE					= 0x0001;
  int   EVENTPLAY					= 0x0002;
  int   EVENTPLAYEDNEXT		= 0x0004;
  int   EVENTPRE					= 0x0008;
  int   EVENTPOST					= 0x0010;
  int   EVENTIDTITLE			= 0x0020;
  int   EVENTSTDBY				= 0x0040;
  int   EVENTNOTPLAYED		= 0x0080;
  int   EVENTRANSHORT			= 0x0100;
  int   EVENTSKIPPED			= 0x0200;
  int   EVENTPREPPED			= 0x0400;
  int   EVENTNOTSWITCHED	= 0x0800;
  int   EVENTPREVIEWED		= 0x1000;
  int   EVENTROLLNEXT			= 0x2000;
  int   EVENTSHORT				= 0x4000;
  int   EVENTLONG					= 0x8000;

  int   EVENT_DONE				= (EVENTDONE|EVENTPOST|EVENTNOTPLAYED|EVENTRANSHORT|EVENTSKIPPED);


	int nClock = -1; 
	while(!g_dlldata.thread[0]->bKillThread)
	{





		if (
				 (g_data.m_pscte104MsgNationalBreak == NULL)
			 ||(g_data.m_pscte104MsgLocal060Break == NULL)
			 ||(g_data.m_pscte104MsgLocal120Break == NULL)
			 ||(g_data.m_pscte104MsgBreakEnd == NULL)
//			 ||(g_data.m_pEvents==NULL) // no events yet   // dont do this here.  it might be that there are no events!
			 )
		{
			if(g_data.m_pscte104MsgNationalBreak == NULL)
			{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Initializing National Break Template Object" );  //(Dispatch message)
				g_data.m_pscte104MsgNationalBreak = g_data.m_scte104.CreateMultiMessageObject(2, 0);
				if(g_data.m_pscte104MsgNationalBreak)
				{
					g_data.InitializeNationalBreakObject();
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Initialized National Break Template Object" );  //(Dispatch message)
				}
			}
			if(g_data.m_pscte104MsgLocal060Break == NULL)
			{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Initializing Local 060 Break Template Object" );  //(Dispatch message)
				g_data.m_pscte104MsgLocal060Break = g_data.m_scte104.CreateMultiMessageObject(2, 0);
				if(g_data.m_pscte104MsgLocal060Break)
				{
					g_data.InitializeLocal060BreakObject();
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Initialized  Local 060 Break Template Object" );  //(Dispatch message)
				}
			}
			if(g_data.m_pscte104MsgLocal120Break == NULL)
			{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Initializing Local 120 Break Template Object" );  //(Dispatch message)
				g_data.m_pscte104MsgLocal120Break = g_data.m_scte104.CreateMultiMessageObject(2, 0);
				if(g_data.m_pscte104MsgLocal120Break)
				{
					g_data.InitializeLocal120BreakObject();
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Initialized  Local 120 Break Template Object" );  //(Dispatch message)
				}
			}
			if(g_data.m_pscte104MsgBreakEnd == NULL)
			{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Initializing Break End Template Object" );  //(Dispatch message)
				g_data.m_pscte104MsgBreakEnd = g_data.m_scte104.CreateMultiMessageObject(2, 0);
				if(g_data.m_pscte104MsgBreakEnd)
				{
					g_data.InitializeBreakEndObject();
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Initialized Break End Template Object" );  //(Dispatch message)
				}
			}

		}


		// monitor connection to Tabulator
		if((g_data.m_socketTabulator == NULL)&&(!g_dlldata.thread[0]->bKillThread))
		{
			// Open connection
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing control client to %s:%d", g_settings.m_pszTabulatorHost, g_settings.m_nTabulatorPort); 
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", errorstring );  //(Dispatch message)

			while(g_data.m_bInCommand) Sleep(1);
			g_data.m_bInCommand = TRUE;
			if(g_data.m_net.OpenConnection(g_settings.m_pszTabulatorHost, g_settings.m_nTabulatorPort, &g_data.m_socketTabulator, 
				5000, 5000, errorstring)<NET_SUCCESS)
			{
				g_data.m_bInCommand = FALSE;

				if(!bSocketReport)
				{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", errorstring );  //(Dispatch message)
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error connecting Tabulator at %s:%d. %s", g_settings.m_pszTabulatorHost, g_settings.m_nTabulatorPort, errorstring); 
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", errorstring);  //(Dispatch message)
					
					g_data.SendMsg(CX_SENDMSG_ERROR, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", errorstring);
					bSocketReport = true;
				}
				g_data.m_socketTabulator = NULL;
			}
			else
			{
				g_data.m_bInCommand = FALSE;
				bSocketReport = false;
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connected to Tabulator at %s:%d", g_settings.m_pszTabulatorHost, g_settings.m_nTabulatorPort); 
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", errorstring );  //(Dispatch message)
				g_data.SendMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", errorstring);
			}
		}

		if((g_data.m_socketTabulator != NULL)&&(!g_dlldata.thread[0]->bKillThread))  // check for disconnection
		{
			if(!g_data.m_bInCommand)
			{
				timeval tv;
				tv.tv_sec = 0; 
				tv.tv_usec = 500;  // timeout value
				int nNumSockets;
				fd_set fds;

				FD_ZERO(&fds);  // Zero this out each time
				FD_SET(g_data.m_socketTabulator, &fds);

				nNumSockets = select(0, &fds, NULL, NULL, &tv);
				if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
				{
					nClock = -1;
				}
				else
				if(
						(nNumSockets==0) // 0 = timed out, -1 = error
					||(!(FD_ISSET(g_data.m_socketTabulator, &fds)))
					) 
				{ 
					nClock = -1;
				}
				else // there is recv data.
				{ // wait some delay.
					if(nClock<0)
					{
						nClock = clock();
					}
					else
					{
						if((clock() > nClock+1000)&&(!g_data.m_bInCommand))
						{
							g_data.m_bInCommand = TRUE;
							// one second and nothing - could be a disconnect - just reinit, this is unsolicited data anyway
		g_data.m_net.CloseConnection(g_data.m_socketTabulator);
		g_data.m_socketTabulator = NULL;
							g_data.m_bInCommand = FALSE;
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Lost connection from Tabulator at %s:%d", g_settings.m_pszTabulatorHost, g_settings.m_nTabulatorPort); 
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", errorstring );  //(Dispatch message)
if(!g_dlldata.thread[0]->bKillThread)
{
		g_data.SendMsg(CX_SENDMSG_ERROR, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", errorstring);
}

						}
					}
				}
			}
		}



		// make sure Tabulator events are all synchronized
		if(g_data.m_pEvents)
		{ 
			EnterCriticalSection(&(g_data.m_pEvents->m_critEvents));

			EnterCriticalSection(&g_data.m_critEventsSettings);

			int nChanges = 0;
/*
			if((g_data.m_ppOpportuneEvents)&&(g_data.m_nOpportuneEvents>0))
			{
				int oe=0;

				while(oe<g_data.m_nOpportuneEvents)
				{
					if(g_data.m_ppOpportuneEvents[oe])
					{
						if(g_data.m_ppOpportuneEvents[oe]->m_bEventTokenChanged)
						{
							nChanges+=STREAMDATA_EVENT_TOKEN; 
							break; // only need one.
						}
					}
					oe++;
				}
			}

			nChanges |= ((g_data.m_pEvents!=NULL) && ((g_data.m_pEvents->m_nLastEventMod!=g_data.m_nLastEventMod))?STREAMDATA_EVENT_MOD:0) ;
*/
			nChanges = 
				(
					(g_data.m_bNationalBreakEventChanged?STREAMDATA_EVENT_NATIONAL:0)
				 |(g_data.m_bLocal060BreakEventChanged?STREAMDATA_EVENT_LOCAL060:0)
				 |(g_data.m_bLocal120BreakEventChanged?STREAMDATA_EVENT_LOCAL120:0)
				 |(g_data.m_bBreakEndEventChanged?STREAMDATA_EVENT_BREAKEND:0) 
				 |((g_data.m_pEvents!=NULL) && ((g_data.m_pEvents->m_nLastEventMod!=g_data.m_nLastEventMod))?STREAMDATA_EVENT_MOD:0) 
				);

/*
			nChanges = g_data.m_bCrawlEventChanged?1:0;
			nChanges = 0;
			nChanges |= ((g_data.m_pEvents!=NULL) && ((g_data.m_pEvents->m_nLastEventMod!=g_data.m_nLastEventMod))?STREAMDATA_EVENT_MOD:0) ;
*/
			LeaveCriticalSection(&g_data.m_critEventsSettings);


			if( (nChanges) && (g_data.m_pEvents!=NULL) )
			{
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_EVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "checking events %d != %d, changes=0x%02x. %d events", g_data.m_nLastEventMod,g_data.m_pEvents->m_nLastEventMod, nChanges, g_data.m_pEvents->m_nEvents);
			
				if(g_data.m_pEvents->m_ppEvents)
				{
/*
	CTabulatorEventArray* m_pNationalBreakEvents;
	CTabulatorEventArray* m_pLocalBreakEvents;
	CTabulatorEventArray* m_pBreakEndEvents;

	CSCTE104MultiMessage* m_pscte104MsgNationalBreak;
	CSCTE104MultiMessage* m_pscte104MsgLocalBreak;
	CSCTE104MultiMessage* m_pscte104MsgBreakEnd;

	bool m_bNationalBreakEventChanged;
	bool m_bLocalBreakEventChanged;
	bool m_bBreakEndEventChanged;

	char* m_pszSCTE104NationalEvent;  // the event identifier of the replace ad event
	char* m_pszSCTE104LocalEvent;  // the event identifier of the replace content evnet
	char* m_pszSCTE104BreakEndEvent;  // the event identifier of the "BLOCK only" event


*/
					if(nChanges&(STREAMDATA_EVENT_NATIONAL|STREAMDATA_EVENT_MOD))
					{
EnterCriticalSection(&g_settings.m_crit);
						strcpy(szToken, g_settings.m_pszSCTE104NationalEvent?g_settings.m_pszSCTE104NationalEvent:"");
LeaveCriticalSection(&g_settings.m_crit);
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_EVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "checking events for %s", szToken);


						if(strlen(szToken)>0)
						{
							int n = g_data.RefreshTabulatorEvents( g_data.m_pNationalBreakEvents, szToken );

if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_EVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "got %d events for %s", n, szToken);

							if(n>=0)
							{
EnterCriticalSection(&g_data.m_critEventsSettings);
								g_data.m_bNationalBreakEventChanged=false; //reset
LeaveCriticalSection(&g_data.m_critEventsSettings);
							}
						}
					}
					if(nChanges&(STREAMDATA_EVENT_LOCAL060|STREAMDATA_EVENT_MOD))
					{
EnterCriticalSection(&g_settings.m_crit);
						strcpy(szToken, g_settings.m_pszSCTE104Local060Event?g_settings.m_pszSCTE104Local060Event:"");
LeaveCriticalSection(&g_settings.m_crit);
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_EVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "checking events for %s", szToken);

						if(strlen(szToken)>0)
						{
							int n = g_data.RefreshTabulatorEvents( g_data.m_pLocal060BreakEvents, szToken );

if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_EVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "got %d events for %s", n, szToken);

							if(n>=0)
							{
EnterCriticalSection(&g_data.m_critEventsSettings);
								g_data.m_bLocal060BreakEventChanged=false; //reset
LeaveCriticalSection(&g_data.m_critEventsSettings);
							}
						}
					}
					if(nChanges&(STREAMDATA_EVENT_LOCAL120|STREAMDATA_EVENT_MOD))
					{
EnterCriticalSection(&g_settings.m_crit);
						strcpy(szToken, g_settings.m_pszSCTE104Local120Event?g_settings.m_pszSCTE104Local120Event:"");
LeaveCriticalSection(&g_settings.m_crit);
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_EVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "checking events for %s", szToken);

						if(strlen(szToken)>0)
						{
							int n = g_data.RefreshTabulatorEvents( g_data.m_pLocal120BreakEvents, szToken );

if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_EVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "got %d events for %s", n, szToken);

							if(n>=0)
							{
EnterCriticalSection(&g_data.m_critEventsSettings);
								g_data.m_bLocal120BreakEventChanged=false; //reset
LeaveCriticalSection(&g_data.m_critEventsSettings);
							}
						}
					}
					if(nChanges&(STREAMDATA_EVENT_BREAKEND|STREAMDATA_EVENT_MOD))
					{
EnterCriticalSection(&g_settings.m_crit);
						strcpy(szToken, g_settings.m_pszSCTE104BreakEndEvent?g_settings.m_pszSCTE104BreakEndEvent:"");
LeaveCriticalSection(&g_settings.m_crit);
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_EVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "checking events for %s", szToken);

						if(strlen(szToken)>0)
						{
							int n = g_data.RefreshTabulatorEvents( g_data.m_pBreakEndEvents, szToken );

if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_EVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "got %d events for %s", n, szToken);

							if(n>=0)
							{
EnterCriticalSection(&g_data.m_critEventsSettings);
								g_data.m_bBreakEndEventChanged=false; //reset
LeaveCriticalSection(&g_data.m_critEventsSettings);
							}
						}
					}
				}
				else
				{
					if(g_data.m_pNationalBreakEvents) 
					{
						EnterCriticalSection(&(g_data.m_pNationalBreakEvents->m_critEvents));
						g_data.m_pNationalBreakEvents->DeleteEvents();
						LeaveCriticalSection(&(g_data.m_pNationalBreakEvents->m_critEvents));
					}

					if(g_data.m_pLocal060BreakEvents) 
					{
						EnterCriticalSection(&(g_data.m_pLocal060BreakEvents->m_critEvents));
						g_data.m_pLocal060BreakEvents->DeleteEvents();
						LeaveCriticalSection(&(g_data.m_pLocal060BreakEvents->m_critEvents));
					}

					if(g_data.m_pLocal120BreakEvents) 
					{
						EnterCriticalSection(&(g_data.m_pLocal120BreakEvents->m_critEvents));
						g_data.m_pLocal120BreakEvents->DeleteEvents();
						LeaveCriticalSection(&(g_data.m_pLocal120BreakEvents->m_critEvents));
					}

					if(g_data.m_pBreakEndEvents) 
					{
						EnterCriticalSection(&(g_data.m_pBreakEndEvents->m_critEvents));
						g_data.m_pBreakEndEvents->DeleteEvents();
						LeaveCriticalSection(&(g_data.m_pBreakEndEvents->m_critEvents));
					}
				}
				g_data.m_nLastEventMod=g_data.m_pEvents->m_nLastEventMod;
				// safety reset;
			EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bNationalBreakEventChanged=false;
				g_data.m_bLocal060BreakEventChanged=false;
				g_data.m_bLocal120BreakEventChanged=false;
				g_data.m_bBreakEndEventChanged=false;
			LeaveCriticalSection(&g_data.m_critEventsSettings);

			}

			LeaveCriticalSection(&(g_data.m_pEvents->m_critEvents));
			bNullEventReport = false;
		}
		else
		{
			if(!bNullEventReport)
			{
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_EVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "Tabulator events pointer was NULL" );

				bNullEventReport = true;
			}
		}


// need to refresh stuff when things change.
// ReplacementRules
// DescriptionPattern
// ClassificationRules
///// ********************
// note!  these values are init.  other changed from interface are signaled
// by the interface sending a COM object command processed in DLLCtrl.
// need a way, sometime soon, to detect changes from automated scripts.

		EnterCriticalSection(&g_data.m_critTableData);
		
//		if(g_data.m_nReplacementRules<0)		g_data.GetReplacementRules();
//		if(g_data.m_nDescriptionPatterns<0) g_data.GetDescriptionPatterns();
//		if(g_data.m_nClassificationRules<0) g_data.GetClassificationRules();
//		if(g_data.m_nOpportuneEvents<0) g_data.GetOpportuneEvents();
//		if(g_data.m_nExclusionRules<0) g_data.GetExclusionRules();
		
		LeaveCriticalSection(&g_data.m_critTableData);

	// need to refresh stuff when things change.
	// Automation events

		EnterCriticalSection(&g_data.m_critAutomationEvents);
		int nReturn = g_data.GetAutomationListStatus();
		LeaveCriticalSection(&g_data.m_critAutomationEvents);
	// get automation events, and analyze them

		if( nReturn >= TABULATOR_SUCCESS )
		{
			// if the state indicates no connection, I should purge all the events, clear the output variables
			if(
				  (g_data.m_AutoList.m_nState < 0 )
				||(g_data.m_AutoList.m_nChange < 0 )
				||(g_data.m_AutoList.m_nDisplay < 0 )
				||(g_data.m_AutoList.m_nSysChange < 0 )
				||(g_data.m_AutoList.m_dblLastUpdate < 0.0 )
				)
			{
				// purge.
				g_data.m_bAutoAnalysisThreadKill = true;

				EnterCriticalSection(&g_data.m_critAutomationEvents);
				if((g_data.m_ppAutoEvents != NULL)&&(g_data.m_nAutoEvents>0))
				{
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_AUTOEVENTS)) 
{
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "Deleting %d automation events", g_data.m_nAutoEvents);
//	Sleep(500);
}
					int i=0;
					while(i<g_data.m_nAutoEvents)
					{
/*
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_AUTOEVENTS)) 
{
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "Deleting automation event %d", i);
	Sleep(500);
}
*/
						if(g_data.m_ppAutoEvents[i]) delete g_data.m_ppAutoEvents[i];
/*
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_AUTOEVENTS)) 
{
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "Deleted automation event %d", i);
	Sleep(500);
}
*/
						i++;
					}
					delete [] g_data.m_ppAutoEvents;
					g_data.m_ppAutoEvents=NULL;
					g_data.m_nAutoEvents = 0;

if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_AUTOEVENTS)) 
{
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "Deleted automation events");
//	Sleep(500);
}
				}
				LeaveCriticalSection(&g_data.m_critAutomationEvents);


				if(!bClearedOutputVars)
				{

				// purge output variables.

if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_AUTOEVENTS)) 
{
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "Clearing output variables");
//	Sleep(500);
}

					EnterCriticalSection(&g_data.m_critOutputVariables);
					g_data.ClearOutputVariables();
					LeaveCriticalSection(&g_data.m_critOutputVariables);
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_AUTOEVENTS)) 
{
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "Cleared output variables");
//	Sleep(500);
}
					bClearedOutputVars = true;

				}

		if((g_settings.m_bShowDiagnosticWindow)&&(pOutputDlg)&&(g_dlldata.thread)&&(g_dlldata.thread[0])&&(!g_dlldata.thread[0]->bKillThread)&&((nLastOutput<clock()-1666)||(nLastOutput>clock())||(g_data.m_AutoList.m_nState != LastAutoList.m_nState)) )
		{
//				AfxMessageBox("updating");
			nLastOutput = clock();
//					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "updating  ********");//   Sleep(250);//(Dispatch message)
			pOutputDlg->UpdateOutputDialog();
//					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "updated  ********");//   Sleep(250);//(Dispatch message)
//				AfxMessageBox("updated");
		}

				LastAutoList = g_data.m_AutoList;
		
			}
			else
			{
				if(
					  (g_data.m_AutoList.m_nState != LastAutoList.m_nState)
					||(g_data.m_AutoList.m_nChange != LastAutoList.m_nChange)
					||(g_data.m_AutoList.m_nDisplay != LastAutoList.m_nDisplay)
					||(g_data.m_AutoList.m_nCount != LastAutoList.m_nCount)
					)
				{
					g_data.m_bAutoAnalysisThreadKill = true; // kill analysis where it is.


if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_AUTOLISTVARS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "AUTOMATION list vars: state:%x change:%d display:%d count:%d", 
									g_data.m_AutoList.m_nState,
									g_data.m_AutoList.m_nChange,
									g_data.m_AutoList.m_nDisplay,
									g_data.m_AutoList.m_nCount
								);


					if(g_settings.m_nAutoListGetDelayMS>0)
					{
						// delay!
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_AUTOLISTVARS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "delaying %d ms for event get", g_settings.m_nAutoListGetDelayMS);

						_timeb timeNow;
						_timeb timeDelay;
						_ftime(&timeDelay);
						_ftime(&timeNow);

						timeDelay.time += g_settings.m_nAutoListGetDelayMS/1000;
						timeDelay.millitm += g_settings.m_nAutoListGetDelayMS%1000;
						while(timeDelay.millitm>999)
						{
							timeDelay.time++;
							timeDelay.millitm -= 1000;
						}

						while (
										(	
											(timeNow.time<timeDelay.time)
										||((timeNow.time==timeDelay.time)&&(timeNow.millitm<timeDelay.millitm))
										)
									&&(g_dlldata.thread[0])&&(!g_dlldata.thread[0]->bKillThread)
									)
						{
							Sleep(1);
							_ftime(&timeNow);
						}
	
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_AUTOLISTVARS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "delayed %d ms for event get", g_settings.m_nAutoListGetDelayMS);
					}


				// if the state is OK but different, I should pull the events
					EnterCriticalSection(&g_data.m_critAutomationEvents);
					nReturn = g_data.GetAutomationEvents();

if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_AUTOEVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "Returned %d automation events", nReturn);


					if( nReturn >= TABULATOR_SUCCESS )
					{
						// do the output variables and kick off the long term analysis thread.
						if(g_data.m_AutoList.m_nState & LISTISPLAYING)
						{
							bClearedOutputVars = false;
							
							EnterCriticalSection(&g_data.m_critOutputVariables);


							// NOT for serial tones.  never overwrite the current interval until a new one is found.
//							g_data.ClearOutputVariables(); // need to clear these, refresh secondaries and so forth.

							// do the main output vars here.
							g_data.m_Output.m_b_list_is_playing = true;  // automation list is playing a primary event.

							bool bAllOutputVarsFound = false;
							bool bTonesOutputVarsFound = false;
							bool bSCTE104OutputVarsFound = false;
							bool bPlayingPriFound = false;

							int i=0;
							int nPlayIndex = -1;
							int nPriTime=-1;
							int nPriDur=-1;
							int nPriIndex=-1;

							int nInpointIndex=-1;
							int nOutpointIndex=-1;

							int nNationalInpointIndex=-1;
							int nNationalOutpointIndex=-1;

							int nAccumulatedLocalDur=0;
							int nAccumulatedNationalDur=0;
							int nAccumulatedTonesDur=0;

							bool bInsideNationalInterval = false;
							bool bInsideTonesInterval = false;


							while((i<g_data.m_nAutoEvents)&&(!bAllOutputVarsFound))
							{

								if(g_data.m_ppAutoEvents[i])
								{
									if(bPlayingPriFound)
									{
										if(((g_data.m_ppAutoEvents[i]->m_usType)&128)==0 )
										{
											nPriTime = (int)g_data.m_ppAutoEvents[i]->m_ulOnAirTimeMS;
											nPriIndex = i;
											nPriDur = (int)g_data.m_ppAutoEvents[i]->m_ulDurationMS;

											if(bInsideNationalInterval)
											{
												nAccumulatedNationalDur += nPriDur;
											}
											if(bInsideTonesInterval)
											{
												nAccumulatedTonesDur += nPriDur;
											}
										}
										else
										{
											if((g_settings.m_nLocalHarrisTagSecEventType<0)||(g_data.m_ppAutoEvents[i]->m_usType==g_settings.m_nLocalHarrisTagSecEventType))
											{
												// check it, this may be a tag.
												if((g_settings.m_pszSCTE104Local060ID)&&(g_data.m_ppAutoEvents[i]->m_pszID))
												{
													if(strcmp(g_settings.m_pszSCTE104Local060ID, g_data.m_ppAutoEvents[i]->m_pszID)==0)
													{
														nAccumulatedLocalDur += 60000;

	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: Local 060 found at %d, accumulated local dur = %d", i,nAccumulatedLocalDur );
	}

													}
												}
												if((g_settings.m_pszSCTE104Local120ID)&&(g_data.m_ppAutoEvents[i]->m_pszID))
												{
													if(strcmp(g_settings.m_pszSCTE104Local120ID, g_data.m_ppAutoEvents[i]->m_pszID)==0)
													{
														nAccumulatedLocalDur += 120000;

	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: Local 120 found at %d, accumulated local dur = %d", i,nAccumulatedLocalDur );
	}
													}
												}
											}



											bool bTagMatched=false;
											// secondary, calculate the deal.
											// FIRST, FOR TONES
											if((g_settings.m_nHarrisTagSecEventType<0)||(g_data.m_ppAutoEvents[i]->m_usType==g_settings.m_nHarrisTagSecEventType))
											{
												// check it, this may be a tag.

												if((g_settings.m_pszInpointID)&&(g_data.m_ppAutoEvents[i]->m_pszID))
												{
													if(strcmp(g_settings.m_pszInpointID, g_data.m_ppAutoEvents[i]->m_pszID)==0)
													{
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: DTMF inpoint %s found at %d", g_data.m_ppAutoEvents[i]->m_pszID, i );
	}

														if(nPriTime>=0) // need a valid time. (otherwise it could be a non-done orphaned secondary at the top, so skip.
														{
															nInpointIndex = i; 

															bInsideTonesInterval = true;
															nAccumulatedTonesDur += nPriDur;
															
															if(g_settings.m_bUseInpointSecTiming)
															{
																g_data.m_Output.m_n_next_start_tagged_event_start = g_data.m_ppAutoEvents[i]->m_ulOnAirTimeMS;
																nAccumulatedTonesDur -= (g_data.m_ppAutoEvents[i]->m_ulOnAirTimeMS - nPriTime);
															}
															else 
															{
																g_data.m_Output.m_n_next_start_tagged_event_start = nPriTime;
															}

															bTagMatched=true; //continue;  // move to next event.
														}
													}
												}
												
												if(!bTagMatched)
												{
													if((g_settings.m_pszOutpointID)&&(g_data.m_ppAutoEvents[i]->m_pszID))
													{
														if(strcmp(g_settings.m_pszOutpointID, g_data.m_ppAutoEvents[i]->m_pszID)==0)
														{
															nOutpointIndex = i; 

															if(nInpointIndex<0)
															{
																// found an outpoint before an inpoint, do nothing
																// we are in an interval currently, do not override output data


																// actually, if on init, we can skip ahead since there is nothing to override.
																// so on init, m_Output.m_n_next_end_tagged_event_start = -1;

																// and also if we have played the on air time of the primary or secondary already, we are technically out of the interval, so also skip;

																if(
																	  // init condition
																	  (g_data.m_Output.m_n_next_end_tagged_event_start<0)
																	||(
																	    // outpoint primary has played
																			((nPlayIndex == nPriIndex)&&(!g_settings.m_bUseOutpointSecTiming))// this is the playing event.
																			// outpoint secondary has played
																		||((nPlayIndex == nPriIndex)&&(g_settings.m_bUseOutpointSecTiming)&&(g_data.m_Output.m_n_current_servertime>=(int)g_data.m_ppAutoEvents[i]->m_ulOnAirTimeMS))// this is the playing event.
																		)
																	)

																{
																	nOutpointIndex = -1;  //reset
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: DTMF Outpoint %s found at %d, prior to inpoint, skipping", g_data.m_ppAutoEvents[i]->m_pszID, i );
	}
																}
																else
																{

																	bTonesOutputVarsFound = true; 

	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: DTMF Outpoint %s found at %d, prior to inpoint", g_data.m_ppAutoEvents[i]->m_pszID, i );
	}
																}

															}
															else
															{
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: DTMF Outpoint %s found at %d", g_data.m_ppAutoEvents[i]->m_pszID, i );
	}

																if(nPriTime>=0) // need a valid time. (otherwise it could be a non-done orphaned secondary at the top, so skip.
																{
																	if(bInsideTonesInterval)
																	{
																		nAccumulatedTonesDur -= nPriDur; // had counted this primary, but want to un-count it now.
																	}

																	// inpoint was found prior.
																	if(g_settings.m_bUseOutpointSecTiming)
																	{
																		g_data.m_Output.m_n_next_end_tagged_event_start = g_data.m_ppAutoEvents[i]->m_ulOnAirTimeMS;
																		nAccumulatedTonesDur += (g_data.m_ppAutoEvents[i]->m_ulOnAirTimeMS - nPriTime);
																	}
																	else 
																	{
																		g_data.m_Output.m_n_next_end_tagged_event_start = nPriTime;
																	}

																	/// now calc the interval
																	g_data.m_Output.m_n_next_tagged_interval_accumulated_duration = nAccumulatedTonesDur;

																	g_data.m_Output.m_n_next_tagged_interval_subtracted_duration =
																		g_data.m_Output.m_n_next_end_tagged_event_start - g_data.m_Output.m_n_next_start_tagged_event_start;

																	// we probably wrapped midnight.  So lets add the right number of milliseconds.
																	while(g_data.m_Output.m_n_next_tagged_interval_subtracted_duration<0)
																		g_data.m_Output.m_n_next_tagged_interval_subtracted_duration += 86400000;

																	while(g_data.m_Output.m_n_next_tagged_interval_subtracted_duration>=8640000)
																		g_data.m_Output.m_n_next_tagged_interval_subtracted_duration -= 86400000;
														
																	bTonesOutputVarsFound = true; 



	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: DTMF duration-based Interval duration: %d ms", g_data.m_Output.m_n_next_tagged_interval_accumulated_duration);
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: DTMF on air time Interval duration: %d ms", g_data.m_Output.m_n_next_tagged_interval_subtracted_duration);
		if(g_data.m_Output.m_n_next_tagged_interval_accumulated_duration != g_data.m_Output.m_n_next_tagged_interval_subtracted_duration)
		{
			int nDiff = abs(g_data.m_Output.m_n_next_tagged_interval_accumulated_duration-g_data.m_Output.m_n_next_tagged_interval_subtracted_duration);
			if((g_settings.m_bRoundToNearestUnit)&&(nDiff > (g_settings.m_nDurationUnitDivisor/2)))
			{
				g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *X* OUTPUT: DTMF Interval duration calculations differ by %d ms", nDiff	);
			}
			else
			{
				g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     * * OUTPUT: DTMF Interval duration calculations differ by %d ms", nDiff	);
			}
		}

		tcv.MStoBCDHMSF(g_data.m_Output.m_n_next_start_tagged_event_start, &ulTCOV,(double)g_settings.m_nFrameRate); // using ideal
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: DTMF interval start   %08x %d", ulTCOV, g_data.m_Output.m_n_next_start_tagged_event_start);
		tcv.MStoBCDHMSF(g_data.m_Output.m_n_next_end_tagged_event_start, &ulTCOV,(double)g_settings.m_nFrameRate); // using ideal
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: DTMF interval end     %08x %d", ulTCOV, g_data.m_Output.m_n_next_end_tagged_event_start);

	}



																}
															}
														}
													}
												}
											}
//THEN, FOR SCTE 104;

											bTagMatched=false;
											if((g_settings.m_nNationalHarrisTagSecEventType<0)||(g_data.m_ppAutoEvents[i]->m_usType==g_settings.m_nNationalHarrisTagSecEventType))
											{
												// check it, this may be a tag.

												if((g_settings.m_pszSCTE104NationalInpointID)&&(g_data.m_ppAutoEvents[i]->m_pszID))
												{
													if(strcmp(g_settings.m_pszSCTE104NationalInpointID, g_data.m_ppAutoEvents[i]->m_pszID)==0)
													{
														if(nPriTime>=0) // need a valid time. (otherwise it could be a non-done orphaned secondary at the top, so skip.
														{
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: National Inpoint %s found at %d", g_data.m_ppAutoEvents[i]->m_pszID, i );
	}

															nNationalInpointIndex = i; 

															bInsideNationalInterval = true;
															nAccumulatedNationalDur += nPriDur;

															if(g_settings.m_bUseNationalInpointSecTiming)
															{
																g_data.m_Output.m_n_next_start_tagged_national_event_start = g_data.m_ppAutoEvents[i]->m_ulOnAirTimeMS;
																nAccumulatedNationalDur -= (g_data.m_ppAutoEvents[i]->m_ulOnAirTimeMS - nPriTime);
															}
															else 
															{
																g_data.m_Output.m_n_next_start_tagged_national_event_start = nPriTime;
															}

															bTagMatched=true; //continue;  // move to next event.
														}
													}
												}
												
												if(!bTagMatched)
												{
													if((g_settings.m_pszSCTE104NationalOutpointID)&&(g_data.m_ppAutoEvents[i]->m_pszID))
													{
														if(strcmp(g_settings.m_pszSCTE104NationalOutpointID, g_data.m_ppAutoEvents[i]->m_pszID)==0)
														{
															nNationalOutpointIndex = i; 

															if(nNationalInpointIndex<0)
															{
																// found an outpoint before an inpoint, do nothing
																// we are in an interval currently, do not override output data

																// actually, if on init, we can skip ahead since there is nothing to override.
																// so on init, m_Output.m_n_next_end_tagged_national_event_start = -1;

																
																if(
																	  // init condition
																	  (g_data.m_Output.m_n_next_end_tagged_national_event_start<0)
																	||(
																	    // outpoint primary has played
																			((nPlayIndex == nPriIndex)&&(!g_settings.m_bUseNationalOutpointSecTiming))// this is the playing event.
																			// outpoint secondary has played
																		||((nPlayIndex == nPriIndex)&&(g_settings.m_bUseNationalOutpointSecTiming)&&(g_data.m_Output.m_n_current_servertime>=(int)g_data.m_ppAutoEvents[i]->m_ulOnAirTimeMS))// this is the playing event.
																		)
																	)
																{
																	nNationalOutpointIndex = -1;  //reset
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: National Outpoint %s found at %d, prior to inpoint, skipping", g_data.m_ppAutoEvents[i]->m_pszID, i );
	}
																}
																else
																{

																	bSCTE104OutputVarsFound = true; 

	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: National Outpoint %s found at %d, prior to inpoint", g_data.m_ppAutoEvents[i]->m_pszID, i );
	}
																}

															}
															else
															{
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: National Outpoint %s found at %d", g_data.m_ppAutoEvents[i]->m_pszID, i );
	}
																if(nPriTime>=0) // need a valid time. (otherwise it could be a non-done orphaned secondary at the top, so skip.
																{
																	if(bInsideNationalInterval)
																	{
																		nAccumulatedNationalDur -= nPriDur; // had counted this primary, but want to un-count it now.
																	}

																	// inpoint was found prior.
																	if(g_settings.m_bUseNationalOutpointSecTiming)
																	{
																		g_data.m_Output.m_n_next_end_tagged_national_event_start = g_data.m_ppAutoEvents[i]->m_ulOnAirTimeMS;
																		nAccumulatedNationalDur += (g_data.m_ppAutoEvents[i]->m_ulOnAirTimeMS - nPriTime);
																	}
																	else 
																	{
																		g_data.m_Output.m_n_next_end_tagged_national_event_start = nPriTime;
																	}

																	/// now calc the interval
																	g_data.m_Output.m_n_next_tagged_national_interval_accumulated_duration = nAccumulatedNationalDur - nAccumulatedLocalDur;

																	g_data.m_Output.m_n_next_tagged_national_interval_subtracted_duration =
																		g_data.m_Output.m_n_next_end_tagged_national_event_start - g_data.m_Output.m_n_next_start_tagged_national_event_start;

																	// we probably wrapped midnight.  So lets add the right number of milliseconds.
																	while(g_data.m_Output.m_n_next_tagged_national_interval_subtracted_duration<0)
																		g_data.m_Output.m_n_next_tagged_national_interval_subtracted_duration += 86400000;

																	while(g_data.m_Output.m_n_next_tagged_national_interval_subtracted_duration>=8640000)
																		g_data.m_Output.m_n_next_tagged_national_interval_subtracted_duration -= 86400000;


																	g_data.m_Output.m_n_next_tagged_national_interval_subtracted_duration -= nAccumulatedLocalDur;
														
																	bSCTE104OutputVarsFound = true; 


	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
	{
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: National duration-based Interval duration: %d ms", g_data.m_Output.m_n_next_tagged_national_interval_accumulated_duration);
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: National on air time Interval duration: %d ms", g_data.m_Output.m_n_next_tagged_national_interval_subtracted_duration);

		if(g_data.m_Output.m_n_next_tagged_national_interval_accumulated_duration != g_data.m_Output.m_n_next_tagged_national_interval_subtracted_duration)
		{
			int nDiff = abs(g_data.m_Output.m_n_next_tagged_national_interval_accumulated_duration-g_data.m_Output.m_n_next_tagged_national_interval_subtracted_duration);

			// just going to intentionally use the Tones divisor here, to flag the difference as an error ...
			if((g_settings.m_bRoundToNearestUnit)&&(nDiff > (g_settings.m_nDurationUnitDivisor/2)))
			{
				g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *X* OUTPUT: National Interval duration calculations differ by %d ms", nDiff	);
			}
			else
			{
				g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     * * OUTPUT: National Interval duration calculations differ by %d ms", nDiff	);
			}
		}

		tcv.MStoBCDHMSF(g_data.m_Output.m_n_next_start_tagged_event_start, &ulTCOV,(double)g_settings.m_nFrameRate); // using ideal
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: National interval start   %08x %d", ulTCOV, g_data.m_Output.m_n_next_start_tagged_national_event_start);
		tcv.MStoBCDHMSF(g_data.m_Output.m_n_next_end_tagged_event_start, &ulTCOV,(double)g_settings.m_nFrameRate); // using ideal
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: National interval end     %08x %d", ulTCOV, g_data.m_Output.m_n_next_end_tagged_national_event_start);

	}



																}
															}
														}
													}
												}
											}


										}
									}
									else
									{
										if((!(g_data.m_ppAutoEvents[i]->m_usStatus & EVENT_DONE))&&(g_data.m_ppAutoEvents[i]->m_usStatus&EVENTPLAY)&&((g_data.m_ppAutoEvents[i]->m_usType&128)==0 )) //primary!
										{
											nPriTime = (int)g_data.m_ppAutoEvents[i]->m_ulOnAirTimeMS;
											bPlayingPriFound = true;
											nPlayIndex = i;
											nPriIndex = i;
											nPriDur = (int)g_data.m_ppAutoEvents[i]->m_ulDurationMS;
										// don't look at done events.

											// this IS our playing primary

											// dont have these, screw it, use TC.


											g_data.GetServerTime();

/*
		_timeb timestamp;
		_ftime(&timestamp);

			// just get "now"
			// get the current TC.
			if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
			{
				unsigned long ulTimeCode;
				EnterCriticalSection(&g_data.m_critTimeCode);
				ulTimeCode = g_data.m_ulTimeCode;
	//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
				LeaveCriticalSection(&g_data.m_critTimeCode);

				g_data.m_Output.m_n_current_servertime = g_data.m_timeconv.BCDHMSFtoMS(ulTimeCode, ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)); // make it into ideal ms.
				g_data.m_Output.m_n_current_systemtime =g_data.m_Output.m_n_current_servertime;
				
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_TIMECODE)) 
 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "AutomationData:***** **", "TC %d", g_data.m_Output.m_n_current_servertime); // Sleep(250); //(Dispatch message)


			}
			else
			{xxx
				// use system time
				g_data.m_Output.m_n_current_systemtime = ((timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))%86400)*1000 + timestamp.millitm;
				g_data.m_Output.m_n_current_systemtime = g_data.m_Output.m_n_current_servertime;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_TIMECODE)) 
 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "AutomationData:***** **", "TC sys %d", g_data.m_Output.m_n_current_servertime); // Sleep(250); //(Dispatch message)
			}
*/
	 
//	g_data.m_Output.m_b_in_secondary=false; 


//if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_TIMECODE)) 
//g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "AutomationData:***** **", "chicken"); Sleep(250); //(Dispatch message)



											// here, current means the currently playing primary event.
											g_data.m_Output.m_n_current_primary_start=(int)g_data.m_ppAutoEvents[i]->m_ulOnAirTimeMS;			// in ideal MS; 
											g_data.m_Output.m_n_current_primary_duration=(int)g_data.m_ppAutoEvents[i]->m_ulDurationMS;		// in ideal MS; 
//if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_TIMECODE)) 
//g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "AutomationData:***** **", "chicken2"); Sleep(250); //(Dispatch message)

//	g_data.m_Output.m_n_timeofnext_secondarybegin_or_priend=g_data.m_Output.m_n_current_primary_start+g_data.m_Output.m_n_current_primary_duration;  // in ideal MS; 
//	g_data.m_Output.m_n_timeofcurrent_secondaryend_or_pribegin=g_data.m_Output.m_n_current_primary_start;  // in ideal MS; 

if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
{
	tcv.MStoBCDHMSF(g_data.m_Output.m_n_current_primary_start, &ulTCOV,(double)g_settings.m_nFrameRate); // using ideal
	g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "  --- OUTPUT assignment: current_primary_start    = %08x %d", ulTCOV, g_data.m_Output.m_n_current_primary_start);
	tcv.MStoBCDHMSF(g_data.m_Output.m_n_current_primary_duration, &ulTCOV,(double)g_settings.m_nFrameRate); // using ideal
	g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "  --- OUTPUT assignment: current_primary_duration = %08x %d", ulTCOV, g_data.m_Output.m_n_current_primary_duration);

/*	tcv.MStoBCDHMSF(g_data.m_Output.m_n_timeofnext_secondarybegin_or_priend, &ulTCOV,(double)g_settings.m_nFrameRate); // using ideal
	g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "  --- OUTPUT assignment: timeofnext_secondarybegin_or_priend = %08x %d", ulTCOV, g_data.m_Output.m_n_timeofnext_secondarybegin_or_priend);
	tcv.MStoBCDHMSF(g_data.m_Output.m_n_timeofcurrent_secondaryend_or_pribegin, &ulTCOV,(double)g_settings.m_nFrameRate); // using ideal
	g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "  --- OUTPUT assignment: timeofcurrent_secondaryend_or_pribegin = %08x %d", ulTCOV, g_data.m_Output.m_n_timeofcurrent_secondaryend_or_pribegin);
*/
//Sleep(250);
}


											if(g_data.m_Output.m_psz_current_primary_ID) free(g_data.m_Output.m_psz_current_primary_ID);
											g_data.m_Output.m_psz_current_primary_ID=NULL;
											if(g_data.m_ppAutoEvents[i]->m_pszID)
											{
												g_data.m_Output.m_psz_current_primary_ID = (char*)malloc(strlen(g_data.m_ppAutoEvents[i]->m_pszID)+1);
												if(g_data.m_Output.m_psz_current_primary_ID) strcpy(g_data.m_Output.m_psz_current_primary_ID, g_data.m_ppAutoEvents[i]->m_pszID);
											}

if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
{
	g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "  --- OUTPUT assignment: current_primary_ID = %s", g_data.m_Output.m_psz_current_primary_ID?g_data.m_Output.m_psz_current_primary_ID:"(null)");
//Sleep(250);
}
											if(g_data.m_Output.m_psz_current_primary_description) free(g_data.m_Output.m_psz_current_primary_description);
											g_data.m_Output.m_psz_current_primary_description=NULL;
											if(g_data.m_ppAutoEvents[i]->m_pszTitle)
											{
												g_data.m_Output.m_psz_current_primary_description = (char*)malloc(strlen(g_data.m_ppAutoEvents[i]->m_pszTitle)+1); 
												if(g_data.m_Output.m_psz_current_primary_description) strcpy(g_data.m_Output.m_psz_current_primary_description, g_data.m_ppAutoEvents[i]->m_pszTitle);
											}

if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
{
	g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "  --- OUTPUT assignment: current_primary_description = %s", g_data.m_Output.m_psz_current_primary_description?g_data.m_Output.m_psz_current_primary_description:"(null)");
//Sleep(250);
}

										}  // was playing!
									} // playing was not found yet.
									
								}// event exists

								if((bTonesOutputVarsFound)&&(bSCTE104OutputVarsFound)) bAllOutputVarsFound = true;

								i++;
							}

							LeaveCriticalSection(&g_data.m_critOutputVariables);



					LeaveCriticalSection(&g_data.m_critAutomationEvents);

/*
// remove this for now, just a nicety.

							g_data.m_bAutoAnalysisThreadKill = false;
							if(_beginthread(AutomationAnalysisThread, 0, (void*)(NULL))==-1)
							{
							//error.
					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "Error starting autoamtion analysis thread");//   Sleep(250);//(Dispatch message)
							//**MSG
							}
*/
						}
						else //  otherwise, list is not playing, so must purge output variables
						{
					LeaveCriticalSection(&g_data.m_critAutomationEvents);

							if(!bClearedOutputVars)
							{
								EnterCriticalSection(&g_data.m_critOutputVariables);
								g_data.ClearOutputVariables(); // true excludes the "current" long for data, that can stay
								LeaveCriticalSection(&g_data.m_critOutputVariables);
								bClearedOutputVars = true;
							}
						}


if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OUTPUTVARS)) 
{
	g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: List playing %d", g_data.m_Output.m_b_list_is_playing);

	g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: Current_primary_ID %s", g_data.m_Output.m_psz_current_primary_ID?g_data.m_Output.m_psz_current_primary_ID:"(null)");
	g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: Current_primary_description %s", g_data.m_Output.m_psz_current_primary_description?g_data.m_Output.m_psz_current_primary_description:"(null)");

	tcv.MStoBCDHMSF(g_data.m_Output.m_n_current_primary_start, &ulTCOV,(double)g_settings.m_nFrameRate); // using ideal
	g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: Current_primary_start      %08x %d", ulTCOV, g_data.m_Output.m_n_current_primary_start);
	tcv.MStoBCDHMSF(g_data.m_Output.m_n_current_primary_duration, &ulTCOV,(double)g_settings.m_nFrameRate); // using ideal
	g_ptabmsgr->DM(MSG_ICONINFO, NULL, szEventSource, "     *   OUTPUT: Current_primary_duration   %08x %d", ulTCOV, g_data.m_Output.m_n_current_primary_duration);

}


		if((g_settings.m_bShowDiagnosticWindow)&&(pOutputDlg)&&(g_dlldata.thread)&&(g_dlldata.thread[0])&&(!g_dlldata.thread[0]->bKillThread)&&((nLastOutput<clock()-1666)||(nLastOutput>clock())||(g_data.m_AutoList.m_nState != LastAutoList.m_nState)) )
		{
//				AfxMessageBox("updating");
			nLastOutput = clock();
//					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "updating  ********");//   Sleep(250);//(Dispatch message)
			pOutputDlg->UpdateOutputDialog();
//					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "updated  ********");//   Sleep(250);//(Dispatch message)
//				AfxMessageBox("updated");
		}

						LastAutoList = g_data.m_AutoList;

					}
					else
					{
					LeaveCriticalSection(&g_data.m_critAutomationEvents);

					}
				}
			}

/*
	m_nState=-1;
	m_nChange=-1;
	m_nDisplay=-1;
	m_nSysChange=-1;
	m_nCount=-1;
	m_nLookahead=-1;
	m_dblLastUpdate = -1.0;
*/



		}




		if((g_settings.m_bShowDiagnosticWindow)&&(pOutputDlg==NULL)&&(g_dlldata.thread)&&(g_dlldata.thread[0])&&(!g_dlldata.thread[0]->bKillThread))
		{
			pOutputDlg = new COutputDlg;

			if(pOutputDlg)
			{
//				AfxMessageBox("new");
				if(pOutputDlg->Create(AfxGetApp()->GetMainWnd()))
				{
	//				AfxMessageBox("created");
					pOutputDlg->ShowWindow(SW_SHOW);
					pOutputDlg->SetWindowPos(&pOutputDlg->wndTopMost, 0,0,0,0, SWP_NOSIZE);

//				AfxMessageBox("created and shown");
					pOutputDlg->UpdateOutputDialog();

				}
				else
				{
//					AfxMessageBox("Problem");
					delete pOutputDlg;
					pOutputDlg = NULL;
				}
			}
		}



		if((!g_settings.m_bShowDiagnosticWindow)&&(pOutputDlg)&&(g_dlldata.thread)&&(g_dlldata.thread[0])&&(!g_dlldata.thread[0]->bKillThread))
		{
				pOutputDlg->ShowWindow(SW_HIDE);
//						g_data.m_pOutputDlg->OnExit();
				delete pOutputDlg;
				pOutputDlg = NULL;
		}


		if(!g_dlldata.thread[0]->bKillThread) Sleep(15);
	}


	if(pOutputDlg)
	{
//		AfxMessageBox("hiding");
		pOutputDlg->ShowWindow(SW_HIDE);

//		AfxMessageBox("destroying");
//		g_data.m_pOutputDlg->OnExit();
//		AfxMessageBox("destroyed");
		delete pOutputDlg;
		pOutputDlg = NULL;
	}


	g_data.m_bTimeCodeThreadKill = true;
	g_data.m_bAutoAnalysisThreadKill=true;

	while((g_data.m_bTimeCodeThreadStarted)||(g_data.m_bAutoAnalysisThreadStarted))
	{
		Sleep(10);
	}

	if(g_data.m_socketTabulator)
	{
		// I could send the "disconnect" command to Tabulator so it does not throw an error.... someday
		CNetData data;
		// just disconnect
		data.m_ucCmd = TABULATOR_CMD_HELLO;
		data.m_ucType = NET_TYPE_PROTOCOL1; // has data but no subcommand.
		int n = g_data.m_net.SendData(&data, g_data.m_socketTabulator, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT);//|NET_SND_NO_RX);
		if(n >= NET_SUCCESS)
		{
			g_data.m_net.SendData(NULL, g_data.m_socketTabulator, 5000, 0, NET_SND_CLNTACK, NULL);
		}
		g_data.m_net.CloseConnection(g_data.m_socketTabulator);
		g_data.m_socketTabulator = NULL;

		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnected from Tabulator at %s:%d", g_settings.m_pszTabulatorHost, g_settings.m_nTabulatorPort); 
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", errorstring );  //(Dispatch message)
		g_data.SendMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", errorstring);
	}

	Sleep(100);//let messages get there.

	g_ptabmsgr = NULL;

	if((g_dlldata.thread)&&(g_dlldata.thread[0]))
	{
		g_dlldata.thread[0]->bThreadStarted = false;
		g_dlldata.thread[0]->nThreadState &= ~1; ///started
	}
}


void AutomationAnalysisThread(void* pvArgs)
{
	CTimeConvert timeconv; //time converison utility

  int   EVENTDONE					= 0x0001;
  int   EVENTPLAY					= 0x0002;
  int   EVENTPLAYEDNEXT		= 0x0004;
  int   EVENTPRE					= 0x0008;
  int   EVENTPOST					= 0x0010;
  int   EVENTIDTITLE			= 0x0020;
  int   EVENTSTDBY				= 0x0040;
  int   EVENTNOTPLAYED		= 0x0080;
  int   EVENTRANSHORT			= 0x0100;
  int   EVENTSKIPPED			= 0x0200;
  int   EVENTPREPPED			= 0x0400;
  int   EVENTNOTSWITCHED	= 0x0800;
  int   EVENTPREVIEWED		= 0x1000;
  int   EVENTROLLNEXT			= 0x2000;
  int   EVENTSHORT				= 0x4000;
  int   EVENTLONG					= 0x8000;

  int   EVENT_DONE				= (EVENTDONE|EVENTPOST|EVENTNOTPLAYED|EVENTRANSHORT|EVENTSKIPPED);

	g_data.m_bAutoAnalysisThreadStarted=true;

	int i=0;
	while((!g_data.m_bAutoAnalysisThreadKill)&&(g_data.m_ppAutoEvents != NULL)&&(g_data.m_nAutoEvents>0)&&(i<g_data.m_nAutoEvents))
	{
		EnterCriticalSection(&g_data.m_critAutomationEvents);
		if((!g_data.m_bAutoAnalysisThreadKill)&&(g_data.m_ppAutoEvents != NULL)&&(g_data.m_nAutoEvents>0)&&(i<g_data.m_nAutoEvents)) // check all this again when in the critsec, we own it now.
		{
			if(!(g_data.m_ppAutoEvents[i]->m_usStatus & (EVENT_DONE)))// don't look at done events
			{
				// check out this event.
//				g_data.m_ppAutoEvents[i]->m_ulClassification = g_data.GetClassification(g_data.m_ppAutoEvents[i]);

				// then do all these other thigns to estimate what is going to happen later.
				// we will save this for Phase 2



			}
		}
		LeaveCriticalSection(&g_data.m_critAutomationEvents);
		i++;
	}


	g_data.m_bAutoAnalysisThreadStarted=false;
}

/*
void TimerExpiryDelayThread(void* pvArgs)
{
	int nWait= clock();
	int nTimerExpiry = 0;
	
	EnterCriticalSection(&g_data.m_critSyncVariables);
	if(g_data.m_Sync.m_nLastTimerEventType & (STREAMDATA_MANUAL_TYPE_ADREPL|STREAMDATA_MANUAL_TYPE_CONTENTREPL))
	{
		nTimerExpiry = g_settings.m_nTimerExpiryManualDelayMS;
	}
	else
	{		
		nTimerExpiry = g_settings.m_nTimerExpiryAutoDelayMS;
	}
	LeaveCriticalSection(&g_data.m_critSyncVariables);

	nWait += nTimerExpiry;

	while((clock()<nWait)&&(!g_dlldata.thread[0]->bKillThread)){  Sleep(0); }

	if(!g_dlldata.thread[0]->bKillThread)
	{
		// recheck state...
		if(g_data.CheckState()==STREAMDATA_STATE_AUTO)  // have to check on timer expiry
		{
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_TIMER)) 
 g_ptabmsgr->DM(MSG_ICONINFO, NULL, "StreamData:debug", "Delayed %d ms after timer expiry", nTimerExpiry);//   Sleep(250);//(Dispatch message)
				// do it now.
	EnterCriticalSection(&g_data.m_critPlayEvent);  // need to enforce one at a time.
				int nReturn = g_data.Replace(true, true);
	LeaveCriticalSection(&g_data.m_critPlayEvent);  // need to enforce one at a time.
	// Just do the AUTO replace thingie with current time value replacement.  This came from GTP.  no params.
	//puts it into the AUTO state.

			if(nReturn<TABULATOR_SUCCESS)
			{
 g_ptabmsgr->DM(MSG_ICONERROR, NULL, "StreamData:debug", "Error %d on Replace after timer expiry", nReturn);//   Sleep(250);//(Dispatch message)
			}

		}
		else
		{
			// log that manual event cannot proceed because state is not AUTO.
			//TODO
	g_data.SendMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "A timer expiry event was discontinued because the state was not AUTO.");
		}
	}
	g_data.m_Sync.m_bTimerActionPending=false;

}
*/




void DelayDTMFThread(void* pvArgs)
{
	if(pvArgs == NULL) return;
	COutputVariables* pov = (COutputVariables*)pvArgs;

	_timeb now;
	_timeb tick;

	char pszSource[MAX_PATH];
	_snprintf(pszSource, MAX_PATH-1, "%s:DTMF_Delay", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");

	//	pov->m_n_current_primary_start = clock(); // override with a "now" value.  in case critsecs take time, we can compensate
	// no, could not do this.
//								pov->m_n_current_primary_start = now.time;
//								pov->m_n_next_start_tagged_national_event_start = now.millitm; // since not used otherwise, only usingtaged duration for DTMF, not national tagged.
	if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_TRIGGER))
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "Waiting %d milliseconds starting at %d.%03d", (pov->m_n_current_primary_duration>0)?pov->m_n_current_primary_duration:0, pov->m_n_current_primary_start, pov->m_n_next_start_tagged_national_event_start);  //(Dispatch message)
	}

	tick.time = pov->m_n_current_primary_start;
	tick.millitm = pov->m_n_next_start_tagged_national_event_start;

	if(pov->m_n_current_primary_duration>0)
	{ 
		int nWait = pov->m_n_current_primary_duration;
		_ftime(&now);

		nWait = pov->m_n_current_primary_duration - (now.time-tick.time)*1000 - (now.millitm-tick.millitm);

		if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_TRIGGER))
		{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "Wait is now %d milliseconds %d %d", nWait, (nWait/1000), (nWait%1000));  //(Dispatch message)
		}

		if(nWait>0)  // in case we already waited that amount or more.
		{
			tick.time += (nWait/1000);
			tick.millitm += (nWait%1000);
			if(tick.millitm > 999)
			{
				tick.time ++;
				tick.millitm -= 1000;
			}
		}

		while(
			     (g_dlldata.thread)&&(g_dlldata.thread[0])&&(!g_dlldata.thread[0]->bKillThread)
				 &&(
				     (now.time < tick.time)
					 ||((now.time == tick.time)&&(now.millitm < tick.millitm))
					 )
				 )
		{
			Sleep(1);
			_ftime(&now);
		}

		if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_TRIGGER))
		{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "timecheck: %d %d %d  %d.%03d %d.%03d %d",
					(g_dlldata.thread), (g_dlldata.thread[0]), (!g_dlldata.thread[0]->bKillThread),
					 now.time, now.millitm,   
					 tick.time, tick.millitm, clock()
					);  //(Dispatch message)
		}

	}




	if((g_dlldata.thread)&&(g_dlldata.thread[0])&&(g_dlldata.thread[0]->bKillThread))
	{
		if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_TRIGGER))
		{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "DTMF aborted");  //(Dispatch message)
		}
		return; // was killed, dont proceed
	}

	if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_TRIGGER))
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "Delay period concluded");  //(Dispatch message)
	}


/////////////////////
//this is the code from the send DTMF above.  no time to abstact it into a function that both things call.  just copy for right now. and modify inline

/*				if(strcmp(pch, "Send_DTMF")==0)  // Promotor wants to send the next dtmf string now.
					{
						nReturn = CLIENT_ERROR;
						if(!g_settings.m_bEnableTones)
						{
							szData.Format("DTMF tones are not enabled");
							char* pchReturn =  (char*)malloc(szData.GetLength()+1);
							if(pchReturn)
							{
								sprintf(pchReturn, "%s", szData);
								(*ppvoid) = pchReturn;
							}
						}
						else
						{  // has to be enabled to get here, so remove all this.

*/
							EnterCriticalSection(&g_data.m_critOutputVariables);
							g_data.GetServerTime(true); // calc only!
//							COutputVariables ov = g_data.m_Output;
							pov->m_n_current_systemtime =	g_data.m_Output.m_n_current_systemtime;
							pov->m_n_current_servertime =	g_data.m_Output.m_n_current_servertime;
							LeaveCriticalSection(&g_data.m_critOutputVariables);

//							CString szData;

//							ov.m_psz_current_primary_ID = NULL; // nulled because not used, do not want actual string freed
//							ov.m_psz_current_primary_description = NULL;//nulled because not used, do not want actual string freed

							int nDur = g_settings.m_bTonesUseDurationAddition?pov->m_n_next_tagged_interval_accumulated_duration:pov->m_n_next_tagged_interval_subtracted_duration;

							if(nDur<=0)
							{
								// no tones to send.
								// err msg
//								szData.Format("No interval detected; DTMF string cannot be formatted");
//								char* pchReturn =  (char*)malloc(szData.GetLength()+1);
//								if(pchReturn)
//								{
//									sprintf(pchReturn, "%s", szData);
//									(*ppvoid) = pchReturn;
//								}

								// asrun.
	if(g_settings.m_bAsRunDTMF)
	{
				if(pov->m_n_current_servertime>=0)
				{
					int nHours;
					int nMinutes;
					int nSeconds;
					int nFrames;

					g_data.m_timeconv.MStoHMSF(pov->m_n_current_servertime, &nHours, &nMinutes, &nSeconds, &nFrames, 
						(/*g_settings.m_bDF?29.97:*/(double)g_settings.m_nFrameRate) // ideal
						);

					g_ptabmsgr->DM(MSG_ICONERROR, 
							(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
							g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF",
							"Error: No interval detected; DTMF string cannot be formatted at server time %02d:%02d:%02d%s%02d", 
							nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames
						);   //(Dispatch message)
				}
				else
				{
					// no server time
					g_ptabmsgr->DM(MSG_ICONERROR, 
						(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
							g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF", 
							"No interval detected; DTMF string cannot be formatted"
						);   //(Dispatch message)
				}
	}
							}
							else
							{							
								char* pchDTMF = g_data.FormatDTMF(nDur);

								if(pchDTMF)
								{
									char pszToken[MAX_PATH];

									if((g_settings.m_pszDTMFAddress)&&(strlen(g_settings.m_pszDTMFAddress)))
									{
										_snprintf(pszToken, MAX_PATH-1, "%s|%s|%s|%02x%02x%02x%02x", 
											g_settings.m_pszTabulatorPlugInModule?g_settings.m_pszTabulatorPlugInModule:"Videoframe", 
											g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF",
											pchDTMF,
											g_settings.m_ucDTMFAddress[0],
											g_settings.m_ucDTMFAddress[1],
											g_settings.m_ucDTMFAddress[2],
											g_settings.m_ucDTMFAddress[3]
											);
									}
									else
									{
										_snprintf(pszToken, MAX_PATH-1, "%s|%s|%s", 
											g_settings.m_pszTabulatorPlugInModule?g_settings.m_pszTabulatorPlugInModule:"Videoframe", 
											g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF",
											pchDTMF
											);
									}


	if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_TRIGGER))
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "Triggering with %s", pszToken);  //(Dispatch message)
	}
									unsigned char* pucDataBuffer = (unsigned char*)pszToken;
									char errorstring[DB_ERRORSTRING_LEN];
									int nReturn = g_data.SendTabulatorCommand(&pucDataBuffer, g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF", g_settings.m_bAsRunDTMF?false:true, pchDTMF, pov, errorstring);

									free(pchDTMF);

									if(nReturn>CLIENT_ERROR)
									{
										 // success
//										(*ppvoid)= NULL;
									}
									else
									{
										//err msg
//										szData.Format("Error %d sending DTMF command with %s: %s", nReturn, pszToken, errorstring);
//										char* pchReturn =  (char*)malloc(szData.GetLength()+1);
//										if(pchReturn)
//										{
//											sprintf(pchReturn, "%s", szData);
//											(*ppvoid) = pchReturn;
//										}
										// should be logged by SendTabulatorCommand
									}

/*
									if((pucDataBuffer)&&(pucDataBuffer != (unsigned char*)pszToken))
									{
										free(pucDataBuffer);
									}
*/
								}
								else
								{
									//err msg
//									szData.Format("No DTMF format available for duration %d", nDur);
//									char* pchReturn =  (char*)malloc(szData.GetLength()+1);
//									if(pchReturn)
//									{
//										sprintf(pchReturn, "%s", szData);
//										(*ppvoid) = pchReturn;
//									}
	if(g_settings.m_bAsRunDTMF)
	{
				if(pov->m_n_current_servertime>=0)
				{
					int nHours;
					int nMinutes;
					int nSeconds;
					int nFrames;

					g_data.m_timeconv.MStoHMSF(pov->m_n_current_servertime, &nHours, &nMinutes, &nSeconds, &nFrames, 
						(/*g_settings.m_bDF?29.97:*/(double)g_settings.m_nFrameRate) // ideal
						);

					g_ptabmsgr->DM(MSG_ICONERROR, 
							(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
							g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF", 
							"Error: No DTMF format available for duration %d at server time %02d:%02d:%02d%s%02d", 
							nDur,
							nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames
						);   //(Dispatch message)
				}
				else
				{
					// no server time
					g_ptabmsgr->DM(MSG_ICONERROR, 
						(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
							g_settings.m_pszSendDTMFToken?g_settings.m_pszSendDTMFToken:"Send_DTMF", 
							"Error: No DTMF format available for duration %d", 
							nDur
						);   //(Dispatch message)
				}

	}
								}
							}
						//}  // if enabled
//					}  //DTMF





//////////////////////

									
	delete pov;

	
}

