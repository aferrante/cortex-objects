// StreamDataCore.h: interface for the CStreamDataCore class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_StreamDataCORE_H__D426F8FF_BC44_4257_869A_044999574215__INCLUDED_)
#define AFX_StreamDataCORE_H__D426F8FF_BC44_4257_869A_044999574215__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../../../Common/TXT/BufferUtil.h"
#include "../../../Common/API/Adrienne/AdrienneUtil.h"

class CStreamDataCore  
{
public:
	CStreamDataCore();
	virtual ~CStreamDataCore();

	CBufferUtil m_bu;
	CAdrienneUtil       m_tc; // time code utility (if installed - otherwise, no bother)

	BOOL IsPointInWnd(CPoint point, CWnd* pWnd);
	BOOL IsPointInRect(CPoint point, CRect rcClientRect);
  CString CALEncodeBrackets(CString szString);

};

#endif // !defined(AFX_StreamDataCORE_H__D426F8FF_BC44_4257_869A_044999574215__INCLUDED_)
