// StreamDataData.h: interface for the CStreamDataData and CStreamDataMessage classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_StreamDataDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_)
#define AFX_StreamDataDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../../../Common/LAN/NetUtil.h"
#include "../../../Cortex Objects/Tabulator/TabulatorDefines.h"
//#include "../../../Cortex Objects/Tabulator/TabulatorData.h"
#include "../../../Common/MFC/ODBC/DBUtil.h"
#include "../../../Common/TIME/TimeConvert.h"
#include "../../../Common/API/SCTE/SCTE104Util.h"
#include "../../../Common/TXT/BufferUtil.h"


#define SENDMSG_SENDER_MAXLEN 64
#define SENDMSG_MESSAGE_MAXLEN 512


// same as CNucleusEvent definition, currently.

class CTabulatorEvent  
{
public:
	CTabulatorEvent();
	virtual ~CTabulatorEvent();

	unsigned long m_ulFlags;
	double m_dblTriggerTime; 
	bool m_bAnalyzed;
	bool m_bReAnalyzed;
	int  m_nAnalyzedTriggerID;
	int  m_nEventID;
	int  m_nDestType;
	
	int m_nType;
	CString m_szName;
	CString m_szHost;
	CString m_szSource;
	CString m_szScene;
	CString m_szIdentifier;
	CString m_szValue;
	CString m_szParamDependencies;
	CString m_szDestModule;
	CString m_szLayer;
};



class CTabulatorEventArray  
{
public:
	CTabulatorEventArray();
	virtual ~CTabulatorEventArray();

	void	DeleteEvents();

	int m_nEvents;
	CTabulatorEvent** m_ppEvents;
	CRITICAL_SECTION m_critEvents;
	int m_nEventMod;
	int m_nLastEventMod;

};

class CAutomationList  
{
public:
	CAutomationList();
	virtual ~CAutomationList();

	int m_nState;
	int m_nChange;
	int m_nDisplay;
	int m_nSysChange;
	int m_nCount; 
	int m_nLookahead;
	double m_dblLastUpdate;
};


class CAutomationEvent  
{
public:
	CAutomationEvent();
	virtual ~CAutomationEvent();

 	int m_nID;
	unsigned short m_usType;
	char* m_pszID;
	char* m_pszTitle;
	char* m_pszReconcileKey;
	char* m_pszData;
	unsigned char  m_ucSegment;
	unsigned long  m_ulOnAirTimeMS;					// ideal milliseconds as appears in the list
	unsigned long  m_ulOnAirLocalUnixDate;  //in seconds, offset from January 1, 1900
	unsigned long  m_ulDurationMS;          // ideal milliseconds as appears in the list
	unsigned long  m_ulSOMMS;               // ideal milliseconds as appears in the list
	unsigned short m_usStatus;
//	unsigned short m_usControl; // was this
	unsigned long m_ulControl;  // made it this, storing revent->extrathree (which is extended event control in recfm80) in the upper word

	// potential output variables.
//	unsigned long m_ulClassification;
	char* m_pszOutputString; // like:
	// Replace Ad BLOCK (for advertisments that are the first in the pod)
	// PASS (for not-ads that do not have replacements)
	// ReplaceContent <ID1> <ID2> BLOCK (for not-ads that do not have replacements)
	// PENDING (for items not analyzed yet)
	// Timer suppressed (for items suppressed by the timer - either type)
};


class CExclusionRule
{
public:
	CExclusionRule();
	virtual ~CExclusionRule();

	//create table ExclusionRules (ID int identity(1,1), pattern varchar(128), col_name varchar(64), auto_event_type int, event_type int, valid_date decimal(20,3), expire_date decimal(20,3))  
	int m_nID;
	char* m_pszPattern;
	char* m_pszColName;
	int m_nAutoEventType;  // Automation event type, for matching
	int m_nEventType;  // internal event type, ON, OFF, or DURATION

	double m_dblValidDate;
	double m_dblExpireDate;

};

/*
class COpportuneEvent
{
public:
	COpportuneEvent();
	virtual ~COpportuneEvent();

	//create table OpportuneEvents (ID int identity(1,1), event_name varchar(64), event_duration int, valid_date decimal(20,3), expire_date decimal(20,3))  
	int m_nID;
	CTabulatorEventArray* m_pTabulatorEvents;
	char* m_pszEventToken;
	int m_nDuration;

	bool m_bEventTokenChanged;  // the event identifier token has changed

	double m_dblValidDate;
	double m_dblExpireDate;

	double m_dblLastPlayed; // sort by this to do a loop.
};


class COpportuneIntervalData
{
public:
	COpportuneIntervalData();
	virtual ~COpportuneIntervalData();

	unsigned short m_usType; // free or disallowed
	char* m_pszDescription;
	unsigned long  m_ulOffsetTimeMS;				// ideal milliseconds as appears in the list
	unsigned long  m_ulDurationMS;          // ideal milliseconds as appears in the list
};

*/


class CMessageOverrideValues
{
public:
	CMessageOverrideValues();
	virtual ~CMessageOverrideValues();

	int m_nPrerollMS;
	int m_nDurationMS;
	char* m_pszIDString; //'N' or 'L' followed by pipe character ('|') followed by the "PPL Program ID", (For example "N|74915")
	unsigned char m_uchSource;  // Local -- 0x32 (Distributor Advertisement Start)  //National -- 0x30 (Provider Advertisement Start)
};



class COutputVariables
{
public:
	COutputVariables();
	virtual ~COutputVariables();

	bool m_b_list_is_playing;  // automation list is playing a primary event.

	// here, current means the currently playing primary event.
	int m_n_current_primary_start; // in ideal MS; 
	int m_n_current_primary_duration;  //in ideal MS 
	char* m_psz_current_primary_ID; // = Harris ID of current
	char* m_psz_current_primary_description;// = Harris Title of current

	int m_n_current_systemtime; // in ideal MS; 
	int m_n_current_servertime; // in ideal MS; 
	double m_dbl_last_servertime; // in ideal MS since epoch
	double m_dbl_last_servertime_update; // in ideal MS since epoch; 

	int m_n_next_start_tagged_local_event_start; // in ideal MS; 
	int m_n_next_end_tagged_local_event_start; // in ideal MS; 
	int m_n_next_tagged_local_interval_subtracted_duration; // in ideal MS - difference of on-air times.
	int m_n_next_tagged_local_interval_accumulated_duration; // in ideal MS - accumulated durations. 
	char* m_psz_local_primary_ID; // = PPL ID of local (given by start)

	int m_n_next_start_tagged_national_event_start; // in ideal MS; 
	int m_n_next_end_tagged_national_event_start; // in ideal MS; 
	int m_n_next_tagged_national_interval_subtracted_duration; // in ideal MS - difference of on-air times.
	int m_n_next_tagged_national_interval_accumulated_duration; // in ideal MS - accumulated durations.
	char* m_psz_national_primary_ID; // = PPL ID of local (given by start)

};



class CStreamDataData  
{
public:
	CStreamDataData();
	virtual ~CStreamDataData();

	// remove pipes
	CString Encode(CString szText);
	// put back pipes
	CString Decode(CString szText);

	int InitDLLData(DLLthread_t* pData);

	CString XMLTextNodeValue(IXMLDOMNodePtr pNode);

	int SendMsg(int nType, char* pszSender, char* pszMessage, ...);
//	int SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...);
	int SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...);


//	SOCKET m_socket;
//	CRITICAL_SECTION m_critClientSocket;

	CNetUtil m_net;
	CDBUtil m_dbu;
	CSCTE104Util m_scte104;  //global just so the message number is global (not that it matters - it gets reassigned at Libretto).
	CTimeConvert m_timeconv; //time converison utility
	CBufferUtil m_bu;

	SOCKET m_socketTabulator;
	BOOL m_bInCommand;


//	CRITICAL_SECTION m_critMsg;
	CRITICAL_SECTION m_critSQL;
	CRITICAL_SECTION m_critOutputSQL;

	CTabulatorEventArray* m_pEvents;
//	CTabulatorEventArray* m_pInEvents;
//	CTabulatorEventArray* m_pOutEvents;
//	CTabulatorEventArray* m_pTextEvents;
	CRITICAL_SECTION m_critEventsSettings;



//	int TestPattern(char* pszValue, char* pszPattern);
//	int CheckTimer();
//	int CheckTimerExpiryAlreadyProcessedAutomationEvent();
//	int CheckState();

	int PlayEvent(CTabulatorEventArray** ppEvent, CMessageOverrideValues* pOverrides=NULL, char* pszInfo=NULL);
	CRITICAL_SECTION m_critPlayEvent;  // need to enforce one at a time.


	int RefreshTabulatorEvents(	CTabulatorEventArray* m_pEvents, char* pszToken );

//	int GetOpportuneEvents();
//	int GetExclusionRules();
//	int GetReplacementRules();
//	int GetDescriptionPatterns();
//	int GetClassificationRules();
	int GetAutomationEvents();
	int GetAutomationListStatus();
//	int GetClassification(CAutomationEvent* pEvent);

	int ReportAutomationEvents();

//	int Replace(bool bCurrent, bool bTimer=false);
//	int Replace_Manual(CMessageOverrideValues* pOverrides);
//	int Enter_Pass(bool bTimer=false);
//	int Enter_Block();
	int Query_Ready();
	int GetServerTime(bool bCalcOnly=false);
//	int CountCurrentReplacementRules(double dblTime); // use system time

//	int Do_Opportunities();
//	bool m_bDoingOpp;
//	bool m_bDoingOppCancel;

	int IdealAddTimeAndDuration(int nStart, int nDuration);
//	char* FormatDTMF(int nDurationMS);

	int ClearOutputVariables();
	
	bool m_bInitalEventGetSucceeded;

	int m_nLastEventMod;


	unsigned long m_ul_splice_event_id;    // most significant bit first
	unsigned short m_us_unique_program_id;  // most significant bit first

	unsigned long IncrementSpliceEventID();
	unsigned short IncrementUniqueProgID();


	CDBUtil*  m_pdb;
	CDBconn*  m_pdbConn;

	CDBUtil*  m_pdbOutput;
	CDBconn*  m_pdbOutputConn;

	CDBUtil*  m_pdbAutomation;
	CDBconn*  m_pdbAutomationConn;

//	CRITICAL_SECTION m_critTransact;

	CRITICAL_SECTION m_critTimeCode;
	unsigned long m_ulTimeCode;
	unsigned char m_ucTimeCodeBits;
	unsigned long m_ulTimeCodeElapsed;
	bool m_bTimeCodeThreadStarted;
	bool m_bTimeCodeThreadKill;

	int m_nLastTime;
	int m_nLastMillitm;
	int m_nTimeIncr;
	int m_nMillitmIncr;


//	int GetStreamDatas(char* pszInfo=NULL);
//	int FindStreamData(char* pszStreamDataName);

/*
	COpportuneEvent** m_ppOpportuneEvents;
	int m_nOpportuneEvents;
	int m_nOpportuneEventsArraySize;

	CExclusionRule** m_ppExclusionRules;
	int m_nExclusionRules;
	int m_nExclusionRulesArraySize;
*/

	CRITICAL_SECTION m_critTableData;

	CAutomationList m_AutoList;

	CAutomationEvent** m_ppAutoEvents;
	int m_nAutoEvents;
	int m_nAutoEventsArraySize;

	COutputVariables m_Output;
	CRITICAL_SECTION m_critOutputVariables;

//	CSyncVariables m_Sync;
//	CRITICAL_SECTION m_critSyncVariables;

//	CTabulatorEventArray* m_pCrawlEvents;
//	bool m_bCrawlEventChanged;


/*
//just here for the descriptions
	m_bReplaceAdEventChanged=false;  // the event identifier of the replace ad has changed
	m_bReplaceContentEventChanged=false;  // the event identifier of the replace content has changed
	m_bEventNotificationEventChanged=false;  // the event identifier of the "BLOCK only" event has changed
	m_bRejoinMainEventChanged=false;  // the event identifier of the PASS tabulator event (includes Rejoin Main) has changed
	m_bTimerEventChanged=false;  // the event identifier of the BLOCK GPO w Timer event has changed
*/
	CTabulatorEventArray* m_pNationalBreakEvents;
	CTabulatorEventArray* m_pLocalBreakEvents;
//	CTabulatorEventArray* m_pLocal060BreakEvents;
//	CTabulatorEventArray* m_pLocal120BreakEvents;
//	CTabulatorEventArray* m_pBreakEndEvents;

	CSCTE104MultiMessage* m_pscte104MsgNationalBreak;
	CSCTE104MultiMessage* m_pscte104MsgLocalBreak;
//	CSCTE104MultiMessage* m_pscte104MsgLocal060Break;
//	CSCTE104MultiMessage* m_pscte104MsgLocal120Break;
//	CSCTE104MultiMessage* m_pscte104MsgBreakEnd;

	bool m_bNationalBreakEventChanged;
	bool m_bLocalBreakEventChanged;
//	bool m_bLocal060BreakEventChanged;
//	bool m_bLocal120BreakEventChanged;
//	bool m_bBreakEndEventChanged;

	int InitializeGeneric(CSCTE104MultiMessage* m_pscte104Msg);
	int InitializeNationalBreakObject();
	int InitializeLocalBreakObject();
//	int InitializeLocal060BreakObject();
//	int InitializeLocal120BreakObject();
//	int InitializeBreakEndObject();


	int UpdateGeneric(CSCTE104MultiMessage* m_pscte104Msg, unsigned char uchSource, int nPrerollMS, int nDurationMS, char* pszIDString, bool bSuppressIncrement);
	int ParseData(char* pszData, char** ppszProgID);


	int m_nAutomationProcessID; // ID of the primary event we are processing currently, if we are.
	bool m_bAutoAnalysisThreadStarted;
	bool m_bAutoAnalysisThreadKill;

	CRITICAL_SECTION m_critAutomationEvents;

	int SendTabulatorCommand(unsigned char** ppucDataBuffer, char* pszAsRunEventID, bool bSuppressAsRun=false, char* pszDTMF=NULL, COutputVariables* pOv=NULL, char* pszInfo=NULL);

};




#endif // !defined(AFX_StreamDataDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_)
