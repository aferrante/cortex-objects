// CollectorData.cpp: implementation of the CCollectorData and CCollectorMessage classes.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Collector.h"
#include "CollectorData.h"
#include "CollectorSettings.h"
#include <atlbase.h>
#include <process.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CCollectorData	g_data;
extern CCollectorSettings	g_settings;
extern CMessager*			g_ptabmsgr;
extern DLLdata_t       g_dlldata;


void PublishEventsThread( void* pDataObj );
#ifndef REMOVE_CLIENTSERVER
void PublishDataThread(void* pDataObj);
void PublishTickerThread(void* pDataObj);
#endif //#ifndef REMOVE_CLIENTSERVER


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/*
CCollectorMessage::CCollectorMessage()
{
	m_szMessage="";
	m_szRevisedMessage="";
	m_nTimestamp =-1;
	m_nLastUpdate =-1;
	m_nID = -1;
	m_nStatus = COLLECTOR_MSG_STATUS_NONE;
	m_nType = COLLECTOR_MSG_TYPE_NONE;
	m_nPlayed = 0;
	m_dblIndex = -0.1;
}

CCollectorMessage::~CCollectorMessage()
{
}


CString CCollectorMessage::SQLUpdate(bool bTicker, double dblSeekIndex)
{
  CString szSQL="";
	if(bTicker)
	{
	char* szMsg = g_data.m_dbu.EncodeQuotes(m_szMessage);
	if(m_nStatus&COLLECTOR_MSG_STATUS_INSERTED)
	{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "upd item (%d [%s])", m_nID, m_szMessage); //(Dispatch message)
		szSQL.Format("UPDATE %s SET \
[text] = '%s', \
[sort_order] = %.4f, \
[timestamp] = %d, \
[updated] = %d, \
[status] = %d, \
[type] = %d, \
[played] = %d WHERE [id] = %d AND [sort_order] >= %.4f AND [sort_order] <= %.4f", 
					g_settings.m_pszTicker, 
					(szMsg?szMsg:m_szMessage),
					m_dblIndex,
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|COLLECTOR_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed,
					m_nID,
					dblSeekIndex-0.0000009,
					dblSeekIndex+0.0000009
					);  // table name
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "upd item (%d [%s]) SQL %s", m_nID, m_szMessage, szSQL);//(Dispatch message)
	}
	else
	{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "ins item (%d [%s])", m_nID, m_szMessage); //(Dispatch message)
		szSQL.Format("INSERT INTO %s (\
[id], \
[text], \
[sort_order], \
[timestamp], \
[updated], \
[status], \
[type], \
[played]) VALUES \
(%d, '%s', %.4f, %d, %d, %d, %d, %d)",
					g_settings.m_pszTicker,  // table name
					m_nID,
					(szMsg?szMsg:m_szMessage),
					m_dblIndex,
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|COLLECTOR_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "ins item (%d [%s]) SQL %s", m_nID, m_szMessage, szSQL); //(Dispatch message)
	}
	if(szMsg) free(szMsg);
	}
	else
	{
	char* szMsg = g_data.m_dbu.EncodeQuotes(m_szMessage);
	char* szRevMsg = g_data.m_dbu.EncodeQuotes(m_szRevisedMessage);
	if(m_nStatus&COLLECTOR_MSG_STATUS_INSERTED)
	{
		szSQL.Format("UPDATE %s SET \
[text] = '%s', \
[revision] = '%s', \
[timestamp] = %d, \
[updated] = %d, \
[status] = %d, \
[type] = %d, \
[played] = %d WHERE [id] = %d", g_settings.m_pszFeed, 
					(szMsg?szMsg:m_szMessage),
					(szRevMsg?szRevMsg:m_szRevisedMessage),
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|COLLECTOR_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed,
					m_nID);  // table name
	}
	else
	{

		szSQL.Format("INSERT INTO %s (\
[id], \
[text], \
[revision], \
[timestamp], \
[updated], \
[status], \
[type], \
[played]) VALUES \
(%d, '%s', '%s', %d, %d, %d, %d, %d)",
					g_settings.m_pszFeed,  // table name
					m_nID,
					(szMsg?szMsg:m_szMessage),
					(szRevMsg?szRevMsg:m_szRevisedMessage),
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|COLLECTOR_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed);
	}

	if(szMsg) free(szMsg);
	if(szRevMsg) free(szRevMsg);
	}

	return szSQL;
}

*/

CCollectorMessage::CCollectorMessage()
{
	m_szMessage="";//message varchar(4096),
	m_szAux1="";//aux_data1 varchar(1024),
	m_szAux2="";//aux_data2 varchar(1024), 
	m_nLastPlayed =-1;//last_played int, 
	m_nID =-1;  //id int identity(1,1), 
	m_nStatus=COLLECTOR_MSG_STATUS_NONE; //status int,
//	int m_nType;  // used to have defaults... now just use unlimited cycles, but prob want to go back to having defaults.
	m_nValidFrom=-1; //valid_from int,
	m_nExpires=-1;//expires_on int, 
	m_nPlayLimit=-1;//allowed_cycles int, 
	m_nPlayed=0;//actual_cycles int, 
	m_dblIndex=-1.0;//dwell_override decimal(20,3),
	m_dblDwellOverride=-1.0;//sort_order decimal(20,3),
	m_bActive=false;//	active int,
	m_nLastUpdate = -1;

	//m_nSourceID = -1;
}

CCollectorMessage::~CCollectorMessage()
{
}


CString CCollectorMessage::SQLUpdate(bool bTicker, double dblSeekIndex)
{
  CString szSQL="";
	if(bTicker)
	{
//	char* szMsg = g_data.m_dbu.EncodeQuotes(m_szMessage);
//	char* szAux1 = g_data.m_dbu.EncodeQuotes(m_szAux1);
//	char* szAux2 = g_data.m_dbu.EncodeQuotes(m_szAux2);

	if(1)//m_nStatus&COLLECTOR_MSG_STATUS_INSERTED)  because, we arent actually using the memory stacks, we are taking it from the db, so just update.
	{

/*
create table Collector_1_Ticker(
id int identity(1,1), 
message varchar(4096), 
aux_data1 varchar(1024), 
aux_data2 varchar(1024), 
sort_order decimal(20,3), 
dwell_override decimal(20,3), 
valid_from int, 
expires_on int, 
allowed_cycles int, 
actual_cycles int, 
active int, 
status int, 
last_played int, 
created_on int, 
created_by varchar(32), 
last_modified_on int, 
last_modified_by varchar(32));   
*/

 // ok this if is not coded, but the else is.

	//	also we dont need to do any event editing, just stats!

		if(m_nLastPlayed<0)
		{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "upd item (%d [%s])", m_nID, m_szMessage); //(Dispatch message)
		szSQL.Format("UPDATE %s SET \
[actual_cycles] = %d, \
[status] = %d WHERE [id] = %d",// AND [sort_order] >= %.4f AND [sort_order] <= %.4f", 
					g_settings.m_pszTicker, 
//					(szMsg?szMsg:m_szMessage),
//					(szAux1?szAux1:m_szAux1),
//					(szAux2?szAux2:m_szAux2),
//					m_dblIndex,
//					m_nTimestamp,
					m_nPlayed,
					m_nStatus,//|COLLECTOR_MSG_STATUS_INSERTED,
					m_nID
//					,dblSeekIndex-0.0000009,
//					dblSeekIndex+0.0000009
					);  // table name
		}
		else
		{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "upd item (%d [%s])", m_nID, m_szMessage); //(Dispatch message)
		szSQL.Format("UPDATE %s SET \
[actual_cycles] = %d, \
[status] = %d, \
[last_played] = %d WHERE [id] = %d",// AND [sort_order] >= %.4f AND [sort_order] <= %.4f", 
					g_settings.m_pszTicker, 
//					(szMsg?szMsg:m_szMessage),
//					(szAux1?szAux1:m_szAux1),
//					(szAux2?szAux2:m_szAux2),
//					m_dblIndex,
//					m_nTimestamp,
					m_nPlayed,
					m_nStatus,//|COLLECTOR_MSG_STATUS_INSERTED,
					m_nLastPlayed,
					m_nID
//					,dblSeekIndex-0.0000009,
//					dblSeekIndex+0.0000009
					);  // table name
		}
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "upd item (%d [%s]) SQL %s", m_nID, m_szMessage, szSQL);//(Dispatch message)
	}
		/*
	else
	{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "ins item (%d [%s])", m_nID, m_szMessage); //(Dispatch message)
		szSQL.Format("INSERT INTO %s (\
[id], \
[text], \
[sort_order], \
[timestamp], \
[updated], \
[status], \
[type], \
[played]) VALUES \
(%d, '%s', %.4f, %d, %d, %d, %d, %d)",
					g_settings.m_pszTicker,  // table name
					m_nID,
					(szMsg?szMsg:m_szMessage),
					m_dblIndex,
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|COLLECTOR_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "ins item (%d [%s]) SQL %s", m_nID, m_szMessage, szSQL); //(Dispatch message)
	}
	*/
//	if(szMsg) free(szMsg);
//	if(szAux1) free(szAux1);
//	if(szAux2) free(szAux2);
	}
/*
	else
	{
	char* szMsg = g_data.m_dbu.EncodeQuotes(m_szMessage);
	char* szRevMsg = g_data.m_dbu.EncodeQuotes(m_szRevisedMessage);
	if(m_nStatus&COLLECTOR_MSG_STATUS_INSERTED)
	{
		szSQL.Format("UPDATE %s SET \
[text] = '%s', \
[revision] = '%s', \
[timestamp] = %d, \
[updated] = %d, \
[status] = %d, \
[type] = %d, \
[played] = %d WHERE [id] = %d", g_settings.m_pszFeed, 
					(szMsg?szMsg:m_szMessage),
					(szRevMsg?szRevMsg:m_szRevisedMessage),
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|COLLECTOR_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed,
					m_nID);  // table name
	}
	else
	{

		szSQL.Format("INSERT INTO %s (\
[id], \
[text], \
[revision], \
[timestamp], \
[updated], \
[status], \
[type], \
[played]) VALUES \
(%d, '%s', '%s', %d, %d, %d, %d, %d)",
					g_settings.m_pszFeed,  // table name
					m_nID,
					(szMsg?szMsg:m_szMessage),
					(szRevMsg?szRevMsg:m_szRevisedMessage),
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|COLLECTOR_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed);
	}

	if(szMsg) free(szMsg);
	if(szRevMsg) free(szRevMsg);
	}
*/
	return szSQL;
}



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCollectorData::CCollectorData()
{

	m_pEvents = NULL;

	m_nMessagesDelta=0;
	m_nTickerMessagesDelta=0;
	m_bMovedItem=false;

	m_bCyclingTickerDefaults=FALSE;

//	m_bTransact = false;

	m_bPublishingData=FALSE;
	m_bPublishingTicker=FALSE;

	m_dblLastDataOutUpdate=-1.0;
	m_dblLastTickerOutUpdate=-1.0;

	m_nLastEventMod = -3;

	m_nMessagesUpcounting = 0;
	m_nTickerMessagesUpcounting = 0;
	m_bMessagesUpcounting = false;
	m_bTickerMessagesUpcounting = false;


	m_bForceRefresh=FALSE;
	m_bSortTimestampsAsc=TRUE;

	m_bFilterBad=FALSE;
	m_bFilterCommitted=FALSE;
	m_bFilterDeleted=FALSE;
	m_bFilter=FALSE;
	m_bFilterGood=FALSE;
	m_bFilterGreat=FALSE;
	m_bFilterLength=FALSE;
	m_bFilterPlayed=FALSE;
	m_bFilterUnmarked=FALSE;

	m_nMinutesComparator=-1;
	m_nLengthComparator=-1;
	m_nTimesPlayedComparator=-1;
	m_nMinutesValue=-1;
	m_nLengthValue=-1;
	m_nTimesPlayedValue=-1;

	m_dblLastDataUpdate=-2.0;
	m_dblLastTickerUpdate=-2.0;


	m_pInEvents = new CTabulatorEventArray;
	m_pOutEvents = new CTabulatorEventArray;
	m_pTextEvents = new CTabulatorEventArray;


	m_bDialogStarted = false;
	m_socket=NULL;

	m_pmsgCommit = NULL;
	m_bQueued = false;

	m_nLastID=-1;
	m_nLastFoundIndex=-1;
	m_nLastInternalID = -100;

	m_msg = NULL;
	m_nMessages=0;

	m_ticker = NULL;
	m_nTickerMessages=0;

	m_dblTickerMessageCursor=0.0;
	m_dblTickerDefaultCursor=-0.001;
	m_dblTickerMessageMax=0.0;
	m_dblTickerDefaultMax=0.0;
	m_nTickerIDMaxNeg = -100;

	m_tickersocket=NULL;

	m_pdb=NULL;
	m_pdbConn=NULL;
	InitializeCriticalSection(&m_critSQL);
	InitializeCriticalSection(&m_critEventsSettings);

	_ftime(&m_timeStart);


	m_msgout=NULL;
	m_nMessagesOut=0;
	m_tickerout=NULL;
	m_nTickerMessagesOut=0;
	m_nMessagesOutArray=0;
	m_nTickerMessagesOutArray=0;

	InitializeCriticalSection(&m_critMsgOut);
	InitializeCriticalSection(&m_critTickerOut);

	
//	InitializeCriticalSection(&m_critClientSocket);
	InitializeCriticalSection(&m_critMsg);
	InitializeCriticalSection(&m_critTicker);
	InitializeCriticalSection(&m_critMarks);
	InitializeCriticalSection(&m_critDefaultID);
	InitializeCriticalSection(&m_critTransact);

	

	m_hcurArrow=LoadCursor(NULL,IDC_ARROW);
	m_hcurNoDrop=LoadCursor(NULL, IDC_NO);
	m_hcurSize=LoadCursor(NULL,IDC_SIZEWE);
	m_hcurNS=LoadCursor(NULL,IDC_SIZENS);
	m_bDraggingCommit = FALSE;
	m_bDraggingUncommit = FALSE;
	m_bDraggingHDivider = FALSE;
	m_bDraggingVDivider = FALSE;

	m_bCommitEnabled=FALSE;
	m_szCommitData=_T("");
	m_bCommitMode=TRUE; //c.f. Uncommit.
	m_nCommitID = -1;
	m_nCommitInsertBeforeID=-1;
	m_dblCommitBeforeIndex = -0.001;
	m_bCommitDefaultSource = FALSE;
	m_dblCommitIndex  = -0.001;

	m_dwCommitSchedTime = 0;
	m_dwCommitExpiryTime = 0;

	m_bTickerReady = false;
	m_bTickerGotNext = false;
	m_pmsgPlaying=NULL;
	m_pmsgNext=NULL;

	m_bInEventChanged = false;  // the event identifier of the transition in (Server_Ticker_In) command has changed
	m_bOutEventChanged = false;  // the event identifier of the transition out (Server_Ticker_Out) command has changed
	m_bTextEventChanged = false;  // the event identifier of the text message event has changed

}

CCollectorData::~CCollectorData()
{
//	DeleteCriticalSection(&m_critClientSocket);
	DeleteCriticalSection(&m_critMsg);
	DeleteCriticalSection(&m_critTicker);
	DeleteCriticalSection(&m_critMarks);
	DeleteCriticalSection(&m_critSQL);
	DeleteCriticalSection(&m_critDefaultID);
	DeleteCriticalSection(&m_critTransact);
	DeleteCriticalSection(&m_critMsgOut);
	DeleteCriticalSection(&m_critTickerOut);
	DeleteCriticalSection(&m_critEventsSettings);
	if(	m_pInEvents) delete  m_pInEvents;
	if(	m_pOutEvents) delete  m_pOutEvents;
	if(	m_pTextEvents) delete  m_pTextEvents;

}

// remove pipes
CString CCollectorData::Encode(CString szText)
{
  CString szOut = "";
  int i;
  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '|')
      szOut += "%:";
    else if (szText[i] == '%')
      szOut += "%%";
    else if ((szText[i] != '\n')&&(szText[i] != '\r'))  //strip returns while we are at it.
       szOut += (char) szText[i];
  }
  return szOut;
}

// put back pipes
CString CCollectorData::Decode(CString szText)
{
  CString szOut = "";
  int i;

  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '%')
    {
      i++; if (i>=szText.GetLength()) break;
      if (szText[i] == '%')
      {
        szOut += "%";
      }
      else if (szText[i] == ':')
      {
        szOut += "|";
      }
    }
    else szOut += (char) szText[i];
  }
  return szOut;
}


int CCollectorData::InsertMessage(CCollectorMessage* pevent, int index)
{
	EnterCriticalSection(&m_critMsg);
	if(pevent)
	{
		CCollectorMessage** ppObj = new CCollectorMessage*[m_nMessages+1];
		if(ppObj)
		{
			CCollectorMessage** ppDelObj = m_msg;
			int o=0;
			if((m_msg)&&(m_nMessages>0))
			{
				if((index<0)||(index>=m_nMessages))
				{
					while(o<m_nMessages)
					{
						ppObj[o] = m_msg[o];
						o++;
					}
					ppObj[m_nMessages] = pevent;
					index = m_nMessages;
				}
				else
				{
					while(o<index)
					{
						ppObj[o] = m_msg[o];
						o++;
					}
					ppObj[o] = pevent;
					while(o<m_nMessages)
					{
						ppObj[o+1] = m_msg[o];
						o++;
					}
				}
			}
			else
			{
				ppObj[m_nMessages] = pevent;  // just add the one
				index = m_nMessages;
			}

			m_msg = ppObj;
			if(ppDelObj) delete [] ppDelObj;
			m_nMessages++;
	LeaveCriticalSection(&m_critMsg);
			return index;
		}
	}

	LeaveCriticalSection(&m_critMsg);
	return CLIENT_ERROR;
}

int CCollectorData::DeleteMessage(int index)  // removes from array
{
//	EnterCriticalSection(&m_critMsg);
	if((index>=0)&&(index<m_nMessages)&&(m_msg))
	{
		CCollectorMessage** ppDelObj = m_msg;
		if(m_nMessages>1)
		{
			CCollectorMessage** ppObj = new CCollectorMessage*[m_nMessages-1];
			if(ppObj)
			{
				CCollectorMessage* pDelEventObj = NULL;
				m_nMessages--;
				int o=0;
				while(o<index)
				{
					ppObj[o] = m_msg[o];
					o++;
				}
				pDelEventObj = m_msg[o];
				while(o<m_nMessages)
				{
					ppObj[o] = m_msg[o+1];
					o++;
				}

				m_msg = ppObj;
				if(ppDelObj) delete [] ppDelObj;
				if(pDelEventObj) delete pDelEventObj;
//	LeaveCriticalSection(&m_critMsg);
				return CLIENT_SUCCESS;
			}
		}
		else
		{
			m_nMessages = 0;
			m_msg = NULL;
			if(ppDelObj) delete [] ppDelObj;
//	LeaveCriticalSection(&m_critMsg);
			return CLIENT_SUCCESS;
		}
	}
//	LeaveCriticalSection(&m_critMsg);
	return CLIENT_ERROR;
}

int CCollectorData::DeleteAllMessages()  // removes entire array
{
	int nReturn = CLIENT_ERROR;
	EnterCriticalSection(&m_critMsg);
	if(m_nMessages>0) nReturn = m_nMessages;
	if(m_msg)
	{
		while(m_nMessages>0)
		{
			m_nMessages--;
			if(m_msg[m_nMessages]) delete(m_msg[m_nMessages]);
			if(m_nMessages==0)
			{
				delete [] m_msg;
				m_msg = NULL;
			}
		}
	}
	LeaveCriticalSection(&m_critMsg);

	return nReturn;
}

int CCollectorData::GetMessagesSince(int nTime, CString* pszReturn, int nSkipMax) 
{
	CString szReturn="0";
	int n=0,t=0,s=0;
	EnterCriticalSection(&m_critMsgOut);
	if(m_msgout)
	{
		CString szData="";
		while((n<m_nMessagesOut)&&(t<g_settings.m_nMaxMessages))
		{
			if((m_msgout[n])&&(m_msgout[n]->m_nLastUpdate>=nTime))
			{
				s++;
				if(s>=nSkipMax)
				{
					szData+= (char)29;
#ifndef REMOVE_CLIENTSERVER
					szData+=MessageToString(m_msgout[n]);
#endif //#ifndef REMOVE_CLIENTSERVER

					t++;
				}
			}
			n++;
		}
		szReturn.Format("%d%s", t, szData);
	}
	LeaveCriticalSection(&m_critMsgOut);
	if(pszReturn)
	{
		*pszReturn = szReturn;
		if(n<m_nMessagesOut) // means, we had max messages.  have to re-request
			return CLIENT_REFRESH;
		else
			return CLIENT_SUCCESS;
	}
	else
	 return CLIENT_ERROR;
}


int CCollectorData::InsertTickerMessage(CCollectorMessage* pevent, int index)
{
	EnterCriticalSection(&m_critTicker);
	if(pevent)
	{
		CCollectorMessage** ppObj = new CCollectorMessage*[m_nTickerMessages+1];
		if(ppObj)
		{
			CCollectorMessage** ppDelObj = m_ticker;
			int o=0;
			if((m_ticker)&&(m_nTickerMessages>0))
			{
				if((index<0)||(index>=m_nTickerMessages))
				{
					while(o<m_nTickerMessages)
					{
						ppObj[o] = m_ticker[o];
						o++;
					}
					ppObj[m_nTickerMessages] = pevent;
					index = m_nTickerMessages;
				}
				else
				{
					while(o<index)
					{
						ppObj[o] = m_ticker[o];
						o++;
					}
					ppObj[o] = pevent;
					while(o<m_nTickerMessages)
					{
						ppObj[o+1] = m_ticker[o];
						o++;
					}
				}
			}
			else
			{
				ppObj[m_nTickerMessages] = pevent;  // just add the one
				index = m_nTickerMessages;
			}

			m_ticker = ppObj;
			if(ppDelObj) delete [] ppDelObj;
			m_nTickerMessages++;
	LeaveCriticalSection(&m_critTicker);
			return index;
		}
	}

	LeaveCriticalSection(&m_critTicker);
	return CLIENT_ERROR;
}

int CCollectorData::DeleteTickerMessage(int index)  // removes from array
{
//	EnterCriticalSection(&m_critTicker);
	if((index>=0)&&(index<m_nTickerMessages)&&(m_ticker))
	{
		CCollectorMessage** ppDelObj = m_ticker;
		if(m_nTickerMessages>1)
		{
			CCollectorMessage** ppObj = new CCollectorMessage*[m_nTickerMessages-1];
			if(ppObj)
			{
				CCollectorMessage* pDelEventObj = NULL;
				m_nTickerMessages--;
				int o=0;
				while(o<index)
				{
					ppObj[o] = m_ticker[o];
					o++;
				}
				pDelEventObj = m_ticker[o];
				while(o<m_nTickerMessages)
				{
					ppObj[o] = m_ticker[o+1];
					o++;
				}

				m_ticker = ppObj;
				if(ppDelObj) delete [] ppDelObj;
				if(pDelEventObj) delete pDelEventObj;
//	LeaveCriticalSection(&m_critTicker);
				return CLIENT_SUCCESS;
			}
		}
		else
		{
			m_nTickerMessages = 0;
			m_ticker = NULL;
			if(ppDelObj) delete [] ppDelObj;
//	LeaveCriticalSection(&m_critTicker);
			return CLIENT_SUCCESS;
		}
	}
//	LeaveCriticalSection(&m_critTicker);
	return CLIENT_ERROR;
}

int CCollectorData::DeleteAllTickerMessages(int nMode)  // removes entire array
{
	int nReturn = CLIENT_ERROR;
	if(nMode<0)
	{
		EnterCriticalSection(&m_critTicker);
		if(m_nTickerMessages>0) nReturn = m_nTickerMessages;
		while((m_nTickerMessages>0)&&(m_ticker))
		{
			m_nTickerMessages--;
			if(m_ticker[m_nTickerMessages]) delete(m_ticker[m_nTickerMessages]);
			if(m_nTickerMessages==0)
			{
				delete [] m_ticker;
				m_ticker = NULL;
			}
		}
		LeaveCriticalSection(&m_critTicker);
	}
	else
	{
		int nDel=0, i=0, j=0;
		EnterCriticalSection(&m_critTicker);
		if(m_ticker)
		{
			while(i<m_nTickerMessages)
			{
				if(m_ticker[i])
				{
					if(
						  ((nMode==0)&&(m_ticker[i]->m_nID>0))
						||((nMode==1)&&(m_ticker[i]->m_nID<0))
						)
					{
						delete(m_ticker[i]);
						nDel++;
					}
					else
					{
						if(i>j)
						{
							m_ticker[j] = m_ticker[i]; // move it up.
						}
						j++;
					}
				}

				i++;
			}
			m_nTickerMessages = j;
			if(m_nTickerMessages==0)
			{
				delete [] m_ticker;
				m_ticker = NULL;
			}
		}
		LeaveCriticalSection(&m_critTicker);
	}
	return nReturn;
}

int CCollectorData::GetTickerMessagesSince(int nTime, CString* pszReturn, int nSkipMax) 
{
	CString szReturn="0";
	int n=0,t=0,s=0;
	EnterCriticalSection(&m_critTickerOut);
	if(m_tickerout)
	{
		CString szData="";
		while((n<m_nTickerMessagesOut)&&(t<g_settings.m_nMaxMessages))
		{
			if((m_tickerout[n])&&(m_tickerout[n]->m_nLastUpdate>=nTime))
			{
				s++;
				if(s>=nSkipMax)
				{
					szData+= (char)29;
#ifndef REMOVE_CLIENTSERVER
					szData+=MessageToString(m_tickerout[n]);
#endif //#ifndef REMOVE_CLIENTSERVER
					
					t++;
				}
			}
			n++;
		}
		szReturn.Format("%d%s", t, szData);
	}
	LeaveCriticalSection(&m_critTickerOut);
	if(pszReturn)
	{
		*pszReturn = szReturn;
		if(n<m_nTickerMessagesOut) // means, we had max messages.  have to re-request
			return CLIENT_REFRESH;
		else
			return CLIENT_SUCCESS;
	}
	else
	 return CLIENT_ERROR;
}

int CCollectorData::FindTickerID(int nID, double dblIndex, int nStartIndex)
{
	EnterCriticalSection(&m_critTicker);
	if((m_ticker)&&(m_nTickerMessages))
	{
		if((nStartIndex<0)||(nStartIndex>=m_nTickerMessages)) nStartIndex=0;
		int n=nStartIndex;
		BOOL bDone = false; 
		BOOL bFirst = true; 
		while(!bDone)
		{
			while(((n<m_nTickerMessages)&&(bFirst))||((n<nStartIndex)&&(!bFirst)))
			{
				if(m_ticker[n])
				{
					if(m_ticker[n]->m_nID==nID) 
					{
//	LeaveCriticalSection(&m_critTicker);
//						return n;
// /*// just do by Id for now  // was going to but it breaks it somehow
						if(dblIndex > -0.0001)
						{
							if((m_ticker[n]->m_dblIndex>=dblIndex-0.0000009)&&(m_ticker[n]->m_dblIndex<=dblIndex+0.0000009)) 
							{
	LeaveCriticalSection(&m_critTicker);
								return n;
							}
						}
						else
						{
	LeaveCriticalSection(&m_critTicker);
							return n;
						}
// */
					} 
				}
				n++;
			}
			if((nStartIndex>0)&&(bFirst))
			{
				n=0;
			}
			else
			{
				bDone = true;
			}
			bFirst = false; 
		}
	}
	LeaveCriticalSection(&m_critTicker);
	return CLIENT_ERROR;
}



CString CCollectorData::XMLTextNodeValue(IXMLDOMNodePtr pNode)
{
	CString szText = _T("");
	if(pNode)
	{
		IXMLDOMNodeListPtr pValues = pNode->GetchildNodes();
		if (pValues != NULL)
		{
			IXMLDOMNodePtr pValue;
			while(pValue = pValues->nextNode())
			{
				DOMNodeType valnodeType;
				pValue->get_nodeType(&valnodeType);

				if(valnodeType == NODE_TEXT)
				{
USES_CONVERSION;
					szText = W2T(_bstr_t(pValue->GetnodeValue()));
					szText.TrimLeft();
					szText.TrimRight();
					return szText;
				}
			}
		}
	}
	return szText;
}

int CCollectorData::FindID(int nID, int nStartIndex)
{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Collector_DataDownload", "entering"); Sleep(100); //(Dispatch message)
	EnterCriticalSection(&m_critMsg);
	if((m_msg)&&(m_nMessages))
	{
		if((nStartIndex<0)||(nStartIndex>=m_nMessages)) nStartIndex=0;
		int n=nStartIndex;
		BOOL bDone = false; 
		BOOL bFirst = true; 
		while(!bDone)
		{
			while(((n<m_nMessages)&&(bFirst))||((n<nStartIndex)&&(!bFirst)))
			{

				if(m_msg[n])
				{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Collector_DataDownload", "%d=?%d", m_msg[n]->m_nID, nID);   Sleep(100);//(Dispatch message)
					if(m_msg[n]->m_nID==nID) 
					{
	LeaveCriticalSection(&m_critMsg);
						return n;
					} 
				}
				n++;
			}
			if((nStartIndex>0)&&(bFirst))
			{
				n=0;
			}
			else
			{
				bDone = true;
			}
			bFirst = false; 
		}
	}
	LeaveCriticalSection(&m_critMsg);
	return CLIENT_ERROR;
}



CCollectorMessage* CCollectorData::GetNextTickerMessage(CDBUtil* pdb, CDBconn* pdbConn, CCollectorMessage** ppPlaying, char* pszInfo)
{
	CCollectorMessage* pmsg = NULL;

	if((pdb)&&(pdbConn))
	{
		char szTickerSource[MAX_PATH]; sprintf(szTickerSource, "%s_GetNextTickerMessage", g_settings.m_pszInternalAppName);
		_timeb now;  //needed for expiration dates
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_SQLSTRING_MAXLEN];

			// read in events 
			/*
			get playing event -
			
			get top 1 events where status&playing, sort by index desc; // gets latest playing event

			if none
				get top 1 events where !default, sort by index;
			else
				get top 1 events where !default and index>playing, sort by index;

			if none, and playing event exists and not a default
				get top 1 events where default and index>cursor, sort by index;		
				if!none(cursor = default index
			else
				cursor - playing index
			*/


/*
create table Collector_1_Ticker(id int identity(1,1), message varchar(4096), aux_data1 varchar(1024), aux_data2 varchar(1024), 
sort_order  decimal(20,3), dwell_override decimal(20,3), valid_from int, expires_on int, allowed_cycles int, actual_cycles int, active int, 
status int, last_played int, created_on int, created_by varchar(32), last_modified_on int, last_modified_by varchar(32)); 
*/


		// need to find the last of the playing events, so sort descending
// this worked, but no sensitivity to allowed cycles or expiration date.  ah, but dont need that to find playing event, see next event stuff below

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 \
[id], [message], [aux_data1], [aux_data2], [sort_order], [dwell_override], [valid_from], [expires_on], [allowed_cycles], [actual_cycles], [active], \
[status], [last_played] FROM %s WHERE [status] & %d <> 0 AND [active] > 0 ORDER BY [sort_order] DESC", //HARDCODE
						(((g_settings.m_pszTicker )&&(strlen(g_settings.m_pszTicker )))?g_settings.m_pszTicker:"Collector_1_Ticker"),
						COLLECTOR_MSG_STATUS_PLAYING
						);

if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_SQL)) 
  g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "PLAYING: %s",szSQL);  //(Dispatch message)

					CCollectorMessage* pPlaying = NULL;
					if(ppPlaying) *ppPlaying = NULL;
					CRecordset* prs = pdb->Retrieve(pdbConn, szSQL, errorstring);
					bool bPlaying = false;
					if(prs)
					{
						CString szTemp;
						int nReturn = CLIENT_ERROR;
						int nIndex = 0;
						if (!prs->IsEOF())
						{
							try
							{
								pPlaying =  new CCollectorMessage;
								if(pPlaying)
								{
									if(ppPlaying) *ppPlaying = pPlaying;
									prs->GetFieldValue("id", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pPlaying->m_nID = atoi(szTemp);
										if((	m_nTickerIDMaxNeg>pPlaying->m_nID)&&(pPlaying->m_nID<0)) // defaults have negative IDs
										{
	EnterCriticalSection(&m_critDefaultID);
											m_nTickerIDMaxNeg=pPlaying->m_nID;
	LeaveCriticalSection(&m_critDefaultID);
										}


										prs->GetFieldValue("message", pPlaying->m_szMessage);//HARDCODE
										prs->GetFieldValue("aux_data1", pPlaying->m_szAux1);//HARDCODE
										prs->GetFieldValue("aux_data2", pPlaying->m_szAux2);//HARDCODE
										prs->GetFieldValue("sort_order", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pPlaying->m_dblIndex = atof(szTemp);

											if(	m_dblTickerDefaultMax<pPlaying->m_dblIndex) m_dblTickerDefaultMax=pPlaying->m_dblIndex;
										}
//if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_SQL))  g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "A");  //(Dispatch message)

										prs->GetFieldValue("dwell_override", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pPlaying->m_dblDwellOverride = atof(szTemp);
											if(pPlaying->m_dblDwellOverride<0.001) pPlaying->m_dblDwellOverride = -1.0;
										}
										else pPlaying->m_dblDwellOverride = -1.0;

//if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_SQL))  g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "B");  //(Dispatch message)
										prs->GetFieldValue("valid_from", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pPlaying->m_nValidFrom = atoi(szTemp);
										}
										else pPlaying->m_nValidFrom = -1;
							
//if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_SQL))  g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "C");  //(Dispatch message)
										prs->GetFieldValue("expires_on", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pPlaying->m_nExpires = atoi(szTemp);
										}
										else pPlaying->m_nExpires = -1;
//if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_SQL))  g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "D");  //(Dispatch message)

										prs->GetFieldValue("allowed_cycles", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pPlaying->m_nPlayLimit = atoi(szTemp);
											if(pPlaying->m_nPlayLimit<0) pPlaying->m_nPlayLimit = -1;
										}
										else pPlaying->m_nPlayLimit = -1;
//if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_SQL))  g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "E");  //(Dispatch message)

										prs->GetFieldValue("actual_cycles", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pPlaying->m_nPlayed = atoi(szTemp);
										}
										else pPlaying->m_nPlayed = 0;

//if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_SQL))  g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "F");  //(Dispatch message)
										prs->GetFieldValue("active", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pPlaying->m_bActive = ((atoi(szTemp)>0)?true:false);
										}
										else pPlaying->m_bActive = false;
//if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_SQL))  g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "G");  //(Dispatch message)

										prs->GetFieldValue("status", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pPlaying->m_nStatus = atoi(szTemp);
										}
										else pPlaying->m_nStatus = 0;
//if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_SQL))  g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "H");  //(Dispatch message)

										prs->GetFieldValue("last_played", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pPlaying->m_nLastPlayed = atoi(szTemp);
										}




/*

										prs->GetFieldValue("timestamp", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pPlaying->m_nTimestamp = atoi(szTemp);
										}
										if(pPlaying->m_nTimestamp<0)
										{
											_ftime(&now);
											pPlaying->m_nTimestamp=now.time;
										}

										prs->GetFieldValue("updated", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pPlaying->m_nLastUpdate = atoi(szTemp);
										}
										prs->GetFieldValue("status", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pPlaying->m_nStatus= (atoi(szTemp)|COLLECTOR_MSG_STATUS_INSERTED);
										}
										prs->GetFieldValue("type", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pPlaying->m_nType = atoi(szTemp);
										}
										prs->GetFieldValue("played", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pPlaying->m_nPlayed = atoi(szTemp);
										}
*/
										bPlaying = true;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_SQL)) 
  g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Playing item found: id=%d [%s]",pPlaying->m_nID, pPlaying->m_szMessage);  //(Dispatch message)

									}

								}

							}
							catch(CException *e)// CDBException *e, CMemoryException *m)  
							{
								if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
			
									if(g_ptabmsgr)
									{
										char errorstring[DB_ERRORSTRING_LEN];
										_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
										g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, errorstring );
									}
			
								}
								else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
								{
									// The error code is in e->m_nRetCode
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory\n%s", szSQL);
									if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, "Retrieve: Caught exception: out of memory\n%s", szSQL);
								}
								else 
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
									if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, "Retrieve: Caught other exception\n%s", szSQL);
								}
								e->Delete();

							} 
							catch( ... )
							{
								if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception.\n%s", szSQL);
								if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, "Retrieve: Caught exception\n%s", szSQL);
							}
							prs->MoveNext();
						}
						delete prs;
						prs = NULL;
					}
					else
					{
						//print err
					}

/*
			if none
				get top 1 events where index>cursor, sort by index;

			else
				get top 1 events where index>playing, sort by index;

*/
				_ftime(&now);

				if(g_settings.m_bTickerPlayOnce) 
				{

					if((bPlaying)&&(pPlaying))
					{

// the three lines beginning with AND were added to create sensitivity to number of cycles and valid and exp dates.
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 \
[id], [message], [aux_data1], [aux_data2], [sort_order], [dwell_override], [valid_from], [expires_on], [allowed_cycles], [actual_cycles], [active], \
[status], [last_played] FROM %s WHERE [sort_order] > %.4f AND [id] > 0 AND [status] & %d = 0 AND [active] > 0 \
AND (([valid_from] IS NULL OR [valid_from] <= 0) OR ([valid_from] IS NOT NULL AND [valid_from] > 0 AND [valid_from] < %d)) \
AND (([expires_on] IS NULL OR [expires_on] <= 0) OR ([expires_on] IS NOT NULL AND [expires_on] > 0 AND [expires_on] > %d)) \
AND (([allowed_cycles] IS NULL OR [allowed_cycles] <= 0) OR ([allowed_cycles] IS NOT NULL AND [allowed_cycles] > 0 AND [actual_cycles] IS NOT NULL AND [actual_cycles] > 0 AND [allowed_cycles] > [actual_cycles])) \
ORDER BY [sort_order] ASC", //HARDCODE
						(((g_settings.m_pszTicker )&&(strlen(g_settings.m_pszTicker )))?g_settings.m_pszTicker:"Ticker"),
						(pPlaying)->m_dblIndex,
						(COLLECTOR_MSG_STATUS_PLAYING|COLLECTOR_MSG_STATUS_PLAYED|COLLECTOR_MSG_STATUS_COMMIT),
						now.time + (g_settings.m_bUseLocalTime?((now.dstflag?3600:0)-(now.timezone*60)):0), 
						now.time + (g_settings.m_bUseLocalTime?((now.dstflag?3600:0)-(now.timezone*60)):0)
						);
//whoa!  now that is some query
					}
					else
					{
// the three lines beginning with AND were added to create sensitivity to number of cycles and valid and exp dates.
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 \
[id], [message], [aux_data1], [aux_data2], [sort_order], [dwell_override], [valid_from], [expires_on], [allowed_cycles], [actual_cycles], [active], \
[status], [last_played] FROM %s WHERE [sort_order] > %.4f AND [id] > 0 AND [status] & %d = 0 AND [active] > 0 \
AND (([valid_from] IS NULL OR [valid_from] <= 0) OR ([valid_from] IS NOT NULL AND [valid_from] > 0 AND [valid_from] < %d)) \
AND (([expires_on] IS NULL OR [expires_on] <= 0) OR ([expires_on] IS NOT NULL AND [expires_on] > 0 AND [expires_on] > %d)) \
AND (([allowed_cycles] IS NULL OR [allowed_cycles] <= 0) OR ([allowed_cycles] IS NOT NULL AND [allowed_cycles] > 0 AND [actual_cycles] IS NOT NULL AND [actual_cycles] > 0 AND [allowed_cycles] > [actual_cycles])) \
ORDER BY [sort_order] ASC", //HARDCODE
						(((g_settings.m_pszTicker )&&(strlen(g_settings.m_pszTicker )))?g_settings.m_pszTicker:"Ticker"),
						m_dblTickerMessageCursor,
						(COLLECTOR_MSG_STATUS_PLAYING|COLLECTOR_MSG_STATUS_PLAYED|COLLECTOR_MSG_STATUS_COMMIT),
						now.time + (g_settings.m_bUseLocalTime?((now.dstflag?3600:0)-(now.timezone*60)):0), 
						now.time + (g_settings.m_bUseLocalTime?((now.dstflag?3600:0)-(now.timezone*60)):0)
						);
					}
				}
				else
				{
					if((bPlaying)&&(pPlaying))
					{
// the three lines beginning with AND were added to create sensitivity to number of cycles and valid and exp dates.
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 \
[id], [message], [aux_data1], [aux_data2], [sort_order], [dwell_override], [valid_from], [expires_on], [allowed_cycles], [actual_cycles], [active], \
[status], [last_played] FROM %s WHERE [sort_order] > %.4f AND [id] > 0 AND [active] > 0 \
AND (([valid_from] IS NULL OR [valid_from] <= 0) OR ([valid_from] IS NOT NULL AND [valid_from] > 0 AND [valid_from] < %d)) \
AND (([expires_on] IS NULL OR [expires_on] <= 0) OR ([expires_on] IS NOT NULL AND [expires_on] > 0 AND [expires_on] > %d)) \
AND (([allowed_cycles] IS NULL OR [allowed_cycles] <= 0) OR ([allowed_cycles] IS NOT NULL AND [allowed_cycles] > 0 AND [actual_cycles] IS NOT NULL AND [actual_cycles] > 0 AND [allowed_cycles] > [actual_cycles])) \
ORDER BY [sort_order] ASC", //HARDCODE
						(((g_settings.m_pszTicker )&&(strlen(g_settings.m_pszTicker )))?g_settings.m_pszTicker:"Ticker"),
						(pPlaying)->m_dblIndex,
						now.time + (g_settings.m_bUseLocalTime?((now.dstflag?3600:0)-(now.timezone*60)):0), 
						now.time + (g_settings.m_bUseLocalTime?((now.dstflag?3600:0)-(now.timezone*60)):0)
						);
					}
					else
					{
// the three lines beginning with AND were added to create sensitivity to number of cycles and valid and exp dates.
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 \
[id], [message], [aux_data1], [aux_data2], [sort_order], [dwell_override], [valid_from], [expires_on], [allowed_cycles], [actual_cycles], [active], \
[status], [last_played] FROM %s WHERE [sort_order] > %.4f AND [id] > 0 AND [active] > 0 \
AND (([valid_from] IS NULL OR [valid_from] <= 0) OR ([valid_from] IS NOT NULL AND [valid_from] > 0 AND [valid_from] < %d)) \
AND (([expires_on] IS NULL OR [expires_on] <= 0) OR ([expires_on] IS NOT NULL AND [expires_on] > 0 AND [expires_on] > %d)) \
AND (([allowed_cycles] IS NULL OR [allowed_cycles] <= 0) OR ([allowed_cycles] IS NOT NULL AND [allowed_cycles] > 0 AND [actual_cycles] IS NOT NULL AND [actual_cycles] > 0 AND [allowed_cycles] > [actual_cycles])) \
ORDER BY [sort_order] ASC", //HARDCODE
						(((g_settings.m_pszTicker )&&(strlen(g_settings.m_pszTicker )))?g_settings.m_pszTicker:"Ticker"),
						m_dblTickerMessageCursor,
						now.time + (g_settings.m_bUseLocalTime?((now.dstflag?3600:0)-(now.timezone*60)):0), 
						now.time + (g_settings.m_bUseLocalTime?((now.dstflag?3600:0)-(now.timezone*60)):0)
						);
					}
				}

if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_SQL)) 
 g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "[%d]NEXT: %s",((bPlaying)&&(pPlaying)), szSQL);  //(Dispatch message)

				if((g_dlldata.nNumThreads>COLLECTOR_TICKER)&&(g_dlldata.thread)&&(g_dlldata.thread[COLLECTOR_TICKER])&&(g_dlldata.thread[COLLECTOR_TICKER]->nThreadState&COLLECTOR_TICKER_CYCLING))
				{
					prs=NULL;
				}
				else
				{
					prs = pdb->Retrieve(pdbConn, szSQL, errorstring);
				}
				if(prs)
				{
					CString szTemp;
					int nReturn = CLIENT_ERROR;
					int nIndex = 0;
					if (!prs->IsEOF())
					{
						try
						{
							pmsg = new CCollectorMessage;
							if(pmsg)
							{
								prs->GetFieldValue("id", szTemp);//HARDCODE
								if(szTemp.GetLength())
								{
									pmsg->m_nID = atoi(szTemp);
									if((	m_nTickerIDMaxNeg>pmsg->m_nID)&&(pmsg->m_nID<0)) // defaults have negative IDs
									{
	EnterCriticalSection(&m_critDefaultID);
										m_nTickerIDMaxNeg=pmsg->m_nID;
	LeaveCriticalSection(&m_critDefaultID);
									}

									prs->GetFieldValue("message", pmsg->m_szMessage);//HARDCODE
									prs->GetFieldValue("aux_data1", pmsg->m_szAux1);//HARDCODE
									prs->GetFieldValue("aux_data2", pmsg->m_szAux2);//HARDCODE
									prs->GetFieldValue("sort_order", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_dblIndex = atof(szTemp);

										if(	m_dblTickerDefaultMax<pmsg->m_dblIndex) m_dblTickerDefaultMax=pmsg->m_dblIndex;
									}

									prs->GetFieldValue("dwell_override", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_dblDwellOverride = atof(szTemp);
										if(pmsg->m_dblDwellOverride<0.001) pmsg->m_dblDwellOverride = -1.0;
									}
									else pmsg->m_dblDwellOverride = -1.0;

									prs->GetFieldValue("valid_from", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_nValidFrom = atoi(szTemp);
									}
									else pmsg->m_nValidFrom = -1;
						
									prs->GetFieldValue("expires_on", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_nExpires = atoi(szTemp);
									}
									else pmsg->m_nExpires = -1;

									prs->GetFieldValue("allowed_cycles", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_nPlayLimit = atoi(szTemp);
										if(pmsg->m_nPlayLimit<0) pmsg->m_nPlayLimit = -1;
									}
									else pmsg->m_nPlayLimit = -1;

									prs->GetFieldValue("actual_cycles", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_nPlayed = atoi(szTemp);
									}
									else pmsg->m_nPlayed = 0;

									prs->GetFieldValue("active", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_bActive = ((atoi(szTemp)>0)?true:false);
									}
									else pmsg->m_bActive = false;

									prs->GetFieldValue("status", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_nStatus = atoi(szTemp);
									}
									else pmsg->m_nStatus = 0;

									prs->GetFieldValue("last_played", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_nLastPlayed = atoi(szTemp);
									}
if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_SQL)) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "FOUND: [%s]%d with [%s]",pmsg->m_szMessage, pmsg->m_nStatus, szSQL);  //(Dispatch message)

																																																																																													 
/*

									prs->GetFieldValue("timestamp", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_nTimestamp = atoi(szTemp);
									}
									if(pmsg->m_nTimestamp<0)
									{
										_ftime(&now);
										pmsg->m_nTimestamp=now.time;
									}
									prs->GetFieldValue("updated", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_nLastUpdate = atoi(szTemp);
									}
									prs->GetFieldValue("status", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_nStatus= (atoi(szTemp)|COLLECTOR_MSG_STATUS_INSERTED);
									}


									prs->GetFieldValue("type", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_nType = atoi(szTemp);
									}
									prs->GetFieldValue("played", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_nPlayed = atoi(szTemp);
									}
									*/
								}
							}
						}
						catch(CException *e)// CDBException *e, CMemoryException *m)  
						{
							if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
							{
								if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
		
								if(g_ptabmsgr)
								{
									char errorstring[DB_ERRORSTRING_LEN];
									_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
									g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, errorstring );
								}
		
							}
							else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
							{
								// The error code is in e->m_nRetCode
								if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory\n%s", szSQL);
								if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, "Retrieve: Caught exception: out of memory\n%s", szSQL);
							}
							else 
							{
								if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
								if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, "Retrieve: Caught other exception\n%s", szSQL);
							}
							e->Delete();

						} 
						catch( ... )
						{
							if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception.\n%s", szSQL);
							if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, "Retrieve: Caught exception\n%s", szSQL);
						}
						prs->MoveNext();
					}
					delete prs;
					prs = NULL;
				}
				else
				{
					//print err
				}


				if(!g_settings.m_bTickerPlayOnce)
				{
					if(pmsg==NULL) 
					{
	//if nothing greater than the index, need to re-query the non-defaults to see if there are any events in the regular stack to recirculate.

						_ftime(&now);


// the three lines beginning with AND were added to create sensitivity to number of cycles and valid and exp dates.

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 \
[id], [message], [aux_data1], [aux_data2], [sort_order], [dwell_override], [valid_from], [expires_on], [allowed_cycles], [actual_cycles], [active], \
[status], [last_played] FROM %s WHERE [id] > 0 AND [active] > 0 \
AND (([valid_from] IS NULL OR [valid_from] <= 0) OR ([valid_from] IS NOT NULL AND [valid_from] > 0 AND [valid_from] < %d)) \
AND (([expires_on] IS NULL OR [expires_on] <= 0) OR ([expires_on] IS NOT NULL AND [expires_on] > 0 AND [expires_on] > %d)) \
AND (([allowed_cycles] IS NULL OR [allowed_cycles] <= 0) OR ([allowed_cycles] IS NOT NULL AND [allowed_cycles] > 0 AND [actual_cycles] IS NOT NULL AND [actual_cycles] > 0 AND [allowed_cycles] > [actual_cycles])) \
ORDER BY [sort_order] ASC", //HARDCODE
							(((g_settings.m_pszTicker )&&(strlen(g_settings.m_pszTicker )))?g_settings.m_pszTicker:"Ticker"),
							now.time + (g_settings.m_bUseLocalTime?((now.dstflag?3600:0)-(now.timezone*60)):0), 
							now.time + (g_settings.m_bUseLocalTime?((now.dstflag?3600:0)-(now.timezone*60)):0)
							);
						
if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_SQL)) 
 g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "NEXT2: %s",szSQL);  //(Dispatch message)

						prs = pdb->Retrieve(pdbConn, szSQL, errorstring);
						if(prs)
						{
							CString szTemp;
							int nReturn = CLIENT_ERROR;
							int nIndex = 0;
							if (!prs->IsEOF())
							{
								try
								{
									pmsg = new CCollectorMessage;
									if(pmsg)
									{
										prs->GetFieldValue("id", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nID = atoi(szTemp);
											if((	m_nTickerIDMaxNeg>pmsg->m_nID)&&(pmsg->m_nID<0)) // defaults have negative IDs
											{
	EnterCriticalSection(&m_critDefaultID);
												m_nTickerIDMaxNeg=pmsg->m_nID;
	LeaveCriticalSection(&m_critDefaultID);
											}

											prs->GetFieldValue("message", pmsg->m_szMessage);//HARDCODE
											prs->GetFieldValue("aux_data1", pmsg->m_szAux1);//HARDCODE
											prs->GetFieldValue("aux_data2", pmsg->m_szAux2);//HARDCODE
											prs->GetFieldValue("sort_order", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_dblIndex = atof(szTemp);

												if(	m_dblTickerDefaultMax<pmsg->m_dblIndex) m_dblTickerDefaultMax=pmsg->m_dblIndex;
											}

											prs->GetFieldValue("dwell_override", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_dblDwellOverride = atoi(szTemp);
												if(pmsg->m_dblDwellOverride<0.001) pmsg->m_dblDwellOverride = -1.0;
											}
											else pmsg->m_dblDwellOverride = -1.0;

											prs->GetFieldValue("valid_from", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nValidFrom = atoi(szTemp);
											}
											else pmsg->m_nValidFrom = -1;
								
											prs->GetFieldValue("expires_on", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nExpires = atoi(szTemp);
											}
											else pmsg->m_nExpires = -1;

											prs->GetFieldValue("allowed_cycles", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nPlayLimit = atoi(szTemp);
												if(pmsg->m_nPlayLimit<0) pmsg->m_nPlayLimit = -1;
											}
											else pmsg->m_nPlayLimit = -1;

											prs->GetFieldValue("actual_cycles", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nPlayed = atoi(szTemp);
											}
											else pmsg->m_nPlayed = 0;

											prs->GetFieldValue("active", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_bActive = ((atoi(szTemp)>0)?true:false);
											}
											else pmsg->m_bActive = false;

											prs->GetFieldValue("status", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nStatus = atoi(szTemp);
											}
											else pmsg->m_nStatus = 0;

											prs->GetFieldValue("last_played", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nLastPlayed = atoi(szTemp);
											}
if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_SQL)) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "FOUND2: [%s]%d with [%s]",pmsg->m_szMessage, pmsg->m_nStatus, szSQL);  //(Dispatch message)

/*

											prs->GetFieldValue("timestamp", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nTimestamp = atoi(szTemp);
											}
											if(pmsg->m_nTimestamp<0)
											{
												_ftime(&now);
												pmsg->m_nTimestamp=now.time;
											}											
											prs->GetFieldValue("updated", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nLastUpdate = atoi(szTemp);
											}
											prs->GetFieldValue("status", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nStatus= (atoi(szTemp)|COLLECTOR_MSG_STATUS_INSERTED);
											}
											prs->GetFieldValue("type", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nType = atoi(szTemp);
											}
											prs->GetFieldValue("played", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nPlayed = atoi(szTemp);
											}
											*/
										}
									}
								}
								catch(CException *e)// CDBException *e, CMemoryException *m)  
								{
									if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
									{
										if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
				
										if(g_ptabmsgr)
										{
											char errorstring[DB_ERRORSTRING_LEN];
											_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
											g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, errorstring );
										}
				
									}
									else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
									{
										// The error code is in e->m_nRetCode
										if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory\n%s", szSQL);
										if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, "Retrieve: Caught exception: out of memory\n%s", szSQL);
									}
									else 
									{
										if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
										if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, "Retrieve: Caught other exception\n%s", szSQL);
									}
									e->Delete();

								} 
								catch( ... )
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception.\n%s", szSQL);
									if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, "Retrieve: Caught exception\n%s", szSQL);
								}
								prs->MoveNext();
							}
							delete prs;
							prs = NULL;
						}
						else
						{
							//print err
						}
					}
				}
#define SKIP_DEFAULTS

#ifndef SKIP_DEFAULTS
				if(pmsg==NULL)
				{
					//try defaults
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 \
[id], [message], [aux_data1], [aux_data2], [sort_order], [dwell_override], [valid_from], [expires_on], [allowed_cycles], [actual_cycles], [active], \
[status], [last_played] FROM %s WHERE [sort_order] > %.4f AND [id] < 0 AND [active] > 0 ORDER BY [sort_order] ASC", //HARDCODE
						(((g_settings.m_pszTicker )&&(strlen(g_settings.m_pszTicker )))?g_settings.m_pszTicker:"Ticker"),
						m_dblTickerDefaultCursor
						);

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "%s",szSQL);  //(Dispatch message)
					prs = pdb->Retrieve(pdbConn, szSQL, errorstring);
					if(prs)
					{
						CString szTemp;
						int nReturn = CLIENT_ERROR;
						int nIndex = 0;
						if (!prs->IsEOF())
						{
							try
							{
								pmsg = new CCollectorMessage;
								if(pmsg)
								{
									prs->GetFieldValue("id", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_nID = atoi(szTemp);
										if((	m_nTickerIDMaxNeg>pmsg->m_nID)&&(pmsg->m_nID<0)) // defaults have negative IDs
										{
	EnterCriticalSection(&m_critDefaultID);
											m_nTickerIDMaxNeg=pmsg->m_nID;
	LeaveCriticalSection(&m_critDefaultID);
										}


										prs->GetFieldValue("text", pmsg->m_szMessage);//HARDCODE
										prs->GetFieldValue("index", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_dblIndex = atof(szTemp);

											if(	m_dblTickerDefaultMax<pmsg->m_dblIndex) m_dblTickerDefaultMax=pmsg->m_dblIndex;
										}
										prs->GetFieldValue("timestamp", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nTimestamp = atoi(szTemp);
										}
										if(pmsg->m_nTimestamp<0)
										{
											_ftime(&now);
											pmsg->m_nTimestamp=now.time;
										}										
										prs->GetFieldValue("updated", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nLastUpdate = atoi(szTemp);
										}
										prs->GetFieldValue("status", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nStatus= (atoi(szTemp)|COLLECTOR_MSG_STATUS_INSERTED);
										}
										prs->GetFieldValue("type", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nType = atoi(szTemp);
										}
										prs->GetFieldValue("played", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nPlayed = atoi(szTemp);
										}
									}
								}
							}
							catch(CException *e)// CDBException *e, CMemoryException *m)  
							{
								if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
			
									if(g_ptabmsgr)
									{
										char errorstring[DB_ERRORSTRING_LEN];
										_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
										g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, errorstring );
									}
			
								}
								else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
								{
									// The error code is in e->m_nRetCode
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory\n%s", szSQL);
									if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, "Retrieve: Caught exception: out of memory\n%s", szSQL);
								}
								else 
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
									if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, "Retrieve: Caught other exception\n%s", szSQL);
								}
								e->Delete();

							} 
							catch( ... )
							{
								if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception.\n%s", szSQL);
								if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, "Retrieve: Caught exception\n%s", szSQL);
							}
							prs->MoveNext();
						}
						delete prs;
						prs = NULL;
					}
					else
					{
						//print err
					}

					
				}
				if(pmsg==NULL)
				{
					//try recirc defaults
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 \
[id], [message], [aux_data1], [aux_data2], [sort_order], [dwell_override], [valid_from], [expires_on], [allowed_cycles], [actual_cycles], [active], \
[status], [last_played] FROM %s WHERE AND [id] < 0 AND [type] & %d = 0 ORDER BY [sort_order] ASC", //HARDCODE
						(((g_settings.m_pszTicker )&&(strlen(g_settings.m_pszTicker )))?g_settings.m_pszTicker:"Ticker"),
						COLLECTOR_MSG_TYPE_DISABLED
						);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "%s",szSQL);  //(Dispatch message)


					prs = pdb->Retrieve(pdbConn, szSQL, errorstring);
					if(prs)
					{
						CString szTemp;
						int nReturn = CLIENT_ERROR;
						int nIndex = 0;
						if (!prs->IsEOF())
						{
							try
							{
								pmsg = new CCollectorMessage;
								if(pmsg)
								{
									prs->GetFieldValue("id", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_nID = atoi(szTemp);
										if((	m_nTickerIDMaxNeg>pmsg->m_nID)&&(pmsg->m_nID<0)) // defaults have negative IDs
										{
	EnterCriticalSection(&m_critDefaultID);
											m_nTickerIDMaxNeg=pmsg->m_nID;
	LeaveCriticalSection(&m_critDefaultID);
										}

										prs->GetFieldValue("text", pmsg->m_szMessage);//HARDCODE
										prs->GetFieldValue("index", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_dblIndex = atof(szTemp);

											if(	m_dblTickerDefaultMax<pmsg->m_dblIndex) m_dblTickerDefaultMax=pmsg->m_dblIndex;
										}
										prs->GetFieldValue("timestamp", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nTimestamp = atoi(szTemp);
										}
										if(pmsg->m_nTimestamp<0)
										{
											_ftime(&now);
											pmsg->m_nTimestamp=now.time;
										}										
										prs->GetFieldValue("updated", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nLastUpdate = atoi(szTemp);
										}
										prs->GetFieldValue("status", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nStatus= (atoi(szTemp)|COLLECTOR_MSG_STATUS_INSERTED);
										}
										prs->GetFieldValue("type", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nType = atoi(szTemp);
										}
										prs->GetFieldValue("played", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nPlayed = atoi(szTemp);
										}
									}
								}
							}
							catch(CException *e)// CDBException *e, CMemoryException *m)  
							{
								if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
			
									if(g_ptabmsgr)
									{
										char errorstring[DB_ERRORSTRING_LEN];
										_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
										g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, errorstring );
									}
			
								}
								else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
								{
									// The error code is in e->m_nRetCode
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory\n%s", szSQL);
									if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, "Retrieve: Caught exception: out of memory\n%s", szSQL);
								}
								else 
								{
									if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
									if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, "Retrieve: Caught other exception\n%s", szSQL);
								}
								e->Delete();

							} 
							catch( ... )
							{
								if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception.\n%s", szSQL);
								if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, szTickerSource, "Retrieve: Caught exception\n%s", szSQL);
							}
							prs->MoveNext();
						}
						delete prs;
						prs = NULL;
					}
					else
					{
						//print err
					}

					
				}
#endif // #ifndef SKIP_DEFAULTS

/*
					if((pmsg==NULL)&&(bPlaying)&&(pPlaying)&&(pPlaying->m_nID>=0))
					{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 \
[id], [text], [sort_order], [timestamp], [updated], [status], [type], [played] FROM %s WHERE [sort_order] > %.4f AND [id] < 0 ORDER BY [sort_order] ASC", //HARDCODE
							(((g_settings.m_pszTicker )&&(strlen(g_settings.m_pszTicker )))?g_settings.m_pszTicker:"Ticker"),
							m_dblTickerDefaultCursor
							);
						
							
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "no messages: %s",szSQL);  //(Dispatch message)

						prs = pdb->Retrieve(pdbConn, szSQL, errorstring);
						if(prs)
						{
							CString szTemp;
							int nReturn = CLIENT_ERROR;
							int nIndex = 0;
							if (!prs->IsEOF())
							{
								try
								{
									pmsg = new CCollectorMessage;
									if(pmsg)
									{
										prs->GetFieldValue("id", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nID = atoi(szTemp);
											if((	m_nTickerIDMaxNeg>pmsg->m_nID)&&(pmsg->m_nID<0)) // defaults have negative IDs
												m_nTickerIDMaxNeg=pmsg->m_nID;


											prs->GetFieldValue("text", pmsg->m_szMessage);//HARDCODE
											prs->GetFieldValue("index", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_dblIndex = atof(szTemp);

												if(	m_dblTickerDefaultMax<pmsg->m_dblIndex) m_dblTickerDefaultMax=pmsg->m_dblIndex;
											}
											prs->GetFieldValue("timestamp", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nTimestamp = atoi(szTemp);
											}
											prs->GetFieldValue("updated", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nLastUpdate = atoi(szTemp);
											}
											prs->GetFieldValue("status", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nStatus= (atoi(szTemp)|COLLECTOR_MSG_STATUS_INSERTED);
											}
											prs->GetFieldValue("type", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nType = atoi(szTemp);
											}
											prs->GetFieldValue("played", szTemp);//HARDCODE
											if(szTemp.GetLength())
											{
												pmsg->m_nPlayed = atoi(szTemp);
											}
										}
									}
								}
								catch(...)
								{
								}
								prs->MoveNext();
							}
							delete prs;
							prs = NULL;
						}
						else
						{
							//print err
						}
					}

				}
*/
				if((ppPlaying==NULL)&&(pPlaying!=NULL)) delete pPlaying;

	}

	return pmsg;
}

int CCollectorData::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszMessages)&&(strlen(g_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			g_settings.m_pszMessages,
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			g_settings.m_pszMessages
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
//			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return TABULATOR_SUCCESS;
		}
	}
	return TABULATOR_ERROR;
}



int CCollectorData::SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszAsRun)&&(strlen(g_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log")
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Collector_Asrun", "SQL %s", szSQL) ;// Sleep(100); //(Dispatch message)

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
//			IncrementDatabaseMods(g_settings.m_pszAsRun, dberrorstring);
			return TABULATOR_SUCCESS;
		}
		else
		{
if(g_ptabmsgr)
{
	char szSource[MAX_PATH]; sprintf(szSource, "%s_Asrun", g_settings.m_pszInternalAppName);
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "SQL error %s", dberrorstring) ;// Sleep(100); //(Dispatch message)
}
		}
		LeaveCriticalSection(&m_critSQL);

	}
	else
		{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Collector_Asrun", "null conditions");// Sleep(100); //(Dispatch message)
		}
	return TABULATOR_ERROR;
}

int CCollectorData::PlayEvent(CDBUtil* pdb, CDBconn* pdbConn, CTabulatorEventArray** ppEvent, CCollectorMessage* pItem, bool bPreamble, char* pszInfo)
{
	CTabulatorEventArray* pEvent = NULL;
	char szTickerSource[MAX_PATH]; sprintf(szTickerSource, "%s_Ticker", g_settings.m_pszInternalAppName);
	if(ppEvent) pEvent = *ppEvent;
	if((pdb)&&(pdbConn)&&(pEvent)&&(pEvent->m_ppEvents)&&(pEvent->m_nEvents>0))
	{
		char szCommandSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];
		int i=0;
		_timeb timebBeginTick;
		_timeb timebTriggerTick;
		_timeb timebNowTick;
		EnterCriticalSection(&pEvent->m_critEvents);
		_ftime(&timebBeginTick);
		CIS2Core is2; // for util functions only.
		CBufferUtil bu;
//			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "PlayEvent event"); //(Dispatch message)

	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "PlayEvent event %s with %d actions", pEvent->m_ppEvents[0]->m_szScene, pEvent->m_nEvents); //(Dispatch message)

		while((i<pEvent->m_nEvents)&&(g_dlldata.thread[COLLECTOR_TICKER])&&(!g_dlldata.thread[COLLECTOR_TICKER]->bKillThread))
		{
			if((pEvent->m_ppEvents)&&(pEvent->m_ppEvents[i]))
			{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "-X- text event %02d %s %.0f", i, pEvent->m_ppEvents[i]->m_szIdentifier, pEvent->m_ppEvents[i]->m_dblTriggerTime); //(Dispatch message)
				// delay by the offest amount;
				timebTriggerTick.time = timebBeginTick.time+((int)(pEvent->m_ppEvents[i]->m_dblTriggerTime))/1000;
				timebTriggerTick.millitm = (unsigned short)(timebBeginTick.millitm + ((int)(pEvent->m_ppEvents[i]->m_dblTriggerTime))%1000);
				while(timebTriggerTick.millitm>999)
				{
					timebTriggerTick.time++;
					timebTriggerTick.millitm-=1000;
				}
				_ftime(&timebNowTick);

				// assemble the string to send:

				if(pEvent->m_ppEvents[i]->m_szIdentifier.GetLength())
				{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "--- text event %02d %s", i, pEvent->m_ppEvents[i]->m_szIdentifier); //(Dispatch message)

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "pchEncodedLocal: %s", pchEncodedLocal);  Sleep(50);//(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "want to send %s", proj);  Sleep(50);//(Dispatch message)

					CString szII;
					if(pEvent->m_ppEvents[i]->m_nType==0)  //anim
					{
/*
[18:44] rpbutterphly: Open
[18:44] rpbutterphly: Load
[18:44] rpbutterphly: Play
[18:44] rpbutterphly: Stop
[18:45] rpbutterphly: Close
*/
/*
3000 Duet LEX
3001 Duet HyperX
3002 Duet MicroX
3003 Channel Box
3004 CAL Box
*/

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Type is anim, with dest type: %d", pEvent->m_ppEvents[i]->m_nDestType);  Sleep(50);//(Dispatch message)

						switch(g_settings.m_nGraphicsHostType)//pEvent->m_ppEvents[i]->m_nDestType) // hardcoded to CB for now.
						{
						case TABULATOR_DESTTYPE_MIRANDA_IS2://					2001  // Imagestore 2
						case TABULATOR_DESTTYPE_MIRANDA_INT://					2002  // Intuition
						case TABULATOR_DESTTYPE_MIRANDA_IS300://				2003  // Imagestore 300
						case TABULATOR_DESTTYPE_MIRANDA_ISHD://				2004  // Imagestore HD
							{
								// no anims supported yet
							} break;

						case TABULATOR_DESTTYPE_CHYRON_CHANNELBOX://		3003  // Channel Box
							{
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Open")==0)
								{
									szII.Format("B\\OP\\%s\\\\", pEvent->m_ppEvents[i]->m_szScene);
								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Load")==0)
								{
									szII.Format("B\\LO\\%s\\\\", pEvent->m_ppEvents[i]->m_szScene);
								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Play")==0)
								{
									szII.Format("B\\PL\\%s\\\\", pEvent->m_ppEvents[i]->m_szScene);
								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Stop")==0)
								{
									szII.Format("B\\ST\\%s\\\\", pEvent->m_ppEvents[i]->m_szScene);
								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Close")==0)
								{
									szII.Format("B\\CL\\%s\\\\", pEvent->m_ppEvents[i]->m_szScene);
								}

								//system-wide commands
/* removing syscommands
								else  
									if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase(pnucleus->m_settings.m_pszStopAll?pnucleus->m_settings.m_pszStopAll:"StopAll")==0)
								{
/*
Courtney Tompos
Software Engineer
Chyron Corporation
631-845-2141:

To stop all scenes (but leave them open), transmit:
Y\<1B>\\<CR><LF>

To close all scenes, transmit either:
Y\<CD>\\<CR><LF>
Or
Y\<FE>\\<CR><LF>
*/
/* removing syscommands

									szII.Format("Y\\%c\\\\", 0x1b);
								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase(pnucleus->m_settings.m_pszCloseAll?pnucleus->m_settings.m_pszCloseAll:"CloseAll")==0)
								{
									szII.Format("Y\\%c\\\\", 0xcd); // 0xcd seems to be a bit faster than 0xfe, maybe not appreciably so, but given the choice we use 0xcd
								}
removing syscommands */
								else  // prob a mistake so just use update.
								{


										//��#
									char* pch = bu.EscapeChar(ParseValue(pEvent->m_ppEvents[i]->m_szValue, bPreamble, pItem).GetBuffer(0), '\\');
									if(pch)
									{
										szII.Format("B\\UP\\%s\\%s%s�%s\\\\", 
											pEvent->m_ppEvents[i]->m_szScene, 
											pEvent->m_ppEvents[i]->m_szIdentifier, 
											"",//(pnucleus->m_settings.m_bUseUTF8?"�":""),
											pch);
										free(pch);
									}
									else
									{
										szII.Format("B\\UP\\%s\\%s%s�%s\\\\", 
											pEvent->m_ppEvents[i]->m_szScene, 
											pEvent->m_ppEvents[i]->m_szIdentifier, 
											"",//(pnucleus->m_settings.m_bUseUTF8?"�":""),
											ParseValue(pEvent->m_ppEvents[i]->m_szValue, bPreamble, pItem));
									}
								}
							} break;
							// following cases are "Lyric"
/* removing cases
						case TABULATOR_DESTTYPE_CHYRON_LEX://					3000	// Duet LEX
						case TABULATOR_DESTTYPE_CHYRON_HYPERX://				3001  // Duet HyperX
						case TABULATOR_DESTTYPE_CHYRON_MICROX://				3002  // Duet MicroX
						case TABULATOR_DESTTYPE_CHYRON_CALBOX://				3004  // CAL Box
							{
/*
[15:33] rpbutterphly: load   = W command
[15:33] rpbutterphly: play   = not V\6   always use  Y\<D5><F3> for release, and immediate return
[15:33] rpbutterphly: release = V\5\15
[15:33] rpbutterphly: unload  //not impl
[15:33] rpbutterphly: clear    Y\<D5><FE><Frame Buffer>\\<CR><LF>

*/
/* removing cases
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Load")==0)
								{
/*
									if(pChannelObj->m_nLastLyricAlias<0)
									{
										pChannelObj->m_nLastLyricAlias = pnucleus->m_settings.m_nMinLyricAlias;
									}
									else
									{
										pChannelObj->m_nLastLyricAlias++;  
										if (pChannelObj->m_nLastLyricAlias>pnucleus->m_settings.m_nMaxLyricAlias)
											pChannelObj->m_nLastLyricAlias = pnucleus->m_settings.m_nMinLyricAlias;
									}
									szII.Format("W\\%d\\%s\\", pChannelObj->m_nLastLyricAlias, pEvent->m_ppEvents[i]->m_szScene);
*/
///// new method
/* removing cases
									int idxA = pChannelObj->ReturnAliasIndex(pEvent->m_ppEvents[i]->m_szScene);

									if(idxA<0)
									{
										idxA = pChannelObj->AddAlias(pEvent->m_ppEvents[i]->m_szScene);
									}

									if(idxA<0)
									{												
//													szII.Format("W\\%d\\%s\\", pChannelObj->m_nLastLyricAlias, pEvent->m_ppEvents[i]->m_szScene); // no choice
										szII.Format("W\\%s\\%s\\", pEvent->m_ppEvents[i]->m_szScene, pEvent->m_ppEvents[i]->m_szScene); // no choice
									}
									else
									{										
										szII.Format("W\\%d\\%s\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pEvent->m_ppEvents[i]->m_szScene);

										// prob need to set this after a success code .... at some point.
										pChannelObj->m_ppLyricAlias[idxA]->bMessageLoaded = true;
									}

////////////////
									for(int maxfields=0; maxfields<99; maxfields++)
									{
										szII += " \\";
									}
									szII += "\\";  // term II


//												pChannelObj->m_nCurrentMessage = atoi(pEvent->m_ppEvents[i]->m_szScene); // not sure why bothering to do this.
//												pChannelObj->m_bMessageLoaded = true;
//												pChannelObj->m_bMessageRead = false;  // reset this guy


								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Read")==0)
								{
									int idxA = pChannelObj->ReturnAliasIndex(pEvent->m_ppEvents[i]->m_szScene);
									if(idxA<0)
									{

										szII.Format("V\\5\\13\\1\\%s\\%s\\1\\\\", pEvent->m_ppEvents[i]->m_szLayer, pEvent->m_ppEvents[i]->m_szScene);
//													szII.Format("V\\5\\13\\1\\%s\\%d\\1\\\\", pEvent->m_ppEvents[i]->m_szLayer, pChannelObj->m_nLastLyricAlias);
									}
									else
									{
										szII.Format("V\\5\\13\\1\\%s\\%d\\1\\\\", pEvent->m_ppEvents[i]->m_szLayer, pChannelObj->m_ppLyricAlias[idxA]->nAlias);
										pChannelObj->m_ppLyricAlias[idxA]->bMessageRead = true; 
									}
								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Play")==0)
								{
									szII.Format("Y\\%c%c%s\\\\", 0xd5, 0xf3, pEvent->m_ppEvents[i]->m_szLayer);  //we'll see if this works.
								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Release")==0)
								{
									if(pnucleus->m_settings.m_bReleaseIsV_5_15)
									{
										szII.Format("V\\5\\15\\\\");
									}
									else
									{
/*

Andrea Guarini	aguarini@chyron.com
The only other way to release a pause is via a macro (E) command.  The
following will release a pause on Frame Buffer 2.

E\Lyric.FrameBuffer(2).ReleasePause\\

*/
/* removing cases

										szII.Format("E\\Lyric.FrameBuffer(%s).ReleasePause\\\\", pEvent->m_ppEvents[i]->m_szLayer);  //we'll see if this works.

									}

								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Unload")==0)
								{
									// same as clear for now.
									szII.Format("Y\\%c%c%s\\\\", 0xd5, 0xfe, pEvent->m_ppEvents[i]->m_szLayer);  //we'll see if this works.
									int idxA = pChannelObj->ReturnAliasIndex(pEvent->m_ppEvents[i]->m_szScene);
									if(idxA>=0)
									{
										pChannelObj->m_ppLyricAlias[idxA]->bMessageLoaded = false;
									}

								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Clear")==0)
								{
									szII.Format("Y\\%c%c%s\\\\", 0xd5, 0xfe, pEvent->m_ppEvents[i]->m_szLayer);  //we'll see if this works.
									int idxA = pChannelObj->ReturnAliasIndex(pEvent->m_ppEvents[i]->m_szScene);
									if(idxA>=0)
									{
										pChannelObj->m_ppLyricAlias[idxA]->bMessageLoaded = false;
									}
								}
								else  // prob a mistake so just use update.
								{
									int idxA = pChannelObj->ReturnAliasIndex(pEvent->m_ppEvents[i]->m_szScene);
									if(idxA>=0)
									{
										char* pch = bu.EscapeChar(ParseValue(pEvent->m_ppEvents[i]->m_szValue, pItem).GetBuffer(0), '\\');
										if(pch)
										{
											if(pChannelObj->m_ppLyricAlias[idxA]->bMessageRead)
												szII.Format("U\\*\\%s\\%s\\\\", pEvent->m_ppEvents[i]->m_szIdentifier, pch);
											else
												szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pEvent->m_ppEvents[i]->m_szIdentifier, pch);
											free(pch);
										}
										else
										{
											if(pChannelObj->m_ppLyricAlias[idxA]->bMessageRead)
												szII.Format("U\\*\\%s\\%s\\\\", pEvent->m_ppEvents[i]->m_szIdentifier, ParseValue(pEvent->m_ppEvents[i]->m_szValue, pItem));
											else
												szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pEvent->m_ppEvents[i]->m_szIdentifier, ParseValue(pEvent->m_ppEvents[i]->m_szValue, pItem));

										}
									}
								}
							} break;
removing cases  */

						case TABULATOR_DESTTYPE_HARRIS_ICONII://				5000
							{

/*
// IconStation II
IconStation supports four CII commands for greater output flexibility.
The first CII command allows you to load a layout and fire a Salvo.
The second CII command allows you to clear preview and clear output.
The third CII command allows you to poll the IconStation system to confirm IconStation is still running and responding to commands.
The fourth CII command allows you to update your item source text at any time.

Channel Entry
The Channel entry equals A or B.
A = program channel
B = preview channel

T\7\LayoutSalvo\Channel\\   - Load a Layout and Fire a Salvo
T\14\Channel\\   - Clear Preview and Clear Output
*\\   - Poll IconStation (returns *<CRLF>)
I\42\<LayoutTags><LayoutName>TheLayoutNameGoesHere</LayoutName><Region><Name>TheItemNameGoesHere</Name><Tag><Name>TheTagNameGoesHere</Name><Text>TheTextGoesHere</Text></Tag></Region></LayoutTags>\\    - Update Item Source Text
Example Command:
I\42\<LayoutTags><LayoutName>UpNext</LayoutName><Region><Name>Title1</Name><Tag> <Name>Field1</Name><Text>Economic Outlook</Text></Tag></Region></LayoutTags>\\

//stuff has this structure:
<LayoutTags>
<LayoutName>InfoChannel</LayoutName>
<Region> // region 1
<Name>Crawl</Name> // the name of the region
<Tag> // a "tag" within a region
<Name>1-1</Name>  // predefined tag name such as Field1, Field2, 1-1, 2-1, etc, see documentation
<Text>blarg</Text>  // text to put in the tag (field) in the region
</Tag>
</Region>
<Region> // an additional region in the layout - one with multiple "tags"
<Name>Title_area</Name>
<Tag>
<Name>Field1</Name>
<Text>coming up next</Text>
</Tag>
<Tag>
<Name>Field2</Name>
<Text>more text</Text>
</Tag>
</Region>
</LayoutTags>


Enter the name of the data tag between the second set of <Name></Name> tags. The data
tag name depends on the item type

Item Type Examples
Title To update the text on the first line of your title item enter:
Field1
To update the text on the second line of your title item enter:
Field2
To update the text on the third line of your title item enter:
Field3
Continue entering the next field number for the following lines in
your title item.
Crawl To update the text for a crawl item enter:
1-1
Roll To update the text for a roll item enter:
1-1
Title Table To update the text in a title table item, use the row-column
naming conventation. See the following examples.
To update the text in the first row, first column enter:
1-1
To update the text in the second row, first column enter:
2-1
To update the text in the first row, second column enter:
1-2
To update the text in the second row, second column enter:
2-2

To accomplish this, the identifier will have the following format:
RegionName:TagName

*/


								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Load")==0)
								{
									switch(g_settings.m_nLayoutSalvoFormat)//pnucleus->m_settings.m_nLayoutSalvoFormat)
									{
									default:
									case TABULATOR_HARRIS_ICONII_LSFORMAT_NUM4://				0 // 4 digit numerical LayoutSalvoFormat
										{
											szII.Format("T\\7\\%s00\\%s\\\\", pEvent->m_ppEvents[i]->m_szScene,pEvent->m_ppEvents[i]->m_szLayer);
										} break;
									case TABULATOR_HARRIS_ICONII_LSFORMAT_NUM8://				1 // 8 digit numerical LayoutSalvoFormat
										{
											szII.Format("T\\7\\%s0000\\%s\\\\", pEvent->m_ppEvents[i]->m_szScene,pEvent->m_ppEvents[i]->m_szLayer);
										}	break;
									case TABULATOR_HARRIS_ICONII_LSFORMAT_ALPHA://			2 // alphanumerical string LayoutSalvoFormat
										{
											szII.Format("T\\7\\%s/\\%s\\\\", pEvent->m_ppEvents[i]->m_szScene,pEvent->m_ppEvents[i]->m_szLayer);
										} break;
									}
									
								}
								else
/*
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("LoadFire")==0)
								{
									switch(g_settings.m_nLayoutSalvoFormat)
									{
									default:
									case TABULATOR_HARRIS_ICONII_LSFORMAT_NUM4://				0 // 4 digit numerical LayoutSalvoFormat
									case TABULATOR_HARRIS_ICONII_LSFORMAT_NUM8://				1 // 8 digit numerical LayoutSalvoFormat
										{
											szII.Format("T\\7\\%s%s\\%s\\\\", pEvent->m_ppEvents[i]->m_szScene,ParseValue(pEvent->m_ppEvents[i]->m_szValue, bPreamble, pItem),pEvent->m_ppEvents[i]->m_szLayer);
										} break;
									case TABULATOR_HARRIS_ICONII_LSFORMAT_ALPHA://			2 // alphanumerical string LayoutSalvoFormat
										{
											szII.Format("T\\7\\%s/%s\\%s\\\\", pEvent->m_ppEvents[i]->m_szScene,ParseValue(pEvent->m_ppEvents[i]->m_szValue, bPreamble, pItem),pEvent->m_ppEvents[i]->m_szLayer);
										} break;
									}
								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Fire")==0)
								{
									switch(g_settings.m_nLayoutSalvoFormat)
									{
									default:
									case TABULATOR_HARRIS_ICONII_LSFORMAT_NUM4://				0 // 4 digit numerical LayoutSalvoFormat
										{
											szII.Format("T\\7\\00%s\\%s\\\\", ParseValue(pEvent->m_ppEvents[i]->m_szValue, pItem),pEvent->m_ppEvents[i]->m_szLayer);
										} break;
									case TABULATOR_HARRIS_ICONII_LSFORMAT_NUM8://				1 // 8 digit numerical LayoutSalvoFormat
										{
											szII.Format("T\\7\\0000%s\\%s\\\\", ParseValue(pEvent->m_ppEvents[i]->m_szValue, pItem),pEvent->m_ppEvents[i]->m_szLayer);
										}	break;
									case TABULATOR_HARRIS_ICONII_LSFORMAT_ALPHA://			2 // alphanumerical string LayoutSalvoFormat
										{
											szII.Format("T\\7\\/%s\\%s\\\\", ParseValue(pEvent->m_ppEvents[i]->m_szValue, pItem),pEvent->m_ppEvents[i]->m_szLayer);
										} break;
									}
								}
								else
*/
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Clear")==0)
								{
									szII.Format("T\\14\\%s\\\\", pEvent->m_ppEvents[i]->m_szLayer);
								}
								else  // its a salvo
								{
									switch(g_settings.m_nLayoutSalvoFormat)//pnucleus->m_settings.m_nLayoutSalvoFormat)
									{
									default:
									case TABULATOR_HARRIS_ICONII_LSFORMAT_NUM4://				0 // 4 digit numerical LayoutSalvoFormat
										{
											szII.Format("T\\7\\00%s\\%s\\\\", pEvent->m_ppEvents[i]->m_szIdentifier, pEvent->m_ppEvents[i]->m_szLayer);
										} break;
									case TABULATOR_HARRIS_ICONII_LSFORMAT_NUM8://				1 // 8 digit numerical LayoutSalvoFormat
										{
											szII.Format("T\\7\\0000%s\\%s\\\\", pEvent->m_ppEvents[i]->m_szIdentifier, pEvent->m_ppEvents[i]->m_szLayer);
										}	break;
									case TABULATOR_HARRIS_ICONII_LSFORMAT_ALPHA://			2 // alphanumerical string LayoutSalvoFormat
										{
											szII.Format("T\\7\\/%s\\%s\\\\", pEvent->m_ppEvents[i]->m_szIdentifier, pEvent->m_ppEvents[i]->m_szLayer);
										} break;
									}

/*
// update a region...?
									CString szRegion; 
									CString szTag;
									char* pszValue = bu.XMLEncode(ParseValue(pEvent->m_ppEvents[i]->m_szValue, pItem).GetBuffer(0)); // have to XML encode any values...I think.  this is a point to be clarified.

									int nColon = pEvent->m_ppEvents[i]->m_szIdentifier.ReverseFind(':');
									if(nColon>=0)
									{
										szRegion=pEvent->m_ppEvents[i]->m_szIdentifier.Left(nColon);
										szTag=pEvent->m_ppEvents[i]->m_szIdentifier.Right(nColon+1);
										if(szTag.IsEmpty()) szTag = "Field1"; // "safe" default
									}
									else
									{
										szRegion = pEvent->m_ppEvents[i]->m_szIdentifier;
										szTag = "Field1"; // "safe" default
									}

									szII.Format("I\\42\\<LayoutTags><LayoutName>%s</LayoutName><Region><Name>%s</Name><Tag><Name>%s</Name><Text>%s</Text></Tag></Region></LayoutTags>\\\\", 
										pEvent->m_ppEvents[i]->m_szScene,
										szRegion, szTag, (pszValue?pszValue:""));
									if(pszValue) free(pszValue);
*/

								}

							} break;

						} //switch

					} // if(pEvent->m_ppEvents[i]->m_nType==0)  //anim was true
					else  // export
					{

	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "--- text parse %02d %s", i, pEvent->m_ppEvents[i]->m_szValue); //(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Type is export, with dest type: %d", pEvent->m_ppEvents[i]->m_nDestType);  Sleep(50);//(Dispatch message)
						switch(g_settings.m_nGraphicsHostType)//pEvent->m_ppEvents[i]->m_nDestType) hardcode for now
						{

						case TABULATOR_DESTTYPE_MIRANDA_IS2://					2001  // Imagestore 2
						case TABULATOR_DESTTYPE_MIRANDA_INT://					2002  // Intuition
						case TABULATOR_DESTTYPE_MIRANDA_IS300://				2003  // Imagestore 300
						case TABULATOR_DESTTYPE_MIRANDA_ISHD://				2004  // Imagestore HD
							{
								// setting the datasource
								char* pch = is2.EscapeSpecialChars(ParseValue(pEvent->m_ppEvents[i]->m_szValue, bPreamble, pItem).GetBuffer(0));
								if(pch)
								{
									szII.Format("m003%s|%s",pEvent->m_ppEvents[i]->m_szIdentifier, pch);
									free(pch);
								}
								else
								{
									szII.Format("m003%s|%s",pEvent->m_ppEvents[i]->m_szIdentifier, ParseValue(pEvent->m_ppEvents[i]->m_szValue, bPreamble, pItem));
								}

							} break;

						case TABULATOR_DESTTYPE_CHYRON_CHANNELBOX://		3003  // Channel Box
							{
										//��#
								char* pch = bu.EscapeChar(ParseValue(pEvent->m_ppEvents[i]->m_szValue, bPreamble, pItem).GetBuffer(0), '\\');
								if(pch)
								{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "--- text parsed %02d %s", i, pch); //(Dispatch message)
									szII.Format("B\\UP\\%s\\%s%s�%s\\\\", 
										pEvent->m_ppEvents[i]->m_szScene, 
										pEvent->m_ppEvents[i]->m_szIdentifier, 
										"",//(pnucleus->m_settings.m_bUseUTF8?"�":""),
										pch);
									free(pch);
								}
								else
								{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "--- text not parsed %02d %s", i, pEvent->m_ppEvents[i]->m_szValue); //(Dispatch message)
									szII.Format("B\\UP\\%s\\%s%s�%s\\\\", 
										pEvent->m_ppEvents[i]->m_szScene, 
										pEvent->m_ppEvents[i]->m_szIdentifier, 
										"",//(pnucleus->m_settings.m_bUseUTF8?"�":""),
										ParseValue(pEvent->m_ppEvents[i]->m_szValue, bPreamble, pItem));
								}
							} break;
							// following cases are "Lyric"
/* removing cases
						case TABULATOR_DESTTYPE_CHYRON_LEX://					3000	// Duet LEX
						case TABULATOR_DESTTYPE_CHYRON_HYPERX://				3001  // Duet HyperX
						case TABULATOR_DESTTYPE_CHYRON_MICROX://				3002  // Duet MicroX
						case TABULATOR_DESTTYPE_CHYRON_CALBOX://				3004  // CAL Box
							{
								int idxA = pChannelObj->ReturnAliasIndex(pEvent->m_ppEvents[i]->m_szScene);
								if(idxA>=0)
								{
									char* pch = bu.EscapeChar(ParseValue(pEvent->m_ppEvents[i]->m_szValue, pItem).GetBuffer(0), '\\');
									if(pch)
									{
										if(pChannelObj->m_ppLyricAlias[idxA]->bMessageRead)
											szII.Format("U\\*\\%s\\%s\\\\", pEvent->m_ppEvents[i]->m_szIdentifier, pch);
										else
											szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pEvent->m_ppEvents[i]->m_szIdentifier, pch);
										free(pch);
									}
									else
									{
										if(pChannelObj->m_ppLyricAlias[idxA]->bMessageRead)
											szII.Format("U\\*\\%s\\%s\\\\", pEvent->m_ppEvents[i]->m_szIdentifier, ParseValue(pEvent->m_ppEvents[i]->m_szValue, pItem));
										else
											szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_ppLyricAlias[idxA]->nAlias, pEvent->m_ppEvents[i]->m_szIdentifier, ParseValue(pEvent->m_ppEvents[i]->m_szValue, pItem));

									}
								}
								else
								{
// have to try....  but can't.. theres nothing to find.
/*
									char* pch = bu.EscapeChar(ParseValue(pEvent->m_ppEvents[i]->m_szValue, pItem).GetBuffer(0), '\\');
									if(pch)
									{
										if(pChannelObj->m_bMessageRead)
											szII.Format("U\\*\\%s\\%s\\\\", pEvent->m_ppEvents[i]->m_szIdentifier, pch);
										else
											szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_nLastLyricAlias, pEvent->m_ppEvents[i]->m_szIdentifier, pch);
										free(pch);
									}
									else
									{
										if(pChannelObj->m_bMessageRead)
											szII.Format("U\\*\\%s\\%s\\\\", pEvent->m_ppEvents[i]->m_szIdentifier, ParseValue(pEvent->m_ppEvents[i]->m_szValue, pItem));
										else
											szII.Format("U\\%d\\%s\\%s\\\\", pChannelObj->m_nLastLyricAlias, pEvent->m_ppEvents[i]->m_szIdentifier, ParseValue(pEvent->m_ppEvents[i]->m_szValue, pItem));

									}
*/
/* removing cases
								}
							} break;
removing cases */

						case TABULATOR_DESTTYPE_HARRIS_ICONII://				5000
							{
								CString szRegion; 
								CString szTag;
								char* pszValue = bu.XMLEncode(ParseValue(pEvent->m_ppEvents[i]->m_szValue, bPreamble, pItem).GetBuffer(0)); // have to XML encode any values...I think.  this is a point to be clarified.

								int nColon = pEvent->m_ppEvents[i]->m_szIdentifier.ReverseFind(':');
								if(nColon>=0)
								{
									szRegion=pEvent->m_ppEvents[i]->m_szIdentifier.Left(nColon);
									szTag=pEvent->m_ppEvents[i]->m_szIdentifier.Mid(nColon+1);
									if(szTag.IsEmpty()) szTag = "Field1"; // "safe" default
								}
								else
								{
									szRegion = pEvent->m_ppEvents[i]->m_szIdentifier;
									szTag = "Field1"; // "safe" default
								}

								szII.Format("I\\42\\<LayoutTags><LayoutName>%s</LayoutName><Region><Name>%s</Name><Tag><Name>%s</Name><Text>%s</Text></Tag></Region></LayoutTags>\\\\", 
									pEvent->m_ppEvents[i]->m_szScene,
									szRegion, szTag, (pszValue?pszValue:""));
								if(pszValue) free(pszValue);
							

/*
// IconStation II
IconStation supports four CII commands for greater output flexibility.
The first CII command allows you to load a layout and fire a Salvo.
The second CII command allows you to clear preview and clear output.
The third CII command allows you to poll the IconStation system to confirm IconStation is still running and responding to commands.
The fourth CII command allows you to update your item source text at any time.

Channel Entry
The Channel entry equals A or B.
A = program channel
B = preview channel

T\7\LayoutSalvo\Channel\\   - Load a Layout and Fire a Salvo
T\14\Channel\\   - Clear Preview and Clear Output
*\\   - Poll IconStation (returns *<CRLF>)
I\42\<LayoutTags><LayoutName>TheLayoutNameGoesHere</LayoutName><Region><Name>TheItemNameGoesHere</Name><Tag><Name>TheTagNameGoesHere</Name><Text>TheTextGoesHere</Text></Tag></Region></LayoutTags>\\    - Update Item Source Text
Example Command:
I\42\<LayoutTags><LayoutName>UpNext</LayoutName><Region><Name>Title1</Name><Tag> <Name>Field1</Name><Text>Economic Outlook</Text></Tag></Region></LayoutTags>\\

//stuff has this structure:
<LayoutTags>
<LayoutName>InfoChannel</LayoutName>
<Region> // region 1
<Name>Crawl</Name> // the name of the region
<Tag> // a "tag" within a region
<Name>1-1</Name>  // predefined tag name such as Field1, Field2, 1-1, 2-1, etc, see documentation
<Text>blarg</Text>  // text to put in the tag (field) in the region
</Tag>
</Region>
<Region> // an additional region in the layout - one with multiple "tags"
<Name>Title_area</Name>
<Tag>
<Name>Field1</Name>
<Text>coming up next</Text>
</Tag>
<Tag>
<Name>Field2</Name>
<Text>more text</Text>
</Tag>
</Region>
</LayoutTags>
*/
							
							} break;

						} //switch
					}//if(pEvent->m_ppEvents[i]->m_nType==0)  //anim was false
					
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "II is now: %s", szII);  Sleep(50);//(Dispatch message)
					char* pchEncodedLocal = pdb->EncodeQuotes(szII);

			
					while((	(timebNowTick.time<timebTriggerTick.time)
								||((timebNowTick.time==timebTriggerTick.time)&&(timebNowTick.millitm<timebTriggerTick.millitm))
								)
								&&(g_dlldata.thread[COLLECTOR_TICKER])&&(!g_dlldata.thread[COLLECTOR_TICKER]->bKillThread))
					{
						Sleep(1);
						_ftime(&timebNowTick);
					}

					if((g_settings.m_pszTextEvent)&&(strlen(g_settings.m_pszTextEvent)>0))
					{
						g_data.SendAsRunMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, "%s: Triggered graphics device at %s with: %s", g_settings.m_pszTextEvent, g_settings.m_pszDestinationHost, szII);
					}
					else
					{
						g_data.SendAsRunMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, "Triggered graphics device at %s with: %s", g_settings.m_pszDestinationHost, szII);
					}

					if((pchEncodedLocal)&&(strlen(pchEncodedLocal)>0))
					{
						_snprintf(szCommandSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(local, remote, action, host, timestamp, username) \
VALUES ('%s', '', %d, '%s', %d.%d, 'sys')", //HARDCODE
														g_settings.m_pszModule?g_settings.m_pszModule:"Libretto",//"Libretto", //pEndObj->m_pszDBName?pEndObj->m_pszDBName:"Libretto",  // need to get this.
							//							pEndObj->m_pszQueue?pEndObj->m_pszQueue:"Queue",  // ok need to change this too!  argh! its not the endpoint object, that is the automation module....
														g_settings.m_pszQueue?g_settings.m_pszQueue:"Command_Queue",
														pchEncodedLocal?pchEncodedLocal:szII,
														256,
														g_settings.m_pszDestinationHost,
														timebNowTick.time,
														timebNowTick.millitm
														);
//	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Trigger SQL: %s", szCommandSQL); // Sleep(50); //(Dispatch message)
						if(pdb->ExecuteSQL(pdbConn, szCommandSQL, errorstring)<DB_SUCCESS)
						{
					//**MSG
g_ptabmsgr->DM(MSG_ICONHAND, NULL, szTickerSource, "ERROR executing SQL: %s", errorstring); // Sleep(50); //(Dispatch message)
						}
						else
						{
						
if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_TRIGGER)) 
	{
		g_ptabmsgr->DM(MSG_ICONHAND, NULL, szTickerSource, "command %d sent [est time %d%d][%d%d + trig %.0f]: %s", 
			pEvent->m_ppEvents[i]->m_nEventID,
			timebNowTick.time,
			timebNowTick.millitm,
			timebBeginTick.time,
			timebBeginTick.millitm,
			pEvent->m_ppEvents[i]->m_dblTriggerTime, szII); // Sleep(50);//(Dispatch message)
	}
						}
					} // if(pchEncodedLocal)&& strlen
					if(pchEncodedLocal) free(pchEncodedLocal);
				}//if(pEvent->m_ppEvents[i]->m_szIdentifier.GetLength())

			}// if((pEvent->m_ppEvents)&&(pEvent->m_ppEvents[i]))
			if(g_dlldata.thread[COLLECTOR_TICKER]->bKillThread) break;
			i++;
			
		} // while
		LeaveCriticalSection(&pEvent->m_critEvents);
		return CLIENT_SUCCESS;
	}
	else	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "PlayEvent event exited with no action"); //(Dispatch message)

	return CLIENT_ERROR;
}

CString CCollectorData::ParseValue(CString szValue, bool bPreamble, CCollectorMessage* pItem)
{
	CString szReturn="";
	int nLen = szValue.GetLength();
	int i=0;
	char szTickerSource[MAX_PATH]; sprintf(szTickerSource, "%s_ParamParse", g_settings.m_pszInternalAppName);

	while(i<nLen)
	{
		bool bDelimFound=false;
		char ch = szValue.GetAt(i);
		if(0)//pnucleus->m_settings.m_bUseUTF8)  // support later...
		{
			if((ch == '�')&&(szValue.GetAt(i+1) == '�'))	
			{
				bDelimFound=true; i+=2;
			}
		}
		else
		{
			if(ch == '�')
			{
				bDelimFound=true; i++;
			}
		}

		if(bDelimFound)
		{
			CString szParam = "";

			bDelimFound=false;												
			while((!bDelimFound)&&(i<nLen))
			{
				ch = szValue.GetAt(i);
	//				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "ch: %c",ch);// Sleep(100); //(Dispatch message)
				if(0)//pnucleus->m_settings.m_bUseUTF8)
				{
					if((ch == '�')&&(szValue.GetAt(i+1) == '�'))	
					{
						bDelimFound=true; i+=2;
					}
				}
				else
				{
					if(ch == '�')
					{
						bDelimFound=true; i++;
					}
				}
				if(bDelimFound)
				{
					
if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_PARSE)) 
 g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "end param: %s",szParam);// Sleep(100); //(Dispatch message)

					i--; 
					if(pItem)
					{
						if((g_settings.m_pszMessageToken)&&(szParam.CompareNoCase(g_settings.m_pszMessageToken)==0))
						{
							if(bPreamble) szReturn+=g_settings.m_pszTickerMessagePreamble;
							szReturn+=pItem->m_szMessage;
						}
						else
						if((g_settings.m_pszAux1Token)&&(szParam.CompareNoCase(g_settings.m_pszAux1Token)==0))
						{
							szReturn+=pItem->m_szAux1;
						}
						else
						if((g_settings.m_pszAux2Token)&&(szParam.CompareNoCase(g_settings.m_pszAux2Token)==0))
						{
							szReturn+=pItem->m_szAux2;
						}
					}
				}
				else
				{
					szParam+=ch;
//					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "param: %s",szParam);// Sleep(100); //(Dispatch message)
				}
				i++;
			}
			i--;
		}
		else
		{
			szReturn+=ch;
		}
		i++;
	}

	return szReturn;												
}


int CCollectorData::SetTickerItem(CDBUtil* pdb, CDBconn* pdbConn, CCollectorMessage* pItem, char* pszInfo)
{
	if((pdb)&&(pdbConn)&&(pItem))
	{
		EnterCriticalSection(&m_critTicker);
//		int q = FindTickerID(pItem->m_nID, pItem->m_dblIndex, -1);
		if(0)//q<0)
		{

			// not found, must add
			LeaveCriticalSection(&m_critTicker);
			CCollectorMessage* pNew = new CCollectorMessage;
			if(pNew) *pNew = *pItem;
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SetTickerItem", "inserting ticker item (%d [%s]).", pItem->m_nID, pItem->m_szMessage); //(Dispatch message)
			InsertTickerMessage(pNew);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SetTickerItem", "inserted ticker item (%d [%s]).", pItem->m_nID, pItem->m_szMessage); //(Dispatch message)

			char szSQL[DB_SQLSTRING_MAXLEN];
			char errorstring[DB_SQLSTRING_MAXLEN];
			// insert in db
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SetTickerItem", "get SQL for ticker item (%d [%s]).", pItem->m_nID, pItem->m_szMessage); //(Dispatch message)
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", pNew->SQLUpdate(true, pNew->m_dblIndex)	);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SetTickerItem", "setting ticker item (%d [%s]) SQL [%s].", pNew->m_nID, pNew->m_szMessage, szSQL); //(Dispatch message)

		//					AfxMessageBox(szSQL);
			if (pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
			{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SetTickerItem", "setting ticker item (%d [%s]) error [%s] SQL [%s].", pNew->m_nID, pNew->m_szMessage, errorstring, szSQL); //(Dispatch message)
				//err

			}
			else
			{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SetTickerItem", "set ticker item (%d [%s]) success SQL [%s].", pNew->m_nID, pNew->m_szMessage, szSQL); //(Dispatch message)
				pNew->m_nStatus |= COLLECTOR_MSG_STATUS_INSERTED;
			}
		}
		else
		{
			//found, must modify.
//			if(m_ticker[q]) *m_ticker[q] = *pItem;
		//	g_data.m_nLastFoundIndex = q;

			char szSQL[DB_SQLSTRING_MAXLEN];
			char errorstring[DB_SQLSTRING_MAXLEN];

			// insert or modify in db
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", pItem->SQLUpdate(true,  pItem->m_dblIndex)	);
//			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", m_ticker[q]->SQLUpdate(true,  m_ticker[q]->m_dblIndex)	);
			LeaveCriticalSection(&m_critTicker);

//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SetTickerItem", "setting ticker item (%d [%s]) SQL [%s].", pItem->m_nID, pItem->m_szMessage, szSQL); //(Dispatch message)
		//					AfxMessageBox(szSQL);
			if (pdb->ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
			{
				//err
if(g_ptabmsgr)
{
	char szTickerSource[MAX_PATH]; sprintf(szTickerSource, "%s_SetTickerItem", g_settings.m_pszInternalAppName);
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "set ticker item (%d [%s]) error [%s] SQL [%s].", pItem->m_nID, pItem->m_szMessage, errorstring, szSQL); //(Dispatch message)
}
			}
			else
			{
if((g_ptabmsgr)&&(g_settings.m_ulDebug&COLLECTOR_DEBUG_SQL))
{ 
	char szTickerSource[MAX_PATH]; sprintf(szTickerSource, "%s_SetTickerItem", g_settings.m_pszInternalAppName);
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "set ticker item (%d [%s]) success SQL [%s].", pItem->m_nID, pItem->m_szMessage, szSQL); //(Dispatch message)
}
				pItem->m_nStatus |= COLLECTOR_MSG_STATUS_INSERTED;
//				m_ticker[q]->m_nStatus |= COLLECTOR_MSG_STATUS_INSERTED;
			}
		}
	}
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SetTickerItem", "set ticker item (%d [%s]) complete", pItem->m_nID, pItem->m_szMessage);  Sleep(100);//(Dispatch message)

	return CLIENT_ERROR;
}

#ifndef REMOVE_CLIENTSERVER
int CCollectorData::StringToMessage(CString szString, CCollectorMessage* pevent)
{
	if((pevent)&&(szString.GetLength()))
	{
		char chFields[2];
		sprintf(chFields,"%c",28);
		int f=0, n=0;
		CString szField = szString;
		CString szTemp;
		do
		{
			f=szField.Find(chFields);
			if(f>=0)
			{
				n++;
				szTemp = szField.Mid(f+1);
				szField=szTemp;
			}
		} while((f>=0)&&(szField.GetLength()>0)&&(n<=8));

	//AfxMessageBox(szString);

		f=0;
		if(n>=8)
		{
			while((f<=n)&&(f<9)) // prevent runaway
			{
				int i=szString.Find(chFields);
				if(i>-1){szField = szString.Left(i);}
				else {szField=szString;}  //last field.
	//CString foo; foo.Format(_T("parsing field %d of %d: %s"), f, n, szField); AfxMessageBox(foo);
				switch(f)
				{
				case  0: pevent->m_szMessage = szField; break;
				case  1: pevent->m_szRevisedMessage = szField; break;
				case  2: pevent->m_nTimestamp = atoi(szField); break;
				case  3: pevent->m_nLastUpdate = atoi(szField); break;
				case  4: pevent->m_nID = atoi(szField); break;
				case  5: pevent->m_nStatus = atoi(szField); break;
				case  6: pevent->m_nType = atoi(szField); break;
				case  7: pevent->m_nPlayed = atoi(szField); break;
				case  8: pevent->m_dblIndex = atof(szField); break;
				default: break;
				}
				if(i>-1){szField = szString.Mid(i+1);}
				else {szField=_T("");}
				szString = szField;
				f++;
			}
			return CLIENT_SUCCESS;
		}
	}
	return CLIENT_ERROR;

}

CString CCollectorData::MessageToString(CCollectorMessage* pevent)
{
	CString szString="";
	if(pevent)
	{
		szString.Format(_T("%s%c%s%c%d%c%d%c%d%c%d%c%d%c%d%c%0.06f"),
			pevent->m_szMessage, 28,
			pevent->m_szRevisedMessage, 28,
			pevent->m_nTimestamp, 28,
			pevent->m_nLastUpdate, 28,
			pevent->m_nID, 28,
			pevent->m_nStatus, 28,
			pevent->m_nType, 28,
			pevent->m_nPlayed, 28,
			pevent->m_dblIndex);
	}
	return szString;
}
#endif //#ifndef REMOVE_CLIENTSERVER


int CCollectorData::InitDLLData(DLLthread_t* pData)
{
	if(pData)
	{
		InitializeCriticalSection(&pData->m_crit);
		EnterCriticalSection(&pData->m_crit);
		pData->bKillThread=true;
		pData->bThreadStarted=false;
		pData->nThreadState=0;  //generic
		pData->nThreadControl=0;  //generic
		pData->m_dblLastUpdate=-1.0;
		pData->pszThreadName=NULL;
		strcpy(pData->pszThreadStateText, "");//256 chars max

	// URLs may use format specifiers
		pData->pszBaseURL=NULL;
		pData->pszInfoURL=NULL;
		pData->pszConfirmURL=NULL;

		pData->pszSeriesParamName=NULL;    //  %S
		pData->pszProgramParamName=NULL;   //  %P
		pData->pszEpisodeParamName=NULL;   //  %E

		pData->pszSeriesID=NULL;    //  %s
		pData->pszProgramID=NULL;   //  %p
		pData->pszEpisodeID=NULL;   //  %e

		// some generic parameters
		pData->pszToDateParamName=NULL;				//  %t
		pData->pszFromDateParamName=NULL;			//  %f
		pData->pszNowParamName=NULL;					//  %n
		pData->pszBatchParamName=NULL;				//  %b
		pData->pszFromIDParamName=NULL;				//  %m  // start id
		pData->pszGetIDParamName=NULL;				//  %i

		pData->pszLastID=NULL;  // last ID gotten   %l

		pData->nBatchParam=250;
		pData->nShortIntervalMS=0;
		pData->nLongIntervalMS=5000;
		pData->bDirParamStyle=true;  // true is baseurl/paramname/paramvalue/  false is baseurl?paramname=paramvalue

		_ftime(&pData->timebTick); // the time check inside the thread
		_ftime(&pData->timebLastPullTick); // the last time data pulled inside the thread
		pData->nIDrateAvg=-1;
		pData->nIDrateMax=-1;
		pData->nIDrateMin=-1;
		pData->nNumFailures=0;
		pData->nNumSuccess=0;

		pData->pszHost=NULL;
		pData->nPort=80;
		pData->nAuxValue=-1;

		LeaveCriticalSection(&pData->m_crit);
		return CLIENT_SUCCESS;
	}

	return CLIENT_ERROR;
}


void CCollectorData::PublishData()
{
#ifdef REMOVE_CLIENTSERVER
	return; // to remove client server services, client is now the database
#else
	if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_DATA)&&(g_dlldata.thread[COLLECTOR_DATA])&&(g_dlldata.thread[COLLECTOR_DATA]->bKillThread))
		return;
	//or, spin a thread
	if( _beginthread(PublishDataThread, 0, (void*)this)== -1)
	{
//error
	}
/*
	else
	{

	}
*/
#endif
}

void CCollectorData::PublishTicker()
{
#ifdef REMOVE_CLIENTSERVER
	return; // to remove client server services, client is now the database
#else

	if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_TICKER)&&(g_dlldata.thread[COLLECTOR_TICKER])&&(g_dlldata.thread[COLLECTOR_TICKER]->bKillThread))
		return;

	//or, spin a thread
	if( _beginthread(PublishTickerThread, 0, (void*)this)== -1)
	{
//error
	}
/*
	else
	{

	}
*/
#endif
}

#ifndef REMOVE_CLIENTSERVER

void PublishDataThread( void* pDataObj )
{
//	CCollectorData* pdata = (CCollectorData*)pDataObj;
//	if(CCollectorData == NULL) return;
	if(g_data.m_bPublishingData)
	{
		return;
	}
	DLLthread_t* pthread = NULL;
	if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_DATA)&&(g_dlldata.thread[COLLECTOR_DATA]))
	{
		pthread = g_dlldata.thread[COLLECTOR_DATA];
	}
	else return; // cont do nothin else...!

	double u = pthread->m_dblLastUpdate; 

	if( !pthread->bKillThread )
	{
		g_data.m_bPublishingData = TRUE;
		CCollectorMessage** xx = NULL;

		EnterCriticalSection(&g_data.m_critMsgOut);  //  critical section.
		if(	pthread->bKillThread )
		{
			LeaveCriticalSection(&g_data.m_critMsgOut);
			g_data.m_bPublishingData = FALSE;
			return;
		}
		EnterCriticalSection(&g_data.m_critMsg);  //  critical section.
		if(	pthread->bKillThread )
		{
			LeaveCriticalSection(&g_data.m_critMsg);
			LeaveCriticalSection(&g_data.m_critMsgOut);
			g_data.m_bPublishingData = FALSE;
			return;
		}


		if((!pthread->bKillThread)&&(	g_data.m_nMessagesOutArray<g_data.m_nMessages))
		{
			xx = new CCollectorMessage*[g_data.m_nMessages];
			if(xx)
			{
				if(g_data.m_msgout)
				{
					memcpy(xx, g_data.m_msgout, (g_data.m_nMessagesOutArray)*(sizeof(CCollectorMessage*)));
				}
				while((g_data.m_nMessagesOutArray<g_data.m_nMessages)&&(!pthread->bKillThread))
				{
					xx[g_data.m_nMessagesOutArray] = new CCollectorMessage;
					g_data.m_nMessagesOutArray++;
				}
				if(g_data.m_msgout)
				{
					try
					{
						delete [] g_data.m_msgout;
					}
					catch(...)
					{
					}
				}
				g_data.m_msgout = xx;
			}
		}
		else
		{
			xx=g_data.m_msgout;
		}
		if(xx)
		{
			g_data.m_nMessagesOut=0;
			CCollectorData* pevt = NULL;
			if(g_data.m_nMessages)
			{
				while((g_data.m_nMessagesOut<g_data.m_nMessages)&&(g_data.m_nMessagesOut<g_data.m_nMessagesOutArray)&&(!pthread->bKillThread))
				{
					if(g_data.m_msg[g_data.m_nMessagesOut])
					{
						*(g_data.m_msgout[g_data.m_nMessagesOut]) = *(g_data.m_msg[g_data.m_nMessagesOut]);
					}
					else
					{
						// hmmm... null, should never happen.
						g_data.m_msgout[g_data.m_nMessagesOut] = NULL;
					}
					g_data.m_nMessagesOut++;
				}
			}
		}
		LeaveCriticalSection(&g_data.m_critMsg);  //  critical section.
		LeaveCriticalSection(&g_data.m_critMsgOut);  //  critical section.
	}
	g_data.m_bPublishingData = FALSE;

	if(u<pthread->m_dblLastUpdate)
	{ // there was an increment attempt while we were doing zis.
		//or, spin a thread
		if( _beginthread(PublishDataThread, 0, (void*)pDataObj)== -1)
		{
			return;
		}
		if(u-g_data.m_dblLastDataOutUpdate>1.0) g_data.m_dblLastDataOutUpdate = u; // so as not to totally stall it out if lots of rapid changes
	}
	else
	{
		g_data.m_dblLastDataOutUpdate = pthread->m_dblLastUpdate;
	}

}


void PublishTickerThread( void* pDataObj )
{
//	CCollectorData* pdata = (CCollectorData*)pDataObj;
//	if(CCollectorData == NULL) return;
	if(g_data.m_bPublishingTicker)
	{
		return;
	}
	DLLthread_t* pthread = NULL;
	if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_TICKER)&&(g_dlldata.thread[COLLECTOR_TICKER]))
	{
		pthread = g_dlldata.thread[COLLECTOR_TICKER];
	}
	else return; // cont do nothin else...!

	double u = pthread->m_dblLastUpdate; 

	if( !pthread->bKillThread )
	{
		g_data.m_bPublishingTicker = TRUE;
		CCollectorMessage** xx = NULL;

		EnterCriticalSection(&g_data.m_critTickerOut);  //  critical section.
		if(	pthread->bKillThread )
		{
			LeaveCriticalSection(&g_data.m_critTickerOut);
			g_data.m_bPublishingTicker = FALSE;
			return;
		}
		EnterCriticalSection(&g_data.m_critTicker);  //  critical section.
		if(	pthread->bKillThread )
		{
			LeaveCriticalSection(&g_data.m_critTicker);
			LeaveCriticalSection(&g_data.m_critTickerOut);
			g_data.m_bPublishingTicker = FALSE;
			return;
		}


		if((!pthread->bKillThread)&&(	g_data.m_nTickerMessagesOutArray<g_data.m_nTickerMessages))
		{
			xx = new CCollectorMessage*[g_data.m_nTickerMessages];
			if(xx)
			{
				if(g_data.m_msgout)
				{
					memcpy(xx, g_data.m_tickerout, (g_data.m_nTickerMessagesOutArray)*(sizeof(CCollectorMessage*)));
				}
				while((g_data.m_nTickerMessagesOutArray<g_data.m_nTickerMessages)&&(!pthread->bKillThread))
				{
					xx[g_data.m_nTickerMessagesOutArray] = new CCollectorMessage;
					g_data.m_nTickerMessagesOutArray++;
				}
				if(g_data.m_tickerout)
				{
					try
					{
						delete [] g_data.m_tickerout;
					}
					catch(...)
					{
					}
				}
				g_data.m_tickerout = xx;
			}
		}
		else
		{
			xx=g_data.m_tickerout;
		}
		if(xx)
		{
			g_data.m_nTickerMessagesOut=0;
			CCollectorData* pevt = NULL;
			if(g_data.m_nTickerMessages)
			{
				while((g_data.m_nTickerMessagesOut<g_data.m_nTickerMessages)&&(g_data.m_nTickerMessagesOut<g_data.m_nTickerMessagesOutArray)&&(!pthread->bKillThread))
				{
					if(g_data.m_ticker[g_data.m_nTickerMessagesOut])
					{
						*(g_data.m_tickerout[g_data.m_nTickerMessagesOut]) = *(g_data.m_ticker[g_data.m_nTickerMessagesOut]);
					}
					else
					{
						// hmmm... null, should never happen.
						g_data.m_tickerout[g_data.m_nTickerMessagesOut] = NULL;
					}
					g_data.m_nTickerMessagesOut++;
				}
			}
		}
		LeaveCriticalSection(&g_data.m_critTicker);  //  critical section.
		LeaveCriticalSection(&g_data.m_critTickerOut);  //  critical section.
	}
	g_data.m_bPublishingTicker = FALSE;

	if(u<pthread->m_dblLastUpdate)
	{ // there was an increment attempt while we were doing zis.
		//or, spin a thread
		if( _beginthread(PublishTickerThread, 0, (void*)pDataObj)== -1)
		{
			return;
		}
		if(u-g_data.m_dblLastTickerOutUpdate>1.0) g_data.m_dblLastTickerOutUpdate = u;// so as not to totally stall it out if lots of rapid changes
	}
	else
	{
		g_data.m_dblLastTickerOutUpdate = pthread->m_dblLastUpdate;
	}
}

#endif // #ifndef REMOVE_CLIENTSERVER





