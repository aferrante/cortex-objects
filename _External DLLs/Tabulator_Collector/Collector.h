// Collector.h : main header file for the COLLECTOR DLL
//

#if !defined(AFX_COLLECTOR_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
#define AFX_COLLECTOR_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#import "msxml3.dll" named_guids 
using namespace MSXML2;

//#include "../../../Client/ClientDefines.h"
#include "../../../Common/MSG/Messager.h"
#include "../../../Common/TXT/FileUtil.h"

#include "..\..\..\Common\MFC\Inet\Inet.h"
#include "..\..\..\Common\API\Miranda\IS2Core.h"  // for IS2 util functions

#define REMOVE_CLIENTSERVER  // for web-based UI only


#include "CollectorCore.h"
#include "CollectorData.h"
#include "CollectorDlg.h"
#include "CollectorSettings.h"

#define SETTINGS_FILENAME "collector.ini"

#define VERSION_STRING "1.0.0.1"
#define COLLECTOR_DATA 0
#define COLLECTOR_TICKER 1
#define COLLECTOR_UI 2
#define COLLECTOR_THREADS 3

// from cortex.h but static so just define here.
//send message type
#define CX_SENDMSG_ERROR   0
#define CX_SENDMSG_INFO	   1
#define CX_SENDMSG_NOTE	   2



#define COLLECTOR_DATA_ERROR			0x0001
#define COLLECTOR_DATA_STOPPED		0x0000
#define COLLECTOR_DATA_STARTED		0x0010
#define COLLECTOR_DATA_PAUSED		  0x0040
#define COLLECTOR_DATA_INPROG		  0x0080

#define COLLECTOR_TICKER_ERROR		0x0001
#define COLLECTOR_TICKER_STOPPED  0x0000
#define COLLECTOR_TICKER_STARTED  0x0010
#define COLLECTOR_TICKER_CYCLING  0x0020
#define COLLECTOR_TICKER_PAUSED	  0x0040
#define COLLECTOR_TICKER_INPROG	  0x0080

#define COLLECTOR_SYNCH_ERROR			0x0001
#define COLLECTOR_SYNCH_STOPPED		0x0000
#define COLLECTOR_SYNCH_STARTED		0x0010

#define COLLECTOR_DEBUG_STATUS			0x00000001
#define COLLECTOR_DEBUG_FEED				0x00000002
#define COLLECTOR_DEBUG_TICKER			0x00000004
#define COLLECTOR_DEBUG_EVENTS			0x00000008
#define COLLECTOR_DEBUG_TRIGGER			0x00000010
#define COLLECTOR_DEBUG_PARSE				0x00000020
#define COLLECTOR_DEBUG_SQL					0x00000040

#define COLLECTOR_TICKER_ENGINE_INT		0x0000  // internal ticker engine
#define COLLECTOR_TICKER_ENGINE_VGE		0x0001  // VDS Generic Engine (by wyk)

#define COLLECTOR_EVENT_IN		0x01
#define COLLECTOR_EVENT_OUT		0x02
#define COLLECTOR_EVENT_TEXT	0x04
#define COLLECTOR_EVENT_MOD		0x80

// statuses
/*
#define MSG_STATUS_PLAYED			0x01 // played 
#define MSG_STATUS_ONAIR			0x02 // on air now!
#define MSG_STATUS_ARCHIVED		0x04 // archived
#define MSG_STATUS_COMMIT			0x08 // sent to ticker engine
#define MSG_STATUS_ERROR			0x80 // error

/*
#define DB_ATTRIB_NONE				0x00 // attribute - convenience, reset
#define DB_ATTRIB_MAX					0x10 // highest independent value
#define DB_ATTRIB_NOARC				0x01 // attribute - do not archive item after show - delete after show
#define DB_ATTRIB_REINSNOW		0x02 // attribute - reinsert into active playstack at end (for cycling within clip)
#define DB_ATTRIB_REINSAFTER	0x04 // attribute - reinsert into active playstack at end (for cycling within show, plays once per clip)
#define DB_ATTRIB_SCHED				0x08 // attribute - do not play until after unixdate kept in dwTimeOnAir
#define DB_ATTRIB_EXP					0x10 // attribute - expire item after unixdate kept in dwDuration 
*/

/////////////////////////////////////////////////////////////////////////////
// CCollectorApp
// See Collector.cpp for the implementation of this class
//

class CCollectorApp : public CWinApp
{
public:
	CCollectorApp();
	int  DLLCtrl(void** ppvoid, UINT nType);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCollectorApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	char m_szSettingsFilename[MAX_PATH];

	//{{AFX_MSG(CCollectorApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLLECTOR_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
