// CollectorSettings.cpp : implementation file
//

#include "stdafx.h"
#include "Collector.h"
#include "CollectorSettings.h"
#include "CollectorTestFeed.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


extern DLLdata_t      g_dlldata;
extern CCollectorData			g_data;
extern CCollectorSettings	g_settings;
extern CMessager*			g_pmsgr;
extern CCollectorApp				theApp;

/////////////////////////////////////////////////////////////////////////////
// CCollectorSettings dialog


CCollectorSettings::CCollectorSettings(CWnd* pParent /*=NULL*/)
	: CDialog(CCollectorSettings::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCollectorSettings)
	m_bAutoconnect = FALSE;
	m_bTickerPlayOnce = TRUE;
	m_szHost = _T("127.0.0.1");
	m_szBaseURL = _T("http://www.feed.com/");
	m_nBatch = 250;
	m_nPort = TABULATOR_PORT_CMD;
	m_nPollIntervalMS = 5000;
	m_szProgramID = _T("10");
	m_nRequestIntervalMS = 0;
	m_nTickerPort = 7795;
	m_szTickerHost = _T("127.0.0.1");
	m_bSmut_ReplaceCurses = FALSE;
	m_nDoneCount = 3;
	//}}AFX_DATA_INIT

	m_ulDebug = 0;

	m_crCommitted = RGB(255,0,0);

	m_nDefaultDwellTime = 10000;

	m_bHasDlg = true;  //set to false to disable DoModal;
	m_bIsServer = false;
	m_bObtainedFromServer = false;
	m_bFromDialog = false;
	m_bUseFeed = true;

	m_pszAppName = NULL;
	m_pszInternalAppName = NULL;
	m_pszAboutText = NULL;
	m_pszSettingsText = NULL;

	m_pszMessageToken = NULL; 
	m_pszAux1Token = NULL; 
	m_pszAux2Token = NULL; 


	// DSN params
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;

	m_pszFeed = NULL;  // the Messages table name
	m_pszTicker = NULL;
	m_pszAsRunFilename = NULL;
	m_pszAsRunDestination = NULL;

	m_pszMessages = NULL;  // the Messages table name
	m_pszAsRun = NULL;  // the As-run table name
//	m_pszEventView = NULL;  // the Event view name // to pull events if nec.
	m_pszSettings = NULL;

	m_pszDestinationHost = NULL;

	m_pszTickerMessagePreamble = NULL;

	m_pszTickerBackplateName = NULL; 
	m_pszTickerLogoName = NULL; 
	m_nTickerBackplateID=0; 
	m_nTickerLogoID=2; 
	m_nTickerTextID=1;
	m_nTriggerBuffer  =12;

	m_nTickerEngineType = COLLECTOR_TICKER_ENGINE_INT; // send events to Libretto or wherever!

	m_nMaxMessages = 500;

	m_pszModule=NULL;  // the module name (Libretto)
	m_pszQueue=NULL;  // the module's queue table name (Queue of Libretto.dbo.Queue)

	m_pszInEvent=NULL;  // the event identifier of the transition in (Server_Ticker_In) command
	m_pszOutEvent=NULL;  // the event identifier of the transition out (Server_Ticker_Out) command
	m_pszTextEvent=NULL;  // the event identifier of the text message event

	m_nGraphicsHostType = TABULATOR_DESTTYPE_CHYRON_CHANNELBOX;

	m_bAutoPurgeExpired=false;
	m_bAutoPurgeExceeded=false;
	m_nAutoPurgeExpiredAfterMins=60;
	m_nAutoPurgeExceededAfterMins=60;

	m_bAutostartTicker=false;
	m_bAutostartData=false;
	m_bUseLocalTime=true;  // or use unixtime


	m_nLayoutSalvoFormat = TABULATOR_HARRIS_ICONII_LSFORMAT_ALPHA;

	InitializeCriticalSection(&m_crit);
}

CCollectorSettings::~CCollectorSettings()
{
	EnterCriticalSection(&m_crit);
	if(m_pszAppName) free(m_pszAppName);
	if(m_pszInternalAppName) free(m_pszInternalAppName);
	if(m_pszAboutText) free(m_pszAboutText);
	if(m_pszSettingsText) free(m_pszSettingsText);

	if(m_pszDSN) free(m_pszDSN);
	if(m_pszUser) free(m_pszUser);
	if(m_pszPW) free(m_pszPW);
	if(m_pszFeed) free(m_pszFeed);
	if(m_pszTicker) free(m_pszTicker);
	if(m_pszAsRunFilename) free(m_pszAsRunFilename);
	if(m_pszTickerMessagePreamble) free(m_pszTickerMessagePreamble);
	if(m_pszTickerBackplateName) free(m_pszTickerBackplateName);
	if(m_pszTickerLogoName) free(m_pszTickerLogoName);
	if(m_pszAsRun) free(m_pszAsRun);
	if(m_pszSettings) free(m_pszSettings);
	if(m_pszDestinationHost) free(m_pszDestinationHost);
	if(m_pszAsRunDestination) free(m_pszAsRunDestination);

	if(m_pszModule) free(m_pszModule);
	if(m_pszQueue) free(m_pszQueue);
	if(m_pszInEvent) free(m_pszInEvent);
	if(m_pszOutEvent) free(m_pszOutEvent);
	if(m_pszTextEvent) free(m_pszTextEvent);

	if(m_pszMessageToken) free(m_pszMessageToken);
	if(m_pszAux1Token) free(m_pszAux1Token);
	if(m_pszAux2Token) free(m_pszAux2Token);

	LeaveCriticalSection(&m_crit);
	DeleteCriticalSection(&m_crit);


}


void CCollectorSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCollectorSettings)
	DDX_Check(pDX, IDC_CHECK_AUTOCONNECT, m_bAutoconnect);
	DDX_Check(pDX, IDC_CHECK_PLAYONCE, m_bTickerPlayOnce);
	DDX_CBString(pDX, IDC_COMBO_HOST, m_szHost);
	DDX_Text(pDX, IDC_EDIT_BASEURL, m_szBaseURL);
	DDX_Text(pDX, IDC_EDIT_BATCH, m_nBatch);
	DDV_MinMaxInt(pDX, m_nBatch, 0, 2000000);
	DDX_Text(pDX, IDC_EDIT_C_PORT, m_nPort);
	DDV_MinMaxInt(pDX, m_nPort, 1024, 65535);
	DDX_Text(pDX, IDC_EDIT_POLL, m_nPollIntervalMS);
	DDV_MinMaxInt(pDX, m_nPollIntervalMS, 0, 86400000);
	DDX_Text(pDX, IDC_EDIT_PROGRAM, m_szProgramID);
	DDX_Text(pDX, IDC_EDIT_REQ, m_nRequestIntervalMS);
	DDV_MinMaxInt(pDX, m_nRequestIntervalMS, 0, 86400000);
	DDX_Text(pDX, IDC_EDIT_T_PORT, m_nTickerPort);
	DDV_MinMaxInt(pDX, m_nTickerPort, 1024, 65535);
	DDX_Text(pDX, IDC_EDIT_HOST, m_szTickerHost);
	DDX_Check(pDX, IDC_CHECK_SMUT, m_bSmut_ReplaceCurses);
	DDX_Text(pDX, IDC_EDIT_DONE, m_nDoneCount);
	DDV_MinMaxInt(pDX, m_nDoneCount, 0, 2000000);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCollectorSettings, CDialog)
	//{{AFX_MSG_MAP(CCollectorSettings)
	ON_BN_CLICKED(IDC_BUTTON_TESTFEED, OnButtonTestfeed)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_CHECK_PLAYONCE, OnCheckPlayonce)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCollectorSettings message handlers

CString CCollectorSettings::RemoteSettingsToString()
{
	CString szSettings="";
	if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_TICKER)&&(g_dlldata.thread[COLLECTOR_DATA])&&(g_dlldata.thread[COLLECTOR_TICKER])) 
	{
		szSettings.Format(_T("%s%c%s%c%s%c%s%c%s%c%s%c%d%c%d%c%d%c%d%c%s%c%d%c%d%c%d%c%s%c%d%c%d%c%d"),
				g_dlldata.thread[COLLECTOR_DATA]->pszThreadName, 28,
				g_dlldata.thread[COLLECTOR_DATA]->pszBaseURL, 28,
				g_dlldata.thread[COLLECTOR_DATA]->pszProgramParamName, 28,
				g_dlldata.thread[COLLECTOR_DATA]->pszProgramID, 28,
				g_dlldata.thread[COLLECTOR_DATA]->pszBatchParamName, 28,
				g_dlldata.thread[COLLECTOR_DATA]->pszFromIDParamName, 28,
				g_dlldata.thread[COLLECTOR_DATA]->nBatchParam, 28,
				g_dlldata.thread[COLLECTOR_DATA]->nShortIntervalMS, 28,
				g_dlldata.thread[COLLECTOR_DATA]->nLongIntervalMS, 28,
				(g_dlldata.thread[COLLECTOR_DATA]->bDirParamStyle?1:0), 28,
				g_dlldata.thread[COLLECTOR_TICKER]->pszThreadName, 28,
				g_dlldata.thread[COLLECTOR_TICKER]->nBatchParam, 28,
				g_dlldata.thread[COLLECTOR_TICKER]->nShortIntervalMS, 28,
				g_dlldata.thread[COLLECTOR_TICKER]->nLongIntervalMS, 28,
				g_dlldata.thread[COLLECTOR_TICKER]->pszHost, 28,
				g_dlldata.thread[COLLECTOR_TICKER]->nPort, 28,
				g_dlldata.thread[COLLECTOR_TICKER]->nAuxValue, 28,
				g_settings.m_nDoneCount);
	}

	return  szSettings;
}

int CCollectorSettings::StringToRemoteSettings(CString szSettings)
{
	if(szSettings.GetLength())
	{

		char chFields[2];
		sprintf(chFields,"%c",28);
		int f=0, n=0;
		CString szField = szSettings;
		CString szTemp;
		do
		{
			f=szField.Find(chFields);
			if(f>=0)
			{
				n++;
				szTemp = szField.Mid(f+1);
				szField=szTemp;
			}
		} while((f>=0)&&(szField.GetLength()>0)&&(n<=17));

//	AfxMessageBox(szSettings);

		f=0;
		if(n>=16)
		{
			while((f<=n)&&(f<18)) // prevent runaway
			{
				int i=szSettings.Find(chFields);
				if(i>-1){szField = szSettings.Left(i);}
				else {szField=szSettings;}  //last field.

				char* pch = NULL;
				char* pchTemp = NULL;
	//CString foo; foo.Format(_T("parsing field %d of %d: %s"), f, n, szField); AfxMessageBox(foo);
				switch(f)
				{
				case  0: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_DATA)&&(g_dlldata.thread[COLLECTOR_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
								pchTemp = g_dlldata.thread[COLLECTOR_DATA]->pszThreadName;
								g_dlldata.thread[COLLECTOR_DATA]->pszThreadName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  1: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_DATA)&&(g_dlldata.thread[COLLECTOR_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
								pchTemp = g_dlldata.thread[COLLECTOR_DATA]->pszBaseURL;
								g_dlldata.thread[COLLECTOR_DATA]->pszBaseURL  = pch;
								LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
								g_settings.m_szBaseURL = szField;
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  2: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_DATA)&&(g_dlldata.thread[COLLECTOR_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
								pchTemp = g_dlldata.thread[COLLECTOR_DATA]->pszProgramParamName;
								g_dlldata.thread[COLLECTOR_DATA]->pszProgramParamName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  3: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_DATA)&&(g_dlldata.thread[COLLECTOR_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
								pchTemp = g_dlldata.thread[COLLECTOR_DATA]->pszProgramID;
								g_dlldata.thread[COLLECTOR_DATA]->pszProgramID  = pch;
								LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
								g_settings.m_szProgramID = szField;
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  4: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_DATA)&&(g_dlldata.thread[COLLECTOR_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
								pchTemp = g_dlldata.thread[COLLECTOR_DATA]->pszBatchParamName;
								g_dlldata.thread[COLLECTOR_DATA]->pszBatchParamName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  5: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_DATA)&&(g_dlldata.thread[COLLECTOR_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
								pchTemp = g_dlldata.thread[COLLECTOR_DATA]->pszFromIDParamName;
								g_dlldata.thread[COLLECTOR_DATA]->pszFromIDParamName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
//									pData->pszToDateParamName = fu.GetIniString("DataDownload", "ToDateParamName", "To", pData->pszToDateParamName);   //  %p
//									pData->pszFromDateParamName = fu.GetIniString("DataDownload", "FromDateParamName", "From", pData->pszFromDateParamName);   //  %p
//									pData->pszNowParamName = fu.GetIniString("DataDownload", "NowParamName", "time", pData->pszNowParamName);   //  %p
//									pData->pszBatchParamName = fu.GetIniString("DataDownload", "BatchParamName", "limit", pData->pszBatchParamName);   //  %p
//									pData->pszFromIDParamName = fu.GetIniString("DataDownload", "FromIDParamName", "startid", pData->pszFromIDParamName);   //  %p
//									pData->pszGetIDParamName = fu.GetIniString("DataDownload", "GetIDParamName", "id", pData->pszGetIDParamName);   //  %p

//									pData->pszToDateParamName=NULL;				//  %t
//									pData->pszFromDateParamName=NULL;			//  %f
//									pData->pszNowParamName=NULL;					//  %n
//									pData->pszBatchParamName=NULL;				//  %b
//									pData->pszFromIDParamName=NULL;				//  %m  // start id
//									pData->pszGetIDParamName=NULL;				//  %i
				case  6: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_DATA)&&(g_dlldata.thread[COLLECTOR_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
							g_dlldata.thread[COLLECTOR_DATA]->nBatchParam  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
							g_settings.m_nBatch = atoi(szField);
						}
					} break;
				case  7: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_DATA)&&(g_dlldata.thread[COLLECTOR_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
							g_dlldata.thread[COLLECTOR_DATA]->nShortIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
							g_settings.m_nRequestIntervalMS = atoi(szField);
						}
					} break;
				case  8: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_DATA)&&(g_dlldata.thread[COLLECTOR_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
							g_dlldata.thread[COLLECTOR_DATA]->nLongIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
							g_settings.m_nPollIntervalMS = atoi(szField);
						}
					} break;
				case  9: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_DATA)&&(g_dlldata.thread[COLLECTOR_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
							g_dlldata.thread[COLLECTOR_DATA]->bDirParamStyle  = ((atoi(szField)>0)?true:false);
							LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_DATA]->m_crit);
						}
					} break;



				case  10: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_TICKER)&&(g_dlldata.thread[COLLECTOR_TICKER])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[COLLECTOR_TICKER]->m_crit);
								pchTemp = g_dlldata.thread[COLLECTOR_TICKER]->pszThreadName;
								g_dlldata.thread[COLLECTOR_TICKER]->pszThreadName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_TICKER]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  11: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_TICKER)&&(g_dlldata.thread[COLLECTOR_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[COLLECTOR_TICKER]->m_crit);
							g_dlldata.thread[COLLECTOR_TICKER]->nBatchParam  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_TICKER]->m_crit);
						}
					} break;
				case  12: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_TICKER)&&(g_dlldata.thread[COLLECTOR_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[COLLECTOR_TICKER]->m_crit);
							g_dlldata.thread[COLLECTOR_TICKER]->nShortIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_TICKER]->m_crit);
						}
					} break;
				case  13: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_TICKER)&&(g_dlldata.thread[COLLECTOR_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[COLLECTOR_TICKER]->m_crit);
							g_dlldata.thread[COLLECTOR_TICKER]->nLongIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_TICKER]->m_crit);
						}
					} break;
				case  14: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_TICKER)&&(g_dlldata.thread[COLLECTOR_TICKER])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[COLLECTOR_TICKER]->m_crit);
								pchTemp = g_dlldata.thread[COLLECTOR_TICKER]->pszHost;
								g_dlldata.thread[COLLECTOR_TICKER]->pszHost  = pch;
								LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_TICKER]->m_crit);
								g_settings.m_szTickerHost = szField;
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  15: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_TICKER)&&(g_dlldata.thread[COLLECTOR_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[COLLECTOR_TICKER]->m_crit);
							g_dlldata.thread[COLLECTOR_TICKER]->nPort  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_TICKER]->m_crit);
							g_settings.m_nTickerPort = atoi(szField);
						}
					} break;
				case  16: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>COLLECTOR_TICKER)&&(g_dlldata.thread[COLLECTOR_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[COLLECTOR_TICKER]->m_crit);
							g_dlldata.thread[COLLECTOR_TICKER]->nAuxValue  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[COLLECTOR_TICKER]->m_crit);
							g_settings.m_bTickerPlayOnce = ((atoi(szField)>0)?true:false);
						}
					} break;
				case  17: 
					{
						g_settings.m_nDoneCount = atoi(szField);
					} break;

				default: break;
				}
				if(i>-1){szField = szSettings.Mid(i+1);}
				else {szField=_T("");}
				szSettings = szField;
				f++;
			}
			return CLIENT_SUCCESS;
		}
	}
	return CLIENT_ERROR;
	
	
	
}


int CCollectorSettings::Settings(bool bRead)
{
	CFileUtil fu;
	if(fu.GetSettings(theApp.m_szSettingsFilename, false)&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
			char* pch;
			pch = fu.GetIniString("Global", "AppName", "Collector");
			if(pch)
			{
				if(m_pszAppName) free(m_pszAppName);
				m_pszAppName = pch;
			}

			pch = fu.GetIniString("Global", "InternalAppName", "Collector");
			if(pch)
			{
				if(m_pszInternalAppName) free(m_pszInternalAppName);
				m_pszInternalAppName = pch;
			}

			pch = (char*)malloc(strlen(m_pszAppName)+strlen(VERSION_STRING)+strlen(", version ")+1);
			if(pch)
			{
				if(m_pszAboutText) free(m_pszAboutText);
				m_pszAboutText = pch;
				sprintf(m_pszAboutText, "%s, version %s", m_pszAppName, VERSION_STRING);
			}

			pch = fu.GetIniString("Global", "SettingsText", "Collector settings...");
			if(pch)
			{
				if(m_pszSettingsText) free(m_pszSettingsText);
				m_pszSettingsText = pch;
			}

			pch = fu.GetIniString("Client", "Host", "127.0.0.1");
			if(pch)
			{
				m_szHost.Format("%s", pch);
				free(pch); pch=NULL;
			}
			m_nPort = fu.GetIniInt("Client", "Port", TABULATOR_PORT_CMD);
			m_bAutoconnect = fu.GetIniInt("Client", "Autoconnect", 1)?true:false;
			m_ulDebug = fu.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.
			
			m_bIsServer = fu.GetIniInt("Global", "IsServer", 0)?true:false;

			m_bUseFeed = fu.GetIniInt("Global", "UseFeed", 0)?true:false;

			if(m_bIsServer)
			{

				m_nMaxMessages = fu.GetIniInt("Global", "MaxMessages", 500);
				m_pszDSN = fu.GetIniString("Database", "DSN", "Tabulator", m_pszDSN);
				m_pszUser = fu.GetIniString("Database", "DBUser", "sa", m_pszUser);
				m_pszPW = fu.GetIniString("Database", "DBPassword", "", m_pszPW);
//				m_pszSettings = fu.GetIniString("Database", "SettingsTableName", "Settings", m_pszSettings );  // the Settings table name
				m_pszTicker = fu.GetIniString("Database", "TickerTableName", "Ticker", m_pszTicker);  // the Exchange table name
				m_pszFeed = fu.GetIniString("Database", "FeedTableName", "Feed", m_pszFeed);  // the Messages table name
				m_pszAsRunFilename = fu.GetIniString("Ticker", "AsRunFileSpec", "collector_asrun|YD||1|", m_pszAsRunFilename);  // the as run filename
				m_pszAsRunDestination = fu.GetIniString("Ticker", "AsRunToken", "collector_asrun", m_pszAsRunDestination);  // the as run destination name

				m_pszTickerMessagePreamble = fu.GetIniString("Ticker", "MessagePreamble", "   �   ", m_pszTickerMessagePreamble);

				m_pszTickerBackplateName = fu.GetIniString("Ticker", "TickerBackplateName", "BACKPLATE", m_pszTickerBackplateName);
				m_pszTickerLogoName = fu.GetIniString("Ticker", "TickerLogoName", "LOGO", m_pszTickerLogoName);
				m_nTickerBackplateID=fu.GetIniInt("Ticker", "TickerBackplateID", 0); 
				m_nTickerLogoID=fu.GetIniInt("Ticker", "TickerLogoID", 2); 
				m_nTickerTextID=fu.GetIniInt("Ticker", "TickerTextID", 1); 

//				m_nDefaultDwellTime=fu.GetIniInt("DefaultValues", "MessageDwellTime", 10000);
				
				m_nTriggerBuffer  = fu.GetIniInt("Ticker", "TriggerBufferItems", 12);
				if(m_nTriggerBuffer>127) m_nTriggerBuffer=127;
				if(m_nTriggerBuffer<1) m_nTriggerBuffer=12; //default

				m_pszMessages=fu.GetIniString("Database", "MessagesTableName", "Messages", m_pszMessages);   // the Messages table name
				m_pszAsRun=fu.GetIniString("Database", "AsRunTableName", "AsRun", m_pszAsRun);  // the As-run table name
				m_pszSettings=fu.GetIniString("Database", "SettingsTableName", "Collector_1_Settings", m_pszSettings);  // the settings table name

				m_pszDestinationHost=fu.GetIniString("Ticker", "GraphicsHost", "127.0.0.1", m_pszDestinationHost);  // do we want to direct drive the host ever?  or always shoot to Libretto or other module.? (still need the host either way but need to know what drive mode)
				m_nGraphicsHostType = fu.GetIniInt("Ticker", "GraphicsHostType", TABULATOR_DESTTYPE_CHYRON_CHANNELBOX);

				m_pszModule=fu.GetIniString("Ticker", "GraphicsModule", "Libretto", m_pszModule);  // the module name (Libretto)
				m_pszQueue=fu.GetIniString("Ticker", "GraphicsQueue", "Command_Queue", m_pszQueue); // the module's queue table name (Queue of Libretto.dbo.Queue)


				char setbuf[MAX_PATH];
				if(m_pszInEvent) strcpy(setbuf, m_pszInEvent); else strcpy(setbuf, "");
				m_pszInEvent=fu.GetIniString("Ticker", "InEventID", "1000", m_pszInEvent);  // the event identifier of the transition in (Server_Ticker_In) command
									EnterCriticalSection(&g_data.m_critEventsSettings);
				if (strcmp(setbuf, m_pszInEvent)) 
				{
									EnterCriticalSection(&g_data.m_critEventsSettings);
					g_data.m_bInEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);
				}
				if(m_pszOutEvent) strcpy(setbuf, m_pszOutEvent); else strcpy(setbuf, "");
				m_pszOutEvent=fu.GetIniString("Ticker", "OutEventID", "1001", m_pszOutEvent);  // the event identifier of the transition out (Server_Ticker_Out) command
									EnterCriticalSection(&g_data.m_critEventsSettings);
				if (strcmp(setbuf, m_pszOutEvent)) 
				{
									EnterCriticalSection(&g_data.m_critEventsSettings);
					g_data.m_bOutEventChanged = true;  // the event identifier of the transition out (Server_Ticker_Out) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);
				}
				if(m_pszTextEvent) strcpy(setbuf, m_pszTextEvent); else strcpy(setbuf, "");
				m_pszTextEvent=fu.GetIniString("Ticker", "TextEventID", "1002", m_pszTextEvent);  // the event identifier of the text message event
				if (strcmp(setbuf, m_pszTextEvent))
				{
									EnterCriticalSection(&g_data.m_critEventsSettings);
					g_data.m_bTextEventChanged = true;  // the event identifier of the text message event has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);
				}
				m_pszMessageToken=fu.GetIniString("Ticker", "MessageToken", "Collector|1|Message", m_pszMessageToken);  // the paramater identifier of the text field
				m_pszAux1Token=fu.GetIniString("Ticker", "Aux1Token", "Collector|2|Aux1", m_pszAux1Token);   // the paramater identifier of the aux1 field
				m_pszAux2Token=fu.GetIniString("Ticker", "Aux2Token", "Collector|3|Aux2", m_pszAux2Token);   // the paramater identifier of the aux2 field

				m_nDoneCount= fu.GetIniInt("Ticker", "DoneCount", 3); 
				m_bAutostartTicker=fu.GetIniInt("Ticker", "AutoStart", 0)?true:false;
				m_bAutostartData=fu.GetIniInt("DataDownload", "AutoStart", 0)?true:false;
				m_bUseLocalTime=fu.GetIniInt("Ticker", "UseLocalTime", 1)?true:false;  // or use unixtime

				// graphics specific stuff:
				m_nLayoutSalvoFormat = fu.GetIniInt("Icon", "LayoutSalvoFormat", TABULATOR_HARRIS_ICONII_LSFORMAT_ALPHA);

				m_bAutoPurgeExpired = fu.GetIniInt("Ticker", "AutoPurgeExpired", 0)?true:false;
				m_bAutoPurgeExceeded = fu.GetIniInt("Ticker", "AutoPurgeExceeded", 0)?true:false;
				m_nAutoPurgeExpiredAfterMins = fu.GetIniInt("Ticker", "AutoPurgeExpiredAfterMins", 60);
				m_nAutoPurgeExceededAfterMins = fu.GetIniInt("Ticker", "AutoPurgeExceededAfterMins", 60);
				m_nDefaultDwellTime = fu.GetIniInt("Ticker", "dwell_time", 10000);

//CString foo; foo.Format("[%s]", m_pszTickerMessagePreamble);
//if(g_pmsgr) g_pmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "preamble setting", "%s", foo);  //(Dispatch message)
//else AfxMessageBox(foo);
				m_bObtainedFromServer = false;
				if(g_dlldata.nNumThreads>COLLECTOR_TICKER)
				{

					DLLthread_t* pData = NULL;

					int n=0;

					while(n<g_dlldata.nNumThreads)
					{
						if((g_dlldata.thread)&&(g_dlldata.thread[n]))
						{
							pData = g_dlldata.thread[n];
							switch(n)
							{
							case COLLECTOR_DATA:// 0
								{
									EnterCriticalSection(&pData->m_crit);

									pData->pszThreadName = fu.GetIniString("DataDownload", "Threadname", "DataDownloader", pData->pszThreadName);

								// URLs may use format specifiers

									pData->pszBaseURL = fu.GetIniString("DataDownload", "BaseURL", "http://services.collector.com/feed/list/", pData->pszBaseURL);

		//							AfxMessageBox(pData->pszBaseURL);
									m_szBaseURL = pData->pszBaseURL;

//									pData->pszInfoURL=NULL;
//									pData->pszConfirmURL=NULL;

//									pData->pszSeriesParamName=NULL;    //  %S
									pData->pszProgramParamName = fu.GetIniString("DataDownload", "ProgramParamName", "program", pData->pszProgramParamName);
//									pData->pszEpisodeParamName=NULL;   //  %E

//									pData->pszSeriesID=NULL;    //  %s
									pData->pszProgramID = fu.GetIniString("DataDownload", "ProgramID", "10", pData->pszProgramID);   //  %p
									m_szProgramID = pData->pszProgramID;
//									pData->pszEpisodeID=NULL;   //  %e

									// some generic parameters
									pData->pszToDateParamName = fu.GetIniString("DataDownload", "ToDateParamName", "To", pData->pszToDateParamName);   //  %p
									pData->pszFromDateParamName = fu.GetIniString("DataDownload", "FromDateParamName", "From", pData->pszFromDateParamName);   //  %p
									pData->pszNowParamName = fu.GetIniString("DataDownload", "NowParamName", "time", pData->pszNowParamName);   //  %p
									pData->pszBatchParamName = fu.GetIniString("DataDownload", "BatchParamName", "limit", pData->pszBatchParamName);   //  %p
									pData->pszFromIDParamName = fu.GetIniString("DataDownload", "FromIDParamName", "startid", pData->pszFromIDParamName);   //  %p
									pData->pszGetIDParamName = fu.GetIniString("DataDownload", "GetIDParamName", "id", pData->pszGetIDParamName);   //  %p

//									pData->pszToDateParamName=NULL;				//  %t
//									pData->pszFromDateParamName=NULL;			//  %f
//									pData->pszNowParamName=NULL;					//  %n
//									pData->pszBatchParamName=NULL;				//  %b
//									pData->pszFromIDParamName=NULL;				//  %m  // start id
//									pData->pszGetIDParamName=NULL;				//  %i

									pData->nBatchParam = fu.GetIniInt("DataDownload", "BatchParam", 250);   //  %p
									m_nBatch = pData->nBatchParam;
									pData->nShortIntervalMS = fu.GetIniInt("DataDownload", "ShortIntervalMS", 0);   //  %p
									m_nRequestIntervalMS = pData->nShortIntervalMS;
									pData->nLongIntervalMS = fu.GetIniInt("DataDownload", "LongIntervalMS", 5000);   //  %p
									m_nPollIntervalMS = pData->nLongIntervalMS;
									pData->bDirParamStyle = fu.GetIniInt("DataDownload", "DirParamStyle", 1)?true:false;   //  %p

									LeaveCriticalSection(&pData->m_crit);

								} break;
							case COLLECTOR_TICKER:// 1
								{
									EnterCriticalSection(&pData->m_crit);

									pData->pszThreadName = fu.GetIniString("Ticker", "Threadname", "TickerEngine", pData->pszThreadName);
									pData->nBatchParam = fu.GetIniInt("Ticker", "BatchParam", 10);   //  %p
									pData->nShortIntervalMS = fu.GetIniInt("Ticker", "ShortIntervalMS", 1);   //  %p
									pData->nLongIntervalMS = fu.GetIniInt("Ticker", "LongIntervalMS", 5000);   //  %p

									pData->pszHost = fu.GetIniString("Ticker", "GraphicsEngineHost", "127.0.0.1", pData->pszHost);  
									m_szTickerHost = pData->pszHost;
									pData->nPort = fu.GetIniInt("Ticker", "GraphicsEnginePort", 7795);  
									m_nTickerPort = pData->nPort;
									pData->nAuxValue = fu.GetIniInt("Ticker", "PlayItemsOnce", 1);  
									m_bTickerPlayOnce = ((pData->nAuxValue>0)?true:false);
									
									LeaveCriticalSection(&pData->m_crit);

								} break;
							case COLLECTOR_UI:// 2
								{
									EnterCriticalSection(&pData->m_crit);

									pData->pszThreadName = fu.GetIniString("Synchronizer", "Threadname", "Synchronizer", pData->pszThreadName);
									pData->nBatchParam = fu.GetIniInt("Synchronizer", "BatchParam", 250);   //  %p
									pData->nShortIntervalMS = fu.GetIniInt("Synchronizer", "ShortIntervalMS", 100);   //  %p
									pData->nLongIntervalMS = fu.GetIniInt("Synchronizer", "LongIntervalMS", 5000);   //  %p

									LeaveCriticalSection(&pData->m_crit);

								} break;
							}
						}
	

						n++;
					}


				}

			}
			else
			{

				// local UI settings;

				m_bSmut_ReplaceCurses = fu.GetIniInt("UserInterface", "SmutReplaceCurses", 0)?true:false;   //  %p


				// have to get settings from server.
				int nReturn = NET_SUCCESS;
				char info[8192];
				if(g_data.m_socket==NULL)
				{
	//EnterCriticalSection(&g_data.m_critClientSocket);
					nReturn = g_data.m_net.OpenConnection(m_szHost.GetBuffer(0), m_nPort, &g_data.m_socket, 
						10000, 10000, info);
					if(nReturn<NET_SUCCESS)
					{
	//LeaveCriticalSection(&g_data.m_critClientSocket);
						//MessageBox(info, "Network Error", MB_OK);
					}
					else
					{
	//LeaveCriticalSection(&g_data.m_critClientSocket);
					}

				}

				if(g_data.m_socket==NULL)
				{
					CString szError;
					szError.Format("Could not obtain settings from server (error %d).\nSettings are default or last-known values, not necessarily representative of the actual server settings.\n\nError message:\n%s", nReturn, info);
					MessageBox(szError, "Network Error", MB_OK);
					m_bObtainedFromServer = false;
				}
				else
				{
					// get the settings!
					CNetData* pdata = new CNetData;
					if(pdata)
					{

						pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

						pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
						pdata->m_ucSubCmd = 0;       // the subcommand byte

						char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
						if(pch)
						{
							sprintf(pch, "%s|Server_Get_Setting", m_pszInternalAppName);

							pdata->m_pucData =  (unsigned char*) pch;
							pdata->m_ulDataLen = strlen(pch);
						}

				//AfxMessageBox("sending");
				//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
				//AfxMessageBox( (char*)pdata->m_pucData );
						//char info[8192];
				//if(!//g_data.m_bTransact)
				{
					//g_data.m_bTransact = true;
	EnterCriticalSection(&g_data.m_critTransact);
	//EnterCriticalSection(&g_data.m_critClientSocket);
//							AfxMessageBox((char*)pdata->m_pucData);
						int nReturn = g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, info);
						if(nReturn>=NET_SUCCESS)
						{
//							AfxMessageBox((char*)pdata->m_pucData);

							g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
	//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
					//g_data.m_bTransact = false;
							if(StringToRemoteSettings(pdata->m_pucData)<CLIENT_SUCCESS)
							{
//							AfxMessageBox("fake");
								CString szError;
								szError.Format("An error occurred obtaining settings from server.\nSettings are default or last-known values, not necessarily representative of the actual server settings.");
								MessageBox(szError, "Communication Error", MB_OK);
								m_bObtainedFromServer = false;
							}
							else
							{
								m_bObtainedFromServer = true;
							}
							//send ack
				//			AfxMessageBox("ack");
						}
						else
						{
	//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
					//g_data.m_bTransact = false;
							CString szError;
							szError.Format("Could not obtain settings from server (error %d).\nSettings are default or last-known values, not necessarily representative of the actual server settings.\n\nError message:\n%s", nReturn, info);
							MessageBox(szError, "Communication Error", MB_OK);
							m_bObtainedFromServer = false;
						}

						delete pdata;
				}
					}
				}
			}

//			int GetIniInt(char* pszSection, char* pszEntry, int nDefaultValue);
		}
		else // write
		{
			if(m_pszAppName) fu.SetIniString("Global", "AppName", m_pszAppName);
			if(m_pszAppName) fu.SetIniString("Global", "InternalAppName", m_pszInternalAppName);
//			if(m_pszAboutText) fu.SetIniString("Global", "AboutText", m_pszAboutText);  // dont save
			if(m_pszSettingsText) fu.SetIniString("Global", "SettingsText", m_pszSettingsText);
//			int SetIniInt(char* pszSection, char* pszEntry, int nValue, char* pszComment=NULL);

			fu.SetIniString("Client", "Host", m_szHost.GetBuffer(0));
			fu.SetIniInt("Client", "Port", m_nPort);
			fu.SetIniInt("Client", "Autoconnect", (m_bAutoconnect?1:0));

			fu.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.
			fu.SetIniInt("Global", "IsServer", (m_bIsServer?1:0));
			fu.SetIniInt("Global", "UseFeed", (m_bUseFeed?1:0));

			if(m_bIsServer)
			{
				fu.SetIniInt("Global", "MaxMessages", m_nMaxMessages);
				fu.SetIniString("Ticker", "AsRunFileSpec", m_pszAsRunFilename);  
				fu.SetIniString("Ticker", "AsRunToken", m_pszAsRunDestination);  // the as run destination name
				fu.SetIniString("Ticker", "MessagePreamble", m_pszTickerMessagePreamble);
				fu.SetIniInt("Ticker", "DoneCount", m_nDoneCount); 
				fu.SetIniString("Ticker", "TickerBackplateName", "BACKPLATE", m_pszTickerBackplateName);
				fu.SetIniString("Ticker", "TickerLogoName", "LOGO", m_pszTickerLogoName);
				fu.SetIniInt("Ticker", "TickerBackplateID", 0); 
				fu.SetIniInt("Ticker", "TickerLogoID", 2); 
				fu.SetIniInt("Ticker", "TickerTextID", 1); 
				
				fu.SetIniInt("Ticker", "TriggerBufferItems", m_nTriggerBuffer);

//				fu.SetIniInt("DefaultValues", "MessageDwellTime", m_nDefaultDwellTime);

				fu.SetIniString("Database", "DSN", m_pszDSN);
				fu.SetIniString("Database", "DBUser", m_pszUser);
				fu.SetIniString("Database", "DBPassword", m_pszPW);
				fu.SetIniString("Database", "TickerTableName", m_pszTicker);  // the Ticker table name
				fu.SetIniString("Database", "FeedTableName", m_pszFeed);  // the Messages table name

				fu.SetIniString("Database", "AsRunTableName", m_pszAsRun);  // the As-run table name
				fu.SetIniString("Database", "SettingsTableName", m_pszSettings);  // the settings table name
				fu.SetIniString("Database", "MessagesTableName", m_pszMessages);   // the Messages table name


				fu.SetIniString("Ticker", "GraphicsHost", m_pszDestinationHost);  // do we want to direct drive the host ever?  or always shoot to Libretto or other module.? (still need the host either way but need to know what drive mode)
				fu.SetIniInt("Ticker", "GraphicsHostType", m_nGraphicsHostType);


				fu.SetIniString("Ticker", "GraphicsModule",  m_pszModule);  // the module name (Libretto)
				fu.SetIniString("Ticker", "GraphicsQueue", m_pszQueue); // the module's queue table name (Queue of Libretto.dbo.Queue)

				fu.SetIniString("Ticker", "InEventID", m_pszInEvent);  // the event identifier of the transition in (Server_Ticker_In) command
				fu.SetIniString("Ticker", "OutEventID", m_pszOutEvent);  // the event identifier of the transition out (Server_Ticker_Out) command
				fu.SetIniString("Ticker", "TextEventID", m_pszTextEvent);  // the event identifier of the text message event

				fu.SetIniString("Ticker", "MessageToken", m_pszMessageToken);  // the paramater identifier of the text field
				fu.SetIniString("Ticker", "Aux1Token", m_pszAux1Token);   // the paramater identifier of the aux1 field
				fu.SetIniString("Ticker", "Aux2Token", m_pszAux2Token);   // the paramater identifier of the aux2 field

				fu.SetIniInt("Ticker", "AutoPurgeExpired", (m_bAutoPurgeExpired?1:0));
				fu.SetIniInt("Ticker", "AutoPurgeExceeded", (m_bAutoPurgeExceeded?1:0));
				fu.SetIniInt("Ticker", "AutoPurgeExpiredAfterMins", m_nAutoPurgeExpiredAfterMins);
				fu.SetIniInt("Ticker", "AutoPurgeExceededAfterMins", m_nAutoPurgeExceededAfterMins);
				fu.SetIniInt("Ticker", "dwell_time", m_nDefaultDwellTime);

				fu.SetIniInt("Ticker", "AutoStart", (m_bAutostartTicker?1:0));
				fu.SetIniInt("DataDownload", "AutoStart", (m_bAutostartData?1:0));
				fu.SetIniInt("Ticker", "UseLocalTime", (m_bUseLocalTime?1:0));

				fu.SetIniInt("Icon", "LayoutSalvoFormat", m_nLayoutSalvoFormat);


				m_bObtainedFromServer = false;
				DLLthread_t* pData = NULL;

				int n=0;

				while(n<g_dlldata.nNumThreads)
				{
					if((g_dlldata.thread)&&(g_dlldata.thread[n]))
					{
						pData = g_dlldata.thread[n];
						switch(n)
						{
						case COLLECTOR_DATA:// 0
							{
								EnterCriticalSection(&pData->m_crit);
								if(m_bFromDialog)
								{
									char* pchI = (char*)malloc(m_szBaseURL.GetLength()+1);
									if(pchI)
									{
										sprintf(pchI, "%s", m_szBaseURL);
										if(pData->pszBaseURL) free(pData->pszBaseURL);
										pData->pszBaseURL = pchI;
									}
									pchI = (char*)malloc(m_szProgramID.GetLength()+1);
									if(pchI)
									{
										sprintf(pchI, "%s", m_szProgramID);
										if(pData->pszProgramID) free(pData->pszProgramID);
										pData->pszProgramID = pchI;
									}

									pData->nBatchParam = m_nBatch;
									pData->nLongIntervalMS =		m_nPollIntervalMS;
									pData->nShortIntervalMS =		m_nRequestIntervalMS;

								}

								fu.SetIniString("DataDownload", "Threadname", pData->pszThreadName);

							// URLs may use format specifiers

								fu.SetIniString("DataDownload", "BaseURL", pData->pszBaseURL);
//									pData->pszInfoURL=NULL;
//									pData->pszConfirmURL=NULL;

//									pData->pszSeriesParamName=NULL;    //  %S
								fu.SetIniString("DataDownload", "ProgramParamName", pData->pszProgramParamName);
//									pData->pszEpisodeParamName=NULL;   //  %E

//									pData->pszSeriesID=NULL;    //  %s
								fu.SetIniString("DataDownload", "ProgramID", pData->pszProgramID);   //  %p
//									pData->pszEpisodeID=NULL;   //  %e

								// some generic parameters
								fu.SetIniString("DataDownload", "ToDateParamName",  pData->pszToDateParamName);   //  %p
								fu.SetIniString("DataDownload", "FromDateParamName",  pData->pszFromDateParamName);   //  %p
								fu.SetIniString("DataDownload", "NowParamName",  pData->pszNowParamName);   //  %p
								fu.SetIniString("DataDownload", "BatchParamName",  pData->pszBatchParamName);   //  %p
								fu.SetIniString("DataDownload", "FromIDParamName", pData->pszFromIDParamName);   //  %p
								fu.SetIniString("DataDownload", "GetIDParamName", pData->pszGetIDParamName);   //  %p

//									pData->pszToDateParamName=NULL;				//  %t
//									pData->pszFromDateParamName=NULL;			//  %f
//									pData->pszNowParamName=NULL;					//  %n
//									pData->pszBatchParamName=NULL;				//  %b
//									pData->pszFromIDParamName=NULL;				//  %m  // start id
//									pData->pszGetIDParamName=NULL;				//  %i

								fu.SetIniInt("DataDownload", "BatchParam", pData->nBatchParam);   //  %p
								fu.SetIniInt("DataDownload", "ShortIntervalMS", pData->nShortIntervalMS);   //  %p
								fu.SetIniInt("DataDownload", "LongIntervalMS", pData->nLongIntervalMS);   //  %p
								fu.SetIniInt("DataDownload", "DirParamStyle", pData->bDirParamStyle?1:0);   //  %p

								LeaveCriticalSection(&pData->m_crit);

							} break;
						case COLLECTOR_TICKER:// 1
							{
								EnterCriticalSection(&pData->m_crit);

								fu.SetIniString("Ticker", "Threadname", pData->pszThreadName);

								if(m_bFromDialog)
								{
									char* pchI = (char*)malloc(m_szTickerHost.GetLength()+1);
									if(pchI)
									{
										sprintf(pchI, "%s", m_szTickerHost);
										if(pData->pszHost) free(pData->pszHost);
										pData->pszHost = pchI;
									}
									pData->nAuxValue = 	m_bTickerPlayOnce?1:0;
									pData->nPort = m_nTickerPort;

								}

								fu.SetIniInt("Ticker", "BatchParam", pData->nBatchParam );   //  %p
								fu.SetIniInt("Ticker", "ShortIntervalMS", pData->nShortIntervalMS);   //  %p
								fu.SetIniInt("Ticker", "LongIntervalMS", pData->nLongIntervalMS);   //  %p

								fu.SetIniString("Ticker", "GraphicsEngineHost", pData->pszHost);  
								fu.SetIniInt("Ticker", "GraphicsEnginePort", pData->nPort);  
								fu.SetIniInt("Ticker", "PlayItemsOnce", 	pData->nAuxValue);  

								LeaveCriticalSection(&pData->m_crit);

							} break;
						case COLLECTOR_UI:// 2
							{
								EnterCriticalSection(&pData->m_crit);

								fu.SetIniString("Synchronizer", "Threadname", pData->pszThreadName);
								fu.SetIniInt("Synchronizer", "BatchParam", pData->nBatchParam);   //  %p
								fu.SetIniInt("Synchronizer", "ShortIntervalMS", pData->nShortIntervalMS);   //  %p
								fu.SetIniInt("Synchronizer", "LongIntervalMS", pData->nLongIntervalMS );   //  %p

								LeaveCriticalSection(&pData->m_crit);

							} break;
						}
					}


					n++;
				}

			}
			else
			{

				fu.SetIniInt("UserInterface", "SmutReplaceCurses", 	m_bSmut_ReplaceCurses);


				// send settings to server.

				// NO.  ONLY send settings to server if dialog OK is pressed.  so, handle elsewhere.
			}

			if(!(fu.SetSettings(theApp.m_szSettingsFilename, false)&FILEUTIL_MALLOC_OK)) return CLIENT_ERROR;
		}

		return CLIENT_SUCCESS;
	}
//	else AfxMessageBox("failed!");
	return CLIENT_ERROR;
}

void CCollectorSettings::OnButtonTestfeed() 
{
	if(!UpdateData(TRUE)) return;
	CCollectorTestFeed dlg;

	EnterCriticalSection(&g_settings.m_crit);
	int nLength = m_szBaseURL.GetLength();
	if(nLength>0)
	{
		if(m_szBaseURL.GetAt(nLength-1)!='/')
		{
			if(g_dlldata.thread[COLLECTOR_DATA]->bDirParamStyle)
			{
				dlg.m_szURL.Format("%s/%s/%s/%s/1/%s/%d", m_szBaseURL, 
					g_dlldata.thread[COLLECTOR_DATA]->pszProgramParamName, m_szProgramID,
					g_dlldata.thread[COLLECTOR_DATA]->pszFromIDParamName, 
					g_dlldata.thread[COLLECTOR_DATA]->pszBatchParamName, g_dlldata.thread[COLLECTOR_DATA]->nBatchParam
					);
			}
			else
			{
				dlg.m_szURL.Format("%s?%s=%s&%s=1&%s=%d", m_szBaseURL, 
					g_dlldata.thread[COLLECTOR_DATA]->pszProgramParamName, m_szProgramID,
					g_dlldata.thread[COLLECTOR_DATA]->pszFromIDParamName, 
					g_dlldata.thread[COLLECTOR_DATA]->pszBatchParamName, g_dlldata.thread[COLLECTOR_DATA]->nBatchParam
					);
			}
		}
		else
		{
			if(g_dlldata.thread[COLLECTOR_DATA]->bDirParamStyle)
			{
				dlg.m_szURL.Format("%s%s/%s/%s/1/%s/%d", m_szBaseURL, 
					g_dlldata.thread[COLLECTOR_DATA]->pszProgramParamName, m_szProgramID,
					g_dlldata.thread[COLLECTOR_DATA]->pszFromIDParamName, 
					g_dlldata.thread[COLLECTOR_DATA]->pszBatchParamName, g_dlldata.thread[COLLECTOR_DATA]->nBatchParam
					);
			}
			else
			{
				dlg.m_szURL.Format("%s?%s=%s&%s=1&%s=%d", m_szBaseURL.Left(nLength-1), 
					g_dlldata.thread[COLLECTOR_DATA]->pszProgramParamName, m_szProgramID,
					g_dlldata.thread[COLLECTOR_DATA]->pszFromIDParamName, 
					g_dlldata.thread[COLLECTOR_DATA]->pszBatchParamName, g_dlldata.thread[COLLECTOR_DATA]->nBatchParam
					);
			}

		}
	}
	LeaveCriticalSection(&g_settings.m_crit);

//	dlg.m_szURL.Format("%s", g_dlldata.thread[COLLECTOR_DATA]->pszBaseURL);
	dlg.DoModal();
}

void CCollectorSettings::OnOK() 
{
	if(!UpdateData(TRUE)) return;

	// TODO: Add extra validation here
	m_bFromDialog = true;
			EnterCriticalSection(&g_settings.m_crit);
	Settings(false);
			LeaveCriticalSection(&g_settings.m_crit);
	m_bFromDialog = false;
	// plus, send settings to the server if we can.
	if(!m_bIsServer)
	{
		if(!m_bObtainedFromServer)
		{
			if(AfxMessageBox("The settings have not been initialized from the server.\nContinuing will overwrite settings on the server.\nAre you sure you want to continue?", MB_YESNO) != IDYES) return;
		}
		else
		{
			if(AfxMessageBox("Continuing will overwrite settings on the server.\nAre you sure you want to continue?", MB_YESNO) != IDYES) return;
		}

		// have to be connected to server.
		if(g_data.m_socket==NULL)
		{
			char info[8192];
	//EnterCriticalSection(&g_data.m_critClientSocket);
			if(g_data.m_net.OpenConnection(m_szHost.GetBuffer(0), m_nPort, &g_data.m_socket, 
				10000, 10000, info)<NET_SUCCESS)
			{
	//LeaveCriticalSection(&g_data.m_critClientSocket);
				MessageBox(info, "Network Error", MB_OK);
			}
			else
			{
	//LeaveCriticalSection(&g_data.m_critClientSocket);
			}
		}

		if(g_data.m_socket==NULL)
		{
			AfxMessageBox("Could not send settings to server.");
			m_bObtainedFromServer = false;
		}
		else
		{

			if(g_dlldata.nNumThreads>COLLECTOR_TICKER)
			{

				DLLthread_t* pData = NULL;

				int n=0;

				while(n<g_dlldata.nNumThreads)
				{
					if((g_dlldata.thread)&&(g_dlldata.thread[n]))
					{
						char* pch=NULL;
						pData = g_dlldata.thread[n];
						switch(n)
						{
						case COLLECTOR_DATA:// 0
							{
								EnterCriticalSection(&pData->m_crit);

								pch = pData->pszBaseURL;

								pData->pszBaseURL = (char*)malloc(m_szBaseURL.GetLength()+1);
								if(pData->pszBaseURL)
								{
									sprintf(pData->pszBaseURL, "%s",m_szBaseURL);
									free(pch);
								}

								pch = pData->pszProgramID;

								pData->pszProgramID = (char*)malloc(m_szProgramID.GetLength()+1);
								if(pData->pszProgramID)
								{
									sprintf(pData->pszProgramID, "%s",m_szProgramID);
									free(pch);
								}

								pData->nBatchParam = m_nBatch;
								pData->nShortIntervalMS = m_nRequestIntervalMS;
								pData->nLongIntervalMS = m_nPollIntervalMS;

								LeaveCriticalSection(&pData->m_crit);

							} break;
						case COLLECTOR_TICKER:// 1
							{
								EnterCriticalSection(&pData->m_crit);

								pch = pData->pszHost;
								pData->pszHost = (char*)malloc(m_szTickerHost.GetLength()+1);
								if(pData->pszHost)
								{
									sprintf(pData->pszHost, "%s",m_szTickerHost);
									free(pch);
								}
								pData->nPort = m_nTickerPort;
								pData->nAuxValue = m_bTickerPlayOnce;
								LeaveCriticalSection(&pData->m_crit);

							} break;
						case COLLECTOR_UI:// 2
							{
							} break;
						}
					}


					n++;
				}


			}


			// get the settings!
			CNetData* pdata = new CNetData;
			if(pdata)
			{

				pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

				pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
				pdata->m_ucSubCmd = 0;       // the subcommand byte

				char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
				if(pch)
				{
					sprintf(pch, "%s|Server_Set_Setting|%s", m_pszInternalAppName, RemoteSettingsToString());

					pdata->m_pucData =  (unsigned char*) pch;
					pdata->m_ulDataLen = strlen(pch);
				}

		//AfxMessageBox("sending");
		//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
		//AfxMessageBox( (char*)pdata->m_pucData );
				//if(!//g_data.m_bTransact)
				{
					//g_data.m_bTransact = true;
	EnterCriticalSection(&g_data.m_critTransact);
	//EnterCriticalSection(&g_data.m_critClientSocket);
				if(g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT)>=NET_SUCCESS)
				{
					g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
					//send ack
		//			AfxMessageBox("ack");
	//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
					//g_data.m_bTransact = false;
				}
				else
				{
	//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
					//g_data.m_bTransact = false;
					AfxMessageBox("Could not send settings to server.");
					m_bObtainedFromServer = false;
				}

				delete pdata;
				}
			}
		}
	}


	CDialog::OnOK();
}

void CCollectorSettings::OnButtonConnect() 
{
	if(((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->FindStringExact( -1, m_szHost )==CB_ERR)
	{
		((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->AddString(m_szHost);
		((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SelectString(-1, m_szHost);
	}

	FILE* fp = fopen("collectornet.cfg", "wb");

	if(fp)
	{
		UpdateData(TRUE);
		CString szText;
		int nCount=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCount();
		int nSel=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCurSel();  //CB_ERR
		if(nSel==CB_ERR)
		{
			if(m_szHost.GetLength()>0)
				fprintf(fp, "*%s\n", m_szHost);
		}
		if(nCount>0)
		{
			int i=0;
			while(i<nCount)
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetLBText( i, szText );
				if(nSel==i)
				{
					fprintf(fp, "*");
				}
				fprintf(fp, "%s\n", szText);
				i++;
			}
		}
		fclose(fp);
	}

	if(g_data.m_socket)
	{ //disconnect
	//EnterCriticalSection(&g_data.m_critClientSocket);
		g_data.m_net.CloseConnection(g_data.m_socket);
		g_data.m_socket=NULL;
	//LeaveCriticalSection(&g_data.m_critClientSocket);
	}
	else
	{ //connect
		if(!UpdateData(TRUE)) return;
		char info[8192];
	//EnterCriticalSection(&g_data.m_critClientSocket);
		if(g_data.m_net.OpenConnection(m_szHost.GetBuffer(0), m_nPort, &g_data.m_socket, 
			10000, 10000, info)<NET_SUCCESS)
		{
	//LeaveCriticalSection(&g_data.m_critClientSocket);
			MessageBox(info, "Network Error", MB_OK);
		}
		else
		{
	//LeaveCriticalSection(&g_data.m_critClientSocket);

		}
	}

	if(g_data.m_socket)
	{
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Disconnect");
	}
	else
	{
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
	}

}

BOOL CCollectorSettings::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	FILE* fp = fopen("collectornet.cfg", "rb");

	if(g_data.m_socket)
	{
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Disconnect");
	}
	else
	{
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
	}


	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		unsigned long ulFileLen = ftell(fp);
		char* pchFile = (char*) malloc(ulFileLen+1); // term zero
		if(pchFile)
		{
			fseek(fp, 0, SEEK_SET);

			fread(pchFile, sizeof(char), ulFileLen, fp);
			*(pchFile+ulFileLen) = 0; // term zero

			int i=0; int nSel=-1;
			char* pch = strtok(pchFile, "\r\n");
			while(pch != NULL)
			{
				if(*pch=='*')
				{
					nSel = i;
					pch++;
				}
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->InsertString(i, pch);

				pch = strtok(NULL, "\r\n");
				i++;
			}

			if(g_data.m_socket==NULL)
			{
				if ((i>0)&&(nSel>=0)&&(nSel<i))
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SetCurSel(nSel);
					UpdateData(TRUE);
				}
			}
			else
			{
				if(((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->FindStringExact( -1, m_szHost )==CB_ERR)
				{
					((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->AddString(m_szHost);
					((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SelectString(-1, m_szHost);
				}
			}

			free(pchFile);

		}
		UpdateData(FALSE);
		fclose(fp);
	}

			EnterCriticalSection(&g_settings.m_crit);
	Settings(true);
			LeaveCriticalSection(&g_settings.m_crit);

	if(m_bTickerPlayOnce)
	{
		GetDlgItem(IDC_STATIC_DONE)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_DONE)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_DONE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT_DONE)->ShowWindow(SW_SHOW);
	}
	else
	{
		GetDlgItem(IDC_STATIC_DONE)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_DONE)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_DONE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT_DONE)->ShowWindow(SW_HIDE);
	}

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCollectorSettings::OnCheckPlayonce() 
{
	m_bTickerPlayOnce = ((CButton*)GetDlgItem(IDC_CHECK_PLAYONCE))->GetCheck();
	if(m_bTickerPlayOnce)
	{
		GetDlgItem(IDC_STATIC_DONE)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_DONE)->EnableWindow(TRUE);
		GetDlgItem(IDC_STATIC_DONE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_EDIT_DONE)->ShowWindow(SW_SHOW);
	}
	else
	{
		GetDlgItem(IDC_STATIC_DONE)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_DONE)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_DONE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EDIT_DONE)->ShowWindow(SW_HIDE);
	}
}



int CCollectorSettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	if((m_bIsServer)&&(m_pszSettings)&&(strlen(m_pszSettings)>0)&&(Settings(true)==TABULATOR_SUCCESS))
	{

		CDBUtil  db;
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];
		strcpy(errorstring, "");

		CDBconn* pdbConn = db.CreateNewConnection(m_pszDSN, m_pszUser, m_pszPW);
		if(pdbConn)
		{
			if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
			{
				if(g_pmsgr) g_pmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Collector_Settings:database_connect", errorstring);  //(Dispatch message)
				if(pszInfo)
				{
					_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s", errorstring);
				}
			}
			else
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
				CRecordset* prs = db.Retrieve(pdbConn, szSQL, pszInfo);
				if(prs)
				{
					int nReturn = TABULATOR_ERROR;
					int nIndex = 0;
					while ((!prs->IsEOF()))
					{
						CString szCategory="";
						CString szParameter="";
						CString szValue="";
						CString szTemp="";
						int min, max;
						bool bmin = false, bmax = false;
						try
						{
							prs->GetFieldValue("category", szCategory);  //HARDCODE
							prs->GetFieldValue("parameter", szParameter);  //HARDCODE
							prs->GetFieldValue("value", szValue);  //HARDCODE
							prs->GetFieldValue("min_value", szTemp);  //HARDCODE
							if(szTemp.GetLength())
							{
								min = atoi(szTemp);
								bmin = true;
							}
							prs->GetFieldValue("max_value", szTemp);  //HARDCODE
							if(szTemp.GetLength())
							{
								max = atoi(szTemp);
								bmax = true;
							}
						}
						catch( ... )
						{
						}

						int nLength = szValue.GetLength();
/*
						if(szCategory.CompareNoCase("Main")==0)
						{
							if(szParameter.CompareNoCase("Name")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszName) free(m_pszName);
										m_pszName = pch;
									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("License")==0)
						{
							if(szParameter.CompareNoCase("Key")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszLicense) free(m_pszLicense);
										m_pszLicense = pch;

										// recompile license key params
										if(g_ptabulator->m_data.m_key.m_pszLicenseString) free(g_ptabulator->m_data.m_key.m_pszLicenseString);
										g_ptabulator->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
										if(g_ptabulator->m_data.m_key.m_pszLicenseString)
										sprintf(g_ptabulator->m_data.m_key.m_pszLicenseString, "%s", szValue);

										g_ptabulator->m_data.m_key.InterpretKey();

										char errorstring[MAX_MESSAGE_LENGTH];
										if(g_ptabulator->m_data.m_key.m_bValid)
										{
											unsigned long i=0;
											while(i<g_ptabulator->m_data.m_key.m_ulNumParams)
											{
												if((g_ptabulator->m_data.m_key.m_ppszParams)
													&&(g_ptabulator->m_data.m_key.m_ppszValues)
													&&(g_ptabulator->m_data.m_key.m_ppszParams[i])
													&&(g_ptabulator->m_data.m_key.m_ppszValues[i]))
												{
													if(stricmp(g_ptabulator->m_data.m_key.m_ppszParams[i], "max")==0)
													{
		//												g_ptabulator->m_data.m_nMaxLicensedDevices = atoi(g_ptabulator->m_data.m_key.m_ppszValues[i]);
													}
												}
												i++;
											}
										
											if(
													(
														(!g_ptabulator->m_data.m_key.m_bExpires)
													||((g_ptabulator->m_data.m_key.m_bExpires)&&(!g_ptabulator->m_data.m_key.m_bExpired))
													||((g_ptabulator->m_data.m_key.m_bExpires)&&(g_ptabulator->m_data.m_key.m_bExpireForgiveness)&&(g_ptabulator->m_data.m_key.m_ulExpiryDate+g_ptabulator->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
													)
												&&(
														(!g_ptabulator->m_data.m_key.m_bMachineSpecific)
													||((g_ptabulator->m_data.m_key.m_bMachineSpecific)&&(g_ptabulator->m_data.m_key.m_bValidMAC))
													)
												)
											{
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
												g_ptabulator->m_data.SetStatusText(errorstring, TABULATOR_STATUS_OK);
											}
											else
											{
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
												g_ptabulator->m_data.SetStatusText(errorstring, TABULATOR_STATUS_ERROR);
											}
										}
										else
										{
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
											g_ptabulator->m_data.SetStatusText(errorstring, TABULATOR_STATUS_ERROR);
										}

									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("CommandServer")==0)
						{
							if(szParameter.CompareNoCase("ListenPort")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
									{
										m_usCommandPort = nLength;
									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("StatusServer")==0)
						{
							if(szParameter.CompareNoCase("ListenPort")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
									{
										m_usStatusPort = nLength;
									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("Messager")==0)
						{
							if(szParameter.CompareNoCase("UseEmail")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bUseEmail = true;
									else m_bUseEmail = false;
								}
							}
							else
							if(szParameter.CompareNoCase("UseNet")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bUseNetwork = true;
									else m_bUseNetwork = false;
								}
							}
							else
							if(szParameter.CompareNoCase("UseLog")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bUseLog = true;
									else m_bUseLog = false;
								}
							}
							else
							if(szParameter.CompareNoCase("ReportSuccessfulOperation")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bReportSuccessfulOperation = true;
									else m_bReportSuccessfulOperation = false;
								}
							}
						}
						else
		/*
						if(szCategory.CompareNoCase("FileHandling")==0)
						{
							// we are not going to allow DSN params to change via the DB.
							if(szParameter.CompareNoCase("DeleteSourceFileOnTransfer")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bDeleteSourceFileOnTransfer = true;
									else m_bDeleteSourceFileOnTransfer = false;
								}
							}
						}
						else
		*/
						if(szCategory.CompareNoCase("Ticker")==0)
						{
							if(szParameter.CompareNoCase("GraphicsHost")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszDestinationHost)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszDestinationHost) free(m_pszDestinationHost);
										m_pszDestinationHost = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("GraphicsModule")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszModule)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszModule) free(m_pszModule);
										m_pszModule = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("GraphicsQueue")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszQueue)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszQueue) free(m_pszQueue);
										m_pszQueue = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("InEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszInEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bInEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszInEvent) free(m_pszInEvent);
										m_pszInEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("OutEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszOutEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bOutEventChanged = true;  // the event identifier of the transition out (Server_Ticker_Out) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszOutEvent) free(m_pszOutEvent);
										m_pszOutEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("TextEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszTextEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bTextEventChanged = true;  // the event identifier of the text message event has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszTextEvent) free(m_pszTextEvent);
										m_pszTextEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("PlayItemsOnce")==0)
							{
								if(nLength>0)
								{
									m_bTickerPlayOnce = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("DoneCount")==0)
							{
								if(nLength>0)
								{
									m_nDoneCount = atoi(szValue);
									if(m_nDoneCount<0) m_nDoneCount=0;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExpired")==0)
							{
								if(nLength>0)
								{
									m_bAutoPurgeExpired = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExceeded")==0)
							{
								if(nLength>0)
								{
									m_bAutoPurgeExceeded = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExpiredAfterMins")==0)
							{
								if(nLength>0)
								{
									m_nAutoPurgeExpiredAfterMins = atoi(szValue);
									if(m_nAutoPurgeExpiredAfterMins<0) m_nAutoPurgeExpiredAfterMins=0;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExceededAfterMins")==0)
							{
								if(nLength>0)
								{
									m_nAutoPurgeExceededAfterMins = atoi(szValue);
									if(m_nAutoPurgeExceededAfterMins<0) m_nAutoPurgeExceededAfterMins=0;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoStart")==0)
							{
								if(nLength>0)
								{
									m_bAutostartTicker = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("UseLocalTime")==0)
							{
								if(nLength>0)
								{
									m_bUseLocalTime = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("dwell_time")==0)
							{
								if(nLength>0)
								{
									m_nDefaultDwellTime = atoi(szValue);
									if(m_nDefaultDwellTime<0) m_nDefaultDwellTime=10000; //default
								}
							}
						}
						else
						if(szCategory.CompareNoCase("DataDownload")==0)
						{
							if(szParameter.CompareNoCase("AutoStart")==0)
							{
								if(nLength>0)
								{
									m_bAutostartData = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
						}
						else
						if(szCategory.CompareNoCase("default_value")==0)
						{
							if(szParameter.CompareNoCase("dwell_time")==0)
							{
								if(nLength>0)
								{
									m_nDefaultDwellTime = atoi(szValue);
									if(m_nDefaultDwellTime<0) m_nDefaultDwellTime=10000; //default
								}
							}
						}
						else
						if(szCategory.CompareNoCase("Database")==0)
						{
							// we are not going to allow DSN params to change via the DB.
							if(szParameter.CompareNoCase("SettingsTableName")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSettings)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSettings) free(m_pszSettings);
										m_pszSettings = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunTableName")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszAsRun)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszAsRun) free(m_pszAsRun);
										m_pszAsRun = pch;
									}
								}
							}
		
							else
							if(szParameter.CompareNoCase("MessagesTableName")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszMessages)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszMessages) free(m_pszMessages);
										m_pszMessages = pch;
									}
								}
							}
/*
							else
							if(szParameter.CompareNoCase("QueueTableName")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszQueue) free(m_pszQueue);
										m_pszQueue = pch;
									}
								}
							}
		/*
							else
							if(szParameter.CompareNoCase("ConnectionsTableName")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszConnections) free(m_pszConnections);
										m_pszConnections = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("LiveEventsTableName")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszLiveEvents) free(m_pszLiveEvents);
										m_pszLiveEvents = pch;
									}
								}
							}
		*/
						}


						nIndex++;
						prs->MoveNext();
					}
					prs->Close();

					if(pszInfo)
					{
						_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
					}
					delete prs;
					prs = NULL;

					Settings(false); //write
					return TABULATOR_SUCCESS;
				}
			}
		}
		else
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: Connection pointer was NULL.");
		}

	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. ",
				m_bIsServer?"":"Not a server. "
				);
		}
	}
	return TABULATOR_ERROR;
}


