// SerialTones.h : main header file for the SerialTones DLL
//

#if !defined(AFX_SerialTones_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
#define AFX_SerialTones_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#import "msxml3.dll" named_guids 
using namespace MSXML2;

#include "../../../Applications/Generic/Client/ClientDefines.h"
#include "../../../Common/MSG/Messager.h"
#include "../../../Common/TXT/FileUtil.h"
#include "..\..\..\Cortex\3.0.4.5\CortexDefines.h"

//#include "..\..\..\..\Common\MFC\Inet\Inet.h"
//#include "..\..\..\..\Common\API\Miranda\IS2Core.h"  // for IS2 util functions


#define REMOVE_CLIENTSERVER  // for web-based UI only


#define SETTINGS_FILENAME "serialtones.ini"
#define VERSION_STRING "1.0.0.1"



#define SERIALTONES_EVENT_TOKEN									0x00000001   // any token has changed - must iterate to find which.
#define SERIALTONES_EVENT_MOD										0x00000100

#define SERIALTONES_ACTION_TYPE_NONE			0x00000000  // nothing
#define SERIALTONES_ACTION_TYPE_SQL				0x00000001  // run a SQL query
#define SERIALTONES_ACTION_TYPE_CXNET			0x00000002  // send a set of commands and chars in the cortex binary protocol on a network connection
#define SERIALTONES_ACTION_TYPE_INET			0x00000003  // send a set of plain old chars on a network connection


/* event_type values */  
#define SERIALTONES_EXCLUSION_TYPE_UNKNOWN			0x00 
#define SERIALTONES_EXCLUSION_TYPE_ON						0x01 
#define SERIALTONES_EXCLUSION_TYPE_OFF					0x02 
#define SERIALTONES_EXCLUSION_TYPE_DURATION			0x03 


// pattern matching
#define SERIALTONES_PATTERN_ERROR							-1
#define SERIALTONES_PATTERN_MATCH							0 
#define SERIALTONES_PATTERN_NO_MATCH						1 

// bulk sizes
#define SERIALTONES_DATA_ARRAY_INCREMENT		64 


// from cortex.h but static so just define here.
//send message type
#define CX_SENDMSG_ERROR   0
#define CX_SENDMSG_INFO	   1
#define CX_SENDMSG_NOTE	   2


#define SERIALTONES_DEBUG_SQL								0x00000001
#define SERIALTONES_DEBUG_EVENTS						0x00000002
#define SERIALTONES_DEBUG_TRIGGER						0x00000004
#define SERIALTONES_DEBUG_AUTOLIST					0x00000008
#define SERIALTONES_DEBUG_AUTOEVENTS				0x00000010
#define SERIALTONES_DEBUG_TIMER							0x00000020
#define SERIALTONES_DEBUG_TIMECODE					0x00000040
#define SERIALTONES_DEBUG_CALLFUNC					0x00000080
#define SERIALTONES_DEBUG_PATTERN						0x00000200
#define SERIALTONES_DEBUG_STATE							0x00000400
#define SERIALTONES_DEBUG_OUTPUTVARS				0x00000800
#define SERIALTONES_DEBUG_AUTOLISTVARS			0x00001000
#define SERIALTONES_DEBUG_AUTOEVENTVARS			0x00002000
#define SERIALTONES_DEBUG_FINALOUTPUTVARS		0x00004000


#define SERIALTONES_INVALID_TIMEADDRESS				0xffffffff
#define SERIALTONES_USECURRENT_TIMEADDRESS		0xfffffffe


#define SERIALTONES_FLAG_FOUND 0x00000010


#define SERIALTONES_EXISTS				0x01
#define SERIALTONES_NONEXISTANT		0x02
#define SERIALTONES_UNKNOWN				0x00



/////////////////////////////////////////////////////////////////////////////
// COutputDlg dialog

class COutputDlg : public CDialog
{
// Construction
public:
	COutputDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(COutputDlg)
	enum { IDD = IDD_DIALOG_OUTPUT };
	CListCtrl	m_lc;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COutputDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation


public:
		//sizing
	BOOL m_bVis;
	BOOL 	m_bNewSizeInit;
	CRect m_rcDlg;
	CRect m_rcCtrl[1];

	void OnExit();
	void UpdateOutputDialog();

protected:

	// Generated message map functions
	//{{AFX_MSG(COutputDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


//#include "SerialTonesCore.h"
//#include "SerialTonesData.h"
//#include "SerialTonesSettings.h"

// HERE is theACII SET:
// starting here -> !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
// note the space in the first position.


/////////////////////////////////////////////////////////////////////////////
// CSerialTonesApp
// See SerialTones.cpp for the implementation of this class
//

class CSerialTonesApp : public CWinApp
{
public:
	CSerialTonesApp();
	int  DLLCtrl(void** ppvoid, UINT nType);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerialTonesApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	char m_szSettingsFilename[MAX_PATH];

	//{{AFX_MSG(CSerialTonesApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////



//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SerialTones_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
