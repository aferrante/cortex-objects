// SerialTonesData.cpp: implementation of the CSerialTonesData and CSerialTonesMessage classes.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SerialTones.h"
#include "SerialTonesCore.h"
#include "SerialTonesData.h"
#include "SerialTonesSettings.h"
#include <atlbase.h>
#include <process.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CSerialTonesCore	g_core;
extern CSerialTonesData	g_data;
extern CSerialTonesSettings	g_settings;
extern CMessager*			g_ptabmsgr;
extern DLLdata_t       g_dlldata;

#define SENDMSG_EVENT_MAXLEN 64
#define SENDMSG_SENDER_MAXLEN 64
#define SENDMSG_MESSAGE_MAXLEN 512


extern void ExecuteOpportunitiesThread(void* pvArgs);




//////////////////////////////////////////////////////////////////////
// CTabulatorEvent Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTabulatorEvent::CTabulatorEvent()
{
	m_ulFlags = TABULATOR_FLAG_DISABLED;
	m_dblTriggerTime = -1.0; 
	m_bAnalyzed = false;
	m_bReAnalyzed = false;
	m_nEventID = -1;	
	m_nAnalyzedTriggerID = -1;
	m_nType = -1;
	m_nDestType = -1;
}

CTabulatorEvent::~CTabulatorEvent()
{
}

CTabulatorEventArray::CTabulatorEventArray()
{
	m_nEvents = 0; 
	m_ppEvents = NULL;
	InitializeCriticalSection(&m_critEvents);
	m_nEventMod=0;
	m_nLastEventMod=-1;
}

CTabulatorEventArray::~CTabulatorEventArray()
{
	EnterCriticalSection(&m_critEvents);
	DeleteEvents();
	LeaveCriticalSection(&m_critEvents);
	DeleteCriticalSection(&m_critEvents);
}

void	CTabulatorEventArray::DeleteEvents()
{
	if((m_nEvents)&&(m_ppEvents))
	{
		int i=0;
		while(i<m_nEvents)
		{
			if(m_ppEvents[i]) delete m_ppEvents[i];
			m_ppEvents[i] = NULL;
			i++;
		}
		delete [] m_ppEvents;
	}
	m_ppEvents = NULL;
	m_nEvents = 0;
}


CAutomationList::CAutomationList()
{
	m_nState=-1;
	m_nChange=-1;
	m_nDisplay=-1;
	m_nSysChange=-1;
	m_nCount=-1;
	m_nLookahead=-1;
	m_dblLastUpdate = -1.0;
}

CAutomationList::~CAutomationList()
{
}


CAutomationEvent::CAutomationEvent()
{
 	m_nID = -1;
	m_usType = 0xffff;
	m_pszID = NULL;
	m_pszTitle = NULL;
	m_pszReconcileKey = NULL;
	m_ucSegment = 0xff;
	m_ulOnAirTimeMS = 0xffffffff;					// ideal milliseconds as appears in the list
	m_ulOnAirLocalUnixDate = 0xffffffff;  //in seconds, offset from January 1, 1900
	m_ulDurationMS = 0xffffffff;          // ideal milliseconds as appears in the list
	m_ulSOMMS = 0xffffffff;               // ideal milliseconds as appears in the list
	m_usStatus = 0xffff;
//	unsigned short m_usControl; // was this
	m_ulControl = 0xffffffff;  // made it this, storing revent->extrathree (which is extended event control in recfm80) in the upper word

	// potential output variables.
//	m_ulClassification = SERIALTONES_CLASS_TYPE_NONE;
	m_pszOutputString = NULL; // like:
	// Replace Ad BLOCK (for advertisments that are the first in the pod)
	// PASS (for not-ads that do not have replacements)
	// ReplaceContent <ID1> <ID2> BLOCK (for not-ads that do not have replacements)
	// PENDING (for items not analyzed yet)
	// Timer suppressed (for items suppressed by the timer - either type)
}


CAutomationEvent::~CAutomationEvent()
{
	if(m_pszID) free(m_pszID);
	if(m_pszTitle) free(m_pszTitle);
	if(m_pszReconcileKey) free(m_pszReconcileKey);
	if(m_pszOutputString) free(m_pszOutputString);
}



COutputVariables::COutputVariables()
{
	m_b_list_is_playing = false;  // automation list is playing a primary event.

	//NOTE!  three different meaings of the word "current" - be aware!

	// here, current means the currently playing primary event.
	m_n_current_primary_start=SERIALTONES_INVALID_TIMEADDRESS; // in ideal MS; 
	m_n_current_primary_duration=SERIALTONES_INVALID_TIMEADDRESS;  //in ideal MS 
	m_psz_current_primary_ID=NULL; // = Harris ID of current
	m_psz_current_primary_description=NULL;// = Harris Title of current

	m_n_current_systemtime=0; // in ideal MS; 
	m_n_current_servertime=0; // in ideal MS; 
	m_dbl_last_servertime=-1.0; // in ideal MS since epoch, local time; 
	m_dbl_last_servertime_update=-1.0; // in ideal MS since epoch; 

	m_n_next_start_tagged_event_start = -1; // in ideal MS; -1 means not found
	m_n_next_end_tagged_event_start = -1; // in ideal MS;  -1 means not found

	m_n_next_tagged_interval_duration = 0; // in ideal MS; 
}

COutputVariables::~COutputVariables()
{
	if(m_psz_current_primary_ID) free(m_psz_current_primary_ID);
	if(m_psz_current_primary_description) free(m_psz_current_primary_description);

	m_n_current_systemtime=0; // in ideal MS; 
	m_n_current_servertime=0; // in ideal MS; 
}


/*


CReplacementRule::CReplacementRule()
{
	m_nID=-1;
	m_pszProgramID=NULL;
	m_pszReplacementID=NULL;

 	m_nSequence=-1;
	m_nDuration=-1; // in ideal MS, invalid to start
	m_nDurationInFrames = -1; // invalid
	m_nOutputDuration = 0; // nuttin

	m_dblValidDate = 0.0;
	m_dblExpireDate = 0.0;

	m_nOffset = 0; // time address 0
}

CReplacementRule::~CReplacementRule()
{
	if(m_pszReplacementID) free(m_pszReplacementID);
	if(m_pszProgramID) free(m_pszProgramID);
}


CDescriptionPattern::CDescriptionPattern()
{
	m_nID=-1;
	m_pszProgramID=NULL;
	m_pszPattern=NULL;

	m_dblValidDate = 0.0;
	m_dblExpireDate = 0.0;
}

CDescriptionPattern::~CDescriptionPattern()
{
	if(m_pszPattern) free(m_pszPattern);
	if(m_pszProgramID) free(m_pszProgramID);
}

CClassificationRule::CClassificationRule()
{
	m_nID=-1;
	m_pszPattern=NULL;
	m_pszColName=NULL;
	m_nEventType=SERIALTONES_CLASS_TYPE_NONE;
}

CClassificationRule::~CClassificationRule()
{
	if(m_pszPattern) free(m_pszPattern);
	if(m_pszColName) free(m_pszColName);
}
*/

/*
COpportuneIntervalData::COpportuneIntervalData()
{
	m_pszDescription = NULL;
	m_usType = SERIALTONES_UNKNOWN; // free or disallowed
	m_ulOffsetTimeMS=SERIALTONES_INVALID_TIMEADDRESS;				// ideal milliseconds as appears in the list
	m_ulDurationMS=SERIALTONES_INVALID_TIMEADDRESS;          // ideal milliseconds as appears in the list
}

COpportuneIntervalData::~COpportuneIntervalData()
{
	if(m_pszDescription) free(m_pszDescription);
}


COpportuneEvent::COpportuneEvent()
{
	m_nID=-1;
	m_pTabulatorEvents=NULL;
	m_pszEventToken=NULL;
	m_nDuration = -1; // invalid

	m_bEventTokenChanged = false;  // the event identifier token has changed

	m_dblValidDate  = 0.0;
	m_dblExpireDate = 0.0;

	m_dblLastPlayed = 0.0;
}

COpportuneEvent::~COpportuneEvent()
{
	if(m_pTabulatorEvents) delete m_pTabulatorEvents;
	if(m_pszEventToken) free(m_pszEventToken);
}


CExclusionRule::CExclusionRule()
{
	m_nID=-1;
	m_pszPattern=NULL;
	m_pszColName=NULL;
	m_nAutoEventType=-1;  // Automation event type, for matching start invalid
	m_nEventType=SERIALTONES_EXCLUSION_TYPE_UNKNOWN;  // internal event type, ON, OFF, or DURATION

	m_dblValidDate = 0.0;
	m_dblExpireDate = 0.0;
}

CExclusionRule::~CExclusionRule()
{
	if(m_pszPattern) free(m_pszPattern);
	if(m_pszColName) free(m_pszColName);
}
*/


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/*
CSerialTonesMessage::CSerialTonesMessage()
{
	m_szMessage="";
	m_szRevisedMessage="";
	m_nTimestamp =-1;
	m_nLastUpdate =-1;
	m_nID = -1;
	m_nStatus = SerialTones_MSG_STATUS_NONE;
	m_nType = SerialTones_MSG_TYPE_NONE;
	m_nPlayed = 0;
	m_dblIndex = -0.1;
}

CSerialTonesMessage::~CSerialTonesMessage()
{
}


CString CSerialTonesMessage::SQLUpdate(bool bTicker, double dblSeekIndex)
{
  CString szSQL="";
	if(bTicker)
	{
	char* szMsg = g_data.m_dbu.EncodeQuotes(m_szMessage);
	if(m_nStatus&SerialTones_MSG_STATUS_INSERTED)
	{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "upd item (%d [%s])", m_nID, m_szMessage); //(Dispatch message)
		szSQL.Format("UPDATE %s SET \
[text] = '%s', \
[sort_order] = %.4f, \
[timestamp] = %d, \
[updated] = %d, \
[status] = %d, \
[type] = %d, \
[played] = %d WHERE [id] = %d AND [sort_order] >= %.4f AND [sort_order] <= %.4f", 
					g_settings.m_pszTicker, 
					(szMsg?szMsg:m_szMessage),
					m_dblIndex,
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|SerialTones_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed,
					m_nID,
					dblSeekIndex-0.0000009,
					dblSeekIndex+0.0000009
					);  // table name
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "upd item (%d [%s]) SQL %s", m_nID, m_szMessage, szSQL);//(Dispatch message)
	}
	else
	{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "ins item (%d [%s])", m_nID, m_szMessage); //(Dispatch message)
		szSQL.Format("INSERT INTO %s (\
[id], \
[text], \
[sort_order], \
[timestamp], \
[updated], \
[status], \
[type], \
[played]) VALUES \
(%d, '%s', %.4f, %d, %d, %d, %d, %d)",
					g_settings.m_pszTicker,  // table name
					m_nID,
					(szMsg?szMsg:m_szMessage),
					m_dblIndex,
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|SerialTones_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "ins item (%d [%s]) SQL %s", m_nID, m_szMessage, szSQL); //(Dispatch message)
	}
	if(szMsg) free(szMsg);
	}
	else
	{
	char* szMsg = g_data.m_dbu.EncodeQuotes(m_szMessage);
	char* szRevMsg = g_data.m_dbu.EncodeQuotes(m_szRevisedMessage);
	if(m_nStatus&SerialTones_MSG_STATUS_INSERTED)
	{
		szSQL.Format("UPDATE %s SET \
[text] = '%s', \
[revision] = '%s', \
[timestamp] = %d, \
[updated] = %d, \
[status] = %d, \
[type] = %d, \
[played] = %d WHERE [id] = %d", g_settings.m_pszFeed, 
					(szMsg?szMsg:m_szMessage),
					(szRevMsg?szRevMsg:m_szRevisedMessage),
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|SerialTones_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed,
					m_nID);  // table name
	}
	else
	{

		szSQL.Format("INSERT INTO %s (\
[id], \
[text], \
[revision], \
[timestamp], \
[updated], \
[status], \
[type], \
[played]) VALUES \
(%d, '%s', '%s', %d, %d, %d, %d, %d)",
					g_settings.m_pszFeed,  // table name
					m_nID,
					(szMsg?szMsg:m_szMessage),
					(szRevMsg?szRevMsg:m_szRevisedMessage),
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|SerialTones_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed);
	}

	if(szMsg) free(szMsg);
	if(szRevMsg) free(szRevMsg);
	}

	return szSQL;
}

*/

/*
int CSerialTonesItem::PublishItem(unsigned long ulFlags)
{
	if(( g_data.m_pdb )&&(	g_data.m_pdbConn))
	{

		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		if(ulFlags&SERIALTONES_UPDATE_COUNTONLY)
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET count = %d WHERE uid = %d",  //HARDCODE
					((g_settings.m_pszSerialTones)&&(strlen(g_settings.m_pszSerialTones)))?g_settings.m_pszSerialTones:"SerialTones",
					m_nCount,
					m_nItemID
					);
		}
		else
		{			
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d, duration = %d, start_tc = %d, stop_tc = %d, \
count = %d, paused_duration = %d, paused_count = %d, last_started = %.03f, last_stopped = %.03f WHERE uid = %d",  //HARDCODE
					((g_settings.m_pszSerialTones)&&(strlen(g_settings.m_pszSerialTones)))?g_settings.m_pszSerialTones:"SerialTones",
					m_ulStatus,

					m_ulDuration,
					((m_ulStartTC==0xffffffff)?-1:m_ulStartTC),
					((m_ulStopTC==0xffffffff)?-1:m_ulStopTC),

					m_nCount,
					m_ulPausedDuration,
					m_ulTimesPaused,
					
					(((double)(m_timeLastStarted.time)) + ((double)(m_timeLastStarted.millitm))/1000.0),
					(((double)(m_timeLastStopped.time)) + ((double)(m_timeLastStopped.millitm))/1000.0),

					m_nItemID
					);
		}
//			Sleep(1); // dont peg processor
if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_SQL)) 
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:update", "SQL: %s", szSQL); // Sleep(250); //(Dispatch message)

EnterCriticalSection(&g_data.m_critSQL);
		if(g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//**MSG
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "SerialTones:update", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

		}
LeaveCriticalSection(&g_data.m_critSQL);

	}
	return CLIENT_ERROR;
}
*/


CMessageOverrideValues::CMessageOverrideValues()
{

}

CMessageOverrideValues::~CMessageOverrideValues()
{
}










//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSerialTonesData::CSerialTonesData()
{
	m_bInitalEventGetSucceeded = false;
	m_socketTabulator = NULL;
	m_bInCommand = FALSE;

	m_bAutoAnalysisThreadStarted=false;
	m_bAutoAnalysisThreadKill=true;

	m_nAutomationProcessID = -1;
//	m_pCrawlEvents = new CTabulatorEventArray;


	m_nLastTime = -1;
	m_nLastMillitm = -1;
	m_nTimeIncr = 0;
	m_nMillitmIncr = 0;

//	m_bDoingOpp=false;
//	m_bDoingOppCancel=true;

//	m_bCrawlEventChanged=true;




	m_pEvents = NULL;
//	m_pInEvents = new CTabulatorEventArray;
//	m_pOutEvents = new CTabulatorEventArray;
//	m_pTextEvents = new CTabulatorEventArray;

//	m_socket=NULL;

	m_pdb=NULL;
	m_pdbConn=NULL;
	m_pdbOutput=NULL;
	m_pdbOutputConn=NULL;
	InitializeCriticalSection(&m_critSQL);
	InitializeCriticalSection(&m_critOutputSQL);
	

	InitializeCriticalSection(&m_critTimeCode);
	m_bTimeCodeThreadStarted = false;
	m_bTimeCodeThreadKill = true;

	m_ucTimeCodeBits = 0xff; //invalid value
	m_ulTimeCode = 0xffffffff; //invalid value
	m_ulTimeCodeElapsed = 0;

	InitializeCriticalSection(&m_critEventsSettings);

	m_nLastEventMod = -3;

//	m_bReplaceAdEventChanged=false;  // the event identifier of the replace ad has changed
//	m_bReplaceContentEventChanged=false;  // the event identifier of the replace content has changed
//	m_bEventNotificationEventChanged=false;  // the event identifier of the "BLOCK only" event has changed
//	m_bRejoinMainEventChanged=false;  // the event identifier of the PASS tabulator event (includes Rejoin Main) has changed
//	m_bTimerEventChanged=false;  // the event identifier of the BLOCK GPO w Timer event has changed

//	m_pReplaceAdEvents = new CTabulatorEventArray;
//	m_pReplaceContentEvents = new CTabulatorEventArray;
//	m_pEventNotificationEvents = new CTabulatorEventArray;
//	m_pRejoinMainEvents = new CTabulatorEventArray;
//	m_pTimerEvents = new CTabulatorEventArray;


//	m_ppSerialTones=NULL;
//	m_nSerialTones=0;


//	m_ppReplacementRules=NULL;
//	m_nReplacementRules=-1;  //uninit

//	m_ppDescriptionPatterns=NULL;
//	m_nDescriptionPatterns=-1;  //uninit

//	m_ppClassificationRules=NULL;
//	m_nClassificationRules=-1;  //uninit

//	m_nReplacementRulesArraySize = 0;
//	m_nDescriptionPatternsArraySize = 0;
//	m_nClassificationRulesArraySize = 0;

//	m_ppOpportuneEvents=NULL;
//	m_nOpportuneEvents=-1;
//	m_nOpportuneEventsArraySize=0;

//	m_ppExclusionRules=NULL;
//	m_nExclusionRules=-1;
//	m_nExclusionRulesArraySize=0;

	InitializeCriticalSection(&m_critTableData);

	InitializeCriticalSection(&m_critOutputVariables);
//	InitializeCriticalSection(&m_critSyncVariables);
	
	InitializeCriticalSection(&m_critAutomationEvents);
//	InitializeCriticalSection(&m_critPlayEvent);  // need to enforce one at a time.


}

CSerialTonesData::~CSerialTonesData()
{
	m_bAutoAnalysisThreadKill=true;
	m_bTimeCodeThreadKill = true;

	while((m_bAutoAnalysisThreadStarted)||(m_bTimeCodeThreadStarted)) Sleep(10);


//	DeleteCriticalSection(&m_critClientSocket);
	DeleteCriticalSection(&m_critSQL);
	DeleteCriticalSection(&m_critOutputSQL);
//	if(	m_pInEvents) delete  m_pInEvents;
//	if(	m_pOutEvents) delete  m_pOutEvents;
//	if(	m_pTextEvents) delete  m_pTextEvents;
	DeleteCriticalSection(&m_critTimeCode);
	DeleteCriticalSection(&m_critEventsSettings);
//	if(	m_pReplaceAdEvents) delete  m_pReplaceAdEvents;
//	if(	m_pReplaceContentEvents) delete  m_pReplaceContentEvents;
//	if(	m_pEventNotificationEvents) delete  m_pEventNotificationEvents;
//	if(	m_pRejoinMainEvents) delete  m_pRejoinMainEvents;
//	if(	m_pTimerEvents) delete  m_pTimerEvents;


	EnterCriticalSection(&m_critTableData);
/*
	if((m_ppReplacementRules)&&(m_nReplacementRules>0))
	{
		int i=0;
		while(i<m_nReplacementRules)
		{
			if(m_ppReplacementRules[i])
			{
				delete m_ppReplacementRules[i];
			}
			i++;
		}
		delete [] m_ppReplacementRules;
	}
	m_ppReplacementRules=NULL;
	m_nReplacementRules=0; 

	if((m_ppDescriptionPatterns)&&(m_nDescriptionPatterns>0))
	{
		int i=0;
		while(i<m_nDescriptionPatterns)
		{
			if(m_ppDescriptionPatterns[i])
			{
				delete m_ppDescriptionPatterns[i];
			}
			i++;
		}
		delete [] m_ppDescriptionPatterns;
	}
	m_ppDescriptionPatterns=NULL;
	m_nDescriptionPatterns=0; 

	if((m_ppClassificationRules)&&(m_nClassificationRules>0))
	{
		int i=0;
		while(i<m_nClassificationRules)
		{
			if(m_ppClassificationRules[i])
			{
				delete m_ppClassificationRules[i];
			}
			i++;
		}
		delete [] m_ppClassificationRules;
	}
	m_ppClassificationRules=NULL;
	m_nClassificationRules=0; 
*/

/*
	if((m_ppOpportuneEvents)&&(m_nOpportuneEvents>0))
	{
		int i=0;
		while(i<m_nOpportuneEvents)
		{
			if(m_ppOpportuneEvents[i])
			{
				delete m_ppOpportuneEvents[i];
			}
			i++;
		}
		delete [] m_ppOpportuneEvents;
	}
	m_ppOpportuneEvents=NULL;
	m_nOpportuneEvents=0; 
	if((m_ppExclusionRules)&&(m_nExclusionRules>0))
	{
		int i=0;
		while(i<m_nExclusionRules)
		{
			if(m_ppExclusionRules[i])
			{
				delete m_ppExclusionRules[i];
			}
			i++;
		}
		delete [] m_ppExclusionRules;
	}
	m_ppExclusionRules=NULL;
	m_nExclusionRules=0; 

*/

	LeaveCriticalSection(&m_critTableData);

	DeleteCriticalSection(&m_critTableData);
	DeleteCriticalSection(&m_critOutputVariables);
//	DeleteCriticalSection(&m_critSyncVariables);
	DeleteCriticalSection(&m_critAutomationEvents);
//	DeleteCriticalSection(&m_critPlayEvent);  // need to enforce one at a time.
}

/*
int CSerialTonesData::Do_Opportunities()
{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "Do_Opportunities"); // Sleep(250); //(Dispatch message)

	if(m_bDoingOpp)
	{
		m_bDoingOppCancel = true; // kill anything up right now

		while(m_bDoingOpp)
		{
			Sleep(15);
		}

	}

	m_bDoingOppCancel = false;
//	m_bDoingOpp = true;


	if(_beginthread(ExecuteOpportunitiesThread, 0, (void*)(NULL))==-1)
	{
	//error.
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "Error starting serialtones execution thread");//   Sleep(250);//(Dispatch message)
	//**MSG
	}
	else
		{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "ExecuteOpportunitiesThread begun"); // Sleep(250); //(Dispatch message)

		}

	return -69;
}
*/

// remove pipes
CString CSerialTonesData::Encode(CString szText)
{
  CString szOut = "";
  int i;
  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '|')
      szOut += "%:";
    else if (szText[i] == '%')
      szOut += "%%";
    else if ((szText[i] != '\n')&&(szText[i] != '\r'))  //strip returns while we are at it.
       szOut += (char) szText[i];
  }
  return szOut;
}

// put back pipes
CString CSerialTonesData::Decode(CString szText)
{
  CString szOut = "";
  int i;

  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '%')
    {
      i++; if (i>=szText.GetLength()) break;
      if (szText[i] == '%')
      {
        szOut += "%";
      }
      else if (szText[i] == ':')
      {
        szOut += "|";
      }
    }
    else szOut += (char) szText[i];
  }
  return szOut;
}


CString CSerialTonesData::XMLTextNodeValue(IXMLDOMNodePtr pNode)
{
	CString szText = _T("");
	if(pNode)
	{
		IXMLDOMNodeListPtr pValues = pNode->GetchildNodes();
		if (pValues != NULL)
		{
			IXMLDOMNodePtr pValue;
			while(pValue = pValues->nextNode())
			{
				DOMNodeType valnodeType;
				pValue->get_nodeType(&valnodeType);

				if(valnodeType == NODE_TEXT)
				{
USES_CONVERSION;
					szText = W2T(_bstr_t(pValue->GetnodeValue()));
					szText.TrimLeft();
					szText.TrimRight();
					return szText;
				}
			}
		}
	}
	return szText;
}



int CSerialTonesData::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszMessages)&&(strlen(g_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		if(g_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime) \
VALUES ('%s', '%s', %d, %d.%03d)", // HARDCODE
				g_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				g_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				g_settings.m_pszMessages
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
//			IncrementDatabaseMods(g_settings.m_pszMessages, dberrorstring);
		LeaveCriticalSection(&m_critSQL);
			return TABULATOR_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return TABULATOR_ERROR;
}
/*
int CSerialTonesData::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszMessages)&&(strlen(g_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			g_settings.m_pszMessages,
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			g_settings.m_pszMessages
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
//			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return TABULATOR_SUCCESS;
		}
	}
	return TABULATOR_ERROR;
}
*/

int CSerialTonesData::SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...)
{
 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszAsRun)&&(strlen(g_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		if((pszEventID)&&(strlen(pszEventID)>SENDMSG_EVENT_MAXLEN)) pszEventID[SENDMSG_EVENT_MAXLEN-1]=0;
		
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);
		char* pchEvent = m_pdb->EncodeQuotes(pszEventID);
		if(g_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, channelid, eventid) \
VALUES ('%s', '%s', %d, %d.%03d, %d, '%s')", // HARDCODE
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm,
				nChannelID,
				pchEvent?pchEvent:""
				);
		}
		else
		{

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log")
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
		if(pchEvent) free(pchEvent);

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
//			IncrementDatabaseMods(g_settings.m_pszAsRun, dberrorstring);
			return TABULATOR_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);

	}
	return TABULATOR_ERROR;
}

/*
int CSerialTonesData::SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszAsRun)&&(strlen(g_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log")
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SerialTones_Asrun", "SQL %s", szSQL) ;// Sleep(100); //(Dispatch message)

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
//			IncrementDatabaseMods(g_settings.m_pszAsRun, dberrorstring);
			return TABULATOR_SUCCESS;
		}
		else
		{
if(g_ptabmsgr)
{
	char szSource[MAX_PATH]; sprintf(szSource, "%s_Asrun", g_settings.m_pszInternalAppName);
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "SQL error %s", dberrorstring) ;// Sleep(100); //(Dispatch message)
}
		}
		LeaveCriticalSection(&m_critSQL);

	}
	else
		{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SerialTones_Asrun", "null conditions");// Sleep(100); //(Dispatch message)
		}
	return TABULATOR_ERROR;
}
*/

int CSerialTonesData::SendTabulatorCommand(unsigned char** ppucDataBuffer, char* pszAsRunEventID, bool bSuppressAsRun, char* pszDTMF, COutputVariables* pOv)
{
	if((m_socketTabulator)&&(ppucDataBuffer))
	{
		CNetData data;
		while(m_bInCommand) Sleep(1);
		m_bInCommand =TRUE;

		unsigned char* pucOrig = *ppucDataBuffer;
		unsigned char* puc = NULL;
		int nLen = strlen((char*)(*ppucDataBuffer));
		if((*ppucDataBuffer)&&(nLen)) puc = (unsigned char*)malloc(nLen+1);
		if(puc) memcpy(puc, *ppucDataBuffer, nLen+1); // includes term 0;
		data.m_ucCmd = TABULATOR_CMD_PLUGINCALL;
		data.m_pucData=puc;
		data.m_ulDataLen = (puc?nLen:0);
		data.m_ucType = ((NET_TYPE_PROTOCOL1|NET_TYPE_KEEPOPEN|NET_SND_KEEPOPENLCL)|(*ppucDataBuffer?NET_TYPE_HASDATA:0)); // has data but no subcommand.

		int n = m_net.SendData(&data, m_socketTabulator, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT);//|NET_SND_NO_RX);
		if(data.m_pucData!=NULL)
		{
			*ppucDataBuffer = data.m_pucData;  //return the buffer;
			data.m_pucData=NULL;  // null it so that it does not get freed on destructor
		}
		else
		{
			*ppucDataBuffer = NULL;
		}
		if(n < NET_SUCCESS)
		{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "SerialTones_Tabulator_command", "Net error %d sending cmd 0x%02x buffer: %s", n, TABULATOR_CMD_PLUGINCALL, (*ppucDataBuffer)?((char*)(*ppucDataBuffer)):"");   //(Dispatch message)
			m_net.CloseConnection(m_socketTabulator);
			m_socketTabulator = NULL;
			m_bInCommand =FALSE;

			//ASRUN
if(!bSuppressAsRun)
{
	g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, "Error %d sending to %s at %s:%d with: %s", 
										n,
										g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
										g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
										g_settings.m_nTabulatorPort, 
										(char*)(*ppucDataBuffer));

if(g_ptabmsgr)
{
	g_ptabmsgr->DM(MSG_ICONERROR, 
		(((g_settings.m_pszAsRunDestination)&&(strlen(g_settings.m_pszAsRunDestination)))?g_settings.m_pszAsRunDestination:"serialtones_asrun"), 
		pszAsRunEventID, "Net error %d sending cmd 0x%02x buffer: %s", 
		n, TABULATOR_CMD_PLUGINCALL, (*ppucDataBuffer)?((char*)(*ppucDataBuffer)):"");   //(Dispatch message)
}
// asrun file message
// 12-22-2008 05:23:03.421: Sent [D100^M] (5 bytes) at server time 05:23:02.19, event hit time 05:22:58.06 (duration was calculated as 100 seconds) 

}
		}
		else
		{
			m_net.SendData(NULL, m_socketTabulator, 5000, 0, NET_SND_CLNTACK, NULL);
			m_bInCommand =FALSE;

if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_CALLFUNC))
{ 
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SerialTones", "Successfully executed %s with return data: %02x,%02x %s (%d bytes data) - returning %d", 
									(pucOrig?((char*)pucOrig):"null"),
									data.m_ucCmd,    // the command byte
									data.m_ucSubCmd,   // the subcommand byte
									(((*ppucDataBuffer)&&(data.m_ulDataLen))?(char*)*ppucDataBuffer:""),
									data.m_ulDataLen,
									(data.m_ucSubCmd&0x80)?((int)(data.m_ucSubCmd - 256)):((int)data.m_ucSubCmd)
									);
}

			//ASRUN
if(!bSuppressAsRun)
{
	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, "Successfully executed %s with return data: %02x,%02x %s (%d bytes data)", 
									(pucOrig?((char*)pucOrig):"null"),
									data.m_ucCmd,    // the command byte
									data.m_ucSubCmd,   // the subcommand byte
									(((*ppucDataBuffer)&&(data.m_ulDataLen))?(char*)*ppucDataBuffer:""),
									data.m_ulDataLen
									);

if(g_ptabmsgr)
{
// asrun file message
// 12-22-2008 05:23:03.421: Sent [D100^M] (5 bytes) at server time 05:23:02.19, event hit time 05:22:58.06 (duration was calculated as 100 seconds) 


	if(data.m_ucSubCmd&0x80)
	{
		// negative value!

		if(pOv) // this came from promotor and we calculated automation variables
		{

			if(pOv->m_n_current_servertime>=0)
			{

				int nHours;
				int nMinutes;
				int nSeconds;
				int nFrames;

				g_data.m_timeconv.MStoHMSF(pOv->m_n_current_servertime, &nHours, &nMinutes, &nSeconds, &nFrames, 
					(/*g_settings.m_bDF?29.97:*/(double)g_settings.m_nFrameRate) // ideal
					);

				g_ptabmsgr->DM(MSG_ICONERROR, 
					(((g_settings.m_pszAsRunDestination)&&(strlen(g_settings.m_pszAsRunDestination)))?g_settings.m_pszAsRunDestination:"serialtones_asrun"), 
					pszAsRunEventID, "Error %d %s%s%ssending DTMF sequence %s (%d bytes) at server time %02d:%02d:%02d%s%02d", 
					(int)(data.m_ucSubCmd - 256),
					(*ppucDataBuffer)?"(":"",
					(*ppucDataBuffer)?((char*)(*ppucDataBuffer)):"",
					(*ppucDataBuffer)?") ":"",
					pszDTMF?pszDTMF:"",
					pszDTMF?strlen(pszDTMF):0,
					nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames
					
					);   //(Dispatch message)
			}
			else
			{
				// no server time
				g_ptabmsgr->DM(MSG_ICONERROR, 
					(((g_settings.m_pszAsRunDestination)&&(strlen(g_settings.m_pszAsRunDestination)))?g_settings.m_pszAsRunDestination:"serialtones_asrun"), 
					pszAsRunEventID, "Error %d %s%s%ssending DTMF sequence %s", 
					(int)(data.m_ucSubCmd - 256),
					(*ppucDataBuffer)?"(":"",
					(*ppucDataBuffer)?((char*)(*ppucDataBuffer)):"",
					(*ppucDataBuffer)?") ":"",
					pszDTMF?pszDTMF:""
					);   //(Dispatch message)
			}
		}
		else // test
		{
			g_ptabmsgr->DM(MSG_ICONERROR, 
				(((g_settings.m_pszAsRunDestination)&&(strlen(g_settings.m_pszAsRunDestination)))?g_settings.m_pszAsRunDestination:"serialtones_asrun"), 
				pszAsRunEventID, "Error %d %s%s%ssending test DTMF sequence [%s]", 
				(int)(data.m_ucSubCmd - 256),
				(*ppucDataBuffer)?"(":"",
				(*ppucDataBuffer)?((char*)(*ppucDataBuffer)):"",
				(*ppucDataBuffer)?") ":"",
				pszDTMF?pszDTMF:""
				);   //(Dispatch message)

		}
	}
	else
	{
		if(pOv) // this came from promotor and we calculated automation variables
		{
			int nHours;
			int nMinutes;
			int nSeconds;
			int nFrames;
			int nIntHours;
			int nIntMinutes;
			int nIntSeconds;
			int nIntFrames;

			if(pOv->m_n_current_servertime>=0)
			{

				g_data.m_timeconv.MStoHMSF(pOv->m_n_current_servertime, &nHours, &nMinutes, &nSeconds, &nFrames, 
					(/*g_settings.m_bDF?29.97:*/(double)g_settings.m_nFrameRate) // ideal
					);
				g_data.m_timeconv.MStoHMSF(pOv->m_n_next_start_tagged_event_start, &nIntHours, &nIntMinutes, &nIntSeconds, &nIntFrames, 
					(/*g_settings.m_bDF?29.97:*/(double)g_settings.m_nFrameRate) // ideal
					);

// 12-22-2008 05:23:03.421: Sent [D100^M] (5 bytes) at server time 05:23:02.19, event hit time 05:22:58.06 (duration was calculated as 100 seconds) 

				g_ptabmsgr->DM(MSG_ICONNONE, 
					(((g_settings.m_pszAsRunDestination)&&(strlen(g_settings.m_pszAsRunDestination)))?g_settings.m_pszAsRunDestination:"serialtones_asrun"), 
					pszAsRunEventID, "Sent DTMF sequence %s (%d bytes) at server time %02d:%02d:%02d%s%02d, interval start time %02d:%02d:%02d%s%02d (duration was calculated as %s ms)", 
					pszDTMF?pszDTMF:"",
					pszDTMF?strlen(pszDTMF):0,
					nHours, nMinutes, nSeconds, g_settings.m_bDF?".":":", nFrames,
					nIntHours, nIntMinutes, nIntSeconds, g_settings.m_bDF?".":":", nIntFrames,
					pOv->m_n_next_tagged_interval_duration					
					);   //(Dispatch message)
			}
			else
			{
				// no server time
				g_data.m_timeconv.MStoHMSF(pOv->m_n_current_systemtime, &nHours, &nMinutes, &nSeconds, &nFrames, 
					(/*g_settings.m_bDF?29.97:*/(double)g_settings.m_nFrameRate) // ideal
					);

				// to get this far we needed an interval...
				g_data.m_timeconv.MStoHMSF(pOv->m_n_next_start_tagged_event_start, &nIntHours, &nIntMinutes, &nIntSeconds, &nIntFrames, 
					(/*g_settings.m_bDF?29.97:*/(double)g_settings.m_nFrameRate) // ideal
					);

// 12-22-2008 05:23:03.421: Sent [D100^M] (5 bytes) at server time 05:23:02.19, event hit time 05:22:58.06 (duration was calculated as 100 seconds) 

				g_ptabmsgr->DM(MSG_ICONNONE, 
					(((g_settings.m_pszAsRunDestination)&&(strlen(g_settings.m_pszAsRunDestination)))?g_settings.m_pszAsRunDestination:"serialtones_asrun"), 
					pszAsRunEventID, "Sent DTMF sequence %s (%d bytes) at system time %02d:%02d:%02d%s%02d, interval start time %02d:%02d:%02d%s%02d (duration was calculated as %s ms)", 
					pszDTMF?pszDTMF:"",
					pszDTMF?strlen(pszDTMF):0,
					nHours, nMinutes, nSeconds, g_settings.m_bDF?".":":", nFrames,
					nIntHours, nIntMinutes, nIntSeconds, g_settings.m_bDF?".":":", nIntFrames,
					pOv->m_n_next_tagged_interval_duration					
					);   //(Dispatch message)
			}
		}
		else // test
		{
			g_ptabmsgr->DM(MSG_ICONNONE, 
					(((g_settings.m_pszAsRunDestination)&&(strlen(g_settings.m_pszAsRunDestination)))?g_settings.m_pszAsRunDestination:"serialtones_asrun"), 
					pszAsRunEventID, "Sent test DTMF sequence %s (%d bytes)", 
					pszDTMF?pszDTMF:"",
					pszDTMF?strlen(pszDTMF):0
				);   //(Dispatch message)

		}
	}
}


}
//			return TABULATOR_SUCCESS;

// since it is an unsigned char we are getting back, we have a choice to make.
// either negative values were converted, and we have -1 == 0xff... or something.  But I think this will be the most common so let's use
// twos complement to return an int value between -128 and 127.

			if(data.m_ucSubCmd&0x80)
			{
				// negative value!
				return (int)(data.m_ucSubCmd - 256);
			}
			else
			{
				// positive value or zero.
				return (int)data.m_ucSubCmd;
			}
		}
	}
	return TABULATOR_ERROR;
}




int CSerialTonesData::ClearOutputVariables()
{
	m_Output.m_b_list_is_playing = false;  // automation list is playing a primary event.

	// here, current means the currently playing primary event.
	m_Output.m_n_current_primary_start=SERIALTONES_INVALID_TIMEADDRESS; // in ideal MS; 
	m_Output.m_n_current_primary_duration=SERIALTONES_INVALID_TIMEADDRESS;  //in ideal MS 
	m_Output.m_psz_current_primary_ID=NULL; // = Harris ID of current
	m_Output.m_psz_current_primary_description=NULL;// = Harris Title of current

	m_Output.m_n_current_systemtime=0; // in ideal MS; 
	m_Output.m_n_current_servertime=0; // in ideal MS; 

	
	m_Output.m_dbl_last_servertime=-1.0; // in ideal MS since epoch local time; 
	m_Output.m_dbl_last_servertime_update=-1.0; // in ideal MS since epoch; 

	m_Output.m_n_next_start_tagged_event_start = -1; // in ideal MS; -1 means not found
	m_Output.m_n_next_end_tagged_event_start = -1; // in ideal MS;  -1 means not found

	m_Output.m_n_next_tagged_interval_duration = 0; // in ideal MS; 

	return TABULATOR_SUCCESS;

}
/*
int CSerialTonesData::GetClassification(CAutomationEvent* pEvent)
{
	if(pEvent)
	{

if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_CLASSRULES)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetClassification", "Checking event [%s][%s] against %d patterns", 
		pEvent->m_pszID?pEvent->m_pszID:"(null)", 
		pEvent->m_pszTitle?pEvent->m_pszTitle:"(null)", 
		m_nClassificationRules); // Sleep(250); //(Dispatch message)
}

		if((m_ppClassificationRules)&&(m_nClassificationRules>0))
		{
			int i=0;
			while(i<m_nClassificationRules)
			{
				if(m_ppClassificationRules[i])
				{

					if(
						  (m_ppClassificationRules[i]->m_pszPattern)
						&&(strlen(m_ppClassificationRules[i]->m_pszPattern)>0)
						&&(m_ppClassificationRules[i]->m_pszColName)
						&&(strlen(m_ppClassificationRules[i]->m_pszColName)>0)
						)
					{

if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_CLASSRULES)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetClassification", "Checking event [%s][%s] against pattern #%d: %s in %s", 
		pEvent->m_pszID?pEvent->m_pszID:"(null)", 
		pEvent->m_pszTitle?pEvent->m_pszTitle:"(null)", 
		i, m_ppClassificationRules[i]->m_pszPattern, m_ppClassificationRules[i]->m_pszColName); // Sleep(250); //(Dispatch message)
}


/*
	[event_id] [varchar](32) // reconcile key
	[event_clip] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[event_title] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
* /
						if(stricmp(m_ppClassificationRules[i]->m_pszColName, "event_clip")==0)
						{
							if((pEvent->m_pszID)&&(strlen(pEvent->m_pszID)>0))
							{
								if(TestPattern(pEvent->m_pszID, m_ppClassificationRules[i]->m_pszPattern) == SERIALTONES_PATTERN_MATCH)
								{
									return m_ppClassificationRules[i]->m_nEventType;
								}
							}
						}
						else
						if(stricmp(m_ppClassificationRules[i]->m_pszColName, "event_title")==0)
						{
							if((pEvent->m_pszTitle)&&(strlen(pEvent->m_pszTitle)>0))
							{
								if(TestPattern(pEvent->m_pszTitle, m_ppClassificationRules[i]->m_pszPattern) == SERIALTONES_PATTERN_MATCH)
								{
									return m_ppClassificationRules[i]->m_nEventType;
								}
							}
						}
					}
				}
				i++;
			}
		}
	}
	return SERIALTONES_CLASS_TYPE_NONE;
}
*/


int CSerialTonesData::GetAutomationEvents()
{
	if((m_pdbConn)&&(m_pdb)&&(g_settings.m_pszAutoServer)&&(strlen(g_settings.m_pszAutoServer)>0)&&(g_settings.m_nAutoServerListNumber>0))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		if(g_settings.m_nAutomationAnalysisLookahead>0)
		{
			if(g_settings.m_bAutomationLookahead)
			{
				sprintf(errorstring, " AND event_position < %d", (min(g_settings.m_nAutomationAnalysisLookahead, g_data.m_AutoList.m_nLookahead))  );
			}
			else
			{
				sprintf(errorstring, " AND event_position < %d", (g_settings.m_nAutomationAnalysisLookahead)  );
			}
		}
		else
		{
			if(g_settings.m_bAutomationLookahead)
			{
				sprintf(errorstring, " AND event_position < %d", (g_data.m_AutoList.m_nLookahead)  );
			}
			else
			{
				sprintf (errorstring, "");
			}
		}


		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT itemid, event_id, event_clip, event_segment, event_title, \
event_type, event_status, event_time_mode, event_start, event_duration \
FROM %s.dbo.%s WHERE server_name = '%s' AND server_list = %d%s \
ORDER BY event_position", // if we get ads first, those are easier to actuate, so let's prioritize them
			((g_settings.m_pszAutoModule)&&(strlen(g_settings.m_pszAutoModule)))?g_settings.m_pszAutoModule:"Sentinel",
			((g_settings.m_pszAutoEvents)&&(strlen(g_settings.m_pszAutoEvents)))?g_settings.m_pszAutoEvents:"Events",
			g_settings.m_pszAutoServer, g_settings.m_nAutoServerListNumber,
			errorstring
			); 
		strcpy (errorstring, "");

		//AND (event_type & 128 = 0)%s  - removed this so I can get the secondary events

/*

 	int m_nID;
	unsigned short m_usType;
	char* m_pszID;
	char* m_pszTitle;
	char* m_pszReconcileKey;
	unsigned char  m_ucSegment;
	unsigned long  m_ulOnAirTimeMS;					// ideal milliseconds as appears in the list
	unsigned long  m_ulOnAirLocalUnixDate;  //in seconds, offset from January 1, 1900
	unsigned long  m_ulDurationMS;          // ideal milliseconds as appears in the list
	unsigned long  m_ulSOMMS;               // ideal milliseconds as appears in the list
	unsigned short m_usStatus;
//	unsigned short m_usControl; // was this
	unsigned long m_ulControl;  // made it this, storing revent->extrathree (which is extended event control in recfm80) in the upper word

	// potential output variables.
	unsigned long m_ulClassification;
	char* m_pszOutputString; // like:


USE [sentinel]
GO
/ ****** Object:  Table [dbo].[Events]    Script Date: 12/19/2011 00:39:06 ****** /
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events](
	[itemid] [int] NOT NULL,
	[event_key] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[server_name] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[server_list] [int] NULL,
	[list_id] [int] NULL,
	[event_id] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[event_clip] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[event_segment] [int] NULL,
	[event_title] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[event_data] [varchar](4096) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[event_type] [int] NULL,
	[event_status] [int] NULL,
	[event_time_mode] [int] NULL,
	[event_start] [decimal](20, 3) NULL,
	[event_duration] [int] NULL,
	[event_calc_start] [decimal](20, 3) NULL,
	[event_calc_duration] [int] NULL,
	[event_calc_end] [decimal](20, 3) NULL,
	[event_position] [int] NULL,
	[event_last_update] [decimal](20, 3) NULL,
	[parent_uid] [int] NULL,
	[parent_key] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[parent_id] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[parent_clip] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[parent_segment] [int] NULL,
	[parent_title] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[parent_type] [int] NULL,
	[parent_status] [int] NULL,
	[parent_time_mode] [int] NULL,
	[parent_start] [decimal](20, 3) NULL,
	[parent_duration] [int] NULL,
	[parent_calc_start] [decimal](20, 3) NULL,
	[parent_calc_duration] [int] NULL,
	[parent_calc_end] [decimal](20, 3) NULL,
	[parent_position] [int] NULL,
	[app_data] [varchar](512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[app_data_aux] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF

*/
if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_AUTOEVENTS)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetAutomationEvents", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "GetAutomationEvents");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.

			// first let's preserve the existing list.
			CAutomationEvent** ppTemp  = NULL;
			int nNumEvents = m_nAutoEvents;
			if(m_nAutoEvents>0)
			{
				ppTemp = new CAutomationEvent*[m_nAutoEvents];
				if(ppTemp)
				{
					memcpy(ppTemp, m_ppAutoEvents, sizeof(CAutomationEvent*)*m_nAutoEvents);
					memset(m_ppAutoEvents, 0, sizeof(CAutomationEvent*)*m_nAutoEventsArraySize);  // do the whole array
					m_nAutoEvents = 0;
				}
				else
				{
					nNumEvents=0;
				}
			}

			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nAutoEventsArraySize) // have to expand
				{
					CAutomationEvent** ppNew = new CAutomationEvent*[m_nAutoEventsArraySize + SERIALTONES_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nAutoEventsArraySize += SERIALTONES_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(CAutomationEvent*)*m_nAutoEventsArraySize);
						if(m_ppAutoEvents)
						{
							memcpy(ppNew, m_ppAutoEvents, sizeof(CAutomationEvent*)*nIndex); // it's an array of pointers
						}
						m_ppAutoEvents = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppAutoEvents==NULL)
				{
					m_nAutoEventsArraySize = 0;
					m_nAutoEvents=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nAutoEvents)
				{
					m_ppAutoEvents[nIndex] = new CAutomationEvent;  // guaranteed that there will be array space, above.
				}

				if(m_ppAutoEvents[nIndex])
				{
					CString szTemp;
// SELECT itemid, event_id, event_clip, event_segment, event_title,
// event_type, event_status, event_time_mode,	event_start, event_duration
					
					prs->GetFieldValue("itemid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("event_id", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						if(m_ppAutoEvents[nIndex]->m_pszReconcileKey != NULL)
						{
							free(m_ppAutoEvents[nIndex]->m_pszReconcileKey);
						}
						m_ppAutoEvents[nIndex]->m_pszReconcileKey = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppAutoEvents[nIndex]->m_pszReconcileKey)
						{
							strcpy(m_ppAutoEvents[nIndex]->m_pszReconcileKey, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("event_clip", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						if(m_ppAutoEvents[nIndex]->m_pszID != NULL)
						{
							free(m_ppAutoEvents[nIndex]->m_pszID);
						}
						m_ppAutoEvents[nIndex]->m_pszID = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppAutoEvents[nIndex]->m_pszID)
						{
							strcpy(m_ppAutoEvents[nIndex]->m_pszID, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("event_segment", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_ucSegment = (unsigned char)atol(szTemp);
					}
					prs->GetFieldValue("event_title", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						if(m_ppAutoEvents[nIndex]->m_pszTitle != NULL)
						{
							free(m_ppAutoEvents[nIndex]->m_pszTitle);
						}
						m_ppAutoEvents[nIndex]->m_pszTitle = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppAutoEvents[nIndex]->m_pszTitle)
						{
							strcpy(m_ppAutoEvents[nIndex]->m_pszTitle, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("event_type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_usType = (unsigned short)atol(szTemp);
					}
					prs->GetFieldValue("event_status", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_usStatus = (unsigned short)atol(szTemp);
					}
					prs->GetFieldValue("event_time_mode", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_ulControl = (unsigned short)atol(szTemp);
					}
					double dblOnAir;
					prs->GetFieldValue("event_start", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						dblOnAir = atof(szTemp);

if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_AUTOEVENTS)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetAutomationEvents", "obtained %.3f", dblOnAir);//  Sleep(250); //(Dispatch message)
}

						m_ppAutoEvents[nIndex]->m_ulOnAirLocalUnixDate = ((unsigned long)(int)(dblOnAir));

						dblOnAir -= (double)(int)(dblOnAir);

if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_AUTOEVENTS)) 
{
	// this is unbelievable.  0.166 -> 166.000 when *1000.0, but then when casted to unsigned long, reverts to 165!  shady floating points!
// 2011-Dec-30 07:31:43.769 [H ] calc 0.166 166.000 165 - SerialTones:GetAutomationEvents - 
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetAutomationEvents", "calc %.3f %.3f %d %d", dblOnAir, (dblOnAir)*1000.0, (unsigned long)((dblOnAir)*1000.0), m_ppAutoEvents[nIndex]->m_ulOnAirLocalUnixDate);//  Sleep(250); //(Dispatch message)/
}


//						m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS = (unsigned long)((dblOnAir)*1000.0 +.999999999) + (m_ppAutoEvents[nIndex]->m_ulOnAirLocalUnixDate%86400)*1000; // ideal not real
						m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS = (unsigned long)(((dblOnAir)*1000.0) +.1) + (m_ppAutoEvents[nIndex]->m_ulOnAirLocalUnixDate%86400)*1000; // ideal not real

						// OK had to add the .0001 in first just to tip the balance.  then we have nice correct integer values.
						// then instead of using the 0.99999999 as explained, just going to do the same wasy as I did duration

						if(m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS%10) m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS++;  // these are ideal values.  so will either end in 0, 3, or 6.  if not 0, increment up by one ms.

						
						// the 0.99999999 comes from the fact that Sentinel stores the ideal ms in increments of 33 ms, like:
						// .000, .033, .066, .100, .133 etc.  which means that they are rounded.
						// So something that has a frame of 01 would get 033 milliseconds, not 033.333333 ms
						// this truncs to 33 and so we get frame number 00.
						// by adding .99999 we up this to 34.66666666 which truncs to 34 and we are all set.
						// similar for 66.6666 -> 67.  BUT, 100ms -> 100.999999999 ms -> 100.  so we are good with round values.

						if((m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS == 0)&&(!(m_ppAutoEvents[nIndex]->m_ulControl&0x10))) // this could be an error, attempt to correct it.
						{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetAutomationEvents", "Detected time address of 00:00:00.00" );//  Sleep(250); //(Dispatch message)/

							if(nIndex>0)
							{
								if(m_ppAutoEvents[nIndex-1])
								{
									if((m_ppAutoEvents[nIndex-1]->m_ulOnAirTimeMS  + m_ppAutoEvents[nIndex-1]->m_ulDurationMS)%86400000)
									{
										m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS  =  m_ppAutoEvents[nIndex-1]->m_ulOnAirTimeMS  + m_ppAutoEvents[nIndex-1]->m_ulDurationMS; 
										m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS %= 86400000; // in case rolled over
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetAutomationEvents", "Re-assigned start time of event from 0 to %d", m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS );//  Sleep(250); //(Dispatch message)/
									}
								}
							}
						}

					}
					prs->GetFieldValue("event_duration", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_ulDurationMS = atol(szTemp); // neg durations ever? no, guess not.
						if(m_ppAutoEvents[nIndex]->m_ulDurationMS%10) m_ppAutoEvents[nIndex]->m_ulDurationMS++;  // these are ideal values.  so will either end in 0, 3, or 6.  if not 0, increment up by one ms.
					}
				
if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_AUTOEVENTS)) 
{
	unsigned long ulTCoat;
	unsigned long ulTCdur;
	CTimeConvert tcv;
	tcv.MStoBCDHMSF(m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS, &ulTCoat,(double)g_settings.m_nFrameRate); // using ideal
	tcv.MStoBCDHMSF(m_ppAutoEvents[nIndex]->m_ulDurationMS, &ulTCdur,(double)g_settings.m_nFrameRate); // using ideal

	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetAutomationEvents", "%02d[%s][%s] oat %08x %d dur %08x %d", nIndex,
		m_ppAutoEvents[nIndex]->m_pszID?m_ppAutoEvents[nIndex]->m_pszID:"(null)",
		m_ppAutoEvents[nIndex]->m_pszTitle?m_ppAutoEvents[nIndex]->m_pszTitle:"(null)",
		ulTCoat, m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS,
		ulTCdur, m_ppAutoEvents[nIndex]->m_ulDurationMS
		);//  Sleep(250); //(Dispatch message)
}
				}
				nIndex++;
				prs->MoveNext();
			}


			if(nIndex<m_nAutoEvents)
			{
if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_AUTOEVENTS)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetAutomationEvents", "Deleting overflow %d>%d", m_nAutoEvents, nIndex);//  Sleep(250); //(Dispatch message)
}
				int i=nIndex;
				while(i<m_nAutoEvents)
				{
					if(m_ppAutoEvents[i]) delete m_ppAutoEvents[i];
					m_ppAutoEvents[i] = NULL;
					i++;
				}
if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_AUTOEVENTS)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetAutomationEvents", "Deleted overflow, events now %d", nIndex); // Sleep(250); //(Dispatch message)
}
			}

			m_nAutoEvents = nIndex;

			if((m_ppAutoEvents)&&(m_nAutoEvents)&&(ppTemp != NULL)&&(nNumEvents>0))
			{
				// match to preserve previously calc'd output vars
				int nLastFound = -1;
				
				int i=0;
				while(i<m_nAutoEvents)
				{
					if(m_ppAutoEvents[i])
					{
						int nCount=0;
						int j = nLastFound+1;
						int nEndIndex = nNumEvents-1;

						while(nCount < nNumEvents) // max search them all
						{
							if(j>nEndIndex) j=0;
							if(ppTemp[j])
							{
								if(ppTemp[j]->m_nID == m_ppAutoEvents[i]->m_nID)
								{
									// match!
									m_ppAutoEvents[i]->m_pszOutputString  = ppTemp[j]->m_pszOutputString;
									ppTemp[j]->m_pszOutputString = NULL; //prevents freeing

//									m_ppAutoEvents[i]->m_ulClassification = ppTemp[j]->m_ulClassification;

									nLastFound = j;

									break;
								}
							}
							j++;							
							nCount++; 
						}
						if(nCount >= nNumEvents) // didnt find.
							nLastFound = -1;
					}
					i++;
				}
				
		//		nNumEvents = m_nAutoEvents;  // this is just wrong.
			}

			if((ppTemp != NULL)&&(nNumEvents>0))
			{
				int i=0;
				while(i<nNumEvents)
				{
					if(ppTemp[i]) delete ppTemp[i];
					i++;
				}
				delete [] ppTemp;
			}

			m_bInitalEventGetSucceeded = true;

			prs->Close();
			delete prs;

			return nIndex;
		}
	}
	return TABULATOR_ERROR;
}

int CSerialTonesData::GetServerTime(bool bCalcOnly)
{
	if((g_settings.m_bUseTimeCode)&&(m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
	{
		unsigned long ulTimeCode;
		EnterCriticalSection(&m_critTimeCode);
		ulTimeCode = m_ulTimeCode;
//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
		LeaveCriticalSection(&m_critTimeCode);

		m_Output.m_n_current_servertime = m_timeconv.BCDHMSFtoMS(ulTimeCode, ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)); // make it into ideal ms.
		m_Output.m_n_current_systemtime = m_Output.m_n_current_servertime;
		
if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_TIMECODE)) 
g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "AutomationData:***** **", "TC %d", m_Output.m_n_current_servertime); // Sleep(250); //(Dispatch message)


		return TABULATOR_SUCCESS;

	}
	else
	if(bCalcOnly)
	{
		_timeb timestamp;
		_ftime(&timestamp);

		// get system time, (not local time)
		double dblSystemTime = (double)(((unsigned long)(timestamp.time)*1000) + (unsigned long)timestamp.millitm);
		m_Output.m_n_current_systemtime = ((timestamp.time/* - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)*/ )%86400)*1000 + timestamp.millitm;

		if((m_Output.m_b_list_is_playing)&&(m_Output.m_dbl_last_servertime>0.0)&&(m_Output.m_dbl_last_servertime_update>0.0))
		{
			m_Output.m_n_current_servertime = (int)(((__int64)(m_Output.m_dbl_last_servertime))%86400000)
				+ (int)(m_Output.m_dbl_last_servertime_update - dblSystemTime); 

			if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_TIMECODE)) 
g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "AutomationData:***** **", "TC sys %d", g_data.m_Output.m_n_current_servertime); // Sleep(250); //(Dispatch message)
			return TABULATOR_SUCCESS;

		}
		else
		{
			m_Output.m_n_current_servertime = m_Output.m_n_current_systemtime;
		}
	}
	else
	if((m_pdbConn)&&(m_pdb)&&(g_settings.m_pszAutoServer)&&(strlen(g_settings.m_pszAutoServer)>0)&&(g_settings.m_nAutoServerListNumber>0))
	{
		double dblSystemTime=-1.0;

		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT server_time, server_last_update \
FROM %s.dbo.%s WHERE server = '%s' AND (flags & 1 = 1)",  // has to be active
			((g_settings.m_pszAutoModule)&&(strlen(g_settings.m_pszAutoModule)))?g_settings.m_pszAutoModule:"Sentinel",
			((g_settings.m_pszAutoServers)&&(strlen(g_settings.m_pszAutoServers)))?g_settings.m_pszAutoServers:"Connections",
			g_settings.m_pszAutoServer
			); 


if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_AUTOLIST)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetServerTime", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "GetAutomationListStatus");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		int nIndex = 0;
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			if (!prs->IsEOF())
			{
				nIndex++;
				CString szTemp;

				double dblServerTime=-1.0;
				double dblSystemUpdateTime=-1.0;
				prs->GetFieldValue("server_time", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					dblServerTime = atof(szTemp);
				}
				prs->GetFieldValue("server_last_update", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					dblSystemUpdateTime = atof(szTemp);
				}

				prs->Close();
				delete prs;

				_timeb timestamp;
				_ftime(&timestamp);

				// get system time, (not local time)
				dblSystemTime = (double)(((unsigned long)(timestamp.time)*1000) + (unsigned long)timestamp.millitm);
				m_Output.m_n_current_systemtime = ((timestamp.time/* - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)*/ )%86400)*1000 + timestamp.millitm;

				if((m_Output.m_b_list_is_playing)&&(dblServerTime>0.0)&&(dblSystemUpdateTime>0.0))
				{
 				// call these outside.
//					EnterCriticalSection(&g_data.m_critOutputVariables);
			
					m_Output.m_dbl_last_servertime = dblServerTime;    // in ideal MS since epoch, local time! 
					m_Output.m_dbl_last_servertime_update = dblSystemUpdateTime; // in ideal MS in ideal MS since epoch; 

					m_Output.m_n_current_servertime = (int)(((__int64)(m_Output.m_dbl_last_servertime))%86400000)
						+ (int)(m_Output.m_dbl_last_servertime_update - dblSystemTime); 

					if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_TIMECODE)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "AutomationData:***** **", "TC sys %d", m_Output.m_n_current_servertime); // Sleep(250); //(Dispatch message)

//					LeaveCriticalSection(&m_critOutputVariables);
				}
				else
				{
					m_Output.m_n_current_servertime = m_Output.m_n_current_systemtime;
				}
		
			}
			prs->Close();
			delete prs;

		}
		
		if(dblSystemTime<0.0)
		{
			_timeb timestamp;
			_ftime(&timestamp);

			// get system time, (not local time)
			dblSystemTime = (double)(((unsigned long)(timestamp.time)*1000) + (unsigned long)timestamp.millitm);
			m_Output.m_n_current_systemtime = ((timestamp.time/* - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)*/ )%86400)*1000 + timestamp.millitm;
			m_Output.m_n_current_servertime = m_Output.m_n_current_systemtime;

		}
		else
		{
			return TABULATOR_SUCCESS;
		}

	}
	else
	{
			_timeb timestamp;
			_ftime(&timestamp);

			// get system time, (not local time)
			double dblSystemTime = (double)(((unsigned long)(timestamp.time)*1000) + (unsigned long)timestamp.millitm);
			m_Output.m_n_current_systemtime = ((timestamp.time/* - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)*/ )%86400)*1000 + timestamp.millitm;
			m_Output.m_n_current_servertime = m_Output.m_n_current_systemtime;
	}
	return TABULATOR_ERROR;

}

int CSerialTonesData::GetAutomationListStatus()
{
	if((m_pdbConn)&&(m_pdb)&&(g_settings.m_pszAutoServer)&&(strlen(g_settings.m_pszAutoServer)>0)&&(g_settings.m_nAutoServerListNumber>0))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update \
FROM %s.dbo.%s WHERE server = '%s' AND listid = %d AND (flags & 1 = 1)",  // has to be active
			((g_settings.m_pszAutoModule)&&(strlen(g_settings.m_pszAutoModule)))?g_settings.m_pszAutoModule:"Sentinel",
			((g_settings.m_pszAutoLists)&&(strlen(g_settings.m_pszAutoLists)))?g_settings.m_pszAutoLists:"Channels",
			g_settings.m_pszAutoServer, g_settings.m_nAutoServerListNumber

			); 

		/*
class CAutomationList  
{
public:
	CAutomationList();
	virtual ~CAutomationList();

	int m_nState;
	int m_nChange;
	int m_nDisplay;
	int m_nSysChange;
	int m_nCount; 
	int m_nLookahead;
	double m_dblLastUpdate;
};

USE [sentinel]
GO
/ ****** Object:  Table [dbo].[Channels]    Script Date: 12/19/2011 00:55:03 ****** /
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Channels](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[flags] [int] NOT NULL,
	[description] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[server] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[listid] [int] NULL,
	[list_state] [int] NULL,
	[list_changed] [int] NULL,
	[list_display] [int] NULL,
	[list_syschange] [int] NULL,
	[list_count] [int] NULL,
	[list_lookahead] [int] NULL,
	[list_last_update] [decimal](20, 3) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF

*/



if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_AUTOLIST)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetAutomationListStatus", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "GetAutomationListStatus");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		int nIndex = 0;
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			if (!prs->IsEOF())
			{
				nIndex++;
				CString szTemp;

// SELECT list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update
				
				prs->GetFieldValue("list_state", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nState = atoi(szTemp);
				}
				prs->GetFieldValue("list_changed", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nChange = atoi(szTemp);
				}
				prs->GetFieldValue("list_display", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nDisplay = atoi(szTemp);
				}
				prs->GetFieldValue("list_syschange", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nSysChange = atoi(szTemp);
				}
				prs->GetFieldValue("list_count", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nCount = atoi(szTemp);
				}
				prs->GetFieldValue("list_lookahead", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nLookahead = atoi(szTemp);
				}
				prs->GetFieldValue("list_last_update", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_dblLastUpdate = atof(szTemp);
				}


				if((m_AutoList.m_nState<0)||(m_AutoList.m_nChange<0))	m_bInitalEventGetSucceeded = true; // nothing was connected or active so OK, call that initialized.


				prs->Close();
				delete prs;
				return TABULATOR_SUCCESS;
			
			}
			else
			{
				m_bInitalEventGetSucceeded = true; // nothing was active so OK, call that initialized.
			}

			prs->Close();
			delete prs;
		}

		if(nIndex==0)
		{
			// no records, either no automation or whatever...
			m_AutoList.m_nState=-1;
			m_AutoList.m_nChange=-1;
			m_AutoList.m_nDisplay=-1;
			m_AutoList.m_nSysChange=-1;
			m_AutoList.m_nCount=-1;
			m_AutoList.m_nLookahead=-1;
			m_AutoList.m_dblLastUpdate = -1.0;
		}
	}
	return TABULATOR_ERROR;
}

/*
int CSerialTonesData::GetOpportuneEvents()
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		// unfortunately, I cannot just get the ones within the valid dates, since time progresses, and I only get this table when it changes.
		// I have to weed out the ones as I go....
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, event_name, valid_date, expire_date FROM %s ORDER BY ID", 
			((g_settings.m_pszOpportuneEvents)&&(strlen(g_settings.m_pszOpportuneEvents)))?g_settings.m_pszOpportuneEvents:"OpportuneEvents"
			); 

	//create table OpportuneEvents (ID int identity(1,1), event_name varchar(64), event_duration int, valid_date decimal(20,3), expire_date decimal(20,3))  
if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetOpportuneEvents", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "GetOpportuneEvents");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nOpportuneEventsArraySize) // have to expand
				{
					COpportuneEvent** ppNew = new COpportuneEvent*[m_nOpportuneEventsArraySize + SERIALTONES_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nOpportuneEventsArraySize += SERIALTONES_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(COpportuneEvent*)*m_nOpportuneEventsArraySize);
						if(m_ppOpportuneEvents)
						{
							memcpy(ppNew, m_ppOpportuneEvents, sizeof(COpportuneEvent*)*nIndex); // it's an array of pointers
						}
						m_ppOpportuneEvents = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppOpportuneEvents==NULL)
				{
					m_nOpportuneEventsArraySize = 0;
					m_nOpportuneEvents=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nOpportuneEvents)
				{
					m_ppOpportuneEvents[nIndex] = new COpportuneEvent;  // guaranteed that there will be array space, above.
				}

				if(m_ppOpportuneEvents[nIndex])
				{
// removed all length validation.
//if something is "blank" then let it be - it has to overwrite any old data.

					CString szTemp;
	//SELECT ID, programID, sequence, replacementID, duration, valid_date, expire_date
					prs->GetFieldValue("ID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppOpportuneEvents[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("event_name", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						if(m_ppOpportuneEvents[nIndex]->m_pszEventToken != NULL)
						{
							free(m_ppOpportuneEvents[nIndex]->m_pszEventToken);
						}
						m_ppOpportuneEvents[nIndex]->m_pszEventToken = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppOpportuneEvents[nIndex]->m_pszEventToken)
						{
							strcpy(m_ppOpportuneEvents[nIndex]->m_pszEventToken, szTemp.GetBuffer(0));
						}
					}

					prs->GetFieldValue("event_duration", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppOpportuneEvents[nIndex]->m_nDuration = atoi(szTemp);
					}
					prs->GetFieldValue("valid_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppOpportuneEvents[nIndex]->m_dblValidDate = atof(szTemp);
					}
					prs->GetFieldValue("expire_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppOpportuneEvents[nIndex]->m_dblExpireDate = atof(szTemp);
					}
				}

				m_ppOpportuneEvents[nIndex]->m_bEventTokenChanged = true; // just do, regardless

if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_OPPEVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SerialTones:debug", "OE%03d ID %d, %s, %.3f-%.3f", 
	nIndex, 
	m_ppOpportuneEvents[nIndex]->m_nID,
	m_ppOpportuneEvents[nIndex]->m_pszEventToken,
	m_ppOpportuneEvents[nIndex]->m_dblValidDate,
	m_ppOpportuneEvents[nIndex]->m_dblExpireDate
	);

				nIndex++;
				prs->MoveNext();
			}

			if(nIndex<m_nOpportuneEvents)
			{
				int i=nIndex;
				while(i<m_nOpportuneEvents)
				{
					if(m_ppOpportuneEvents[i]) delete m_ppOpportuneEvents[i];
					m_ppOpportuneEvents[i] = NULL;
					i++;
				}
			}
			m_nOpportuneEvents = nIndex;

			prs->Close();
			delete prs;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_OPPEVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SerialTones:debug", "OE: %d", m_nOpportuneEvents	);



			return nIndex;
		}
	}
	return TABULATOR_ERROR;
}


int CSerialTonesData::GetExclusionRules()
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		// unfortunately, I cannot just get the ones within the valid dates, since time progresses, and I only get this table when it changes.
		// I have to weed out the ones as I go....
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, pattern, col_name, auto_event_type, event_type, valid_date, expire_date FROM %s ORDER BY ID", 
			((g_settings.m_pszExclusionRules)&&(strlen(g_settings.m_pszExclusionRules)))?g_settings.m_pszExclusionRules:"ExclusionRules"
			); 

	//create table ExclusionRules (ID int identity(1,1), pattern varchar(128), col_name varchar(64), auto_event_type int, event_type int, valid_date decimal(20,3), expire_date decimal(20,3))  
if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetExclusionRules", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "GetExclusionRules");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nExclusionRulesArraySize) // have to expand
				{
					CExclusionRule** ppNew = new CExclusionRule*[m_nExclusionRulesArraySize + SERIALTONES_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nExclusionRulesArraySize += SERIALTONES_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(CExclusionRule*)*m_nExclusionRulesArraySize);
						if(m_ppExclusionRules)
						{
							memcpy(ppNew, m_ppExclusionRules, sizeof(CExclusionRule*)*nIndex); // it's an array of pointers
						}
						m_ppExclusionRules = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppExclusionRules==NULL)
				{
					m_nExclusionRulesArraySize = 0;
					m_nExclusionRules=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nExclusionRules)
				{
					m_ppExclusionRules[nIndex] = new CExclusionRule;  // guaranteed that there will be array space, above.
				}

				if(m_ppExclusionRules[nIndex])
				{
// removed all length validation.
//if something is "blank" then let it be - it has to overwrite any old data.

					CString szTemp;
	//SELECT ID, programID, sequence, replacementID, duration, valid_date, expire_date
					prs->GetFieldValue("ID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppExclusionRules[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("pattern", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						if(m_ppExclusionRules[nIndex]->m_pszPattern != NULL)
						{
							free(m_ppExclusionRules[nIndex]->m_pszPattern);
						}
						m_ppExclusionRules[nIndex]->m_pszPattern = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppExclusionRules[nIndex]->m_pszPattern)
						{
							strcpy(m_ppExclusionRules[nIndex]->m_pszPattern, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("col_name", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						if(m_ppExclusionRules[nIndex]->m_pszColName != NULL)
						{
							free(m_ppExclusionRules[nIndex]->m_pszColName);
						}
						m_ppExclusionRules[nIndex]->m_pszColName = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppExclusionRules[nIndex]->m_pszColName)
						{
							strcpy(m_ppExclusionRules[nIndex]->m_pszColName, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("auto_event_type", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppExclusionRules[nIndex]->m_nAutoEventType = atoi(szTemp);
					}
					prs->GetFieldValue("event_type", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppExclusionRules[nIndex]->m_nEventType = atoi(szTemp);
					}
					prs->GetFieldValue("valid_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppExclusionRules[nIndex]->m_dblValidDate = atof(szTemp);
					}
					prs->GetFieldValue("expire_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppExclusionRules[nIndex]->m_dblExpireDate = atof(szTemp);
					}
				}


if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_EXCLRULES)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SerialTones:debug", "ER%03d ID %d, %s %s, atype %d, type %d, %.3f-%.3f", 
	nIndex, 
	m_ppExclusionRules[nIndex]->m_nID,
	m_ppExclusionRules[nIndex]->m_pszPattern,
	m_ppExclusionRules[nIndex]->m_pszColName,
	m_ppExclusionRules[nIndex]->m_nAutoEventType,
	m_ppExclusionRules[nIndex]->m_nEventType,
	m_ppExclusionRules[nIndex]->m_dblValidDate,
	m_ppExclusionRules[nIndex]->m_dblExpireDate
	
	);

				nIndex++;
				prs->MoveNext();
			}

			if(nIndex<m_nExclusionRules)
			{
				int i=nIndex;
				while(i<m_nExclusionRules)
				{
					if(m_ppExclusionRules[i]) delete m_ppExclusionRules[i];
					m_ppExclusionRules[i] = NULL;
					i++;
				}
			}
			m_nExclusionRules = nIndex;

			prs->Close();
			delete prs;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_EXCLRULES)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SerialTones:debug", "ER: %d", m_nExclusionRules	);



			return nIndex;
		}
	}
	return TABULATOR_ERROR;
}
*/

/*
int CSerialTonesData::GetReplacementRules()
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		// unfortunately, I cannot just get the ones within the valid dates, since time progresses, and I only get this table when it changes.
		// I have to weed out the ones as I go....
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, programID, sequence, replacementID, duration, valid_date, expire_date FROM %s ORDER BY sequence", 
			((g_settings.m_pszReplacementRules)&&(strlen(g_settings.m_pszReplacementRules)))?g_settings.m_pszReplacementRules:"ReplacementRules"
			); 

//create table ReplacementRules (ID int identity(1,1), programID varchar(16), sequence int, replacementID varchar(16), duration int, valid_date decimal(20,3), expire_date decimal(20,3))  
if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetReplacementRules", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "GetReplacementRules");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nReplacementRulesArraySize) // have to expand
				{
					CReplacementRule** ppNew = new CReplacementRule*[m_nReplacementRulesArraySize + SERIALTONES_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nReplacementRulesArraySize += SERIALTONES_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(CReplacementRule*)*m_nReplacementRulesArraySize);
						if(m_ppReplacementRules)
						{
							memcpy(ppNew, m_ppReplacementRules, sizeof(CReplacementRule*)*nIndex); // it's an array of pointers
						}
						m_ppReplacementRules = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppReplacementRules==NULL)
				{
					m_nReplacementRulesArraySize = 0;
					m_nReplacementRules=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nReplacementRules)
				{
					m_ppReplacementRules[nIndex] = new CReplacementRule;  // guaranteed that there will be array space, above.
				}

				if(m_ppReplacementRules[nIndex])
				{
// removed all length validation.
//if something is "blank" then let it be - it has to overwrite any old data.

					CString szTemp;
	//SELECT ID, programID, sequence, replacementID, duration, valid_date, expire_date
					prs->GetFieldValue("ID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppReplacementRules[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("programID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						if(m_ppReplacementRules[nIndex]->m_pszProgramID != NULL)
						{
							free(m_ppReplacementRules[nIndex]->m_pszProgramID);
						}
						m_ppReplacementRules[nIndex]->m_pszProgramID = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppReplacementRules[nIndex]->m_pszProgramID)
						{
							strcpy(m_ppReplacementRules[nIndex]->m_pszProgramID, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("sequence", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppReplacementRules[nIndex]->m_nSequence = atoi(szTemp);
					}
					prs->GetFieldValue("replacementID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						if(m_ppReplacementRules[nIndex]->m_pszReplacementID != NULL)
						{
							free(m_ppReplacementRules[nIndex]->m_pszReplacementID);
						}
						m_ppReplacementRules[nIndex]->m_pszReplacementID = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppReplacementRules[nIndex]->m_pszReplacementID)
						{
							strcpy(m_ppReplacementRules[nIndex]->m_pszReplacementID, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("duration", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppReplacementRules[nIndex]->m_nDuration = atoi(szTemp);

						// following line is so that we do not lose a frame due to rounding down 33.3333 to 33 and 66.6666 to 66.
						if(m_ppReplacementRules[nIndex]->m_nDuration%10) m_ppReplacementRules[nIndex]->m_nDuration++;


						if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
						{
//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp

							if(g_data.m_ucTimeCodeBits&0x80) // drop frame...
							{
								m_ppReplacementRules[nIndex]->m_nDurationInFrames = g_data.m_timeconv.MStoF(g_data.m_timeconv.NDFMStoDFMS( m_ppReplacementRules[nIndex]->m_nDuration ), 29.97) ;
							}
							else
							{
								m_ppReplacementRules[nIndex]->m_nDurationInFrames = g_data.m_timeconv.MStoF( m_ppReplacementRules[nIndex]->m_nDuration , ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) ;
							}
						}
						else  // system time
						{
							if(g_settings.m_bDF) // drop frame...
							{
								m_ppReplacementRules[nIndex]->m_nDurationInFrames = g_data.m_timeconv.MStoF(g_data.m_timeconv.NDFMStoDFMS( m_ppReplacementRules[nIndex]->m_nDuration ), 29.97) ;
							}
							else
							{
								m_ppReplacementRules[nIndex]->m_nDurationInFrames = g_data.m_timeconv.MStoF( m_ppReplacementRules[nIndex]->m_nDuration , g_settings.m_nFrameRate) ;
							}
						}



					}
					prs->GetFieldValue("valid_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppReplacementRules[nIndex]->m_dblValidDate = atof(szTemp);
					}
					prs->GetFieldValue("expire_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppReplacementRules[nIndex]->m_dblExpireDate = atof(szTemp);
					}
				}


if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_REPLRULES)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SerialTones:debug", "RR%03d ID %s, seq %d, ReplID %s, Dur %d, %.3f-%.3f", 
	nIndex, 
	m_ppReplacementRules[nIndex]->m_pszProgramID,
	m_ppReplacementRules[nIndex]->m_nSequence,
	m_ppReplacementRules[nIndex]->m_pszReplacementID,
	m_ppReplacementRules[nIndex]->m_nDuration, // in ideal MS
	m_ppReplacementRules[nIndex]->m_dblValidDate,
	m_ppReplacementRules[nIndex]->m_dblExpireDate
	
	);

				nIndex++;
				prs->MoveNext();
			}

			if(nIndex<m_nReplacementRules)
			{
				int i=nIndex;
				while(i<m_nReplacementRules)
				{
					if(m_ppReplacementRules[i]) delete m_ppReplacementRules[i];
					m_ppReplacementRules[i] = NULL;
					i++;
				}
			}
			m_nReplacementRules = nIndex;

			prs->Close();
			delete prs;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_REPLRULES)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SerialTones:debug", "RR: %d", m_nReplacementRules	);



			return nIndex;
		}
	}
	return TABULATOR_ERROR;
}

int CSerialTonesData::GetDescriptionPatterns()
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		// unfortunately, I cannot just get the ones within the valid dates, since time progresses, and I only get this table when it changes.
		// I have to weed out the ones as I go....
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, programID, pattern, valid_date, expire_date FROM %s ORDER BY ID", 
			((g_settings.m_pszDescriptionPattern)&&(strlen(g_settings.m_pszDescriptionPattern)))?g_settings.m_pszDescriptionPattern:"DescriptionPattern"
			); 

//create table DescriptionPattern (ID int identity(1,1), programID varchar(16), pattern varchar(128), valid_date decimal(20,3), expire_date decimal(20,3))  

if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetDescriptionPatterns", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "GetDescriptionPatterns");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nDescriptionPatternsArraySize) // have to expand
				{
					CDescriptionPattern** ppNew = new CDescriptionPattern*[m_nDescriptionPatternsArraySize + SERIALTONES_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nDescriptionPatternsArraySize += SERIALTONES_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(CDescriptionPattern*)*m_nDescriptionPatternsArraySize);
						if(m_ppDescriptionPatterns)
						{
							memcpy(ppNew, m_ppDescriptionPatterns, sizeof(CDescriptionPattern*)*nIndex); // it's an array of pointers
						}
						m_ppDescriptionPatterns = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppDescriptionPatterns==NULL)
				{
					m_nDescriptionPatternsArraySize = 0;
					m_nDescriptionPatterns=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nDescriptionPatterns)
				{
					m_ppDescriptionPatterns[nIndex] = new CDescriptionPattern;  // guaranteed that there will be array space, above.
				}

				if(m_ppDescriptionPatterns[nIndex])
				{
					CString szTemp;
// removed all length validation. EXCEPT for pattern - that you need
//if something is "blank" then let it be - it has to overwrite any old data.
	//SELECT ID, programID, pattern, valid_date, expire_date
					prs->GetFieldValue("ID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppDescriptionPatterns[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("programID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						if(m_ppDescriptionPatterns[nIndex]->m_pszProgramID != NULL)
						{
							free(m_ppDescriptionPatterns[nIndex]->m_pszProgramID);
						}
						m_ppDescriptionPatterns[nIndex]->m_pszProgramID = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppDescriptionPatterns[nIndex]->m_pszProgramID)
						{
							strcpy(m_ppDescriptionPatterns[nIndex]->m_pszProgramID, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("pattern", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						if(m_ppDescriptionPatterns[nIndex]->m_pszPattern != NULL)
						{
							free(m_ppDescriptionPatterns[nIndex]->m_pszPattern);
						}
						m_ppDescriptionPatterns[nIndex]->m_pszPattern = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppDescriptionPatterns[nIndex]->m_pszPattern)
						{
							strcpy(m_ppDescriptionPatterns[nIndex]->m_pszPattern, szTemp.GetBuffer(0));
						}
					}
					else
					{
						if(m_ppDescriptionPatterns[nIndex]->m_pszPattern != NULL)
						{
							free(m_ppDescriptionPatterns[nIndex]->m_pszPattern);
						}
						m_ppDescriptionPatterns[nIndex]->m_pszPattern = NULL;
					}
					prs->GetFieldValue("valid_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppDescriptionPatterns[nIndex]->m_dblValidDate = atof(szTemp);
					}
					prs->GetFieldValue("expire_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppDescriptionPatterns[nIndex]->m_dblExpireDate = atof(szTemp);
					}
				}

if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_DESCPATTERNS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SerialTones:debug", "DP%03d ID %s, Pattern %s, %.3f-%.3f", 
	nIndex, 
	m_ppDescriptionPatterns[nIndex]->m_pszProgramID,
	m_ppDescriptionPatterns[nIndex]->m_pszPattern,
	m_ppDescriptionPatterns[nIndex]->m_dblValidDate,
	m_ppDescriptionPatterns[nIndex]->m_dblExpireDate
	
	);


				nIndex++;
				prs->MoveNext();
			}

			if(nIndex<m_nDescriptionPatterns)
			{
				int i=nIndex;
				while(i<m_nDescriptionPatterns)
				{
					if(m_ppDescriptionPatterns[i]) delete m_ppDescriptionPatterns[i];
					m_ppDescriptionPatterns[i] = NULL;
					i++;
				}
			}

			m_nDescriptionPatterns = nIndex;

			prs->Close();
			delete prs;


if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_DESCPATTERNS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SerialTones:debug", "DP: %d", m_nDescriptionPatterns	);



			return nIndex;
		}
	}
	return TABULATOR_ERROR;
}


int CSerialTonesData::GetClassificationRules()
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, pattern, col_name, event_type FROM %s ORDER BY event_type", // if we get ads first, those are easier to actuate, so let's prioritize them
			((g_settings.m_pszClassificationRules)&&(strlen(g_settings.m_pszClassificationRules)))?g_settings.m_pszClassificationRules:"ClassificationRules"
			); 

//create table ClassificationRules (ID int identity(1,1), pattern varchar(128), col_name varchar(64), event_type int)  

if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetClassificationRules", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "GetClassificationRules");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nClassificationRulesArraySize) // have to expand
				{
					CClassificationRule** ppNew = new CClassificationRule*[m_nClassificationRulesArraySize + SERIALTONES_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nClassificationRulesArraySize += SERIALTONES_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(CClassificationRule*)*m_nClassificationRulesArraySize);
						if(m_ppClassificationRules)
						{
							memcpy(ppNew, m_ppClassificationRules, sizeof(CClassificationRule*)*nIndex); // it's an array of pointers
						}
						m_ppClassificationRules = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppClassificationRules==NULL)
				{
					m_nClassificationRulesArraySize = 0;
					m_nClassificationRules=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nClassificationRules)
				{
					m_ppClassificationRules[nIndex] = new CClassificationRule;  // guaranteed that there will be array space, above.
				}

				if(m_ppClassificationRules[nIndex])
				{
					CString szTemp;
// removed all length validation. EXCEPT for pattern - that you need
//if something is "blank" then let it be - it has to overwrite any old data.
	//SELECT ID, pattern, col_name, event_type 
					prs->GetFieldValue("ID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppClassificationRules[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("pattern", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						if(m_ppClassificationRules[nIndex]->m_pszPattern != NULL)
						{
							free(m_ppClassificationRules[nIndex]->m_pszPattern);
						}
						m_ppClassificationRules[nIndex]->m_pszPattern = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppClassificationRules[nIndex]->m_pszPattern)
						{
							strcpy(m_ppClassificationRules[nIndex]->m_pszPattern, szTemp.GetBuffer(0));
						}
					}
					else
					{
						if(m_ppClassificationRules[nIndex]->m_pszPattern != NULL)
						{
							free(m_ppClassificationRules[nIndex]->m_pszPattern);
						}
						m_ppClassificationRules[nIndex]->m_pszPattern = NULL;
					}

					prs->GetFieldValue("col_name", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						if(m_ppClassificationRules[nIndex]->m_pszColName != NULL)
						{
							free(m_ppClassificationRules[nIndex]->m_pszColName);
						}
						m_ppClassificationRules[nIndex]->m_pszColName = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppClassificationRules[nIndex]->m_pszColName)
						{
							strcpy(m_ppClassificationRules[nIndex]->m_pszColName, szTemp.GetBuffer(0));
						}
					}
					else
					{
						if(m_ppClassificationRules[nIndex]->m_pszColName != NULL)
						{
							free(m_ppClassificationRules[nIndex]->m_pszColName);
						}
						m_ppClassificationRules[nIndex]->m_pszColName = NULL;
					}
					prs->GetFieldValue("event_type", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppClassificationRules[nIndex]->m_nEventType = atoi(szTemp);
					}
				}

if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_CLASSRULES)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetClassificationRules", "CR%d [%s][%s] %d", 
		nIndex,
		m_ppClassificationRules[nIndex]->m_pszColName?m_ppClassificationRules[nIndex]->m_pszColName:"(null)",
		m_ppClassificationRules[nIndex]->m_pszPattern?m_ppClassificationRules[nIndex]->m_pszPattern:"(null)",
		m_ppClassificationRules[nIndex]->m_nEventType
		); // Sleep(250); //(Dispatch message)
}


				nIndex++;
				prs->MoveNext();
			}


			if(nIndex<m_nClassificationRules)
			{
				int i=nIndex;
				while(i<m_nClassificationRules)
				{
					if(m_ppClassificationRules[i]) delete m_ppClassificationRules[i];
					m_ppClassificationRules[i] = NULL;
					i++;
				}
			}

			m_nClassificationRules = nIndex;

			prs->Close();
			delete prs;

			return nIndex;
		}
	}
	return TABULATOR_ERROR;
}
*/
/*
int CSerialTonesData::CountCurrentReplacementRules(double dblTime)// use system time
{
	if(m_ppReplacementRules)
	{
		int nCount=0;
		int i=0;
		while(i<m_nReplacementRules)
		{
			if(m_ppReplacementRules[i])
			{

				if(
					  (dblTime < m_ppReplacementRules[i]->m_dblExpireDate)  // not <= because = it's expired now
					&&(dblTime >= m_ppReplacementRules[i]->m_dblValidDate)  //  is >= because = means it's valid now
					)
				{
					nCount++;
				}
			}

			i++;
		}
		return nCount;
	}
	return 0;
}

*/

int CSerialTonesData::ReportAutomationEvents()
{

	if((m_ppAutoEvents)&&(m_nAutoEvents)&&(g_ptabmsgr))
	{
		// match to preserve previously calc'd output vars
		int nLastFound = -1;
		unsigned long ulTCoat;
		unsigned long ulTCdur;
		CTimeConvert tcv;

		unsigned long ulTimeCode;
		if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
		{
			EnterCriticalSection(&g_data.m_critTimeCode);
			ulTimeCode = g_data.m_ulTimeCode;
//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
			LeaveCriticalSection(&g_data.m_critTimeCode);
		}
		else
		{
			// use system time
			_timeb timestamp;
			_ftime(&timestamp);
			int nTC = ((timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))%86400)*1000 + timestamp.millitm;
			ulTimeCode = tcv.MStoBCDHMSF(nTC, &ulTimeCode, g_settings.m_nFrameRate);
		}



		
		g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:ReportAutomationEvents", "************************ %d events, logging %d at TC %08x", m_nAutoEvents, (g_settings.m_nAutomationLoggingLookahead>0)?g_settings.m_nAutomationLoggingLookahead:m_nAutoEvents, ulTimeCode);
		int i=0;
		while (
						(i<m_nAutoEvents)
					&&(
							(g_settings.m_nAutomationLoggingLookahead<=0)
						||(
								(g_settings.m_nAutomationLoggingLookahead>0)
							&&(i<g_settings.m_nAutomationLoggingLookahead)
							)
						)
					)
		{
			if(m_ppAutoEvents[i])
			{
				tcv.MStoBCDHMSF(m_ppAutoEvents[i]->m_ulOnAirTimeMS, &ulTCoat,(double)g_settings.m_nFrameRate); // using ideal
				tcv.MStoBCDHMSF(m_ppAutoEvents[i]->m_ulDurationMS, &ulTCdur,(double)g_settings.m_nFrameRate); // using ideal

				g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:ReportAutomationEvents", "%04d oat:%08x dur:%08x status:%04x type:%04x [%s][%s]", 
					i,
					ulTCoat,ulTCdur, 
					m_ppAutoEvents[i]->m_usStatus,
					m_ppAutoEvents[i]->m_usType,
					m_ppAutoEvents[i]->m_pszID?m_ppAutoEvents[i]->m_pszID:"(null)",
					m_ppAutoEvents[i]->m_pszTitle?m_ppAutoEvents[i]->m_pszTitle:"(null)"
					);//  Sleep(250); //(Dispatch message)

			}
			i++;
		}
		g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:ReportAutomationEvents", "************************");

		return i;
	}
	else
	{
		if(g_ptabmsgr)
			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:ReportAutomationEvents", "There are no automation events.");

		return 0;
	}
}






int CSerialTonesData::Query_Ready()
{
	if(m_bInitalEventGetSucceeded) return TABULATOR_SUCCESS;
	return TABULATOR_ERROR;
}

char* CSerialTonesData::FormatDTMF(int nDurationMS)
{
	if(nDurationMS<0) return NULL;
	char pszDTMF[MAX_PATH];

	int nDur = (nDurationMS
		+ g_settings.m_bRoundToNearestUnit?(g_settings.m_nDurationUnitDivisor/2):0)
		/ g_settings.m_nDurationUnitDivisor;

	if((g_settings.m_pszDTMFFormat)&&(strlen(g_settings.m_pszDTMFFormat)>0))
	{
		_snprintf(pszDTMF, MAX_PATH, g_settings.m_pszDTMFFormat, nDur);
	}
	else
	{
		_snprintf(pszDTMF, MAX_PATH, "D%03d", nDur);
	}

	nDur = strlen(pszDTMF);
	if(nDur>0)
	{
		char* pch = (char*)malloc(nDur+1);
		if(pch)
		{
			strcpy(pch, pszDTMF);
			return pch;
		}
	}
	return NULL;
}


int CSerialTonesData::IdealAddTimeAndDuration(int nStart, int nDuration)
{

	if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
	{
//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp

		if(g_data.m_ucTimeCodeBits&0x80) // drop frame...
		{
	return m_timeconv.DFMStoNDFMS( m_timeconv.NDFMStoDFMS(nStart) + m_timeconv.NDFMStoDFMS(nDuration) );
		}
		else
		{
	return nStart + nDuration;
		}

	}
	else  // system time
	{
		if(g_settings.m_bDF) // drop frame...
		{
	return m_timeconv.DFMStoNDFMS( m_timeconv.NDFMStoDFMS(nStart) + m_timeconv.NDFMStoDFMS(nDuration) );
		}
		else
		{
	return nStart + nDuration;
		}
	}

}

/*
int CSerialTonesData::TestPattern(char* pszValue, char* pszPattern)
{
	if((pszValue)&&(pszPattern)&&(strlen(pszPattern)>0)&&(m_pdbOutput)&&(m_pdbOutputConn))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		// have to encode quotes.
		char* pchEncodedValue   = m_pdbOutput->EncodeQuotes(pszValue);
		char* pchEncodedPattern = m_pdbOutput->EncodeQuotes(pszPattern);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select case when '%s' like '%s' then 'YES' else 'NO' end as col1",
						(pchEncodedValue?pchEncodedValue:pszValue),
						(pchEncodedPattern?pchEncodedPattern:pszPattern)
				);
		
		if(pchEncodedValue) free(pchEncodedValue);
		if(pchEncodedPattern) free(pchEncodedPattern);


if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_PATTERN|SERIALTONES_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:TestPattern", "Checking event %s against %s with %s", pszValue, pszPattern, szSQL); // Sleep(250); //(Dispatch message)
}

		EnterCriticalSection(&m_critOutputSQL);
		CRecordset* prs = m_pdbOutput->Retrieve(m_pdbOutputConn, szSQL, errorstring);
		if(prs)
		{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "Got Destinations");  Sleep(250); //(Dispatch message)
			if(!prs->IsEOF())
			{
				CString szTemp;
				prs->GetFieldValue("col1", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					int nReturn = SERIALTONES_PATTERN_NO_MATCH;
					if(szTemp.CompareNoCase("YES")==0)
					{
if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_PATTERN)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:TestPattern", "Query returned YES"); // Sleep(250); //(Dispatch message)
}

						nReturn = SERIALTONES_PATTERN_MATCH;
						
					}
					prs->Close();
					delete prs;

					LeaveCriticalSection(&m_critOutputSQL);
					
					return nReturn;
				}
			}
			prs->Close();
			delete prs;
		}
		else
		{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "SerialTones:TestPattern", "ERROR: %s", errorstring); // Sleep(250); //(Dispatch message)
		}
		LeaveCriticalSection(&m_critOutputSQL);
	}
if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_PATTERN)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:TestPattern", "ERROR"); // Sleep(250); //(Dispatch message)
}
	return SERIALTONES_PATTERN_ERROR;
}
*/




/*



int CSerialTonesData::RefreshTabulatorEvents( CTabulatorEventArray* pEvents, char* pszToken )
{
	if((pEvents)&&(pszToken))
	{
		EnterCriticalSection(&(pEvents->m_critEvents));

		pEvents->DeleteEvents();
		int e=0;
		int n=0;
		bool bFound=false;
		bool bAllocated=false;
		while(e<g_data.m_pEvents->m_nEvents)
		{
			if(g_data.m_pEvents->m_ppEvents[e]->m_szName.Compare(pszToken)==0)
			{
				bFound=true;
				if(g_data.m_pEvents->m_nLastEventMod!=pEvents->m_nLastEventMod)
				{
					pEvents->m_nLastEventMod = g_data.m_pEvents->m_nLastEventMod;
				}
			}
			else
			{
				if(bFound) break; // just stop it there. - must be contiguous
			}

			if(bFound)
			{
				if(!bAllocated)
				{
					pEvents->m_ppEvents = new CTabulatorEvent*[g_settings.m_nTriggerBuffer];
					bAllocated = true; // check for errors someday
				}
				pEvents->m_ppEvents[pEvents->m_nEvents] = new CTabulatorEvent;
				if(pEvents->m_ppEvents[pEvents->m_nEvents])
				{
					n++;
					*(pEvents->m_ppEvents[pEvents->m_nEvents]) = *(g_data.m_pEvents->m_ppEvents[e]);
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szName = g_data.m_pEvents->m_ppEvents[e]->m_szName;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szHost = g_data.m_pEvents->m_ppEvents[e]->m_szHost;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szSource = g_data.m_pEvents->m_ppEvents[e]->m_szSource;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szScene = g_data.m_pEvents->m_ppEvents[e]->m_szScene;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szIdentifier = g_data.m_pEvents->m_ppEvents[e]->m_szIdentifier;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szValue = g_data.m_pEvents->m_ppEvents[e]->m_szValue;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szParamDependencies = g_data.m_pEvents->m_ppEvents[e]->m_szParamDependencies;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szDestModule = g_data.m_pEvents->m_ppEvents[e]->m_szDestModule;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szLayer = g_data.m_pEvents->m_ppEvents[e]->m_szLayer;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_EVENTS)) 
g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName, "in event %02d: %s, %.3f: %s:%s:%s",
		 pEvents->m_nEvents,
		 pEvents->m_ppEvents[pEvents->m_nEvents]->m_szName,
		 pEvents->m_ppEvents[pEvents->m_nEvents]->m_dblTriggerTime,
		 pEvents->m_ppEvents[pEvents->m_nEvents]->m_szScene,
		 pEvents->m_ppEvents[pEvents->m_nEvents]->m_szIdentifier,
		 pEvents->m_ppEvents[pEvents->m_nEvents]->m_szValue
		 );// Sleep(100); //(Dispatch message)
					pEvents->m_nEvents++;
				}
			}

			e++;
		}
		LeaveCriticalSection(&(pEvents->m_critEvents));


		return n;
	}
	return TABULATOR_ERROR;
}
*/


int CSerialTonesData::InitDLLData(DLLthread_t* pData)
{
	if(pData)
	{
		InitializeCriticalSection(&pData->m_crit);
		EnterCriticalSection(&pData->m_crit);
		pData->bKillThread=true;
		pData->bThreadStarted=false;
		pData->nThreadState=0;  //generic
		pData->nThreadControl=0;  //generic
		pData->m_dblLastUpdate=-1.0;
		pData->pszThreadName=NULL;
		strcpy(pData->pszThreadStateText, "");//256 chars max

	// URLs may use format specifiers
		pData->pszBaseURL=NULL;
		pData->pszInfoURL=NULL;
		pData->pszConfirmURL=NULL;

		pData->pszSeriesParamName=NULL;    //  %S
		pData->pszProgramParamName=NULL;   //  %P
		pData->pszEpisodeParamName=NULL;   //  %E

		pData->pszSeriesID=NULL;    //  %s
		pData->pszProgramID=NULL;   //  %p
		pData->pszEpisodeID=NULL;   //  %e

		// some generic parameters
		pData->pszToDateParamName=NULL;				//  %t
		pData->pszFromDateParamName=NULL;			//  %f
		pData->pszNowParamName=NULL;					//  %n
		pData->pszBatchParamName=NULL;				//  %b
		pData->pszFromIDParamName=NULL;				//  %m  // start id
		pData->pszGetIDParamName=NULL;				//  %i

		pData->pszLastID=NULL;  // last ID gotten   %l

		pData->nBatchParam=250;
		pData->nShortIntervalMS=0;
		pData->nLongIntervalMS=5000;
		pData->bDirParamStyle=true;  // true is baseurl/paramname/paramvalue/  false is baseurl?paramname=paramvalue

		_ftime(&pData->timebTick); // the time check inside the thread
		_ftime(&pData->timebLastPullTick); // the last time data pulled inside the thread
		pData->nIDrateAvg=-1;
		pData->nIDrateMax=-1;
		pData->nIDrateMin=-1;
		pData->nNumFailures=0;
		pData->nNumSuccess=0;

		pData->pszHost=NULL;
		pData->nPort=80;
		pData->nAuxValue=-1;

		LeaveCriticalSection(&pData->m_crit);
		return CLIENT_SUCCESS;
	}

	return CLIENT_ERROR;
}

/*
int CSerialTonesData::GetSerialTones(char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT uid, name, description, type, status, duration, start_tc, stop_tc, \
count, paused_duration, paused_count, last_started, last_stopped, action_type, action_data FROM %s ORDER BY uid",  //HARDCODE
			((g_settings.m_pszSerialTones)&&(strlen(g_settings.m_pszSerialTones)))?g_settings.m_pszSerialTones:"SerialTones"
			); 
		
/*		

create table SerialTones (uid  int identity(1,1), name varchar(64), description varchar(256), type int, status int, duration int, start_tc int, stop_tc int, \
count int, paused_duration int, paused_count int, last_started decimal(20,3), last_stopped decimal(20,3), action_type int, action_data varchar(2048))  
   


	int m_nItemID;

	unsigned long m_ulType;  // up count, down count
	unsigned long m_ulStatus;

	unsigned long m_ulDuration; // if a downcounter, the total number of milliseconds to count down.  If an upcounter, it should be the maximum count, or zero if none.
	unsigned long m_ulStartTC; // if a scheduled autodata, start from this millisecond offset.
	unsigned long m_ulStopTC; // the time code at finish

	unsigned long m_ulCount; // if a downcounter, the total number of milliseconds left.  If an upcounter, elapsed duration
	unsigned long m_ulPausedDuration; // elapsed milliseconds while paused.
	unsigned long m_ulTimesPaused;
	
	_timeb m_timeLastStarted;
	_timeb m_timeLastStopped;

	char* m_pszName;
	char* m_pszDescription;

	bool m_bKillSerialTones;
	bool m_bSerialTonesStarted;

	// need something here for "action" to do when the autodata finishes.
	unsigned long m_ulActionType;  // up count, down count
	char* m_pszActionData;
	* /

if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetSerialTones", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "GetSerialTones");  Sleep(250); //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "Got Destinations");  Sleep(250); //(Dispatch message)

			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = CLIENT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetSerialTones", "index %d", nIndex); // Sleep(250); //(Dispatch message)
}
				CString szName="";
				CString szDesc="";
				CString szActionData="";
				CString szTemp;

				int nItemID = -1;
				bool bItemFound = false;
				bool bFlagsFound = false;
				bool bFound = false;


				unsigned long ulType;  // up count, down count
//				unsigned long ulStatus;

//				unsigned long ulDuration; // if a downcounter, the total number of milliseconds to count down.  If an upcounter, it should be the maximum count, or zero if none.
//				unsigned long ulStartTC; // if a scheduled autodata, start from this millisecond offset.
//				unsigned long ulStopTC; // the time code at finish

//				unsigned long ulCount; // if a downcounter, the total number of milliseconds left.  If an upcounter, elapsed duration
//				unsigned long ulPausedDuration; // elapsed milliseconds while paused.
//				unsigned long ulTimesPaused;
				
//				_timeb timeLastStarted;
//				_timeb timeLastStopped;

				unsigned long ulActionType;  // up count, down count

				unsigned long ulFlags = 0; // used for volatile status


				try
				{
					prs->GetFieldValue("uid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bItemFound = true;
						nItemID = atoi(szTemp);
					}
					prs->GetFieldValue("name", szName);//HARDCODE
					szName.TrimLeft(); szName.TrimRight();

					prs->GetFieldValue("description", szDesc);//HARDCODE
					szDesc.TrimLeft(); szDesc.TrimRight();

					prs->GetFieldValue("type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulType = atol(szTemp);
					}

/*

	// these need to be set on the fly
					prs->GetFieldValue("status", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulStatus = atol(szTemp);
					}

					prs->GetFieldValue("duration", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulDuration = atol(szTemp);
					}


					prs->GetFieldValue("start_tc", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulStartTC = atol(szTemp);
					}

					prs->GetFieldValue("stop_tc", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulStopTC = atol(szTemp);
					}
* /

					prs->GetFieldValue("count", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulCount = atol(szTemp);
					}
					prs->GetFieldValue("paused_duration", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulPausedDuration = atol(szTemp);
					}
					prs->GetFieldValue("paused_count", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulTimesPaused = atol(szTemp);
					}
					prs->GetFieldValue("last_started", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						timeLastStarted.time = atol(szTemp);
						timeLastStarted.millitm = ((unsigned short)((atof(szTemp) - ((double)(timeLastStarted.time))) *1000.0));
					}
					prs->GetFieldValue("last_stopped", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						timeLastStopped.time = atol(szTemp);
						timeLastStopped.millitm = ((unsigned short)((atof(szTemp) - ((double)(timeLastStarted.time))) *1000.0));
					}
* /
					prs->GetFieldValue("action_type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulActionType = atol(szTemp);
					}
					prs->GetFieldValue("action_data", szDesc);//HARDCODE
					szDesc.TrimLeft(); szDesc.TrimRight();


					bFlagsFound = true; // made it thru the whole record
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
* /
if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetSerialTones", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL); // Sleep(250); //(Dispatch message)
}
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetSerialTones", "Retrieve: Caught exception: out of memory\n%s", szSQL); // Sleep(250); //(Dispatch message)
}
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetSerialTones", "Retrieve: Caught other exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)
}
					}
					e->Delete();
				} 
				catch( ... )
				{
					if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception.\n%s", szSQL);

if(g_settings.m_ulDebug&(SERIALTONES_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:GetSerialTones", "Retrieve: Caught exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)
}

				}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "Got Destination: %s (0x%08x)", szName, ulFlags);  Sleep(250); //(Dispatch message)


//////////////////////////////////////////////////////////////////////////////
// temporarily going to just ignore all this, this is only going to be called once at startup and that is it.
// so this will not get hit


/*
				if(0) //(m_ppSerialTones)&&(m_nSerialTones))
				{
					int nTemp=0;
					while(nTemp<m_nSerialTones)
					{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "c2");  Sleep(250); //(Dispatch message)
						if((m_ppSerialTones[nTemp]))//&&(m_ppSerialTones[nTemp]->m_pszName))
						{
							// changing the follwing to unique on dest id.
				//			if((szName.GetLength()>0)&&(szName.CompareNoCase(m_ppSerialTones[nTemp]->m_pszName)==0))
							if(m_ppSerialTones[nTemp]->m_nItemID == nItemID)
							{
								bFound = true;
								bool bRefresh = false;

								if(
										(szDesc.GetLength()>0)
									&&((m_ppSerialTones[nTemp]->m_pszDesc==NULL)||(szDesc.CompareNoCase(m_ppSerialTones[nTemp]->m_pszDesc)))
									)
								{
									if(m_ppSerialTones[nTemp]->m_pszDesc) free(m_ppSerialTones[nTemp]->m_pszDesc);

									m_ppSerialTones[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(m_ppSerialTones[nTemp]->m_pszDesc) sprintf(m_ppSerialTones[nTemp]->m_pszDesc, szDesc);
								}

								if(
										(szName.GetLength()>0)
									&&((m_ppSerialTones[nTemp]->m_pszName==NULL)||(szName.CompareNoCase(m_ppSerialTones[nTemp]->m_pszName)))
									)
								{
									if(m_ppSerialTones[nTemp]->m_pszName) free(m_ppSerialTones[nTemp]->m_pszName);

									m_ppSerialTones[nTemp]->m_pszName = (char*)malloc(szName.GetLength()+1); 
									if(m_ppSerialTones[nTemp]->m_pszName) sprintf(m_ppSerialTones[nTemp]->m_pszName, szName);
								}

								if(bItemFound) m_ppSerialTones[nTemp]->m_nItemID = nItemID;
								m_ppSerialTones[nTemp]->m_ulType = ulType;

								if(dblDiskspaceThreshold>0.0) m_ppSerialTones[nTemp]->m_dblDiskPercent = dblDiskspaceThreshold;
//								m_ppSerialTones[nTemp]->m_dblDiskKBFree = dblDiskspaceFree;
//								m_ppSerialTones[nTemp]->m_dblDiskKBTotal = dblDiskspaceTotal;

//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "c3");  Sleep(250); //(Dispatch message)

								if(
										((bFlagsFound)&&(!(ulFlags&CLIENT_FLAG_ENABLED))&&((m_ppSerialTones[nTemp]->m_ulFlags)&CLIENT_FLAG_ENABLED))
									)
								{
									m_ppSerialTones[nTemp]->m_bKillMonThread = true;

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
										m_ppSerialTones[nTemp]->m_pszDesc?m_ppSerialTones[nTemp]->m_pszDesc:m_ppSerialTones[nTemp]->m_pszName);  
									g_ptabmsgr->DM(MSG_ICONINFO, NULL, "SerialTones:destination_change", errorstring);  Sleep(20);  //(Dispatch message)
									m_ppSerialTones[nTemp]->m_bKillConnThread = true;
									m_ppSerialTones[nTemp]->m_ulStatus = CLIENT_STATUS_NOTCON;
									if(m_ppSerialTones[nTemp]->m_socket != NULL)
									{
										m_ppSerialTones[nTemp]->m_net.CloseConnection(m_ppSerialTones[nTemp]->m_socket); // re-establish
									}
									m_ppSerialTones[nTemp]->m_socket = NULL;

								}

								if(
										((bFlagsFound)&&(ulFlags&CLIENT_FLAG_ENABLED)&&(!((m_ppSerialTones[nTemp]->m_ulFlags)&CLIENT_FLAG_ENABLED)))
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating %s...", 
//										m_ppSerialTones[nTemp]->m_pszClientName?m_ppSerialTones[nTemp]->m_pszClientName:"Libretto",  
										m_ppSerialTones[nTemp]->m_pszDesc?m_ppSerialTones[nTemp]->m_pszDesc:m_ppSerialTones[nTemp]->m_pszName);  
									g_ptabmsgr->DM(MSG_ICONINFO, NULL, "SerialTones:destination_change", errorstring); //  Sleep(20);  //(Dispatch message)
//									m_ppSerialTones[nTemp]->m_pDlg->OnButtonConnect();
									//**** if connect set following status


									if((m_ppSerialTones[nTemp]->m_socket == NULL)&&(!m_ppSerialTones[nTemp]->m_bConnThreadStarted))
									{
										m_ppSerialTones[nTemp]->m_ulStatus = CLIENT_STATUS_CONN;
										if(_beginthread(LibrettoAsynchConnThread, 0, (void*)(m_ppSerialTones[nTemp]))==-1)
										{
										//error.

										//**MSG
										}
										else
										{
											int nWait = 0;
											while((m_ppSerialTones[nTemp]->m_ulStatus == CLIENT_STATUS_CONN)&&(nWait<1000))  // 0.5 seconds?
											{
												nWait++;
												Sleep(1);
											}
										}
									}

									if(!m_ppSerialTones[nTemp]->m_bMonThreadStarted)
									{
										m_ppSerialTones[nTemp]->m_bKillMonThread = false;
										if(_beginthread(LibrettoConnMonitorThread, 0, (void*)(m_ppSerialTones[nTemp]))==-1)
										{
										//error.
									g_ptabmsgr->DM(MSG_ICONERROR, NULL, "SerialTones:destination_change", "Error starting connection monitor for %s", 
										m_ppSerialTones[nTemp]->m_pszDesc?m_ppSerialTones[nTemp]->m_pszDesc:m_ppSerialTones[nTemp]->m_pszName); //  Sleep(20);  //(Dispatch message)

										//**MSG
										}
									}


									if(!m_ppSerialTones[nTemp]->m_bCommandQueueThreadStarted)
									{
										m_ppSerialTones[nTemp]->m_bKillCommandQueueThread = false;
										if(_beginthread(LibrettoCommandQueueThread, 0, (void*)(m_ppSerialTones[nTemp]))==-1)
										{
										//error.
									g_ptabmsgr->DM(MSG_ICONERROR, NULL, "SerialTones:destination_change", "Error starting command queue for %s", 
										m_ppSerialTones[nTemp]->m_pszDesc?m_ppSerialTones[nTemp]->m_pszDesc:m_ppSerialTones[nTemp]->m_pszName); //  Sleep(20);  //(Dispatch message)

										//**MSG
										}
									}


									

/*
									if(m_ppSerialTones[nTemp]->m_socket == NULL)
									{
										m_ppSerialTones[nTemp]->m_net.OpenConnection(m_ppSerialTones[nTemp]->m_pszName, g_settings.m_nPort, &m_ppSerialTones[nTemp]->m_socket); // re-establish
										char tnbuffer[32]; sprintf(tnbuffer, "\\\\%c%c", 13,10);
										m_ppSerialTones[nTemp]->SendTelnetCommand(tnbuffer, 2);
										unsigned char* pucBuffer = NULL;
										unsigned long ulBufferLen = 256;
										m_ppSerialTones[nTemp]->m_net.GetLine(&pucBuffer, &ulBufferLen, m_ppSerialTones[nTemp]->m_socket);
										if(pucBuffer) free(pucBuffer);
									}
* /
									bRefresh = true;
//									m_ppSerialTones[nTemp]->m_bKillConnThread = false;
	//								if(_beginthread(LibrettoConnectionThread, 0, (void*)m_ppSerialTones[nTemp])<0)
	//								{
										//error.

										//**MSG
							
										
		//							}
									//**** should check return value....

									m_ppSerialTones[nTemp]->m_ulStatus = CLIENT_STATUS_CONN;
								}

								if(bFlagsFound) m_ppSerialTones[nTemp]->m_ulFlags = ulFlags|CLIENT_FLAG_FOUND;
								else m_ppSerialTones[nTemp]->m_ulFlags |= CLIENT_FLAG_FOUND;

								if(bRefresh)m_ppSerialTones[nTemp]->m_ulFlags |= CLIENT_FLAG_REFRESH;

							}
						}
						nTemp++;
					}
				}
// temporarily going to just ignore all this, this is only going to be called once at startup and that is it.
// so this (above) will not get hit
//////////////////////////////////////////////////////////////////////////////

* /



				if((!bFound)&&(szName.GetLength()>0)) // have to add.
				{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "adding %s", szName);  Sleep(250); //(Dispatch message)
					CSerialTonesItem* pscno = new CSerialTonesItem;
					if(pscno)
					{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "new obj for %s", szName);  Sleep(250); //(Dispatch message)
						CSerialTonesItem** ppObj = new CSerialTonesItem*[m_nSerialTones+1];
						if(ppObj)
						{
				
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "new array for %s", szName);  Sleep(250); //(Dispatch message)
							int o=0;
							if((m_ppSerialTones)&&(m_nSerialTones>0))
							{
								while(o<m_nSerialTones)
								{
									ppObj[o] = m_ppSerialTones[o];
									o++;
								}
								delete [] m_ppSerialTones;

							}
							ppObj[o] = pscno;
							m_ppSerialTones = ppObj;
							m_nSerialTones++;

							ppObj[o]->m_pszName = (char*)malloc(szName.GetLength()+1); 
							if(ppObj[o]->m_pszName) sprintf(ppObj[o]->m_pszName, "%s", szName);

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}
							if(bItemFound) ppObj[o]->m_nItemID = nItemID;
							ppObj[o]->m_ulType = ulType;

							if(szActionData.GetLength()>0)
							{
								ppObj[o]->m_pszActionData = (char*)malloc(szActionData.GetLength()+1); 
								if(ppObj[o]->m_pszActionData) sprintf(ppObj[o]->m_pszActionData, "%s", szActionData);
							}
							ppObj[o]->m_ulActionType = ulActionType;



//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", " added flags: %s (0x%08x) %d", szName, ppObj[o]->m_ulFlags, ((ppObj[o]->m_ulFlags)&CLIENT_FLAG_ENABLED));  Sleep(250); //(Dispatch message)


							ppObj[o]->m_ulFlags |= SERIALTONES_FLAG_FOUND;
						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			// and insert media list refresh into the queue for all new destinatons.
			nIndex = 0;
			while(nIndex<m_nSerialTones)
			{
				if((m_ppSerialTones)&&(m_ppSerialTones[nIndex]))
				{

					if((m_ppSerialTones[nIndex]->m_ulFlags)&SERIALTONES_FLAG_FOUND)
					{
						(m_ppSerialTones[nIndex]->m_ulFlags) &= ~SERIALTONES_FLAG_FOUND;

						nIndex++;
					}
					else
					{
						if(m_ppSerialTones[nIndex]->m_bSerialTonesStarted)
						{
							int nWait =  clock()+1500;
							m_ppSerialTones[nIndex]->m_bKillSerialTones = true;

							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
									m_ppSerialTones[nIndex]->m_pszDesc?m_ppSerialTones[nIndex]->m_pszDesc:m_ppSerialTones[nIndex]->m_pszName);  
								if(g_ptabmsgr)  g_ptabmsgr->DM(MSG_ICONINFO, NULL, "SerialTones:remove_autodata", errorstring);  // Sleep(20);  //(Dispatch message)
//									m_ppSerialTones[nTemp]->m_pDlg->OnDisconnect();


							//**** disconnect
//								while(m_ppSerialTones[nIndex]->m_bConnThreadStarted) Sleep(1);

							while(
										 (
										   (m_ppSerialTones[nIndex]->m_bSerialTonesStarted)
										 )
										&&
										 (clock()<nWait)
									 )
							{
								Sleep(1);
							}

							m_nSerialTones--;

							int nTemp=nIndex;
							while(nTemp<m_nSerialTones)
							{
								m_ppSerialTones[nTemp]=m_ppSerialTones[nTemp+1];
								nTemp++;
							}
							m_ppSerialTones[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
	}
//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "SerialTones:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return CLIENT_ERROR;
}


int CSerialTonesData::FindSerialTones(char* pszSerialTonesName)
{
	if((pszSerialTonesName)&&(m_ppSerialTones))
	{
		int nTemp=0;
		while(nTemp<m_nSerialTones)
		{
			if(m_ppSerialTones[nTemp])
			{
				if(m_ppSerialTones[nTemp]->m_pszName)
				{
					if(stricmp(m_ppSerialTones[nTemp]->m_pszName, pszSerialTonesName)==0) return nTemp;
				}
			}
			nTemp++;
		}
	}
	return CLIENT_ERROR;

}
*/

/*

// the way we are doing this, we have to limit the tabulator events to having max of only ONE
// stream message per event.  Because there is one override being sent in....
// at least for now, this is how it needs to be.
int CSerialTonesData::PlayEvent(CTabulatorEventArray** ppEvent, CMessageOverrideValues* pOverrides, char* pszInfo)
{
	CTabulatorEventArray* pEvent = NULL;
	char szTickerSource[MAX_PATH]; sprintf(szTickerSource, "%s_Output", g_settings.m_pszInternalAppName);
	if(ppEvent) pEvent = *ppEvent;
	if((m_pdbOutput)&&(m_pdbOutputConn)&&(pEvent)&&(pEvent->m_ppEvents)&&(pEvent->m_nEvents>0))
	{
		char szCommandSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];
		char TabulatorToken[MAX_PATH];
		int nTabulatorTokenLen=0;
		if((g_settings.m_pszTabulatorModule)&&(strlen(g_settings.m_pszTabulatorModule)))
		{
			_snprintf(TabulatorToken, MAX_PATH, "%s:", g_settings.m_pszTabulatorModule);
		}
		else
		{
			strcpy(TabulatorToken, "Tabulator:");
		}	
		
		nTabulatorTokenLen = strlen(TabulatorToken);
	
		int i=0;
		_timeb timebBeginTick;
		_timeb timebTriggerTick;
		_timeb timebNowTick;
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Entering"); //(Dispatch message)
		EnterCriticalSection(&pEvent->m_critEvents);
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Entered"); //(Dispatch message)
		_ftime(&timebBeginTick);
		CBufferUtil bu;
//			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "PlayEvent event"); //(Dispatch message)
Sleep(250);
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "PlayEvent event %s with %d actions", pEvent->m_ppEvents[0]->m_szScene, pEvent->m_nEvents); //(Dispatch message)

		while((i<pEvent->m_nEvents)&&(g_dlldata.thread)&&(g_dlldata.thread[0])&&(!g_dlldata.thread[0]->bKillThread)&&(!m_bDoingOppCancel))
		{
			if((pEvent->m_ppEvents)&&(pEvent->m_ppEvents[i]))
			{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "-X- text event %02d %s %.0f", i, pEvent->m_ppEvents[i]->m_szIdentifier, pEvent->m_ppEvents[i]->m_dblTriggerTime); //(Dispatch message)
				// delay by the offest amount;
				timebTriggerTick.time = timebBeginTick.time+((int)(pEvent->m_ppEvents[i]->m_dblTriggerTime))/1000;
				timebTriggerTick.millitm = (unsigned short)(timebBeginTick.millitm + ((int)(pEvent->m_ppEvents[i]->m_dblTriggerTime))%1000);
				while(timebTriggerTick.millitm>999)
				{
					timebTriggerTick.time++;
					timebTriggerTick.millitm-=1000;
				}
				_ftime(&timebNowTick);

				// assemble the string to send:

				if(pEvent->m_ppEvents[i]->m_szIdentifier.GetLength())
				{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "--- event %02d %s with %s", i, pEvent->m_ppEvents[i]->m_szIdentifier, pEvent->m_ppEvents[i]->m_szValue); //(Dispatch message)

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "pchEncodedLocal: %s", pchEncodedLocal);  Sleep(50);//(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "want to send %s", proj);  Sleep(50);//(Dispatch message)

					CString szII = "";
					CString szReadableII = "";
					bool bTabulatorEvent = false;
					if(pEvent->m_ppEvents[i]->m_nType==0)  //anim
					{
/*
						switch(pEvent->m_ppEvents[i]->m_nDestType)
						{
/*						case CX_DESTTYPE_VDS_WATERCOOLER://           9000  // watercooler!
							{
							} break;
* /
						case CX_DESTTYPE_MIRANDA_IS2://					2001  // Imagestore 2
						case CX_DESTTYPE_MIRANDA_INT://					2002  // Intuition
						case CX_DESTTYPE_MIRANDA_IS300://				2003  // Imagestore 300
						case CX_DESTTYPE_MIRANDA_HDIS://				2101  // Imagestore HD
						case CX_DESTTYPE_MIRANDA_HDINT://       2102  // Intuition HD
						case CX_DESTTYPE_MIRANDA_HDIS300://     2103  // Imagestore 300 HD
							{
* /								// no anims supported yet...old comment.  NOW:

								// OK, need to deal with Load (Load a file into a layer), Play (Fade up)  and Clear (Fade Down)
								//  could use CUT up and down, but for this we can just make FADE be immediate, so just use fade.
								// NOT using fade to black etc, using fade keyer, because we want background to pass.

								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Load")==0)
								{
									// layer needs to be a %1x representation of the layer number  (we have it as a string)
									szII.Format("R0%s%s",pEvent->m_ppEvents[i]->m_szLayer, pEvent->m_ppEvents[i]->m_szSource);  //using source, not scene, since IS2 only dealas with filenames, there are no scenes
//												szII.Format("R0%s%s",pEvent->m_ppEvents[i]->m_szLayer, pEvent->m_ppEvents[i]->m_szScene);
								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Play")==0)
								{
									//Layer needs to be a %d representation of the layer number (we have it as a string)
									szII.Format("1%s 1",pEvent->m_ppEvents[i]->m_szLayer);
								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Clear")==0)
								{
									//Layer needs to be a %d representation of the layer number (we have it as a string)
									szII.Format("1%s 0",pEvent->m_ppEvents[i]->m_szLayer);
								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Command")==0)
								{
									//the whole command must be coded in, exactly as it is to be sent to the Miranda
									// can be used for anything, fade transition settings, easytext commands, squeezy, etc
									szII.Format("%s",pEvent->m_ppEvents[i]->m_szValue);
								}

	//						} break;
	//					}

						// else not supported!

if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_TRIGGER)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "II is now: %s", szII); //(Dispatch message)
					} // if(pEvent->m_ppEvents[i]->m_nType==0)  //anim was true
					else  // export
					{

if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_TRIGGER)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "--- parse %02d %s with %s", i, pEvent->m_ppEvents[i]->m_szIdentifier, pEvent->m_ppEvents[i]->m_szValue); //(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Type is export, with dest type: %d", pEvent->m_ppEvents[i]->m_nDestType);  Sleep(50);//(Dispatch message)

						// searching for the tabulator token.
						
						if(stricmp(TabulatorToken, pEvent->m_ppEvents[i]->m_szIdentifier.Left(nTabulatorTokenLen).GetBuffer(0))==0)
						{
							bTabulatorEvent = true;  // !

							// just regular assemble
							if(pEvent->m_ppEvents[i]->m_szValue.GetLength()>0)
							{
								szII.Format("%s|%s", 
											pEvent->m_ppEvents[i]->m_szIdentifier.Mid(nTabulatorTokenLen), 
											pEvent->m_ppEvents[i]->m_szValue);
							}
							else
							{
								// could be just the token, no value, then you get no pipe.
								szII.Format("%s", pEvent->m_ppEvents[i]->m_szIdentifier.Mid(nTabulatorTokenLen));
							}
						}	// else nothing, not supported!						
					}//if(pEvent->m_ppEvents[i]->m_nType==0)  //anim was false
					
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "II is now: %s", szII);  Sleep(50);//(Dispatch message)

					int nLen = szII.GetLength();
					if(nLen>0)  //ignore blank stuff, failure
					{

if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_TRIGGER)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "waiting for trigger II: %s", szII); //(Dispatch message)

						char eventIDbuffer[256];
						_snprintf(eventIDbuffer, 256, "%s", pEvent->m_ppEvents[i]->m_szName);
						char* pchEncodedLocal = NULL;

						while((	(timebNowTick.time<timebTriggerTick.time)
									||((timebNowTick.time==timebTriggerTick.time)&&(timebNowTick.millitm<timebTriggerTick.millitm))
									)
									&&(g_dlldata.thread[0])&&(!g_dlldata.thread[0]->bKillThread)&&(!m_bDoingOppCancel))
						{
							Sleep(1);
							_ftime(&timebNowTick);
						}

						if(bTabulatorEvent)
						{

							if(g_data.m_socketTabulator)
							{
								CNetData data;
								data.m_ucType = (NET_TYPE_PROTOCOL1|NET_TYPE_KEEPOPEN|NET_SND_KEEPOPENLCL); // has data but no subcommand.
								data.m_ucCmd		= TABULATOR_CMD_PLUGINCALL;
								data.m_pucData	= (unsigned char*)malloc(nLen+1);
								if(data.m_pucData)
								{
									memcpy(data.m_pucData, szII.GetBuffer(0), nLen);
									data.m_pucData[nLen]=0;
									data.m_ulDataLen = nLen;
									data.m_ucType |= NET_TYPE_HASDATA;
								}

	g_data.m_bInCommand = TRUE;
								int nReturn = g_data.m_net.SendData(&data, g_data.m_socketTabulator, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, errorstring);
								
								if(nReturn<NET_SUCCESS)
								{
									// log it.
									g_data.m_net.CloseConnection(g_data.m_socketTabulator, errorstring);
									g_data.m_socketTabulator =  NULL;

									g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, eventIDbuffer, g_settings.m_pszInternalAppName, "Error %d sending to %s at %s:%d with: %s", 
										nReturn,
										g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
										g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
										g_settings.m_nTabulatorPort, 
										szII);
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, g_settings.m_pszInternalAppName, "Error %d (%s) sending to %s at %s:%d with: %s", 
						nReturn, errorstring,
						g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
						g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
						g_settings.m_nTabulatorPort, 
						szII);
								}
								else
								{
									g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, eventIDbuffer, g_settings.m_pszInternalAppName, "Successfully executed %s with return data: %02x,%02x %s (%d bytes data)", 
									szII,
									data.m_ucCmd,    // the command byte
									data.m_ucSubCmd,   // the subcommand byte
									(((data.m_pucData)&&(data.m_ulDataLen))?(char*)data.m_pucData:""),
									data.m_ulDataLen
									);

if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_TRIGGER)) 
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, g_settings.m_pszInternalAppName, "Action %s executed successfully with return data: %02x,%02x %s (%d bytes data)", 
								szII,
								data.m_ucCmd,    // the command byte
								data.m_ucSubCmd,   // the subcommand byte
								(((data.m_pucData)&&(data.m_ulDataLen))?(char*)data.m_pucData:""),
								data.m_ulDataLen

							); // Sleep(250); //(Dispatch message)
									// just send an ack
									g_data.m_net.SendData(NULL, g_data.m_socketTabulator, 5000, 0, NET_SND_CLNTACK, errorstring);
								}
	g_data.m_bInCommand = FALSE;

							}
							else
							{
								// can't send!

								g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, eventIDbuffer, g_settings.m_pszInternalAppName, "Error sending to %s at %s:%d with: %s", 
									g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
									g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
									g_settings.m_nTabulatorPort, 
									szII);

if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, g_settings.m_pszInternalAppName, "NULL socket error sending to %s at %s:%d with: %s", 
						g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
						g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
						g_settings.m_nTabulatorPort, 
						szII);
							}

							// send it!
						}
						else
						{
							pchEncodedLocal = m_pdbOutput->EncodeQuotes(szII);

							if((pchEncodedLocal)&&(strlen(pchEncodedLocal)>0))
							{

								if(
									  (g_data.m_nLastTime == timebNowTick.time)
									&&(g_data.m_nLastMillitm == timebNowTick.millitm)
									)
								{
									g_data.m_nMillitmIncr++;
									if(timebNowTick.millitm + g_data.m_nMillitmIncr > 999)
									{
										g_data.m_nTimeIncr++;
										g_data.m_nMillitmIncr -= 1000;
									}
								}
								else
								{
									if( 
										  (timebNowTick.time>g_data.m_nLastTime+g_data.m_nTimeIncr)
										||(
										    (timebNowTick.time==g_data.m_nLastTime+g_data.m_nTimeIncr)
										  &&(timebNowTick.millitm>g_data.m_nLastMillitm+g_data.m_nMillitmIncr)
											)
										)
									{
										// the last tick stuff has not overtaken the new now time, and otherwise would cause an ordering problem, so we can reset to zero
										g_data.m_nMillitmIncr = 0;
										g_data.m_nTimeIncr = 0;
									}
									else
									{
										// lapping did occur, so need to just adjust back
										// knowing that g_data.m_nLastTime and g_data.m_nLastMillitm are going to get rest to being equal to 
										// timebNowTick, we can calc where we can pick up the increment values from.

//										int nSec   = g_data.m_nLastTime + g_data.m_nTimeIncr - timebNowTick.time;
//										int nMilli = 1 + g_data.m_nLastMillitm + g_data.m_nMillitmIncr - timebNowTick.millitm; // don't forget to add one!

										g_data.m_nTimeIncr += (g_data.m_nLastTime - timebNowTick.time);
										g_data.m_nMillitmIncr += (1 + g_data.m_nLastMillitm - timebNowTick.millitm); // don't forget to add one!

										while(timebNowTick.millitm + g_data.m_nMillitmIncr < 0)
										{
											g_data.m_nTimeIncr--;
											g_data.m_nMillitmIncr += 1000;
										}
										while(timebNowTick.millitm + g_data.m_nMillitmIncr > 999)
										{
											g_data.m_nTimeIncr++;
											g_data.m_nMillitmIncr -= 1000;
										}
									}
								}

								_snprintf(szCommandSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(local, remote, action, host, timestamp, username) \
VALUES ('%s', '', %d, '%s', %d.%03d, 'sys')", //HARDCODE
															g_settings.m_pszModule?g_settings.m_pszModule:"Libretto",//"Libretto", //pEndObj->m_pszDBName?pEndObj->m_pszDBName:"Libretto",  // need to get this.
								//							pEndObj->m_pszQueue?pEndObj->m_pszQueue:"Queue",  // ok need to change this too!  argh! its not the endpoint object, that is the automation module....
															g_settings.m_pszQueue?g_settings.m_pszQueue:"Command_Queue",
															pchEncodedLocal?pchEncodedLocal:szII,
															256,
															g_settings.m_pszDrivenHost,
															timebNowTick.time + g_data.m_nTimeIncr,
															timebNowTick.millitm + g_data.m_nMillitmIncr
															);
	//	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Trigger SQL: %s", szCommandSQL); // Sleep(50); //(Dispatch message)
	EnterCriticalSection(&m_critOutputSQL);
								int nSQLReturn = m_pdbOutput->ExecuteSQL(m_pdbOutputConn, szCommandSQL, errorstring);
	LeaveCriticalSection(&m_critOutputSQL);
								if(nSQLReturn<DB_SUCCESS)
								{
							//**MSG
		g_ptabmsgr->DM(MSG_ICONHAND, NULL, szTickerSource, "ERROR %d executing SQL: %s", nSQLReturn, errorstring); // Sleep(50); //(Dispatch message)


									g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, eventIDbuffer, g_settings.m_pszInternalAppName, "Error %s sending data to device at %s: %s", nSQLReturn, g_settings.m_pszDrivenHost, g_settings.m_bReadableEncodedAsRun?szReadableII:szII);
								}
								else
								{
									g_data.m_nLastTime = timebNowTick.time;
									g_data.m_nLastMillitm = timebNowTick.millitm;


									g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, eventIDbuffer, g_settings.m_pszInternalAppName, "Sent data to device at %s: %s", g_settings.m_pszDrivenHost, g_settings.m_bReadableEncodedAsRun?szReadableII:szII);
		
							
		if((g_ptabmsgr)&&(g_settings.m_ulDebug&SERIALTONES_DEBUG_TRIGGER)) 
			{
				g_ptabmsgr->DM(MSG_ICONHAND, NULL, szTickerSource, "command %d sent [est time %d.%.03d][%d.%03d + trig %.0f][db %d.%03d]: %s", 
					pEvent->m_ppEvents[i]->m_nEventID,
					timebNowTick.time,
					timebNowTick.millitm,
					timebBeginTick.time,
					timebBeginTick.millitm,
					pEvent->m_ppEvents[i]->m_dblTriggerTime, 
					timebNowTick.time + g_data.m_nTimeIncr,
					timebNowTick.millitm + g_data.m_nMillitmIncr,
					szII); // Sleep(50);//(Dispatch message)
			}
								}
							
							} // if(pchEncodedLocal)&& strlen
							if(pchEncodedLocal) free(pchEncodedLocal);
						} // else from if bTabulator event.
					} // szII had nonzero length 
				}//if(pEvent->m_ppEvents[i]->m_szIdentifier.GetLength())
			}// if((pEvent->m_ppEvents)&&(pEvent->m_ppEvents[i]))
			if(g_dlldata.thread[0]->bKillThread) break;
			i++;
			
		} // while
		LeaveCriticalSection(&pEvent->m_critEvents);
//		m_bDoingOppCancel = false;
//		m_bDoingOpp=false;
		return CLIENT_SUCCESS;
	}
	else	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "PlayEvent event exited with no action [%d%d%d%d %d]", 
		(m_pdbOutput!=NULL), 
		(m_pdbOutputConn!=NULL),
		(pEvent!=NULL),
		(pEvent->m_ppEvents!=NULL),
		(pEvent->m_nEvents>0)
		); //(Dispatch message)

//	m_bDoingOppCancel = false;
//	m_bDoingOpp=false;
	return CLIENT_ERROR;
}

*/

