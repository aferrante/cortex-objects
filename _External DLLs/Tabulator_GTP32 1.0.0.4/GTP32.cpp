// GTP32.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "GTP32.h"
#include "../../../Cortex Objects/Tabulator/TabulatorDefines.h"
#include "../../../Cortex Objects/Tabulator/TabulatorData.h"
#include <process.h>
#include "../../../Common/MFC/ODBC/DBUtil.h"
#include "../../../Common/TXT/BufferUtil.h"
#include <objsafe.h>
#include <atlbase.h>
#include "../../../Cortex/3.0.4.5/CortexShared.h" // included to have xml messaging objects and other shared things

void InitThread(void* pvArgs);
void KillMonitorThread(void* pvArgs);


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

#define GTP32DLG_MINSIZEX 200
#define GTP32DLG_MINSIZEY 150



/////////////////////////////////////////////////////////////////////////////
// CGTP32App

BEGIN_MESSAGE_MAP(CGTP32App, CWinApp)
	//{{AFX_MSG_MAP(CGTP32App)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGTP32App construction

CGTP32App::CGTP32App()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	AfxInitRichEdit();
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CGTP32App object

CGTP32App				theApp;
CGTP32Core			g_core;
CGTP32Data			g_data;
CGTP32Settings	g_settings;
CMessager*			g_ptabmsgr = NULL;
DLLdata_t       g_dlldata;



int  CGTP32App::DLLCtrl(void** ppvoid, UINT nType)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
//	char szSQL[DB_SQLSTRING_MAXLEN];
//	char errorstring[MAX_MESSAGE_LENGTH];

//	char szFeedSource[MAX_PATH]; sprintf(szFeedSource, "%s_DataDownload", g_settings.m_pszInternalAppName);
//	char szTickerSource[MAX_PATH]; sprintf(szTickerSource, "%s_Ticker", g_settings.m_pszInternalAppName);
	int nReturn = CLIENT_SUCCESS;
	switch(nType)
	{
	case DLLCMD_MAINDLG_BEGIN://					0x00
		{
		} break;
	case DLLCMD_MAINDLG_END://						0x01
		{
		} break;
	case DLLCMD_SETTINGSTXT://						0x02
		{
			if(ppvoid) // we should be passing in the address of a char*.
			{
				if((g_settings.m_pszSettingsText)&&(strlen(g_settings.m_pszSettingsText)>0))
				{
					char* pch = (char*)malloc(strlen(g_settings.m_pszSettingsText)+1);
					if(pch)
					{
						strcpy(pch, g_settings.m_pszSettingsText);
						(*ppvoid)= pch;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
				}
				else
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_DOSETTINGSDLG://					0x03
		{
			// takes a NULL pointer.
/*
			if(g_settings.m_bHasDlg)
			{
				g_settings.DoModal();
			}
			else
*/
			{
				// just refresh
			EnterCriticalSection(&g_settings.m_crit);

				if(g_settings.Settings(true)==CLIENT_SUCCESS)
					nReturn = CLIENT_REFRESH;
				else nReturn = CLIENT_ERROR;

			LeaveCriticalSection(&g_settings.m_crit);
			}
		} break;
	case DLLCMD_GETABOUTTXT://						0x04
		{
			if(ppvoid) // we should be passing in the address of a char*.
			{
				if((g_settings.m_pszAboutText)&&(strlen(g_settings.m_pszAboutText)>0))
				{
					char* pch = (char*)malloc(strlen(g_settings.m_pszAboutText)+1);
					if(pch)
					{
						strcpy(pch, g_settings.m_pszAboutText);
						(*ppvoid)= pch;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
				}
				else
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_GETAPPNAME://							0x05
		{
			if(ppvoid) // we should be passing in the address of a char*.
			{
				if((g_settings.m_pszAppName)&&(strlen(g_settings.m_pszAppName)>0))
				{
					char* pch = (char*)malloc(strlen(g_settings.m_pszAppName)+1);
					if(pch)
					{
						strcpy(pch, g_settings.m_pszAppName);
						(*ppvoid)= pch;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
				}
				else
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
			
		} break;
	case DLLCMD_SETDISPATCHER://					0x06
		{
			if (*ppvoid)
			{
				g_ptabmsgr = (CMessager*)*ppvoid;
//				CString szMsg; szMsg.Format("%08x dispatcher",g_pmd);
//				AfxMessageBox(szMsg);
//				AfxMessageBox(AfxGetApp()->m_pszAppName);
//				AfxMessageBox(AfxGetApp()->m_pszExeName);

/*
					if(g_settings.m_bIsServer)
					{

						if(g_ptabmsgr) g_ptabmsgr->AddDestination(
							MSG_DESTTYPE_LOG, 
							(((g_settings.m_pszAsRunDestination)&&(strlen(g_settings.m_pszAsRunDestination)))?g_settings.m_pszAsRunDestination:"asrun"), 
							((g_settings.m_pszAsRunFilename!=NULL)?g_settings.m_pszAsRunFilename:"gtp32_asrun|YD||1|")
							);
					}
*/
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "GTP32", "Version %s (%s %s) obtained dispatcher", VERSION_STRING, __DATE__, __TIME__);//  Sleep(250); //(Dispatch message)
			}
			else
			{
				nReturn = CLIENT_ERROR;
			}  

		} break;
	case DLLCMD_GETMINX://							0x07
		{
			if(ppvoid) // we should be passing in the address of an int.
			{
				(*ppvoid) = (void*)GTP32DLG_MINSIZEX;
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_GETMINY://							0x08
		{
			if(ppvoid) // we should be passing in the address of an int.
			{
				(*ppvoid) = (void*)GTP32DLG_MINSIZEY;
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_GETDATAOBJ://							0x09  // gets a pointer to the main data object
		{
			if(ppvoid) // we should be passing in the address of an data object.
			{
				(*ppvoid) = (void*)(&g_dlldata);
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_CALLFUNC://								0x0a  // call a function....  
		{
			if(ppvoid) // we should be passing in the address of an char buffer.
			{
				char* pch = (char*)(*ppvoid);
				if((pch)&&(strlen(pch)))
				{
					// format is func name|arg|arg|arg  where if arg is a string, it is pipe encoded.
					char* pchDelim = strchr(pch, '|');
					if(pchDelim) *pchDelim = 0; //temporary null term.

		//			AfxMessageBox(pch);

					
					if(strcmp(pch, "Server_Get_Status")==0)
					{
						if(1) // success
						{
								// status return shall be:

							_timeb timebTick;
							_ftime(&timebTick);

							CString  szData;
							szData.Format("%d.%03d", timebTick.time, timebTick.millitm);

							// time|datalastupdate|datastate|time|tickerlastupdate|tickerstate

							//assemble some data, then formulate a response and send

							char* pchReturn =  (char*)malloc(szData.GetLength()+1);
							if(pchReturn)
							{
								sprintf(pchReturn, "%s", szData);
								(*ppvoid) = pchReturn;
							}
						}
						else
						{
							(*ppvoid)= NULL;
							nReturn = CLIENT_ERROR;
						}
					}
					else
					if(strcmp(pch, "Server_Refresh_Setting")==0)
					{
						(*ppvoid)= NULL;
			EnterCriticalSection(&g_settings.m_crit);
						g_settings.Settings(true);						
			LeaveCriticalSection(&g_settings.m_crit);
					}

					else
					if(strcmp(pch, "Server_Refresh_Table")==0)
					{
						nReturn = CLIENT_ERROR;
						if(pchDelim)
						{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker Commit {%s}", pchDelim+1);// Sleep(100); //(Dispatch message)
							CSafeBufferUtil sbu;
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_SINGLEDELIM);
							if((pch)&&(strlen(pch)))
							{
								// table name. // find out which one to refresh
								if(stricmp(pch, "ALL")==0)
								{
	EnterCriticalSection(&g_data.m_critButtons);
									g_data.GetButtons();
									g_data.GetManualEvents();
	LeaveCriticalSection(&g_data.m_critButtons);
									nReturn = CLIENT_SUCCESS;
								}
								else
								if((g_settings.m_pszButtons)&&(stricmp(pch, g_settings.m_pszButtons)==0))
								{
	EnterCriticalSection(&g_data.m_critButtons);
									g_data.GetButtons();
	LeaveCriticalSection(&g_data.m_critButtons);
									nReturn = CLIENT_SUCCESS;
								}
								else
								if((g_settings.m_pszManualEvents)&&(stricmp(pch, g_settings.m_pszManualEvents)==0))
								{
	EnterCriticalSection(&g_data.m_critButtons);
									g_data.GetManualEvents();
	LeaveCriticalSection(&g_data.m_critButtons);
									nReturn = CLIENT_SUCCESS;
								}
							}
						}
						(*ppvoid)= NULL;

					}

					else
					if(strcmp(pch, "Button_Press")==0)
					{
						nReturn = CLIENT_ERROR;
						if(pchDelim)
						{
if(g_settings.m_ulDebug&(GTP32_DEBUG_CALLFUNC))
{ 
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "GTP32", "Button Press [%s]", pchDelim+1);// Sleep(500); //(Dispatch message)
}
							CSafeBufferUtil sbu;
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_SINGLEDELIM);
							if(pch)
							{
								int nIndex = atoi(pch); // make sure it exists!
								if((nIndex>0)&&(nIndex<87))  // 86 is the actual max button number
								{
//if(g_settings.m_ulDebug&(GTP32_DEBUG_CALLFUNC))
//{ can't use the logging this way, need to "press" buttons that are > the 32 button array. fix later.
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "GTP32", "Button %d flags 0x%08x", nIndex, g_data.m_Buttons[nIndex-1].m_ulFlags);// Sleep(500); //(Dispatch message)
//}
//									if(g_data.m_Buttons[nIndex-1].m_ulFlags&GTP32_BUTTON_ENABLED)  // also, enabled is just for UI, let's always make plug-in calls work.
									{
										nReturn = g_data.SendGTP32Command(GTP32_REQUEST_TYPE_KEYPRESS, nIndex, 1); // type like button press or GPO, nValue will be 0 or 1, basically on or off.

										if(nReturn>=GTP32_SUCCESS)
										{
											Sleep(g_settings.m_nMinKeypressDelay); // min button press delay

											nReturn = g_data.SendGTP32Command(GTP32_REQUEST_TYPE_KEYPRESS, nIndex, 0); // type like button press or GPO, nValue will be 0 or 1, basically on or off.

										}
									}
								}
							}
						}
						(*ppvoid)= NULL;

					}
					else
					if(strcmp(pch, "GPO_Set")==0)
					{
						nReturn = CLIENT_ERROR;
						if(pchDelim)
						{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker Commit {%s}", pchDelim+1);// Sleep(100); //(Dispatch message)
							CSafeBufferUtil sbu;
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_SINGLEDELIM);
							if(pch)
							{
								int nIndex = atoi(pch); // make sure it exists!
								if((nIndex>0)&&(nIndex<33))
								{
									pch = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
									if(pch)
									{
										int nValue = atoi(pch);
										// send the Button press and receive the response.  Then send the button un-press and receive that response

										nReturn = g_data.SendGTP32Command(GTP32_REQUEST_TYPE_GPO, nIndex, nValue); // type like button press or GPO, nValue will be 0 or 1, basically on or off.
									}
								}
							}
						}
						(*ppvoid)= NULL;

					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
					
					if(pchDelim) *pchDelim = '|'; //replace pipe.

				}
				else 
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_SETEVENTS://						0x0b
		{
			if(ppvoid)
			{
//				g_data.m_pEvents = (CTabulatorEventArray*)ppvoid;
			}
		} break;
	case DLLCMD_GETDBSETTINGS://						0x0c
		{
			EnterCriticalSection(&g_settings.m_crit);
			g_settings.GetFromDatabase();
			LeaveCriticalSection(&g_settings.m_crit);
		} break;
	case DLLCMD_SETGLOBALSTATE://						0x0d
		{
/*			if(ppvoid)
			{
				char statetoken[MAX_PATH];
				strncpy(statetoken, (char*)ppvoid, MAX_PATH);
				int n=0;
				while(n<g_dlldata.nNumThreads)
				{
					if((g_dlldata.thread)&&(g_dlldata.thread[n]))
					{
						if(stricmp(statetoken, "resume")==0)
						{
							switch(n)
							{
							case GTP32_DATA:// 0
								g_dlldata.thread[GTP32_DATA]->nThreadState &= ~GTP32_DATA_PAUSED; break;
							case GTP32_TICKER:// 1
								g_dlldata.thread[GTP32_TICKER]->nThreadState &= ~GTP32_TICKER_PAUSED; break;
							default:
							case GTP32_UI:// 2
								break;
							}
						}
						else
						if(stricmp(statetoken, "suspend")==0)
						{
							switch(n)
							{
							case GTP32_DATA:// 0
								g_dlldata.thread[GTP32_DATA]->nThreadState |= GTP32_DATA_PAUSED; break;
							case GTP32_TICKER:// 1
								g_dlldata.thread[GTP32_TICKER]->nThreadState |= GTP32_TICKER_PAUSED; break;
							default:
							case GTP32_UI:// 2
								break;
							}
						}
					}
					n++;
				}
			}
			else nReturn = CLIENT_ERROR;
*/

		} break;

		

	default: nReturn = CLIENT_UNKNOWN; break;
	}
	return nReturn;
}

BOOL CGTP32App::InitInstance() 
{
#if _WIN32_WINNT >= 0x0400
		HRESULT hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		HRESULT hRes = CoInitialize(NULL);
#endif
//	AfxMessageBox(m_pszAppName);
//	AfxMessageBox(m_pszExeName);
//	AfxMessageBox("Foo");

	strcpy(m_szSettingsFilename, SETTINGS_FILENAME);

	if((m_pszAppName)&&(strlen(m_pszAppName)>0)) sprintf(m_szSettingsFilename, "%s.ini", m_pszAppName);

	InitializeCriticalSection(&g_dlldata.m_crit);
	EnterCriticalSection(&g_dlldata.m_crit);
//	g_dlldata.thread = new DLLthread_t*[GTP32_THREADS]; // 3 threads, data download and ticker engine, and UI conn

	g_dlldata.nNumThreads=0;
	g_dlldata.thread = new DLLthread_t*[1]; // 1 thread, timecode thread
	
	if(g_dlldata.thread)
	{
		g_dlldata.nNumThreads=1;
		g_dlldata.thread[0] = new DLLthread_t;
		g_data.InitDLLData(g_dlldata.thread[0]);
		g_data.InitDLLData(g_dlldata.thread[0]);

		g_dlldata.thread[0]->bKillThread=false;

	}
		
 	LeaveCriticalSection(&g_dlldata.m_crit);

//	g_dlldata.pDlg = g_pdlg; // if there is a dialog....

//	AfxMessageBox("Foo1");
			EnterCriticalSection(&g_settings.m_crit);
//			if(bIsServer)
//			{
//	g_settings.Settings(true); // need to get the Setting for the Settings table name! // NO do this in GetFromDatabase by rearraging the initial if
	g_settings.GetFromDatabase(); // does Settings(true) first
//			}
//			else
//			{
//	g_settings.Settings(true); // need to get the thread settings...
//			}
			LeaveCriticalSection(&g_settings.m_crit);
	

		// create the db connection...


//		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];
		g_data.m_pdb = new CDBUtil;
		g_data.m_pdbConn = g_data.m_pdb->CreateNewConnection(g_settings.m_pszDSN, g_settings.m_pszUser, g_settings.m_pszPW);
		if(g_data.m_pdbConn)
		{
			if(g_data.m_pdb->ConnectDatabase(g_data.m_pdbConn, errorstring)<DB_SUCCESS)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "GTP32_Settings:database_connect", errorstring);  //(Dispatch message)
			}
			else
			{

			}
		}
		else
		{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "GTP32_Settings:database_connect", errorstring);  //(Dispatch message)
		}


	if(_beginthread(InitThread, 0, (void*)NULL)==-1)
	{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "GTP32_Settings:database_connect", "Error starting init thread");  //(Dispatch message)
	}


//	AfxMessageBox("Foo2");
	return CWinApp::InitInstance();
}




int CGTP32App::ExitInstance() 
{
	g_ptabmsgr = NULL;  // it's already dead. most likely

	g_data.StopServer();
	g_data.Disconnect();

	EnterCriticalSection(&g_settings.m_crit);
	g_settings.Settings(false);
	LeaveCriticalSection(&g_settings.m_crit);

  CoUninitialize(); //XML
	
	return CWinApp::ExitInstance();
}


void InitThread(void* pvArgs)
{
	Sleep(100);

	EnterCriticalSection(&g_data.m_critButtons);
									g_data.GetButtons();
	LeaveCriticalSection(&g_data.m_critButtons);

/*
	_timeb timeStart;
	_ftime(&timeStart);

	if((g_settings.m_nStartupButton>0)&&(g_settings.m_nStartupButton<33)&&(g_settings.m_nStartupDelay))
	{
		timeStart.time += g_settings.m_nStartupDelay/1000;
		timeStart.millitm += g_settings.m_nStartupDelay%1000;
		while(timeStart.millitm>999)
		{
			timeStart.millitm -= 1000;
			timeStart.time++;
		}
	}
*/		
	g_data.Connect();
	g_data.StartServer();


		if(_beginthread(KillMonitorThread, 0, (void*)(NULL))==-1)
		{
		//error.
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "Error starting kill monitor thread");//   Sleep(250);//(Dispatch message)
		//**MSG
		}


}


void KillMonitorThread(void* pvArgs)
{
	if((g_dlldata.thread)&&(g_dlldata.thread[0]))
	{
		g_dlldata.thread[0]->bThreadStarted = true;
		g_dlldata.thread[0]->nThreadState |= 1; ///started
	}

	char errorstring[MAX_MESSAGE_LENGTH];
	bool bSocketReport = false;

	Sleep(100); // let Tabulator seed the messenger.

	_timeb timeStart;

	int nClock = -1; 
	int nLastReady = -1; 
	int nLastQuery = -1;
	bool bStartupDone = false;

	while(!g_dlldata.thread[0]->bKillThread)
	{

		// monitor connection to Tabulator
		if((g_data.m_socketTabulator == NULL)&&(!g_dlldata.thread[0]->bKillThread))
		{
			// Open connection
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing control client to %s:%d", g_settings.m_pszTabulatorHost, g_settings.m_nTabulatorPort); 
			
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName, errorstring );  //(Dispatch message)

			while(g_data.m_bInCommand) Sleep(1);
			g_data.m_bInCommand = TRUE;
			if(g_data.m_net.OpenConnection(g_settings.m_pszTabulatorHost, g_settings.m_nTabulatorPort, &g_data.m_socketTabulator, 
				5000, 5000, errorstring)<NET_SUCCESS)
			{
				g_data.m_bInCommand = FALSE;

				if(!bSocketReport)
				{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL,  g_settings.m_pszInternalAppName, errorstring );  //(Dispatch message)
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error connecting Tabulator at %s:%d. %s", g_settings.m_pszTabulatorHost, g_settings.m_nTabulatorPort, errorstring); 
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, g_settings.m_pszInternalAppName, errorstring);  //(Dispatch message)
					
					g_data.SendMsg(CX_SENDMSG_ERROR, g_settings.m_pszInternalAppName, errorstring);
					bSocketReport = true;
				}
				g_data.m_socketTabulator = NULL;
			}
			else
			{
				g_data.m_bInCommand = FALSE;

				bSocketReport = false;
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Connected to Tabulator at %s:%d", g_settings.m_pszTabulatorHost, g_settings.m_nTabulatorPort); 
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName, errorstring );  //(Dispatch message)
				g_data.SendMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, errorstring);
			}

		}

		if((g_data.m_socketTabulator != NULL)&&(!g_dlldata.thread[0]->bKillThread))  // check for disconnection
		{
			if(!g_data.m_bInCommand)
			{
				timeval tv;
				tv.tv_sec = 0; 
				tv.tv_usec = 500;  // timeout value
				int nNumSockets;
				fd_set fds;

				FD_ZERO(&fds);  // Zero this out each time
				FD_SET(g_data.m_socketTabulator, &fds);

				nNumSockets = select(0, &fds, NULL, NULL, &tv);
				if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
				{
					nClock = -1;
				}
				else
				if(
						(nNumSockets==0) // 0 = timed out, -1 = error
					||(!(FD_ISSET(g_data.m_socketTabulator, &fds)))
					) 
				{ 
					nClock = -1;
				}
				else // there is recv data.
				{ // wait some delay.
					if(nClock<0)
					{
						nClock = clock();
					}
					else
					{
						if((clock() > nClock+1000)&&(!g_data.m_bInCommand))
						{
							g_data.m_bInCommand = TRUE;
							// one second and nothing - could be a disconnect - just reinit, this is unsolicited data anyway
		g_data.m_net.CloseConnection(g_data.m_socketTabulator);
		g_data.m_socketTabulator = NULL;
							g_data.m_bInCommand = FALSE;
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Lost connection from Tabulator at %s:%d", g_settings.m_pszTabulatorHost, g_settings.m_nTabulatorPort); 
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName, errorstring );  //(Dispatch message)
if(!g_dlldata.thread[0]->bKillThread)
{
		g_data.SendMsg(CX_SENDMSG_ERROR, g_settings.m_pszInternalAppName, errorstring);
}

						}
					}
				}
			}


			if(
				  (g_settings.m_nStartupButton>0)
				&&(g_settings.m_nStartupButton<33)
				&&(g_settings.m_nStartupDelay>=0)
				&&(!g_dlldata.thread[0]->bKillThread)
				&&(g_data.m_bButtonsInitialized) // has to be initialized to start the counter.
				)
			{
				if((nLastReady<0)&&(nLastQuery + 1000 < clock())) // once per second max
				{
					// Query the automation module for readiness

					char pszToken[MAX_PATH];
_snprintf(pszToken, MAX_PATH-1, "%s|%s", g_settings.m_pszTabulatorPlugInModule, g_settings.m_pszTabulatorCommandQueryReady);
unsigned char* pucDataBuffer = (unsigned char*)pszToken;
int nTabReturn = g_data.SendTabulatorCommand(&pucDataBuffer, "Query", true);
												if(nTabReturn>=GTP32_SUCCESS)
												{
													if((pucDataBuffer)&&(pucDataBuffer != (unsigned char*)pszToken))
													{
														free(pucDataBuffer);
													}

													_ftime(&timeStart);

													timeStart.time += g_settings.m_nStartupDelay/1000;
													timeStart.millitm += g_settings.m_nStartupDelay%1000;
													while(timeStart.millitm>999)
													{
														timeStart.millitm -= 1000;
														timeStart.time++;
													}

													nLastReady = clock();
													nLastQuery = clock();
												}
												else
												{
													// not ready yet.
													nLastQuery = clock();

												}
				



				}
			}
		}


		if(
			  (nLastReady>0)
			&&(nLastQuery + 5000 < clock()) // once per 5 seconds max (or make this configurable later)
			&&(!g_data.m_bInGTP32Command)
			)
		{
			// send a heartbeat to check the connection.to the GTP32
			nLastQuery = clock();
	
		}


		if(!bStartupDone)
		{
			if(
					(g_data.m_s != NULL) // must have conn to gtp32
				&&(nLastReady>0) // must be ready
				&&(g_settings.m_nStartupButton>0)
				&&(g_settings.m_nStartupButton<33) // must be in range
				&&(g_settings.m_nStartupDelay>=0)  // cant be turned off
				&&(!g_dlldata.thread[0]->bKillThread) // not killing
				)
			{
				// press a button.....

if(g_settings.m_ulDebug&(GTP32_DEBUG_CALLFUNC|GTP32_DEBUG_INIT))
{ 
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "GTP32", "Button Press [%d] flags 0x%08x", g_settings.m_nStartupButton, g_data.m_Buttons[g_settings.m_nStartupButton-1].m_ulFlags);// Sleep(500); //(Dispatch message)
}
				int nReturn = g_data.SendGTP32Command(GTP32_REQUEST_TYPE_KEYPRESS, g_settings.m_nStartupButton, 1); // type like button press or GPO, nValue will be 0 or 1, basically on or off.

				if(nReturn>=GTP32_SUCCESS)
				{
					Sleep(g_settings.m_nMinKeypressDelay); // min button press delay

					nReturn = g_data.SendGTP32Command(GTP32_REQUEST_TYPE_KEYPRESS, g_settings.m_nStartupButton, 0); // type like button press or GPO, nValue will be 0 or 1, basically on or off.

					bStartupDone=true; // done!
				}
			}
		}



		Sleep(10);
	}

	if(g_data.m_socketTabulator)
	{
		// I could send the "disconnect" command to Tabulator so it does not throw an error.... 

		CNetData data;
		// just disconnect
		data.m_ucCmd = TABULATOR_CMD_HELLO;
		data.m_ucType = NET_TYPE_PROTOCOL1; // has data but no subcommand.
		g_data.m_bInCommand = TRUE;
		int n = g_data.m_net.SendData(&data, g_data.m_socketTabulator, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT);//|NET_SND_NO_RX);
		if(n >= NET_SUCCESS)
		{
			g_data.m_net.SendData(NULL, g_data.m_socketTabulator, 5000, 0, NET_SND_CLNTACK, NULL);
		}
		g_data.m_net.CloseConnection(g_data.m_socketTabulator);
		g_data.m_socketTabulator = NULL;
		g_data.m_bInCommand = FALSE;

		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Disconnected from Tabulator at %s:%d", g_settings.m_pszTabulatorHost, g_settings.m_nTabulatorPort); 
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName, errorstring );  //(Dispatch message)
		g_data.SendMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, errorstring);
	}

	g_data.StopServer();
	g_data.Disconnect();

	Sleep(100); //let messages get there.
	g_ptabmsgr = NULL;

	if((g_dlldata.thread)&&(g_dlldata.thread[0]))
	{
		g_dlldata.thread[0]->bThreadStarted = false;
		g_dlldata.thread[0]->nThreadState &= ~1; ///started
	}

}

