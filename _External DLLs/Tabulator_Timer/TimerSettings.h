#if !defined(AFX_TimerSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_)
#define AFX_TimerSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TimerSettings.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTimerSettings 

class CTimerSettings //: public CDialog
{
// Construction
public:
	CTimerSettings(/*CWnd* pParent = NULL*/);   // standard constructor
	virtual ~CTimerSettings();   // standard destructor
/*
// Dialog Data
	//{{AFX_DATA(CTimerSettings)
	enum { IDD = IDD_DIALOG_SETTINGS };
	BOOL	m_bAutoconnect;
	BOOL	m_bTickerPlayOnce;
	CString	m_szHost;
	CString	m_szBaseURL;
	int		m_nBatch;
	int		m_nPort;
	int		m_nPollIntervalMS;
	CString	m_szProgramID;
	int		m_nRequestIntervalMS;
	int		m_nTickerPort;
	CString	m_szTickerHost;
	BOOL	m_bSmut_ReplaceCurses;
	int		m_nDoneCount;
	//}}AFX_DATA

	// internal
	bool m_bHasDlg;
	bool m_bIsServer;
	bool m_bObtainedFromServer;
	bool m_bFromDialog;

	int m_nMaxMessages;

	// UI
	COLORREF m_crCommitted;
*/
	CRITICAL_SECTION m_crit;

		// database stuff
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;

//	bool m_bUseFeed; // use the data download thread
//	char* m_pszFeed;  // the Feed table name
//	char* m_pszTicker;  // the Ticker table name
	char* m_pszMessages;  // the Messages table name
	char* m_pszAsRun;  // the As-run table name
	char* m_pszSettings;  // the Settings table name
	char* m_pszTimers;  // the Timers table name

	bool m_bMillisecondMessaging;			// use millisecond resolution for messages and asrun


//	char* m_pszDestinationHost;  // do we want to direct drive the host ever?  or always shoot to Libretto or other module.? (still need the host either way but need to know what drive mode)

//	need module info, libretto command queue etc.
/*
	char* m_pszModule;  // the module name (Libretto)
	char* m_pszQueue;  // the module's queue table name (Queue of Libretto.dbo.Queue)


	char* m_pszInEvent;  // the event identifier of the transition in (Server_Ticker_In) command
	char* m_pszOutEvent;  // the event identifier of the transition out (Server_Ticker_Out) command
	char* m_pszTextEvent;  // the event identifier of the text message event
*/

//	char* m_pszAsRunFilename; 
//	char* m_pszAsRunDestination;

/*
	char* m_pszTickerMessagePreamble; 

	char* m_pszTickerBackplateName;   // for external graphics engine use.
	char* m_pszTickerLogoName;    // for external graphics engine use.
	int m_nTickerBackplateID; 
	int m_nTickerLogoID; 
	int m_nTickerTextID; 

	int m_nTickerEngineType; 
	int m_nTriggerBuffer; // number of items to hold in a buffer for triggering
	int m_nDefaultDwellTime;

	char* m_pszMessageToken; 
	char* m_pszAux1Token; 
	char* m_pszAux2Token; 

	bool m_bAutoPurgeExpired;
	bool m_bAutoPurgeExceeded;
	int m_nAutoPurgeExpiredAfterMins;
	int m_nAutoPurgeExceededAfterMins;
	bool m_bAutostartTicker;
	bool m_bAutostartData;
	bool m_bUseLocalTime;  // or use unixtime

	int m_nGraphicsHostType;

	//Harris Icon specific
	int m_nLayoutSalvoFormat; // 0= 4 digit numerical, 1=8 digit numerical, 2 = strings  
*/
	//exposed
	char* m_pszAppName;
	char* m_pszInternalAppName;
	char* m_pszAboutText;
	char* m_pszSettingsText;

	int GetFromDatabase(char* pszInfo=NULL);  //only get.  Interface publishes

	int Settings(bool bRead);

//	CString RemoteSettingsToString();
//	int StringToRemoteSettings(CString szSettings);

	unsigned long m_ulDebug;  // prints out debug statements that & with this.
	bool m_bUseMidnight86MsCompensation;			// use 86.4 ms compensation factor over midnight


	bool m_bUseTimeCode;			// use adrienne timecode
	int m_nFrameRate;   // for time code addresses NTSC=30, PAL=25
	bool m_bDF;  // Drop frame


	int m_nMinimumTimerIntervalMS;   // minimum time interval between count updates to SQL server - have to manage them.... too many too fast can make the SQL server fall over
	int m_nCriticalThresholdMS;   // number of MS before zero count down (or final count up) where we do not apply m_nMinimumTimerIntervalMS.   for accurate granularity


/*
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTimerSettings)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTimerSettings)
	afx_msg void OnButtonTestfeed();
	virtual void OnOK();
	afx_msg void OnButtonConnect();
	virtual BOOL OnInitDialog();
	afx_msg void OnCheckPlayonce();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

*/
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TimerSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_)
