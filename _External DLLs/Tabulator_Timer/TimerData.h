// TimerData.h: interface for the CTimerData and CTimerMessage classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TimerDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_)
#define AFX_TimerDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../../../Common/LAN/NetUtil.h"
#include "../../../Cortex Objects/Tabulator/TabulatorDefines.h"
#include "../../../Cortex Objects/Tabulator/TabulatorData.h"
#include "../../../Common/MFC/ODBC/DBUtil.h"
#include "../../../Common/TIME/TimeConvert.h"


#define SENDMSG_SENDER_MAXLEN 64
#define SENDMSG_MESSAGE_MAXLEN 512

class CTimerItem  
{
public:
	CTimerItem();
	virtual ~CTimerItem();

	int m_nItemID;

	unsigned long m_ulType;  // up count, down count
	unsigned long m_ulStatus;

	unsigned long m_ulDuration; // if a downcounter, the total number of milliseconds to count down.  If an upcounter, it should be the maximum count, or zero if none.
	unsigned long m_ulStartTC; // if a scheduled timer, start from this millisecond offset.
	unsigned long m_ulStopTC; // the time code at finish, stop here if an upcounter and specified.

	int m_nCount; // if a downcounter, the total number of milliseconds left.  If an upcounter, elapsed duration
	unsigned long m_ulPausedDuration; // elapsed milliseconds while paused.
	unsigned long m_ulTimesPaused;
	
	_timeb m_timeLastStarted;
	_timeb m_timeLastStopped;

	char* m_pszName;
	char* m_pszDesc;

	bool m_bKillTimer;
	bool m_bTimerStarted;

	// need something here for "action" to do when the timer finishes.
	unsigned long m_ulActionType;  // up count, down count
	char* m_pszActionData;

	unsigned long m_ulFlags; // used for volatile status

	CTimeConvert m_timeconv; //time converison utility
	CNetUtil m_net;  // for an action involving network

	int PublishItem(unsigned long ulFlags=TIMER_UPDATE_ALL);

};



class CTimerData  
{
public:
	CTimerData();
	virtual ~CTimerData();

	// remove pipes
	CString Encode(CString szText);
	// put back pipes
	CString Decode(CString szText);

	int InitDLLData(DLLthread_t* pData);

	CString XMLTextNodeValue(IXMLDOMNodePtr pNode);

	int SendMsg(int nType, char* pszSender, char* pszMessage, ...);
//	int SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...);
	int SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...);


//	SOCKET m_socket;
//	CRITICAL_SECTION m_critClientSocket;

	CNetUtil m_net;
	CDBUtil m_dbu;

//	CRITICAL_SECTION m_critMsg;
	CRITICAL_SECTION m_critSQL;

//	CTabulatorEventArray* m_pEvents;
//	CTabulatorEventArray* m_pInEvents;
//	CTabulatorEventArray* m_pOutEvents;
//	CTabulatorEventArray* m_pTextEvents;
//	CRITICAL_SECTION m_critEventsSettings;

	CTimerItem** m_ppTimers;
	int m_nTimers;

	CDBUtil*  m_pdb;
	CDBconn* m_pdbConn;

//	CRITICAL_SECTION m_critTransact;

	CRITICAL_SECTION m_critTimeCode;
	unsigned long m_ulTimeCode;
	unsigned char m_ucTimeCodeBits;
	unsigned long m_ulTimeCodeElapsed;
	bool m_bTimeCodeThreadStarted;
	bool m_bTimeCodeThreadKill;

	int GetTimers(char* pszInfo=NULL);
	int FindTimer(char* pszTimerName);
};


#endif // !defined(AFX_TimerDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_)
