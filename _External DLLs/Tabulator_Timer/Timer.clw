; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
ClassCount=4
Class1=CTimerApp
LastClass=CTimerDlg
NewFileInclude2=#include "Timer.h"
ResourceCount=0
NewFileInclude1=#include "stdafx.h"
Class2=CTimerSettings
LastTemplate=CDialog
Class3=CTimerDlg
Class4=CTimerTestFeed

[CLS:CTimerApp]
Type=0
HeaderFile=Timer.h
ImplementationFile=Timer.cpp
Filter=N
LastObject=CTimerApp
BaseClass=CWinApp
VirtualFilter=AC

[CLS:CTimerSettings]
Type=0
HeaderFile=TimerSettings.h
ImplementationFile=TimerSettings.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_CHECK_PLAYONCE
VirtualFilter=dWC

[CLS:CTimerDlg]
Type=0
HeaderFile=TimerDlg.h
ImplementationFile=TimerDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_CHECK_CYCLE

[CLS:CTimerTestFeed]
Type=0
HeaderFile=TimerTestFeed.h
ImplementationFile=TimerTestFeed.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CTimerTestFeed

