// Timer.h : main header file for the Timer DLL
//

#if !defined(AFX_Timer_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
#define AFX_Timer_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#import "msxml3.dll" named_guids 
using namespace MSXML2;

#include "../../../Applications/Generic/Client/ClientDefines.h"
#include "../../../Common/MSG/Messager.h"
#include "../../../Common/TXT/FileUtil.h"

//#include "..\..\..\..\Common\MFC\Inet\Inet.h"
//#include "..\..\..\..\Common\API\Miranda\IS2Core.h"  // for IS2 util functions

#define REMOVE_CLIENTSERVER  // for web-based UI only


#define SETTINGS_FILENAME "timer.ini"
#define VERSION_STRING "1.0.0.4"




//#define TIMER_TYPE_MASK   0x00000008 // don't need (yet)
#define TIMER_TYPE_DOWN						0x00000000  //default
#define TIMER_TYPE_UP							0x00000001  


#define TIMER_STATUS_NONE					0x00000000
#define TIMER_STATUS_STARTED			0x00000001  // set when started, even if not counting yet.
#define TIMER_STATUS_PAUSED				0x00000002  // otherwise, either counting or stopped
#define TIMER_STATUS_FINAL				0x00000004  // set when finished counting.
#define TIMER_STATUS_STOPPED			0x00000008  // set when stopped before finished counting.
#define TIMER_STATUS_COUNTING			0x00000010  // otherwise, pending if started
#define TIMER_STATUS_ACTIONOK			0x00000100  // the action was completed
#define TIMER_STATUS_ACTIONERROR	0x00000020  // error attempting the action.

#define TIMER_ACTION_TYPE_NONE		0x00000000  // nothing
#define TIMER_ACTION_TYPE_SQL		  0x00000001  // run a SQL query
#define TIMER_ACTION_TYPE_CXNET		0x00000002  // send a set of commands and chars in the cortex binary protocol on a network connection
#define TIMER_ACTION_TYPE_INET		0x00000003  // send a set of plain old chars on a network connection


// from cortex.h but static so just define here.
//send message type
#define CX_SENDMSG_ERROR   0
#define CX_SENDMSG_INFO	   1
#define CX_SENDMSG_NOTE	   2


#define TIMER_DEBUG_SQL				0x0001
#define TIMER_DEBUG_ACTIONS   0x0002
#define TIMER_DEBUG_SET			  0x0004



#define TIMER_FLAG_FOUND 0x00000010

#define TIMER_UPDATE_ALL					0x00000000
#define TIMER_UPDATE_COUNTONLY		0x00000001
#define TIMER_UPDATE_STATUSONLY		0x00000002



#include "TimerCore.h"
#include "TimerData.h"
#include "TimerSettings.h"


/////////////////////////////////////////////////////////////////////////////
// CTimerApp
// See Timer.cpp for the implementation of this class
//

class CTimerApp : public CWinApp
{
public:
	CTimerApp();
	int  DLLCtrl(void** ppvoid, UINT nType);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTimerApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	char m_szSettingsFilename[MAX_PATH];

	//{{AFX_MSG(CTimerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Timer_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
