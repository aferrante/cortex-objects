// TimerCore.cpp: implementation of the CTimerCore class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Timer.h"
#include "TimerCore.h"
#include <process.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


extern CTimerData					g_data;
extern CTimerSettings			g_settings;
extern CTimerCore					g_core;
extern CMessager*					g_ptabmsgr;
extern DLLdata_t      g_dlldata;



void TimeCodeDataThread(void* pvArgs);
void TimerThread(void* pvArgs);
void TimerWaitThread(void* pvArgs);




//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTimerCore::CTimerCore()
{

}

CTimerCore::~CTimerCore()
{

}


BOOL CTimerCore::IsPointInWnd(CPoint point, CWnd* pWnd) 
{
	CRect rcRect;
	pWnd->GetWindowRect(&rcRect);
	if(pWnd)
		return IsPointInRect( point,  rcRect); 
	else
		return FALSE;
}

BOOL CTimerCore::IsPointInRect(CPoint point, CRect rcWindowRect) 
{
	if(	(point.x>rcWindowRect.left)&&
			(point.x<rcWindowRect.right)&&
			(point.y>rcWindowRect.top)&&
			(point.y<rcWindowRect.bottom)	)
		return TRUE;
	else
		return FALSE;
}

// This function will encode a line of text for display
// under CAL.  Call this BEFORE adding any escape 
// sequences (such as [f 1] to change the font to font 1)
CString CTimerCore::CALEncodeBrackets(CString szString)
{
  int k;
	char ch;
  for (k=0; k<szString.GetLength(); k++)
  {
		ch = szString.GetAt(k);
    if (( ch == '[')||( ch == '\\'))
    {
      szString = szString.Left(k) + "\\" + 
        szString.Right(szString.GetLength() - k);
      k++;
    }
  }
  return szString;
}



void TimeCodeDataThread(void* pvArgs)
{
	CTimerData* p = &(g_data);
	
	p->m_bTimeCodeThreadStarted=true;
	g_dlldata.thread[0]->nThreadState |= 0x10; ///started TC
	SetThreadPriority(GetCurrentThread(), REALTIME_PRIORITY_CLASS);

	BOOL bNewInput = TRUE;
//	BOOL bAttemptSync = FALSE;  // dont even need.

	int nLastSleep = -1;
	int nLastValidTime = -1;
	int nValidLoops = 0;
	unsigned long ulLastTC=0xffffffff;
	int nLastTCTime = -1;
	int nNumTCRepeats = 0;
	int nLastSync = 0;
//	SYSTEMTIME SystemTime;  // for jam sync the clock

	while((!p->m_bTimeCodeThreadKill)&&(!g_dlldata.thread[0]->bKillThread))
	{
		unsigned long tick = clock();
		p->m_ucTimeCodeBits=0x80; //default to DF
		if(g_core.m_tc.m_hConnection == INVALID_HANDLE_VALUE)
		{
	EnterCriticalSection(&p->m_critTimeCode);
							p->m_ulTimeCode = 0xffffffff;
							p->m_ulTimeCodeElapsed = 0;
	LeaveCriticalSection(&p->m_critTimeCode);

//			p->SetWindowText(" Adrienne LTC/RDR: no device");
			// connect.
			g_core.m_tc.ConnectToCard();

			if(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE)
			{
//				p->SetWindowText(" Adrienne LTC/RDR: connected!");

				g_core.m_tc.GetBoardCapabilitesCode();
				if(!(g_core.m_tc.m_ucBoardCapabilitesCode & 0x10)) // no capabilities
				{
					bNewInput = TRUE;
//					bAttemptSync = FALSE; // disengage
//					p->GetDlgItem(IDC_STATIC_TEXT)->SetWindowText("Sync disengaged!  LTC not installed");
//					p->SetWindowText("Adrienne LTC/RDR: no LTC RDR");
					g_core.m_tc.DisconnectFromCard();  // nothing else to do.
					Sleep(300);
				}
				else
				{
//					p->SetWindowText(" Adrienne LTC/RDR: LTC installed");
					nLastValidTime = clock(); // to reset timing
					nLastTCTime = clock(); // to reset timing
				}
			}
		}
		else
		{
		
			//connected, so just grab status and time if status allows.
			int nBoardCheck = clock();
			g_core.m_tc.GetBoardInputsAndOpModeCode();
			int nBoardEnd = clock();

//			g_core.m_tc.GetBoardOpModeCode();
//			g_core.m_tc.GetBoardInputsCode();
			//inputs
//If bit is set... (Register mapping)
//Bit 6 => L21 input data OK
//Bit 5 => VITC input data OK
//Bit 4 => LTC input data OK
//Bit 3 => Wide VSYNC pulses detected
//Bit 1 => ODD(1) or EVEN(0) video field
//Bit 0 => Video input OK	
			//op modes
//If bit is set... (Register mapping)
//Bit 7 => Diagnostics active
//Bit 6 => FILM(24fps) mode selected
//Bit 5 => LTC(1) or VITC(0) selected
//Bit 4 => EBU(25fps) mode selected
//Bit 3 => VITC Generator active
//Bit 2 => LTC Generator active
//Bit 1 => VITC and/or L21 Reader active
//Bit 0 => LTC Reader active	
			CString t; 
			CString g; 

			if(g_core.m_tc.m_ucBoardOpModeCode&0x20)
			{
				if(g_core.m_tc.m_ucBoardInputsCode&0x10)
				{
					if(bNewInput)
					{
	EnterCriticalSection(&p->m_critTimeCode);
						p->m_ucTimeCodeBits = g_core.m_tc.GetTimeCodeBits();  //get actual DF
	LeaveCriticalSection(&p->m_critTimeCode);
						bNewInput = FALSE;
					}

/*
1) Read and save the status of the VALIDATION byte at 1Ah.
2) Go back to step #1 if bit 7 is high (update in progress).
3) Quickly read and save all of the time, user, and embedded bits which are required by your application. Do not process yet.
4) Read the VALIDATION byte again, then compare it with the value read in step #1.
5) Go back to step #1 if the VALIDATION byte has changed.
6) If desired, read the input signal status byte at 0Ch to make sure that the time code data you just read is current.
7) Process and/or display the time code data as desired. Note that if you attempt to display the time bits as they are read, on many PC�s the
   next time code field or frame will appear before you have finished displaying the previous one. The results will not be pleasant, hence our recommendations.
8) Update the rest of your program operations.
9) Go back to step #1 (NOT STEP #3!).
*/
					int nValidCheck = 0;
					int nValidStart = clock();
					int nValidEnd = 0;
					int nTimeEnd = 0;

					unsigned char vc = g_core.m_tc.GetValidationCode();
					while((!p->m_bTimeCodeThreadKill)&&(vc&0x80))
					{
						Sleep(1);
						vc = g_core.m_tc.GetValidationCode();
						nValidCheck++;
					}
					nValidEnd = clock();
					unsigned long TC = g_core.m_tc.GetTimeCode();
					nTimeEnd = clock();

					if((!p->m_bTimeCodeThreadKill)&&(vc == g_core.m_tc.GetValidationCode()))
					{
						if(TC != 0xffffffff)
						{
	EnterCriticalSection(&p->m_critTimeCode);
							p->m_ulTimeCode = TC;
							p->m_ulTimeCodeElapsed = 0;
	LeaveCriticalSection(&p->m_critTimeCode);

							int nValidTime = clock() - nLastValidTime;
							if(nValidTime > 34) // >1 frame, hardcode for now.  means we skipped a frame
							{
/*
								FILE* fp;
								fp = fopen("LTCRDR.txt","ab");
								if(fp)
								{
									_timeb timestamp;
									_ftime(&timestamp);
									char logtmbuf[48]; // need 33;
									tm* theTime = localtime( &timestamp.time	);
									strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

									fprintf(fp, "%s%03d Delayed update: %d ms since last update; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
										nValidTime,
										nBoardCheck, nBoardEnd,
										nValidStart,
										nValidEnd,
										nValidCheck,
										nTimeEnd,
										nValidLoops,
										nLastSleep
										);

									fclose(fp);
								}
*/
							}
							
							int nTCTime = nTimeEnd - nLastTCTime;
							if(ulLastTC != TC) // time address has changed
							{

								if(nTCTime > 3*16) // 3 16ms timeslices at most, sampling error for one frame with 16 ms tolerance
								{
/*
									FILE* fp;
									fp = fopen("LTCRDR.txt","ab");
									if(fp)
									{
										_timeb timestamp;
										_ftime(&timestamp);
										char logtmbuf[48]; // need 33;
										tm* theTime = localtime( &timestamp.time	);
										strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

										fprintf(fp, "%s%03d Delayed register change %08x->%08x: %d ms since last update with %d repeats; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
											ulLastTC, TC,
											nTCTime, nNumTCRepeats,
											nBoardCheck, nBoardEnd,
											nValidStart,
											nValidEnd,
											nValidCheck,
											nTimeEnd,
											nValidLoops,
											nLastSleep
											);

										fclose(fp);
									}
*/

								}

/*
								if((TC<0x00000010)||(TC>0x23595915)) // log around midnight
								{
									FILE* fp;
									fp = fopen("LTCRDR.txt","ab");
									if(fp)
									{
										_timeb timestamp;
										_ftime(&timestamp);
										char logtmbuf[48]; // need 33;
										tm* theTime = localtime( &timestamp.time	);
										strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

										fprintf(fp, "%s%03d CHECK register change %08x->%08x: %d ms since last update with %d repeats; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
											ulLastTC, TC,
											nTCTime, nNumTCRepeats,
											nBoardCheck, nBoardEnd,
											nValidStart,
											nValidEnd,
											nValidCheck,
											nTimeEnd,
											nValidLoops,
											nLastSleep
											);

										fclose(fp);
									}

								}

*/
								nLastTCTime = nTimeEnd;
								ulLastTC = TC;
								nNumTCRepeats = 0;
							}
							else
							{
								nNumTCRepeats++;
							}

							nValidLoops=0;
							nLastValidTime = nTimeEnd; // just reset

/*
							t.Format(" Adrienne LTC/RDR: %02x:%02x:%02x%s%02x  op:0x%02x in:0x%02x",
								(unsigned char)((TC&0xff000000)>>24),
								(unsigned char)((TC&0x00ff0000)>>16),
								(unsigned char)((TC&0x0000ff00)>>8),  df&0x80?";":":",
								(unsigned char)(TC&0x000000ff),
								g_core.m_tc.m_ucBoardOpModeCode,
								g_core.m_tc.m_ucBoardInputsCode
								);
*/

						}
						else
						{
	EnterCriticalSection(&p->m_critTimeCode);
							p->m_ulTimeCodeElapsed = clock() - nLastValidTime;
	LeaveCriticalSection(&p->m_critTimeCode);

/*
							t.Format(" Adrienne LTC/RDR: --:--:--%s--  op:0x%02x in:0x%02x %d",
									df&0x80?";":":",
									g_core.m_tc.m_ucBoardOpModeCode,
									g_core.m_tc.m_ucBoardInputsCode, clock()  // added clock so disply does something
								);

							FILE* fp;
							fp = fopen("LTCRDR.txt","ab");
							if(fp)
							{
								_timeb timestamp;
								_ftime(&timestamp);
								char logtmbuf[48]; // need 33;
								tm* theTime = localtime( &timestamp.time	);
								strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

								fprintf(fp, "%s%03d TC ERROR %d ms since last update with %d repeats; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
									
									nTimeEnd - nLastValidTime, nNumTCRepeats,
									nBoardCheck, nBoardEnd,
									nValidStart,
									nValidEnd,
									nValidCheck,
									nTimeEnd,
									nValidLoops,
									nLastSleep
									);

								fclose(fp);
							}
*/

						}
//						p->GetWindowText(g);
//						if(g.Compare(t))	p->SetWindowText(t);			// compare makes it less blinky
					}
					else
					{
						nValidLoops++;
						// p->SetWindowText("Adrienne LTC/RDR: no LTC input"); // just leave it at last update
					}

				}
				else
				{
	EnterCriticalSection(&p->m_critTimeCode);
							p->m_ulTimeCodeElapsed = clock() - nLastValidTime;
	LeaveCriticalSection(&p->m_critTimeCode);

					bNewInput = TRUE;
//					p->m_bSync = FALSE; // disengage
//					p->GetDlgItem(IDC_STATIC_TEXT)->SetWindowText("Sync disengaged!  Lost input");

//					p->GetWindowText(g);
//					t.Format(" Adrienne LTC/RDR: no LTC input op:0x%02x in:0x%02x", g_core.m_tc.m_ucBoardOpModeCode, g_core.m_tc.m_ucBoardInputsCode);
//					if(g.Compare(t))	p->SetWindowText(t);			// compare gets rid of blinky
				}
			}
			else
			{
				bNewInput = TRUE;
//				p->m_bSync = FALSE; // disengage
//				p->GetDlgItem(IDC_STATIC_TEXT)->SetWindowText("Sync disengaged!  LTC not selected");

//				p->GetWindowText(g);
//				t.Format(" Adrienne LTC/RDR: LTC not selected op:0x%02x cap:0x%02x", g_core.m_tc.m_ucBoardOpModeCode, g_core.m_tc.m_ucBoardCapabilitesCode);
//				if(g.Compare(t))	p->SetWindowText(t);			// compare gets rid of blinky
	EnterCriticalSection(&p->m_critTimeCode);
							p->m_ulTimeCodeElapsed = clock() - nLastValidTime;
	LeaveCriticalSection(&p->m_critTimeCode);
			}
		}

/*
		tick = 10 - (clock() - tick);

		if(tick<10)  // this should allow 3 attempts per frame (time address).
		{
			nLastSleep = tick;
			Sleep(tick);
		}
		else
		{
			Sleep(0);
			nLastSleep = 0;
		}
*/
		Sleep(1) ;// 33 times per frame, possibly
		nLastSleep = 1;
	}
	//		AfxMessageBox("exiting");


//	p->SetWindowText(" Disconnecting"); Sleep(1);
	g_core.m_tc.DisconnectFromCard();

//	p->SetWindowText(" Disconnected");
	p->m_bTimeCodeThreadStarted=false;
	g_dlldata.thread[0]->nThreadState &= ~0x10; ///started

}


void TimerWaitThread(void* pvArgs)
{
	CTimerItem* p = (CTimerItem*) pvArgs;
	if(p)
	{
//													p->m_ulDuration = ulDuration;
//													p->m_ulStartTC = ulStartTC;
//													p->m_pszActionData = (char*)g_data.m_ppTimers[nIndex];

		CTimerItem* pActual = (CTimerItem*)p->m_pszActionData;
		if(pActual)
		{
			while(pActual->m_bTimerStarted){ Sleep(0); }

			pActual->m_ulDuration = p->m_ulDuration;
			pActual->m_ulStartTC = p->m_ulStartTC;

	if(g_settings.m_ulDebug&(TIMER_DEBUG_SET)) 
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:Timer_Start", "Timer [%s] starting after wait", pActual->m_pszName?pActual->m_pszName:"(null)"); // Sleep(250); //(Dispatch message)

			pActual->m_bKillTimer = false;

			if(_beginthread(TimerThread, 0, (void*)(pActual))==-1)
			{
			//error.
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "Timers:debug", "Error starting timer thread after wait for %s", pActual->m_pszName?pActual->m_pszName:"(null)");//   Sleep(250);//(Dispatch message)
			//**MSG
			}

		}

		p->m_pszActionData = NULL; // clear this out, it was referential.

		delete p;
	}
}




void TimerThread(void* pvArgs)
{
	CTimerItem* p = (CTimerItem*) pvArgs;

	if(p == NULL) return;

	char errorstring[MAX_MESSAGE_LENGTH];
	
	_ftime(&p->m_timeLastStarted);
	p->m_bTimerStarted = true;
	p->m_nCount = 0;// temp init.

	int nCount = -1; // init

//	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, p->m_pszName, g_settings.m_pszInternalAppName, "The timer was sent TC %08x", p->m_ulStartTC);

/*
	m_ulType=TIMER_TYPE_DOWN;  // up count, down count
	m_ulStatus=TIMER_STATUS_NONE;

	m_ulDuration=0; // if a downcounter, the total number of milliseconds to count down.  If an upcounter, it should just be zero.
	m_ulStartTC=0xffffffff; // if a scheduled timer, start from this millisecond offset.
	m_ulStopTC=0xffffffff; // the time code at finish

	m_ulCount=0; // if a downcounter, the total number of milliseconds left.  If an upcounter, elapsed duration
	m_ulPausedDuration=0; // elapsed milliseconds while paused.
	m_ulTimesPaused=0;
	
	m_timeLastStarted.time=0;
	m_timeLastStarted.millitm=0;
	m_timeLastStopped.time=0;
	m_timeLastStopped.millitm=0;

	m_pszName=NULL;
	m_pszDesc=NULL;

	m_bKillTimer=true;
	m_bTimerStarted=false;

	m_ulActionType = TIMER_ACTION_TYPE_NONE;  
	m_pszActionData = NULL;
*/

	// init some values:

	p->m_ulStatus=TIMER_STATUS_STARTED;
	p->m_ulPausedDuration=0;
	p->m_ulTimesPaused=0;
/*
// actually leave these alone, history.
	p->m_timeLastStarted.time=0;
	p->m_timeLastStarted.millitm=0;
	p->m_timeLastStopped.time=0;
	p->m_timeLastStopped.millitm=0;
*/

	p->PublishItem(); // initial

	unsigned long ulTimeCode = 0xffffffff;
	unsigned long ulTimeCodeElapsed = 0;

//	_timeb timeTarget; // the calculated stop target.
	_timeb timePause; // the last time it was paused.
	_timeb timeNow; // now!
	_timeb timeLastSQL; // last time we updated SQL server

	_ftime(&timeLastSQL);

	_timeb timeCompensatedStartTime; // the time it was started, compensated if nec for TC's in the past.

	// first, start the timer.

	unsigned long ulStatus = 0;
	// then continue counting (unless paused etc)
	while((!p->m_bKillTimer)&&(!g_dlldata.thread[0]->bKillThread))
	{
		// count!
		if(p->m_ulStatus&TIMER_STATUS_COUNTING)
		{
			// count!
			if(p->m_ulStatus&TIMER_STATUS_PAUSED)
			{
				//no counting, just check for non paused ness.
				if(!(ulStatus&TIMER_STATUS_PAUSED))
				{
					// we were just paused.
					p->m_ulTimesPaused++;
					_ftime(&timePause);
					ulStatus = p->m_ulStatus;
					p->PublishItem();

					// get the current TC.
					if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
					{
						EnterCriticalSection(&g_data.m_critTimeCode);
						ulTimeCode = g_data.m_ulTimeCode;
			//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
						LeaveCriticalSection(&g_data.m_critTimeCode);
					}
					else
					{
						// use system time
						int nNowMS = ((timePause.time - (timePause.timezone*60) +(timePause.dstflag?3600:0))%86400)*1000 + timePause.millitm;
						p->m_timeconv.MStoBCDHMSF(nNowMS, &ulTimeCode, (g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate));
					}


					g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, p->m_pszName, g_settings.m_pszInternalAppName, "The timer was paused at %02x:%02x:%02x.%02x.",
							((ulTimeCode&0xff000000)>>24),
							((ulTimeCode&0x00ff0000)>>16),
							((ulTimeCode&0x0000ff00)>>8),
							(ulTimeCode&0x000000ff)
						);

				}
			}
			else
			{
				//not paused
				_ftime(&timeNow);  // get the time!

				int nDiff;

				// first check if we just came out of pause;
				if(ulStatus&TIMER_STATUS_PAUSED)
				{
					// we were just paused, so have to calc times and deal.
					ulStatus = p->m_ulStatus;

					nDiff = (timeNow.time - timePause.time)*1000 + (timeNow.millitm - timePause.millitm);  // do this to avoid negative millitm later.

					p->m_ulPausedDuration += nDiff;
/*
					timeTarget.time += nDiff/1000;
					timeTarget.millitm += (unsigned short)(nDiff%1000);
					while(timeTarget.millitm>999)
					{
						timeTarget.time ++;
						timeTarget.millitm -= 1000;
					}
*/
					p->PublishItem();



					// get the current TC.
					if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
					{
						EnterCriticalSection(&g_data.m_critTimeCode);
						ulTimeCode = g_data.m_ulTimeCode;
			//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
						LeaveCriticalSection(&g_data.m_critTimeCode);
					}
					else
					{
						// use system time
						int nNowMS = ((timeNow.time - (timeNow.timezone*60) +(timeNow.dstflag?3600:0))%86400)*1000 + timeNow.millitm;  // TC is local...
						p->m_timeconv.MStoBCDHMSF(nNowMS, &ulTimeCode, (g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate));
					}


					g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, p->m_pszName, g_settings.m_pszInternalAppName, "The timer was resumed at %02x:%02x:%02x.%02x.",
							((ulTimeCode&0xff000000)>>24),
							((ulTimeCode&0x00ff0000)>>16),
							((ulTimeCode&0x0000ff00)>>8),
							(ulTimeCode&0x000000ff)
						);

				}
				else
				{
					// then count!

					bool bDone = false;
					nDiff = (timeNow.time - timeCompensatedStartTime.time)*1000 + (timeNow.millitm - timeCompensatedStartTime.millitm);  // do this to avoid negative millitm later.
					
					if(p->m_ulType == TIMER_TYPE_DOWN)
					{ // down counter
						p->m_nCount = p->m_ulDuration + p->m_ulPausedDuration - nDiff; // counting down 
						if(p->m_nCount<=0)
						{
							p->m_nCount=0;
							bDone = true;
						}
					}
					else
					{ //up counter
						p->m_nCount = nDiff - p->m_ulPausedDuration; // counting up 
						if(p->m_nCount >= (int)p->m_ulDuration)
						{
							p->m_nCount=p->m_ulDuration;
							bDone = true;
						}
					}

					if(bDone)
					{
						// go final
						p->m_ulStatus &= ~(TIMER_STATUS_COUNTING|TIMER_STATUS_STARTED);
						p->m_ulStatus |= TIMER_STATUS_FINAL;
						_ftime(&p->m_timeLastStopped);

						// get the current TC.
						if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
						{
							EnterCriticalSection(&g_data.m_critTimeCode);
							p->m_ulStopTC = g_data.m_ulTimeCode;
//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
							LeaveCriticalSection(&g_data.m_critTimeCode);
						}
						else
						{
							// use system time
							int nNowMS = ((p->m_timeLastStopped.time - (p->m_timeLastStopped.timezone*60) +(p->m_timeLastStopped.dstflag?3600:0))%86400)*1000 + p->m_timeLastStopped.millitm;
							p->m_timeconv.MStoBCDHMSF(nNowMS, &p->m_ulStopTC, (g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate));
						}
						// publish now before action, in case the action has some timeout that could delay the db write
						p->PublishItem();


	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, p->m_pszName, g_settings.m_pszInternalAppName, "The timer ran to completion.  It was stopped at %02x:%02x:%02x.%02x.",
			((p->m_ulStopTC&0xff000000)>>24),
			((p->m_ulStopTC&0x00ff0000)>>16),
			((p->m_ulStopTC&0x0000ff00)>>8),
			(p->m_ulStopTC&0x000000ff)
		);

						// do the "action"!
						switch(p->m_ulActionType)
						{
						case TIMER_ACTION_TYPE_SQL:
							{
								// run a query.
								p->m_ulStatus |= TIMER_STATUS_ACTIONERROR;
								if((p->m_pszActionData)&&(strlen(p->m_pszActionData)>0))
								{
									if(( g_data.m_pdb )&&(	g_data.m_pdbConn))
									{

if(g_settings.m_ulDebug&(TIMER_DEBUG_SQL)) 
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:action", "SQL: %s", p->m_pszActionData?p->m_pszActionData:"NULL"); // Sleep(250); //(Dispatch message)

EnterCriticalSection(&g_data.m_critSQL);
		if(g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, p->m_pszActionData, errorstring)<DB_SUCCESS)
		{
			//**MSG
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:action", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)
	g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, p->m_pszName, g_settings.m_pszInternalAppName, "ERROR executing SQL: %s", errorstring);

		}
		else
		{
			p->m_ulStatus &= ~TIMER_STATUS_ACTIONERROR;
			p->m_ulStatus |= TIMER_STATUS_ACTIONOK;
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:action", "SQL query executed successfully: %s", p->m_pszActionData?p->m_pszActionData:"NULL"); // Sleep(250); //(Dispatch message)
	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, p->m_pszName, g_settings.m_pszInternalAppName, "SQL query executed successfully");
		}
LeaveCriticalSection(&g_data.m_critSQL);

									}
								}
							} break;
						case TIMER_ACTION_TYPE_CXNET:
							{
								// need some parameters, such as:
								// host:port. command id, subcommand, data.
								// pipe delim is fine.

								p->m_ulStatus |= TIMER_STATUS_ACTIONERROR;
								if(p->m_pszActionData)
								{
									unsigned long ulLen = strlen(p->m_pszActionData);
									if(ulLen)
									{
										CNetData data;

										data.m_ucType = NET_TYPE_PROTOCOL1;      // defined type - indicates which protocol to use, structure of data

										CSafeBufferUtil sbu;
										char* pchHost = sbu.Token(p->m_pszActionData, ulLen, ":", MODE_SINGLEDELIM);
										if((pchHost)&&(strlen(pchHost)>0))
										{
											char* pchBuffer = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
											if((pchBuffer)&&(strlen(pchBuffer)>0))
											{
												int nPort = atoi(pchBuffer);
												pchBuffer = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
												if(pchBuffer)
												{
													ulLen = strlen(pchBuffer);
													if(ulLen)
													{
														CBufferUtil bu;
														data.m_ucCmd = (unsigned char)bu.xtol(pchBuffer, ulLen);
														pchBuffer = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
														if(pchBuffer)
														{
															ulLen = strlen(pchBuffer);
															if(ulLen)
															{
																data.m_ucSubCmd = (unsigned char)bu.xtol(pchBuffer, ulLen);
																data.m_ucType |= NET_TYPE_HASSUBC;
															}


															// here, have to send the rest of the buffer.
															char* pchData = p->m_pszActionData + (pchBuffer - pchHost + ulLen + 1);  //+1 gets you past the delimiter, or term 0 if it ends, but we check the length here.


//if(g_settings.m_ulDebug&(TIMER_DEBUG_ACTIONS)) 
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:action", "%d+%d > %d", p->m_pszActionData, strlen(p->m_pszActionData), pchData);

															if(p->m_pszActionData + strlen(p->m_pszActionData) > pchData)  // there is data
															{
																// here, have to send the rest of the buffer.
																ulLen = strlen(pchData);
																if(ulLen)
																{
																	data.m_pucData = (unsigned char*)malloc(ulLen+1);
																	if(data.m_pucData)
																	{
																		memcpy(data.m_pucData, pchData, ulLen);
																		data.m_pucData[ulLen]=0;
																		data.m_ulDataLen = ulLen;
																		data.m_ucType |= NET_TYPE_HASDATA;
																	}
																}
															}
														}

if(g_settings.m_ulDebug&(TIMER_DEBUG_ACTIONS)) 
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:action", "Sending to %s:%d cmd %02x %02x %s (%d bytes)", 
															pchHost?pchHost:"(null)", nPort,
															data.m_ucCmd, data.m_ucSubCmd, data.m_pucData?(char*)data.m_pucData:"(null)", data.m_ulDataLen); // Sleep(250); //(Dispatch message)


														SOCKET s;
														int nReturn = p->m_net.OpenConnection(pchHost, nPort, &s, 5000, 5000, errorstring);

														if(nReturn<NET_SUCCESS)
														{
															// log it.
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "Timers:action", "Error connecting to %s:%d to send cmd %02x %02x %s (%d bytes)", 
															pchHost?pchHost:"(null)", nPort,
															data.m_ucCmd, data.m_ucSubCmd, data.m_pucData?(char*)data.m_pucData:"(null)", data.m_ulDataLen); // Sleep(250); //(Dispatch message)
														}
														else
														{
															nReturn = p->m_net.SendData(&data, s, 5000, 0, NET_SND_CMDTOSVR, errorstring);
															
															if(nReturn<NET_SUCCESS)
															{
																// log it.
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "Timers:action", "Error %d sending to %s:%d cmd %02x %02x %s (%d bytes): %s", nReturn,
															pchHost?pchHost:"(null)", nPort,
															data.m_ucCmd, data.m_ucSubCmd, data.m_pucData?(char*)data.m_pucData:"(null)", data.m_ulDataLen, errorstring); // Sleep(250); //(Dispatch message)
															}
															else
															{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:action", "Action %s executed successfully with return data: %02x,%02x %s (%d bytes data)", 
																	p->m_pszActionData,
																	data.m_ucCmd,    // the command byte
																	data.m_ucSubCmd,   // the subcommand byte
																	(((data.m_pucData)&&(data.m_ulDataLen))?(char*)data.m_pucData:""),
																	data.m_ulDataLen

																); // Sleep(250); //(Dispatch message)
	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, p->m_pszName, g_settings.m_pszInternalAppName, "Action %s executed successfully with return data: %02x,%02x %s (%d bytes data)", 
																p->m_pszActionData,
																data.m_ucCmd,    // the command byte
																data.m_ucSubCmd,   // the subcommand byte
																(((data.m_pucData)&&(data.m_ulDataLen))?(char*)data.m_pucData:""),
																data.m_ulDataLen
																);
																// just send an ack
																p->m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK, errorstring);
																p->m_ulStatus &= ~TIMER_STATUS_ACTIONERROR;
																p->m_ulStatus |= TIMER_STATUS_ACTIONOK;
																
															}
														}

														if(s)
														{
															p->m_net.CloseConnection(s, errorstring);
														}

													}
												}
											}
										}
									}

								}
								if(p->m_ulStatus&TIMER_STATUS_ACTIONERROR)
								{
									if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:action", "ERROR executing action: %s", p->m_pszActionData?p->m_pszActionData:"NULL"); // Sleep(250); //(Dispatch message)
	g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, p->m_pszName, g_settings.m_pszInternalAppName, "ERROR executing action: %s", p->m_pszActionData?p->m_pszActionData:"NULL");

								}
							} break;
						case TIMER_ACTION_TYPE_INET:
							{
								// need some parameters, such as:
								// host:port. data.
								// pipe delim is fine.
								p->m_ulStatus |= TIMER_STATUS_ACTIONERROR;
								if(p->m_pszActionData)
								{
									unsigned long ulLen = strlen(p->m_pszActionData);
									if(ulLen)
									{
										CNetData data;

										data.m_ucType = NET_TYPE_PROTOCOL1;      // defined type - indicates which protocol to use, structure of data

										CSafeBufferUtil sbu;
										char* pchHost = sbu.Token(p->m_pszActionData, ulLen, ":", MODE_SINGLEDELIM);
										if((pchHost)&&(strlen(pchHost)>0))
										{
											char* pchBuffer = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
											if((pchBuffer)&&(strlen(pchBuffer)>0))
											{
												int nPort = atoi(pchBuffer);
												pchBuffer = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
												if(pchBuffer)
												{
													ulLen = strlen(pchBuffer);
													if(ulLen)
													{
														data.m_pucData = (unsigned char*)malloc(ulLen+1);
														if(data.m_pucData)
														{
															memcpy(data.m_pucData, pchBuffer, ulLen);
															data.m_pucData[ulLen]=0;
															data.m_ulDataLen = ulLen;
															data.m_ucType |= NET_TYPE_HASDATA;
														}

														SOCKET s;
														int nReturn = p->m_net.OpenConnection(pchHost, nPort, &s, 5000, 5000, errorstring);

														if(nReturn<NET_SUCCESS)
														{
															// log it.
														}
														else
														{
//															nReturn = p->m_net.SendData(&data, s, 5000, 0, NET_SND_CMDTOSVR, errorstring);
															nReturn = p->m_net.SendLine(data.m_pucData, data.m_ulDataLen, s, EOLN_NONE, true, 5000, errorstring);
															
															if(nReturn<NET_SUCCESS)
															{
																// log it.
															}
															else
															{

if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:action", "Action %s executed successfully", 
																	p->m_pszActionData
																); // Sleep(250); //(Dispatch message)
	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, p->m_pszName, g_settings.m_pszInternalAppName, "Action %s executed successfully", 
																p->m_pszActionData
																);

																// just close the conn... no idea if there will be any return data, so don't receive any.
																// we could fdset and select to see if there is any, but really no need - there is nothing we can do about it anyway.
																p->m_ulStatus &= ~TIMER_STATUS_ACTIONERROR;
																p->m_ulStatus |= TIMER_STATUS_ACTIONOK;

															}
														}
														if(s)
														{
															p->m_net.CloseConnection(s, errorstring);
														}
													}
												}
											}
										}
									}

								}
								if(p->m_ulStatus&TIMER_STATUS_ACTIONERROR)
								{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:action", "ERROR executing action: %s", p->m_pszActionData?p->m_pszActionData:"NULL"); // Sleep(250); //(Dispatch message)
	g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, p->m_pszName, g_settings.m_pszInternalAppName, "ERROR executing action: %s", p->m_pszActionData?p->m_pszActionData:"NULL");

								}

							} break;
						case TIMER_ACTION_TYPE_NONE:
						default:
							{
							} break;
						}

						p->PublishItem();
						p->m_bKillTimer = true;

					}
					else
					{
						// just publish the count.

						// use these settings to lighten the load on the SQL server.
//	int m_nMinimumTimerIntervalMS;   // minimum time interval between count updates to SQL server - have to manage them.... too many too fast can make the SQL server fall over
//	int m_nCriticalThresholdMS;   // number of MS before zero count down (or final count up) where we do not apply m_nMinimumTimerIntervalMS.   for accurate granularity

						int nRemainingMS = 0;

						if(p->m_ulType == TIMER_TYPE_DOWN)
						{ // down counter
							nRemainingMS = p->m_nCount;
						}
						else
						{ //up counter
							nRemainingMS = p->m_ulDuration - p->m_nCount;
						}


						if(nCount != p->m_nCount)  // be a different count, otherwise no point in updating the SQL server
						{
							if(
									(g_settings.m_nMinimumTimerIntervalMS<1)  // if we are just jamming, there is no point in checking critical threshold
							  ||(
									  ( g_settings.m_nCriticalThresholdMS > 0 )
									&&( nRemainingMS <= g_settings.m_nCriticalThresholdMS )
									)  // we are in the critical threshold
//							  ||(g_settings.m_nCriticalThresholdMS<=0)  // its ALWAYS the critical threshold   OR, commented out, it's NEVER the critical threshold.
								)
							{
								p->PublishItem(TIMER_UPDATE_COUNTONLY);
								nCount = p->m_nCount;
								_ftime(&timeLastSQL);

							}
							else  // we are not necessarily hitting the DB as fast as possible (with a diff value)
							{ // so let's check the interval

								nRemainingMS = timeNow.time - timeLastSQL.time;
								nRemainingMS *= 1000;
								nRemainingMS += (timeNow.millitm - timeLastSQL.millitm);

								if(g_settings.m_nMinimumTimerIntervalMS<=nRemainingMS) // we have elapsed the time span
								{
									p->PublishItem(TIMER_UPDATE_COUNTONLY);
									nCount = p->m_nCount;
									_ftime(&timeLastSQL);
								}

							}
						}
					}
				}
			}
		}
		else
		{
			// check to start counting!
			if(p->m_ulStartTC == 0xffffffff) // just use the duration from "now"
			{
//	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, p->m_pszName, g_settings.m_pszInternalAppName, "The timer uses NOW");

				if(p->m_ulType == TIMER_TYPE_DOWN)
				{ // down counter
					p->m_nCount = p->m_ulDuration; // counting down from now!
				}
				else
				{ //up counter
					p->m_nCount = 0; // counting up from now!
				}

				// set the start TC
				if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
				{
					EnterCriticalSection(&g_data.m_critTimeCode);
					p->m_ulStartTC = g_data.m_ulTimeCode;
	//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
					LeaveCriticalSection(&g_data.m_critTimeCode);
//	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, p->m_pszName, g_settings.m_pszInternalAppName, "The timer sets TC start");
				}
				else
				{
					// use system time
					_ftime(&timeNow);  // get the time!
					int nNowMS = ((timeNow.time - (timeNow.timezone*60) +(timeNow.dstflag?3600:0))%86400)*1000 + timeNow.millitm;  // TC is local...
					p->m_timeconv.MStoBCDHMSF(nNowMS, &p->m_ulStartTC, (g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate));
//	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, p->m_pszName, g_settings.m_pszInternalAppName, "The timer sets systime start");

				}
				_ftime(&timeCompensatedStartTime);

			}
			else
			{
				// have to use the time code to start counting from, then apply duration.
				int nNowMS;
				int nStartMS;
				int nDiff;
				// get the current TC.
				if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
				{
//	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, p->m_pszName, g_settings.m_pszInternalAppName, "The timer was sent TC %08x, TC %d %d", p->m_ulStartTC, nNowMS, nStartMS);
					EnterCriticalSection(&g_data.m_critTimeCode);
					ulTimeCode = g_data.m_ulTimeCode;
					ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;
					LeaveCriticalSection(&g_data.m_critTimeCode);

					nNowMS = p->m_timeconv.BCDHMSFtoMS(ulTimeCode, ((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0))) + ulTimeCodeElapsed;
					nStartMS = p->m_timeconv.BCDHMSFtoMS(p->m_ulStartTC, ((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)));

				}
				else
				{
					// use system time
					_ftime(&timeNow);  // get the time!
					nNowMS = ((timeNow.time - (timeNow.timezone*60) +(timeNow.dstflag?3600:0))%86400)*1000 + timeNow.millitm;  // TC is local...
					nStartMS = p->m_timeconv.BCDHMSFtoMS(p->m_ulStartTC, (g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate));
//	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, p->m_pszName, g_settings.m_pszInternalAppName, "The timer was sent TC, sys %d %d", nNowMS, nStartMS);
				}

				if(abs(nStartMS-nNowMS) > 43200000 ) // 12 hour difference!
				{
					// have to assume that the time code has crossed midnight.  so it might be 11:59 now, but we want to start the timer at 00:01, in 2 mins. not 23 hours 58 mins ago!
					// Or, it is 00:01 now, but we wanted to start the timer back at 11:59, 2 mins ago, not 23 hours 58 mins from now!
					// take the smaller one and add 24 hours to it.

					if(g_settings.m_bUseMidnight86MsCompensation)
					{
						if(nStartMS<nNowMS) nStartMS += (86400000 + 86);// so if 11:59 now and start in 2 mins, then we're good
						if(nStartMS>nNowMS) nNowMS += (86400000 + 86); // if 00:01 now and start 2 mins ago, also good.
						// 86 is the 86.4 millseconds between time code 23:59:59:29 and 00:00:00:00.... sigh.  we crossed midnight.  this is what you have to do.
					}
					else // don't
					{
						if(nStartMS<nNowMS) nStartMS += (86400000);// so if 11:59 now and start in 2 mins, then we're good
						if(nStartMS>nNowMS) nNowMS += (86400000); // if 00:01 now and start 2 mins ago, also good.
					}
				}
				nDiff = nNowMS - nStartMS;  // milliseconds so far.  (negative if not started yet, which is OK.)

				if(nDiff>=0)
				{
					_ftime(&timeCompensatedStartTime); // close enough to do this now.
					timeCompensatedStartTime.time -= nDiff/1000;
					if(nDiff%1000 > timeCompensatedStartTime.millitm)
					{
						timeCompensatedStartTime.time--;
						timeCompensatedStartTime.millitm += (1000 - nDiff/1000);
					}
					else
					{
						timeCompensatedStartTime.millitm -= nDiff%1000;
					}

					if(p->m_ulType == TIMER_TYPE_DOWN)
					{ // down counter
						p->m_nCount = p->m_ulDuration - nDiff; // counting down from a little while ago!
					}
					else
					{ //up counter
						p->m_nCount = nDiff; // counting up from a little while ago!
					}
				}
				else
				{
					if(p->m_ulType == TIMER_TYPE_DOWN)
					{ // down counter
						p->m_nCount = p->m_ulDuration; // counting down from soon...
					}
					else
					{ //up counter
						p->m_nCount = 0; // counting up from soon....
					}
					continue; // not counting yet...
				}
			}
/*

			timeTarget.time += p->m_ulDuration/1000;
			timeTarget.millitm += (unsigned short)(p->m_ulDuration%1000);
			while(timeTarget.millitm>999)
			{
				timeTarget.time ++;
				timeTarget.millitm -= 1000;
			}
*/
			p->m_ulStatus |= TIMER_STATUS_COUNTING;
			_ftime(&p->m_timeLastStarted);
			p->PublishItem();


			unsigned long ulDurationTC;

			if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
			{
				p->m_timeconv.MStoBCDHMSF(p->m_ulDuration, &ulDurationTC, ((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)));
			}
			else
			{
				p->m_timeconv.MStoBCDHMSF(p->m_ulDuration, &ulDurationTC, (g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate));
			}


	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, p->m_pszName, g_settings.m_pszInternalAppName, "The timer is counting %02x:%02x:%02x.%02x from %02x:%02x:%02x.%02x.",
//			p->m_ulDuration,
			((ulDurationTC&0xff000000)>>24),
			((ulDurationTC&0x00ff0000)>>16),
			((ulDurationTC&0x0000ff00)>>8),
			(ulDurationTC&0x000000ff),
			((p->m_ulStartTC&0xff000000)>>24),
			((p->m_ulStartTC&0x00ff0000)>>16),
			((p->m_ulStartTC&0x0000ff00)>>8),
			(p->m_ulStartTC&0x000000ff)
		);

		}
	}

  if(	p->m_ulStatus&(TIMER_STATUS_COUNTING|TIMER_STATUS_STARTED) ) // means we got a "kill"
	{

		p->m_nCount=0; // going to make it zero so it's not counting anymore.

		_ftime(&p->m_timeLastStopped);

		// get the current TC.
		if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
		{
			EnterCriticalSection(&g_data.m_critTimeCode);
			p->m_ulStopTC = g_data.m_ulTimeCode;
//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
			LeaveCriticalSection(&g_data.m_critTimeCode);
		}
		else
		{
			// use system time
			int nNowMS = ((p->m_timeLastStopped.time - (p->m_timeLastStopped.timezone*60) +(p->m_timeLastStopped.dstflag?3600:0))%86400)*1000 + p->m_timeLastStopped.millitm;
			p->m_timeconv.MStoBCDHMSF(nNowMS, &p->m_ulStopTC, (g_settings.m_bDF?29.97:(double)g_settings.m_nFrameRate));
		}


		p->m_ulStatus = TIMER_STATUS_STOPPED;
		p->PublishItem();
	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, p->m_pszName, g_settings.m_pszInternalAppName, "The timer did not run to completion.  It was stopped at %02x:%02x:%02x.%02x.",
			((p->m_ulStopTC&0xff000000)>>24),
			((p->m_ulStopTC&0x00ff0000)>>16),
			((p->m_ulStopTC&0x0000ff00)>>8),
			(p->m_ulStopTC&0x000000ff)
		);
	}

	p->m_bTimerStarted = false;

}




