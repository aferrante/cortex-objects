// Timer.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "Timer.h"
#include "../../../Cortex Objects/Tabulator/TabulatorDefines.h"
#include "../../../Cortex Objects/Tabulator/TabulatorData.h"
#include <process.h>
#include "../../../Common/MFC/ODBC/DBUtil.h"
#include "../../../Common/TXT/BufferUtil.h"
#include <objsafe.h>
#include <atlbase.h>
#include "../../../Cortex/3.0.4.5/CortexShared.h" // included to have xml messaging objects and other shared things



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

#define TimerDLG_MINSIZEX 200
#define TimerDLG_MINSIZEY 150

extern void TimerThread(void* pvArgs);
extern void TimerWaitThread(void* pvArgs);

void KillMonitorThread(void* pvArgs);


/////////////////////////////////////////////////////////////////////////////
// CTimerApp

BEGIN_MESSAGE_MAP(CTimerApp, CWinApp)
	//{{AFX_MSG_MAP(CTimerApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTimerApp construction

CTimerApp::CTimerApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	AfxInitRichEdit();
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CTimerApp object

CTimerApp				theApp;
CTimerCore			g_core;
CTimerData			g_data;
CTimerSettings	g_settings;
CMessager*			g_ptabmsgr = NULL;
DLLdata_t       g_dlldata;



int  CTimerApp::DLLCtrl(void** ppvoid, UINT nType)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
//	char szSQL[DB_SQLSTRING_MAXLEN];
//	char errorstring[MAX_MESSAGE_LENGTH];

//	char szFeedSource[MAX_PATH]; sprintf(szFeedSource, "%s_DataDownload", g_settings.m_pszInternalAppName);
//	char szTickerSource[MAX_PATH]; sprintf(szTickerSource, "%s_Ticker", g_settings.m_pszInternalAppName);
	int nReturn = CLIENT_SUCCESS;
	switch(nType)
	{
	case DLLCMD_MAINDLG_BEGIN://					0x00
		{
		} break;
	case DLLCMD_MAINDLG_END://						0x01
		{
		} break;
	case DLLCMD_SETTINGSTXT://						0x02
		{
			if(ppvoid) // we should be passing in the address of a char*.
			{
				if((g_settings.m_pszSettingsText)&&(strlen(g_settings.m_pszSettingsText)>0))
				{
					char* pch = (char*)malloc(strlen(g_settings.m_pszSettingsText)+1);
					if(pch)
					{
						strcpy(pch, g_settings.m_pszSettingsText);
						(*ppvoid)= pch;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
				}
				else
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_DOSETTINGSDLG://					0x03
		{
			// takes a NULL pointer.
/*
			if(g_settings.m_bHasDlg)
			{
				g_settings.DoModal();
			}
			else
*/
			{
				// just refresh
			EnterCriticalSection(&g_settings.m_crit);

				if(g_settings.Settings(true)==CLIENT_SUCCESS)
					nReturn = CLIENT_REFRESH;
				else nReturn = CLIENT_ERROR;

			LeaveCriticalSection(&g_settings.m_crit);
			}
		} break;
	case DLLCMD_GETABOUTTXT://						0x04
		{
			if(ppvoid) // we should be passing in the address of a char*.
			{
				if((g_settings.m_pszAboutText)&&(strlen(g_settings.m_pszAboutText)>0))
				{
					char* pch = (char*)malloc(strlen(g_settings.m_pszAboutText)+1);
					if(pch)
					{
						strcpy(pch, g_settings.m_pszAboutText);
						(*ppvoid)= pch;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
				}
				else
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_GETAPPNAME://							0x05
		{
			if(ppvoid) // we should be passing in the address of a char*.
			{
				if((g_settings.m_pszAppName)&&(strlen(g_settings.m_pszAppName)>0))
				{
					char* pch = (char*)malloc(strlen(g_settings.m_pszAppName)+1);
					if(pch)
					{
						strcpy(pch, g_settings.m_pszAppName);
						(*ppvoid)= pch;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
				}
				else
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
			
		} break;
	case DLLCMD_SETDISPATCHER://					0x06
		{
			if (*ppvoid)
			{
				g_ptabmsgr = (CMessager*)*ppvoid;
//				CString szMsg; szMsg.Format("%08x dispatcher",g_pmd);
//				AfxMessageBox(szMsg);
//				AfxMessageBox(AfxGetApp()->m_pszAppName);
//				AfxMessageBox(AfxGetApp()->m_pszExeName);

/*
					if(g_settings.m_bIsServer)
					{

						if(g_ptabmsgr) g_ptabmsgr->AddDestination(
							MSG_DESTTYPE_LOG, 
							(((g_settings.m_pszAsRunDestination)&&(strlen(g_settings.m_pszAsRunDestination)))?g_settings.m_pszAsRunDestination:"asrun"), 
							((g_settings.m_pszAsRunFilename!=NULL)?g_settings.m_pszAsRunFilename:"timer_asrun|YD||1|")
							);
					}
*/
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Timer", "Version %s (%s) obtained dispatcher", VERSION_STRING, __DATE__);//  Sleep(250); //(Dispatch message)
			}
			else
			{
				nReturn = CLIENT_ERROR;
			}  

		} break;
	case DLLCMD_GETMINX://							0x07
		{
			if(ppvoid) // we should be passing in the address of an int.
			{
				(*ppvoid) = (void*)TimerDLG_MINSIZEX;
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_GETMINY://							0x08
		{
			if(ppvoid) // we should be passing in the address of an int.
			{
				(*ppvoid) = (void*)TimerDLG_MINSIZEY;
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_GETDATAOBJ://							0x09  // gets a pointer to the main data object
		{
			if(ppvoid) // we should be passing in the address of an data object.
			{
				(*ppvoid) = (void*)(&g_dlldata);
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_CALLFUNC://								0x0a  // call a function....  
		{
			if(ppvoid) // we should be passing in the address of an char buffer.
			{
				char* pch = (char*)(*ppvoid);
				if((pch)&&(strlen(pch)))
				{
					// format is func name|arg|arg|arg  where if arg is a string, it is pipe encoded.
					char* pchDelim = strchr(pch, '|');
					if(pchDelim) *pchDelim = 0; //temporary null term.

		//			AfxMessageBox(pch);

					
					if(strcmp(pch, "Server_Get_Status")==0)
					{
						if(1) // success
						{
								// status return shall be:

							_timeb timebTick;
							_ftime(&timebTick);

							CString  szData;
							szData.Format("%d.%03d", timebTick.time, timebTick.millitm);

							// time|datalastupdate|datastate|time|tickerlastupdate|tickerstate

							//assemble some data, then formulate a response and send

							char* pchReturn =  (char*)malloc(szData.GetLength()+1);
							if(pchReturn)
							{
								sprintf(pchReturn, "%s", szData);
								(*ppvoid) = pchReturn;
							}
						}
						else
						{
							(*ppvoid)= NULL;
							nReturn = CLIENT_ERROR;
						}
					}
					else
					if(strcmp(pch, "Server_Refresh_Setting")==0)
					{
						(*ppvoid)= NULL;
			EnterCriticalSection(&g_settings.m_crit);
						g_settings.Settings(true);						
			LeaveCriticalSection(&g_settings.m_crit);
					}
					else
					if(strcmp(pch, "Server_Refresh_Table")==0)
					{
						nReturn = CLIENT_ERROR;
						if(pchDelim)
						{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker Commit {%s}", pchDelim+1);// Sleep(100); //(Dispatch message)
							CSafeBufferUtil sbu;
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_SINGLEDELIM);
							if((pch)&&(strlen(pch)))
							{
								// table name.
								
							}
						}
						(*ppvoid)= NULL;
					}
					else
					if(strcmp(pch, "Timer_Start")==0)
					{
						nReturn = CLIENT_ERROR;
						if(pchDelim)
						{
//		g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, "CallFunc", g_settings.m_pszInternalAppName, "Start timer with %s", pchDelim+1);

if(g_settings.m_ulDebug&(TIMER_DEBUG_SET)) 
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:Timer_Start", "Start timer with %s", pchDelim+1); // Sleep(250); //(Dispatch message)

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker Commit {%s}", pchDelim+1);// Sleep(100); //(Dispatch message)


							CSafeBufferUtil sbu;
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_SINGLEDELIM);
							if(pch)
							{
								int nIndex = g_data.FindTimer(pch); // make sure it exists!
								if(nIndex>=0)
								{
									// Timer needs to be set with duration and possible start time code.

									pch = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
									if(pch)
									{
										// good, we have a required duration in real milliseconds.
										if(strlen(pch)>0)
										{
											unsigned long ulDuration = atol(pch);
											unsigned long ulStartTC=0xffffffff;
											// first let's kill it if it's in progress already, since we now know we have a valid timer set request
											g_data.m_ppTimers[nIndex]->m_bKillTimer = true;


//		g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, g_data.m_ppTimers[nIndex]->m_pszName, g_settings.m_pszInternalAppName, "Start timer with %s", pch);
											pch = sbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
											if((pch)&&(strlen(pch)>0)&&(strcmp(pch, "0"))) // if it was "0" it came from the database. if we really wanted midnight, we'll send 00000000
											{// we have a start tc in drop frame non-compensated, BCD encoded TC .
												ulStartTC = atol(pch);
//g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, g_data.m_ppTimers[nIndex]->m_pszName, g_settings.m_pszInternalAppName, "Start timer with Time code: %s = %08x", pch, ulTC);
/*

												if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
												{
													g_data.m_ppTimers[nIndex]->m_timeconv.MStoBCDHMSF(ulTC, &g_data.m_ppTimers[nIndex]->m_ulStartTC, ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0) );
												}
												else
												{
													// use system time
													g_data.m_ppTimers[nIndex]->m_timeconv.MStoBCDHMSF(ulTC, &g_data.m_ppTimers[nIndex]->m_ulStartTC, (double)g_settings.m_nFrameRate);
												}
*/

												

//	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, g_data.m_ppTimers[nIndex]->m_pszName, g_settings.m_pszInternalAppName, "The timer is sending TC %08x", g_data.m_ppTimers[nIndex]->m_ulStartTC);
											}
/*
											else
											{
//		g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, g_data.m_ppTimers[nIndex]->m_pszName, g_settings.m_pszInternalAppName, "Start timer with no Time code");
												 g_data.m_ppTimers[nIndex]->m_ulStartTC = 0xffffffff;  // clear this!
//												 g_data.m_ppTimers[nIndex]->m_ulStartTC = 0x11450000;  // clear this!
//	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, g_data.m_ppTimers[nIndex]->m_pszName, g_settings.m_pszInternalAppName, "The timer is sending no TC (%08x)", g_data.m_ppTimers[nIndex]->m_ulStartTC);
											}

*/

if(g_settings.m_ulDebug&(TIMER_DEBUG_SET)) 
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:Timer_Start", "Checking timer in progress %d", g_data.m_ppTimers[nIndex]->m_bTimerStarted); // Sleep(250); //(Dispatch message)

											if(g_data.m_ppTimers[nIndex]->m_bTimerStarted)
											{
												CTimerItem* p = new CTimerItem;
												if(p)
												{
													p->m_ulDuration = ulDuration;
													p->m_ulStartTC = ulStartTC;
													p->m_pszActionData = (char*)g_data.m_ppTimers[nIndex];
													if(_beginthread(TimerWaitThread, 0, (void*)(p))==-1)
													{
													//error.
								if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "Timers:debug", "Error starting timer wait thread for %s", g_data.m_ppTimers[nIndex]->m_pszName);//   Sleep(250);//(Dispatch message)
													//**MSG
													}
													else
													{
														nReturn = CLIENT_REFRESH;
													}
												}
											}
											else
											{

												g_data.m_ppTimers[nIndex]->m_ulDuration = ulDuration;
												g_data.m_ppTimers[nIndex]->m_ulStartTC = ulStartTC;
	if(g_settings.m_ulDebug&(TIMER_DEBUG_SET)) 
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:Timer_Start", "Timer not in progress"); // Sleep(250); //(Dispatch message)

												g_data.m_ppTimers[nIndex]->m_bKillTimer = false;

												if(_beginthread(TimerThread, 0, (void*)(g_data.m_ppTimers[nIndex]))==-1)
												{
												//error.
							if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "Timers:debug", "Error starting timer thread for %s", g_data.m_ppTimers[nIndex]->m_pszName);//   Sleep(250);//(Dispatch message)
												//**MSG
												}
												else
												{
													nReturn = CLIENT_SUCCESS;
												}
											}
										}
									}
								}
							}
						}
						(*ppvoid)= NULL;

if(g_settings.m_ulDebug&(TIMER_DEBUG_SET)) 
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:Timer_Start", "Handled timer start"); // Sleep(250); //(Dispatch message)

					}
					else
					if(strcmp(pch, "Timer_Stop")==0)
					{
						nReturn = CLIENT_ERROR;
						if(pchDelim)
						{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker Commit {%s}", pchDelim+1);// Sleep(100); //(Dispatch message)
							CSafeBufferUtil sbu;
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_SINGLEDELIM);
							if(pch)
							{
								int nIndex = g_data.FindTimer(pch); // make sure it exists!
								if(nIndex>=0)
								{
									// just kill it
									g_data.m_ppTimers[nIndex]->m_bKillTimer = true;
									nReturn = CLIENT_SUCCESS;
								}
							}
						}
						(*ppvoid)= NULL;
					}
					else
					if(strcmp(pch, "Timer_Pause")==0)
					{

						nReturn = CLIENT_ERROR;
						if(pchDelim)
						{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker Commit {%s}", pchDelim+1);// Sleep(100); //(Dispatch message)
							CSafeBufferUtil sbu;
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_SINGLEDELIM);
							if(pch)
							{
								int nIndex = g_data.FindTimer(pch); // make sure it exists!
								if((nIndex>=0)&&(g_data.m_ppTimers[nIndex]->m_bTimerStarted)) // otherwise if not started, you cant really pause it...
								{
									if(g_data.m_ppTimers[nIndex]->m_ulStatus&TIMER_STATUS_COUNTING) // let's make sure it's counting too - otherwise it will not start right, maybe
										g_data.m_ppTimers[nIndex]->m_ulStatus |= TIMER_STATUS_PAUSED;

									nReturn = CLIENT_SUCCESS;
								}
							}
						}
						(*ppvoid)= NULL;
					}
					else
					if(strcmp(pch, "Timer_Resume")==0)
					{
						nReturn = CLIENT_ERROR;
						if(pchDelim)
						{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker Commit {%s}", pchDelim+1);// Sleep(100); //(Dispatch message)
							CSafeBufferUtil sbu;
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_SINGLEDELIM);
							if(pch)
							{
								int nIndex = g_data.FindTimer(pch); // make sure it exists!
								if((nIndex>=0)&&(g_data.m_ppTimers[nIndex]->m_bTimerStarted)) // otherwise if not started, you cant really resume it...
								{
									g_data.m_ppTimers[nIndex]->m_ulStatus &= ~TIMER_STATUS_PAUSED;

									nReturn = CLIENT_SUCCESS;
								}
							}
						}
						(*ppvoid)= NULL;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;

					}
					
					if(pchDelim) *pchDelim = '|'; //replace pipe.

				}
				else 
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_SETEVENTS://						0x0b
		{
			if(ppvoid)
			{
//				g_data.m_pEvents = (CTabulatorEventArray*)ppvoid;
			}
		} break;
	case DLLCMD_GETDBSETTINGS://						0x0c
		{
			EnterCriticalSection(&g_settings.m_crit);
			g_settings.GetFromDatabase();
			LeaveCriticalSection(&g_settings.m_crit);
		} break;
	case DLLCMD_SETGLOBALSTATE://						0x0d
		{
/*			if(ppvoid)
			{
				char statetoken[MAX_PATH];
				strncpy(statetoken, (char*)ppvoid, MAX_PATH);
				int n=0;
				while(n<g_dlldata.nNumThreads)
				{
					if((g_dlldata.thread)&&(g_dlldata.thread[n]))
					{
						if(stricmp(statetoken, "resume")==0)
						{
							switch(n)
							{
							case Timer_DATA:// 0
								g_dlldata.thread[Timer_DATA]->nThreadState &= ~Timer_DATA_PAUSED; break;
							case Timer_TICKER:// 1
								g_dlldata.thread[Timer_TICKER]->nThreadState &= ~Timer_TICKER_PAUSED; break;
							default:
							case Timer_UI:// 2
								break;
							}
						}
						else
						if(stricmp(statetoken, "suspend")==0)
						{
							switch(n)
							{
							case Timer_DATA:// 0
								g_dlldata.thread[Timer_DATA]->nThreadState |= Timer_DATA_PAUSED; break;
							case Timer_TICKER:// 1
								g_dlldata.thread[Timer_TICKER]->nThreadState |= Timer_TICKER_PAUSED; break;
							default:
							case Timer_UI:// 2
								break;
							}
						}
					}
					n++;
				}
			}
			else nReturn = CLIENT_ERROR;
*/

		} break;

		

	default: nReturn = CLIENT_UNKNOWN; break;
	}
	return nReturn;
}

BOOL CTimerApp::InitInstance() 
{
#if _WIN32_WINNT >= 0x0400
		HRESULT hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		HRESULT hRes = CoInitialize(NULL);
#endif
//	AfxMessageBox(m_pszAppName);
//	AfxMessageBox(m_pszExeName);
//	AfxMessageBox("Foo");

	strcpy(m_szSettingsFilename, SETTINGS_FILENAME);

	if((m_pszAppName)&&(strlen(m_pszAppName)>0)) sprintf(m_szSettingsFilename, "%s.ini", m_pszAppName);

	InitializeCriticalSection(&g_dlldata.m_crit);
	EnterCriticalSection(&g_dlldata.m_crit);

	g_dlldata.nNumThreads=0;
	g_dlldata.thread = new DLLthread_t*[1]; // 1 thread, timecode thread
	
	if(g_dlldata.thread)
	{
		g_dlldata.nNumThreads=1;
		g_dlldata.thread[0] = new DLLthread_t;
		g_data.InitDLLData(g_dlldata.thread[0]);
		g_data.InitDLLData(g_dlldata.thread[0]);

		g_dlldata.thread[0]->bKillThread=false;

		if(_beginthread(KillMonitorThread, 0, (void*)(NULL))==-1)
		{
		//error.
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "Error starting kill monitor thread");//   Sleep(250);//(Dispatch message)
		//**MSG
		}


	}
		
 	LeaveCriticalSection(&g_dlldata.m_crit);


//	AfxMessageBox("Foo2");
	return CWinApp::InitInstance();
}




int CTimerApp::ExitInstance() 
{
	g_ptabmsgr = NULL;

	// kill all the timers.

//	char errorstring[MAX_MESSAGE_LENGTH];
	int nIndex = 0;
	while(nIndex<g_data.m_nTimers)
	{
		if((g_data.m_ppTimers)&&(g_data.m_ppTimers[nIndex]))
		{

			if((g_data.m_ppTimers[nIndex]->m_ulFlags&TIMER_STATUS_COUNTING)||(g_data.m_ppTimers[nIndex]->m_bTimerStarted))
			{
				int nWait =  clock()+1500;
				g_data.m_ppTimers[nIndex]->m_bKillTimer = true;

//				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
//						g_data.m_ppTimers[nIndex]->m_pszDesc?g_data.m_ppTimers[nIndex]->m_pszDesc:g_data.m_ppTimers[nIndex]->m_pszName);  
//					g_ptabmsgr->DM(MSG_ICONINFO, NULL, "Timers:remove_timer", errorstring);  // Sleep(20);  //(Dispatch message)

				
				//									g_data.m_ppTimers[nTemp]->m_pDlg->OnDisconnect();


				//**** disconnect
//								while(g_data.m_ppTimers[nIndex]->m_bConnThreadStarted) Sleep(1);

				while(
							 (
								 (g_data.m_ppTimers[nIndex]->m_bTimerStarted)
							 )
							&&
							 (clock()<nWait)
						 )
				{
					Sleep(1);
				}

			}
		}
		nIndex++;
	}
	g_data.m_bTimeCodeThreadKill = true;

	while(g_data.m_bTimeCodeThreadStarted) Sleep(10);

			EnterCriticalSection(&g_settings.m_crit);
	g_settings.Settings(false);
			LeaveCriticalSection(&g_settings.m_crit);

  CoUninitialize(); //XML
	
	return CWinApp::ExitInstance();
}



void KillMonitorThread(void* pvArgs)
{
	g_dlldata.thread[0]->bThreadStarted = true;
	g_dlldata.thread[0]->nThreadState |= 1; ///started


	while((g_ptabmsgr == NULL)&&(!g_dlldata.thread[0]->bKillThread)) Sleep(10);  // want to have the messaging started...


	if(!g_dlldata.thread[0]->bKillThread)
	{
//	g_dlldata.pDlg = g_pdlg; // if there is a dialog....

//	AfxMessageBox("Foo1");
			EnterCriticalSection(&g_settings.m_crit);
//			if(bIsServer)
//			{
	g_settings.GetFromDatabase(); // does Settings(true) first
//			}
//			else
//			{
//	g_settings.Settings(true); // need to get the thread settings...
//			}
			LeaveCriticalSection(&g_settings.m_crit);
	

		// create the db connection...


		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];
		g_data.m_pdb = new CDBUtil;
		g_data.m_pdbConn = g_data.m_pdb->CreateNewConnection(g_settings.m_pszDSN, g_settings.m_pszUser, g_settings.m_pszPW);
		if(g_data.m_pdbConn)
		{
			if(g_data.m_pdb->ConnectDatabase(g_data.m_pdbConn, errorstring)<DB_SUCCESS)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Timer_Settings:database_connect", errorstring);  //(Dispatch message)
			}
			else
			{

	// get the timers.
				// but first set them all to "reset".

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = '0', duration = '0', start_tc = '0', stop_tc = '0', \
count = '0', paused_duration = '0', paused_count = '0', last_started = '0', last_stopped = '0' WHERE uid > -1",  //HARDCODE
						((g_settings.m_pszTimers)&&(strlen(g_settings.m_pszTimers)))?g_settings.m_pszTimers:"Timers"
						);
	//			Sleep(1); // dont peg processor
if(g_settings.m_ulDebug&(TIMER_DEBUG_SQL)) 
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "SQL: %s", szSQL); // Sleep(250); //(Dispatch message)
				if(g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
				{
					//**MSG
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

				}

				g_data.GetTimers();




			}
		}
		else
		{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Timer_Settings:database_connect", errorstring);  //(Dispatch message)
		}
	}

	while(!g_dlldata.thread[0]->bKillThread)
	{
		Sleep(10);
	}
	g_data.m_bTimeCodeThreadKill = true;
	while(g_data.m_bTimeCodeThreadStarted)
	{
		Sleep(10);
	}

	g_ptabmsgr = NULL;

	g_dlldata.thread[0]->bThreadStarted = false;
	g_dlldata.thread[0]->nThreadState &= ~1; ///started

}

