// TimerData.cpp: implementation of the CTimerData and CTimerMessage classes.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Timer.h"
#include "TimerData.h"
#include "TimerSettings.h"
#include <atlbase.h>
#include <process.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CTimerData	g_data;
extern CTimerSettings	g_settings;
extern CMessager*			g_ptabmsgr;
extern DLLdata_t       g_dlldata;


#define SENDMSG_EVENT_MAXLEN 64
#define SENDMSG_SENDER_MAXLEN 64
#define SENDMSG_MESSAGE_MAXLEN 512


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/*
CTimerMessage::CTimerMessage()
{
	m_szMessage="";
	m_szRevisedMessage="";
	m_nTimestamp =-1;
	m_nLastUpdate =-1;
	m_nID = -1;
	m_nStatus = Timer_MSG_STATUS_NONE;
	m_nType = Timer_MSG_TYPE_NONE;
	m_nPlayed = 0;
	m_dblIndex = -0.1;
}

CTimerMessage::~CTimerMessage()
{
}


CString CTimerMessage::SQLUpdate(bool bTicker, double dblSeekIndex)
{
  CString szSQL="";
	if(bTicker)
	{
	char* szMsg = g_data.m_dbu.EncodeQuotes(m_szMessage);
	if(m_nStatus&Timer_MSG_STATUS_INSERTED)
	{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "upd item (%d [%s])", m_nID, m_szMessage); //(Dispatch message)
		szSQL.Format("UPDATE %s SET \
[text] = '%s', \
[sort_order] = %.4f, \
[timestamp] = %d, \
[updated] = %d, \
[status] = %d, \
[type] = %d, \
[played] = %d WHERE [id] = %d AND [sort_order] >= %.4f AND [sort_order] <= %.4f", 
					g_settings.m_pszTicker, 
					(szMsg?szMsg:m_szMessage),
					m_dblIndex,
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|Timer_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed,
					m_nID,
					dblSeekIndex-0.0000009,
					dblSeekIndex+0.0000009
					);  // table name
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "upd item (%d [%s]) SQL %s", m_nID, m_szMessage, szSQL);//(Dispatch message)
	}
	else
	{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "ins item (%d [%s])", m_nID, m_szMessage); //(Dispatch message)
		szSQL.Format("INSERT INTO %s (\
[id], \
[text], \
[sort_order], \
[timestamp], \
[updated], \
[status], \
[type], \
[played]) VALUES \
(%d, '%s', %.4f, %d, %d, %d, %d, %d)",
					g_settings.m_pszTicker,  // table name
					m_nID,
					(szMsg?szMsg:m_szMessage),
					m_dblIndex,
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|Timer_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "ins item (%d [%s]) SQL %s", m_nID, m_szMessage, szSQL); //(Dispatch message)
	}
	if(szMsg) free(szMsg);
	}
	else
	{
	char* szMsg = g_data.m_dbu.EncodeQuotes(m_szMessage);
	char* szRevMsg = g_data.m_dbu.EncodeQuotes(m_szRevisedMessage);
	if(m_nStatus&Timer_MSG_STATUS_INSERTED)
	{
		szSQL.Format("UPDATE %s SET \
[text] = '%s', \
[revision] = '%s', \
[timestamp] = %d, \
[updated] = %d, \
[status] = %d, \
[type] = %d, \
[played] = %d WHERE [id] = %d", g_settings.m_pszFeed, 
					(szMsg?szMsg:m_szMessage),
					(szRevMsg?szRevMsg:m_szRevisedMessage),
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|Timer_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed,
					m_nID);  // table name
	}
	else
	{

		szSQL.Format("INSERT INTO %s (\
[id], \
[text], \
[revision], \
[timestamp], \
[updated], \
[status], \
[type], \
[played]) VALUES \
(%d, '%s', '%s', %d, %d, %d, %d, %d)",
					g_settings.m_pszFeed,  // table name
					m_nID,
					(szMsg?szMsg:m_szMessage),
					(szRevMsg?szRevMsg:m_szRevisedMessage),
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|Timer_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed);
	}

	if(szMsg) free(szMsg);
	if(szRevMsg) free(szRevMsg);
	}

	return szSQL;
}

*/

CTimerItem::CTimerItem()
{
	m_nItemID = -1;

	m_ulType=TIMER_TYPE_DOWN;  // up count, down count
	m_ulStatus=TIMER_STATUS_NONE;

	m_ulDuration=0; // if a downcounter, the total number of milliseconds to count down.  If an upcounter, it should just be zero.
	m_ulStartTC=0xffffffff; // if a scheduled timer, start from this millisecond offset.
	m_ulStopTC=0xffffffff; // the time code at finish

	m_nCount=0; // if a downcounter, the total number of milliseconds left.  If an upcounter, elapsed duration
	m_ulPausedDuration=0; // elapsed milliseconds while paused.
	m_ulTimesPaused=0;
	
	m_timeLastStarted.time=0;
	m_timeLastStarted.millitm=0;
	m_timeLastStopped.time=0;
	m_timeLastStopped.millitm=0;

	m_pszName=NULL;
	m_pszDesc=NULL;

	m_bKillTimer=true;
	m_bTimerStarted=false;

	m_ulActionType = TIMER_ACTION_TYPE_NONE;  
	m_pszActionData = NULL;
}


CTimerItem::~CTimerItem()
{
	if(m_pszName) free(m_pszName);
	if(m_pszDesc) free(m_pszDesc);
	if(m_pszActionData) free(m_pszActionData);
}


int CTimerItem::PublishItem(unsigned long ulFlags)
{
	if(( g_data.m_pdb )&&(	g_data.m_pdbConn))
	{

		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		if(ulFlags&TIMER_UPDATE_COUNTONLY)
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET count = %d WHERE uid = %d",  //HARDCODE
					((g_settings.m_pszTimers)&&(strlen(g_settings.m_pszTimers)))?g_settings.m_pszTimers:"Timers",
					m_nCount,
					m_nItemID
					);
		}
		else
		{			
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d, duration = %d, start_tc = %d, stop_tc = %d, \
count = %d, paused_duration = %d, paused_count = %d, last_started = %.03f, last_stopped = %.03f WHERE uid = %d",  //HARDCODE
					((g_settings.m_pszTimers)&&(strlen(g_settings.m_pszTimers)))?g_settings.m_pszTimers:"Timers",
					m_ulStatus,

					m_ulDuration,
					((m_ulStartTC==0xffffffff)?-1:m_ulStartTC),
					((m_ulStopTC==0xffffffff)?-1:m_ulStopTC),

					m_nCount,
					m_ulPausedDuration,
					m_ulTimesPaused,
					
					(((double)(m_timeLastStarted.time)) + ((double)(m_timeLastStarted.millitm))/1000.0),
					(((double)(m_timeLastStopped.time)) + ((double)(m_timeLastStopped.millitm))/1000.0),

					m_nItemID
					);
		}
//			Sleep(1); // dont peg processor
if(g_settings.m_ulDebug&(TIMER_DEBUG_SQL)) 
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:update", "SQL: %s", szSQL); // Sleep(250); //(Dispatch message)

EnterCriticalSection(&g_data.m_critSQL);
		if(g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//**MSG
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "Timers:update", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

		}
LeaveCriticalSection(&g_data.m_critSQL);

	}
	return CLIENT_ERROR;
}



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTimerData::CTimerData()
{

//	m_pEvents = NULL;
//	m_pInEvents = new CTabulatorEventArray;
//	m_pOutEvents = new CTabulatorEventArray;
//	m_pTextEvents = new CTabulatorEventArray;

//	m_socket=NULL;

	m_pdb=NULL;
	m_pdbConn=NULL;
	InitializeCriticalSection(&m_critSQL);

	InitializeCriticalSection(&m_critTimeCode);
	m_bTimeCodeThreadStarted = false;
	m_bTimeCodeThreadKill = true;

	m_ucTimeCodeBits = 0xff; //invalid value
	m_ulTimeCode = 0xffffffff; //invalid value
	m_ulTimeCodeElapsed = 0;

	m_ppTimers=NULL;
	m_nTimers=0;

}

CTimerData::~CTimerData()
{
	m_bTimeCodeThreadKill = true;
//	DeleteCriticalSection(&m_critClientSocket);
	DeleteCriticalSection(&m_critSQL);
//	if(	m_pInEvents) delete  m_pInEvents;
//	if(	m_pOutEvents) delete  m_pOutEvents;
//	if(	m_pTextEvents) delete  m_pTextEvents;
	DeleteCriticalSection(&m_critTimeCode);

}

// remove pipes
CString CTimerData::Encode(CString szText)
{
  CString szOut = "";
  int i;
  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '|')
      szOut += "%:";
    else if (szText[i] == '%')
      szOut += "%%";
    else if ((szText[i] != '\n')&&(szText[i] != '\r'))  //strip returns while we are at it.
       szOut += (char) szText[i];
  }
  return szOut;
}

// put back pipes
CString CTimerData::Decode(CString szText)
{
  CString szOut = "";
  int i;

  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '%')
    {
      i++; if (i>=szText.GetLength()) break;
      if (szText[i] == '%')
      {
        szOut += "%";
      }
      else if (szText[i] == ':')
      {
        szOut += "|";
      }
    }
    else szOut += (char) szText[i];
  }
  return szOut;
}


CString CTimerData::XMLTextNodeValue(IXMLDOMNodePtr pNode)
{
	CString szText = _T("");
	if(pNode)
	{
		IXMLDOMNodeListPtr pValues = pNode->GetchildNodes();
		if (pValues != NULL)
		{
			IXMLDOMNodePtr pValue;
			while(pValue = pValues->nextNode())
			{
				DOMNodeType valnodeType;
				pValue->get_nodeType(&valnodeType);

				if(valnodeType == NODE_TEXT)
				{
USES_CONVERSION;
					szText = W2T(_bstr_t(pValue->GetnodeValue()));
					szText.TrimLeft();
					szText.TrimRight();
					return szText;
				}
			}
		}
	}
	return szText;
}

int CTimerData::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszMessages)&&(strlen(g_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		if(g_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime) \
VALUES ('%s', '%s', %d, %d.%03d)", // HARDCODE
				g_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				g_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				g_settings.m_pszMessages
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
//			IncrementDatabaseMods(g_settings.m_pszMessages, dberrorstring);
		LeaveCriticalSection(&m_critSQL);
			return TABULATOR_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return TABULATOR_ERROR;
}
/*

int CTimerData::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszMessages)&&(strlen(g_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			g_settings.m_pszMessages,
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			g_settings.m_pszMessages
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
//			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return TABULATOR_SUCCESS;
		}
	}
	return TABULATOR_ERROR;
}

*/

int CTimerData::SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...)
{
 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszAsRun)&&(strlen(g_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		if((pszEventID)&&(strlen(pszEventID)>SENDMSG_EVENT_MAXLEN)) pszEventID[SENDMSG_EVENT_MAXLEN-1]=0;
		
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);
		char* pchEvent = m_pdb->EncodeQuotes(pszEventID);
		if(g_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, channelid, eventid) \
VALUES ('%s', '%s', %d, %d.%03d, %d, '%s')", // HARDCODE
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm,
				nChannelID,
				pchEvent?pchEvent:""
				);
		}
		else
		{

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log")
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
		if(pchEvent) free(pchEvent);

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
//			IncrementDatabaseMods(g_settings.m_pszAsRun, dberrorstring);
			return TABULATOR_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);

	}
	return TABULATOR_ERROR;
}

/*
int CTimerData::SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszAsRun)&&(strlen(g_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log")
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Timer_Asrun", "SQL %s", szSQL) ;// Sleep(100); //(Dispatch message)

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
//			IncrementDatabaseMods(g_settings.m_pszAsRun, dberrorstring);
			return TABULATOR_SUCCESS;
		}
		else
		{
if(g_ptabmsgr)
{
	char szSource[MAX_PATH]; sprintf(szSource, "%s_Asrun", g_settings.m_pszInternalAppName);
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "SQL error %s", dberrorstring) ;// Sleep(100); //(Dispatch message)
}
		}
		LeaveCriticalSection(&m_critSQL);

	}
	else
		{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Timer_Asrun", "null conditions");// Sleep(100); //(Dispatch message)
		}
	return TABULATOR_ERROR;
}
*/



int CTimerData::InitDLLData(DLLthread_t* pData)
{
	if(pData)
	{
		InitializeCriticalSection(&pData->m_crit);
		EnterCriticalSection(&pData->m_crit);
		pData->bKillThread=true;
		pData->bThreadStarted=false;
		pData->nThreadState=0;  //generic
		pData->nThreadControl=0;  //generic
		pData->m_dblLastUpdate=-1.0;
		pData->pszThreadName=NULL;
		strcpy(pData->pszThreadStateText, "");//256 chars max

	// URLs may use format specifiers
		pData->pszBaseURL=NULL;
		pData->pszInfoURL=NULL;
		pData->pszConfirmURL=NULL;

		pData->pszSeriesParamName=NULL;    //  %S
		pData->pszProgramParamName=NULL;   //  %P
		pData->pszEpisodeParamName=NULL;   //  %E

		pData->pszSeriesID=NULL;    //  %s
		pData->pszProgramID=NULL;   //  %p
		pData->pszEpisodeID=NULL;   //  %e

		// some generic parameters
		pData->pszToDateParamName=NULL;				//  %t
		pData->pszFromDateParamName=NULL;			//  %f
		pData->pszNowParamName=NULL;					//  %n
		pData->pszBatchParamName=NULL;				//  %b
		pData->pszFromIDParamName=NULL;				//  %m  // start id
		pData->pszGetIDParamName=NULL;				//  %i

		pData->pszLastID=NULL;  // last ID gotten   %l

		pData->nBatchParam=250;
		pData->nShortIntervalMS=0;
		pData->nLongIntervalMS=5000;
		pData->bDirParamStyle=true;  // true is baseurl/paramname/paramvalue/  false is baseurl?paramname=paramvalue

		_ftime(&pData->timebTick); // the time check inside the thread
		_ftime(&pData->timebLastPullTick); // the last time data pulled inside the thread
		pData->nIDrateAvg=-1;
		pData->nIDrateMax=-1;
		pData->nIDrateMin=-1;
		pData->nNumFailures=0;
		pData->nNumSuccess=0;

		pData->pszHost=NULL;
		pData->nPort=80;
		pData->nAuxValue=-1;

		LeaveCriticalSection(&pData->m_crit);
		return CLIENT_SUCCESS;
	}

	return CLIENT_ERROR;
}


int CTimerData::GetTimers(char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT uid, name, description, type, status, duration, start_tc, stop_tc, \
count, paused_duration, paused_count, last_started, last_stopped, action_type, action_data FROM %s ORDER BY uid",  //HARDCODE
			((g_settings.m_pszTimers)&&(strlen(g_settings.m_pszTimers)))?g_settings.m_pszTimers:"Timers"
			); 
		
/*		

create table Timers (uid  int identity(1,1), name varchar(64), description varchar(256), type int, status int, duration int, start_tc int, stop_tc int, \
count int, paused_duration int, paused_count int, last_started decimal(20,3), last_stopped decimal(20,3), action_type int, action_data varchar(2048))  
   


	int m_nItemID;

	unsigned long m_ulType;  // up count, down count
	unsigned long m_ulStatus;

	unsigned long m_ulDuration; // if a downcounter, the total number of milliseconds to count down.  If an upcounter, it should be the maximum count, or zero if none.
	unsigned long m_ulStartTC; // if a scheduled timer, start from this millisecond offset.
	unsigned long m_ulStopTC; // the time code at finish

	unsigned long m_ulCount; // if a downcounter, the total number of milliseconds left.  If an upcounter, elapsed duration
	unsigned long m_ulPausedDuration; // elapsed milliseconds while paused.
	unsigned long m_ulTimesPaused;
	
	_timeb m_timeLastStarted;
	_timeb m_timeLastStopped;

	char* m_pszName;
	char* m_pszDescription;

	bool m_bKillTimer;
	bool m_bTimerStarted;

	// need something here for "action" to do when the timer finishes.
	unsigned long m_ulActionType;  // up count, down count
	char* m_pszActionData;
	*/

if(g_settings.m_ulDebug&(TIMER_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:GetTimers", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "GetTimers");  Sleep(250); //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "Got Destinations");  Sleep(250); //(Dispatch message)

			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = CLIENT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
if(g_settings.m_ulDebug&(TIMER_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:GetTimers", "index %d", nIndex); // Sleep(250); //(Dispatch message)
}
				CString szName="";
				CString szDesc="";
				CString szActionData="";
				CString szTemp;

				int nItemID = -1;
				bool bItemFound = false;
				bool bFlagsFound = false;
				bool bFound = false;


				unsigned long ulType;  // up count, down count
//				unsigned long ulStatus;

//				unsigned long ulDuration; // if a downcounter, the total number of milliseconds to count down.  If an upcounter, it should be the maximum count, or zero if none.
//				unsigned long ulStartTC; // if a scheduled timer, start from this millisecond offset.
//				unsigned long ulStopTC; // the time code at finish

//				unsigned long ulCount; // if a downcounter, the total number of milliseconds left.  If an upcounter, elapsed duration
//				unsigned long ulPausedDuration; // elapsed milliseconds while paused.
//				unsigned long ulTimesPaused;
				
//				_timeb timeLastStarted;
//				_timeb timeLastStopped;

				unsigned long ulActionType;  // up count, down count

				unsigned long ulFlags = 0; // used for volatile status


				try
				{
					prs->GetFieldValue("uid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bItemFound = true;
						nItemID = atoi(szTemp);
					}
					prs->GetFieldValue("name", szName);//HARDCODE
					szName.TrimLeft(); szName.TrimRight();

					prs->GetFieldValue("description", szDesc);//HARDCODE
					szDesc.TrimLeft(); szDesc.TrimRight();

					prs->GetFieldValue("type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulType = atol(szTemp);
					}

/*

	// these need to be set on the fly
					prs->GetFieldValue("status", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulStatus = atol(szTemp);
					}

					prs->GetFieldValue("duration", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulDuration = atol(szTemp);
					}


					prs->GetFieldValue("start_tc", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulStartTC = atol(szTemp);
					}

					prs->GetFieldValue("stop_tc", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulStopTC = atol(szTemp);
					}
* /

					prs->GetFieldValue("count", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulCount = atol(szTemp);
					}
					prs->GetFieldValue("paused_duration", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulPausedDuration = atol(szTemp);
					}
					prs->GetFieldValue("paused_count", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulTimesPaused = atol(szTemp);
					}
					prs->GetFieldValue("last_started", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						timeLastStarted.time = atol(szTemp);
						timeLastStarted.millitm = ((unsigned short)((atof(szTemp) - ((double)(timeLastStarted.time))) *1000.0));
					}
					prs->GetFieldValue("last_stopped", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						timeLastStopped.time = atol(szTemp);
						timeLastStopped.millitm = ((unsigned short)((atof(szTemp) - ((double)(timeLastStarted.time))) *1000.0));
					}
*/
					prs->GetFieldValue("action_type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulActionType = atol(szTemp);
					}
					prs->GetFieldValue("action_data", szActionData);//HARDCODE
					szActionData.TrimLeft(); szActionData.TrimRight();


					bFlagsFound = true; // made it thru the whole record
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
if(g_settings.m_ulDebug&(TIMER_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:GetTimers", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL); // Sleep(250); //(Dispatch message)
}
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
if(g_settings.m_ulDebug&(TIMER_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:GetTimers", "Retrieve: Caught exception: out of memory\n%s", szSQL); // Sleep(250); //(Dispatch message)
}
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
if(g_settings.m_ulDebug&(TIMER_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:GetTimers", "Retrieve: Caught other exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)
}
					}
					e->Delete();
				} 
				catch( ... )
				{
					if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception.\n%s", szSQL);

if(g_settings.m_ulDebug&(TIMER_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:GetTimers", "Retrieve: Caught exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)
}

				}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "Got Destination: %s (0x%08x)", szName, ulFlags);  Sleep(250); //(Dispatch message)


//////////////////////////////////////////////////////////////////////////////
// temporarily going to just ignore all this, this is only going to be called once at startup and that is it.
// so this will not get hit


/*
				if(0) //(m_ppTimers)&&(m_nTimers))
				{
					int nTemp=0;
					while(nTemp<m_nTimers)
					{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "c2");  Sleep(250); //(Dispatch message)
						if((m_ppTimers[nTemp]))//&&(m_ppTimers[nTemp]->m_pszName))
						{
							// changing the follwing to unique on dest id.
				//			if((szName.GetLength()>0)&&(szName.CompareNoCase(m_ppTimers[nTemp]->m_pszName)==0))
							if(m_ppTimers[nTemp]->m_nItemID == nItemID)
							{
								bFound = true;
								bool bRefresh = false;

								if(
										(szDesc.GetLength()>0)
									&&((m_ppTimers[nTemp]->m_pszDesc==NULL)||(szDesc.CompareNoCase(m_ppTimers[nTemp]->m_pszDesc)))
									)
								{
									if(m_ppTimers[nTemp]->m_pszDesc) free(m_ppTimers[nTemp]->m_pszDesc);

									m_ppTimers[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(m_ppTimers[nTemp]->m_pszDesc) sprintf(m_ppTimers[nTemp]->m_pszDesc, szDesc);
								}

								if(
										(szName.GetLength()>0)
									&&((m_ppTimers[nTemp]->m_pszName==NULL)||(szName.CompareNoCase(m_ppTimers[nTemp]->m_pszName)))
									)
								{
									if(m_ppTimers[nTemp]->m_pszName) free(m_ppTimers[nTemp]->m_pszName);

									m_ppTimers[nTemp]->m_pszName = (char*)malloc(szName.GetLength()+1); 
									if(m_ppTimers[nTemp]->m_pszName) sprintf(m_ppTimers[nTemp]->m_pszName, szName);
								}

								if(bItemFound) m_ppTimers[nTemp]->m_nItemID = nItemID;
								m_ppTimers[nTemp]->m_ulType = ulType;

								if(dblDiskspaceThreshold>0.0) m_ppTimers[nTemp]->m_dblDiskPercent = dblDiskspaceThreshold;
//								m_ppTimers[nTemp]->m_dblDiskKBFree = dblDiskspaceFree;
//								m_ppTimers[nTemp]->m_dblDiskKBTotal = dblDiskspaceTotal;

//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "c3");  Sleep(250); //(Dispatch message)

								if(
										((bFlagsFound)&&(!(ulFlags&CLIENT_FLAG_ENABLED))&&((m_ppTimers[nTemp]->m_ulFlags)&CLIENT_FLAG_ENABLED))
									)
								{
									m_ppTimers[nTemp]->m_bKillMonThread = true;

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
										m_ppTimers[nTemp]->m_pszDesc?m_ppTimers[nTemp]->m_pszDesc:m_ppTimers[nTemp]->m_pszName);  
									g_ptabmsgr->DM(MSG_ICONINFO, NULL, "Timers:destination_change", errorstring);  Sleep(20);  //(Dispatch message)
									m_ppTimers[nTemp]->m_bKillConnThread = true;
									m_ppTimers[nTemp]->m_ulStatus = CLIENT_STATUS_NOTCON;
									if(m_ppTimers[nTemp]->m_socket != NULL)
									{
										m_ppTimers[nTemp]->m_net.CloseConnection(m_ppTimers[nTemp]->m_socket); // re-establish
									}
									m_ppTimers[nTemp]->m_socket = NULL;

								}

								if(
										((bFlagsFound)&&(ulFlags&CLIENT_FLAG_ENABLED)&&(!((m_ppTimers[nTemp]->m_ulFlags)&CLIENT_FLAG_ENABLED)))
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating %s...", 
//										m_ppTimers[nTemp]->m_pszClientName?m_ppTimers[nTemp]->m_pszClientName:"Libretto",  
										m_ppTimers[nTemp]->m_pszDesc?m_ppTimers[nTemp]->m_pszDesc:m_ppTimers[nTemp]->m_pszName);  
									g_ptabmsgr->DM(MSG_ICONINFO, NULL, "Timers:destination_change", errorstring); //  Sleep(20);  //(Dispatch message)
//									m_ppTimers[nTemp]->m_pDlg->OnButtonConnect();
									//**** if connect set following status


									if((m_ppTimers[nTemp]->m_socket == NULL)&&(!m_ppTimers[nTemp]->m_bConnThreadStarted))
									{
										m_ppTimers[nTemp]->m_ulStatus = CLIENT_STATUS_CONN;
										if(_beginthread(LibrettoAsynchConnThread, 0, (void*)(m_ppTimers[nTemp]))==-1)
										{
										//error.

										//**MSG
										}
										else
										{
											int nWait = 0;
											while((m_ppTimers[nTemp]->m_ulStatus == CLIENT_STATUS_CONN)&&(nWait<1000))  // 0.5 seconds?
											{
												nWait++;
												Sleep(1);
											}
										}
									}

									if(!m_ppTimers[nTemp]->m_bMonThreadStarted)
									{
										m_ppTimers[nTemp]->m_bKillMonThread = false;
										if(_beginthread(LibrettoConnMonitorThread, 0, (void*)(m_ppTimers[nTemp]))==-1)
										{
										//error.
									g_ptabmsgr->DM(MSG_ICONERROR, NULL, "Timers:destination_change", "Error starting connection monitor for %s", 
										m_ppTimers[nTemp]->m_pszDesc?m_ppTimers[nTemp]->m_pszDesc:m_ppTimers[nTemp]->m_pszName); //  Sleep(20);  //(Dispatch message)

										//**MSG
										}
									}


									if(!m_ppTimers[nTemp]->m_bCommandQueueThreadStarted)
									{
										m_ppTimers[nTemp]->m_bKillCommandQueueThread = false;
										if(_beginthread(LibrettoCommandQueueThread, 0, (void*)(m_ppTimers[nTemp]))==-1)
										{
										//error.
									g_ptabmsgr->DM(MSG_ICONERROR, NULL, "Timers:destination_change", "Error starting command queue for %s", 
										m_ppTimers[nTemp]->m_pszDesc?m_ppTimers[nTemp]->m_pszDesc:m_ppTimers[nTemp]->m_pszName); //  Sleep(20);  //(Dispatch message)

										//**MSG
										}
									}


									

/*
									if(m_ppTimers[nTemp]->m_socket == NULL)
									{
										m_ppTimers[nTemp]->m_net.OpenConnection(m_ppTimers[nTemp]->m_pszName, g_settings.m_nPort, &m_ppTimers[nTemp]->m_socket); // re-establish
										char tnbuffer[32]; sprintf(tnbuffer, "\\\\%c%c", 13,10);
										m_ppTimers[nTemp]->SendTelnetCommand(tnbuffer, 2);
										unsigned char* pucBuffer = NULL;
										unsigned long ulBufferLen = 256;
										m_ppTimers[nTemp]->m_net.GetLine(&pucBuffer, &ulBufferLen, m_ppTimers[nTemp]->m_socket);
										if(pucBuffer) free(pucBuffer);
									}
* /
									bRefresh = true;
//									m_ppTimers[nTemp]->m_bKillConnThread = false;
	//								if(_beginthread(LibrettoConnectionThread, 0, (void*)m_ppTimers[nTemp])<0)
	//								{
										//error.

										//**MSG
							
										
		//							}
									//**** should check return value....

									m_ppTimers[nTemp]->m_ulStatus = CLIENT_STATUS_CONN;
								}

								if(bFlagsFound) m_ppTimers[nTemp]->m_ulFlags = ulFlags|CLIENT_FLAG_FOUND;
								else m_ppTimers[nTemp]->m_ulFlags |= CLIENT_FLAG_FOUND;

								if(bRefresh)m_ppTimers[nTemp]->m_ulFlags |= CLIENT_FLAG_REFRESH;

							}
						}
						nTemp++;
					}
				}
// temporarily going to just ignore all this, this is only going to be called once at startup and that is it.
// so this (above) will not get hit
//////////////////////////////////////////////////////////////////////////////

*/



				if((!bFound)&&(szName.GetLength()>0)) // have to add.
				{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "adding %s", szName);  Sleep(250); //(Dispatch message)
					CTimerItem* pscno = new CTimerItem;
					if(pscno)
					{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "new obj for %s", szName);  Sleep(250); //(Dispatch message)
						CTimerItem** ppObj = new CTimerItem*[m_nTimers+1];
						if(ppObj)
						{
				
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "new array for %s", szName);  Sleep(250); //(Dispatch message)
							int o=0;
							if((m_ppTimers)&&(m_nTimers>0))
							{
								while(o<m_nTimers)
								{
									ppObj[o] = m_ppTimers[o];
									o++;
								}
								delete [] m_ppTimers;

							}
							ppObj[o] = pscno;
							m_ppTimers = ppObj;
							m_nTimers++;

							ppObj[o]->m_pszName = (char*)malloc(szName.GetLength()+1); 
							if(ppObj[o]->m_pszName) sprintf(ppObj[o]->m_pszName, "%s", szName);

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}
							if(bItemFound) ppObj[o]->m_nItemID = nItemID;
							ppObj[o]->m_ulType = ulType;

							if(szActionData.GetLength()>0)
							{
								ppObj[o]->m_pszActionData = (char*)malloc(szActionData.GetLength()+1); 
								if(ppObj[o]->m_pszActionData) sprintf(ppObj[o]->m_pszActionData, "%s", szActionData);
							}
							ppObj[o]->m_ulActionType = ulActionType;



//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", " added flags: %s (0x%08x) %d", szName, ppObj[o]->m_ulFlags, ((ppObj[o]->m_ulFlags)&CLIENT_FLAG_ENABLED));  Sleep(250); //(Dispatch message)


							ppObj[o]->m_ulFlags |= TIMER_FLAG_FOUND;
						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			// and insert media list refresh into the queue for all new destinatons.
			nIndex = 0;
			while(nIndex<m_nTimers)
			{
				if((m_ppTimers)&&(m_ppTimers[nIndex]))
				{

					if((m_ppTimers[nIndex]->m_ulFlags)&TIMER_FLAG_FOUND)
					{
						(m_ppTimers[nIndex]->m_ulFlags) &= ~TIMER_FLAG_FOUND;

						nIndex++;
					}
					else
					{
						if(m_ppTimers[nIndex]->m_bTimerStarted)
						{
							int nWait =  clock()+1500;
							m_ppTimers[nIndex]->m_bKillTimer = true;

							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
									m_ppTimers[nIndex]->m_pszDesc?m_ppTimers[nIndex]->m_pszDesc:m_ppTimers[nIndex]->m_pszName);  
								if(g_ptabmsgr)  g_ptabmsgr->DM(MSG_ICONINFO, NULL, "Timers:remove_timer", errorstring);  // Sleep(20);  //(Dispatch message)
//									m_ppTimers[nTemp]->m_pDlg->OnDisconnect();


							//**** disconnect
//								while(m_ppTimers[nIndex]->m_bConnThreadStarted) Sleep(1);

							while(
										 (
										   (m_ppTimers[nIndex]->m_bTimerStarted)
										 )
										&&
										 (clock()<nWait)
									 )
							{
								Sleep(1);
							}

							m_nTimers--;

							int nTemp=nIndex;
							while(nTemp<m_nTimers)
							{
								m_ppTimers[nTemp]=m_ppTimers[nTemp+1];
								nTemp++;
							}
							m_ppTimers[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
	}
//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return CLIENT_ERROR;
}


int CTimerData::FindTimer(char* pszTimerName)
{
	if((pszTimerName)&&(m_ppTimers))
	{
		int nTemp=0;
		while(nTemp<m_nTimers)
		{
			if(m_ppTimers[nTemp])
			{
				if(m_ppTimers[nTemp]->m_pszName)
				{
					if(stricmp(m_ppTimers[nTemp]->m_pszName, pszTimerName)==0) return nTemp;
				}
			}
			nTemp++;
		}
	}
	return CLIENT_ERROR;

}
