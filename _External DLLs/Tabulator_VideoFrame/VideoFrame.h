// VideoFrame.h : main header file for the VideoFrame DLL
//

#if !defined(AFX_VIDEOFRAME_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
#define AFX_VIDEOFRAME_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#import "msxml3.dll" named_guids 
using namespace MSXML2;

#include "../../../Applications/Generic/Client/ClientDefines.h"
#include "../../../Common/MSG/Messager.h"
#include "../../../Common/TXT/FileUtil.h"
#include "..\..\..\Common\API\Videoframe\SIMPLE.h"

//#include "..\..\..\..\Common\MFC\Inet\Inet.h"
//#include "..\..\..\..\Common\API\Miranda\IS2Core.h"  // for IS2 util functions


#include "VideoFrameCore.h"
#include "VideoFrameData.h"
#include "VideoFrameSettings.h"

#define SETTINGS_FILENAME "videoframe.ini"
#define VERSION_STRING "1.0.0.4"

/*
#define VIDEOFRAME_DATA 0
#define VIDEOFRAME_TICKER 1
#define VIDEOFRAME_UI 2
#define VIDEOFRAME_THREADS 3
*/
// from cortex.h but static so just define here.
//send message type
#define CX_SENDMSG_ERROR   0
#define CX_SENDMSG_INFO	   1
#define CX_SENDMSG_NOTE	   2

#define VIDEOFRAME_ERROR					-1
#define VIDEOFRAME_SUCCESS				0


#define VIDEOFRAME_BUTTON_STATUS_INDETERMINATE		0xff
#define VIDEOFRAME_BUTTON_STATUS_OFF							0x00
#define VIDEOFRAME_BUTTON_STATUS_ON							0x01


// FLAGS!
#define VIDEOFRAME_BUTTON_DISABLED			0x00
#define VIDEOFRAME_BUTTON_ENABLED			0x01

// some disney specific types.
#define VIDEOFRAME_BUTTON_TYPE_MASK				0xf0
#define VIDEOFRAME_BUTTON_TYPE_NONE				0x00
#define VIDEOFRAME_BUTTON_TYPE_AUTO				0x10
#define VIDEOFRAME_BUTTON_TYPE_PASS				0x20
#define VIDEOFRAME_BUTTON_TYPE_BLOCK				0x30
#define VIDEOFRAME_BUTTON_TYPE_INDICATOR		0x40
#define VIDEOFRAME_BUTTON_TYPE_MANUAL			0x50

// interface types
#define VIDEOFRAME_REQUEST_TYPE_GPO				2
#define VIDEOFRAME_REQUEST_TYPE_KEYPRESS		6

// manual event types
#define VIDEOFRAME_MANUAL_EVENT_TYPE_NONE					0
#define VIDEOFRAME_MANUAL_EVENT_BREAK_END					1
#define VIDEOFRAME_MANUAL_EVENT_NATIONAL_BREAK		2


#define VIDEOFRAME_DEBUG_SQL						0x00000001
#define VIDEOFRAME_DEBUG_CALLFUNC			0x00000002
#define VIDEOFRAME_DEBUG_BUTTONPRESS		0x00000004
#define VIDEOFRAME_DEBUG_GPOSET				0x00000008
#define VIDEOFRAME_DEBUG_MANEVT				0x00000010
#define VIDEOFRAME_DEBUG_INIT					0x00000020
#define VIDEOFRAME_DEBUG_EVENTHANDLER	0x00000040


/*

#define VIDEOFRAME_DATA_ERROR			0x0001
#define VIDEOFRAME_DATA_STOPPED		0x0000
#define VIDEOFRAME_DATA_STARTED		0x0010
#define VIDEOFRAME_DATA_PAUSED		  0x0040
#define VIDEOFRAME_DATA_INPROG		  0x0080

#define VIDEOFRAME_TICKER_ERROR		0x0001
#define VIDEOFRAME_TICKER_STOPPED  0x0000
#define VIDEOFRAME_TICKER_STARTED  0x0010
#define VIDEOFRAME_TICKER_CYCLING  0x0020
#define VIDEOFRAME_TICKER_PAUSED	  0x0040
#define VIDEOFRAME_TICKER_INPROG	  0x0080

#define VIDEOFRAME_SYNCH_ERROR			0x0001
#define VIDEOFRAME_SYNCH_STOPPED		0x0000
#define VIDEOFRAME_SYNCH_STARTED		0x0010

#define VIDEOFRAME_DEBUG_STATUS			0x00000001
#define VIDEOFRAME_DEBUG_FEED				0x00000002
#define VIDEOFRAME_DEBUG_TICKER			0x00000004
#define VIDEOFRAME_DEBUG_EVENTS			0x00000008
#define VIDEOFRAME_DEBUG_TRIGGER			0x00000010
#define VIDEOFRAME_DEBUG_PARSE				0x00000020
#define VIDEOFRAME_DEBUG_SQL					0x00000040

#define VIDEOFRAME_TICKER_ENGINE_INT		0x0000  // internal ticker engine
#define VIDEOFRAME_TICKER_ENGINE_VGE		0x0001  // VDS Generic Engine (by wyk)

#define VIDEOFRAME_EVENT_IN		0x01
#define VIDEOFRAME_EVENT_OUT		0x02
#define VIDEOFRAME_EVENT_TEXT	0x04
#define VIDEOFRAME_EVENT_MOD		0x80

// statuses
/*
#define MSG_STATUS_PLAYED			0x01 // played 
#define MSG_STATUS_ONAIR			0x02 // on air now!
#define MSG_STATUS_ARCHIVED		0x04 // archived
#define MSG_STATUS_COMMIT			0x08 // sent to ticker engine
#define MSG_STATUS_ERROR			0x80 // error

/*
#define DB_ATTRIB_NONE				0x00 // attribute - convenience, reset
#define DB_ATTRIB_MAX					0x10 // highest independent value
#define DB_ATTRIB_NOARC				0x01 // attribute - do not archive item after show - delete after show
#define DB_ATTRIB_REINSNOW		0x02 // attribute - reinsert into active playstack at end (for cycling within clip)
#define DB_ATTRIB_REINSAFTER	0x04 // attribute - reinsert into active playstack at end (for cycling within show, plays once per clip)
#define DB_ATTRIB_SCHED				0x08 // attribute - do not play until after unixdate kept in dwTimeOnAir
#define DB_ATTRIB_EXP					0x10 // attribute - expire item after unixdate kept in dwDuration 
*/

/////////////////////////////////////////////////////////////////////////////
// CVideoFrameApp
// See VideoFrame.cpp for the implementation of this class
//

class CVideoFrameApp : public CWinApp
{
public:
	CVideoFrameApp();
	int  DLLCtrl(void** ppvoid, UINT nType);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVideoFrameApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	char m_szSettingsFilename[MAX_PATH];

	//{{AFX_MSG(CVideoFrameApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VIDEOFRAME_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
