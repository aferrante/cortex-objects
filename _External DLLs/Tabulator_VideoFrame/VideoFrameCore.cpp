// VideoFrameCore.cpp: implementation of the CVideoFrameCore class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VideoFrame.h"
#include "VideoFrameCore.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


extern CVideoFrameData			g_data;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVideoFrameCore::CVideoFrameCore()
{

}

CVideoFrameCore::~CVideoFrameCore()
{

}


BOOL CVideoFrameCore::IsPointInWnd(CPoint point, CWnd* pWnd) 
{
	CRect rcRect;
	pWnd->GetWindowRect(&rcRect);
	if(pWnd)
		return IsPointInRect( point,  rcRect); 
	else
		return FALSE;
}

BOOL CVideoFrameCore::IsPointInRect(CPoint point, CRect rcWindowRect) 
{
	if(	(point.x>rcWindowRect.left)&&
			(point.x<rcWindowRect.right)&&
			(point.y>rcWindowRect.top)&&
			(point.y<rcWindowRect.bottom)	)
		return TRUE;
	else
		return FALSE;
}

// This function will encode a line of text for display
// under CAL.  Call this BEFORE adding any escape 
// sequences (such as [f 1] to change the font to font 1)
CString CVideoFrameCore::CALEncodeBrackets(CString szString)
{
  int k;
	char ch;
  for (k=0; k<szString.GetLength(); k++)
  {
		ch = szString.GetAt(k);
    if (( ch == '[')||( ch == '\\'))
    {
      szString = szString.Left(k) + "\\" + 
        szString.Right(szString.GetLength() - k);
      k++;
    }
  }
  return szString;
}
