// VideoFrameData.cpp: implementation of the CVideoFrameData and CVideoFrameMessage classes.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VideoFrame.h"
#include "VideoFrameData.h"
#include "VideoFrameSettings.h"
#include <atlbase.h>
#include <process.h>
//#include "../../../Common/SNMP/SNMPUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


#define SENDMSG_EVENT_MAXLEN 64
#define SENDMSG_SENDER_MAXLEN 64
#define SENDMSG_MESSAGE_MAXLEN 512

void DelayButtonActionThread(void* pvArgs);
void VF200ServerHandlerThread(void* pvArgs);


extern CVideoFrameData	g_data;
extern CVideoFrameSettings	g_settings;
extern CMessager*			g_ptabmsgr;
extern DLLdata_t       g_dlldata;

//CSNMPUtil u;  //global just so the message number is global
CVFSimpleUtil u;

CVideoFrameButton::CVideoFrameButton()
{
	m_nItemID = -1; // i.e. unassigned.
	strcpy(m_pchButtonOnLabel,"");
	strcpy(m_pchButtonOffLabel,"");
	strcpy(m_pchTallyEventLabel,"");
	strcpy(m_pchTallyEventValue,"1");
	strcpy(m_pchClickEventLabel,"");

	m_nLastClickValue = -1; // boolean (toggle)

	m_ulStatus = VIDEOFRAME_BUTTON_STATUS_INDETERMINATE;  // to start off with
	m_ulFlags = VIDEOFRAME_BUTTON_DISABLED;

	// manual events association
	strcpy(m_pchManualEventName,"");
	strcpy(m_pchManualEventProgID,"");
	m_nManualEventType = VIDEOFRAME_MANUAL_EVENT_TYPE_NONE;
	m_nManualEventDuration = -1; // invalid value...
}

CVideoFrameButton::~CVideoFrameButton()
{
}

int CVideoFrameButton::UpdateStatus()
{
	if(( g_data.m_pdb )&&(	g_data.m_pdbConn) && (m_ulStatus != VIDEOFRAME_BUTTON_STATUS_INDETERMINATE)) // do not update with unknown values
	{

		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d WHERE buttonID = %d",  //HARDCODE
				((g_settings.m_pszButtons)&&(strlen(g_settings.m_pszButtons)))?g_settings.m_pszButtons:"Buttons",
				m_ulStatus,
				m_nItemID
				);

//			Sleep(1); // dont peg processor
if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_SQL)) 
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:button_update", "SQL: %s", szSQL); // Sleep(250); //(Dispatch message)

EnterCriticalSection(&g_data.m_critSQL);
		int nReturn = g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring);
		if(nReturn<DB_SUCCESS)
		{
			//**MSG
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "VideoFrame:button_update", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

		}
LeaveCriticalSection(&g_data.m_critSQL);







		return nReturn;
	}
	return VIDEOFRAME_ERROR;
}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVideoFrameData::CVideoFrameData()
{

	m_szDTMF = _T("");
	InitializeCriticalSection(&m_critDTMF);
//	m_pEvents = NULL;
//	m_pInEvents = new CTabulatorEventArray;
//	m_pOutEvents = new CTabulatorEventArray;
//	m_pTextEvents = new CTabulatorEventArray;

//	m_socket=NULL;

	m_bButtonsInitialized = FALSE;
	m_bInitConn = FALSE;		// initialization condition
	m_bInitAuto = FALSE;		// initialization condition
	m_bInitPass = FALSE;		// initialization condition
	m_bInitBlock = FALSE;	// initialization condition
	m_bInitAutoClick = FALSE;		// initialization condition
	m_bInitPassClick = FALSE;		// initialization condition
	m_bInitBlockClick = FALSE;	// initialization condition

	m_bInVideoFrameCommand = FALSE;
	m_s=NULL;

	m_bInCommand = FALSE;
	m_socketTabulator = NULL;

	m_pdb=NULL;
	m_pdbConn=NULL;
	InitializeCriticalSection(&m_critSQL);

//	m_ppButtons=NULL;
//	m_nNumButtons=0;
	InitializeCriticalSection(&m_critButtons);
	memset(((char *)&m_saiGTPhost), 0, (sizeof (m_saiGTPhost)));

}

CVideoFrameData::~CVideoFrameData()
{
	DeleteCriticalSection(&m_critDTMF);

//	DeleteCriticalSection(&m_critClientSocket);
	DeleteCriticalSection(&m_critSQL);
//	if(	m_pInEvents) delete  m_pInEvents;
//	if(	m_pOutEvents) delete  m_pOutEvents;
//	if(	m_pTextEvents) delete  m_pTextEvents;

	// should delete the buttons
	DeleteCriticalSection(&m_critButtons);
}

// remove pipes
CString CVideoFrameData::Encode(CString szText)
{
  CString szOut = "";
  int i;
  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '|')
      szOut += "%:";
    else if (szText[i] == '%')
      szOut += "%%";
    else if ((szText[i] != '\n')&&(szText[i] != '\r'))  //strip returns while we are at it.
       szOut += (char) szText[i];
  }
  return szOut;
}

// put back pipes
CString CVideoFrameData::Decode(CString szText)
{
  CString szOut = "";
  int i;

  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '%')
    {
      i++; if (i>=szText.GetLength()) break;
      if (szText[i] == '%')
      {
        szOut += "%";
      }
      else if (szText[i] == ':')
      {
        szOut += "|";
      }
    }
    else szOut += (char) szText[i];
  }
  return szOut;
}


CString CVideoFrameData::XMLTextNodeValue(IXMLDOMNodePtr pNode)
{
	CString szText = _T("");
	if(pNode)
	{
		IXMLDOMNodeListPtr pValues = pNode->GetchildNodes();
		if (pValues != NULL)
		{
			IXMLDOMNodePtr pValue;
			while(pValue = pValues->nextNode())
			{
				DOMNodeType valnodeType;
				pValue->get_nodeType(&valnodeType);

				if(valnodeType == NODE_TEXT)
				{
USES_CONVERSION;
					szText = W2T(_bstr_t(pValue->GetnodeValue()));
					szText.TrimLeft();
					szText.TrimRight();
					return szText;
				}
			}
		}
	}
	return szText;
}

int CVideoFrameData::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszMessages)&&(strlen(g_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		if(g_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime) \
VALUES ('%s', '%s', %d, %d.%03d)", // HARDCODE
				g_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				g_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				g_settings.m_pszMessages
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		if((g_ptabmsgr)&&(g_settings.m_ulDebug&VIDEOFRAME_DEBUG_SQL)) 
		{
			g_ptabmsgr->DM(MSG_ICONINFO, NULL, "Videoframe:send_msg", "%s", szSQL );
		}


		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
//			IncrementDatabaseMods(g_settings.m_pszMessages, dberrorstring);
		LeaveCriticalSection(&m_critSQL);
			return TABULATOR_SUCCESS;
		}
		else
		{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "Videoframe:send_msg", "Error executing SQL: %s", dberrorstring);//   Sleep(250);//(Dispatch message)
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return TABULATOR_ERROR;
}

/*

int CVideoFrameData::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszMessages)&&(strlen(g_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			g_settings.m_pszMessages,
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			g_settings.m_pszMessages
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
//			IncrementDatabaseMods(g_settings.m_pszMessages, dberrorstring);
			return TABULATOR_SUCCESS;
		}
	}
	return TABULATOR_ERROR;
}
*/


int CVideoFrameData::SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...)
{
 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszAsRun)&&(strlen(g_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		if((pszEventID)&&(strlen(pszEventID)>SENDMSG_EVENT_MAXLEN)) pszEventID[SENDMSG_EVENT_MAXLEN-1]=0;
		
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);
		char* pchEvent = m_pdb->EncodeQuotes(pszEventID);
		if(g_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, channelid, eventid) \
VALUES ('%s', '%s', %d, %d.%03d, %d, '%s')", // HARDCODE
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm,
				nChannelID,
				pchEvent?pchEvent:""
				);
		}
		else
		{

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log")
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
		if(pchEvent) free(pchEvent);

		if((g_ptabmsgr)&&(g_settings.m_ulDebug&VIDEOFRAME_DEBUG_SQL)) 
		{
			g_ptabmsgr->DM(MSG_ICONINFO, NULL, "Videoframe:send_asrun_msg", "%s", szSQL );
		}

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
//			IncrementDatabaseMods(g_settings.m_pszAsRun, dberrorstring);
			return TABULATOR_SUCCESS;
		}
		else
		{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "Videoframe:send_asrun_msg", "Error executing SQL: %s", dberrorstring);//   Sleep(250);//(Dispatch message)
		}
		LeaveCriticalSection(&m_critSQL);

	}
	return TABULATOR_ERROR;
}



/*
was this, but need to use new table def
int CVideoFrameData::SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszAsRun)&&(strlen(g_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log")
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Videoframe_Asrun", "SQL %s", szSQL) ;// Sleep(100); //(Dispatch message)

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
//			IncrementDatabaseMods(g_settings.m_pszAsRun, dberrorstring);
			return TABULATOR_SUCCESS;
		}
		else
		{
if(g_ptabmsgr)
{
	char szSource[MAX_PATH]; sprintf(szSource, "%s_Asrun", g_settings.m_pszInternalAppName);
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "SQL error %s", dberrorstring) ;// Sleep(100); //(Dispatch message)
}
		}
		LeaveCriticalSection(&m_critSQL);

	}
	else
		{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Videoframe_Asrun", "null conditions");// Sleep(100); //(Dispatch message)
		}
	return TABULATOR_ERROR;
}
*/



int CVideoFrameData::InitDLLData(DLLthread_t* pData)
{
	if(pData)
	{
		InitializeCriticalSection(&pData->m_crit);
		EnterCriticalSection(&pData->m_crit);
		pData->bKillThread=true;
		pData->bThreadStarted=false;
		pData->nThreadState=0;  //generic
		pData->nThreadControl=0;  //generic
		pData->m_dblLastUpdate=-1.0;
		pData->pszThreadName=NULL;
		strcpy(pData->pszThreadStateText, "");//256 chars max

	// URLs may use format specifiers
		pData->pszBaseURL=NULL;
		pData->pszInfoURL=NULL;
		pData->pszConfirmURL=NULL;

		pData->pszSeriesParamName=NULL;    //  %S
		pData->pszProgramParamName=NULL;   //  %P
		pData->pszEpisodeParamName=NULL;   //  %E

		pData->pszSeriesID=NULL;    //  %s
		pData->pszProgramID=NULL;   //  %p
		pData->pszEpisodeID=NULL;   //  %e

		// some generic parameters
		pData->pszToDateParamName=NULL;				//  %t
		pData->pszFromDateParamName=NULL;			//  %f
		pData->pszNowParamName=NULL;					//  %n
		pData->pszBatchParamName=NULL;				//  %b
		pData->pszFromIDParamName=NULL;				//  %m  // start id
		pData->pszGetIDParamName=NULL;				//  %i

		pData->pszLastID=NULL;  // last ID gotten   %l

		pData->nBatchParam=250;
		pData->nShortIntervalMS=0;
		pData->nLongIntervalMS=5000;
		pData->bDirParamStyle=true;  // true is baseurl/paramname/paramvalue/  false is baseurl?paramname=paramvalue

		_ftime(&pData->timebTick); // the time check inside the thread
		_ftime(&pData->timebLastPullTick); // the last time data pulled inside the thread
		pData->nIDrateAvg=-1;
		pData->nIDrateMax=-1;
		pData->nIDrateMin=-1;
		pData->nNumFailures=0;
		pData->nNumSuccess=0;

		pData->pszHost=NULL;
		pData->nPort=80;
		pData->nAuxValue=-1;

		LeaveCriticalSection(&pData->m_crit);
		return CLIENT_SUCCESS;
	}

	return CLIENT_ERROR;
}

/*
int CVideoFrameData::SendVideoFrameCommand(int nType, int nIndex, int nValue) // type like button press or GPO, nValue will be 0 or 1, basically on or off.
{
	char szError[8096];
	if((nIndex>0)&&(nIndex<87)) // hardcoded , 86 is the max
	{
//u.CreateSNMPMessage(SNMP_SETREQUESTPDU, m_szCommunity.GetBuffer(0), "1.3.6.1.4.1.21541.6.1.0", pVal, 1); // this is a stand in value

		CSNMPMessage* pMessage = new CSNMPMessage;
		if(pMessage)
		{
			if(m_s == NULL) Connect();

			int nReturn = VIDEOFRAME_ERROR;
			if(m_s == NULL)
			{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "VideoFrame", "SendCommand - connection was NULL!"); Sleep(500); //(Dispatch message)
			}
			else
			{
				unsigned char* buffer = NULL;
				unsigned long buflen=0;
				unsigned char ucMsg = u.IncrementMessageNumber();
				pMessage->m_ulRequest = (unsigned long)ucMsg;

				pMessage->m_ucPDUType = SNMP_SETREQUESTPDU;

				// fill out all the other stuff
				if(pMessage->m_pchOID)
				{
					free(pMessage->m_pchOID);
				}
				char oidbuffer[256];
				sprintf(oidbuffer, "1.3.6.1.4.1.21541.%d.%d.0", nType, nIndex);

				pMessage->m_pchOID = (char*)malloc(strlen(oidbuffer)+1);
				if(pMessage->m_pchOID)
				{
					strcpy(pMessage->m_pchOID, oidbuffer);
				}

				pMessage->m_pchCommunity = (char*)malloc(strlen("public")+1); //hardcoded
				if(pMessage->m_pchCommunity)
				{
					strcpy(pMessage->m_pchCommunity, "public");
				}

				// change the value.
				if(pMessage->m_pucValue)
				{
					free(pMessage->m_pucValue);
				}
				unsigned char* pVal = (unsigned char*)malloc(2);
				if(pVal)
				{
					pVal[0] = nValue;
					pMessage->m_ulValueLength=1;
				}
				else pMessage->m_ulValueLength=0;
				pMessage->m_pucValue = pVal; // even if null

				buffer = u.ReturnMessageBuffer(pMessage);
				if(buffer)
				{	
					buflen = u.ReturnLength(&buffer[1]);
					buflen += u.ReturnLength(buflen)+1;
				}
				
	//			CBufferUtil bu;
	//unsigned long ulSend = buflen;
	//char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_DELIM32BIT);
	//AfxMessageBox(pchSnd);

	unsigned long ulSend = buflen;
	char* pchSnd = m_bu.ReadableHex((char*)buffer, &ulSend, MODE_FULLHEX);

				while(m_bInVideoFrameCommand) Sleep(1); // wait until not busy
				m_bInVideoFrameCommand = TRUE;

				nReturn =	m_net.SendLine(buffer, buflen, m_s, EOLN_NONE, false, 5000, szError);
				if(nReturn<NET_SUCCESS)
				{
					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "VideoFrame:SEND", "Error sending command, return code %d %s",nReturn,szError); // Sleep(250); //(Dispatch message)
					Disconnect();
					Connect();
				}
				else
				{

//	if(g_settings.m_bLogTransactions)
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:SEND", "Sent %d bytes: %s", buflen,
			pchSnd?pchSnd:(char*)buffer); // Sleep(250); //(Dispatch message)
	}

					unsigned char* puc = NULL;
					unsigned long ulLen=0;

					nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);
					if(nReturn<NET_SUCCESS)
					{
						if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "VideoFrame:RECV", "Error sending command, return code %d %s",nReturn,szError); // Sleep(250); //(Dispatch message)
						Disconnect();
						Connect();
					}
					else
					{
						if(ulLen==0) //disconnected
						{
							Disconnect();
							Connect();
						}
						else
						{

		unsigned long ulRecv = ulLen;
		char* pchRecv = m_bu.ReadableHex((char*)puc, &ulRecv, MODE_FULLHEX);

//		if(g_settings.m_bLogTransactions)
		{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:SEND", "Recd %d bytes: %s", ulLen,
				pchRecv?pchRecv:(char*)puc); // Sleep(250); //(Dispatch message)
		}
						//		free(puc); // dont free it!
								if(pchRecv) free(pchRecv);
						}
					}

					if(puc) free(puc);
				}
			
				m_bInVideoFrameCommand = FALSE;
				if(pchSnd) free(pchSnd);

				if(buffer) free(buffer);
			}


			delete pMessage;

			return nReturn;

		}
		else
		{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "VideoFrame:ERROR", "No message object");
		}

	}

	return VIDEOFRAME_ERROR;

}
*/

int CVideoFrameData::SendTabulatorCommand(unsigned char** ppucDataBuffer, char* pszAsRunEventID, bool bSuppressAsRun)
{
	if((m_socketTabulator)&&(ppucDataBuffer))
	{
		CNetData data;
		while(m_bInCommand) Sleep(1);
		m_bInCommand =TRUE;

		unsigned char* pucOrig = *ppucDataBuffer;
		unsigned char* puc = NULL;
		int nLen = strlen((char*)(*ppucDataBuffer));
		if((*ppucDataBuffer)&&(nLen)) puc = (unsigned char*)malloc(nLen+1);
		if(puc) memcpy(puc, *ppucDataBuffer, nLen+1); // includes term 0;
		data.m_ucCmd = TABULATOR_CMD_PLUGINCALL;
		data.m_pucData=puc;
		data.m_ulDataLen = (puc?nLen:0);
		data.m_ucType = ((NET_TYPE_PROTOCOL1|NET_TYPE_KEEPOPEN|NET_SND_KEEPOPENLCL)|(*ppucDataBuffer?NET_TYPE_HASDATA:0)); // has data but no subcommand.
		int n = m_net.SendData(&data, m_socketTabulator, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT);//|NET_SND_NO_RX);
		if(data.m_pucData!=NULL)
		{
			*ppucDataBuffer = data.m_pucData;  //return the buffer;
			data.m_pucData=NULL;  // null it so that it does not get freed on destructor
		}
		else
		{
			*ppucDataBuffer = NULL;
		}
		if(n < NET_SUCCESS)
		{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "Videoframe_Tabulator_command", "Net error %d sending cmd 0x%02x buffer: %s", n, TABULATOR_CMD_PLUGINCALL, (*ppucDataBuffer)?((char*)(*ppucDataBuffer)):"");   //(Dispatch message)
			m_net.CloseConnection(m_socketTabulator);
			m_socketTabulator = NULL;
			m_bInCommand =FALSE;

			//ASRUN
if(!bSuppressAsRun) g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, "Error %d sending to %s at %s:%d with: %s", 
										n,
										g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
										g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
										g_settings.m_nTabulatorPort, 
										(char*)(*ppucDataBuffer));

		}
		else
		{
			m_net.SendData(NULL, m_socketTabulator, 5000, 0, NET_SND_CLNTACK, NULL);
			m_bInCommand =FALSE;

if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_CALLFUNC))
{ 
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "VideoFrame", "Successfully executed %s with return data: %02x,%02x %s (%d bytes data) - returning %d", 
									(pucOrig?((char*)pucOrig):"null"),
									data.m_ucCmd,    // the command byte
									data.m_ucSubCmd,   // the subcommand byte
									(((*ppucDataBuffer)&&(data.m_ulDataLen))?(char*)*ppucDataBuffer:""),
									data.m_ulDataLen,
									(data.m_ucSubCmd&0x80)?((int)(data.m_ucSubCmd - 256)):((int)data.m_ucSubCmd)
									);
}

			//ASRUN
if(!bSuppressAsRun) g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, "Successfully executed %s with return data: %02x,%02x %s (%d bytes data)", 
									(pucOrig?((char*)pucOrig):"null"),
									data.m_ucCmd,    // the command byte
									data.m_ucSubCmd,   // the subcommand byte
									(((*ppucDataBuffer)&&(data.m_ulDataLen))?(char*)*ppucDataBuffer:""),
									data.m_ulDataLen
									);
//			return VIDEOFRAME_SUCCESS;

// since it is an unsigned char we are getting back, we have a choice to make.
// either negative values were converted, and we have -1 == 0xff... or something.  But I think this will be the most common so let's use
// twos complement to return an int value between -128 and 127.

			if(data.m_ucSubCmd&0x80)
			{
				// negative value!
				return (int)(data.m_ucSubCmd - 256);
			}
			else
			{
				// positive value or zero.
				return (int)data.m_ucSubCmd;
			}
		}
	}
	return VIDEOFRAME_ERROR;
}





int CVideoFrameData::GetManualEvents(char* pszInfo)
{
	return -69;
}



int CVideoFrameData::GPIMessage(char** ppchMsg, int nSequence)
{
	if(ppchMsg)
	{
		char* p = (char*)malloc(256);
		
		if(p)
		{
			int i=0;
			while (i < g_settings.m_nNumButtons)
			{
				*(p+i) = (m_Buttons[i].m_ulStatus==VIDEOFRAME_BUTTON_STATUS_ON)?'1':'0';  // has to be == because could be VIDEOFRAME_BUTTON_STATUS_INDETERMINATE
				i++;
			}
			*(p+i) = 0;
			char buffer[64];

			sprintf(buffer, ",%d", nSequence);
			strcat(p, buffer);

/*
			sprintf(*ppchMsg, "%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d,%d",
				m_bTallyState[0],
				m_bTallyState[1],
				m_bTallyState[2],
				m_bTallyState[3],
				m_bTallyState[4],
				m_bTallyState[5],
				m_bTallyState[6],
				m_bTallyState[7],
				m_bTallyState[8],
				m_bTallyState[9],
				m_bTallyState[10],
				m_bTallyState[11],
				m_bTallyState[12],
				m_bTallyState[13],
				m_bTallyState[14],
				m_bTallyState[15],
				u.m_nSequence);
*/

			*ppchMsg = p;

			return 0;
		}

	}
	return -1;
}

int CVideoFrameData::ButtonEventAction(int nButton)
{
	// here we send a tabulator event if the action is a manual event press.
	if((nButton>=0)&&(nButton<VF_MAX_BUTTONS)) 
	{
		if(g_data.m_Buttons[nButton].m_nManualEventType == VIDEOFRAME_MANUAL_EVENT_BREAK_END)// remove this &&(g_data.m_Buttons[nButton].m_ulFlags&VIDEOFRAME_BUTTON_ENABLED))
			// let enable just deal with the interface and external calls, but NOT GPI changes from VF
		{
			if(
				  (strlen(g_data.m_Buttons[nButton].m_pchManualEventProgID)>0)
				&&(g_data.m_Buttons[nButton].m_nManualEventDuration>=0)
				)
			{
				char pszToken[MAX_PATH];

				if((g_data.m_Buttons[nButton].m_pchClickEventLabel)&&(strlen(g_data.m_Buttons[nButton].m_pchClickEventLabel)>0))
				{
					_snprintf(pszToken, MAX_PATH-1, "%s|%s|%s|%d", 
						g_data.m_Buttons[nButton].m_pchClickEventLabel, 
						g_settings.m_pszTabulatorCommandBreakEnd?g_settings.m_pszTabulatorCommandBreakEnd:"Break_End",
						g_data.m_Buttons[nButton].m_pchManualEventProgID,
						g_data.m_Buttons[nButton].m_nManualEventDuration
						);
				}
				else
				{
					_snprintf(pszToken, MAX_PATH-1, "%s|%s|%s|%d", 
						g_settings.m_pszTabulatorPlugInModule?g_settings.m_pszTabulatorPlugInModule:"StreamData", 
						g_settings.m_pszTabulatorCommandBreakEnd?g_settings.m_pszTabulatorCommandBreakEnd:"Break_End",
						g_data.m_Buttons[nButton].m_pchManualEventProgID,
						g_data.m_Buttons[nButton].m_nManualEventDuration
						);
				}
				unsigned char* pucDataBuffer = (unsigned char*)pszToken;

				int nReturn = g_data.SendTabulatorCommand(&pucDataBuffer, g_data.m_Buttons[nButton].m_pchManualEventName, true);

				if((pucDataBuffer)&&(pucDataBuffer != (unsigned char*)pszToken))
				{
					free(pucDataBuffer);
				}
												
				return nReturn;
			}
			else
			{

				char pszAsRunEventID[64];
				if(strlen(g_data.m_Buttons[nButton].m_pchManualEventName)>0)
				{
					_snprintf(pszAsRunEventID, 63, "%s", g_data.m_Buttons[nButton].m_pchManualEventName);
				}
				else
				{
					_snprintf(pszAsRunEventID, 63, "ManualEvent %d", nButton+1);
				}

				g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, 
					"Invalid data configured for button number %d.", 
					g_data.m_Buttons[nButton].m_nItemID
					);
				if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_MANEVT))
				{
					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "Videoframe", 
						"Invalid data configured for button number %d.", 
						g_data.m_Buttons[nButton].m_nItemID); // Sleep(250); //(Dispatch message)
				}

			}

		}
		else
		if(g_data.m_Buttons[nButton].m_nManualEventType == VIDEOFRAME_MANUAL_EVENT_NATIONAL_BREAK)// remove this &&(g_data.m_Buttons[nButton].m_ulFlags&VIDEOFRAME_BUTTON_ENABLED))
			// let enable just deal with the interface and external calls, but NOT GPI changes from VF
		{
			if((g_settings.m_bNationalBreakPrerollIsDelay)&&(g_data.m_Buttons[nButton].m_nManualEventDuration>0))
			{
				CVideoFrameButton* pButton = new CVideoFrameButton;
				if(pButton)
				{
					pButton->m_nItemID = g_data.m_Buttons[nButton].m_nItemID; 
					pButton->m_ulFlags = g_data.m_Buttons[nButton].m_ulFlags; 
					pButton->m_nManualEventDuration = g_data.m_Buttons[nButton].m_nManualEventDuration; 

					strcpy(pButton->m_pchManualEventName, g_data.m_Buttons[nButton].m_pchManualEventName);
					strcpy(pButton->m_pchManualEventProgID, g_data.m_Buttons[nButton].m_pchManualEventProgID);
					strcpy(pButton->m_pchClickEventLabel, g_data.m_Buttons[nButton].m_pchClickEventLabel);

					if(_beginthread(DelayButtonActionThread, 0, (void*)pButton)==-1)
					{
						if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Videoframe_ButtonAction:National_Break", "Error starting delay thread");  //(Dispatch message)
						delete pButton;
					}
				}
				else
				{
					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Videoframe_ButtonAction:National_Break", "Error allocating object for delay thread");  //(Dispatch message)
				}
			}
			else
			if(
//				  (strlen(g_data.m_Buttons[nButton].m_pchManualEventProgID)>0)
				//&&
				  (g_data.m_Buttons[nButton].m_nManualEventDuration>=0)  // only require preroll
				)
			{
				char pszToken[MAX_PATH];
				if((g_data.m_Buttons[nButton].m_pchClickEventLabel)&&(strlen(g_data.m_Buttons[nButton].m_pchClickEventLabel)>0))
				{
					_snprintf(pszToken, MAX_PATH-1, "%s|%s|%s|%d", 
						g_data.m_Buttons[nButton].m_pchClickEventLabel, 
						g_settings.m_pszTabulatorCommandNationalBreak?g_settings.m_pszTabulatorCommandNationalBreak:"National_Break",
						g_data.m_Buttons[nButton].m_pchManualEventProgID?g_data.m_Buttons[nButton].m_pchManualEventProgID:"",
						g_data.m_Buttons[nButton].m_nManualEventDuration
						);
				}
				else
				{
					_snprintf(pszToken, MAX_PATH-1, "%s|%s|%s|%d", 
						g_settings.m_pszTabulatorPlugInModule?g_settings.m_pszTabulatorPlugInModule:"StreamData", 
						g_settings.m_pszTabulatorCommandNationalBreak?g_settings.m_pszTabulatorCommandNationalBreak:"National_Break",
						g_data.m_Buttons[nButton].m_pchManualEventProgID?g_data.m_Buttons[nButton].m_pchManualEventProgID:"",
						g_data.m_Buttons[nButton].m_nManualEventDuration
						);
				}
				unsigned char* pucDataBuffer = (unsigned char*)pszToken;

				int nReturn = g_data.SendTabulatorCommand(&pucDataBuffer, g_data.m_Buttons[nButton].m_pchManualEventName, true);

				if((pucDataBuffer)&&(pucDataBuffer != (unsigned char*)pszToken))
				{
					free(pucDataBuffer);
				}
												
				return nReturn;
			}
			else
			{
				char pszAsRunEventID[64];
				if(strlen(g_data.m_Buttons[nButton].m_pchManualEventName)>0)
				{
					_snprintf(pszAsRunEventID, 63, "%s", g_data.m_Buttons[nButton].m_pchManualEventName);
				}
				else
				{
					_snprintf(pszAsRunEventID, 63, "ManualEvent %d", nButton+1);
				}

				g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, 
					"Invalid data configured for button number %d.", 
					g_data.m_Buttons[nButton].m_nItemID
					);
				if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_MANEVT))
				{
					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "Videoframe", 
						"Invalid data configured for button number %d.", 
						g_data.m_Buttons[nButton].m_nItemID); // Sleep(250); //(Dispatch message)
				}
			}
		// else nothing supported so just exit.

		}
		else
		{
			char pszAsRunEventID[64];

			if(g_data.m_Buttons[nButton].m_ulFlags&VIDEOFRAME_BUTTON_ENABLED)
			{
				if(strlen(g_data.m_Buttons[nButton].m_pchManualEventName)>0)
				{
					_snprintf(pszAsRunEventID, 63, "%s", g_data.m_Buttons[nButton].m_pchManualEventName);
				}
				else
				{
					_snprintf(pszAsRunEventID, 63, "ManualEvent %d", nButton+1);
				}
				g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, "Unrecognized manual event type %d configured for button number %d.", 
				g_data.m_Buttons[nButton].m_nManualEventType,
				g_data.m_Buttons[nButton].m_nItemID);

				if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_MANEVT))
				{
					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "Videoframe", "Unrecognized manual event type %d configured for button number %d.", 
						g_data.m_Buttons[nButton].m_nManualEventType,
						g_data.m_Buttons[nButton].m_nItemID); // Sleep(250); //(Dispatch message)
				}
			}
			else
			{
				_snprintf(pszAsRunEventID, 63, "Relay %d", nButton+1);
				g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, "Relay %d is disabled, could not execute manual event.",
					g_data.m_Buttons[nButton].m_nItemID);

				if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_MANEVT))
				{ 
					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Videoframe", "Relay %d is disabled", nButton+1);// Sleep(500); //(Dispatch message)
				}
			}
		}

	}

	return -1;
}



int CVideoFrameData::SetButtonsOff(char* pszInfo)
{
	int i=0;
	while(i<VF_MAX_BUTTONS)
	{
		g_data.m_Buttons[i].m_ulStatus = VIDEOFRAME_BUTTON_STATUS_OFF;
		i++;
	}

	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d",  //HARDCODE
				((g_settings.m_pszButtons)&&(strlen(g_settings.m_pszButtons)))?g_settings.m_pszButtons:"Buttons",
				VIDEOFRAME_BUTTON_STATUS_OFF
				);

//			Sleep(1); // dont peg processor
if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_SQL)) 
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:button_reset", "SQL: %s", szSQL); // Sleep(250); //(Dispatch message)

EnterCriticalSection(&g_data.m_critSQL);
		int nReturn = g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring);
		if(nReturn<DB_SUCCESS)
		{
			//**MSG
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "VideoFrame:button_reset", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

		}
LeaveCriticalSection(&g_data.m_critSQL);

		if(pszInfo) strcpy(pszInfo, errorstring);

		return nReturn;
	}
	return VIDEOFRAME_ERROR;
}


int CVideoFrameData::GetButtons(char* pszInfo)
{

	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT buttonID, enabled, button_name_on, button_name_off, \
button_type, tally_label, tally_value, click_label, \
me_name, me_event_type, me_programID, me_duration FROM %s ORDER BY buttonID",  //HARDCODE
			((g_settings.m_pszButtons)&&(strlen(g_settings.m_pszButtons)))?g_settings.m_pszButtons:"ButtonInfo"
			); 
		
/*		


ManualEvents.name as me_name, ManualEvents.event_type as me_event_type, 
ManualEvents.programID as me_programID, ManualEvents.duration as me_duration


create table Buttons (buttonID int, enabled int, status int, button_name_on varchar(256), 
button_name_off varchar(256), button_type varchar(16), tally_label varchar(256), tally_value varchar(64) )  

create view ButtonInfo as select Buttons.buttonID, Buttons.enabled, Buttons.status, Buttons.button_name_on, Buttons.button_name_off, Buttons.button_type, 
Buttons.tally_label, Buttons.tally_value, Buttons.click_label, ManualEvents.name as me_name, ManualEvents.event_type as me_event_type, 
ManualEvents.programID as me_programID, ManualEvents.duration as me_duration from Buttons left join ManualEvents on Buttons.buttonID = ManualEvents.buttonID    


	int m_nItemID;

	char m_pchButtonOnLabel[256];
	char m_pchButtonOffLabel[256];

	char m_pchTallyEventLabel[256];
	char m_pchTallyEventValue[64];

	unsigned long m_ulStatus; // current tally.
	unsigned long m_ulFlags;

	// manual events association
	strcpy(m_pchManualEventName,"");
	strcpy(m_pchManualEventProgID,"");
	m_nManualEventType = VIDEOFRAME_MANUAL_EVENT_TYPE_NONE;
	m_nManualEventDuration = -1; // invalid value...

*/

if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:GetButtons", "%s", szSQL); // Sleep(250); //(Dispatch message)
}

//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "GetButtons");  Sleep(250); //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "Got Destinations");  Sleep(250); //(Dispatch message)

			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = CLIENT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_SQL)) 
{	
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:GetButtons", "index %d", nIndex); // Sleep(250); //(Dispatch message) 
}

				try
				{
					CString szTemp;
					prs->GetFieldValue("buttonID", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						int nItemID = atoi(szTemp);

						// here, find the right button.
						if((nItemID>0)&&(nItemID<=VF_MAX_BUTTONS)) // hardcoded 1 thru 32. NOT anymore
						{
							g_data.m_Buttons[nItemID-1].m_nItemID = nItemID;
							nItemID--;

							prs->GetFieldValue("enabled", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							
							g_data.m_Buttons[nItemID].m_ulFlags = (atoi(szTemp)>0)?VIDEOFRAME_BUTTON_ENABLED:VIDEOFRAME_BUTTON_DISABLED;

							prs->GetFieldValue("button_name_on", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							strcpy(g_data.m_Buttons[nItemID].m_pchButtonOnLabel, szTemp);

							prs->GetFieldValue("button_name_off", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							strcpy(g_data.m_Buttons[nItemID].m_pchButtonOffLabel, szTemp);

							prs->GetFieldValue("button_type", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();

/* button_type values (auto, pass, block, indicator, manual) */ 
							
							if(szTemp.CompareNoCase("auto")==0)
							{
								g_data.m_Buttons[nItemID].m_ulFlags |= VIDEOFRAME_BUTTON_TYPE_AUTO;
							}
							else
							if(szTemp.CompareNoCase("pass")==0)
							{
								g_data.m_Buttons[nItemID].m_ulFlags |= VIDEOFRAME_BUTTON_TYPE_PASS;
							}
							else
							if(szTemp.CompareNoCase("block")==0)
							{
								g_data.m_Buttons[nItemID].m_ulFlags |= VIDEOFRAME_BUTTON_TYPE_BLOCK;
							}
							else
							if(szTemp.CompareNoCase("indicator")==0)
							{
								g_data.m_Buttons[nItemID].m_ulFlags |= VIDEOFRAME_BUTTON_TYPE_INDICATOR;
							}
							else
							if(szTemp.CompareNoCase("manual")==0)
							{
								g_data.m_Buttons[nItemID].m_ulFlags |= VIDEOFRAME_BUTTON_TYPE_MANUAL;
							}
							else // not a registered type! just disable
							{
								g_data.m_Buttons[nItemID].m_ulFlags = VIDEOFRAME_BUTTON_TYPE_NONE; // disables and sets to zero type.
							}


							prs->GetFieldValue("tally_label", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							strcpy(g_data.m_Buttons[nItemID].m_pchTallyEventLabel, szTemp);

							prs->GetFieldValue("tally_value", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							strcpy(g_data.m_Buttons[nItemID].m_pchTallyEventValue, szTemp);

							prs->GetFieldValue("click_label", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							strcpy(g_data.m_Buttons[nItemID].m_pchClickEventLabel, szTemp);

// ManualEvents.name as me_name, ManualEvents.event_type as me_event_type, 
// ManualEvents.programID as me_programID, ManualEvents.duration as me_duration
/*
	strcpy(m_pchManualEventName,"");
	strcpy(m_pchManualEventProgID,"");
	m_nManualEventType = VIDEOFRAME_MANUAL_EVENT_TYPE_NONE;
	m_nManualEventDuration = -1; // invalid value...
*/
		
							prs->GetFieldValue("me_name", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							strcpy(g_data.m_Buttons[nItemID].m_pchManualEventName, szTemp);
							
							prs->GetFieldValue("me_event_type", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							g_data.m_Buttons[nItemID].m_nManualEventType = atoi(szTemp);




/*
							if(g_data.m_Buttons[nItemID].m_nManualEventType == VIDEOFRAME_MANUAL_EVENT_REPLACE_AD)
							{
								prs->GetFieldValue("me_duration", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								g_data.m_Buttons[nItemID].m_nManualEventDuration = atoi(szTemp);
								// following line is  so that we do not lose a frame due to rounding down 33.3333 to 33 and 66.6666 to 66.
								if(g_data.m_Buttons[nItemID].m_nManualEventDuration%10)g_data.m_Buttons[nItemID].m_nManualEventDuration++;

if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_SQL|VIDEOFRAME_DEBUG_MANEVT)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:GetButtons", "Manual Event %d Type was Ad Replacement; duration %d", nItemID+1,  g_data.m_Buttons[nItemID].m_nManualEventDuration); // Sleep(250); //(Dispatch message)
}

							}
							else
							if(g_data.m_Buttons[nItemID].m_nManualEventType == VIDEOFRAME_MANUAL_EVENT_REPLACE_CONTENT)
							{
								prs->GetFieldValue("me_programID", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								strncpy(g_data.m_Buttons[nItemID].m_pchManualEventProgID, szTemp, 63);
if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_SQL|VIDEOFRAME_DEBUG_MANEVT)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:GetButtons", "Manual Event %d Type was Content Replacement; ID %s", nItemID+1,  g_data.m_Buttons[nItemID].m_pchManualEventProgID); // Sleep(250); //(Dispatch message)
}
							}
							else  //neither or nothing.
							{
								g_data.m_Buttons[nItemID].m_nManualEventType = VIDEOFRAME_MANUAL_EVENT_TYPE_NONE;  // enforce
								g_data.m_Buttons[nItemID].m_nManualEventDuration = -1; // invalid value...
								strncpy(g_data.m_Buttons[nItemID].m_pchManualEventProgID, "", 63);

if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_SQL|VIDEOFRAME_DEBUG_MANEVT)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:GetButtons", "Manual Event %d Type was Unknown", nItemID+1); // Sleep(250); //(Dispatch message)
}
							}
*/


							if(
								  (g_data.m_Buttons[nItemID].m_nManualEventType == VIDEOFRAME_MANUAL_EVENT_BREAK_END)
								||(g_data.m_Buttons[nItemID].m_nManualEventType == VIDEOFRAME_MANUAL_EVENT_NATIONAL_BREAK)
								)
							{
								prs->GetFieldValue("me_programID", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								strncpy(g_data.m_Buttons[nItemID].m_pchManualEventProgID, szTemp, 63);

								prs->GetFieldValue("me_duration", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								g_data.m_Buttons[nItemID].m_nManualEventDuration = atoi(szTemp);
								// following line is  so that we do not lose a frame due to rounding down 33.3333 to 33 and 66.6666 to 66.
								if(g_data.m_Buttons[nItemID].m_nManualEventDuration%10) g_data.m_Buttons[nItemID].m_nManualEventDuration++;


if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_SQL|VIDEOFRAME_DEBUG_MANEVT)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:GetButtons", "Manual Event %d Type was %s; preroll %d ms; DTMF: %s", nItemID+1,  (g_data.m_Buttons[nItemID].m_nManualEventType == VIDEOFRAME_MANUAL_EVENT_BREAK_END)?"Break End":"National Break", g_data.m_Buttons[nItemID].m_nManualEventDuration, g_data.m_Buttons[nItemID].m_pchManualEventProgID ); // Sleep(250); //(Dispatch message)
}

							}
							else  //neither or nothing.
							{
								g_data.m_Buttons[nItemID].m_nManualEventType = VIDEOFRAME_MANUAL_EVENT_TYPE_NONE;  // enforce
								g_data.m_Buttons[nItemID].m_nManualEventDuration = -1; // invalid value...
								strncpy(g_data.m_Buttons[nItemID].m_pchManualEventProgID, "", 63);

if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_SQL|VIDEOFRAME_DEBUG_MANEVT)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:GetButtons", "Manual Event %d Type was Unknown", nItemID+1); // Sleep(250); //(Dispatch message)
}
							}


						}
					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:GetButtons", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL); // Sleep(250); //(Dispatch message)
}					
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:GetButtons", "Retrieve: Caught exception: out of memory\n%s", szSQL); // Sleep(250); //(Dispatch message)
}
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:GetButtons", "Retrieve: Caught other exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)
}
					}
					e->Delete();
				} 
				catch( ... )
				{
					if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception.\n%s", szSQL);

if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:GetButtons", "Retrieve: Caught exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)
}
					

				}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "Got Destination: %s (0x%08x)", szName, ulFlags);  Sleep(250); //(Dispatch message)

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			return nReturn;
		}
		else
		{
if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_SQL)) 
{	
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:GetButtons", "returned NULL recordset [%s]", pszInfo?pszInfo:"NULL"); // Sleep(250); //(Dispatch message) 
}

		}
	}
//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return VIDEOFRAME_ERROR;
}

int CVideoFrameData::StartServer()
{
	m_bInitAuto = g_settings.m_bInitRequiresAuto?false:true;		// initialization condition
	m_bInitPass = g_settings.m_bInitRequiresPass?false:true;		// initialization condition
	m_bInitBlock = g_settings.m_bInitRequiresBlock?false:true;	// initialization condition
	m_bInitConn =  g_settings.m_bInitRequiresConnection?false:true;	// initialization condition

	m_bInitAutoClick=m_bInitAuto;		// initialization condition
	m_bInitPassClick=m_bInitPass;		// initialization condition
	m_bInitBlockClick=m_bInitBlock;	// initialization condition


	CNetServer* pServer = new CNetServer;
	pServer->m_usPort = g_settings.m_nPort;
	pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

//	pServer->m_nAf = PF_INET;
//	pServer->m_nType = SOCK_DGRAM; 
	pServer->m_nProtocol = 0;

	pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
	if(pServer->m_pszName)strcpy(pServer->m_pszName, "Videoframe listener");

	pServer->m_pszStatus;				// status buffer with error messages from thread
	pServer->m_lpfnHandler = VF200ServerHandlerThread;			// pointer to the thread that handles the request.
	pServer->m_lpObject = this;								// pointer to the object passed to the handler thread.
	pServer->m_lpMsgObj = &m_net;					// pointer to the object with the Message function.

	char errorstring[DB_ERRORSTRING_LEN];

	int n = m_net.StartServer(pServer, &m_net, 5000, errorstring);
	if(n<NET_SUCCESS)
	{
		//report failure
//		CString Q; Q.Format("Failed to start server with error %d", n);
//		AfxMessageBox(Q);
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Videoframe_server_init", "Failed to start server with error %d %s", n, errorstring);  //(Dispatch message)

		if(pServer)
		{
			if(pServer->m_pszName) free(pServer->m_pszName);
			delete pServer;
		}
	}
	else
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "Videoframe_server_init", "Started server on port %d", g_settings.m_nPort);  //(Dispatch message)
	}
	return n;
}

int CVideoFrameData::StopServer()
{
	char errorstring[DB_ERRORSTRING_LEN];
	int n= m_net.StopServer(g_settings.m_nPort, 5000, errorstring);
	if(n<NET_SUCCESS)
	{
		//report failure
//		CString Q; Q.Format("Failed to start server with error %d", n);
//		AfxMessageBox(Q);
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Videoframe_server_uninit", "Failed to stop server with error %d %s", n, errorstring);  //(Dispatch message)
	}
	else
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "Videoframe_server_uninit", "Stopped server on port %d with %d", g_settings.m_nPort, n);  //(Dispatch message)
	}
	return n;
}

/*
int CVideoFrameData::Connect()
{

	hostent* h=NULL;

	h = gethostbyname(g_settings.m_pszHost);
	if(h!=NULL)
	{
		memcpy(&m_saiGTPhost.sin_addr, h->h_addr, 4);
/*
		CString foo; foo.Format("Got %d.%d.%d.%d for %s", 
			m_saiGTPhost.sin_addr.s_net, 
			m_saiGTPhost.sin_addr.s_host, 
			m_saiGTPhost.sin_addr.s_lh, 
			m_saiGTPhost.sin_addr.s_impno,
			g_settings.m_pszHost);
		AfxMessageBox(foo);
* /
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VIDEOFRAME_connect_init", 
			"Got %d.%d.%d.%d for %s", 
			m_saiGTPhost.sin_addr.s_net, 
			m_saiGTPhost.sin_addr.s_host, 
			m_saiGTPhost.sin_addr.s_lh, 
			m_saiGTPhost.sin_addr.s_impno,
			g_settings.m_pszHost);  //(Dispatch message)
	}

	char szError[8096];
	if(m_net.OpenConnection(g_settings.m_pszHost, (short)(g_settings.m_nPort), 
		&m_s, 
		5000,
		5000,
		szError,
		AF_INET,SOCK_DGRAM,IPPROTO_UDP  // UDP for SNMP
		)<NET_SUCCESS)
	{
		m_s = NULL;
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "VIDEOFRAME_connect_init", "Error connecting to %s:%d", g_settings.m_pszHost, g_settings.m_nPort);  //(Dispatch message)
	}
	else
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VIDEOFRAME_connect_init", "Connected to %s:%d", g_settings.m_pszHost, g_settings.m_nPort);  //(Dispatch message)

		return VIDEOFRAME_SUCCESS;
	}
	return VIDEOFRAME_ERROR;
}

int CVideoFrameData::Disconnect()
{
	int n = m_net.CloseConnection(m_s);
	m_s = NULL;
	return n;
}

void USP32ServerHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	CVideoFrameData* pdata = (CVideoFrameData*)pClient->m_lpObject;
	CSNMPUtil snmpu;
	CBufferUtil bu;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		char* pchSnd = NULL;
		unsigned long ulSnd=0;

		CNetUtil net(false); // local object for utility functions.
		char pszToken[MAX_PATH];

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
//		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it


		CNetDatagramData** ppData=NULL;
		int nNumDataObj=0;

		struct timeval tv;
		tv.tv_sec = 0; tv.tv_usec = 30;  // timeout value
		fd_set fds;
		int nNumSockets;
		FD_ZERO(&fds);

		FD_SET(pClient->m_socket, &fds);

		nNumSockets = select(0, &fds, NULL, NULL, &tv);

//		SOCKADDR_IN si;
		int nSize = sizeof(pClient->m_si);

		while (
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&(nNumSockets>0)
					)
		{
			char* pchRecv = (char*)malloc(NET_COMMBUFFERSIZE);
			if(pchRecv)
			{
				int nNumBytes 
					=  recvfrom(
											pClient->m_socket,
											pchRecv,
											NET_COMMBUFFERSIZE,
											0,
											(struct sockaddr*)&(pClient->m_si),
											&nSize
										);

				if(
						(nNumBytes>0)
					&&(g_data.m_saiGTPhost.sin_addr.s_net == pClient->m_si.sin_addr.s_net)
					&&(g_data.m_saiGTPhost.sin_addr.s_host == pClient->m_si.sin_addr.s_host)
					&&(g_data.m_saiGTPhost.sin_addr.s_lh == pClient->m_si.sin_addr.s_lh)
					&&(g_data.m_saiGTPhost.sin_addr.s_impno == pClient->m_si.sin_addr.s_impno) // only accept communication from the VideoFrame!
					)
				{

					MessageLogging_t* pMsgLog = new MessageLogging_t;
					if(pMsgLog)
					{
						pMsgLog->pchReadableHexSnd=NULL;
						pMsgLog->ulReadableHexSnd=0;
						pMsgLog->pchReadableSnd=NULL;
						pMsgLog->ulReadableSnd=0;
						pMsgLog->pchReadableHexRcv=NULL;
						pMsgLog->ulReadableHexRcv=0;
						pMsgLog->pchReadableRcv=NULL;
						pMsgLog->ulReadableRcv=0;
					}

/*
typedef struct MessageLogging_t
{
	char* pchReadableHexSnd;
	unsigned long ulReadableHexSnd;
	char* pchReadableSnd;
	unsigned long ulReadableSnd;
	char* pchReadableHexRcv;
	unsigned long ulReadableHexRcv;
	char* pchReadableRcv;
	unsigned long ulReadableRcv;

} MessageLogging_t;
* /
					if(pMsgLog)
					{
pMsgLog->ulReadableRcv = nNumBytes;
pMsgLog->pchReadableHexRcv = bu.ReadableHex((char*)pchRecv, &pMsgLog->ulReadableRcv, MODE_FULLHEX);
					}

if((g_settings.m_bLogTransactions)&&(pMsgLog))
{

	if(pMsgLog->pchReadableHexRcv)
	{

		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:RECV", "Listener Recd %d bytes: %s", nNumBytes,
							pMsgLog->pchReadableHexRcv); // Sleep(250); //(Dispatch message)

		//free(pchSnd);

		CSNMPMessage* pMessage = u.CreateSNMPMessageFromBuffer((unsigned char*)pchRecv);
		pMsgLog->pchReadableSnd = (char*)u.ReturnReadableMessageBuffer(pMessage);
		delete pMessage;

		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:RECV", "Listener Recd message: %s", pMsgLog->pchReadableSnd); // Sleep(250); //(Dispatch message)
	}
}


					BOOL bNew = TRUE;  // Always do new, at least for now.  these SNMP messages are short and come in fast, but they come in whole.  So what was happening was that two would come in immediately, and we would assemble them into one buffer, and then process just the first part of the buffer, effectively ignoring the second msg.

/*
					BOOL bNew = FALSE;

					// first check the host list
					if(ppData)
					{
						int i=0;
						BOOL bFound = FALSE;
						while(i<nNumDataObj)
						{
							if(
									(ppData[i]->m_si.sin_port == si.sin_port) 
								&&(ppData[i]->m_si.sin_addr.S_un.S_addr == si.sin_addr.S_un.S_addr)
								) // check both port and address.
							{
								// add here
								unsigned char* pucNew = (unsigned char*)malloc(nNumBytes + ppData[i]->m_ulRecv);
								if(pucNew)
								{
									if(ppData[i]->m_pucRecv)
									{
										memcpy(pucNew, ppData[i]->m_pucRecv, ppData[i]->m_ulRecv);			
										free(ppData[i]->m_pucRecv);
									}

									memcpy(pucNew + ppData[i]->m_ulRecv, pchRecv, nNumBytes);

									ppData[i]->m_ulRecv += nNumBytes;
									ppData[i]->m_pucRecv = pucNew;
								}
								bFound = TRUE;
								break;
							}
						
							i++;
						}
						if(!bFound) bNew = TRUE;
					}
					else
					{
						bNew = TRUE;
						// add new.
					}
* /

					if(bNew)
					{
						CNetDatagramData* pData=new CNetDatagramData;

						if(pData)
						{
							pData->m_lpObject = NULL;
							pData->m_pucRecv = NULL;
							pData->m_ulRecv = 0;

							if(pMsgLog) pData->m_lpObject = pMsgLog;

							CNetDatagramData** ppTempData = new CNetDatagramData*[nNumDataObj+1];
							if(ppTempData)
							{
								if(ppData)
								{
									memcpy(ppTempData, ppData, nNumDataObj*sizeof(CNetDatagramData*));
									delete [] ppData;
								}

								ppTempData[nNumDataObj++] = pData;
								ppData = ppTempData;

							}
							unsigned char* pucNew = (unsigned char*)malloc(nNumBytes);
							if(pucNew)
							{
								memcpy(pucNew, pchRecv, nNumBytes);

								pData->m_ulRecv = nNumBytes;
								pData->m_pucRecv = pucNew;
								memcpy(&pData->m_si, &(pClient->m_si), sizeof(pClient->m_si));
								//pData->m_si.sin_addr.S_un.S_addr = si.sin_addr.S_un.S_addr;
							}

						}
					}
				}

				free(pchRecv); // free this, we have copied it to the object buffer (or failed, separately)
			}

			tv.tv_sec = 0; tv.tv_usec = 1000;  // longer timeout value for data continuation
			FD_ZERO(&fds);
			FD_SET(pClient->m_socket, &fds);
			nNumSockets = select(0, &fds, NULL, NULL, &tv);

		}

		// no more data on pipe - respond!

		int q=0;
		while(q<nNumDataObj)
		{
			if(ppData[q])
			{

				if(ppData[q]->m_pucRecv)
				{
					// create a message from the buffer.

					CSNMPMessage* pMessage = u.CreateSNMPMessageFromBuffer((unsigned char*)ppData[q]->m_pucRecv);

					if(pMessage)
					{

//CString Q; Q.Format("Error status %d; index %d", pMessage->m_ulErrorStatus, pMessage->m_ulErrorIndex);
//AfxMessageBox(Q);

						if(pMessage->m_pucValue)
						{

if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_EVENTHANDLER))
{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:RECV", "checking event handlers for event %s", pMessage->m_pucValue); // Sleep(250); //(Dispatch message)
}
							// deal with the event value.
							int i=0;
							while(i<32)  // hardcoded 32 buttons
							{
								bool bStateChange = false;
								bool bClickChange = false;
								int nLen = 0;

								char buffer[256];
								if(strlen(pdata->m_Buttons[i].m_pchTallyEventLabel))
								{

									sprintf(buffer, "%s:",pdata->m_Buttons[i].m_pchTallyEventLabel);
									nLen = strlen(buffer);


	if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_EVENTHANDLER))
	{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:RECV", "checking event handler %d: %s", i+1,buffer); // Sleep(250); //(Dispatch message)
	}


									if(strnicmp(buffer, (char*)(pMessage->m_pucValue), nLen)==0)
									{
										// event matches
	//									AfxMessageBox((char*)pMessage->m_pucValue + nLen);
										bool bVal = (atoi((char*)pMessage->m_pucValue + nLen)==0)?false:true;
										

	if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_EVENTHANDLER))
	{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:RECV", "MATCH event handler %d: %s: value is %s, %d", i+1,(char*)(pMessage->m_pucValue), (char*)pMessage->m_pucValue + nLen, bVal); // Sleep(250); //(Dispatch message)
	}

										unsigned long ulNewStatus;

										if(atoi(pdata->m_Buttons[i].m_pchTallyEventValue)==0)
										{
											// want to match to false
											ulNewStatus = bVal?VIDEOFRAME_BUTTON_STATUS_OFF:VIDEOFRAME_BUTTON_STATUS_ON;
										}
										else
										{
											// want to match to true
											ulNewStatus = bVal?VIDEOFRAME_BUTTON_STATUS_ON:VIDEOFRAME_BUTTON_STATUS_OFF;
										}

										if(pdata->m_Buttons[i].m_ulStatus != ulNewStatus)
										{
											pdata->m_Buttons[i].m_ulStatus = ulNewStatus;
											bStateChange = true;
											pdata->m_Buttons[i].UpdateStatus(); // send it to the database

											// and now evaluate what to do about this GPO state change press (if anything).
										}

// do initialization:
										if(!g_data.m_bButtonsInitialized)
										{
											switch(pdata->m_Buttons[i].m_ulFlags&VIDEOFRAME_BUTTON_TYPE_MASK)
											{
											case VIDEOFRAME_BUTTON_TYPE_AUTO://				0x10
												{
													if((g_settings.m_bInitRequiresAuto)&&(!g_data.m_bInitAuto))
													{

if((g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:button_init", "AUTO button initialized"); // Sleep(250); //(Dispatch message)

														g_data.m_bInitAuto = TRUE;
													}
												} break;
											case VIDEOFRAME_BUTTON_TYPE_PASS://				0x20
												{
													if((g_settings.m_bInitRequiresPass)&&(!g_data.m_bInitPass))
													{
if((g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:button_init", "PASS button initialized"); // Sleep(250); //(Dispatch message)
														g_data.m_bInitPass = TRUE;
													}
												} break;
											case VIDEOFRAME_BUTTON_TYPE_BLOCK://				0x30
												{
													if((g_settings.m_bInitRequiresBlock)&&(!g_data.m_bInitBlock))
													{
if((g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:button_init", "BLOCK button initialized"); // Sleep(250); //(Dispatch message)
														g_data.m_bInitBlock = TRUE;
													}
												} break;
											}

											if(
												  (g_data.m_bInitAuto)&&(g_data.m_bInitPass)&&(g_data.m_bInitBlock)
												&&(g_data.m_bInitAutoClick)&&(g_data.m_bInitPassClick)&&(g_data.m_bInitBlockClick)
												)
											{
if((g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:button_init", "All required buttons initialized (A%dP%dB%d)", 
	 g_settings.m_bInitRequiresAuto,g_settings.m_bInitRequiresPass,g_settings.m_bInitRequiresBlock); // Sleep(250); //(Dispatch message)

												g_data.m_bButtonsInitialized = TRUE;
											}
										}



//									pdata->m_button[i].Invalidate();
									}
								}

								if(strlen(pdata->m_Buttons[i].m_pchClickEventLabel))
								{
									sprintf(buffer, "%s:",pdata->m_Buttons[i].m_pchClickEventLabel);
									int nLen = strlen(buffer);

	if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_EVENTHANDLER))
	{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:RECV", "checking click handler %d: %s", i+1,buffer); // Sleep(250); //(Dispatch message)
	}


									if(strnicmp(buffer, (char*)(pMessage->m_pucValue), nLen)==0)
									{
										// click event matches
	//									AfxMessageBox((char*)pMessage->m_pucValue + nLen);
										int nVal = atoi((char*)pMessage->m_pucValue + nLen);
										
	if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_EVENTHANDLER))
	{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:RECV", "MATCH click handler %d: %s: value is %s, %d", i+1,(char*)(pMessage->m_pucValue), (char*)pMessage->m_pucValue + nLen, nVal); // Sleep(250); //(Dispatch message)
	}

										if(pdata->m_Buttons[i].m_nLastClickValue<0) // init, just assign
										{
											pdata->m_Buttons[i].m_nLastClickValue = nVal;
										}
										else
										{
											// want to match to see if different - clicking will toggle value.
											if(pdata->m_Buttons[i].m_nLastClickValue != nVal)
											{
												pdata->m_Buttons[i].m_nLastClickValue = nVal;
												bClickChange = true;
											}
										}


										if(!g_data.m_bButtonsInitialized)
										{
											switch(pdata->m_Buttons[i].m_ulFlags&VIDEOFRAME_BUTTON_TYPE_MASK)
											{
											case VIDEOFRAME_BUTTON_TYPE_AUTO://				0x10
												{
													if((g_settings.m_bInitRequiresAuto)&&(!g_data.m_bInitAutoClick))
													{

if((g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:button_init", "AUTO button click handler initialized"); // Sleep(250); //(Dispatch message)

														g_data.m_bInitAutoClick = TRUE;
													}
												} break;
											case VIDEOFRAME_BUTTON_TYPE_PASS://				0x20
												{
													if((g_settings.m_bInitRequiresPass)&&(!g_data.m_bInitPassClick))
													{
if((g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:button_init", "PASS button click handler initialized"); // Sleep(250); //(Dispatch message)
														g_data.m_bInitPassClick = TRUE;
													}
												} break;
											case VIDEOFRAME_BUTTON_TYPE_BLOCK://				0x30
												{
													if((g_settings.m_bInitRequiresBlock)&&(!g_data.m_bInitBlockClick))
													{
if((g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:button_init", "BLOCK button click handler initialized"); // Sleep(250); //(Dispatch message)
														g_data.m_bInitBlockClick = TRUE;
													}
												} break;
											}

											if(
												  (g_data.m_bInitAuto)&&(g_data.m_bInitPass)&&(g_data.m_bInitBlock)
												&&(g_data.m_bInitAutoClick)&&(g_data.m_bInitPassClick)&&(g_data.m_bInitBlockClick)
												)
											{
if((g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "VideoFrame:button_init", "ALL required buttons initialized (A%dP%dB%d)", 
	 g_settings.m_bInitRequiresAuto,g_settings.m_bInitRequiresPass,g_settings.m_bInitRequiresBlock); // Sleep(250); //(Dispatch message)

												g_data.m_bButtonsInitialized = TRUE;
											}
										}


//									pdata->m_button[i].Invalidate();
									}


								// at this point we know if a button was clicked or a tally was changed (usually GPO status)

									// here, we have a click handler and we know it has clicked or not.
									// if clicked, do something

									if(bClickChange)
									{
										switch(pdata->m_Buttons[i].m_ulFlags&VIDEOFRAME_BUTTON_TYPE_MASK)
										{
										default:
										case VIDEOFRAME_BUTTON_TYPE_NONE://				0x00
										case VIDEOFRAME_BUTTON_TYPE_INDICATOR://		0x40
										case VIDEOFRAME_BUTTON_TYPE_MANUAL://			0x50
											{
												// do nothing for key presses here.
												// (even manual mode, we will just go on GPO status for now)
												// this is the disney hardcode part of this right now.
											} break;
										case VIDEOFRAME_BUTTON_TYPE_AUTO://				0x10
											{
	if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_BUTTONPRESS))
	{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame", "The AUTO button was pressed."); // Sleep(250); //(Dispatch message)
	}
	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, "ButtonClick", g_settings.m_pszInternalAppName, "The AUTO button was pressed.");
												// do the re-enter AUTO control mode event associated with this button.
												//TODO
/*
AUTO Control State
On entering (or re-entering) the AUTO Control State, Promotor shall:
1. Illuminate Button 1 ("AUTO") on the Basic Operator Panel.		// handled by the GTP 32 setup

2. If there is a Selected Automation System and it is currently executing a primary event, process an Automation Event with the in-progress primary event, 
   substituting for purposes of this processing:
			a. The current LTC time address in place of the starting time address of the in-progress primary event, and
			b. The remaining duration of the in-progress primary event in place of the duration of the in-progress primary event.


Processing Automation Events
To process an Automation Event, given a primary event, Promotor shall follow these steps:
1. (In Phase Two only:) If the Event Masking Timer is counting down, go to Step 7 of this procedure.
   (Before Phase Two:) If the Event Masking Timer is counting down, stop processing of this Automation Event.

	// we can send it over to the automation_data.dll for cancellation or processing.

2. If Promotor is not in the AUTO Control State, stop processing of this Automation Event.

	// This can be evaluated at Promotor to prevent this event being sent.
	// For the purposes of the button press here, we can assemble the event and send it over to the automation_data.dll for cancellation or processing

3. If the primary event is classified as Advertisement:
     a. Compute the Pod Duration, which shall be the sum of the duration of the current primary event and the durations of each of the
        immediately following primary events, if any, in the playlist that are also classified as Advertisement.
        (Note: The ad pod ends with the first event that is not classified as Advertisement, and the durations of this event and following events shall not be added to the Pod Duration.)
     b. Generate and insert a Replace Ad Message, populated as follows:
        - PodDur: The Pod Duration.
        - LongFormProgramID: The Program ID of the Current Long-Form Program.
        - LongFormProgramDescription: The Program Description of the Current Long-Form Program.
     c. Start the Event Masking Timer with the Pod Duration.
     d. Extinguish Button 2 ("PASS") on the Basic Operator Panel.
     e. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.
     f. Close the Downstream Keyer Control GPO.
     g. Stop processing of this Automation Event.


												// the Replace Ad tabulator event.
												//  Replace Ad Message message

												//  BLOCK GPO w Timer message
												//  Start Timer1
												//  SET USP GPO 3 ON
												//  SET USP GPO 3 OFF "un-press" it - no effect


else do the complex thing:

4. Retrieve the entries in the Replacement Rules Table having an Original Program ID equal to the Program ID of the primary event.

5. If no entries were retrieved from the Replacement Rules Table in the previous step, evaluate entries in the Description Pattern Table, and for
   each entry in the Description Pattern Table having a Matching Pattern satisfied by the Program Description of the primary event, retrieve the
   entries from the Replacement Rules Table having an Original Program ID equal to the Program ID in the Description Pattern Table entry.

6. If one or more entries were retrieved from the Replacement Rules Table in either of the two previous steps:
     a. Create an Effective Replacement Rules List, which shall contain the retrieved Replacement Rules Table entries, sorted by Replacement
        Sequence, from lowest to highest. Each entry in the Effective Replacement Rules List shall also have an Offset, which initially shall be the time address 00:00:00;00.
        (Note: The Effective Replacement Rules List need not persist after processing of the Automation Event is complete.)
     b. Compute the Required Replacement Duration, which shall be the starting time address of the Next Long-Form Program less the starting time address of the current primary event
     c. Compute the Trial Total Replacement Duration, which shall be the sum of the Replacement Durations of the entries in the Effective Replacement Rules List, less the sum of the Offsets of the entries in the Effective Replacement Rules List.
     d. If the Trial Total Replacement Duration is less than the Required Replacement Duration, increase the Replacement Duration of the last entry in the Effective Replacement Rules List by the difference between the Required Replacement Duration and the Trial Total Replacement Duration.
     e. If the Trial Total Replacement Duration is greater than the Required Replacement Duration, repeat the following steps until the Trial Total Replacement Duration is equal to the Required Replacement Duration:
          i. Set the Offset of the first entry in the Effective Replacement Rules List to the difference between the Trial Total Replacement Duration and the Required Replacement Duration.
          ii. If the Offset of the first entry in the Effective Replacement Rules List is greater than or equal to the Replacement Duration of the list entry, delete the entry from the list.
          iii. Recompute the Trial Total Replacement Duration.
     f. If there are no entries remaining in the Effective Replacement Rules List, stop processing of this Automation Event.
     g. For each entry in the Effective Replacement Rules List:
		 {
          i. Generate and insert a Replace Content Message, populated as follows:
						 - ReplProgramID: The Replacement Program ID in the list entry.
						 - ReplLength: The Replacement Duration in the list entry.
						 - Offset: The Offset in the list entry.

												// the Replace Content tabulator event.
												//  Replace Content Message message

          ii. Add to the Actual Total Replacement Duration the Replacement Duration in the list entry.
		 }
     h. Start the Event Masking Timer with the Actual Total Replacement Duration.
     i. Extinguish Button 2 ("PASS") on the Basic Operator Panel.
     j. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.
     k. Close the Downstream Keyer Control GPO.
     l. Stop processing of this Automation Event.


												//  BLOCK GPO w Timer message
												//  Start Timer1
												//  SET USP GPO 3 ON
												//  SET USP GPO 3 OFF "un-press" it - no effect



7. In Phase Two only, if the primary event, evaluated using the Network Source Identifiers, is associated with a network source:
     a. If the Network Replacement Operations List (updated as Stream Message Events are processed) indicates that a network-managed ad replacement operation, content replacement operation, or indefinite replacement operation is in progress:
          i. Generate and insert Stream Messages as required to carry out this replacement operation, provided that the effective duration of a network-managed ad replacement operation, other than an indefinite replacement operation, shall be limited to the starting time address of the Next Long-Form Program less the starting time address of the current primary event.
          ii. If the network-managed replacement operation is an ad replacement operation or content replacement operation, start the Event Masking Timer with the effective duration of the replacement operation.
          iii. Extinguish Button 2 ("PASS") on the Basic Operator Panel.
          iv. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.
          v. Close the Downstream Keyer Control GPO.
     b. Otherwise:
          i. Stop the Event Masking Timer.
          ii. Illuminate Button 2 ("PASS") on the Basic Operator Panel.
          iii. Extinguish Button 3 ("BLOCK") on the Basic Operator Panel.
          iv. Open the Downstream Keyer Control GPO.
          v. Generate and insert a Rejoin Main Message, thereby returning the TV+ audience to the Program Stream.
     c. Stop processing of this Automation Event.



	
Otherwise:
			a. Stop the Event Masking Timer.
			b. Illuminate Button 2 ("PASS") on the Basic Operator Panel.		// NOT handled by the GTP 32 setup
			c. Extinguish Button 3 ("BLOCK") on the Basic Operator Panel.		// NOT handled by the GTP 32 setup
			d. Open the Downstream Keyer Control GPO.												// NOT handled by the GTP 32 setup
			e. Generate and insert a Rejoin Main Message, thereby returning the TV+ audience to the Program Stream.
* /

												// This means, send the automation DLL a command of type AUTO and have it do all this logic
												// in the first instance it does the big complicated thing.
												// in the otherwise, it does the same thing as it does for PASS
												// so these tabluator events needed.

												// the AUTO tabulator events:

												// the Replace Ad tabulator event.
												//  Replace Ad Message message

												// the Replace Content tabulator event.
												//  Replace Content Message message

												//  BLOCK GPO w Timer message
												//  Start Timer1
												//  SET USP GPO 3 ON
												//  SET USP GPO 3 OFF "un-press" it - no effect



												// the otherwise event:

												// the PASS tabulator event:
												//  Stop Timer1
												//  SET USP GPO 2 ON
												//  Rejoin Main message
												//  SET USP GPO 2 OFF "un-press" it - no effect

//all of this is triggered with a single token:

// AutomationData|Replace_Current


_snprintf(pszToken, MAX_PATH-1, "%s|%s", g_settings.m_pszTabulatorPlugInModule, g_settings.m_pszTabulatorCommandReplaceCurrent);
unsigned char* pucDataBuffer = (unsigned char*)pszToken;
int nTabReturn = g_data.SendTabulatorCommand(&pucDataBuffer, "AUTO");
												if(nTabReturn>=VIDEOFRAME_SUCCESS)
												{
													if((pucDataBuffer)&&(pucDataBuffer != (unsigned char*)pszToken))
													{
														free(pucDataBuffer);
													}
												}




											} break;
										case VIDEOFRAME_BUTTON_TYPE_PASS://				0x20
											{
	if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_BUTTONPRESS))
	{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame", "The PASS button was pressed."); // Sleep(250); //(Dispatch message)
	}
	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, "ButtonClick", g_settings.m_pszInternalAppName, "The PASS button was pressed.");
												// do the re-enter PASS control mode event associated with this button.
												//TODO

/*
PASS Control State
On entering (or re-entering) the PASS Control State, Promotor shall:

1. Stop the Event Masking Timer.
2. Extinguish Button 1 ("AUTO") on the Basic Operator Panel.		// handled by the GTP 32 setup
3. Illuminate Button 2 ("PASS") on the Basic Operator Panel.		// handled by the GTP 32 setup
4. Extinguish Button 3 ("BLOCK") on the Basic Operator Panel.		// handled by the GTP 32 setup
5. Open the Downstream Keyer Control GPO.												// handled by the GTP 32 setup

6. Generate and insert a Rejoin Main Message, thereby returning the TV+ audience to the Program Stream.
* /
												// the PASS tabulator event:
												//  Stop Timer1
												//  SET USP GPO 2 ON
												//  Rejoin Main message
												//  SET USP GPO 2 OFF "un-press" it - no effect

//all of this is triggered with a single token:

// AutomationData|Enter_Pass


_snprintf(pszToken, MAX_PATH-1, "%s|%s", g_settings.m_pszTabulatorPlugInModule, g_settings.m_pszTabulatorCommandEnterPass);
unsigned char* pucDataBuffer = (unsigned char*)pszToken;
int nTabReturn = g_data.SendTabulatorCommand(&pucDataBuffer, "PASS");
												if(nTabReturn>=VIDEOFRAME_SUCCESS)
												{
													if((pucDataBuffer)&&(pucDataBuffer != (unsigned char*)pszToken))
													{
														free(pucDataBuffer);
													}
												}





											} break;
										case VIDEOFRAME_BUTTON_TYPE_BLOCK://				0x30
											{
	if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_BUTTONPRESS))
	{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame", "The BLOCK button was pressed."); // Sleep(250); //(Dispatch message)
	}
	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, "ButtonClick", g_settings.m_pszInternalAppName, "The BLOCK button was pressed.");

												// do the re-enter BLOCK control mode event associated with this button.
												//TODO
/*
BLOCK Control State
On entering (or re-entering) the BLOCK Control State, Promotor shall:
1. Extinguish Button 1 ("AUTO") on the Basic Operator Panel.   // handled by the GTP 32 setup
2. Generate and insert an Event Notification Message having the value "TV+:BLOCKED" in the ProgramID field, thereby initiating an indefinite replacement operation at upLynk.
3. Extinguish Button 2 ("PASS") on the Basic Operator Panel.   // handled by the GTP 32 setup
4. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.  // handled by the GTP 32 setup
5. Close the Downstream Keyer Control GPO.										 // handled by the GTP 32 setup
* /
	
												// the BLOCK tabulator event.
												//  Event notification message
												//  SET USP GPO 3 ON
												//  SET USP GPO 3 OFF "un-press" it - no effect


//all of this is triggered with a single token:

// AutomationData|Enter_Block


_snprintf(pszToken, MAX_PATH-1, "%s|%s", g_settings.m_pszTabulatorPlugInModule, g_settings.m_pszTabulatorCommandEnterBlock);
unsigned char* pucDataBuffer = (unsigned char*)pszToken;
int nTabReturn = g_data.SendTabulatorCommand(&pucDataBuffer, "BLOCK");
												if(nTabReturn>=VIDEOFRAME_SUCCESS)
												{
													if((pucDataBuffer)&&(pucDataBuffer != (unsigned char*)pszToken))
													{
														free(pucDataBuffer);
													}
												}

											} break;
										}
									}
								}
								else
								{
									// there was no click handler, use the tally as indicator of "pressed-ness"

									if(bStateChange)
									{
										switch(pdata->m_Buttons[i].m_ulFlags&VIDEOFRAME_BUTTON_TYPE_MASK)
										{
										default:
										case VIDEOFRAME_BUTTON_TYPE_NONE://				0x00
										case VIDEOFRAME_BUTTON_TYPE_AUTO://				0x10
										case VIDEOFRAME_BUTTON_TYPE_PASS://				0x20
										case VIDEOFRAME_BUTTON_TYPE_BLOCK://				0x30
										case VIDEOFRAME_BUTTON_TYPE_INDICATOR://		0x40
											{
												// do nothing for state changes on these types of buttons.
											} break;
										case VIDEOFRAME_BUTTON_TYPE_MANUAL://			0x50
											{
												// if this is pressed, it means that a manual event button was pressed.
												if(pdata->m_Buttons[i].m_ulStatus&VIDEOFRAME_BUTTON_STATUS_ON)
												{

	if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_BUTTONPRESS))
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame", "Manual Event button number %d was pressed.", pdata->m_Buttons[i].m_nItemID); // Sleep(250); //(Dispatch message)
	}
	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, "ButtonClick", g_settings.m_pszInternalAppName, "Manual Event button number %d was pressed.", pdata->m_Buttons[i].m_nItemID);

													// do the manual event associated with this button.
													//TODO
/*
To process a Manual Event, given an invoking GPO number, Promotor shall follow
these steps:
1. If the Event Masking Timer is counting down or Promotor is not in the AUTO
Control State, stop processing of this Manual Event.



2. Retrieve the entry, if any, in the Manual Event Table having a GPO Number equal to the invoking GPO number.
If there is no such entry, stop processing of this Manual Event.

	// we are going to do step 2 first, since there is no consequence.
	// then send over the info and let the automation_data.dll cancel the event if step 1 evaluates.


3. If the Event Type in the Manual Event Table entry is Ad Replacement:
  a. Generate and insert a Replace Ad Message, populated as follows:
   - PodDur: The Duration in the Manual Event Table entry.
   - LongFormProgramID: The value "TV+:UNKNOWN"
   - LongFormProgramDescription: Blank.
  b. Start the Event Masking Timer with the Duration in the Manual Event Table entry.
  c. Extinguish Button 2 ("PASS") on the Basic Operator Panel.
  d. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.
  e. Close the Downstream Keyer Control GPO.
  f. Stop processing of this Manual Event.

												// the Replace Ad tabulator event.
												//  Replace Ad Message message

												//  BLOCK GPO w Timer message
												//  Start Timer1
												//  SET USP GPO 3 ON
												//  SET USP GPO 3 OFF "un-press" it - no effect



4. If the Event Type in the Manual Event Table entry is Program Replacement:
  a. Retrieve the entries in the Replacement Rules Table having an Original Program ID equal to the Program ID in the Retrieved Manual Event Table entry.
  b. If there are no such entries, stop processing of this Manual Event.
  c. For each retrieved Replacement Rules Table entry, sorted by Replacement Sequence, from lowest to highest:
	{
		i. Generate and insert a Replace Content Message, populated as follows:
		 - ReplLength: The Replacement Duration in the Replacement Rules Table entry.
		 - Offset: The time address 00:00:00;00.
		 - ReplProgramID: The Replacement Program ID in the Replacement Rules Table entry.

												// the Replace Content tabulator event.
												//  Replace Content Message message

	 ii. Add the Replacement Duration in the Replacement Rules Table entry to the Total Replacement Duration.
	}
5. Start the Event Masking Timer with the Total Replacement Duration.

												//  BLOCK GPO w Timer message
												//  Start Timer1
												//  SET USP GPO 3 ON
												//  SET USP GPO 3 OFF "un-press" it - no effect

6. Extinguish Button 2 ("PASS") on the Basic Operator Panel.
7. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.
8. Close the Downstream Keyer Control GPO.
9. Stop processing of this Manual Event.
* /

//all of this is triggered with a single token:

//AutomationData|Replace_Manual|<manual type>|<manual replacement data (=Pod dur if type is ad, original ID if type is content)>  // the actual replace command, results in the commands being sent.to Libretto, uses the manual event procedure



													char pszAsRunEventID[64];
													if(pdata->m_Buttons[i].m_nManualEventType == VIDEOFRAME_MANUAL_EVENT_TYPE_NONE)
													{
														_snprintf(pszAsRunEventID, 63, "ManualEvent %d", i);

														// just log it.

	if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_MANEVT))
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame", "No Manual Event configured for button number %d.", pdata->m_Buttons[i].m_nItemID); // Sleep(250); //(Dispatch message)
	}
	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, "No Manual Event configured for button number %d.", pdata->m_Buttons[i].m_nItemID);

													}
													else
													{
														if(strlen(pdata->m_Buttons[i].m_pchManualEventName)>0)
														{
															_snprintf(pszAsRunEventID, 63, "%s", pdata->m_Buttons[i].m_pchManualEventName);
														}
														else
														{
															_snprintf(pszAsRunEventID, 63, "ManualEvent %d", i);
														}

														memset(pszToken, 0, MAX_PATH); // clear it.
														if(pdata->m_Buttons[i].m_nManualEventType == VIDEOFRAME_MANUAL_EVENT_REPLACE_AD)
														{

_snprintf(pszToken, MAX_PATH-1, "%s|%s|%d|%d", 
					g_settings.m_pszTabulatorPlugInModule, 
					g_settings.m_pszTabulatorCommandReplaceManual,
					pdata->m_Buttons[i].m_nManualEventType,
					pdata->m_Buttons[i].m_nManualEventDuration
					);
														}
														else
														if(pdata->m_Buttons[i].m_nManualEventType == VIDEOFRAME_MANUAL_EVENT_REPLACE_CONTENT)
														{
															if(strlen(pdata->m_Buttons[i].m_pchManualEventProgID)>0)
															{
_snprintf(pszToken, MAX_PATH-1, "%s|%s|%d|%s", 
					g_settings.m_pszTabulatorPlugInModule, 
					g_settings.m_pszTabulatorCommandReplaceManual,
					pdata->m_Buttons[i].m_nManualEventType,
					pdata->m_Buttons[i].m_pchManualEventProgID
					);
															}
															else
															{
																// log the bad data.
	g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, "No Program ID configured for button number %d.", pdata->m_Buttons[i].m_nItemID);
	if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_MANEVT))
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "VideoFrame", "No Program ID configured for button number %d.", pdata->m_Buttons[i].m_nItemID); // Sleep(250); //(Dispatch message)
	}
															}	
														}
														else
														{
	g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, "Unrecognized manual event type %d configured for button number %d.", 
		pdata->m_Buttons[i].m_nManualEventType,
		pdata->m_Buttons[i].m_nItemID);
	if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_MANEVT))
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "VideoFrame", "Unrecognized manual event type %d configured for button number %d.", 
			pdata->m_Buttons[i].m_nManualEventType,
			pdata->m_Buttons[i].m_nItemID); // Sleep(250); //(Dispatch message)
	}
														}

														if(strlen(pszToken)>0)
														{
unsigned char* pucDataBuffer = (unsigned char*)pszToken;
int nTabReturn = g_data.SendTabulatorCommand(&pucDataBuffer, pszAsRunEventID);
															if(nTabReturn>=VIDEOFRAME_SUCCESS)
															{
																if((pucDataBuffer)&&(pucDataBuffer != (unsigned char*)pszToken))
																{
																	free(pucDataBuffer);
																}
															}
														}
													}

												}
											} break;
										}
									}
								}




								i++;
							}



	if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_EVENTHANDLER))
{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:RECV", "finished checking %d event handlers for event %s%c%c", i,pMessage->m_pucValue, 13, 10); // Sleep(250); //(Dispatch message)
}
							free(pMessage->m_pucValue);

						}
						pMessage->m_pucValue = NULL;
						pMessage->m_ulValueLength=0;
						pMessage->m_ucPDUType = SNMP_GETRESPONSEPDU;
						pMessage->m_ulErrorStatus = 0;
						pMessage->m_ulErrorIndex = 0;


//CString Q; Q.Format("Error status %d; index %d", pMessage->m_ulErrorStatus, pMessage->m_ulErrorIndex);
//AfxMessageBox(Q);


						// then send a response

						pchSnd = (char*)u.ReturnMessageBuffer(pMessage);
						ulSnd = u.ReturnLength((unsigned char*)pchSnd+1);
						ulSnd += u.ReturnLength(ulSnd)+1;

						sendto(
							pClient->m_socket,
							(char*)pchSnd, 
							ulSnd,
							0,
							(struct sockaddr *) &(ppData[q]->m_si),
							nSize
						);

if(ppData[q]->m_lpObject)
{
	((MessageLogging_t*)(ppData[q]->m_lpObject))->ulReadableHexSnd = ulSnd;
	((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd = bu.ReadableHex((char*)pchSnd, &((MessageLogging_t*)(ppData[q]->m_lpObject))->ulReadableHexSnd, MODE_FULLHEX);
	((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableSnd = (char*)u.ReturnReadableMessageBuffer(pMessage);
}
						delete pMessage;

						free(pchSnd);  pchSnd=NULL;
					}
					else
					{
						// just echo it

						sendto(
							pClient->m_socket,
							(char*)ppData[q]->m_pucRecv, 
							ppData[q]->m_ulRecv,
							0,
							(struct sockaddr *) &(ppData[q]->m_si),
							nSize
						);
ulSnd = ppData[q]->m_ulRecv;
if(ppData[q]->m_lpObject)
{
	((MessageLogging_t*)(ppData[q]->m_lpObject))->ulReadableHexSnd = ulSnd;
	((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd = bu.ReadableHex((char*)ppData[q]->m_pucRecv, &((MessageLogging_t*)(ppData[q]->m_lpObject))->ulReadableHexSnd, MODE_FULLHEX);

	CSNMPMessage* pTempMessage = u.CreateSNMPMessageFromBuffer((unsigned char*)ppData[q]->m_pucRecv);
	((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableSnd = (char*)u.ReturnReadableMessageBuffer(pTempMessage);
	delete pTempMessage;
}
					}



if((g_settings.m_bLogTransactions)&&(ppData[q]->m_lpObject))
{

	if(((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd)
	{

		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:SEND", "Listener Sent %d bytes: %s", 	ulSnd,
					((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd); // Sleep(250); //(Dispatch message)

		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:SEND", "Listener Sent message: %s%c%c", 	
					((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableSnd, 13, 10); // Sleep(250); //(Dispatch message)
	//		free(pchRecv);  // no, use below.
	}
}

/*

	// this was to update a list box, but we don;t have this in the module, just the tester that this code came from

						int nCount = pdata->m_lcResponses.GetItemCount();

						pdata->m_lcResponses.InsertItem(nCount, "<-");
/*
						if(pchSnd)
						{
							m_lcResponses.InsertItem(nCount, pchSnd);
						}
						else
						{
							m_lcResponses.InsertItem(nCount, (char*)buffer);
						}

						
						if(pchRecv)
						{
							m_lcResponses.SetItemText(nCount, 1, (char*)pchRecv );
						}
						else
						{
							m_lcResponses.SetItemText(nCount, 1, (char*)puc );
						}

* /
						if((ppData[q]->m_lpObject)&&(((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexRcv))
						{
							pdata->m_lcResponses.SetItemText(nCount, 1, ((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexRcv);
						}
						else
						{
							pdata->m_lcResponses.SetItemText(nCount, 1, (char*)ppData[q]->m_pucRecv);
						}
						
						if((ppData[q]->m_lpObject)&&(((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd))
						{
							pdata->m_lcResponses.SetItemText(nCount, 2, (char*)((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd );
						}
						else
						{
							pdata->m_lcResponses.SetItemText(nCount, 2, (char*)ppData[q]->m_pucRecv );
						}
						pdata->m_lcResponses.EnsureVisible(nCount, FALSE);
						while(nCount>LISTBOXMAX){  pdata->m_lcResponses.DeleteItem(0); nCount--; } //delete oldest
* /
							
				}


				if(ppData[q]->m_lpObject)
				{
					// have to free all the buffers.
					MessageLogging_t* pMsgLog = (MessageLogging_t*) ppData[q]->m_lpObject;

					if(pMsgLog->pchReadableHexRcv) free(pMsgLog->pchReadableHexRcv);
					if(pMsgLog->pchReadableHexSnd) free(pMsgLog->pchReadableHexSnd);
					if(pMsgLog->pchReadableRcv) free(pMsgLog->pchReadableRcv);
					if(pMsgLog->pchReadableSnd) free(pMsgLog->pchReadableSnd);

					delete ppData[q]->m_lpObject;
				}
				if(ppData[q]->m_pucRecv) free(ppData[q]->m_pucRecv);

				delete ppData[q];
			}
			q++;
		}


		delete [] ppData;

//		pdata->SetButtonText();

		if(pchSnd) free(pchSnd);

	}

	(*(pClient->m_pulConnections))--;

	delete pClient; // was created with new in the thread that spawned this one.
}
*/

void DelayButtonActionThread(void* pvArgs)
{
	if(pvArgs == NULL) return;
	CVideoFrameButton* pButton = (CVideoFrameButton*)pvArgs;

	if(g_settings.m_ulDebug&(VIDEOFRAME_DEBUG_BUTTONPRESS))
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "Videoframe_ButtonAction:Delay", "Waiting %d milliseconds", pButton->m_nManualEventDuration);  //(Dispatch message)
	}

	Sleep(pButton->m_nManualEventDuration);

	char pszToken[MAX_PATH];
	if((pButton->m_pchClickEventLabel)&&(strlen(pButton->m_pchClickEventLabel)>0))
	{
		_snprintf(pszToken, MAX_PATH-1, "%s|%s|%s", 
			pButton->m_pchClickEventLabel, 
			g_settings.m_pszTabulatorCommandNationalBreak?g_settings.m_pszTabulatorCommandNationalBreak:"National_Break",
			pButton->m_pchManualEventProgID?pButton->m_pchManualEventProgID:""//,
	//		pButton->m_nManualEventDuration  // sincle this is the delay here, we do not want to override the preroll with this value.
			);
	}
	else
	{
		_snprintf(pszToken, MAX_PATH-1, "%s|%s|%s", 
			g_settings.m_pszTabulatorPlugInModule?g_settings.m_pszTabulatorPlugInModule:"StreamData", 
			g_settings.m_pszTabulatorCommandNationalBreak?g_settings.m_pszTabulatorCommandNationalBreak:"National_Break",
			pButton->m_pchManualEventProgID?pButton->m_pchManualEventProgID:""//,
	//		pButton->m_nManualEventDuration  // sincle this is the delay here, we do not want to override the preroll with this value.
			);
	}
	unsigned char* pucDataBuffer = (unsigned char*)pszToken;

	int nReturn = g_data.SendTabulatorCommand(&pucDataBuffer, pButton->m_pchManualEventName, true);

	if((pucDataBuffer)&&(pucDataBuffer != (unsigned char*)pszToken))
	{
		free(pucDataBuffer);
	}
									
	delete pButton;

	
}


void VF200ServerHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;
		CVideoFrameData* pdata = (CVideoFrameData*)pClient->m_lpObject;

		CNetUtil net(false); // local object for utility functions.

		char errorstring[MAX_MESSAGE_LENGTH];
		strcpy(errorstring, "");

		int nThreadID = GetCurrentThreadId();

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
//		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		CNetData data;

		char* pchBuffer = NULL;
		unsigned long ulBufferLen = 0;
		char* pch = NULL;
		char* pchXML = NULL;
		char* pchXMLStream = NULL;
		unsigned long ulAccumulatedBufferLen = 0;
		unsigned long ulRetry = 0;

		unsigned long ulConnLastMessage = 0;
		unsigned long ulConnTimeout = 0;

//		char filename[MAX_PATH];
//		char lastrxfilename[MAX_PATH];
//		char lasttxfilename[MAX_PATH];
		int nRxDupes=0;
		int nTxDupes=0;

		FILE* fp = NULL;

		_timeb timestamp;
		_timeb timeactive;
		_timeb timeperiodic;

		_ftime(&timeactive);
		_ftime(&timeperiodic);

		bool bCloseCommand = false;


if(g_settings.m_bLogTransactions)
{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:socket_connection", "Connection from %d.%d.%d.%d on socket %d established in thread %d.", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,
				pClient->m_socket, nThreadID);

}

		EnterCriticalSection(&u.m_critThreadSync);
		CVFSimpleThreadSync* pThreadSync = u.AddThreadSync(nThreadID);
		if(pThreadSync)
		{
			pThreadSync->m_psi = &(pClient->m_si.sin_addr); // just give it a pointer to the address.
		}
		LeaveCriticalSection(&u.m_critThreadSync);

/*
if(g_settings.m_bLogTransactions)//&&(pMsgLog))
{

		FILE* logfp = fopen(LOGFILENAME, "ab");
		if(logfp)
		{
			_timeb timestamp;
			_ftime(&timestamp);
			char logtmbuf[48]; // need 33;
			tm* theTime = localtime( &timestamp.time	);
			strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

//AfxMessageBox(pchSnd);

			fprintf(logfp, "%s%03d Connection from %d.%d.%d.%d on socket %d established.\r\n", 
				logtmbuf, timestamp.millitm,
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,
				pClient->m_socket);


			fclose(logfp);
		}
}

*/

		// seed inactivity timer here so that it does not just immediately fail.
		_ftime( &timestamp );
		ulConnLastMessage = timestamp.time;
		ulConnTimeout = g_settings.m_nConnectionTimeout;

		bool bInitThread = true;

		while ( 
									(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
//								&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
								&&(pClient->m_socket!=NULL)
								&&(!bCloseCommand)
					)
		{

			if(!bInitThread)
			{

			pchBuffer = NULL;
			ulBufferLen = 0;

/*	if(g_ptabmsgr)
	{

			g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:socket_traffic", "%d.%d.%d.%d (%d): getting", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,	pClient->m_socket

				);


	}
*/			
			int nReturnCode = net.GetLine((unsigned char**)&pchBuffer, &ulBufferLen, pClient->m_socket, NET_RCV_ONCE, errorstring);
/*
	if(g_ptabmsgr)
	{

			g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:socket_traffic", "%d.%d.%d.%d (%d): got %d, %d bytes", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,	pClient->m_socket, nReturnCode, ulBufferLen

				);


	}
	*/		
			if((nReturnCode == NET_SUCCESS)&&(ulBufferLen>0))  // put the length check here.  for now.  recv() returning 0 on disconnect returns 0 len but success.
			{
				ulRetry = 0;
				//process any received XML.
				// have to keep accumulating until we find an <EOT> char.
				if((pchBuffer)&&(ulBufferLen)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
				{
					int nLen = 0;
					if(pchXMLStream) nLen = ulAccumulatedBufferLen;
					pch = (char*) malloc(nLen+ulBufferLen+1);  //term 0
					if(pch)
					{
						char* pchEnd = NULL;
						char* pchNext = NULL;
						if(pchXMLStream)  // we have an old buffer.
						{
							memcpy(pch, pchXMLStream, ulAccumulatedBufferLen);
							memcpy(pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen);
							free(pchXMLStream); 
							pchXMLStream = pch;  // reassign!
							ulAccumulatedBufferLen += ulBufferLen;
							*(pch+ulAccumulatedBufferLen) = 0;  // null term
						}
						else
						{
							// this is new.
							// first we have to skip all chars that are not a '<'
							// pchEnd = strchr(pchBuffer, '<');  // have to find first tag, we are only interested in XML.
							// used to use strchr but if there are leading zeros in the buffer, we never get past them

							pchEnd = pchBuffer;
//							while((*pchEnd!='<')&&(pchEnd<pchBuffer+ulBufferLen)) pchEnd++;  // no skipping, not XML

							if(pchEnd<pchBuffer+ulBufferLen)
							{
								strcpy(pch, pchEnd);
								ulAccumulatedBufferLen += (ulBufferLen-(pchEnd-pchBuffer));
								pchXMLStream = pch;  // reassign!
								*(pch+ulAccumulatedBufferLen) = 0;  // null term
							}
							else  // not found!
							{
								free(pch);
								pchXMLStream = NULL;
							}
						}

						if(pchBuffer) free(pchBuffer);
						pchBuffer = NULL;
						if(pchXMLStream)
						{

							pchEnd = strchr(pchXMLStream, 0x04);  //EOT = 4
							while((pchEnd)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
							{
							// found a token. EOT
								pchEnd++;

								pchNext = pchEnd;
								while((*pchNext!=0x01)&&(pchNext<pchEnd+strlen(pchEnd))) pchNext++; // SOH = 1

								if(pchNext<pchEnd+strlen(pchEnd))
								{
									// we found a remainder.
									nLen = strlen(pchNext);
									pch = (char*) malloc(nLen+1);  //term 0
									if(pch)
									{
										memcpy(pch, pchNext, nLen);
										*(pch+nLen) = 0;
									}
								}
								else pch = NULL;


								pchXML = pchXMLStream; // just use it.
								*pchEnd = 0; //null terminate it

								pchXMLStream = pch;  // take the rest of the stream.
								if(pchXMLStream) ulAccumulatedBufferLen = strlen(pchXMLStream);
								else ulAccumulatedBufferLen=0;

								_ftime(&timeactive);  // reset the inactivity timer.

//GENERAL MESSAGE FORMAT

//In the SIMPLE protocol, the general form of a message is:

// <SOH><unit number>,<command code>[,[protocol ID],[protocol version number]]<STX>[message body]<ETX><checksum char 1><checksum char 2><EOT>
// Note that two ASCII zeroes are always considered a correct checksum. If a nonzero checksum is present then it should be verified and if incorrect the message should be discarded.

								// (char*)pchXML) has the data now

								// parse and do stuff.

								CVFSimpleCmd cmd;
								CVFSimpleUtil util;

if(g_settings.m_bLogTransactions)
{
	if(g_ptabmsgr)
	{
			char* b=util.PrintCommand(pchXML);

			g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:socket_traffic", "%d.%d.%d.%d (%d): Rx: %s", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno, pClient->m_socket, b?b:"(null)");

			if(b) free(b);
	}

}
/*
if((pdata)&&(pdata->m_bLogTransactions))//&&(pMsgLog))
{

		FILE* logfp = fopen(LOGFILENAME, "ab");
		if(logfp)
		{
			_timeb timestamp;
			_ftime(&timestamp);
			char logtmbuf[48]; // need 33;
			tm* theTime = localtime( &timestamp.time	);
			strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

//AfxMessageBox(pchSnd);

			char* b=util.PrintCommand(pchXML);

			fprintf(logfp, "%s%03d %d.%d.%d.%d: Rx: %s\r\n", 
				logtmbuf, timestamp.millitm,
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno, b?b:"(null)");

			if(b) free(b);


			fclose(logfp);
		}
}

*/

								
//AfxMessageBox(pchXML);

								int nParse = util.ParseCommand(pchXML, &cmd);
//AfxMessageBox("02");
								if(nParse>=0)
								{
									char pchResponse[1024];
									unsigned long ulBufLen =0;
//if(cmd.m_pchCommand) AfxMessageBox(cmd.m_pchCommand);
//else AfxMessageBox("null");


									if(cmd.m_pchUnitNumber) free(cmd.m_pchUnitNumber);   
									cmd.m_pchUnitNumber=NULL;

									EnterCriticalSection(&g_settings.m_crit);

									if(g_settings.m_pszUnitNumber)
									{
										cmd.m_pchUnitNumber = (char*)malloc(strlen(g_settings.m_pszUnitNumber)+1);
										if(cmd.m_pchUnitNumber) strcpy(cmd.m_pchUnitNumber, g_settings.m_pszUnitNumber); 
									}
									else
									{
										cmd.m_pchUnitNumber = (char*)malloc(2);
										if(cmd.m_pchUnitNumber) strcpy(cmd.m_pchUnitNumber, "0"); 
									}
									LeaveCriticalSection(&g_settings.m_crit);


									if(stricmp(cmd.m_pchCommand, "SR")==0)
									{
										pchResponse[0] = 0x06; //ACK
										pchResponse[1] = 0;
										ulBufLen = 1;				
										
										if((cmd.m_pchMessage)&&(strlen(cmd.m_pchMessage)>2))
										{
											int nGPI = atoi(cmd.m_pchMessage) -1;
											if((pdata)&&(nGPI>=0)&&(nGPI<VF_MAX_BUTTONS))
											{
	EnterCriticalSection(&pdata->m_critButtons);
												
												pdata->m_Buttons[nGPI].m_ulStatus = (*(cmd.m_pchMessage+2)=='0')?VIDEOFRAME_BUTTON_STATUS_OFF:VIDEOFRAME_BUTTON_STATUS_ON;

//												pdata->m_bTallyState[nGPI] = (*(cmd.m_pchMessage+2)=='0')?FALSE:TRUE;

	// set the color back.
//		pdata->m_button[nGPI].SetDisabledColor((pdata->m_bTallyState[nGPI]?0x00005500:GetSysColor(COLOR_BTNFACE)));
//		pdata->m_button[nGPI].SetBGColor((pdata->m_bTallyState[nGPI]?0x0000ff00:GetSysColor(COLOR_BTNFACE)), TRUE);

//												pdata->SetButtonText();
												pdata->m_Buttons[nGPI].UpdateStatus();
												if(pdata->m_Buttons[nGPI].m_ulStatus == VIDEOFRAME_BUTTON_STATUS_ON) pdata->ButtonEventAction(nGPI);

//												pdata->m_button[nGPI].Invalidate();
	LeaveCriticalSection(&pdata->m_critButtons);

											}
		EnterCriticalSection(&u.m_critThreadSync);
											u.SetThreadSync(VF_CMD_GTR);
		LeaveCriticalSection(&u.m_critThreadSync);
										}
									}
									else if(stricmp(cmd.m_pchCommand, "SRS")==0)
									{
										pchResponse[0] = 0x06; //ACK
										pchResponse[1] = 0;
										ulBufLen = 1;				
										
										if((cmd.m_pchMessage)&&(strlen(cmd.m_pchMessage)>0))
										{
											if(pdata)
											{
												int nGPI = 0;
	EnterCriticalSection(&pdata->m_critButtons);
												int nLen = strlen(cmd.m_pchMessage);

												while(
															 (nGPI<VF_MAX_BUTTONS)&&(nGPI<nLen)
														 )
												{
													unsigned long ulStatus = pdata->m_Buttons[nGPI].m_ulStatus;

													if(*(cmd.m_pchMessage+nGPI)=='0')
													{
														pdata->m_Buttons[nGPI].m_ulStatus = VIDEOFRAME_BUTTON_STATUS_OFF;
													}
													else
													if(*(cmd.m_pchMessage+nGPI)=='1')
													{
														pdata->m_Buttons[nGPI].m_ulStatus = VIDEOFRAME_BUTTON_STATUS_ON;
													}
													else break; // stop processing if we have a char that is not 0 or 1
//													pdata->m_bTallyState[nGPI] = (cmd.m_pchMessage[nGPI]=='0')?FALSE:TRUE;

			// set the color back.
		//		pdata->m_button[nGPI].SetDisabledColor((pdata->m_bTallyState[nGPI]?0x00005500:GetSysColor(COLOR_BTNFACE)));
		//		pdata->m_button[nGPI].SetBGColor((pdata->m_bTallyState[nGPI]?0x0000ff00:GetSysColor(COLOR_BTNFACE)), TRUE);

	//												pdata->SetButtonText();
													if(ulStatus != pdata->m_Buttons[nGPI].m_ulStatus)
													{
														pdata->m_Buttons[nGPI].UpdateStatus();
														if(pdata->m_Buttons[nGPI].m_ulStatus == VIDEOFRAME_BUTTON_STATUS_ON) pdata->ButtonEventAction(nGPI); // on button down!
													}
	//												pdata->m_button[nGPI].Invalidate();
													
													nGPI++;

												}
											}
	LeaveCriticalSection(&pdata->m_critButtons);

		EnterCriticalSection(&u.m_critThreadSync);
											u.SetThreadSync(VF_CMD_GTR);
		LeaveCriticalSection(&u.m_critThreadSync);
										}
									}
									else if(stricmp(cmd.m_pchCommand, "SGTX")==0)
									{
//AfxMessageBox("1");

										if((pdata)&&(cmd.m_pchMessage)&&(pThreadSync))
										{
	EnterCriticalSection(&u.m_critThreadSync);
											if(stricmp(cmd.m_pchMessage, "1")==0)
											{
												pThreadSync->m_bEnableTransmitGPI = TRUE;
											}
											else
											{
	EnterCriticalSection(&pThreadSync->m_critSequence);
												pThreadSync->m_nSequence = 1;
	LeaveCriticalSection(&pThreadSync->m_critSequence);
												pThreadSync->m_bEnableTransmitGPI = FALSE;
											}
	LeaveCriticalSection(&u.m_critThreadSync);

//AfxMessageBox("2");
										// now create a response

//An example of the initial response to an 'SGTX' command sent from a test utility is as follows:
//<SOH>0,SGTX<STX>0000000000000000,11<ETX>cb<EOT>

											if(cmd.m_pchMessage) free(cmd.m_pchMessage);   
											cmd.m_pchMessage=NULL;
	EnterCriticalSection(&pThreadSync->m_critSequence);
	EnterCriticalSection(&pdata->m_critButtons);
											pdata->GPIMessage(&cmd.m_pchMessage, pThreadSync->m_nSequence);
	LeaveCriticalSection(&pdata->m_critButtons);
	LeaveCriticalSection(&pThreadSync->m_critSequence);

//AfxMessageBox("3");

//AfxMessageBox(cmd.m_pchMessage);
//AfxMessageBox("3");
											char* pchTemp = NULL;
											util.CreateCommand(&cmd, &pchTemp);
//AfxMessageBox("x");
											if(pchTemp)
											{
//AfxMessageBox(pchTemp);
												strcpy(pchResponse,pchTemp);
												free(pchTemp);
												ulBufLen = strlen(pchResponse);
											}
//AfxMessageBox("4");
											
	EnterCriticalSection(&pThreadSync->m_critSequence);
											pThreadSync->IncrementSequence();
	LeaveCriticalSection(&pThreadSync->m_critSequence);
//											pdata->m_nSequence++; 
//											if(pdata->m_nSequence>9999) pdata->m_nSequence=1;

		EnterCriticalSection(&u.m_critThreadSync);
											u.SetThreadSync(VF_CMD_SGTX);
											pThreadSync->m_bTransmitSGTX = false;  // set to false for this thread, since we are sending it directly.
		LeaveCriticalSection(&u.m_critThreadSync);

										}

									}


//											unsigned long ulBufLen = strlen(pchResponse); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)pchResponse, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{

if(g_settings.m_bLogTransactions)
{
	if(g_ptabmsgr)
	{
			g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:socket_traffic", "Error %d sending response message to %d.%d.%d.%d (%d): %s.  Resetting connection.", 
				nReturn,
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,pClient->m_socket, pszStatus);
	}

}
/*
if((pdata)&&(pdata->m_bLogTransactions))//&&(pMsgLog))
{

		FILE* logfp = fopen(LOGFILENAME, "ab");
		if(logfp)
		{
			_timeb timestamp;
			_ftime(&timestamp);
			char logtmbuf[48]; // need 33;
			tm* theTime = localtime( &timestamp.time	);
			strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

//AfxMessageBox(pchSnd);

			fprintf(logfp, "%s%03d Error %d sending response message to %d.%d.%d.%d: %s\r\nResetting connection.\r\n", 
				logtmbuf, timestamp.millitm,
				nReturn, pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno, pszStatus);

			fclose(logfp);
		}
}
*/
												break; // break out and discontinue sending.
											}
											else
											{
/*
												if(pdata)
												{

													int nCount = pdata->m_lcResponses.GetItemCount();
													pdata->m_lcResponses.InsertItem(nCount, "<-");
													char* a=util.PrintCommand(pchXML);
													char* b=util.PrintCommand(pchResponse);

													pdata->m_lcResponses.SetItemText(nCount, 1, a?a:"(null)" );
													pdata->m_lcResponses.SetItemText(nCount, 2, b?b:"(null)" );
													pdata->m_lcResponses.EnsureVisible(nCount, FALSE);
													while(nCount>LISTBOXMAX){  pdata->m_lcResponses.DeleteItem(0); nCount--; } //delete oldest
													if(a) free(b);
													if(b) free(b);
												}
*/

if(g_settings.m_bLogTransactions)
{
	if((g_ptabmsgr)&&(ulBufLen>0))
	{
			char* b=util.PrintCommand(pchResponse);

			g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:socket_traffic", "%d.%d.%d.%d (%d): Tx: %s", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno, pClient->m_socket, b?b:"(null)");

			if(b) free(b);
	}

}
/*
if((pdata)&&(pdata->m_bLogTransactions))//&&(pMsgLog))
{

		FILE* logfp = fopen(LOGFILENAME, "ab");
		if(logfp)
		{
			_timeb timestamp;
			_ftime(&timestamp);
			char logtmbuf[48]; // need 33;
			tm* theTime = localtime( &timestamp.time	);
			strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

//AfxMessageBox(pchSnd);

			char* b=util.PrintCommand(pchResponse);

			fprintf(logfp, "%s%03d %d.%d.%d.%d: Tx: %s\r\n", 
				logtmbuf, timestamp.millitm,
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno, b?b:"(null)");

			if(b) free(b);


			fclose(logfp);
		}
}
*/

											//reset inactivity timer

												_ftime( &timestamp );
												ulConnLastMessage = timestamp.time;


											}
										
									
								}
								else
								{
									// bad parse.

if(g_settings.m_bLogTransactions)
{
	if(g_ptabmsgr)
	{
			char* b=util.PrintCommand(pchXML);

			g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:socket_traffic", "%d.%d.%d.%d (%d): Error %d Bad command: %s", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno, pClient->m_socket, nParse, b?b:"(null)");

			if(b) free(b);
	}

}
/*
if((pdata)&&(pdata->m_bLogTransactions))//&&(pMsgLog))
{

		FILE* logfp = fopen(LOGFILENAME, "ab");
		if(logfp)
		{
			_timeb timestamp;
			_ftime(&timestamp);
			char logtmbuf[48]; // need 33;
			tm* theTime = localtime( &timestamp.time	);
			strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

//AfxMessageBox(pchSnd);

			char* b=util.PrintCommand(pchXML);

			fprintf(logfp, "%s%03d %d.%d.%d.%d: Error %d Bad command: %s\r\n", 
				logtmbuf, timestamp.millitm,
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno, nParse, b?b:"(null)");

			if(b) free(b);

			fclose(logfp);
		}
}
	*/
								}


//AfxMessageBox("002");
/*
								if(pucUUID)
								{
									try { RpcStringFree(&pucUUID); } catch(...){}
								}
								pucUUID = NULL;

//AfxMessageBox("003");
*/
								if(pchXML) 
								{
									try { free(pchXML); } catch(...){}
								}
								pchXML = NULL;
//AfxMessageBox("004");
/*
								if(pchRXID) 
								{
									try { free(pchRXID); } catch(...){}
								}
								pchRXID = NULL;
*/
//AfxMessageBox("005");
								if (pchXMLStream) 
									pchEnd = strchr(pchXMLStream, 0x04); //EOT = 4
								else pchEnd = NULL;
//AfxMessageBox("006");
							}  // while there are messages
						}//if(pchXMLStream)
						// dont do a free(pch);  
					}  // else out of mem, so just skip
					else
					{
						// out of mem.. .log error?
					}

				} //if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))

			} // if getline succeeds.
			else
			{
				// here, we either have timed out because theres no data, or, the connection has been lost.
				// if we lost the conn, no need to re-establish, we are a client handler, let the client reconnect if they want.
				if( 
					  (nReturnCode == NET_ERROR_CONN)		// connection lost
					||(ulBufferLen==0)  // addded this.  needed to porcess disconnect.
					)
				{
					ulRetry++;

if(g_settings.m_bLogTransactions)
{
	if(g_ptabmsgr)
	{
			g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:socket_traffic", "Lost connection from %d.%d.%d.%d on socket %d.", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno, 
				pClient->m_socket);
	}

}
/*
if((pdata)&&(pdata->m_bLogTransactions))//&&(pMsgLog))
{

		FILE* logfp = fopen(LOGFILENAME, "ab");
		if(logfp)
		{
			_timeb timestamp;
			_ftime(&timestamp);
			char logtmbuf[48]; // need 33;
			tm* theTime = localtime( &timestamp.time	);
			strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

//AfxMessageBox(pchSnd);

			fprintf(logfp, "%s%03d Lost connection from %d.%d.%d.%d on socket %d.\r\n", 
				logtmbuf, timestamp.millitm,
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,
				pClient->m_socket);


			fclose(logfp);
		}
}
*/
					break;  // break out of while loop, closes connection, starts again
				}
				else
				{
//									g_pnucleus->m_msgr.DM(MSG_ICONERROR, NULL, szNucleusSource, "check %d", clock());  //(Dispatch message)

					// we prob just havent gotten a message because no data.  
					// so, check to see if its time for publication service
									// no, do this in the stall loop


				}

			} // end of else from if(nReturnCode == NET_SUCCESS)


			if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;}
			
			if(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) Sleep(1);  // dont peg processor


			} //if(!bInitThread) // want to skip the getting of data right at the beginning, in case there isnt any and the connection just closes.
      bInitThread = false;  // the following will do that check and then go back up to get the data if there is any - or deal with it if the connection closes

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Videoframe:ClientHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
						break; // out of while to close thigns out
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
//						g_pnucleus->m_msgr.DM(MSG_ICONERROR, NULL, szNucleusSource, "check %d", clock());  //(Dispatch message)

/*
						if(nPeriodic>0)
						{
							_ftime( &timestamp );
							if((timeperiodic.time + nPeriodic)< timestamp.time)
							{
								// send asynchronous message

								//create a response UUID
								UUID uuida;
								UuidCreate(&uuida); // could check response code to be RPC_S_OK ?

								unsigned char* pucUUIDa = NULL;
								UuidToString(&uuida, &pucUUIDa);

								msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUIDa);

								if(pucUUIDa)
								{
									try { RpcStringFree(&pucUUIDa); } catch(...){}
								}
								pucUUIDa = NULL;


								msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"system\" code=\"0\">");

								msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
								msg.ClearContent(CX_XML_BUFFER_FLAGS);

								unsigned long ulDataLen = 0;
								char* pchStatus = g_pnucleus->m_data.GetStatusText(&ulDataLen);
								if(pchStatus)
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0x%08x</code><text>%s</text></status><time>%d.%03d</time></system>",
										ulDataLen,
										pchStatus,
										timestamp.time, timestamp.millitm
										);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0xffffffff</code><text>unavailable</text></status><time>%d.%03d</time></system>",
										timestamp.time, timestamp.millitm
										);

								}

								if(pchStatus){try { free(pchStatus); } catch(...){} pchStatus=NULL;}

								ulDataLen = 0;
								if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
								{
									ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
								}
								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
									ulDataLen,
									net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
									);

								msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>%s status message</msg>", 
									(g_pnucleus->m_settings.m_pszName?g_pnucleus->m_settings.m_pszName:"Nucleus"));

								msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


								// now, send and log.
	////////////////////////////////////////////// code copied from above.  if modified, must be 2 places.  sorry...
									// send it
			_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_pnucleus)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending system message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_pnucleus->m_msgr.DM(MSG_ICONERROR, NULL, szNucleusSource, errorstring);  //(Dispatch message)
				g_pnucleus->SendMsg(CX_SENDMSG_ERROR, szNucleusSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
								
	//AfxMessageBox("oop");
									// log it

									//debug file write
									if((g_pnucleus)&&(g_pnucleus->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
									{
										strcpy(filename, "Logs");
										_mkdir(filename);  // if exists already np
										strcat(filename, "\\");
										sprintf(errorstring, "%d.%d.%d.%d", 
											pClient->m_si.sin_addr.s_net, 
											pClient->m_si.sin_addr.s_host, 
											pClient->m_si.sin_addr.s_lh, 
											pClient->m_si.sin_addr.s_impno);

										strcat(filename, errorstring);

										_mkdir(filename);  // if exists already np

		//								_ftime( &timestamp );  // use timestamp we just had..

										tm* theTime = localtime( &timestamp.time	);

										sprintf(errorstring, "\\%s_", 
											(g_pnucleus->m_settings.m_pszName?g_pnucleus->m_settings.m_pszName:"Nucleus"));

										strcat(filename, errorstring);

										char filenametemp[MAX_PATH];
										strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

										strcat(filename, filenametemp);

										int nOffset = strlen(filename);
										sprintf(filename+nOffset,"%03d",timestamp.millitm);

										if (strcmp(lasttxfilename, filename)==0)
										{
											nTxDupes++;
										}
										else
										{
											nTxDupes=0;
											strcpy(lasttxfilename, filename);
										}
										nOffset = strlen(filename);
										sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

										fp = fopen(filename, "wb");
										if(fp)
										{
											int b=CX_XML_BUFFER_CORTEXBEGIN;
											while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
											{
	//											fwrite(filename, 1, strlen(filename),fp);
												if(msg.m_pchResponse[b])
												{
													if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

													fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
													fflush(fp);
												}
												b++;

											}
											fclose(fp);
											fp = NULL;
										}
									}


									b=CX_XML_BUFFER_TX;
									while(b<CX_XML_BUFFER_DATAEND)
									{
										if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
										{
											try { free(msg.m_pchResponse[b]); } catch(...){}
											msg.m_pchResponse[b] = NULL;
										}
										b++;
									}
	/////////////////////////////////////////////
							}

						}
*/
/*
//no - use built in timeout setting
						//and check timeout
						int nTimed = 60; // 60 second timeout
						if(nTimed>0)
						{
							_ftime( &timestamp );
							if((timeactive.time + nTimed) < timestamp.time)
							{
								//kill it

								bCloseCommand = true;
							}
							
						}
*/

						// and lets check if we've timed out on the connection, no matter what the success was
						_ftime( &timestamp );
						if(
								(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
							&&(ulConnTimeout>0)
							&&(ulConnLastMessage>0)
							&&((ulConnLastMessage + ulConnTimeout) < (unsigned long)timestamp.time )
							)
						{
							// we timed out, need to disconnect
							ulRetry++;

			if(g_settings.m_bLogTransactions)
			{
				if(g_ptabmsgr)
				{
						g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:socket_traffic", "Connection from %d.%d.%d.%d on socket %d timed out.", 
							pClient->m_si.sin_addr.s_net, 
							pClient->m_si.sin_addr.s_host, 
							pClient->m_si.sin_addr.s_lh, 
							pClient->m_si.sin_addr.s_impno, pClient->m_socket);
				}
			}
			/*
			if((pdata)&&(pdata->m_bLogTransactions))//&&(pMsgLog))
			{

					FILE* logfp = fopen(LOGFILENAME, "ab");
					if(logfp)
					{
						_timeb timestamp;
						_ftime(&timestamp);
						char logtmbuf[48]; // need 33;
						tm* theTime = localtime( &timestamp.time	);
						strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

			//AfxMessageBox(pchSnd);

						fprintf(logfp, "%s%03d Connection from %d.%d.%d.%d on socket %d timed out.\r\n", 
							logtmbuf, timestamp.millitm,
							pClient->m_si.sin_addr.s_net, 
							pClient->m_si.sin_addr.s_host, 
							pClient->m_si.sin_addr.s_lh, 
							pClient->m_si.sin_addr.s_impno,
							pClient->m_socket);


						fclose(logfp);
					}
			}
			*/
							if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;} //free this memory before breaking out of the while.

							bCloseCommand = true;
							break;  // break out of while loop, closes connection, starts again
						}


						// check if need to send anything

						if((pdata)&&(pThreadSync))
						{
							CString src; 


							char pchResponse[1024];
							unsigned long ulBufLen =0;

							CVFSimpleCmd cmd;
							CVFSimpleUtil util;

							bool bAsRun = false;

							if((pThreadSync->m_bEnableTransmitGPI)&&(pThreadSync->m_bTransmitGPI))
							{
								src.Format("GetRelays");

								EnterCriticalSection(&g_settings.m_crit);

								if(g_settings.m_pszUnitNumber)
								{
									cmd.m_pchUnitNumber = (char*)malloc(strlen(g_settings.m_pszUnitNumber)+1);
									if(cmd.m_pchUnitNumber) strcpy(cmd.m_pchUnitNumber, g_settings.m_pszUnitNumber); 
								}
								else
								{
									cmd.m_pchUnitNumber = (char*)malloc(2);
									if(cmd.m_pchUnitNumber) strcpy(cmd.m_pchUnitNumber, "0"); 
								}

								LeaveCriticalSection(&g_settings.m_crit);

								cmd.m_pchCommand = (char*)malloc(4);
								if(cmd.m_pchCommand) strcpy(cmd.m_pchCommand, "GTR");

	EnterCriticalSection(&pThreadSync->m_critSequence);
	EnterCriticalSection(&pdata->m_critButtons);
								pdata->GPIMessage(&cmd.m_pchMessage, pThreadSync->m_nSequence);
	LeaveCriticalSection(&pdata->m_critButtons);
	LeaveCriticalSection(&pThreadSync->m_critSequence);

								char* pchTemp = NULL;
								util.CreateCommand(&cmd, &pchTemp);
								if(pchTemp)
								{
									strcpy(pchResponse,pchTemp);
									free(pchTemp);
									ulBufLen = strlen(pchResponse);

									if((ulBufLen>0)&&(g_settings.m_bAsRunGTR)) bAsRun = true;
								}
								
	EnterCriticalSection(&pThreadSync->m_critSequence);
								pThreadSync->IncrementSequence();
	LeaveCriticalSection(&pThreadSync->m_critSequence);
//								pdata->m_nSequence++; 
//								if(pdata->m_nSequence>9999) pdata->m_nSequence=1;


								pThreadSync->m_bTransmitGPI=false;
							}
							else  // put else so only one gets done per round
							if((pThreadSync->m_bEnableTransmitGPI)&&(pThreadSync->m_bTransmitSGTX)) // let's make sure that SGTX was enabled on the other connections before sending them these async queries.
							{
								src.Format("SGTX");

								EnterCriticalSection(&g_settings.m_crit);

								if(g_settings.m_pszUnitNumber)
								{
									cmd.m_pchUnitNumber = (char*)malloc(strlen(g_settings.m_pszUnitNumber)+1);
									if(cmd.m_pchUnitNumber) strcpy(cmd.m_pchUnitNumber, g_settings.m_pszUnitNumber); 
								}
								else
								{
									cmd.m_pchUnitNumber = (char*)malloc(2);
									if(cmd.m_pchUnitNumber) strcpy(cmd.m_pchUnitNumber, "0"); 
								}

								LeaveCriticalSection(&g_settings.m_crit);

								cmd.m_pchCommand = (char*)malloc(5);
								if(cmd.m_pchCommand) strcpy(cmd.m_pchCommand, "SGTX");

	EnterCriticalSection(&pThreadSync->m_critSequence);
	EnterCriticalSection(&pdata->m_critButtons);
								pdata->GPIMessage(&cmd.m_pchMessage, pThreadSync->m_nSequence);
	LeaveCriticalSection(&pdata->m_critButtons);
	LeaveCriticalSection(&pThreadSync->m_critSequence);

								char* pchTemp = NULL;
								util.CreateCommand(&cmd, &pchTemp);
								if(pchTemp)
								{
									strcpy(pchResponse,pchTemp);
									free(pchTemp);
									ulBufLen = strlen(pchResponse);

									if((ulBufLen>0)&&(g_settings.m_bAsRunSGTX)) bAsRun = true;

								}
								
	EnterCriticalSection(&pThreadSync->m_critSequence);
								pThreadSync->IncrementSequence();
	LeaveCriticalSection(&pThreadSync->m_critSequence);
//								pdata->m_nSequence++; 
//								if(pdata->m_nSequence>9999) pdata->m_nSequence=1;


								pThreadSync->m_bTransmitSGTX=false;
							}
							else  // put else so only one gets done per round
							{

	EnterCriticalSection(&pdata->m_critDTMF);							
								if((pThreadSync->m_bTransmitDTMF)&&(pdata->m_szDTMF.GetLength()>0))
								{
									src.Format("DTMF");

									EnterCriticalSection(&g_settings.m_crit);

									if(g_settings.m_pszUnitNumber)
									{
										cmd.m_pchUnitNumber = (char*)malloc(strlen(g_settings.m_pszUnitNumber)+1);
										if(cmd.m_pchUnitNumber) strcpy(cmd.m_pchUnitNumber, g_settings.m_pszUnitNumber); 
									}
									else
									{
										cmd.m_pchUnitNumber = (char*)malloc(2);
										if(cmd.m_pchUnitNumber) strcpy(cmd.m_pchUnitNumber, "0"); 
									}

									LeaveCriticalSection(&g_settings.m_crit);

									cmd.m_pchCommand = (char*)malloc(5);
									if(cmd.m_pchCommand) strcpy(cmd.m_pchCommand, "GSDA");


									cmd.m_pchMessage = (char*)malloc(pdata->m_szDTMF.GetLength()+32);
									if(cmd.m_pchMessage) sprintf(cmd.m_pchMessage, "%d,%d,\"%s\"", g_settings.m_nDTMFport, pdata->m_szDTMF.GetLength(), pdata->m_szDTMF);


									char* pchTemp = NULL;
									util.CreateCommand(&cmd, &pchTemp);
									if(pchTemp)
									{
										strcpy(pchResponse,pchTemp);
										free(pchTemp);
										ulBufLen = strlen(pchResponse);
										if((ulBufLen>0)&&(g_settings.m_bAsRunDTMF)) bAsRun = true;
									}
									
									pThreadSync->m_bTransmitDTMF=false;
								}
	LeaveCriticalSection(&pdata->m_critDTMF);

							}


							if(ulBufLen>0)
							{
											int nReturn = net.SendLine((unsigned char*)pchResponse, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{


	if(bAsRun)
	{
		CString msg; 
		msg.Format("Error %d sending command to %d.%d.%d.%d (%d): %s", 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno,	pClient->m_socket,
					pszStatus);

		g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, src.GetBuffer(0), g_settings.m_pszInternalAppName, msg.GetBuffer(0));
	}

if(g_settings.m_bLogTransactions)
{
	if(g_ptabmsgr)
	{
			g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:socket_traffic", "Error %d sending message to %d.%d.%d.%d on socket %d: %s.  Resetting connection.", 
				nReturn,
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno, 
				pClient->m_socket,
				pszStatus);
	}
}
/*
if((pdata)&&(pdata->m_bLogTransactions))//&&(pMsgLog))
{

		FILE* logfp = fopen(LOGFILENAME, "ab");
		if(logfp)
		{
			_timeb timestamp;
			_ftime(&timestamp);
			char logtmbuf[48]; // need 33;
			tm* theTime = localtime( &timestamp.time	);
			strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

//AfxMessageBox(pchSnd);

			fprintf(logfp, "%s%03d Error %d sending message to %d.%d.%d.%d: %s\r\nResetting connection.\r\n", 
				logtmbuf, timestamp.millitm,
				nReturn, pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno, pszStatus);

			fclose(logfp);
		}
}
*/

												break; // break out and discontinue sending.
											}
											else
											{

/*
												if(pdata)
												{
													int nCount = pdata->m_lcResponses.GetItemCount();
													pdata->m_lcResponses.InsertItem(nCount, "->");
													char* b=util.PrintCommand(pchResponse);
													pdata->m_lcResponses.SetItemText(nCount, 2, b?b:"(null)" );
													pdata->m_lcResponses.EnsureVisible(nCount, FALSE);
													while(nCount>LISTBOXMAX){  pdata->m_lcResponses.DeleteItem(0); nCount--; } //delete oldest
													if(b) free(b);
												}
*/

	char* b1=util.PrintCommand(pchResponse);

	if(bAsRun)
	{
		CString msg; 
		msg.Format("Command sent to %d.%d.%d.%d (%d): %s", 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno,	pClient->m_socket,
					b1?b1:"(null)");

		g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, src.GetBuffer(0), g_settings.m_pszInternalAppName, msg.GetBuffer(0));
	}

if(g_settings.m_bLogTransactions)
{
	if(g_ptabmsgr)
	{

			g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:socket_traffic", "%d.%d.%d.%d (%d): Tx (%s): %s", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,	pClient->m_socket,			
				cmd.m_pchCommand?cmd.m_pchCommand:"(null)",				
				b1?b1:"(null)");


	}

}
	if(b1) free(b1);

/*
if((pdata)&&(pdata->m_bLogTransactions))//&&(pMsgLog))
{

		FILE* logfp = fopen(LOGFILENAME, "ab");
		if(logfp)
		{
			_timeb timestamp;
			_ftime(&timestamp);
			char logtmbuf[48]; // need 33;
			tm* theTime = localtime( &timestamp.time	);
			strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

//AfxMessageBox(pchSnd);

			char* b=util.PrintCommand(pchResponse);

			fprintf(logfp, "%s%03d %d.%d.%d.%d: Tx (%s): %s\r\n", 
				logtmbuf, timestamp.millitm,
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno, 
				cmd.m_pchCommand?cmd.m_pchCommand:"(null)",				
				b?b:"(null)");

			if(b) free(b);


			fclose(logfp);
		}
}
*/

//	if(pdata) 	pdata->OnChangeEditDtmfPublic();

											}
							}

						}

						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
/*
	if(g_ptabmsgr)
	{

			g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:socket_traffic", "%d.%d.%d.%d (%d): Rx exists %d.%d.%d.%d", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,	pClient->m_socket,	

									(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) ,
								((pClient->m_ucType)&NET_TYPE_KEEPOPEN) ,
								(pClient->m_socket!=NULL),
								(!bCloseCommand)

				);


	}
*/						break; 
					}
				}
			}
		} 

cleanup:

//		AfxMessageBox("cleanup");
//AfxMessageBox("010");

//		pDoc->Release();  // destructor calls this so don't do it here. 
//		if(pSafety) pSafety->Release();

//AfxMessageBox("011");

// cleanup:  //was here but want to uninit com etc
//AfxMessageBox("012");
if(g_settings.m_bLogTransactions)
{
	if(g_ptabmsgr)
	{
			g_ptabmsgr->DM(MSG_ICONINFO, NULL, "VideoFrame:socket_connection", "Connection from %d.%d.%d.%d on socket %d closed.", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno, 
				pClient->m_socket);
	}
}
/*
if((pdata)&&(pdata->m_bLogTransactions))//&&(pMsgLog))
{

		FILE* logfp = fopen(LOGFILENAME, "ab");
		if(logfp)
		{
			_timeb timestamp;
			_ftime(&timestamp);
			char logtmbuf[48]; // need 33;
			tm* theTime = localtime( &timestamp.time	);
			strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

//AfxMessageBox(pchSnd);

			fprintf(logfp, "%s%03d Connection from %d.%d.%d.%d on socket %d closed.\r\n", 
				logtmbuf, timestamp.millitm,
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,
				pClient->m_socket);


			fclose(logfp);
		}
}
*/

//AfxMessageBox("014");

		if(pch!=NULL) { try{free(pch);} catch(...) {} } // must free the data buffer 

//AfxMessageBox("015");

		shutdown(pClient->m_socket, SD_BOTH);
//AfxMessageBox("016");
		closesocket(pClient->m_socket);
//AfxMessageBox("017");
		(*(pClient->m_pulConnections))--;
//AfxMessageBox("018");

/*
		if(((*(pClient->m_pulConnections))<=0)&&(pdata))
		{
			pdata->GetDlgItem(IDC_BUTTON_SENDDTMF)->EnableWindow(FALSE);
		}
*/

		EnterCriticalSection(&u.m_critThreadSync);
		if(pThreadSync) u.RemoveThreadSync(nThreadID);
		LeaveCriticalSection(&u.m_critThreadSync);
	}
//		AfxMessageBox("end cleanup");
//AfxMessageBox("019");
/*
	try
	{  
		CoUninitialize(); //XML
	}
	catch(...)
	{
		if(g_pnucleus) g_pnucleus->m_msgr.DM(MSG_ICONERROR, NULL, szNucleusSource, "Exception in CoUninitialize");  //(Dispatch message)
	}
*/
//AfxMessageBox("020");

	try
	{
		delete pClient; // was created with new in the thread that spawned this one.
	}	catch(...) {}

}

