// VideoFrameSettings.cpp : implementation file
//

#include "stdafx.h"
#include "VideoFrame.h"
#include "VideoFrameSettings.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


extern DLLdata_t      g_dlldata;
extern CVideoFrameData			g_data;
extern CVideoFrameSettings	g_settings;
extern CMessager*			g_ptabmsgr;
extern CVideoFrameApp			theApp;

/////////////////////////////////////////////////////////////////////////////
// CVideoFrameSettings dialog


CVideoFrameSettings::CVideoFrameSettings(/*CWnd* pParent /*=NULL*/)
//	: CDialog(CVideoFrameSettings::IDD, pParent)
{
/*
	//{{AFX_DATA_INIT(CVideoFrameSettings)
	m_bAutoconnect = FALSE;
	m_bTickerPlayOnce = TRUE;
	m_szHost = _T("127.0.0.1");
	m_szBaseURL = _T("http://www.feed.com/");
	m_nBatch = 250;
	m_nPort = TABULATOR_PORT_CMD;
	m_nPollIntervalMS = 5000;
	m_szProgramID = _T("10");
	m_nRequestIntervalMS = 0;
	m_nTickerPort = 7795;
	m_szTickerHost = _T("127.0.0.1");
	m_bSmut_ReplaceCurses = FALSE;
	m_nDoneCount = 3;
	//}}AFX_DATA_INIT
*/
	m_ulDebug = 0;

	m_pszAppName = NULL;
	m_pszInternalAppName = NULL;
	m_pszAboutText = NULL;
	m_pszSettingsText = NULL;

	m_pszButtons = NULL;  // the Buttons table name
	m_pszManualEvents = NULL;


	m_bAsRunGTR = false;
	m_bAsRunSGTX = false;
	m_bAsRunDTMF = true;

	m_bLogTransactions = false;
	m_bMillisecondMessaging=true;			// use millisecond resolution for messages and asrun


	m_bInitRequiresConnection=false;		// initialization condition
	m_bInitRequiresAuto=false;		// initialization condition
	m_bInitRequiresPass=false;		// initialization condition
	m_bInitRequiresBlock=false;	// initialization condition

	m_nNumButtons = 32;

//	m_pszHost=NULL;  // the VideoFrame host IP
	m_nPort=5050;  // the VideoFrame host port
	m_nDTMFport=1;
	m_pszUnitNumber = NULL;
	m_nConnectionTimeout = 60; // one minute  0 or less turns it off


	m_nMinKeypressDelay = 100;


	m_pszTabulatorHost=NULL;  // the Tabulator host IP or name
	m_nTabulatorPort = TABULATOR_PORT_CMD;  // the Tabulator host port

	m_nStartupDelay = 2000; // only required if there is a startup action.
	m_nStartupButton = -1; // only required if there is a startup button press to occur.


	m_pszTabulatorModule=NULL;  // module name, "Tabulator"
	m_pszTabulatorPlugInModule=NULL;  // the Tabulator module name (AutomationData)
//	m_pszTabulatorCommandReplaceManual=NULL;  // the Replace command with Manual events
//	m_pszTabulatorCommandReplaceCurrent=NULL;  // the Replace command with current event info
//	m_pszTabulatorCommandEnterPass=NULL;  // entering the PASS state
//	m_pszTabulatorCommandEnterBlock=NULL;  // entering the BLOCK state
	m_pszTabulatorCommandQueryReady=NULL;  // query the readiness state of the automation

	m_pszTabulatorCommandBreakEnd=NULL;  // the Break End command, send to StreamData dll 
	m_pszTabulatorCommandNationalBreak=NULL;  // the National Break command, send to StreamData dll 

	m_bNationalBreakPrerollIsDelay = true; // if true, waits this number of MS, and sends the command with preroll zero; if false, just sends the command with the preroll



/*
	m_pszMessageToken = NULL; 
	m_pszAux1Token = NULL; 
	m_pszAux2Token = NULL; 
*/

	// DSN params
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;

/*
	m_pszFeed = NULL;  // the Messages table name
	m_pszTicker = NULL;
*/
	
//	m_pszAsRunFilename = NULL;
//	m_pszAsRunDestination = NULL;

	m_pszMessages = NULL;  // the Messages table name
	m_pszAsRun = NULL;  // the As-run table name
//	m_pszEventView = NULL;  // the Event view name // to pull events if nec.
	m_pszSettings = NULL;

//	m_pszDestinationHost = NULL;

/*
	m_pszTickerMessagePreamble = NULL;

	m_pszTickerBackplateName = NULL; 
	m_pszTickerLogoName = NULL; 
	m_nTickerBackplateID=0; 
	m_nTickerLogoID=2; 
	m_nTickerTextID=1;
	m_nTriggerBuffer  =12;

	m_nTickerEngineType = VideoFrame_TICKER_ENGINE_INT; // send events to Libretto or wherever!

	m_nMaxMessages = 500;

	m_pszModule=NULL;  // the module name (Libretto)
	m_pszQueue=NULL;  // the module's queue table name (Queue of Libretto.dbo.Queue)

	m_pszInEvent=NULL;  // the event identifier of the transition in (Server_Ticker_In) command
	m_pszOutEvent=NULL;  // the event identifier of the transition out (Server_Ticker_Out) command
	m_pszTextEvent=NULL;  // the event identifier of the text message event

	m_nGraphicsHostType = TABULATOR_DESTTYPE_CHYRON_CHANNELBOX;

	m_bAutoPurgeExpired=false;
	m_bAutoPurgeExceeded=false;
	m_nAutoPurgeExpiredAfterMins=60;
	m_nAutoPurgeExceededAfterMins=60;

	m_bAutostartTicker=false;
	m_bAutostartData=false;
	m_bUseLocalTime=true;  // or use unixtime


	m_nLayoutSalvoFormat = TABULATOR_HARRIS_ICONII_LSFORMAT_ALPHA;
*/


	InitializeCriticalSection(&m_crit);
}

CVideoFrameSettings::~CVideoFrameSettings()
{
	EnterCriticalSection(&m_crit);
	if(m_pszAppName) free(m_pszAppName);
	if(m_pszInternalAppName) free(m_pszInternalAppName);
	if(m_pszAboutText) free(m_pszAboutText);
	if(m_pszSettingsText) free(m_pszSettingsText);

	if(m_pszDSN) free(m_pszDSN);
	if(m_pszUser) free(m_pszUser);
	if(m_pszPW) free(m_pszPW);
//	if(m_pszFeed) free(m_pszFeed);
//	if(m_pszTicker) free(m_pszTicker);
//	if(m_pszAsRunFilename) free(m_pszAsRunFilename);
/*
	if(m_pszTickerMessagePreamble) free(m_pszTickerMessagePreamble);
	if(m_pszTickerBackplateName) free(m_pszTickerBackplateName);
	if(m_pszTickerLogoName) free(m_pszTickerLogoName);
*/
	if(m_pszAsRun) free(m_pszAsRun);
	if(m_pszSettings) free(m_pszSettings);
//	if(m_pszDestinationHost) free(m_pszDestinationHost);
//	if(m_pszAsRunDestination) free(m_pszAsRunDestination);
/*
	if(m_pszModule) free(m_pszModule);
	if(m_pszQueue) free(m_pszQueue);
	if(m_pszInEvent) free(m_pszInEvent);
	if(m_pszOutEvent) free(m_pszOutEvent);
	if(m_pszTextEvent) free(m_pszTextEvent);

	if(m_pszMessageToken) free(m_pszMessageToken);
	if(m_pszAux1Token) free(m_pszAux1Token);
	if(m_pszAux2Token) free(m_pszAux2Token);
*/


	if(m_pszButtons) free(m_pszButtons);
	if(m_pszManualEvents) free(m_pszManualEvents);


	LeaveCriticalSection(&m_crit);
	DeleteCriticalSection(&m_crit);

	if(m_pszTabulatorHost) free(m_pszTabulatorHost);
	if(m_pszTabulatorModule) free(m_pszTabulatorModule);
	if(m_pszTabulatorPlugInModule) free(m_pszTabulatorPlugInModule);
//	if(m_pszTabulatorCommandReplaceManual) free(m_pszTabulatorCommandReplaceManual);
//	if(m_pszTabulatorCommandReplaceCurrent) free(m_pszTabulatorCommandReplaceCurrent);
//	if(m_pszTabulatorCommandEnterPass) free(m_pszTabulatorCommandEnterPass);
//	if(m_pszTabulatorCommandEnterBlock) free(m_pszTabulatorCommandEnterBlock);
	if(m_pszTabulatorCommandQueryReady) free(m_pszTabulatorCommandQueryReady);

	if(m_pszTabulatorCommandBreakEnd) free(m_pszTabulatorCommandBreakEnd);
	if(m_pszTabulatorCommandNationalBreak) free(m_pszTabulatorCommandNationalBreak);




}

/*

void CVideoFrameSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CVideoFrameSettings)
	DDX_Check(pDX, IDC_CHECK_AUTOCONNECT, m_bAutoconnect);
	DDX_Check(pDX, IDC_CHECK_PLAYONCE, m_bTickerPlayOnce);
	DDX_CBString(pDX, IDC_COMBO_HOST, m_szHost);
	DDX_Text(pDX, IDC_EDIT_BASEURL, m_szBaseURL);
	DDX_Text(pDX, IDC_EDIT_BATCH, m_nBatch);
	DDV_MinMaxInt(pDX, m_nBatch, 0, 2000000);
	DDX_Text(pDX, IDC_EDIT_C_PORT, m_nPort);
	DDV_MinMaxInt(pDX, m_nPort, 1024, 65535);
	DDX_Text(pDX, IDC_EDIT_POLL, m_nPollIntervalMS);
	DDV_MinMaxInt(pDX, m_nPollIntervalMS, 0, 86400000);
	DDX_Text(pDX, IDC_EDIT_PROGRAM, m_szProgramID);
	DDX_Text(pDX, IDC_EDIT_REQ, m_nRequestIntervalMS);
	DDV_MinMaxInt(pDX, m_nRequestIntervalMS, 0, 86400000);
	DDX_Text(pDX, IDC_EDIT_T_PORT, m_nTickerPort);
	DDV_MinMaxInt(pDX, m_nTickerPort, 1024, 65535);
	DDX_Text(pDX, IDC_EDIT_HOST, m_szTickerHost);
	DDX_Check(pDX, IDC_CHECK_SMUT, m_bSmut_ReplaceCurses);
	DDX_Text(pDX, IDC_EDIT_DONE, m_nDoneCount);
	DDV_MinMaxInt(pDX, m_nDoneCount, 0, 2000000);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CVideoFrameSettings, CDialog)
	//{{AFX_MSG_MAP(CVideoFrameSettings)
	ON_BN_CLICKED(IDC_BUTTON_TESTFEED, OnButtonTestfeed)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_CHECK_PLAYONCE, OnCheckPlayonce)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVideoFrameSettings message handlers

CString CVideoFrameSettings::RemoteSettingsToString()
{
	CString szSettings="";
	if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_TICKER)&&(g_dlldata.thread[VideoFrame_DATA])&&(g_dlldata.thread[VideoFrame_TICKER])) 
	{
		szSettings.Format(_T("%s%c%s%c%s%c%s%c%s%c%s%c%d%c%d%c%d%c%d%c%s%c%d%c%d%c%d%c%s%c%d%c%d%c%d"),
				g_dlldata.thread[VideoFrame_DATA]->pszThreadName, 28,
				g_dlldata.thread[VideoFrame_DATA]->pszBaseURL, 28,
				g_dlldata.thread[VideoFrame_DATA]->pszProgramParamName, 28,
				g_dlldata.thread[VideoFrame_DATA]->pszProgramID, 28,
				g_dlldata.thread[VideoFrame_DATA]->pszBatchParamName, 28,
				g_dlldata.thread[VideoFrame_DATA]->pszFromIDParamName, 28,
				g_dlldata.thread[VideoFrame_DATA]->nBatchParam, 28,
				g_dlldata.thread[VideoFrame_DATA]->nShortIntervalMS, 28,
				g_dlldata.thread[VideoFrame_DATA]->nLongIntervalMS, 28,
				(g_dlldata.thread[VideoFrame_DATA]->bDirParamStyle?1:0), 28,
				g_dlldata.thread[VideoFrame_TICKER]->pszThreadName, 28,
				g_dlldata.thread[VideoFrame_TICKER]->nBatchParam, 28,
				g_dlldata.thread[VideoFrame_TICKER]->nShortIntervalMS, 28,
				g_dlldata.thread[VideoFrame_TICKER]->nLongIntervalMS, 28,
				g_dlldata.thread[VideoFrame_TICKER]->pszHost, 28,
				g_dlldata.thread[VideoFrame_TICKER]->nPort, 28,
				g_dlldata.thread[VideoFrame_TICKER]->nAuxValue, 28,
				g_settings.m_nDoneCount);
	}

	return  szSettings;
}

int CVideoFrameSettings::StringToRemoteSettings(CString szSettings)
{
	if(szSettings.GetLength())
	{

		char chFields[2];
		sprintf(chFields,"%c",28);
		int f=0, n=0;
		CString szField = szSettings;
		CString szTemp;
		do
		{
			f=szField.Find(chFields);
			if(f>=0)
			{
				n++;
				szTemp = szField.Mid(f+1);
				szField=szTemp;
			}
		} while((f>=0)&&(szField.GetLength()>0)&&(n<=17));

//	AfxMessageBox(szSettings);

		f=0;
		if(n>=16)
		{
			while((f<=n)&&(f<18)) // prevent runaway
			{
				int i=szSettings.Find(chFields);
				if(i>-1){szField = szSettings.Left(i);}
				else {szField=szSettings;}  //last field.

				char* pch = NULL;
				char* pchTemp = NULL;
	//CString foo; foo.Format(_T("parsing field %d of %d: %s"), f, n, szField); AfxMessageBox(foo);
				switch(f)
				{
				case  0: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_DATA)&&(g_dlldata.thread[VideoFrame_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
								pchTemp = g_dlldata.thread[VideoFrame_DATA]->pszThreadName;
								g_dlldata.thread[VideoFrame_DATA]->pszThreadName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  1: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_DATA)&&(g_dlldata.thread[VideoFrame_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
								pchTemp = g_dlldata.thread[VideoFrame_DATA]->pszBaseURL;
								g_dlldata.thread[VideoFrame_DATA]->pszBaseURL  = pch;
								LeaveCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
								g_settings.m_szBaseURL = szField;
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  2: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_DATA)&&(g_dlldata.thread[VideoFrame_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
								pchTemp = g_dlldata.thread[VideoFrame_DATA]->pszProgramParamName;
								g_dlldata.thread[VideoFrame_DATA]->pszProgramParamName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  3: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_DATA)&&(g_dlldata.thread[VideoFrame_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
								pchTemp = g_dlldata.thread[VideoFrame_DATA]->pszProgramID;
								g_dlldata.thread[VideoFrame_DATA]->pszProgramID  = pch;
								LeaveCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
								g_settings.m_szProgramID = szField;
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  4: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_DATA)&&(g_dlldata.thread[VideoFrame_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
								pchTemp = g_dlldata.thread[VideoFrame_DATA]->pszBatchParamName;
								g_dlldata.thread[VideoFrame_DATA]->pszBatchParamName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  5: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_DATA)&&(g_dlldata.thread[VideoFrame_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
								pchTemp = g_dlldata.thread[VideoFrame_DATA]->pszFromIDParamName;
								g_dlldata.thread[VideoFrame_DATA]->pszFromIDParamName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
//									pData->pszToDateParamName = fu.GetIniString("DataDownload", "ToDateParamName", "To", pData->pszToDateParamName);   //  %p
//									pData->pszFromDateParamName = fu.GetIniString("DataDownload", "FromDateParamName", "From", pData->pszFromDateParamName);   //  %p
//									pData->pszNowParamName = fu.GetIniString("DataDownload", "NowParamName", "time", pData->pszNowParamName);   //  %p
//									pData->pszBatchParamName = fu.GetIniString("DataDownload", "BatchParamName", "limit", pData->pszBatchParamName);   //  %p
//									pData->pszFromIDParamName = fu.GetIniString("DataDownload", "FromIDParamName", "startid", pData->pszFromIDParamName);   //  %p
//									pData->pszGetIDParamName = fu.GetIniString("DataDownload", "GetIDParamName", "id", pData->pszGetIDParamName);   //  %p

//									pData->pszToDateParamName=NULL;				//  %t
//									pData->pszFromDateParamName=NULL;			//  %f
//									pData->pszNowParamName=NULL;					//  %n
//									pData->pszBatchParamName=NULL;				//  %b
//									pData->pszFromIDParamName=NULL;				//  %m  // start id
//									pData->pszGetIDParamName=NULL;				//  %i
				case  6: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_DATA)&&(g_dlldata.thread[VideoFrame_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
							g_dlldata.thread[VideoFrame_DATA]->nBatchParam  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
							g_settings.m_nBatch = atoi(szField);
						}
					} break;
				case  7: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_DATA)&&(g_dlldata.thread[VideoFrame_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
							g_dlldata.thread[VideoFrame_DATA]->nShortIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
							g_settings.m_nRequestIntervalMS = atoi(szField);
						}
					} break;
				case  8: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_DATA)&&(g_dlldata.thread[VideoFrame_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
							g_dlldata.thread[VideoFrame_DATA]->nLongIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
							g_settings.m_nPollIntervalMS = atoi(szField);
						}
					} break;
				case  9: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_DATA)&&(g_dlldata.thread[VideoFrame_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
							g_dlldata.thread[VideoFrame_DATA]->bDirParamStyle  = ((atoi(szField)>0)?true:false);
							LeaveCriticalSection(&g_dlldata.thread[VideoFrame_DATA]->m_crit);
						}
					} break;



				case  10: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_TICKER)&&(g_dlldata.thread[VideoFrame_TICKER])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[VideoFrame_TICKER]->m_crit);
								pchTemp = g_dlldata.thread[VideoFrame_TICKER]->pszThreadName;
								g_dlldata.thread[VideoFrame_TICKER]->pszThreadName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[VideoFrame_TICKER]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  11: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_TICKER)&&(g_dlldata.thread[VideoFrame_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[VideoFrame_TICKER]->m_crit);
							g_dlldata.thread[VideoFrame_TICKER]->nBatchParam  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[VideoFrame_TICKER]->m_crit);
						}
					} break;
				case  12: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_TICKER)&&(g_dlldata.thread[VideoFrame_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[VideoFrame_TICKER]->m_crit);
							g_dlldata.thread[VideoFrame_TICKER]->nShortIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[VideoFrame_TICKER]->m_crit);
						}
					} break;
				case  13: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_TICKER)&&(g_dlldata.thread[VideoFrame_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[VideoFrame_TICKER]->m_crit);
							g_dlldata.thread[VideoFrame_TICKER]->nLongIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[VideoFrame_TICKER]->m_crit);
						}
					} break;
				case  14: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_TICKER)&&(g_dlldata.thread[VideoFrame_TICKER])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[VideoFrame_TICKER]->m_crit);
								pchTemp = g_dlldata.thread[VideoFrame_TICKER]->pszHost;
								g_dlldata.thread[VideoFrame_TICKER]->pszHost  = pch;
								LeaveCriticalSection(&g_dlldata.thread[VideoFrame_TICKER]->m_crit);
								g_settings.m_szTickerHost = szField;
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  15: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_TICKER)&&(g_dlldata.thread[VideoFrame_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[VideoFrame_TICKER]->m_crit);
							g_dlldata.thread[VideoFrame_TICKER]->nPort  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[VideoFrame_TICKER]->m_crit);
							g_settings.m_nTickerPort = atoi(szField);
						}
					} break;
				case  16: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>VideoFrame_TICKER)&&(g_dlldata.thread[VideoFrame_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[VideoFrame_TICKER]->m_crit);
							g_dlldata.thread[VideoFrame_TICKER]->nAuxValue  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[VideoFrame_TICKER]->m_crit);
							g_settings.m_bTickerPlayOnce = ((atoi(szField)>0)?true:false);
						}
					} break;
				case  17: 
					{
						g_settings.m_nDoneCount = atoi(szField);
					} break;

				default: break;
				}
				if(i>-1){szField = szSettings.Mid(i+1);}
				else {szField=_T("");}
				szSettings = szField;
				f++;
			}
			return CLIENT_SUCCESS;
		}
	}
	return CLIENT_ERROR;
	
	
	
}

*/


int CVideoFrameSettings::Settings(bool bRead)
{
	CFileUtil fu;
	if(fu.GetSettings(theApp.m_szSettingsFilename, false)&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
			char* pch;
			pch = fu.GetIniString("Global", "AppName", "VideoFrameInterface");
			if(pch)
			{
				if(m_pszAppName) free(m_pszAppName);
				m_pszAppName = pch;
			}

			pch = fu.GetIniString("Global", "InternalAppName", "VideoFrame");
			if(pch)
			{
				if(m_pszInternalAppName) free(m_pszInternalAppName);
				m_pszInternalAppName = pch;
			}

			pch = (char*)malloc(strlen(m_pszAppName)+strlen(VERSION_STRING)+strlen(", version ")+1);
			if(pch)
			{
				if(m_pszAboutText) free(m_pszAboutText);
				m_pszAboutText = pch;
				sprintf(m_pszAboutText, "%s, version %s", m_pszAppName, VERSION_STRING);
			}

			pch = fu.GetIniString("Global", "SettingsText", "VideoFrame Interface settings...");
			if(pch)
			{
				if(m_pszSettingsText) free(m_pszSettingsText);
				m_pszSettingsText = pch;
			} 

			m_ulDebug = fu.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.

			m_pszDSN = fu.GetIniString("Database", "DSN", "Tabulator", m_pszDSN);
			m_pszUser = fu.GetIniString("Database", "DBUser", "sa", m_pszUser);
			m_pszPW = fu.GetIniString("Database", "DBPassword", "", m_pszPW);
//				m_pszSettings = fu.GetIniString("Database", "SettingsTableName", "Settings", m_pszSettings );  // the Settings table name

//			m_pszAsRunFilename = fu.GetIniString("Ticker", "AsRunFileSpec", "videoframe_asrun|YD||1|", m_pszAsRunFilename);  // the as run filename
//			m_pszAsRunDestination = fu.GetIniString("Ticker", "AsRunToken", "videoframe_asrun", m_pszAsRunDestination);  // the as run destination name


			m_pszMessages=fu.GetIniString("Database", "MessagesTableName", "Messages", m_pszMessages);   // the Messages table name
			m_pszAsRun=fu.GetIniString("Database", "AsRunTableName", "AsRun_Log", m_pszAsRun);  // the As-run table name
			m_pszSettings=fu.GetIniString("Database", "SettingsTableName", "VideoFrame_6_Settings", m_pszSettings);  // the settings table name
			m_pszButtons=fu.GetIniString("Database", "ButtonsTableName", "VideoFrame_6_ButtonInfo", m_pszButtons);  // the Buttons table name now a VIEW
			m_pszManualEvents=fu.GetIniString("Database", "ManualEventsTableName", "VideoFrame_6_ManualEvents", m_pszManualEvents);  // the Manual events table name

	
			m_bLogTransactions = fu.GetIniInt("Database", "LogTransactions", 0)?true:false;  // if true, logs all the comm

			m_bAsRunGTR = fu.GetIniInt("Database", "AsRunGTR", 0)?true:false;
			m_bAsRunSGTX = fu.GetIniInt("Database", "AsRunSGTX", 0)?true:false;
			m_bAsRunDTMF = fu.GetIniInt("Database", "AsRunDTMF", 1)?true:false;



			m_bMillisecondMessaging = fu.GetIniInt("Messager", "MillisecondMessaging", 1)?true:false;			// use millisecond resolution for messages and asrun


//			m_pszHost = fu.GetIniString("VideoFrame", "HostAddress", "0.0.0.0", m_pszHost);  // the VideoFrame host IP


			int nPort = fu.GetIniInt("VideoFrame", "HostPort", 5050);  // the VideoFrame host port
			if(m_nPort != nPort)
			{
				g_data.StopServer();
				m_nPort = nPort;
				g_data.StartServer();
			}



			m_nNumButtons = fu.GetIniInt("VideoFrame", "NumButtons",  32);
			if(m_nNumButtons<=0) m_nNumButtons = 1;
			if(m_nNumButtons>VF_MAX_BUTTONS) m_nNumButtons = VF_MAX_BUTTONS;
			m_nDTMFport = fu.GetIniInt("VideoFrame", "DTMFport",  1);
			if(m_nDTMFport<=0) m_nDTMFport = 1;

			m_pszUnitNumber=fu.GetIniString("VideoFrame", "UnitNumber", "0", m_pszUnitNumber);

	 
			m_nConnectionTimeout = fu.GetIniInt("VideoFrame", "ConnectionTimeout", 60); // one minute,  0 or less turns it off);


			m_nMinKeypressDelay = fu.GetIniInt("VideoFrame", "MinKeypressDelayMS", 100);

			m_pszTabulatorHost = fu.GetIniString("Tabulator", "HostAddress", "127.0.0.1", m_pszTabulatorHost);  // the Tabulator host IP or name
			m_nTabulatorPort = fu.GetIniInt("Tabulator", "CommandServerPort", TABULATOR_PORT_CMD);  // the Tabulator host port

			m_pszTabulatorModule = fu.GetIniString("Tabulator", "ModuleName", "Tabulator", m_pszTabulatorModule);  // module name, "Tabulator"
			m_pszTabulatorPlugInModule = fu.GetIniString("Tabulator", "PlugInModuleName", "StreamData", m_pszTabulatorPlugInModule);  // the Tabulator module name (AutomationData)
//			m_pszTabulatorCommandReplaceManual = fu.GetIniString("Tabulator", "ManualToken", "Replace_Manual", m_pszTabulatorCommandReplaceManual);  // the Replace command with Manual events
//			m_pszTabulatorCommandReplaceCurrent = fu.GetIniString("Tabulator", "InProgressToken", "Replace_Current", m_pszTabulatorCommandReplaceCurrent);  // the Replace command with current event info
//			m_pszTabulatorCommandEnterPass = fu.GetIniString("Tabulator", "EnterPassToken", "Enter_Pass", m_pszTabulatorCommandEnterPass);  // entering the PASS state
//			m_pszTabulatorCommandEnterBlock = fu.GetIniString("Tabulator", "EnterBlockToken", "Enter_Block", m_pszTabulatorCommandEnterBlock);  // entering the BLOCK state
			m_pszTabulatorCommandQueryReady = fu.GetIniString("Tabulator", "QueryReadyToken", "Query_Ready", m_pszTabulatorCommandQueryReady);   // query the readiness state of the automation

			m_pszTabulatorCommandBreakEnd = fu.GetIniString("Tabulator", "BreakEndToken", "Break_End", m_pszTabulatorCommandBreakEnd);   // break end command
			m_pszTabulatorCommandNationalBreak = fu.GetIniString("Tabulator", "NationalBreakToken", "National_Break", m_pszTabulatorCommandNationalBreak);  // the National Break command, send to StreamData dll 

			m_bNationalBreakPrerollIsDelay = fu.GetIniInt("Tabulator", "NationalBreakPrerollIsDelay",  1)?true:false;  // if true, waits this number of MS, and sends the command with preroll zero; if false, just sends the command with the preroll

			
			m_nStartupDelay = fu.GetIniInt("Startup", "StartupDelayMS", 2000); // only required if there is a startup action.
			m_nStartupButton = fu.GetIniInt("Startup", "StartupButton",  -1); // only required if there is a startup button press to occur.

			m_bInitRequiresConnection = fu.GetIniInt("Startup", "InitRequiresConn",  0)?true:false; 	// initialization condition
			m_bInitRequiresAuto = fu.GetIniInt("Startup", "InitRequiresAuto",  0)?true:false; 	// initialization condition
			m_bInitRequiresPass = fu.GetIniInt("Startup", "InitRequiresPass",  0)?true:false; 	// initialization condition
			m_bInitRequiresBlock = fu.GetIniInt("Startup", "InitRequiresBlock",  0)?true:false; 	// initialization condition



/*
			m_pszModule=fu.GetIniString("Ticker", "GraphicsModule", "Libretto", m_pszModule);  // the module name (Libretto)
			m_pszQueue=fu.GetIniString("Ticker", "GraphicsQueue", "Command_Queue", m_pszQueue); // the module's queue table name (Queue of Libretto.dbo.Queue)


			char setbuf[MAX_PATH];
			if(m_pszInEvent) strcpy(setbuf, m_pszInEvent); else strcpy(setbuf, "");
			m_pszInEvent=fu.GetIniString("Ticker", "InEventID", "1000", m_pszInEvent);  // the event identifier of the transition in (Server_Ticker_In) command
								EnterCriticalSection(&g_data.m_critEventsSettings);
			if (strcmp(setbuf, m_pszInEvent)) 
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bInEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}
			if(m_pszOutEvent) strcpy(setbuf, m_pszOutEvent); else strcpy(setbuf, "");
			m_pszOutEvent=fu.GetIniString("Ticker", "OutEventID", "1001", m_pszOutEvent);  // the event identifier of the transition out (Server_Ticker_Out) command
								EnterCriticalSection(&g_data.m_critEventsSettings);
			if (strcmp(setbuf, m_pszOutEvent)) 
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bOutEventChanged = true;  // the event identifier of the transition out (Server_Ticker_Out) command has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}
			if(m_pszTextEvent) strcpy(setbuf, m_pszTextEvent); else strcpy(setbuf, "");
			m_pszTextEvent=fu.GetIniString("Ticker", "TextEventID", "1002", m_pszTextEvent);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszTextEvent))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bTextEventChanged = true;  // the event identifier of the text message event has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}

*/			

//			int GetIniInt(char* pszSection, char* pszEntry, int nDefaultValue);
		}
		else // write
		{
			if(m_pszAppName) fu.SetIniString("Global", "AppName", m_pszAppName);
			if(m_pszAppName) fu.SetIniString("Global", "InternalAppName", m_pszInternalAppName);
//			if(m_pszAboutText) fu.SetIniString("Global", "AboutText", m_pszAboutText);  // dont save
			if(m_pszSettingsText) fu.SetIniString("Global", "SettingsText", m_pszSettingsText);
//			int SetIniInt(char* pszSection, char* pszEntry, int nValue, char* pszComment=NULL);

			fu.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.


//				fu.SetIniInt("DefaultValues", "MessageDwellTime", m_nDefaultDwellTime);

			fu.SetIniString("Database", "DSN", m_pszDSN);
			fu.SetIniString("Database", "DBUser", m_pszUser);
			fu.SetIniString("Database", "DBPassword", m_pszPW);


			fu.SetIniString("Database", "AsRunTableName", m_pszAsRun);  // the As-run table name
			fu.SetIniString("Database", "SettingsTableName", m_pszSettings);  // the settings table name
			fu.SetIniString("Database", "MessagesTableName", m_pszMessages);   // the Messages table name


			fu.SetIniString("Database", "ButtonsTableName", m_pszButtons);  // the Buttons table name
			fu.SetIniString("Database", "ManualEventsTableName", m_pszManualEvents);  // the Manual events table name

			fu.SetIniInt("Database", "AsRunGTR", m_bAsRunGTR?1:0); 
			fu.SetIniInt("Database", "AsRunSGTX", m_bAsRunSGTX?1:0); 
			fu.SetIniInt("Database", "AsRunDTMF", m_bAsRunDTMF?1:0); 

			fu.SetIniInt("Database", "LogTransactions", m_bLogTransactions?1:0);  // if true, logs all the comm
			fu.SetIniInt("Messager", "MillisecondMessaging", m_bMillisecondMessaging?1:0);		// use millisecond resolution for messages and asrun

//			fu.SetIniString("VideoFrame", "HostAddress", m_pszHost);  // the VideoFrame host IP
			fu.SetIniInt("VideoFrame", "HostPort", m_nPort);  // the VideoFrame host port
			fu.SetIniInt("VideoFrame", "NumButtons", m_nNumButtons);
			fu.SetIniInt("VideoFrame", "DTMFport",  m_nDTMFport);
			fu.SetIniString("VideoFrame", "UnitNumber", m_pszUnitNumber);

			fu.SetIniInt("VideoFrame", "ConnectionTimeout",  m_nConnectionTimeout);


			fu.SetIniInt("VideoFrame", "MinKeypressDelayMS", m_nMinKeypressDelay);

			fu.SetIniString("Tabulator", "ModuleName", m_pszTabulatorModule);  // module name, "Tabulator"
			fu.SetIniString("Tabulator", "HostAddress", m_pszTabulatorHost);  // the Tabulator host IP or name
			fu.SetIniInt("Tabulator", "CommandServerPort", m_nTabulatorPort);  // the Tabulator host port
			fu.SetIniString("Tabulator", "PlugInModuleName",  m_pszTabulatorPlugInModule);  // the Tabulator module name (AutomationData)


/*
removing write on these, not applicable
			fu.SetIniString("Tabulator", "ManualToken", m_pszTabulatorCommandReplaceManual);  // the Replace command with Manual events
			fu.SetIniString("Tabulator", "InProgressToken", m_pszTabulatorCommandReplaceCurrent);  // the Replace command with current event info
			fu.SetIniString("Tabulator", "EnterPassToken", m_pszTabulatorCommandEnterPass);  // entering the PASS state
			fu.SetIniString("Tabulator", "EnterBlockToken", m_pszTabulatorCommandEnterBlock);  // entering the BLOCK state
*/
			fu.SetIniString("Tabulator", "QueryReadyToken", m_pszTabulatorCommandQueryReady);   // query the readiness state of the automation

			fu.SetIniString("Tabulator", "BreakEndToken", m_pszTabulatorCommandBreakEnd);   // break end command
			fu.SetIniString("Tabulator", "NationalBreakToken", m_pszTabulatorCommandNationalBreak);  // the National Break command, send to StreamData dll 

			fu.SetIniInt("Tabulator", "NationalBreakPrerollIsDelay",  m_bNationalBreakPrerollIsDelay?1:0);  // if true, waits this number of MS, and sends the command with preroll zero; if false, just sends the command with the preroll

			fu.SetIniInt("Startup", "StartupDelayMS", m_nStartupDelay); // only required if there is a startup action.
			fu.SetIniInt("Startup", "StartupButton",  m_nStartupButton); // only required if there is a startup button press to occur.

			fu.SetIniInt("Startup", "InitRequiresConn",  m_bInitRequiresConnection?1:0); 	// initialization condition
/*
removing write on these, not applicable
			fu.SetIniInt("Startup", "InitRequiresAuto",  m_bInitRequiresAuto?1:0); 	// initialization condition
			fu.SetIniInt("Startup", "InitRequiresPass",  m_bInitRequiresPass?1:0); 	// initialization condition
			fu.SetIniInt("Startup", "InitRequiresBlock",  m_bInitRequiresBlock?1:0); 	// initialization condition
*/


/*
			fu.SetIniString("Ticker", "GraphicsModule",  m_pszModule);  // the module name (Libretto)
			fu.SetIniString("Ticker", "GraphicsQueue", m_pszQueue); // the module's queue table name (Queue of Libretto.dbo.Queue)

/*
			fu.SetIniString("Ticker", "InEventID", m_pszInEvent);  // the event identifier of the transition in (Server_Ticker_In) command
			fu.SetIniString("Ticker", "OutEventID", m_pszOutEvent);  // the event identifier of the transition out (Server_Ticker_Out) command
			fu.SetIniString("Ticker", "TextEventID", m_pszTextEvent);  // the event identifier of the text message event
*/

			if(!(fu.SetSettings(theApp.m_szSettingsFilename, false)&FILEUTIL_MALLOC_OK)) return CLIENT_ERROR;
		}

		return CLIENT_SUCCESS;
	}
//	else AfxMessageBox("failed!");
	return CLIENT_ERROR;
}




int CVideoFrameSettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	if((Settings(true)==TABULATOR_SUCCESS)&&(m_pszSettings)&&(strlen(m_pszSettings)>0))
	{

		CDBUtil  db;
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];
		strcpy(errorstring, "");

		CDBconn* pdbConn = db.CreateNewConnection(m_pszDSN, m_pszUser, m_pszPW);
		if(pdbConn)
		{
			if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "VideoFrame_Settings:database_connect", errorstring);  //(Dispatch message)
				if(pszInfo)
				{
					_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s", errorstring);
				}
			}
			else
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
				CRecordset* prs = db.Retrieve(pdbConn, szSQL, pszInfo);
				if(prs)
				{
					int nReturn = TABULATOR_ERROR;
					int nIndex = 0;
					while ((!prs->IsEOF()))
					{
						CString szCategory="";
						CString szParameter="";
						CString szValue="";
						CString szTemp="";
						int min, max;
						bool bmin = false, bmax = false;
						try
						{
							prs->GetFieldValue("category", szCategory);  //HARDCODE
							prs->GetFieldValue("parameter", szParameter);  //HARDCODE
							prs->GetFieldValue("value", szValue);  //HARDCODE
							prs->GetFieldValue("min_value", szTemp);  //HARDCODE
							if(szTemp.GetLength())
							{
								min = atoi(szTemp);
								bmin = true;
							}
							prs->GetFieldValue("max_value", szTemp);  //HARDCODE
							if(szTemp.GetLength())
							{
								max = atoi(szTemp);
								bmax = true;
							}
						}
						catch( ... )
						{
						}

						int nLength = szValue.GetLength();
/*
						if(szCategory.CompareNoCase("Main")==0)
						{
							if(szParameter.CompareNoCase("Name")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszName) free(m_pszName);
										m_pszName = pch;
									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("License")==0)
						{
							if(szParameter.CompareNoCase("Key")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszLicense) free(m_pszLicense);
										m_pszLicense = pch;

										// recompile license key params
										if(g_ptabulator->m_data.m_key.m_pszLicenseString) free(g_ptabulator->m_data.m_key.m_pszLicenseString);
										g_ptabulator->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
										if(g_ptabulator->m_data.m_key.m_pszLicenseString)
										sprintf(g_ptabulator->m_data.m_key.m_pszLicenseString, "%s", szValue);

										g_ptabulator->m_data.m_key.InterpretKey();

										char errorstring[MAX_MESSAGE_LENGTH];
										if(g_ptabulator->m_data.m_key.m_bValid)
										{
											unsigned long i=0;
											while(i<g_ptabulator->m_data.m_key.m_ulNumParams)
											{
												if((g_ptabulator->m_data.m_key.m_ppszParams)
													&&(g_ptabulator->m_data.m_key.m_ppszValues)
													&&(g_ptabulator->m_data.m_key.m_ppszParams[i])
													&&(g_ptabulator->m_data.m_key.m_ppszValues[i]))
												{
													if(stricmp(g_ptabulator->m_data.m_key.m_ppszParams[i], "max")==0)
													{
		//												g_ptabulator->m_data.m_nMaxLicensedDevices = atoi(g_ptabulator->m_data.m_key.m_ppszValues[i]);
													}
												}
												i++;
											}
										
											if(
													(
														(!g_ptabulator->m_data.m_key.m_bExpires)
													||((g_ptabulator->m_data.m_key.m_bExpires)&&(!g_ptabulator->m_data.m_key.m_bExpired))
													||((g_ptabulator->m_data.m_key.m_bExpires)&&(g_ptabulator->m_data.m_key.m_bExpireForgiveness)&&(g_ptabulator->m_data.m_key.m_ulExpiryDate+g_ptabulator->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
													)
												&&(
														(!g_ptabulator->m_data.m_key.m_bMachineSpecific)
													||((g_ptabulator->m_data.m_key.m_bMachineSpecific)&&(g_ptabulator->m_data.m_key.m_bValidMAC))
													)
												)
											{
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
												g_ptabulator->m_data.SetStatusText(errorstring, TABULATOR_STATUS_OK);
											}
											else
											{
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
												g_ptabulator->m_data.SetStatusText(errorstring, TABULATOR_STATUS_ERROR);
											}
										}
										else
										{
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
											g_ptabulator->m_data.SetStatusText(errorstring, TABULATOR_STATUS_ERROR);
										}

									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("CommandServer")==0)
						{
							if(szParameter.CompareNoCase("ListenPort")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
									{
										m_usCommandPort = nLength;
									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("StatusServer")==0)
						{
							if(szParameter.CompareNoCase("ListenPort")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
									{
										m_usStatusPort = nLength;
									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("Messager")==0)
						{
							if(szParameter.CompareNoCase("UseEmail")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bUseEmail = true;
									else m_bUseEmail = false;
								}
							}
							else
							if(szParameter.CompareNoCase("UseNet")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bUseNetwork = true;
									else m_bUseNetwork = false;
								}
							}
							else
							if(szParameter.CompareNoCase("UseLog")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bUseLog = true;
									else m_bUseLog = false;
								}
							}
							else
							if(szParameter.CompareNoCase("ReportSuccessfulOperation")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bReportSuccessfulOperation = true;
									else m_bReportSuccessfulOperation = false;
								}
							}
						}
						else
		/*
						if(szCategory.CompareNoCase("FileHandling")==0)
						{
							// we are not going to allow DSN params to change via the DB.
							if(szParameter.CompareNoCase("DeleteSourceFileOnTransfer")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bDeleteSourceFileOnTransfer = true;
									else m_bDeleteSourceFileOnTransfer = false;
								}
							}
						}
						else
		*/

/*						if(szCategory.CompareNoCase("Ticker")==0)
						{
							if(szParameter.CompareNoCase("GraphicsHost")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszDestinationHost)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszDestinationHost) free(m_pszDestinationHost);
										m_pszDestinationHost = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("GraphicsModule")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszModule)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszModule) free(m_pszModule);
										m_pszModule = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("GraphicsQueue")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszQueue)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszQueue) free(m_pszQueue);
										m_pszQueue = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("InEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszInEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bInEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszInEvent) free(m_pszInEvent);
										m_pszInEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("OutEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszOutEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bOutEventChanged = true;  // the event identifier of the transition out (Server_Ticker_Out) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszOutEvent) free(m_pszOutEvent);
										m_pszOutEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("TextEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszTextEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bTextEventChanged = true;  // the event identifier of the text message event has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszTextEvent) free(m_pszTextEvent);
										m_pszTextEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("PlayItemsOnce")==0)
							{
								if(nLength>0)
								{
									m_bTickerPlayOnce = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("DoneCount")==0)
							{
								if(nLength>0)
								{
									m_nDoneCount = atoi(szValue);
									if(m_nDoneCount<0) m_nDoneCount=0;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExpired")==0)
							{
								if(nLength>0)
								{
									m_bAutoPurgeExpired = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExceeded")==0)
							{
								if(nLength>0)
								{
									m_bAutoPurgeExceeded = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExpiredAfterMins")==0)
							{
								if(nLength>0)
								{
									m_nAutoPurgeExpiredAfterMins = atoi(szValue);
									if(m_nAutoPurgeExpiredAfterMins<0) m_nAutoPurgeExpiredAfterMins=0;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExceededAfterMins")==0)
							{
								if(nLength>0)
								{
									m_nAutoPurgeExceededAfterMins = atoi(szValue);
									if(m_nAutoPurgeExceededAfterMins<0) m_nAutoPurgeExceededAfterMins=0;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoStart")==0)
							{
								if(nLength>0)
								{
									m_bAutostartTicker = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("UseLocalTime")==0)
							{
								if(nLength>0)
								{
									m_bUseLocalTime = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("dwell_time")==0)
							{
								if(nLength>0)
								{
									m_nDefaultDwellTime = atoi(szValue);
									if(m_nDefaultDwellTime<0) m_nDefaultDwellTime=10000; //default
								}
							}
						}
						else
						if(szCategory.CompareNoCase("DataDownload")==0)
						{
							if(szParameter.CompareNoCase("AutoStart")==0)
							{
								if(nLength>0)
								{
									m_bAutostartData = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
						}
						else
						if(szCategory.CompareNoCase("default_value")==0)
						{
							if(szParameter.CompareNoCase("dwell_time")==0)
							{
								if(nLength>0)
								{
									m_nDefaultDwellTime = atoi(szValue);
									if(m_nDefaultDwellTime<0) m_nDefaultDwellTime=10000; //default
								}
							}
						}
						else

*/
						if(szCategory.CompareNoCase("Database")==0)
						{
							// we are not going to allow DSN params to change via the DB.
							if(szParameter.CompareNoCase("SettingsTableName")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSettings)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSettings) free(m_pszSettings);
										m_pszSettings = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunTableName")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszAsRun)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszAsRun) free(m_pszAsRun);
										m_pszAsRun = pch;
									}
								}
							}
		
/*
							else
							if(szParameter.CompareNoCase("MessagesTableName")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszMessages)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszMessages) free(m_pszMessages);
										m_pszMessages = pch;
									}
								}
							}
/*
							else
							if(szParameter.CompareNoCase("QueueTableName")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszQueue) free(m_pszQueue);
										m_pszQueue = pch;
									}
								}
							}
		/*
							else
							if(szParameter.CompareNoCase("ConnectionsTableName")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszConnections) free(m_pszConnections);
										m_pszConnections = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("LiveEventsTableName")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszLiveEvents) free(m_pszLiveEvents);
										m_pszLiveEvents = pch;
									}
								}
							}
		*/
						}
						else
						if(szCategory.CompareNoCase("VideoFrame")==0)
						{
							// we are not going to allow DSN params to change via the DB.
/*
							if(szParameter.CompareNoCase("HostAddress")==0)
							{

								if((nLength>0)&&(szValue.Compare(m_pszHost)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszHost) free(m_pszHost);
										m_pszHost = pch;
									}
								}
							}
							else
*/
							if(szParameter.CompareNoCase("HostPort")==0)
							{
								if(nLength>0)
								{

									int nPort = atoi(szValue);
									if(nPort<0) nPort=5050; //default
									if(m_nPort != nPort)
									{
										g_data.StopServer();
										m_nPort = nPort;
										g_data.StartServer();
									}
								}
							}
							else
							if(szParameter.CompareNoCase("NumButtons")==0)
							{
								if(nLength>0)
								{
									m_nNumButtons = atoi(szValue);
									if(m_nNumButtons<=0) m_nNumButtons = 1;
									if(m_nNumButtons>VF_MAX_BUTTONS) m_nNumButtons = VF_MAX_BUTTONS;
								}
							}
							else
							if(szParameter.CompareNoCase("DTMFport")==0)
							{
								if(nLength>0)
								{
									m_nDTMFport = atoi(szValue);
									if(m_nDTMFport<=0) m_nDTMFport = 1;
								}
							}
							else
							if(szParameter.CompareNoCase("UnitNumber")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszUnitNumber)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszUnitNumber) free(m_pszUnitNumber);
										m_pszUnitNumber = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("ConnectionTimeout")==0)
							{
								if(nLength>0)
								{
									m_nConnectionTimeout = atoi(szValue);
									if(m_nConnectionTimeout<=0) m_nConnectionTimeout = 0;
								}
							}
						}


						nIndex++;
						prs->MoveNext();
					}
					prs->Close();

					if(pszInfo)
					{
						_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
					}
					delete prs;
					prs = NULL;

					Settings(false); //write
					return TABULATOR_SUCCESS;
				}
			}
		}
		else
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: Connection pointer was NULL.");
		}

	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. "

				);
		}
	}
	return TABULATOR_ERROR;
}


