// VideoFrameData.h: interface for the CVideoFrameData and CVideoFrameMessage classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VIDEOFRAMEDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_)
#define AFX_VIDEOFRAMEDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../../../Common/LAN/NetUtil.h"
#include "../../../Cortex Objects/Tabulator/TabulatorDefines.h"
#include "../../../Cortex Objects/Tabulator/TabulatorData.h"
#include "../../../Common/MFC/ODBC/DBUtil.h"


#define SENDMSG_SENDER_MAXLEN 64
#define SENDMSG_MESSAGE_MAXLEN 512

#define VF_MAX_BUTTONS 64

/*
typedef struct MessageLogging_t
{
	char* pchReadableHexSnd;
	unsigned long ulReadableHexSnd;
	char* pchReadableSnd;
	unsigned long ulReadableSnd;
	char* pchReadableHexRcv;
	unsigned long ulReadableHexRcv;
	char* pchReadableRcv;
	unsigned long ulReadableRcv;

} MessageLogging_t;
*/



class CVideoFrameButton
{
public:
	CVideoFrameButton();
	virtual ~CVideoFrameButton();

	int m_nItemID;

	char m_pchButtonOnLabel[256];
	char m_pchButtonOffLabel[256];

	char m_pchTallyEventLabel[32];
	char m_pchTallyEventValue[32];

	char m_pchClickEventLabel[32];
	int  m_nLastClickValue; // boolean (toggle)

	unsigned long m_ulStatus; // current tally.
	unsigned long m_ulFlags;

	// manual events association
	char m_pchManualEventName[32];
	int  m_nManualEventType;
	char m_pchManualEventProgID[64];
	int  m_nManualEventDuration;

	int UpdateStatus();
};



class CVideoFrameData  
{
public:
	CVideoFrameData();
	virtual ~CVideoFrameData();

	// remove pipes
	CString Encode(CString szText);
	// put back pipes
	CString Decode(CString szText);

	int InitDLLData(DLLthread_t* pData);

	CString XMLTextNodeValue(IXMLDOMNodePtr pNode);

	int SendMsg(int nType, char* pszSender, char* pszMessage, ...);
//	int SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...);
	int SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...);


	CString m_szDTMF;
	CRITICAL_SECTION m_critDTMF;


	BOOL m_bInVideoFrameCommand;
	SOCKET m_s;
	sockaddr_in m_saiGTPhost;

	BOOL m_bInCommand;
	SOCKET m_socketTabulator;


	BOOL m_bButtonsInitialized;
	
	BOOL m_bInitConn;		// initialization condition
	BOOL m_bInitAuto;		// initialization condition
	BOOL m_bInitPass;		// initialization condition
	BOOL m_bInitBlock;	// initialization condition
	BOOL m_bInitAutoClick;		// initialization condition
	BOOL m_bInitPassClick;		// initialization condition
	BOOL m_bInitBlockClick;	// initialization condition




//	CRITICAL_SECTION m_critClientSocket;

	CNetUtil m_net;
	CDBUtil m_dbu;
	CBufferUtil m_bu;

//	CRITICAL_SECTION m_critMsg;
	CRITICAL_SECTION m_critSQL;

//	CTabulatorEventArray* m_pEvents;
//	CTabulatorEventArray* m_pInEvents;
//	CTabulatorEventArray* m_pOutEvents;
//	CTabulatorEventArray* m_pTextEvents;
//	CRITICAL_SECTION m_critEventsSettings;


	CDBUtil*  m_pdb;
	CDBconn*  m_pdbConn;

//	CRITICAL_SECTION m_critTransact;


	CVideoFrameButton m_Buttons[VF_MAX_BUTTONS]; // hard code the max numbah
//	int m_nNumButtons;
	CRITICAL_SECTION m_critButtons;


	int SetButtonsOff(char* pszInfo=NULL);
	int GetButtons(char* pszInfo=NULL);
	int GetManualEvents(char* pszInfo=NULL);
	int ButtonEventAction(int nButton);


//	int SendVideoFrameCommand(int nType, int nIndex, int nValue); // type like button press or GPO, nValue will be 0 or 1, basically on or off.
	int SendTabulatorCommand(unsigned char** ppucDataBuffer, char* pszAsRunEventID, bool bSuppressAsRun=false);

	int StartServer();
	int StopServer();
//	int Connect();
//	int Disconnect();

	int GPIMessage(char** ppchMsg, int nSequence);


};


#endif // !defined AFX_VIDEOFRAMEDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_
