// LibrettoParseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LibrettoParse.h"
#include "LibrettoParseDlg.h"
#include "..\..\..\..\Common\TXT\BufferUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLibrettoParseDlg dialog

CLibrettoParseDlg::CLibrettoParseDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLibrettoParseDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLibrettoParseDlg)
	m_szItem = _T("");
	m_bFilterHex = TRUE;
	m_szSubItems = _T("");
	m_szHexData = _T("");
	m_nOutputType = 1;
	m_bSuppressRepeats = FALSE;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	
	m_pchBuffer=NULL;
}

void CLibrettoParseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLibrettoParseDlg)
	DDX_Control(pDX, IDC_LIST1, m_lcLog);
	DDX_Text(pDX, IDC_EDIT1, m_szItem);
	DDX_Check(pDX, IDC_CHECK1, m_bFilterHex);
	DDX_Text(pDX, IDC_EDIT2, m_szSubItems);
	DDX_Text(pDX, IDC_EDIT3, m_szHexData);
	DDX_Radio(pDX, IDC_RADIO_TXT, m_nOutputType);
	DDX_Check(pDX, IDC_CHECK_SUPPRESS_RPT, m_bSuppressRepeats);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CLibrettoParseDlg, CDialog)
	//{{AFX_MSG_MAP(CLibrettoParseDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_SELECT, OnButtonSelect)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST1, OnItemchangedList1)
	ON_BN_CLICKED(IDC_BUTTON_OUTFILE, OnButtonOutfile)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLibrettoParseDlg message handlers

BOOL CLibrettoParseDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	CRect rc; 
	m_lcLog.GetWindowRect(&rc);
	m_lcLog.InsertColumn(0, "Log data", LVCFMT_LEFT, rc.Width()-16, 0 );

	ListView_SetExtendedListViewStyle(m_lcLog.m_hWnd, LVS_EX_FULLROWSELECT);

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLibrettoParseDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLibrettoParseDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLibrettoParseDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CLibrettoParseDlg::OnOK() 
{
	// TODO: Add extra validation here
	
//	CDialog::OnOK();
}

void CLibrettoParseDlg::OnButtonSelect() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);


	CFileDialog dlg(TRUE, NULL, NULL, OFN_FILEMUSTEXIST, "Libretto Log Files (*.log)|*.log|All Files (*.*)|*.*||");
//	 "Select Libretto log file"
	if( dlg.DoModal ()==IDOK )
	{
		CString pathName = dlg.GetPathName();
		CString fileName = dlg.GetFileName();

		CWaitCursor cw;
		//		 AfxMessageBox(pathName);

		FILE* fp;
		fp = fopen(pathName, "rb");
		if(fp)
		{
			fseek(fp, 0, SEEK_END);
			unsigned long ulen = ftell(fp);

			char* c = NULL;
			c = (char*)malloc(ulen+1); 
			if(c)
			{
				fseek(fp, 0, SEEK_SET);
				fread(c, sizeof(char), ulen, fp);
				*(c+ulen) = 0;

				// OK so now....
				// divide stuff up into records.

				m_lcLog.DeleteAllItems();

				CSafeBufferUtil sbu;

				char* pch = sbu.Token(c, ulen, "\r\n", MODE_MULTIDELIM);

				while(pch)
				{

					if(m_bFilterHex)
					{
						CString szItem = pch;
						if(szItem.Mid(strlen("2011-Dec-27 09:16:30.113"), strlen(" [H ] sending")).CompareNoCase(" [H ] sending")==0)
						{
							if(
								  (szItem.Mid(strlen("2011-Dec-27 09:16:30.113 [H ] sending"), 4).CompareNoCase(":  -") )
								&&(szItem.Mid(strlen("2011-Dec-27 09:16:30.113 [H ] sending"), 4).CompareNoCase(": ��") )
								)
							{
								m_lcLog.InsertItem(m_lcLog.GetItemCount(), pch);
							}
						}
					}
					else
					{
						m_lcLog.InsertItem(m_lcLog.GetItemCount(), pch);
					}

					pch = sbu.Token(NULL, NULL, "\r\n", MODE_MULTIDELIM);
				}

				if(m_pchBuffer) free(m_pchBuffer);

				m_pchBuffer = c;
				m_ulLen = ulen;
				m_szFilename = fileName;
			}
			else
			{
				AfxMessageBox("Error allocating memory for file analysis");
			}
			fclose(fp);

		}
		else
		{
			AfxMessageBox("Error opening file");
		}
	}

}

void CLibrettoParseDlg::OnItemchangedList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	int nCount = m_lcLog.GetItemCount();
	int nItem = m_lcLog.GetNextItem( -1,  LVNI_SELECTED );

	CString szItem;
	if((nItem>=0)&&(nItem<nCount))
	{
		szItem = m_lcLog.GetItemText(nItem, 0);

		//Parse that biznich;

		if(
				(szItem.Mid(strlen("2011-Dec-27 09:16:30.113"), strlen(" [H ] sending")).CompareNoCase(" [H ] sending")==0)
			)

//2011-Dec-27 10:15:02.191 [H ] sending: [00][03][00][0d][ff][ff][ff][ff][00][00][45][00][00] - keep_alive - 
//2011-Dec-27 10:15:14.066 [H ] sending 100 bytes (r=0): [ff][ff][00][64][00][00][03][00][00][00][02][0a][0f][0e][02][02][81][50][00][47][44][41][54][47][01][32][30][31][31][2d][31][32][2d][32][37][0a][0f][0e][02][00][00][00][00][54][56][2b][3a][55][4e][4b][4e][4f][57][4e][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][81][50][00][05][44][41][54][47][04] - Libretto:command_queue - 
//2011-Dec-27 10:15:14.066 [H ] sending: �� - Libretto:debug - 

		{
			// it's a hex send


			m_szHexData = "";

			m_szSubItems = "";

			int nStartIndex = strlen("2011-Dec-27 09:16:30.113 [H ] sending");
			int nType = 0;

			if(szItem.Mid(nStartIndex,3).CompareNoCase(": [")==0)
			{
				nType = 1; // hex send, single
				nStartIndex += 2;
			}
			else
			if(szItem.Mid(nStartIndex).Find(" bytes (r=")>0)
			{
				nType = 2; // hex send, double
				nStartIndex += szItem.Mid(nStartIndex).Find(": [");
				if(nStartIndex>=0)
				{
					nStartIndex += 2;

//CString foo; foo.Format("%d %s", nStartIndex, szItem.Mid(nStartIndex));
//AfxMessageBox(foo);
				}
				else
					nType=0;
			}

			if(nType == 0)
			{
				m_szItem.Format("%s\r\n\r\n%s", szItem.Left(strlen("2011-Dec-27 09:16:30.113")), szItem.Mid(strlen("2011-Dec-27 09:16:30.113 ")) );
			}
			else
			{

				int nEndIndex = szItem.ReverseFind(']')+1;

				if(nEndIndex>0)
				{
					CString szHexBuffer = szItem.Mid(nStartIndex, nEndIndex-nStartIndex);
					
					m_szHexData = "";

	//				m_szItem = szHexBuffer;

					CBufferUtil bu;
					unsigned long ulBufLen = szHexBuffer.GetLength();


					unsigned long x=0;
					bool bAlt = false;
					while(x<ulBufLen)
					{
						CString szX = szHexBuffer.Mid(x, 16);
						m_szHexData += szX;
						if(bAlt)
						{
							m_szHexData += "\r\n";
						}
						else
						{
							m_szHexData += "\t";
						}

						bAlt = !bAlt;

						x+= 16;

					}



					char* pchBuffer = szHexBuffer.GetBuffer(0);
					char* pch = bu.DecodeReadableHex(pchBuffer, &ulBufLen);

					if(pch)
					{
						m_szItem.Format("%d bytes, %d %d", ulBufLen, *pch, *(pch+1));
					// here we have a binary buffer.

						if((*pch == (char)0xff)&&(*(pch+1) == (char)0xff))
						{
							// multi message
							m_szItem.Format("SCTE104 multi message, %d bytes\r\n\r\n", ulBufLen);

//							AfxMessageBox(szHexBuffer);
							// lets deal:

/*
multiple_operation_message() 
{
	Reserved 2 uimsbf
	messageSize 2 uimsbf
	protocol_version 1 uimsbf
	AS_index 1 uimsbf
	message_number 1 uimsbf
	DPI_PID_index 2 uimsbf
	SCTE35_protocol_version 1 uimsbf
	timestamp() * Varies
	num_ops 1 uimsbf
	for (i=0; i < num_ops; i++ ) 
	{
		opID 2
		data_length 2
		data() * Varies
	}
}
*/


							CString szPart;

							if(ulBufLen>=10)
							{
								szPart.Format("Reserved = 0x%02x%02x\r\n", (unsigned char)*pch,  (unsigned char)*(pch+1));
								m_szItem += szPart;

								int nSize = ( ((unsigned char) (*(pch+2))) <<8) + ((unsigned char)(*(pch+3)));

								szPart.Format("messageSize = 0x%02x%02x (%d bytes)\r\n", (unsigned char)*(pch+2),  (unsigned char)*(pch+3), nSize);
								m_szItem += szPart;
								szPart.Format("protocol_version = 0x%02x\r\n", (unsigned char)*(pch+4));
								m_szItem += szPart;
								szPart.Format("AS_index = 0x%02x\r\n", (unsigned char)*(pch+5));
								m_szItem += szPart;
//								szPart.Format("message_number = 0x%02x (%d decimal)\r\n", (unsigned char)*(pch+10), *(pch+10));
								szPart.Format("message_number = 0x%02x\r\n", (unsigned char)*(pch+6));
								m_szItem += szPart;
								szPart.Format("DPI_PID_index = 0x%02x%02x\r\n", (unsigned char)*(pch+7),  (unsigned char)*(pch+8));
								m_szItem += szPart;

								szPart.Format("SCTE35_protocol_version = 0x%02x\r\n", (unsigned char)*(pch+9));
								m_szItem += szPart;

							}
							bool bErr = false;
							int nOffset = 10;
							if(ulBufLen>10)
							{
								switch(*(pch+10))
								{
								case 0:  // time type is not required.
									{
										szPart.Format("time_type = 0x%02x (no time required)\r\n", (unsigned char)*(pch+nOffset));
										m_szItem += szPart;
									} break;
								case 1:  // time type is UTC time
									{
										szPart.Format("time_type = 0x%02x (UTC time)\r\n", (unsigned char)*(pch+nOffset));
										m_szItem += szPart;

										int nTime = 
											((*(pch+nOffset+1))<<24) + 
											((*(pch+nOffset+2))<<16) +
											((*(pch+nOffset+3))<<8) +
											((*(pch+nOffset+4)));

										szPart.Format("UTC_seconds = %d\r\n", nTime);
										m_szItem += szPart;

										nTime = 
											((*(pch+nOffset+5))<<8) +
											((*(pch+nOffset+6)));

										szPart.Format("UTC_microseconds = %d\r\n", nTime);
										m_szItem += szPart;

										nOffset += 7;

									} break;
								case 2:  // hmsf
									{
										szPart.Format("time_type = 0x%02x (HMSF)\r\n", (unsigned char)*(pch+nOffset));
										m_szItem += szPart;
										szPart.Format("HH:MM:SS;FF = %02d:%02d:%02d;%02d\r\n", (unsigned char)*(pch+nOffset+1), (unsigned char)*(pch+nOffset+2), (unsigned char)*(pch+nOffset+3), (unsigned char)*(pch+nOffset+4));
										m_szItem += szPart;
										nOffset += 5;
									} break;
								case 3:  //gpi
									{
										szPart.Format("time_type = 0x%02x (GPI)\r\n", (unsigned char)*(pch+nOffset));
										m_szItem += szPart;
										szPart.Format("GPI_number = %02d\r\nGPI_edge = %02d\r\n", (unsigned char)*(pch+nOffset+1), (unsigned char)*(pch+nOffset+2));
										m_szItem += szPart;
										nOffset += 3;
									} break;
								default:
									{
										szPart.Format("time_type = 0x%02x  - Parse Error on time type\r\n", (unsigned char)*(pch+nOffset));
										bErr = true;
										m_szItem += szPart;
									} break;
								}
							}

							if((!bErr) && (ulBufLen > (unsigned long)nOffset))
							{

/*
	num_ops 1 uimsbf
	for (i=0; i < num_ops; i++ ) 
	{
		opID 2
		data_length 2
		data() * Varies
	}
*/
								int nNumOps = *(pch+nOffset);
								szPart.Format("num_ops = %02d\r\n", nNumOps);
								m_szItem += szPart;

								CString szTitle;
								szTitle = "*** UNKNOWN EVENT ***";
								
								nOffset++;
								int i=0;

								m_szSubItems = "";
								while(i<nNumOps)
								{
									szPart.Format("opId = 0x%02x%02x\r\n", (unsigned char)*(pch+nOffset),  (unsigned char)*(pch+nOffset+1));
									m_szSubItems += szPart;

									int nSize = ((*(pch+nOffset+2))<<8) + *(pch+nOffset+3);

									szPart.Format("data_length = 0x%02x%02x (%d bytes)\r\n", (unsigned char)*(pch+nOffset+2),  (unsigned char)*(pch+nOffset+3), nSize);
									m_szSubItems += szPart;

									if(ulBufLen > (unsigned long)nOffset+8)
									{

										szPart.Format("SMPTE registered ID = %c%c%c%c\r\n", 
											(unsigned char)*(pch+nOffset+4),  
											(unsigned char)*(pch+nOffset+5),
											(unsigned char)*(pch+nOffset+6),
											(unsigned char)*(pch+nOffset+7)
											);
										m_szSubItems += szPart;

										switch(*(pch+nOffset+8))
										{
										case 1: 
											{
												szTitle = "*** EVENT NOTIFICATION ***";
												szPart.Format("proprietary_command = %d (event notification)\r\n", 
													(unsigned char)*(pch+nOffset+8)											
													);

												m_szSubItems += szPart;
												szPart.Format("EventStartDate = %c%c%c%c%c%c%c%c%c%c\r\n", 
													(unsigned char)*(pch+nOffset+9),											
													(unsigned char)*(pch+nOffset+10),											
													(unsigned char)*(pch+nOffset+11),											
													(unsigned char)*(pch+nOffset+12),											
													(unsigned char)*(pch+nOffset+13),											
													(unsigned char)*(pch+nOffset+14),											
													(unsigned char)*(pch+nOffset+15),											
													(unsigned char)*(pch+nOffset+16),											
													(unsigned char)*(pch+nOffset+17),											
													(unsigned char)*(pch+nOffset+18)											
													);

												m_szSubItems += szPart;
												szPart.Format("EventStart = %02d:%02d:%02d;%02d\r\n", 
													(unsigned char)*(pch+nOffset+19),											
													(unsigned char)*(pch+nOffset+20),											
													(unsigned char)*(pch+nOffset+21),											
													(unsigned char)*(pch+nOffset+22)									
													);

												m_szSubItems += szPart;
												szPart.Format("EventDur = %02d:%02d:%02d;%02d\r\n", 
													(unsigned char)*(pch+nOffset+23),											
													(unsigned char)*(pch+nOffset+24),											
													(unsigned char)*(pch+nOffset+25),											
													(unsigned char)*(pch+nOffset+26)									
													);

												m_szSubItems += szPart;

												char buffer[36];
												memset(buffer, 0, 36);
												memcpy(buffer, pch+nOffset+27, 16);

//												AfxMessageBox(buffer);

												CString szLabel = buffer;

												szLabel.TrimLeft();
												szLabel.TrimRight();

												if(szLabel.GetLength()>0)
												{
													szPart.Format("ProgramID = \"%s\"\r\n", szLabel );
												}
												else
												{
													szPart.Format("ProgramID = <Blank>\r\n" );
												}
												m_szSubItems += szPart;

												memset(buffer, 0, 36);
												memcpy(buffer, pch+nOffset+27+16, 32);

												szLabel = buffer;

												szLabel.TrimLeft();
												szLabel.TrimRight();

												if(szLabel.GetLength()>0)
												{
													szPart.Format("ProgramDescription = \"%s\"\r\n", szLabel );
												}
												else
												{
													szPart.Format("ProgramDescription = <Blank>\r\n" );
												}


											} break;
										case 2: 
											{
												szTitle = "*** REPLACE AD ***";
												szPart.Format("proprietary_command = %d (replace ad)\r\n", 
													(unsigned char)*(pch+nOffset+8)											
													);

												m_szSubItems += szPart;
												szPart.Format("PodDur = %02d:%02d:%02d;%02d\r\n", 
													(unsigned char)*(pch+nOffset+9),											
													(unsigned char)*(pch+nOffset+10),											
													(unsigned char)*(pch+nOffset+11),											
													(unsigned char)*(pch+nOffset+12)									
													);

												m_szSubItems += szPart;

												char buffer[36];
												memset(buffer, 0, 36);
												memcpy(buffer, pch+nOffset+13, 16);

//												AfxMessageBox(buffer);

												CString szLabel = buffer;

												szLabel.TrimLeft();
												szLabel.TrimRight();

												if(szLabel.GetLength()>0)
												{
													szPart.Format("LongFormProgramID = \"%s\"\r\n", szLabel );
												}
												else
												{
													szPart.Format("LongFormProgramID = <Blank>\r\n" );
												}


												memset(buffer, 0, 36);
												memcpy(buffer, pch+nOffset+13+16, 32);

												m_szSubItems += szPart;

												szLabel = buffer;

												szLabel.TrimLeft();
												szLabel.TrimRight();

												if(szLabel.GetLength()>0)
												{
													szPart.Format("LongFormProgramDescription = \"%s\"\r\n", szLabel );
												}
												else
												{
													szPart.Format("LongFormProgramDescription = <Blank>\r\n" );
												}

											} break;
										case 3: 
											{
												szTitle = "*** REPLACE CONTENT ***";
												szPart.Format("proprietary_command = %d (replace content)\r\n", 
													(unsigned char)*(pch+nOffset+8)											
													);


												m_szSubItems += szPart;
												szPart.Format("ReplLength = %02d:%02d:%02d;%02d\r\n", 
													(unsigned char)*(pch+nOffset+9),											
													(unsigned char)*(pch+nOffset+10),											
													(unsigned char)*(pch+nOffset+11),											
													(unsigned char)*(pch+nOffset+12)									
													);

												m_szSubItems += szPart;
												szPart.Format("Offset = %02d:%02d:%02d;%02d\r\n", 
													(unsigned char)*(pch+nOffset+13),											
													(unsigned char)*(pch+nOffset+14),											
													(unsigned char)*(pch+nOffset+15),											
													(unsigned char)*(pch+nOffset+16)									
													);

												m_szSubItems += szPart;

												char buffer[36];
												memset(buffer, 0, 36);
												memcpy(buffer, pch+nOffset+17, 16);

//												AfxMessageBox(buffer);

												CString szLabel = buffer;

												szLabel.TrimLeft();
												szLabel.TrimRight();

												if(szLabel.GetLength()>0)
												{
													szPart.Format("ReplProgramID = \"%s\"\r\n", szLabel );
												}
												else
												{
													szPart.Format("ReplProgramID = <Blank>\r\n" );
												}

											} break;
										case 4: 
											{
												szTitle = "*** REJOIN MAIN ***";
												szPart.Format("proprietary_command = %d (rejoin main)\r\n", 
													(unsigned char)*(pch+nOffset+8)											
													);
											} break;
										case 5: 
											{
												szTitle = "*** CURRENT TIME ***";
												szPart.Format("proprietary_command = %d (current time)\r\n", 
													(unsigned char)*(pch+nOffset+8)											
													);
											} break;
										default:
											{
												szTitle = "*** UNKNOWN EVENT ***";
												szPart.Format("proprietary_command = %d (unknown)\r\n", 
													(unsigned char)*(pch+nOffset+8)											
													);
											} break;
										}
										m_szSubItems += szPart;
									}


									nOffset += nSize+4;
									szPart.Format("\r\n");
									m_szSubItems += szPart;

									i++;
								}


								szPart = szTitle + "\r\n\r\n"+ m_szItem;
								m_szItem = szPart;


							}




						}
						else
						{
							// single message
							m_szItem.Format("SCTE104 single message, %d bytes\r\n\r\n", ulBufLen);

							// OK, quick parse this biznitch

							CString szPart;

							if(ulBufLen>=13)
							{
/*
single_operation_message() 
{
	opID 2 uimsbf
	messageSize 2 uimsbf
	result 2 uimsbf
	result_extension 2 uimsbf
	protocol_version 1 uimsbf
	AS_index 1 uimsbf
	message_number 1 uimsbf
	DPI_PID_index 2 uimsbf
	data() * Varies
}
*/

								szPart.Format("opId = 0x%02x%02x\r\n", (unsigned char)*pch,  (unsigned char)*(pch+1));
								m_szItem += szPart;

								int nSize = ( ((unsigned char) (*(pch+2))) <<8) + ((unsigned char)(*(pch+3)));

								szPart.Format("messageSize = 0x%02x%02x (%d bytes)\r\n", (unsigned char)*(pch+2),  (unsigned char)*(pch+3), nSize);
								m_szItem += szPart;

								szPart.Format("result = 0x%02x%02x\r\n", (unsigned char)*(pch+4),  (unsigned char)*(pch+5));
								m_szItem += szPart;
								szPart.Format("result_extension = 0x%02x%02x\r\n", (unsigned char)*(pch+6),  (unsigned char)*(pch+7));
								m_szItem += szPart;
								szPart.Format("protocol_version = 0x%02x\r\n", (unsigned char)*(pch+8));
								m_szItem += szPart;
								szPart.Format("AS_index = 0x%02x\r\n", (unsigned char)*(pch+9));
								m_szItem += szPart;
//								szPart.Format("message_number = 0x%02x (%d decimal)\r\n", (unsigned char)*(pch+10), *(pch+10));
								szPart.Format("message_number = 0x%02x\r\n", (unsigned char)*(pch+10));
								m_szItem += szPart;
								szPart.Format("DPI_PID_index = 0x%02x%02x\r\n", (unsigned char)*(pch+11),  (unsigned char)*(pch+12));
								m_szItem += szPart;

								if(ulBufLen>13)
								{
									szPart.Format("data (%d bytes) = %s\r\n", ulBufLen-13, (pch+13));
									m_szItem += szPart;
								}
							}
							else
							{
								szPart = "Parse Error";
								m_szItem += szPart;
							}

						}
					}
					else
					{
						m_szItem = "Parse Error";
					}
				}
				else
				{
					m_szItem = "Parse Error";
				}


			}
		}
		else
		{
			// otherwise it's some other thing
			m_szItem.Format("%s\r\n\r\n%s", szItem.Left(strlen("2011-Dec-27 09:16:30.113")), szItem.Mid(strlen("2011-Dec-27 09:16:30.113 ")) );
		}

	}
	else
	{
		m_szItem="";
	}

	UpdateData(FALSE);

	*pResult = 0;
}

void CLibrettoParseDlg::OnButtonOutfile() 
{

	UpdateData(TRUE);

	if((m_szFilename.GetLength())&&(m_pchBuffer))
	{
		CBufferUtil bu;
		CString szFile = m_szFilename;
		CString szDelim;

		if(m_nOutputType == 1) // csv
		{
			szFile+="_parsedoutput.csv";
		}
		else
		{
			szFile+="_parsedoutput.txt";
		}


		FILE* fp;
		fp = fopen(szFile, "wb");
		if(fp)
		{
			// OK so now....
			// divide stuff up into records.

			CSafeBufferUtil sbu;

			char* pch = sbu.Token(m_pchBuffer, m_ulLen, "\r\n", MODE_MULTIDELIM);

			while(pch)
			{

				CString szItem = pch;
				if(szItem.Mid(strlen("2011-Dec-27 09:16:30.113"), strlen(" [H ] sending")).CompareNoCase(" [H ] sending")==0)
				{
					if(
							(szItem.Mid(strlen("2011-Dec-27 09:16:30.113 [H ] sending"), 4).CompareNoCase(":  -") )
						&&(szItem.Mid(strlen("2011-Dec-27 09:16:30.113 [H ] sending"), 4).CompareNoCase(": ��") )
						)
					{
						CString szItem;
						szItem = pch;

////////////////////////////////////////////////

		//Parse that biznich;

		if(
				(szItem.Mid(strlen("2011-Dec-27 09:16:30.113"), strlen(" [H ] sending")).CompareNoCase(" [H ] sending")==0)
			)

//2011-Dec-27 10:15:02.191 [H ] sending: [00][03][00][0d][ff][ff][ff][ff][00][00][45][00][00] - keep_alive - 
//2011-Dec-27 10:15:14.066 [H ] sending 100 bytes (r=0): [ff][ff][00][64][00][00][03][00][00][00][02][0a][0f][0e][02][02][81][50][00][47][44][41][54][47][01][32][30][31][31][2d][31][32][2d][32][37][0a][0f][0e][02][00][00][00][00][54][56][2b][3a][55][4e][4b][4e][4f][57][4e][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][20][81][50][00][05][44][41][54][47][04] - Libretto:command_queue - 
//2011-Dec-27 10:15:14.066 [H ] sending: �� - Libretto:debug - 

		{
			// it's a hex send

			CString szHexData;
			CString szSubItems;


			szHexData = "";

			szSubItems = "";

			int nStartIndex = strlen("2011-Dec-27 09:16:30.113 [H ] sending");
			int nType = 0;

			if(szItem.Mid(nStartIndex,3).CompareNoCase(": [")==0)
			{
				nType = 1; // hex send, single
				nStartIndex += 2;
			}
			else
			if(szItem.Mid(nStartIndex).Find(" bytes (r=")>0)
			{

				if((m_bSuppressRepeats)&&(szItem.Mid(nStartIndex).Find(" bytes (r=0")<0))
				{
					nType=0;
				}
				else
				{

					nType = 2; // hex send, double
					nStartIndex += szItem.Mid(nStartIndex).Find(": [");
					if(nStartIndex>=0)
					{
						nStartIndex += 2;

	//CString foo; foo.Format("%d %s", nStartIndex, szItem.Mid(nStartIndex));
	//AfxMessageBox(foo);
					}
					else
						nType=0;
				}

			}

			if(nType == 0)
			{
				// szItem.Format("%s\r\n\r\n%s", szItem.Left(strlen("2011-Dec-27 09:16:30.113")), szItem.Mid(strlen("2011-Dec-27 09:16:30.113 ")) );
			}
			else
			{
				int nEndIndex = szItem.ReverseFind(']')+1;

				if(nEndIndex>0)
				{
					CString szHexBuffer = szItem.Mid(nStartIndex, nEndIndex-nStartIndex);
					
					szHexData = "";

	//				szItem = szHexBuffer;

					CBufferUtil bu;
					unsigned long ulBufLen = szHexBuffer.GetLength();


					unsigned long x=0;
					bool bAlt = false;
					while(x<ulBufLen)
					{
						CString szX = szHexBuffer.Mid(x, 16);
						szHexData += szX;
						if(bAlt)
						{
							szHexData += "\r\n";
						}
						else
						{
							szHexData += "\t";
						}

						bAlt = !bAlt;

						x+= 16;

					}



					char* pchBuffer = szHexBuffer.GetBuffer(0);
					char* pch = bu.DecodeReadableHex(pchBuffer, &ulBufLen);

					if(pch)
					{
						szItem.Format("%d bytes, %d %d", ulBufLen, *pch, *(pch+1));
					// here we have a binary buffer.

						if((*pch == (char)0xff)&&(*(pch+1) == (char)0xff))
						{
							// multi message
							szItem.Format("SCTE104 multi message, %d bytes\r\n\r\n", ulBufLen);

//							AfxMessageBox(szHexBuffer);
							// lets deal:

/*
multiple_operation_message() 
{
	Reserved 2 uimsbf
	messageSize 2 uimsbf
	protocol_version 1 uimsbf
	AS_index 1 uimsbf
	message_number 1 uimsbf
	DPI_PID_index 2 uimsbf
	SCTE35_protocol_version 1 uimsbf
	timestamp() * Varies
	num_ops 1 uimsbf
	for (i=0; i < num_ops; i++ ) 
	{
		opID 2
		data_length 2
		data() * Varies
	}
}
*/


							CString szPart;

							if(ulBufLen>=10)
							{
								szPart.Format("Reserved = 0x%02x%02x\r\n", (unsigned char)*pch,  (unsigned char)*(pch+1));
								szItem += szPart;

								int nSize = ( ((unsigned char) (*(pch+2))) <<8) + ((unsigned char)(*(pch+3)));

								szPart.Format("messageSize = 0x%02x%02x (%d bytes)\r\n", (unsigned char)*(pch+2),  (unsigned char)*(pch+3), nSize);
								szItem += szPart;
								szPart.Format("protocol_version = 0x%02x\r\n", (unsigned char)*(pch+4));
								szItem += szPart;
								szPart.Format("AS_index = 0x%02x\r\n", (unsigned char)*(pch+5));
								szItem += szPart;
//								szPart.Format("message_number = 0x%02x (%d decimal)\r\n", (unsigned char)*(pch+10), *(pch+10));
								szPart.Format("message_number = 0x%02x\r\n", (unsigned char)*(pch+6));
								szItem += szPart;
								szPart.Format("DPI_PID_index = 0x%02x%02x\r\n", (unsigned char)*(pch+7),  (unsigned char)*(pch+8));
								szItem += szPart;

								szPart.Format("SCTE35_protocol_version = 0x%02x\r\n", (unsigned char)*(pch+9));
								szItem += szPart;

							}
							bool bErr = false;
							int nOffset = 10;
							if(ulBufLen>10)
							{
								switch(*(pch+10))
								{
								case 0:  // time type is not required.
									{
										szPart.Format("time_type = 0x%02x (no time required)\r\n", (unsigned char)*(pch+nOffset));
										szItem += szPart;
									} break;
								case 1:  // time type is UTC time
									{
										szPart.Format("time_type = 0x%02x (UTC time)\r\n", (unsigned char)*(pch+nOffset));
										szItem += szPart;

										int nTime = 
											((*(pch+nOffset+1))<<24) + 
											((*(pch+nOffset+2))<<16) +
											((*(pch+nOffset+3))<<8) +
											((*(pch+nOffset+4)));

										szPart.Format("UTC_seconds = %d\r\n", nTime);
										szItem += szPart;

										nTime = 
											((*(pch+nOffset+5))<<8) +
											((*(pch+nOffset+6)));

										szPart.Format("UTC_microseconds = %d\r\n", nTime);
										szItem += szPart;

										nOffset += 7;

									} break;
								case 2:  // hmsf
									{
										szPart.Format("time_type = 0x%02x (HMSF)\r\n", (unsigned char)*(pch+nOffset));
										szItem += szPart;
										szPart.Format("HH:MM:SS;FF = %02d:%02d:%02d;%02d\r\n", (unsigned char)*(pch+nOffset+1), (unsigned char)*(pch+nOffset+2), (unsigned char)*(pch+nOffset+3), (unsigned char)*(pch+nOffset+4));
										szItem += szPart;
										nOffset += 5;
									} break;
								case 3:  //gpi
									{
										szPart.Format("time_type = 0x%02x (GPI)\r\n", (unsigned char)*(pch+nOffset));
										szItem += szPart;
										szPart.Format("GPI_number = %02d\r\nGPI_edge = %02d\r\n", (unsigned char)*(pch+nOffset+1), (unsigned char)*(pch+nOffset+2));
										szItem += szPart;
										nOffset += 3;
									} break;
								default:
									{
										szPart.Format("time_type = 0x%02x  - Parse Error on time type\r\n", (unsigned char)*(pch+nOffset));
										bErr = true;
										szItem += szPart;
									} break;
								}
							}

							if((!bErr) && (ulBufLen > (unsigned long)nOffset))
							{

/*
	num_ops 1 uimsbf
	for (i=0; i < num_ops; i++ ) 
	{
		opID 2
		data_length 2
		data() * Varies
	}
*/
								int nNumOps = *(pch+nOffset);
								szPart.Format("num_ops = %02d\r\n", nNumOps);
								szItem += szPart;

								CString szTitle;
								szTitle = "*** UNKNOWN EVENT ***";
								
								nOffset++;
								int i=0;

								szSubItems = "";
								while(i<nNumOps)
								{
									szPart.Format("opId = 0x%02x%02x\r\n", (unsigned char)*(pch+nOffset),  (unsigned char)*(pch+nOffset+1));
									szSubItems += szPart;

									int nSize = ((*(pch+nOffset+2))<<8) + *(pch+nOffset+3);

									szPart.Format("data_length = 0x%02x%02x (%d bytes)\r\n", (unsigned char)*(pch+nOffset+2),  (unsigned char)*(pch+nOffset+3), nSize);
									szSubItems += szPart;

									if(ulBufLen > (unsigned long)nOffset+8)
									{

										szPart.Format("SMPTE registered ID = %c%c%c%c\r\n", 
											(unsigned char)*(pch+nOffset+4),  
											(unsigned char)*(pch+nOffset+5),
											(unsigned char)*(pch+nOffset+6),
											(unsigned char)*(pch+nOffset+7)
											);
										szSubItems += szPart;

										switch(*(pch+nOffset+8))
										{
										case 1: 
											{
												szTitle = "*** EVENT NOTIFICATION ***";
												szPart.Format("proprietary_command = %d (event notification)\r\n", 
													(unsigned char)*(pch+nOffset+8)											
													);

												szSubItems += szPart;
												szPart.Format("EventStartDate = %c%c%c%c%c%c%c%c%c%c\r\n", 
													(unsigned char)*(pch+nOffset+9),											
													(unsigned char)*(pch+nOffset+10),											
													(unsigned char)*(pch+nOffset+11),											
													(unsigned char)*(pch+nOffset+12),											
													(unsigned char)*(pch+nOffset+13),											
													(unsigned char)*(pch+nOffset+14),											
													(unsigned char)*(pch+nOffset+15),											
													(unsigned char)*(pch+nOffset+16),											
													(unsigned char)*(pch+nOffset+17),											
													(unsigned char)*(pch+nOffset+18)											
													);

												szSubItems += szPart;
												szPart.Format("EventStart = %02d:%02d:%02d;%02d\r\n", 
													(unsigned char)*(pch+nOffset+19),											
													(unsigned char)*(pch+nOffset+20),											
													(unsigned char)*(pch+nOffset+21),											
													(unsigned char)*(pch+nOffset+22)									
													);

												szSubItems += szPart;
												szPart.Format("EventDur = %02d:%02d:%02d;%02d\r\n", 
													(unsigned char)*(pch+nOffset+23),											
													(unsigned char)*(pch+nOffset+24),											
													(unsigned char)*(pch+nOffset+25),											
													(unsigned char)*(pch+nOffset+26)									
													);

												szSubItems += szPart;

												char buffer[36];
												memset(buffer, 0, 36);
												memcpy(buffer, pch+nOffset+27, 16);

//												AfxMessageBox(buffer);

												CString szLabel = buffer;

												szLabel.TrimLeft();
												szLabel.TrimRight();

												if(szLabel.GetLength()>0)
												{
													szPart.Format("ProgramID = \"%s\"\r\n", szLabel );
												}
												else
												{
													szPart.Format("ProgramID = <Blank>\r\n" );
												}
												szSubItems += szPart;

												memset(buffer, 0, 36);
												memcpy(buffer, pch+nOffset+27+16, 32);

												szLabel = buffer;

												szLabel.TrimLeft();
												szLabel.TrimRight();

												if(szLabel.GetLength()>0)
												{
													szPart.Format("ProgramDescription = \"%s\"\r\n", szLabel );
												}
												else
												{
													szPart.Format("ProgramDescription = <Blank>\r\n" );
												}


											} break;
										case 2: 
											{
												szTitle = "*** REPLACE AD ***";
												szPart.Format("proprietary_command = %d (replace ad)\r\n", 
													(unsigned char)*(pch+nOffset+8)											
													);

												szSubItems += szPart;
												szPart.Format("PodDur = %02d:%02d:%02d;%02d\r\n", 
													(unsigned char)*(pch+nOffset+9),											
													(unsigned char)*(pch+nOffset+10),											
													(unsigned char)*(pch+nOffset+11),											
													(unsigned char)*(pch+nOffset+12)									
													);

												szSubItems += szPart;

												char buffer[36];
												memset(buffer, 0, 36);
												memcpy(buffer, pch+nOffset+13, 16);

//												AfxMessageBox(buffer);

												CString szLabel = buffer;

												szLabel.TrimLeft();
												szLabel.TrimRight();

												if(szLabel.GetLength()>0)
												{
													szPart.Format("LongFormProgramID = \"%s\"\r\n", szLabel );
												}
												else
												{
													szPart.Format("LongFormProgramID = <Blank>\r\n" );
												}


												memset(buffer, 0, 36);
												memcpy(buffer, pch+nOffset+13+16, 32);

												szSubItems += szPart;

												szLabel = buffer;

												szLabel.TrimLeft();
												szLabel.TrimRight();

												if(szLabel.GetLength()>0)
												{
													szPart.Format("LongFormProgramDescription = \"%s\"\r\n", szLabel );
												}
												else
												{
													szPart.Format("LongFormProgramDescription = <Blank>\r\n" );
												}

											} break;
										case 3: 
											{
												szTitle = "*** REPLACE CONTENT ***";
												szPart.Format("proprietary_command = %d (replace content)\r\n", 
													(unsigned char)*(pch+nOffset+8)											
													);


												szSubItems += szPart;
												szPart.Format("ReplLength = %02d:%02d:%02d;%02d\r\n", 
													(unsigned char)*(pch+nOffset+9),											
													(unsigned char)*(pch+nOffset+10),											
													(unsigned char)*(pch+nOffset+11),											
													(unsigned char)*(pch+nOffset+12)									
													);

												szSubItems += szPart;
												szPart.Format("Offset = %02d:%02d:%02d;%02d\r\n", 
													(unsigned char)*(pch+nOffset+13),											
													(unsigned char)*(pch+nOffset+14),											
													(unsigned char)*(pch+nOffset+15),											
													(unsigned char)*(pch+nOffset+16)									
													);

												szSubItems += szPart;

												char buffer[36];
												memset(buffer, 0, 36);
												memcpy(buffer, pch+nOffset+17, 16);

//												AfxMessageBox(buffer);

												CString szLabel = buffer;

												szLabel.TrimLeft();
												szLabel.TrimRight();

												if(szLabel.GetLength()>0)
												{
													szPart.Format("ReplProgramID = \"%s\"\r\n", szLabel );
												}
												else
												{
													szPart.Format("ReplProgramID = <Blank>\r\n" );
												}

											} break;
										case 4: 
											{
												szTitle = "*** REJOIN MAIN ***";
												szPart.Format("proprietary_command = %d (rejoin main)\r\n", 
													(unsigned char)*(pch+nOffset+8)											
													);
											} break;
										case 5: 
											{
												szTitle = "*** CURRENT TIME ***";
												szPart.Format("proprietary_command = %d (current time)\r\n", 
													(unsigned char)*(pch+nOffset+8)											
													);
											} break;
										default:
											{
												szTitle = "*** UNKNOWN EVENT ***";
												szPart.Format("proprietary_command = %d (unknown)\r\n", 
													(unsigned char)*(pch+nOffset+8)											
													);
											} break;
										}
										szSubItems += szPart;
									}


									nOffset += nSize+4;
									szPart.Format("\r\n");
									szSubItems += szPart;

									i++;
								}


								szPart = szTitle + "\r\n\r\n"+ szItem;
								szItem = szPart;


							}



							// here we have everything.
							

							if(m_nOutputType == 1) // csv
							{
								// have to escape quotes.

								fprintf(fp, "\"");
								
								char* pchOut = bu.EscapeChar(szItem.GetBuffer(0), '"');
								if(pchOut)
								{
									fprintf(fp, "%s\r\n\r\n", pchOut);
									free(pchOut);
								}
								else
								{
									fprintf(fp, "Parse error");
								}

								pchOut = bu.EscapeChar(szSubItems.GetBuffer(0), '"');
								if(pchOut)
								{
									fprintf(fp, "%s", pchOut);
									free(pchOut);
								}
								else
								{
									fprintf(fp, "Parse error");
								}

								fprintf(fp, "\"\r\n");
							}
							else
							{
								fprintf(fp, "%s\r\n\r\n%s\r\n\r\n\r\n\r\n", szItem, szSubItems);
							}





						}
						else
						{
							// single message // ignore
						}
					}
					else
					{
						szItem = "Parse Error";
					}
				}
				else
				{
					szItem = "Parse Error";
				}

			}
		}

						//////////////////////////////////////////////////////
					
					
					}
				}
				pch = sbu.Token(NULL, NULL, "\r\n", MODE_MULTIDELIM);
			}

			fclose(fp);


			CString foo;
			foo.Format("Output file %s generated", szFile);
			AfxMessageBox(foo);

		}
		else
		{
			AfxMessageBox("Error opening output file");
		}
	}
	else
	{
		AfxMessageBox("No file to generate");
	}
	
}
