// WheelData.h: interface for the CWheelData and CWheelMessage classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WHEELDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_)
#define AFX_WHEELDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "..\..\..\Common\LAN\NetUtil.h"
#include "..\..\..\Cortex Objects\Tabulator\TabulatorDefines.h"
#include "..\..\..\Cortex Objects\Tabulator\TabulatorData.h"
#include "..\..\..\Common\MFC\ODBC\DBUtil.h"

#define WHEEL_MSG_STATUS_NONE				0x00000000
#define WHEEL_MSG_TYPE_NONE					0x00000000
#define WHEEL_MSG_TYPE_DISABLED			0x00000001 // temp for now... not really a type
#define WHEEL_MSG_MARK_NONE					0x00000000
#define WHEEL_MSG_MARK_BAD					0x00000001
#define WHEEL_MSG_MARK_GOOD					0x00000002
#define WHEEL_MSG_MARK_GREAT				0x00000003
#define WHEEL_MSG_MARK_DEL					0x00000010
#define WHEEL_MSG_MARK_MASK					0x0000000f
#define WHEEL_MSG_STATUS_REVISED		0x00000020
#define WHEEL_MSG_STATUS_COMMIT			0x00000100  //commit to ticker engine
#define WHEEL_MSG_STATUS_PLAYING		0x00000200
#define WHEEL_MSG_STATUS_PLAYED			0x00000400
#define WHEEL_MSG_STATUS_INSERTED		0x00001000
#define WHEEL_MSG_STATUS_ERROR			0x00002000
#define WHEEL_MSG_STATUS_QUEUED			0x00004000
#define WHEEL_MSG_TYPE_DEFAULT			0x00000001
#define WHEEL_MSG_STATUS_SENT				0x00010000  // sent to ticker database
#define WHEEL_MSG_STATUS_TALLY			0x00100000  

#define WHEEL_COLOR_COMMIT		RGB(255,255,128)
#define WHEEL_COLOR_PLAYING		RGB(128,255,128)
#define WHEEL_COLOR_PLAYED		RGB(223,233,233)
#define WHEEL_COLOR_ERROR			RGB(255,64,64)
#define WHEEL_COLOR_DISABLED	RGB(128,128,128)
//#define WHEEL_COLOR_DISABLED_TXT	RGB(223,223,223)

#define SENDMSG_SENDER_MAXLEN 64
#define SENDMSG_MESSAGE_MAXLEN 512

/*  // old style with feed-ticker for Kargo with client app
class CWheelMessage
{
public:
	CWheelMessage();
	virtual ~CWheelMessage();

	CString SQLUpdate(bool bTicker, double dblSeekIndex);

	CString m_szMessage;
	CString m_szRevisedMessage;
	int m_nTimestamp;
	int m_nLastUpdate;
	int m_nID;
	int m_nStatus;
	int m_nType;
	int m_nPlayed;
	double m_dblIndex;
};
*/
/*
create table Wheel_1_Ticker(
id int identity(1,1), 
message varchar(4096), 
aux_data1 varchar(1024), 
aux_data2 varchar(1024), 
sort_order decimal(20,3), 
dwell_override decimal(20,3), 
valid_from int, 
expires_on int, 
allowed_cycles int, 
actual_cycles int, 
active int, 
status int, 
last_played int, 
created_on int, 
created_by varchar(32), 
last_modified_on int, 
last_modified_by varchar(32));   
*/

class CWheelMessage
{
public:
	CWheelMessage();
	virtual ~CWheelMessage();

	CString SQLUpdate(bool bTicker, double dblSeekIndex);

	CString m_szMessage;//message varchar(4096),
	CString m_szAux1;//aux_data1 varchar(1024),
	CString m_szAux2;//aux_data2 varchar(1024), 
	int m_nLastPlayed;//last_played int, 
	int m_nID;  //id int identity(1,1), 
//	int m_nSourceID;  //need a field for this at some point. // not used yet.
	int m_nStatus; //status int,
//	int m_nType;  // used to have defaults... now just use unlimited cycles, but prob want to go back to having defaults.
	int m_nValidFrom; //valid_from int,
	int m_nExpires;//expires_on int, 
	int m_nPlayLimit;//allowed_cycles int, 
	int m_nPlayed;//actual_cycles int, 
	double m_dblIndex;//dwell_override decimal(20,3),
	double m_dblDwellOverride;//sort_order decimal(20,3),
	bool m_bActive;//	active int,

	// not using these internally:
	/*
created_on int, 
created_by varchar(32), 
last_modified_on int, 
last_modified_by varchar(32));   
*/
	int m_nLastUpdate; // but using this internally, not in the table.
};


class CWheelData  
{
public:
	CWheelData();
	virtual ~CWheelData();

	// remove pipes
	CString Encode(CString szText);
	// put back pipes
	CString Decode(CString szText);

	void PublishData();
	void PublishTicker();

	int InitDLLData(DLLthread_t* pData);

	int InsertMessage(CWheelMessage* pevent, int index=-1);
	int DeleteMessage(int index);  // removes from array
	int DeleteAllMessages();  // removes entire array
	int FindID(int nID, int nStartIndex=0);
	int GetMessagesSince(int nTime, CString* pszReturn, int nSkipMax);

	CString XMLTextNodeValue(IXMLDOMNodePtr pNode);

	int InsertTickerMessage(CWheelMessage* pevent, int index=-1);
	int DeleteTickerMessage(int index);  // removes from array
	int DeleteAllTickerMessages(int nMode=-1);  // removes entire array
	int FindTickerID(int nID, double dblIndex=-1.0, int nStartIndex=0);
	int GetTickerMessagesSince(int nTime, CString* pszReturn, int nSkipMax);

#ifndef REMOVE_CLIENTSERVER
	int StringToMessage(CString szString, CWheelMessage* pevent);
	CString MessageToString(CWheelMessage* pevent);
#endif //#ifndef REMOVE_CLIENTSERVER
	int SendMsg(int nType, char* pszSender, char* pszMessage, ...);
	int SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...);

	CWheelMessage* GetNextTickerMessage(CDBUtil* pdb, CDBconn* pdbConn, CWheelMessage** ppPlaying, char* pszInfo);
	int SetTickerItem(CDBUtil* pdb, CDBconn* pdbConn, CWheelMessage* pItem, char* pszInfo);
	int PlayEvent(CDBUtil* pdb, CDBconn* pdbConn, CTabulatorEventArray** ppEvent, CWheelMessage* pItem, bool bPreamble, char* pszInfo);
	CString ParseValue(CString szValue, bool bPreamble, CWheelMessage* pItem);

	bool m_bDialogStarted;
	SOCKET m_socket;
//	CRITICAL_SECTION m_critClientSocket;

	CNetUtil m_net;
	CDBUtil m_dbu;

	int m_nLastID;
	int m_nLastFoundIndex;
	int m_nLastInternalID;

	CWheelMessage** m_msg;
	int m_nMessages;
	int m_nMessagesDelta;
	int m_nMessagesUpcounting;
	bool m_bMessagesUpcounting;
	CRITICAL_SECTION m_critMsg;
	CRITICAL_SECTION m_critSQL;

	CWheelMessage** m_msgout;
	int m_nMessagesOut;
	int m_nMessagesOutArray;
	CRITICAL_SECTION m_critMsgOut;
	double m_dblLastDataOutUpdate;

	CWheelMessage** m_tickerout;
	int m_nTickerMessagesOut;
	int m_nTickerMessagesOutArray;
	CRITICAL_SECTION m_critTickerOut;
	double m_dblLastTickerOutUpdate;

	BOOL m_bPublishingData;
	BOOL m_bPublishingTicker;
	BOOL m_bCyclingTickerDefaults;

	CTabulatorEventArray* m_pEvents;

	CTabulatorEventArray* m_pInEvents;
	CTabulatorEventArray* m_pOutEvents;
	CTabulatorEventArray* m_pTextEvents;
	CRITICAL_SECTION m_critEventsSettings;

	int m_nLastEventMod;

	CDBUtil*  m_pdb;
	CDBconn* m_pdbConn;

	SOCKET m_tickersocket;
	CWheelMessage** m_ticker;
	int m_nTickerMessages;
	bool m_bTickerReady;
	bool m_bTickerGotNext;
	CString m_szNextTickerMessage;
	int m_nTickerMessagesDelta;
	int m_nTickerMessagesUpcounting;
	bool m_bTickerMessagesUpcounting;

	bool m_bMovedItem;

	CWheelMessage* m_pmsgPlaying;
	CWheelMessage* m_pmsgNext;

	CRITICAL_SECTION m_critDefaultID;
	CRITICAL_SECTION m_critTicker;
	double m_dblTickerMessageCursor;
	double m_dblTickerDefaultCursor;
	double m_dblTickerMessageMax;
	double m_dblTickerDefaultMax;
	int m_nTickerIDMaxNeg;

	CRITICAL_SECTION m_critMarks;
	CString m_szMarksBuffer;
	_timeb m_timeMarks;
	_timeb m_timeStart;
	_timeb m_timeLastPlay;
	int m_nDwellTime;
	bool m_bQueued;

	bool m_bInEventChanged;  // the event identifier of the transition in (Server_Ticker_In) command has changed
	bool m_bOutEventChanged;  // the event identifier of the transition out (Server_Ticker_Out) command has changed
	bool m_bTextEventChanged;  // the event identifier of the text message event has changed



	CRITICAL_SECTION m_critTransact;
//	bool m_bTransact;

	BOOL	m_bSortTimestampsAsc;
	BOOL	m_bForceRefresh;

	BOOL	m_bFilterAge;
	BOOL	m_bFilterBad;
	BOOL	m_bFilterCommitted;
	BOOL	m_bFilterDeleted;
	BOOL	m_bFilter;
	BOOL	m_bFilterGood;
	BOOL	m_bFilterGreat;
	BOOL	m_bFilterLength;
	BOOL	m_bFilterPlayed;
	BOOL	m_bFilterUnmarked;

	int	m_nMinutesComparator;
	int	m_nLengthComparator;
	int	m_nTimesPlayedComparator;
	int	m_nMinutesValue;
	int	m_nLengthValue;
	int	m_nTimesPlayedValue;

	double m_dblLastDataUpdate;
	double m_dblLastTickerUpdate;


	//UI stuff
	HCURSOR m_hcurArrow;
	HCURSOR	m_hcurNoDrop;
	HCURSOR	m_hcurCommit;
	HCURSOR	m_hcurUncommit;
	HCURSOR	m_hcurSize;
	HCURSOR	m_hcurNS;
	
	BOOL m_bDraggingCommit;
	BOOL m_bDraggingUncommit;
	BOOL m_bDraggingHDivider;
	BOOL m_bDraggingVDivider;
	CRect m_rcLimit;

	CPoint m_ptStartDivDrag;
	CPoint m_ptDownClick;

	BOOL m_bCommitEnabled;
	CString m_szCommitData;
	int m_nCommitID;
	double m_dblCommitIndex;
	double m_dblCommitBeforeIndex;
	int m_nCommitInsertBeforeID;
	int m_nCommitStatus;
	CWheelMessage* m_pmsgCommit; 
	BYTE m_byteCommitAttribs;
	DWORD m_dwCommitSchedTime;
	DWORD m_dwCommitExpiryTime;
	BOOL m_bCommitMode; //c.f. Uncommit.
	BOOL m_bCommitDefaultSource;

};


#endif // !defined(AFX_WHEELDATA_H__158D01CF_EC57_4682_8691_33F7AF8082AD__INCLUDED_)
