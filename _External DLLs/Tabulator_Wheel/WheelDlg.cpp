// WheelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Wheel.h"
#include "WheelDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "..\..\..\Common\TXT\SmutFilter\smutfilter.h"

#define COMP_EQ 0
#define COMP_LT 1
#define COMP_GT 2
#define COMP_EQEX 3 //explicit

extern CWheelApp			theApp;
extern CWheelCore			g_core;
extern CWheelData			g_data;
extern CWheelDlg*			g_pdlg;
extern CWheelSettings	g_settings;
extern CMessager*					g_ptabmsgr;
extern DLLdata_t       g_dlldata;

CSmutFilter* g_psmut;


/////////////////////////////////////////////////////////////////////////////
// CWheelDlg dialog


CWheelDlg::CWheelDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWheelDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWheelDlg)
	m_bFilterAge = FALSE;
	m_bFilterBad = FALSE;
	m_bFilterCommitted = FALSE;
	m_bFilterDeleted = FALSE;
	m_bFilter = FALSE;
	m_bFilterGood = FALSE;
	m_bFilterGreat = FALSE;
	m_bFilterLength = FALSE;
	m_bFilterPlayed = FALSE;
	m_bFilterUnmarked = FALSE;
	m_szMinutes = _T(">60");
	m_szLength = _T("<100");
	m_szTimesPlayed = _T(">0");
	//}}AFX_DATA_INIT

	m_nWindowLast=-1;
	m_bDontGetOlderFeedRecords = FALSE;
	m_bChoiceMade = FALSE;

	m_bSortTimestampsAsc=TRUE;
	m_bVis=FALSE;
	m_bNewSizeInit=TRUE;
	m_dblLastDataUpdate=-1.0;
	m_dblLastTickerUpdate=-1.0;
//	m_szCurrentPreviewText=_T("");
	m_bDefeatSmut = FALSE;
	m_nPointerLast=-1;

	m_nSelIdx=-1;

	m_nMinutesComparator=COMP_GT;
	m_nLengthComparator=COMP_LT;
	m_nTimesPlayedComparator=COMP_GT;
	m_nMinutesValue=60;
	m_nLengthValue=100;
	m_nTimesPlayedValue=0;

	InitializeCriticalSection(&m_critData);
	InitializeCriticalSection(&m_critDefault);
	InitializeCriticalSection(&m_critTicker);
}


void CWheelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWheelDlg)
	DDX_Check(pDX, IDC_CHECK_AGE, m_bFilterAge);
	DDX_Check(pDX, IDC_CHECK_BAD, m_bFilterBad);
	DDX_Check(pDX, IDC_CHECK_COMMITTED, m_bFilterCommitted);
	DDX_Check(pDX, IDC_CHECK_DEL, m_bFilterDeleted);
	DDX_Check(pDX, IDC_CHECK_FILTER, m_bFilter);
	DDX_Check(pDX, IDC_CHECK_GOOD, m_bFilterGood);
	DDX_Check(pDX, IDC_CHECK_GREAT, m_bFilterGreat);
	DDX_Check(pDX, IDC_CHECK_LENGTH, m_bFilterLength);
	DDX_Check(pDX, IDC_CHECK_PLAYED, m_bFilterPlayed);
	DDX_Check(pDX, IDC_CHECK_UNMARKED, m_bFilterUnmarked);
	DDX_Text(pDX, IDC_EDIT_MINS, m_szMinutes);
	DDX_Text(pDX, IDC_EDIT_NUMCHARS, m_szLength);
	DDX_Text(pDX, IDC_EDIT_TIMESPLAYED, m_szTimesPlayed);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWheelDlg, CDialog)
	//{{AFX_MSG_MAP(CWheelDlg)
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_BN_CLICKED(IDC_BUTTON_COMMIT, OnButtonCommit)
	ON_BN_CLICKED(IDC_BUTTON_COMMIT2, OnButtonCommitDefault)
	ON_BN_CLICKED(IDC_BUTTON_DATA, OnButtonData)
	ON_BN_CLICKED(IDC_BUTTON_TICKER, OnButtonTicker)
	ON_BN_CLICKED(IDC_CHECK_AGE, OnCheckAge)
	ON_BN_CLICKED(IDC_CHECK_BAD, OnCheckBad)
	ON_BN_CLICKED(IDC_CHECK_COMMITTED, OnCheckCommitted)
	ON_BN_CLICKED(IDC_CHECK_DEL, OnCheckDel)
	ON_BN_CLICKED(IDC_CHECK_FILTER, OnCheckFilter)
	ON_BN_CLICKED(IDC_CHECK_GOOD, OnCheckGood)
	ON_BN_CLICKED(IDC_CHECK_GREAT, OnCheckGreat)
	ON_BN_CLICKED(IDC_CHECK_LENGTH, OnCheckLength)
	ON_BN_CLICKED(IDC_CHECK_PLAYED, OnCheckPlayed)
	ON_BN_CLICKED(IDC_CHECK_UNMARKED, OnCheckUnmarked)
	ON_EN_CHANGE(IDC_RICHEDIT_VIEW, OnChangeRicheditView)
	ON_NOTIFY(LVN_BEGINDRAG, IDC_LIST_PLAYSTACK, OnBegindragListPlaystack)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_PLAYSTACK, OnItemchangedListPlaystack)
	ON_NOTIFY(LVN_ITEMCHANGING, IDC_LIST_PLAYSTACK, OnItemchangingListPlaystack)
	ON_WM_WINDOWPOSCHANGED()
	ON_NOTIFY(NM_SETFOCUS, IDC_LIST_PLAYSTACK, OnSetfocusListPlaystack)
	ON_NOTIFY(NM_KILLFOCUS, IDC_LIST_PLAYSTACK, OnKillfocusListPlaystack)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_PLAYSTACK, OnRclickListPlaystack)
	ON_NOTIFY(LVN_BEGINDRAG, IDC_LIST_DEFAULTSTACK, OnBegindragListDefaultstack)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_DEFAULTSTACK, OnItemchangedListDefaultstack)
	ON_NOTIFY(LVN_ITEMCHANGING, IDC_LIST_DEFAULTSTACK, OnItemchangingListDefaultstack)
	ON_NOTIFY(NM_SETFOCUS, IDC_LIST_DEFAULTSTACK, OnSetfocusListDefaultstack)
	ON_NOTIFY(NM_KILLFOCUS, IDC_LIST_DEFAULTSTACK, OnKillfocusListDefaultstack)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_DEFAULTSTACK, OnRclickListDefaultstack)
	ON_NOTIFY(LVN_BEGINDRAG, IDC_LIST_DATASTACK, OnBegindragListText)
	ON_NOTIFY(NM_SETFOCUS, IDC_LIST_DATASTACK, OnSetfocusListText)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_DATASTACK, OnRclickListText)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_DATASTACK, OnRdblclkListText)
	ON_NOTIFY(LVN_ITEMCHANGING, IDC_LIST_DATASTACK, OnItemchangingListText)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_DATASTACK, OnItemchangedListText)
	ON_NOTIFY(NM_KILLFOCUS, IDC_LIST_DATASTACK, OnKillfocusListText)
	ON_EN_CHANGE(IDC_EDIT_MINS, OnChangeEditMins)
	ON_EN_CHANGE(IDC_EDIT_NUMCHARS, OnChangeEditNumchars)
	ON_EN_CHANGE(IDC_EDIT_TIMESPLAYED, OnChangeEditTimesplayed)
	ON_NOTIFY(EN_KILLFOCUS, IDC_RICHEDIT_VIEW, OnKillfocusRicheditView)
	ON_NOTIFY(TTN_NEEDTEXT, 0, OnToolTipNotify)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST_DATASTACK, OnColumnclickListDatastack)
	ON_COMMAND(ID_CMD_CLEARDEFAULT, OnClearDefault)
	ON_COMMAND(ID_CMD_CLEARTICKER, OnClearTicker)
	ON_COMMAND(ID_CMD_CLEARFEED, OnClearFeed)
	ON_COMMAND(ID_CMD_REFRESH, OnRefresh)
	ON_COMMAND(ID_CMD_SETTINGSDLG, OnSettingsDlg)
	ON_COMMAND(ID_CMD_TOGGLEACTIVE, OnToggleActive)
	ON_BN_CLICKED(IDC_CHECK_CYCLE, OnCheckCycle)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CWheelDlg message handlers





void CWheelDlg::OnCancel() 
{
//	CDialog::OnCancel();
}

void CWheelDlg::OnOK() 
{
//CDialog::OnOK();
}

BOOL CWheelDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(IDD, pParentWnd);
}

#ifndef REMOVE_CLIENTSERVER

BOOL CWheelDlg::IsFilteredOut(CWheelMessage* pmsg)
{
	if(!m_bFilter) return FALSE;

	if(pmsg)
	{
		if((m_bFilterBad)&&((pmsg->m_nStatus&WHEEL_MSG_MARK_MASK)==WHEEL_MSG_MARK_BAD)) return TRUE;

		if((m_bFilterGood)&&((pmsg->m_nStatus&WHEEL_MSG_MARK_MASK)==WHEEL_MSG_MARK_GOOD)) return TRUE;

		if((m_bFilterGreat)&&((pmsg->m_nStatus&WHEEL_MSG_MARK_MASK)==WHEEL_MSG_MARK_GREAT)) return TRUE;

		if((m_bFilterUnmarked)&&((pmsg->m_nStatus&WHEEL_MSG_MARK_MASK)==WHEEL_MSG_MARK_NONE)) return TRUE;

		if((m_bFilterCommitted)&&(pmsg->m_nStatus&WHEEL_MSG_STATUS_SENT)) return TRUE;


		if((m_bFilterAge)&&(m_nMinutesValue>0))
		{
			if((g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA]))
			{
				switch(m_nMinutesComparator)
				{
				case COMP_EQ: //0
					{
						if((g_dlldata.thread[WHEEL_DATA]->timebTick.time - pmsg->m_nTimestamp)/60 == m_nMinutesValue) 	return TRUE;
					} break;
				case COMP_LT: //1
					{
						if((g_dlldata.thread[WHEEL_DATA]->timebTick.time - pmsg->m_nTimestamp)/60 < m_nMinutesValue) 	return TRUE;
					} break;
				case COMP_GT: //2
					{
						if((g_dlldata.thread[WHEEL_DATA]->timebTick.time - pmsg->m_nTimestamp)/60 > m_nMinutesValue) 	return TRUE;
					} break;
				}
			}
		}

		if((m_bFilterLength)&&(m_nLengthValue>0))
		{
			int nLen = pmsg->m_szRevisedMessage.GetLength();
			if(nLen<=0) nLen = pmsg->m_szMessage.GetLength();
				switch(m_nLengthComparator)
				{
				case COMP_EQ: //0
					{
						if(nLen == m_nLengthValue) 	return TRUE;
					} break;
				case COMP_LT: //1
					{
						if(nLen < m_nLengthValue) 	return TRUE;
					} break;
				case COMP_GT: //2
					{
						if(nLen > m_nLengthValue) 	return TRUE;
					} break;
				}
		}

		if((m_bFilterPlayed)&&(m_nTimesPlayedValue>=0))
		{
				switch(m_nTimesPlayedComparator)
				{
				case COMP_EQ: //0
					{
						if(pmsg->m_nPlayed == m_nTimesPlayedValue) 	return TRUE;
					} break;
				case COMP_LT: //1
					{
						if(pmsg->m_nPlayed < m_nTimesPlayedValue) 	return TRUE;
					} break;
				case COMP_GT: //2
					{
						if(pmsg->m_nPlayed > m_nTimesPlayedValue) 	return TRUE;
					} break;
				}
		}

		return FALSE;
	}

	return TRUE;
}

#endif //#ifndef REMOVE_CLIENTSERVER

void CWheelDlg::ClearTickerSelection()
{
	int nItem= m_lceTicker.GetNextItem( -1,  LVNI_SELECTED );
	if (nItem>-1) // valid selection
	{
		m_lceTicker.SetItem(nItem, 0, LVIF_STATE, NULL, 0, ~LVIS_SELECTED, LVIS_SELECTED, 0);
	}
}

void CWheelDlg::ClearDefaultSelection()
{
	int nItem= m_lceDefault.GetNextItem( -1,  LVNI_SELECTED );
	if (nItem>-1) // valid selection
	{
		m_lceDefault.SetItem(nItem, 0, LVIF_STATE, NULL, 0, ~LVIS_SELECTED, LVIS_SELECTED, 0);
	}
}

void CWheelDlg::ClearFeedSelection()
{
	int nItem= m_lceData.GetNextItem( -1,  LVNI_SELECTED );
	if (nItem>-1) // valid selection
	{
		m_lceData.SetItem(nItem, 0, LVIF_STATE, NULL, 0, ~LVIS_SELECTED, LVIS_SELECTED, 0);
	}
}

void CWheelDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
//sizing stuff
	if((bShow)&&(m_bNewSizeInit))
	{
		int nCtrlID = 0;

		GetClientRect(&m_rcDlg); 
		CRect rcDlg=m_rcDlg;
		for (int i=0;i<DLG_NUM_MOVING_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_BNTKR: nCtrlID=IDC_BUTTON_TICKER;break;
			case ID_STHWIPE: nCtrlID=IDC_STATIC_HWIPE;break;
			case ID_STVWIPE: nCtrlID=IDC_STATIC_VWIPE;break;
			case ID_STHWIPE2: nCtrlID=IDC_STATIC_HWIPE2;break;
			case ID_REFEED: nCtrlID=IDC_RICHEDIT_DOWNLOADSTATUS;break;
			case ID_RETICK: nCtrlID=IDC_RICHEDIT_TICKERSTATUS;break;
			case ID_REPREV: nCtrlID=IDC_RICHEDIT_VIEW;break;
			case ID_LCEFEED: nCtrlID=IDC_LIST_DATASTACK;break;
			case ID_BNCOMMIT: nCtrlID=IDC_BUTTON_COMMIT;break;
			case ID_BNCOMMIT2: nCtrlID=IDC_BUTTON_COMMIT2;break;
			case ID_LCEDEFLT: nCtrlID=IDC_LIST_DEFAULTSTACK;break;
			case ID_LCESTACK: nCtrlID=IDC_LIST_PLAYSTACK;break;
			case ID_STCLIPRC: nCtrlID=IDC_STATIC_CLIPRECT;break;
			case ID_BNCYCLE: nCtrlID=IDC_CHECK_CYCLE;break;
			case ID_INVF:			nCtrlID=IDC_STATIC_INV;break;
			default: nCtrlID=0;break;
			}
			if(nCtrlID)
			{
				GetDlgItem(nCtrlID)->GetWindowRect(&m_rcCtrl[i]);
				ScreenToClient(&m_rcCtrl[i]);
			}
		}

		for (i=0;i<DLG_NUM_STATIC_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_BNFDEL: nCtrlID=IDC_CHECK_DEL;break;
			case ID_BNFUN:	nCtrlID=IDC_CHECK_UNMARKED;break;
			case ID_BNFGD:	nCtrlID=IDC_CHECK_GOOD;break;
			case ID_BNFBD:	nCtrlID=IDC_CHECK_BAD;break;
			case ID_BNFGT:	nCtrlID=IDC_CHECK_GREAT;break;
			case ID_BNFCM:	nCtrlID=IDC_CHECK_COMMITTED;break;
			case ID_BNFPL:	nCtrlID=IDC_CHECK_PLAYED;break;
			case ID_BNFPLV: nCtrlID=IDC_EDIT_TIMESPLAYED;break;
			case ID_BNFCH:	nCtrlID=IDC_CHECK_LENGTH;break;
			case ID_BNFCHV: nCtrlID=IDC_EDIT_NUMCHARS;break;
			case ID_BNFTM:	nCtrlID=IDC_CHECK_AGE;break;
			case ID_BNFTMV:	nCtrlID=IDC_EDIT_MINS;break;
			default: nCtrlID=0;break;
			}
			if(nCtrlID)
			{
				GetDlgItem(nCtrlID)->GetWindowRect(&m_rcCtrlStatic[i]);
				ScreenToClient(&m_rcCtrlStatic[i]);
			}
		}

		m_bVis=TRUE;
		m_bNewSizeInit=FALSE;
	}
}

void CWheelDlg::OnSize(UINT nType, int cx, int cy) 
{
	int nCtrlID = 0;

	int dx,dy;
	CRect rcList;
	dx=m_rcDlg.right-cx;
	dy=m_rcDlg.bottom-cy;

	if((m_bVis)&&(cx>0)&&(cy>0))
	{
		CWnd* pWnd;

		pWnd=GetDlgItem(IDC_BUTTON_TICKER);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_BNTKR].left-dx,
			m_rcCtrl[ID_BNTKR].top,
			0,0,
//			m_rcCtrl[ID_BNTKR].Width()-dx,  // goes with right edge
//			m_rcCtrl[ID_BNTKR].Height(),//-dy,  // goes with bottom edge, 
			SWP_NOZORDER|SWP_NOSIZE
			);

		pWnd=GetDlgItem(IDC_RICHEDIT_VIEW);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			m_rcCtrl[ID_REPREV].left,
			m_rcCtrl[ID_REPREV].top-dy,
			(m_rcCtrl[ID_REPREV].right-m_rcCtrl[ID_REPREV].left)-dx, 
			(m_rcCtrl[ID_REPREV].bottom-m_rcCtrl[ID_REPREV].top),  
			SWP_NOZORDER
			);

		pWnd=GetDlgItem(IDC_STATIC_INV);
		if(pWnd!=NULL)pWnd->SetWindowPos( 
			&wndTop,
			0, 0, 
			(m_rcCtrl[ID_INVF].right-m_rcCtrl[ID_INVF].left)-dx, 
			(m_rcCtrl[ID_INVF].bottom-m_rcCtrl[ID_INVF].top)-dy,  
			SWP_NOZORDER|SWP_NOMOVE
			);


		int nRcBottom=0, nRcTop=0;

		GetWindowRect(&g_data.m_rcLimit);
		nRcTop=g_data.m_rcLimit.top; nRcBottom=g_data.m_rcLimit.bottom;
	
		GetDlgItem(IDC_RICHEDIT_VIEW)->GetWindowRect(&g_data.m_rcLimit);
		g_data.m_rcLimit.top = nRcTop;  g_data.m_rcLimit.bottom = nRcBottom;
		g_data.m_rcLimit.left+= (30);  g_data.m_rcLimit.right -= (30);

		ScreenToClient(&g_data.m_rcLimit);

		if(m_rcCtrl[ID_STHWIPE].left-dx<g_data.m_rcLimit.left)
		{
			OffsetCtrlRects((m_rcCtrl[ID_STHWIPE].left-dx)-g_data.m_rcLimit.left, 0);
		}
		else
		if(m_rcCtrl[ID_STHWIPE].left-dx>g_data.m_rcLimit.right)
		{
			OffsetCtrlRects(g_data.m_rcLimit.right-(m_rcCtrl[ID_STHWIPE].left-dx), 0);
		}

		SetWidths();
/*
		for (int i=0;i<DLG_NUM_MOVING_CONTROLS;i++)  
		{
			switch(i)
			{
			case ID_BNTKR: nCtrlID=IDC_BUTTON_TICKER;break;
			case ID_STHWIPE: nCtrlID=IDC_STATIC_HWIPE;break;
			case ID_STVWIPE: nCtrlID=IDC_STATIC_VWIPE;break;
			case ID_STHWIPE2: nCtrlID=IDC_STATIC_HWIPE2;break;
			case ID_REFEED: nCtrlID=IDC_RICHEDIT_DOWNLOADSTATUS;break;
			case ID_RETICK: nCtrlID=IDC_RICHEDIT_TICKERSTATUS;break;
			case ID_REPREV: nCtrlID=IDC_RICHEDIT_VIEW;break;
			case ID_LCEFEED: nCtrlID=IDC_LIST_DATASTACK;break;
			case ID_BNCOMMIT: nCtrlID=IDC_BUTTON_COMMIT;break;
			case ID_BNCOMMIT2: nCtrlID=IDC_BUTTON_COMMIT2;break;
			case ID_LCEDEFLT: nCtrlID=IDC_LIST_DEFAULTSTACK;break;
			case ID_BNCYCLE: nCtrlID=IDC_CHECK_CYCLE;break;
			case ID_LCESTACK: nCtrlID=IDC_LIST_PLAYSTACK;break;
			default: nCtrlID=0;break;
			}
			if(nCtrlID) GetDlgItem(nCtrlID)->Invalidate();
		}
*/
	}		
}

BOOL CWheelDlg::OnToolTipNotify( UINT id, NMHDR* pNMHDR, LRESULT* pResult )
{
	AfxMessageBox("TTN");
	TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
	UINT nID =pNMHDR->idFrom;
	if (pTTT->uFlags & TTF_IDISHWND)
	{
		// idFrom is actually the HWND of the tool
		nID = ::GetDlgCtrlID((HWND)nID);
		if(nID)
		{
			switch(nID)
			{

			case IDC_BUTTON_COMMIT:		{	_snprintf(pTTT->szText, 79, "Queue text message for ticker playout");	} break;
			case IDC_BUTTON_COMMIT2:	{ _snprintf(pTTT->szText, 79, "Queue text message for default ticker playout");	} break;
			case IDC_CHECK_FILTER:	{ _snprintf(pTTT->szText, 79, "Filter out messages based on attributes");	} break;
			case IDC_BUTTON_TICKER:	{ _snprintf(pTTT->szText, 79, "%s the ticker %s", 
																((g_dlldata.thread[WHEEL_TICKER]->nThreadState&WHEEL_TICKER_STARTED)?"Take":"out"),
																((g_dlldata.thread[WHEEL_TICKER]->nThreadState&WHEEL_TICKER_STARTED)?"Bring":"in")
																);	} break;
			default: break;
			}

			return TRUE;
		}
	}
	return FALSE;
}



BOOL CWheelDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
//AfxMessageBox("FF");

	if(!g_settings.m_bIsServer)
	{
		g_psmut = new CSmutFilter;

		if(g_ptabmsgr==NULL)
		{
			g_ptabmsgr = new CMessager;
			if(g_ptabmsgr) g_ptabmsgr->AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, "log1", "TextToAir|YD||1|");
		}

	}
	EnableToolTips(TRUE);

	m_bUseSysDefaultColors = TRUE;  // if you want to use default sytsem dialog BG color (COLOR_3DFACE)
	// TODO: Add extra initialization here
	// g_settings.Settings(true);  //moved this to init instance - tabluator dll may not init the dialog!
	g_data.m_hcurCommit=AfxGetApp()->LoadCursor(IDC_CURSOR_COMMIT);
	g_data.m_hcurUncommit=AfxGetApp()->LoadCursor(IDC_CURSOR_UNCOMMIT);


	m_lceTicker.SubclassDlgItem(IDC_LIST_PLAYSTACK, this);
	m_lceTicker.SetHeight(16);
	m_lceTicker.SetImages(IDB_BITMAP_STACKICONS, 38, RGB(255,255,255)); 

	m_lceDefault.SubclassDlgItem(IDC_LIST_DEFAULTSTACK, this);
	m_lceDefault.SetHeight(16);
	m_lceDefault.SetImages(IDB_BITMAP_STACKICONS, 38, RGB(255,255,255)); 

	m_lceData.SubclassDlgItem(IDC_LIST_DATASTACK, this);
	m_lceData.SetHeight(16);
	m_lceData.SetImages(IDB_BITMAP_DRICONS, 26, RGB(255,255,255)); 
//AfxMessageBox("FF1");

	m_editexInfo.SetRichEdit((CRichEditCtrl* )GetDlgItem(IDC_RICHEDIT_VIEW));

	m_editexData.SetRichEdit((CRichEditCtrl* )GetDlgItem(IDC_RICHEDIT_DOWNLOADSTATUS));
	m_editexTicker.SetRichEdit((CRichEditCtrl* )GetDlgItem(IDC_RICHEDIT_TICKERSTATUS));

	m_fsFontData.szFace="Arial";//"Square721 Cn BT";
	m_fsFontData.nPoints= 110;
	m_fsFontData.nStyle= FSS_BOLD|FSS_ITALIC;
	m_fsFontData.nEffects= FSE_NONE;
	m_fsFontData.crBG= RGB(0,0,0);
	m_fsFontData.crText= RGB(255,255,0);

	m_fsFontTicker.szFace="Arial";//"Square721 Cn BT";
	m_fsFontTicker.nPoints= 110;
	m_fsFontTicker.nStyle= FSS_BOLD|FSS_ITALIC;
	m_fsFontTicker.nEffects= FSE_NONE;
	m_fsFontTicker.crBG= RGB(0,0,0);
	m_fsFontTicker.crText= RGB(255,255,0);


	CFont* font;
	font = GetFont();
	LOGFONT lf;
	font->GetLogFont(&lf);
//AfxMessageBox("FF1x");

	m_fsFontPreview.szFace.Format("%s", lf.lfFaceName);
	m_fsFontPreview.nPoints= 85;
	m_fsFontPreview.nStyle= FSS_REGULAR;
	m_fsFontPreview.nEffects= FSE_NONE;
//	m_fsFontPreview.crBG= GetSysColor(COLOR_3DFACE);
//	m_fsFontPreview.crText= GetSysColor(COLOR_BTNTEXT);
	m_fsFontPreview.crBG= RGB(255,255,255);
	m_fsFontPreview.crText= RGB(0,0,0);
//AfxMessageBox("FF1x1");

	m_editexInfo.SetBackgroundColor( FALSE, m_fsFontPreview.crBG );
	m_editexInfo.SetFont( m_fsFontPreview, 0,-1);
	m_editexInfo.GetWnd()->SetWindowText( "Information on selected item.");

	m_editexData.SetBackgroundColor( FALSE, m_fsFontData.crBG );
	m_editexData.SetFont( m_fsFontData, 0,-1);
	m_editexData.GetWnd()->SetWindowText( "Initializing...");

	m_editexTicker.SetBackgroundColor( FALSE, m_fsFontTicker.crBG );
	m_editexTicker.SetFont( m_fsFontTicker, 0,-1);
	m_editexTicker.GetWnd()->SetWindowText( "Initializing...");
//AfxMessageBox("FF1x2");

//AfxMessageBox("FF2");

	// Load all bitmaps, store handles
	CBitmap bmp;
/*
#define INS				0
#define REM				1
#define RELOAD		2
#define BAD				3
#define UNMARKED	4
#define GOOD			5
#define GREAT			6
#define COMMITTED	7
#define PLAYED		8
#define LENGTH		9
#define AGE				10

*/

  bmp.LoadBitmap(IDB_BITMAP_RELOAD);
	m_hbmp[RELOAD] = m_bmp.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmp.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BN_INSERT);
	m_hbmp[INS] = m_bmp.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmp.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BN_REMOVE);
	m_hbmp[REM] = m_bmp.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmp.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BN_X);
	m_hbmp[BAD] = m_bmp.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmp.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BN_Q);
	m_hbmp[UNMARKED] = m_bmp.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmp.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BN_CHK);
	m_hbmp[GOOD] = m_bmp.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmp.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BN_GREAT);
	m_hbmp[GREAT] = m_bmp.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmp.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BN_COMMITTED);
	m_hbmp[COMMITTED] = m_bmp.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmp.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BN_PLAYED);
	m_hbmp[PLAYED] = m_bmp.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmp.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BN_LENGTH);
	m_hbmp[LENGTH] = m_bmp.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmp.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));
	bmp.DeleteObject();
  bmp.LoadBitmap(IDB_BN_AGE);
	m_hbmp[AGE] = m_bmp.ButtonBitmap(GetDC()->GetSafeHdc(), HBITMAP(bmp), m_bmp.GetTopLeftColor(GetDC()->GetSafeHdc(), HBITMAP(bmp)));

	bmp.DeleteObject();

	((CButton*)GetDlgItem(IDC_BUTTON_COMMIT))->SetBitmap(m_hbmp[INS]);
	((CButton*)GetDlgItem(IDC_BUTTON_COMMIT2))->SetBitmap(m_hbmp[INS]);


	((CButton*)GetDlgItem(IDC_CHECK_AGE))->SetBitmap(m_hbmp[AGE]);
	((CButton*)GetDlgItem(IDC_CHECK_BAD))->SetBitmap(m_hbmp[BAD]);
	((CButton*)GetDlgItem(IDC_CHECK_COMMITTED))->SetBitmap(m_hbmp[COMMITTED]);
	((CButton*)GetDlgItem(IDC_CHECK_GOOD))->SetBitmap(m_hbmp[GOOD]);
	((CButton*)GetDlgItem(IDC_CHECK_GREAT))->SetBitmap(m_hbmp[GREAT]);
	((CButton*)GetDlgItem(IDC_CHECK_LENGTH))->SetBitmap(m_hbmp[LENGTH]);
	((CButton*)GetDlgItem(IDC_CHECK_PLAYED))->SetBitmap(m_hbmp[PLAYED]);
	((CButton*)GetDlgItem(IDC_CHECK_UNMARKED))->SetBitmap(m_hbmp[UNMARKED]);


	GetDlgItem(IDC_BUTTON_COMMIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON_COMMIT2)->EnableWindow(FALSE);
	g_data.m_bCommitEnabled=FALSE;

  CRect rcList;
  m_lceTicker.GetWindowRect(&rcList);
	
  if(m_lceTicker.InsertColumn(
    0, 
    "Ticker Playout", 
    LVCFMT_LEFT, 
    rcList.Width()-20,
    -1
    ) <0) AfxMessageBox("could not add playout column");

  m_lceDefault.GetWindowRect(&rcList);
	
  if(m_lceDefault.InsertColumn(
    0, 
    "Default Messages", 
    LVCFMT_LEFT, 
    rcList.Width()-20,
    -1
    ) <0) AfxMessageBox("could not add default messages column");

  m_lceData.GetWindowRect(&rcList);
	
  if(m_lceData.InsertColumn(
    0, 
    "Available Items", 
    LVCFMT_LEFT, 
    rcList.Width()-20,
    -1
    ) <0) AfxMessageBox("could not add data column");

//AfxMessageBox("FF3");

//	m_lce.InsertItem(0,"debug text");
//	CString x; x.Format("width %d",rcList.Width());
//	AfxMessageBox(x);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CWheelDlg::OnExit()
{
	// add cleanup here if necesary;
	if((g_dlldata.thread)&&(g_dlldata.nNumThreads>WHEEL_UI)&&(g_dlldata.thread[WHEEL_UI]))
	{
		g_dlldata.thread[WHEEL_UI]->bKillThread=true;

		int nWait=clock()+2000;
		while((g_dlldata.thread[WHEEL_UI]->bThreadStarted)&&(clock()<nWait))
		{
			Sleep(100);
		}
	}

	if(g_psmut)
	{
		delete g_psmut;
		g_psmut = NULL;
	}

/*
	while
		(
			(g_dlldata.thread[WHEEL_DATA]->bThreadStarted)
		||(g_dlldata.thread[WHEEL_TICKER]->bThreadStarted)
		||(g_dlldata.thread[WHEEL_UI]->bThreadStarted)
		)
	{
		FILE* fp = fopen("waiting.txt", "ab");
		if(fp)
		{
			fprintf(fp,"%d %d %d\r\n",			
				(g_dlldata.thread[WHEEL_DATA]->bThreadStarted),
				(g_dlldata.thread[WHEEL_TICKER]->bThreadStarted),
				(g_dlldata.thread[WHEEL_UI]->bThreadStarted));
			fclose(fp);
		}
		Sleep(5);
	}
*/
	// save settings;
//	g_settings.Settings(false);

/*	
	EnterCriticalSection(&m_critData);
	EnterCriticalSection(&m_critDefault);
	EnterCriticalSection(&m_critTicker);

/*
	DeleteCriticalSection(&m_critData);
	DeleteCriticalSection(&m_critDefault);
	DeleteCriticalSection(&m_critTicker);
*/
	CDialog::OnCancel();
}
void CWheelDlg::SetWidths()
{
	CRect rc;
	int dx,dy;

	GetClientRect(&rc);

	dx=m_rcDlg.right-rc.Width();
	dy=m_rcDlg.bottom-rc.Height();

	CWnd* pWnd;

	pWnd=GetDlgItem(IDC_STATIC_HWIPE);
	if(pWnd!=NULL)
	{
		CRect rc;
		pWnd->GetWindowRect(&rc);
		ScreenToClient(&rc);
		pWnd->SetWindowPos( 
		&wndTop,
		m_rcCtrl[ID_STHWIPE].left-dx, 
		m_rcCtrl[ID_STHWIPE].top,  
		(m_rcCtrl[ID_STHWIPE].right-m_rcCtrl[ID_STHWIPE].left),
		(m_rcCtrl[ID_STHWIPE].bottom-m_rcCtrl[ID_STHWIPE].top),  
		SWP_NOZORDER
		);
	}
	pWnd=GetDlgItem(IDC_STATIC_HWIPE2);
	if(pWnd!=NULL)
	{
		CRect rc;
		pWnd->GetWindowRect(&rc);
		ScreenToClient(&rc);
		pWnd->SetWindowPos( 
		&wndTop,
		m_rcCtrl[ID_STHWIPE2].left-dx, 
		m_rcCtrl[ID_STHWIPE2].top,  
		(m_rcCtrl[ID_STHWIPE2].right-m_rcCtrl[ID_STHWIPE2].left),
		(m_rcCtrl[ID_STHWIPE2].bottom-m_rcCtrl[ID_STHWIPE2].top)-dy,  
		SWP_NOZORDER
		);
	}

	pWnd=GetDlgItem(IDC_LIST_DATASTACK);
	if(pWnd!=NULL)pWnd->SetWindowPos( 
		&wndTop,
		0,0,
		(m_rcCtrl[ID_LCEFEED].right-m_rcCtrl[ID_LCEFEED].left)-dx,
		(m_rcCtrl[ID_LCEFEED].bottom-m_rcCtrl[ID_LCEFEED].top)-dy,  
		SWP_NOZORDER|SWP_NOMOVE
		);

	pWnd=GetDlgItem(IDC_BUTTON_COMMIT);
	if(pWnd!=NULL)pWnd->SetWindowPos( 
		&wndTop,
		m_rcCtrl[ID_BNCOMMIT].left-dx,
		m_rcCtrl[ID_BNCOMMIT].top-dy,  
		0,0,
		SWP_NOZORDER|SWP_NOSIZE
		);
	pWnd=GetDlgItem(IDC_BUTTON_TICKER);
	if(pWnd!=NULL)pWnd->SetWindowPos( 
		&wndTop,
		m_rcCtrl[ID_BNTKR].left-dx,
		m_rcCtrl[ID_BNTKR].top,  
		0,0,
		SWP_NOZORDER|SWP_NOSIZE
		);
	pWnd=GetDlgItem(IDC_BUTTON_COMMIT2);
	if(pWnd!=NULL)pWnd->SetWindowPos( 
		&wndTop,
		m_rcCtrl[ID_BNCOMMIT2].left-dx,
		m_rcCtrl[ID_BNCOMMIT2].top,  
		0,0,
		SWP_NOZORDER|SWP_NOSIZE
		);
	pWnd=GetDlgItem(IDC_RICHEDIT_VIEW);
	if(pWnd!=NULL)pWnd->SetWindowPos( 
		&wndTop,
		m_rcCtrl[ID_REPREV].left, 
		m_rcCtrl[ID_REPREV].top-dy,  
		(m_rcCtrl[ID_REPREV].right-m_rcCtrl[ID_REPREV].left)-dx, 
		(m_rcCtrl[ID_REPREV].bottom-m_rcCtrl[ID_REPREV].top),  
		SWP_NOZORDER
		);
	
	pWnd=GetDlgItem(IDC_RICHEDIT_DOWNLOADSTATUS);
	if(pWnd!=NULL)pWnd->SetWindowPos( 
		&wndTop,
		m_rcCtrl[ID_REFEED].left, 
		m_rcCtrl[ID_REFEED].top,  
		(m_rcCtrl[ID_REFEED].right-m_rcCtrl[ID_REFEED].left)-dx, 
		(m_rcCtrl[ID_REFEED].bottom-m_rcCtrl[ID_REFEED].top),  
		SWP_NOZORDER
		);
	
	pWnd=GetDlgItem(IDC_RICHEDIT_TICKERSTATUS);
	if(pWnd!=NULL)pWnd->SetWindowPos( 
		&wndTop,
		m_rcCtrl[ID_RETICK].left-dx, 
		m_rcCtrl[ID_RETICK].top,  
		(m_rcCtrl[ID_RETICK].right-m_rcCtrl[ID_RETICK].left), 
		(m_rcCtrl[ID_RETICK].bottom-m_rcCtrl[ID_RETICK].top),  
		SWP_NOZORDER
		);

	pWnd=GetDlgItem(IDC_LIST_DATASTACK);
	if(pWnd!=NULL)pWnd->SetWindowPos( 
		&wndTop,
		m_rcCtrl[ID_LCEFEED].left,
		m_rcCtrl[ID_LCEFEED].top,  
		(m_rcCtrl[ID_LCEFEED].right-m_rcCtrl[ID_LCEFEED].left)-dx,
		(m_rcCtrl[ID_LCEFEED].bottom-m_rcCtrl[ID_LCEFEED].top)-dy,  
		SWP_NOZORDER
		);
	pWnd=GetDlgItem(IDC_STATIC_VWIPE);
	if(pWnd!=NULL)pWnd->SetWindowPos( 
		&wndTop,
		m_rcCtrl[ID_STVWIPE].left-dx,
		m_rcCtrl[ID_STVWIPE].top,  
		(m_rcCtrl[ID_STVWIPE].right-m_rcCtrl[ID_STVWIPE].left),
		(m_rcCtrl[ID_STVWIPE].bottom-m_rcCtrl[ID_STVWIPE].top),  
		SWP_NOZORDER
		);

	pWnd=GetDlgItem(IDC_STATIC_CLIPRECT);
	if(pWnd!=NULL)pWnd->SetWindowPos( 
		&wndTop,
		m_rcCtrl[ID_STCLIPRC].left-dx,
		m_rcCtrl[ID_STCLIPRC].top,  
		(m_rcCtrl[ID_STCLIPRC].right-m_rcCtrl[ID_STCLIPRC].left),
		(m_rcCtrl[ID_STCLIPRC].bottom-m_rcCtrl[ID_STCLIPRC].top),  
		SWP_NOZORDER
		);
	pWnd=GetDlgItem(IDC_LIST_PLAYSTACK);
	if(pWnd!=NULL)pWnd->SetWindowPos( 
		&wndTop,
		m_rcCtrl[ID_LCESTACK].left-dx,
		m_rcCtrl[ID_LCESTACK].top,  
		(m_rcCtrl[ID_LCESTACK].right-m_rcCtrl[ID_LCESTACK].left),
		(m_rcCtrl[ID_LCESTACK].bottom-m_rcCtrl[ID_LCESTACK].top)-dy,  
		SWP_NOZORDER
		);
	pWnd=GetDlgItem(IDC_LIST_DEFAULTSTACK);
	if(pWnd!=NULL)pWnd->SetWindowPos( 
		&wndTop,
		m_rcCtrl[ID_LCEDEFLT].left-dx,
		m_rcCtrl[ID_LCEDEFLT].top,  
		(m_rcCtrl[ID_LCEDEFLT].right-m_rcCtrl[ID_LCEDEFLT].left),
		(m_rcCtrl[ID_LCEDEFLT].bottom-m_rcCtrl[ID_LCEDEFLT].top),  
		SWP_NOZORDER
		);

	pWnd=GetDlgItem(IDC_CHECK_CYCLE);
	if(pWnd!=NULL)pWnd->SetWindowPos( 
		&wndTop,
		m_rcCtrl[ID_BNCYCLE].left-dx,
		m_rcCtrl[ID_BNCYCLE].top,  
		0,0,  
		SWP_NOZORDER|SWP_NOSIZE
		);


	if(m_lceTicker.GetColumnWidth(0)<(m_rcCtrl[ID_LCESTACK].right-m_rcCtrl[ID_LCESTACK].left)-dx)
		m_lceTicker.SetColumnWidth( 0, (m_rcCtrl[ID_LCESTACK].right-m_rcCtrl[ID_LCESTACK].left)-dx);
	
	if(m_lceDefault.GetColumnWidth(0)<(m_rcCtrl[ID_LCEDEFLT].right-m_rcCtrl[ID_LCEDEFLT].left)-dx)
		m_lceDefault.SetColumnWidth( 0, (m_rcCtrl[ID_LCEDEFLT].right-m_rcCtrl[ID_LCEDEFLT].left)-dx);

	
	if(m_lceData.GetColumnWidth(0)<(m_rcCtrl[ID_LCEFEED].right-m_rcCtrl[ID_LCEFEED].left)-dx)
		m_lceData.SetColumnWidth( 0, (m_rcCtrl[ID_LCEFEED].right-m_rcCtrl[ID_LCEFEED].left)-dx);

	// disable buttons....
	DisableButtons(dx);
}

void CWheelDlg::DisableButtons(int dx)
{
	// disable buttons....
	int nCtrlID=0;
	for (int i=0;i<DLG_NUM_STATIC_CONTROLS;i++)  
	{
		switch(i)
		{
//		case ID_BNFDEL: nCtrlID=IDC_CHECK_DEL;break;
		case ID_BNFUN:	nCtrlID=IDC_CHECK_UNMARKED;break;
		case ID_BNFGD:	nCtrlID=IDC_CHECK_GOOD;break;
		case ID_BNFBD:	nCtrlID=IDC_CHECK_BAD;break;
		case ID_BNFGT:	nCtrlID=IDC_CHECK_GREAT;break;
		case ID_BNFCM:	nCtrlID=IDC_CHECK_COMMITTED;break;
		case ID_BNFPL:	nCtrlID=IDC_CHECK_PLAYED;break;
		case ID_BNFPLV: nCtrlID=IDC_EDIT_TIMESPLAYED;break;
		case ID_BNFCH:	nCtrlID=IDC_CHECK_LENGTH;break;
		case ID_BNFCHV: nCtrlID=IDC_EDIT_NUMCHARS;break;
		case ID_BNFTM:	nCtrlID=IDC_CHECK_AGE;break;
		case ID_BNFTMV:	nCtrlID=IDC_EDIT_MINS;break;
		default: nCtrlID=0;break;
		}
		if(nCtrlID)
		{
			if((m_rcCtrl[ID_STCLIPRC].left-dx > m_rcCtrlStatic[i].right)&&(m_bFilter))
			{
				GetDlgItem(nCtrlID)->EnableWindow(TRUE);
				GetDlgItem(nCtrlID)->ShowWindow(SW_SHOW);
			}
			else
			{
				GetDlgItem(nCtrlID)->EnableWindow(FALSE);
				GetDlgItem(nCtrlID)->ShowWindow(SW_HIDE);
			}
		}
	}
}

void CWheelDlg::OffsetCtrlRects(int dx, int dy)
{
																		m_rcCtrl[ID_REFEED].right-=dx;
																		m_rcCtrl[ID_LCEFEED].right-=dx;

	m_rcCtrl[ID_BNCOMMIT].left-=dx;		m_rcCtrl[ID_BNCOMMIT].right-=dx;
	m_rcCtrl[ID_BNCOMMIT2].left-=dx;	m_rcCtrl[ID_BNCOMMIT2].right-=dx;
	m_rcCtrl[ID_STHWIPE].left-=dx;		m_rcCtrl[ID_STHWIPE].right-=dx;
	m_rcCtrl[ID_STHWIPE2].left-=dx;		m_rcCtrl[ID_STHWIPE2].right-=dx;
	m_rcCtrl[ID_LCEDEFLT].left-=dx;
	m_rcCtrl[ID_LCESTACK].left-=dx;
	m_rcCtrl[ID_STVWIPE].left-=dx;
	m_rcCtrl[ID_RETICK].left-=dx;
	m_rcCtrl[ID_STCLIPRC].left-=dx;

	m_rcCtrl[ID_BNCOMMIT2].top-=dy;		m_rcCtrl[ID_BNCOMMIT2].bottom-=dy;
	m_rcCtrl[ID_STVWIPE].top-=dy;			m_rcCtrl[ID_STVWIPE].bottom-=dy;
																		m_rcCtrl[ID_LCEDEFLT].bottom-=dy;
	m_rcCtrl[ID_LCESTACK].top-=dy;
																		m_rcCtrl[ID_STHWIPE].bottom-=dy;
	m_rcCtrl[ID_STHWIPE2].top-=dy;

	m_rcCtrl[ID_BNCYCLE].top-=dy;		m_rcCtrl[ID_BNCYCLE].bottom-=dy;
}


void CWheelDlg::OnMouseMove(UINT nFlags, CPoint point) 
{
	HCURSOR hCur = GetCursor();
//	CString foo; foo.Format("%d: %d,%d", GetTickCount(), point.x, point.y);
//	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(foo);

	if((!g_data.m_bDraggingCommit)&&(!g_data.m_bDraggingUncommit)) // if we're dragging we want to deal with cursor for divider only.
	{
		m_nWindowLast=-1;
		m_nPointerLast = -2;
		if((nFlags&MK_LBUTTON)&&((g_data.m_bDraggingHDivider)||(g_data.m_bDraggingVDivider)))
		{
			if(g_data.m_hcurSize!=hCur) SetCursor(g_data.m_hcurSize);

			if(!g_core.IsPointInRect( point,  g_data.m_rcLimit))
			{
				if(point.x>g_data.m_rcLimit.right) point.x=g_data.m_rcLimit.right;
				if(point.x<g_data.m_rcLimit.left) point.x=g_data.m_rcLimit.left;
//				CDialog::OnMouseMove(nFlags, point);
//				return;
			}

			int dx = g_data.m_ptStartDivDrag.x-point.x;
			int dy = g_data.m_ptStartDivDrag.y-point.y;
			if(g_data.m_bDraggingHDivider)	OffsetCtrlRects(dx, 0);
			if(g_data.m_bDraggingVDivider)	OffsetCtrlRects(0, dy);
			SetWidths();
			
/*
			int nCtrlID;
			for (int i=0;i<DLG_NUM_MOVING_CONTROLS;i++)  
			{
				switch(i)
				{
				case ID_BNTKR: nCtrlID=IDC_BUTTON_TICKER;break;
				case ID_STHWIPE: nCtrlID=IDC_STATIC_HWIPE;break;
				case ID_STVWIPE: nCtrlID=IDC_STATIC_VWIPE;break;
				case ID_STHWIPE2: nCtrlID=IDC_STATIC_HWIPE2;break;
				case ID_REFEED: nCtrlID=IDC_RICHEDIT_DOWNLOADSTATUS;break;
				case ID_RETICK: nCtrlID=IDC_RICHEDIT_TICKERSTATUS;break;
				case ID_REPREV: nCtrlID=IDC_RICHEDIT_VIEW;break;
				case ID_LCEFEED: nCtrlID=IDC_LIST_DATASTACK;break;
				case ID_BNCOMMIT: nCtrlID=IDC_BUTTON_COMMIT;break;
				case ID_BNCOMMIT2: nCtrlID=IDC_BUTTON_COMMIT2;break;
				case ID_LCEDEFLT: nCtrlID=IDC_LIST_DEFAULTSTACK;break;
				case ID_LCESTACK: nCtrlID=IDC_LIST_PLAYSTACK;break;
				case ID_BNCYCLE: nCtrlID=IDC_CHECK_CYCLE;break;
//				case ID_INVF:			nCtrlID=IDC_LIST_PLAYSTACK;break;
				case ID_STCLIPRC: nCtrlID=IDC_STATIC_CLIPRECT;break;
				default: nCtrlID=0;break;
				}

				if(nCtrlID) GetDlgItem(nCtrlID)->Invalidate();
			}
*/
			if(g_data.m_ptStartDivDrag != point)
			{
//				GetDlgItem(IDC_STATIC_INV)->ShowWindow(SW_SHOW);
//				GetDlgItem(IDC_STATIC_INV)->ShowWindow(SW_HIDE);

				Invalidate();
			
				g_data.m_ptStartDivDrag.x=point.x;
				g_data.m_ptStartDivDrag.y=point.y;
			}
		}
		else
		{
			CPoint scrnpt=point;
			ClientToScreen(&scrnpt);
			CWnd* pWnd;
			CWnd* pWnd2;
			pWnd = GetDlgItem(IDC_STATIC_HWIPE);
			pWnd2 = GetDlgItem(IDC_STATIC_HWIPE2);
			BOOL bWipe = FALSE;
			if((pWnd)&&(pWnd2))
			{
				if((g_core.IsPointInWnd(scrnpt, pWnd))||(g_core.IsPointInWnd(scrnpt, pWnd2))) // mouseover the slide tab
				{
					if(g_data.m_hcurSize!=hCur) SetCursor(g_data.m_hcurSize);
					bWipe = TRUE;
				}
			}
			if(!bWipe)
			{
				pWnd = GetDlgItem(IDC_STATIC_VWIPE);
				if(pWnd)
				{
					if(g_core.IsPointInWnd(scrnpt, pWnd)) // mouseover the slide tab
					{
						if(g_data.m_hcurNS!=hCur) SetCursor(g_data.m_hcurNS);
						bWipe = TRUE;
					}
				}
			}
			if(!bWipe)
			{
//					GetDlgItem(IDC_RICHEDIT_VIEW)->SetWindowText("out");
				if(g_data.m_hcurArrow!=hCur) SetCursor(g_data.m_hcurArrow);
			}
		}
	}
	else // we're dragging a text item...
	{
		CPoint scrnpt=point;
		ClientToScreen(&scrnpt);

		if((g_data.m_bDraggingCommit)||(g_data.m_bDraggingUncommit))
		{
			CRect rcBounds;
			GetDlgItem(IDC_LIST_PLAYSTACK)->GetWindowRect(&rcBounds);
			// if in the playstack, we need to illuminate the insertion point




//				m_lceDefault.SetItemText(0,0, "false");

			if((g_core.IsPointInWnd(scrnpt, GetDlgItem(IDC_LIST_PLAYSTACK))) 
				&&(m_pointLast!=point))
			{	
				m_pointLast = point;
				CRect rcList;

//				m_lceDefault.SetItemText(0,0,"true");

				// undo any yellow highlight in the default
				int nCount = m_lceDefault.GetItemCount();
				for (int nItem=0; nItem < nCount; nItem++)
				{
					if ((BOOL)m_lceDefault.GetItemData(nItem,13))
					{
/*
						if(m_lceDefault.GetItemData(nItem,1)&WHEEL_MSG_MARK_DEL)
						{
 							m_lceDefault.SetItemColor(nItem, 0, LCEX_BGCLR, RGB(160,160,160),	TRUE);// unknown songs with RGB(128,128,128) show up OK
						}
						else
						{
*/
						m_lceDefault.SetItemColor(nItem, 0, LCEX_BGCLR,	
														(COLORREF)m_lceDefault.GetItemData(nItem,3), //BG color - show color
														TRUE);
//						}
						m_lceDefault.SetItemData(nItem,0,13);
						m_lceDefault.RedrawItems(nItem,nItem);
					}
				}
				m_lceDefault.UpdateWindow();

				nCount = m_lceTicker.GetItemCount();
				int nSel = m_lceTicker.GetNextItem( -1,  LVNI_SELECTED );
				CPoint pt=point;
				ClientToScreen(&pt);
				m_lceTicker.ScreenToClient(&pt);
				int nPointerLast=-1;
				int nHitTest = m_lceTicker.HitTest(pt);
				
				if(m_nWindowLast!=ID_LCESTACK)	m_nPointerLast = -2;
				m_nWindowLast=ID_LCESTACK;

				if( nHitTest != m_nPointerLast)
				{

					BOOL bPlacementOK = TRUE;
					BOOL bOnItems = FALSE;

/*
					if ((BOOL)m_lceTicker.GetItemData(nCount-1,13))
					{
						m_lceTicker.SetItemColor(nCount-1, 0, LCEX_BGCLR,	
														GetSysColor(COLOR_WINDOW), //blank the final item
														TRUE);
						m_lceTicker.SetItemData(nCount-1,0,13);
						m_lceTicker.RedrawItems(nCount-1,nCount-1);
					}
*/
					for (int nItem=0; nItem < nCount; nItem++)
					{
						BOOL bTarget = (BOOL)m_lceTicker.GetItemData(nItem,13);
						m_lceTicker.GetItemRect( nItem, rcList, LVIR_BOUNDS);
							
						if((nSel==nItem)&&(!g_data.m_bCommitDefaultSource))
						{
							m_lceTicker.SetItem(nItem, 0, LVIF_STATE, NULL, 0, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED, 0);
							CWheelMessage* pmsg = (CWheelMessage*)m_lceTicker.GetItemDataPtr(nItem,0);
 							if(pmsg)
							{
								m_lceTicker.SetItemBitmap(nItem, 0, m_lceTicker.GetImage(ReturnTickerBitmapIndex(pmsg->m_nStatus, TRUE))); //pointer
							}
							bOnItems = TRUE;
						}
						else
						{
							if (
									(pt.y >= rcList.top) && 
									(pt.y < rcList.bottom)
									)
							{
								nPointerLast = nItem;

								CWheelMessage* pmsg = (CWheelMessage*)m_lceTicker.GetItemDataPtr(nItem,0);

								if((pmsg)&&(g_settings.m_bTickerPlayOnce)&&(pmsg->m_nStatus&(WHEEL_MSG_STATUS_COMMIT|WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_PLAYED|WHEEL_MSG_MARK_DEL)))
								{
									bPlacementOK = FALSE;
									bOnItems = TRUE;
								}
								else
								{
									// determine placement OK
									if(nItem==nCount-1) 
									{
										g_data.m_nCommitInsertBeforeID = -1; // put at end
										g_data.m_dblCommitBeforeIndex = -0.001;
									}
									else
									{
										bOnItems = TRUE;
										g_data.m_nCommitInsertBeforeID = (pmsg?pmsg->m_nID:-1);//m_lceTicker.GetItemData(nItem);
										g_data.m_dblCommitBeforeIndex = (pmsg?pmsg->m_dblIndex:-.001);
									}

									if (!bTarget) // only re-set if it was selected before.
									{
										m_lceTicker.SetItemColor(nItem, 0, LCEX_BGCLR,	
																	g_settings.m_crCommitted, //BG colors for drop selection
																		TRUE);
										m_lceTicker.SetItemData(nItem,1,13);
										m_lceTicker.RedrawItems(nItem,nItem);
									}
								}
							}
							else// if(m_nPointerLast == nItem)
							{

								if (bTarget) // only re-set if it was selected before.
								{
									m_lceTicker.SetItemColor(nItem, 0, LCEX_BGCLR,	
																	(COLORREF)m_lceTicker.GetItemData(nItem,3), //BG color - show color
																	TRUE);
									m_lceTicker.SetItemData(nItem,0,13);
									m_lceTicker.RedrawItems(nItem,nItem);
								}
							}

						}
					}

					if(bPlacementOK)
					{
						if(g_data.m_bDraggingCommit)
						{
							if(g_data.m_hcurCommit!=hCur) SetCursor(g_data.m_hcurCommit);
						}
						if(g_data.m_bDraggingUncommit)
						{
							if(g_data.m_hcurNS!=hCur) SetCursor(g_data.m_hcurNS);
						}
						g_data.m_bCommitEnabled=TRUE;

						if(!bOnItems) // put at end
						{
		//					AfxMessageBox("XXX");
							//bPlacementOK = TRUE;
							g_data.m_nCommitInsertBeforeID = -1;
							g_data.m_dblCommitBeforeIndex = -0.001;
						//	bOnItems = TRUE;

							if(!(BOOL)m_lceTicker.GetItemData(nCount-1,13))
							{
								m_lceTicker.SetItemData(nCount-1,1,13);
								m_lceTicker.SetItemColor(nCount-1, 0, LCEX_BGCLR,	
									g_settings.m_crCommitted, //BG colors for drop selection
									TRUE);
								m_lceTicker.RedrawItems(nCount-1,nCount-1);
							}
						}

					}
					else
					{
						if(g_data.m_hcurNoDrop!=hCur) SetCursor(g_data.m_hcurNoDrop);	
						g_data.m_bCommitEnabled=FALSE;
					}

					m_nPointerLast=nPointerLast;
				}
			
			}
			else  		

			if((g_core.IsPointInWnd(scrnpt, GetDlgItem(IDC_LIST_DEFAULTSTACK)))
				&&(m_pointLast!=point))
			{	
				m_pointLast = point;
				CRect rcList;

				// undo highlight inthe main playstack
				int nCount = m_lceTicker.GetItemCount();
				for (int nItem=0; nItem < nCount; nItem++)
				{
					if ((BOOL)m_lceTicker.GetItemData(nItem,13))
					{
/*
						if(m_lceTicker.GetItemData(nItem,1)&WHEEL_MSG_MARK_DEL)
						{
 							m_lceTicker.SetItemColor(nItem, 0, LCEX_BGCLR, RGB(160,160,160),	TRUE);// unknown songs with RGB(128,128,128) show up OK
						}
						else
						{
*/
						m_lceTicker.SetItemColor(nItem, 0, LCEX_BGCLR,	
														(COLORREF)m_lceTicker.GetItemData(nItem,3), //BG color - show color
														TRUE);
//						}
						m_lceTicker.SetItemData(nItem,0,13);
						m_lceTicker.RedrawItems(nItem,nItem);
					}
				}

				m_lceTicker.UpdateWindow();

				nCount = m_lceDefault.GetItemCount();
				int nSel = m_lceDefault.GetNextItem( -1,  LVNI_SELECTED );
				CPoint pt=point;
				ClientToScreen(&pt);
				m_lceDefault.ScreenToClient(&pt);
				int nPointerLast=-1;
				int nHitTest = m_lceDefault.HitTest(pt);
				
				if(m_nWindowLast!=ID_LCEDEFLT)	m_nPointerLast = -2;
				m_nWindowLast=ID_LCEDEFLT;

				if( nHitTest != m_nPointerLast)
				{

					BOOL bPlacementOK = TRUE;
					BOOL bOnItems = FALSE;

/*
					if ((BOOL)m_lceDefault.GetItemData(nCount-1,13))
					{
						m_lceDefault.SetItemColor(nCount-1, 0, LCEX_BGCLR,	
														GetSysColor(COLOR_WINDOW), //blank the final item
														TRUE);
						m_lceDefault.SetItemData(nCount-1,0,13);
						m_lceDefault.RedrawItems(nCount-1,nCount-1);
					}
*/
					for (int nItem=0; nItem < nCount; nItem++)
					{
						BOOL bTarget = (BOOL)m_lceDefault.GetItemData(nItem,13);
						m_lceDefault.GetItemRect( nItem, rcList, LVIR_BOUNDS);
							
						if((nSel==nItem)&&(!g_data.m_bCommitDefaultSource))
						{
							m_lceDefault.SetItem(nItem, 0, LVIF_STATE, NULL, 0, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED, 0);
							CWheelMessage* pmsg = (CWheelMessage*)m_lceDefault.GetItemDataPtr(nItem,0);
 							if(pmsg)
							{
								m_lceDefault.SetItemBitmap(nItem, 0, m_lceDefault.GetImage(ReturnTickerBitmapIndex(pmsg->m_nStatus, TRUE))); //pointer
							}
							bOnItems = TRUE;
						}
						else
						{
							if (
									(pt.y >= rcList.top) && 
									(pt.y < rcList.bottom)
									)
							{
								nPointerLast = nItem;

								CWheelMessage* pmsg = (CWheelMessage*)m_lceDefault.GetItemDataPtr(nItem,0);

/*
// always allow default.

								if((pmsg)&&(g_settings.m_bTickerPlayOnce)&&(pmsg->m_nStatus&(WHEEL_MSG_STATUS_COMMIT|WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_PLAYED|WHEEL_MSG_MARK_DEL)))
								{
									bPlacementOK = FALSE;
									bOnItems = TRUE;
								}
								else
*/
								{
									// determine placement OK
									if(nItem==nCount-1) 
									{
										g_data.m_nCommitInsertBeforeID = -1; // put at end
										g_data.m_dblCommitBeforeIndex = -0.001;
									}
									else
									{
										bOnItems = TRUE;
										g_data.m_nCommitInsertBeforeID = (pmsg?pmsg->m_nID:-1);//m_lceDefault.GetItemData(nItem);
										g_data.m_dblCommitBeforeIndex = (pmsg?pmsg->m_dblIndex:-.001);
									}

									if (!bTarget) // only re-set if it was selected before.
									{
										m_lceDefault.SetItemColor(nItem, 0, LCEX_BGCLR,	
																	g_settings.m_crCommitted, //BG colors for drop selection
																		TRUE);
										m_lceDefault.SetItemData(nItem,1,13);
										m_lceDefault.RedrawItems(nItem,nItem);
									}
								}
							}
							else// if(m_nPointerLast == nItem)
							{

								if (bTarget) // only re-set if it was selected before.
								{
									m_lceDefault.SetItemColor(nItem, 0, LCEX_BGCLR,	
																	(COLORREF)m_lceDefault.GetItemData(nItem,3), //BG color - show color
																	TRUE);
									m_lceDefault.SetItemData(nItem,0,13);
									m_lceDefault.RedrawItems(nItem,nItem);
								}
							}

						}
					}

					if(bPlacementOK)
					{
						if(g_data.m_bDraggingCommit)
						{
							if(g_data.m_hcurCommit!=hCur) SetCursor(g_data.m_hcurCommit);
						}
						if(g_data.m_bDraggingUncommit)
						{
							if(g_data.m_hcurNS!=hCur) SetCursor(g_data.m_hcurNS);
						}
						g_data.m_bCommitEnabled=TRUE;

						if(!bOnItems) // put at end
						{
		//					AfxMessageBox("XXX");
							//bPlacementOK = TRUE;
							g_data.m_nCommitInsertBeforeID = -1;
							g_data.m_dblCommitBeforeIndex = -0.001;
						//	bOnItems = TRUE;

							if(!(BOOL)m_lceDefault.GetItemData(nCount-1,13))
							{
								m_lceDefault.SetItemData(nCount-1,1,13);
								m_lceDefault.SetItemColor(nCount-1, 0, LCEX_BGCLR,	
									g_settings.m_crCommitted, //BG colors for drop selection
									TRUE);
								m_lceDefault.RedrawItems(nCount-1,nCount-1);
							}
						}

					}
					else
					{
						if(g_data.m_hcurNoDrop!=hCur) SetCursor(g_data.m_hcurNoDrop);	
						g_data.m_bCommitEnabled=FALSE;
					}

					m_nPointerLast=nPointerLast;
				}

			}
			else  		

			if((g_core.IsPointInWnd(scrnpt, GetDlgItem(IDC_LIST_DATASTACK))) //in the comment window, just set the cursor
				&&(m_pointLast!=point))
			{
				m_nWindowLast=ID_LCEFEED;

				m_nPointerLast = -2; //reset
				m_pointLast=point;
				if(g_data.m_bDraggingCommit)
				{
					if(g_data.m_hcurCommit!=hCur) SetCursor(g_data.m_hcurCommit);
				}
				if(g_data.m_bDraggingUncommit)
				{
					if(g_data.m_hcurUncommit!=hCur) SetCursor(g_data.m_hcurUncommit);
				}
				// undo any yellow highlight
				int nCount = m_lceTicker.GetItemCount();
				for (int nItem=0; nItem < nCount; nItem++)
				{
					if ((BOOL)m_lceTicker.GetItemData(nItem,13))
					{
/*
						if(m_lceTicker.GetItemData(nItem,1)&WHEEL_MSG_MARK_DEL)
						{
 							m_lceTicker.SetItemColor(nItem, 0, LCEX_BGCLR, RGB(160,160,160),	TRUE);// unknown songs with RGB(128,128,128) show up OK
						}
						else
						{
*/
						m_lceTicker.SetItemColor(nItem, 0, LCEX_BGCLR,	
														(COLORREF)m_lceTicker.GetItemData(nItem,3), //BG color - show color
														TRUE);
//						}
						m_lceTicker.SetItemData(nItem,0,13);
						m_lceTicker.RedrawItems(nItem,nItem);
					}
				}
				// undo any yellow highlight
				nCount = m_lceDefault.GetItemCount();
				for (nItem=0; nItem < nCount; nItem++)
				{
					if ((BOOL)m_lceDefault.GetItemData(nItem,13))
					{
/*
						if(m_lceDefault.GetItemData(nItem,1)&WHEEL_MSG_MARK_DEL)
						{
 							m_lceDefault.SetItemColor(nItem, 0, LCEX_BGCLR, RGB(160,160,160),	TRUE);// unknown songs with RGB(128,128,128) show up OK
						}
						else
						{
*/
						m_lceDefault.SetItemColor(nItem, 0, LCEX_BGCLR,	
														(COLORREF)m_lceDefault.GetItemData(nItem,3), //BG color - show color
														TRUE);
//						}
						m_lceDefault.SetItemData(nItem,0,13);
						m_lceDefault.RedrawItems(nItem,nItem);
					}
				}

			}
			else if(m_pointLast!=point) // because we already dealt with it if we haven't moved.
			{
				m_nWindowLast=-1;
				m_nPointerLast = -2; //reset

				m_pointLast=point;
				if(g_data.m_hcurNoDrop!=hCur) SetCursor(g_data.m_hcurNoDrop);
				CSize size; size.cx=0; 
				if(scrnpt.y<rcBounds.top)
				{
					size.cy=-16;
					m_lceTicker.Scroll(size);
				}
				else
				if(scrnpt.y>rcBounds.bottom)
				{
					size.cy=16;
					m_lceTicker.Scroll(size);
				}
		//		else
				{
					// undo any yellow highlight
					int nCount = m_lceTicker.GetItemCount();
					for (int nItem=0; nItem < nCount; nItem++)
					{
						if ((BOOL)m_lceTicker.GetItemData(nItem,13))
						{
/*
							if(m_lceTicker.GetItemData(nItem,1)&WHEEL_MSG_MARK_DEL)
							{
 								m_lceTicker.SetItemColor(nItem, 0, LCEX_BGCLR, RGB(160,160,160),	TRUE);// unknown songs with RGB(128,128,128) show up OK
							}
							else
							{
*/
							m_lceTicker.SetItemColor(nItem, 0, LCEX_BGCLR,	
															(COLORREF)m_lceTicker.GetItemData(nItem,3), //BG color - show color
															TRUE);
//							}
							m_lceTicker.SetItemData(nItem,0,13);
							m_lceTicker.RedrawItems(nItem,nItem);
						}
					}
					// undo any yellow highlight
					nCount = m_lceDefault.GetItemCount();
					for (nItem=0; nItem < nCount; nItem++)
					{
						if ((BOOL)m_lceDefault.GetItemData(nItem,13))
						{
/*
							if(m_lceDefault.GetItemData(nItem,1)&WHEEL_MSG_MARK_DEL)
							{
 								m_lceDefault.SetItemColor(nItem, 0, LCEX_BGCLR, RGB(160,160,160),	TRUE);// unknown songs with RGB(128,128,128) show up OK
							}
							else
							{
*/
							m_lceDefault.SetItemColor(nItem, 0, LCEX_BGCLR,	
															(COLORREF)m_lceDefault.GetItemData(nItem,3), //BG color - show color
															TRUE);
//							}
							m_lceDefault.SetItemData(nItem,0,13);
							m_lceDefault.RedrawItems(nItem,nItem);
						}
					}
				}
			}

		}
	}

	CDialog::OnMouseMove(nFlags, point);
}

void CWheelDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
//	CString foo; foo.Format("DOWN %d: %d,%d", GetTickCount(), point.x, point.y);
//	GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(foo);
	CPoint scrnpt=point;
	g_data.m_ptDownClick = point;
	ClientToScreen(&scrnpt);
	if((g_core.IsPointInWnd(scrnpt, GetDlgItem(IDC_STATIC_HWIPE)))||(g_core.IsPointInWnd(scrnpt, GetDlgItem(IDC_STATIC_HWIPE2))))
	{
		SetCapture();
		g_data.m_bDraggingHDivider=TRUE;
		g_data.m_ptStartDivDrag = point;
		HCURSOR hCur = GetCursor();
		if(g_data.m_hcurSize!=hCur) SetCursor(g_data.m_hcurSize);
	}
	else
	if(g_core.IsPointInWnd(scrnpt, GetDlgItem(IDC_STATIC_VWIPE)))
	{
		SetCapture();
		g_data.m_bDraggingVDivider=TRUE;
		g_data.m_ptStartDivDrag = point;
		HCURSOR hCur = GetCursor();
		if(g_data.m_hcurNS!=hCur) SetCursor(g_data.m_hcurNS);
	}
	CDialog::OnLButtonDown(nFlags, point);
}

void CWheelDlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	HCURSOR hCur = GetCursor();
	if(g_data.m_bDraggingCommit)
	{
		CPoint scrnpt=point;
		ClientToScreen(&scrnpt);

//		WaitForSingleObject(g_data.m_pClickEvent->m_hObject, 10000);	

		if(g_core.IsPointInWnd(scrnpt, GetDlgItem(IDC_LIST_PLAYSTACK)))
		{	
			if(g_data.m_hcurCommit!=hCur) SetCursor(g_data.m_hcurCommit);	

			//if(!//g_data.m_bTransact)
			{
				//g_data.m_bTransact = true;
//			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Wheel_Ticker", "entering OnButtonCommit 0");// Sleep(100); //(Dispatch message)
//	EnterCriticalSection(&g_data.m_critTransact);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Wheel_Ticker", "entered OnButtonCommit 0");// Sleep(100); //(Dispatch message)
			OnButtonCommit(); 
//	LeaveCriticalSection(&g_data.m_critTransact);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Wheel_Ticker", "left OnButtonCommit 0");// Sleep(100); //(Dispatch message)
				//g_data.m_bTransact = false;
			}
		}
		else
		if(g_core.IsPointInWnd(scrnpt, GetDlgItem(IDC_LIST_DEFAULTSTACK)))
		{	
			if(g_data.m_hcurCommit!=hCur) SetCursor(g_data.m_hcurCommit);	
			//if(!//g_data.m_bTransact)
			{
				//g_data.m_bTransact = true;
//	EnterCriticalSection(&g_data.m_critTransact);
				OnButtonCommitDefault(); 
//	LeaveCriticalSection(&g_data.m_critTransact);
				//g_data.m_bTransact = false;
			}
		}
		else
		{	if(g_data.m_hcurNoDrop!=hCur) SetCursor(g_data.m_hcurNoDrop);	}
	}

	if(g_data.m_bDraggingUncommit)
	{
		CPoint scrnpt=point;
		ClientToScreen(&scrnpt);

		if(g_core.IsPointInWnd(scrnpt, GetDlgItem(IDC_LIST_PLAYSTACK)))
		{	
			if(g_data.m_hcurCommit!=hCur) SetCursor(g_data.m_hcurCommit);	
			if(g_data.m_bCommitDefaultSource)
			{
				//if(!//g_data.m_bTransact)
				{
					//g_data.m_bTransact = true;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Wheel_Ticker", "entering OnButtonCommit 1 ");// Sleep(100); //(Dispatch message)
//AfxMessageBox("OnButtonCommit");
//	EnterCriticalSection(&g_data.m_critTransact);
					OnButtonCommit(); // position obtained inside
//	LeaveCriticalSection(&g_data.m_critTransact);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Wheel_Ticker", "left OnButtonCommit 1");// Sleep(100); //(Dispatch message)
					//g_data.m_bTransact = false;
				}
			}
			else
			{
				//if(!//g_data.m_bTransact)
				{
					//g_data.m_bTransact = true;
//AfxMessageBox("MoveStackItem");
//	EnterCriticalSection(&g_data.m_critTransact);
					MoveStackItem(FALSE); // position obtained inside
//	LeaveCriticalSection(&g_data.m_critTransact);
					//g_data.m_bTransact = false;
				}
			}
		}
		else
		if(g_core.IsPointInWnd(scrnpt, GetDlgItem(IDC_LIST_DATASTACK)))
		{	
			if(g_data.m_hcurCommit!=hCur) SetCursor(g_data.m_hcurUncommit);	
				//if(!//g_data.m_bTransact)
				{
					//g_data.m_bTransact = true;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Wheel_Ticker", "entering OnButtonCommit 2");// Sleep(100); //(Dispatch message)
//AfxMessageBox("OnButtonCommit");
//	EnterCriticalSection(&g_data.m_critTransact);
			OnButtonCommit(); // position obtained inside
//	LeaveCriticalSection(&g_data.m_critTransact);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Wheel_Ticker", "left OnButtonCommit 2");// Sleep(100); //(Dispatch message)
					//g_data.m_bTransact = false;
				}
		}
		if(g_core.IsPointInWnd(scrnpt, GetDlgItem(IDC_LIST_DEFAULTSTACK)))
		{	
			if(g_data.m_hcurCommit!=hCur) SetCursor(g_data.m_hcurCommit);	
			if(g_data.m_bCommitDefaultSource)
			{
				//if(!//g_data.m_bTransact)
				{
					//g_data.m_bTransact = true;
//AfxMessageBox("MoveStackItem Default");
//	EnterCriticalSection(&g_data.m_critTransact);
			MoveStackItem(TRUE); // position obtained inside
//	LeaveCriticalSection(&g_data.m_critTransact);
					//g_data.m_bTransact = false;
				}
			}
			else
			{
				//if(!//g_data.m_bTransact)
				{
					//g_data.m_bTransact = true;
//	EnterCriticalSection(&g_data.m_critTransact);
				OnButtonCommitDefault(); // position obtained inside
//	LeaveCriticalSection(&g_data.m_critTransact);
					//g_data.m_bTransact = false;
				}
			}
		}
		else
		{	if(g_data.m_hcurNoDrop!=hCur) SetCursor(g_data.m_hcurNoDrop);	}
	}

	g_data.m_bDraggingHDivider=FALSE;
	g_data.m_bDraggingVDivider=FALSE;
	g_data.m_bDraggingCommit=FALSE;
	g_data.m_bDraggingUncommit=FALSE;
	ReleaseCapture();
	// undo any yellow highlight
	int nCount = m_lceTicker.GetItemCount();
	for (int nItem=0; nItem < nCount; nItem++)
	{
		if ((BOOL)m_lceTicker.GetItemData(nItem,13))
		{
//			CWheelMessage* pmsg = (CWheelMessage*) m_lceTicker.GetItemDataPtr(nItem,0);
/*			if(m_lceTicker.GetItemData(nItem,1)&WHEEL_MSG_MARK_DEL)
			{
 				m_lceTicker.SetItemColor(nItem, 0, LCEX_BGCLR, RGB(160,160,160),	TRUE);// unknown songs with RGB(128,128,128) show up OK
			}
			else
			{
*/
			m_lceTicker.SetItemColor(nItem, 0, LCEX_BGCLR,	
											(COLORREF)m_lceTicker.GetItemData(nItem,3), //BG color - show color
											TRUE);
//			}
			m_lceTicker.SetItemData(nItem,0,13);
			m_lceTicker.RedrawItems(nItem,nItem);
		}
	}
	nCount = m_lceDefault.GetItemCount();
	for (nItem=0; nItem < nCount; nItem++)
	{
		if ((BOOL)m_lceDefault.GetItemData(nItem,13))
		{
/*			if(m_lceDefault.GetItemData(nItem,1)&WHEEL_MSG_MARK_DEL)
			{
 				m_lceDefault.SetItemColor(nItem, 0, LCEX_BGCLR, RGB(160,160,160),	TRUE);// unknown songs with RGB(128,128,128) show up OK
			}
			else
			{
*/
			m_lceDefault.SetItemColor(nItem, 0, LCEX_BGCLR,	
											(COLORREF)m_lceDefault.GetItemData(nItem,3), //BG color - show color
											TRUE);
//			}
			m_lceDefault.SetItemData(nItem,0,13);
			m_lceDefault.RedrawItems(nItem,nItem);
		}
	}

	if(g_data.m_ptDownClick != point)
	{
		Invalidate();
//		GetDlgItem(IDC_STATIC_INV)->ShowWindow(SW_SHOW);
//		GetDlgItem(IDC_STATIC_INV)->ShowWindow(SW_HIDE);
	}
	if(g_data.m_hcurArrow!=hCur) SetCursor(g_data.m_hcurArrow);
//	g_data.m_bUpdatePlayoutList=TRUE;

//	ShowTickerSelection();
	
	CDialog::OnLButtonUp(nFlags, point);
}



void CWheelDlg::OnButtonCommit() 
{
#ifndef REMOVE_CLIENTSERVER
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Wheel_Ticker", "OnButtonCommit ");// Sleep(100); //(Dispatch message)
	if(g_data.m_socket==NULL)
	{
		MessageBox("Cannot make changes unless connected.","Connection Error",MB_ICONERROR);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Wheel_Ticker", "OnButtonCommit return ");// Sleep(100); //(Dispatch message)
		return;
	}
	char info[8192];

	ClearDefaultSelection();
	ClearTickerSelection();

	if( 
		  (g_data.m_bCommitEnabled)
		&&(g_data.m_szCommitData.GetLength()>0)
		&&(g_data.m_nCommitID!=-1)
		)
	{


		CWaitCursor cw;
		if(g_data.m_bCommitMode)
		{
			if(!g_data.m_bDraggingCommit)
			{
				// button press, insert at end.
				g_data.m_nCommitInsertBeforeID = -1;
				g_data.m_dblCommitBeforeIndex = -0.001;

			}

			// TODO: Add your control notification handler code here
			CNetData* pdata = new CNetData;
			if(pdata)
			{

				pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

				pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
				pdata->m_ucSubCmd = 0;       // the subcommand byte

				char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
				if(pch)
				{
					_snprintf(pch, MAX_MESSAGE_LENGTH-1, "%s|Server_Ticker_Commit|1|%d|%.4f|%d|%s", g_settings.m_pszInternalAppName,  //1 is not a default.
						g_data.m_nCommitInsertBeforeID, 
						g_data.m_dblCommitBeforeIndex,
						g_data.m_nCommitID,
						g_data.Encode(g_data.m_szCommitData));
					pdata->m_pucData =  (unsigned char*) pch;
					pdata->m_ulDataLen = strlen(pch);
				}
//AfxMessageBox(pch); // return;

		//AfxMessageBox("sending");
		//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
		//AfxMessageBox( (char*)pdata->m_pucData );
		//EnterCriticalSection(&g_data.m_critClientSocket);
	EnterCriticalSection(&g_data.m_critTransact);
				if(g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, info)>=NET_SUCCESS)
				{
					g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
	LeaveCriticalSection(&g_data.m_critTransact);
		//LeaveCriticalSection(&g_data.m_critClientSocket);
			//		EnterCriticalSection(&g_data.m_critMsg);
					if((g_data.m_pmsgCommit)&&(g_data.m_szCommitData.Compare(g_data.m_pmsgCommit->m_szMessage)))
					{
						g_data.m_pmsgCommit->m_szRevisedMessage = g_data.m_szCommitData;
						g_data.m_pmsgCommit->m_nStatus |= (WHEEL_MSG_STATUS_QUEUED|WHEEL_MSG_STATUS_REVISED);
						_ftime(&g_data.m_timeMarks);
						g_data.m_timeMarks.time+=2; // 2 seconds later.
						g_data.m_bQueued = true;
					}
			//		LeaveCriticalSection(&g_data.m_critMsg);

				}
				else
				{
	LeaveCriticalSection(&g_data.m_critTransact);
		//LeaveCriticalSection(&g_data.m_critClientSocket);
					CString szReply; szReply.Format("A network error occurred while trying to queue the text message.\n\n%s",info);
					MessageBox(szReply,"Error Message",MB_ICONERROR);
				}
				delete pdata;
			}
			
		}
		else  //uncommitting
		{
			if(g_data.m_nCommitID==-1) return;

			if(g_data.m_nCommitStatus&(WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_COMMIT))
			{
				return;  // archived items, just ignore.
			}

			CNetData* pdata = new CNetData;
			if(pdata)
			{

				pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

				pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
				pdata->m_ucSubCmd = 0;       // the subcommand byte

				char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
				if(pch)
				{
					_snprintf(pch, MAX_MESSAGE_LENGTH-1, "%s|Server_Ticker_Delete|%d|%.4f", g_settings.m_pszInternalAppName, g_data.m_nCommitID, g_data.m_dblCommitIndex);
					pdata->m_pucData =  (unsigned char*) pch;
					pdata->m_ulDataLen = strlen(pch);
				}
//AfxMessageBox(pch); //return;

		//AfxMessageBox("sending");
		//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
		//AfxMessageBox( (char*)pdata->m_pucData );
		//EnterCriticalSection(&g_data.m_critClientSocket);
	EnterCriticalSection(&g_data.m_critTransact);
				if(g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, info)>=NET_SUCCESS)
				{
					g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
	LeaveCriticalSection(&g_data.m_critTransact);
		//LeaveCriticalSection(&g_data.m_critClientSocket);
				}
				else
				{
	LeaveCriticalSection(&g_data.m_critTransact);
		//LeaveCriticalSection(&g_data.m_critClientSocket);
					CString szReply; szReply.Format("A network error occurred while trying to remove the text message from the queue.\n\n%s",info);
					MessageBox(szReply,"Error Message",MB_ICONERROR);
				}
				delete pdata;
			}
		}
	}

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "Wheel_Ticker", "OnButtonCommit exit ");// Sleep(100); //(Dispatch message)
#endif //#ifndef REMOVE_CLIENTSERVER
	
}

void CWheelDlg::OnButtonCommitDefault() 
{
#ifndef REMOVE_CLIENTSERVER

	if(g_data.m_socket==NULL)
	{
		MessageBox("Cannot make changes unless connected.","Connection Error",MB_ICONERROR);
		return;
	}
	char info[8192];
	ClearDefaultSelection();
	ClearTickerSelection();

	if( 
		  (g_data.m_bCommitEnabled)
		&&(g_data.m_szCommitData.GetLength()>0)
		&&(g_data.m_nCommitID-=-1)
		)
	{
		CWaitCursor cw;
		if(g_data.m_bCommitMode)
		{
			if(!g_data.m_bDraggingCommit)
			{
				// button press, insert at end.
				g_data.m_nCommitInsertBeforeID = -1;
				g_data.m_dblCommitBeforeIndex = -0.001;

			}

			// TODO: Add your control notification handler code here
			CNetData* pdata = new CNetData;
			if(pdata)
			{

				pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

				pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
				pdata->m_ucSubCmd = 0;       // the subcommand byte

				char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
				if(pch)
				{
					_snprintf(pch, MAX_MESSAGE_LENGTH-1, "%s|Server_Ticker_Commit|0|%d|%.4f|%d|%s", g_settings.m_pszInternalAppName, 
						g_data.m_nCommitInsertBeforeID, 
						g_data.m_dblCommitBeforeIndex,
						g_data.m_nCommitID,
						g_data.Encode(g_data.m_szCommitData));
					pdata->m_pucData =  (unsigned char*) pch;
					pdata->m_ulDataLen = strlen(pch);
				}

//AfxMessageBox(pch); //return;
		//AfxMessageBox("sending");
		//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
		//AfxMessageBox( (char*)pdata->m_pucData );
		//EnterCriticalSection(&g_data.m_critClientSocket);
	EnterCriticalSection(&g_data.m_critTransact);
				if(g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, info)>=NET_SUCCESS)
				{
					g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
		//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
				}
				else
				{
		//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
					CString szReply; szReply.Format("A network error occurred while trying to send the text message to the default stack.\n\n%s",info);
					MessageBox(szReply,"Error Message",MB_ICONERROR);
				}
				delete pdata;
			}
			
		}
		else  //uncommitting
		{
			if(g_data.m_nCommitID==-1) return;

			if(g_data.m_nCommitStatus&(WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_COMMIT))
			{
				return;  // archived items, just ignore.
			}

			CNetData* pdata = new CNetData;
			if(pdata)
			{

				pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

				pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
				pdata->m_ucSubCmd = 0;       // the subcommand byte

				char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
				if(pch)
				{
					_snprintf(pch, MAX_MESSAGE_LENGTH-1, "%s|Server_Ticker_Delete|%d|%.4f", g_settings.m_pszInternalAppName, g_data.m_nCommitID, g_data.m_dblCommitIndex);
					pdata->m_pucData =  (unsigned char*) pch;
					pdata->m_ulDataLen = strlen(pch);
				}
//AfxMessageBox(pch);// return;

		//AfxMessageBox("sending");
		//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
		//AfxMessageBox( (char*)pdata->m_pucData );
		//EnterCriticalSection(&g_data.m_critClientSocket);
	EnterCriticalSection(&g_data.m_critTransact);
				if(g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, info)>=NET_SUCCESS)
				{
					g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
		//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
				}
				else
				{
		//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
					CString szReply; szReply.Format("A network error occurred while trying to remove the text message from the default stack.\n\n%s",info);
					MessageBox(szReply,"Error Message",MB_ICONERROR);
				}
				delete pdata;
			}



		}

	}



	// TODO: Add your control notification handler code here
/*  // NO - we dont want to revise when making a default.  defaults get their own IDs, etc.
	EnterCriticalSection(&g_data.m_critMsg);
	if(g_data.m_pmsgCommit)
	{
		if((g_data.m_pmsgCommit)&&(g_data.m_szCommitData.Compare(g_data.m_pmsgCommit->m_szMessage)))
		{
			g_data.m_szCommitData = m_szCurrentPreviewText;
			g_data.m_pmsgCommit->m_szRevisedMessage = m_szCurrentPreviewText;
			g_data.m_pmsgCommit->m_nStatus|=WHEEL_MSG_STATUS_QUEUED;
			_ftime(&g_data.m_timeMarks);
			g_data.m_timeMarks.time+=2; // 2 seconds later.
			g_data.m_bQueued = true;

		}
	}
	LeaveCriticalSection(&g_data.m_critMsg);
*/	
#endif //#ifndef REMOVE_CLIENTSERVER
}

void CWheelDlg::OnButtonData() 
{
	// TODO: Add your control notification handler code here
	if(g_settings.m_bIsServer)
	{
		if((g_dlldata.nNumThreads>WHEEL_DATA)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA]))
		{
			if(g_dlldata.thread[WHEEL_DATA]->nThreadState&WHEEL_DATA_STARTED)
			{
				g_dlldata.thread[WHEEL_DATA]->nThreadControl=WHEEL_DATA_STOPPED; // resets error
			}
			else
			{
				g_dlldata.thread[WHEEL_DATA]->nThreadControl=WHEEL_DATA_STARTED;
			}
		}
	}
	else
	{
		CNetData* pdata = new CNetData;
		if(pdata)
		{

			pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

			pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
			pdata->m_ucSubCmd = 0;       // the subcommand byte

			char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
			if(pch)
			{
				if((g_dlldata.nNumThreads>WHEEL_DATA)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA]))
				{
					if(g_dlldata.thread[WHEEL_DATA]->nThreadState==WHEEL_DATA_STARTED)
					{
						sprintf(pch, "%s|Server_Data_Stop", g_settings.m_pszInternalAppName);
					}
					else
					{
						sprintf(pch, "%s|Server_Data_Start", g_settings.m_pszInternalAppName);
					}
				}
				pdata->m_pucData =  (unsigned char*) pch;
				pdata->m_ulDataLen = strlen(pch);
			}

	//AfxMessageBox("sending");
	//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
	//AfxMessageBox( (char*)pdata->m_pucData );
	EnterCriticalSection(&g_data.m_critTransact);
//EnterCriticalSection(&g_data.m_critClientSocket);
			if(g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT)>=NET_SUCCESS)
			{
				g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
			}
//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
			delete pdata;
		}

	}
}

void CWheelDlg::OnButtonTicker() 
{
	// TODO: Add your control notification handler code here
	if(g_settings.m_bIsServer)
	{
		if((g_dlldata.nNumThreads>WHEEL_TICKER)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_TICKER]))
		{
			if((g_dlldata.thread[WHEEL_TICKER]->nThreadState&WHEEL_TICKER_STARTED)==WHEEL_TICKER_STOPPED)
			{
				g_dlldata.thread[WHEEL_TICKER]->nThreadControl=WHEEL_TICKER_STARTED;
			}
			else
			if(g_dlldata.thread[WHEEL_TICKER]->nThreadState&WHEEL_TICKER_STARTED)
			{
				g_dlldata.thread[WHEEL_TICKER]->nThreadControl=WHEEL_TICKER_STOPPED;
			}
		}
	}
	else
	{
		CNetData* pdata = new CNetData;
		if(pdata)
		{

			pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

			pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
			pdata->m_ucSubCmd = 0;       // the subcommand byte

			char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
			if(pch)
			{
				if((g_dlldata.nNumThreads>WHEEL_DATA)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA]))
				{
					if((g_dlldata.thread[WHEEL_TICKER]->nThreadState&WHEEL_TICKER_STARTED)==WHEEL_TICKER_STOPPED)
					{
						sprintf(pch, "%s|Server_Ticker_In", g_settings.m_pszInternalAppName);
					}
					else
					if(g_dlldata.thread[WHEEL_TICKER]->nThreadState&WHEEL_TICKER_STARTED)
					{
						sprintf(pch, "%s|Server_Ticker_Out", g_settings.m_pszInternalAppName);
					}
				}
				pdata->m_pucData =  (unsigned char*) pch;
				pdata->m_ulDataLen = strlen(pch);
			}

	//AfxMessageBox("sending");
	//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
	//AfxMessageBox( (char*)pdata->m_pucData );
	EnterCriticalSection(&g_data.m_critTransact);
//EnterCriticalSection(&g_data.m_critClientSocket);
			if(g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT)>=NET_SUCCESS)
			{
				g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
			}
//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
			delete pdata;
		}
	}
}

void CWheelDlg::OnCheckAge() 
{
	// TODO: Add your control notification handler code here
	m_bFilterAge = ((CButton*)GetDlgItem(IDC_CHECK_AGE))->GetCheck();
}

void CWheelDlg::OnCheckBad() 
{
	// TODO: Add your control notification handler code here
	m_bFilterBad = ((CButton*)GetDlgItem(IDC_CHECK_BAD))->GetCheck();
//	if(m_bFilterBad) AfxMessageBox("filter bad");
//	else AfxMessageBox("not filter bad");
}

void CWheelDlg::OnCheckCommitted() 
{
	// TODO: Add your control notification handler code here
	m_bFilterCommitted = ((CButton*)GetDlgItem(IDC_CHECK_COMMITTED))->GetCheck();
}

void CWheelDlg::OnCheckDel() 
{
	// TODO: Add your control notification handler code here
//	m_bFilterCommitted = ((CButton*)GetDlgItem(IDC_CHECK_COMMITTED))->GetCheck();
}

void CWheelDlg::OnCheckFilter() 
{
	// TODO: Add your control notification handler code here
	m_bFilter = ((CButton*)GetDlgItem(IDC_CHECK_FILTER))->GetCheck();
/*
	GetDlgItem(IDC_CHECK_AGE)->EnableWindow(m_bFilter); 
	GetDlgItem(IDC_CHECK_BAD)->EnableWindow(m_bFilter);
	GetDlgItem(IDC_CHECK_COMMITTED)->EnableWindow(m_bFilter); 
	GetDlgItem(IDC_CHECK_GOOD)->EnableWindow(m_bFilter); 
	GetDlgItem(IDC_CHECK_GREAT)->EnableWindow(m_bFilter); 
	GetDlgItem(IDC_CHECK_LENGTH)->EnableWindow(m_bFilter); 
	GetDlgItem(IDC_CHECK_PLAYED)->EnableWindow(m_bFilter);
	GetDlgItem(IDC_CHECK_UNMARKED)->EnableWindow(m_bFilter);
*/

	CRect rc;
	GetClientRect(&rc);
	DisableButtons(m_rcDlg.right-rc.Width());
}

void CWheelDlg::OnCheckGood() 
{
	// TODO: Add your control notification handler code here
	m_bFilterGood = ((CButton*)GetDlgItem(IDC_CHECK_GOOD))->GetCheck();
}

void CWheelDlg::OnCheckGreat() 
{
	// TODO: Add your control notification handler code here
	m_bFilterGreat = ((CButton*)GetDlgItem(IDC_CHECK_GREAT))->GetCheck();
}

void CWheelDlg::OnCheckLength() 
{
	// TODO: Add your control notification handler code here
	m_bFilterLength = ((CButton*)GetDlgItem(IDC_CHECK_LENGTH))->GetCheck();
}

void CWheelDlg::OnCheckPlayed() 
{
	// TODO: Add your control notification handler code here
	m_bFilterPlayed = ((CButton*)GetDlgItem(IDC_CHECK_PLAYED))->GetCheck();
	
}

void CWheelDlg::OnCheckUnmarked() 
{
	// TODO: Add your control notification handler code here
	m_bFilterUnmarked = ((CButton*)GetDlgItem(IDC_CHECK_UNMARKED))->GetCheck();
}

void CWheelDlg::SendMarks()
{
	EnterCriticalSection(&g_data.m_critMarks);
	if((g_data.m_socket!=NULL)&&(g_data.m_szMarksBuffer.GetLength()))
	{
	}
	LeaveCriticalSection(&g_data.m_critMarks);
}

void CWheelDlg::MoveStackItem(BOOL bDefault)
{
	if(g_data.m_socket==NULL)
	{
		MessageBox("Cannot make changes unless connected.","Connection Error",MB_ICONERROR);
		return;
	}
	char info[8192];

	if(g_data.m_nCommitID==-1) return;
	if(g_data.m_nCommitStatus&(WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_COMMIT))
	{
		return;  // archived items, just ignore.
	}

	if(bDefault)
	{
		ClearDefaultSelection();
		if(g_data.m_nCommitInsertBeforeID>0) ClearTickerSelection();
	}
	else
	{
		ClearTickerSelection();
		if(g_data.m_nCommitInsertBeforeID<0) ClearDefaultSelection();
	}
	CNetData* pdata = new CNetData;
	if(pdata)
	{

		pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

		pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
		pdata->m_ucSubCmd = 0;       // the subcommand byte

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
		if(pch)
		{
		_snprintf(pch, MAX_MESSAGE_LENGTH-1, "%s|Server_Ticker_Move|%d|%d|%.4f|%d|%.4f",  g_settings.m_pszInternalAppName,  //1 is not a default.
						(bDefault?0:1),
						g_data.m_nCommitInsertBeforeID, 
						g_data.m_dblCommitBeforeIndex,
						g_data.m_nCommitID,
						g_data.m_dblCommitIndex
						);
			pdata->m_pucData =  (unsigned char*) pch;
			pdata->m_ulDataLen = strlen(pch);
		}
//AfxMessageBox(pch);// return;

//AfxMessageBox("sending");
//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
//AfxMessageBox( (char*)pdata->m_pucData );
//EnterCriticalSection(&g_data.m_critClientSocket);
	EnterCriticalSection(&g_data.m_critTransact);
		if(g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, info)>=NET_SUCCESS)
		{
			g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
		}
		else
		{
//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
			CString szReply; szReply.Format("A network error occurred while trying to move the text message.\n\n%s",info);
			MessageBox(szReply,"Error Message",MB_ICONERROR);
		}
		delete pdata;
	}

}

void CWheelDlg::OnBegindragListDefaultstack(NMHDR* pNMHDR, LRESULT* pResult)
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	ClearTickerSelection();
	ClearFeedSelection();
	m_lceDefault.SetFocus();
	ShowDefaultSelection();
	int nItem= m_lceDefault.GetNextItem( -1,  LVNI_SELECTED );
	if (nItem>-1) // valid selection
	{
		SetCapture();
		
		g_data.m_bCommitDefaultSource=TRUE;
		g_data.m_bDraggingCommit=FALSE;
		g_data.m_bDraggingUncommit=TRUE;
	}
	
	*pResult = 0;
}


void CWheelDlg::OnBegindragListPlaystack(NMHDR* pNMHDR, LRESULT* pResult) 
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	ClearDefaultSelection();
	ClearFeedSelection();
	m_lceTicker.SetFocus();
	ShowTickerSelection();
	int nItem= m_lceTicker.GetNextItem( -1,  LVNI_SELECTED );
	if (nItem>-1) // valid selection
	{
		SetCapture();
		
		g_data.m_bCommitDefaultSource=FALSE;
		g_data.m_bDraggingCommit=FALSE;
		g_data.m_bDraggingUncommit=TRUE;
	}
	
	*pResult = 0;
}
void CWheelDlg::OnItemchangedListDefaultstack(NMHDR* pNMHDR, LRESULT* pResult)
{

	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	if(pNMListView->uChanged == LVIF_STATE)
	{
		if(!((pNMListView->uOldState&(LVNI_SELECTED)))
			&&(pNMListView->uNewState&(LVNI_SELECTED)))
		{
			ShowDefaultSelection();
		}
	}
	
	*pResult = 0;
}


void CWheelDlg::OnItemchangedListPlaystack(NMHDR* pNMHDR, LRESULT* pResult) 
{

	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	if(pNMListView->uChanged == LVIF_STATE)
	{
		if(!((pNMListView->uOldState&(LVNI_SELECTED)))
			&&(pNMListView->uNewState&(LVNI_SELECTED)))
		{
			ShowTickerSelection();
		}
	}
	
	*pResult = 0;
}
void CWheelDlg::OnItemchangingListDefaultstack(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	if(pNMListView->uChanged == LVIF_STATE)
	{
		if((pNMListView->uOldState&(LVNI_SELECTED|LVNI_FOCUSED))
			&&(!(pNMListView->uNewState&(LVNI_SELECTED|LVNI_FOCUSED))))
		{
			CWheelMessage* pmsg = (CWheelMessage*)m_lceDefault.GetItemDataPtr(pNMListView->iItem, 0);
			if(pmsg)
			{
				m_lceDefault.SetItemBitmap(pNMListView->iItem, 0, m_lceDefault.GetImage(ReturnDefaultBitmapIndex(pmsg->m_nStatus, FALSE))); 
			}
		}
	}	
	*pResult = 0;
}


void CWheelDlg::OnItemchangingListPlaystack(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	if(pNMListView->uChanged == LVIF_STATE)
	{
		if((pNMListView->uOldState&(LVNI_SELECTED|LVNI_FOCUSED))
			&&(!(pNMListView->uNewState&(LVNI_SELECTED|LVNI_FOCUSED))))
		{
			CWheelMessage* pmsg = (CWheelMessage*)m_lceTicker.GetItemDataPtr(pNMListView->iItem, 0);
			if(pmsg)
			{
				m_lceTicker.SetItemBitmap(pNMListView->iItem, 0, m_lceTicker.GetImage(ReturnTickerBitmapIndex(pmsg->m_nStatus, FALSE))); 
			}
		}
	}	
	*pResult = 0;
}

void CWheelDlg::OnWindowPosChanged( WINDOWPOS* lpwndpos )
{
	CDialog::OnWindowPosChanged( lpwndpos );

	if((lpwndpos->flags&SWP_NOSIZE)&&(lpwndpos->flags&SWP_NOMOVE)) // must be a Zorder thing, or uncovering window
	{
//		CString foo; foo.Format("%x",lpwndpos->flags);
//		AfxMessageBox(foo);
		RedrawWindow(NULL, NULL,
		 RDW_INVALIDATE|RDW_UPDATENOW|RDW_ERASE|RDW_ALLCHILDREN|RDW_INTERNALPAINT|RDW_FRAME
		 );
	}
//	if(m_bVis) OnSelchangeTab1(NULL,NULL) ;
}

void CWheelDlg::OnSetfocusListDefaultstack(NMHDR* pNMHDR, LRESULT* pResult)
{
	// TODO: Add your control notification handler code here
	// go thru all comment windows and clear any selection
	// try this a different way.  clear self on kill focus, instead of clear other on setfocus.
	/// ok that didnt work, the killfocus clears the edit fields even if it isnt the playstack clicked, so definitely use this
	ClearFeedSelection();
	ClearTickerSelection();
	*pResult = 0;
}


void CWheelDlg::OnSetfocusListPlaystack(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	// go thru all comment windows and clear any selection
	// try this a different way.  clear self on kill focus, instead of clear other on setfocus.
	/// ok that didnt work, the killfocus clears the edit fields even if it isnt the playstack clicked, so definitely use this
	ClearFeedSelection();
	ClearDefaultSelection();
	*pResult = 0;
}

void CWheelDlg::OnKillfocusListDefaultstack(NMHDR* pNMHDR, LRESULT* pResult)
{
//	ClearSelection();
	*pResult = 0;
}

void CWheelDlg::OnKillfocusListPlaystack(NMHDR* pNMHDR, LRESULT* pResult) 
{
//	ClearSelection();
	*pResult = 0;
}
void CWheelDlg::OnRclickListDefaultstack(NMHDR* pNMHDR, LRESULT* pResult)
{
#ifndef REMOVE_CLIENTSERVER
//	*pResult = 0; return; // temp to disable menu
	// TODO: Add your control notification handler code here
// HAVE TO ADD A CLEAR ALL IN DB	
	CPoint pt,lpt;
	GetCursorPos(&pt);

	lpt=pt;
//	g_data.m_szCurrentPlayoutClip="";
			
/*
	int nCount = m_lceTicker.GetItemCount();
	if(nCount>1) // blank one at end
	{
		m_lceTicker.ScreenToClient(&lpt);
		int nItem = m_lceTicker.HitTest(lpt);
		if((nItem>0)&&(nItem<nCount-1))
		{
			g_data.m_szCurrentPlayoutClip=*(CString*)(m_lceTicker.GetItemDataPtr(nItem, 1));
			g_data.m_nCurrentPlayoutClipStatus = m_lceTicker.GetItemData(nItem,1);
		}
	}
*/
	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU1));
	CMenu* pMenu; 

	int nItem= m_lceDefault.GetNextItem( -1,  LVNI_SELECTED );
	if (nItem<0) // no selection
	{
		pMenu = menu.GetSubMenu(2);
	}
	else
	{ // determine active or inactive.
		CWheelMessage* pmsg = (CWheelMessage*) m_lceDefault.GetItemDataPtr(nItem, 0);
		if(pmsg)
		{
			if(pmsg->m_nType&WHEEL_MSG_TYPE_DISABLED)
			{
				pMenu = menu.GetSubMenu(1);
			}
			else
			{
				pMenu = menu.GetSubMenu(0);
			}
		}
		else
		{
			pMenu = menu.GetSubMenu(2);
		}
	}



//	pMenu = menu.GetSubMenu(0);
/*
	if(
			(g_data.m_szCurrentPlayoutClip.GetLength()<=0)||
			(g_data.m_nCurrentPlayoutClipStatus&(DB_STATUS_PLAYED|DB_STATUS_ARCHIVED))
		)
		
//		((g_data.m_byteShowMode==2)&&(!g_data.m_bHistoryCleared))) // after show, allows only clearing archive db
		pMenu->EnableMenuItem(ID_CMD_CLEARCLIP, MF_GRAYED|MF_BYCOMMAND );
	else
*/
	if(pMenu)
	{
		pMenu->EnableMenuItem(ID_CMD_CLEARCLIP, MF_ENABLED|MF_BYCOMMAND );
	}
	SetForegroundWindow();
	pMenu->TrackPopupMenu(TPM_RIGHTBUTTON, pt.x, pt.y, this);
	PostMessage(WM_USER);
#endif //#ifndef REMOVE_CLIENTSERVER
	
	*pResult = 0;
}


void CWheelDlg::OnRclickListPlaystack(NMHDR* pNMHDR, LRESULT* pResult) 
{
#ifndef REMOVE_CLIENTSERVER
//	*pResult = 0; return; // temp to disable menu
	// TODO: Add your control notification handler code here
// HAVE TO ADD A CLEAR ALL IN DB	
	CPoint pt,lpt;
	GetCursorPos(&pt);

	lpt=pt;
//	g_data.m_szCurrentPlayoutClip="";
			
/*
	int nCount = m_lceTicker.GetItemCount();
	if(nCount>1) // blank one at end
	{
		m_lceTicker.ScreenToClient(&lpt);
		int nItem = m_lceTicker.HitTest(lpt);
		if((nItem>0)&&(nItem<nCount-1))
		{
			g_data.m_szCurrentPlayoutClip=*(CString*)(m_lceTicker.GetItemDataPtr(nItem, 1));
			g_data.m_nCurrentPlayoutClipStatus = m_lceTicker.GetItemData(nItem,1);
		}
	}
*/
	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU1));
	CMenu* pMenu; 

	int nItem= m_lceTicker.GetNextItem( -1,  LVNI_SELECTED );
	if (nItem<0) // no selection
	{
		pMenu = menu.GetSubMenu(2);
	}
	else
	{ // determine active or inactive.
		CWheelMessage* pmsg = (CWheelMessage*) m_lceTicker.GetItemDataPtr(nItem, 0);
		if(pmsg)
		{
			if(pmsg->m_nType&WHEEL_MSG_TYPE_DISABLED)
			{
				pMenu = menu.GetSubMenu(1);
			}
			else
			{
				pMenu = menu.GetSubMenu(0);
			}
		}
		else
		{
			pMenu = menu.GetSubMenu(2);
		}
	}


	// if no item sub 2, if enabled, sub 0 if disabled sub 1)
//	pMenu = menu.GetSubMenu(0);
/*
	if(
			(g_data.m_szCurrentPlayoutClip.GetLength()<=0)||
			(g_data.m_nCurrentPlayoutClipStatus&(DB_STATUS_PLAYED|DB_STATUS_ARCHIVED))
		)
		
//		((g_data.m_byteShowMode==2)&&(!g_data.m_bHistoryCleared))) // after show, allows only clearing archive db
		pMenu->EnableMenuItem(ID_CMD_CLEARCLIP, MF_GRAYED|MF_BYCOMMAND );
	else
*/
	if(pMenu)
	{
		pMenu->EnableMenuItem(ID_CMD_CLEARCLIP, MF_ENABLED|MF_BYCOMMAND );
	}

	SetForegroundWindow();
	pMenu->TrackPopupMenu(TPM_RIGHTBUTTON, pt.x, pt.y, this);
	PostMessage(WM_USER);
#endif //#ifndef REMOVE_CLIENTSERVER
	
	*pResult = 0;
}

void CWheelDlg::OnChangeRicheditView() 
{
	if(!m_bDefeatSmut)
	{
//AfxMessageBox("dd");
//		try
		{
			DoPreviewSmutCheck();
			GetDlgItem(IDC_RICHEDIT_VIEW)->GetWindowText(g_data.m_szCommitData);
		}
//		catch(...)
		{
		}
//	AfxMessageBox("qq");	
	}
}



int CWheelDlg::ReturnTickerBitmapIndex(int nStatus, BOOL bSel)
{
	int nReturnStatus = 0;
//	if(nStatus&WHEEL_MSG_MARK_DEL)													{		nReturnStatus = 4; }
//	else
	if(nStatus&WHEEL_MSG_STATUS_ERROR)											{		nReturnStatus = 6; }
	else
	if(nStatus&WHEEL_MSG_STATUS_PLAYING)										{		nReturnStatus = 1; }
	else
	if(nStatus&WHEEL_MSG_STATUS_COMMIT)											{		nReturnStatus = 2; }
	else
	if(nStatus&(WHEEL_MSG_STATUS_PLAYED|WHEEL_MSG_MARK_DEL)){		nReturnStatus = 3; }

	if(bSel)
	{
		nReturnStatus +=9;
	}
	return nReturnStatus;
}

int CWheelDlg::ReturnDefaultBitmapIndex(int nStatus, BOOL bSel)
{
	int nReturnStatus = 5;
//	if(nStatus&(WHEEL_MSG_STATUS_PLAYED|WHEEL_MSG_MARK_DEL)){		nReturnStatus = 3; }
//	else
//	if(nStatus&WHEEL_MSG_MARK_DEL)													{		nReturnStatus = 4; }
//	else
	if(nStatus&WHEEL_MSG_STATUS_ERROR)											{		nReturnStatus = 6; }
	else
	if(nStatus&WHEEL_MSG_STATUS_PLAYING)										{		nReturnStatus = 1; }
	else
	if(nStatus&WHEEL_MSG_STATUS_COMMIT)											{		nReturnStatus = 2; }

	if(bSel)
	{
		nReturnStatus +=9;
	}
	return nReturnStatus;
}

int CWheelDlg::ReturnDataBitmapIndex(int nMark, BOOL bSel)
{
	int nReturnMark = WHEEL_MSG_STATUS_NONE;
	switch(nMark&0x0f)
	{
	case WHEEL_MSG_STATUS_NONE:	nReturnMark = 0; break;
	case WHEEL_MSG_MARK_GOOD:		nReturnMark = 1; break;
	case WHEEL_MSG_MARK_BAD:		nReturnMark = 2; break;
	case WHEEL_MSG_MARK_GREAT:	nReturnMark = 3; break;
	}

	if(bSel)
	{
		nReturnMark +=9;
	}
	else
	{
		if(nMark&WHEEL_MSG_STATUS_SENT)
		{
			nReturnMark +=4;
		}
	}
	return nReturnMark;
}

void CWheelDlg::DoPreviewSmutCheck() 
{
	CHARRANGE chrgInit;
	m_editexInfo.GetWnd()->GetSel(chrgInit);
  int numCurses=0, *startEndPairs=NULL, listSize=0;
  unsigned long wordsChecked;
  //  BOOL bReplaceCurses;
  
  CString szTemp, szCodedText;
	FontSettings_t fsFont;
  m_editexInfo.GetWnd()->GetWindowText(szTemp);
  
  if(g_psmut) g_psmut->m_bHighlightWholeWord=FALSE;

	// following is necessary to deal with CRLFs for multiline:

	unsigned long ulLength = szTemp.GetLength()+1;  // include term zero

	char* pch = m_bu.StripChar(szTemp.GetBuffer(0), &ulLength, 13);
	if(pch)
	{
		szTemp.Format("%s",pch); free(pch);
//		ulLength = szTemp.GetLength();
	}
//AfxMessageBox("xxx");
/*
	char* pBuf = szTemp.GetBuffer(ulLength+1);
	*(pBuf+ulLength) = 0;  // put a terminating zero back in...
	szTemp.ReleaseBuffer(ulLength);
*/
	if(g_psmut) 
	{
		numCurses = g_psmut->languageFilter(&szTemp, &startEndPairs, &listSize, g_settings.m_bSmut_ReplaceCurses, &wordsChecked);
	}
//AfxMessageBox("b");

//	AfxMessageBox(szTemp);
//AfxMessageBox("xxxq");
  if(g_settings.m_bSmut_ReplaceCurses)
	{
//AfxMessageBox("hhh");
		m_bDefeatSmut = true;
//AfxMessageBox("qq121");
		m_editexInfo.GetWnd()->SetWindowText(szTemp); //  dont need the if, but may make it micro-faster without the set.
//AfxMessageBox("qq");
		m_bDefeatSmut = false;
		GetDlgItem(IDC_RICHEDIT_VIEW)->GetWindowText(g_data.m_szCommitData);
	}

	fsFont=m_editexInfo.GetFont();
	m_editexInfo.SetFont(fsFont, 0,-1);

//AfxMessageBox(szTemp);
	BOOL bFinalCurse=FALSE;
  for(int i=0;i<numCurses;i++)
  {
//		CString foo; foo.Format("%s\n%d,%d",szTemp,startEndPairs[i+i],startEndPairs[i+i+1]+1);
//		AfxMessageBox(foo);
		fsFont =	m_editexInfo.GetFont();
		fsFont.crText=RGB(255,0,0);
		fsFont.nStyle|=FSS_BOLD;
		m_editexInfo.SetFont(fsFont, startEndPairs[i+i],startEndPairs[i+i+1]+1);
		if(szTemp.GetLength()==startEndPairs[i+i+1]+1) bFinalCurse=TRUE;
  }

	fsFont =	m_editexInfo.GetFont();
	// following is so that a final curse doesnt set the font for subsequently typed stuff
	if(!bFinalCurse)m_editexInfo.SetFont(fsFont, szTemp.GetLength()-1,-1);

	m_editexInfo.GetWnd()->SetSel(chrgInit);

//	m_editexInfo.GetWnd()->Invalidate();
}


void CWheelDlg::ShowTickerSelection() 
{
	int nCount = m_lceTicker.GetItemCount();
	int nItem = m_lceTicker.GetNextItem( -1,  LVNI_SELECTED );

	if((nItem>=0)&&(nItem==nCount-1)) // theres a white space at end
	{
 		m_lceTicker.SetItemColor(nItem, 0, LCEX_BGCLR, GetSysColor(COLOR_WINDOW),TRUE);// make sure its blank.
		m_lceTicker.SetItemBitmap(nItem, 0, m_lceTicker.GetImage(0)); 
	}


	if((nItem>=0)&&(nItem<nCount-1)) // theres a white space at end
	{
		CWheelMessage* pmsg = (CWheelMessage*) m_lceTicker.GetItemDataPtr(nItem, 0);
		if(pmsg)
		{
			g_data.m_pmsgCommit = pmsg;
//			m_lceTicker.SetItem(nItem, 0, LVIF_STATE, NULL, 0, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED, 0);
			g_data.m_nCommitStatus=pmsg->m_nStatus; // for judging if we should move stuff or not

			m_lceTicker.SetItemBitmap(nItem, 0, m_lceTicker.GetImage(ReturnTickerBitmapIndex(pmsg->m_nStatus, TRUE)));
			m_lceTicker.RedrawItems( nItem,nItem );

			g_data.m_nCommitID = pmsg->m_nID;
			g_data.m_dblCommitIndex = pmsg->m_dblIndex;
			m_nSelIdx = g_data.m_nCommitID;

			g_data.m_szCommitData.Format("%s", pmsg->m_szMessage);
//AfxMessageBox(g_data.m_szCommitData);

			m_bDefeatSmut=TRUE;
			GetDlgItem(IDC_RICHEDIT_VIEW)->SetWindowText("");//clears any red font letters
			m_bDefeatSmut=FALSE;
			GetDlgItem(IDC_RICHEDIT_VIEW)->SetWindowText(g_data.m_szCommitData);

//AfxMessageBox(g_data.m_szCommitData);
			if(g_data.m_socket!=NULL) 
			{
				GetDlgItem(IDC_BUTTON_COMMIT)->EnableWindow(TRUE);
				GetDlgItem(IDC_BUTTON_COMMIT2)->EnableWindow(FALSE);
				g_data.m_bCommitEnabled=TRUE;
			}

			((CButton*)GetDlgItem(IDC_BUTTON_COMMIT))->SetBitmap(m_hbmp[REM]);
			((CButton*)GetDlgItem(IDC_BUTTON_COMMIT2))->SetBitmap(m_hbmp[REM]);

/*
		CString szText="";
		nStatus = m_lceTicker.GetItemData(nItem,4); // attribs, actually
		if((nStatus&DB_ATTRIB_SCHED)||(nStatus&DB_ATTRIB_EXP))
		{
			CTime time;
			szText="\n";
			CString szAdd;
			if(nStatus&DB_ATTRIB_SCHED)
			{
				time = CTime((time_t)m_lceTicker.GetItemData(nItem,5));
//				szText.Format("%sScheduled from %s.  ", szText, time.Format("%d-%b-%Y %H:%M:%S"));
				szAdd.Format("Scheduled from %s.  ", time.Format("%d-%b-%Y %H:%M:%S"));
				szText+=szAdd;
			}
			if(nStatus&DB_ATTRIB_EXP)
			{
				time = CTime((time_t)m_lceTicker.GetItemData(nItem,6));
//				szText.Format("%sExpires on %s.", szText, time.Format("%d-%b-%Y %H:%M:%S"));
				szAdd.Format("Expires on %s.", time.Format("%d-%b-%Y %H:%M:%S"));
				szText+=szAdd;
			}
		}
*/
		// set Preview
//		nStatus = m_lceTicker.GetItemData(nItem,2); // feed index, actually
//		g_data.m_nCommitFeedIndex = nStatus;
//		m_nSelFeedIndex = g_data.m_nCommitFeedIndex;
		}
		else nItem=-1;
	}
	if(nItem<0)
	{
		GetDlgItem(IDC_BUTTON_COMMIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_COMMIT2)->EnableWindow(FALSE);
			// set Preview
		m_bDefeatSmut=TRUE;
		GetDlgItem(IDC_RICHEDIT_VIEW)->SetWindowText("");//clears any red font letters
		m_bDefeatSmut=FALSE;
		GetDlgItem(IDC_RICHEDIT_VIEW)->SetWindowText("Information: No item selected");
		g_data.m_bCommitEnabled=FALSE;
		g_data.m_szCommitData=_T("");
		g_data.m_nCommitID = -1;
		g_data.m_dblCommitIndex = -0.001;

//		g_data.m_szCommitClip=_T("");
		g_data.m_nCommitStatus = -1;
		m_nSelIdx = g_data.m_nCommitID;
	}
	g_data.m_bCommitMode=FALSE; //uncommit.

	m_lceTicker.UpdateWindow();
//	m_lce.Invalidate();
}


void CWheelDlg::ShowDefaultSelection() 
{
	int nCount = m_lceDefault.GetItemCount();
	int nItem = m_lceDefault.GetNextItem( -1,  LVNI_SELECTED );

	if((nItem>=0)&&(nItem==nCount-1)) // theres a white space at end
	{
 		m_lceDefault.SetItemColor(nItem, 0, LCEX_BGCLR, GetSysColor(COLOR_WINDOW),TRUE);// make sure its blank.
		m_lceDefault.SetItemBitmap(nItem, 0, m_lceDefault.GetImage(0)); 
	}


	if((nItem>=0)&&(nItem<nCount-1)) // theres a white space at end
	{
		CWheelMessage* pmsg = (CWheelMessage*) m_lceDefault.GetItemDataPtr(nItem, 0);
		if(pmsg)
		{
			g_data.m_pmsgCommit = pmsg;
//			m_lceDefault.SetItem(nItem, 0, LVIF_STATE, NULL, 0, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED, 0);
			g_data.m_nCommitStatus=pmsg->m_nStatus; // for judging if we should move stuff or not

			m_lceDefault.SetItemBitmap(nItem, 0, m_lceDefault.GetImage(ReturnDefaultBitmapIndex(pmsg->m_nStatus, TRUE)));
			m_lceDefault.RedrawItems( nItem,nItem );

			g_data.m_nCommitID = pmsg->m_nID;
			g_data.m_dblCommitIndex = pmsg->m_dblIndex;

			m_nSelIdx = g_data.m_nCommitID;

			g_data.m_szCommitData.Format("%s", pmsg->m_szMessage);

			m_bDefeatSmut=TRUE;
			GetDlgItem(IDC_RICHEDIT_VIEW)->SetWindowText("");//clears any red font letters
			m_bDefeatSmut=FALSE;
			GetDlgItem(IDC_RICHEDIT_VIEW)->SetWindowText(g_data.m_szCommitData);

			if(g_data.m_socket!=NULL) 
			{
				GetDlgItem(IDC_BUTTON_COMMIT)->EnableWindow(FALSE);
				GetDlgItem(IDC_BUTTON_COMMIT2)->EnableWindow(TRUE);
				g_data.m_bCommitEnabled=TRUE;
			}

			((CButton*)GetDlgItem(IDC_BUTTON_COMMIT))->SetBitmap(m_hbmp[REM]);
			((CButton*)GetDlgItem(IDC_BUTTON_COMMIT2))->SetBitmap(m_hbmp[REM]);

/*
		CString szText="";
		nStatus = m_lceDefault.GetItemData(nItem,4); // attribs, actually
		if((nStatus&DB_ATTRIB_SCHED)||(nStatus&DB_ATTRIB_EXP))
		{
			CTime time;
			szText="\n";
			CString szAdd;
			if(nStatus&DB_ATTRIB_SCHED)
			{
				time = CTime((time_t)m_lceDefault.GetItemData(nItem,5));
//				szText.Format("%sScheduled from %s.  ", szText, time.Format("%d-%b-%Y %H:%M:%S"));
				szAdd.Format("Scheduled from %s.  ", time.Format("%d-%b-%Y %H:%M:%S"));
				szText+=szAdd;
			}
			if(nStatus&DB_ATTRIB_EXP)
			{
				time = CTime((time_t)m_lceDefault.GetItemData(nItem,6));
//				szText.Format("%sExpires on %s.", szText, time.Format("%d-%b-%Y %H:%M:%S"));
				szAdd.Format("Expires on %s.", time.Format("%d-%b-%Y %H:%M:%S"));
				szText+=szAdd;
			}
		}
*/
		// set Preview
//		nStatus = m_lceDefault.GetItemData(nItem,2); // feed index, actually
//		g_data.m_nCommitFeedIndex = nStatus;
//		m_nSelFeedIndex = g_data.m_nCommitFeedIndex;
		}
		else nItem=-1;
	}
	if(nItem<0)
	{
		GetDlgItem(IDC_BUTTON_COMMIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_COMMIT2)->EnableWindow(FALSE);
			// set Preview
		m_bDefeatSmut=TRUE;
		GetDlgItem(IDC_RICHEDIT_VIEW)->SetWindowText("");//clears any red font letters
		m_bDefeatSmut=FALSE;
		GetDlgItem(IDC_RICHEDIT_VIEW)->SetWindowText("Information: No item selected");
		g_data.m_bCommitEnabled=FALSE;
		g_data.m_szCommitData=_T("");
		g_data.m_nCommitID = -1;
		g_data.m_dblCommitIndex = -0.001;

//		g_data.m_szCommitClip=_T("");
		g_data.m_nCommitStatus = -1;
		m_nSelIdx = g_data.m_nCommitID;
	}
	g_data.m_bCommitMode=FALSE; //uncommit.

	m_lceDefault.UpdateWindow();
//	m_lce.Invalidate();
}


void CWheelDlg::ShowDataSelection(BOOL bDefeatReset) 
{
#ifndef REMOVE_CLIENTSERVER
	int nCount = m_lceData.GetItemCount();
	int nItem = m_lceData.GetNextItem( -1,  LVNI_SELECTED );

//	CString foo; 
//	foo.Format("%d: %d", GetTickCount(), nItem);
//	g_pdlg->GetDlgItem(IDC_STATIC_STATUS)->SetWindowText(foo);

	if((nItem>=0)&&(nItem<nCount))
	{


//			AfxMessageBox("XXXX");
		CWheelMessage* pmsg = (CWheelMessage*) m_lceData.GetItemDataPtr(nItem, 0);
//			AfxMessageBox("XXXX 1234");
/*
		if((pmsg)&&(bDefeatReset)&&(g_data.m_nCommitID!=pmsg->m_nID))
		{
			//lets unselect the thing
			m_lceData.SetItem(nItem, 0, LVIF_STATE, NULL, 0, ~(LVIS_SELECTED|LVIS_FOCUSED), LVIS_SELECTED|LVIS_FOCUSED, 0);
			pmsg=NULL;
		}
*/
		if(pmsg)
		{
			g_data.m_pmsgCommit = pmsg;
//		m_lceData.SetItem(nItem, 0, LVIF_STATE, NULL, 0, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED, 0);
			m_lceData.SetItemBitmap(nItem, 0, m_lceData.GetImage(ReturnDataBitmapIndex(pmsg->m_nStatus, TRUE)));
//		m_lceData.RedrawItems( nItem,nItem );
//AfxMessageBox("Foo");
		// need to assemble the thing
			m_lceData.RedrawItems( nItem,nItem );
		
			// need to assemble the thing
			g_data.m_nCommitID = pmsg->m_nID;
			g_data.m_dblCommitIndex = pmsg->m_dblIndex;

			g_data.m_bCommitMode=TRUE; //commit.

//AfxMessageBox("chicken");

			if(m_nSelIdx!=g_data.m_nCommitID)
			{
				m_nSelIdx = g_data.m_nCommitID;

//AfxMessageBox("abc");
				if(pmsg->m_szRevisedMessage.GetLength())
				{
					g_data.m_szCommitData.Format("%s", pmsg->m_szRevisedMessage);
				}
				else
				{
					g_data.m_szCommitData.Format("%s", pmsg->m_szMessage);
				}

//AfxMessageBox(g_data.m_szCommitData);
				m_bDefeatSmut=TRUE;
				GetDlgItem(IDC_RICHEDIT_VIEW)->SetWindowText("");//clears any red font letters
//AfxMessageBox("dd");
				m_bDefeatSmut=FALSE;
				GetDlgItem(IDC_RICHEDIT_VIEW)->SetWindowText(g_data.m_szCommitData);

//AfxMessageBox(g_data.m_szCommitData);

			} // else editing right now, do not mess with!

//AfxMessageBox("123");

			if(g_data.m_socket!=NULL) 
			{
				GetDlgItem(IDC_BUTTON_COMMIT)->EnableWindow(TRUE);
				GetDlgItem(IDC_BUTTON_COMMIT2)->EnableWindow(TRUE);
				g_data.m_bCommitEnabled=TRUE;
			}

			((CButton*)GetDlgItem(IDC_BUTTON_COMMIT))->SetBitmap(m_hbmp[INS]);
			((CButton*)GetDlgItem(IDC_BUTTON_COMMIT2))->SetBitmap(m_hbmp[INS]);
		}
		else nItem=-1;
	}
//AfxMessageBox("qqqq");
	if((nItem<0)&&(!bDefeatReset))
	{
		g_data.m_pmsgCommit = NULL;

		GetDlgItem(IDC_BUTTON_COMMIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_COMMIT2)->EnableWindow(FALSE);
			// set Preview
		m_bDefeatSmut=TRUE;
		GetDlgItem(IDC_RICHEDIT_VIEW)->SetWindowText("");//clears any red font letters
		m_bDefeatSmut=FALSE;
		GetDlgItem(IDC_RICHEDIT_VIEW)->SetWindowText("Information: No item selected");

		g_data.m_bCommitEnabled=FALSE;
		g_data.m_szCommitData=_T("");
		g_data.m_nCommitID = -1;
//		g_data.m_szCommitClip=_T("");
		g_data.m_nCommitStatus = -1;
		g_data.m_dblCommitIndex = -0.001;

		m_nSelIdx = g_data.m_nCommitID;
		g_data.m_bCommitMode=FALSE; //c.f. Uncommit.

	}
	m_lceData.UpdateWindow();
//	GetDlgItem(IDC_RICHEDIT_VIEW)->Invalidate();

//	m_lceData.Invalidate();
#endif //#ifndef REMOVE_CLIENTSERVER

}


void CWheelDlg::OnBegindragListText(NMHDR* pNMHDR, LRESULT* pResult) 
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
//	ShowSelection();
	ClearDefaultSelection(); // try this a different way.  clear self on kill focus, instead of clear other on setfocus. (nope didnt work- putting back now)
	ClearTickerSelection(); // try this a different way.  clear self on kill focus, instead of clear other on setfocus. (nope didnt work- putting back now)
	m_lceData.SetFocus();
	ShowDataSelection();
	int nItem= m_lceData.GetNextItem( -1,  LVNI_SELECTED );
	if (nItem>-1) // valid selection
	{
//		HCURSOR hCur = GetCursor();
//		if(g_data.m_hcurCommit!=hCur) SetCursor(g_data.m_hcurCommit);

		SetCapture();

		g_data.m_bDraggingCommit=TRUE;
		g_data.m_bDraggingUncommit=FALSE;
	}

}

void CWheelDlg::OnSetfocusListText(NMHDR* pNMHDR, LRESULT* pResult) 
{
	ClearDefaultSelection(); // try this a different way.  clear self on kill focus, instead of clear other on setfocus. (nope didnt work- putting back now)
	ClearTickerSelection(); // try this a different way.  clear self on kill focus, instead of clear other on setfocus. (nope didnt work- putting back now)
	ShowDataSelection();
	*pResult = 0;
}

void CWheelDlg::OnRdblclkListText(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnRclickListText(pNMHDR, pResult);
	*pResult = 0;
}

void CWheelDlg::OnRclickListText(NMHDR* pNMHDR, LRESULT* pResult) 
{
//	EnterCriticalSection(&m_critData);

	int nCount = m_lceData.GetItemCount();
	int nItem = m_lceData.GetNextItem( -1,  LVNI_SELECTED );
	if((nItem>=0)&&(nItem<nCount))
	{
		// mark and then set a timer to send the mark
		_ftime(&g_data.m_timeMarks);
		g_data.m_timeMarks.time+=2; // 2 seconds later.
		CWheelMessage* pmsg = (CWheelMessage*) m_lceData.GetItemDataPtr(nItem, 0);
		if(pmsg)
		{
				// increment mark
			switch(pmsg->m_nStatus&WHEEL_MSG_MARK_MASK)
			{
			case WHEEL_MSG_MARK_NONE:  pmsg->m_nStatus &= ~WHEEL_MSG_MARK_MASK; pmsg->m_nStatus|=WHEEL_MSG_MARK_GOOD; break; // nothing - > good
			case WHEEL_MSG_MARK_BAD:   pmsg->m_nStatus &= ~WHEEL_MSG_MARK_MASK; pmsg->m_nStatus|=WHEEL_MSG_MARK_GREAT; break; // bad - >	great
			case WHEEL_MSG_MARK_GOOD:  pmsg->m_nStatus &= ~WHEEL_MSG_MARK_MASK; pmsg->m_nStatus|=WHEEL_MSG_MARK_BAD; break; // good - > bad
			case WHEEL_MSG_MARK_GREAT: pmsg->m_nStatus &= ~WHEEL_MSG_MARK_MASK; break; // great - > nothing
			default: pmsg->m_nStatus &= ~WHEEL_MSG_MARK_MASK; break; // ? - > nothing
			}
		

			pmsg->m_nStatus|=WHEEL_MSG_STATUS_QUEUED;
			g_data.m_bQueued = true;

			m_lceData.SetItemBitmap(nItem, 0, m_lceData.GetImage(ReturnDataBitmapIndex(pmsg->m_nStatus, TRUE))); 
		}
		m_lceData.RedrawItems( nItem,nItem );
		m_lceData.UpdateWindow();
	}
//	LeaveCriticalSection(&m_critData);

	*pResult = 0;
}



void CWheelDlg::OnItemchangingListText(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	if(pNMListView->uChanged == LVIF_STATE)
	{
		if((pNMListView->uOldState&(LVNI_SELECTED|LVNI_FOCUSED))
			&&(!(pNMListView->uNewState&(LVNI_SELECTED|LVNI_FOCUSED))))
		{
			CWheelMessage* pmsg = (CWheelMessage*) m_lceData.GetItemDataPtr(pNMListView->iItem, 0);
			if(pmsg)
			{
				m_lceData.SetItemBitmap(pNMListView->iItem, 0, m_lceData.GetImage(ReturnDataBitmapIndex(pmsg->m_nStatus, FALSE))); 
			}
		}
	}

	*pResult = 0;
}

void CWheelDlg::OnItemchangedListText(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	if(pNMListView->uChanged == LVIF_STATE)
	{
		if(!((pNMListView->uOldState&(LVNI_SELECTED)))
			&&(pNMListView->uNewState&(LVNI_SELECTED)))
		{
//			CString foo; foo.Format("%x", pNMListView->hdr.code);
//			AfxMessageBox(foo);
			ShowDataSelection();
//			int nMark=m_lceData.GetItemData(pNMListView->iItem, 1);
//			m_lceData.SetItemBitmap(pNMListView->iItem, 0, m_lceData.GetImage(ReturnBitmapIndex(nMark, TRUE))); 
		}
	}
	
	*pResult = 0;
}


void CWheelDlg::OnKillfocusListText(NMHDR* pNMHDR, LRESULT* pResult) 
{
//	ClearSelection();
	
	*pResult = 0;
}



void CWheelDlg::OnChangeEditMins() 
{
	CString szMinutesOrig;
	CString szMinutes;
	CString szMinutesValue="";
	int nMinComp=COMP_EQ;
	GetDlgItem(IDC_EDIT_MINS)->GetWindowText(szMinutesOrig);

	DWORD dwSel = ((CEdit*)GetDlgItem(IDC_EDIT_MINS))->GetSel();

	int nLen = szMinutesOrig.GetLength();
	if(nLen>0)
	{
		int i=0;
		while(i<nLen)
		{
			char ch = szMinutesOrig.GetAt(i);

			if(i==0)
			{
				if(ch=='<')
				{
					nMinComp=COMP_LT;
				}
				else
				if(ch=='>')
				{
					nMinComp=COMP_GT;
				}
				else
				if(ch=='=')
				{
					nMinComp=COMP_EQEX;
				}
				else
				if((ch>47)&&(ch<58))
				{
					szMinutesValue += ch;
				}
			}
			else
			{
				if((ch>47)&&(ch<58))
				{
					szMinutesValue += ch;
				}
			}
			i++;
		}
	}

	if(nMinComp==COMP_EQ)
	{
		m_nMinutesValue = atoi(szMinutesValue);
		if(nLen<=0) szMinutes="";
		else szMinutes.Format("%d", m_nMinutesValue);
		m_nMinutesComparator=COMP_EQ;
	}
	else
	{
		switch(nMinComp)
		{
		case COMP_LT:
			{
				m_nMinutesValue = atoi(szMinutesValue);
				if(nLen<=1) szMinutes="<";
				else szMinutes.Format("<%d", m_nMinutesValue);
				m_nMinutesComparator=COMP_LT;
			} break;
		case COMP_GT:
			{
				m_nMinutesValue = atoi(szMinutesValue);
				if(nLen<=1) szMinutes=">";
				else szMinutes.Format(">%d", m_nMinutesValue);
				m_nMinutesComparator=COMP_GT;
			} break;
		case COMP_EQEX:
			{
				m_nMinutesValue = atoi(szMinutesValue);
				if(nLen<=1) szMinutes="=";
				else szMinutes.Format("=%d", m_nMinutesValue);
				m_nMinutesComparator=COMP_EQ;
			} break;
		}
	}
////AfxMessageBox(szTimesPlayed+ "  "+m_szTimesPlayed +"   "+szMinutesOrig);

	if(szMinutesOrig.Compare(m_szMinutes))
	{
		m_szMinutes = szMinutes;
		GetDlgItem(IDC_EDIT_MINS)->SetWindowText(m_szMinutes);
	}

	((CEdit*)GetDlgItem(IDC_EDIT_MINS))->SetSel(HIWORD(dwSel), LOWORD(dwSel));

}

void CWheelDlg::OnChangeEditNumchars() 
{
	CString szLengthOrig;
	CString szLength;
	CString szLengthValue="";
	int nLenComp=COMP_EQ;
	GetDlgItem(IDC_EDIT_NUMCHARS)->GetWindowText(szLengthOrig);

	DWORD dwSel = ((CEdit*)GetDlgItem(IDC_EDIT_NUMCHARS))->GetSel();

	int nLen = szLengthOrig.GetLength();
	if(nLen>0)
	{
		int i=0;
		while(i<nLen)
		{
			char ch = szLengthOrig.GetAt(i);

			if(i==0)
			{
				if(ch=='<')
				{
					nLenComp=COMP_LT;
				}
				else
				if(ch=='>')
				{
					nLenComp=COMP_GT;
				}
				else
				if(ch=='=')
				{
					nLenComp=COMP_EQEX;
				}
				else
				if((ch>47)&&(ch<58))
				{
					szLengthValue += ch;
				}
			}
			else
			{
				if((ch>47)&&(ch<58))
				{
					szLengthValue += ch;
				}
			}
			i++;
		}
	}

	if(nLenComp==COMP_EQ)
	{
		m_nLengthValue = atoi(szLengthValue);
		if(nLen<=0) szLength="";
		else szLength.Format("%d", m_nLengthValue);
		m_nLengthComparator=COMP_EQ;
	}
	else
	{
		switch(nLenComp)
		{
		case COMP_LT:
			{
				m_nLengthValue = atoi(szLengthValue);
				if(nLen<=1) szLength="<";
				else szLength.Format("<%d", m_nLengthValue);
				m_nLengthComparator=COMP_LT;
			} break;
		case COMP_GT:
			{
				m_nLengthValue = atoi(szLengthValue);
				if(nLen<=1) szLength=">";
				else szLength.Format(">%d", m_nLengthValue);
				m_nLengthComparator=COMP_GT;
			} break;
		case COMP_EQEX:
			{
				m_nLengthValue = atoi(szLengthValue);
				if(nLen<=1) szLength="=";
				else szLength.Format("=%d", m_nLengthValue);
				m_nLengthComparator=COMP_EQ;
			} break;
		}
	}

	if(szLengthOrig.Compare(m_szLength))
	{
		m_szLength = szLength;
		GetDlgItem(IDC_EDIT_NUMCHARS)->SetWindowText(m_szLength);
	}

	((CEdit*)GetDlgItem(IDC_EDIT_NUMCHARS))->SetSel(HIWORD(dwSel), LOWORD(dwSel));

}

void CWheelDlg::OnChangeEditTimesplayed() 
{
	CString szTimesPlayedOrig;
	CString szTimesPlayed;
	CString szTimesPlayedValue="";
	int nTimeComp=COMP_EQ;
	GetDlgItem(IDC_EDIT_TIMESPLAYED)->GetWindowText(szTimesPlayedOrig);
//AfxMessageBox("goat");

	DWORD dwSel = ((CEdit*)GetDlgItem(IDC_EDIT_TIMESPLAYED))->GetSel();
//AfxMessageBox("eel");

	int nLen = szTimesPlayedOrig.GetLength();
	if(nLen>0)
	{
		int i=0;
		while(i<nLen)
		{
			char ch = szTimesPlayedOrig.GetAt(i);

			if(i==0)
			{
				if(ch=='<')
				{
					nTimeComp=COMP_LT;
				}
				else
				if(ch=='>')
				{
					nTimeComp=COMP_GT;
				}
				else
				if(ch=='=')
				{
					nTimeComp=COMP_EQEX;
				}
				else
				if((ch>47)&&(ch<58))
				{
					szTimesPlayedValue += ch;
				}
			}
			else
			{
				if((ch>47)&&(ch<58))
				{
					szTimesPlayedValue += ch;
				}
			}
			i++;
		}
	}
//AfxMessageBox("fox");

	if(nTimeComp==COMP_EQ)
	{
		m_nTimesPlayedValue = atoi(szTimesPlayedValue);
		if(nLen<=0) szTimesPlayed="";
		else szTimesPlayed.Format("%d", m_nTimesPlayedValue);
		m_nTimesPlayedComparator=COMP_EQ;
	}
	else
	{
		switch(nTimeComp)
		{
		case COMP_LT:
			{
				m_nTimesPlayedValue = atoi(szTimesPlayedValue);
				if(nLen<=1) szTimesPlayed="<";
				else szTimesPlayed.Format("<%d", m_nTimesPlayedValue);
				m_nTimesPlayedComparator=COMP_LT;
			} break;
		case COMP_GT:
			{
				m_nTimesPlayedValue = atoi(szTimesPlayedValue);
				if(nLen<=1) szTimesPlayed=">";
				else szTimesPlayed.Format(">%d", m_nTimesPlayedValue);
				m_nTimesPlayedComparator=COMP_GT;
			} break;
		case COMP_EQEX:
			{
				m_nTimesPlayedValue = atoi(szTimesPlayedValue);
				if(nLen<=1) szTimesPlayed="=";
				else szTimesPlayed.Format("=%d", m_nTimesPlayedValue);
				m_nTimesPlayedComparator=COMP_EQ;
			} break;
		}
	}
//AfxMessageBox(szTimesPlayed+ "  "+m_szTimesPlayed +"   "+szTimesPlayedOrig);
//AfxMessageBox(m_szTimesPlayed);

	if(szTimesPlayedOrig.Compare(m_szTimesPlayed))
	{
		m_szTimesPlayed = szTimesPlayed;
		GetDlgItem(IDC_EDIT_TIMESPLAYED)->SetWindowText(m_szTimesPlayed);
	}
	((CEdit*)GetDlgItem(IDC_EDIT_TIMESPLAYED))->SetSel(HIWORD(dwSel), LOWORD(dwSel));
//AfxMessageBox("chicken2");

}



void CWheelDlg::OnKillfocusRicheditView(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
/*
	EnterCriticalSection(&g_data.m_critMsg);
	if(g_data.m_pmsgCommit)
	{
		if(m_szCurrentPreviewText.Compare(g_data.m_pmsgCommit->m_szMessage))
		{
			g_data.m_szCommitData = m_szCurrentPreviewText;
			g_data.m_pmsgCommit->m_szRevisedMessage = m_szCurrentPreviewText;
			g_data.m_pmsgCommit->m_nStatus|=WHEEL_MSG_STATUS_QUEUED;
		}
	}
	LeaveCriticalSection(&g_data.m_critMsg);
*/	
	*pResult = 0;
}

void CWheelDlg::OnClearTicker()
{
	if(g_data.m_socket==NULL)
	{
		MessageBox("Cannot make changes unless connected.","Connection Error",MB_ICONERROR);
		return;
	}

	if(MessageBox("This action will stop the ticker before clearing the database.\r\nAre you sure you want to delete all ticker messages, excluding default messages?", "Confirm Action", MB_ICONQUESTION|MB_YESNO)==IDYES)
	{
	EnterCriticalSection(&g_data.m_critTransact);
			CNetData* pdata = new CNetData;
			if(pdata)
			{

				pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

				pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
				pdata->m_ucSubCmd = 0;       // the subcommand byte

				char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
				if(pch)
				{
					_snprintf(pch, MAX_MESSAGE_LENGTH-1, "%s|Server_Ticker_Clear|0", g_settings.m_pszInternalAppName);
					pdata->m_pucData =  (unsigned char*) pch;
					pdata->m_ulDataLen = strlen(pch);
				}
//AfxMessageBox(pch); // return;

	char info[8192];
		//EnterCriticalSection(&g_data.m_critClientSocket);
				if(g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, info)>=NET_SUCCESS)
				{
					g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
		//LeaveCriticalSection(&g_data.m_critClientSocket);
				}
				else
				{
		//LeaveCriticalSection(&g_data.m_critClientSocket);
					CString szReply; szReply.Format("A network error occurred while trying to clear the ticker data.\n\n%s",info);
					MessageBox(szReply,"Error Message",MB_ICONERROR);
				}
				delete pdata;
			}
	LeaveCriticalSection(&g_data.m_critTransact);
	}

}

void CWheelDlg::OnClearDefault()
{
	if(g_data.m_socket==NULL)
	{
		MessageBox("Cannot make changes unless connected.","Connection Error",MB_ICONERROR);
		return;
	}

	if(MessageBox("This action will stop the ticker before clearing the database.\r\nAre you sure you want to delete all default messages?", "Confirm Action", MB_ICONQUESTION|MB_YESNO)==IDYES)
	{
	EnterCriticalSection(&g_data.m_critTransact);
			CNetData* pdata = new CNetData;
			if(pdata)
			{

				pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

				pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
				pdata->m_ucSubCmd = 0;       // the subcommand byte

				char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
				if(pch)
				{
					_snprintf(pch, MAX_MESSAGE_LENGTH-1, "%s|Server_Ticker_Clear|1", g_settings.m_pszInternalAppName);
					pdata->m_pucData =  (unsigned char*) pch;
					pdata->m_ulDataLen = strlen(pch);
				}
//AfxMessageBox(pch); // return;

	char info[8192];
		//EnterCriticalSection(&g_data.m_critClientSocket);
				if(g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, info)>=NET_SUCCESS)
				{
					g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
		//LeaveCriticalSection(&g_data.m_critClientSocket);
				}
				else
				{
		//LeaveCriticalSection(&g_data.m_critClientSocket);
					CString szReply; szReply.Format("A network error occurred while trying to clear the ticker data.\n\n%s",info);
					MessageBox(szReply,"Error Message",MB_ICONERROR);
				}
				delete pdata;
			}
	LeaveCriticalSection(&g_data.m_critTransact);
	}

}


void CWheelDlg::OnToggleActive()
{
	if(g_data.m_socket==NULL)
	{
		MessageBox("Cannot make changes unless connected.","Connection Error",MB_ICONERROR);
		return;
	}

	if(g_data.m_nCommitID==-1) return;

	if(g_data.m_nCommitStatus&(WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_COMMIT))
	{
		return;  // archived items, just ignore.
	}

	CNetData* pdata = new CNetData;
	if(pdata)
	{

		pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

		pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
		pdata->m_ucSubCmd = 0;       // the subcommand byte

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
		if(pch)
		{
			_snprintf(pch, MAX_MESSAGE_LENGTH-1, "%s|Server_Ticker_Activate|%d|%.4f", g_settings.m_pszInternalAppName, g_data.m_nCommitID, g_data.m_dblCommitIndex);
			pdata->m_pucData =  (unsigned char*) pch;
			pdata->m_ulDataLen = strlen(pch);
		}
//AfxMessageBox(pch); //return;

//AfxMessageBox("sending");
//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
//AfxMessageBox( (char*)pdata->m_pucData );
//EnterCriticalSection(&g_data.m_critClientSocket);
		char info[8192];

EnterCriticalSection(&g_data.m_critTransact);
		if(g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, info)>=NET_SUCCESS)
		{
			g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
LeaveCriticalSection(&g_data.m_critTransact);
//LeaveCriticalSection(&g_data.m_critClientSocket);
		}
		else
		{
LeaveCriticalSection(&g_data.m_critTransact);
//LeaveCriticalSection(&g_data.m_critClientSocket);
			CString szReply; szReply.Format("A network error occurred while trying to enable or disable the text message in the queue.\n\n%s",info);
			MessageBox(szReply,"Error Message",MB_ICONERROR);
		}
		delete pdata;
	}

}


void CWheelDlg::OnClearFeed()
{

	if(g_data.m_socket==NULL)
	{
		MessageBox("Cannot make changes unless connected.","Connection Error",MB_ICONERROR);
		return;
	}

	int nDlgResponse = MessageBox("This action will stop the data download before clearing the database.\r\nAre you sure you want to reset all feed data?\r\n\r\nPress [Yes] to reset the feed starting new downloads from the beginning\r\nPress [No] to reset the feed starting new downloads from the most recent message\r\nPress [Cancel] to discontinue this action.", "Confirm Action", MB_ICONQUESTION|MB_YESNOCANCEL);
	if((nDlgResponse==IDYES)||(nDlgResponse==IDNO))
	{
	EnterCriticalSection(&g_data.m_critTransact);

			CNetData* pdata = new CNetData;
			if(pdata)
			{

				pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

				pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
				pdata->m_ucSubCmd = 0;       // the subcommand byte

				char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
				if(pch)
				{
					_snprintf(pch, MAX_MESSAGE_LENGTH-1, "%s|Server_Data_Clear|%d", g_settings.m_pszInternalAppName, ((nDlgResponse==IDYES)?1:0));
					pdata->m_pucData =  (unsigned char*) pch;
					pdata->m_ulDataLen = strlen(pch);
				}
//AfxMessageBox(pch); // return;

	EnterCriticalSection(&m_critData);
	g_pdlg->m_lceData.DeleteAllItems();
	LeaveCriticalSection(&m_critData);


	char info[8192];
		//EnterCriticalSection(&g_data.m_critClientSocket);
				if(g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, info)>=NET_SUCCESS)
				{
					g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
		//LeaveCriticalSection(&g_data.m_critClientSocket);
				}
				else
				{
		//LeaveCriticalSection(&g_data.m_critClientSocket);
					CString szReply; szReply.Format("A network error occurred while trying to clear the feed data.\n\n%s",info);
					MessageBox(szReply,"Error Message",MB_ICONERROR);
				}
				delete pdata;
			}
	LeaveCriticalSection(&g_data.m_critTransact);

	}
}

void CWheelDlg::OnRefresh()
{
	m_dblLastTickerUpdate =-1.0;

	if(m_bDontGetOlderFeedRecords)
	{
		if(MessageBox("Would you like to set the Client to request only the most recent feed records?",
			"Connection Option",
			MB_ICONQUESTION|MB_YESNO)==IDYES)
		{
			m_bDontGetOlderFeedRecords = TRUE;
			_ftime(&g_dlldata.thread[WHEEL_UI]->timebTick);
			m_dblLastDataUpdate = (double)(g_dlldata.thread[WHEEL_UI]->timebTick.time-300); //5 mins
		}
		else
		{
			m_bDontGetOlderFeedRecords = FALSE;
			m_dblLastDataUpdate =-1.0;
		}
	}
	else
	{
		m_dblLastDataUpdate =-1.0;
	}
}

void CWheelDlg::OnSettingsDlg()
{
	if(g_settings.m_bHasDlg)
	{
		g_settings.DoModal();
	}
	else MessageBox("Settings are unavailable", "Error", MB_OK);

}



void CWheelDlg::OnColumnclickListDatastack(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	if(m_bSortTimestampsAsc)
	{
		m_bSortTimestampsAsc=FALSE;
	}
	else
	{
		m_bSortTimestampsAsc=TRUE;
	}
	
	*pResult = 0;
}

void CWheelDlg::OnCheckCycle() 
{
	CWaitCursor cw;
	// should put the isserver part in here
	CNetData* pdata = new CNetData;
	if(pdata)
	{

		pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

		pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
		pdata->m_ucSubCmd = 0;       // the subcommand byte

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
		if(pch)
		{
			sprintf(pch, "%s|Server_Ticker_Cycle", g_settings.m_pszInternalAppName);
			pdata->m_pucData =  (unsigned char*) pch;
			pdata->m_ulDataLen = strlen(pch);
		}

//AfxMessageBox("sending");
//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
//AfxMessageBox( (char*)pdata->m_pucData );
EnterCriticalSection(&g_data.m_critTransact);
//EnterCriticalSection(&g_data.m_critClientSocket);
		if(g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT)>=NET_SUCCESS)
		{
			g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
		}
//LeaveCriticalSection(&g_data.m_critClientSocket);
LeaveCriticalSection(&g_data.m_critTransact);
		delete pdata;
	}
}


