#if !defined(AFX_WHEELDLG_H__F935B8A6_4FC2_4AB9_A958_9A2C93601BBC__INCLUDED_)
#define AFX_WHEELDLG_H__F935B8A6_4FC2_4AB9_A958_9A2C93601BBC__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// WheelDlg.h : header file
//
#define ID_BNTKR		 0
#define ID_STHWIPE	 1
#define ID_STVWIPE	 2
#define ID_STHWIPE2	 3
#define ID_REFEED		 4
#define ID_RETICK		 5
#define ID_REPREV		 6
#define ID_BNCOMMIT  7
#define ID_BNCOMMIT2 8
#define ID_STCLIPRC  9
#define ID_LCEFEED	 10
#define ID_LCEDEFLT  11
#define ID_LCESTACK  12
#define ID_BNCYCLE	 13
#define ID_INVF			 14


#define DLG_NUM_MOVING_CONTROLS 15


#define ID_BNFDEL		 0
#define ID_BNFUN		 1
#define ID_BNFGD		 2
#define ID_BNFBD		 3
#define ID_BNFGT		 4
#define ID_BNFCM		 5
#define ID_BNFPL		 6
#define ID_BNFPLV		 7
#define ID_BNFCH		 8
#define ID_BNFCHV		 9
#define ID_BNFTM		 10
#define ID_BNFTMV		 11

#define DLG_NUM_STATIC_CONTROLS 12



#define INS				0
#define REM				1
#define RELOAD		2
#define BAD				3
#define UNMARKED	4
#define GOOD			5
#define GREAT			6
#define COMMITTED	7
#define PLAYED		8
#define LENGTH		9
#define AGE				10

#include "..\..\..\Common\MFC\ListCtrlEx\ListCtrlEx.h"
#include "..\..\..\Common\MFC\EditEx\EditEx.h"
#include "..\..\..\Common\IMG\BMP\CBmpUtil_MFC.h"
#include "..\..\..\Common\TXT\BufferUtil.h"


/////////////////////////////////////////////////////////////////////////////
// CWheelDlg dialog

class CWheelDlg : public CDialog
{
// Construction
public:
	CWheelDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CWheelDlg)
	enum { IDD = IDD_DIALOG_MAIN };
	BOOL	m_bFilterAge;
	BOOL	m_bFilterBad;
	BOOL	m_bFilterCommitted;
	BOOL	m_bFilterDeleted;
	BOOL	m_bFilter;
	BOOL	m_bFilterGood;
	BOOL	m_bFilterGreat;
	BOOL	m_bFilterLength;
	BOOL	m_bFilterPlayed;
	BOOL	m_bFilterUnmarked;
	CString	m_szMinutes;
	CString	m_szLength;
	CString	m_szTimesPlayed;
	//}}AFX_DATA

	BOOL	m_bSortTimestampsAsc;
	BOOL	m_bDontGetOlderFeedRecords;
	BOOL	m_bChoiceMade;

	int	m_nMinutesComparator;
	int	m_nLengthComparator;
	int	m_nTimesPlayedComparator;
	int	m_nMinutesValue;
	int	m_nLengthValue;
	int	m_nTimesPlayedValue;

	CRITICAL_SECTION m_critData;
	CRITICAL_SECTION m_critDefault;
	CRITICAL_SECTION m_critTicker;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWheelDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


	//sizing
	BOOL  m_bVis;
	CRect m_rcDlg;
	CRect m_rcCtrl[DLG_NUM_MOVING_CONTROLS];
	CRect m_rcCtrlStatic[DLG_NUM_STATIC_CONTROLS];

public:
	BOOL 	m_bNewSizeInit;

	void DisableButtons(int dx);

	void OnToggleActive();
	void OnClearTicker();
	void OnClearDefault();
	void OnClearFeed();
	void OnSettingsDlg();
	void OnRefresh();

	void OnExit();
	void SetWidths();
	void OffsetCtrlRects(int dx, int dy);

	void ClearTickerSelection();
	void ClearDefaultSelection();
	void ClearFeedSelection();
	
#ifndef REMOVE_CLIENTSERVER
	BOOL IsFilteredOut(CWheelMessage* pmsg);
#endif //#ifndef REMOVE_CLIENTSERVER
	
	double m_dblLastDataUpdate;
	double m_dblLastTickerUpdate;

	CBmpUtil m_bmp;
	CBufferUtil m_bu;
	//CWheelData m_data; // just for util funcs

//	CString m_szCurrentPreviewText;
	BOOL   m_bDefeatSmut;  // or the colors specificed below

	CListCtrlEx m_lceData;
	CListCtrlEx m_lceTicker;
	CListCtrlEx m_lceDefault;
	CEditEx m_editexInfo;
	CEditEx m_editexData;
	CEditEx m_editexTicker;
	FontSettings_t m_fsFontData;
	FontSettings_t m_fsFontTicker;
	FontSettings_t m_fsFontPreview;
	CPoint m_pointLast;

	int m_nWindowLast;
	int m_nPointerLast;
	int m_nSelIdx;


	BOOL   m_bUseSysDefaultColors;  // or the colors specificed below
	CBrush m_brush; // def BG color for dialog 
	COLORREF m_crDefBG; //yep


	HBITMAP m_hbmp[11];

	void DoPreviewSmutCheck();
	void ShowTickerSelection();
	void ShowDefaultSelection();
	int	 ReturnTickerBitmapIndex(int nStatus, BOOL bSel=FALSE);
	int	 ReturnDefaultBitmapIndex(int nStatus, BOOL bSel=FALSE);

	void MoveStackItem(BOOL bDefault=FALSE);
	void SendMarks();


	int ReturnMark(int nID);
	int ReturnDataBitmapIndex(int nMark, BOOL bSel=FALSE);
	void ShowDataSelection(BOOL bDefeatReset=FALSE);



// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CWheelDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL OnInitDialog();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnButtonCommit();
	afx_msg void OnButtonCommitDefault();
	afx_msg void OnButtonData();
	afx_msg void OnButtonTicker();
	afx_msg void OnCheckAge();
	afx_msg void OnCheckBad();
	afx_msg void OnCheckCommitted();
	afx_msg void OnCheckDel();
	afx_msg void OnCheckFilter();
	afx_msg void OnCheckGood();
	afx_msg void OnCheckGreat();
	afx_msg void OnCheckLength();
	afx_msg void OnCheckPlayed();
	afx_msg void OnCheckUnmarked();
	afx_msg void OnChangeRicheditView();
	afx_msg void OnBegindragListPlaystack(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangedListPlaystack(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangingListPlaystack(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnWindowPosChanged( WINDOWPOS* lpwndpos );
	afx_msg void OnSetfocusListPlaystack(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusListPlaystack(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclickListPlaystack(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBegindragListDefaultstack(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangedListDefaultstack(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangingListDefaultstack(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSetfocusListDefaultstack(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusListDefaultstack(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclickListDefaultstack(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBegindragListText(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSetfocusListText(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclickListText(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRdblclkListText(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangingListText(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangedListText(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusListText(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeEditMins();
	afx_msg void OnChangeEditNumchars();
	afx_msg void OnChangeEditTimesplayed();
	afx_msg void OnKillfocusRicheditView(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg BOOL OnToolTipNotify( UINT id, NMHDR* pNMHDR, LRESULT* pResult );
	afx_msg void OnColumnclickListDatastack(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCheckCycle();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WHEELDLG_H__F935B8A6_4FC2_4AB9_A958_9A2C93601BBC__INCLUDED_)
