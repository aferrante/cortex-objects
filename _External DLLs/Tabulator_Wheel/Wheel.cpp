// Wheel.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "Wheel.h"
#include "..\..\..\Cortex Objects\Tabulator\TabulatorDefines.h"
#include "..\..\..\Cortex Objects\Tabulator\TabulatorData.h"
#include <process.h>
#include "..\..\..\Common\MFC\ODBC\DBUtil.h"
#include "..\..\..\Common\TXT\BufferUtil.h"
#include <objsafe.h>
#include <atlbase.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

#define WHEELDLG_MINSIZEX 200
#define WHEELDLG_MINSIZEY 150


#ifndef REMOVE_CLIENTSERVER
void DataDownloadThread(void* pvArgs);
#endif //#ifndef REMOVE_CLIENTSERVER
void TickerEngineThread(void* pvArgs);
void SynchronizationThread(void* pvArgs);


/////////////////////////////////////////////////////////////////////////////
// CWheelApp

BEGIN_MESSAGE_MAP(CWheelApp, CWinApp)
	//{{AFX_MSG_MAP(CWheelApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWheelApp construction

CWheelApp::CWheelApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	AfxInitRichEdit();
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CWheelApp object

CWheelApp				theApp;
CWheelCore			g_core;
CWheelData			g_data;
CWheelDlg*			g_pdlg = NULL;
CWheelSettings	g_settings;
CMessager*			g_ptabmsgr = NULL;
DLLdata_t       g_dlldata;



int  CWheelApp::DLLCtrl(void** ppvoid, UINT nType)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];

	char szFeedSource[MAX_PATH]; sprintf(szFeedSource, "%s_DataDownload", g_settings.m_pszInternalAppName);
	char szTickerSource[MAX_PATH]; sprintf(szTickerSource, "%s_Ticker", g_settings.m_pszInternalAppName);
	int nReturn = CLIENT_SUCCESS;
	switch(nType)
	{
	case DLLCMD_MAINDLG_BEGIN://					0x00
		{
			//gets the address of a Dlg pointer.
			if(!g_data.m_bDialogStarted)
			{
				g_pdlg = new CWheelDlg;

				if(*ppvoid) // we should be passing in the parent Wnd.
				{
//	AfxMessageBox("Foo");

					if(g_pdlg->Create((CWnd*)(*ppvoid))) 
					{
//	AfxMessageBox("Foox");
						g_data.m_bDialogStarted = true;
						if (g_pdlg)
						{
							*ppvoid = g_pdlg;
							g_dlldata.pDlg = g_pdlg; // if there is a dialog....

	if(g_pdlg->MessageBox("Would you like to set the Client to request only the most recent feed records?",
		"Connection Option",
		MB_ICONQUESTION|MB_YESNO)==IDYES)
	{
		g_pdlg->m_bDontGetOlderFeedRecords = TRUE;
	}
	else
	{
		g_pdlg->m_bDontGetOlderFeedRecords = FALSE;
	}

	g_pdlg->m_bChoiceMade=TRUE;

						}
						else
						{
							*ppvoid=NULL;
							g_dlldata.pDlg = NULL;
						}

						g_pdlg->ShowWindow(SW_SHOW);
						g_pdlg->ShowWindow(SW_HIDE);
					}
					else
					{
//	AfxMessageBox("Fooz");
						*ppvoid=NULL;
						g_dlldata.pDlg = NULL;
						g_data.m_bDialogStarted = false;
						nReturn = CLIENT_ERROR; //error
					}
				} 
				else 
				{
					*ppvoid=NULL;
					g_dlldata.pDlg = NULL;
					g_data.m_bDialogStarted = false;
					nReturn = CLIENT_ERROR; //error, need the parent wnd
				}
			}

		} break;
	case DLLCMD_MAINDLG_END://						0x01
		{
			if(g_data.m_bDialogStarted)
			{
				g_data.m_bDialogStarted = false;
				if(g_pdlg) g_pdlg->OnExit();
			}
		} break;
	case DLLCMD_SETTINGSTXT://						0x02
		{
			if(ppvoid) // we should be passing in the address of a char*.
			{
				if((g_settings.m_pszSettingsText)&&(strlen(g_settings.m_pszSettingsText)>0))
				{
					char* pch = (char*)malloc(strlen(g_settings.m_pszSettingsText)+1);
					if(pch)
					{
						strcpy(pch, g_settings.m_pszSettingsText);
						(*ppvoid)= pch;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
				}
				else
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_DOSETTINGSDLG://					0x03
		{
			// takes a NULL pointer.
			if(g_settings.m_bHasDlg)
			{
				g_settings.DoModal();
			}
			else
			{
				// just refresh
			EnterCriticalSection(&g_settings.m_crit);

				if(g_settings.Settings(true)==CLIENT_SUCCESS)
					nReturn = CLIENT_REFRESH;
				else nReturn = CLIENT_ERROR;

			LeaveCriticalSection(&g_settings.m_crit);
			}
		} break;
	case DLLCMD_GETABOUTTXT://						0x04
		{
			if(ppvoid) // we should be passing in the address of a char*.
			{
				if((g_settings.m_pszAboutText)&&(strlen(g_settings.m_pszAboutText)>0))
				{
					char* pch = (char*)malloc(strlen(g_settings.m_pszAboutText)+1);
					if(pch)
					{
						strcpy(pch, g_settings.m_pszAboutText);
						(*ppvoid)= pch;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
				}
				else
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_GETAPPNAME://							0x05
		{
			if(ppvoid) // we should be passing in the address of a char*.
			{
				if((g_settings.m_pszAppName)&&(strlen(g_settings.m_pszAppName)>0))
				{
					char* pch = (char*)malloc(strlen(g_settings.m_pszAppName)+1);
					if(pch)
					{
						strcpy(pch, g_settings.m_pszAppName);
						(*ppvoid)= pch;
					}
					else
					{
						(*ppvoid)= NULL;
						nReturn = CLIENT_ERROR;
					}
				}
				else
				{
					(*ppvoid)= NULL;
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
			
		} break;
	case DLLCMD_SETDISPATCHER://					0x06
		{
			if (*ppvoid)
			{
				g_ptabmsgr = (CMessager*)*ppvoid;
//				CString szMsg; szMsg.Format("%08x dispatcher",g_pmd);
//				AfxMessageBox(szMsg);
//				AfxMessageBox(AfxGetApp()->m_pszAppName);
//				AfxMessageBox(AfxGetApp()->m_pszExeName);

					if(g_settings.m_bIsServer)
					{

						if(g_ptabmsgr) g_ptabmsgr->AddDestination(
							MSG_DESTTYPE_LOG, 
							(((g_settings.m_pszAsRunDestination)&&(strlen(g_settings.m_pszAsRunDestination)))?g_settings.m_pszAsRunDestination:"asrun"), 
							((g_settings.m_pszAsRunFilename!=NULL)?g_settings.m_pszAsRunFilename:"wheel_asrun|YD||1|")
							);
					}
			}
			else
			{
				nReturn = CLIENT_ERROR;
			}  

		} break;
	case DLLCMD_GETMINX://							0x07
		{
			if(ppvoid) // we should be passing in the address of an int.
			{
				(*ppvoid) = (void*)WHEELDLG_MINSIZEX;
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_GETMINY://							0x08
		{
			if(ppvoid) // we should be passing in the address of an int.
			{
				(*ppvoid) = (void*)WHEELDLG_MINSIZEY;
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_GETDATAOBJ://							0x09  // gets a pointer to the main data object
		{
			if(ppvoid) // we should be passing in the address of an data object.
			{
				(*ppvoid) = (void*)(&g_dlldata);
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_CALLFUNC://								0x0a  // call a function....  
		{
			if(ppvoid) // we should be passing in the address of an char buffer.
			{
				char* pch = (char*)(*ppvoid);
				if((pch)&&(strlen(pch)))
				{
					// format is func name|arg|arg|arg  where if arg is a string, it is pipe encoded.
					char* pchDelim = strchr(pch, '|');
					if(pchDelim) *pchDelim = 0; //temporary null term.

		//			AfxMessageBox(pch);

					
					if(strcmp(pch, "Server_Get_Status")==0)
					{
						if((g_dlldata.nNumThreads>WHEEL_TICKER)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA])&&(g_dlldata.thread[WHEEL_TICKER]))
						{
								// status return shall be:

							CString  szData;
							szData.Format("%d.%03d|%.3f|%d|%d.%03d|%.3f|%d|%d|%d|%s|%s", 
												g_dlldata.thread[WHEEL_DATA]->timebTick.time,
												g_dlldata.thread[WHEEL_DATA]->timebTick.millitm,
												g_data.m_dblLastDataOutUpdate,
												g_dlldata.thread[WHEEL_DATA]->nThreadState,
												g_dlldata.thread[WHEEL_TICKER]->timebTick.time,
												g_dlldata.thread[WHEEL_TICKER]->timebTick.millitm,
												g_data.m_dblLastTickerOutUpdate,
												g_dlldata.thread[WHEEL_TICKER]->nThreadState,
												g_data.m_nMessagesOut,
												g_data.m_nTickerMessagesOut,
												g_dlldata.thread[WHEEL_DATA]->pszThreadStateText, //256 chars max
												g_dlldata.thread[WHEEL_TICKER]->pszThreadStateText //256 chars max
							
								);

								// time|datalastupdate|datastate|time|tickerlastupdate|tickerstate

							pch =  (char*)malloc(szData.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szData);
								*ppvoid = pch;
							}
						}
						else
						{
							nReturn = CLIENT_ERROR;
						}
					}
					else
					if(strcmp(pch, "Server_Data_Start")==0)
					{
						if((g_dlldata.nNumThreads>WHEEL_DATA)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA]))
						{
							g_dlldata.thread[WHEEL_DATA]->nThreadControl = WHEEL_DATA_STARTED;
//data not supported							g_dlldata.thread[WHEEL_DATA]->nThreadState |= WHEEL_DATA_INPROG;
						}
						else
						{
							nReturn = CLIENT_ERROR;
						}
					}
					else
					if(strcmp(pch, "Server_Data_Stop")==0)
					{
						if((g_dlldata.nNumThreads>WHEEL_DATA)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA]))
						{
							g_dlldata.thread[WHEEL_DATA]->nThreadControl = WHEEL_DATA_STOPPED;
//data not supported							g_dlldata.thread[WHEEL_DATA]->nThreadState |= WHEEL_DATA_INPROG;
						}
						else
						{
							nReturn = CLIENT_ERROR;
						}
					}
					else
					if(strcmp(pch, "Server_Data_Pause")==0)
					{
						if((g_dlldata.nNumThreads>WHEEL_DATA)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA]))
						{
							g_dlldata.thread[WHEEL_DATA]->nThreadControl |= WHEEL_DATA_PAUSED;
//							g_dlldata.thread[WHEEL_DATA]->nThreadState |= WHEEL_DATA_INPROG;
						}
						else
						{
							nReturn = CLIENT_ERROR;
						}
					}
					else
					if(strcmp(pch, "Server_Data_Resume")==0)
					{
						if((g_dlldata.nNumThreads>WHEEL_DATA)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA]))
						{
							g_dlldata.thread[WHEEL_DATA]->nThreadControl &= ~WHEEL_DATA_PAUSED;
//							g_dlldata.thread[WHEEL_DATA]->nThreadState |= WHEEL_DATA_INPROG;
						}
						else
						{
							nReturn = CLIENT_ERROR;
						}
					}

					else
					if(strcmp(pch, "Server_Data_Clear")==0)
					{
						bool bReset = ((atoi(pchDelim+1)!=0)?1:0);

						if((g_dlldata.thread[WHEEL_DATA]->bThreadStarted)&&(g_dlldata.thread[WHEEL_DATA]->nThreadState&WHEEL_DATA_STARTED))
						{
							g_dlldata.thread[WHEEL_DATA]->nThreadControl = WHEEL_DATA_STOPPED;
							while(((g_dlldata.thread[WHEEL_DATA]->nThreadState&WHEEL_DATA_STARTED)!=WHEEL_DATA_STOPPED)&&(!g_dlldata.thread[WHEEL_DATA]->bKillThread))
							{
								Sleep(10);
							}
						}

						g_data.DeleteAllMessages();

						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", g_settings.m_pszFeed);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "inserting id %d to SQL with [%s]", msg.m_nID, szSQL);// Sleep(100); //(Dispatch message)
//	AfxMessageBox(szSQL);
EnterCriticalSection(&g_data.m_critSQL);

						if((g_data.m_pdb)&&(g_data.m_pdbConn))	
						{
							if (g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
							{
								//err
if(g_ptabmsgr)
{
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "ERROR deleting all IDs [%s] for [%s]", errorstring, szSQL);// Sleep(100); //(Dispatch message)
}

							}
						}

LeaveCriticalSection(&g_data.m_critSQL);

						if(bReset)
						{
							g_data.m_nLastID=1;
						}

						_timeb now;
						_ftime(&now);

						g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate = (double)now.time + ((double)(now.millitm)/1000.0);

						g_data.PublishData();
	
					}
					else
#ifndef REMOVE_CLIENTSERVER
					if(strcmp(pch, "Server_Data_Revision")==0)
					{
//	AfxMessageBox("Server_Data_Revision");
						if(pchDelim)
						{
//	AfxMessageBox(pchDelim+1);

							CSafeBufferUtil sbu;
							char chMessage[2];
							char chField[2];
							sprintf(chMessage, "%c", 29); //GS
							sprintf(chField, "%c", 28);  //FS
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), chMessage, MODE_MULTIDELIM);

							int n=0;
							_timeb now;

							while(pch)
							{
								switch(n)
								{
								case 0: 
									{
										n=atoi(pch);
									} break;
								default:
									{
										CWheelMessage* pmsg = new CWheelMessage;
										if(pmsg)
										{
											
//	AfxMessageBox(pch);

											if(g_data.StringToMessage(pch, pmsg)>=CLIENT_SUCCESS)
											{
//	AfxMessageBox("StringToMessage done");

												EnterCriticalSection(&g_data.m_critMsg);
//	AfxMessageBox("FindID");
												int q = g_data.FindID(pmsg->m_nID, g_data.m_nLastFoundIndex);
//	AfxMessageBox("FindID done");

												if(q<0)
												{
													// not found, must add
													LeaveCriticalSection(&g_data.m_critMsg);
//	AfxMessageBox("inserting");

													if(!(pmsg->m_nStatus&WHEEL_MSG_MARK_DEL))
													{

														g_data.InsertMessage(pmsg);
	//	AfxMessageBox("inserted");
														_ftime(&now);
														pmsg->m_nLastUpdate = now.time+1;


														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", pmsg->SQLUpdate(false, 0.0)	);
		//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "inserting id %d to SQL with [%s]", msg.m_nID, szSQL);// Sleep(100); //(Dispatch message)
	//	AfxMessageBox(szSQL);

		//	AfxMessageBox(szSQL);
EnterCriticalSection(&g_data.m_critSQL);

														if((g_data.m_pdb)&&(g_data.m_pdbConn))	
														{
															if (g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
															{
																//err
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "ERROR inserting id %d to SQL [%s] for [%s]", pmsg->m_nID, errorstring, szSQL);// Sleep(100); //(Dispatch message)

															}
															else
															{
																pmsg->m_nStatus |= WHEEL_MSG_STATUS_INSERTED;
			//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "inserted id %d to SQL with [%s]", msg.m_nID, szSQL);// Sleep(100); //(Dispatch message)
															}
														}
LeaveCriticalSection(&g_data.m_critSQL);
													}
													//else nothing!
												}
												else
												{
													//found, must modify.
//	AfxMessageBox("modifying");

													if(pmsg->m_nStatus&WHEEL_MSG_MARK_DEL)
													{
														
														g_data.DeleteMessage(q);
														LeaveCriticalSection(&g_data.m_critMsg);
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE [id]=%d", g_settings.m_pszFeed, pmsg->m_nID);
		//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "inserting id %d to SQL with [%s]", msg.m_nID, szSQL);// Sleep(100); //(Dispatch message)
//		AfxMessageBox(szSQL);

		//	AfxMessageBox(szSQL);
EnterCriticalSection(&g_data.m_critSQL);

														if((g_data.m_pdb)&&(g_data.m_pdbConn))	
														{
															if (g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
															{
																//err
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "ERROR deleting id %d [%s] for [%s]", pmsg->m_nID, errorstring, szSQL);// Sleep(100); //(Dispatch message)

															}
														}


LeaveCriticalSection(&g_data.m_critSQL);

													}
													else
													{

														if(g_data.m_msg[q])
														{
	//	AfxMessageBox("equating");
															*g_data.m_msg[q] = *pmsg;
	//	AfxMessageBox("equated");
															delete pmsg; pmsg=NULL;
														}
														else
														{
															g_data.m_msg[q] = pmsg;
														}
														LeaveCriticalSection(&g_data.m_critMsg);


														_ftime(&now);
														g_data.m_msg[q]->m_nLastUpdate = now.time+1;

														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", g_data.m_msg[q]->SQLUpdate(false, 0.0)	);
		//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "inserting id %d to SQL with [%s]", msg.m_nID, szSQL);// Sleep(100); //(Dispatch message)
//		AfxMessageBox(szSQL);

		//	AfxMessageBox(szSQL);
EnterCriticalSection(&g_data.m_critSQL);

														if((g_data.m_pdb)&&(g_data.m_pdbConn))	
														{
															if (g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
															{
																//err
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "ERROR inserting id %d to SQL [%s] for [%s]", g_data.m_msg[q]->m_nID, errorstring, szSQL);// Sleep(100); //(Dispatch message)

															}
															else
															{
																g_data.m_msg[q]->m_nStatus |= WHEEL_MSG_STATUS_INSERTED;
			//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "inserted id %d to SQL with [%s]", msg.m_nID, szSQL);// Sleep(100); //(Dispatch message)
															}
														}


LeaveCriticalSection(&g_data.m_critSQL);

													}
												}


											}
										}
									} break;
								}
							
							//	n++;

								pch = sbu.Token(NULL, NULL, chMessage, MODE_MULTIDELIM);
							}
//	AfxMessageBox("end");

							if((n>0)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA]))
							{
								g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate = (double)now.time + ((double)(now.millitm)/1000.0);
								g_data.PublishData();
							}
//	AfxMessageBox("updated");

						}
					}
					else
#endif //#ifndef REMOVE_CLIENTSERVER
					if(strcmp(pch, "Server_Data_Get")==0)
					{
						if(pchDelim)
						{
							int nTime = -1;
							int nSkip = 0;
							CSafeBufferUtil sbu;
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_MULTIDELIM);

							int n=0;

							while((pch)&&(n<2))
							{
								switch(n)
								{
								case 0: 
									{
										nTime=atoi(pch);
									} break;
								case 1: 
									{
										nSkip=atoi(pch);
									} break;
								}
							
								n++;

								pch = sbu.Token(NULL, NULL, "|", MODE_MULTIDELIM);
							}
							CString szData;
							nReturn = g_data.GetMessagesSince(nTime, &szData, nSkip);

							pch =  (char*)malloc(szData.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szData);
								*ppvoid = pch;
							}
							else
							{
								nReturn = CLIENT_ERROR;
							}
						}
						else
						{
							nReturn = CLIENT_ERROR;
						}

					}
					else
					if(strcmp(pch, "Server_Refresh_Setting")==0)
					{
			EnterCriticalSection(&g_settings.m_crit);
						g_settings.Settings(true);						
			LeaveCriticalSection(&g_settings.m_crit);
					}
					else
					if(strcmp(pch, "Server_Set_Setting")==0)
					{
	char szSettingSource[MAX_PATH]; sprintf(szSettingSource, "%s_DLL_settings", g_settings.m_pszInternalAppName);

						CString szSettings;
						szSettings.Format("%s", (pchDelim+1));
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSettingSource, "using [%s]",szSettings); //(Dispatch message)
						nReturn = g_settings.StringToRemoteSettings(szSettings);
			EnterCriticalSection(&g_settings.m_crit);
						g_settings.Settings(false);
			LeaveCriticalSection(&g_settings.m_crit);
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSettingSource, "set: [%s]",g_settings.RemoteSettingsToString()); //(Dispatch message)

					}
					else
					if(strcmp(pch, "Server_Get_Setting")==0)
					{
						CString szSettings = g_settings.RemoteSettingsToString();
						nReturn = szSettings.GetLength();
		//						AfxMessageBox(szSettings);
				
						pch =  (char*)malloc(nReturn+1);
						if(pch)
						{
							sprintf(pch, "%s", szSettings);
							*ppvoid = pch;
						}
					}
					else

					// had to add built in server side ticker engine control
					if(strcmp(pch, "Server_Ticker_In")==0)
					{
						if((g_dlldata.nNumThreads>WHEEL_TICKER)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_TICKER]))
						{
							g_dlldata.thread[WHEEL_TICKER]->nThreadControl = WHEEL_TICKER_STARTED;
							g_dlldata.thread[WHEEL_TICKER]->nThreadState |= WHEEL_TICKER_INPROG;
						}
						else
						{
							nReturn = CLIENT_ERROR;
						}
					}
					else
					if(strcmp(pch, "Server_Ticker_Pause")==0)
					{
						if((g_dlldata.nNumThreads>WHEEL_TICKER)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_TICKER]))
						{
							g_dlldata.thread[WHEEL_TICKER]->nThreadState |= WHEEL_TICKER_PAUSED;
//							g_dlldata.thread[WHEEL_TICKER]->nThreadState |= WHEEL_TICKER_INPROG;
						}
						else
						{
							nReturn = CLIENT_ERROR;
						}
					}
					else
					if(strcmp(pch, "Server_Ticker_Resume")==0)
					{
						if((g_dlldata.nNumThreads>WHEEL_TICKER)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_TICKER]))
						{
							g_dlldata.thread[WHEEL_TICKER]->nThreadState &= ~WHEEL_TICKER_PAUSED;
//							g_dlldata.thread[WHEEL_TICKER]->nThreadState |= WHEEL_TICKER_INPROG;
						}
						else
						{
							nReturn = CLIENT_ERROR;
						}
					}
					else
					if(strcmp(pch, "Server_Ticker_Cycle")==0)
					{
						if((g_dlldata.nNumThreads>WHEEL_TICKER)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_TICKER]))
						{
							if(g_dlldata.thread[WHEEL_TICKER]->nThreadState&WHEEL_TICKER_CYCLING)
							{
								g_dlldata.thread[WHEEL_TICKER]->nThreadState &= ~WHEEL_TICKER_CYCLING;
							}
							else
							{
								g_dlldata.thread[WHEEL_TICKER]->nThreadState |= WHEEL_TICKER_CYCLING;
							}
						}
						else
						{
							nReturn = CLIENT_ERROR;
						}
					}
					else
					if(strcmp(pch, "Server_Ticker_Out")==0)
					{
						if((g_dlldata.nNumThreads>WHEEL_TICKER)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_TICKER]))
						{
							g_dlldata.thread[WHEEL_TICKER]->nThreadControl = WHEEL_TICKER_STOPPED;
							g_dlldata.thread[WHEEL_TICKER]->nThreadState |= WHEEL_TICKER_INPROG;
						}
						else
						{
							nReturn = CLIENT_ERROR;
						}
					}
					else
#ifndef REMOVE_CLIENTSERVER
					if(strcmp(pch, "Server_Ticker_Commit")==0)
					{

/*
					_snprintf(pch, MAX_MESSAGE_LENGTH-1, "%s|Server_Ticker_Commit|1|%d|%.4f|%d|%s", g_settings.m_pszInternalAppName,  //1 is not a default.
						g_data.m_nCommitInsertBeforeID, 
						g_data.m_dblCommitBeforeIndex,
						g_data.m_nCommitID,
						g_data.m_szCommitData);
*/
						if(pchDelim)
						{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker Commit {%s}", pchDelim+1);// Sleep(100); //(Dispatch message)
							CSafeBufferUtil sbu;
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_MULTIDELIM);

							int n=0;
							_timeb now;

							bool bDefault = FALSE;
							int nCommitInsertBeforeID=-1;
							double dblCommitBeforeIndex=-0.001;
							CWheelMessage* pmsg = new CWheelMessage;
							if(pmsg)
							{

								while((pch)&&(n<5))
								{
									switch(n)
									{
									case 0: 
										{
											bDefault=((atoi(pch)==0)?true:false);
								//			pmsg->m_nType = (bDefault?WHEEL_MSG_TYPE_DEFAULT:WHEEL_MSG_STATUS_NONE);  // don't need this part - reusing type for disabled now.
										} break;
									case 1: 
										{
											nCommitInsertBeforeID=atoi(pch);
										} break;
									case 2: 
										{
											dblCommitBeforeIndex=atof(pch);
										} break;
									case 3: 
										{
											pmsg->m_nID=atoi(pch);
										} break;
									case 4: 
										{
											pmsg->m_szMessage=g_data.Decode(pch);
										} break;
									}
								
									n++;

									pch = sbu.Token(NULL, NULL, "|", MODE_MULTIDELIM);
								}
	//	AfxMessageBox("end");

								if(n>4)// otherwise not valid
								{
									if(bDefault) // get a new ID.
									{
		EnterCriticalSection(&g_data.m_critDefaultID);
										g_data.m_nTickerIDMaxNeg--;
										pmsg->m_nID = g_data.m_nTickerIDMaxNeg;
		LeaveCriticalSection(&g_data.m_critDefaultID);

									}
									if(nCommitInsertBeforeID==-1) // put at end.
									{
										_ftime( &now );
										
										pmsg->m_dblIndex = ((double)((now.time-1198108800)*1000.0) + (double)(now.millitm));
									}
									else
									{//retrieve the previous index (0.000 if none), then take average to now.

										double dblPreviousToCommitBeforeIndex=0.0000000000;

										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 [sort_order] FROM %s WHERE [sort_order] < %.4f AND [id] %s 0 ORDER BY [sort_order] DESC",	
											g_settings.m_pszTicker, dblCommitBeforeIndex, (bDefault?"<":">")
											);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "inserting id %d to SQL with [%s]", msg.m_nID, szSQL);// Sleep(100); //(Dispatch message)
//	AfxMessageBox(szSQL);

//	AfxMessageBox(szSQL);
EnterCriticalSection(&g_data.m_critSQL);

										if((g_data.m_pdb)&&(g_data.m_pdbConn))	
										{

											CRecordset* prs = g_data.m_pdb->Retrieve(g_data.m_pdbConn, szSQL, errorstring);
											if(prs)
											{
												CString szTemp;
												int nReturn = CLIENT_ERROR;
												if (!prs->IsEOF())
												{
													try
													{
														prs->GetFieldValue("index", szTemp);//HARDCODE
														if(szTemp.GetLength())
														{
															dblPreviousToCommitBeforeIndex=atof(szTemp);
														}
													}
													catch(...)
													{
													}
												}
												prs->Close();
												delete prs;
											}

LeaveCriticalSection(&g_data.m_critSQL);

										}
										pmsg->m_dblIndex = (dblCommitBeforeIndex + dblPreviousToCommitBeforeIndex)/2.0;
									}
									g_data.InsertTickerMessage(pmsg);
//	AfxMessageBox("inserted");
									_ftime( &now );

									pmsg->m_nLastUpdate = now.time+1;
									pmsg->m_nTimestamp = pmsg->m_nLastUpdate;

								
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", pmsg->SQLUpdate(true, pmsg->m_dblIndex)	);

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "***** inserting id %d to SQL [%s]", pmsg->m_nID, szSQL);// Sleep(100); //(Dispatch message)
									if((g_data.m_pdb)&&(g_data.m_pdbConn))	
									{
EnterCriticalSection(&g_data.m_critSQL);

										if (g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
										{
LeaveCriticalSection(&g_data.m_critSQL);

											//err
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "ERROR inserting id %d to SQL [%s] for [%s]", pmsg->m_nID, errorstring, szSQL);// Sleep(100); //(Dispatch message)

										}
										else
										{
LeaveCriticalSection(&g_data.m_critSQL);

											pmsg->m_nStatus |= WHEEL_MSG_STATUS_INSERTED;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "inserted id %d to SQL with [%s]", msg.m_nID, szSQL);// Sleep(100); //(Dispatch message)
// now lets find it in the data

											if(!bDefault)
											{
								EnterCriticalSection(&g_data.m_critMsg);
//	AfxMessageBox("FindID");
												int q = g_data.FindID(pmsg->m_nID, g_data.m_nLastFoundIndex);
//	AfxMessageBox("FindID done");x
												if(q>=0)
												{
													//found, must modify.
//	AfxMessageBox("modifying");
													if(g_data.m_msg[q])
													{
//	AfxMessageBox("equating");
														g_data.m_msg[q]->m_nPlayed = pmsg->m_nPlayed;
														g_data.m_msg[q]->m_nStatus |= WHEEL_MSG_STATUS_SENT;
//	AfxMessageBox("equated");
													
														_ftime(&now);
														g_data.m_msg[q]->m_nLastUpdate = now.time+1;

														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", g_data.m_msg[q]->SQLUpdate(false, 0.0)	);
														
								LeaveCriticalSection(&g_data.m_critMsg);

		//	AfxMessageBox(szSQL);
EnterCriticalSection(&g_data.m_critSQL);
														if (g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
														{
															//err
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "ERROR updating id %d with error [%s] for [%s]", g_data.m_msg[q]->m_nID, errorstring, szSQL);// Sleep(100); //(Dispatch message)

														}
														else
														{
															g_data.m_msg[q]->m_nStatus |= WHEEL_MSG_STATUS_INSERTED;
		//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "inserted id %d to SQL with [%s]", msg.m_nID, szSQL);// Sleep(100); //(Dispatch message)
														}
LeaveCriticalSection(&g_data.m_critSQL);

													}
													else
													{
								LeaveCriticalSection(&g_data.m_critMsg);
													}
												}
												else
												{
								LeaveCriticalSection(&g_data.m_critMsg);
												}
											}

										}
									}
								}
								else
								{
									delete pmsg;
									nReturn = CLIENT_ERROR;
								}

								if((g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA])&&(g_dlldata.thread[WHEEL_TICKER]))
								{
									_ftime(&now);
									g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate = (double)now.time + ((double)(now.millitm)/1000.0);
									g_dlldata.thread[WHEEL_TICKER]->m_dblLastUpdate = g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate;
									g_data.PublishData();
									g_data.PublishTicker();

								}
							}
							else
							{
								nReturn = CLIENT_ERROR;
							}
//	AfxMessageBox("updated");

						}
						else
						{
							nReturn = CLIENT_ERROR;
						}
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "exit Ticker Commit ");// Sleep(100); //(Dispatch message)

					}
					else
					if(strcmp(pch, "Server_Ticker_Move")==0)
					{
/*
		_snprintf(pch, MAX_MESSAGE_LENGTH-1, "%s|Server_Ticker_Move|%d|%d|%.4f|%d|%.4f",  g_settings.m_pszInternalAppName,  //1 is not a default.
						(bDefault?0:1),
						g_data.m_nCommitInsertBeforeID, 
						g_data.m_dblCommitBeforeIndex,
						g_data.m_nCommitID,
						g_data.m_dblCommitIndex
						);
*/
						if(pchDelim)
						{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "moving [%s] ",pchDelim+1);// Sleep(100); //(Dispatch message)

							CSafeBufferUtil sbu;
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_MULTIDELIM);

							int n=0;
							_timeb now;

							bool bDefault = FALSE;
							int nCommitInsertBeforeID=-1;
							double dblCommitBeforeIndex=-0.001;
							int nCommitID=-1;
							double dblCommitIndex=-0.001;
							double dblNewIndex=-0.001;

							while((pch)&&(n<5))
							{
								switch(n)
								{
								case 0: 
									{
										bDefault=((atoi(pch)==0)?true:false);
									} break;
								case 1: 
									{
										nCommitInsertBeforeID=atoi(pch);
									} break;
								case 2: 
									{
										dblCommitBeforeIndex=atof(pch);
									} break;
								case 3: 
									{
										nCommitID=atoi(pch);
									} break;
								case 4: 
									{
										dblCommitIndex=atof(pch);
									} break;
								}
							
								n++;

								pch = sbu.Token(NULL, NULL, "|", MODE_MULTIDELIM);
							}
//	AfxMessageBox("end");

							if((n>4)&&(nCommitID!=-1))// otherwise not valid
							{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "moving %d front of %d", nCommitID, nCommitInsertBeforeID);// Sleep(100); //(Dispatch message)
								if(nCommitInsertBeforeID==-1) // put at end.
								{
									_ftime( &now );
									
									dblNewIndex = ((double)((now.time-1198108800)*1000.0) + (double)(now.millitm));
								}
								else
								{//retrieve the previous index (0.000 if none), then take average to now.

									double dblPreviousToCommitBeforeIndex=0.0000000000;

									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 [sort_order] FROM %s WHERE [sort_order] < %.4f AND [id] %s 0 ORDER BY [sort_order] DESC",	
										g_settings.m_pszTicker, dblCommitBeforeIndex, (bDefault?"<":">")
										);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "inserting id %d to SQL with [%s]", msg.m_nID, szSQL);// Sleep(100); //(Dispatch message)
//	AfxMessageBox(szSQL);
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "moving SQL [%s]",szSQL);// Sleep(100); //(Dispatch message)

//	AfxMessageBox(szSQL);
EnterCriticalSection(&g_data.m_critSQL);

									if((g_data.m_pdb)&&(g_data.m_pdbConn))	
									{

										CRecordset* prs = g_data.m_pdb->Retrieve(g_data.m_pdbConn, szSQL, errorstring);
										if(prs)
										{
											CString szTemp;
											int nReturn = CLIENT_ERROR;
											if (!prs->IsEOF())
											{
												try
												{
													prs->GetFieldValue("index", szTemp);//HARDCODE
													if(szTemp.GetLength())
													{
														dblPreviousToCommitBeforeIndex=atof(szTemp);
													}
												}
												catch(...)
												{
												}
											}
											prs->Close();
											delete prs;
										}

LeaveCriticalSection(&g_data.m_critSQL);

									}
									dblNewIndex = (dblCommitBeforeIndex + dblPreviousToCommitBeforeIndex)/2.0;
								}

								CWheelMessage* pmsg = NULL; 
								EnterCriticalSection(&g_data.m_critTicker);
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "moving finding");// Sleep(100); //(Dispatch message)

								int q=g_data.FindTickerID(nCommitID, dblCommitIndex);
								if(q>=0)
								{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "moving found [%d]",q);// Sleep(100); //(Dispatch message)
									pmsg = g_data.m_ticker[q];
								

	//	AfxMessageBox("inserted");
								
									dblCommitBeforeIndex =  pmsg->m_dblIndex;
									pmsg->m_dblIndex = dblNewIndex;

									_ftime( &now );
									pmsg->m_nLastUpdate = now.time+1;
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", pmsg->SQLUpdate(true, dblCommitBeforeIndex)	);

									LeaveCriticalSection(&g_data.m_critTicker);
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "moving modify [%s]",szSQL);// Sleep(100); //(Dispatch message)

	//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "***** inserting id %d to SQL [%s]", pmsg->m_nID, szSQL);// Sleep(100); //(Dispatch message)
									if((g_data.m_pdb)&&(g_data.m_pdbConn))	
									{
EnterCriticalSection(&g_data.m_critSQL);

										if (g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
										{
LeaveCriticalSection(&g_data.m_critSQL);

											//err
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "ERROR inserting id %d to SQL [%s] for [%s]", pmsg->m_nID, errorstring, szSQL);// Sleep(100); //(Dispatch message)

										}
										else
										{
LeaveCriticalSection(&g_data.m_critSQL);

											pmsg->m_nStatus |= WHEEL_MSG_STATUS_INSERTED;
	//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "inserted id %d to SQL with [%s]", msg.m_nID, szSQL);// Sleep(100); //(Dispatch message)
	// now lets find it in the data
										}
									}
								}
								else
								{
									LeaveCriticalSection(&g_data.m_critTicker);
								}
							}
							else
							{
								nReturn = CLIENT_ERROR;
							}

							if((g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA])&&(g_dlldata.thread[WHEEL_TICKER]))
							{
								_ftime(&now);
//								g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate = (double)now.time + ((double)(now.millitm)/1000.0);
								g_dlldata.thread[WHEEL_TICKER]->m_dblLastUpdate = (double)now.time + ((double)(now.millitm)/1000.0);
//								g_dlldata.thread[WHEEL_TICKER]->m_dblLastUpdate = g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate;
//								g_data.PublishData();
								g_data.PublishTicker();
							}
//	AfxMessageBox("updated");

						}
						else
						{
							nReturn = CLIENT_ERROR;
						}

					}
					else
					if(strcmp(pch, "Server_Ticker_Delete")==0)
					{
/*
					_snprintf(pch, MAX_MESSAGE_LENGTH-1, "%s|Server_Ticker_Delete|%d|%.4f", 
					g_settings.m_pszInternalAppName, 
					g_data.m_nCommitID, 
					g_data.m_dblCommitIndex);
*/
						if(pchDelim)
						{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "deleting [%s] ",pchDelim+1);// Sleep(100); //(Dispatch message)
							CSafeBufferUtil sbu;
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_MULTIDELIM);

							int n=0;
							_timeb now;

							int nCommitID=-1;
							double dblCommitIndex=-0.001;

							while((pch)&&(n<2))
							{
								switch(n)
								{
								case 0: 
									{
										nCommitID=atoi(pch);
									} break;
								case 1: 
									{
										dblCommitIndex=atof(pch);
									} break;
								}
							
								n++;

								pch = sbu.Token(NULL, NULL, "|", MODE_MULTIDELIM);
							}
//	AfxMessageBox("end");

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "deleting");// Sleep(100); //(Dispatch message)
							if(n>1)// otherwise not valid
							{

								EnterCriticalSection(&g_data.m_critTicker);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "finding");// Sleep(100); //(Dispatch message)
								int q = g_data.FindTickerID(nCommitID, dblCommitIndex);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "found %d", q);// Sleep(100); //(Dispatch message)
								if(q>=0)
								{
									//found, must delete.
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "found status 0x%08x", g_data.m_ticker[q]->m_nStatus);// Sleep(100); //(Dispatch message)
									if(!(g_data.m_ticker[q]->m_nStatus&(WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_COMMIT)))
									{
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE [id]=%d AND [sort_order] >= %.4f AND [sort_order] <= %.4f",
											g_settings.m_pszTicker, 
											nCommitID,
											dblCommitIndex-0.0000009,
											dblCommitIndex+0.0000009
											);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "***** deleting: SQL [%s]", szSQL);// Sleep(100); //(Dispatch message)

										g_data.DeleteTickerMessage(q);
										LeaveCriticalSection(&g_data.m_critTicker);
										if((g_data.m_pdb)&&(g_data.m_pdbConn))	
										{

	//	AfxMessageBox(szSQL);
EnterCriticalSection(&g_data.m_critSQL);
													if (g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
													{
														//err
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "ERROR deleting: [%s] for [%s]", errorstring, szSQL);// Sleep(100); //(Dispatch message)

													}
													else
													{
										//				g_data.m_msg[q]->m_nStatus |= WHEEL_MSG_STATUS_INSERTED; // cant do this, we deleted it already!
	//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "deleted using SQL [%s]",, szSQL);// Sleep(100); //(Dispatch message)
													}
LeaveCriticalSection(&g_data.m_critSQL);

										}

									}
									else
									{
										LeaveCriticalSection(&g_data.m_critTicker);
									}

								}
								else
								{
									nReturn = CLIENT_ERROR;
									LeaveCriticalSection(&g_data.m_critTicker);
								}

							}
							else
							{
								nReturn = CLIENT_ERROR;
							}

							if((g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA])&&(g_dlldata.thread[WHEEL_TICKER]))
							{
								_ftime(&now);
								g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate = (double)now.time + ((double)(now.millitm)/1000.0);
								g_dlldata.thread[WHEEL_TICKER]->m_dblLastUpdate = g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate;
								g_data.PublishData();
								g_data.PublishTicker();
							}
						}
						else
						{
							nReturn = CLIENT_ERROR;
						}
//	AfxMessageBox("updated");

					}
					else
					if(strcmp(pch, "Server_Ticker_Activate")==0) // toggle...
					{
/*
					_snprintf(pch, MAX_MESSAGE_LENGTH-1, "%s|Server_Ticker_Activate|%d|%.4f", 
					g_settings.m_pszInternalAppName, 
					g_data.m_nCommitID, 
					g_data.m_dblCommitIndex);
*/
						if(pchDelim)
						{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "toggling active status [%s] ",pchDelim+1);// Sleep(100); //(Dispatch message)
							CSafeBufferUtil sbu;
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_MULTIDELIM);

							int n=0;
							_timeb now;

							int nCommitID=-1;
							double dblCommitIndex=-0.001;

							while((pch)&&(n<2))
							{
								switch(n)
								{
								case 0: 
									{
										nCommitID=atoi(pch);
									} break;
								case 1: 
									{
										dblCommitIndex=atof(pch);
									} break;
								}
							
								n++;

								pch = sbu.Token(NULL, NULL, "|", MODE_MULTIDELIM);
							}
//	AfxMessageBox("end");

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "deleting");// Sleep(100); //(Dispatch message)
							if(n>1)// otherwise not valid
							{

								EnterCriticalSection(&g_data.m_critTicker);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "finding");// Sleep(100); //(Dispatch message)
								int q = g_data.FindTickerID(nCommitID, dblCommitIndex);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "found %d", q);// Sleep(100); //(Dispatch message)
								if(q>=0)
								{
									//found, must delete.
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "found status 0x%08x", g_data.m_ticker[q]->m_nStatus);// Sleep(100); //(Dispatch message)
									if(g_data.m_ticker[q]->m_nType&WHEEL_MSG_TYPE_DISABLED)
									{
										g_data.m_ticker[q]->m_nType &= ~WHEEL_MSG_TYPE_DISABLED;
									}
									else
									{
										g_data.m_ticker[q]->m_nType |= WHEEL_MSG_TYPE_DISABLED;
									}
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET type = %d WHERE [id]=%d AND [sort_order] >= %.4f AND [sort_order] <= %.4f",
										g_settings.m_pszTicker, 
										g_data.m_ticker[q]->m_nType,
										nCommitID,
										dblCommitIndex-0.0000009,
										dblCommitIndex+0.0000009
										);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "***** deleting: SQL [%s]", szSQL);// Sleep(100); //(Dispatch message)

									LeaveCriticalSection(&g_data.m_critTicker);
									if((g_data.m_pdb)&&(g_data.m_pdbConn))	
									{

//	AfxMessageBox(szSQL);
EnterCriticalSection(&g_data.m_critSQL);
												if (g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
												{
													//err
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "ERROR toggling active status: [%s] for [%s]", errorstring, szSQL);// Sleep(100); //(Dispatch message)

												}
												else
												{
												}
LeaveCriticalSection(&g_data.m_critSQL);

									}


								}
								else
								{
									nReturn = CLIENT_ERROR;
									LeaveCriticalSection(&g_data.m_critTicker);
								}

							}
							else
							{
								nReturn = CLIENT_ERROR;
							}

							if((g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA])&&(g_dlldata.thread[WHEEL_TICKER]))
							{
								_ftime(&now);
								g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate = (double)now.time + ((double)(now.millitm)/1000.0);
								g_dlldata.thread[WHEEL_TICKER]->m_dblLastUpdate = g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate;
								g_data.PublishData();
								g_data.PublishTicker();
							}
						}
						else
						{
							nReturn = CLIENT_ERROR;
						}
//	AfxMessageBox("updated");

					}
					else
#endif //#ifndef REMOVE_CLIENTSERVER
					if(strcmp(pch, "Server_Ticker_Clear")==0)
					{

						if((g_dlldata.thread[WHEEL_TICKER]->bThreadStarted)&&(g_dlldata.thread[WHEEL_TICKER]->nThreadState&WHEEL_TICKER_STARTED))
						{
							g_dlldata.thread[WHEEL_TICKER]->nThreadControl = WHEEL_TICKER_STOPPED;
							while(((g_dlldata.thread[WHEEL_TICKER]->nThreadState&WHEEL_TICKER_STARTED)!=WHEEL_TICKER_STOPPED)&&(!g_dlldata.thread[WHEEL_TICKER]->bKillThread))
							{
								Sleep(10);
							}
						}

						int nDefault = 0;
						if(pchDelim) nDefault = atoi(pchDelim+1);

						g_data.DeleteAllTickerMessages(nDefault);

						if(g_data.m_nTickerMessages<=0)
						{
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", g_settings.m_pszTicker);
						}
						else
						{
							if(nDefault==1)
							{
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE [id] < 0 ", g_settings.m_pszTicker);
							}
							else
							{
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE [id] > 0 ", g_settings.m_pszTicker);
							}
						}

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "inserting id %d to SQL with [%s]", msg.m_nID, szSQL);// Sleep(100); //(Dispatch message)
//	AfxMessageBox(szSQL);
EnterCriticalSection(&g_data.m_critSQL);

						if((g_data.m_pdb)&&(g_data.m_pdbConn))	
						{
							if (g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
							{
								//err
								if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "ERROR deleting %s IDs [%s] for [%s]", ((nDefault==1)?"default":"ticker"), errorstring, szSQL);// Sleep(100); //(Dispatch message)

							}
						}

LeaveCriticalSection(&g_data.m_critSQL);
	
						_timeb now;
						_ftime(&now);

						g_dlldata.thread[WHEEL_TICKER]->m_dblLastUpdate = (double)now.time + ((double)(now.millitm)/1000.0);
						g_data.PublishTicker();

					}
					else
#ifndef REMOVE_CLIENTSERVER
					if(strcmp(pch, "Server_Ticker_Get")==0)
					{
						if(pchDelim)
						{
							int nTime = -1;
							int nSkip = 0;
							CSafeBufferUtil sbu;
							pch = sbu.Token(pchDelim+1, strlen(pchDelim+1), "|", MODE_MULTIDELIM);

							int n=0;

							while((pch)&&(n<2))
							{
								switch(n)
								{
								case 0: 
									{
										nTime=atoi(pch);
									} break;
								case 1: 
									{
										nSkip=atoi(pch);
									} break;
								}
							
								n++;

								pch = sbu.Token(NULL, NULL, "|", MODE_MULTIDELIM);
							}
						

							CString szData;
							nReturn = g_data.GetTickerMessagesSince(nTime, &szData, nSkip);

							pch =  (char*)malloc(szData.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szData);
								*ppvoid = pch;
							}
							else
							{
								nReturn = CLIENT_ERROR;
							}
						}
						else
						{
							nReturn = CLIENT_ERROR;
						}
							
					}
					else
#endif //#ifndef REMOVE_CLIENTSERVER


					{
						nReturn = CLIENT_ERROR;
					}
					
					if(pchDelim) *pchDelim = '|'; //replace pipe.

				}
				else 
				{
					nReturn = CLIENT_ERROR;
				}
			}
			else 
			{
				nReturn = CLIENT_ERROR;
			}
		} break;
	case DLLCMD_SETEVENTS://						0x0b
		{
			if(ppvoid)
			{
				g_data.m_pEvents = (CTabulatorEventArray*)ppvoid;
			}
		} break;
	case DLLCMD_GETDBSETTINGS://						0x0c
		{
			EnterCriticalSection(&g_settings.m_crit);
			g_settings.GetFromDatabase();
			LeaveCriticalSection(&g_settings.m_crit);
		} break;
	case DLLCMD_SETGLOBALSTATE://						0x0d
		{
			if(ppvoid)
			{
				char statetoken[MAX_PATH];
				strncpy(statetoken, (char*)ppvoid, MAX_PATH);
				int n=0;
				while(n<g_dlldata.nNumThreads)
				{
					if((g_dlldata.thread)&&(g_dlldata.thread[n]))
					{
						if(stricmp(statetoken, "resume")==0)
						{
							switch(n)
							{
							case WHEEL_DATA:// 0
								g_dlldata.thread[WHEEL_DATA]->nThreadState &= ~WHEEL_DATA_PAUSED; break;
							case WHEEL_TICKER:// 1
								g_dlldata.thread[WHEEL_TICKER]->nThreadState &= ~WHEEL_TICKER_PAUSED; break;
							default:
							case WHEEL_UI:// 2
								break;
							}
						}
						else
						if(stricmp(statetoken, "suspend")==0)
						{
							switch(n)
							{
							case WHEEL_DATA:// 0
								g_dlldata.thread[WHEEL_DATA]->nThreadState |= WHEEL_DATA_PAUSED; break;
							case WHEEL_TICKER:// 1
								g_dlldata.thread[WHEEL_TICKER]->nThreadState |= WHEEL_TICKER_PAUSED; break;
							default:
							case WHEEL_UI:// 2
								break;
							}
						}
					}
					n++;
				}
			}
			else nReturn = CLIENT_ERROR;

		} break;

		

	default: nReturn = CLIENT_UNKNOWN; break;
	}
	return nReturn;
}

BOOL CWheelApp::InitInstance() 
{
#if _WIN32_WINNT >= 0x0400
		HRESULT hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		HRESULT hRes = CoInitialize(NULL);
#endif
//	AfxMessageBox(m_pszAppName);
//	AfxMessageBox(m_pszExeName);
//	AfxMessageBox("Foo");

	strcpy(m_szSettingsFilename, SETTINGS_FILENAME);

	if((m_pszAppName)&&(strlen(m_pszAppName)>0)) sprintf(m_szSettingsFilename, "%s.ini", m_pszAppName);

	InitializeCriticalSection(&g_dlldata.m_crit);
	EnterCriticalSection(&g_dlldata.m_crit);
	g_dlldata.thread = new DLLthread_t*[WHEEL_THREADS]; // 3 threads, data download and ticker engine, and UI conn
	bool bIsServer = false;
	if(g_dlldata.thread)
	{
		g_dlldata.thread[WHEEL_DATA] = new DLLthread_t;
		g_data.InitDLLData(g_dlldata.thread[WHEEL_DATA]);

		g_dlldata.thread[WHEEL_TICKER] = new DLLthread_t;
		g_data.InitDLLData(g_dlldata.thread[WHEEL_TICKER]);

		g_dlldata.thread[WHEEL_UI] = new DLLthread_t;
		g_data.InitDLLData(g_dlldata.thread[WHEEL_UI]);

		g_dlldata.nNumThreads=WHEEL_THREADS;


		DLLthread_t* pData = NULL;

		_ftime(&g_data.m_timeStart);

		int n=0;

		CFileUtil fu;

		
		if(fu.GetSettings(m_szSettingsFilename, false) ==	FILEUTIL_MALLOC_OK)
		{
			bIsServer = fu.GetIniInt("Global", "IsServer", 0)?true:false;

			while(n<g_dlldata.nNumThreads)
			{
				if((g_dlldata.thread)&&(g_dlldata.thread[n]))
				{
					pData = g_dlldata.thread[n];
					switch(n)
					{
					case WHEEL_DATA:// 0
						{
							EnterCriticalSection(&pData->m_crit);

							pData->pszThreadName = fu.GetIniString("DataDownload", "Threadname", "DataDownloader", pData->pszThreadName);

						// URLs may use format specifiers

							pData->pszBaseURL = fu.GetIniString("DataDownload", "BaseURL", "http://services.wheel.com/feed/list/", pData->pszBaseURL);
	//									pData->pszInfoURL=NULL;
	//									pData->pszConfirmURL=NULL;

	//									pData->pszSeriesParamName=NULL;    //  %S
							pData->pszProgramParamName = fu.GetIniString("DataDownload", "ProgramParamName", "program", pData->pszProgramParamName);
	//									pData->pszEpisodeParamName=NULL;   //  %E

	//									pData->pszSeriesID=NULL;    //  %s
							pData->pszProgramID = fu.GetIniString("DataDownload", "ProgramID", "10", pData->pszProgramID);   //  %p
	//									pData->pszEpisodeID=NULL;   //  %e

							// some generic parameters
							pData->pszToDateParamName = fu.GetIniString("DataDownload", "ToDateParamName", "To", pData->pszToDateParamName);   //  %p
							pData->pszFromDateParamName = fu.GetIniString("DataDownload", "FromDateParamName", "From", pData->pszFromDateParamName);   //  %p
							pData->pszNowParamName = fu.GetIniString("DataDownload", "NowParamName", "time", pData->pszNowParamName);   //  %p
							pData->pszBatchParamName = fu.GetIniString("DataDownload", "BatchParamName", "limit", pData->pszBatchParamName);   //  %p
							pData->pszFromIDParamName = fu.GetIniString("DataDownload", "FromIDParamName", "startid", pData->pszFromIDParamName);   //  %p
							pData->pszGetIDParamName = fu.GetIniString("DataDownload", "GetIDParamName", "id", pData->pszGetIDParamName);   //  %p

	//									pData->pszToDateParamName=NULL;				//  %t
	//									pData->pszFromDateParamName=NULL;			//  %f
	//									pData->pszNowParamName=NULL;					//  %n
	//									pData->pszBatchParamName=NULL;				//  %b
	//									pData->pszFromIDParamName=NULL;				//  %m  // start id
	//									pData->pszGetIDParamName=NULL;				//  %i

							pData->nBatchParam = fu.GetIniInt("DataDownload", "BatchParam", 250);   //  %p
							pData->nShortIntervalMS = fu.GetIniInt("DataDownload", "ShortIntervalMS", 0);   //  %p
							pData->nLongIntervalMS = fu.GetIniInt("DataDownload", "LongIntervalMS", 5000);   //  %p
							pData->bDirParamStyle = fu.GetIniInt("DataDownload", "DirParamStyle", 1)?true:false;   //  %p

							LeaveCriticalSection(&pData->m_crit);

						} break;
					case WHEEL_TICKER:// 1
						{
							EnterCriticalSection(&pData->m_crit);

							pData->pszThreadName = fu.GetIniString("Ticker", "Threadname", "TickerEngine", pData->pszThreadName);
							pData->nBatchParam = fu.GetIniInt("Ticker", "BatchParam", 250);   //  %p
							pData->nShortIntervalMS = fu.GetIniInt("Ticker", "ShortIntervalMS", 100);   //  %p
							pData->nLongIntervalMS = fu.GetIniInt("Ticker", "LongIntervalMS", 5000);   //  %p

							pData->pszHost = fu.GetIniString("Ticker", "GraphicsEngineHost", "127.0.0.1", pData->pszHost);  
							pData->nPort = fu.GetIniInt("Ticker", "GraphicsEnginePort", 7795);  
							pData->nAuxValue = fu.GetIniInt("Ticker", "PlayItemsOnce", 1);  


							LeaveCriticalSection(&pData->m_crit);

						} break;
					case WHEEL_UI:// 2
						{
							EnterCriticalSection(&pData->m_crit);

							pData->pszThreadName = fu.GetIniString("Synchronizer", "Threadname", "Synchronizer", pData->pszThreadName);
							pData->nBatchParam = fu.GetIniInt("Synchronizer", "BatchParam", 250);   //  %p
							pData->nShortIntervalMS = fu.GetIniInt("Synchronizer", "ShortIntervalMS", 100);   //  %p
							pData->nLongIntervalMS = fu.GetIniInt("Synchronizer", "LongIntervalMS", 5000);   //  %p

							LeaveCriticalSection(&pData->m_crit);

						} break;
					}
				}


				n++;
			}
		}

	}
	else
	{
		g_dlldata.nNumThreads=0;
	}
 	LeaveCriticalSection(&g_dlldata.m_crit);

	g_dlldata.pDlg = g_pdlg; // if there is a dialog....

//	AfxMessageBox("Foo1");
			EnterCriticalSection(&g_settings.m_crit);
			if(bIsServer)
			{
	g_settings.GetFromDatabase(); // does Settings(true) first
			}
			else
			{
	g_settings.Settings(true); // need to get the thread settings...
			}
			LeaveCriticalSection(&g_settings.m_crit);
	
	if(g_dlldata.nNumThreads>WHEEL_UI)
	{
		if((g_dlldata.thread)&&(g_dlldata.thread[WHEEL_UI]))
		{
			g_dlldata.thread[WHEEL_UI]->bKillThread = false;
			if(!g_dlldata.thread[WHEEL_UI]->bThreadStarted)
			{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SynchronizationThread", "begin thread"); //(Dispatch message)
				if(_beginthread(SynchronizationThread, 0, (void*)NULL)==-1)
				{
					//error.

					AfxMessageBox("Error beginning synchronization thread.\nThe application must exit.");
					return FALSE;
					
				}
				else
				{

//				while(!g_psentinel->m_data.m_ppConnObj[nTemp]->m_bConnThreadStarted) Sleep(100);
				}
			}
		}
	}


//	AfxMessageBox("Foo2");
	return CWinApp::InitInstance();
}


#ifndef REMOVE_CLIENTSERVER
void DataDownloadThread(void* pvArgs)
{	
	char szFeedSource[MAX_PATH]; sprintf(szFeedSource, "%s_DataDownload", g_settings.m_pszInternalAppName);
	if((g_settings.m_bUseFeed)&&(g_dlldata.thread)&&(g_dlldata.nNumThreads>WHEEL_DATA))
	{
		if(g_dlldata.thread[WHEEL_DATA]->bThreadStarted) return; //only one thread pls.
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "DataDownloadThread", "thread begun"); //(Dispatch message)
		g_dlldata.thread[WHEEL_DATA]->bThreadStarted=true;
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "Download thread for [%s] begun.", g_dlldata.thread[WHEEL_DATA]->pszBaseURL);  //(Dispatch message)

		DLLthread_t* pData = g_dlldata.thread[WHEEL_DATA];

		if(pData)
		{
			if(g_settings.m_bAutostartData)
			{
				pData->nThreadControl = WHEEL_DATA_STARTED;
			}
			else 
			{
				pData->nThreadControl = WHEEL_DATA_STOPPED;
			}

			pData->nThreadState=WHEEL_DATA_STOPPED;
		}
#if _WIN32_WINNT >= 0x0400
		HRESULT hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		HRESULT hRes = CoInitialize(NULL);
#endif
		CInet inet;

	EnterCriticalSection(&g_data.m_critSQL);

	  CDBUtil db;
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];
		strcpy(errorstring, "");

		CDBconn* pdbConn = db.CreateNewConnection(g_settings.m_pszDSN, g_settings.m_pszUser, g_settings.m_pszPW);
		if(pdbConn)
		{
			if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, szFeedSource, errorstring);  //(Dispatch message)
			}
			else
			{
				// do the following even if the get database fails.
				if(
						(g_settings.m_pszFeed)
					&&(strlen(g_settings.m_pszFeed)>0)
					)
				{

					db.AddTable(pdbConn, g_settings.m_pszFeed); 
					if(db.TableExists(pdbConn, g_settings.m_pszFeed)==DB_EXISTS)
					{
						// get the schema
						db.GetTableInfo(pdbConn, g_settings.m_pszFeed);
					}
					else
					{
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 

"CREATE TABLE [%s] (\"id\" int NOT NULL, \
\"text\" varchar(1024) NOT NULL, \
\"revision\" varchar(1024), \
\"timestamp\" int NOT NULL, \
\"updated\" int NOT NULL, \
\"status\" int NOT NULL, \
\"type\" int NOT NULL, \
\"played\" int NULL);",
										g_settings.m_pszFeed
									);

					//					AfxMessageBox(szSQL);


						if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{

						}

					}
/*
				// now that the table existence has been ensured, delete things that are old.



					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE status = 0",
														g_settings.m_pszFeed  // table name
													);
					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{

					}

*/

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT \
[id], [text], [revision], [timestamp], [updated], [status], [type], [played] FROM %s ORDER BY [timestamp] ASC", //HARDCODE
						((g_settings.m_pszFeed )&&(strlen(g_settings.m_pszFeed )))?g_settings.m_pszFeed:"Feed");

			//create table Queue (itemid int identity(1,1) NOT NULL, filename_source varchar(256), filename_dest varchar(256), 
			//action int, host varchar(64), timestamp float, username varchar(32));

					CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
					if(prs)
					{
						CString szTemp;
						int nReturn = CLIENT_ERROR;
						int nIndex = 0;
						while ((!prs->IsEOF()))
						{
							try
							{
								CWheelMessage* pmsg =  new CWheelMessage;
								if(pmsg)
								{
									prs->GetFieldValue("id", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										pmsg->m_nID = atoi(szTemp);
										prs->GetFieldValue("text", pmsg->m_szMessage);//HARDCODE
										prs->GetFieldValue("revision", pmsg->m_szRevisedMessage);//HARDCODE
										prs->GetFieldValue("timestamp", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nTimestamp = atoi(szTemp);
										}
										if(pmsg->m_nTimestamp<0)
										{
											_ftime(&pData->timebTick);
											pmsg->m_nTimestamp=pData->timebTick.time;
										}
										prs->GetFieldValue("updated", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nLastUpdate = atoi(szTemp);
//											pData->m_nLastUpdate=pmsg->m_nLastUpdate;
										}
										prs->GetFieldValue("status", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nStatus= (atoi(szTemp)|WHEEL_MSG_STATUS_INSERTED);
										}
										prs->GetFieldValue("type", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nType = atoi(szTemp);
										}
										prs->GetFieldValue("played", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nPlayed = atoi(szTemp);
										}
										
										g_data.InsertMessage(pmsg);
									}

								}

							}
							catch(...)
							{
							}
							prs->MoveNext();
						}
						delete prs;
						_ftime(&pData->timebTick);
						pData->m_dblLastUpdate=(double)(pData->timebTick.time) + ((double)(pData->timebTick.millitm)/1000.0);
						g_data.PublishData();

					}
					else
					{
						//print err
					}

				}				
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", g_settings.m_pszDSN, g_settings.m_pszUser, g_settings.m_pszPW); 
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, szFeedSource, errorstring);  //(Dispatch message)

			//**MSG
		}

		g_data.m_pdb=&db;
		g_data.m_pdbConn=pdbConn;

	LeaveCriticalSection(&g_data.m_critSQL);

		IXMLDOMDocumentPtr pDoc(__uuidof(DOMDocument));  // let's have only one object per connection
		HRESULT hr;//= pDoc.CreateInstance(__uuidof(DOMDocument));
		hr = pDoc->put_async(VARIANT_FALSE);
										
		// Remove document safety options
		IObjectSafety* pSafety=NULL;
		DWORD dwSupported, dwEnabled;
			

		if ( SUCCEEDED(pDoc->QueryInterface(IID_IObjectSafety, (void**)&pSafety)))
		{
			pSafety->GetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, &dwSupported, &dwEnabled );
			pSafety->SetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, dwSupported, 0 );
		}

		CString szURL;

USES_CONVERSION;

		g_data.m_nLastID=1;

		while((!pData->bKillThread)&&(g_settings.m_bUseFeed))
		{
			_ftime(&pData->timebTick);

			switch(pData->nThreadControl)
			{
				case WHEEL_DATA_STARTED:
				{
					if(!(pData->nThreadState&WHEEL_DATA_STARTED))
					{
						pData->nThreadState|=WHEEL_DATA_STARTED;
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "Data download has been started."); //(Dispatch message)
					}
				} break;
				case WHEEL_DATA_STOPPED:
				{
					if(pData->nThreadState&WHEEL_DATA_STARTED)
					{
//					pData->nThreadState=WHEEL_DATA_STOPPED;
						pData->nThreadState &= ~WHEEL_DATA_STARTED;
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "Data download has been stopped."); //(Dispatch message)
						strcpy(pData->pszThreadStateText, "");
					}
				} break;
			}

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "entering"); Sleep(100); //(Dispatch message)


			switch(pData->nThreadState&WHEEL_DATA_STARTED) // startup status
			{
				case WHEEL_DATA_STARTED:
				{
					// get XML;
					_timeb now;


		//					AfxMessageBox(szURL);
					try
					{
			EnterCriticalSection(&g_settings.m_crit);

						int nLength = strlen(pData->pszBaseURL);
						if(nLength>0)
						{
							if(pData->pszBaseURL[nLength-1]!='/')
							{
								if(g_dlldata.thread[WHEEL_DATA]->bDirParamStyle)
								{
									szURL.Format("%s/%s/%s/%s/%d/%s/%d", pData->pszBaseURL, 
										g_dlldata.thread[WHEEL_DATA]->pszProgramParamName, pData->pszProgramID,
										g_dlldata.thread[WHEEL_DATA]->pszFromIDParamName,  g_data.m_nLastID,
										g_dlldata.thread[WHEEL_DATA]->pszBatchParamName, g_dlldata.thread[WHEEL_DATA]->nBatchParam
										);
								}
								else
								{
									szURL.Format("%s?%s=%s&%s=%d&%s=%d", pData->pszBaseURL, 
										g_dlldata.thread[WHEEL_DATA]->pszProgramParamName, pData->pszProgramID,
										g_dlldata.thread[WHEEL_DATA]->pszFromIDParamName, g_data.m_nLastID,
										g_dlldata.thread[WHEEL_DATA]->pszBatchParamName, g_dlldata.thread[WHEEL_DATA]->nBatchParam
										);
								}
							}
							else
							{
								if(g_dlldata.thread[WHEEL_DATA]->bDirParamStyle)
								{
									szURL.Format("%s%s/%s/%s/%d/%s/%d", pData->pszBaseURL, 
										g_dlldata.thread[WHEEL_DATA]->pszProgramParamName, pData->pszProgramID,
										g_dlldata.thread[WHEEL_DATA]->pszFromIDParamName, g_data.m_nLastID,
										g_dlldata.thread[WHEEL_DATA]->pszBatchParamName, g_dlldata.thread[WHEEL_DATA]->nBatchParam
										);
								}
								else
								{
									pData->pszBaseURL[nLength-1]=0;
									szURL.Format("%s?%s=%s&%s=%d&%s=%d", pData->pszBaseURL, 
										g_dlldata.thread[WHEEL_DATA]->pszProgramParamName, pData->pszProgramID,
										g_dlldata.thread[WHEEL_DATA]->pszFromIDParamName, g_data.m_nLastID,
										g_dlldata.thread[WHEEL_DATA]->pszBatchParamName, g_dlldata.thread[WHEEL_DATA]->nBatchParam
										);
								}
							}
						}
			LeaveCriticalSection(&g_settings.m_crit);

						CString szTextData;
						CString szInfo;
						int nMsgs=0;
						unsigned long ulTotalTime;
						unsigned long ulTime = clock();
						_ftime(&now);

if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "Downloading [%s].", szURL);  //(Dispatch message)

						if(inet.RetrieveURLToText(szURL, &szTextData, FALSE, FALSE, "", &szInfo)>=INET_SUCCESS)
						{
			//						AfxMessageBox("success");
							ulTotalTime = clock() - ulTime;
	//						szTime.Format("time to download: %d ms", ulTotalTime);

							_ftime(&pData->timebLastPullTick);

							pDoc->put_async(VARIANT_FALSE);
							hr = pDoc->loadXML(szTextData.GetBuffer(0));
							if(hr!=VARIANT_TRUE)
							{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "error loading XML");  //(Dispatch message)
								pData->nNumFailures++;
								pData->nThreadState=WHEEL_DATA_ERROR;
								_snprintf(pData->pszThreadStateText, 256, "XML error in feed");

							}
							else
							{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "success loading XML");  //(Dispatch message)
								int nMaxID=0;
								BOOL bFeedFound = FALSE;
								IXMLDOMNodeListPtr pChildNodes = pDoc->GetchildNodes();
								if (pChildNodes != NULL)
								{
									IXMLDOMNodePtr pChild;
									BOOL vcommand = FALSE;
									while(pChild = pChildNodes->nextNode())
									{
										DOMNodeType nodeType;
										pChild->get_nodeType(&nodeType);

										if(nodeType == NODE_ELEMENT)
										{
											CString tag = W2T(pChild->GetnodeName());
											if(tag.CompareNoCase(_T("feed"))==0)
											{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "found feed");  //(Dispatch message)

												pData->nThreadState=WHEEL_DATA_STARTED; //resets error
												strcpy(pData->pszThreadStateText, "");
												bFeedFound = TRUE;
				
												pData->nNumSuccess++;
												IXMLDOMNodeListPtr pFeedChildNodes = pChild->GetchildNodes();
												if (pChildNodes != NULL)
												{
													IXMLDOMNodePtr pFeedChild;
													while(pFeedChild = pFeedChildNodes->nextNode())
													{
														pFeedChild->get_nodeType(&nodeType);

														if(nodeType == NODE_ELEMENT)
														{
															CString tag = W2T(pFeedChild->GetnodeName());
															if(tag.CompareNoCase(_T("sms"))==0)
															{
																CWheelMessage msg;
																msg.m_szMessage = g_data.XMLTextNodeValue(pFeedChild);
																IXMLDOMNamedNodeMapPtr pAttrMap = pFeedChild->Getattributes();
																if (pAttrMap != NULL)
																{
																	IXMLDOMAttributePtr pAttribute;
																	while(pAttribute = pAttrMap->nextNode())
																	{
																		// get attribs
																		CString attrib;
																		attrib = W2T(pAttribute->GetnodeName());
																		if(attrib.CompareNoCase(_T("id"))==0)
																		{
																			msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
																		}
																		else
																		if(attrib.CompareNoCase(_T("ts"))==0)
																		{
																			msg.m_nTimestamp = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
																		}

																	}

																}

																_ftime(&pData->timebTick);

																if(msg.m_nTimestamp<0)
																{
																	msg.m_nTimestamp=pData->timebTick.time;
																}

																msg.m_nLastUpdate = pData->timebTick.time+1;
//																pData->m_nLastUpdate=pData->timebTick.time;


									//							AfxMessageBox(msg.m_szMessage);

																if((msg.m_nID>0)&&(msg.m_nTimestamp>0))
																{
																	nMsgs++;
																	// add to register and db if not there already.
//AfxMessageBox("finding ID");
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "finding");// Sleep(100); //(Dispatch message)

																	int nID = g_data.FindID(msg.m_nID, g_data.m_nLastFoundIndex);
																	if(nID>=0)
																	{
//AfxMessageBox("found ID");
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "found id %d at index %d", msg.m_nID, nID);// Sleep(100); //(Dispatch message)
																		g_data.m_nLastFoundIndex = nID;
																	}
																	else //add it...
																	{
																		CWheelMessage* pmsg =  new CWheelMessage;
																		if(pmsg)
																		{
																			pmsg->m_szMessage = msg.m_szMessage;
																			pmsg->m_nTimestamp = msg.m_nTimestamp;
																			pmsg->m_nLastUpdate =msg.m_nLastUpdate;
																			pmsg->m_nID = msg.m_nID;
//AfxMessageBox("inserting ID");
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "inserting id %d", msg.m_nID);// Sleep(100); //(Dispatch message)

																			g_data.InsertMessage(pmsg);

																			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", pmsg->SQLUpdate(false, 0.0)	);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "inserting id %d to SQL with [%s]", msg.m_nID, szSQL);// Sleep(100); //(Dispatch message)

	//	AfxMessageBox(szSQL);
	EnterCriticalSection(&g_data.m_critSQL);
																			if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
																			{
																				//err
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "ERROR inserting id %d to SQL [%s] for [%s]", pmsg->m_nID, errorstring, szSQL);// Sleep(100); //(Dispatch message)

																			}
																			else
																			{
																				pmsg->m_nStatus |= WHEEL_MSG_STATUS_INSERTED;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "inserted id %d to SQL with [%s]", msg.m_nID, szSQL);// Sleep(100); //(Dispatch message)
																			}
	LeaveCriticalSection(&g_data.m_critSQL);
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}

									_ftime(&pData->timebTick);
									pData->m_dblLastUpdate=(double)(pData->timebTick.time) + ((double)(pData->timebTick.millitm)/1000.0);
									g_data.PublishData();

								}

								if(bFeedFound)
								{
									g_data.m_nLastID = nMaxID;
									int nRate = nMsgs/((int)ulTotalTime);

									if((nRate<pData->nIDrateMin)||(pData->nIDrateMin==0)) pData->nIDrateMin = nRate;
									if((nRate>pData->nIDrateMax)||(pData->nIDrateMax==0)) pData->nIDrateMax = nRate;
									if(pData->nNumSuccess>1)
									{
										pData->nIDrateAvg = (pData->nIDrateAvg*(pData->nNumSuccess-1) + nRate)/pData->nNumSuccess;

									}
									else
									{
										pData->nIDrateAvg=nRate;
									}
								}
								else
								{
									// we downloaded something but not the feed!
									pData->nThreadState|=WHEEL_DATA_ERROR;
									strcpy(pData->pszThreadStateText, "No data in feed");
								}
							}
						}
						else
						{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "FAIL: %s [%s].", szInfo, szURL);  //(Dispatch message)
			//						AfxMessageBox("fail");
							pData->nNumFailures++;
							pData->nThreadState=WHEEL_DATA_ERROR;

							_snprintf(pData->pszThreadStateText, 256, "Failure loading feed");

							ulTotalTime = clock() - ulTime;
		//					szTime.Format("failed to download after: %d ms", ulTotalTime);
						}

						if(nMsgs==1)
						{
							_ftime(&now);
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "waiting %d ms.", pData->nLongIntervalMS);  //(Dispatch message)

							now.time += pData->nLongIntervalMS/1000;
							now.millitm += (unsigned short)(pData->nLongIntervalMS%1000); // fractional second updates
							if(now.millitm>999)
							{
								now.time++;
								now.millitm%=1000;
							}
										
							while(
											(
												( pData->timebTick.time < now.time )
											||((pData->timebTick.time == now.time)&&(pData->timebTick.millitm < now.millitm))
											)
										&&(!pData->bKillThread)
										)
							{
								_ftime(&pData->timebTick);
							}

						}
						else
						{
							_ftime(&now);
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "Downloaded %d messages from [%s].", nMsgs, szURL);  //(Dispatch message)

if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "waiting %d ms.", pData->nShortIntervalMS);  //(Dispatch message)
							now.time += pData->nShortIntervalMS/1000;
							now.millitm += (unsigned short)(pData->nShortIntervalMS%1000); // fractional second updates
							if(now.millitm>999)
							{
								now.time++;
								now.millitm%=1000;
							}
										
							while(
											(
												( pData->timebTick.time < now.time )
											||((pData->timebTick.time == now.time)&&(pData->timebTick.millitm < now.millitm))
											)
										&&(!pData->bKillThread)
										)
							{
								_ftime(&pData->timebTick);
							}

						}
					}
					catch (...)
					{
			//			AfxMessageBox("exception");
							pData->nNumFailures++;
							pData->nThreadState=WHEEL_DATA_ERROR;
							_snprintf(pData->pszThreadStateText, 256, "Error loading feed");

	//						ulTotalTime = clock() - ulTime;
					}
				} break;
				case WHEEL_DATA_STOPPED:
				{
					// simply do nothing.
				} break;

//	x
			} // switch

			if(pData->nThreadState&WHEEL_DATA_ERROR) 	pData->nThreadState &= ~WHEEL_DATA_STARTED;  // reset this.

			Sleep(1);
		}

		if(pSafety) pSafety->Release();
		CoUninitialize(); //XML
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szFeedSource, "Download thread for [%s] ended.", g_dlldata.thread[WHEEL_DATA]->pszBaseURL);  //(Dispatch message)

		g_dlldata.thread[WHEEL_DATA]->bThreadStarted=false;
	}

}
#endif //#ifndef REMOVE_CLIENTSERVER


void TickerEngineThread(void* pvArgs)
{	
	char szTickerSource[MAX_PATH]; sprintf(szTickerSource, "%s_Ticker", g_settings.m_pszInternalAppName);
	char szLogDests[MAX_PATH]; sprintf(szLogDests, "%s,log", (((g_settings.m_pszAsRunDestination)&&(strlen(g_settings.m_pszAsRunDestination)))?g_settings.m_pszAsRunDestination:"asrun"));

	if((g_dlldata.thread)&&(g_dlldata.nNumThreads>WHEEL_TICKER))
	{
		if(g_dlldata.thread[WHEEL_TICKER]->bThreadStarted) return; //only one thread pls.
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "thread begun"); //(Dispatch message)
		g_dlldata.thread[WHEEL_TICKER]->bThreadStarted=true;
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker thread for [%s:%d] begun.", g_dlldata.thread[WHEEL_TICKER]->pszHost, g_dlldata.thread[WHEEL_TICKER]->nPort );  //(Dispatch message)
		DLLthread_t* pData = g_dlldata.thread[WHEEL_TICKER];
		_timeb now;
		if(pData)
		{
			if(g_settings.m_bAutostartTicker)
			{
				pData->nThreadControl = WHEEL_TICKER_STARTED;
			}
			else 
			{
				pData->nThreadControl = WHEEL_TICKER_STOPPED;

			}

			pData->nThreadState|=WHEEL_TICKER_ERROR;
			_snprintf(pData->pszThreadStateText, 256, "Initializing ticker");
			_ftime(&now);
			pData->m_dblLastUpdate=(double)(now.time) + ((double)(now.millitm)/1000.0);


		}

		g_data.m_nDwellTime = g_settings.m_nDefaultDwellTime;

	  CDBUtil db;
		CNetUtil net;
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];
		strcpy(errorstring, "");

if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, szTickerSource, "HAVE %d databased ticker items", g_data.m_nTickerMessages);  //(Dispatch message)
		int nRecords = 0;

		CDBconn* pdbConn = db.CreateNewConnection(g_settings.m_pszDSN, g_settings.m_pszUser, g_settings.m_pszPW);
		if(pdbConn)
		{
			if(g_data.m_pdbConn == NULL)
			{
				g_data.m_pdb=&db;
				g_data.m_pdbConn=pdbConn;
			}


			if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, szTickerSource, errorstring);  //(Dispatch message)
			}
			else
			{
// have to set up the ticker state
				bool bFirst = true;

				if((pdbConn!=NULL)&&(pdbConn->m_bConnected))
				{
	//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Connection established to %s.", g_settings.m_pszModule); //(Dispatch message)

							strcpy(pData->pszThreadStateText, "");

							if(bFirst)
								pData->nThreadState=WHEEL_TICKER_STOPPED; 
							else
								pData->nThreadState=WHEEL_TICKER_STARTED; // only because on re-establish we want to presume in so we can take out.  if we presume out and its really in we have to put in to take out
							bFirst = false;
							g_data.m_bTickerReady = false; // have to reset this.

				}



				// do the following even if the get database fails.
				if(
						(g_settings.m_pszTicker)
					&&(strlen(g_settings.m_pszTicker)>0)
					)
				{
					db.AddTable(pdbConn, g_settings.m_pszTicker); 
					if(db.TableExists(pdbConn, g_settings.m_pszTicker)==DB_EXISTS)
					{
						// get the schema
						db.GetTableInfo(pdbConn, g_settings.m_pszTicker);
					}
					else
					{
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, 

"CREATE TABLE [%s] (\"id\" int identity(1,1) NOT NULL, \
\"message\" varchar(4096) NOT NULL, \
\"aux_data1\" varchar(1024) NOT NULL, \
\"aux_data2\" varchar(1024) NOT NULL, \
\"sort_order\" decimal(20,3), \
\"dwell_override\" decimal(20,3), \
\"valid_from\" int, \
\"expires_on\" int, \
\"allowed_cycles\" int, \
\"actual_cycles\" int, \
\"valid_from\" int, \
\"active\" int, \
\"status\" int, \
\"last_played\" int, \
\"created_on\" int, \
\"created_by\" varchar(32), \
\"last_modified_on\" int, \
\"last_modified_by\" varchar(32)\
);",
										g_settings.m_pszTicker
									);

/*
create table Wheel_1_Ticker(id int identity(1,1), message varchar(4096), aux_data1 varchar(1024), aux_data2 varchar(1024),
sort_order decimal(20,3), dwell_override decimal(20,3), valid_from int, expires_on int, allowed_cycles int, actual_cycles int, 
active int, status int, last_played int, created_on int, created_by varchar(32), last_modified_on int, last_modified_by varchar(32));   
*/
					//					AfxMessageBox(szSQL);


						if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
							if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, szTickerSource, errorstring);  //(Dispatch message)

						}

					}

/*
				// now that the table existence has been ensured, delete things that are old.

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE type <> %d",
														g_settings.m_pszTicker, WHEEL_MSG_TYPE_DEFAULT  // table name
													);
*/
					// now that the table existence has been ensured, set anything playing or cued etc to done.

/*
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET [status] = [status] | %d WHERE [status] & %d <> 0",
														g_settings.m_pszTicker, 
														WHEEL_MSG_STATUS_PLAYED,
														(WHEEL_MSG_STATUS_COMMIT|WHEEL_MSG_STATUS_PLAYING)
  // table name
													);
					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, szTickerSource, errorstring);  //(Dispatch message)

					}
*/
// #ifndef REMOVE_CLIENTSERVER  // ok, i DO want to do this init, just to get whatever is in there now, and reset any statuses that might be off....

/* create table Wheel_1_Ticker(id int identity(1,1), message varchar(4096), aux_data1 varchar(1024), 
					aux_data2 varchar(1024), sort_order decimal(20,3), dwell_override decimal(20,3), valid_from int, 
						expires_on int, allowed_cycles int, actual_cycles int, active int, status int, last_played int, 
						created_on int, created_by varchar(32), last_modified_on int, last_modified_by varchar(32)); 
*/

					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT \
[id], [message], [aux_data1], [aux_data2], [sort_order], [dwell_override], [valid_from], \
[expires_on], [allowed_cycles], [actual_cycles], [active], [status], [last_played] \
FROM %s ORDER BY [sort_order] ASC", //HARDCODE
						(((g_settings.m_pszTicker )&&(strlen(g_settings.m_pszTicker )))?g_settings.m_pszTicker:"Ticker")
						);
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, szTickerSource, "SQL: [%s]", szSQL);  //(Dispatch message)

			//create table Queue (itemid int identity(1,1) NOT NULL, filename_source varchar(256), filename_dest varchar(256), 
			//action int, host varchar(64), timestamp float, username varchar(32));

					CRecordset* prs = db.Retrieve(pdbConn, szSQL, errorstring);
					if(prs)
					{
						CString szTemp;
						int nReturn = CLIENT_ERROR;
						while (!prs->IsEOF())
						{
							try
							{
								CWheelMessage* pmsg =  new CWheelMessage;
								if(pmsg)
								{
									prs->GetFieldValue("id", szTemp);//HARDCODE
									if(szTemp.GetLength())
									{
										nRecords++;
//				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, szTickerSource, "got msg id=%s", szTemp);  //(Dispatch message)

										pmsg->m_nID = atoi(szTemp);
										if((	g_data.m_nTickerIDMaxNeg>pmsg->m_nID)&&(pmsg->m_nID<0)) // defaults have negative IDs
										{
	EnterCriticalSection(&g_data.m_critDefaultID);
											g_data.m_nTickerIDMaxNeg=pmsg->m_nID;
	LeaveCriticalSection(&g_data.m_critDefaultID);
										}

										prs->GetFieldValue("message", pmsg->m_szMessage);//HARDCODE
										prs->GetFieldValue("aux_data1", pmsg->m_szAux1);//HARDCODE
										prs->GetFieldValue("aux_data2", pmsg->m_szAux2);//HARDCODE
										prs->GetFieldValue("sort_order", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_dblIndex = atof(szTemp);

											if(	g_data.m_dblTickerDefaultMax<pmsg->m_dblIndex) g_data.m_dblTickerDefaultMax=pmsg->m_dblIndex;
										}
										prs->GetFieldValue("dwell_override", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_dblDwellOverride = atoi(szTemp);
											if(pmsg->m_dblDwellOverride<0.001) pmsg->m_dblDwellOverride = -1.0;
										}
										else pmsg->m_dblDwellOverride = -1.0;

							//			prs->GetFieldValue("valid_from", szTemp);//HARDCODE
							//			prs->GetFieldValue("expires_on", szTemp);//HARDCODE

										prs->GetFieldValue("allowed_cycles", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nPlayLimit = atoi(szTemp);
											if(pmsg->m_nPlayLimit<0) pmsg->m_nPlayLimit = -1;
										}
										else pmsg->m_nPlayLimit = -1;

										prs->GetFieldValue("actual_cycles", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nPlayed = atoi(szTemp);
										}
										else pmsg->m_nPlayed = 0;

										prs->GetFieldValue("active", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_bActive = ((atoi(szTemp)>0)?true:false);
										}
										else pmsg->m_bActive = false;

										prs->GetFieldValue("status", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nStatus = atoi(szTemp);
										}
										else pmsg->m_nStatus = 0;

										prs->GetFieldValue("last_played", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nLastPlayed = atoi(szTemp);
										}

/*  older stuff...
										if(pmsg->m_nTimestamp<0)
										{
											_ftime(&pData->timebTick);
											pmsg->m_nTimestamp=pData->timebTick.time;
										}

										prs->GetFieldValue("updated", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nLastUpdate = atoi(szTemp);
//											pData->m_nLastUpdate=pmsg->m_nLastUpdate;
										}
										prs->GetFieldValue("status", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nStatus = (atoi(szTemp)|WHEEL_MSG_STATUS_INSERTED);
										}
										prs->GetFieldValue("type", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nType = atoi(szTemp);
										}
										prs->GetFieldValue("played", szTemp);//HARDCODE
										if(szTemp.GetLength())
										{
											pmsg->m_nPlayed = atoi(szTemp);
										}
										
*/
// need the following for initializing stuff that might have played status at the beginning.
										if(g_data.InsertTickerMessage(pmsg)<CLIENT_SUCCESS)
										{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, szTickerSource, "could not insert item id= %d", pmsg->m_nID);  //(Dispatch message)
										}
									}

								}

							}
							catch(...)
							{
							}
							prs->MoveNext();
						}
						delete prs;

						_ftime(&pData->timebTick);
						pData->m_dblLastUpdate=(double)(pData->timebTick.time) + ((double)(pData->timebTick.millitm)/1000.0);
						g_data.PublishTicker();

					}
					else
					{
						//print err
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, szTickerSource, "return set was NULL (err:%s) [%s]", errorstring, szSQL);  //(Dispatch message)
					}
				}
//#endif //			#ifndef REMOVE_CLIENTSERVER
	
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", g_settings.m_pszDSN, g_settings.m_pszUser, g_settings.m_pszPW); 
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, szTickerSource, errorstring);  //(Dispatch message)

			//**MSG
		}

if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND|MSG_PRI_HIGH, NULL, szTickerSource, "%d ticker items after %d retrieved", g_data.m_nTickerMessages, nRecords);  //(Dispatch message)


		if(g_data.m_ticker)
		{
			int nInit = 0;
			while(nInit<g_data.m_nTickerMessages)
			{
				if(g_data.m_ticker[nInit])
				{
					if(g_data.m_ticker[nInit]->m_nStatus&(WHEEL_MSG_STATUS_COMMIT|WHEEL_MSG_STATUS_PLAYING))
					{
						g_data.m_ticker[nInit]->m_nStatus &= ~(WHEEL_MSG_STATUS_COMMIT|WHEEL_MSG_STATUS_PLAYING);
						g_data.m_ticker[nInit]->m_nStatus |= WHEEL_MSG_STATUS_PLAYED;
						// not updating nPlayed because not sure they really ran...
						g_data.SetTickerItem(&db, pdbConn, g_data.m_ticker[nInit], NULL);
					}
				}

				nInit++;
			}
		}

		Sleep(200);
		if(g_data.m_pEvents)
		{
			while(g_data.m_nLastEventMod!=g_data.m_pEvents->m_nLastEventMod)
			{
				Sleep(10); // have to wait until we have gotten some events!
			}
		}
//		_ftime(&now);
//		pData->m_nLastUpdate=now.time;
		
		char info[8192];
		bool bFirst = true;
		while(!g_dlldata.thread[WHEEL_TICKER]->bKillThread)
		{
			_ftime(&pData->timebTick);

			// connect.
			if((pdbConn==NULL)||(!pdbConn->m_bConnected))//g_data.m_tickersocket==NULL)
			{
				pData->nThreadState|=WHEEL_TICKER_ERROR;
				_snprintf(pData->pszThreadStateText, 256, "Error connecting to graphics engine");

				if(pdbConn)
				{
					if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
					{
						if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Ticker:database_connect", errorstring);  //(Dispatch message)

//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Opening connection to %s:%d.", pData->pszHost, pData->nPort); //(Dispatch message)
//				if(net.OpenConnection(pData->pszHost, pData->nPort, &g_data.m_tickersocket, 
//						10000, 10000, info)<NET_SUCCESS)
//				{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Error opening connection to %s:%d.  %s", pData->pszHost, pData->nPort, info); //(Dispatch message)
					//error...
						pData->nThreadState|=WHEEL_TICKER_ERROR;
						_snprintf(pData->pszThreadStateText, 256, "Error opening connection to graphics engine");

	//					_ftime(&now);
	//					pData->m_nLastUpdate=now.time;

	//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "waiting %d ms to open connection to %s:%d.", pData->nShortIntervalMS, pData->pszHost, pData->nPort); //(Dispatch message)
						now.time += pData->nShortIntervalMS/1000;
						now.millitm += (unsigned short)(pData->nShortIntervalMS%1000); // fractional second updates
						if(now.millitm>999)
						{
							now.time++;
							now.millitm%=1000;
						}
									
						while(
										(
											( pData->timebTick.time < now.time )
										||((pData->timebTick.time == now.time)&&(pData->timebTick.millitm < now.millitm))
										)
									&&(!pData->bKillThread)
									)
						{
							_ftime(&pData->timebTick);
						}

						//error...
						g_data.m_tickersocket=NULL;
					}
					else
					{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Connection established to %s:%d.", pData->pszHost, pData->nPort); //(Dispatch message)
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Connection established to %s.", g_settings.m_pszModule); //(Dispatch message)

						strcpy(pData->pszThreadStateText, "");

						if(bFirst)
							pData->nThreadState=WHEEL_TICKER_STOPPED; 
						else
							pData->nThreadState=WHEEL_TICKER_STARTED; // only because on re-establish we want to presume in so we can take out.  if we presume out and its really in we have to put in to take out
						bFirst = false;
						g_data.m_bTickerReady = false; // have to reset this.
					}
				}
			}
			else
			{
				pdbConn = db.CreateNewConnection(g_settings.m_pszDSN, g_settings.m_pszUser, g_settings.m_pszPW);
				if(g_data.m_pdbConn == NULL)
				{
					g_data.m_pdb=&db;
					g_data.m_pdbConn=pdbConn;
				}

			}

//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "pdbConn = %d (pdbConn->m_bConnected)=%d (!(pData->nThreadState&WHEEL_TICKER_ERROR)) = %d", 
//		pdbConn, (pdbConn->m_bConnected),(!(pData->nThreadState&WHEEL_TICKER_ERROR)) ); //(Dispatch message)
			
//			if((g_data.m_tickersocket!=NULL)&&(!(pData->nThreadState&WHEEL_TICKER_ERROR)))

			if(((pdbConn!=NULL)&&(pdbConn->m_bConnected))&&(!(pData->nThreadState&WHEEL_TICKER_ERROR)))
			{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "pData->nThreadControl %d", pData->nThreadControl); //(Dispatch message)
				switch(pData->nThreadControl)
				{
					case WHEEL_TICKER_STOPPED:
					{
						if((pData->nThreadState&WHEEL_TICKER_STARTED) != WHEEL_TICKER_STOPPED)
						{
							//stop ticker

	//						CString szCommand;
	//						szCommand.Format("%cTG%c2%c",1,3,4);

	//						unsigned long ulBufferLen = szCommand.GetLength();
								int nResponse =CLIENT_SUCCESS;
								if((g_settings.m_pszOutEvent)&&(strlen(g_settings.m_pszOutEvent)>0))
								{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "sending out event %s", g_settings.m_pszOutEvent); //(Dispatch message)

									nResponse = g_data.PlayEvent(&db, pdbConn, &g_data.m_pOutEvents, NULL, false, info);
								}

							// send the command....
//							if(net.SendLine((unsigned char*)szCommand.GetBuffer(0), ulBufferLen, g_data.m_tickersocket, EOLN_NONE, false, 
//								5000, info)<NET_SUCCESS)
							if(nResponse<CLIENT_SUCCESS)
							{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "error sending out event %s: %s", g_settings.m_pszOutEvent, info); //(Dispatch message)
								//error
								_ftime(&pData->timebTick);
	//							net.CloseConnection(g_data.m_tickersocket);
								g_data.m_tickersocket = NULL;
								pData->nThreadState|=WHEEL_TICKER_ERROR;
								_snprintf(pData->pszThreadStateText, 256, "Error stopping ticker");

							}
							else
							{
								_ftime(&pData->timebTick);

								if((g_settings.m_pszOutEvent)&&(strlen(g_settings.m_pszOutEvent)>0))
								{
									g_data.SendAsRunMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, "%s: %s ticker process stopped", g_settings.m_pszOutEvent, g_settings.m_pszInternalAppName);
								}
								else
								{
									g_data.SendAsRunMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, "%s ticker process stopped", g_settings.m_pszInternalAppName);
								}
								
								g_data.SendMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, "%s ticker process stopped", g_settings.m_pszInternalAppName);
								
								//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "success sending [%s], receiving.", szCommand); //(Dispatch message)

/*
else nothing, no comm back from libretto on this build

								// recv
								unsigned char* pucBuffer=NULL;
								unsigned long ulBufferLen=0;
								if(net.GetLine(&pucBuffer, &ulBufferLen, g_data.m_tickersocket, NET_RCV_ONCE, info)<NET_SUCCESS)
								{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "error receiving."); //(Dispatch message)

									//error
									_ftime(&pData->timebTick);
									net.CloseConnection(g_data.m_tickersocket);
									g_data.m_tickersocket = NULL;
									pData->nThreadState|=WHEEL_TICKER_ERROR;
									_snprintf(pData->pszThreadStateText, 256, "Error communicating to ticker");
								}
								else
								{
									if((ulBufferLen>0)&&(pucBuffer))
									{

//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "received [%s].", (char*)pucBuffer); //(Dispatch message)
										szCommand.Format("%cSS%c0%cTG%c",1,3,3,4);
										if(szCommand.CompareNoCase((char*)pucBuffer)==0)
										{
*/
											_ftime(&pData->timebTick);
											if(g_data.m_pmsgPlaying)
											{
												g_data.m_pmsgPlaying->m_nStatus &= ~(WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_COMMIT);
												g_data.m_pmsgPlaying->m_nStatus |= WHEEL_MSG_STATUS_PLAYED;
												g_data.m_pmsgPlaying->m_nPlayed++;
												g_data.m_pmsgPlaying->m_nLastPlayed = pData->timebTick.time + (g_settings.m_bUseLocalTime?((pData->timebTick.dstflag?3600:0)-(pData->timebTick.timezone*60)):0);
												g_data.m_pmsgPlaying->m_nLastUpdate=pData->timebTick.time+1;
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "ticker out: setting playing ticker item (playing = %d [%s].", g_data.m_pmsgPlaying->m_nID,  g_data.m_pmsgPlaying->m_szMessage); //(Dispatch message)

												g_data.SetTickerItem(&db, pdbConn, g_data.m_pmsgPlaying, info);
												delete g_data.m_pmsgPlaying;
												g_data.m_pmsgPlaying = NULL;
											}
											if(g_data.m_pmsgNext)
											{
												g_data.m_pmsgNext->m_nStatus &= ~(WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_COMMIT);
// these werent actually played.... so dont say they were!
//												g_data.m_pmsgNext->m_nStatus |= WHEEL_MSG_STATUS_PLAYED;
//												g_data.m_pmsgNext->m_nPlayed++;
//												g_data.m_pmsgNext->m_nLastPlayed = pData->timebTick.time + (g_settings.m_bUseLocalTime?((pData->timebTick.dstflag?3600:0)-(pData->timebTick.timezone*60)):0);
												g_data.m_pmsgNext->m_nLastUpdate=pData->timebTick.time+1;
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "ticker out: setting committed ticker item (committed = %d [%s].", g_data.m_pmsgNext->m_nID,  g_data.m_pmsgNext->m_szMessage); //(Dispatch message)

												g_data.SetTickerItem(&db, pdbConn, g_data.m_pmsgNext, info);
												delete g_data.m_pmsgNext;
												g_data.m_pmsgNext = NULL;
											}

											pData->nThreadState=WHEEL_TICKER_STOPPED;  //resets any error
											strcpy(pData->pszThreadStateText, "");

											_ftime(&pData->timebTick);
											pData->m_dblLastUpdate=(double)(pData->timebTick.time) + ((double)(pData->timebTick.millitm)/1000.0);
											g_data.PublishTicker();


	if(g_ptabmsgr)		g_ptabmsgr->DM(MSG_ICONNONE, szLogDests, szTickerSource, "Ticker has been stopped."); //(Dispatch message)
	/*
//											pData->m_nLastUpdate=pData->timebTick.time;
										}
										else
										{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "ERROR ticker out received [%s].", (char*)pucBuffer); //(Dispatch message)
											//error
											_ftime(&pData->timebTick);
											net.CloseConnection(g_data.m_tickersocket);
											g_data.m_tickersocket = NULL;
											pData->nThreadState|=WHEEL_TICKER_ERROR;
											_snprintf(pData->pszThreadStateText, 256, "Error response from ticker");
										}
										free(pucBuffer);
									}
								}
								*/
							}
							g_dlldata.thread[WHEEL_TICKER]->nThreadState &= ~WHEEL_TICKER_INPROG;// done with operation, remove flag.  
							// is there any way that the operation could be missed and this flag stays there forever? maybe. prob need ot add a counter someday.

						}
					} break;
					case WHEEL_TICKER_STARTED:
					{
						if(!(pData->nThreadState&WHEEL_TICKER_STARTED))
						{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "a"); //(Dispatch message)

							// read in events 
							/*
							get playing event -
							
							get top 1 events where status&playing, sort by index desc; // gets latest playing event

							if none
								get top 1 events, sort by index;
							else
								get top 1 events where index>playing, sort by index;

							if none, 
								get top 1 events where default and index>cursor, sort by index;		
								if!none(cursor = default index
							else
							  cursor - playing index
							*/

							// start ticker
							/*
							if none, 
								just put in the ticker
							else
							  put in the ticker with the first item

							*/

							sprintf(szLogDests, "%s,log", (((g_settings.m_pszAsRunDestination)&&(strlen(g_settings.m_pszAsRunDestination)))?g_settings.m_pszAsRunDestination:"asrun"));

							if( g_data.m_pmsgPlaying) delete  g_data.m_pmsgPlaying;
							if( g_data.m_pmsgNext) delete  g_data.m_pmsgNext;

							g_data.m_pmsgPlaying = NULL;

							g_data.m_szNextTickerMessage = "";
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "a1"); //(Dispatch message)
							g_data.m_pmsgNext = g_data.GetNextTickerMessage(&db, pdbConn, &g_data.m_pmsgPlaying, info);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "a2"); //(Dispatch message)
							if((g_data.m_pmsgNext)||(g_data.m_pmsgNext->m_szMessage))
							{
								g_data.m_szNextTickerMessage = /*.Format("%s", g_core.CALEncodeBrackets(*/g_data.m_pmsgNext->m_szMessage;//));
								g_data.m_pmsgNext->m_nStatus |= WHEEL_MSG_STATUS_COMMIT;
//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "a2b: %s", g_data.m_szNextTickerMessage ); //(Dispatch message)
							}
							else
							{
//							if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Q"); //(Dispatch message)
							}
//							if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "a3"); //(Dispatch message)


/*
							CString szCommand;
							CString szBackplate = _T("");
							CString szLogo = _T("");

							if((g_settings.m_pszTickerBackplateName)&&(strlen(g_settings.m_pszTickerBackplateName)))
								szBackplate.Format("SD%c%d%c%s%c",3, g_settings.m_nTickerBackplateID, 3, g_settings.m_pszTickerBackplateName, 29);

							if((g_settings.m_pszTickerLogoName)&&(strlen(g_settings.m_pszTickerLogoName)))
								szLogo.Format("SD%c%d%c%s%c",3, g_settings.m_nTickerLogoID, 3, g_settings.m_pszTickerLogoName, 29);
*/
							BOOL bInitialMessage = TRUE;
							int nResponse;
							if(g_data.m_szNextTickerMessage.GetLength())
							{
								if((g_settings.m_pszInEvent)&&(strlen(g_settings.m_pszInEvent)>0))
								{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "sending in event %s", g_settings.m_pszInEvent); //(Dispatch message)
							nResponse = g_data.PlayEvent(&db, pdbConn, &g_data.m_pInEvents, NULL, false, info);
								}
	//						if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "sending text event %s with %d, %d (%d), %s", g_settings.m_pszTextEvent,
	//							g_data.m_pTextEvents, g_data.m_pTextEvents->m_ppEvents,g_data.m_pTextEvents->m_nEvents,
	//							((g_data.m_pTextEvents->m_ppEvents)&&(g_data.m_pTextEvents->m_ppEvents[0]))?g_data.m_pTextEvents->m_ppEvents[0]->m_szScene:"NULL"); //(Dispatch message)
							nResponse = g_data.PlayEvent(&db, pdbConn, &g_data.m_pTextEvents, g_data.m_pmsgNext, false, info);
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "sent text event %s", g_settings.m_pszTextEvent); //(Dispatch message)

/*								szCommand.Format("%cST%c0%c%s%sSD%c%d%c%s%c%cTG%c1%c",
									1,3, 29,
									szBackplate,
									szLogo,
									3, g_settings.m_nTickerTextID, 3, g_data.m_szNextTickerMessage, 23, 29,
									3,4);
*/
							}
							else  //either no message or paused
							{
								if((g_settings.m_pszInEvent)&&(strlen(g_settings.m_pszInEvent)>0))
								{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "sending in event %s ", g_settings.m_pszInEvent); //(Dispatch message)
							nResponse = g_data.PlayEvent(&db, pdbConn, &g_data.m_pInEvents, NULL, false, info);
								}
								else nResponse = CLIENT_SUCCESS;
								
								bInitialMessage = FALSE;
/*								szCommand.Format("%cST%c0%cTG%c1%c",
									1,3, 29,
									szBackplate,
									szLogo,
									3,4);
	*/
							}

//							unsigned long ulBufferLen = szCommand.GetLength();
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "sending [%s].", szCommand); //(Dispatch message)

							// send the command....
				//			if(net.SendLine((unsigned char*)szCommand.GetBuffer(0), ulBufferLen, g_data.m_tickersocket, EOLN_NONE, false, 
					//			5000, info)<NET_SUCCESS)

							if (nResponse < CLIENT_SUCCESS)
							{
								//error
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "error %d sending event.", nResponse); //(Dispatch message)
								_ftime(&pData->timebTick);
	//							net.CloseConnection(g_data.m_tickersocket);
								g_data.m_tickersocket = NULL;	
								pData->nThreadState|=WHEEL_TICKER_ERROR;
								_snprintf(pData->pszThreadStateText, 256, "Error starting ticker");

							}
							else
							{
								if((g_settings.m_pszInEvent)&&(strlen(g_settings.m_pszInEvent)>0))
								{
									g_data.SendAsRunMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, "%s: %s ticker process started", g_settings.m_pszInEvent, g_settings.m_pszInternalAppName);
								}
								else
								{
									g_data.SendAsRunMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, "%s ticker process started", g_settings.m_pszInternalAppName);
								}
								g_data.SendMsg(CX_SENDMSG_INFO, g_settings.m_pszInternalAppName, "%s ticker process started", g_settings.m_pszInternalAppName);

								if(g_data.m_szNextTickerMessage.GetLength()) 
								{
									_ftime(&g_data.m_timeLastPlay); 
									if((g_data.m_pmsgNext)&&(g_data.m_pmsgNext->m_dblDwellOverride>0.001))
									{
										g_data.m_nDwellTime = int(g_data.m_pmsgNext->m_dblDwellOverride);
									}
									else
									{
										g_data.m_nDwellTime = g_settings.m_nDefaultDwellTime;
									}
								}

								_ftime(&pData->timebTick);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "success sending [%s], receiving.", szCommand); //(Dispatch message)
/*
								// recv
								unsigned char* pucBuffer=NULL;
								unsigned long ulBufferLen=0;
								if(net.GetLine(&pucBuffer, &ulBufferLen, g_data.m_tickersocket, NET_RCV_ONCE, info)<NET_SUCCESS)
								{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "error receiving."); //(Dispatch message)
									//error
									_ftime(&pData->timebTick);
									net.CloseConnection(g_data.m_tickersocket);
									g_data.m_tickersocket = NULL;
									pData->nThreadState|=WHEEL_TICKER_ERROR;
									_snprintf(pData->pszThreadStateText, 256, "Error communicating to ticker");

								}
								else
								{
									if((ulBufferLen>0)&&(pucBuffer))
									{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "received [%s].", (char*)pucBuffer); //(Dispatch message)
										szCommand.Format("%cSS%c0%cTG%c",1,3,3,4);
										if(szCommand.CompareNoCase((char*)pucBuffer)==0)
										{
*/
											_ftime(&pData->timebTick);
											pData->nThreadState=WHEEL_TICKER_STARTED;  //resets error
											strcpy(pData->pszThreadStateText, "");

											pData->m_dblLastUpdate=(double)(pData->timebTick.time) + ((double)(pData->timebTick.millitm)/1000.0);

	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, szLogDests, szTickerSource, "Ticker has been started."); //(Dispatch message)

											if(g_data.m_pmsgPlaying)
											{
												g_data.m_pmsgPlaying->m_nStatus &= ~(WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_COMMIT);
												g_data.m_pmsgPlaying->m_nStatus |= WHEEL_MSG_STATUS_PLAYED;
												g_data.m_pmsgPlaying->m_nPlayed++;
												g_data.m_pmsgPlaying->m_nLastPlayed = pData->timebTick.time + (g_settings.m_bUseLocalTime?((pData->timebTick.dstflag?3600:0)-(pData->timebTick.timezone*60)):0);
												g_data.m_pmsgPlaying->m_nLastUpdate=pData->timebTick.time+1;

	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "IN setting playing ticker item (playing = %d [%s].", g_data.m_pmsgPlaying->m_nID,  g_data.m_pmsgPlaying->m_szMessage); //(Dispatch message)
												g_data.SetTickerItem(&db, pdbConn, g_data.m_pmsgPlaying, info);

												
												delete g_data.m_pmsgPlaying;
												g_data.m_pmsgPlaying = NULL;

											}

											if(g_data.m_pmsgNext)
											{
												if(g_data.m_pmsgNext->m_nID>0)
												{
													g_data.m_dblTickerMessageCursor = g_data.m_pmsgNext->m_dblIndex;
												}
												else
												{
													g_data.m_dblTickerDefaultCursor = g_data.m_pmsgNext->m_dblIndex;
													g_data.m_dblTickerMessageCursor = 0.0;
												}
												
												g_data.m_pmsgNext->m_nStatus |= WHEEL_MSG_STATUS_PLAYING;
												g_data.m_pmsgNext->m_nLastUpdate=pData->timebTick.time+1;
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "IN setting current ticker item (next = %d [%s]).", g_data.m_pmsgNext->m_nID, g_data.m_pmsgNext->m_szMessage); //(Dispatch message)

												g_data.SetTickerItem(&db, pdbConn, g_data.m_pmsgNext, info);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "set current ticker item (next = %d [%s]).", g_data.m_pmsgNext->m_nID, g_data.m_pmsgNext->m_szMessage); //(Dispatch message)
												g_data.m_pmsgPlaying = g_data.m_pmsgNext;
												g_data.m_pmsgNext = NULL;
											}
											else
											{
												g_data.m_dblTickerMessageCursor = 0.0;
												g_data.m_dblTickerDefaultCursor = 0.0;
											}


											if(bInitialMessage)
											{
												g_data.m_bTickerReady = false;
											}
											else
											{
												g_data.m_bTickerReady = true;
											}
											g_data.m_bTickerGotNext = false;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "m_bTickerGotNext", "false after IN"); //(Dispatch message)
											_ftime(&pData->timebTick);
											pData->m_dblLastUpdate=(double)(pData->timebTick.time) + ((double)(pData->timebTick.millitm)/1000.0);

											g_data.PublishTicker();

											g_data.m_szNextTickerMessage = "";
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "COMPLETE"); //(Dispatch message)
/*
										}
										else
										{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "ERROR ticker in received [%s].", (char*)pucBuffer); //(Dispatch message)

											//error
											_ftime(&pData->timebTick);
											net.CloseConnection(g_data.m_tickersocket);
											g_data.m_tickersocket = NULL;
											pData->nThreadState|=WHEEL_TICKER_ERROR;
											_snprintf(pData->pszThreadStateText, 256, "Error response from ticker");

										}
										free(pucBuffer);
									}
								}
							*/
							}
							g_dlldata.thread[WHEEL_TICKER]->nThreadState &= ~WHEEL_TICKER_INPROG;// done with operation, remove flag.  
							// is there any way that the operation could be missed and this flag stays there forever? maybe. prob need ot add a counter someday.

						}
						else
						{
							//get the next text to play, purge old, etc....
							if((g_data.m_bTickerReady)&&(g_data.m_bTickerGotNext))
							{

	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "sending text event %s", g_settings.m_pszTextEvent); //(Dispatch message)

								int nResponse = g_data.PlayEvent(&db, pdbConn, &g_data.m_pTextEvents, g_data.m_pmsgNext, true, info);
/*
								// send the next one
								CString szCommand;
								szCommand.Format("%cSD%c%d%c%s%c%cTG%c1%c",
										1,
										3, g_settings.m_nTickerTextID, 3, g_data.m_szNextTickerMessage, 23, 29,
										3, 4);
								unsigned long ulBufferLen = szCommand.GetLength();
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "sending [%s].", szCommand); //(Dispatch message)
*/
								// send the command....
				//				if(net.SendLine((unsigned char*)szCommand.GetBuffer(0), ulBufferLen, g_data.m_tickersocket, EOLN_NONE, false, 
				//					5000, info)<NET_SUCCESS)

								if(nResponse<CLIENT_SUCCESS)
								{
									//error
									_ftime(&pData->timebTick);
			//						net.CloseConnection(g_data.m_tickersocket);
									g_data.m_tickersocket = NULL;
									pData->nThreadState|=WHEEL_TICKER_ERROR;
									_snprintf(pData->pszThreadStateText, 256, "Error sending text to ticker");
								}
								else
								{
									_ftime(&pData->timebTick);
									_ftime(&g_data.m_timeLastPlay);

									if((g_data.m_pmsgNext)&&(g_data.m_pmsgNext->m_dblDwellOverride>0.001))
									{
										g_data.m_nDwellTime = int(g_data.m_pmsgNext->m_dblDwellOverride);
									}
									else
									{
										g_data.m_nDwellTime = g_settings.m_nDefaultDwellTime;
									}

/*
								// recv
									unsigned char* pucBuffer=NULL;
									unsigned long ulBufferLen=0;
									if(net.GetLine(&pucBuffer, &ulBufferLen, g_data.m_tickersocket, NET_RCV_ONCE, info)<NET_SUCCESS)
									{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "sent next error receiving."); Sleep(25);//(Dispatch message)
										//error
										_ftime(&pData->timebTick);
										net.CloseConnection(g_data.m_tickersocket);
										g_data.m_tickersocket = NULL;
										pData->nThreadState|=WHEEL_TICKER_ERROR;
										_snprintf(pData->pszThreadStateText, 256, "Error communicating to ticker");

									}
									else
									{
										if((ulBufferLen>0)&&(pucBuffer))
										{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "sent next received [%s].", (char*)pucBuffer); Sleep(25);//(Dispatch message)
											szCommand.Format("%cSS%c0%cTG%c",1,3,3,4);
											if(szCommand.CompareNoCase((char*)pucBuffer)==0)
											{
*/
												strcpy(pData->pszThreadStateText, "");

												_ftime(&pData->timebTick);
												pData->m_dblLastUpdate=(double)(pData->timebTick.time) + ((double)(pData->timebTick.millitm)/1000.0);
												g_data.m_pmsgNext->m_nLastUpdate=pData->timebTick.time+1;
												g_data.PublishTicker();

												// why is this commented out below?
/*
												if(g_data.m_pmsgPlaying)
												{
													g_data.m_pmsgPlaying->m_nStatus &= ~(WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_COMMIT);
													g_data.m_pmsgPlaying->m_nStatus |= WHEEL_MSG_STATUS_PLAYED;
													g_data.m_pmsgPlaying->m_nPlayed++;
													g_data.m_pmsgPlaying->m_nLastPlayed = pData->timebTick.time + (g_settings.m_bUseLocalTime?((pData->timebTick.dstflag?3600:0)-(pData->timebTick.timezone*60)):0);
													g_data.m_pmsgPlaying->m_nLastUpdate=pData->timebTick.time;
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "setting playing ticker item (playing = %d [%s].", g_data.m_pmsgPlaying->m_nID,  g_data.m_pmsgPlaying->m_szMessage); //(Dispatch message)

													g_data.SetTickerItem(&db, pdbConn, g_data.m_pmsgPlaying, info);
													delete g_data.m_pmsgPlaying;
													g_data.m_pmsgPlaying = NULL;
												}
*/
												if(g_data.m_pmsgNext)
												{
													if(g_data.m_pmsgNext->m_nID>0)
													{
														g_data.m_dblTickerMessageCursor = g_data.m_pmsgNext->m_dblIndex;
													}
													else
													{
														g_data.m_dblTickerDefaultCursor = g_data.m_pmsgNext->m_dblIndex;
														g_data.m_dblTickerMessageCursor = 0.0;
													}

													g_data.m_pmsgNext->m_nStatus |= WHEEL_MSG_STATUS_PLAYING;
													g_data.m_pmsgNext->m_nLastUpdate=pData->timebTick.time+1;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "setting current ticker item (next = %d [%s]).", g_data.m_pmsgNext->m_nID, g_data.m_pmsgNext->m_szMessage); //Sleep(25);//(Dispatch message)
													g_data.SetTickerItem(&db, pdbConn, g_data.m_pmsgNext, info);
													g_data.m_pmsgPlaying = g_data.m_pmsgNext;
													g_data.m_pmsgNext = NULL;

												}

												g_data.m_bTickerReady = false;
												g_data.m_bTickerGotNext=false;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "m_bTickerGotNext", "false after sent next"); Sleep(25);//(Dispatch message)

												_ftime(&pData->timebTick);
												pData->m_dblLastUpdate=(double)(pData->timebTick.time) + ((double)(pData->timebTick.millitm)/1000.0);
												g_data.PublishTicker();
/*
											}
											else
											{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "ERROR sent next received [%s].", (char*)pucBuffer); //(Dispatch message)
												//error
												_ftime(&pData->timebTick);
												net.CloseConnection(g_data.m_tickersocket);
												g_data.m_tickersocket = NULL;
												pData->nThreadState|=WHEEL_TICKER_ERROR;
												_snprintf(pData->pszThreadStateText, 256, "Error response from ticker");
											}
											free(pucBuffer);
										}
									}
								*/
								}
							}
							else
							{ // get next one, and check for ready
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "checking next"); //(Dispatch message)

								if(!g_data.m_bTickerGotNext)
								{
									if( g_data.m_pmsgNext)
									{
										delete  g_data.m_pmsgNext; // just to be safe
										g_data.m_pmsgNext = NULL;
									}
									g_data.m_pmsgNext = g_data.GetNextTickerMessage(&db, pdbConn, NULL, info);
									if(g_data.m_pmsgNext)
									{
										_ftime(&pData->timebTick);
										g_data.m_bTickerGotNext = true;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "m_bTickerGotNext", "true after !m_bTickerGotNext"); //(Dispatch message)

										g_data.m_szNextTickerMessage.Format("%s%s", g_settings.m_pszTickerMessagePreamble, /*g_core.CALEncodeBrackets(*/g_data.m_pmsgNext->m_szMessage);//));
										g_data.m_pmsgNext->m_nLastUpdate=pData->timebTick.time+1;
										g_data.m_pmsgNext->m_nStatus |= WHEEL_MSG_STATUS_COMMIT;

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "GOT next [%s]",g_data.m_szNextTickerMessage); //(Dispatch message)
										g_data.SetTickerItem(&db, pdbConn, g_data.m_pmsgNext, info);

										_ftime(&pData->timebTick);
										pData->m_dblLastUpdate=(double)(pData->timebTick.time) + ((double)(pData->timebTick.millitm)/1000.0);
										g_data.PublishTicker();

									}
									else
									{
										g_data.m_dblTickerMessageCursor = 0.0;
										g_data.m_dblTickerDefaultCursor = 0.0;
									}
								}
								
								if(!g_data.m_bTickerReady)
								{

	// have to wait for timeout.
									_timeb timeNow;
									_timeb timeNext;
									_ftime(&timeNow);
							//		_ftime(&g_data.m_timeLastPlay);

									timeNext.time = g_data.m_timeLastPlay.time + g_data.m_nDwellTime/1000;
									timeNext.millitm = g_data.m_timeLastPlay.millitm + g_data.m_nDwellTime%1000;

									while(timeNext.millitm>999)
									{
										timeNext.time++;
										timeNext.millitm -= 1000;
									}
									
									if(
										  (!(pData->nThreadState&WHEEL_TICKER_PAUSED))
										&&(
												(timeNow.time>timeNext.time)
											||((timeNow.time==timeNext.time)&&(timeNow.millitm>timeNext.millitm))
											)
										)
									{

/*

									struct timeval tv;
									tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
									fd_set fds;
									int nNumSockets;
									FD_ZERO(&fds);

									FD_SET(g_data.m_tickersocket, &fds);
									nNumSockets = select(0, &fds, NULL, NULL, &tv);

									if ( nNumSockets == INVALID_SOCKET )
									{
										// report the error but keep going
									/*
									if(pClient->m_lpMsgObj)
										{
											int nErrorCode = WSAGetLastError();
											char* pchError = net.WinsockEnglish(nErrorCode);
											_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError);
											((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Tabulator:CommandHandlerThread");
											free(pchError);
										}
										* /
									
										//close
										_ftime(&pData->timebTick);
										net.CloseConnection(g_data.m_tickersocket);
										g_data.m_tickersocket = NULL;
										pData->nThreadState|=WHEEL_TICKER_ERROR;
										_snprintf(pData->pszThreadStateText, 256, "Lost connection to graphics engine");


									}
									else
									if(
											(nNumSockets==0) // 0 = timed out, -1 = error
										||(!(FD_ISSET(g_data.m_tickersocket, &fds)))
										) 
									{ 
										// no data
									} 
									else // there is recv data.
									{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "status ready!"); //(Dispatch message)

										unsigned char* pucBuffer=NULL;
										unsigned long ulBufferLen=0;
										if(net.GetLine(&pucBuffer, &ulBufferLen, g_data.m_tickersocket, NET_RCV_ONCE, info)<NET_SUCCESS)
										{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "status ready error [%s].", info); //(Dispatch message)
											//error
											_ftime(&pData->timebTick);
											net.CloseConnection(g_data.m_tickersocket);
											g_data.m_tickersocket = NULL;
											pData->nThreadState|=WHEEL_TICKER_ERROR;
											_snprintf(pData->pszThreadStateText, 256, "Lost connection to graphics engine");
										}
										else
										{
											if((ulBufferLen>0)&&(pucBuffer))
											{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "status READY received [%s].", (char*)pucBuffer); //(Dispatch message)
												CString szCommand;
												szCommand.Format("%cSS%c11%c",1,3,4);
												if(szCommand.CompareNoCase((char*)pucBuffer)==0)
												{
												*/
													g_data.m_bTickerReady=true;

													double dblPlayingIndex = g_data.m_dblTickerMessageCursor;
													if(g_data.m_pmsgPlaying)
													{
														g_data.m_pmsgPlaying->m_nStatus &= ~(WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_COMMIT);
														g_data.m_pmsgPlaying->m_nStatus |= WHEEL_MSG_STATUS_PLAYED;
														g_data.m_pmsgPlaying->m_nPlayed++;
														g_data.m_pmsgPlaying->m_nLastPlayed = pData->timebTick.time + (g_settings.m_bUseLocalTime?((pData->timebTick.dstflag?3600:0)-(pData->timebTick.timezone*60)):0);
														dblPlayingIndex = g_data.m_pmsgPlaying->m_dblIndex;

if(g_data.m_pmsgPlaying->m_nID<0)
{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, szLogDests, szTickerSource, "Default id %d played [%s] (%d times so far)", -g_data.m_pmsgPlaying->m_nID,  g_data.m_pmsgPlaying->m_szMessage, g_data.m_pmsgPlaying->m_nPlayed); //(Dispatch message)
}
else
{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, szLogDests, szTickerSource, "Message id %d played [%s] (%d times so far)", g_data.m_pmsgPlaying->m_nID,  g_data.m_pmsgPlaying->m_szMessage, g_data.m_pmsgPlaying->m_nPlayed); //(Dispatch message)
}
														if(g_data.m_pmsgNext)
														{
															if(g_data.m_pmsgNext->m_nID == g_data.m_pmsgPlaying->m_nID)  // so as not to override with old num times played data.
															{
																g_data.m_pmsgNext->m_nPlayed = g_data.m_pmsgPlaying->m_nPlayed;
															}
														}
														g_data.m_pmsgPlaying->m_nLastUpdate=pData->timebTick.time+1;

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "got ready pulse: setting playing ticker item (playing = %d [%s].", g_data.m_pmsgPlaying->m_nID,  g_data.m_pmsgPlaying->m_szMessage); //(Dispatch message)
														g_data.SetTickerItem(&db, pdbConn, g_data.m_pmsgPlaying, info);

														_ftime(&pData->timebTick);
														pData->m_dblLastUpdate=(double)(pData->timebTick.time) + ((double)(pData->timebTick.millitm)/1000.0);
														g_data.PublishTicker();

														// now update the feed one if nec

														if(g_data.m_pmsgPlaying->m_nID>0)  // if not default...
														{
/* remove for now we only have ticker items
															EnterCriticalSection(&g_data.m_critMsg);
															int q = g_data.FindID(g_data.m_pmsgPlaying->m_nID, g_data.m_nLastFoundIndex);
															if(q<0)
															{
																// not found, forget it
																LeaveCriticalSection(&g_data.m_critMsg);
															}
															else
															{
																g_data.m_msg[q]->m_nPlayed=g_data.m_pmsgPlaying->m_nPlayed;
																g_data.m_msg[q]->m_nStatus |= WHEEL_MSG_STATUS_PLAYED;
																_ftime(&pData->timebTick);
																g_data.m_msg[q]->m_nLastPlayed = pData->timebTick.time + (g_settings.m_bUseLocalTime?((pData->timebTick.dstflag?3600:0)-(pData->timebTick.timezone*60)):0);
																g_data.m_msg[q]->m_nLastUpdate=pData->timebTick.time+1;

																_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "%s", g_data.m_msg[q]->SQLUpdate(false,  0.0)	);
																LeaveCriticalSection(&g_data.m_critMsg);

if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SetTickerItem", "setting feed item after ticker play SQL [%s].",szSQL); //(Dispatch message)
			//					AfxMessageBox(szSQL);
																if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
																{
																	//err
														if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SetFeedItem", "setting feed item (%d [%s]) error [%s] SQL [%s].", g_data.m_msg[q]->m_nID, g_data.m_msg[q]->m_szMessage, errorstring, szSQL); //(Dispatch message)


																}
																else
																{
													//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SetTickerItem", "set feed item (%d [%s]) success SQL [%s].", m_msg[q]->m_nID, m_msg[q]->m_szMessage, szSQL); //(Dispatch message)
																	g_data.m_msg[q]->m_nStatus |= WHEEL_MSG_STATUS_INSERTED;
																	if((g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA]))
																	{
																		g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate=(double)(pData->timebTick.time) + ((double)(pData->timebTick.millitm)/1000.0);
																		g_data.PublishData();
																	}
																}

															}
*/
														}


														delete g_data.m_pmsgPlaying;
														g_data.m_pmsgPlaying = NULL;
													}









/////////////////////////////////////////////////
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "retrieving done");// Sleep(100); //(Dispatch message)
												// now, have to see how many done items there are, delete oldest etc.

//ONLY PURGE ACTIVE ONES.  this way you can inactivate ones and keep them around to deal with, change expiry etc.
// first deal with expiry purge yo yos, then do the done count thing thing
											if(g_settings.m_bAutoPurgeExpired)
											{
												_timeb now;
												_ftime(&now);
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE [status] & %d = 0 AND  [active] > 0 AND [expires_on] IS NOT NULL AND [expires_on] > 0 AND [expires_on] < %d",
													g_settings.m_pszTicker, 
													WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_COMMIT, // edge case, do not delete.
													(now.time + (g_settings.m_bUseLocalTime?((now.dstflag?3600:0)-(now.timezone*60)):0)-(g_settings.m_nAutoPurgeExpiredAfterMins*60))
													);
												EnterCriticalSection(&g_data.m_critSQL);

												if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
												{
													//err
										if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Error autopurging expired items with SQL [%s]\n%s.", szSQL, errorstring); //(Dispatch message)

												}
												else
												{
										if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_SQL)) 
											g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Autopurged expired items with [%s].", szSQL); //(Dispatch message)
												}
												LeaveCriticalSection(&g_data.m_critSQL);
											}


											if(g_settings.m_bAutoPurgeExceeded)
											{
												_timeb now;
												_ftime(&now);
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE [status] & %d = 0 AND [active] > 0 AND [allowed_cycles] IS NOT NULL AND [allowed_cycles] > 0 AND [actual_cycles] IS NOT NULL AND [actual_cycles] > 0 AND [allowed_cycles] <= [actual_cycles] AND [last_played] < %d",
													g_settings.m_pszTicker, 
													WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_COMMIT, // edge case, do not delete.
													(now.time + (g_settings.m_bUseLocalTime?((now.dstflag?3600:0)-(now.timezone*60)):0)-(g_settings.m_nAutoPurgeExceededAfterMins*60))
													);
												EnterCriticalSection(&g_data.m_critSQL);

												if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
												{
													//err
										if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Error autopurging expired items with SQL [%s]\n%s.", szSQL, errorstring); //(Dispatch message)

												}
												else
												{
										if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_SQL)) 
											g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Autopurged cycle exceeded items with [%s].", szSQL); //(Dispatch message)
												}
												LeaveCriticalSection(&g_data.m_critSQL);
											}





											if((g_settings.m_bTickerPlayOnce)&&(g_settings.m_nDoneCount<g_data.m_nTickerMessages))
											{
/*
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT [id],[sort_order] FROM %s WHERE [status] & %d <> 0 AND [id] > 0 AND [sort_order] <= %.4f ORDER BY [sort_order] DESC",	
													g_settings.m_pszTicker, 
													WHEEL_MSG_STATUS_PLAYED,
													dblPlayingIndex
													);
*/
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT [id], [status] FROM %s WHERE [status] & %d <> 0 AND [id] > 0 AND [sort_order] <= %.4f ORDER BY [sort_order] DESC",	
													g_settings.m_pszTicker, 
													WHEEL_MSG_STATUS_PLAYED,
													dblPlayingIndex
													);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "retrieve done SQL with [%s]", szSQL);// Sleep(100); //(Dispatch message)
		//	AfxMessageBox(szSQL);

		//	AfxMessageBox(szSQL);
												BOOL bUpdateDone=FALSE;
		EnterCriticalSection(&g_data.m_critSQL);

												if((g_data.m_pdb)&&(g_data.m_pdbConn))	
												{
													CRecordset* prs = g_data.m_pdb->Retrieve(g_data.m_pdbConn, szSQL, errorstring);
													if(prs)
													{
														CString szTemp;
														int nCount=0;
														while(!prs->IsEOF())
														{
															nCount++;
															try
															{
																int nCommitID=-1;
																int nStatus=-1;
																double dblCommitIndex=-.001;


																prs->GetFieldValue("id", szTemp);//HARDCODE
																if(szTemp.GetLength())
																{
																	nCommitID=atoi(szTemp);
																}
/*
																prs->GetFieldValue("index", szTemp);//HARDCODE
																if(szTemp.GetLength())
																{
																	dblCommitIndex=atof(szTemp);
																}
*/

																prs->GetFieldValue("status", szTemp);//HARDCODE
																if(szTemp.GetLength())
																{
																	nStatus=atoi(szTemp);
																}

																if((nCommitID != -1)&&(nCount>g_settings.m_nDoneCount))
																{
											
																	EnterCriticalSection(&g_data.m_critTicker);
									//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "finding");// Sleep(100); //(Dispatch message)
	//																int q = g_data.FindTickerID(nCommitID, dblCommitIndex);
									//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "found %d", q);// Sleep(100); //(Dispatch message)
	//																if(q>=0)
																	{
																		//found, must delete.
					//				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "found status 0x%08x", g_data.m_ticker[q]->m_nStatus);// Sleep(100); //(Dispatch message)
//																		if(!(g_data.m_ticker[q]->m_nStatus&(WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_COMMIT)))
																		if((nStatus!= -1)&&(!(nStatus&(WHEEL_MSG_STATUS_PLAYING|WHEEL_MSG_STATUS_COMMIT))))
																		{
																			/*
																			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE [id]=%d AND [sort_order] >= %.4f AND [sort_order] <= %.4f",
																				g_settings.m_pszTicker, 
																				nCommitID,
																				dblCommitIndex-0.0000009,
																				dblCommitIndex+0.0000009
																				);
																			*/
																			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE [id]=%d",
																				g_settings.m_pszTicker, 
																				nCommitID
																				);
						//				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "***** deleting: SQL [%s]", szSQL);// Sleep(100); //(Dispatch message)

														//					g_data.DeleteTickerMessage(q);
																			LeaveCriticalSection(&g_data.m_critTicker);
																			if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
																			{
																				//err
							if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "ERROR deleting: [%s] for [%s]", errorstring, szSQL);// Sleep(100); //(Dispatch message)

																			}
																			else
																			{
																//				g_data.m_msg[q]->m_nStatus |= WHEEL_MSG_STATUS_INSERTED;
							//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "deleted using SQL [%s]",, szSQL);// Sleep(100); //(Dispatch message)
																			}
																			bUpdateDone=TRUE;

																		}
																
																		else
																		{
																			LeaveCriticalSection(&g_data.m_critTicker);
																		}

																	}
													/*
																	else
																	{
																		LeaveCriticalSection(&g_data.m_critTicker);
																	}
													*/
									/////////////////////////////////												
												
																}

															}
															catch(...)
															{
															}
															prs->MoveNext();
														}
														prs->Close();
														delete prs;
													}

		LeaveCriticalSection(&g_data.m_critSQL);

																_ftime(&pData->timebTick);

													if((bUpdateDone)&&(g_dlldata.thread)&&(g_dlldata.thread[WHEEL_DATA]))
													{
														g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate=(double)(pData->timebTick.time) + ((double)(pData->timebTick.millitm)/1000.0);
														g_data.PublishData();

													}

												}
												else
												{
		LeaveCriticalSection(&g_data.m_critSQL);
												}
											}






/*








												}
												else
												{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "ERROR status ready received [%s].", (char*)pucBuffer); //(Dispatch message)
													//error
													_ftime(&pData->timebTick);
													net.CloseConnection(g_data.m_tickersocket);
													g_data.m_tickersocket = NULL;
												}
												free(pucBuffer);
											}
										}
									}
*/
								}
							}
							}
							_ftime(&pData->timebTick);
						//	pData->m_nLastUpdate=pData->timebTick.time;
						}

					} break;
				} //switch control
			}
			Sleep(1);
		} // while not kill!

		// clear cued, but we can leave playing if there was no out event to clear it.

		if((g_settings.m_pszOutEvent)&&(strlen(g_settings.m_pszOutEvent)>0)&&(g_data.m_pOutEvents)&&(g_data.m_pOutEvents->m_nEvents>0))
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
[status] = [status] & ~%d WHERE [status] & %d <> 0",// AND [sort_order] >= %.4f AND [sort_order] <= %.4f", 
					g_settings.m_pszTicker, 
//					(szMsg?szMsg:m_szMessage),
//					(szAux1?szAux1:m_szAux1),
//					(szAux2?szAux2:m_szAux2),
//					m_dblIndex,
//					m_nTimestamp,
					(WHEEL_MSG_STATUS_COMMIT|WHEEL_MSG_STATUS_PLAYING),
					(WHEEL_MSG_STATUS_COMMIT|WHEEL_MSG_STATUS_PLAYING)
					);  // table name
		}
		else // leave the playing one playing.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
[status] = [status] & ~%d WHERE [status] & %d <> 0",// AND [sort_order] >= %.4f AND [sort_order] <= %.4f", 
					g_settings.m_pszTicker, 
//					(szMsg?szMsg:m_szMessage),
//					(szAux1?szAux1:m_szAux1),
//					(szAux2?szAux2:m_szAux2),
//					m_dblIndex,
//					m_nTimestamp,
					WHEEL_MSG_STATUS_COMMIT,
					WHEEL_MSG_STATUS_COMMIT
					);  // table name
		}
		EnterCriticalSection(&g_data.m_critSQL);

		if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//err
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Error clearing status at thread end with SQL [%s]\n%s.", szSQL, errorstring); //(Dispatch message)

		}
		else
		{
if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_SQL)) 
g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "clearing status at thread end with [%s].", szSQL); //(Dispatch message)
		}
		LeaveCriticalSection(&g_data.m_critSQL);

//		net.CloseConnection(g_data.m_tickersocket);
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Ticker thread for [%s:%d] ended.", g_dlldata.thread[WHEEL_TICKER]->pszHost, g_dlldata.thread[WHEEL_TICKER]->nPort );  //(Dispatch message)

		g_dlldata.thread[WHEEL_TICKER]->bThreadStarted=false;
	}
}


void SynchronizationThread(void* pvArgs)
{	
//	AfxMessageBox("X");
	while((g_pdlg==NULL)&&(!g_settings.m_bIsServer)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread)) Sleep(100); // stall until we have a dlg.
//AfxMessageBox("Y");
	if(g_pdlg)
	{
		if(!g_settings.m_bIsServer)
		{
			while((!g_pdlg->m_bNewSizeInit)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread)) Sleep(100);
		}
//AfxMessageBox("Q");
		if(!g_settings.m_bIsServer)
		{
			while((!g_pdlg->m_bChoiceMade)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread)) Sleep(100);
		}
	}
//AfxMessageBox("Z");

if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SynchronizationThread", "thread begun"); //(Dispatch message)

	if((g_dlldata.thread)&&(g_dlldata.nNumThreads>WHEEL_UI))
	{
		if(g_dlldata.thread[WHEEL_UI]->bThreadStarted) return; // one only!

		g_dlldata.thread[WHEEL_UI]->bThreadStarted=true;
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "DataSynchronizer:begin", "DataSynchronizer thread begun." );  //(Dispatch message)
		DLLthread_t* pData = g_dlldata.thread[WHEEL_UI];
		CNetData* pdata = new CNetData;
//		char errorstring[MAX_MESSAGE_LENGTH];
		_ftime(&g_dlldata.thread[WHEEL_UI]->timebTick);
//AfxMessageBox("Z0");

		if((g_pdlg)&&(!g_settings.m_bIsServer))
		{
			if(g_pdlg->m_bDontGetOlderFeedRecords)
			{
				g_pdlg->m_dblLastDataUpdate = (double)(g_dlldata.thread[WHEEL_UI]->timebTick.time-300); //5 mins
//				CString foo; foo.Format("data :%.0f", g_pdlg->m_dblLastDataUpdate); AfxMessageBox(foo);
			}
		}

		while(!g_dlldata.thread[WHEEL_UI]->bKillThread)
		{
			Sleep(20);
			if(g_data.m_bDialogStarted)
			{
				// have to be connected to server.
				if((g_data.m_socket==NULL)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
				{
					char info[8192];
			//EnterCriticalSection(&g_data.m_critClientSocket);
					if(g_data.m_net.OpenConnection(g_settings.m_szHost.GetBuffer(0), g_settings.m_nPort, &g_data.m_socket, 
						10000, 10000, info)<NET_SUCCESS)
					{
			//LeaveCriticalSection(&g_data.m_critClientSocket);
//						MessageBox(info, "Network Error", MB_OK);
						pData->nThreadState=WHEEL_SYNCH_ERROR;
						_snprintf(pData->pszThreadStateText, 256, "Error connecting to host");
					}
					else
					{
			//LeaveCriticalSection(&g_data.m_critClientSocket);
					}
				}

				if((g_data.m_socket!=NULL)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
				{

					if(g_settings.m_bIsServer)
					{
						// dont need to send for status...
						pData->nThreadState=WHEEL_SYNCH_STARTED;
						strcpy(pData->pszThreadStateText, "");

					}
					else
					{
						// get the settings!
						if(pdata)
						{

							pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

							pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
							pdata->m_ucSubCmd = 0;       // the subcommand byte

							char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
							if(pch)
							{
								sprintf(pch, "%s|Server_Get_Status", g_settings.m_pszInternalAppName);

								pdata->m_pucData =  (unsigned char*) pch;
								pdata->m_ulDataLen = strlen(pch);
							}

					//AfxMessageBox("sending");
					//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
					//AfxMessageBox( (char*)pdata->m_pucData );
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "entering status 0");// Sleep(100); //(Dispatch message)
				//if(!//g_data.m_bTransact)
			//	{
					//g_data.m_bTransact = true;
	EnterCriticalSection(&g_data.m_critTransact);
				//EnterCriticalSection(&g_data.m_critClientSocket);
							int nRV = NET_ERROR;
							if(!g_dlldata.thread[WHEEL_UI]->bKillThread)
							{
								nRV = g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT);
							}
							if(nRV>=NET_SUCCESS)
							{
								if(!g_dlldata.thread[WHEEL_UI]->bKillThread) 
								{
									g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
								}
								else
								{
									free(pdata->m_pucData);
									pdata->m_ulDataLen=0;
									pdata->m_pucData = NULL;
								}
								//send ack
					//			AfxMessageBox("ack");
				//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
	//g_data.m_bTransact = false;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "left status 0");// Sleep(100); //(Dispatch message)


								pData->nThreadState=WHEEL_SYNCH_STARTED;

								// here de-compile the status message and do what we need to do on the UI.
								// status return shall be:

								// time|datalastupdate|datastate|time|tickerlastupdate|tickerstate

								if((pdata->m_pucData)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
								{
if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_STATUS))
{
	char szSource[MAX_PATH]; sprintf(szSource, "%s_Status", g_settings.m_pszInternalAppName);
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "status was [%s]", pdata->m_pucData);// Sleep(100); //(Dispatch message)
}

									CSafeBufferUtil sbu;
									pch = sbu.Token((char*)pdata->m_pucData, pdata->m_ulDataLen, "|", MODE_MULTIDELIM);

									int n=0;
									while((pch)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
									{
										switch(n)
										{
										case 0: 
											{
												g_dlldata.thread[WHEEL_DATA]->timebTick.time = atoi(pch);
												g_dlldata.thread[WHEEL_DATA]->timebTick.millitm = ((int)(atof(pch) - (double)g_dlldata.thread[WHEEL_DATA]->timebTick.time))*1000;
											} break;
										case 1: 
											{
												g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate = atof(pch);
											} break;
										case 2: 
											{
												g_dlldata.thread[WHEEL_DATA]->nThreadState = atoi(pch);
											} break;
										case 3: 
											{
												g_dlldata.thread[WHEEL_TICKER]->timebTick.time = atoi(pch);
												g_dlldata.thread[WHEEL_TICKER]->timebTick.millitm = ((int)(atof(pch) - (double)g_dlldata.thread[WHEEL_TICKER]->timebTick.time))*1000;
											} break;
										case 4: 
											{
												g_dlldata.thread[WHEEL_TICKER]->m_dblLastUpdate = atof(pch);
											} break;
										case 5: 
											{
												g_dlldata.thread[WHEEL_TICKER]->nThreadState = atoi(pch);
											} break;
										case 6: 
											{
												g_data.m_nMessagesDelta = g_data.m_nMessages-atoi(pch);
											} break;
										case 7: 
											{
												g_data.m_nTickerMessagesDelta = g_data.m_nTickerMessages-atoi(pch);
											} break;
										case 8: 
											{
												_snprintf(g_dlldata.thread[WHEEL_DATA]->pszThreadStateText, 255, "%s", pch);
											} break;
										case 9: 
											{
												_snprintf(g_dlldata.thread[WHEEL_TICKER]->pszThreadStateText, 255, "%s", pch);
											} break;
										}
										n++;

										pch = sbu.Token(NULL, NULL, "|", MODE_MULTIDELIM);
									}

								}

								if(pdata->m_pucData)
								{
									free(pdata->m_pucData);
									pdata->m_pucData=NULL;
									pdata->m_ulDataLen=0;
								}
					
							}
							else
							{
				//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
	//g_data.m_bTransact = false;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "left status 1");// Sleep(100); //(Dispatch message)

//AfxMessageBox("Could not get status.");
								if(!g_dlldata.thread[WHEEL_UI]->bKillThread) Sleep(1000);
								pData->nThreadState=WHEEL_SYNCH_ERROR;
								_snprintf(pData->pszThreadStateText, 256, "Error getting status from host");
								g_data.m_socket=NULL; // rests conn


							}
	//			}
						}
						else
						{
							pdata = new CNetData;
						}
					}

					if(pData->nThreadState==WHEEL_SYNCH_STARTED)
					{
						if(
							  (!g_dlldata.thread[WHEEL_UI]->bKillThread)
							&&(g_data.m_socket!=NULL)
							&&(
							    (g_pdlg->m_dblLastDataUpdate!=g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate)
								||(g_data.m_nMessagesDelta>0)
								||(g_data.m_bMessagesUpcounting)
								)
							)
						{//download everything new

							if(g_settings.m_bIsServer)
							{
								// we already have the event stacks set up

								//set equal on success
								g_pdlg->m_dblLastDataUpdate = g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate;
							}
#ifndef REMOVE_CLIENTSERVER
							else if(g_data.m_bDialogStarted)
							{
								// get the settings!
								if(pdata)
								{

									pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

									pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
									pdata->m_ucSubCmd = 0;       // the subcommand byte

									char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
									if(pch)
									{
										if((g_data.m_nMessagesDelta>0)&&(!g_data.m_bMessagesUpcounting)) // have to start from beginning
										{
											g_data.m_nMessagesUpcounting = 0; // starts over.
											if(g_pdlg->m_bDontGetOlderFeedRecords)
											{
												_ftime(&g_dlldata.thread[WHEEL_UI]->timebTick);
												sprintf(pch, "%s|Server_Data_Get|%d|0", g_settings.m_pszInternalAppName, g_dlldata.thread[WHEEL_UI]->timebTick.time-300 );
											}
											else
											{
												sprintf(pch, "%s|Server_Data_Get|0|0", g_settings.m_pszInternalAppName );
											}
										}
										else
										{
											sprintf(pch, "%s|Server_Data_Get|%.0f|%d", g_settings.m_pszInternalAppName, g_pdlg->m_dblLastDataUpdate, g_data.m_nMessagesUpcounting );
										}

if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_FEED)) 
{
	CString szSource; szSource.Format("%s_Feed", g_settings.m_pszInternalAppName)
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "calling: %s",pch);// Sleep(100); //(Dispatch message)
}
										pdata->m_pucData =  (unsigned char*) pch;
										pdata->m_ulDataLen = strlen(pch);
									}

	g_pdlg->m_fsFontData.crBG= RGB(0,0,0);
	g_pdlg->m_fsFontData.crText= RGB(255,255,0);
	g_pdlg->m_editexData.SetFont( g_pdlg->m_fsFontData, 0,-1);
	g_pdlg->m_editexData.GetWnd()->SetWindowText( "Downloading...");


							//AfxMessageBox("sending");
							//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
							//AfxMessageBox( (char*)pdata->m_pucData );
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "entering Server_Data_Get");// Sleep(100); //(Dispatch message)
				//if(!//g_data.m_bTransact)
				{
					//g_data.m_bTransact = true;
	EnterCriticalSection(&g_data.m_critTransact);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "entered Server_Data_Get");// Sleep(100); //(Dispatch message)
						//EnterCriticalSection(&g_data.m_critClientSocket);
									int nRV = NET_ERROR;
									if(!g_dlldata.thread[WHEEL_UI]->bKillThread)
											nRV = g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, errorstring);
									if(nRV>=NET_SUCCESS)
									{
//	g_pdlg->m_editexData.GetWnd()->SetWindowText( "Compiling...");
										if(!g_dlldata.thread[WHEEL_UI]->bKillThread) 
										{
											g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
										}
										else
										{
											free(pdata->m_pucData);
											pdata->m_ulDataLen=0;
											pdata->m_pucData = NULL;
										}

										//send ack
							//			AfxMessageBox("ack");
						//LeaveCriticalSection(&g_data.m_critClientSocket);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "leaving Server_Data_Get");// Sleep(100); //(Dispatch message)
	LeaveCriticalSection(&g_data.m_critTransact);
//g_data.m_bTransact = false;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "left Server_Data_Get");// Sleep(100); //(Dispatch message)

										if(pdata->m_ucSubCmd == CLIENT_REFRESH)
										{
											g_data.m_bMessagesUpcounting=true;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_FEED)) 
{
	CString szSource; szSource.Format("%s_Feed", g_settings.m_pszInternalAppName)
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "feed returned CLIENT_REFRESH");// Sleep(100); //(Dispatch message)
}
										}
										else // reset in case of error too.
										{
											g_data.m_bMessagesUpcounting=false;
											g_data.m_nMessagesUpcounting = 0;
										}
//(!g_dlldata.thread[WHEEL_UI]->bKillThread)
										// here de-compile the data message and do what we need to do on the UI.
										if((pdata->m_pucData)&&(pdata->m_ucSubCmd != CLIENT_ERROR)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
										{
											CSafeBufferUtil sbu;
											char chMessage[2];
											char chField[2];
											sprintf(chMessage, "%c", 29); //GS
											sprintf(chField, "%c", 28);  //FS
											pch = sbu.Token((char*)pdata->m_pucData, pdata->m_ulDataLen, chMessage, MODE_MULTIDELIM);

											int nMaxTimestamp = 0;

											int n=0;
											while((pch)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
											{
												switch(n)
												{
												case 0: 
													{
														n=atoi(pch);
if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_FEED)) 
{
	CString szSource; szSource.Format("%s_Feed", g_settings.m_pszInternalAppName)
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "feed packet returned %d messages", n);// Sleep(100); //(Dispatch message)
}
// use the tally as the skip counter
														if(pdata->m_ucSubCmd == CLIENT_REFRESH)
														{
															g_data.m_nMessagesUpcounting += n;

if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_FEED)) 
{
	CString szSource; szSource.Format("%s_Feed", g_settings.m_pszInternalAppName)
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "feed returned CLIENT_REFRESH");// Sleep(100); //(Dispatch message)
}
														}
														else
														{
															g_data.m_nMessagesUpcounting = 0;
														}


													} break;
												default:
													{
														CWheelMessage* pmsg = new CWheelMessage;
														if(pmsg)
														{
															if(g_data.StringToMessage(pch, pmsg)>=CLIENT_SUCCESS)
															{
																if(pmsg->m_nTimestamp>nMaxTimestamp) nMaxTimestamp = pmsg->m_nTimestamp;

																EnterCriticalSection(&g_data.m_critMsg);
																int q = g_data.FindID(pmsg->m_nID, g_data.m_nLastFoundIndex);
																if(q<0)
																{
																	// not found, must add
																	LeaveCriticalSection(&g_data.m_critMsg);
																	g_data.InsertMessage(pmsg);
																	if(g_data.m_nMessagesDelta>0)pmsg->m_nStatus|=WHEEL_MSG_STATUS_TALLY;
																}
																else
																{
																	//found, must modify.
																	if(g_data.m_msg[q])
																	{
																		if(g_data.m_msg[q]->m_nStatus&WHEEL_MSG_STATUS_QUEUED)
																		{// don't lose these changes.
																			pmsg->m_nStatus |= (WHEEL_MSG_STATUS_QUEUED|(g_data.m_msg[q]->m_nStatus&WHEEL_MSG_MARK_MASK));

																			if(g_data.m_msg[q]->m_nStatus&WHEEL_MSG_STATUS_REVISED)
																			{
																				if(g_data.m_msg[q]->m_szRevisedMessage.GetLength())
																				{
																					pmsg->m_szRevisedMessage = g_data.m_msg[q]->m_szRevisedMessage;
																				}
																			}
																		}
																		*g_data.m_msg[q] = *pmsg;
																		delete pmsg; pmsg=NULL;
																	}
																	else
																	{
																		g_data.m_msg[q] = pmsg;
																	}
																	if(g_data.m_nMessagesDelta>0) g_data.m_msg[q]->m_nStatus|=WHEEL_MSG_STATUS_TALLY;

																	g_data.m_nLastFoundIndex = q;
																	LeaveCriticalSection(&g_data.m_critMsg);
																}
															}
														}
													} break;
												}
											
											//	n++;

												pch = sbu.Token(NULL, NULL, chMessage, MODE_MULTIDELIM);
											}

if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_FEED)) 
{
	CString szSource; szSource.Format("%s_Feed", g_settings.m_pszInternalAppName)
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "nMaxTimestamp is now %d", nMaxTimestamp);// Sleep(100); //(Dispatch message)
}

											if(pdata->m_pucData)
											{
												free(pdata->m_pucData);
												pdata->m_pucData=NULL;
												pdata->m_ulDataLen=0;
											}

											if((!g_data.m_bMessagesUpcounting )&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
											{
if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_FEED)) 
{
	CString szSource; szSource.Format("%s_Feed", g_settings.m_pszInternalAppName)
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "not upcounting; removing feed items");// Sleep(100); //(Dispatch message)
}
											// have to remove not found ones.
												if(g_data.m_nMessagesDelta>0)
												{
													int j=0;
													EnterCriticalSection(&g_data.m_critMsg);

													if(g_data.m_msg)
													{
														while((j<g_data.m_nMessages)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
														{
															if(g_data.m_msg[j])
															{
																if(!(g_data.m_msg[j]->m_nStatus&WHEEL_MSG_STATUS_TALLY))
																{
																	g_data.DeleteMessage(j);
																}
																else
																{
																	g_data.m_msg[j]->m_nStatus &= ~WHEEL_MSG_STATUS_TALLY;
																	j++;
																}
															}
															else
															{
																j++;
															}														
														}
													}
													LeaveCriticalSection(&g_data.m_critMsg);
												}
											}

											// sorting the data on every turn.  I guess that is OK even if upcounting.

											//sort the data
											if((n>0)&&(g_data.m_msg)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
											{
												EnterCriticalSection(&g_data.m_critMsg);

											// after things obtained, do a cocktail sort on timestamp
												g_data.m_bSortTimestampsAsc=g_pdlg->m_bSortTimestampsAsc;

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "cocktail sort on timestamp"); //(Dispatch message)

												int last = g_data.m_nMessages-1;
												int first = 0;
												int ix=0;
												BOOL bDone = TRUE; 
												CWheelMessage* pmsg=NULL;
												if(g_pdlg->m_bSortTimestampsAsc)
												{
													while((bDone == TRUE)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
													{
														bDone = FALSE; 
														for(ix = first; ix < last; ix++)
														{
															if((g_data.m_msg[ix])&&(g_data.m_msg[ix+1]))
															{
																if(g_data.m_msg[ix]->m_nTimestamp > g_data.m_msg[ix+1]->m_nTimestamp)
																{
																	pmsg = g_data.m_msg[ix]; 
																	g_data.m_msg[ix] = g_data.m_msg[ix+1]; 
																	g_data.m_msg[ix+1]=pmsg;
																	bDone = TRUE;
	//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "swap from last"); //(Dispatch message)

																}
															}
														}
														last--; 
														for(ix = last; ix > first; ix--)
														{
															if((g_data.m_msg[ix])&&(g_data.m_msg[ix-1]))
															{
																if(g_data.m_msg[ix]->m_nTimestamp < g_data.m_msg[ix-1]->m_nTimestamp)
																{
																	pmsg = g_data.m_msg[ix]; 
																	g_data.m_msg[ix] = g_data.m_msg[ix-1]; 
																	g_data.m_msg[ix-1]=pmsg;
	//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "swap from first"); //(Dispatch message)
																	bDone = TRUE;
																}
															}
														}
														first++;  
													}

												}
												else
												{
													while((bDone == TRUE)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
													{
														bDone = FALSE; 
														for(ix = first; ix < last; ix++)
														{
															if((g_data.m_msg[ix])&&(g_data.m_msg[ix+1]))
															{
																if(g_data.m_msg[ix]->m_nTimestamp < g_data.m_msg[ix+1]->m_nTimestamp)
																{
																	pmsg = g_data.m_msg[ix]; 
																	g_data.m_msg[ix] = g_data.m_msg[ix+1]; 
																	g_data.m_msg[ix+1]=pmsg;
																	bDone = TRUE;
	//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "swap from last"); //(Dispatch message)

																}
															}
														}
														last--; 
														for(ix = last; ix > first; ix--)
														{
															if((g_data.m_msg[ix])&&(g_data.m_msg[ix-1]))
															{
																if(g_data.m_msg[ix]->m_nTimestamp > g_data.m_msg[ix-1]->m_nTimestamp)
																{
																	pmsg = g_data.m_msg[ix]; 
																	g_data.m_msg[ix] = g_data.m_msg[ix-1]; 
																	g_data.m_msg[ix-1]=pmsg;
	//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "swap from first"); //(Dispatch message)
																	bDone = TRUE;
																}
															}
														}
														first++;  
													}
												}
												LeaveCriticalSection(&g_data.m_critMsg);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "performed cocktail sort on timestamp"); //(Dispatch message)
											}




	if(g_dlldata.thread[WHEEL_DATA]->nThreadState&WHEEL_DATA_ERROR)
	{
		g_pdlg->m_fsFontData.crText= RGB(0,0,0);
		g_pdlg->m_fsFontData.crBG= RGB(255,0,0);
		g_pdlg->m_editexData.SetFont( g_pdlg->m_fsFontData, 0,-1);
		if(strlen(g_dlldata.thread[WHEEL_DATA]->pszThreadStateText)>0)
		{
			g_pdlg->m_editexData.GetWnd()->SetWindowText( "Data feed error");
		}
		else
		{
			g_pdlg->m_editexData.GetWnd()->SetWindowText(g_dlldata.thread[WHEEL_DATA]->pszThreadStateText);
		}
	}
	else
	{
		g_pdlg->m_fsFontData.crBG= RGB(0,0,0);
		g_pdlg->m_fsFontData.crText= RGB(0,255,0);
		g_pdlg->m_editexData.SetFont( g_pdlg->m_fsFontData, 0,-1);
		CString szText; szText.Format("Data feed %s", ((g_dlldata.thread[WHEEL_DATA]->nThreadState&WHEEL_DATA_STARTED)?"downloading":"stopped"));
		g_pdlg->m_editexData.GetWnd()->SetWindowText( szText);
	}

											//set equal on success
											if( !g_data.m_bMessagesUpcounting )
											{
if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_FEED)) 
{
	CString szSource; szSource.Format("%s_Feed", g_settings.m_pszInternalAppName)
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "finished upcounting, setting last update to %.3f", g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate);// Sleep(100); //(Dispatch message)
}												g_pdlg->m_dblLastDataUpdate = g_dlldata.thread[WHEEL_DATA]->m_dblLastUpdate;
											}
											else
											{
if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_FEED)) 
{
	CString szSource; szSource.Format("%s_Feed", g_settings.m_pszInternalAppName)
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "still upcounting, processed %d messages so far", g_data.m_nMessagesUpcounting);// Sleep(100); //(Dispatch message)
}
//												g_pdlg->m_dblLastDataUpdate = (double)nMaxTimestamp; // causes next batch to go.
// this did not work because timestamps are not in order in feed
											}
										}

										if(pdata->m_pucData)
										{
											free(pdata->m_pucData);
											pdata->m_ulDataLen=0;
											pdata->m_pucData = NULL;
										}
									}
									else
									{
						//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
	//g_data.m_bTransact = false;

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "left Server_Data_Get 1");// Sleep(100); //(Dispatch message)

//AfxMessageBox("Could not get playstack info.");
	g_pdlg->m_fsFontData.crBG= RGB(0,0,0);
	g_pdlg->m_fsFontData.crText= RGB(255,0,0);
	g_pdlg->m_editexData.SetFont( g_pdlg->m_fsFontData, 0,-1);
//	CString szError; szError.Format("Comm error %d: %s", nRV, errorstring);
	CString szError; szError.Format("Comm error %d", nRV);
	g_pdlg->m_editexData.GetWnd()->SetWindowText(szError);
								_snprintf(pData->pszThreadStateText, 256, "Error getting feed data");
								g_data.m_socket=NULL; // rests conn


									}
						}
								}
								else
								{
									pdata = new CNetData;
								}
						
								if(g_pdlg)
								{
									LVCOLUMN lvColumn;
									lvColumn.mask = LVCF_TEXT;
									char count[128];
									sprintf(count, "Available Items (%d)", g_data.m_nMessages);
									lvColumn.pszText  = count;
									g_pdlg->m_lceData.SetColumn(0, &lvColumn);
								}
							}

#endif //#ifndef REMOVE_CLIENTSERVER

						}
//						CString foo; foo.Format("%d %d",g_pdlg->m_nLastTickerUpdate,g_dlldata.thread[WHEEL_TICKER]->m_nLastUpdate);
//AfxMessageBox(foo);
						if(
							  (!g_dlldata.thread[WHEEL_UI]->bKillThread)
							&&(g_data.m_socket!=NULL)
							&&(
							    (g_pdlg->m_dblLastTickerUpdate!=g_dlldata.thread[WHEEL_TICKER]->m_dblLastUpdate)
								||(g_data.m_nTickerMessagesDelta>0)
								||(g_data.m_bMovedItem)
								||(g_data.m_bTickerMessagesUpcounting)
								)
							)
						{//download everything new
				
							if(g_settings.m_bIsServer)
							{
								// we already have the event stacks set up

								//set equal on success
								g_pdlg->m_dblLastTickerUpdate = g_dlldata.thread[WHEEL_TICKER]->m_dblLastUpdate;
							}
#ifndef REMOVE_CLIENTSERVER
							else if(g_data.m_bDialogStarted)
							{
								// get the settings!
								if(pdata)
								{

									pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

									pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
									pdata->m_ucSubCmd = 0;       // the subcommand byte

									char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
									if(pch)
									{
										if(((g_data.m_nTickerMessagesDelta>0)||(g_data.m_bMovedItem))&&(!g_data.m_bTickerMessagesUpcounting))
										{
											g_data.m_nTickerMessagesUpcounting = 0; // have to start over
											sprintf(pch, "%s|Server_Ticker_Get|-2|0", g_settings.m_pszInternalAppName ); //sometimes we get -1 timestamps for soem reason.. argh
										}
										else
										{
											sprintf(pch, "%s|Server_Ticker_Get|%.0f|%d", g_settings.m_pszInternalAppName, g_pdlg->m_dblLastTickerUpdate, g_data.m_nTickerMessagesUpcounting );
										}

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "%s", pch); //(Dispatch message)

										pdata->m_pucData =  (unsigned char*) pch;
										pdata->m_ulDataLen = strlen(pch);
									}

	g_pdlg->m_fsFontTicker.crBG= RGB(0,0,0);
	g_pdlg->m_fsFontTicker.crText= RGB(255,255,0);
	g_pdlg->m_editexTicker.SetFont( g_pdlg->m_fsFontTicker, 0,-1);
	g_pdlg->m_editexTicker.GetWnd()->SetWindowText( "Updating...");

							//AfxMessageBox("sending");
							//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
							//AfxMessageBox( (char*)pdata->m_pucData );
	//if(!//g_data.m_bTransact)
		{
		//g_data.m_bTransact = true;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "enter Server_Ticker_Get");// Sleep(100); //(Dispatch message)
	EnterCriticalSection(&g_data.m_critTransact);
						//EnterCriticalSection(&g_data.m_critClientSocket);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "entered Server_Ticker_Get");// Sleep(100); //(Dispatch message)

									int nRV = NET_ERROR;
									if(!g_dlldata.thread[WHEEL_UI]->bKillThread)
											nRV = g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, errorstring);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "reply from Server_Ticker_Get %d", nRV);// Sleep(100); //(Dispatch message)
									if(nRV>=NET_SUCCESS)
									{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "reply back Server_Ticker_Get");// Sleep(100); //(Dispatch message)
										if(!g_dlldata.thread[WHEEL_UI]->bKillThread) 
										{
											g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
										}
										else
										{
											free(pdata->m_pucData);
											pdata->m_ulDataLen=0;
										}
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "replied Server_Ticker_Get");// Sleep(100); //(Dispatch message)
//	g_pdlg->m_editexTicker.GetWnd()->SetWindowText( "Compiling...");
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "leaving Server_Ticker_Get");// Sleep(100); //(Dispatch message)
										//send ack
							//			AfxMessageBox("ack");
						//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
	//g_data.m_bTransact = false;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "left Server_Ticker_Get");// Sleep(100); //(Dispatch message)


										if(pdata->m_ucSubCmd == CLIENT_REFRESH)
										{
											g_data.m_bTickerMessagesUpcounting = true;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_FEED)) 
{
	CString szSource; szSource.Format("%s_Ticker", g_settings.m_pszInternalAppName)
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "feed returned CLIENT_REFRESH");// Sleep(100); //(Dispatch message)
}
										}
										else
										{
											g_data.m_bTickerMessagesUpcounting = false; //also resets in case of err
											g_data.m_nTickerMessagesUpcounting = 0; //also resets in case of err
										}

										// here de-compile the data message and do what we need to do on the UI.
										if((pdata->m_pucData)&&(pdata->m_ucSubCmd != CLIENT_ERROR)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
										{
											CSafeBufferUtil sbu;
											char chMessage[2];
											char chField[2];
											sprintf(chMessage, "%c", 29); //GS
											sprintf(chField, "%c", 28);  //FS
											pch = sbu.Token((char*)pdata->m_pucData, pdata->m_ulDataLen, chMessage, MODE_MULTIDELIM);

											if(!g_data.m_bTickerMessagesUpcounting) g_data.m_bMovedItem = false;

											int nMaxTimestamp = 0;

											int n=0;
											while((pch)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
											{
												switch(n)
												{
												case 0: 
													{
														n=atoi(pch);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "Got %d items", n); //(Dispatch message)
														if(pdata->m_ucSubCmd == CLIENT_REFRESH) g_data.m_nTickerMessagesUpcounting += n;
														else g_data.m_nTickerMessagesUpcounting = 0;
													} break;
												default:
													{
														CWheelMessage* pmsg = new CWheelMessage;
														if(pmsg)
														{
															if(g_data.StringToMessage(pch, pmsg)>=CLIENT_SUCCESS)
															{

																if(pmsg->m_nTimestamp>nMaxTimestamp) nMaxTimestamp = pmsg->m_nTimestamp;

																EnterCriticalSection(&g_data.m_critTicker);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "finding %d  %.4f", pmsg->m_nID, pmsg->m_dblIndex); //(Dispatch message)

																int q = g_data.FindTickerID(pmsg->m_nID, pmsg->m_dblIndex);
																if(q<0)
																{
																	// not found, must add
																	LeaveCriticalSection(&g_data.m_critTicker);
																	g_data.InsertTickerMessage(pmsg);

																	if(g_data.m_nTickerMessagesDelta==0)
																	{
																		// then we just inserted but shouldna, so we must have moved.
																		g_data.m_bMovedItem=true;
																	}
																	else
																	if(g_data.m_nTickerMessagesDelta>0) pmsg->m_nStatus|=WHEEL_MSG_STATUS_TALLY;
																}
																else
																{
																	//found, must modify.
																	if(g_data.m_ticker[q])
																	{
																		*g_data.m_ticker[q] = *pmsg;
																		delete pmsg; pmsg=NULL;
																	}
																	else
																	{
																		g_data.m_ticker[q] = pmsg;
																	}

																	if(g_data.m_nTickerMessagesDelta>0) g_data.m_ticker[q]->m_nStatus|=WHEEL_MSG_STATUS_TALLY;

									//								g_data.m_nLastFoundIndex = q;
																	LeaveCriticalSection(&g_data.m_critTicker);
																}
															}
														}
													} break;
												}
											
											//	n++;

												if(g_data.m_bMovedItem)
													pch = NULL;
												else
													pch = sbu.Token(NULL, NULL, chMessage, MODE_MULTIDELIM);
											}

											if(pdata->m_pucData)
											{
												free(pdata->m_pucData);
												pdata->m_pucData=NULL;
												pdata->m_ulDataLen=0;
											}
										
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "retrieved %d, count = %d", n, g_data.m_nTickerMessages); //(Dispatch message)
											if((!g_data.m_bTickerMessagesUpcounting)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
											{
											// have to remove not found ones.
												if(g_data.m_nTickerMessagesDelta>0)
												{
													int j=0;
													EnterCriticalSection(&g_data.m_critTicker);

													if(g_data.m_msg)
													{
														while((j<g_data.m_nTickerMessages)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
														{
															if(g_data.m_ticker[j])
															{
																if(!(g_data.m_ticker[j]->m_nStatus&WHEEL_MSG_STATUS_TALLY))
																{
																	g_data.DeleteTickerMessage(j);
																}
																else
																{
																	g_data.m_ticker[j]->m_nStatus &= ~WHEEL_MSG_STATUS_TALLY;
																	j++;
																}
															}
															else
															{
																j++;
															}														
														}
													}
													LeaveCriticalSection(&g_data.m_critTicker);
												}
											}

											// sorting the data on every turn.  I guess that is OK even if upcounting. - nah, lets let the ticker ones finish upcounting first
											//sort the data
											if((n>0)&&(g_data.m_ticker)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread)&&(!g_data.m_bMovedItem)&&(!g_data.m_bTickerMessagesUpcounting))
											{
												EnterCriticalSection(&g_data.m_critTicker);

											// after things obtained, do a cocktail sort on index
//AfxMessageBox("QQ");
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "performing cocktail sort on index last = %d", g_data.m_nTickerMessages-1); //(Dispatch message)
if(0)//g_ptabmsgr)
{
	n=0; 
	while(n<g_data.m_nTickerMessages)
	{
		g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "   PRE ticker stack [%s: %.4f]", g_data.m_ticker[n]->m_szMessage, g_data.m_ticker[n]->m_dblIndex);
		n++;
	}

}

												int last = g_data.m_nTickerMessages-1;
												int first = 0;
												int ix=0;
												BOOL bDone = FALSE; 
												CWheelMessage* pmsg=NULL;
												while((!bDone)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
												{
													bDone = TRUE; 
													for(ix = first; ix < last; ix++)
													{
														if((g_data.m_ticker[ix])&&(g_data.m_ticker[ix+1]))
														{
															if(g_data.m_ticker[ix]->m_dblIndex > g_data.m_ticker[ix+1]->m_dblIndex)
															{
																pmsg = g_data.m_ticker[ix]; 
																g_data.m_ticker[ix] = g_data.m_ticker[ix+1]; 
																g_data.m_ticker[ix+1]=pmsg;
																bDone = FALSE;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "performed swap on index from top (%d) [%s]v[%s]",ix, g_data.m_ticker[ix+1]->m_szMessage,g_data.m_ticker[ix]->m_szMessage); //(Dispatch message)
															}
														}
													}
													last--; 
													for(ix = last; ix > first; ix--)
													{
														if((g_data.m_ticker[ix])&&(g_data.m_ticker[ix-1]))
														{
															if(g_data.m_ticker[ix]->m_dblIndex < g_data.m_ticker[ix-1]->m_dblIndex)
															{
																pmsg = g_data.m_ticker[ix]; 
																g_data.m_ticker[ix] = g_data.m_ticker[ix-1]; 
																g_data.m_ticker[ix-1]=pmsg;
																bDone = FALSE;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "performed swap on index from bottom (%d) [%s]^[%s]",ix, g_data.m_ticker[ix-1]->m_szMessage,g_data.m_ticker[ix]->m_szMessage); //(Dispatch message)
															}
														}
													}
													first++;  
												}
if(0)//g_ptabmsgr)
{
	n=0; 
	while(n<g_data.m_nTickerMessages)
	{
		g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "   ticker stack [%s: %.4f]", g_data.m_ticker[n]->m_szMessage, g_data.m_ticker[n]->m_dblIndex);
		n++;
	}

}


												LeaveCriticalSection(&g_data.m_critTicker);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "performed cocktail sort on index"); //(Dispatch message)
											}
//AfxMessageBox("QQ2");

	if(g_dlldata.thread[WHEEL_TICKER]->nThreadState&WHEEL_TICKER_ERROR)
	{
		g_pdlg->m_fsFontTicker.crText= RGB(0,0,0);
		g_pdlg->m_fsFontTicker.crBG= RGB(255,0,0);
		g_pdlg->m_editexTicker.SetFont( g_pdlg->m_fsFontTicker, 0,-1);
		if(strlen(g_dlldata.thread[WHEEL_TICKER]->pszThreadStateText)>0)
		{
			g_pdlg->m_editexTicker.GetWnd()->SetWindowText( "Ticker engine error");
		}
		else
		{
			g_pdlg->m_editexTicker.GetWnd()->SetWindowText(g_dlldata.thread[WHEEL_TICKER]->pszThreadStateText);
		}
	}
	else
	{
		g_pdlg->m_fsFontTicker.crBG= RGB(0,0,0);
		g_pdlg->m_fsFontTicker.crText= RGB(0,255,0);
		g_pdlg->m_editexTicker.SetFont( g_pdlg->m_fsFontTicker, 0,-1);
		CString szText; szText.Format("Ticker %s", ((g_dlldata.thread[WHEEL_TICKER]->nThreadState&WHEEL_DATA_STARTED)?"running":"stopped"));
		g_pdlg->m_editexTicker.GetWnd()->SetWindowText( szText);
	}




											//set equal on success
											if(!g_data.m_bMovedItem)
											{
												if(!g_data.m_bTickerMessagesUpcounting)
												{
if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_FEED)) 
{
	CString szSource; szSource.Format("%s_Ticker", g_settings.m_pszInternalAppName)
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "finished upcounting, setting last update to %.3f", g_dlldata.thread[WHEEL_TICKER]->m_dblLastUpdate);// Sleep(100); //(Dispatch message)
}
													g_pdlg->m_dblLastTickerUpdate = g_dlldata.thread[WHEEL_TICKER]->m_dblLastUpdate;
												}
												else
												{
													//g_pdlg->m_dblLastTickerUpdate = (double)nMaxTimestamp; // causes next batch to go.\\
// this did not work because timestamps are not in order in feed
if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_FEED))
{ 
	CString szSource; szSource.Format("%s_Ticker", g_settings.m_pszInternalAppName)
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "still upcounting, processed %d messages so far", g_data.m_nTickerMessagesUpcounting);// Sleep(100); //(Dispatch message)
}
												}
											}

										}
										if(pdata->m_pucData)
										{
											free(pdata->m_pucData);
											pdata->m_ulDataLen=0;
											pdata->m_pucData = NULL;
										}


									}
									else
									{
						//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
	//g_data.m_bTransact = false;
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "left Server_Ticker_Get 1");// Sleep(100); //(Dispatch message)
//AfxMessageBox("Could not get ticker info.");
	g_pdlg->m_fsFontTicker.crBG= RGB(0,0,0);
	g_pdlg->m_fsFontTicker.crText= RGB(255,0,0);
	g_pdlg->m_editexTicker.SetFont( g_pdlg->m_fsFontTicker, 0,-1);
//	CString szError; szError.Format("Comm error %d: %s", nRV, errorstring);
	CString szError; szError.Format("Comm error %d", nRV);
	g_pdlg->m_editexTicker.GetWnd()->SetWindowText(szError);
								_snprintf(pData->pszThreadStateText, 256, "Error getting ticker info");
								g_data.m_socket=NULL; // rests conn

									}
}
								}
								else
								{
									pdata = new CNetData;
								}
							}

#endif //#ifndef REMOVE_CLIENTSERVER

//g_data.m_bTransact = false;

						}						

#ifndef REMOVE_CLIENTSERVER

						if((g_data.m_bDialogStarted)&&(g_pdlg)&&(!g_settings.m_bIsServer)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
						{

							// fill the list box.

							if(((g_data.m_dblLastTickerUpdate!=g_pdlg->m_dblLastTickerUpdate)||(g_data.m_bTickerMessagesUpcounting))&&(!g_data.m_bMovedItem))
							{
								if(!g_data.m_bTickerMessagesUpcounting) g_data.m_dblLastTickerUpdate=g_pdlg->m_dblLastTickerUpdate;
								
	EnterCriticalSection(&g_pdlg->m_critTicker);
	EnterCriticalSection(&g_pdlg->m_critDefault);
								int nRows = g_pdlg->m_lceTicker.GetItemCount();
								int nDefaultRows = g_pdlg->m_lceDefault.GetItemCount();
								int nTickerItem = g_pdlg->m_lceTicker.GetNextItem( -1,  LVNI_SELECTED );
								int nDefaultItem = g_pdlg->m_lceDefault.GetNextItem( -1,  LVNI_SELECTED );

//AfxMessageBox("X");
								int i=0;
								int t=0;
								int d=0;

								if(g_data.m_ticker)
								{
									while((i<g_data.m_nTickerMessages)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
									{
										CString szText = "";
										CString szMessage = "";
										BOOL bChange;

										if(g_data.m_ticker[i])
										{
											bChange = FALSE;
											if(g_data.m_ticker[i]->m_nID>=0)//positive, do real ticker msg
											{

												if(g_settings.m_bTickerPlayOnce)
												{
													szMessage = g_data.m_ticker[i]->m_szMessage;
												}
												else
												{
													szMessage.Format("[%d] %s", g_data.m_ticker[i]->m_nPlayed, g_data.m_ticker[i]->m_szMessage);
												}


												
												if(t<nRows)
												{
													szText = g_pdlg->m_lceTicker.GetItemText(t,0);
													if(szText.Compare(szMessage))
													{
														g_pdlg->m_lceTicker.SetItemText(t, 0, szMessage); 
														bChange = TRUE;
													}
												}
												else
												{
													g_pdlg->m_lceTicker.InsertItem( t, szMessage );
													g_pdlg->m_lceTicker.SetItemData(t, -1, 0);

													bChange = TRUE;
												}
												int w=g_pdlg->m_lceTicker.GetStringWidth(szMessage);
												if(g_pdlg->m_lceTicker.GetColumnWidth(0)<w)		g_pdlg->m_lceTicker.SetColumnWidth( 0, w);

												g_pdlg->m_lceTicker.SetItemDataPtr(t, g_data.m_ticker[i], 0);


												w = g_pdlg->m_lceTicker.GetItemData(t, 0);
												g_pdlg->m_lceTicker.SetItemData(t, g_data.m_ticker[i]->m_nStatus, 0);

												// set icon and color.

												if(w!=g_data.m_ticker[i]->m_nStatus)
												{
													bChange = TRUE;
												

													g_pdlg->m_lceTicker.SetItemBitmap(t, 0, g_pdlg->m_lceTicker.GetImage(g_pdlg->ReturnTickerBitmapIndex(g_data.m_ticker[i]->m_nStatus, (nTickerItem==t))));

													COLORREF cr;

													if(g_data.m_ticker[i]->m_nType&WHEEL_MSG_TYPE_DISABLED)
													{
														cr = WHEEL_COLOR_DISABLED;
													}
													else
													{

														if(g_data.m_ticker[i]->m_nStatus&WHEEL_MSG_STATUS_ERROR)
														{
															cr = WHEEL_COLOR_ERROR;
	//														g_pdlg->m_lceTicker.SetItemData(t,(DWORD)(WHEEL_COLOR_ERROR),3);
														}
														else
														if(g_data.m_ticker[i]->m_nStatus&WHEEL_MSG_STATUS_PLAYING)
														{
															cr = WHEEL_COLOR_PLAYING;
	//														g_pdlg->m_lceTicker.SetItemData(t,(DWORD)(WHEEL_COLOR_PLAYING),3);
														}
														else
														if(g_data.m_ticker[i]->m_nStatus&WHEEL_MSG_STATUS_COMMIT)
														{
															cr = WHEEL_COLOR_COMMIT;
	//														g_pdlg->m_lceTicker.SetItemData(t,(DWORD)(WHEEL_COLOR_COMMIT),3);
														}
														else
														if(g_data.m_ticker[i]->m_nStatus&WHEEL_MSG_STATUS_PLAYED)
														{
															cr = WHEEL_COLOR_PLAYED;
	//														g_pdlg->m_lceTicker.SetItemData(t,(DWORD)(WHEEL_COLOR_PLAYED),3);
														}
														else
														{
															cr = GetSysColor(COLOR_WINDOW);
	//														g_pdlg->m_lceTicker.SetItemData(t,(DWORD)(GetSysColor(COLOR_WINDOW)),3);
														}
													}
													g_pdlg->m_lceTicker.SetItemData(t,(DWORD)(cr),3);
 //													g_pdlg->m_lceTicker.SetItemColor(t, 0, LCEX_BGCLR, (COLORREF)g_pdlg->m_lceTicker.GetItemData(t,3), TRUE);// make sure its blank.
 													if(!(BOOL)g_pdlg->m_lceTicker.GetItemData(t,13))
													{
 														g_pdlg->m_lceTicker.SetItemColor(t, 0, LCEX_BGCLR, cr, TRUE);// make sure its blank.
													}
												}

												if(bChange)
												{
													if(t>=nRows)
														g_pdlg->m_lceTicker.Update(t);
													else
														g_pdlg->m_lceTicker.RedrawItems( t,t );
												}
												t++;



											}
											else //negative, do default ticker msg
											{
												szMessage.Format("[%d] %s", g_data.m_ticker[i]->m_nPlayed, g_data.m_ticker[i]->m_szMessage);

												if(d<nDefaultRows)
												{
													szText = g_pdlg->m_lceDefault.GetItemText(d,0);
													if(szText.Compare(szMessage))
													{
														g_pdlg->m_lceDefault.SetItemText(d, 0, szMessage); 
													}
												}
												else
												{
													g_pdlg->m_lceDefault.InsertItem( d, szMessage );
												}
												int w=g_pdlg->m_lceDefault.GetStringWidth(szMessage);
												if(g_pdlg->m_lceDefault.GetColumnWidth(0)<w)		g_pdlg->m_lceDefault.SetColumnWidth( 0, w);


												g_pdlg->m_lceDefault.SetItemDataPtr(d, g_data.m_ticker[i], 0);

												// set icon and color.
												g_pdlg->m_lceDefault.SetItemBitmap(d, 0, g_pdlg->m_lceDefault.GetImage(g_pdlg->ReturnDefaultBitmapIndex(g_data.m_ticker[i]->m_nStatus, (nDefaultItem==d))));

												if(g_data.m_ticker[i]->m_nType&WHEEL_MSG_TYPE_DISABLED)
												{
													g_pdlg->m_lceDefault.SetItemData(d,(DWORD)(WHEEL_COLOR_DISABLED),3);
												}
												else
												{


													if(g_data.m_ticker[i]->m_nStatus&WHEEL_MSG_STATUS_ERROR)
													{
														g_pdlg->m_lceDefault.SetItemData(d,(DWORD)(WHEEL_COLOR_ERROR),3);
													}
													else
													if(g_data.m_ticker[i]->m_nStatus&WHEEL_MSG_STATUS_PLAYING)
													{
														g_pdlg->m_lceDefault.SetItemData(d,(DWORD)(WHEEL_COLOR_PLAYING),3);
													}
													else
													if(g_data.m_ticker[i]->m_nStatus&WHEEL_MSG_STATUS_COMMIT)
													{
														g_pdlg->m_lceDefault.SetItemData(d,(DWORD)(WHEEL_COLOR_COMMIT),3);
													}
													else
													if(g_data.m_ticker[i]->m_nStatus&WHEEL_MSG_STATUS_PLAYED)
													{
														g_pdlg->m_lceDefault.SetItemData(d,(DWORD)(WHEEL_COLOR_PLAYED),3);
													}
													else
													{
														g_pdlg->m_lceDefault.SetItemData(d,(DWORD)(GetSysColor(COLOR_WINDOW)),3);
													}
												}

 												if(!(BOOL)g_pdlg->m_lceDefault.GetItemData(d,13))
												{
 													g_pdlg->m_lceDefault.SetItemColor(d, 0, LCEX_BGCLR, (COLORREF)g_pdlg->m_lceDefault.GetItemData(d,3),TRUE);// make sure its blank.
												}
												g_pdlg->m_lceDefault.RedrawItems( d,d );

												d++;
											}
										}
										i++;
									}
								}

								if(!g_dlldata.thread[WHEEL_UI]->bKillThread)
								{
									while((t+1<nRows)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
									{
										g_pdlg->m_lceTicker.DeleteItem(nRows-1);
										nRows--;
									}
								}
								if(!g_dlldata.thread[WHEEL_UI]->bKillThread)
								{
									while((d+1<nDefaultRows)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
									{
										g_pdlg->m_lceDefault.DeleteItem(nDefaultRows-1);
										nDefaultRows--;
									}
								}

								
								
								if(!g_dlldata.thread[WHEEL_UI]->bKillThread)
								{
									CString szText;
									if(t<nRows)
									{
										szText = g_pdlg->m_lceTicker.GetItemText(t,0);
										if(szText.Compare(""))
										{
											g_pdlg->m_lceTicker.SetItemText(t, 0, ""); 
										}
									}
									else
									{
										g_pdlg->m_lceTicker.InsertItem( t, "" );
										g_pdlg->m_lceTicker.SetItemData(t,0,13);
	//									AfxMessageBox("ding");
									}
//								g_pdlg->m_lceTicker.InsertItem(t,""); // add a blank one at end
 									if(!(BOOL)g_pdlg->m_lceTicker.GetItemData(t,13))
									{
										g_pdlg->m_lceTicker.SetItemColor(t, 0, LCEX_BGCLR, GetSysColor(COLOR_WINDOW),TRUE);// make sure its blank.
										g_pdlg->m_lceTicker.SetItemBitmap(t, 0, g_pdlg->m_lceTicker.GetImage(0)); 
									}
									g_pdlg->m_lceTicker.SetItemData(t, GetSysColor(COLOR_WINDOW),3);
									g_pdlg->m_lceTicker.SetItemData(t, WHEEL_MSG_STATUS_NONE, 0);


									g_pdlg->m_lceTicker.RedrawItems( t,t );

									if(d<nDefaultRows)
									{
										szText = g_pdlg->m_lceDefault.GetItemText(d,0);
										if(szText.Compare(""))
										{
											g_pdlg->m_lceDefault.SetItemText(d, 0, ""); 
										}
									}
									else
									{
										g_pdlg->m_lceDefault.InsertItem( d, "" );
										g_pdlg->m_lceDefault.SetItemData(d,0,13);
									}

								
//								g_pdlg->m_lceDefault.InsertItem(d,""); // add a blank one at end
 									if(!(BOOL)g_pdlg->m_lceDefault.GetItemData(d,13))
									{
										g_pdlg->m_lceDefault.SetItemColor(d, 0, LCEX_BGCLR, GetSysColor(COLOR_WINDOW),TRUE);// make sure its blank.
										g_pdlg->m_lceDefault.SetItemBitmap(d, 0, g_pdlg->m_lceDefault.GetImage(0)); 
									}

									g_pdlg->m_lceDefault.SetItemData(d, GetSysColor(COLOR_WINDOW),3);
									g_pdlg->m_lceDefault.SetItemData(d, WHEEL_MSG_STATUS_NONE, 0);
									g_pdlg->m_lceDefault.RedrawItems( d,d );


									LVCOLUMN lvColumn;
									lvColumn.mask = LVCF_TEXT;
									char count[128];
									sprintf(count, "Ticker Playout (%d)", t);
									lvColumn.pszText  = count;
									g_pdlg->m_lceTicker.SetColumn(0, &lvColumn);
									sprintf(count, "Default Messages (%d)", d);
									lvColumn.pszText  = count;
									g_pdlg->m_lceDefault.SetColumn(0, &lvColumn);



									g_pdlg->m_lceTicker.UpdateWindow();
									g_pdlg->m_lceDefault.UpdateWindow();
								}
	LeaveCriticalSection(&g_pdlg->m_critDefault);
	LeaveCriticalSection(&g_pdlg->m_critTicker);

							}	








							// deal with the list box...
							BOOL bFiltrationChange = FALSE;

							if( 
								  (!g_dlldata.thread[WHEEL_UI]->bKillThread)
								&&(
										(g_data.m_bFilterAge!=g_pdlg->m_bFilterAge)
									||(g_data.m_bFilterBad!=g_pdlg->m_bFilterBad)
									||(g_data.m_bFilterCommitted!=g_pdlg->m_bFilterCommitted)
									||(g_data.m_bFilter!=g_pdlg->m_bFilter)
									||(g_data.m_bFilterGood!=g_pdlg->m_bFilterGood)
									||(g_data.m_bFilterGreat!=g_pdlg->m_bFilterGreat)
									||(g_data.m_bFilterLength!=g_pdlg->m_bFilterLength)
									||(g_data.m_bFilterPlayed!=g_pdlg->m_bFilterPlayed)
									||(g_data.m_bFilterUnmarked!=g_pdlg->m_bFilterUnmarked)
									||(g_data.m_nMinutesComparator!=g_pdlg->m_nMinutesComparator)
									||(g_data.m_nLengthComparator!=g_pdlg->m_nLengthComparator)
									||(g_data.m_nTimesPlayedComparator!=g_pdlg->m_nTimesPlayedComparator)
									||(g_data.m_nMinutesValue!=g_pdlg->m_nMinutesValue)
									||(g_data.m_nLengthValue!=g_pdlg->m_nLengthValue)
									||(g_data.m_nTimesPlayedValue!=g_pdlg->m_nTimesPlayedValue)
									)
								)
							{
								bFiltrationChange = TRUE;
							}
							if(
									(g_data.m_dblLastDataUpdate!=g_pdlg->m_dblLastDataUpdate)
								||(g_data.m_bForceRefresh)
								||(bFiltrationChange)
								||(g_data.m_bSortTimestampsAsc!=g_pdlg->m_bSortTimestampsAsc)
			//					||(g_data.m_bMessagesUpcounting) // adds new ones to list box even while tallying
								)
							{
								//if( !g_data.m_bMessagesUpcounting )   // ok dont add these, it causes intermediate list clears  I think.
								g_data.m_dblLastDataUpdate=g_pdlg->m_dblLastDataUpdate;
								g_data.m_bFilterAge=g_pdlg->m_bFilterAge;
								g_data.m_bFilterBad=g_pdlg->m_bFilterBad;
								g_data.m_bFilterCommitted=g_pdlg->m_bFilterCommitted;
								g_data.m_bFilter=g_pdlg->m_bFilter;
								g_data.m_bFilterGood=g_pdlg->m_bFilterGood;
								g_data.m_bFilterGreat=g_pdlg->m_bFilterGreat;
								g_data.m_bFilterLength=g_pdlg->m_bFilterLength;
								g_data.m_bFilterPlayed=g_pdlg->m_bFilterPlayed;
								g_data.m_bFilterUnmarked=g_pdlg->m_bFilterUnmarked;
								g_data.m_nMinutesComparator=g_pdlg->m_nMinutesComparator;
								g_data.m_nLengthComparator=g_pdlg->m_nLengthComparator;
								g_data.m_nTimesPlayedComparator=g_pdlg->m_nTimesPlayedComparator;
								g_data.m_nMinutesValue=g_pdlg->m_nMinutesValue;
								g_data.m_nLengthValue=g_pdlg->m_nLengthValue;
								g_data.m_nTimesPlayedValue=g_pdlg->m_nTimesPlayedValue;
								

								g_data.m_bForceRefresh = FALSE;
	EnterCriticalSection(&g_pdlg->m_critData);
								// fill the list box appropriately

								_ftime(&g_dlldata.thread[WHEEL_UI]->timebTick);

								int tzd=(g_dlldata.thread[WHEEL_UI]->timebTick.timezone*60 - (g_dlldata.thread[WHEEL_UI]->timebTick.dstflag?3600:0));

								int tz;
								int n=0;
								int i=0;
								BOOL bDefeat = TRUE;
								EnterCriticalSection(&g_data.m_critMsg);







								if((g_data.m_bSortTimestampsAsc!=g_pdlg->m_bSortTimestampsAsc)&&(g_data.m_msg)&&(g_data.m_nMessages>1)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
								{
								// after things obtained, do a fast reverse sort

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "reverse sort on timestamp"); //(Dispatch message)

									int last = g_data.m_nMessages-1;
									int ix=0;
									BOOL bDone = FALSE; 
									CWheelMessage* pmsg=NULL;
									if(g_pdlg->m_bSortTimestampsAsc)
									{
										while((bDone == FALSE)&&(ix<last)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
										{
											if((g_data.m_msg[ix])&&(g_data.m_msg[last]))
											{
												if(g_data.m_msg[ix]->m_nTimestamp < g_data.m_msg[last]->m_nTimestamp) 
												{
													bDone = TRUE;
												}
												else
												{
													pmsg = g_data.m_msg[ix]; 
													g_data.m_msg[ix] = g_data.m_msg[last]; 
													g_data.m_msg[last]=pmsg;
												}
											}
											
											ix++;
											last--;
										}
									}
									else
									{
										while((bDone == FALSE)&&(ix<last)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
										{
											if((g_data.m_msg[ix])&&(g_data.m_msg[last]))
											{
												if(g_data.m_msg[ix]->m_nTimestamp > g_data.m_msg[last]->m_nTimestamp) 
												{
													bDone = TRUE;
												}
												else
												{
													pmsg = g_data.m_msg[ix]; 
													g_data.m_msg[ix] = g_data.m_msg[last]; 
													g_data.m_msg[last]=pmsg;
												}
											}
											ix++;
											last--;
										}
									}
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "synchronizer", "performed reverse sort on timestamp"); //(Dispatch message)
								}
								g_data.m_bSortTimestampsAsc=g_pdlg->m_bSortTimestampsAsc;









								int nRows = g_pdlg->m_lceData.GetItemCount();
								int nItem = g_pdlg->m_lceData.GetNextItem( -1,  LVNI_SELECTED );
								int nSelID = 0;
								CWheelMessage* pmsg = NULL;
								if(nItem>=0)
								{
									pmsg = (CWheelMessage*) g_pdlg->m_lceData.GetItemDataPtr(nItem, 0);
									if(pmsg)
									{
										nSelID = pmsg->m_nID;
									}
								}




								int nFiltered=0; int nHeight;
								CRect rc;
								if(g_pdlg->m_bFilter)
								{
									g_pdlg->m_lceData.GetClientRect(&rc);
									nHeight= (rc.Height()/16)+1;
								}

								if(g_data.m_msg)
								{
									CString szText = "";
									CString szMessage = "";
									BOOL bChange;

									while((i<g_data.m_nMessages)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
									{
										if(g_data.m_msg[i])
										{
	//AfxMessageBox("f00");
											if(!g_pdlg->IsFilteredOut(g_data.m_msg[i]))
											{
	//AfxMessageBox("f00y");
												tz = g_data.m_msg[i]->m_nTimestamp-tzd;
	//#define DEBUG_CHECKING
	#undef DEBUG_CHECKING

	#ifdef DEBUG_CHECKING

												if(g_data.m_msg[i]->m_szRevisedMessage.GetLength()>0)
												{
													szMessage.Format("(%d=%02d:%02d:%02d)  %s",
														g_data.m_msg[i]->m_nTimestamp,
														(tz%86400)/3600,
														((tz/60)%60),
														((tz%60)%60),
														g_data.m_msg[i]->m_szRevisedMessage
														);
												}
												else
												{
													szMessage.Format("(%d=%02d:%02d:%02d)  %s", 
														g_data.m_msg[i]->m_nTimestamp,
														(tz%86400)/3600,
														((tz/60)%60),
														((tz%60)%60),
														g_data.m_msg[i]->m_szMessage
														);
												}

	#else

												if(g_settings.m_bTickerPlayOnce)
												{
													if(g_data.m_msg[i]->m_szRevisedMessage.GetLength()>0)
													{
														szMessage.Format("(%02d:%02d:%02d)  %s", 
															(tz%86400)/3600,
															((tz/60)%60),
															((tz%60)%60),
															g_data.m_msg[i]->m_szRevisedMessage
															);
													}
													else
													{
														szMessage.Format("(%02d:%02d:%02d)  %s", 
															(tz%86400)/3600,
															((tz/60)%60),
															((tz%60)%60),
															g_data.m_msg[i]->m_szMessage
															);
													}
												}
												else
												{
													if(g_data.m_msg[i]->m_szRevisedMessage.GetLength()>0)
													{
														szMessage.Format("[%d] (%02d:%02d:%02d)  %s", 
															g_data.m_msg[i]->m_nPlayed,
															(tz%86400)/3600,
															((tz/60)%60),
															((tz%60)%60),
															g_data.m_msg[i]->m_szRevisedMessage
															);
													}
													else
													{
														szMessage.Format("[%d] (%02d:%02d:%02d)  %s", 
															g_data.m_msg[i]->m_nPlayed,
															(tz%86400)/3600,
															((tz/60)%60),
															((tz%60)%60),
															g_data.m_msg[i]->m_szMessage
															);
													}
												}

	#endif
												bChange = FALSE;
												if(n<nRows)
												{
													szText = g_pdlg->m_lceData.GetItemText(n,0);
													if(szText.Compare(szMessage))
													{
														g_pdlg->m_lceData.SetItemText(n, 0, szMessage);
														bChange = TRUE;
													}
												}
												else
												{
													g_pdlg->m_lceData.InsertItem( n, szMessage );
													g_pdlg->m_lceData.SetItemData(n, -1, 0);
													bChange = TRUE;
												}

												int w=g_pdlg->m_lceData.GetStringWidth(szMessage);
												if(g_pdlg->m_lceData.GetColumnWidth(0)<w)		g_pdlg->m_lceData.SetColumnWidth( 0, w);

												g_pdlg->m_lceData.SetItemDataPtr(n, g_data.m_msg[i], 0);

												w = g_pdlg->m_lceData.GetItemData(n, 0);
												g_pdlg->m_lceData.SetItemData(n, g_data.m_msg[i]->m_nStatus, 0);

												// set icon and color.
												if((nItem==n)&&(g_data.m_msg[i])&&(g_data.m_nCommitID!=g_data.m_msg[i]->m_nID))
												{
													//lets unselect the thing
													g_pdlg->m_lceData.SetItem(n, 0, LVIF_STATE, NULL, 0, ~(LVIS_SELECTED|LVIS_FOCUSED), LVIS_SELECTED|LVIS_FOCUSED, 0);
													nItem=-1;
													bDefeat=FALSE;
												}
												if((nItem!=n)&&(g_data.m_msg[i])&&(nSelID==g_data.m_msg[i]->m_nID))
												{
													//lets select the thing
													g_pdlg->m_lceData.SetItem(n, 0, LVIF_STATE, NULL, 0, (LVIS_SELECTED|LVIS_FOCUSED), LVIS_SELECTED|LVIS_FOCUSED, 0);
													nItem=-1;
													bDefeat=FALSE;
												}



												if(w!=g_data.m_msg[i]->m_nStatus)
												{
													bChange = TRUE;
												
													g_pdlg->m_lceData.SetItemBitmap(n, 0, g_pdlg->m_lceData.GetImage(g_pdlg->ReturnDataBitmapIndex(g_data.m_msg[i]->m_nStatus, (nItem==n))));


													if(g_data.m_msg[i]->m_nStatus&WHEEL_MSG_STATUS_ERROR)
													{
		//												g_pdlg->m_lceData.SetItemData(n,(DWORD)(WHEEL_COLOR_ERROR),3);
 														g_pdlg->m_lceData.SetItemColor(n, 0, LCEX_BGCLR, WHEEL_COLOR_ERROR, TRUE);// make sure its blank.
													}
	/*
	// playing and cued status not transferred to feed side
													else
													if(g_data.m_msg[i]->m_nStatus&WHEEL_MSG_STATUS_PLAYING)
													{
														g_pdlg->m_lceData.SetItemData(n,(DWORD)(WHEEL_COLOR_PLAYING),3);
													}
													else
													if(g_data.m_msg[i]->m_nStatus&WHEEL_MSG_STATUS_COMMIT)
													{
														g_pdlg->m_lceData.SetItemData(n,(DWORD)(WHEEL_COLOR_COMMIT),3);
													}
													else
	*/
													if(g_data.m_msg[i]->m_nStatus&WHEEL_MSG_STATUS_PLAYED)
													{
		//												g_pdlg->m_lceData.SetItemData(n,(DWORD)(WHEEL_COLOR_PLAYED),3);
 														g_pdlg->m_lceData.SetItemColor(n, 0, LCEX_BGCLR, WHEEL_COLOR_PLAYED, TRUE);// make sure its blank.
													}
													else
													{
		//												g_pdlg->m_lceData.SetItemData(n,(DWORD)(GetSysColor(COLOR_WINDOW)),3);
 														g_pdlg->m_lceData.SetItemColor(n, 0, LCEX_BGCLR, GetSysColor(COLOR_WINDOW), TRUE);// make sure its blank.
													}

 		//											g_pdlg->m_lceData.SetItemColor(n, 0, LCEX_BGCLR, (COLORREF)g_pdlg->m_lceData.GetItemData(n,3), TRUE);// make sure its blank.

												}

												if(bChange)
												{
													if(n>=nRows)
														g_pdlg->m_lceData.Update(n);
													else
														g_pdlg->m_lceData.RedrawItems( n,n );
												}

												n++;
											}

	// this didnt work.... sadly.
											else
											{
												nFiltered++;
												// if more have been filtered than one list box's worth, and there are more than two list boxes worth left to filter,
												// delete all and start again, will prob be faster for large lists.
												if((bFiltrationChange)&&(nRows>0)&&(nFiltered>nHeight)&&((g_data.m_nMessages-i)>(nHeight<<1)))
												{
													g_pdlg->m_lceData.DeleteAllItems();
													i=g_data.m_nMessages;
													n=nRows;
													g_data.m_bForceRefresh=TRUE;
												}
											}

	//AfxMessageBox("f00x");
										}


										MSG msg;
										while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
											AfxGetApp()->PumpMessage();

										if(
												(g_data.m_dblLastDataUpdate!=g_pdlg->m_dblLastDataUpdate)
											||(g_data.m_bForceRefresh)
											||(g_data.m_bFilterAge!=g_pdlg->m_bFilterAge)
											||(g_data.m_bFilterBad!=g_pdlg->m_bFilterBad)
											||(g_data.m_bFilterCommitted!=g_pdlg->m_bFilterCommitted)
											||(g_data.m_bFilter!=g_pdlg->m_bFilter)
											||(g_data.m_bFilterGood!=g_pdlg->m_bFilterGood)
											||(g_data.m_bFilterGreat!=g_pdlg->m_bFilterGreat)
											||(g_data.m_bFilterLength!=g_pdlg->m_bFilterLength)
											||(g_data.m_bFilterPlayed!=g_pdlg->m_bFilterPlayed)
											||(g_data.m_bFilterUnmarked!=g_pdlg->m_bFilterUnmarked)
											||(g_data.m_nMinutesComparator!=g_pdlg->m_nMinutesComparator)
											||(g_data.m_nLengthComparator!=g_pdlg->m_nLengthComparator)
											||(g_data.m_nTimesPlayedComparator!=g_pdlg->m_nTimesPlayedComparator)
											||(g_data.m_nMinutesValue!=g_pdlg->m_nMinutesValue)
											||(g_data.m_nLengthValue!=g_pdlg->m_nLengthValue)
											||(g_data.m_nTimesPlayedValue!=g_pdlg->m_nTimesPlayedValue)
											)
										{
											i=g_data.m_nMessages;
											n=nRows;
										}

										i++;
									}
								}
								LeaveCriticalSection(&g_data.m_critMsg);

								if(((nRows-n)>n)&&(nRows>0)&((nRows-n)>(nHeight<<1)))
								{
									g_pdlg->m_lceData.DeleteAllItems();
									g_data.m_bForceRefresh=TRUE;
								}
								else
								{
									if(!g_dlldata.thread[WHEEL_UI]->bKillThread)
									{
										while((n<nRows)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
										{
											g_pdlg->m_lceData.DeleteItem(n);
											nRows--;
										}
									}
								}
								g_pdlg->ShowDataSelection(bDefeat);  // calls update window
	LeaveCriticalSection(&g_pdlg->m_critData);

							}
	



							_ftime(&g_dlldata.thread[WHEEL_UI]->timebTick);
							if( (g_data.m_socket!=NULL)
								&&(g_data.m_bQueued)
								&&(!g_dlldata.thread[WHEEL_UI]->bKillThread)
								&&(g_dlldata.thread[WHEEL_UI]->timebTick.time>g_data.m_timeMarks.time)
								)
							{
								// send queued items

								int n=0;
								int i=0;

								CString szReturn="0";
								EnterCriticalSection(&g_data.m_critMsg);
								if(g_data.m_msg)
								{
									CString szData="";
									while((n<g_data.m_nMessages)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
									{
										if((g_data.m_msg[n])&&(g_data.m_msg[n]->m_nStatus&WHEEL_MSG_STATUS_QUEUED))
										{
											szData+= (char)29;
											szData+=g_data.MessageToString(g_data.m_msg[n]);

											i++;
										}
										n++;
									}
									szReturn.Format("%d%s", i, szData);
								}

								if(i>0)
								{
	//								AfxMessageBox(szReturn);

									if(pdata)
									{

										pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA|NET_TYPE_KEEPOPEN;      // defined type - indicates which protocol to use, structure of data

										pdata->m_ucCmd = TABULATOR_CMD_PLUGINCALL;       // the command byte
										pdata->m_ucSubCmd = 0;       // the subcommand byte

										char* pch = (char*)malloc(strlen(g_settings.m_pszInternalAppName)+szReturn.GetLength()+32); 
										if(pch)
										{
											sprintf(pch, "%s|Server_Data_Revision|%s", g_settings.m_pszInternalAppName, szReturn);

											pdata->m_pucData =  (unsigned char*) pch;
											pdata->m_ulDataLen = strlen(pch);
										}

								//AfxMessageBox("sending");
								//AfxMessageBox( sentinel.m_data.m_pszCortexHost );
								//AfxMessageBox( (char*)pdata->m_pucData );
										//if(!//g_data.m_bTransact)
										{
//g_data.m_bTransact = true;
	EnterCriticalSection(&g_data.m_critTransact);
							//EnterCriticalSection(&g_data.m_critClientSocket);
										int nRV = NET_ERROR;
										if(!g_dlldata.thread[WHEEL_UI]->bKillThread)
											nRV = g_data.m_net.SendData(pdata, g_data.m_socket, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT);
										if(nRV>=NET_SUCCESS)
										{
											if(!g_dlldata.thread[WHEEL_UI]->bKillThread) g_data.m_net.SendData(NULL, g_data.m_socket, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT);
											//send ack
								//			AfxMessageBox("ack");
							//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
//g_data.m_bTransact = false;
										
											if(pdata->m_pucData)
											{
												free(pdata->m_pucData);
												pdata->m_pucData=NULL;
												pdata->m_ulDataLen=0;
											}

											n=0;
											if(g_data.m_msg)
											{
												while((n<g_data.m_nMessages)&&(!g_dlldata.thread[WHEEL_UI]->bKillThread))
												{
													if((g_data.m_msg[n])&&(g_data.m_msg[n]->m_nStatus&WHEEL_MSG_STATUS_QUEUED))
													{
														g_data.m_msg[n]->m_nStatus &= ~(WHEEL_MSG_STATUS_QUEUED|WHEEL_MSG_STATUS_REVISED);
													}
													n++;
												}
											}
											g_data.m_bQueued=false;
							
										}
										else
										{
							//LeaveCriticalSection(&g_data.m_critClientSocket);
	LeaveCriticalSection(&g_data.m_critTransact);
//g_data.m_bTransact = false;

			//AfxMessageBox("Could not get status.");
											Sleep(1000);
											pData->nThreadState=WHEEL_SYNCH_ERROR;
								_snprintf(pData->pszThreadStateText, 256, "Error sending to host");
								g_data.m_socket=NULL; // rests conn

										}
										}
									}
									else
									{
										pdata = new CNetData;
									}

								}
								else
								{
								}

								LeaveCriticalSection(&g_data.m_critMsg);


							}

						}

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "checking thread state %d ",g_dlldata.thread[WHEEL_TICKER]->nThreadState);// Sleep(100); //(Dispatch message)

#endif //#ifndef REMOVE_CLIENTSERVER

						if(!g_dlldata.thread[WHEEL_UI]->bKillThread)
						{
							if(!(g_dlldata.thread[WHEEL_DATA]->nThreadState&WHEEL_DATA_STARTED))
							{

								CString szText;
								g_pdlg->GetDlgItem(IDC_BUTTON_DATA)->GetWindowText(szText);
								if(szText.Compare("Start Data"))
									g_pdlg->GetDlgItem(IDC_BUTTON_DATA)->SetWindowText("Start Data");
							}
							else
							{
								CString szText;
								g_pdlg->GetDlgItem(IDC_BUTTON_DATA)->GetWindowText(szText);
								if(szText.Compare("Stop Data"))
									g_pdlg->GetDlgItem(IDC_BUTTON_DATA)->SetWindowText("Stop Data");
							}

							if(g_dlldata.thread[WHEEL_TICKER]->nThreadState&WHEEL_TICKER_ERROR)
							{
								CString szText;
								g_pdlg->GetDlgItem(IDC_BUTTON_TICKER)->GetWindowText(szText);
									if(szText.Compare("Ticker Error"))
								g_pdlg->GetDlgItem(IDC_BUTTON_TICKER)->SetWindowText("Ticker Error");
								g_pdlg->GetDlgItem(IDC_BUTTON_TICKER)->EnableWindow(FALSE);
							}
							else
							if(g_dlldata.thread[WHEEL_TICKER]->nThreadState&WHEEL_TICKER_STARTED)
							{
	//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "TICKEROUT");// Sleep(100); //(Dispatch message)

								CString szText;
								g_pdlg->GetDlgItem(IDC_BUTTON_TICKER)->GetWindowText(szText);
								if(szText.Compare("Ticker Out"))
									{
	//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "TICKEROUT set %s", szText);// Sleep(100); //(Dispatch message)
									g_pdlg->GetDlgItem(IDC_BUTTON_TICKER)->SetWindowText("Ticker Out");
								}
								g_pdlg->GetDlgItem(IDC_BUTTON_TICKER)->EnableWindow(TRUE);

							}
							else
	//						if((g_dlldata.thread[WHEEL_TICKER]->nThreadState&WHEEL_TICKER_STARTED)==WHEEL_TICKER_STOPPED)
							{
	//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "TICKERIN");// Sleep(100); //(Dispatch message)
								CString szText;
								g_pdlg->GetDlgItem(IDC_BUTTON_TICKER)->GetWindowText(szText);
									if(szText.Compare("Ticker In"))
									{
	//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "TICKERIN set %s", szText);// Sleep(100); //(Dispatch message)
								g_pdlg->GetDlgItem(IDC_BUTTON_TICKER)->SetWindowText("Ticker In");
									}
								g_pdlg->GetDlgItem(IDC_BUTTON_TICKER)->EnableWindow(TRUE);

							}

							if(g_dlldata.thread[WHEEL_TICKER]->nThreadState&WHEEL_TICKER_CYCLING)
							{
								BOOL bChecked = ((CButton*)g_pdlg->GetDlgItem(IDC_CHECK_CYCLE))->GetCheck();
								CString szText;
								g_pdlg->GetDlgItem(IDC_CHECK_CYCLE)->GetWindowText(szText);
								if(szText.Compare("Resume ticker playout"))
									g_pdlg->GetDlgItem(IDC_CHECK_CYCLE)->SetWindowText("Resume ticker playout");
								if(!bChecked)
									((CButton*)g_pdlg->GetDlgItem(IDC_CHECK_CYCLE))->SetCheck(TRUE);
							}
							else
							{
								BOOL bChecked = ((CButton*)g_pdlg->GetDlgItem(IDC_CHECK_CYCLE))->GetCheck();
								CString szText;
								g_pdlg->GetDlgItem(IDC_CHECK_CYCLE)->GetWindowText(szText);
								if(szText.Compare("Push to cycle defaults"))
									g_pdlg->GetDlgItem(IDC_CHECK_CYCLE)->SetWindowText("Push to cycle defaults");
								if(bChecked)
									((CButton*)g_pdlg->GetDlgItem(IDC_CHECK_CYCLE))->SetCheck(FALSE);
							}


							g_pdlg->GetDlgItem(IDC_BUTTON_DATA)->EnableWindow(TRUE);
						}
					}
				}
				else if(!g_dlldata.thread[WHEEL_UI]->bKillThread)
				{
					g_pdlg->GetDlgItem(IDC_BUTTON_DATA)->EnableWindow(FALSE);
					g_pdlg->GetDlgItem(IDC_BUTTON_TICKER)->EnableWindow(FALSE);

	g_pdlg->m_fsFontData.crBG= RGB(0,0,0);
	g_pdlg->m_fsFontData.crText= RGB(255,0,0);
	g_pdlg->m_editexData.SetFont( g_pdlg->m_fsFontData, 0,-1);
	g_pdlg->m_editexData.GetWnd()->SetWindowText("Comm error getting feed data");


	g_pdlg->m_fsFontTicker.crBG= RGB(0,0,0);
	g_pdlg->m_fsFontTicker.crText= RGB(255,0,0);
	g_pdlg->m_editexTicker.SetFont( g_pdlg->m_fsFontTicker, 0,-1);
	g_pdlg->m_editexTicker.GetWnd()->SetWindowText("Comm error getting ticker info");


				}


			}
			
			if(g_settings.m_bIsServer)
			{
#ifndef REMOVE_CLIENTSERVER
				if((g_dlldata.thread[WHEEL_DATA])&&(g_settings.m_bUseFeed))
				{
					if((!g_dlldata.thread[WHEEL_UI]->bKillThread)&&(!g_dlldata.thread[WHEEL_DATA]->bThreadStarted))
					{
						g_dlldata.thread[WHEEL_DATA]->bKillThread = false;
						if(_beginthread(DataDownloadThread, 0, (void*)NULL)==-1)
						{
							//error.

							g_dlldata.thread[WHEEL_DATA]->nThreadState=WHEEL_DATA_ERROR;
							_snprintf(pData->pszThreadStateText, 256, "Error starting feed");

//							AfxMessageBox("Error beginning data download thread.");

							Sleep(1000);
							
						}
						else
						{
							while((!g_dlldata.thread[WHEEL_UI]->bKillThread)&&(!g_dlldata.thread[WHEEL_DATA]->bThreadStarted)) Sleep(50);
						}
					}
				}
#endif //#ifndef REMOVE_CLIENTSERVER

				if(g_dlldata.thread[WHEEL_TICKER])
				{
					if((!g_dlldata.thread[WHEEL_UI]->bKillThread)&&(!g_dlldata.thread[WHEEL_TICKER]->bThreadStarted))
					{
						g_dlldata.thread[WHEEL_TICKER]->bKillThread = false;
						if(_beginthread(TickerEngineThread, 0, (void*)NULL)==-1)
						{
							//error.
							g_dlldata.thread[WHEEL_TICKER]->nThreadState=WHEEL_TICKER_ERROR;
							_snprintf(g_dlldata.thread[WHEEL_TICKER]->pszThreadStateText, 256, "Error starting ticker engine");

//							AfxMessageBox("Error beginning ticker thread.");

							Sleep(1000);
							
						}
						else
						{
							while((!g_dlldata.thread[WHEEL_UI]->bKillThread)&&(!g_dlldata.thread[WHEEL_TICKER]->bThreadStarted)) Sleep(50);
						}
					}
				}

				// deal with events.

				if(g_data.m_pInEvents ==NULL) g_data.m_pInEvents = new CTabulatorEventArray;
				if(g_data.m_pOutEvents ==NULL) g_data.m_pOutEvents = new CTabulatorEventArray;
				if(g_data.m_pTextEvents ==NULL) g_data.m_pTextEvents = new CTabulatorEventArray;
				
				char szEventSource[MAX_PATH]; sprintf(szEventSource, "%s_Events", g_settings.m_pszInternalAppName);
				if(g_data.m_pEvents)
				{ 
					EnterCriticalSection(&(g_data.m_pEvents->m_critEvents));

					EnterCriticalSection(&g_data.m_critEventsSettings);

					int nChanges = 
						(
							(g_data.m_bInEventChanged?WHEEL_EVENT_IN:0)
						 |(g_data.m_bOutEventChanged?WHEEL_EVENT_OUT:0)
						 |(g_data.m_bTextEventChanged?WHEEL_EVENT_TEXT:0) 
						 |((g_data.m_pEvents->m_nLastEventMod!=g_data.m_nLastEventMod)?WHEEL_EVENT_MOD:0) 
						);

					LeaveCriticalSection(&g_data.m_critEventsSettings);


					if(/*(g_data.m_pEvents->m_nLastEventMod!=g_data.m_nLastEventMod)||(*/nChanges)//)
					{
if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_EVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "checking events %d != %d, changes=0x%02x", g_data.m_nLastEventMod,g_data.m_pEvents->m_nLastEventMod, nChanges);
					
						if(g_data.m_pEvents->m_ppEvents)
						{
							EnterCriticalSection(&(g_data.m_pInEvents->m_critEvents));

							g_data.m_pInEvents->DeleteEvents();
							int e=0;
							bool bFound=false;
							bool bAllocated=false;
							while((nChanges&(WHEEL_EVENT_IN|WHEEL_EVENT_MOD))&&(e<g_data.m_pEvents->m_nEvents))
							{
				EnterCriticalSection(&g_settings.m_crit);
								if((g_settings.m_pszInEvent)&&(strlen(g_settings.m_pszInEvent)>0)&&(g_data.m_pEvents->m_ppEvents[e]->m_szName.Compare(g_settings.m_pszInEvent)==0))
								{
				LeaveCriticalSection(&g_settings.m_crit);
									bFound=true;
									if(g_data.m_pEvents->m_nLastEventMod!=g_data.m_pInEvents->m_nLastEventMod)
									{
										g_data.m_pInEvents->m_nLastEventMod = g_data.m_pEvents->m_nLastEventMod;
					EnterCriticalSection(&g_data.m_critEventsSettings);
										g_data.m_bInEventChanged=false; //reset
					LeaveCriticalSection(&g_data.m_critEventsSettings);
									}
								}
								else
								{
				LeaveCriticalSection(&g_settings.m_crit);
									if(bFound) break; // just stop it there.
								}

								if(bFound)
								{
									if(!bAllocated)
									{
										g_data.m_pInEvents->m_ppEvents = new CTabulatorEvent*[g_settings.m_nTriggerBuffer];
										bAllocated = true; // check for errors someday
									}
									g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents] = new CTabulatorEvent;
									if(g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents])
									{
										*(g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents]) = *(g_data.m_pEvents->m_ppEvents[e]);
										g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents]->m_szName = g_data.m_pEvents->m_ppEvents[e]->m_szName;
										g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents]->m_szHost = g_data.m_pEvents->m_ppEvents[e]->m_szHost;
										g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents]->m_szSource = g_data.m_pEvents->m_ppEvents[e]->m_szSource;
										g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents]->m_szScene = g_data.m_pEvents->m_ppEvents[e]->m_szScene;
										g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents]->m_szIdentifier = g_data.m_pEvents->m_ppEvents[e]->m_szIdentifier;
										g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents]->m_szValue = g_data.m_pEvents->m_ppEvents[e]->m_szValue;
										g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents]->m_szParamDependencies = g_data.m_pEvents->m_ppEvents[e]->m_szParamDependencies;
										g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents]->m_szDestModule = g_data.m_pEvents->m_ppEvents[e]->m_szDestModule;
										g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents]->m_szLayer = g_data.m_pEvents->m_ppEvents[e]->m_szLayer;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_EVENTS)) 
g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "in event %02d: %s, %.3f: %s:%s:%s",
							 g_data.m_pInEvents->m_nEvents,
							 g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents]->m_szName,
							 g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents]->m_dblTriggerTime,
							 g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents]->m_szScene,
							 g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents]->m_szIdentifier,
							 g_data.m_pInEvents->m_ppEvents[g_data.m_pInEvents->m_nEvents]->m_szValue
							 );// Sleep(100); //(Dispatch message)
										g_data.m_pInEvents->m_nEvents++;
									}
								}

								e++;
							}
							LeaveCriticalSection(&(g_data.m_pInEvents->m_critEvents));

							EnterCriticalSection(&(g_data.m_pOutEvents->m_critEvents));

							g_data.m_pOutEvents->DeleteEvents();
							e=0;
							bFound=false;
							bAllocated = false;
							while((nChanges&(WHEEL_EVENT_OUT|WHEEL_EVENT_MOD))&&(e<g_data.m_pEvents->m_nEvents))
							{
				EnterCriticalSection(&g_settings.m_crit);
								if((g_settings.m_pszOutEvent)&&(strlen(g_settings.m_pszOutEvent)>0)&&(g_data.m_pEvents->m_ppEvents[e]->m_szName.Compare(g_settings.m_pszOutEvent)==0))
								{
				LeaveCriticalSection(&g_settings.m_crit);
									bFound=true;
									if(g_data.m_pEvents->m_nLastEventMod!=g_data.m_pOutEvents->m_nLastEventMod)
									{
										g_data.m_pOutEvents->m_nLastEventMod = g_data.m_pEvents->m_nLastEventMod;
					EnterCriticalSection(&g_data.m_critEventsSettings);
										g_data.m_bOutEventChanged=false; //reset
					LeaveCriticalSection(&g_data.m_critEventsSettings);
									}
								}
								else
								{
				LeaveCriticalSection(&g_settings.m_crit);
									if(bFound) break; // just stop it there.
								}

								if(bFound)
								{
									if(!bAllocated)
									{
										g_data.m_pOutEvents->m_ppEvents = new CTabulatorEvent*[g_settings.m_nTriggerBuffer];
										bAllocated = true; // check for errors someday
									}
									g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents] = new CTabulatorEvent;
									if(g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents])
									{
										*(g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents]) = *(g_data.m_pEvents->m_ppEvents[e]);
										g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents]->m_szName = g_data.m_pEvents->m_ppEvents[e]->m_szName;
										g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents]->m_szHost = g_data.m_pEvents->m_ppEvents[e]->m_szHost;
										g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents]->m_szSource = g_data.m_pEvents->m_ppEvents[e]->m_szSource;
										g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents]->m_szScene = g_data.m_pEvents->m_ppEvents[e]->m_szScene;
										g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents]->m_szIdentifier = g_data.m_pEvents->m_ppEvents[e]->m_szIdentifier;
										g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents]->m_szValue = g_data.m_pEvents->m_ppEvents[e]->m_szValue;
										g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents]->m_szParamDependencies = g_data.m_pEvents->m_ppEvents[e]->m_szParamDependencies;
										g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents]->m_szDestModule = g_data.m_pEvents->m_ppEvents[e]->m_szDestModule;
										g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents]->m_szLayer = g_data.m_pEvents->m_ppEvents[e]->m_szLayer;

if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_EVENTS)) 
g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "out event %02d: %s, %.3f: %s:%s:%s",
							 g_data.m_pOutEvents->m_nEvents,
							 g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents]->m_szName,
							 g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents]->m_dblTriggerTime,
							 g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents]->m_szScene,
							 g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents]->m_szIdentifier,
							 g_data.m_pOutEvents->m_ppEvents[g_data.m_pOutEvents->m_nEvents]->m_szValue
							 );// Sleep(100); //(Dispatch message)

										g_data.m_pOutEvents->m_nEvents++;
									}
								}

								e++;
							}
							LeaveCriticalSection(&(g_data.m_pOutEvents->m_critEvents));
						

							EnterCriticalSection(&(g_data.m_pTextEvents->m_critEvents));

							g_data.m_pTextEvents->DeleteEvents();
							e=0;
							bFound=false;
							bAllocated = false;
							while((nChanges&(WHEEL_EVENT_TEXT|WHEEL_EVENT_MOD))&&(e<g_data.m_pEvents->m_nEvents))
							{
				EnterCriticalSection(&g_settings.m_crit);
								if((g_settings.m_pszTextEvent)&&(strlen(g_settings.m_pszTextEvent)>0)&&(g_data.m_pEvents->m_ppEvents[e]->m_szName.Compare(g_settings.m_pszTextEvent)==0))
								{
				LeaveCriticalSection(&g_settings.m_crit);
									bFound=true;
									if(g_data.m_pEvents->m_nLastEventMod!=g_data.m_pTextEvents->m_nLastEventMod)
									{
										g_data.m_pTextEvents->m_nLastEventMod = g_data.m_pEvents->m_nLastEventMod;
					EnterCriticalSection(&g_data.m_critEventsSettings);
										g_data.m_bTextEventChanged=false; //reset
					LeaveCriticalSection(&g_data.m_critEventsSettings);
									}
								}
								else
								{
				LeaveCriticalSection(&g_settings.m_crit);
									if(bFound) break; // just stop it there.
								}

								if(bFound)
								{
									if(!bAllocated)
									{
										g_data.m_pTextEvents->m_ppEvents = new CTabulatorEvent*[g_settings.m_nTriggerBuffer];
										bAllocated = true; // check for errors someday
									}
									g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents] = new CTabulatorEvent;
									if(g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents])
									{
										*(g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents]) = *(g_data.m_pEvents->m_ppEvents[e]);
										g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents]->m_szName = g_data.m_pEvents->m_ppEvents[e]->m_szName;
										g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents]->m_szHost = g_data.m_pEvents->m_ppEvents[e]->m_szHost;
										g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents]->m_szSource = g_data.m_pEvents->m_ppEvents[e]->m_szSource;
										g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents]->m_szScene = g_data.m_pEvents->m_ppEvents[e]->m_szScene;
										g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents]->m_szIdentifier = g_data.m_pEvents->m_ppEvents[e]->m_szIdentifier;
										g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents]->m_szValue = g_data.m_pEvents->m_ppEvents[e]->m_szValue;
										g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents]->m_szParamDependencies = g_data.m_pEvents->m_ppEvents[e]->m_szParamDependencies;
										g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents]->m_szDestModule = g_data.m_pEvents->m_ppEvents[e]->m_szDestModule;
										g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents]->m_szLayer = g_data.m_pEvents->m_ppEvents[e]->m_szLayer;

if((g_ptabmsgr)&&(g_settings.m_ulDebug&WHEEL_DEBUG_EVENTS)) 
g_ptabmsgr->DM(MSG_ICONNONE, NULL, szEventSource, "text event %02d: %s, %.3f: %s:%s:%s",
							 g_data.m_pTextEvents->m_nEvents,
							 g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents]->m_szName,
							 g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents]->m_dblTriggerTime,
							 g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents]->m_szScene,
							 g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents]->m_szIdentifier,
							 g_data.m_pTextEvents->m_ppEvents[g_data.m_pTextEvents->m_nEvents]->m_szValue
							 );// Sleep(100); //(Dispatch message)
										g_data.m_pTextEvents->m_nEvents++;
									}
								}

								e++;
							}
							LeaveCriticalSection(&(g_data.m_pTextEvents->m_critEvents));
						}
						else
						{
							EnterCriticalSection(&(g_data.m_pInEvents->m_critEvents));
							g_data.m_pInEvents->DeleteEvents();
							LeaveCriticalSection(&(g_data.m_pInEvents->m_critEvents));

							EnterCriticalSection(&(g_data.m_pOutEvents->m_critEvents));
							g_data.m_pOutEvents->DeleteEvents();
							LeaveCriticalSection(&(g_data.m_pOutEvents->m_critEvents));

							EnterCriticalSection(&(g_data.m_pTextEvents->m_critEvents));
							g_data.m_pTextEvents->DeleteEvents();
							LeaveCriticalSection(&(g_data.m_pTextEvents->m_critEvents));

						}
						g_data.m_nLastEventMod=g_data.m_pEvents->m_nLastEventMod;
						// safety reset;
					EnterCriticalSection(&g_data.m_critEventsSettings);
						g_data.m_bInEventChanged=false;
						g_data.m_bOutEventChanged=false;
						g_data.m_bTextEventChanged=false;
					LeaveCriticalSection(&g_data.m_critEventsSettings);

					}

					LeaveCriticalSection(&(g_data.m_pEvents->m_critEvents));
				}
			}
			else  // client.
			{
				if(g_dlldata.thread[WHEEL_DATA]) g_dlldata.thread[WHEEL_DATA]->bKillThread=true;
				if(g_dlldata.thread[WHEEL_TICKER]) g_dlldata.thread[WHEEL_TICKER]->bKillThread=true;
			}
			Sleep(1);

/*
FILE* fp = fopen("waiting.txt", "ab");
if(fp)
{
	fprintf(fp,"inside ui thread %d %d %d\r\n",			
		(g_dlldata.thread[WHEEL_DATA]->bThreadStarted),
		(g_dlldata.thread[WHEEL_TICKER]->bThreadStarted),
		(g_dlldata.thread[WHEEL_UI]->bThreadStarted));
	fclose(fp);
}
*/
		}

		if(pdata)			delete pdata;


		if(g_dlldata.thread[WHEEL_DATA]) g_dlldata.thread[WHEEL_DATA]->bKillThread=true;
		if(g_dlldata.thread[WHEEL_TICKER]) g_dlldata.thread[WHEEL_TICKER]->bKillThread=true;


		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "DataSynchronizer:de-init", "Waiting for threads" );  //(Dispatch message)

		while((g_dlldata.thread[WHEEL_DATA])&&(g_dlldata.thread[WHEEL_TICKER])&&((g_dlldata.thread[WHEEL_DATA]->bThreadStarted)||(g_dlldata.thread[WHEEL_TICKER]->bThreadStarted)))
		{
			Sleep(1);
		}
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "DataSynchronizer:end", "DataSynchronizer thread ended." );  //(Dispatch message)

		g_dlldata.thread[WHEEL_UI]->bThreadStarted=false;
/*
FILE* fp = fopen("waiting.txt", "ab");
if(fp)
{
	fprintf(fp,"inside ui thread end %d %d %d\r\n",			
		(g_dlldata.thread[WHEEL_DATA]->bThreadStarted),
		(g_dlldata.thread[WHEEL_TICKER]->bThreadStarted),
		(g_dlldata.thread[WHEEL_UI]->bThreadStarted));
	fclose(fp);
}
*/
	}
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SynchronizationThread", "thread ended"); //(Dispatch message)

}



int CWheelApp::ExitInstance() 
{
			EnterCriticalSection(&g_settings.m_crit);
	g_settings.Settings(false);
			LeaveCriticalSection(&g_settings.m_crit);

  CoUninitialize(); //XML
	
	return CWinApp::ExitInstance();
}




