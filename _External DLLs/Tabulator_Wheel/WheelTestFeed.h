#if !defined(AFX_WHEELTESTFEED_H__87040D33_5BCE_455A_BD3A_2DCE8150A5CA__INCLUDED_)
#define AFX_WHEELTESTFEED_H__87040D33_5BCE_455A_BD3A_2DCE8150A5CA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WheelTestFeed.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CWheelTestFeed dialog

class CWheelTestFeed : public CDialog
{
// Construction
public:
	CWheelTestFeed(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CWheelTestFeed)
	enum { IDD = IDD_DIALOG_TESTFEED };
	BOOL	m_bFile;
	CString	m_szURL;
	BOOL	m_bTimes;
	//}}AFX_DATA


	CInet m_inet;
	bool m_bDownloading;

		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWheelTestFeed)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
public:
	CString SimpleXMLIndent(CString szXML);


protected:

	// Generated message map functions
	//{{AFX_MSG(CWheelTestFeed)
	afx_msg void OnButtonStart();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WHEELTESTFEED_H__87040D33_5BCE_455A_BD3A_2DCE8150A5CA__INCLUDED_)
