# Microsoft Developer Studio Project File - Name="Wheel" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=Wheel - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Wheel.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Wheel.mak" CFG="Wheel - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Wheel - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "Wheel - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Wheel - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_USRDLL" /Fr /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 mfcs42.lib MSVCRT.lib rpcrt4.lib /nologo /subsystem:windows /dll /machine:I386 /nodefaultlib:"MSVCRT.lib"

!ELSEIF  "$(CFG)" == "Wheel - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_USRDLL" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 mfcs42.lib MSVCRT.lib rpcrt4.lib /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Wheel - Win32 Release"
# Name "Wheel - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\..\Common\TXT\BufferUtil.cpp

!IF  "$(CFG)" == "Wheel - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Wheel - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\..\Common\IMG\BMP\CBmpUtil_MFC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Cortex\3.0.4.4\CortexShared.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MFC\ODBC\DBUtil.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MFC\EditEx\EditEx.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TXT\FileUtil.cpp

!IF  "$(CFG)" == "Wheel - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Wheel - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MFC\Inet\Inet.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\API\Miranda\IS2Core.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\KEY\LicenseKey.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MFC\ListCtrlEx\ListCtrlEx.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TXT\LogUtil.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MSG\Messager.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MSG\MessagingObject.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\LAN\NetUtil.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TTY\Serial.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\SMTP\smtp.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\SMTP\SMTPUtil.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TXT\SmutFilter\smutfilter.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\..\Tabulator\Tabulator.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Tabulator\TabulatorData.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Tabulator\TabulatorDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Tabulator\TabulatorHandler.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Tabulator\TabulatorMain.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Tabulator\TabulatorSettings.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TXT\VDIfile\VDIfile.cpp
# End Source File
# Begin Source File

SOURCE=.\Wheel.cpp
# End Source File
# Begin Source File

SOURCE=.\Wheel.def
# End Source File
# Begin Source File

SOURCE=.\Wheel.rc
# End Source File
# Begin Source File

SOURCE=.\WheelCore.cpp
# End Source File
# Begin Source File

SOURCE=.\WheelData.cpp
# End Source File
# Begin Source File

SOURCE=.\WheelDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WheelSettings.cpp
# End Source File
# Begin Source File

SOURCE=.\WheelTestFeed.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\..\Common\TXT\BufferUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\IMG\BMP\CBmpUtil_MFC.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Cortex\3.0.4.4\CortexDefines.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Cortex\3.0.4.4\CortexShared.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MFC\ODBC\DBUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MFC\EditEx\EditEx.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TXT\FileUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MFC\Inet\Inet.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\API\Miranda\IS2Core.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\KEY\LicenseKey.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MFC\ListCtrlEx\ListCtrlEx.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TXT\LogUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MSG\Messager.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MSG\MessagingObject.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\MSG\msg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\LAN\NetDefines.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\LAN\NetUtil.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TTY\Serial.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\SMTP\smtp.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\SMTP\SMTPUtil.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TXT\SmutFilter\smutfilter.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\..\Tabulator\Tabulator.h
# End Source File
# Begin Source File

SOURCE=..\..\Tabulator\TabulatorData.h
# End Source File
# Begin Source File

SOURCE=..\..\Tabulator\TabulatorDefines.h
# End Source File
# Begin Source File

SOURCE=..\..\Tabulator\TabulatorDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\Tabulator\TabulatorHandler.h
# End Source File
# Begin Source File

SOURCE=..\..\Tabulator\TabulatorMain.h
# End Source File
# Begin Source File

SOURCE=..\..\Tabulator\TabulatorSettings.h
# End Source File
# Begin Source File

SOURCE=..\..\..\Common\TXT\VDIfile\VDIfile.h
# End Source File
# Begin Source File

SOURCE=.\Wheel.h
# End Source File
# Begin Source File

SOURCE=.\WheelCore.h
# End Source File
# Begin Source File

SOURCE=.\WheelData.h
# End Source File
# Begin Source File

SOURCE=.\WheelDlg.h
# End Source File
# Begin Source File

SOURCE=.\WheelSettings.h
# End Source File
# Begin Source File

SOURCE=.\WheelTestFeed.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bb_check.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bb_clr.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bb_reload.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bb_x.bmp
# End Source File
# Begin Source File

SOURCE=".\res\bitmapbn-left.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\bitmapbn-right.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\bn_age.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bn_commi.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bn_great.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bn_lengt.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bn_playe.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bn_q.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cursor_c.cur
# End Source File
# Begin Source File

SOURCE=.\res\cursor_u.cur
# End Source File
# Begin Source File

SOURCE=.\res\dricons.bmp
# End Source File
# Begin Source File

SOURCE=.\res\icons3.bmp
# End Source File
# Begin Source File

SOURCE=.\res\settings.bmp
# End Source File
# Begin Source File

SOURCE=".\res\Status-Blu.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Status-Grn.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Status-Red.bmp"
# End Source File
# Begin Source File

SOURCE=".\res\Status-Yel.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\stdicons.bmp
# End Source File
# Begin Source File

SOURCE=.\res\vidicons.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Wheel.rc2
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
