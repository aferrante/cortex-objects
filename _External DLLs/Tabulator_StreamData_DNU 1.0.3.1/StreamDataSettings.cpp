// StreamDataSettings.cpp : implementation file
//

#include "stdafx.h"
#include "StreamData.h"
#include "StreamDataData.h"
#include "StreamDataSettings.h"
#include <process.h>
#include "../../../Cortex Objects/Tabulator/TabulatorDefines.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


extern DLLdata_t      g_dlldata;
extern CStreamDataData			g_data;
extern CStreamDataSettings	g_settings;
extern CMessager*			g_ptabmsgr;
extern CStreamDataApp			theOppApp;

extern void TimeCodeDataThread(void* pvArgs);


/////////////////////////////////////////////////////////////////////////////
// CStreamDataSettings dialog


CStreamDataSettings::CStreamDataSettings(/*CWnd* pParent /*=NULL*/)
//	: CDialog(CStreamDataSettings::IDD, pParent)
{
/*
	//{{AFX_DATA_INIT(CStreamDataSettings)
	m_bAutoconnect = FALSE;
	m_bTickerPlayOnce = TRUE;
	m_szHost = _T("127.0.0.1");
	m_szBaseURL = _T("http://www.feed.com/");
	m_nBatch = 250;
	m_nPort = TABULATOR_PORT_CMD;
	m_nPollIntervalMS = 5000;
	m_szProgramID = _T("10");
	m_nRequestIntervalMS = 0;
	m_nTickerPort = 7795;
	m_szTickerHost = _T("127.0.0.1");
	m_bSmut_ReplaceCurses = FALSE;
	m_nDoneCount = 3;
	//}}AFX_DATA_INIT
*/
	m_ulDebug = 0;
//	m_nProgramStreamOffset = 0;
//	m_nTimerExpiryManualDelayMS=0;
//	m_nTimerExpiryAutoDelayMS=0;

//	m_nTimerStartOffsetMS = 0;
//	m_nTimerDurationAdjustMS = 0;


	m_pszAppName = NULL;
	m_pszInternalAppName = NULL;
	m_pszAboutText = NULL;
	m_pszSettingsText = NULL;

	m_bMillisecondMessaging=true;			// use millisecond resolution for messages and asrun

	m_bShowDiagnosticWindow = false;


	m_bEnableTones=false;			// allows us to turn off the function, leave the others
	m_bEnableSCTE104=true;		// allows us to turn off the function, leave the others


/*
	m_pszMessageToken = NULL; 
	m_pszAux1Token = NULL; 
	m_pszAux2Token = NULL; 
*/

	// DSN params
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;

/*
	m_pszFeed = NULL;  // the Messages table name
	m_pszTicker = NULL;
*/
	
/*
	m_pszTonesAsRunFilename = NULL;
	m_pszTonesAsRunDestination = NULL;
	m_pszTonesAsRunFileLocation = NULL;
	m_pszTonesAsRunFileFormat = NULL;
	m_nTonesAsRunLogOffsetInterval=18000;

	m_bAsRunDTMF=true;
	m_bAsRunTestDTMF=false;
*/

	m_pszSCTE104AsRunFilename = NULL;
	m_pszSCTE104AsRunDestination = NULL;
	m_pszSCTE104AsRunFileLocation = NULL;
	m_pszSCTE104AsRunFileFormat = NULL;
	m_nSCTE104AsRunLogOffsetInterval=18000;

	m_bAsRunNationalBreak=true;
	m_bAsRunLocalBreak=true;
//	m_bAsRunLocal060Break=true;
//	m_bAsRunLocal120Break=true;
//	m_bAsRunBreakEnd=true;

	m_nNationalMinimumDurationMS=1000; // number of milliseconds, if less than this, message will not be sent
	m_nLocalMinimumDurationMS=1000; // number of milliseconds, if less than this, message will not be sent
	
/*
	m_nNationalHarrisTagSecEventType=-1; // uses tag only if event type matches.  -1 turns this off and just uses any secondary event.
	m_nLocalHarrisTagSecEventType=-1; // uses tag only if event type matches.  -1 turns this off and just uses any secondary event.
	m_bUseNationalInpointSecTiming=true; // uses the secondary event's timing, instead of its primary, for the duration calc
	m_bUseNationalOutpointSecTiming=true;// uses the secondary event's timing, instead of its primary, for the duration calc

	m_bTonesUseDurationAddition=true;  // addup durations in intervals, instead of using difference in time codes.



	m_bDoNationalBreakTones = true;
	m_nNationalBreakTonesDelayMS = 4000;
*/

	m_pszMessages = NULL;  // the Messages table name
	m_pszAsRun = NULL;  // the As-run table name
//	m_pszEventView = NULL;  // the Event view name // to pull events if nec.
	m_pszSettings = NULL;
//	m_pszStreamDatas = NULL;  // the StreamDatas table name


/*
	m_pszCrawlEventID=NULL;  // just do one for now.
	m_nCrawlDurationMS=-1;
	m_nSecType=128;  // pure sec event
	m_nCrawlPaddingMS=5000;
*/


//	m_pszReplaceAdEvent = NULL;  // the event identifier of the transition in (Server_Ticker_In) command
//	m_pszReplaceContentEvent = NULL;  // the event identifier of the transition in (Server_Ticker_In) command
//	m_pszEventNotificationEvent = NULL;  // the event identifier of the "BLOCK only" event
//	m_pszRejoinMainEvent = NULL;  // the event identifier of the PASS tabulator event (includes Rejoin Main)
//	m_pszTimerEvent = NULL;  // the event identifier of the BLOCK GPO w Timer event

	m_pszSCTE104NationalEvent=NULL;  // the event identifier of the national break event
	m_pszSCTE104LocalEvent=NULL;  // the event identifier of the national break event
//	m_pszSCTE104Local060Event=NULL;  // the event identifier of the local break event
//	m_pszSCTE104Local120Event=NULL;  // the event identifier of the local break event
//	m_pszSCTE104BreakEndEvent=NULL;  // the event identifier of break end event

	m_pszSCTE104NationalInpointID=NULL;  // the harris event ID of the inpoint event for national
	m_pszSCTE104NationalOutpointID=NULL;  // the harris event ID of the outpoint event for national
	m_pszSCTE104LocalInpointID=NULL;  // the harris event ID of the inpoint event for national
	m_pszSCTE104LocalOutpointID=NULL;  // the harris event ID of the outpoint event for national
//	m_pszSCTE104Local060ID=NULL;  // the harris event ID of the inpoint event for LCL060
//	m_pszSCTE104Local120ID=NULL;  // the harris event ID of the outpoint event for LCL120
	m_pszSCTE104ProgramTag=NULL;  // the Identifying token of the program identifier

	m_nSCTE104NationalPreroll=8000;  // number of milliseconds preroll
	m_nSCTE104LocalPreroll=8000;  // number of milliseconds preroll

	
	m_bSCTE104UseDurationAddition = true;  // addup durations in intervals, instead of using difference in time codes.

	m_pszAutoModule = NULL;
	m_pszAutoLists = NULL;
	m_pszAutoServers=NULL;   // as in connections

	m_pszAutoEvents = NULL;
//	m_pszReplacementRules = NULL;
//	m_pszDescriptionPattern = NULL;
//	m_pszClassificationRules = NULL;
	m_pszTabulatorModule = NULL;
	m_pszTabulatorHost=NULL;  // the Tabulator host IP or name
	m_nTabulatorPort = TABULATOR_PORT_CMD;  // the Tabulator host port


	m_pszDestinationModule=NULL;  // as in Libretto
	m_pszDestinationTable=NULL;   // as in Destinations

//	m_pszButtonsTableName=NULL;  // for determining state


//	m_pszOpportuneEvents=NULL;
//	m_pszExclusionRules=NULL;

/*
	m_pszSendDTMFToken=NULL;
	m_pszDTMFAddress=NULL;// the IP address of the unit handling the DTMF tones.
	m_ucDTMFAddress[0] = 0;
	m_ucDTMFAddress[1] = 0;
	m_ucDTMFAddress[2] = 0;
	m_ucDTMFAddress[3] = 0;


	m_pszInpointID=NULL;  // the harris event ID of the inpoint event
	m_pszOutpointID=NULL;  // the harris event ID of the outpoint event

// new settings, these were internal before.
	m_nHarrisTagSecEventType=-1; // uses tag only if event type matches.  -1 turns this off and just uses any secondary event.
	m_bUseInpointSecTiming=false; // uses the secondary event's timing, instead of its primary, for the duration calc
	m_bUseOutpointSecTiming=false;// uses the secondary event's timing, instead of its primary, for the duration calc

	m_pszDTMFFormat=NULL;  // the format of the DTMF string
*/

	m_nDurationUnitDivisor=1000; // divides milliseconds to get unit.  so 1000 gets you seconds. 100 gets you tenths.  33 gets you NTSC frames
	m_bRoundToNearestUnit=true;

/*
	char* m_pszCrawlEventID;  // just do one for now.
	int m_nCrawlDurationMS;
	int m_nCrawlPaddingMS;
  int m_nSecType;
*/



/*
	m_pszBlockedIDToken = NULL;
	m_pszBlockedDescToken = NULL;

	m_pszUnknownIDToken = NULL;
	m_pszUnknownDescToken = NULL;


	m_pszColDelimToken = NULL;
	m_pszClassTestFormat = NULL;

	m_pszTimerToken=NULL;
	m_pszTimerStartToken=NULL;
	m_pszTimerTable=NULL;
*/


	m_pszModule=NULL;  // the module name (Libretto)
	m_pszQueue=NULL;  // the module's queue table name (Queue of Libretto.dbo.Queue)
	m_pszDrivenHost=NULL;  // IP address of the VANC inserter for libretto to route properly

	m_nTriggerBuffer = 32;

	m_nAutomationAnalysisLookahead=100;  // number of events to parse.


//	m_pszAutoServer=NULL;  // as in MAIN-DS
	m_nAutoServerListNumber=0;   // as in 1
	m_bAutomationLookahead = false;
	m_nAutoListGetDelayMS=0;   // delay in between incrementer change and a list event get.
	m_nAutomationLoggingLookahead=0;  // number of events to log from top.

	m_bReadableEncodedAsRun = true;

//	m_pszDestinationHost = NULL;

/*
	m_pszTickerMessagePreamble = NULL;

	m_pszTickerBackplateName = NULL; 
	m_pszTickerLogoName = NULL; 
	m_nTickerBackplateID=0; 
	m_nTickerLogoID=2; 
	m_nTickerTextID=1;
	m_nTriggerBuffer  =12;

	m_nTickerEngineType = StreamData_TICKER_ENGINE_INT; // send events to Libretto or wherever!

	m_nMaxMessages = 500;


	m_pszInEvent=NULL;  // the event identifier of the transition in (Server_Ticker_In) command
	m_pszOutEvent=NULL;  // the event identifier of the transition out (Server_Ticker_Out) command
	m_pszTextEvent=NULL;  // the event identifier of the text message event

	m_nGraphicsHostType = TABULATOR_DESTTYPE_CHYRON_CHANNELBOX;

	m_bAutoPurgeExpired=false;
	m_bAutoPurgeExceeded=false;
	m_nAutoPurgeExpiredAfterMins=60;
	m_nAutoPurgeExceededAfterMins=60;

	m_bAutostartTicker=false;
	m_bAutostartData=false;
	m_bUseLocalTime=true;  // or use unixtime


	m_nLayoutSalvoFormat = TABULATOR_HARRIS_ICONII_LSFORMAT_ALPHA;
*/


	InitializeCriticalSection(&m_crit);


	m_bUseTimeCode = false;
	m_nFrameRate=30;   // for time code addresses NTSC=30, PAL=25
	m_bDF=true;  // Drop frame

}

CStreamDataSettings::~CStreamDataSettings()
{
	EnterCriticalSection(&m_crit);
	if(m_pszAppName) free(m_pszAppName);
	if(m_pszInternalAppName) free(m_pszInternalAppName);
	if(m_pszAboutText) free(m_pszAboutText);
	if(m_pszSettingsText) free(m_pszSettingsText);


//	if(m_pszOpportuneEvents) free(m_pszOpportuneEvents);
//	if(m_pszExclusionRules) free(m_pszExclusionRules);

/*
	if(m_pszSendDTMFToken) free(m_pszSendDTMFToken);
	if(m_pszDTMFFormat) free(m_pszDTMFFormat);
	if(m_pszDTMFAddress) free(m_pszDTMFAddress);
*/	

	
	if(m_pszDSN) free(m_pszDSN);
	if(m_pszUser) free(m_pszUser);
	if(m_pszPW) free(m_pszPW);
//	if(m_pszFeed) free(m_pszFeed);
//	if(m_pszTicker) free(m_pszTicker);

/*
	if(m_pszTickerMessagePreamble) free(m_pszTickerMessagePreamble);
	if(m_pszTickerBackplateName) free(m_pszTickerBackplateName);
	if(m_pszTickerLogoName) free(m_pszTickerLogoName);
*/
	if(m_pszAsRun) free(m_pszAsRun);
	if(m_pszSettings) free(m_pszSettings);
//	if(m_pszDestinationHost) free(m_pszDestinationHost);

/*
	if(m_pszTonesAsRunFilename) free(m_pszTonesAsRunFilename);
	if(m_pszTonesAsRunDestination) free(m_pszTonesAsRunDestination);
	if(m_pszTonesAsRunFileLocation) free(m_pszTonesAsRunFileLocation);
	if(m_pszTonesAsRunFileFormat) free(m_pszTonesAsRunFileFormat);
*/
	if(m_pszSCTE104AsRunFilename) free(m_pszSCTE104AsRunFilename);
	if(m_pszSCTE104AsRunDestination) free(m_pszSCTE104AsRunDestination);
	if(m_pszSCTE104AsRunFileLocation) free(m_pszSCTE104AsRunFileLocation);
	if(m_pszSCTE104AsRunFileFormat) free(m_pszSCTE104AsRunFileFormat);

	if(m_pszSCTE104NationalInpointID) free(m_pszSCTE104NationalInpointID);
	if(m_pszSCTE104NationalOutpointID) free(m_pszSCTE104NationalOutpointID);
	if(m_pszSCTE104LocalInpointID) free(m_pszSCTE104LocalInpointID);
	if(m_pszSCTE104LocalOutpointID) free(m_pszSCTE104LocalOutpointID);
//	if(m_pszSCTE104Local060ID) free(m_pszSCTE104Local060ID);
//	if(m_pszSCTE104Local120ID) free(m_pszSCTE104Local120ID);
	if(m_pszSCTE104ProgramTag) free(m_pszSCTE104ProgramTag);



	if(m_pszAutoModule) free(m_pszAutoModule);
	if(m_pszAutoLists) free(m_pszAutoLists);
	if(m_pszAutoServers) free(m_pszAutoServers);
	if(m_pszAutoEvents) free(m_pszAutoEvents);

	if(m_pszDestinationModule) free(m_pszDestinationModule);
	if(m_pszDestinationTable) free(m_pszDestinationTable);

	if(m_pszSCTE104NationalEvent) free(m_pszSCTE104NationalEvent);
	if(m_pszSCTE104LocalEvent) free(m_pszSCTE104LocalEvent);
//	if(m_pszSCTE104Local060Event) free(m_pszSCTE104Local060Event);
//	if(m_pszSCTE104Local120Event) free(m_pszSCTE104Local120Event);
//	if(m_pszSCTE104BreakEndEvent) free(m_pszSCTE104BreakEndEvent);


/*
	if(m_pszModule) free(m_pszModule);
	if(m_pszQueue) free(m_pszQueue);
	if(m_pszInEvent) free(m_pszInEvent);
	if(m_pszOutEvent) free(m_pszOutEvent);
	if(m_pszTextEvent) free(m_pszTextEvent);

	if(m_pszMessageToken) free(m_pszMessageToken);
	if(m_pszAux1Token) free(m_pszAux1Token);
	if(m_pszAux2Token) free(m_pszAux2Token);
*/
	LeaveCriticalSection(&m_crit);
	DeleteCriticalSection(&m_crit);

	if(m_pszTabulatorModule) free(m_pszTabulatorModule);
	if(m_pszTabulatorHost) free(m_pszTabulatorHost);

}

/*

void CStreamDataSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStreamDataSettings)
	DDX_Check(pDX, IDC_CHECK_AUTOCONNECT, m_bAutoconnect);
	DDX_Check(pDX, IDC_CHECK_PLAYONCE, m_bTickerPlayOnce);
	DDX_CBString(pDX, IDC_COMBO_HOST, m_szHost);
	DDX_Text(pDX, IDC_EDIT_BASEURL, m_szBaseURL);
	DDX_Text(pDX, IDC_EDIT_BATCH, m_nBatch);
	DDV_MinMaxInt(pDX, m_nBatch, 0, 2000000);
	DDX_Text(pDX, IDC_EDIT_C_PORT, m_nPort);
	DDV_MinMaxInt(pDX, m_nPort, 1024, 65535);
	DDX_Text(pDX, IDC_EDIT_POLL, m_nPollIntervalMS);
	DDV_MinMaxInt(pDX, m_nPollIntervalMS, 0, 86400000);
	DDX_Text(pDX, IDC_EDIT_PROGRAM, m_szProgramID);
	DDX_Text(pDX, IDC_EDIT_REQ, m_nRequestIntervalMS);
	DDV_MinMaxInt(pDX, m_nRequestIntervalMS, 0, 86400000);
	DDX_Text(pDX, IDC_EDIT_T_PORT, m_nTickerPort);
	DDV_MinMaxInt(pDX, m_nTickerPort, 1024, 65535);
	DDX_Text(pDX, IDC_EDIT_HOST, m_szTickerHost);
	DDX_Check(pDX, IDC_CHECK_SMUT, m_bSmut_ReplaceCurses);
	DDX_Text(pDX, IDC_EDIT_DONE, m_nDoneCount);
	DDV_MinMaxInt(pDX, m_nDoneCount, 0, 2000000);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CStreamDataSettings, CDialog)
	//{{AFX_MSG_MAP(CStreamDataSettings)
	ON_BN_CLICKED(IDC_BUTTON_TESTFEED, OnButtonTestfeed)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_CHECK_PLAYONCE, OnCheckPlayonce)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStreamDataSettings message handlers

CString CStreamDataSettings::RemoteSettingsToString()
{
	CString szSettings="";
	if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_TICKER)&&(g_dlldata.thread[StreamData_DATA])&&(g_dlldata.thread[StreamData_TICKER])) 
	{
		szSettings.Format(_T("%s%c%s%c%s%c%s%c%s%c%s%c%d%c%d%c%d%c%d%c%s%c%d%c%d%c%d%c%s%c%d%c%d%c%d"),
				g_dlldata.thread[StreamData_DATA]->pszThreadName, 28,
				g_dlldata.thread[StreamData_DATA]->pszBaseURL, 28,
				g_dlldata.thread[StreamData_DATA]->pszProgramParamName, 28,
				g_dlldata.thread[StreamData_DATA]->pszProgramID, 28,
				g_dlldata.thread[StreamData_DATA]->pszBatchParamName, 28,
				g_dlldata.thread[StreamData_DATA]->pszFromIDParamName, 28,
				g_dlldata.thread[StreamData_DATA]->nBatchParam, 28,
				g_dlldata.thread[StreamData_DATA]->nShortIntervalMS, 28,
				g_dlldata.thread[StreamData_DATA]->nLongIntervalMS, 28,
				(g_dlldata.thread[StreamData_DATA]->bDirParamStyle?1:0), 28,
				g_dlldata.thread[StreamData_TICKER]->pszThreadName, 28,
				g_dlldata.thread[StreamData_TICKER]->nBatchParam, 28,
				g_dlldata.thread[StreamData_TICKER]->nShortIntervalMS, 28,
				g_dlldata.thread[StreamData_TICKER]->nLongIntervalMS, 28,
				g_dlldata.thread[StreamData_TICKER]->pszHost, 28,
				g_dlldata.thread[StreamData_TICKER]->nPort, 28,
				g_dlldata.thread[StreamData_TICKER]->nAuxValue, 28,
				g_settings.m_nDoneCount);
	}

	return  szSettings;
}

int CStreamDataSettings::StringToRemoteSettings(CString szSettings)
{
	if(szSettings.GetLength())
	{

		char chFields[2];
		sprintf(chFields,"%c",28);
		int f=0, n=0;
		CString szField = szSettings;
		CString szTemp;
		do
		{
			f=szField.Find(chFields);
			if(f>=0)
			{
				n++;
				szTemp = szField.Mid(f+1);
				szField=szTemp;
			}
		} while((f>=0)&&(szField.GetLength()>0)&&(n<=17));

//	AfxMessageBox(szSettings);

		f=0;
		if(n>=16)
		{
			while((f<=n)&&(f<18)) // prevent runaway
			{
				int i=szSettings.Find(chFields);
				if(i>-1){szField = szSettings.Left(i);}
				else {szField=szSettings;}  //last field.

				char* pch = NULL;
				char* pchTemp = NULL;
	//CString foo; foo.Format(_T("parsing field %d of %d: %s"), f, n, szField); AfxMessageBox(foo);
				switch(f)
				{
				case  0: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_DATA)&&(g_dlldata.thread[StreamData_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
								pchTemp = g_dlldata.thread[StreamData_DATA]->pszThreadName;
								g_dlldata.thread[StreamData_DATA]->pszThreadName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  1: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_DATA)&&(g_dlldata.thread[StreamData_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
								pchTemp = g_dlldata.thread[StreamData_DATA]->pszBaseURL;
								g_dlldata.thread[StreamData_DATA]->pszBaseURL  = pch;
								LeaveCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
								g_settings.m_szBaseURL = szField;
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  2: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_DATA)&&(g_dlldata.thread[StreamData_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
								pchTemp = g_dlldata.thread[StreamData_DATA]->pszProgramParamName;
								g_dlldata.thread[StreamData_DATA]->pszProgramParamName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  3: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_DATA)&&(g_dlldata.thread[StreamData_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
								pchTemp = g_dlldata.thread[StreamData_DATA]->pszProgramID;
								g_dlldata.thread[StreamData_DATA]->pszProgramID  = pch;
								LeaveCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
								g_settings.m_szProgramID = szField;
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  4: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_DATA)&&(g_dlldata.thread[StreamData_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
								pchTemp = g_dlldata.thread[StreamData_DATA]->pszBatchParamName;
								g_dlldata.thread[StreamData_DATA]->pszBatchParamName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  5: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_DATA)&&(g_dlldata.thread[StreamData_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
								pchTemp = g_dlldata.thread[StreamData_DATA]->pszFromIDParamName;
								g_dlldata.thread[StreamData_DATA]->pszFromIDParamName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
//									pData->pszToDateParamName = fu.GetIniString("DataDownload", "ToDateParamName", "To", pData->pszToDateParamName);   //  %p
//									pData->pszFromDateParamName = fu.GetIniString("DataDownload", "FromDateParamName", "From", pData->pszFromDateParamName);   //  %p
//									pData->pszNowParamName = fu.GetIniString("DataDownload", "NowParamName", "time", pData->pszNowParamName);   //  %p
//									pData->pszBatchParamName = fu.GetIniString("DataDownload", "BatchParamName", "limit", pData->pszBatchParamName);   //  %p
//									pData->pszFromIDParamName = fu.GetIniString("DataDownload", "FromIDParamName", "startid", pData->pszFromIDParamName);   //  %p
//									pData->pszGetIDParamName = fu.GetIniString("DataDownload", "GetIDParamName", "id", pData->pszGetIDParamName);   //  %p

//									pData->pszToDateParamName=NULL;				//  %t
//									pData->pszFromDateParamName=NULL;			//  %f
//									pData->pszNowParamName=NULL;					//  %n
//									pData->pszBatchParamName=NULL;				//  %b
//									pData->pszFromIDParamName=NULL;				//  %m  // start id
//									pData->pszGetIDParamName=NULL;				//  %i
				case  6: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_DATA)&&(g_dlldata.thread[StreamData_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
							g_dlldata.thread[StreamData_DATA]->nBatchParam  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
							g_settings.m_nBatch = atoi(szField);
						}
					} break;
				case  7: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_DATA)&&(g_dlldata.thread[StreamData_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
							g_dlldata.thread[StreamData_DATA]->nShortIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
							g_settings.m_nRequestIntervalMS = atoi(szField);
						}
					} break;
				case  8: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_DATA)&&(g_dlldata.thread[StreamData_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
							g_dlldata.thread[StreamData_DATA]->nLongIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
							g_settings.m_nPollIntervalMS = atoi(szField);
						}
					} break;
				case  9: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_DATA)&&(g_dlldata.thread[StreamData_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
							g_dlldata.thread[StreamData_DATA]->bDirParamStyle  = ((atoi(szField)>0)?true:false);
							LeaveCriticalSection(&g_dlldata.thread[StreamData_DATA]->m_crit);
						}
					} break;



				case  10: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_TICKER)&&(g_dlldata.thread[StreamData_TICKER])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[StreamData_TICKER]->m_crit);
								pchTemp = g_dlldata.thread[StreamData_TICKER]->pszThreadName;
								g_dlldata.thread[StreamData_TICKER]->pszThreadName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[StreamData_TICKER]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  11: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_TICKER)&&(g_dlldata.thread[StreamData_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[StreamData_TICKER]->m_crit);
							g_dlldata.thread[StreamData_TICKER]->nBatchParam  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[StreamData_TICKER]->m_crit);
						}
					} break;
				case  12: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_TICKER)&&(g_dlldata.thread[StreamData_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[StreamData_TICKER]->m_crit);
							g_dlldata.thread[StreamData_TICKER]->nShortIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[StreamData_TICKER]->m_crit);
						}
					} break;
				case  13: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_TICKER)&&(g_dlldata.thread[StreamData_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[StreamData_TICKER]->m_crit);
							g_dlldata.thread[StreamData_TICKER]->nLongIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[StreamData_TICKER]->m_crit);
						}
					} break;
				case  14: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_TICKER)&&(g_dlldata.thread[StreamData_TICKER])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[StreamData_TICKER]->m_crit);
								pchTemp = g_dlldata.thread[StreamData_TICKER]->pszHost;
								g_dlldata.thread[StreamData_TICKER]->pszHost  = pch;
								LeaveCriticalSection(&g_dlldata.thread[StreamData_TICKER]->m_crit);
								g_settings.m_szTickerHost = szField;
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  15: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_TICKER)&&(g_dlldata.thread[StreamData_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[StreamData_TICKER]->m_crit);
							g_dlldata.thread[StreamData_TICKER]->nPort  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[StreamData_TICKER]->m_crit);
							g_settings.m_nTickerPort = atoi(szField);
						}
					} break;
				case  16: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>StreamData_TICKER)&&(g_dlldata.thread[StreamData_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[StreamData_TICKER]->m_crit);
							g_dlldata.thread[StreamData_TICKER]->nAuxValue  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[StreamData_TICKER]->m_crit);
							g_settings.m_bTickerPlayOnce = ((atoi(szField)>0)?true:false);
						}
					} break;
				case  17: 
					{
						g_settings.m_nDoneCount = atoi(szField);
					} break;

				default: break;
				}
				if(i>-1){szField = szSettings.Mid(i+1);}
				else {szField=_T("");}
				szSettings = szField;
				f++;
			}
			return CLIENT_SUCCESS;
		}
	}
	return CLIENT_ERROR;
	
	
	
}

*/


int CStreamDataSettings::Settings(bool bRead)
{
	CFileUtil fu;
	if(fu.GetSettings(theOppApp.m_szSettingsFilename, false)&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
			char* pch;
			pch = fu.GetIniString("Global", "AppName", "StreamData");
			if(pch)
			{
				if(m_pszAppName) free(m_pszAppName);
				m_pszAppName = pch;
			}

			pch = fu.GetIniString("Global", "InternalAppName", "StreamData");
			if(pch)
			{
				if(m_pszInternalAppName) free(m_pszInternalAppName);
				m_pszInternalAppName = pch;
			}

			pch = (char*)malloc(strlen(m_pszAppName)+strlen(VERSION_STRING)+strlen(", version ")+1);
			if(pch)
			{
				if(m_pszAboutText) free(m_pszAboutText);
				m_pszAboutText = pch;
				sprintf(m_pszAboutText, "%s, version %s", m_pszAppName, VERSION_STRING);
			}

			pch = fu.GetIniString("Global", "SettingsText", "StreamData settings...");
			if(pch)
			{
				if(m_pszSettingsText) free(m_pszSettingsText);
				m_pszSettingsText = pch;
			}

			m_ulDebug = fu.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.

			m_pszDSN = fu.GetIniString("Database", "DSN", "Tabulator", m_pszDSN);
			m_pszUser = fu.GetIniString("Database", "DBUser", "sa", m_pszUser);
			m_pszPW = fu.GetIniString("Database", "DBPassword", "", m_pszPW);
//				m_pszSettings = fu.GetIniString("Database", "SettingsTableName", "Settings", m_pszSettings );  // the Settings table name


			bool bEnableTones = fu.GetIniInt("GlobalConfiguration", "EnableTones", 1)?true:false;     // allows us to turn off the function, leave the others

			bool bEnableSCTE104 = fu.GetIniInt("GlobalConfiguration", "EnableSCTE104", 1)?true:false; // allows us to turn off the function, leave the others


/// as run...
			//TONES
/*
			m_pszTonesAsRunFilename = fu.GetIniString("TonesLogging", "AsRunFileSpec", "Logs\\SerialTones\\T|YD01010500|C:\\Cortex\\Logs\\SerialTones\\T%m%d%y.log|1|1", m_pszTonesAsRunFilename);  // the as run filename
			pch = fu.GetIniString("TonesLogging", "AsRunToken", "serialtones_asrun", NULL);  // the as run destination name
			
			if((!bEnableTones)&&(m_bEnableTones))
			{
				if(m_pszTonesAsRunDestination) g_ptabmsgr->RemoveDestination( m_pszTonesAsRunDestination );
			}
*/
			// need this one line.
			m_bEnableTones = bEnableTones;
/*
			if(pch)
			{
//				if(m_pszTonesAsRunDestination)
				{
					DoTonesAsRunDestinationName(m_pszTonesAsRunDestination, pch);
				}
			}

			//then, process overrides.
			m_pszTonesAsRunFileLocation = fu.GetIniString("TonesLogging", "AsRunFileLocation", "C:\\Cortex\\Logs\\SerialTones\\", m_pszTonesAsRunFileLocation);  // the as run directory.
			m_pszTonesAsRunFileFormat = fu.GetIniString("TonesLogging", "AsRunFileFormat", "T%m%d%y.log", m_pszTonesAsRunFileFormat);
			m_nTonesAsRunLogOffsetInterval = fu.GetIniInt("TonesLogging", "AsRunLogOffsetInterval", 18000);

			if((m_nTonesAsRunLogOffsetInterval < 0)||(m_nTonesAsRunLogOffsetInterval > 86400))
				m_nTonesAsRunLogOffsetInterval = 18000; //default to 5 am.

			DoTonesAsRunFilename();


			m_bAsRunDTMF = fu.GetIniInt("TonesLogging", "AsRunDTMF", 1)?true:false;
			m_bAsRunTestDTMF = fu.GetIniInt("TonesLogging", "AsRunTestDTMF", 0)?true:false;
*/


			//SCTE104
			m_pszSCTE104AsRunFilename = fu.GetIniString("SCTE104Logging", "AsRunFileSpec", "Logs\\StreamData\\S|YD01010500|C:\\Cortex\\Logs\\StreamData\\S%m%d%y.log|1|1", m_pszSCTE104AsRunFilename);  // the as run filename
			pch = fu.GetIniString("SCTE104Logging", "AsRunToken", "streamdata_asrun", NULL);  // the as run destination name
			
			if((!bEnableSCTE104)&&(m_bEnableSCTE104))
			{
				if(m_pszSCTE104AsRunDestination) g_ptabmsgr->RemoveDestination( m_pszSCTE104AsRunDestination );
			}

			m_bEnableSCTE104 = bEnableSCTE104;

			if(pch)
			{
//				if(m_pszSCTE104AsRunDestination)
				{
					DoSCTE104AsRunDestinationName(m_pszSCTE104AsRunDestination, pch);
				}
			}

			//then, process overrides.
			m_pszSCTE104AsRunFileLocation = fu.GetIniString("SCTE104Logging", "AsRunFileLocation", "C:\\Cortex\\Logs\\StreamData\\", m_pszSCTE104AsRunFileLocation);  // the as run directory.
			m_pszSCTE104AsRunFileFormat = fu.GetIniString("SCTE104Logging", "AsRunFileFormat", "S%m%d%y.log", m_pszSCTE104AsRunFileFormat);
			m_nSCTE104AsRunLogOffsetInterval = fu.GetIniInt("SCTE104Logging", "AsRunLogOffsetInterval", 18000);

			if((m_nSCTE104AsRunLogOffsetInterval < 0)||(m_nSCTE104AsRunLogOffsetInterval > 86400))
				m_nSCTE104AsRunLogOffsetInterval = 18000; //default to 5 am.

			DoSCTE104AsRunFilename();


			m_bAsRunNationalBreak = fu.GetIniInt("SCTE104Logging", "AsRunNationalBreak", 1)?true:false;
			m_bAsRunLocalBreak = fu.GetIniInt("SCTE104Logging", "AsRunLocalBreak", 1)?true:false;
//			m_bAsRunLocal060Break = fu.GetIniInt("SCTE104Logging", "AsRunLocal060Break", 1)?true:false;
//			m_bAsRunLocal120Break = fu.GetIniInt("SCTE104Logging", "AsRunLocal120Break", 1)?true:false;
//			m_bAsRunBreakEnd = fu.GetIniInt("SCTE104Logging", "AsRunBreakEnd", 1)?true:false;
			
			
			
			m_pszMessages=fu.GetIniString("Database", "MessagesTableName", "Messages", m_pszMessages);   // the Messages table name
			m_pszAsRun=fu.GetIniString("Database", "AsRunTableName", "AsRun_Log", m_pszAsRun);  // the As-run table name
			m_pszSettings=fu.GetIniString("Database", "SettingsTableName", "StreamData_7_Settings", m_pszSettings);  // the settings table name
//			m_pszStreamDatas=fu.GetIniString("Database", "StreamDatasTableName", "StreamDatas", m_pszStreamDatas);   // the StreamDatas table name

			m_bMillisecondMessaging = fu.GetIniInt("Messager", "MillisecondMessaging", 1)?true:false;			// use millisecond resolution for messages and asrun
			m_bReadableEncodedAsRun = fu.GetIniInt("Messager", "ReadableEncodedAsRun", 1)?true:false;			// hex encode for readability, otherwise base 64 encoded


			m_pszAutoModule = fu.GetIniString("Database", "AutomationModuleName", "Sentinel", m_pszAutoModule);
			m_pszAutoServers = fu.GetIniString("Database", "AutomationConnectionsTableName", "Connections", m_pszAutoServers);
			m_pszAutoLists = fu.GetIniString("Database", "AutomationChannelsTableName", "Channels", m_pszAutoLists);
			m_pszAutoEvents = fu.GetIniString("Database", "AutomationEventsTableName", "Events", m_pszAutoEvents);

			m_pszDestinationModule = fu.GetIniString("Database", "DestinationsModuleName", "Libretto", m_pszDestinationModule);//// as in Libretto
			m_pszDestinationTable = fu.GetIniString("Database", "DestinationsTableName", "Destinations", m_pszDestinationTable); // as in Destinations

//			m_pszOpportuneEvents = fu.GetIniString("Database", "OpportuneEventsTableName", "OpportuneEvents", m_pszOpportuneEvents);
//			m_pszExclusionRules = fu.GetIniString("Database", "ExclusionRulesTableName", "ExclusionRules", m_pszExclusionRules);

//			m_pszReplacementRules = fu.GetIniString("Database", "ReplacementRulesTableName", "ReplacementRules", m_pszReplacementRules);
//			m_pszDescriptionPattern = fu.GetIniString("Database", "DescriptionPatternTableName", "DescriptionPattern", m_pszDescriptionPattern);
//			m_pszClassificationRules = fu.GetIniString("Database", "ClassificationRulesTableName", "ClassificationRules", m_pszClassificationRules);
//			m_pszButtonsTableName = fu.GetIniString("Database", "ButtonsTableName", "Buttons", m_pszButtonsTableName);;  // for determining state

			m_pszTabulatorModule = fu.GetIniString("Tabulator", "TabulatorModuleName", "Tabulator", m_pszTabulatorModule);
//			m_pszTabulatorPlugInModule = fu.GetIniString("Tabulator", "PlugInModuleName", "Videoframe", m_pszTabulatorPlugInModule);  // the Tabulator module name (Videoframe)
//			m_pszSendDTMFToken = fu.GetIniString("Tabulator", "SendDTMFEventLabel", "Send_DTMF", m_pszSendDTMFToken);  
			

			m_pszTabulatorHost = fu.GetIniString("Tabulator", "HostAddress", "127.0.0.1", m_pszTabulatorHost);  // the Tabulator host IP or name
			m_nTabulatorPort = fu.GetIniInt("Tabulator", "CommandServerPort", TABULATOR_PORT_CMD);  // the Tabulator host port


//			m_pszInpointID = fu.GetIniString("TonesConfiguration", "InpointID", "SATROFF", m_pszInpointID);  // // the harris event ID of the inpoint event
//			m_pszOutpointID = fu.GetIniString("TonesConfiguration", "OutpointID", "SATRON", m_pszOutpointID);  // the harris event ID of the outpoint event

// new settings, these were internal before.
/*
			m_nHarrisTagSecEventType = fu.GetIniInt("TonesConfiguration", "HarrisTagSecEventType", -1);  // uses tag only if event type matches.  -1 turns this off and just uses any secondary event.
			if(m_nHarrisTagSecEventType>255) m_nHarrisTagSecEventType=-1;

			m_bUseInpointSecTiming = fu.GetIniInt("TonesConfiguration", "UseInpointSecTiming", 0)?true:false;  // uses the secondary event's timing, instead of its primary, for the duration calc
			m_bUseOutpointSecTiming = fu.GetIniInt("TonesConfiguration", "UseOutpointSecTiming", 0)?true:false;  // uses the secondary event's timing, instead of its primary, for the duration calc

			m_pszDTMFFormat = fu.GetIniString("TonesConfiguration", "DTMFFormat", "D%03d", m_pszDTMFFormat);  // the format of the DTMF string
			m_pszDTMFAddress = fu.GetIniString("TonesConfiguration", "DTMFAddress", "", m_pszDTMFAddress);  

			if((m_pszDTMFAddress)&&(strlen(m_pszDTMFAddress)<7)) // minimum 0.0.0.0
			{
				free(m_pszDTMFAddress); m_pszDTMFAddress=NULL;
			}

			SetTonesIP(m_pszDTMFAddress);

			m_nDurationUnitDivisor = fu.GetIniInt("TonesConfiguration", "DurationUnitDivisor", 1000); // divides milliseconds to get unit.  so 1000 gets you seconds. 10 gets you tenths.  33 gets you NTSC frames
			m_bRoundToNearestUnit = fu.GetIniInt("TonesConfiguration", "RoundToNearestUnit", 1)?true:false; 


			m_bDoNationalBreakTones = fu.GetIniInt("TonesConfiguration", "DoTonesWithNationalBreak", 1)?true:false;
			m_nNationalBreakTonesDelayMS = fu.GetIniInt("TonesConfiguration", "NationalBreakTonesDelayMS", 4000); // milliseconds after receipt of National break command to send DTMF
			m_bTonesUseDurationAddition = fu.GetIniInt("TonesConfiguration", "UseDurationAddition", 1)?true:false; // addup durations in intervals, instead of using difference in time codes.
*/

			m_nDurationUnitDivisor = fu.GetIniInt("SCTE104Configuration", "DurationUnitDivisor", 1000); // divides milliseconds to get unit.  so 1000 gets you seconds. 100 gets you tenths.  33 gets you NTSC frames
			m_bRoundToNearestUnit = fu.GetIniInt("SCTE104Configuration", "RoundToNearestUnit", 1)?true:false; 

			m_pszSCTE104NationalInpointID = fu.GetIniString("SCTE104Configuration", "NationalInpointID", "DAI-BS", m_pszSCTE104NationalInpointID);  
			m_pszSCTE104NationalOutpointID = fu.GetIniString("SCTE104Configuration", "NationalOutpointID", "DAI-BE", m_pszSCTE104NationalOutpointID); 
			m_pszSCTE104LocalInpointID = fu.GetIniString("SCTE104Configuration", "LocalInpointID", "DAI-LS", m_pszSCTE104LocalInpointID);  
			m_pszSCTE104LocalOutpointID = fu.GetIniString("SCTE104Configuration", "LocalOutpointID", "DAI-LE", m_pszSCTE104LocalOutpointID); 
			m_pszSCTE104ProgramTag = fu.GetIniString("SCTE104Configuration", "ProgramTag", "ppl_prog_id=", m_pszSCTE104ProgramTag); 
//			m_pszSCTE104Local060ID = fu.GetIniString("SCTE104Configuration", "Local060ID", "LCL060", m_pszSCTE104Local060ID); 
//			m_pszSCTE104Local120ID = fu.GetIniString("SCTE104Configuration", "Local120ID", "LCL120", m_pszSCTE104Local120ID); 

			m_nSCTE104NationalPreroll = fu.GetIniInt("SCTE104Configuration", "NationalPreroll", 8000);  // number of milliseconds preroll
			m_nSCTE104LocalPreroll = fu.GetIniInt("SCTE104Configuration", "LocalPreroll", 8000);  // number of milliseconds preroll



/*
			m_nNationalHarrisTagSecEventType=fu.GetIniInt("SCTE104Configuration", "NationalHarrisTagSecEventType", -1); // uses tag only if event type matches.  -1 turns this off and just uses any secondary event.
			if(m_nNationalHarrisTagSecEventType>255) m_nHarrisTagSecEventType=-1;
			m_nLocalHarrisTagSecEventType=fu.GetIniInt("SCTE104Configuration", "LocalHarrisTagSecEventType", -1); // uses tag only if event type matches.  -1 turns this off and just uses any secondary event.
			if(m_nLocalHarrisTagSecEventType>255) m_nLocalHarrisTagSecEventType=-1;
*/

			m_nNationalMinimumDurationMS = fu.GetIniInt("SCTE104Configuration", "NationalMinimumDurationMS", 1000); // number of milliseconds, if less than this, message will not be sent
			m_nLocalMinimumDurationMS = fu.GetIniInt("SCTE104Configuration", "LocalMinimumDurationMS", 1000); // number of milliseconds, if less than this, message will not be sent

//			m_bUseNationalInpointSecTiming = fu.GetIniInt("SCTE104Configuration", "UseInpointSecTiming", 1)?true:false;  // uses the secondary event's timing, instead of its primary, for the duration calc
//			m_bUseNationalOutpointSecTiming = fu.GetIniInt("SCTE104Configuration", "UseOutpointSecTiming", 1)?true:false;  // uses the secondary event's timing, instead of its primary, for the duration calc
			m_bSCTE104UseDurationAddition = fu.GetIniInt("SCTE104Configuration", "UseDurationAddition", 1)?true:false; // addup durations in intervals, instead of using difference in time codes.



//			m_pszBlockedIDToken		= fu.GetIniString("Output", "BlockedIDToken", "TV+:BLOCKED", m_pszBlockedIDToken);
//			m_pszBlockedDescToken = fu.GetIniString("Output", "BlockedDescToken", "", m_pszBlockedDescToken);

//			m_pszUnknownIDToken		= fu.GetIniString("Output", "UnknownIDToken", "TV+:UNKNOWN", m_pszUnknownIDToken);
//			m_pszUnknownDescToken = fu.GetIniString("Output", "UnknownDescToken", "", m_pszUnknownDescToken);

//			m_nProgramStreamOffset = fu.GetIniInt("Output", "ProgramStreamOffset", 0);  

//			m_nTimerExpiryManualDelayMS = fu.GetIniInt("Output", "TimerExpiryManualDelayMS", 0);  
//			m_nTimerExpiryAutoDelayMS = fu.GetIniInt("Output", "TimerExpiryDelayMS", 0);  


//			m_nTimerStartOffsetMS = fu.GetIniInt("Output", "TimerStartOffsetMS", 0);  
//			m_nTimerDurationAdjustMS = fu.GetIniInt("Output", "TimerDurationAdjustMS", 0);  


//			if(m_nProgramStreamOffset<0)	m_nProgramStreamOffset=0;
//			if(m_nProgramStreamOffset>30) m_nProgramStreamOffset=30;

//			m_pszColDelimToken = fu.GetIniString("Output", "ColDelimToken", "�", m_pszColDelimToken);
//			m_pszClassTestFormat = fu.GetIniString("Output", "ClassTestFormat", "%i�%t", m_pszClassTestFormat);

//			m_pszTimerToken = fu.GetIniString("Output", "TimerToken", "Timer", m_pszTimerToken);
//			m_pszTimerTable = fu.GetIniString("Database", "TimerTableName", "Timers", m_pszTimerTable);
//			m_pszTimerStartToken = fu.GetIniString("Output", "TimerStartToken", "Timer|Timer_Start", m_pszTimerStartToken);

			m_bShowDiagnosticWindow = fu.GetIniInt("Output", "ShowDiagnosticWindow", 0)?true:false;

			m_nAutomationAnalysisLookahead = fu.GetIniInt("Automation", "AutomationAnalysisLookahead", 100);  // number of events to parse.
//			m_pszAutoServer = fu.GetIniString("Automation", "AutomationServer", "MAIN-DS", m_pszAutoServer);// as in MAIN-DS
			m_nAutoServerListNumber = fu.GetIniInt("Automation", "AutomationListNumber", 0);  // list number
			m_bAutomationLookahead = fu.GetIniInt("Automation", "AutomationLookahead", 0)?true:false;
			m_nAutoListGetDelayMS = fu.GetIniInt("Automation", "AutomationListGetDelayMS", 0);   // delay in between incrementer change and a list event get.
			m_nAutomationLoggingLookahead = fu.GetIniInt("Automation", "AutomationLoggingLookahead",0);  // number of events to log from top.


			m_pszModule=fu.GetIniString("Output", "DrivenModule", "Libretto", m_pszModule);  // the module name (Libretto)
			m_pszQueue=fu.GetIniString("Output", "DrivenQueue", "Command_Queue", m_pszQueue); // the module's queue table name (Queue of Libretto.dbo.Queue)
			m_pszDrivenHost=fu.GetIniString("Output", "DrivenHost", "0.0.0.0", m_pszDrivenHost);  // IP address of the VANC inserter for libretto to route properly

/*
			char setbuf[MAX_PATH];
			if(m_pszCrawlEventID) strcpy(setbuf, m_pszCrawlEventID); else strcpy(setbuf, "");
			m_pszCrawlEventID=fu.GetIniString("Output", "CrawlEventID", "Crawl2", m_pszCrawlEventID);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszCrawlEventID))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bCrawlEventChanged = true;  // the event identifier has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}


//			fu.SetIniString("Output", "CrawlEventID", m_pszCrawlEventID);  
			m_nCrawlDurationMS = fu.GetIniInt("Output", "CrawlDurationMS", 15000);  
			m_nCrawlPaddingMS = fu.GetIniInt("Output", "CrawlPaddingMS", 5000);  
			m_nSecType = fu.GetIniInt("Output", "SecType", 128);  
*/

			m_nTriggerBuffer  = fu.GetIniInt("Output", "TriggerBufferItems", 16);
			if(m_nTriggerBuffer>256) m_nTriggerBuffer=256;
			if(m_nTriggerBuffer<1) m_nTriggerBuffer=16; //default

			char setbuf[MAX_PATH];
			if(m_pszSCTE104NationalEvent) strcpy(setbuf, m_pszSCTE104NationalEvent); else strcpy(setbuf, "");
			m_pszSCTE104NationalEvent=fu.GetIniString("Output", "NationalEventID", "NationalBreak", m_pszSCTE104NationalEvent);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszSCTE104NationalEvent))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bNationalBreakEventChanged = true;  // the event identifier has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}

			if(m_pszSCTE104LocalEvent) strcpy(setbuf, m_pszSCTE104LocalEvent); else strcpy(setbuf, "");
			m_pszSCTE104LocalEvent=fu.GetIniString("Output", "LocalEventID", "LocalBreak", m_pszSCTE104LocalEvent);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszSCTE104LocalEvent))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bLocalBreakEventChanged = true;  // the event identifier has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}

/*
			if(m_pszSCTE104Local060Event) strcpy(setbuf, m_pszSCTE104Local060Event); else strcpy(setbuf, "");
			m_pszSCTE104Local060Event=fu.GetIniString("Output", "Local060EventID", "Local060Break", m_pszSCTE104Local060Event);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszSCTE104Local060Event))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bLocal060BreakEventChanged = true;  // the event identifier has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}

			if(m_pszSCTE104Local120Event) strcpy(setbuf, m_pszSCTE104Local120Event); else strcpy(setbuf, "");
			m_pszSCTE104Local120Event=fu.GetIniString("Output", "Local120EventID", "Local120Break", m_pszSCTE104Local120Event);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszSCTE104Local120Event))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bLocal120BreakEventChanged = true;  // the event identifier has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}

			if(m_pszSCTE104BreakEndEvent) strcpy(setbuf, m_pszSCTE104BreakEndEvent); else strcpy(setbuf, "");
			m_pszSCTE104BreakEndEvent=fu.GetIniString("Output", "BreakEndEventID", "BreakEnd", m_pszSCTE104BreakEndEvent);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszSCTE104BreakEndEvent))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bBreakEndEventChanged = true;  // the event identifier has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}
*/

			// null these out so they get reinitialized if the dtmf sequence changes.
/*
			if(m_pszSCTE104NationalEventDTMFsequence) strcpy(setbuf, m_pszSCTE104NationalEventDTMFsequence); else strcpy(setbuf, "");
			m_pszSCTE104NationalEventDTMFsequence=fu.GetIniString("Output", "SCTE104NationalDTMF", "150*", m_pszSCTE104NationalEventDTMFsequence);  // the DTMFsequence of the national break event
			if (strcmp(setbuf, m_pszSCTE104NationalEventDTMFsequence))
			{
				CSCTE104MultiMessage* m_pscte104Msg = g_data.m_pscte104MsgNationalBreak;
				g_data.m_pscte104MsgNationalBreak = NULL;
				if(m_pscte104Msg) { try{ delete m_pscte104Msg;} catch(...){} }
			}
			
			if(m_pszSCTE104Local060EventDTMFsequence) strcpy(setbuf, m_pszSCTE104Local060EventDTMFsequence); else strcpy(setbuf, "");
			m_pszSCTE104Local060EventDTMFsequence=fu.GetIniString("Output", "SCTE104Local060DTMF", "784*", m_pszSCTE104Local060EventDTMFsequence);  // the DTMFsequence of the local 060 event
			if (strcmp(setbuf, m_pszSCTE104Local060EventDTMFsequence))
			{
				CSCTE104MultiMessage* m_pscte104Msg = g_data.m_pscte104MsgLocal060Break;
				g_data.m_pscte104MsgLocal060Break = NULL;
				if(m_pscte104Msg) { try{ delete m_pscte104Msg;} catch(...){} }
			}

			if(m_pszSCTE104Local120EventDTMFsequence) strcpy(setbuf, m_pszSCTE104Local120EventDTMFsequence); else strcpy(setbuf, "");
			m_pszSCTE104Local120EventDTMFsequence=fu.GetIniString("Output", "SCTE104Local120DTMF", "465*", m_pszSCTE104Local120EventDTMFsequence);  // the DTMFsequence of the local 120 event
			if (strcmp(setbuf, m_pszSCTE104Local120EventDTMFsequence))
			{
				CSCTE104MultiMessage* m_pscte104Msg = g_data.m_pscte104MsgLocal120Break;
				g_data.m_pscte104MsgLocal120Break = NULL;
				if(m_pscte104Msg) { try{ delete m_pscte104Msg;} catch(...){} }
			}

			if(m_pszSCTE104BreakEndEventDTMFsequence) strcpy(setbuf, m_pszSCTE104BreakEndEventDTMFsequence); else strcpy(setbuf, "");
			m_pszSCTE104BreakEndEventDTMFsequence=fu.GetIniString("Output", "SCTE104BreakEndDTMF", "150#", m_pszSCTE104BreakEndEventDTMFsequence);  // the DTMFsequence of the Break End event
			if (strcmp(setbuf, m_pszSCTE104BreakEndEventDTMFsequence))
			{
				CSCTE104MultiMessage* m_pscte104Msg = g_data.m_pscte104MsgBreakEnd;
				g_data.m_pscte104MsgBreakEnd = NULL;
				if(m_pscte104Msg) { try{ delete m_pscte104Msg;} catch(...){} }
			}

*/


/*
			if(m_pszRejoinMainEvent) strcpy(setbuf, m_pszRejoinMainEvent); else strcpy(setbuf, "");
			m_pszRejoinMainEvent=fu.GetIniString("Output", "RejoinMainEventID", "RejoinMain", m_pszRejoinMainEvent);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszRejoinMainEvent))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bRejoinMainEventChanged = true;  // the event identifier has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}

			if(m_pszTimerEvent) strcpy(setbuf, m_pszTimerEvent); else strcpy(setbuf, "");
			m_pszTimerEvent=fu.GetIniString("Output", "TimerEventID", "TimerEvent", m_pszTimerEvent);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszTimerEvent))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bTimerEventChanged = true;  // the event identifier has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}

*/



/*

			char setbuf[MAX_PATH];
			if(m_pszInEvent) strcpy(setbuf, m_pszInEvent); else strcpy(setbuf, "");
			m_pszInEvent=fu.GetIniString("Ticker", "InEventID", "1000", m_pszInEvent);  // the event identifier of the transition in (Server_Ticker_In) command
								EnterCriticalSection(&g_data.m_critEventsSettings);
			if (strcmp(setbuf, m_pszInEvent)) 
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bInEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}
			if(m_pszOutEvent) strcpy(setbuf, m_pszOutEvent); else strcpy(setbuf, "");
			m_pszOutEvent=fu.GetIniString("Ticker", "OutEventID", "1001", m_pszOutEvent);  // the event identifier of the transition out (Server_Ticker_Out) command
								EnterCriticalSection(&g_data.m_critEventsSettings);
			if (strcmp(setbuf, m_pszOutEvent)) 
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bOutEventChanged = true;  // the event identifier of the transition out (Server_Ticker_Out) command has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}
			if(m_pszTextEvent) strcpy(setbuf, m_pszTextEvent); else strcpy(setbuf, "");
			m_pszTextEvent=fu.GetIniString("Ticker", "TextEventID", "1002", m_pszTextEvent);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszTextEvent))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bTextEventChanged = true;  // the event identifier of the text message event has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}

*/			

//			int GetIniInt(char* pszSection, char* pszEntry, int nDefaultValue);


			m_bUseTimeCode = fu.GetIniInt("TimeCode", "UseTCRDR", 0)?true:false; // use Adrienne Time code card 
			m_nFrameRate = fu.GetIniInt("TimeCode", "FrameRate", 30);   // for time code addresses NTSC=30, PAL=25
			m_bDF = fu.GetIniInt("TimeCode", "DropFrame", 1)?true:false;   // Drop frame


			if(m_bUseTimeCode)
			{
				// connect to the time code card if nec
				if(!g_data.m_bTimeCodeThreadStarted)
				{
					g_data.m_bTimeCodeThreadKill = false;

					if(_beginthread(TimeCodeDataThread, 0, (void*)(NULL))==-1)
					{
					//error.
if(g_ptabmsgr)
{
	char pszSource[MAX_PATH];
	_snprintf(pszSource, MAX_PATH-1, "%s:debug", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
	g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "Error starting time code thread");//   Sleep(250);//(Dispatch message)
}
					//**MSG
					}
					else Sleep(250); // let it start...
				}
				
			}
			else
			{
				// disconnect from the time code card if nec
				if(g_data.m_bTimeCodeThreadStarted)
				{
					g_data.m_bTimeCodeThreadKill = true;
				}

			}



		}
		else // write
		{
			if(m_pszAppName) fu.SetIniString("Global", "AppName", m_pszAppName);
			if(m_pszAppName) fu.SetIniString("Global", "InternalAppName", m_pszInternalAppName);
//			if(m_pszAboutText) fu.SetIniString("Global", "AboutText", m_pszAboutText);  // dont save
			if(m_pszSettingsText) fu.SetIniString("Global", "SettingsText", m_pszSettingsText);
//			int SetIniInt(char* pszSection, char* pszEntry, int nValue, char* pszComment=NULL);

			fu.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.

/*
			fu.SetIniString("TonesLogging", "AsRunFileSpec", m_pszTonesAsRunFilename);  // the as run filename
			fu.SetIniString("TonesLogging", "AsRunToken", m_pszTonesAsRunDestination);  // the as run destination name
			fu.SetIniString("TonesLogging", "AsRunFileLocation", m_pszTonesAsRunFileLocation);  // the as run directory.
			fu.SetIniString("TonesLogging", "AsRunFileFormat", m_pszTonesAsRunFileFormat);
			fu.SetIniInt("TonesLogging", "AsRunLogOffsetInterval", m_nTonesAsRunLogOffsetInterval);

			fu.SetIniInt("TonesLogging", "AsRunDTMF", m_bAsRunDTMF?1:0);
			fu.SetIniInt("TonesLogging", "AsRunTestDTMF", m_bAsRunTestDTMF?1:0);
*/

			fu.SetIniString("SCTE104Logging", "AsRunFileSpec", m_pszSCTE104AsRunFilename);  // the as run filename
			fu.SetIniString("SCTE104Logging", "AsRunToken", m_pszSCTE104AsRunDestination);  // the as run destination name
			fu.SetIniString("SCTE104Logging", "AsRunFileLocation", m_pszSCTE104AsRunFileLocation);  // the as run directory.
			fu.SetIniString("SCTE104Logging", "AsRunFileFormat", m_pszSCTE104AsRunFileFormat);
			fu.SetIniInt("SCTE104Logging", "AsRunLogOffsetInterval", m_nSCTE104AsRunLogOffsetInterval);

			fu.SetIniInt("SCTE104Logging", "AsRunNationalBreak", m_bAsRunNationalBreak?1:0);
			fu.SetIniInt("SCTE104Logging", "AsRunLocalBreak", m_bAsRunLocalBreak?1:0);
//			fu.SetIniInt("SCTE104Logging", "AsRunLocal120Break", m_bAsRunLocal120Break?1:0);
//			fu.SetIniInt("SCTE104Logging", "AsRunLocal060Break", m_bAsRunLocal060Break?1:0);
//			fu.SetIniInt("SCTE104Logging", "AsRunBreakEnd", m_bAsRunBreakEnd?1:0);

//				fu.SetIniInt("DefaultValues", "MessageDwellTime", m_nDefaultDwellTime);

			fu.SetIniString("Database", "DSN", m_pszDSN);
			fu.SetIniString("Database", "DBUser", m_pszUser);
			fu.SetIniString("Database", "DBPassword", m_pszPW);


			fu.SetIniString("Database", "AsRunTableName", m_pszAsRun);  // the As-run table name
			fu.SetIniString("Database", "SettingsTableName", m_pszSettings);  // the settings table name
			fu.SetIniString("Database", "MessagesTableName", m_pszMessages);   // the Messages table name
//			fu.SetIniString("Database", "StreamDatasTableName", m_pszStreamDatas);   // the StreamDatas table name
//			fu.SetIniString("Database", "ButtonsTableName", m_pszButtonsTableName);  // for determining state



//			fu.SetIniString("Output", "DrivenModule",  m_pszModule);  // the module name (Libretto)
//			fu.SetIniString("Output", "DrivenQueue", m_pszQueue); // the module's queue table name (Queue of Libretto.dbo.Queue)
			fu.SetIniInt("Output", "TriggerBufferItems", m_nTriggerBuffer);


//			fu.SetIniString("Ticker", "InEventID", m_pszInEvent);  // the event identifier of the transition in (Server_Ticker_In) command
//			fu.SetIniString("Ticker", "OutEventID", m_pszOutEvent);  // the event identifier of the transition out (Server_Ticker_Out) command
//			fu.SetIniString("Ticker", "TextEventID", m_pszTextEvent);  // the event identifier of the text message event



			fu.SetIniString("Database", "AutomationModuleName", m_pszAutoModule);
			fu.SetIniString("Database", "AutomationChannelsTableName", m_pszAutoLists);
			fu.SetIniString("Database", "AutomationEventsTableName", m_pszAutoEvents);
			fu.SetIniString("Database", "AutomationConnectionsTableName", m_pszAutoServers);


			fu.SetIniString("Database", "DestinationsModuleName", m_pszDestinationModule);//// as in Libretto
			fu.SetIniString("Database", "DestinationsTableName",  m_pszDestinationTable); // as in Destinations


//			fu.SetIniString("Database", "ReplacementRulesTableName", m_pszReplacementRules);
//			fu.SetIniString("Database", "DescriptionPatternTableName", m_pszDescriptionPattern);
//			fu.SetIniString("Database", "ClassificationRulesTableName", m_pszClassificationRules);
//			fu.SetIniString("Database", "TimerTableName", m_pszTimerTable);

//			fu.SetIniString("Database", "OpportuneEventsTableName", m_pszOpportuneEvents);
//			fu.SetIniString("Database", "ExclusionRulesTableName", m_pszExclusionRules);


			fu.SetIniString("Tabulator", "HostAddress", m_pszTabulatorHost);  // the Tabulator host IP or name
			fu.SetIniInt("Tabulator", "CommandServerPort", m_nTabulatorPort);  // the Tabulator host port
			fu.SetIniString("Tabulator", "TabulatorModuleName", m_pszTabulatorModule);
//			fu.SetIniString("Tabulator", "PlugInModuleName", m_pszTabulatorPlugInModule);  // the Tabulator module name (Videoframe)
//			fu.SetIniString("Tabulator", "SendDTMFEventLabel", m_pszSendDTMFToken); 


			fu.SetIniInt("GlobalConfiguration", "EnableTones", m_bEnableTones?1:0);     // allows us to turn off the function, leave the others
			fu.SetIniInt("GlobalConfiguration", "EnableSCTE104", m_bEnableSCTE104?1:0); // allows us to turn off the function, leave the others

/*
			fu.SetIniString("TonesConfiguration", "InpointID", m_pszInpointID);  // // the harris event ID of the inpoint event
			fu.SetIniString("TonesConfiguration", "OutpointID", m_pszOutpointID);  // the harris event ID of the outpoint event

// new settings, these were internal before.
			fu.SetIniInt("TonesConfiguration", "UseInpointSecTiming", m_bUseInpointSecTiming?1:0);  // uses the secondary event's timing, instead of its primary, for the duration calc
			fu.SetIniInt("TonesConfiguration", "UseOutpointSecTiming", m_bUseOutpointSecTiming?1:0);  // uses the secondary event's timing, instead of its primary, for the duration calc
			fu.SetIniInt("TonesConfiguration", "HarrisTagSecEventType", m_nHarrisTagSecEventType);  // uses tag only if event type matches.  -1 turns this off and just uses any secondary event.

			fu.SetIniString("TonesConfiguration", "DTMFFormat", m_pszDTMFFormat);  // the format of the DTMF string

			if((m_pszDTMFAddress)&&(strlen(m_pszDTMFAddress)>6))
			{
				fu.SetIniString("TonesConfiguration", "DTMFAddress", m_pszDTMFAddress);  
			}
			else
			{
				fu.SetIniString("TonesConfiguration", "DTMFAddress", "");  
			}

			fu.SetIniInt("TonesConfiguration", "DurationUnitDivisor", m_nDurationUnitDivisor); // divides milliseconds to get unit.  so 1000 gets you seconds. 100 gets you tenths.  33 gets you NTSC frames
			fu.SetIniInt("TonesConfiguration", "RoundToNearestUnit", m_bRoundToNearestUnit?1:0);

			fu.SetIniInt("TonesConfiguration", "DoTonesWithNationalBreak", m_bDoNationalBreakTones?1:0);
			fu.SetIniInt("TonesConfiguration", "NationalBreakTonesDelayMS", m_nNationalBreakTonesDelayMS); // milliseconds after receipt of National break command to send DTMF
			fu.SetIniInt("TonesConfiguration", "UseDurationAddition", m_bTonesUseDurationAddition?1:0); // addup durations in intervals, instead of using difference in time codes.
*/
			fu.SetIniInt("SCTE104Configuration", "DurationUnitDivisor", m_nDurationUnitDivisor); // divides milliseconds to get unit.  so 1000 gets you seconds. 100 gets you tenths.  33 gets you NTSC frames
			fu.SetIniInt("SCTE104Configuration", "RoundToNearestUnit", m_bRoundToNearestUnit?1:0);

			fu.SetIniString("SCTE104Configuration", "NationalInpointID", m_pszSCTE104NationalInpointID);  
			fu.SetIniString("SCTE104Configuration", "NationalOutpointID", m_pszSCTE104NationalOutpointID); 
			fu.SetIniString("SCTE104Configuration", "LocalInpointID", m_pszSCTE104LocalInpointID);  
			fu.SetIniString("SCTE104Configuration", "LocalOutpointID", m_pszSCTE104LocalOutpointID); 
			fu.SetIniString("SCTE104Configuration", "ProgramTag", m_pszSCTE104ProgramTag); 

			fu.SetIniInt("SCTE104Configuration", "NationalPreroll", m_nSCTE104NationalPreroll);  // number of milliseconds preroll
			fu.SetIniInt("SCTE104Configuration", "LocalPreroll", m_nSCTE104LocalPreroll);  // number of milliseconds preroll

//			fu.SetIniString("SCTE104Configuration", "Local060ID", m_pszSCTE104Local060ID); 
//			fu.SetIniString("SCTE104Configuration", "Local120ID", m_pszSCTE104Local120ID); 


//			fu.SetIniInt("SCTE104Configuration", "LocalHarrisTagSecEventType", m_nLocalHarrisTagSecEventType); // uses tag only if event type matches.  -1 turns this off and just uses any secondary event.
//			fu.SetIniInt("SCTE104Configuration", "NationalHarrisTagSecEventType", m_nNationalHarrisTagSecEventType); // uses tag only if event type matches.  -1 turns this off and just uses any secondary event.
			fu.SetIniInt("SCTE104Configuration", "NationalMinimumDurationMS", m_nNationalMinimumDurationMS); // number of milliseconds, if less than this, message will not be sent
			fu.SetIniInt("SCTE104Configuration", "LocalMinimumDurationMS", m_nLocalMinimumDurationMS); // number of milliseconds, if less than this, message will not be sent
//			fu.SetIniInt("SCTE104Configuration", "UseInpointSecTiming", m_bUseNationalInpointSecTiming?1:0);  // uses the secondary event's timing, instead of its primary, for the duration calc
//			fu.SetIniInt("SCTE104Configuration", "UseOutpointSecTiming", m_bUseNationalOutpointSecTiming?1:0);  // uses the secondary event's timing, instead of its primary, for the duration calc
			fu.SetIniInt("SCTE104Configuration", "UseDurationAddition", m_bSCTE104UseDurationAddition?1:0); // addup durations in intervals, instead of using difference in time codes.


			fu.SetIniString("Output", "NationalEventID", m_pszSCTE104NationalEvent);  // the event identifier of the event
			fu.SetIniString("Output", "LocalEventID", m_pszSCTE104LocalEvent);  // the event identifier of the event
//			fu.SetIniString("Output", "Local060EventID", m_pszSCTE104Local060Event);  // the event identifier of the event
//			fu.SetIniString("Output", "Local120EventID", m_pszSCTE104Local120Event);  // the event identifier of the event
//			fu.SetIniString("Output", "BreakEndEventID", m_pszSCTE104BreakEndEvent);  // the event identifier of the event
//			fu.SetIniString("Output", "RejoinMainEventID", m_pszRejoinMainEvent);  // the event identifier of the event
//			fu.SetIniString("Output", "TimerEventID", m_pszTimerEvent);  // the event identifier of the event

			
/*
			fu.SetIniString("Output", "SCTE104NationalDTMF", m_pszSCTE104NationalEventDTMFsequence);  // the DTMFsequence of the national break event
			fu.SetIniString("Output", "SCTE104Local060DTMF", m_pszSCTE104Local060EventDTMFsequence);;  // the DTMFsequence of the local 060 event
			fu.SetIniString("Output", "SCTE104Local120DTMF", m_pszSCTE104Local120EventDTMFsequence);  // the DTMFsequence of the local 120 event
			fu.SetIniString("Output", "SCTE104BreakEndDTMF", m_pszSCTE104BreakEndEventDTMFsequence);  // the DTMFsequence of the Break End event
*/

			fu.SetIniString("Output", "DrivenModule", m_pszModule);  // the module name (Libretto)
			fu.SetIniString("Output", "DrivenQueue", m_pszQueue); // the module's queue table name (Queue of Libretto.dbo.Queue)
			fu.SetIniString("Output", "DrivenHost", m_pszDrivenHost);  // IP address of the VANC inserter for libretto to route properly

//			fu.SetIniString("Output", "CrawlEventID", m_pszCrawlEventID);  
//			fu.SetIniInt("Output", "CrawlDurationMS", m_nCrawlDurationMS);  // IP address of the VANC inserter for libretto to route properly
//			fu.SetIniInt("Output", "SecType", m_nSecType);  

//			fu.SetIniInt("Output", "CrawlPaddingMS", m_nCrawlPaddingMS);  


//			fu.SetIniString("Output", "BlockedIDToken", m_pszBlockedIDToken);
//			fu.SetIniString("Output", "BlockedDescToken", m_pszBlockedDescToken);

//			fu.SetIniString("Output", "UnknownIDToken", m_pszUnknownIDToken);
//			fu.SetIniString("Output", "UnknownDescToken", m_pszUnknownDescToken);
//			fu.SetIniString("Output", "ColDelimToken", m_pszColDelimToken);
//			fu.SetIniString("Output", "ClassTestFormat", m_pszClassTestFormat, "Must match format specifiers in automation key assembly");
//			fu.SetIniString("Output", "TimerToken", m_pszTimerToken);
//			fu.SetIniString("Output", "TimerStartToken", m_pszTimerStartToken);


//			fu.SetIniInt("Output", "ProgramStreamOffset", m_nProgramStreamOffset);  
			fu.SetIniInt("Output", "ShowDiagnosticWindow", m_bShowDiagnosticWindow?1:0);

//			fu.SetIniInt("Output", "TimerExpiryManualDelayMS", m_nTimerExpiryManualDelayMS, "number of milliseconds to delay before evaluating timer expiry commands from manual processes");  
//			fu.SetIniInt("Output", "TimerExpiryDelayMS", m_nTimerExpiryAutoDelayMS, "number of milliseconds to delay before evaluating timer expiry commands from automated processes");  
//			fu.SetIniInt("Output", "TimerStartOffsetMS", m_nTimerStartOffsetMS, "number of milliseconds to offset the timer start");
//			fu.SetIniInt("Output", "TimerDurationAdjustMS", m_nTimerDurationAdjustMS, "number of milliseconds to adjust the timer duration");  


			fu.SetIniInt("Automation", "AutomationAnalysisLookahead", m_nAutomationAnalysisLookahead);  // number of events to parse.
//			fu.SetIniString("Automation", "AutomationServer", m_pszAutoServer);// as in MAIN-DS
			fu.SetIniInt("Automation", "AutomationListNumber", m_nAutoServerListNumber);  // list number
			fu.SetIniInt("Automation", "AutomationLookahead", m_bAutomationLookahead?1:0);
			fu.SetIniInt("Automation", "AutomationListGetDelayMS", m_nAutoListGetDelayMS);   // delay in between incrementer change and a list event get.
			fu.SetIniInt("Automation", "AutomationLoggingLookahead", m_nAutomationLoggingLookahead);  // number of events to log from top.




			fu.SetIniInt("TimeCode", "UseTCRDR", m_bUseTimeCode?1:0, "use Adrienne Time code card"); // use Adrienne Time code card 
			fu.SetIniInt("TimeCode", "FrameRate", m_nFrameRate, "for time code addresses NTSC=30, PAL=25, used only if no TC card available" );   // for time code addresses NTSC=30, PAL=25
			fu.SetIniInt("TimeCode", "DropFrame", m_bDF?1:0, "drop frame mode, used only if no TC card available");    // Drop frame

			fu.SetIniInt("Messager", "MillisecondMessaging", m_bMillisecondMessaging?1:0);		// use millisecond resolution for messages and asrun
			fu.SetIniInt("Messager", "ReadableEncodedAsRun", m_bReadableEncodedAsRun?1:0);			// hex encode for readability, otherwise base 64 encoded


			if(!(fu.SetSettings(theOppApp.m_szSettingsFilename, false)&FILEUTIL_MALLOC_OK)) return CLIENT_ERROR;
		}

		return CLIENT_SUCCESS;
	}
//	else AfxMessageBox("failed!");
	return CLIENT_ERROR;
}




int CStreamDataSettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	
	if((Settings(true)==TABULATOR_SUCCESS)&&(m_pszSettings)&&(strlen(m_pszSettings)>0))
	{
//	theOppApp.MB("in gfdb");

		CDBUtil  db;
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];
		strcpy(errorstring, "");

//	theOppApp.MB("in gfdb cnc with %s, %s, %s", m_pszDSN, m_pszUser, m_pszPW);
		CDBconn* pdbConn = db.CreateNewConnection(m_pszDSN, m_pszUser, m_pszPW);
//	theOppApp.MB("in gfdb cnc returned %d", pdbConn );
		if(pdbConn)
		{
//	theOppApp.MB("connecting" );
			if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
			{
				if(g_ptabmsgr)
				{
					char pszSource[MAX_PATH];
					_snprintf(pszSource, MAX_PATH-1, "%s_Settings:database_connect", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");

					g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, pszSource, errorstring);  //(Dispatch message)
				}
				if(pszInfo)
				{
					_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s", errorstring);
				}
			}
			else
			{
//	theOppApp.MB("retrieving" );
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
				CRecordset* prs = db.Retrieve(pdbConn, szSQL, pszInfo);
//	theOppApp.MB("retrieved %d", prs );
				if(prs)
				{

					int nEnableTones = -1; // not found
					int nEnableSCTE104 = -1;  // not found

					int nReturn = TABULATOR_ERROR;
					int nIndex = 0;
					while ((!prs->IsEOF()))
					{
						CString szCategory="";
						CString szParameter="";
						CString szValue="";
						CString szTemp="";
						int min, max;
						bool bmin = false, bmax = false;
						try
						{
							prs->GetFieldValue("category", szCategory);  //HARDCODE
							prs->GetFieldValue("parameter", szParameter);  //HARDCODE
							prs->GetFieldValue("value", szValue);  //HARDCODE
							prs->GetFieldValue("min_value", szTemp);  //HARDCODE
							if(szTemp.GetLength())
							{
								min = atoi(szTemp);
								bmin = true;
							}
							prs->GetFieldValue("max_value", szTemp);  //HARDCODE
							if(szTemp.GetLength())
							{
								max = atoi(szTemp);
								bmax = true;
							}
						}
						catch( ... )
						{
						}

	//		theOppApp.MB("Setting: %s:%s = %s", szCategory, szParameter, szValue);


						szValue.TrimLeft();szValue.TrimRight();
						int nLength = szValue.GetLength();



						if(szCategory.CompareNoCase("GlobalConfiguration")==0)
						{
							if(szParameter.CompareNoCase("EnableTones")==0)
							{
								if(nLength>0)
								{
									nEnableTones = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?1:0;
								}
							}
							else
							if(szParameter.CompareNoCase("EnableSCTE104")==0)
							{
								if(nLength>0)
								{
									nEnableSCTE104 = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?1:0;
								}
							}
						}
						else
						



/*
						if(szCategory.CompareNoCase("Main")==0)
						{
							if(szParameter.CompareNoCase("Name")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszName) free(m_pszName);
										m_pszName = pch;
									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("License")==0)
						{
							if(szParameter.CompareNoCase("Key")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszLicense) free(m_pszLicense);
										m_pszLicense = pch;

										// recompile license key params
										if(g_ptabulator->m_data.m_key.m_pszLicenseString) free(g_ptabulator->m_data.m_key.m_pszLicenseString);
										g_ptabulator->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
										if(g_ptabulator->m_data.m_key.m_pszLicenseString)
										sprintf(g_ptabulator->m_data.m_key.m_pszLicenseString, "%s", szValue);

										g_ptabulator->m_data.m_key.InterpretKey();

										char errorstring[MAX_MESSAGE_LENGTH];
										if(g_ptabulator->m_data.m_key.m_bValid)
										{
											unsigned long i=0;
											while(i<g_ptabulator->m_data.m_key.m_ulNumParams)
											{
												if((g_ptabulator->m_data.m_key.m_ppszParams)
													&&(g_ptabulator->m_data.m_key.m_ppszValues)
													&&(g_ptabulator->m_data.m_key.m_ppszParams[i])
													&&(g_ptabulator->m_data.m_key.m_ppszValues[i]))
												{
													if(stricmp(g_ptabulator->m_data.m_key.m_ppszParams[i], "max")==0)
													{
		//												g_ptabulator->m_data.m_nMaxLicensedDevices = atoi(g_ptabulator->m_data.m_key.m_ppszValues[i]);
													}
												}
												i++;
											}
										
											if(
													(
														(!g_ptabulator->m_data.m_key.m_bExpires)
													||((g_ptabulator->m_data.m_key.m_bExpires)&&(!g_ptabulator->m_data.m_key.m_bExpired))
													||((g_ptabulator->m_data.m_key.m_bExpires)&&(g_ptabulator->m_data.m_key.m_bExpireForgiveness)&&(g_ptabulator->m_data.m_key.m_ulExpiryDate+g_ptabulator->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
													)
												&&(
														(!g_ptabulator->m_data.m_key.m_bMachineSpecific)
													||((g_ptabulator->m_data.m_key.m_bMachineSpecific)&&(g_ptabulator->m_data.m_key.m_bValidMAC))
													)
												)
											{
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
												g_ptabulator->m_data.SetStatusText(errorstring, TABULATOR_STATUS_OK);
											}
											else
											{
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
												g_ptabulator->m_data.SetStatusText(errorstring, TABULATOR_STATUS_ERROR);
											}
										}
										else
										{
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
											g_ptabulator->m_data.SetStatusText(errorstring, TABULATOR_STATUS_ERROR);
										}

									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("CommandServer")==0)
						{
							if(szParameter.CompareNoCase("ListenPort")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
									{
										m_usCommandPort = nLength;
									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("StatusServer")==0)
						{
							if(szParameter.CompareNoCase("ListenPort")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
									{
										m_usStatusPort = nLength;
									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("Messager")==0)
						{
							if(szParameter.CompareNoCase("UseEmail")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bUseEmail = true;
									else m_bUseEmail = false;
								}
							}
							else
							if(szParameter.CompareNoCase("UseNet")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bUseNetwork = true;
									else m_bUseNetwork = false;
								}
							}
							else
							if(szParameter.CompareNoCase("UseLog")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bUseLog = true;
									else m_bUseLog = false;
								}
							}
							else
							if(szParameter.CompareNoCase("ReportSuccessfulOperation")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bReportSuccessfulOperation = true;
									else m_bReportSuccessfulOperation = false;
								}
							}
						}
						else
		/*
						if(szCategory.CompareNoCase("FileHandling")==0)
						{
							// we are not going to allow DSN params to change via the DB.
							if(szParameter.CompareNoCase("DeleteSourceFileOnTransfer")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bDeleteSourceFileOnTransfer = true;
									else m_bDeleteSourceFileOnTransfer = false;
								}
							}
						}
						else
		*/

						if(szCategory.CompareNoCase("Output")==0)
						{

/*
							if(szParameter.CompareNoCase("ProgramStreamOffset")==0)
							{
								if(nLength>0)
								{
									m_nProgramStreamOffset = atoi(szValue);
									if(m_nProgramStreamOffset<0) m_nProgramStreamOffset=0;
									if(m_nProgramStreamOffset>30) m_nProgramStreamOffset=30;
								}
							}
							else
*/

							if(szParameter.CompareNoCase("DrivenHost")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszDrivenHost)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszDrivenHost) free(m_pszDrivenHost);
										m_pszDrivenHost = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("DrivenModule")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszModule)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszModule) free(m_pszModule);
										m_pszModule = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("DrivenQueue")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszQueue)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszQueue) free(m_pszQueue);
										m_pszQueue = pch;
									}
								}
							}


							else
			

							if(szParameter.CompareNoCase("NationalEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104NationalEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bNationalBreakEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104NationalEvent) free(m_pszSCTE104NationalEvent);
										m_pszSCTE104NationalEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("LocalEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104LocalEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bLocalBreakEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104LocalEvent) free(m_pszSCTE104LocalEvent);
										m_pszSCTE104LocalEvent = pch;
									}
								}
							}
//							else
/*
							if(szParameter.CompareNoCase("Local060EventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104Local060Event)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bLocal060BreakEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104Local060Event) free(m_pszSCTE104Local060Event);
										m_pszSCTE104Local060Event = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("Local120EventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104Local120Event)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bLocal120BreakEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104Local120Event) free(m_pszSCTE104Local120Event);
										m_pszSCTE104Local120Event = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("BreakEndEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104BreakEndEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bBreakEndEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104BreakEndEvent) free(m_pszSCTE104BreakEndEvent);
										m_pszSCTE104BreakEndEvent = pch;
									}
								}
							}

							else

*/

/*
							if(szParameter.CompareNoCase("SCTE104NationalDTMF")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104NationalEventDTMFsequence)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104NationalEventDTMFsequence) free(m_pszSCTE104NationalEventDTMFsequence);
										m_pszSCTE104NationalEventDTMFsequence = pch;

										CSCTE104MultiMessage* m_pscte104Msg = g_data.m_pscte104MsgNationalBreak;
										g_data.m_pscte104MsgNationalBreak = NULL;
										if(m_pscte104Msg) { try{ delete m_pscte104Msg;} catch(...){} }
									}
								}
							}

							else
							if(szParameter.CompareNoCase("SCTE104Local060DTMF")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104Local060EventDTMFsequence)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104Local060EventDTMFsequence) free(m_pszSCTE104Local060EventDTMFsequence);
										m_pszSCTE104Local060EventDTMFsequence = pch;

										CSCTE104MultiMessage* m_pscte104Msg = g_data.m_pscte104MsgLocal060Break;
										g_data.m_pscte104MsgLocal060Break = NULL;
										if(m_pscte104Msg) { try{ delete m_pscte104Msg;} catch(...){} }
									}
								}
							}

							else
							if(szParameter.CompareNoCase("SCTE104Local120DTMF")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104Local120EventDTMFsequence)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104Local120EventDTMFsequence) free(m_pszSCTE104Local120EventDTMFsequence);
										m_pszSCTE104Local120EventDTMFsequence = pch;

										CSCTE104MultiMessage* m_pscte104Msg = g_data.m_pscte104MsgLocal120Break;
										g_data.m_pscte104MsgLocal120Break = NULL;
										if(m_pscte104Msg) { try{ delete m_pscte104Msg;} catch(...){} }
									}
								}
							}
							else
							if(szParameter.CompareNoCase("SCTE104BreakEndDTMF")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104BreakEndEventDTMFsequence)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104BreakEndEventDTMFsequence) free(m_pszSCTE104BreakEndEventDTMFsequence);
										m_pszSCTE104BreakEndEventDTMFsequence = pch;

										CSCTE104MultiMessage* m_pscte104Msg = g_data.m_pscte104MsgBreakEnd;
										g_data.m_pscte104MsgBreakEnd = NULL;
										if(m_pscte104Msg) { try{ delete m_pscte104Msg;} catch(...){} }
									}
								}
							}
							else
*/
/*
							if(szParameter.CompareNoCase("NationalMinimumDurationMS")==0)
							{
								if(nLength>0)
								{
									m_nNationalMinimumDurationMS = atoi(szValue);
								}
							}
*/

/*
							else

							if(szParameter.CompareNoCase("ReplaceAdEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszReplaceAdEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bReplaceAdEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszReplaceAdEvent) free(m_pszReplaceAdEvent);
										m_pszReplaceAdEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("ReplaceContentEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszReplaceContentEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bReplaceContentEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszReplaceContentEvent) free(m_pszReplaceContentEvent);
										m_pszReplaceContentEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("EventNotificationEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszEventNotificationEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bEventNotificationEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszEventNotificationEvent) free(m_pszEventNotificationEvent);
										m_pszEventNotificationEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("RejoinMainEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszRejoinMainEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bRejoinMainEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszRejoinMainEvent) free(m_pszRejoinMainEvent);
										m_pszRejoinMainEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("TimerEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszTimerEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bTimerEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszTimerEvent) free(m_pszTimerEvent);
										m_pszTimerEvent = pch;
									}
								}
							}

*/
						}
						else

						if(szCategory.CompareNoCase("SCTE104Configuration")==0)
						{
							if(szParameter.CompareNoCase("NationalInpointID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104NationalInpointID)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104NationalInpointID) free(m_pszSCTE104NationalInpointID);
										m_pszSCTE104NationalInpointID = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("NationalOutpointID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104NationalOutpointID)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104NationalOutpointID) free(m_pszSCTE104NationalOutpointID);
										m_pszSCTE104NationalOutpointID = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("LocalInpointID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104LocalInpointID)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104LocalInpointID) free(m_pszSCTE104LocalInpointID);
										m_pszSCTE104LocalInpointID = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("LocalOutpointID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104LocalOutpointID)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104LocalOutpointID) free(m_pszSCTE104LocalOutpointID);
										m_pszSCTE104LocalOutpointID = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("ProgramTag")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104ProgramTag)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104ProgramTag) free(m_pszSCTE104ProgramTag);
										m_pszSCTE104ProgramTag = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("NationalPreroll")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if((nLength<0)||(nLength>65535)) m_nSCTE104NationalPreroll=8000; //default to 8 sec
									else m_nSCTE104NationalPreroll = nLength;
								}
							}
							else
							if(szParameter.CompareNoCase("LocalPreroll")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if((nLength<0)||(nLength>65535)) m_nSCTE104LocalPreroll=8000; //default to 8 sec
									else m_nSCTE104LocalPreroll = nLength;
								}
							}
							else


							if(szParameter.CompareNoCase("RoundToNearestUnit")==0)
							{
								if(nLength>0)
								{
									m_bRoundToNearestUnit = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("DurationUnitDivisor")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength<0) m_nDurationUnitDivisor=1000; //default to seconds
									else m_nDurationUnitDivisor = nLength;
								}
							}

							else

/*
							if(szParameter.CompareNoCase("Local060ID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104Local060ID)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104Local060ID) free(m_pszSCTE104Local060ID);
										m_pszSCTE104Local060ID = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("Local120ID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104Local120ID)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104Local120ID) free(m_pszSCTE104Local120ID);
										m_pszSCTE104Local120ID = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("UseInpointSecTiming")==0)
							{
								if(nLength>0)
								{
									m_bUseNationalInpointSecTiming = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("UseOutpointSecTiming")==0)
							{
								if(nLength>0)
								{
									m_bUseNationalOutpointSecTiming = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("LocalHarrisTagSecEventType")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength<0) m_nLocalHarrisTagSecEventType=-1;
									else if(nLength>255) m_nLocalHarrisTagSecEventType=-1;
									else m_nLocalHarrisTagSecEventType = nLength;
								}
							}
							else
							if(szParameter.CompareNoCase("NationalHarrisTagSecEventType")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength<0) m_nNationalHarrisTagSecEventType=-1;
									else if(nLength>255) m_nNationalHarrisTagSecEventType=-1;
									else m_nNationalHarrisTagSecEventType = nLength;
								}
							}
							else
*/
							if(szParameter.CompareNoCase("NationalMinimumDurationMS")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength<0) m_nNationalMinimumDurationMS=-1;
									else m_nNationalMinimumDurationMS = nLength;
								}
							}
							else
							if(szParameter.CompareNoCase("LocalMinimumDurationMS")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength<0) m_nLocalMinimumDurationMS=-1;
									else m_nNationalMinimumDurationMS = nLength;
								}
							}
							else


								
							if(szParameter.CompareNoCase("UseDurationAddition")==0)
							{
								if(nLength>0)
								{
									m_bSCTE104UseDurationAddition = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}

						}
						else


/*
						if(szCategory.CompareNoCase("TonesLogging")==0)
						{
							if(szParameter.CompareNoCase("AsRunFileSpec")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszTonesAsRunFilename)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszTonesAsRunFilename) free(m_pszTonesAsRunFilename);
										m_pszTonesAsRunFilename = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunToken")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszTonesAsRunDestination)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);

										if(pch)
										{
											if(m_pszTonesAsRunDestination)
											{
												DoTonesAsRunDestinationName(m_pszTonesAsRunDestination, pch);
											}
											else	m_pszTonesAsRunDestination = pch;
										}
									}
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunFileLocation")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszTonesAsRunFileLocation)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszTonesAsRunFileLocation) free(m_pszTonesAsRunFileLocation);
										m_pszTonesAsRunFileLocation = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunFileFormat")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszTonesAsRunFileFormat)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszTonesAsRunFileFormat) free(m_pszTonesAsRunFileFormat);
										m_pszTonesAsRunFileFormat = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunLogOffsetInterval")==0)
							{
								if(nLength>0)
								{
									m_nTonesAsRunLogOffsetInterval = atoi(szValue);
									if((m_nTonesAsRunLogOffsetInterval < 0)||(m_nTonesAsRunLogOffsetInterval > 86400))
										m_nTonesAsRunLogOffsetInterval = 18000; //default to 5 am.
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunDTMF")==0)
							{
								if(nLength>0)
								{
									m_bAsRunDTMF = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunTestDTMF")==0)
							{
								if(nLength>0)
								{
									m_bAsRunTestDTMF = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}


						} 
						else
*/

						if(szCategory.CompareNoCase("SCTE104Logging")==0)
						{
							if(szParameter.CompareNoCase("AsRunFileSpec")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104AsRunFilename)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104AsRunFilename) free(m_pszSCTE104AsRunFilename);
										m_pszSCTE104AsRunFilename = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunToken")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104AsRunDestination)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);

										if(pch)
										{
											if(m_pszSCTE104AsRunDestination)
											{
												DoSCTE104AsRunDestinationName(m_pszSCTE104AsRunDestination, pch);
											}
											else	m_pszSCTE104AsRunDestination = pch;
										}
									}
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunFileLocation")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104AsRunFileLocation)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104AsRunFileLocation) free(m_pszSCTE104AsRunFileLocation);
										m_pszSCTE104AsRunFileLocation = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunFileFormat")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSCTE104AsRunFileFormat)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSCTE104AsRunFileFormat) free(m_pszSCTE104AsRunFileFormat);
										m_pszSCTE104AsRunFileFormat = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunLogOffsetInterval")==0)
							{
								if(nLength>0)
								{
									m_nSCTE104AsRunLogOffsetInterval = atoi(szValue);
									if((m_nSCTE104AsRunLogOffsetInterval < 0)||(m_nSCTE104AsRunLogOffsetInterval > 86400))
										m_nSCTE104AsRunLogOffsetInterval = 18000; //default to 5 am.
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunNationalBreak")==0)
							{
								if(nLength>0)
								{
									m_bAsRunNationalBreak = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunLocalBreak")==0)
							{
								if(nLength>0)
								{
									m_bAsRunLocalBreak = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
/*
							else
							if(szParameter.CompareNoCase("AsRunLocal120Break")==0)
							{
								if(nLength>0)
								{
									m_bAsRunLocal120Break = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunLocal060Break")==0)
							{
								if(nLength>0)
								{
									m_bAsRunLocal060Break = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunBreakEnd")==0)
							{
								if(nLength>0)
								{
									m_bAsRunBreakEnd = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
*/

						} 
						else
/*
						if(szCategory.CompareNoCase("TonesConfiguration")==0)
						{

							if(szParameter.CompareNoCase("InpointID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszInpointID)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszInpointID) free(m_pszInpointID);
										m_pszInpointID = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("OutpointID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszOutpointID)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszOutpointID) free(m_pszOutpointID);
										m_pszOutpointID = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("DTMFFormat")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszDTMFFormat)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszDTMFFormat) free(m_pszDTMFFormat);
										m_pszDTMFFormat = pch;
									}
								}
							}
							else


							if(szParameter.CompareNoCase("DTMFAddress")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszDTMFAddress?m_pszDTMFAddress:"")))  // because NULL is allowed on this one.
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszDTMFAddress) free(m_pszDTMFAddress);
										m_pszDTMFAddress = pch;

										if((m_pszDTMFAddress)&&(strlen(m_pszDTMFAddress)<7)) // minimum 0.0.0.0
										{
											free(m_pszDTMFAddress); m_pszDTMFAddress=NULL;
										}

										SetTonesIP(m_pszDTMFAddress);

									}
								}
								else
								if(nLength<=0)
								{
									if(m_pszDTMFAddress){ free(m_pszDTMFAddress); m_pszDTMFAddress=NULL; }
									SetTonesIP(NULL);
								}
							}
							else

							if(szParameter.CompareNoCase("UseInpointSecTiming")==0)
							{
								if(nLength>0)
								{
									m_bUseInpointSecTiming = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("UseOutpointSecTiming")==0)
							{
								if(nLength>0)
								{
									m_bUseOutpointSecTiming = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("RoundToNearestUnit")==0)
							{
								if(nLength>0)
								{
									m_bRoundToNearestUnit = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("HarrisTagSecEventType")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength<0) m_nHarrisTagSecEventType=-1;
									else if(nLength>255) m_nHarrisTagSecEventType=-1;
									else m_nHarrisTagSecEventType = nLength;
								}
							}
							else
							if(szParameter.CompareNoCase("DurationUnitDivisor")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength<0) m_nDurationUnitDivisor=1000; //default to seconds
									else m_nDurationUnitDivisor = nLength;
								}
							}
							else
							if(szParameter.CompareNoCase("DoTonesWithNationalBreak")==0)
							{
								if(nLength>0)
								{
									m_bDoNationalBreakTones = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("NationalBreakTonesDelayMS")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength<0) m_nNationalBreakTonesDelayMS=4000; //default to 4000 mseconds
									else m_nNationalBreakTonesDelayMS = nLength;
								}
							}
							else
							if(szParameter.CompareNoCase("UseDurationAddition")==0)
							{
								if(nLength>0)
								{
									m_bTonesUseDurationAddition = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
						}
						else
*/
/*						if(szCategory.CompareNoCase("Ticker")==0)
						{
							if(szParameter.CompareNoCase("GraphicsHost")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszDestinationHost)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszDestinationHost) free(m_pszDestinationHost);
										m_pszDestinationHost = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("GraphicsModule")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszModule)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszModule) free(m_pszModule);
										m_pszModule = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("GraphicsQueue")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszQueue)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszQueue) free(m_pszQueue);
										m_pszQueue = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("InEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszInEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bInEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszInEvent) free(m_pszInEvent);
										m_pszInEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("OutEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszOutEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bOutEventChanged = true;  // the event identifier of the transition out (Server_Ticker_Out) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszOutEvent) free(m_pszOutEvent);
										m_pszOutEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("TextEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszTextEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bTextEventChanged = true;  // the event identifier of the text message event has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszTextEvent) free(m_pszTextEvent);
										m_pszTextEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("PlayItemsOnce")==0)
							{
								if(nLength>0)
								{
									m_bTickerPlayOnce = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("DoneCount")==0)
							{
								if(nLength>0)
								{
									m_nDoneCount = atoi(szValue);
									if(m_nDoneCount<0) m_nDoneCount=0;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExpired")==0)
							{
								if(nLength>0)
								{
									m_bAutoPurgeExpired = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExceeded")==0)
							{
								if(nLength>0)
								{
									m_bAutoPurgeExceeded = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExpiredAfterMins")==0)
							{
								if(nLength>0)
								{
									m_nAutoPurgeExpiredAfterMins = atoi(szValue);
									if(m_nAutoPurgeExpiredAfterMins<0) m_nAutoPurgeExpiredAfterMins=0;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExceededAfterMins")==0)
							{
								if(nLength>0)
								{
									m_nAutoPurgeExceededAfterMins = atoi(szValue);
									if(m_nAutoPurgeExceededAfterMins<0) m_nAutoPurgeExceededAfterMins=0;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoStart")==0)
							{
								if(nLength>0)
								{
									m_bAutostartTicker = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("UseLocalTime")==0)
							{
								if(nLength>0)
								{
									m_bUseLocalTime = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("dwell_time")==0)
							{
								if(nLength>0)
								{
									m_nDefaultDwellTime = atoi(szValue);
									if(m_nDefaultDwellTime<0) m_nDefaultDwellTime=10000; //default
								}
							}
						}
						else
						if(szCategory.CompareNoCase("DataDownload")==0)
						{
							if(szParameter.CompareNoCase("AutoStart")==0)
							{
								if(nLength>0)
								{
									m_bAutostartData = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
						}
						else
						if(szCategory.CompareNoCase("default_value")==0)
						{
							if(szParameter.CompareNoCase("dwell_time")==0)
							{
								if(nLength>0)
								{
									m_nDefaultDwellTime = atoi(szValue);
									if(m_nDefaultDwellTime<0) m_nDefaultDwellTime=10000; //default
								}
							}
						}
						else

*/

						if(szCategory.CompareNoCase("Automation")==0)
						{
							// we are not going to allow DSN params to change via the DB.
/*
							if(szParameter.CompareNoCase("AutomationServer")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszAutoServer)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszAutoServer) free(m_pszAutoServer);
										m_pszAutoServer = pch;
									}
								}
							}
							else
*/
							if(szParameter.CompareNoCase("AutomationAnalysisLookahead")==0)
							{
								if(nLength>0)
								{
									m_nAutomationAnalysisLookahead = atoi(szValue);// no validation, -1 turns it off.
								}
							}
							else
							if(szParameter.CompareNoCase("AutomationListNumber")==0)
							{
								if(nLength>0)
								{
									m_nAutoServerListNumber = atoi(szValue); // no validation
								}
							}

						}
						else

						if(szCategory.CompareNoCase("Database")==0)
						{
							// we are not going to allow DSN params to change via the DB.
							if(szParameter.CompareNoCase("SettingsTableName")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSettings)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSettings) free(m_pszSettings);
										m_pszSettings = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunTableName")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszAsRun)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszAsRun) free(m_pszAsRun);
										m_pszAsRun = pch;
									}
								}
							}
		
/*
							else
							if(szParameter.CompareNoCase("MessagesTableName")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszMessages)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszMessages) free(m_pszMessages);
										m_pszMessages = pch;
									}
								}
							}
/*
							else							if(szParameter.CompareNoCase("QueueTableName")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszQueue) free(m_pszQueue);
										m_pszQueue = pch;
									}
								}
							}
		/*
							else
							if(szParameter.CompareNoCase("ConnectionsTableName")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszConnections) free(m_pszConnections);
										m_pszConnections = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("LiveEventsTableName")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszLiveEvents) free(m_pszLiveEvents);
										m_pszLiveEvents = pch;
									}
								}
							}
		*/
						}


						nIndex++;
						prs->MoveNext();
					}
					prs->Close();



					if(pszInfo)
					{
						_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
					}
					delete prs;
					prs = NULL;

//	AfxMessageBox("in gfdb do");


					// do this after all things are obtained.
//					DoTonesAsRunFilename();
					DoSCTE104AsRunFilename();



					bool bGlobalConfigChangeTones = false;
					bool bGlobalConfigChangeSCTE = false;

					if(nEnableTones>=0)
					{
						if(
							  ((nEnableTones==0)&&(m_bEnableTones))
							||((nEnableTones==1)&&(!m_bEnableTones))
							)
						{
							bGlobalConfigChangeTones = true;
							m_bEnableTones = !m_bEnableTones; //temp, to write the new setting in.
						}
					}
					if(nEnableSCTE104>=0)
					{
						if(
							  ((nEnableSCTE104==0)&&(m_bEnableSCTE104))
							||((nEnableSCTE104==1)&&(!m_bEnableSCTE104))
							)
						{
							bGlobalConfigChangeSCTE = true;
							m_bEnableSCTE104 = !m_bEnableSCTE104; //temp, to write the new setting in.
						}
					}

					Settings(false); //write

					if((bGlobalConfigChangeTones)||(bGlobalConfigChangeSCTE))
					{
						if(bGlobalConfigChangeTones) { m_bEnableTones =! m_bEnableTones; }
						if(bGlobalConfigChangeSCTE) { m_bEnableSCTE104 =! m_bEnableSCTE104; }
						Settings(true); //read again, sets the as run tokens etc
 					}
//	theOppApp.MB("in gfdb return");

					return TABULATOR_SUCCESS;
				}
			}
		}
		else
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: Connection pointer was NULL.");
		}

	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. "

				);
		}
	}
	return TABULATOR_ERROR;
}

/*
int CStreamDataSettings::DoTonesAsRunFilename()
{
	int nLen = strlen(m_pszTonesAsRunFileLocation);
	if((m_pszTonesAsRunFileLocation)&&(nLen>0))
	{
		char buffer[MAX_PATH];

		nLen--;
		buffer[0] = *(m_pszTonesAsRunFileLocation+nLen);
		while( ((buffer[0]=='\\')||(buffer[0]=='/')) && (nLen>=0))
		{
			*(m_pszTonesAsRunFileLocation+nLen) = 0;
			nLen--;
			if(nLen<0) break;
			buffer[0] = *(m_pszTonesAsRunFileLocation+nLen);
		}

		nLen = strlen(m_pszTonesAsRunFileLocation);

		if(nLen>0)
		{
			sprintf(buffer, "Logs\\SerialTones\\T|YD0101%02d%02d|%s\\%s|1|1", 
				(m_nTonesAsRunLogOffsetInterval/3600),
				(m_nTonesAsRunLogOffsetInterval%3600)/60,
				m_pszTonesAsRunFileLocation, 
				((m_pszTonesAsRunFileFormat)&&(strlen(m_pszTonesAsRunFileFormat)>0))?m_pszTonesAsRunFileFormat:"T%m%d%y.log"
				);

			if((m_pszTonesAsRunFilename==NULL)||(strcmp(m_pszTonesAsRunFilename,buffer)!=0))
			{
				char* pch = (char*) malloc (strlen(buffer)+1);
				if(pch)
				{
					strcpy(pch, buffer);
					if(m_pszTonesAsRunFilename) free(m_pszTonesAsRunFilename);
					m_pszTonesAsRunFilename = pch;


					if(g_ptabmsgr) g_ptabmsgr->ModifyDestination(
							(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
							((g_settings.m_pszTonesAsRunFilename!=NULL)?g_settings.m_pszTonesAsRunFilename:"Logs\\SerialTones\\T|YD01010500|C:\\Cortex\\Logs\\SerialTones\\T%m%d%y.log|1|1"),
							MSG_DESTTYPE_LOG
							);


					return TABULATOR_SUCCESS;
				}
			}
		}
	}

	return TABULATOR_ERROR;
}


int CStreamDataSettings::DoTonesAsRunDestinationName(char* pchOldName, char* pchNewName)
{
	if((g_ptabmsgr)&&(m_bEnableTones))
	{
		if(
			  ((pchOldName)&&(pchNewName)&&(strcmp(pchNewName,pchOldName)))
			||((pchOldName==NULL)&&(pchNewName)&&(strlen(pchNewName)>0))
			)
		{
			if((pchOldName)&&(strlen(pchOldName)>0))
			{
				g_ptabmsgr->RemoveDestination( pchOldName );
			}

			g_ptabmsgr->AddDestination(
											MSG_DESTTYPE_LOG, 
											(((pchNewName)&&(strlen(pchNewName)))?pchNewName:"serialtones_asrun"), 
											((g_settings.m_pszTonesAsRunFilename!=NULL)?g_settings.m_pszTonesAsRunFilename:"Logs\\SerialTones\\T|YD01010500|C:\\Cortex\\Logs\\SerialTones\\T%m%d%y|1|1")
											);
					return TABULATOR_SUCCESS;
		}
	}

	if((pchOldName)&&(pchNewName)&&(strcmp(pchNewName,pchOldName)==0))
	{
		free(pchNewName);
	}
	else
	{
		if(m_pszTonesAsRunDestination) free(m_pszTonesAsRunDestination);
		m_pszTonesAsRunDestination = pchNewName;
	}
	return TABULATOR_ERROR;
}
*/

int CStreamDataSettings::DoSCTE104AsRunFilename()
{
	int nLen = strlen(m_pszSCTE104AsRunFileLocation);
	if((m_pszSCTE104AsRunFileLocation)&&(nLen>0))
	{
		char buffer[MAX_PATH];

		nLen--;
		buffer[0] = *(m_pszSCTE104AsRunFileLocation+nLen);
		while( ((buffer[0]=='\\')||(buffer[0]=='/')) && (nLen>=0))
		{
			*(m_pszSCTE104AsRunFileLocation+nLen) = 0;
			nLen--;
			if(nLen<0) break;
			buffer[0] = *(m_pszSCTE104AsRunFileLocation+nLen);
		}

		nLen = strlen(m_pszSCTE104AsRunFileLocation);

		if(nLen>0)
		{
			sprintf(buffer, "Logs\\StreamData\\S|YD0101%02d%02d|%s\\%s|1|1", 
				(m_nSCTE104AsRunLogOffsetInterval/3600),
				(m_nSCTE104AsRunLogOffsetInterval%3600)/60,
				m_pszSCTE104AsRunFileLocation, 
				((m_pszSCTE104AsRunFileFormat)&&(strlen(m_pszSCTE104AsRunFileFormat)>0))?m_pszSCTE104AsRunFileFormat:"S%m%d%y.log"
				);

			if((m_pszSCTE104AsRunFilename==NULL)||(strcmp(m_pszSCTE104AsRunFilename,buffer)!=0))
			{
				char* pch = (char*) malloc (strlen(buffer)+1);
				if(pch)
				{
					strcpy(pch, buffer);
					if(m_pszSCTE104AsRunFilename) free(m_pszSCTE104AsRunFilename);
					m_pszSCTE104AsRunFilename = pch;


					if(g_ptabmsgr) g_ptabmsgr->ModifyDestination(
							(((g_settings.m_pszSCTE104AsRunDestination)&&(strlen(g_settings.m_pszSCTE104AsRunDestination)))?g_settings.m_pszSCTE104AsRunDestination:"SCTE104_asrun"), 
							((g_settings.m_pszSCTE104AsRunFilename!=NULL)?g_settings.m_pszSCTE104AsRunFilename:"Logs\\StreamData\\S|YD01010500|C:\\Cortex\\Logs\\StreamData\\S%m%d%y.log|1|1"),
							MSG_DESTTYPE_LOG
							);


					return TABULATOR_SUCCESS;
				}
			}
		}
	}

	return TABULATOR_ERROR;
}

int CStreamDataSettings::DoSCTE104AsRunDestinationName(char* pchOldName, char* pchNewName)
{
	if((g_ptabmsgr)&&(m_bEnableSCTE104))
	{
		if(
			  ((pchOldName)&&(pchNewName)&&(strcmp(pchNewName,pchOldName)))
			||((pchOldName==NULL)&&(pchNewName)&&(strlen(pchNewName)>0))
			)
		{
			if((pchOldName)&&(strlen(pchOldName)>0))
			{
				g_ptabmsgr->RemoveDestination( pchOldName );
			}

			g_ptabmsgr->AddDestination(
											MSG_DESTTYPE_LOG, 
											(((pchNewName)&&(strlen(pchNewName)))?pchNewName:"SCTE104_asrun"), 
											((g_settings.m_pszSCTE104AsRunFilename!=NULL)?g_settings.m_pszSCTE104AsRunFilename:"Logs\\StreamData\\S|YD01010500|C:\\Cortex\\Logs\\StreamData\\S%m%d%y|1|1")
											);
					return TABULATOR_SUCCESS;
		}
	}

	if((pchOldName)&&(pchNewName)&&(strcmp(pchNewName,pchOldName)==0))
	{
		free(pchNewName);
	}
	else
	{
		if(m_pszSCTE104AsRunDestination) free(m_pszSCTE104AsRunDestination);
		m_pszSCTE104AsRunDestination = pchNewName;
	}
	return TABULATOR_ERROR;
}

/*

int CStreamDataSettings::SetTonesIP(char* pchIP)
{
	if(pchIP)
	{
//			theOppApp.MB("SetTonesIP %s", pchIP );

		if(strlen(pchIP)>=7) // incoming string like "192.168.0.5" , min is 0.0.0.0
		{
			CSafeBufferUtil sbu;
			char* pch = sbu.Token(pchIP, strlen(pchIP), ".", MODE_SINGLEDELIM);
			int i=0;
			while((pch)&&(strlen(pch)>0)&&(i<4))
			{
				m_ucDTMFAddress[i]=(unsigned char)atol(pch);
				pch = sbu.Token(NULL, NULL, ".", MODE_SINGLEDELIM);

				i++;
			}
			if(i<4)  // some error.
			{
				m_ucDTMFAddress[0]=0;
				m_ucDTMFAddress[1]=0;
				m_ucDTMFAddress[2]=0;
				m_ucDTMFAddress[3]=0;

				return TABULATOR_ERROR;
			}
			return TABULATOR_SUCCESS;
		}

	}
	else
	{
		m_ucDTMFAddress[0]=0;
		m_ucDTMFAddress[1]=0;
		m_ucDTMFAddress[2]=0;
		m_ucDTMFAddress[3]=0;
		return TABULATOR_SUCCESS;
	}

	return TABULATOR_ERROR;
}
*/

