; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CLibrettoParseDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "LibrettoParse.h"

ClassCount=3
Class1=CLibrettoParseApp
Class2=CLibrettoParseDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_LIBRETTOPARSE_DIALOG

[CLS:CLibrettoParseApp]
Type=0
HeaderFile=LibrettoParse.h
ImplementationFile=LibrettoParse.cpp
Filter=N

[CLS:CLibrettoParseDlg]
Type=0
HeaderFile=LibrettoParseDlg.h
ImplementationFile=LibrettoParseDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_CHECK_SUPPRESS_RPT

[CLS:CAboutDlg]
Type=0
HeaderFile=LibrettoParseDlg.h
ImplementationFile=LibrettoParseDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_LIBRETTOPARSE_DIALOG]
Type=1
Class=CLibrettoParseDlg
ControlCount=12
Control1=IDOK,button,1073807361
Control2=IDCANCEL,button,1073807360
Control3=IDC_BUTTON_SELECT,button,1342242816
Control4=IDC_LIST1,SysListView32,1350664205
Control5=IDC_EDIT1,edit,1350633668
Control6=IDC_CHECK1,button,1342242819
Control7=IDC_EDIT2,edit,1350633668
Control8=IDC_EDIT3,edit,1350633668
Control9=IDC_BUTTON_OUTFILE,button,1342242816
Control10=IDC_RADIO_TXT,button,1342308361
Control11=IDC_RADIO_CSV,button,1342177289
Control12=IDC_CHECK_SUPPRESS_RPT,button,1342242819

