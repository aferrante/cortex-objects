//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by StreamData.rc
//
#define IDD_DIALOG_MAIN                 129
#define IDB_BITMAP_STDICONS             129
#define IDD_DIALOG_SETTINGS             130
#define IDD_DIALOG_OUTPUT               131
#define IDB_BITMAP_STATUSYEL            132
#define IDB_BITMAP_STATUSBLU            133
#define IDB_SETTINGS                    135
#define IDB_BN_CHK                      136
#define IDB_BN_X                        137
#define IDB_BN_CLR                      138
#define IDB_BITMAP_STATUSRED            143
#define IDB_BITMAP_STATUSGRN            144
#define IDC_CURSOR_COMMIT               145
#define IDC_CURSOR_UNCOMMIT             146
#define IDB_BITMAP_RELOAD               147
#define IDB_BITMAP_DRICONS              158
#define IDR_MENU1                       158
#define IDB_BITMAP_STACKICONS           159
#define IDB_BN_INSERT                   160
#define IDB_BN_REMOVE                   161
#define IDD_DIALOG_TESTFEED             164
#define IDB_STATEICONS                  166
#define IDB_BN_Q                        167
#define IDB_BN_GREAT                    168
#define IDB_BN_COMMITTED                169
#define IDB_BN_PLAYED                   170
#define IDB_BN_LENGTH                   171
#define IDB_BN_AGE                      172
#define IDC_EDIT_DESC                   1000
#define IDC_STATIC_TEXT                 1001
#define IDC_BUTTON_TICKER               1002
#define IDC_CHECK_BAD                   1004
#define IDC_CHECK_PLAYONCE              1005
#define IDC_CHECK_UNMARKED              1005
#define IDC_CHECK_GOOD                  1006
#define IDC_CHECK_AUTOCONNECT           1006
#define IDC_CHECK_GREAT                 1007
#define IDC_BUTTON_TESTFEED             1007
#define IDC_EDIT_BASEURL                1008
#define IDC_BUTTON_DATA                 1008
#define IDC_STATIC_URL                  1009
#define IDC_CHECK_PLAYED                1009
#define IDC_EDIT_PROGRAM                1010
#define IDC_CHECK_LENGTH                1010
#define IDC_STATIC_PROG                 1011
#define IDC_CHECK_COMMITTED             1011
#define IDC_EDIT_REQ                    1012
#define IDC_CHECK_DEL                   1012
#define IDC_STATIC_REQ                  1013
#define IDC_EDIT_POLL                   1014
#define IDC_STATIC_POLL                 1015
#define IDC_EDIT_BATCH                  1016
#define IDC_STATIC_BATCH                1017
#define IDC_BUTTON_CONNECT              1018
#define IDC_EDIT_HOST                   1019
#define IDC_EDIT_TIMESPLAYED            1021
#define IDC_CHECK_AGE                   1024
#define IDC_EDIT_NUMCHARS               1025
#define IDC_EDIT_MINS                   1026
#define IDC_CHECK_FILTER                1027
#define IDC_STATIC_INV                  1028
#define IDC_STATIC_CLIPRECT             1029
#define IDC_CHECK_SMUT                  1030
#define IDC_STATIC_DONE                 1031
#define IDC_CHECK_CYCLE                 1032
#define IDC_LIST1                       1034
#define IDC_LIST_PLAYSTACK              1051
#define IDC_RICHEDIT_VIEW               1052
#define IDC_LIST_DATASTACK              1053
#define IDC_STATIC_HWIPE                1054
#define IDC_RICHEDIT_TICKERSTATUS       1055
#define IDC_RICHEDIT_STATUS             1056
#define IDC_RICHEDIT_DOWNLOADSTATUS     1056
#define IDC_LIST_DEFAULTSTACK           1057
#define IDC_STATIC_VWIPE                1058
#define IDC_STATIC_HWIPE2               1059
#define IDC_BUTTON_COMMIT               1060
#define IDC_BUTTON_COMMIT2              1061
#define IDC_EDIT_C_PORT                 1062
#define IDC_EDIT_T_PORT                 1063
#define IDC_EDIT_DONE                   1064
#define IDC_BUTTON_START                1081
#define IDC_EDIT_FEEDURL                1085
#define IDC_CHECK_FILE                  1119
#define IDC_CHECK_TIME                  1120
#define IDC_COMBO_HOST                  1143
#define IDC_STATIC_TIME                 1145
#define ID_CMD_CLEARDB                  32771
#define ID_CMD_CLEARTICKER              32771
#define ID_CMD_CLEARCLIP                32772
#define ID_CMD_SETTINGSDLG              32772
#define ID_CMD_SNAPSHOT                 32773
#define ID_CMD_REFRESH                  32773
#define ID_CMD_CLEARMARKS               32774
#define ID_CMD_CLEARFEED                32774
#define ID_CMD_TOGGLEACTIVE             32775
#define ID_CMD_CLEARDEFAULT             32776

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32777
#define _APS_NEXT_CONTROL_VALUE         1036
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
