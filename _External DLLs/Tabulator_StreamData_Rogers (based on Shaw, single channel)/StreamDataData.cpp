// StreamDataData.cpp: implementation of the CStreamDataData and CStreamDataMessage classes.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "StreamData.h"
#include "StreamDataCore.h"
#include "StreamDataData.h"
#include "StreamDataSettings.h"
#include <atlbase.h>
#include <process.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CStreamDataCore	g_core;
extern CStreamDataData	g_data;
extern CStreamDataSettings	g_settings;
extern CMessager*			g_ptabmsgr;
extern DLLdata_t       g_dlldata;

#define SENDMSG_EVENT_MAXLEN 64
#define SENDMSG_SENDER_MAXLEN 64
#define SENDMSG_MESSAGE_MAXLEN 512


extern void ExecuteOpportunitiesThread(void* pvArgs);




//////////////////////////////////////////////////////////////////////
// CTabulatorEvent Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTabulatorEvent::CTabulatorEvent()
{
	m_ulFlags = TABULATOR_FLAG_DISABLED;
	m_dblTriggerTime = -1.0; 
	m_bAnalyzed = false;
	m_bReAnalyzed = false;
	m_nEventID = -1;	
	m_nAnalyzedTriggerID = -1;
	m_nType = -1;
	m_nDestType = -1;
}

CTabulatorEvent::~CTabulatorEvent()
{
}

CTabulatorEventArray::CTabulatorEventArray()
{
	m_nEvents = 0; 
	m_ppEvents = NULL;
	InitializeCriticalSection(&m_critEvents);
	m_nEventMod=0;
	m_nLastEventMod=-1;
}

CTabulatorEventArray::~CTabulatorEventArray()
{
	EnterCriticalSection(&m_critEvents);
	DeleteEvents();
	LeaveCriticalSection(&m_critEvents);
	DeleteCriticalSection(&m_critEvents);
}

void	CTabulatorEventArray::DeleteEvents()
{
	if((m_nEvents)&&(m_ppEvents))
	{
		int i=0;
		while(i<m_nEvents)
		{
			if(m_ppEvents[i]) delete m_ppEvents[i];
			m_ppEvents[i] = NULL;
			i++;
		}
		delete [] m_ppEvents;
	}
	m_ppEvents = NULL;
	m_nEvents = 0;
}


CAutomationList::CAutomationList()
{
	m_nState=-1;
	m_nChange=-1;
	m_nDisplay=-1;
	m_nSysChange=-1;
	m_nCount=-1;
	m_nLookahead=-1;
	m_dblLastUpdate = -1.0;
}

CAutomationList::~CAutomationList()
{
}


CAutomationEvent::CAutomationEvent()
{
 	m_nID = -1;
	m_usType = 0xffff;
	m_pszID = NULL;
	m_pszTitle = NULL;
	m_pszData = NULL;
	m_pszReconcileKey = NULL;
	m_ucSegment = 0xff;
	m_ulOnAirTimeMS = 0xffffffff;					// ideal milliseconds as appears in the list
	m_ulOnAirLocalUnixDate = 0xffffffff;  //in seconds, offset from January 1, 1900
	m_ulDurationMS = 0xffffffff;          // ideal milliseconds as appears in the list
	m_ulSOMMS = 0xffffffff;               // ideal milliseconds as appears in the list
	m_usStatus = 0xffff;
//	unsigned short m_usControl; // was this
	m_ulControl = 0xffffffff;  // made it this, storing revent->extrathree (which is extended event control in recfm80) in the upper word

	// potential output variables.
//	m_ulClassification = STREAMDATA_CLASS_TYPE_NONE;
	m_pszOutputString = NULL; // like:
	// Replace Ad BLOCK (for advertisments that are the first in the pod)
	// PASS (for not-ads that do not have replacements)
	// ReplaceContent <ID1> <ID2> BLOCK (for not-ads that do not have replacements)
	// PENDING (for items not analyzed yet)
	// Timer suppressed (for items suppressed by the timer - either type)
}


CAutomationEvent::~CAutomationEvent()
{
	if(m_pszID) free(m_pszID);
	if(m_pszTitle) free(m_pszTitle);
	if(m_pszReconcileKey) free(m_pszReconcileKey);
	if(m_pszOutputString) free(m_pszOutputString);
	if(m_pszData) free(m_pszData);
}



COutputVariables::COutputVariables()
{
	m_b_list_is_playing = false;  // automation list is playing a primary event.

	//NOTE!  three different meaings of the word "current" - be aware!

	// here, current means the currently playing primary event.
	m_n_current_primary_start=STREAMDATA_INVALID_TIMEADDRESS; // in ideal MS; 
	m_n_current_primary_duration=STREAMDATA_INVALID_TIMEADDRESS;  //in ideal MS 
	m_psz_current_primary_ID=NULL; // = Harris ID of current
	m_psz_current_primary_description=NULL;// = Harris Title of current

	m_n_current_systemtime=0; // in ideal MS; 
	m_n_current_servertime=0; // in ideal MS; 
	m_dbl_last_servertime=-1.0; // in ideal MS since epoch, local time; 
	m_dbl_last_servertime_update=-1.0; // in ideal MS since epoch; 

/*
	m_n_next_start_local_event = -1; // in ideal MS; -1 means not found
	m_n_next_end_local_event = -1; // in ideal MS;  -1 means not found

	m_n_next_local_interval_subtracted_duration=0; // in ideal MS - difference of on-air times.
	m_n_next_local_interval_accumulated_duration=0; // in ideal MS - accumulated durations. 

	m_psz_local_program_ID = NULL; // = PPL ID of local (given by start)

	m_n_next_start_national_event = -1; // in ideal MS; 
	m_n_next_end_national_event = -1; // in ideal MS; 

	m_n_next_national_interval_subtracted_duration=0; // in ideal MS - difference of on-air times.
	m_n_next_national_interval_accumulated_duration=0; // in ideal MS - accumulated durations.

	m_psz_national_program_ID = NULL; // = PPL ID of local (given by start)
*/

	m_n_next_start_splice_request=-1; // in ideal MS; 
	m_n_next_dur_splice_request=-1; // in ideal MS; 

}

COutputVariables::~COutputVariables()
{
//	if(m_psz_current_primary_ID) free(m_psz_current_primary_ID);
//	if(m_psz_current_primary_description) free(m_psz_current_primary_description);
//	if(m_psz_local_program_ID) free(m_psz_local_program_ID);
//	if(m_psz_national_program_ID) free(m_psz_national_program_ID);

	m_n_current_systemtime=0; // in ideal MS; 
	m_n_current_servertime=0; // in ideal MS; 
}


/*


CReplacementRule::CReplacementRule()
{
	m_nID=-1;
	m_pszProgramID=NULL;	
	m_pszReplacementID=NULL;

 	m_nSequence=-1;
	m_nDuration=-1; // in ideal MS, invalid to start
	m_nDurationInFrames = -1; // invalid
	m_nOutputDuration = 0; // nuttin

	m_dblValidDate = 0.0;
	m_dblExpireDate = 0.0;

	m_nOffset = 0; // time address 0
}

CReplacementRule::~CReplacementRule()
{
	if(m_pszReplacementID) free(m_pszReplacementID);
	if(m_pszProgramID) free(m_pszProgramID);
}


CDescriptionPattern::CDescriptionPattern()
{
	m_nID=-1;
	m_pszProgramID=NULL;
	m_pszPattern=NULL;

	m_dblValidDate = 0.0;
	m_dblExpireDate = 0.0;
}

CDescriptionPattern::~CDescriptionPattern()
{
	if(m_pszPattern) free(m_pszPattern);
	if(m_pszProgramID) free(m_pszProgramID);
}

CClassificationRule::CClassificationRule()
{
	m_nID=-1;
	m_pszPattern=NULL;
	m_pszColName=NULL;
	m_nEventType=STREAMDATA_CLASS_TYPE_NONE;
}

CClassificationRule::~CClassificationRule()
{
	if(m_pszPattern) free(m_pszPattern);
	if(m_pszColName) free(m_pszColName);
}
*/

/*
COpportuneIntervalData::COpportuneIntervalData()
{
	m_pszDescription = NULL;
	m_usType = STREAMDATA_UNKNOWN; // free or disallowed
	m_ulOffsetTimeMS=STREAMDATA_INVALID_TIMEADDRESS;				// ideal milliseconds as appears in the list
	m_ulDurationMS=STREAMDATA_INVALID_TIMEADDRESS;          // ideal milliseconds as appears in the list
}

COpportuneIntervalData::~COpportuneIntervalData()
{
	if(m_pszDescription) free(m_pszDescription);
}


COpportuneEvent::COpportuneEvent()
{
	m_nID=-1;
	m_pTabulatorEvents=NULL;
	m_pszEventToken=NULL;
	m_nDuration = -1; // invalid

	m_bEventTokenChanged = false;  // the event identifier token has changed

	m_dblValidDate  = 0.0;
	m_dblExpireDate = 0.0;

	m_dblLastPlayed = 0.0;
}

COpportuneEvent::~COpportuneEvent()
{
	if(m_pTabulatorEvents) delete m_pTabulatorEvents;
	if(m_pszEventToken) free(m_pszEventToken);
}


CExclusionRule::CExclusionRule()
{
	m_nID=-1;
	m_pszPattern=NULL;
	m_pszColName=NULL;
	m_nAutoEventType=-1;  // Automation event type, for matching start invalid
	m_nEventType=STREAMDATA_EXCLUSION_TYPE_UNKNOWN;  // internal event type, ON, OFF, or DURATION

	m_dblValidDate = 0.0;
	m_dblExpireDate = 0.0;
}

CExclusionRule::~CExclusionRule()
{
	if(m_pszPattern) free(m_pszPattern);
	if(m_pszColName) free(m_pszColName);
}
*/


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/*
CStreamDataMessage::CStreamDataMessage()
{
	m_szMessage="";
	m_szRevisedMessage="";
	m_nTimestamp =-1;
	m_nLastUpdate =-1;
	m_nID = -1;
	m_nStatus = StreamData_MSG_STATUS_NONE;
	m_nType = StreamData_MSG_TYPE_NONE;
	m_nPlayed = 0;
	m_dblIndex = -0.1;
}

CStreamDataMessage::~CStreamDataMessage()
{
}


CString CStreamDataMessage::SQLUpdate(bool bTicker, double dblSeekIndex)
{
  CString szSQL="";
	if(bTicker)
	{
	char* szMsg = g_data.m_dbu.EncodeQuotes(m_szMessage);
	if(m_nStatus&StreamData_MSG_STATUS_INSERTED)
	{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "upd item (%d [%s])", m_nID, m_szMessage); //(Dispatch message)
		szSQL.Format("UPDATE %s SET \
[text] = '%s', \
[sort_order] = %.4f, \
[timestamp] = %d, \
[updated] = %d, \
[status] = %d, \
[type] = %d, \
[played] = %d WHERE [id] = %d AND [sort_order] >= %.4f AND [sort_order] <= %.4f", 
					g_settings.m_pszTicker, 
					(szMsg?szMsg:m_szMessage),
					m_dblIndex,
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|StreamData_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed,
					m_nID,
					dblSeekIndex-0.0000009,
					dblSeekIndex+0.0000009
					);  // table name
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "upd item (%d [%s]) SQL %s", m_nID, m_szMessage, szSQL);//(Dispatch message)
	}
	else
	{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "ins item (%d [%s])", m_nID, m_szMessage); //(Dispatch message)
		szSQL.Format("INSERT INTO %s (\
[id], \
[text], \
[sort_order], \
[timestamp], \
[updated], \
[status], \
[type], \
[played]) VALUES \
(%d, '%s', %.4f, %d, %d, %d, %d, %d)",
					g_settings.m_pszTicker,  // table name
					m_nID,
					(szMsg?szMsg:m_szMessage),
					m_dblIndex,
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|StreamData_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "SQLUpdate", "ins item (%d [%s]) SQL %s", m_nID, m_szMessage, szSQL); //(Dispatch message)
	}
	if(szMsg) free(szMsg);
	}
	else
	{
	char* szMsg = g_data.m_dbu.EncodeQuotes(m_szMessage);
	char* szRevMsg = g_data.m_dbu.EncodeQuotes(m_szRevisedMessage);
	if(m_nStatus&StreamData_MSG_STATUS_INSERTED)
	{
		szSQL.Format("UPDATE %s SET \
[text] = '%s', \
[revision] = '%s', \
[timestamp] = %d, \
[updated] = %d, \
[status] = %d, \
[type] = %d, \
[played] = %d WHERE [id] = %d", g_settings.m_pszFeed, 
					(szMsg?szMsg:m_szMessage),
					(szRevMsg?szRevMsg:m_szRevisedMessage),
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|StreamData_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed,
					m_nID);  // table name
	}
	else
	{

		szSQL.Format("INSERT INTO %s (\
[id], \
[text], \
[revision], \
[timestamp], \
[updated], \
[status], \
[type], \
[played]) VALUES \
(%d, '%s', '%s', %d, %d, %d, %d, %d)",
					g_settings.m_pszFeed,  // table name
					m_nID,
					(szMsg?szMsg:m_szMessage),
					(szRevMsg?szRevMsg:m_szRevisedMessage),
					m_nTimestamp,m_nLastUpdate,
					m_nStatus|StreamData_MSG_STATUS_INSERTED,
					m_nType,
					m_nPlayed);
	}

	if(szMsg) free(szMsg);
	if(szRevMsg) free(szRevMsg);
	}

	return szSQL;
}

*/

/*
int CStreamDataItem::PublishItem(unsigned long ulFlags)
{
	if(( g_data.m_pdb )&&(	g_data.m_pdbConn))
	{

		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		if(ulFlags&STREAMDATA_UPDATE_COUNTONLY)
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET count = %d WHERE uid = %d",  //HARDCODE
					((g_settings.m_pszStreamData)&&(strlen(g_settings.m_pszStreamData)))?g_settings.m_pszStreamData:"StreamData",
					m_nCount,
					m_nItemID
					);
		}
		else
		{			
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d, duration = %d, start_tc = %d, stop_tc = %d, \
count = %d, paused_duration = %d, paused_count = %d, last_started = %.03f, last_stopped = %.03f WHERE uid = %d",  //HARDCODE
					((g_settings.m_pszStreamData)&&(strlen(g_settings.m_pszStreamData)))?g_settings.m_pszStreamData:"StreamData",
					m_ulStatus,

					m_ulDuration,
					((m_ulStartTC==0xffffffff)?-1:m_ulStartTC),
					((m_ulStopTC==0xffffffff)?-1:m_ulStopTC),

					m_nCount,
					m_ulPausedDuration,
					m_ulTimesPaused,
					
					(((double)(m_timeLastStarted.time)) + ((double)(m_timeLastStarted.millitm))/1000.0),
					(((double)(m_timeLastStopped.time)) + ((double)(m_timeLastStopped.millitm))/1000.0),

					m_nItemID
					);
		}
//			Sleep(1); // dont peg processor
if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_SQL)) 
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:update", "SQL: %s", szSQL); // Sleep(250); //(Dispatch message)

EnterCriticalSection(&g_data.m_critSQL);
		if(g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//**MSG
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "StreamData:update", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

		}
LeaveCriticalSection(&g_data.m_critSQL);

	}
	return CLIENT_ERROR;
}
*/


CMessageOverrideValues::CMessageOverrideValues()
{
	m_nPrerollMS=-1;
	m_nDurationMS=-1;
	m_pszIDString=NULL; //'N' or 'L' followed by pipe character ('|') followed by the "PPL Program ID", (For example "N|74915")
	m_uchSource = 0x00;
}

CMessageOverrideValues::~CMessageOverrideValues()
{
//	if(m_pszDTMF) free(m_pszDTMF);  //no, do not free this.  it's just a temp assignment.
}










//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CStreamDataData::CStreamDataData()
{
	m_bInitalEventGetSucceeded = false;
	m_socketTabulator = NULL;
	m_bInCommand = FALSE;

	m_bAutoAnalysisThreadStarted=false;
	m_bAutoAnalysisThreadKill=true;

	m_nAutomationProcessID = -1;
//	m_pCrawlEvents = new CTabulatorEventArray;


	m_ul_splice_event_id=0x00000000;  
	m_us_unique_program_id=0x0000; 


	m_nLastTime = -1;
	m_nLastMillitm = -1;
	m_nTimeIncr = 0;
	m_nMillitmIncr = 0;

//	m_bDoingOpp=false;
//	m_bDoingOppCancel=true;

//	m_bCrawlEventChanged=true;


	m_pEvents = NULL;
//	m_pInEvents = new CTabulatorEventArray;
//	m_pOutEvents = new CTabulatorEventArray;
//	m_pTextEvents = new CTabulatorEventArray;

//	m_socket=NULL;

	m_pdb=NULL;
	m_pdbConn=NULL;
	m_pdbOutput=NULL;
	m_pdbOutputConn=NULL;
	m_pdbAutomation=NULL;
	m_pdbAutomationConn=NULL;

	InitializeCriticalSection(&m_critSQL);
	InitializeCriticalSection(&m_critOutputSQL);
	

	InitializeCriticalSection(&m_critTimeCode);
	m_bTimeCodeThreadStarted = false;
	m_bTimeCodeThreadKill = true;

	m_ucTimeCodeBits = 0xff; //invalid value
	m_ulTimeCode = 0xffffffff; //invalid value
	m_ulTimeCodeElapsed = 0;

	InitializeCriticalSection(&m_critEventsSettings);

	m_nLastEventMod = -3;

//	m_bReplaceAdEventChanged=false;  // the event identifier of the replace ad has changed
//	m_bReplaceContentEventChanged=false;  // the event identifier of the replace content has changed
//	m_bEventNotificationEventChanged=false;  // the event identifier of the "BLOCK only" event has changed
//	m_bRejoinMainEventChanged=false;  // the event identifier of the PASS tabulator event (includes Rejoin Main) has changed
//	m_bTimerEventChanged=false;  // the event identifier of the BLOCK GPO w Timer event has changed

//	m_pReplaceAdEvents = new CTabulatorEventArray;
//	m_pReplaceContentEvents = new CTabulatorEventArray;
//	m_pEventNotificationEvents = new CTabulatorEventArray;
//	m_pRejoinMainEvents = new CTabulatorEventArray;
//	m_pTimerEvents = new CTabulatorEventArray;



	m_pSpliceRequestEvents = new CTabulatorEventArray;
//	m_pNationalBreakEvents = new CTabulatorEventArray;
//	m_pLocalBreakEvents = new CTabulatorEventArray;
//	m_pLocal060BreakEvents = new CTabulatorEventArray;
//	m_pLocal120BreakEvents = new CTabulatorEventArray;
//	m_pBreakEndEvents = new CTabulatorEventArray;

	m_pscte104MsgSpliceRequest = NULL;
//	m_pscte104MsgLocalBreak=NULL;
//	m_pscte104MsgNationalBreak=NULL;
//	m_pscte104MsgLocal060Break=NULL;
//	m_pscte104MsgLocal120Break=NULL;
//	m_pscte104MsgBreakEnd=NULL;

	m_bSpliceRequestEventChanged;
//	m_bLocalBreakEventChanged=false;
//	m_bNationalBreakEventChanged=false;
//	m_bLocal060BreakEventChanged=false;
//	m_bLocal120BreakEventChanged=false;
//	m_bBreakEndEventChanged=false;





//	m_ppStreamData=NULL;
//	m_nStreamData=0;


//	m_ppReplacementRules=NULL;
//	m_nReplacementRules=-1;  //uninit

//	m_ppDescriptionPatterns=NULL;
//	m_nDescriptionPatterns=-1;  //uninit

//	m_ppClassificationRules=NULL;
//	m_nClassificationRules=-1;  //uninit

//	m_nReplacementRulesArraySize = 0;
//	m_nDescriptionPatternsArraySize = 0;
//	m_nClassificationRulesArraySize = 0;

//	m_ppOpportuneEvents=NULL;
//	m_nOpportuneEvents=-1;
//	m_nOpportuneEventsArraySize=0;

//	m_ppExclusionRules=NULL;
//	m_nExclusionRules=-1;
//	m_nExclusionRulesArraySize=0;

	InitializeCriticalSection(&m_critTableData);

	InitializeCriticalSection(&m_critOutputVariables);
//	InitializeCriticalSection(&m_critSyncVariables);
	
	InitializeCriticalSection(&m_critAutomationEvents);
	InitializeCriticalSection(&m_critPlayEvent);  // need to enforce one at a time.


}

CStreamDataData::~CStreamDataData()
{
	m_bAutoAnalysisThreadKill=true;
	m_bTimeCodeThreadKill = true;

	while((m_bAutoAnalysisThreadStarted)||(m_bTimeCodeThreadStarted)) Sleep(10);


//	DeleteCriticalSection(&m_critClientSocket);
	DeleteCriticalSection(&m_critSQL);
	DeleteCriticalSection(&m_critOutputSQL);
//	if(	m_pInEvents) delete  m_pInEvents;
//	if(	m_pOutEvents) delete  m_pOutEvents;
//	if(	m_pTextEvents) delete  m_pTextEvents;
	DeleteCriticalSection(&m_critTimeCode);
	DeleteCriticalSection(&m_critEventsSettings);
//	if(	m_pReplaceAdEvents) delete  m_pReplaceAdEvents;
//	if(	m_pReplaceContentEvents) delete  m_pReplaceContentEvents;
//	if(	m_pEventNotificationEvents) delete  m_pEventNotificationEvents;
//	if(	m_pRejoinMainEvents) delete  m_pRejoinMainEvents;
//	if(	m_pTimerEvents) delete  m_pTimerEvents;

	if(	m_pSpliceRequestEvents) delete  m_pSpliceRequestEvents;

//	if(	m_pNationalBreakEvents) delete  m_pNationalBreakEvents;
//	if(	m_pLocalBreakEvents) delete  m_pLocalBreakEvents;
//	if(	m_pLocal060BreakEvents) delete  m_pLocal060BreakEvents;
//	if(	m_pLocal120BreakEvents) delete  m_pLocal120BreakEvents;
//	if(	m_pBreakEndEvents) delete  m_pBreakEndEvents;

	if(m_pscte104MsgSpliceRequest) delete  m_pscte104MsgSpliceRequest;

//	if(m_pscte104MsgNationalBreak) delete  m_pscte104MsgNationalBreak;
//	if(m_pscte104MsgLocalBreak) delete  m_pscte104MsgLocalBreak;
//	if(m_pscte104MsgLocal060Break) delete  m_pscte104MsgLocal060Break;
//	if(m_pscte104MsgLocal120Break) delete  m_pscte104MsgLocal120Break;
//	if(m_pscte104MsgBreakEnd) delete  m_pscte104MsgBreakEnd;

	EnterCriticalSection(&m_critTableData);
/*
	if((m_ppReplacementRules)&&(m_nReplacementRules>0))
	{
		int i=0;
		while(i<m_nReplacementRules)
		{
			if(m_ppReplacementRules[i])
			{
				delete m_ppReplacementRules[i];
			}
			i++;
		}
		delete [] m_ppReplacementRules;
	}
	m_ppReplacementRules=NULL;
	m_nReplacementRules=0; 

	if((m_ppDescriptionPatterns)&&(m_nDescriptionPatterns>0))
	{
		int i=0;
		while(i<m_nDescriptionPatterns)
		{
			if(m_ppDescriptionPatterns[i])
			{
				delete m_ppDescriptionPatterns[i];
			}
			i++;
		}
		delete [] m_ppDescriptionPatterns;
	}
	m_ppDescriptionPatterns=NULL;
	m_nDescriptionPatterns=0; 

	if((m_ppClassificationRules)&&(m_nClassificationRules>0))
	{
		int i=0;
		while(i<m_nClassificationRules)
		{
			if(m_ppClassificationRules[i])
			{
				delete m_ppClassificationRules[i];
			}
			i++;
		}
		delete [] m_ppClassificationRules;
	}
	m_ppClassificationRules=NULL;
	m_nClassificationRules=0; 
*/

/*
	if((m_ppOpportuneEvents)&&(m_nOpportuneEvents>0))
	{
		int i=0;
		while(i<m_nOpportuneEvents)
		{
			if(m_ppOpportuneEvents[i])
			{
				delete m_ppOpportuneEvents[i];
			}
			i++;
		}
		delete [] m_ppOpportuneEvents;
	}
	m_ppOpportuneEvents=NULL;
	m_nOpportuneEvents=0; 
	if((m_ppExclusionRules)&&(m_nExclusionRules>0))
	{
		int i=0;
		while(i<m_nExclusionRules)
		{
			if(m_ppExclusionRules[i])
			{
				delete m_ppExclusionRules[i];
			}
			i++;
		}
		delete [] m_ppExclusionRules;
	}
	m_ppExclusionRules=NULL;
	m_nExclusionRules=0; 

*/

	LeaveCriticalSection(&m_critTableData);

	DeleteCriticalSection(&m_critTableData);
	DeleteCriticalSection(&m_critOutputVariables);
//	DeleteCriticalSection(&m_critSyncVariables);
	DeleteCriticalSection(&m_critAutomationEvents);
	DeleteCriticalSection(&m_critPlayEvent);  // need to enforce one at a time.
}

/*
int CStreamDataData::Do_Opportunities()
{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "Do_Opportunities"); // Sleep(250); //(Dispatch message)

	if(m_bDoingOpp)
	{
		m_bDoingOppCancel = true; // kill anything up right now

		while(m_bDoingOpp)
		{
			Sleep(15);
		}

	}

	m_bDoingOppCancel = false;
//	m_bDoingOpp = true;


	if(_beginthread(ExecuteOpportunitiesThread, 0, (void*)(NULL))==-1)
	{
	//error.
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "Error starting serialtones execution thread");//   Sleep(250);//(Dispatch message)
	//**MSG
	}
	else
		{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "ExecuteOpportunitiesThread begun"); // Sleep(250); //(Dispatch message)

		}

	return -69;
}
*/

// remove pipes
CString CStreamDataData::Encode(CString szText)
{
  CString szOut = "";
  int i;
  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '|')
      szOut += "%:";
    else if (szText[i] == '%')
      szOut += "%%";
    else if ((szText[i] != '\n')&&(szText[i] != '\r'))  //strip returns while we are at it.
       szOut += (char) szText[i];
  }
  return szOut;
}

// put back pipes
CString CStreamDataData::Decode(CString szText)
{
  CString szOut = "";
  int i;

  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '%')
    {
      i++; if (i>=szText.GetLength()) break;
      if (szText[i] == '%')
      {
        szOut += "%";
      }
      else if (szText[i] == ':')
      {
        szOut += "|";
      }
    }
    else szOut += (char) szText[i];
  }
  return szOut;
}


CString CStreamDataData::XMLTextNodeValue(IXMLDOMNodePtr pNode)
{
	CString szText = _T("");
	if(pNode)
	{
		IXMLDOMNodeListPtr pValues = pNode->GetchildNodes();
		if (pValues != NULL)
		{
			IXMLDOMNodePtr pValue;
			while(pValue = pValues->nextNode())
			{
				DOMNodeType valnodeType;
				pValue->get_nodeType(&valnodeType);

				if(valnodeType == NODE_TEXT)
				{
USES_CONVERSION;
					szText = W2T(_bstr_t(pValue->GetnodeValue()));
					szText.TrimLeft();
					szText.TrimRight();
					return szText;
				}
			}
		}
	}
	return szText;
}



int CStreamDataData::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszMessages)&&(strlen(g_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		if(g_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime) \
VALUES ('%s', '%s', %d, %d.%03d)", // HARDCODE
				g_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				g_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				g_settings.m_pszMessages
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_SQL)) 
		{
			char pszSource[MAX_PATH];
			_snprintf(pszSource, MAX_PATH-1, "%s:send_msg", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
			g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "%s", szSQL );
		}

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
//			IncrementDatabaseMods(g_settings.m_pszMessages, dberrorstring);
		LeaveCriticalSection(&m_critSQL);
			return TABULATOR_SUCCESS;
		}
		else
		{
			if(g_ptabmsgr)
			{
				char pszSource[MAX_PATH];
				_snprintf(pszSource, MAX_PATH-1, "%s:send_msg", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
				g_ptabmsgr->DM(MSG_ICONERROR, NULL, pszSource, "Error executing SQL: %s", dberrorstring);//   Sleep(250);//(Dispatch message)
			}
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return TABULATOR_ERROR;
}
/*
int CStreamDataData::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszMessages)&&(strlen(g_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			g_settings.m_pszMessages,
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			g_settings.m_pszMessages
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
//			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring);
			return TABULATOR_SUCCESS;
		}
	}
	return TABULATOR_ERROR;
}
*/

int CStreamDataData::SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...)
{
 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszAsRun)&&(strlen(g_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		if((pszEventID)&&(strlen(pszEventID)>SENDMSG_EVENT_MAXLEN)) pszEventID[SENDMSG_EVENT_MAXLEN-1]=0;
		
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);
		char* pchEvent = m_pdb->EncodeQuotes(pszEventID);
		if(g_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, channelid, eventid) \
VALUES ('%s', '%s', %d, %d.%03d, %d, '%s')", // HARDCODE
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm,
				nChannelID,
				pchEvent?pchEvent:""
				);
		}
		else
		{

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log")
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
		if(pchEvent) free(pchEvent);


		if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_SQL)) 
		{
			char pszSource[MAX_PATH];
			_snprintf(pszSource, MAX_PATH-1, "%s:send_asrun_msg", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
			g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "%s", szSQL );
		}


//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
//			IncrementDatabaseMods(g_settings.m_pszAsRun, dberrorstring);
			return TABULATOR_SUCCESS;
		}
		else
		{
			if(g_ptabmsgr)
			{
				char pszSource[MAX_PATH];
				_snprintf(pszSource, MAX_PATH-1, "%s:send_asrun_msg", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
				g_ptabmsgr->DM(MSG_ICONERROR, NULL, pszSource, "Error executing SQL: %s", dberrorstring);//   Sleep(250);//(Dispatch message)
			}
		}
		LeaveCriticalSection(&m_critSQL);

	}
	return TABULATOR_ERROR;
}

/*
int CStreamDataData::SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszAsRun)&&(strlen(g_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log")
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "StreamData_Asrun", "SQL %s", szSQL) ;// Sleep(100); //(Dispatch message)

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
//			IncrementDatabaseMods(g_settings.m_pszAsRun, dberrorstring);
			return TABULATOR_SUCCESS;
		}
		else
		{
if(g_ptabmsgr)
{
	char szSource[MAX_PATH]; sprintf(szSource, "%s_Asrun", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "SQL error %s", dberrorstring) ;// Sleep(100); //(Dispatch message)
}
		}
		LeaveCriticalSection(&m_critSQL);

	}
	else
		{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "StreamData_Asrun", "null conditions");// Sleep(100); //(Dispatch message)
		}
	return TABULATOR_ERROR;
}
*/

int CStreamDataData::SendTabulatorCommand(unsigned char** ppucDataBuffer, char* pszAsRunEventID, bool bSuppressAsRun, char* pszDTMF, COutputVariables* pOv, char* pszInfo)
{
	if((m_socketTabulator)&&(ppucDataBuffer))
	{
		CNetData data;
		while(m_bInCommand) Sleep(1);
		m_bInCommand =TRUE;

		unsigned char* pucOrig = *ppucDataBuffer;
		unsigned char* puc = NULL;
		int nLen = strlen((char*)(*ppucDataBuffer));
		if((*ppucDataBuffer)&&(nLen)) puc = (unsigned char*)malloc(nLen+1);
		if(puc) memcpy(puc, *ppucDataBuffer, nLen+1); // includes term 0;
		data.m_ucCmd = TABULATOR_CMD_PLUGINCALL;
		data.m_pucData=puc;
		data.m_ulDataLen = (puc?nLen:0);
		data.m_ucType = ((NET_TYPE_PROTOCOL1|NET_TYPE_KEEPOPEN|NET_SND_KEEPOPENLCL)|(*ppucDataBuffer?NET_TYPE_HASDATA:0)); // has data but no subcommand.

		int n = m_net.SendData(&data, m_socketTabulator, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT);//|NET_SND_NO_RX);
		if(data.m_pucData!=NULL)
		{
			*ppucDataBuffer = data.m_pucData;  //return the buffer;
			data.m_pucData=NULL;  // null it so that it does not get freed on destructor
		}
		else
		{
			*ppucDataBuffer = NULL;
		}
		if(n < NET_SUCCESS)
		{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "StreamData_Tabulator_command", "Net error %d sending cmd 0x%02x buffer: %s", n, TABULATOR_CMD_PLUGINCALL, (*ppucDataBuffer)?((char*)(*ppucDataBuffer)):"");   //(Dispatch message)
			m_net.CloseConnection(m_socketTabulator);
			m_socketTabulator = NULL;
			m_bInCommand =FALSE;

			//ASRUN
if(!bSuppressAsRun)
{
	g_data.SendAsRunMsg(CX_SENDMSG_ERROR, g_settings.m_nAutoServerListNumber, pszAsRunEventID, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Error %d sending to %s at %s:%d with: %s", 
										n,
										g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
										g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
										g_settings.m_nTabulatorPort, 
										(char*)(*ppucDataBuffer));

/*
if(g_ptabmsgr)
{
	g_ptabmsgr->DM(MSG_ICONERROR, 
		(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
		pszAsRunEventID, "Net error %d sending cmd 0x%02x buffer: %s", 
		n, TABULATOR_CMD_PLUGINCALL, (*ppucDataBuffer)?((char*)(*ppucDataBuffer)):"");   //(Dispatch message)
}
*/

// asrun file message
// 12-22-2008 05:23:03.421: Sent [D100^M] (5 bytes) at server time 05:23:02.19, event hit time 05:22:58.06 (duration was calculated as 100 seconds) 

}

			if(pszInfo)
			{
				_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Net error %d sending cmd 0x%02x buffer: %s", n, TABULATOR_CMD_PLUGINCALL, (*ppucDataBuffer)?((char*)(*ppucDataBuffer)):"");   //(Dispatch message)
			}
		}
		else
		{
			m_net.SendData(NULL, m_socketTabulator, 5000, 0, NET_SND_CLNTACK, NULL);
			m_bInCommand =FALSE;

if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_CALLFUNC))
{ 
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Successfully executed %s with return data: %02x,%02x %s (%d bytes data) - returning %d", 
									(pucOrig?((char*)pucOrig):"null"),
									data.m_ucCmd,    // the command byte
									data.m_ucSubCmd,   // the subcommand byte
									(((*ppucDataBuffer)&&(data.m_ulDataLen))?(char*)*ppucDataBuffer:""),
									data.m_ulDataLen,
									(data.m_ucSubCmd&0x80)?((int)(data.m_ucSubCmd - 256)):((int)data.m_ucSubCmd)
									);
}

//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "X1"); Sleep(100);

	if(data.m_ucSubCmd&0x80)  // negative value
	{
			if(pszInfo)
			{
				_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Error: %02x,%02x %s (%d bytes data)", 
//										(pucOrig?((char*)pucOrig):"null"),
										data.m_ucCmd,    // the command byte
										data.m_ucSubCmd,   // the subcommand byte
										(((*ppucDataBuffer)&&(data.m_ulDataLen))?(char*)*ppucDataBuffer:""),
										data.m_ulDataLen
										);
			}

	}
	else
	{
			if(pszInfo)
			{
				_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Returned: %02x,%02x %s (%d bytes data)", 
//										(pucOrig?((char*)pucOrig):"null"),
										data.m_ucCmd,    // the command byte
										data.m_ucSubCmd,   // the subcommand byte
										(((*ppucDataBuffer)&&(data.m_ulDataLen))?(char*)*ppucDataBuffer:""),
										data.m_ulDataLen
										);
			}

	}

//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "X2"); Sleep(100);


			//ASRUN
if(!bSuppressAsRun)
{
	if(data.m_ucSubCmd&0x80)  // negative value
	{
		g_data.SendAsRunMsg(CX_SENDMSG_ERROR, g_settings.m_nAutoServerListNumber, pszAsRunEventID, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Successfully executed %s; Returned error: %02x,%02x %s (%d bytes data)", 
										(pucOrig?((char*)pucOrig):"null"),
										data.m_ucCmd,    // the command byte
										data.m_ucSubCmd,   // the subcommand byte
										(((*ppucDataBuffer)&&(data.m_ulDataLen))?(char*)*ppucDataBuffer:""),
										data.m_ulDataLen
										);

	}
	else
	{
		g_data.SendAsRunMsg(CX_SENDMSG_INFO, g_settings.m_nAutoServerListNumber, pszAsRunEventID, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Successfully executed %s with return data: %02x,%02x %s (%d bytes data)", 
										(pucOrig?((char*)pucOrig):"null"),
										data.m_ucCmd,    // the command byte
										data.m_ucSubCmd,   // the subcommand byte
										(((*ppucDataBuffer)&&(data.m_ulDataLen))?(char*)*ppucDataBuffer:""),
										data.m_ulDataLen
										);

	}


/*
if(g_ptabmsgr)
{
// asrun file message
// 12-22-2008 05:23:03.421: Sent [D100^M] (5 bytes) at server time 05:23:02.19, event hit time 05:22:58.06 (duration was calculated as 100 seconds) 


	if(data.m_ucSubCmd&0x80)
	{
		// negative value!
//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "pOv?%d", pOv?1:0);  Sleep(100);

		if(pOv) // this came from promotor and we calculated automation variables
		{
//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "pOv->m_n_current_servertime=%d", pOv->m_n_current_servertime);  Sleep(100);

			if(pOv->m_n_current_servertime>=0)
			{

				int nHours;
				int nMinutes;
				int nSeconds;
				int nFrames;

//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "convert 1");  Sleep(100);
				g_data.m_timeconv.MStoHMSF(pOv->m_n_current_servertime, &nHours, &nMinutes, &nSeconds, &nFrames, 
					(/*g_settings.m_bDF?29.97:* /(double)g_settings.m_nFrameRate) // ideal
					);
//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "convert 2");  Sleep(100);

				g_ptabmsgr->DM(MSG_ICONERROR, 
					(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
					pszAsRunEventID, "Error %d %s%s%ssending DTMF sequence %s (%d bytes) at server time %02d:%02d:%02d%s%02d", 
					(int)(data.m_ucSubCmd - 256),
					(*ppucDataBuffer)?"(":"",
					(*ppucDataBuffer)?((char*)(*ppucDataBuffer)):"",
					(*ppucDataBuffer)?") ":"",
					pszDTMF?pszDTMF:"",
					pszDTMF?strlen(pszDTMF):0,
					nHours, nMinutes, nSeconds, g_settings.m_bDF?";":":", nFrames
					
					);   //(Dispatch message)
//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "sent 1");  Sleep(100);
			}
			else
			{
				// no server time
//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "no server time");  Sleep(100);
				g_ptabmsgr->DM(MSG_ICONERROR, 
					(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
					pszAsRunEventID, "Error %d %s%s%ssending DTMF sequence %s", 
					(int)(data.m_ucSubCmd - 256),
					(*ppucDataBuffer)?"(":"",
					(*ppucDataBuffer)?((char*)(*ppucDataBuffer)):"",
					(*ppucDataBuffer)?") ":"",
					pszDTMF?pszDTMF:""
					);   //(Dispatch message)
//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "sent 2");  Sleep(100);

			}
		}
		else // test
		{
			g_ptabmsgr->DM(MSG_ICONERROR, 
				(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
				pszAsRunEventID, "Error %d %s%s%ssending test DTMF sequence [%s]", 
				(int)(data.m_ucSubCmd - 256),
				(*ppucDataBuffer)?"(":"",
				(*ppucDataBuffer)?((char*)(*ppucDataBuffer)):"",
				(*ppucDataBuffer)?") ":"",
				pszDTMF?pszDTMF:""
				);   //(Dispatch message)

		}
	}
	else
	{
//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "+ pOv?%d", pOv?1:0);  Sleep(100);
		if(pOv) // this came from promotor and we calculated automation variables
		{
			int nHours;
			int nMinutes;
			int nSeconds;
			int nFrames;
			int nIntHours;
			int nIntMinutes;
			int nIntSeconds;
			int nIntFrames;

//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "+ pOv->m_n_current_servertime=%d", pOv->m_n_current_servertime);  Sleep(100);
			if(pOv->m_n_current_servertime>=0)
			{

//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "cvt A");  Sleep(100);
				g_data.m_timeconv.MStoHMSF(pOv->m_n_current_servertime, &nHours, &nMinutes, &nSeconds, &nFrames, 
					(/*g_settings.m_bDF?29.97:* /(double)g_settings.m_nFrameRate) // ideal
					);
//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "cvt B");  Sleep(100);
				g_data.m_timeconv.MStoHMSF(pOv->m_n_next_start_tagged_event_start, &nIntHours, &nIntMinutes, &nIntSeconds, &nIntFrames, 
					(/*g_settings.m_bDF?29.97:* /(double)g_settings.m_nFrameRate) // ideal
					);
//				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "cvt C, %s %d", pszAsRunEventID?pszAsRunEventID:"(null)", pszDTMF?1:0);  Sleep(100);

// 12-22-2008 05:23:03.421: Sent [D100^M] (5 bytes) at server time 05:23:02.19, event hit time 05:22:58.06 (duration was calculated as 100 seconds) 

				g_ptabmsgr->DM(MSG_ICONNONE, 
					(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
					pszAsRunEventID, 
					"Sent DTMF sequence %s (%d bytes) at server time %02d:%02d:%02d%s%02d, interval start time %02d:%02d:%02d%s%02d (duration was calculated as %d ms)", 
					pszDTMF?pszDTMF:"",
					pszDTMF?strlen(pszDTMF):0,
					nHours, nMinutes, nSeconds, (g_settings.m_bDF?".":":"), nFrames,
					nIntHours, nIntMinutes, nIntSeconds, (g_settings.m_bDF?".":":"), nIntFrames,
					g_settings.m_bTonesUseDurationAddition?pOv->m_n_next_tagged_interval_accumulated_duration:pOv->m_n_next_tagged_interval_subtracted_duration					
					);   //(Dispatch message)
//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "sent Q");  Sleep(100);

			}
			else
			{
//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "cvt X");  Sleep(100);
				// no server time
				g_data.m_timeconv.MStoHMSF(pOv->m_n_current_systemtime, &nHours, &nMinutes, &nSeconds, &nFrames, 
					(/*g_settings.m_bDF?29.97:* /(double)g_settings.m_nFrameRate) // ideal
					);

//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "cvt Y");  Sleep(100);
				// to get this far we needed an interval...
				g_data.m_timeconv.MStoHMSF(pOv->m_n_next_start_tagged_event_start, &nIntHours, &nIntMinutes, &nIntSeconds, &nIntFrames, 
					(/*g_settings.m_bDF?29.97:* /(double)g_settings.m_nFrameRate) // ideal
					);
//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "cvt Z");  Sleep(100);

// 12-22-2008 05:23:03.421: Sent [D100^M] (5 bytes) at server time 05:23:02.19, event hit time 05:22:58.06 (duration was calculated as 100 seconds) 

				g_ptabmsgr->DM(MSG_ICONNONE, 
					(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
					pszAsRunEventID, 
					"Sent DTMF sequence %s (%d bytes) at system time %02d:%02d:%02d%s%02d, interval start time %02d:%02d:%02d%s%02d (duration was calculated as %d ms)", 
					pszDTMF?pszDTMF:"",
					pszDTMF?strlen(pszDTMF):0,
					nHours, nMinutes, nSeconds, g_settings.m_bDF?".":":", nFrames,
					nIntHours, nIntMinutes, nIntSeconds, g_settings.m_bDF?".":":", nIntFrames,
					g_settings.m_bTonesUseDurationAddition?pOv->m_n_next_tagged_interval_accumulated_duration:pOv->m_n_next_tagged_interval_subtracted_duration					
					);   //(Dispatch message)
//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "sent QQ");  Sleep(100);
			}
		}
		else // test
		{
			g_ptabmsgr->DM(MSG_ICONNONE, 
					(((g_settings.m_pszTonesAsRunDestination)&&(strlen(g_settings.m_pszTonesAsRunDestination)))?g_settings.m_pszTonesAsRunDestination:"serialtones_asrun"), 
					pszAsRunEventID, "Sent test DTMF sequence %s (%d bytes)", 
					pszDTMF?pszDTMF:"",
					pszDTMF?strlen(pszDTMF):0
				);   //(Dispatch message)

		}
	}
}
*/ 

}
//			return TABULATOR_SUCCESS;

// since it is an unsigned char we are getting back, we have a choice to make.
// either negative values were converted, and we have -1 == 0xff... or something.  But I think this will be the most common so let's use
// twos complement to return an int value between -128 and 127.

			if(data.m_ucSubCmd&0x80)
			{
				// negative value!
				return (int)(data.m_ucSubCmd - 256);
			}
			else
			{
				// positive value or zero.
				return (int)data.m_ucSubCmd;
			}
		}
	}
	return TABULATOR_ERROR;
}


unsigned long CStreamDataData::IncrementSpliceEventID()
{
	if(m_ul_splice_event_id<0x0fffffff) 
		m_ul_splice_event_id++;
	else
		m_ul_splice_event_id=0x00000001; // loop around.
	return m_ul_splice_event_id;

}

unsigned short CStreamDataData::IncrementUniqueProgID()
{
	if(m_us_unique_program_id<0xffff) 
		m_us_unique_program_id++;
	else
		m_us_unique_program_id=0x0001; // loop around.
	return m_us_unique_program_id;

}




int CStreamDataData::ClearOutputVariables()
{
	m_Output.m_b_list_is_playing = false;  // automation list is playing a primary event.

	// here, current means the currently playing primary event.
	m_Output.m_n_current_primary_start=STREAMDATA_INVALID_TIMEADDRESS; // in ideal MS; 
	m_Output.m_n_current_primary_duration=STREAMDATA_INVALID_TIMEADDRESS;  //in ideal MS 
	m_Output.m_psz_current_primary_ID=NULL; // = Harris ID of current
	m_Output.m_psz_current_primary_description=NULL;// = Harris Title of current

	m_Output.m_n_current_systemtime=0; // in ideal MS; 
	m_Output.m_n_current_servertime=0; // in ideal MS; 

	
	m_Output.m_dbl_last_servertime=-1.0; // in ideal MS since epoch local time; 
	m_Output.m_dbl_last_servertime_update=-1.0; // in ideal MS since epoch; 

/*
	m_Output.m_n_next_start_local_event = -1; // in ideal MS; -1 means not found
	m_Output.m_n_next_end_local_event = -1; // in ideal MS;  -1 means not found

	m_Output.m_n_next_local_interval_subtracted_duration=0; // in ideal MS - difference of on-air times.
	m_Output.m_n_next_local_interval_accumulated_duration=0; // in ideal MS - accumulated durations. 

	m_Output.m_psz_local_program_ID = NULL; // = PPL ID of local (given by start)

	m_Output.m_n_next_start_national_event = -1; // in ideal MS; 
	m_Output.m_n_next_end_national_event = -1; // in ideal MS; 

	m_Output.m_n_next_national_interval_subtracted_duration=0; // in ideal MS - difference of on-air times.
	m_Output.m_n_next_national_interval_accumulated_duration=0; // in ideal MS - accumulated durations.

	m_Output.m_psz_national_program_ID = NULL; // = PPL ID of local (given by start)
*/

	m_Output.m_n_next_start_splice_request=-1; // in ideal MS; 
	m_Output.m_n_next_dur_splice_request=-1; // in ideal MS; 

	return TABULATOR_SUCCESS;

}
/*
int CStreamDataData::GetClassification(CAutomationEvent* pEvent)
{
	if(pEvent)
	{

if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_CLASSRULES)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:GetClassification", "Checking event [%s][%s] against %d patterns", 
		pEvent->m_pszID?pEvent->m_pszID:"(null)", 
		pEvent->m_pszTitle?pEvent->m_pszTitle:"(null)", 
		m_nClassificationRules); // Sleep(250); //(Dispatch message)
}

		if((m_ppClassificationRules)&&(m_nClassificationRules>0))
		{
			int i=0;
			while(i<m_nClassificationRules)
			{
				if(m_ppClassificationRules[i])
				{

					if(
						  (m_ppClassificationRules[i]->m_pszPattern)
						&&(strlen(m_ppClassificationRules[i]->m_pszPattern)>0)
						&&(m_ppClassificationRules[i]->m_pszColName)
						&&(strlen(m_ppClassificationRules[i]->m_pszColName)>0)
						)
					{

if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_CLASSRULES)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:GetClassification", "Checking event [%s][%s] against pattern #%d: %s in %s", 
		pEvent->m_pszID?pEvent->m_pszID:"(null)", 
		pEvent->m_pszTitle?pEvent->m_pszTitle:"(null)", 
		i, m_ppClassificationRules[i]->m_pszPattern, m_ppClassificationRules[i]->m_pszColName); // Sleep(250); //(Dispatch message)
}


/*
	[event_id] [varchar](32) // reconcile key
	[event_clip] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[event_title] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
* /
						if(stricmp(m_ppClassificationRules[i]->m_pszColName, "event_clip")==0)
						{
							if((pEvent->m_pszID)&&(strlen(pEvent->m_pszID)>0))
							{
								if(TestPattern(pEvent->m_pszID, m_ppClassificationRules[i]->m_pszPattern) == STREAMDATA_PATTERN_MATCH)
								{
									return m_ppClassificationRules[i]->m_nEventType;
								}
							}
						}
						else
						if(stricmp(m_ppClassificationRules[i]->m_pszColName, "event_title")==0)
						{
							if((pEvent->m_pszTitle)&&(strlen(pEvent->m_pszTitle)>0))
							{
								if(TestPattern(pEvent->m_pszTitle, m_ppClassificationRules[i]->m_pszPattern) == STREAMDATA_PATTERN_MATCH)
								{
									return m_ppClassificationRules[i]->m_nEventType;
								}
							}
						}
					}
				}
				i++;
			}
		}
	}
	return STREAMDATA_CLASS_TYPE_NONE;
}
*/


int CStreamDataData::GetAutomationEvents()
{		

	if( (m_pdbAutomationConn)
		&&(m_pdbAutomation)
		&&(g_settings.m_pszAutoServer)
		&&(strlen(g_settings.m_pszAutoServer)>0)
		&&(g_settings.m_nAutoServerListNumber>0)
		)
	{
		char pszSource[MAX_PATH];
		_snprintf(pszSource, MAX_PATH-1, "%s:GetAutomationEvents", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");

		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		if(g_settings.m_nAutomationAnalysisLookahead>0)
		{
			if(g_settings.m_bAutomationLookahead)
			{
				sprintf(errorstring, " AND event_position < %d", (min(g_settings.m_nAutomationAnalysisLookahead, g_data.m_AutoList.m_nLookahead))  );
			}
			else
			{
				sprintf(errorstring, " AND event_position < %d", (g_settings.m_nAutomationAnalysisLookahead)  );
			}
		}
		else
		{
			if(g_settings.m_bAutomationLookahead)
			{
				sprintf(errorstring, " AND event_position < %d", (g_data.m_AutoList.m_nLookahead)  );
			}
			else
			{
				sprintf (errorstring, "");
			}
		}

		// use following for Harris
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT itemid, event_id, event_clip, event_segment, event_title, \
event_type, event_status, event_time_mode, event_start, event_duration \
FROM %s.dbo.%s WHERE server_name = '%s' AND server_list = %d%s \
ORDER BY event_position", // if we get ads first, those are easier to actuate, so let's prioritize them
			((g_settings.m_pszAutoModule)&&(strlen(g_settings.m_pszAutoModule)))?g_settings.m_pszAutoModule:"Sentinel",
			((g_settings.m_pszAutoEvents)&&(strlen(g_settings.m_pszAutoEvents)))?g_settings.m_pszAutoEvents:"Events",
			g_settings.m_pszAutoServer, g_settings.m_nAutoServerListNumber,
			errorstring
			); 


/*
		// convert to Helios
		// event_type is useless so switch to event_data, which we need anyway, but add <track> and then obtain the track=0 = primary, for type.

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT itemid, event_id, event_clip, event_title, \
event_data, event_status, event_time_mode, event_start, event_duration \
FROM %s.dbo.%s WHERE list_id = %d%s \
ORDER BY event_start", // use event_start since position has no meaning and is -1., also remove event_segment since that does not exist
			((g_settings.m_pszAutoModule)&&(strlen(g_settings.m_pszAutoModule)))?g_settings.m_pszAutoModule:"Helios",
			((g_settings.m_pszAutoEvents)&&(strlen(g_settings.m_pszAutoEvents)))?g_settings.m_pszAutoEvents:"Events",
			g_settings.m_nAutoServerListNumber,
			errorstring
			); 

*/
		strcpy (errorstring, "");

		//AND (event_type & 128 = 0)%s  - removed this so I can get the secondary events

/*

 	int m_nID;
	unsigned short m_usType;
	char* m_pszID;
	char* m_pszTitle;
	char* m_pszReconcileKey;
	unsigned char  m_ucSegment;
	unsigned long  m_ulOnAirTimeMS;					// ideal milliseconds as appears in the list
	unsigned long  m_ulOnAirLocalUnixDate;  //in seconds, offset from January 1, 1900
	unsigned long  m_ulDurationMS;          // ideal milliseconds as appears in the list
	unsigned long  m_ulSOMMS;               // ideal milliseconds as appears in the list
	unsigned short m_usStatus;
//	unsigned short m_usControl; // was this
	unsigned long m_ulControl;  // made it this, storing revent->extrathree (which is extended event control in recfm80) in the upper word

	// potential output variables.
	unsigned long m_ulClassification;
	char* m_pszOutputString; // like:


USE [sentinel]
GO
/ ****** Object:  Table [dbo].[Events]    Script Date: 12/19/2011 00:39:06 ****** /
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events](
	[itemid] [int] NOT NULL,
	[event_key] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[server_name] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[server_list] [int] NULL,
	[list_id] [int] NULL,
	[event_id] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[event_clip] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[event_segment] [int] NULL,
	[event_title] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[event_data] [varchar](4096) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[event_type] [int] NULL,
	[event_status] [int] NULL,
	[event_time_mode] [int] NULL,
	[event_start] [decimal](20, 3) NULL,
	[event_duration] [int] NULL,
	[event_calc_start] [decimal](20, 3) NULL,
	[event_calc_duration] [int] NULL,
	[event_calc_end] [decimal](20, 3) NULL,
	[event_position] [int] NULL,
	[event_last_update] [decimal](20, 3) NULL,
	[parent_uid] [int] NULL,
	[parent_key] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[parent_id] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[parent_clip] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[parent_segment] [int] NULL,
	[parent_title] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[parent_type] [int] NULL,
	[parent_status] [int] NULL,
	[parent_time_mode] [int] NULL,
	[parent_start] [decimal](20, 3) NULL,
	[parent_duration] [int] NULL,
	[parent_calc_start] [decimal](20, 3) NULL,
	[parent_calc_duration] [int] NULL,
	[parent_calc_end] [decimal](20, 3) NULL,
	[parent_position] [int] NULL,
	[app_data] [varchar](512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[app_data_aux] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF



USE [helios]
GO

/****** Object:  Table [dbo].[Events]    Script Date: 01/14/2014 13:48:52 ****** /
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Events](
	[itemid] [int] IDENTITY(1,1) NOT NULL,
	[conn_ip] [varchar](32) NOT NULL,
	[conn_port] [int] NOT NULL,
	[list_id] [int] NOT NULL,
	[event_id] [varchar](32) NOT NULL,
	[event_clip] [varchar](64) NOT NULL,
	[event_title] [varchar](64) NOT NULL,
	[event_data] [varchar](4096) NULL,
	[event_type] [int] NOT NULL,
	[event_status] [int] NOT NULL,
	[event_time_mode] [int] NOT NULL,
	[event_start] [decimal](20, 3) NOT NULL,
	[event_duration] [int] NOT NULL,
	[event_last_update] [decimal](20, 3) NOT NULL,
	[event_calc_start] [decimal](20, 3) NULL,
	[event_position] [int] NOT NULL,
	[parent_id] [varchar](32) NULL,
	[parent_position] [int] NULL,
	[parent_start] [decimal](20, 3) NULL,
	[parent_calc_start] [decimal](20, 3) NULL,
	[parent_duration] [int] NULL,
	[parent_calc_end] [decimal](20, 3) NULL,
	[event_calc_end] [decimal](20, 3) NULL,
	[app_data] [varchar](512) NULL,
	[app_data_aux] [int] NULL,
	[event_start_internal] [decimal](20, 3) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO






*/
if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_AUTOEVENTS)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "GetAutomationEvents");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdbAutomation->Retrieve(m_pdbAutomationConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.

			// first let's preserve the existing list.
			CAutomationEvent** ppTemp  = NULL;
			int nNumEvents = m_nAutoEvents;
			if(m_nAutoEvents>0)
			{
				ppTemp = new CAutomationEvent*[m_nAutoEvents];
				if(ppTemp)
				{
					memcpy(ppTemp, m_ppAutoEvents, sizeof(CAutomationEvent*)*m_nAutoEvents);
					memset(m_ppAutoEvents, 0, sizeof(CAutomationEvent*)*m_nAutoEventsArraySize);  // do the whole array
					m_nAutoEvents = 0;
				}
				else
				{
					nNumEvents=0;
				}
			}

			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nAutoEventsArraySize) // have to expand
				{
					CAutomationEvent** ppNew = new CAutomationEvent*[m_nAutoEventsArraySize + STREAMDATA_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nAutoEventsArraySize += STREAMDATA_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(CAutomationEvent*)*m_nAutoEventsArraySize);
						if(m_ppAutoEvents)
						{
							memcpy(ppNew, m_ppAutoEvents, sizeof(CAutomationEvent*)*nIndex); // it's an array of pointers
						}
						m_ppAutoEvents = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppAutoEvents==NULL)
				{
					m_nAutoEventsArraySize = 0;
					m_nAutoEvents=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nAutoEvents)
				{
					m_ppAutoEvents[nIndex] = new CAutomationEvent;  // guaranteed that there will be array space, above.
				}

				if(m_ppAutoEvents[nIndex])
				{
					CString szTemp;
// SELECT itemid, event_id, event_clip, event_segment, event_title,
// event_type, event_status, event_time_mode,	event_start, event_duration
					
					prs->GetFieldValue("itemid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("event_id", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						if(m_ppAutoEvents[nIndex]->m_pszReconcileKey != NULL)
						{
							free(m_ppAutoEvents[nIndex]->m_pszReconcileKey);
						}
						m_ppAutoEvents[nIndex]->m_pszReconcileKey = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppAutoEvents[nIndex]->m_pszReconcileKey)
						{
							strcpy(m_ppAutoEvents[nIndex]->m_pszReconcileKey, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("event_clip", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						if(m_ppAutoEvents[nIndex]->m_pszID != NULL)
						{
							free(m_ppAutoEvents[nIndex]->m_pszID);
						}
						m_ppAutoEvents[nIndex]->m_pszID = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppAutoEvents[nIndex]->m_pszID)
						{
							strcpy(m_ppAutoEvents[nIndex]->m_pszID, szTemp.GetBuffer(0));
						}
					}


if(0)//g_settings.m_ulDebug&(STREAMDATA_DEBUG_AUTOEVENTS)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "obtained %s", m_ppAutoEvents[nIndex]->m_pszID);
	 Sleep(250); //(Dispatch message)
}

					prs->GetFieldValue("event_segment", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_ucSegment = (unsigned char)atol(szTemp);
					}
					prs->GetFieldValue("event_title", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						if(m_ppAutoEvents[nIndex]->m_pszTitle != NULL)
						{
							free(m_ppAutoEvents[nIndex]->m_pszTitle);
						}
						m_ppAutoEvents[nIndex]->m_pszTitle = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppAutoEvents[nIndex]->m_pszTitle)
						{
							strcpy(m_ppAutoEvents[nIndex]->m_pszTitle, szTemp.GetBuffer(0));
						}
					}
					
//					prs->GetFieldValue("event_data", szTemp);//HARDCODE - omnibus
					prs->GetFieldValue("event_type", szTemp);//HARDCODE - harris
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_usType = (unsigned short)atol(szTemp); 

/* // Helios specific
						int q = szTemp.Find("<track>");
						if(q<0)
						{
							m_ppAutoEvents[nIndex]->m_usType = 65535;  //-1
						}
						else
						{
							m_ppAutoEvents[nIndex]->m_usType = (unsigned short)atol(szTemp.Mid(q+7));  // index plus strlen <track>
						}
*/
/*
						if(m_ppAutoEvents[nIndex]->m_pszData != NULL)
						{
							free(m_ppAutoEvents[nIndex]->m_pszData);
						}
						m_ppAutoEvents[nIndex]->m_pszData = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppAutoEvents[nIndex]->m_pszData)
						{
							strcpy(m_ppAutoEvents[nIndex]->m_pszData, szTemp.GetBuffer(0));
						}
						*/

					}
					prs->GetFieldValue("event_status", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_usStatus = (unsigned short)atol(szTemp);
					}
					prs->GetFieldValue("event_time_mode", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_ulControl = (unsigned short)atol(szTemp);
					}
					double dblOnAir;
					prs->GetFieldValue("event_start", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						dblOnAir = atof(szTemp);

if(0)//(g_settings.m_ulDebug&(STREAMDATA_DEBUG_AUTOEVENTS)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "obtained %.3f", dblOnAir);
	 Sleep(250); //(Dispatch message)
}

						m_ppAutoEvents[nIndex]->m_ulOnAirLocalUnixDate = ((unsigned long)(int)(dblOnAir));

						dblOnAir -= (double)(int)(dblOnAir);

//if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_AUTOEVENTS)) 
//{
	// this is unbelievable.  0.166 -> 166.000 when *1000.0, but then when casted to unsigned long, reverts to 165!  shady floating points!
// 2011-Dec-30 07:31:43.769 [H ] calc 0.166 166.000 165 - StreamData:GetAutomationEvents - 
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "calc %.3f %.3f %d %d", dblOnAir, (dblOnAir)*1000.0, (unsigned long)((dblOnAir)*1000.0), m_ppAutoEvents[nIndex]->m_ulOnAirLocalUnixDate);//  Sleep(250); //(Dispatch message)/
//}


//						m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS = (unsigned long)((dblOnAir)*1000.0 +.999999999) + (m_ppAutoEvents[nIndex]->m_ulOnAirLocalUnixDate%86400)*1000; // ideal not real
						m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS = (unsigned long)(((dblOnAir)*1000.0) +.1) + (m_ppAutoEvents[nIndex]->m_ulOnAirLocalUnixDate%86400)*1000; // ideal not real

						// OK had to add the .0001 in first just to tip the balance.  then we have nice correct integer values.
						// then instead of using the 0.99999999 as explained, just going to do the same way as I did duration

						if(m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS%10) m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS++;  // these are ideal values.  so will either end in 0, 3, or 6.  if not 0, increment up by one ms.

						
						// the 0.99999999 comes from the fact that Sentinel stores the ideal ms in increments of 33 ms, like:
						// .000, .033, .066, .100, .133 etc.  which means that they are rounded.
						// So something that has a frame of 01 would get 033 milliseconds, not 033.333333 ms
						// this truncs to 33 and so we get frame number 00.
						// by adding .99999 we up this to 34.66666666 which truncs to 34 and we are all set.
						// similar for 66.6666 -> 67.  BUT, 100ms -> 100.999999999 ms -> 100.  so we are good with round values.

						if((m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS == 0)&&(!(m_ppAutoEvents[nIndex]->m_ulControl&0x10))) // this could be an error, attempt to correct it.
						{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "Detected time address of 00:00:00.00" );//  Sleep(250); //(Dispatch message)/

							if(nIndex>0)
							{
								if(m_ppAutoEvents[nIndex-1])
								{
									if((m_ppAutoEvents[nIndex-1]->m_ulOnAirTimeMS  + m_ppAutoEvents[nIndex-1]->m_ulDurationMS)%86400000)
									{
										m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS  =  m_ppAutoEvents[nIndex-1]->m_ulOnAirTimeMS  + m_ppAutoEvents[nIndex-1]->m_ulDurationMS; 
										m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS %= 86400000; // in case rolled over
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "Re-assigned start time of event from 0 to %d", m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS );//  Sleep(250); //(Dispatch message)/
									}
								}
							}
						}

					}
					prs->GetFieldValue("event_duration", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						m_ppAutoEvents[nIndex]->m_ulDurationMS = atol(szTemp); // neg durations ever? no, guess not.
						if(m_ppAutoEvents[nIndex]->m_ulDurationMS%10) m_ppAutoEvents[nIndex]->m_ulDurationMS++;  // these are ideal values.  so will either end in 0, 3, or 6.  if not 0, increment up by one ms.
					}
				
if((g_settings.m_ulDebug&(STREAMDATA_DEBUG_AUTOEVENTS)) && (g_ptabmsgr))
{
	unsigned long ulTCoat;
	unsigned long ulTCdur;
	CTimeConvert tcv;
	tcv.MStoBCDHMSF(m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS, &ulTCoat,(double)g_settings.m_nFrameRate); // using ideal
	tcv.MStoBCDHMSF(m_ppAutoEvents[nIndex]->m_ulDurationMS, &ulTCdur,(double)g_settings.m_nFrameRate); // using ideal

//	if(g_ptabmsgr) 
	g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "%10d;%03d: %08x (%08d) dur %08x (%08d) %04xt %08xm %04xs [%s] [%s]", 
		m_ppAutoEvents[nIndex]->m_nID,nIndex,
		ulTCoat, m_ppAutoEvents[nIndex]->m_ulOnAirTimeMS,
		ulTCdur, m_ppAutoEvents[nIndex]->m_ulDurationMS,
		m_ppAutoEvents[nIndex]->m_usType,
		m_ppAutoEvents[nIndex]->m_ulControl,
		m_ppAutoEvents[nIndex]->m_usStatus,
		m_ppAutoEvents[nIndex]->m_pszID?m_ppAutoEvents[nIndex]->m_pszID:"(null)",
		m_ppAutoEvents[nIndex]->m_pszTitle?m_ppAutoEvents[nIndex]->m_pszTitle:"(null)"
		);//  Sleep(250); //(Dispatch message)
}
				}
				nIndex++;
				prs->MoveNext();
			}


			if(nIndex<m_nAutoEvents)
			{
if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_AUTOEVENTS)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "Deleting overflow %d>%d", m_nAutoEvents, nIndex);//  Sleep(250); //(Dispatch message)
}
				int i=nIndex;
				while(i<m_nAutoEvents)
				{
					if(m_ppAutoEvents[i]) delete m_ppAutoEvents[i];
					m_ppAutoEvents[i] = NULL;
					i++;
				}
if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_AUTOEVENTS)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "Deleted overflow, events now %d", nIndex); // Sleep(250); //(Dispatch message)
}
			}

			m_nAutoEvents = nIndex;

			if((m_ppAutoEvents)&&(m_nAutoEvents)&&(ppTemp != NULL)&&(nNumEvents>0))
			{
				// match to preserve previously calc'd output vars
				int nLastFound = -1;
				
				int i=0;
				while(i<m_nAutoEvents)
				{
					if(m_ppAutoEvents[i])
					{
						int nCount=0;
						int j = nLastFound+1;
						int nEndIndex = nNumEvents-1;

						while(nCount < nNumEvents) // max search them all
						{
							if(j>nEndIndex) j=0;
							if(ppTemp[j])
							{
								if(ppTemp[j]->m_nID == m_ppAutoEvents[i]->m_nID)
								{
									// match!
									m_ppAutoEvents[i]->m_pszOutputString  = ppTemp[j]->m_pszOutputString;
									ppTemp[j]->m_pszOutputString = NULL; //prevents freeing

//									m_ppAutoEvents[i]->m_ulClassification = ppTemp[j]->m_ulClassification;

									nLastFound = j;

									break;
								}
							}
							j++;							
							nCount++; 
						}
						if(nCount >= nNumEvents) // didnt find.
							nLastFound = -1;
					}
					i++;
				}
				
		//		nNumEvents = m_nAutoEvents;  // this is just wrong.
			}

			if((ppTemp != NULL)&&(nNumEvents>0))
			{
				int i=0;
				while(i<nNumEvents)
				{
					if(ppTemp[i]) delete ppTemp[i];
					i++;
				}
				delete [] ppTemp;
			}

			m_bInitalEventGetSucceeded = true;

			prs->Close();
			delete prs;

			return nIndex;
		}
		else
		{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "ERROR: %s", errorstring); // Sleep(250); //(Dispatch message)
		}
	}
	return TABULATOR_ERROR;
}

int CStreamDataData::GetServerTime(bool bCalcOnly)
{
	if((g_settings.m_bUseTimeCode)&&(m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
	{
		char pszSource[MAX_PATH];
		_snprintf(pszSource, MAX_PATH-1, "%s:GetServerTime:***** **", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");


		unsigned long ulTimeCode;
		EnterCriticalSection(&m_critTimeCode);
		ulTimeCode = m_ulTimeCode;
//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
		LeaveCriticalSection(&m_critTimeCode);

		m_Output.m_n_current_servertime = m_timeconv.BCDHMSFtoMS(ulTimeCode, ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)); // make it into ideal ms.
		m_Output.m_n_current_systemtime = m_Output.m_n_current_servertime;
		
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_TIMECODE)) 
g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, pszSource, "TC %d", m_Output.m_n_current_servertime); // Sleep(250); //(Dispatch message)


		return TABULATOR_SUCCESS;

	}
	else
	if(bCalcOnly)
	{
		_timeb timestamp;
		_ftime(&timestamp);
		char pszSource[MAX_PATH];
		_snprintf(pszSource, MAX_PATH-1, "%s:GetServerTime:***** **", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");

		// get system time, (not local time)
		double dblSystemTime = (double)(((unsigned long)(timestamp.time)*1000) + (unsigned long)timestamp.millitm);
		m_Output.m_n_current_systemtime = ((timestamp.time/* - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)*/ )%86400)*1000 + timestamp.millitm;

		if((m_Output.m_b_list_is_playing)&&(m_Output.m_dbl_last_servertime>0.0)&&(m_Output.m_dbl_last_servertime_update>0.0))
		{
			m_Output.m_n_current_servertime = (int)(((__int64)(m_Output.m_dbl_last_servertime))%86400000)
				+ (int)(dblSystemTime - m_Output.m_dbl_last_servertime_update); 

			if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_TIMECODE)) 
g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, pszSource, "TC sys calc %d", m_Output.m_n_current_servertime); // Sleep(250); //(Dispatch message)
			return TABULATOR_SUCCESS;

		}
		else
		{
			m_Output.m_n_current_servertime = m_Output.m_n_current_systemtime;
		}
	}

	else
	if((m_pdbAutomationConn)&&(m_pdbAutomation)&&(g_settings.m_pszAutoServer)&&(strlen(g_settings.m_pszAutoServer)>0)&&(g_settings.m_nAutoServerListNumber>0))
	{
		char pszSource[MAX_PATH];
		_snprintf(pszSource, MAX_PATH-1, "%s:GetServerTime", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
		double dblSystemTime=-1.0;

		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT server_time, server_last_update \
FROM %s.dbo.%s WHERE server = '%s' AND (flags & 1 = 1)",  // has to be active
			((g_settings.m_pszAutoModule)&&(strlen(g_settings.m_pszAutoModule)))?g_settings.m_pszAutoModule:"Sentinel",
			((g_settings.m_pszAutoServers)&&(strlen(g_settings.m_pszAutoServers)))?g_settings.m_pszAutoServers:"Connections",
			g_settings.m_pszAutoServer
			); 


if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_AUTOLIST)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "GetAutomationListStatus");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		int nIndex = 0;
		CRecordset* prs = m_pdbAutomation->Retrieve(m_pdbAutomationConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			if (!prs->IsEOF())
			{
				nIndex++;
				CString szTemp;

				_snprintf(pszSource, MAX_PATH-1, "%s:GetServerTime:***** **", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
				double dblServerTime=-1.0;
				double dblSystemUpdateTime=-1.0;
				prs->GetFieldValue("server_time", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					dblServerTime = atof(szTemp);
					if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_TIMECODE)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, pszSource, "DB TC %s", szTemp); // Sleep(250); //(Dispatch message)
				}
				prs->GetFieldValue("server_last_update", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					dblSystemUpdateTime = atof(szTemp);
					if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_TIMECODE)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, pszSource, "DB TC offset %s", szTemp); // Sleep(250); //(Dispatch message)
				}

				prs->Close();
				delete prs;

				_timeb timestamp;
				_ftime(&timestamp);

				// get system time, (not local time)
				dblSystemTime = (double)(((unsigned long)(timestamp.time)*1000) + (unsigned long)timestamp.millitm);
				m_Output.m_n_current_systemtime = ((timestamp.time/* - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)*/ )%86400)*1000 + timestamp.millitm;

				if((m_Output.m_b_list_is_playing)&&(dblServerTime>0.0)&&(dblSystemUpdateTime>0.0))
				{
 				// call these outside.
//					EnterCriticalSection(&g_data.m_critOutputVariables);
			
					m_Output.m_dbl_last_servertime = dblServerTime;    // in ideal MS since epoch, local time! 
					m_Output.m_dbl_last_servertime_update = dblSystemUpdateTime; // in ideal MS since epoch; 

					m_Output.m_n_current_servertime = (int)(((__int64)(m_Output.m_dbl_last_servertime))%86400000)
						+ (int)(dblSystemTime - m_Output.m_dbl_last_servertime_update); 

					if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_TIMECODE)) 
	 g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, pszSource, "TC sys %d", m_Output.m_n_current_servertime); // Sleep(250); //(Dispatch message)

//					LeaveCriticalSection(&m_critOutputVariables);
				}
				else
				{
					m_Output.m_n_current_servertime = m_Output.m_n_current_systemtime;
				}
		
			}
			else
			{
				prs->Close();
				delete prs;
			}

		}
		
		if(dblSystemTime<0.0)
		{
			_timeb timestamp;
			_ftime(&timestamp);

			// get system time, (not local time)
			dblSystemTime = (double)(((unsigned long)(timestamp.time)*1000) + (unsigned long)timestamp.millitm);
			m_Output.m_n_current_systemtime = ((timestamp.time/* - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)*/ )%86400)*1000 + timestamp.millitm;
			m_Output.m_n_current_servertime = m_Output.m_n_current_systemtime;

		}
		else
		{
			return TABULATOR_SUCCESS;
		}

	}

	else
	{
			_timeb timestamp;
			_ftime(&timestamp);

			// get system time, (not local time)
			double dblSystemTime = (double)(((unsigned long)(timestamp.time)*1000) + (unsigned long)timestamp.millitm);
			m_Output.m_n_current_systemtime = ((timestamp.time/* - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)*/ )%86400)*1000 + timestamp.millitm;
			m_Output.m_n_current_servertime = m_Output.m_n_current_systemtime;
	}
	return TABULATOR_ERROR;

}

int CStreamDataData::GetAutomationListStatus()
{
	if(
		  (m_pdbAutomationConn)
		&&(m_pdbAutomation)
		&&(g_settings.m_pszAutoServer)
		&&(strlen(g_settings.m_pszAutoServer)>0)
		&&(g_settings.m_nAutoServerListNumber>0)
		)
	{
		char pszSource[MAX_PATH];
		_snprintf(pszSource, MAX_PATH-1, "%s:GetAutomationListStatus", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

//  use for sentinel
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update \
FROM %s.dbo.%s WHERE server = '%s' AND listid = %d AND (flags & 1 = 1)",  // has to be active
			((g_settings.m_pszAutoModule)&&(strlen(g_settings.m_pszAutoModule)))?g_settings.m_pszAutoModule:"Sentinel",
			((g_settings.m_pszAutoLists)&&(strlen(g_settings.m_pszAutoLists)))?g_settings.m_pszAutoLists:"Channels",
			g_settings.m_pszAutoServer, g_settings.m_nAutoServerListNumber
			); 


/*  Use for Helios
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update \
FROM %s.dbo.%s WHERE ID = %d AND (flags & 1 = 1)",  // has to be active
			((g_settings.m_pszAutoModule)&&(strlen(g_settings.m_pszAutoModule)))?g_settings.m_pszAutoModule:"Helios",
			((g_settings.m_pszAutoLists)&&(strlen(g_settings.m_pszAutoLists)))?g_settings.m_pszAutoLists:"Channels",
			g_settings.m_nAutoServerListNumber
			); 
			*/

		/*
class CAutomationList  
{
public:
	CAutomationList();
	virtual ~CAutomationList();

	int m_nState;
	int m_nChange;
	int m_nDisplay;
	int m_nSysChange;
	int m_nCount; 
	int m_nLookahead;
	double m_dblLastUpdate;
};

USE [sentinel]
GO
/ ****** Object:  Table [dbo].[Channels]    Script Date: 12/19/2011 00:55:03 ****** /
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Channels](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[flags] [int] NOT NULL,
	[description] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[server] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[listid] [int] NULL,
	[list_state] [int] NULL,
	[list_changed] [int] NULL,
	[list_display] [int] NULL,
	[list_syschange] [int] NULL,
	[list_count] [int] NULL,
	[list_lookahead] [int] NULL,
	[list_last_update] [decimal](20, 3) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF

*/



if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_AUTOLIST)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource);  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		int nIndex = 0;
		CRecordset* prs = m_pdbAutomation->Retrieve(m_pdbAutomationConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			if (!prs->IsEOF())
			{
				nIndex++;
				CString szTemp;

// SELECT list_state, list_changed, list_display, list_syschange, list_count, list_lookahead, list_last_update
				
				prs->GetFieldValue("list_state", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nState = atoi(szTemp);
				}
				prs->GetFieldValue("list_changed", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nChange = atoi(szTemp);
				}
				prs->GetFieldValue("list_display", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nDisplay = atoi(szTemp);
				}
				prs->GetFieldValue("list_syschange", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nSysChange = atoi(szTemp);
				}
				prs->GetFieldValue("list_count", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nCount = atoi(szTemp);
				}
				prs->GetFieldValue("list_lookahead", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_nLookahead = atoi(szTemp);
				}
				prs->GetFieldValue("list_last_update", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					m_AutoList.m_dblLastUpdate = atof(szTemp);
				}


				if((m_AutoList.m_nState<0)||(m_AutoList.m_nChange<0))	m_bInitalEventGetSucceeded = true; // nothing was connected or active so OK, call that initialized.


				prs->Close();
				delete prs;
				return TABULATOR_SUCCESS;
			
			}
			else
			{
				m_bInitalEventGetSucceeded = true; // nothing was active so OK, call that initialized.
			}

			prs->Close();
			delete prs;
		}

		if(nIndex==0)
		{
			// no records, either no automation or whatever...
			m_AutoList.m_nState=-1;
			m_AutoList.m_nChange=-1;
			m_AutoList.m_nDisplay=-1;
			m_AutoList.m_nSysChange=-1;
			m_AutoList.m_nCount=-1;
			m_AutoList.m_nLookahead=-1;
			m_AutoList.m_dblLastUpdate = -1.0;
		}
	}
	return TABULATOR_ERROR;
}

/*
int CStreamDataData::GetOpportuneEvents()
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		// unfortunately, I cannot just get the ones within the valid dates, since time progresses, and I only get this table when it changes.
		// I have to weed out the ones as I go....
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, event_name, valid_date, expire_date FROM %s ORDER BY ID", 
			((g_settings.m_pszOpportuneEvents)&&(strlen(g_settings.m_pszOpportuneEvents)))?g_settings.m_pszOpportuneEvents:"OpportuneEvents"
			); 

	//create table OpportuneEvents (ID int identity(1,1), event_name varchar(64), event_duration int, valid_date decimal(20,3), expire_date decimal(20,3))  
if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:GetOpportuneEvents", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "GetOpportuneEvents");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nOpportuneEventsArraySize) // have to expand
				{
					COpportuneEvent** ppNew = new COpportuneEvent*[m_nOpportuneEventsArraySize + STREAMDATA_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nOpportuneEventsArraySize += STREAMDATA_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(COpportuneEvent*)*m_nOpportuneEventsArraySize);
						if(m_ppOpportuneEvents)
						{
							memcpy(ppNew, m_ppOpportuneEvents, sizeof(COpportuneEvent*)*nIndex); // it's an array of pointers
						}
						m_ppOpportuneEvents = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppOpportuneEvents==NULL)
				{
					m_nOpportuneEventsArraySize = 0;
					m_nOpportuneEvents=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nOpportuneEvents)
				{
					m_ppOpportuneEvents[nIndex] = new COpportuneEvent;  // guaranteed that there will be array space, above.
				}

				if(m_ppOpportuneEvents[nIndex])
				{
// removed all length validation.
//if something is "blank" then let it be - it has to overwrite any old data.

					CString szTemp;
	//SELECT ID, programID, sequence, replacementID, duration, valid_date, expire_date
					prs->GetFieldValue("ID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppOpportuneEvents[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("event_name", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						if(m_ppOpportuneEvents[nIndex]->m_pszEventToken != NULL)
						{
							free(m_ppOpportuneEvents[nIndex]->m_pszEventToken);
						}
						m_ppOpportuneEvents[nIndex]->m_pszEventToken = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppOpportuneEvents[nIndex]->m_pszEventToken)
						{
							strcpy(m_ppOpportuneEvents[nIndex]->m_pszEventToken, szTemp.GetBuffer(0));
						}
					}

					prs->GetFieldValue("event_duration", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppOpportuneEvents[nIndex]->m_nDuration = atoi(szTemp);
					}
					prs->GetFieldValue("valid_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppOpportuneEvents[nIndex]->m_dblValidDate = atof(szTemp);
					}
					prs->GetFieldValue("expire_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppOpportuneEvents[nIndex]->m_dblExpireDate = atof(szTemp);
					}
				}

				m_ppOpportuneEvents[nIndex]->m_bEventTokenChanged = true; // just do, regardless

if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OPPEVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "StreamData:debug", "OE%03d ID %d, %s, %.3f-%.3f", 
	nIndex, 
	m_ppOpportuneEvents[nIndex]->m_nID,
	m_ppOpportuneEvents[nIndex]->m_pszEventToken,
	m_ppOpportuneEvents[nIndex]->m_dblValidDate,
	m_ppOpportuneEvents[nIndex]->m_dblExpireDate
	);

				nIndex++;
				prs->MoveNext();
			}

			if(nIndex<m_nOpportuneEvents)
			{
				int i=nIndex;
				while(i<m_nOpportuneEvents)
				{
					if(m_ppOpportuneEvents[i]) delete m_ppOpportuneEvents[i];
					m_ppOpportuneEvents[i] = NULL;
					i++;
				}
			}
			m_nOpportuneEvents = nIndex;

			prs->Close();
			delete prs;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_OPPEVENTS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "StreamData:debug", "OE: %d", m_nOpportuneEvents	);



			return nIndex;
		}
	}
	return TABULATOR_ERROR;
}


int CStreamDataData::GetExclusionRules()
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		// unfortunately, I cannot just get the ones within the valid dates, since time progresses, and I only get this table when it changes.
		// I have to weed out the ones as I go....
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, pattern, col_name, auto_event_type, event_type, valid_date, expire_date FROM %s ORDER BY ID", 
			((g_settings.m_pszExclusionRules)&&(strlen(g_settings.m_pszExclusionRules)))?g_settings.m_pszExclusionRules:"ExclusionRules"
			); 

	//create table ExclusionRules (ID int identity(1,1), pattern varchar(128), col_name varchar(64), auto_event_type int, event_type int, valid_date decimal(20,3), expire_date decimal(20,3))  
if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:GetExclusionRules", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "GetExclusionRules");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nExclusionRulesArraySize) // have to expand
				{
					CExclusionRule** ppNew = new CExclusionRule*[m_nExclusionRulesArraySize + STREAMDATA_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nExclusionRulesArraySize += STREAMDATA_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(CExclusionRule*)*m_nExclusionRulesArraySize);
						if(m_ppExclusionRules)
						{
							memcpy(ppNew, m_ppExclusionRules, sizeof(CExclusionRule*)*nIndex); // it's an array of pointers
						}
						m_ppExclusionRules = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppExclusionRules==NULL)
				{
					m_nExclusionRulesArraySize = 0;
					m_nExclusionRules=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nExclusionRules)
				{
					m_ppExclusionRules[nIndex] = new CExclusionRule;  // guaranteed that there will be array space, above.
				}

				if(m_ppExclusionRules[nIndex])
				{
// removed all length validation.
//if something is "blank" then let it be - it has to overwrite any old data.

					CString szTemp;
	//SELECT ID, programID, sequence, replacementID, duration, valid_date, expire_date
					prs->GetFieldValue("ID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppExclusionRules[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("pattern", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						if(m_ppExclusionRules[nIndex]->m_pszPattern != NULL)
						{
							free(m_ppExclusionRules[nIndex]->m_pszPattern);
						}
						m_ppExclusionRules[nIndex]->m_pszPattern = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppExclusionRules[nIndex]->m_pszPattern)
						{
							strcpy(m_ppExclusionRules[nIndex]->m_pszPattern, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("col_name", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						if(m_ppExclusionRules[nIndex]->m_pszColName != NULL)
						{
							free(m_ppExclusionRules[nIndex]->m_pszColName);
						}
						m_ppExclusionRules[nIndex]->m_pszColName = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppExclusionRules[nIndex]->m_pszColName)
						{
							strcpy(m_ppExclusionRules[nIndex]->m_pszColName, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("auto_event_type", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppExclusionRules[nIndex]->m_nAutoEventType = atoi(szTemp);
					}
					prs->GetFieldValue("event_type", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppExclusionRules[nIndex]->m_nEventType = atoi(szTemp);
					}
					prs->GetFieldValue("valid_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppExclusionRules[nIndex]->m_dblValidDate = atof(szTemp);
					}
					prs->GetFieldValue("expire_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppExclusionRules[nIndex]->m_dblExpireDate = atof(szTemp);
					}
				}


if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_EXCLRULES)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "StreamData:debug", "ER%03d ID %d, %s %s, atype %d, type %d, %.3f-%.3f", 
	nIndex, 
	m_ppExclusionRules[nIndex]->m_nID,
	m_ppExclusionRules[nIndex]->m_pszPattern,
	m_ppExclusionRules[nIndex]->m_pszColName,
	m_ppExclusionRules[nIndex]->m_nAutoEventType,
	m_ppExclusionRules[nIndex]->m_nEventType,
	m_ppExclusionRules[nIndex]->m_dblValidDate,
	m_ppExclusionRules[nIndex]->m_dblExpireDate
	
	);

				nIndex++;
				prs->MoveNext();
			}

			if(nIndex<m_nExclusionRules)
			{
				int i=nIndex;
				while(i<m_nExclusionRules)
				{
					if(m_ppExclusionRules[i]) delete m_ppExclusionRules[i];
					m_ppExclusionRules[i] = NULL;
					i++;
				}
			}
			m_nExclusionRules = nIndex;

			prs->Close();
			delete prs;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_EXCLRULES)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "StreamData:debug", "ER: %d", m_nExclusionRules	);



			return nIndex;
		}
	}
	return TABULATOR_ERROR;
}
*/

/*
int CStreamDataData::GetReplacementRules()
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		// unfortunately, I cannot just get the ones within the valid dates, since time progresses, and I only get this table when it changes.
		// I have to weed out the ones as I go....
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, programID, sequence, replacementID, duration, valid_date, expire_date FROM %s ORDER BY sequence", 
			((g_settings.m_pszReplacementRules)&&(strlen(g_settings.m_pszReplacementRules)))?g_settings.m_pszReplacementRules:"ReplacementRules"
			); 

//create table ReplacementRules (ID int identity(1,1), programID varchar(16), sequence int, replacementID varchar(16), duration int, valid_date decimal(20,3), expire_date decimal(20,3))  
if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:GetReplacementRules", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "GetReplacementRules");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nReplacementRulesArraySize) // have to expand
				{
					CReplacementRule** ppNew = new CReplacementRule*[m_nReplacementRulesArraySize + STREAMDATA_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nReplacementRulesArraySize += STREAMDATA_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(CReplacementRule*)*m_nReplacementRulesArraySize);
						if(m_ppReplacementRules)
						{
							memcpy(ppNew, m_ppReplacementRules, sizeof(CReplacementRule*)*nIndex); // it's an array of pointers
						}
						m_ppReplacementRules = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppReplacementRules==NULL)
				{
					m_nReplacementRulesArraySize = 0;
					m_nReplacementRules=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nReplacementRules)
				{
					m_ppReplacementRules[nIndex] = new CReplacementRule;  // guaranteed that there will be array space, above.
				}

				if(m_ppReplacementRules[nIndex])
				{
// removed all length validation.
//if something is "blank" then let it be - it has to overwrite any old data.

					CString szTemp;
	//SELECT ID, programID, sequence, replacementID, duration, valid_date, expire_date
					prs->GetFieldValue("ID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppReplacementRules[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("programID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						if(m_ppReplacementRules[nIndex]->m_pszProgramID != NULL)
						{
							free(m_ppReplacementRules[nIndex]->m_pszProgramID);
						}
						m_ppReplacementRules[nIndex]->m_pszProgramID = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppReplacementRules[nIndex]->m_pszProgramID)
						{
							strcpy(m_ppReplacementRules[nIndex]->m_pszProgramID, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("sequence", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppReplacementRules[nIndex]->m_nSequence = atoi(szTemp);
					}
					prs->GetFieldValue("replacementID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						if(m_ppReplacementRules[nIndex]->m_pszReplacementID != NULL)
						{
							free(m_ppReplacementRules[nIndex]->m_pszReplacementID);
						}
						m_ppReplacementRules[nIndex]->m_pszReplacementID = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppReplacementRules[nIndex]->m_pszReplacementID)
						{
							strcpy(m_ppReplacementRules[nIndex]->m_pszReplacementID, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("duration", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppReplacementRules[nIndex]->m_nDuration = atoi(szTemp);

						// following line is so that we do not lose a frame due to rounding down 33.3333 to 33 and 66.6666 to 66.
						if(m_ppReplacementRules[nIndex]->m_nDuration%10) m_ppReplacementRules[nIndex]->m_nDuration++;


						if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
						{
//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp

							if(g_data.m_ucTimeCodeBits&0x80) // drop frame...
							{
								m_ppReplacementRules[nIndex]->m_nDurationInFrames = g_data.m_timeconv.MStoF(g_data.m_timeconv.NDFMStoDFMS( m_ppReplacementRules[nIndex]->m_nDuration ), 29.97) ;
							}
							else
							{
								m_ppReplacementRules[nIndex]->m_nDurationInFrames = g_data.m_timeconv.MStoF( m_ppReplacementRules[nIndex]->m_nDuration , ((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) ;
							}
						}
						else  // system time
						{
							if(g_settings.m_bDF) // drop frame...
							{
								m_ppReplacementRules[nIndex]->m_nDurationInFrames = g_data.m_timeconv.MStoF(g_data.m_timeconv.NDFMStoDFMS( m_ppReplacementRules[nIndex]->m_nDuration ), 29.97) ;
							}
							else
							{
								m_ppReplacementRules[nIndex]->m_nDurationInFrames = g_data.m_timeconv.MStoF( m_ppReplacementRules[nIndex]->m_nDuration , g_settings.m_nFrameRate) ;
							}
						}



					}
					prs->GetFieldValue("valid_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppReplacementRules[nIndex]->m_dblValidDate = atof(szTemp);
					}
					prs->GetFieldValue("expire_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppReplacementRules[nIndex]->m_dblExpireDate = atof(szTemp);
					}
				}


if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_REPLRULES)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "StreamData:debug", "RR%03d ID %s, seq %d, ReplID %s, Dur %d, %.3f-%.3f", 
	nIndex, 
	m_ppReplacementRules[nIndex]->m_pszProgramID,
	m_ppReplacementRules[nIndex]->m_nSequence,
	m_ppReplacementRules[nIndex]->m_pszReplacementID,
	m_ppReplacementRules[nIndex]->m_nDuration, // in ideal MS
	m_ppReplacementRules[nIndex]->m_dblValidDate,
	m_ppReplacementRules[nIndex]->m_dblExpireDate
	
	);

				nIndex++;
				prs->MoveNext();
			}

			if(nIndex<m_nReplacementRules)
			{
				int i=nIndex;
				while(i<m_nReplacementRules)
				{
					if(m_ppReplacementRules[i]) delete m_ppReplacementRules[i];
					m_ppReplacementRules[i] = NULL;
					i++;
				}
			}
			m_nReplacementRules = nIndex;

			prs->Close();
			delete prs;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_REPLRULES)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "StreamData:debug", "RR: %d", m_nReplacementRules	);



			return nIndex;
		}
	}
	return TABULATOR_ERROR;
}

int CStreamDataData::GetDescriptionPatterns()
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		// unfortunately, I cannot just get the ones within the valid dates, since time progresses, and I only get this table when it changes.
		// I have to weed out the ones as I go....
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, programID, pattern, valid_date, expire_date FROM %s ORDER BY ID", 
			((g_settings.m_pszDescriptionPattern)&&(strlen(g_settings.m_pszDescriptionPattern)))?g_settings.m_pszDescriptionPattern:"DescriptionPattern"
			); 

//create table DescriptionPattern (ID int identity(1,1), programID varchar(16), pattern varchar(128), valid_date decimal(20,3), expire_date decimal(20,3))  

if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:GetDescriptionPatterns", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "GetDescriptionPatterns");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nDescriptionPatternsArraySize) // have to expand
				{
					CDescriptionPattern** ppNew = new CDescriptionPattern*[m_nDescriptionPatternsArraySize + STREAMDATA_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nDescriptionPatternsArraySize += STREAMDATA_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(CDescriptionPattern*)*m_nDescriptionPatternsArraySize);
						if(m_ppDescriptionPatterns)
						{
							memcpy(ppNew, m_ppDescriptionPatterns, sizeof(CDescriptionPattern*)*nIndex); // it's an array of pointers
						}
						m_ppDescriptionPatterns = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppDescriptionPatterns==NULL)
				{
					m_nDescriptionPatternsArraySize = 0;
					m_nDescriptionPatterns=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nDescriptionPatterns)
				{
					m_ppDescriptionPatterns[nIndex] = new CDescriptionPattern;  // guaranteed that there will be array space, above.
				}

				if(m_ppDescriptionPatterns[nIndex])
				{
					CString szTemp;
// removed all length validation. EXCEPT for pattern - that you need
//if something is "blank" then let it be - it has to overwrite any old data.
	//SELECT ID, programID, pattern, valid_date, expire_date
					prs->GetFieldValue("ID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppDescriptionPatterns[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("programID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						if(m_ppDescriptionPatterns[nIndex]->m_pszProgramID != NULL)
						{
							free(m_ppDescriptionPatterns[nIndex]->m_pszProgramID);
						}
						m_ppDescriptionPatterns[nIndex]->m_pszProgramID = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppDescriptionPatterns[nIndex]->m_pszProgramID)
						{
							strcpy(m_ppDescriptionPatterns[nIndex]->m_pszProgramID, szTemp.GetBuffer(0));
						}
					}
					prs->GetFieldValue("pattern", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						if(m_ppDescriptionPatterns[nIndex]->m_pszPattern != NULL)
						{
							free(m_ppDescriptionPatterns[nIndex]->m_pszPattern);
						}
						m_ppDescriptionPatterns[nIndex]->m_pszPattern = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppDescriptionPatterns[nIndex]->m_pszPattern)
						{
							strcpy(m_ppDescriptionPatterns[nIndex]->m_pszPattern, szTemp.GetBuffer(0));
						}
					}
					else
					{
						if(m_ppDescriptionPatterns[nIndex]->m_pszPattern != NULL)
						{
							free(m_ppDescriptionPatterns[nIndex]->m_pszPattern);
						}
						m_ppDescriptionPatterns[nIndex]->m_pszPattern = NULL;
					}
					prs->GetFieldValue("valid_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppDescriptionPatterns[nIndex]->m_dblValidDate = atof(szTemp);
					}
					prs->GetFieldValue("expire_date", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppDescriptionPatterns[nIndex]->m_dblExpireDate = atof(szTemp);
					}
				}

if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_DESCPATTERNS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "StreamData:debug", "DP%03d ID %s, Pattern %s, %.3f-%.3f", 
	nIndex, 
	m_ppDescriptionPatterns[nIndex]->m_pszProgramID,
	m_ppDescriptionPatterns[nIndex]->m_pszPattern,
	m_ppDescriptionPatterns[nIndex]->m_dblValidDate,
	m_ppDescriptionPatterns[nIndex]->m_dblExpireDate
	
	);


				nIndex++;
				prs->MoveNext();
			}

			if(nIndex<m_nDescriptionPatterns)
			{
				int i=nIndex;
				while(i<m_nDescriptionPatterns)
				{
					if(m_ppDescriptionPatterns[i]) delete m_ppDescriptionPatterns[i];
					m_ppDescriptionPatterns[i] = NULL;
					i++;
				}
			}

			m_nDescriptionPatterns = nIndex;

			prs->Close();
			delete prs;


if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_DESCPATTERNS)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, "StreamData:debug", "DP: %d", m_nDescriptionPatterns	);



			return nIndex;
		}
	}
	return TABULATOR_ERROR;
}


int CStreamDataData::GetClassificationRules()
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT ID, pattern, col_name, event_type FROM %s ORDER BY event_type", // if we get ads first, those are easier to actuate, so let's prioritize them
			((g_settings.m_pszClassificationRules)&&(strlen(g_settings.m_pszClassificationRules)))?g_settings.m_pszClassificationRules:"ClassificationRules"
			); 

//create table ClassificationRules (ID int identity(1,1), pattern varchar(128), col_name varchar(64), event_type int)  

if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:GetClassificationRules", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "GetClassificationRules");  Sleep(250); //(Dispatch message)

// we are just going to blow away the whole array and reload

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, errorstring);
		if(prs)
		{
			// OK, something to do.
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				if(nIndex >= m_nClassificationRulesArraySize) // have to expand
				{
					CClassificationRule** ppNew = new CClassificationRule*[m_nClassificationRulesArraySize + STREAMDATA_DATA_ARRAY_INCREMENT];
					if(ppNew)
					{
						m_nClassificationRulesArraySize += STREAMDATA_DATA_ARRAY_INCREMENT;
						memset(ppNew, 0, sizeof(CClassificationRule*)*m_nClassificationRulesArraySize);
						if(m_ppClassificationRules)
						{
							memcpy(ppNew, m_ppClassificationRules, sizeof(CClassificationRule*)*nIndex); // it's an array of pointers
						}
						m_ppClassificationRules = ppNew;
					}
					else
					{
						// error, cannot continue.
						prs->Close();
						delete prs;
						return TABULATOR_ERROR;
					}
				}

				if(m_ppClassificationRules==NULL)
				{
					m_nClassificationRulesArraySize = 0;
					m_nClassificationRules=-1; // will force re-init;
					prs->Close();
					delete prs;
					return TABULATOR_ERROR;
				}

				if(nIndex>=m_nClassificationRules)
				{
					m_ppClassificationRules[nIndex] = new CClassificationRule;  // guaranteed that there will be array space, above.
				}

				if(m_ppClassificationRules[nIndex])
				{
					CString szTemp;
// removed all length validation. EXCEPT for pattern - that you need
//if something is "blank" then let it be - it has to overwrite any old data.
	//SELECT ID, pattern, col_name, event_type 
					prs->GetFieldValue("ID", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppClassificationRules[nIndex]->m_nID = atoi(szTemp);
					}
					prs->GetFieldValue("pattern", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						if(m_ppClassificationRules[nIndex]->m_pszPattern != NULL)
						{
							free(m_ppClassificationRules[nIndex]->m_pszPattern);
						}
						m_ppClassificationRules[nIndex]->m_pszPattern = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppClassificationRules[nIndex]->m_pszPattern)
						{
							strcpy(m_ppClassificationRules[nIndex]->m_pszPattern, szTemp.GetBuffer(0));
						}
					}
					else
					{
						if(m_ppClassificationRules[nIndex]->m_pszPattern != NULL)
						{
							free(m_ppClassificationRules[nIndex]->m_pszPattern);
						}
						m_ppClassificationRules[nIndex]->m_pszPattern = NULL;
					}

					prs->GetFieldValue("col_name", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						if(m_ppClassificationRules[nIndex]->m_pszColName != NULL)
						{
							free(m_ppClassificationRules[nIndex]->m_pszColName);
						}
						m_ppClassificationRules[nIndex]->m_pszColName = (char*)malloc(szTemp.GetLength()+1);
						if(m_ppClassificationRules[nIndex]->m_pszColName)
						{
							strcpy(m_ppClassificationRules[nIndex]->m_pszColName, szTemp.GetBuffer(0));
						}
					}
					else
					{
						if(m_ppClassificationRules[nIndex]->m_pszColName != NULL)
						{
							free(m_ppClassificationRules[nIndex]->m_pszColName);
						}
						m_ppClassificationRules[nIndex]->m_pszColName = NULL;
					}
					prs->GetFieldValue("event_type", szTemp);//HARDCODE
//					if(szTemp.GetLength())
					{
						m_ppClassificationRules[nIndex]->m_nEventType = atoi(szTemp);
					}
				}

if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_CLASSRULES)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:GetClassificationRules", "CR%d [%s][%s] %d", 
		nIndex,
		m_ppClassificationRules[nIndex]->m_pszColName?m_ppClassificationRules[nIndex]->m_pszColName:"(null)",
		m_ppClassificationRules[nIndex]->m_pszPattern?m_ppClassificationRules[nIndex]->m_pszPattern:"(null)",
		m_ppClassificationRules[nIndex]->m_nEventType
		); // Sleep(250); //(Dispatch message)
}


				nIndex++;
				prs->MoveNext();
			}


			if(nIndex<m_nClassificationRules)
			{
				int i=nIndex;
				while(i<m_nClassificationRules)
				{
					if(m_ppClassificationRules[i]) delete m_ppClassificationRules[i];
					m_ppClassificationRules[i] = NULL;
					i++;
				}
			}

			m_nClassificationRules = nIndex;

			prs->Close();
			delete prs;

			return nIndex;
		}
	}
	return TABULATOR_ERROR;
}
*/
/*
int CStreamDataData::CountCurrentReplacementRules(double dblTime)// use system time
{
	if(m_ppReplacementRules)
	{
		int nCount=0;
		int i=0;
		while(i<m_nReplacementRules)
		{
			if(m_ppReplacementRules[i])
			{

				if(
					  (dblTime < m_ppReplacementRules[i]->m_dblExpireDate)  // not <= because = it's expired now
					&&(dblTime >= m_ppReplacementRules[i]->m_dblValidDate)  //  is >= because = means it's valid now
					)
				{
					nCount++;
				}
			}

			i++;
		}
		return nCount;
	}
	return 0;
}

*/

int CStreamDataData::ReportAutomationEvents()
{

	char pszSource[MAX_PATH];
	_snprintf(pszSource, MAX_PATH-1, "%s:ReportAutomationEvents", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
	if((m_ppAutoEvents)&&(m_nAutoEvents)&&(g_ptabmsgr))
	{
		// match to preserve previously calc'd output vars
		int nLastFound = -1;
		unsigned long ulTCoat;
		unsigned long ulTCdur;
		CTimeConvert tcv;

		unsigned long ulTimeCode;
		if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
		{
			EnterCriticalSection(&g_data.m_critTimeCode);
			ulTimeCode = g_data.m_ulTimeCode;
//							ulTimeCodeElapsed = g_data.m_ulTimeCodeElapsed;  // leave this for now
			LeaveCriticalSection(&g_data.m_critTimeCode);
		}
		else
		{
			// use system time
			_timeb timestamp;
			_ftime(&timestamp);
			int nTC = ((timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))%86400)*1000 + timestamp.millitm;
			ulTimeCode = tcv.MStoBCDHMSF(nTC, &ulTimeCode, g_settings.m_nFrameRate);
		}



		
		g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "************************ %d events, logging %d at TC %08x", m_nAutoEvents, (g_settings.m_nAutomationLoggingLookahead>0)?g_settings.m_nAutomationLoggingLookahead:m_nAutoEvents, ulTimeCode);
		int i=0;
		while (
						(i<m_nAutoEvents)
					&&(
							(g_settings.m_nAutomationLoggingLookahead<=0)
						||(
								(g_settings.m_nAutomationLoggingLookahead>0)
							&&(i<g_settings.m_nAutomationLoggingLookahead)
							)
						)
					)
		{
			if(m_ppAutoEvents[i])
			{
				tcv.MStoBCDHMSF(m_ppAutoEvents[i]->m_ulOnAirTimeMS, &ulTCoat,(double)g_settings.m_nFrameRate); // using ideal
				tcv.MStoBCDHMSF(m_ppAutoEvents[i]->m_ulDurationMS, &ulTCdur,(double)g_settings.m_nFrameRate); // using ideal

				g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "%04d oat:%08x dur:%08x status:%04x type:%04x [%s][%s]", 
					i,
					ulTCoat,ulTCdur, 
					m_ppAutoEvents[i]->m_usStatus,
					m_ppAutoEvents[i]->m_usType,
					m_ppAutoEvents[i]->m_pszID?m_ppAutoEvents[i]->m_pszID:"(null)",
					m_ppAutoEvents[i]->m_pszTitle?m_ppAutoEvents[i]->m_pszTitle:"(null)"
					);//  Sleep(250); //(Dispatch message)

			}
			i++;
		}
		g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "************************");

		return i;
	}
	else
	{
		if(g_ptabmsgr)
			g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "There are no automation events.");

		return 0;
	}
}






int CStreamDataData::Query_Ready()
{
	if(m_bInitalEventGetSucceeded) return TABULATOR_SUCCESS;
	return TABULATOR_ERROR;
}

/*
char* CStreamDataData::FormatDTMF(int nDurationMS)
{
	if(nDurationMS<0) return NULL;
	char pszDTMF[MAX_PATH];

	char pszSource[MAX_PATH];
	_snprintf(pszSource, MAX_PATH-1, "%s:FormatDTMF", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");


	int nDur = (nDurationMS	+ (g_settings.m_bRoundToNearestUnit?(g_settings.m_nDurationUnitDivisor/2):0))
		/ g_settings.m_nDurationUnitDivisor;

if(g_ptabmsgr)
			g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "Duration %d received; %03d calculated",nDurationMS, nDur);

	if((g_settings.m_pszDTMFFormat)&&(strlen(g_settings.m_pszDTMFFormat)>0))
	{
		_snprintf(pszDTMF, MAX_PATH, g_settings.m_pszDTMFFormat, nDur);
	}
	else
	{
		_snprintf(pszDTMF, MAX_PATH, "D%03d", nDur);
	}

if(g_ptabmsgr)g_ptabmsgr->DM(MSG_ICONHAND, NULL, pszSource, "returning %s",pszDTMF);

	nDur = strlen(pszDTMF);
	if(nDur>0)
	{
		char* pch = (char*)malloc(nDur+1);
		if(pch)
		{
			strcpy(pch, pszDTMF);
			return pch;
		}
	}
	return NULL;
}
*/

int CStreamDataData::IdealAddTimeAndDuration(int nStart, int nDuration)
{

	if((g_settings.m_bUseTimeCode)&&(g_data.m_bTimeCodeThreadStarted)&&(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE))
	{
//				((g_data.m_ucTimeCodeBits&0x80)?29.97:((g_data.m_ucTimeCodeBits&0x02)?25.0:30.0)) // this is for df comp

		if(g_data.m_ucTimeCodeBits&0x80) // drop frame...
		{
	return m_timeconv.DFMStoNDFMS( m_timeconv.NDFMStoDFMS(nStart) + m_timeconv.NDFMStoDFMS(nDuration) );
		}
		else
		{
	return nStart + nDuration;
		}

	}
	else  // system time
	{
		if(g_settings.m_bDF) // drop frame...
		{
	return m_timeconv.DFMStoNDFMS( m_timeconv.NDFMStoDFMS(nStart) + m_timeconv.NDFMStoDFMS(nDuration) );
		}
		else
		{
	return nStart + nDuration;
		}
	}

}

/*
int CStreamDataData::TestPattern(char* pszValue, char* pszPattern)
{
	if((pszValue)&&(pszPattern)&&(strlen(pszPattern)>0)&&(m_pdbOutput)&&(m_pdbOutputConn))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		// have to encode quotes.
		char* pchEncodedValue   = m_pdbOutput->EncodeQuotes(pszValue);
		char* pchEncodedPattern = m_pdbOutput->EncodeQuotes(pszPattern);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "select case when '%s' like '%s' then 'YES' else 'NO' end as col1",
						(pchEncodedValue?pchEncodedValue:pszValue),
						(pchEncodedPattern?pchEncodedPattern:pszPattern)
				);
		
		if(pchEncodedValue) free(pchEncodedValue);
		if(pchEncodedPattern) free(pchEncodedPattern);


if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_PATTERN|STREAMDATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:TestPattern", "Checking event %s against %s with %s", pszValue, pszPattern, szSQL); // Sleep(250); //(Dispatch message)
}

		EnterCriticalSection(&m_critOutputSQL);
		CRecordset* prs = m_pdbOutput->Retrieve(m_pdbOutputConn, szSQL, errorstring);
		if(prs)
		{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "Got Destinations");  Sleep(250); //(Dispatch message)
			if(!prs->IsEOF())
			{
				CString szTemp;
				prs->GetFieldValue("col1", szTemp);//HARDCODE
				if(szTemp.GetLength())
				{
					int nReturn = STREAMDATA_PATTERN_NO_MATCH;
					if(szTemp.CompareNoCase("YES")==0)
					{
if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_PATTERN)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:TestPattern", "Query returned YES"); // Sleep(250); //(Dispatch message)
}

						nReturn = STREAMDATA_PATTERN_MATCH;
						
					}
					prs->Close();
					delete prs;

					LeaveCriticalSection(&m_critOutputSQL);
					
					return nReturn;
				}
			}
			prs->Close();
			delete prs;
		}
		else
		{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "StreamData:TestPattern", "ERROR: %s", errorstring); // Sleep(250); //(Dispatch message)
		}
		LeaveCriticalSection(&m_critOutputSQL);
	}
if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_PATTERN)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:TestPattern", "ERROR"); // Sleep(250); //(Dispatch message)
}
	return STREAMDATA_PATTERN_ERROR;
}
*/








int CStreamDataData::RefreshTabulatorEvents( CTabulatorEventArray* pEvents, char* pszToken )
{
	if((pEvents)&&(pszToken))
	{
		EnterCriticalSection(&(pEvents->m_critEvents));

		pEvents->DeleteEvents();
		int e=0;
		int n=0;
		bool bFound=false;
		bool bAllocated=false;
		while(e<g_data.m_pEvents->m_nEvents)
		{
			if(g_data.m_pEvents->m_ppEvents[e]->m_szName.Compare(pszToken)==0)
			{
				bFound=true;
				if(g_data.m_pEvents->m_nLastEventMod!=pEvents->m_nLastEventMod)
				{
					pEvents->m_nLastEventMod = g_data.m_pEvents->m_nLastEventMod;
				}
			}
			else
			{
				if(bFound) break; // just stop it there. - must be contiguous
			}

			if(bFound)
			{
				if(!bAllocated)
				{
					pEvents->m_ppEvents = new CTabulatorEvent*[g_settings.m_nTriggerBuffer];
					bAllocated = true; // check for errors someday
				}
				pEvents->m_ppEvents[pEvents->m_nEvents] = new CTabulatorEvent;
				if(pEvents->m_ppEvents[pEvents->m_nEvents])
				{
					n++;
					*(pEvents->m_ppEvents[pEvents->m_nEvents]) = *(g_data.m_pEvents->m_ppEvents[e]);
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szName = g_data.m_pEvents->m_ppEvents[e]->m_szName;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szHost = g_data.m_pEvents->m_ppEvents[e]->m_szHost;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szSource = g_data.m_pEvents->m_ppEvents[e]->m_szSource;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szScene = g_data.m_pEvents->m_ppEvents[e]->m_szScene;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szIdentifier = g_data.m_pEvents->m_ppEvents[e]->m_szIdentifier;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szValue = g_data.m_pEvents->m_ppEvents[e]->m_szValue;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szParamDependencies = g_data.m_pEvents->m_ppEvents[e]->m_szParamDependencies;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szDestModule = g_data.m_pEvents->m_ppEvents[e]->m_szDestModule;
					pEvents->m_ppEvents[pEvents->m_nEvents]->m_szLayer = g_data.m_pEvents->m_ppEvents[e]->m_szLayer;
if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_EVENTS)) 
g_ptabmsgr->DM(MSG_ICONNONE, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "in event %02d: %s, %.3f: %s:%s:%s",
		 pEvents->m_nEvents,
		 pEvents->m_ppEvents[pEvents->m_nEvents]->m_szName,
		 pEvents->m_ppEvents[pEvents->m_nEvents]->m_dblTriggerTime,
		 pEvents->m_ppEvents[pEvents->m_nEvents]->m_szScene,
		 pEvents->m_ppEvents[pEvents->m_nEvents]->m_szIdentifier,
		 pEvents->m_ppEvents[pEvents->m_nEvents]->m_szValue
		 );// Sleep(100); //(Dispatch message)
					pEvents->m_nEvents++;
				}
			}

			e++;
		}
		LeaveCriticalSection(&(pEvents->m_critEvents));


		return n;
	}
	return TABULATOR_ERROR;
}

//int CStreamDataData::UpdateGeneric(CSCTE104MultiMessage* m_pscte104Msg, unsigned char uchSource, int nPrerollMS, int nDurationMS, char* pszDTMF, bool bSuppressIncrement)
int CStreamDataData::UpdateGeneric(CSCTE104MultiMessage* m_pscte104Msg, unsigned char uchSource, int nPrerollMS, int nDurationMS, char* pszIDString, bool bSuppressIncrement)
{
	if(
		  (m_pscte104Msg)
		&&(m_pscte104Msg->m_ppscte104Ops)
		&&(m_pscte104Msg->m_scte104Hdr.NumberOfOps>=1)
//		&&(m_pscte104Msg->m_scte104Hdr.NumberOfOps>=2)
		&&(m_pscte104Msg->m_ppscte104Ops[0])
//		&&(m_pscte104Msg->m_ppscte104Ops[1])
		)
	{
/*
		if(!bSuppressIncrement)
		{
			IncrementSpliceEventID();
//			IncrementUniqueProgID();
		}
*/

		SCTE104_splice_request_data_t* psrd = (SCTE104_splice_request_data_t*) m_pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr;

		if(psrd)
		{
		// just set it all to zero.

		if(m_pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr)
		{
//AfxMessageBox("HERE 3");
			memset(m_pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr, 0x00, 0x0e); // nulls

//AfxMessageBox("HERE 4");
//				psrd->auto_return_flag = 1;  // everything gets this
		}

/*

reserved 0
spliceStart_normal 1
spliceStart_immediate 2
spliceEnd_normal 3
spliceEnd_immediate 4
splice_cancel 5

11.3 Minimum Pre-roll Time Supported by this Interface
In compliance with the requirements of SCTE 35 [1], and in keeping with the advice of SCTE 67, the minimum non-zero value of pre-roll time shall be 4000 milliseconds for a splice_request. A zero value may be sent if splice_insert_type is spliceStart_immediate or spliceEnd_immediate. If the Automation System is somehow notified of an event with less time than the minimum, it might count itself down to the trigger time and request a spliceStart_immediate operation. 

*/


		if(nPrerollMS<=0) //special case for zero preroll, use splice immediate.
		{
			psrd->splice_insert_type = 2; //(spliceStart_immediate)
			nPrerollMS = 0; // just set this to zero
		}
		else
		{
			// then reset splice type to 1
			psrd->splice_insert_type = 1; //(spliceStart_normal)
		}
//
//		uchSource // will be either = 1 for Invidi, or 2 for Thunder bay, make it so

//		psrd->unique_program_id[0] = 0;//(unsigned char)((us_unique_program_id&0xff00)>>8);    // most significant bit first
		psrd->unique_program_id[1] = uchSource;//(unsigned char)((us_unique_program_id&0x00ff));    // most significant bit first


		if(nPrerollMS>=0) // override the preroll value
		{
			psrd->pre_roll_time[0] = (unsigned char)((nPrerollMS&0x0000ff00)>>8);
			psrd->pre_roll_time[1] = (unsigned char)((nPrerollMS&0x000000ff));
		}

		if(nDurationMS>=0) // override the duration value
		{
			nDurationMS /= 100;  // expressed in tenths of a second 1.0.2.2

			psrd->break_duration[0] = (unsigned char)((nDurationMS&0x0000ff00)>>8);
			psrd->break_duration[1] = (unsigned char)((nDurationMS&0x000000ff));

		}
/*
		if(nPrerollMS>=0) // override the preroll value
		{
			SCTE104_time_signal_request_data_t* ptsrd = (SCTE104_time_signal_request_data_t*) m_pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr;

			if(ptsrd)
			{
				ptsrd->pre_roll_time[0] = (unsigned char)((nPrerollMS&0x0000ff00)>>8);
				ptsrd->pre_roll_time[1] = (unsigned char)((nPrerollMS&0x000000ff));
			}
		}

		SCTE104_insert_segmentation_descriptor_request_data_t* psdrd = (SCTE104_insert_segmentation_descriptor_request_data_t*) m_pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr;
		if(psdrd)
		{
			psdrd->segmentation_event_id[0] = (unsigned char)((m_ul_splice_event_id&0xff000000)>>24);    // most significant bit first
			psdrd->segmentation_event_id[1] = (unsigned char)((m_ul_splice_event_id&0x00ff0000)>>16);    // most significant bit first
			psdrd->segmentation_event_id[2] = (unsigned char)((m_ul_splice_event_id&0x0000ff00)>>8);    // most significant bit first
			psdrd->segmentation_event_id[3] = (unsigned char)((m_ul_splice_event_id&0x000000ff));    // most significant bit first


			//The time, rounded up to whole seconds (in tenths of a second)
			// that's the def of this SCTE object so we cna hardcode whole seconds.

			if(g_settings.m_bRoundToNearestUnit)
			{
					nDurationMS += 500;  // rounding
			}
			nDurationMS /= 1000; // to seconds

//			nDurationMS *= 10;  // expressed in tenths - not for SCTE104_insert_segmentation_descriptor_request_data_t, that was for FNC

			psdrd->duration[0] = (unsigned char)((nDurationMS&0x0000ff00)>>8);
			psdrd->duration[1] = (unsigned char)((nDurationMS&0x000000ff));

			if(pszIDString)
			{
				psdrd->segmentation_upid_length = (unsigned char)strlen(pszIDString)+2;
			}
			else
			{
				psdrd->segmentation_upid_length = 2;
			}
			char buffer[256];

			switch(uchSource)
			{
			case STREAMDATA_SOURCE_NATIONAL: sprintf(buffer, "N|%s", pszIDString?pszIDString:""); break;
			case STREAMDATA_SOURCE_LOCAL: sprintf(buffer, "L|%s", pszIDString?pszIDString:""); break; 
			default: sprintf(buffer, "U|%s", pszIDString?pszIDString:""); break;  //U for unknown
			}


			// re-zero the part of the buffer that might have old data.
//			memset(m_pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr, 0x00, sizeof(SCTE104_insert_segmentation_descriptor_request_data_t)+256); // nulls
			memset(&(psdrd->segmentation_upid_length)+1, 0x00, sizeof(SCTE104_insert_segmentation_descriptor_request_data_t)+256 - (&(psdrd->segmentation_upid_length)+1 - m_pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr)); // nulls


			memcpy(&(psdrd->segmentation_upid_length)+1, buffer, psdrd->segmentation_upid_length); // copy the ID in.

			// reset the length.
			m_pscte104Msg->m_ppscte104Ops[1]->OpDataLengthLow  = sizeof(SCTE104_insert_segmentation_descriptor_request_data_t) + psdrd->segmentation_upid_length;			// xx		data_length [low order byte]			
//			m_pscte104Msg->m_ppscte104Ops[1]->OpDataLengthLow += psdrd->segmentation_upid_length;  // this is wrong, infinitely getting larger, since length was only initialized before in initialize generic

//Local -- 0x32 (Distributor Advertisement Start)
//National -- 0x30 (Provider Advertisement Start)

			memset(&psdrd->segmentation_type_id + psdrd->segmentation_upid_length, uchSource, 1); // add the one source byte
*/
			m_pscte104Msg->UpdateDataLengths();

			return TABULATOR_SUCCESS;
		}
	}
	return TABULATOR_ERROR;

}

int CStreamDataData::ParseData(char* pszData, char** ppszProgID)
{
/*
#define STREAMDATA_EVENTDATA_NOMATCH					0x00   // there are no tokens in the data
#define STREAMDATA_EVENTDATA_PROGID						0x01   // a prog ID exists in the data
#define STREAMDATA_EVENTDATA_NATIONAL_START		0x02   // the national start token exists in the data
#define STREAMDATA_EVENTDATA_NATIONAL_END			0x04   // the national end token exists in the data
#define STREAMDATA_EVENTDATA_LOCAL_START			0x10   // the local start token exists in the data
#define STREAMDATA_EVENTDATA_LOCAL_END				0x20   // the local end token exists in the data
*/

	if((pszData)&&(strlen(pszData))>0)
	{
		int nRV = STREAMDATA_EVENTDATA_NOMATCH;
		bool bFound = false;
		int nOffset=0; //search index offset.
		int nLenData=strlen(pszData); 
		int nLenTag=0; 

/*
		//check for program tag.
		if(g_settings.m_pszSCTE104ProgramTag)
		{
			nLenTag = strlen(g_settings.m_pszSCTE104ProgramTag);
			while((!bFound)&&(nOffset+nLenTag <= nLenData))
			{
//g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "PARSE", "parsing data for %s with %d+%d=%d %d [%s]",g_settings.m_pszSCTE104ProgramTag, nOffset,nLenTag, nOffset+nLenTag, nLenData,pszData);

				char* ch = strstr(pszData+nOffset, g_settings.m_pszSCTE104ProgramTag);
				if(ch!=NULL) //found
				{
					// check that the prior char is a delimiter
					if(ch>pszData+nOffset)
					{
						if((*(ch-1) == ',')||(*(ch-1) == '>')||(*(ch-1) == ' ')||(*(ch-1) == '|')) // good to go
						{
							// now extract the value.
							ch += nLenTag;// skip past the tag itself
							bFound=true;
							char buffer[256];
							int i=0;
							while(i<255)
							{
								if(
									  ( (*(ch+i)>47)&&(*(ch+i)<58) )  // numerical
									||( (*(ch+i)>64)&&(*(ch+i)<123) )  // alpha including underscore etc.
									)
								{
									buffer[i]=*(ch+i);
								}
								else
								{
									buffer[i]=0;
									
									nRV |= STREAMDATA_EVENTDATA_PROGID;
									break;
								}
								
								i++;
							}
							

							if((ppszProgID)&&(nRV&STREAMDATA_EVENTDATA_PROGID)) // allocate a buffer if there is a place to receive it.
							{
								char* pch = (char*)malloc(strlen(buffer)+1);
								if(pch)
								{
									strcpy(pch, buffer);
								}
								*ppszProgID = pch;

							}
						}
						else
						{
							nOffset = ch-pszData + nLenTag;  continue;
						}
					}
					else // right at the start?  doesnt make sense as the data buffer has <userData> at the beginning
					{
						nOffset = nLenTag;  continue;
					}
				}
				else // string not found
				{
					break;
				}
			}
		}
*/
		//check for splice inpoint tag.
		if(g_settings.m_pszSCTE104SpliceInpointID)
		{
			bFound = false;
			nOffset=0;
			nLenTag = strlen(g_settings.m_pszSCTE104SpliceInpointID);
			while((!bFound)&&(nOffset+nLenTag <= nLenData))
			{
//g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "PARSE", "parsing data for %s with %d+%d=%d %d [%s]",g_settings.m_pszSCTE104NationalInpointID, nOffset,nLenTag, nOffset+nLenTag, nLenData,pszData);
				char* ch = strstr(pszData+nOffset, g_settings.m_pszSCTE104SpliceInpointID);
				if(ch!=NULL) //found
				{
					// check that the prior char is a delimiter, and that the char after is also a delimiter
					if(ch>pszData+nOffset)
					{
						if(
							  ((*(ch-1) == ',')||(*(ch-1) == '>')||(*(ch-1) == ' ')||(*(ch-1) == '|')) // good to go on prior
							&&((*(ch+nLenTag) == ',')||(*(ch+nLenTag) == '<')||(*(ch+nLenTag) == ' ')||(*(ch+nLenTag) == '|')) // good to go on after
							)
						{
							bFound=true;
							nRV |= STREAMDATA_EVENTDATA_SPLICE_ID;
						}
						else
						{
							nOffset = ch-pszData + nLenTag;  continue;
						}
					}
					else // right at the start?  doesnt make sense as the data buffer has <userData> at the beginning
					{
						nOffset = nLenTag;  continue;
					}
				}
				else // string not found
				{
					break;
				}
			}
		}


/*

		//check for national inpoint tag.
		if(g_settings.m_pszSCTE104NationalInpointID)
		{
			bFound = false;
			nOffset=0;
			nLenTag = strlen(g_settings.m_pszSCTE104NationalInpointID);
			while((!bFound)&&(nOffset+nLenTag <= nLenData))
			{
//g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "PARSE", "parsing data for %s with %d+%d=%d %d [%s]",g_settings.m_pszSCTE104NationalInpointID, nOffset,nLenTag, nOffset+nLenTag, nLenData,pszData);
				char* ch = strstr(pszData+nOffset, g_settings.m_pszSCTE104NationalInpointID);
				if(ch!=NULL) //found
				{
					// check that the prior char is a delimiter, and that the char after is also a delimiter
					if(ch>pszData+nOffset)
					{
						if(
							  ((*(ch-1) == ',')||(*(ch-1) == '>')||(*(ch-1) == ' ')||(*(ch-1) == '|')) // good to go on prior
							&&((*(ch+nLenTag) == ',')||(*(ch+nLenTag) == '<')||(*(ch+nLenTag) == ' ')||(*(ch+nLenTag) == '|')) // good to go on after
							)
						{
							bFound=true;
							nRV |= STREAMDATA_EVENTDATA_NATIONAL_START;
						}
						else
						{
							nOffset = ch-pszData + nLenTag;  continue;
						}
					}
					else // right at the start?  doesnt make sense as the data buffer has <userData> at the beginning
					{
						nOffset = nLenTag;  continue;
					}
				}
				else // string not found
				{
					break;
				}
			}
		}

		//check for national outpoint tag.
		if(g_settings.m_pszSCTE104NationalOutpointID)
		{
			bFound = false;
			nOffset=0;
			nLenTag = strlen(g_settings.m_pszSCTE104NationalOutpointID);
			while((!bFound)&&(nOffset+nLenTag <= nLenData))
			{
//g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "PARSE", "parsing data for %s with %d+%d=%d %d [%s]",g_settings.m_pszSCTE104NationalOutpointID, nOffset,nLenTag, nOffset+nLenTag, nLenData,pszData);
				char* ch = strstr(pszData+nOffset, g_settings.m_pszSCTE104NationalOutpointID);
				if(ch!=NULL) //found
				{
					// check that the prior char is a delimiter, and that the char after is also a delimiter
					if(ch>pszData+nOffset)
					{
						if(
							  ((*(ch-1) == ',')||(*(ch-1) == '>')||(*(ch-1) == ' ')||(*(ch-1) == '|')) // good to go on prior
							&&((*(ch+nLenTag) == ',')||(*(ch+nLenTag) == '<')||(*(ch+nLenTag) == ' ')||(*(ch+nLenTag) == '|')) // good to go on after
							)
						{
							bFound=true;
							nRV |= STREAMDATA_EVENTDATA_NATIONAL_END;
						}
						else
						{
							nOffset = ch-pszData + nLenTag;  continue;
						}
					}
					else // right at the start?  doesnt make sense as the data buffer has <userData> at the beginning
					{
						nOffset = nLenTag;  continue;
					}
				}
				else // string not found
				{
					break;
				}
			}
		}



		//check for local inpoint tag.
		if(g_settings.m_pszSCTE104LocalInpointID)
		{
			bFound = false;
			nOffset=0;
			nLenTag = strlen(g_settings.m_pszSCTE104LocalInpointID);
			while((!bFound)&&(nOffset+nLenTag <= nLenData))
			{
//g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "PARSE", "parsing data for %s with %d+%d=%d %d [%s]",g_settings.m_pszSCTE104LocalInpointID, nOffset,nLenTag, nOffset+nLenTag, nLenData,pszData);
				char* ch = strstr(pszData+nOffset, g_settings.m_pszSCTE104LocalInpointID);
				if(ch!=NULL) //found
				{
					// check that the prior char is a delimiter, and that the char after is also a delimiter
					if(ch>pszData+nOffset)
					{
						if(
							  ((*(ch-1) == ',')||(*(ch-1) == '>')||(*(ch-1) == ' ')||(*(ch-1) == '|')) // good to go on prior
							&&((*(ch+nLenTag) == ',')||(*(ch+nLenTag) == '<')||(*(ch+nLenTag) == ' ')||(*(ch+nLenTag) == '|')) // good to go on after
							)
						{
							bFound=true;
							nRV |= STREAMDATA_EVENTDATA_LOCAL_START;
						}
						else
						{
							nOffset = ch-pszData + nLenTag;  continue;
						}
					}
					else // right at the start?  doesnt make sense as the data buffer has <userData> at the beginning
					{
						nOffset = nLenTag;  continue;
					}
				}
				else // string not found
				{
					break;
				}
			}
		}

		//check for local outpoint tag.
		if(g_settings.m_pszSCTE104LocalOutpointID)
		{
			bFound = false;
			nOffset=0;
			nLenTag = strlen(g_settings.m_pszSCTE104LocalOutpointID);
			while((!bFound)&&(nOffset+nLenTag <= nLenData))
			{
//g_ptabmsgr->DM(MSG_ICONEXCLAMATION, NULL, "PARSE", "parsing data for %s with %d+%d=%d %d [%s]",g_settings.m_pszSCTE104LocalOutpointID, nOffset,nLenTag, nOffset+nLenTag, nLenData,pszData);
				char* ch = strstr(pszData+nOffset, g_settings.m_pszSCTE104LocalOutpointID);
				if(ch!=NULL) //found
				{
					// check that the prior char is a delimiter, and that the char after is also a delimiter
					if(ch>pszData+nOffset)
					{
						if(
							  ((*(ch-1) == ',')||(*(ch-1) == '>')||(*(ch-1) == ' ')||(*(ch-1) == '|')) // good to go on prior
							&&((*(ch+nLenTag) == ',')||(*(ch+nLenTag) == '<')||(*(ch+nLenTag) == ' ')||(*(ch+nLenTag) == '|')) // good to go on after
							)
						{
							bFound=true;
							nRV |= STREAMDATA_EVENTDATA_LOCAL_END;
						}
						else
						{
							nOffset = ch-pszData + nLenTag;  continue;
						}
					}
					else // right at the start?  doesnt make sense as the data buffer has <userData> at the beginning
					{
						nOffset = nLenTag;  continue;
					}
				}
				else // string not found
				{
					break;
				}
			}
		}
		*/

		return nRV;

	}
	return TABULATOR_ERROR;
}


int CStreamDataData::InitializeGeneric(CSCTE104MultiMessage* m_pscte104Msg)
{
	if(
		  (m_pscte104Msg)
		&&(m_pscte104Msg->m_ppscte104Ops)
		&&(m_pscte104Msg->m_scte104Hdr.NumberOfOps>=1)
//		&&(m_pscte104Msg->m_scte104Hdr.NumberOfOps>=2)
		&&(m_pscte104Msg->m_ppscte104Ops[0])
//		&&(m_pscte104Msg->m_ppscte104Ops[1])
		)
	{
		int nMalloc = 0;


		// everything gets splice_request_data
		m_pscte104Msg->m_ppscte104Ops[0]->OpId1High = 	0x01; //		opID [high order byte]						0x0101 splice_request_data
		m_pscte104Msg->m_ppscte104Ops[0]->OpId1Low = 0x01;		//		opID [low order byte]
		m_pscte104Msg->m_ppscte104Ops[0]->OpDataLengthHigh = 0x00;			// xx		data_length [high order byte]			
		m_pscte104Msg->m_ppscte104Ops[0]->OpDataLengthLow  = 0x0e;			// xx		data_length [low order byte]			

		m_pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr = new unsigned char[0x0e];
//AfxMessageBox("HERE 1");

		SCTE104_splice_request_data_t* psrd = (SCTE104_splice_request_data_t*) m_pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr;
//AfxMessageBox("HERE 2");

		if(m_pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr)
		{
			nMalloc++;
//AfxMessageBox("HERE 3");
			memset(m_pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr, 0x00, 0x0e); // nulls

//AfxMessageBox("HERE 4");
//				psrd->auto_return_flag = 1;  // everything gets this
		}

		
/*
		SCTE104_splice_request_data_t* psrd = (SCTE104_splice_request_data_t*) m_pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr;

		if(m_pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr)
		{
			nMalloc++;
			memset(m_pscte104Msg->m_ppscte104Ops[0]->proprietary_data_ptr, 0x00, 0x0e); // nulls
			psrd->auto_return_flag = 1;  // everything gets this
		}
*/

/*

		// everything gets insert_DTMF_descriptor_request_data() with 4 DTMF chars
		m_pscte104Msg->m_ppscte104Ops[1]->OpId1High = 	0x01; //		opID [high order byte]						0x0109 insert_DTMF_descriptor_request_data
		m_pscte104Msg->m_ppscte104Ops[1]->OpId1Low = 0x09;		//		opID [low order byte]

		if(nDTMFlength<=0) nDTMFlength=0;
		nDTMFlength += 2;

		m_pscte104Msg->m_ppscte104Ops[1]->OpDataLengthHigh = (unsigned char)((nDTMFlength & 0x0000ff00)>>8);			// xx		data_length [high order byte]		
		m_pscte104Msg->m_ppscte104Ops[1]->OpDataLengthLow = (unsigned char)(nDTMFlength & 0x000000ff);			// xx		data_length [high order byte]		
		

		m_pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr = new unsigned char[nDTMFlength];

		if(m_pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr)
		{
			nMalloc++;
			memset(m_pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr, 0x00, nDTMFlength); // nulls

			nDTMFlength -= 2;
			m_pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[0]=0x00; //preroll
			m_pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr[1]=(unsigned char)(nDTMFlength & 0x000000ff);	// dtmf length in bytes
		}
*/
		
/*

		m_pscte104Msg->m_ppscte104Ops[1]->OpId1High = 	0x01; //		opID [high order byte]						0x0109 insert_DTMF_descriptor_request_data, 0x010b insert_segmentation_descriptor_request_data
		m_pscte104Msg->m_ppscte104Ops[1]->OpId1Low = 0x0b;		//		opID [low order byte]

		m_pscte104Msg->m_ppscte104Ops[1]->OpDataLengthHigh = 0x00;			// xx		data_length [high order byte]			(it's not going to be >256 bytes)
		m_pscte104Msg->m_ppscte104Ops[1]->OpDataLengthLow  = sizeof(SCTE104_insert_segmentation_descriptor_request_data_t);			// xx		data_length [low order byte]			
		// have to add the variable length of the Id in later.

		//  but dont need to actually create an object, since it's all zeros.  
		// we'll just adjust the length to shorten it down once we havethe ID and put it in place.
		m_pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr = new unsigned char[sizeof(SCTE104_insert_segmentation_descriptor_request_data_t)+256]; // space for the ID.  max 256

		SCTE104_insert_segmentation_descriptor_request_data_t* psdrd = (SCTE104_insert_segmentation_descriptor_request_data_t*) m_pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr;
		if(m_pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr)
		{
			nMalloc++;
			memset(m_pscte104Msg->m_ppscte104Ops[1]->proprietary_data_ptr, 0x00, sizeof(SCTE104_insert_segmentation_descriptor_request_data_t)+256); // nulls
		}

		if(psdrd)
		{
			psdrd->segmentation_upid_type = 0x0c;  //valid for everything preceding var len ID!
		}

		if(nMalloc==2) return TABULATOR_SUCCESS;
*/
		if(nMalloc==1) return TABULATOR_SUCCESS;
		
	}
	
	return TABULATOR_ERROR;
}

/*
int CStreamDataData::InitializeNationalBreakObject()
{
	if(m_pscte104MsgNationalBreak)
	{
		if(InitializeGeneric(m_pscte104MsgNationalBreak)>=TABULATOR_SUCCESS)
		{
			if(m_pscte104MsgNationalBreak->m_ppscte104Ops)
			{
				SCTE104_time_signal_request_data_t* ptsrd = (SCTE104_time_signal_request_data_t*) m_pscte104MsgNationalBreak->m_ppscte104Ops[0]->proprietary_data_ptr;

				if(ptsrd)
				{
					ptsrd->pre_roll_time[0] = (unsigned char)((g_settings.m_nSCTE104NationalPreroll&0x0000ff00)>>8);
					ptsrd->pre_roll_time[1] = (unsigned char)((g_settings.m_nSCTE104NationalPreroll&0x000000ff));
				}

/*  do all this outside.
				SCTE104_insert_segmentation_descriptor_request_data_t* psdrd = (SCTE104_insert_segmentation_descriptor_request_data_t*) m_pscte104MsgNationalBreak->m_ppscte104Ops[1]->proprietary_data_ptr;
				if(psdrd)
				{

				}
* /
			}
		}

		return m_pscte104MsgNationalBreak->UpdateDataLengths();

	}
	return TABULATOR_ERROR;
}
*/

int CStreamDataData::InitializeSpliceRequestObject()
{
	if(m_pscte104MsgSpliceRequest)
	{
		if(InitializeGeneric(m_pscte104MsgSpliceRequest)>=TABULATOR_SUCCESS)
		{
			if(m_pscte104MsgSpliceRequest->m_ppscte104Ops)
			{
				SCTE104_splice_request_data_t* psrd = (SCTE104_splice_request_data_t*) m_pscte104MsgSpliceRequest->m_ppscte104Ops[0]->proprietary_data_ptr;

/*
				if(ptsrd)
				{
					ptsrd->pre_roll_time[0] = (unsigned char)((g_settings.m_nSCTE104LocalPreroll&0x0000ff00)>>8);
					ptsrd->pre_roll_time[1] = (unsigned char)((g_settings.m_nSCTE104LocalPreroll&0x000000ff));
				}
*/
/*  do all this outside.
				SCTE104_insert_segmentation_descriptor_request_data_t* psdrd = (SCTE104_insert_segmentation_descriptor_request_data_t*) m_pscte104MsgNationalBreak->m_ppscte104Ops[1]->proprietary_data_ptr;
				if(psdrd)
				{

				}
*/
			}
		}
		return m_pscte104MsgSpliceRequest->UpdateDataLengths();
	}
	return TABULATOR_ERROR;
}

/*

int CStreamDataData::InitializeLocalBreakObject()
{
	if(m_pscte104MsgLocalBreak)
	{
		if(InitializeGeneric(m_pscte104MsgLocalBreak)>=TABULATOR_SUCCESS)
		{
			if(m_pscte104MsgLocalBreak->m_ppscte104Ops)
			{
				SCTE104_time_signal_request_data_t* ptsrd = (SCTE104_time_signal_request_data_t*) m_pscte104MsgLocalBreak->m_ppscte104Ops[0]->proprietary_data_ptr;

				if(ptsrd)
				{
					ptsrd->pre_roll_time[0] = (unsigned char)((g_settings.m_nSCTE104LocalPreroll&0x0000ff00)>>8);
					ptsrd->pre_roll_time[1] = (unsigned char)((g_settings.m_nSCTE104LocalPreroll&0x000000ff));
				}

/*  do all this outside.
				SCTE104_insert_segmentation_descriptor_request_data_t* psdrd = (SCTE104_insert_segmentation_descriptor_request_data_t*) m_pscte104MsgNationalBreak->m_ppscte104Ops[1]->proprietary_data_ptr;
				if(psdrd)
				{

				}
* /
			}
		}
		return m_pscte104MsgLocalBreak->UpdateDataLengths();
	}
	return TABULATOR_ERROR;
}
*/


/*
int CStreamDataData::InitializeLocal060BreakObject()
{
	if(m_pscte104MsgLocal060Break)
	{
		int nDTMFLen = g_settings.m_pszSCTE104Local060EventDTMFsequence?strlen(g_settings.m_pszSCTE104Local060EventDTMFsequence):4 ;
		if(InitializeGeneric(m_pscte104MsgLocal060Break, nDTMFLen)>=TABULATOR_SUCCESS)
		{
			if(m_pscte104MsgNationalBreak->m_ppscte104Ops)
			{
				SCTE104_splice_request_data_t* psrd = (SCTE104_splice_request_data_t*) m_pscte104MsgLocal060Break->m_ppscte104Ops[0]->proprietary_data_ptr;

				if(psrd)
				{
					psrd->splice_insert_type = 1; //(spliceStart_normal)
/*
// template, do this outside with "real data"
					psrd->splice_event_id[0] = (unsigned char)((ul_splice_event_id&0x0f000000)>>24) | 0x10;    // most significant bit first
					psrd->splice_event_id[1] = (unsigned char)((ul_splice_event_id&0x00ff0000)>>16);    // most significant bit first
					psrd->splice_event_id[2] = (unsigned char)((ul_splice_event_id&0x0000ff00)>>8);    // most significant bit first
					psrd->splice_event_id[3] = (unsigned char)((ul_splice_event_id&0x000000ff));    // most significant bit first

					psrd->unique_program_id[0] = (unsigned char)((us_unique_program_id&0xff00)>>8);    // most significant bit first
					psrd->unique_program_id[1] = (unsigned char)((us_unique_program_id&0x00ff));    // most significant bit first

					psrd->pre_roll_time[0] = (unsigned char)((n&0x0000ff00)>>8);
					psrd->pre_roll_time[1] = (unsigned char)((n&0x000000ff));

* /
					// 0600 = 0x0258
					psrd->break_duration[0] = (unsigned char)(0x02);
					psrd->break_duration[1] = (unsigned char)(0x58);
				}

				if((m_pscte104MsgLocal060Break->m_ppscte104Ops[1])&&(m_pscte104MsgLocal060Break->m_ppscte104Ops[1]->proprietary_data_ptr))
				{
					if(g_settings.m_pszSCTE104Local060EventDTMFsequence)
					{
						memcpy(&(m_pscte104MsgLocal060Break->m_ppscte104Ops[1]->proprietary_data_ptr[2]), g_settings.m_pszSCTE104Local060EventDTMFsequence, nDTMFLen);
					}
					else  //default
					{
						m_pscte104MsgLocal060Break->m_ppscte104Ops[1]->proprietary_data_ptr[2] = 0x37;
						m_pscte104MsgLocal060Break->m_ppscte104Ops[1]->proprietary_data_ptr[3] = 0x38;
						m_pscte104MsgLocal060Break->m_ppscte104Ops[1]->proprietary_data_ptr[4] = 0x34;
						m_pscte104MsgLocal060Break->m_ppscte104Ops[1]->proprietary_data_ptr[5] = 0x2A;
					}
				}
			}
		}
		return m_pscte104MsgLocal060Break->UpdateDataLengths();

	}
	return TABULATOR_ERROR;
}

int CStreamDataData::InitializeLocal120BreakObject()
{
	if(m_pscte104MsgLocal120Break)
	{
		int nDTMFLen = g_settings.m_pszSCTE104Local120EventDTMFsequence?strlen(g_settings.m_pszSCTE104Local120EventDTMFsequence):4 ;
		if(InitializeGeneric(m_pscte104MsgLocal120Break, nDTMFLen)>=TABULATOR_SUCCESS)
		{
			if(m_pscte104MsgLocal120Break->m_ppscte104Ops)
			{
				SCTE104_splice_request_data_t* psrd = (SCTE104_splice_request_data_t*) m_pscte104MsgLocal120Break->m_ppscte104Ops[0]->proprietary_data_ptr;

				if(psrd)
				{
					psrd->splice_insert_type = 1; //(spliceStart_normal)
/*
// template, do this outside with "real data"
					psrd->splice_event_id[0] = (unsigned char)((ul_splice_event_id&0x0f000000)>>24) | 0x10;    // most significant bit first
					psrd->splice_event_id[1] = (unsigned char)((ul_splice_event_id&0x00ff0000)>>16);    // most significant bit first
					psrd->splice_event_id[2] = (unsigned char)((ul_splice_event_id&0x0000ff00)>>8);    // most significant bit first
					psrd->splice_event_id[3] = (unsigned char)((ul_splice_event_id&0x000000ff));    // most significant bit first

					psrd->unique_program_id[0] = (unsigned char)((us_unique_program_id&0xff00)>>8);    // most significant bit first
					psrd->unique_program_id[1] = (unsigned char)((us_unique_program_id&0x00ff));    // most significant bit first

					psrd->pre_roll_time[0] = (unsigned char)((n&0x0000ff00)>>8);
					psrd->pre_roll_time[1] = (unsigned char)((n&0x000000ff));

* /
					// 1200 = 0x04b0
					psrd->break_duration[0] = (unsigned char)(0x04);
					psrd->break_duration[1] = (unsigned char)(0xb0);
				}

				if((m_pscte104MsgLocal120Break->m_ppscte104Ops[1])&&(m_pscte104MsgLocal120Break->m_ppscte104Ops[1]->proprietary_data_ptr))
				{
					if(g_settings.m_pszSCTE104Local120EventDTMFsequence)
					{
						memcpy(&(m_pscte104MsgLocal120Break->m_ppscte104Ops[1]->proprietary_data_ptr[2]), g_settings.m_pszSCTE104Local120EventDTMFsequence, nDTMFLen);
					}
					else  //default
					{
						m_pscte104MsgLocal120Break->m_ppscte104Ops[1]->proprietary_data_ptr[2] = 0x34;
						m_pscte104MsgLocal120Break->m_ppscte104Ops[1]->proprietary_data_ptr[3] = 0x36;
						m_pscte104MsgLocal120Break->m_ppscte104Ops[1]->proprietary_data_ptr[4] = 0x35;
						m_pscte104MsgLocal120Break->m_ppscte104Ops[1]->proprietary_data_ptr[5] = 0x2A;
					}
				}
			}
		}
		return m_pscte104MsgLocal120Break->UpdateDataLengths();

	}
	return TABULATOR_ERROR;
}

int CStreamDataData::InitializeBreakEndObject()
{
	if(m_pscte104MsgBreakEnd)
	{
		int nDTMFLen = g_settings.m_pszSCTE104BreakEndEventDTMFsequence?strlen(g_settings.m_pszSCTE104BreakEndEventDTMFsequence):4 ;
		if(InitializeGeneric(m_pscte104MsgBreakEnd, nDTMFLen)>=TABULATOR_SUCCESS)
		{
			if(m_pscte104MsgBreakEnd->m_ppscte104Ops)
			{
				SCTE104_splice_request_data_t* psrd = (SCTE104_splice_request_data_t*) m_pscte104MsgBreakEnd->m_ppscte104Ops[0]->proprietary_data_ptr;

				if(psrd)
				{
					psrd->splice_insert_type = 4; // (spliceEnd_immediate)
/*
// template, do this outside with "real data"
					psrd->splice_event_id[0] = (unsigned char)((ul_splice_event_id&0x0f000000)>>24) | 0x10;    // most significant bit first
					psrd->splice_event_id[1] = (unsigned char)((ul_splice_event_id&0x00ff0000)>>16);    // most significant bit first
					psrd->splice_event_id[2] = (unsigned char)((ul_splice_event_id&0x0000ff00)>>8);    // most significant bit first
					psrd->splice_event_id[3] = (unsigned char)((ul_splice_event_id&0x000000ff));    // most significant bit first

					psrd->unique_program_id[0] = (unsigned char)((us_unique_program_id&0xff00)>>8);    // most significant bit first
					psrd->unique_program_id[1] = (unsigned char)((us_unique_program_id&0x00ff));    // most significant bit first
* /
					// zero preroll and break dur

					psrd->pre_roll_time[0] = 0x00;
					psrd->pre_roll_time[1] = 0x00;

					psrd->break_duration[0] = 0x00;
					psrd->break_duration[1] = 0x00;

				}

				if((m_pscte104MsgBreakEnd->m_ppscte104Ops[1])&&(m_pscte104MsgBreakEnd->m_ppscte104Ops[1]->proprietary_data_ptr))
				{
					if(g_settings.m_pszSCTE104BreakEndEventDTMFsequence)
					{
						memcpy(&(m_pscte104MsgBreakEnd->m_ppscte104Ops[1]->proprietary_data_ptr[2]), g_settings.m_pszSCTE104BreakEndEventDTMFsequence, nDTMFLen);
					}
					else  //default
					{
						m_pscte104MsgBreakEnd->m_ppscte104Ops[1]->proprietary_data_ptr[2] = 0x31;
						m_pscte104MsgBreakEnd->m_ppscte104Ops[1]->proprietary_data_ptr[3] = 0x35;
						m_pscte104MsgBreakEnd->m_ppscte104Ops[1]->proprietary_data_ptr[4] = 0x30;
						m_pscte104MsgBreakEnd->m_ppscte104Ops[1]->proprietary_data_ptr[5] = 0x23;
					}
				}
			}
		}
		return m_pscte104MsgBreakEnd->UpdateDataLengths();

	}
	return TABULATOR_ERROR;
}
*/

int CStreamDataData::InitDLLData(DLLthread_t* pData)
{
	if(pData)
	{
		InitializeCriticalSection(&pData->m_crit);
		EnterCriticalSection(&pData->m_crit);
		pData->bKillThread=true;
		pData->bThreadStarted=false;
		pData->nThreadState=0;  //generic
		pData->nThreadControl=0;  //generic
		pData->m_dblLastUpdate=-1.0;
		pData->pszThreadName=NULL;
		strcpy(pData->pszThreadStateText, "");//256 chars max

	// URLs may use format specifiers
		pData->pszBaseURL=NULL;
		pData->pszInfoURL=NULL;
		pData->pszConfirmURL=NULL;

		pData->pszSeriesParamName=NULL;    //  %S
		pData->pszProgramParamName=NULL;   //  %P
		pData->pszEpisodeParamName=NULL;   //  %E

		pData->pszSeriesID=NULL;    //  %s
		pData->pszProgramID=NULL;   //  %p
		pData->pszEpisodeID=NULL;   //  %e

		// some generic parameters
		pData->pszToDateParamName=NULL;				//  %t
		pData->pszFromDateParamName=NULL;			//  %f
		pData->pszNowParamName=NULL;					//  %n
		pData->pszBatchParamName=NULL;				//  %b
		pData->pszFromIDParamName=NULL;				//  %m  // start id
		pData->pszGetIDParamName=NULL;				//  %i

		pData->pszLastID=NULL;  // last ID gotten   %l

		pData->nBatchParam=250;
		pData->nShortIntervalMS=0;
		pData->nLongIntervalMS=5000;
		pData->bDirParamStyle=true;  // true is baseurl/paramname/paramvalue/  false is baseurl?paramname=paramvalue

		_ftime(&pData->timebTick); // the time check inside the thread
		_ftime(&pData->timebLastPullTick); // the last time data pulled inside the thread
		pData->nIDrateAvg=-1;
		pData->nIDrateMax=-1;
		pData->nIDrateMin=-1;
		pData->nNumFailures=0;
		pData->nNumSuccess=0;

		pData->pszHost=NULL;
		pData->nPort=80;
		pData->nAuxValue=-1;

		LeaveCriticalSection(&pData->m_crit);
		return CLIENT_SUCCESS;
	}

	return CLIENT_ERROR;
}

/*
int CStreamDataData::GetStreamData(char* pszInfo)
{
	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT uid, name, description, type, status, duration, start_tc, stop_tc, \
count, paused_duration, paused_count, last_started, last_stopped, action_type, action_data FROM %s ORDER BY uid",  //HARDCODE
			((g_settings.m_pszStreamData)&&(strlen(g_settings.m_pszStreamData)))?g_settings.m_pszStreamData:"StreamData"
			); 
		
/*		

create table StreamData (uid  int identity(1,1), name varchar(64), description varchar(256), type int, status int, duration int, start_tc int, stop_tc int, \
count int, paused_duration int, paused_count int, last_started decimal(20,3), last_stopped decimal(20,3), action_type int, action_data varchar(2048))  
   


	int m_nItemID;

	unsigned long m_ulType;  // up count, down count
	unsigned long m_ulStatus;

	unsigned long m_ulDuration; // if a downcounter, the total number of milliseconds to count down.  If an upcounter, it should be the maximum count, or zero if none.
	unsigned long m_ulStartTC; // if a scheduled autodata, start from this millisecond offset.
	unsigned long m_ulStopTC; // the time code at finish

	unsigned long m_ulCount; // if a downcounter, the total number of milliseconds left.  If an upcounter, elapsed duration
	unsigned long m_ulPausedDuration; // elapsed milliseconds while paused.
	unsigned long m_ulTimesPaused;
	
	_timeb m_timeLastStarted;
	_timeb m_timeLastStopped;

	char* m_pszName;
	char* m_pszDescription;

	bool m_bKillStreamData;
	bool m_bStreamDataStarted;

	// need something here for "action" to do when the autodata finishes.
	unsigned long m_ulActionType;  // up count, down count
	char* m_pszActionData;
	* /

if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:GetStreamData", "%s", szSQL); // Sleep(250); //(Dispatch message)
}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "GetStreamData");  Sleep(250); //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "Got Destinations");  Sleep(250); //(Dispatch message)

			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = CLIENT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:GetStreamData", "index %d", nIndex); // Sleep(250); //(Dispatch message)
}
				CString szName="";
				CString szDesc="";
				CString szActionData="";
				CString szTemp;

				int nItemID = -1;
				bool bItemFound = false;
				bool bFlagsFound = false;
				bool bFound = false;


				unsigned long ulType;  // up count, down count
//				unsigned long ulStatus;

//				unsigned long ulDuration; // if a downcounter, the total number of milliseconds to count down.  If an upcounter, it should be the maximum count, or zero if none.
//				unsigned long ulStartTC; // if a scheduled autodata, start from this millisecond offset.
//				unsigned long ulStopTC; // the time code at finish

//				unsigned long ulCount; // if a downcounter, the total number of milliseconds left.  If an upcounter, elapsed duration
//				unsigned long ulPausedDuration; // elapsed milliseconds while paused.
//				unsigned long ulTimesPaused;
				
//				_timeb timeLastStarted;
//				_timeb timeLastStopped;

				unsigned long ulActionType;  // up count, down count

				unsigned long ulFlags = 0; // used for volatile status


				try
				{
					prs->GetFieldValue("uid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bItemFound = true;
						nItemID = atoi(szTemp);
					}
					prs->GetFieldValue("name", szName);//HARDCODE
					szName.TrimLeft(); szName.TrimRight();

					prs->GetFieldValue("description", szDesc);//HARDCODE
					szDesc.TrimLeft(); szDesc.TrimRight();

					prs->GetFieldValue("type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulType = atol(szTemp);
					}

/*

	// these need to be set on the fly
					prs->GetFieldValue("status", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulStatus = atol(szTemp);
					}

					prs->GetFieldValue("duration", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulDuration = atol(szTemp);
					}


					prs->GetFieldValue("start_tc", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulStartTC = atol(szTemp);
					}

					prs->GetFieldValue("stop_tc", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulStopTC = atol(szTemp);
					}
* /

					prs->GetFieldValue("count", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulCount = atol(szTemp);
					}
					prs->GetFieldValue("paused_duration", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulPausedDuration = atol(szTemp);
					}
					prs->GetFieldValue("paused_count", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulTimesPaused = atol(szTemp);
					}
					prs->GetFieldValue("last_started", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						timeLastStarted.time = atol(szTemp);
						timeLastStarted.millitm = ((unsigned short)((atof(szTemp) - ((double)(timeLastStarted.time))) *1000.0));
					}
					prs->GetFieldValue("last_stopped", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						timeLastStopped.time = atol(szTemp);
						timeLastStopped.millitm = ((unsigned short)((atof(szTemp) - ((double)(timeLastStarted.time))) *1000.0));
					}
* /
					prs->GetFieldValue("action_type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						ulActionType = atol(szTemp);
					}
					prs->GetFieldValue("action_data", szDesc);//HARDCODE
					szDesc.TrimLeft(); szDesc.TrimRight();


					bFlagsFound = true; // made it thru the whole record
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
* /
if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:GetStreamData", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL); // Sleep(250); //(Dispatch message)
}
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:GetStreamData", "Retrieve: Caught exception: out of memory\n%s", szSQL); // Sleep(250); //(Dispatch message)
}
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:GetStreamData", "Retrieve: Caught other exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)
}
					}
					e->Delete();
				} 
				catch( ... )
				{
					if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception.\n%s", szSQL);

if(g_settings.m_ulDebug&(STREAMDATA_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:GetStreamData", "Retrieve: Caught exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)
}

				}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "Got Destination: %s (0x%08x)", szName, ulFlags);  Sleep(250); //(Dispatch message)


//////////////////////////////////////////////////////////////////////////////
// temporarily going to just ignore all this, this is only going to be called once at startup and that is it.
// so this will not get hit


/*
				if(0) //(m_ppStreamData)&&(m_nStreamData))
				{
					int nTemp=0;
					while(nTemp<m_nStreamData)
					{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "c2");  Sleep(250); //(Dispatch message)
						if((m_ppStreamData[nTemp]))//&&(m_ppStreamData[nTemp]->m_pszName))
						{
							// changing the follwing to unique on dest id.
				//			if((szName.GetLength()>0)&&(szName.CompareNoCase(m_ppStreamData[nTemp]->m_pszName)==0))
							if(m_ppStreamData[nTemp]->m_nItemID == nItemID)
							{
								bFound = true;
								bool bRefresh = false;

								if(
										(szDesc.GetLength()>0)
									&&((m_ppStreamData[nTemp]->m_pszDesc==NULL)||(szDesc.CompareNoCase(m_ppStreamData[nTemp]->m_pszDesc)))
									)
								{
									if(m_ppStreamData[nTemp]->m_pszDesc) free(m_ppStreamData[nTemp]->m_pszDesc);

									m_ppStreamData[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(m_ppStreamData[nTemp]->m_pszDesc) sprintf(m_ppStreamData[nTemp]->m_pszDesc, szDesc);
								}

								if(
										(szName.GetLength()>0)
									&&((m_ppStreamData[nTemp]->m_pszName==NULL)||(szName.CompareNoCase(m_ppStreamData[nTemp]->m_pszName)))
									)
								{
									if(m_ppStreamData[nTemp]->m_pszName) free(m_ppStreamData[nTemp]->m_pszName);

									m_ppStreamData[nTemp]->m_pszName = (char*)malloc(szName.GetLength()+1); 
									if(m_ppStreamData[nTemp]->m_pszName) sprintf(m_ppStreamData[nTemp]->m_pszName, szName);
								}

								if(bItemFound) m_ppStreamData[nTemp]->m_nItemID = nItemID;
								m_ppStreamData[nTemp]->m_ulType = ulType;

								if(dblDiskspaceThreshold>0.0) m_ppStreamData[nTemp]->m_dblDiskPercent = dblDiskspaceThreshold;
//								m_ppStreamData[nTemp]->m_dblDiskKBFree = dblDiskspaceFree;
//								m_ppStreamData[nTemp]->m_dblDiskKBTotal = dblDiskspaceTotal;

//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "c3");  Sleep(250); //(Dispatch message)

								if(
										((bFlagsFound)&&(!(ulFlags&CLIENT_FLAG_ENABLED))&&((m_ppStreamData[nTemp]->m_ulFlags)&CLIENT_FLAG_ENABLED))
									)
								{
									m_ppStreamData[nTemp]->m_bKillMonThread = true;

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
										m_ppStreamData[nTemp]->m_pszDesc?m_ppStreamData[nTemp]->m_pszDesc:m_ppStreamData[nTemp]->m_pszName);  
									g_ptabmsgr->DM(MSG_ICONINFO, NULL, "StreamData:destination_change", errorstring);  Sleep(20);  //(Dispatch message)
									m_ppStreamData[nTemp]->m_bKillConnThread = true;
									m_ppStreamData[nTemp]->m_ulStatus = CLIENT_STATUS_NOTCON;
									if(m_ppStreamData[nTemp]->m_socket != NULL)
									{
										m_ppStreamData[nTemp]->m_net.CloseConnection(m_ppStreamData[nTemp]->m_socket); // re-establish
									}
									m_ppStreamData[nTemp]->m_socket = NULL;

								}

								if(
										((bFlagsFound)&&(ulFlags&CLIENT_FLAG_ENABLED)&&(!((m_ppStreamData[nTemp]->m_ulFlags)&CLIENT_FLAG_ENABLED)))
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating %s...", 
//										m_ppStreamData[nTemp]->m_pszClientName?m_ppStreamData[nTemp]->m_pszClientName:"Libretto",  
										m_ppStreamData[nTemp]->m_pszDesc?m_ppStreamData[nTemp]->m_pszDesc:m_ppStreamData[nTemp]->m_pszName);  
									g_ptabmsgr->DM(MSG_ICONINFO, NULL, "StreamData:destination_change", errorstring); //  Sleep(20);  //(Dispatch message)
//									m_ppStreamData[nTemp]->m_pDlg->OnButtonConnect();
									//**** if connect set following status


									if((m_ppStreamData[nTemp]->m_socket == NULL)&&(!m_ppStreamData[nTemp]->m_bConnThreadStarted))
									{
										m_ppStreamData[nTemp]->m_ulStatus = CLIENT_STATUS_CONN;
										if(_beginthread(LibrettoAsynchConnThread, 0, (void*)(m_ppStreamData[nTemp]))==-1)
										{
										//error.

										//**MSG
										}
										else
										{
											int nWait = 0;
											while((m_ppStreamData[nTemp]->m_ulStatus == CLIENT_STATUS_CONN)&&(nWait<1000))  // 0.5 seconds?
											{
												nWait++;
												Sleep(1);
											}
										}
									}

									if(!m_ppStreamData[nTemp]->m_bMonThreadStarted)
									{
										m_ppStreamData[nTemp]->m_bKillMonThread = false;
										if(_beginthread(LibrettoConnMonitorThread, 0, (void*)(m_ppStreamData[nTemp]))==-1)
										{
										//error.
									g_ptabmsgr->DM(MSG_ICONERROR, NULL, "StreamData:destination_change", "Error starting connection monitor for %s", 
										m_ppStreamData[nTemp]->m_pszDesc?m_ppStreamData[nTemp]->m_pszDesc:m_ppStreamData[nTemp]->m_pszName); //  Sleep(20);  //(Dispatch message)

										//**MSG
										}
									}


									if(!m_ppStreamData[nTemp]->m_bCommandQueueThreadStarted)
									{
										m_ppStreamData[nTemp]->m_bKillCommandQueueThread = false;
										if(_beginthread(LibrettoCommandQueueThread, 0, (void*)(m_ppStreamData[nTemp]))==-1)
										{
										//error.
									g_ptabmsgr->DM(MSG_ICONERROR, NULL, "StreamData:destination_change", "Error starting command queue for %s", 
										m_ppStreamData[nTemp]->m_pszDesc?m_ppStreamData[nTemp]->m_pszDesc:m_ppStreamData[nTemp]->m_pszName); //  Sleep(20);  //(Dispatch message)

										//**MSG
										}
									}


									

/*
									if(m_ppStreamData[nTemp]->m_socket == NULL)
									{
										m_ppStreamData[nTemp]->m_net.OpenConnection(m_ppStreamData[nTemp]->m_pszName, g_settings.m_nPort, &m_ppStreamData[nTemp]->m_socket); // re-establish
										char tnbuffer[32]; sprintf(tnbuffer, "\\\\%c%c", 13,10);
										m_ppStreamData[nTemp]->SendTelnetCommand(tnbuffer, 2);
										unsigned char* pucBuffer = NULL;
										unsigned long ulBufferLen = 256;
										m_ppStreamData[nTemp]->m_net.GetLine(&pucBuffer, &ulBufferLen, m_ppStreamData[nTemp]->m_socket);
										if(pucBuffer) free(pucBuffer);
									}
* /
									bRefresh = true;
//									m_ppStreamData[nTemp]->m_bKillConnThread = false;
	//								if(_beginthread(LibrettoConnectionThread, 0, (void*)m_ppStreamData[nTemp])<0)
	//								{
										//error.

										//**MSG
							
										
		//							}
									//**** should check return value....

									m_ppStreamData[nTemp]->m_ulStatus = CLIENT_STATUS_CONN;
								}

								if(bFlagsFound) m_ppStreamData[nTemp]->m_ulFlags = ulFlags|CLIENT_FLAG_FOUND;
								else m_ppStreamData[nTemp]->m_ulFlags |= CLIENT_FLAG_FOUND;

								if(bRefresh)m_ppStreamData[nTemp]->m_ulFlags |= CLIENT_FLAG_REFRESH;

							}
						}
						nTemp++;
					}
				}
// temporarily going to just ignore all this, this is only going to be called once at startup and that is it.
// so this (above) will not get hit
//////////////////////////////////////////////////////////////////////////////

* /



				if((!bFound)&&(szName.GetLength()>0)) // have to add.
				{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "adding %s", szName);  Sleep(250); //(Dispatch message)
					CStreamDataItem* pscno = new CStreamDataItem;
					if(pscno)
					{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "new obj for %s", szName);  Sleep(250); //(Dispatch message)
						CStreamDataItem** ppObj = new CStreamDataItem*[m_nStreamData+1];
						if(ppObj)
						{
				
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "new array for %s", szName);  Sleep(250); //(Dispatch message)
							int o=0;
							if((m_ppStreamData)&&(m_nStreamData>0))
							{
								while(o<m_nStreamData)
								{
									ppObj[o] = m_ppStreamData[o];
									o++;
								}
								delete [] m_ppStreamData;

							}
							ppObj[o] = pscno;
							m_ppStreamData = ppObj;
							m_nStreamData++;

							ppObj[o]->m_pszName = (char*)malloc(szName.GetLength()+1); 
							if(ppObj[o]->m_pszName) sprintf(ppObj[o]->m_pszName, "%s", szName);

							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}
							if(bItemFound) ppObj[o]->m_nItemID = nItemID;
							ppObj[o]->m_ulType = ulType;

							if(szActionData.GetLength()>0)
							{
								ppObj[o]->m_pszActionData = (char*)malloc(szActionData.GetLength()+1); 
								if(ppObj[o]->m_pszActionData) sprintf(ppObj[o]->m_pszActionData, "%s", szActionData);
							}
							ppObj[o]->m_ulActionType = ulActionType;



//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", " added flags: %s (0x%08x) %d", szName, ppObj[o]->m_ulFlags, ((ppObj[o]->m_ulFlags)&CLIENT_FLAG_ENABLED));  Sleep(250); //(Dispatch message)


							ppObj[o]->m_ulFlags |= STREAMDATA_FLAG_FOUND;
						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			// and insert media list refresh into the queue for all new destinatons.
			nIndex = 0;
			while(nIndex<m_nStreamData)
			{
				if((m_ppStreamData)&&(m_ppStreamData[nIndex]))
				{

					if((m_ppStreamData[nIndex]->m_ulFlags)&STREAMDATA_FLAG_FOUND)
					{
						(m_ppStreamData[nIndex]->m_ulFlags) &= ~STREAMDATA_FLAG_FOUND;

						nIndex++;
					}
					else
					{
						if(m_ppStreamData[nIndex]->m_bStreamDataStarted)
						{
							int nWait =  clock()+1500;
							m_ppStreamData[nIndex]->m_bKillStreamData = true;

							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
									m_ppStreamData[nIndex]->m_pszDesc?m_ppStreamData[nIndex]->m_pszDesc:m_ppStreamData[nIndex]->m_pszName);  
								if(g_ptabmsgr)  g_ptabmsgr->DM(MSG_ICONINFO, NULL, "StreamData:remove_autodata", errorstring);  // Sleep(20);  //(Dispatch message)
//									m_ppStreamData[nTemp]->m_pDlg->OnDisconnect();


							//**** disconnect
//								while(m_ppStreamData[nIndex]->m_bConnThreadStarted) Sleep(1);

							while(
										 (
										   (m_ppStreamData[nIndex]->m_bStreamDataStarted)
										 )
										&&
										 (clock()<nWait)
									 )
							{
								Sleep(1);
							}

							m_nStreamData--;

							int nTemp=nIndex;
							while(nTemp<m_nStreamData)
							{
								m_ppStreamData[nTemp]=m_ppStreamData[nTemp+1];
								nTemp++;
							}
							m_ppStreamData[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
	}
//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return CLIENT_ERROR;
}


int CStreamDataData::FindStreamData(char* pszStreamDataName)
{
	if((pszStreamDataName)&&(m_ppStreamData))
	{
		int nTemp=0;
		while(nTemp<m_nStreamData)
		{
			if(m_ppStreamData[nTemp])
			{
				if(m_ppStreamData[nTemp]->m_pszName)
				{
					if(stricmp(m_ppStreamData[nTemp]->m_pszName, pszStreamDataName)==0) return nTemp;
				}
			}
			nTemp++;
		}
	}
	return CLIENT_ERROR;

}
*/



// the way we are doing this, we have to limit the tabulator events to having max of only ONE
// stream message per event.  Because there is one override being sent in....
// at least for now, this is how it needs to be.
int CStreamDataData::PlayEvent(CTabulatorEventArray** ppEvent, CMessageOverrideValues* pOverrides, char* pszInfo)
{
	CTabulatorEventArray* pEvent = NULL;
	char szTickerSource[MAX_PATH]; sprintf(szTickerSource, "%s_Output", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
	if(ppEvent) pEvent = *ppEvent;
	if((m_pdbOutput)&&(m_pdbOutputConn)&&(pEvent)&&(pEvent->m_ppEvents)&&(pEvent->m_nEvents>0))
	{
		char szCommandSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];
		char TabulatorToken[MAX_PATH];
		int nTabulatorTokenLen=0;
		if((g_settings.m_pszTabulatorModule)&&(strlen(g_settings.m_pszTabulatorModule)))
		{
			_snprintf(TabulatorToken, MAX_PATH, "%s:", g_settings.m_pszTabulatorModule);
		}
		else
		{
			strcpy(TabulatorToken, "Tabulator:");
		}	
		
		nTabulatorTokenLen = strlen(TabulatorToken);
	
		int i=0;
		_timeb timebBeginTick;
		_timeb timebTriggerTick;
		_timeb timebNowTick;
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Entering"); //(Dispatch message)
		EnterCriticalSection(&pEvent->m_critEvents);
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "Entered"); //(Dispatch message)
		_ftime(&timebBeginTick);
		CBufferUtil bu;
//			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "PlayEvent event"); //(Dispatch message)
//Sleep(250);
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "PlayEvent event %s with %d actions", pEvent->m_ppEvents[0]->m_szScene, pEvent->m_nEvents); //(Dispatch message)

		while((i<pEvent->m_nEvents)&&(g_dlldata.thread)&&(g_dlldata.thread[0])&&(!g_dlldata.thread[0]->bKillThread))
		{
			if((pEvent->m_ppEvents)&&(pEvent->m_ppEvents[i]))
			{
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "-X- text event %02d %s %.0f", i, pEvent->m_ppEvents[i]->m_szIdentifier, pEvent->m_ppEvents[i]->m_dblTriggerTime); //(Dispatch message)
				// delay by the offest amount;
				timebTriggerTick.time = timebBeginTick.time+((int)(pEvent->m_ppEvents[i]->m_dblTriggerTime))/1000;
				timebTriggerTick.millitm = (unsigned short)(timebBeginTick.millitm + ((int)(pEvent->m_ppEvents[i]->m_dblTriggerTime))%1000);
				while(timebTriggerTick.millitm>999)
				{
					timebTriggerTick.time++;
					timebTriggerTick.millitm-=1000;
				}
				_ftime(&timebNowTick);

				// assemble the string to send:

				if(pEvent->m_ppEvents[i]->m_szIdentifier.GetLength())
				{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "--- event %02d %s with %s, type %d, dest type %d", i, pEvent->m_ppEvents[i]->m_szIdentifier, pEvent->m_ppEvents[i]->m_szValue, pEvent->m_ppEvents[i]->m_nType, pEvent->m_ppEvents[i]->m_nDestType); //(Dispatch message)

//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "pchEncodedLocal: %s", pchEncodedLocal);  Sleep(50);//(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "want to send %s", proj);  Sleep(50);//(Dispatch message)

					CString szII = "";
					CString szReadableII = "";
					bool bTabulatorEvent = false;
					bool bAsRun = false;
					if(pEvent->m_ppEvents[i]->m_nType==0)  //anim
					{

/*
// by pass dest type for now, not in tabulator view (at this point) 20121218

						switch(pEvent->m_ppEvents[i]->m_nDestType)
						{
						case CX_DESTTYPE_VDS_WATERCOOLER://           9000  // watercooler!
							{
							} break;

						case CX_DESTTYPE_MIRANDA_IS2://					2001  // Imagestore 2
						case CX_DESTTYPE_MIRANDA_INT://					2002  // Intuition
						case CX_DESTTYPE_MIRANDA_IS300://				2003  // Imagestore 300
						case CX_DESTTYPE_MIRANDA_HDIS://				2101  // Imagestore HD
						case CX_DESTTYPE_MIRANDA_HDINT://       2102  // Intuition HD
						case CX_DESTTYPE_MIRANDA_HDIS300://     2103  // Imagestore 300 HD
							{
								// no anims supported yet...old comment.  NOW:

								// OK, need to deal with Load (Load a file into a layer), Play (Fade up)  and Clear (Fade Down)
								//  could use CUT up and down, but for this we can just make FADE be immediate, so just use fade.
								// NOT using fade to black etc, using fade keyer, because we want background to pass.

								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Load")==0)
								{
									// layer needs to be a %1x representation of the layer number  (we have it as a string)
									szII.Format("R0%s%s",pEvent->m_ppEvents[i]->m_szLayer, pEvent->m_ppEvents[i]->m_szSource);  //using source, not scene, since IS2 only dealas with filenames, there are no scenes
//												szII.Format("R0%s%s",pEvent->m_ppEvents[i]->m_szLayer, pEvent->m_ppEvents[i]->m_szScene);
								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Play")==0)
								{
									//Layer needs to be a %d representation of the layer number (we have it as a string)
									szII.Format("1%s 1",pEvent->m_ppEvents[i]->m_szLayer);
								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Clear")==0)
								{
									//Layer needs to be a %d representation of the layer number (we have it as a string)
									szII.Format("1%s 0",pEvent->m_ppEvents[i]->m_szLayer);
								}
								else
								if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Command")==0)
								{
									//the whole command must be coded in, exactly as it is to be sent to the Miranda
									// can be used for anything, fade transition settings, easytext commands, squeezy, etc
									szII.Format("%s",pEvent->m_ppEvents[i]->m_szValue);
								}

							} break;

						case CX_DESTTYPE_EVERTZ_7847FSE:
							{
*/
								unsigned char* pucOut = NULL;
								if(
										(pOverrides) // calculated outside
	//								&&(m_pscte104MsgNationalBreak)
	//								&&(m_pscte104MsgReplaceAd->m_scte104Hdr.NumberOfOps>1)
	//								&&(m_pscte104MsgReplaceAd->m_ppscte104Ops)
	//								&&(m_pscte104MsgReplaceAd->m_ppscte104Ops[0])
	//								&&(m_pscte104MsgReplaceAd->m_ppscte104Ops[1])
									)
								{
									if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("SpliceRequest")==0)
									{
										if(m_pSpliceRequestEvents)
										{
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		char pszSource[MAX_PATH];
		_snprintf(pszSource, MAX_PATH-1, "%s:PlayEvent:SpliceRequest", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");

		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "Updating with %d %d %s %d", pOverrides->m_nPrerollMS, pOverrides->m_nDurationMS, pOverrides->m_pszIDString, (pOverrides->m_uchSource!=0x00) ); //Sleep(250);
	}
											// Local -- 0x32 (Distributor Advertisement Start)  //National -- 0x30 (Provider Advertisement Start)
											UpdateGeneric(m_pscte104MsgSpliceRequest, pOverrides->m_uchSource, pOverrides->m_nPrerollMS, pOverrides->m_nDurationMS, pOverrides->m_pszIDString, (pOverrides->m_uchSource!=0x00));
	
											pucOut = m_scte104.ReturnMessageBuffer(m_pscte104MsgSpliceRequest);

											if(g_settings.m_bAsRunSpliceRequests) bAsRun = true;
										}
									}
/*
									else
									if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("NationalBreak")==0)
									{
										if(m_pscte104MsgNationalBreak)
										{
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		char pszSource[MAX_PATH];
		_snprintf(pszSource, MAX_PATH-1, "%s:PlayEvent:NationalBreak", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");

		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "Updating with %d %d %s %d", pOverrides->m_nPrerollMS, pOverrides->m_nDurationMS, pOverrides->m_pszIDString, (pOverrides->m_uchSource!=0x00) ); //Sleep(250);
	}
											// Local -- 0x32 (Distributor Advertisement Start)  //National -- 0x30 (Provider Advertisement Start)
											UpdateGeneric(m_pscte104MsgNationalBreak, STREAMDATA_SOURCE_NATIONAL, pOverrides->m_nPrerollMS, pOverrides->m_nDurationMS, pOverrides->m_pszIDString, (pOverrides->m_uchSource!=0x00));
	
											pucOut = m_scte104.ReturnMessageBuffer(m_pscte104MsgNationalBreak);

											if(g_settings.m_bAsRunNationalBreak) bAsRun = true;
										}
									}
									else
									if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("LocalBreak")==0)
									{
										if(m_pscte104MsgLocalBreak)
										{
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		char pszSource[MAX_PATH];
		_snprintf(pszSource, MAX_PATH-1, "%s:PlayEvent:LocalBreak", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");

		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "Updating with %d %d %s %d", pOverrides->m_nPrerollMS, pOverrides->m_nDurationMS, pOverrides->m_pszIDString, (pOverrides->m_uchSource!=0x00) ); //Sleep(250);
	}
											// Local -- 0x32 (Distributor Advertisement Start)  //National -- 0x30 (Provider Advertisement Start)
											UpdateGeneric(m_pscte104MsgLocalBreak, STREAMDATA_SOURCE_LOCAL, pOverrides->m_nPrerollMS, pOverrides->m_nDurationMS, pOverrides->m_pszIDString, (pOverrides->m_uchSource!=0x00));
	
											pucOut = m_scte104.ReturnMessageBuffer(m_pscte104MsgLocalBreak);

											if(g_settings.m_bAsRunLocalBreak) bAsRun = true;
										}
									}
									*/
/*
									else
									if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Local060Break")==0)
									{
										if(m_pscte104MsgLocal060Break)
										{
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		char pszSource[MAX_PATH];
		_snprintf(pszSource, MAX_PATH-1, "%s:PlayEvent:Local060Break", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "Updating with %d %d %s %d", pOverrides->m_nPrerollMS, pOverrides->m_nDurationMS, pOverrides->m_pszDTMF, (pOverrides->m_uchSource!=0x00) ); // Sleep(250);
	}
											UpdateGeneric(m_pscte104MsgLocal060Break, 0x20, pOverrides->m_nPrerollMS, pOverrides->m_nDurationMS, pOverrides->m_pszDTMF, (pOverrides->m_uchSource!=0x00));
											pucOut = m_scte104.ReturnMessageBuffer(m_pscte104MsgLocal060Break);
											if(g_settings.m_bAsRunLocal060Break) bAsRun = true;
										}
									}
									else
									if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("Local120Break")==0)
									{
										if(m_pscte104MsgLocal120Break)
										{
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		char pszSource[MAX_PATH];
		_snprintf(pszSource, MAX_PATH-1, "%s:PlayEvent:Local120Break", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "Updating with %d %d %s %d", pOverrides->m_nPrerollMS, pOverrides->m_nDurationMS, pOverrides->m_pszDTMF, (pOverrides->m_uchSource!=0x00) ); //Sleep(250);
	}
											UpdateGeneric(m_pscte104MsgLocal120Break, 0x20, pOverrides->m_nPrerollMS, pOverrides->m_nDurationMS, pOverrides->m_pszDTMF, (pOverrides->m_uchSource!=0x00));
											pucOut = m_scte104.ReturnMessageBuffer(m_pscte104MsgLocal120Break);
											if(g_settings.m_bAsRunLocal120Break) bAsRun = true;
										}
									}
									else
									if(pEvent->m_ppEvents[i]->m_szIdentifier.CompareNoCase("BreakEnd")==0)
									{
										if(m_pscte104MsgBreakEnd)
										{
	if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_CALLFUNC)) 
	{
		char pszSource[MAX_PATH];
		_snprintf(pszSource, MAX_PATH-1, "%s:PlayEvent:BreakEnd", g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData");
		g_ptabmsgr->DM(MSG_ICONINFO, NULL, pszSource, "Updating with %d %d %s %d", pOverrides->m_nPrerollMS, pOverrides->m_nDurationMS, pOverrides->m_pszDTMF, (pOverrides->m_uchSource!=0x00) ); //Sleep(250);
	}
											UpdateGeneric(m_pscte104MsgBreakEnd, 0x00, pOverrides->m_nPrerollMS, pOverrides->m_nDurationMS, pOverrides->m_pszDTMF, (pOverrides->m_uchSource!=0x00));
											pucOut = m_scte104.ReturnMessageBuffer(m_pscte104MsgBreakEnd);
											if(g_settings.m_bAsRunBreakEnd) bAsRun = true;
										}
									}
*/
								}

								if(pucOut)
								{
									// encode it for Libretto
									unsigned long ulLen = (*(pucOut+2))&0x000000ff;
				//Q.Format("buflen %d", ulLen);
				//AfxMessageBox(Q);
									ulLen <<= 8; ulLen += (*(pucOut+3))&0x000000ff;
									char* pchBuffer = (char*)pucOut;


									if(g_settings.m_bReadableEncodedAsRun)
									{
										unsigned long ulBufLen = ulLen;
										char* pchHexBuffer = m_bu.ReadableHex((char*)pucOut, &ulBufLen, MODE_FULLHEX);
										if(pchHexBuffer)
										{
											szReadableII = pchHexBuffer;
											free(pchHexBuffer);
										}
									}

									int nEncodeReturn = m_bu.Base64Encode(&pchBuffer, &ulLen, false);
									if(nEncodeReturn>=RETURN_SUCCESS)
									{
										if(pchBuffer)
										{
											szII.Format("%s", pchBuffer);
											free(pchBuffer);
										}
									}

									delete [] pucOut; // yes, new was used
								}
								
/*
							} break;

						}
*/
						// else not supported!

if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_TRIGGER)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "II is now: %s", szII); //(Dispatch message)
					} // if(pEvent->m_ppEvents[i]->m_nType==0)  //anim was true
					else  // export
					{

if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_TRIGGER)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "--- parse %02d %s with %s", i, pEvent->m_ppEvents[i]->m_szIdentifier, pEvent->m_ppEvents[i]->m_szValue); //(Dispatch message)
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Type is export, with dest type: %d", pEvent->m_ppEvents[i]->m_nDestType);  Sleep(50);//(Dispatch message)

						// searching for the tabulator token.
						
						if(stricmp(TabulatorToken, pEvent->m_ppEvents[i]->m_szIdentifier.Left(nTabulatorTokenLen).GetBuffer(0))==0)
						{
							bTabulatorEvent = true;  // !

							// just regular assemble
							if(pEvent->m_ppEvents[i]->m_szValue.GetLength()>0)
							{
								szII.Format("%s|%s", 
											pEvent->m_ppEvents[i]->m_szIdentifier.Mid(nTabulatorTokenLen), 
											pEvent->m_ppEvents[i]->m_szValue);
							}
							else
							{
								// could be just the token, no value, then you get no pipe.
								szII.Format("%s", pEvent->m_ppEvents[i]->m_szIdentifier.Mid(nTabulatorTokenLen));
							}
						}	// else nothing, not supported!						
					}//if(pEvent->m_ppEvents[i]->m_nType==0)  //anim was false
					
//pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "II is now: %s", szII);  Sleep(50);//(Dispatch message)

					int nLen = szII.GetLength();
					if(nLen>0)  //ignore blank stuff, failure
					{

if((g_ptabmsgr)&&(g_settings.m_ulDebug&STREAMDATA_DEBUG_TRIGGER)) 
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "waiting for trigger II: %s", szII); //(Dispatch message)

						char eventIDbuffer[256];
						_snprintf(eventIDbuffer, 256, "%s", pEvent->m_ppEvents[i]->m_szName);
						char* pchEncodedLocal = NULL;

						while((	(timebNowTick.time<timebTriggerTick.time)
									||((timebNowTick.time==timebTriggerTick.time)&&(timebNowTick.millitm<timebTriggerTick.millitm))
									)
									&&(g_dlldata.thread[0])&&(!g_dlldata.thread[0]->bKillThread))
						{
							Sleep(1);
							_ftime(&timebNowTick);
						}

						if(bTabulatorEvent)
						{

							if(g_data.m_socketTabulator)
							{
								CNetData data;
								data.m_ucType = (NET_TYPE_PROTOCOL1|NET_TYPE_KEEPOPEN|NET_SND_KEEPOPENLCL); // has data but no subcommand.
								data.m_ucCmd		= TABULATOR_CMD_PLUGINCALL;
								data.m_pucData	= (unsigned char*)malloc(nLen+1);
								if(data.m_pucData)
								{
									memcpy(data.m_pucData, szII.GetBuffer(0), nLen);
									data.m_pucData[nLen]=0;
									data.m_ulDataLen = nLen;
									data.m_ucType |= NET_TYPE_HASDATA;
								}

	g_data.m_bInCommand = TRUE;
								int nReturn = g_data.m_net.SendData(&data, g_data.m_socketTabulator, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, errorstring);
								
								if(nReturn<NET_SUCCESS)
								{
									// log it.
									g_data.m_net.CloseConnection(g_data.m_socketTabulator, errorstring);
									g_data.m_socketTabulator =  NULL;

									g_data.SendAsRunMsg(CX_SENDMSG_ERROR, g_settings.m_nAutoServerListNumber, eventIDbuffer, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Error %d sending to %s at %s:%d with: %s", 
										nReturn,
										g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
										g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
										g_settings.m_nTabulatorPort, 
										szII);
if(g_ptabmsgr)
{
	g_ptabmsgr->DM(MSG_ICONERROR, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Error %d (%s) sending to %s at %s:%d with: %s", 
						nReturn, errorstring,
						g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
						g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
						g_settings.m_nTabulatorPort, 
						szII);

	if(bAsRun)
	{
	g_ptabmsgr->DM(MSG_ICONERROR, 
		(((g_settings.m_pszSCTE104AsRunDestination)&&(strlen(g_settings.m_pszSCTE104AsRunDestination)))?g_settings.m_pszSCTE104AsRunDestination:"streamdata_asrun"), 
						eventIDbuffer, 
						"Error %d (%s) sending to %s at %s:%d with: %s", 
						nReturn, errorstring,
						g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
						g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
						g_settings.m_nTabulatorPort, 
						szII);
	}

}
								}
								else
								{
									g_data.SendAsRunMsg(CX_SENDMSG_INFO, g_settings.m_nAutoServerListNumber, eventIDbuffer, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Successfully executed %s with return data: %02x,%02x %s (%d bytes data)", 
									szII,
									data.m_ucCmd,    // the command byte
									data.m_ucSubCmd,   // the subcommand byte
									(((data.m_pucData)&&(data.m_ulDataLen))?(char*)data.m_pucData:""),
									data.m_ulDataLen
									);

if(g_ptabmsgr)
{
	if(g_settings.m_ulDebug&STREAMDATA_DEBUG_TRIGGER)
	{

	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Action %s executed successfully with return data: %02x,%02x %s (%d bytes data)", 
								szII,
								data.m_ucCmd,    // the command byte
								data.m_ucSubCmd,   // the subcommand byte
								(((data.m_pucData)&&(data.m_ulDataLen))?(char*)data.m_pucData:""),
								data.m_ulDataLen

							); // Sleep(250); //(Dispatch message)



	 }

	if(bAsRun)
	{
	g_ptabmsgr->DM(MSG_ICONERROR, 
		(((g_settings.m_pszSCTE104AsRunDestination)&&(strlen(g_settings.m_pszSCTE104AsRunDestination)))?g_settings.m_pszSCTE104AsRunDestination:"streamdata_asrun"), 
						eventIDbuffer, 
						"Action %s executed successfully with return data: %02x,%02x %s (%d bytes data)", 
								szII,
								data.m_ucCmd,    // the command byte
								data.m_ucSubCmd,   // the subcommand byte
								(((data.m_pucData)&&(data.m_ulDataLen))?(char*)data.m_pucData:""),
								data.m_ulDataLen

							);
	}

}
									// just send an ack
									g_data.m_net.SendData(NULL, g_data.m_socketTabulator, 5000, 0, NET_SND_CLNTACK, errorstring);
								}
	g_data.m_bInCommand = FALSE;

							}
							else
							{
								// can't send!

								g_data.SendAsRunMsg(CX_SENDMSG_ERROR, g_settings.m_nAutoServerListNumber, eventIDbuffer, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Error sending to %s at %s:%d with: %s", 
									g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
									g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
									g_settings.m_nTabulatorPort, 
									szII);

if(g_ptabmsgr)
{
	g_ptabmsgr->DM(MSG_ICONERROR, NULL, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "NULL socket error sending to %s at %s:%d with: %s", 
						g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
						g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
						g_settings.m_nTabulatorPort, 
						szII);

	if(bAsRun)
	{
	g_ptabmsgr->DM(MSG_ICONERROR, 
		(((g_settings.m_pszSCTE104AsRunDestination)&&(strlen(g_settings.m_pszSCTE104AsRunDestination)))?g_settings.m_pszSCTE104AsRunDestination:"streamdata_asrun"), 
						eventIDbuffer, 
						"Error sending to %s at %s:%d with: %s", 
									g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
									g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
									g_settings.m_nTabulatorPort, 
									szII);
	}

}
							}

							// send it!
						}
						else
						{
							pchEncodedLocal = m_pdbOutput->EncodeQuotes(szII);

							if((pchEncodedLocal)&&(strlen(pchEncodedLocal)>0))
							{

								if(
									  (g_data.m_nLastTime == timebNowTick.time)
									&&(g_data.m_nLastMillitm == timebNowTick.millitm)
									)
								{
									g_data.m_nMillitmIncr++;
									if(timebNowTick.millitm + g_data.m_nMillitmIncr > 999)
									{
										g_data.m_nTimeIncr++;
										g_data.m_nMillitmIncr -= 1000;
									}
								}
								else
								{
									if( 
										  (timebNowTick.time>g_data.m_nLastTime+g_data.m_nTimeIncr)
										||(
										    (timebNowTick.time==g_data.m_nLastTime+g_data.m_nTimeIncr)
										  &&(timebNowTick.millitm>g_data.m_nLastMillitm+g_data.m_nMillitmIncr)
											)
										)
									{
										// the last tick stuff has not overtaken the new now time, and otherwise would cause an ordering problem, so we can reset to zero
										g_data.m_nMillitmIncr = 0;
										g_data.m_nTimeIncr = 0;
									}
									else
									{
										// lapping did occur, so need to just adjust back
										// knowing that g_data.m_nLastTime and g_data.m_nLastMillitm are going to get rest to being equal to 
										// timebNowTick, we can calc where we can pick up the increment values from.

//										int nSec   = g_data.m_nLastTime + g_data.m_nTimeIncr - timebNowTick.time;
//										int nMilli = 1 + g_data.m_nLastMillitm + g_data.m_nMillitmIncr - timebNowTick.millitm; // don't forget to add one!

										g_data.m_nTimeIncr += (g_data.m_nLastTime - timebNowTick.time);
										g_data.m_nMillitmIncr += (1 + g_data.m_nLastMillitm - timebNowTick.millitm); // don't forget to add one!

										while(timebNowTick.millitm + g_data.m_nMillitmIncr < 0)
										{
											g_data.m_nTimeIncr--;
											g_data.m_nMillitmIncr += 1000;
										}
										while(timebNowTick.millitm + g_data.m_nMillitmIncr > 999)
										{
											g_data.m_nTimeIncr++;
											g_data.m_nMillitmIncr -= 1000;
										}
									}
								}

								_snprintf(szCommandSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s.dbo.%s \
(local, remote, action, host, timestamp, username) \
VALUES ('%s', '', %d, '%s', %d.%03d, 'sys')", //HARDCODE
															g_settings.m_pszModule?g_settings.m_pszModule:"Libretto",//"Libretto", //pEndObj->m_pszDBName?pEndObj->m_pszDBName:"Libretto",  // need to get this.
								//							pEndObj->m_pszQueue?pEndObj->m_pszQueue:"Queue",  // ok need to change this too!  argh! its not the endpoint object, that is the automation module....
															g_settings.m_pszQueue?g_settings.m_pszQueue:"Command_Queue",
															pchEncodedLocal?pchEncodedLocal:szII,
															256,
															g_settings.m_pszDrivenHost,
															timebNowTick.time + g_data.m_nTimeIncr,
															timebNowTick.millitm + g_data.m_nMillitmIncr
															);
	//	g_pnucleus->m_msgr.DM(MSG_ICONHAND, NULL, "Nucleus:debug", "Trigger SQL: %s", szCommandSQL); // Sleep(50); //(Dispatch message)
	EnterCriticalSection(&m_critOutputSQL);
								int nSQLReturn = m_pdbOutput->ExecuteSQL(m_pdbOutputConn, szCommandSQL, errorstring);
	LeaveCriticalSection(&m_critOutputSQL);
								if(nSQLReturn<DB_SUCCESS)
								{
							//**MSG
		if(g_ptabmsgr)
		{
			g_ptabmsgr->DM(MSG_ICONHAND, NULL, szTickerSource, "ERROR %d executing SQL: %s", nSQLReturn, errorstring); // Sleep(50); //(Dispatch message)

			if(bAsRun)
			{
	g_ptabmsgr->DM(MSG_ICONERROR, 
		(((g_settings.m_pszSCTE104AsRunDestination)&&(strlen(g_settings.m_pszSCTE104AsRunDestination)))?g_settings.m_pszSCTE104AsRunDestination:"streamdata_asrun"), 
						eventIDbuffer, 
						"Error %s sending data to device at %s: %s", nSQLReturn, g_settings.m_pszDrivenHost, g_settings.m_bReadableEncodedAsRun?szReadableII:szII);
			}
		}


									g_data.SendAsRunMsg(CX_SENDMSG_ERROR, g_settings.m_nAutoServerListNumber, eventIDbuffer, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Error %s sending data to device at %s: %s", nSQLReturn, g_settings.m_pszDrivenHost, g_settings.m_bReadableEncodedAsRun?szReadableII:szII);


								}
								else
								{
									g_data.m_nLastTime = timebNowTick.time;
									g_data.m_nLastMillitm = timebNowTick.millitm;

// this is the call that fails when SQL is busy.
									g_data.SendAsRunMsg(CX_SENDMSG_INFO, g_settings.m_nAutoServerListNumber, eventIDbuffer, g_settings.m_pszInternalAppName?g_settings.m_pszInternalAppName:"StreamData", "Sent data to device at %s: %s", g_settings.m_pszDrivenHost, g_settings.m_bReadableEncodedAsRun?szReadableII:szII);
		
							
		if(g_ptabmsgr)
		{
			if(g_settings.m_ulDebug&STREAMDATA_DEBUG_TRIGGER) 
			{
				g_ptabmsgr->DM(MSG_ICONHAND, NULL, szTickerSource, "command %d sent [est time %d.%.03d][%d.%03d + trig %.0f][db %d.%03d]: %s", 
					pEvent->m_ppEvents[i]->m_nEventID,
					timebNowTick.time,
					timebNowTick.millitm,
					timebBeginTick.time,
					timebBeginTick.millitm,
					pEvent->m_ppEvents[i]->m_dblTriggerTime, 
					timebNowTick.time + g_data.m_nTimeIncr,
					timebNowTick.millitm + g_data.m_nMillitmIncr,
					szII); // Sleep(50);//(Dispatch message)
			}

			if(bAsRun)
			{
	g_ptabmsgr->DM(MSG_ICONNONE, 
		(((g_settings.m_pszSCTE104AsRunDestination)&&(strlen(g_settings.m_pszSCTE104AsRunDestination)))?g_settings.m_pszSCTE104AsRunDestination:"streamdata_asrun"), 
						eventIDbuffer, 
						"Sent data to device at %s: %s", g_settings.m_pszDrivenHost, g_settings.m_bReadableEncodedAsRun?szReadableII:szII);
			}

		}


								}
							
							} // if(pchEncodedLocal)&& strlen
							if(pchEncodedLocal) free(pchEncodedLocal);
						} // else from if bTabulator event.
					} // szII had nonzero length 
				}//if(pEvent->m_ppEvents[i]->m_szIdentifier.GetLength())
			}// if((pEvent->m_ppEvents)&&(pEvent->m_ppEvents[i]))
			if(g_dlldata.thread[0]->bKillThread) break;
			i++;
			
		} // while
		LeaveCriticalSection(&pEvent->m_critEvents);
//		m_bDoingOppCancel = false;
//		m_bDoingOpp=false;
		return CLIENT_SUCCESS;
	}
	else
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, szTickerSource, "PlayEvent event exited with no action [%d%d%d%d %d]", 
		(m_pdbOutput!=NULL), 
		(m_pdbOutputConn!=NULL),
		(pEvent!=NULL),
		(pEvent->m_ppEvents!=NULL),
		(pEvent->m_nEvents>0)
		); //(Dispatch message)

		if(pszInfo)
		{
			sprintf(pszInfo, "PlayEvent event exited with no action [%d%d%d%d %d]", 
				(m_pdbOutput!=NULL), 
				(m_pdbOutputConn!=NULL),
				(pEvent!=NULL),
				(pEvent->m_ppEvents!=NULL),
				(pEvent->m_nEvents>0)
				);
		}
	}

//	m_bDoingOppCancel = false;
//	m_bDoingOpp=false;
	return CLIENT_ERROR;
}



