// StreamData.h : main header file for the StreamData DLL
//

#if !defined(AFX_StreamData_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
#define AFX_StreamData_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#import "msxml3.dll" named_guids 
using namespace MSXML2;

#include "../../../Applications/Generic/Client/ClientDefines.h"
#include "../../../Common/MSG/Messager.h"
#include "../../../Common/TXT/FileUtil.h"
#include "..\..\..\Cortex\3.0.4.5\CortexDefines.h"

//#include "..\..\..\..\Common\MFC\Inet\Inet.h"
//#include "..\..\..\..\Common\API\Miranda\IS2Core.h"  // for IS2 util functions


#define REMOVE_CLIENTSERVER  // for web-based UI only


#define SETTINGS_FILENAME "streamdata.ini"
#define VERSION_STRING "1.0.4.3"    //20150128 1.0.4.x for Rogers. //20151023: 1.0.3.x version for DNU // 1.0.2.x version for Shaw //1.0.1.x version for A&E.  1.0.0.x for Fox

// 1.0.3.0 had this string passed in:
/////////////////////////////////////////////////////////////////////////////////////////////
// DNU:
// going to expect a string like:
// Local_Break|154|8000|0|MACRO:7|MACRO:8|0|0|826*|826#
// local break event type|automation stream ID|inpoint preroll|outpoint preroll|inpoint token|outpoint token|dtmf inpoint preroll|dtmf outpoint preroll|dtmf inpoint sequence|dtmf outpoint sequence
//
// we just pass everything in.  dont need settings.  we grab the first two records after "now" that matches both tokens.  If the next one up is the outpoint, we send the outpoint message
// if inpoint, calc the duration and send that.
// 
// 
/////////////////////////////////////////////////////////////////////////////////////////////
// but now we are going to extract presence or absence of DTMF from the title field of the automation.
// all the rest stays the same, just the last tow fields are comitted and replaced with the full title field from automation, 
// Local_Break|154|8000|0|MACRO:7|MACRO:8|0|0|826* DTMF (Macro 7)
// the last field is the full title field, to which the following parsing will apply:
// we will only include a DTMF_descriptor_request in the SCTE104 messages when all of the first 4 characters are valid DTMF characters (0 through 9, #, *, A, B, C, and D).
// if that test is passed, we will send the first four chars as the DTMF string.  The length 4 is hardcoded.

//1.0.3.2 puts a time offset param in so we can back up the query, so as to trigger it right on the time
// plus immediate spilce types.

//1.0.4.0 keeps all that but is on Harris and uses secondaries tied to primaries
// 1.0.4.1 changed to immediate splice type - oh actually already in there if preroll is <=0.  great.  But also default value of event type is now 137 = GPI.
// 1.0.4.2 capturing NULL on database returns zero records getautomationdata. copyright to 2017
// 1.0.4.3 pegging insertion type to incoming token, instead of result of db query

// TODO 1.0.4.X adding memory structure for last known device and last known duration per channel. 


#define STREAMDATA_EVENT_NATIONAL								0x00000001   // any token has changed - must iterate to find which.
#define STREAMDATA_EVENT_LOCAL   								0x00000002   // any token has changed - must iterate to find which.
//#define STREAMDATA_EVENT_LOCAL120								0x00000004   // any token has changed - must iterate to find which.
//#define STREAMDATA_EVENT_BREAKEND								0x00000008   // any token has changed - must iterate to find which.
#define STREAMDATA_EVENT_MOD										0x00000100

#define STREAMDATA_ACTION_TYPE_NONE			0x00000000  // nothing
#define STREAMDATA_ACTION_TYPE_SQL			0x00000001  // run a SQL query
#define STREAMDATA_ACTION_TYPE_CXNET		0x00000002  // send a set of commands and chars in the cortex binary protocol on a network connection
#define STREAMDATA_ACTION_TYPE_INET			0x00000003  // send a set of plain old chars on a network connection


/* event_type values */  
#define STREAMDATA_EXCLUSION_TYPE_UNKNOWN				0x00 
#define STREAMDATA_EXCLUSION_TYPE_ON						0x01 
#define STREAMDATA_EXCLUSION_TYPE_OFF						0x02 
#define STREAMDATA_EXCLUSION_TYPE_DURATION			0x03 


// pattern matching
#define STREAMDATA_PATTERN_ERROR							-1
#define STREAMDATA_PATTERN_MATCH							0 
#define STREAMDATA_PATTERN_NO_MATCH						1 


#define STREAMDATA_EVENTDATA_NOMATCH					0x00   // there are no tokens in the data
#define STREAMDATA_EVENTDATA_PROGID						0x01   // a prog ID exists in the data
#define STREAMDATA_EVENTDATA_NATIONAL_START		0x02   // the national start token exists in the data
#define STREAMDATA_EVENTDATA_NATIONAL_END			0x04   // the national end token exists in the data
#define STREAMDATA_EVENTDATA_LOCAL_START			0x10   // the local start token exists in the data
#define STREAMDATA_EVENTDATA_LOCAL_END				0x20   // the local end token exists in the data

// Local -- 0x32 (Distributor Advertisement Start)  //National -- 0x30 (Provider Advertisement Start)
#define STREAMDATA_SOURCE_LOCAL						0x32  
#define STREAMDATA_SOURCE_NATIONAL				0x30  


// bulk sizes
#define STREAMDATA_DATA_ARRAY_INCREMENT		64 


// from cortex.h but static so just define here.
//send message type
#define CX_SENDMSG_ERROR   0
#define CX_SENDMSG_INFO	   1
#define CX_SENDMSG_NOTE	   2


#define STREAMDATA_DEBUG_SQL								0x00000001
#define STREAMDATA_DEBUG_EVENTS							0x00000002
#define STREAMDATA_DEBUG_TRIGGER						0x00000004
#define STREAMDATA_DEBUG_AUTOLIST						0x00000008
#define STREAMDATA_DEBUG_AUTOEVENTS					0x00000010
#define STREAMDATA_DEBUG_TIMER							0x00000020
#define STREAMDATA_DEBUG_TIMECODE						0x00000040
#define STREAMDATA_DEBUG_CALLFUNC						0x00000080
#define STREAMDATA_DEBUG_PATTERN						0x00000200
#define STREAMDATA_DEBUG_STATE							0x00000400
#define STREAMDATA_DEBUG_OUTPUTVARS					0x00000800
#define STREAMDATA_DEBUG_AUTOLISTVARS				0x00001000
#define STREAMDATA_DEBUG_AUTOEVENTVARS			0x00002000
#define STREAMDATA_DEBUG_FINALOUTPUTVARS		0x00004000


#define STREAMDATA_INVALID_TIMEADDRESS				0xffffffff
#define STREAMDATA_USECURRENT_TIMEADDRESS		0xfffffffe


#define STREAMDATA_FLAG_FOUND 0x00000010


#define STREAMDATA_EXISTS					0x01
#define STREAMDATA_NONEXISTENT		0x02
#define STREAMDATA_UNKNOWN				0x00



/////////////////////////////////////////////////////////////////////////////
// COutputDlg dialog

class COutputDlg : public CDialog
{
// Construction
public:
	COutputDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(COutputDlg)
	enum { IDD = IDD_DIALOG_OUTPUT };
	CListCtrl	m_lc;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COutputDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation


public:
		//sizing
	BOOL m_bVis;
	BOOL 	m_bNewSizeInit;
	CRect m_rcDlg;
	CRect m_rcCtrl[1];

	void OnExit();
	void UpdateOutputDialog();

protected:

	// Generated message map functions
	//{{AFX_MSG(COutputDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


//#include "StreamDataCore.h"
//#include "StreamDataData.h"
//#include "StreamDataSettings.h"

// HERE is theACII SET:
// starting here -> !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
// note the space in the first position.


/////////////////////////////////////////////////////////////////////////////
// CStreamDataApp
// See StreamData.cpp for the implementation of this class
//

class CStreamDataApp : public CWinApp
{
public:
	CStreamDataApp();
	int  DLLCtrl(void** ppvoid, UINT nType);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStreamDataApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	char m_szSettingsFilename[MAX_PATH];
	bool m_bInitInstanceComplete;

	void MB(char* pszMessage, ...);

	//{{AFX_MSG(CStreamDataApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////



//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_StreamData_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
