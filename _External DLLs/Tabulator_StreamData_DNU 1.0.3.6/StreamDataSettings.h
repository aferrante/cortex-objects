#if !defined(AFX_StreamDataSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_)
#define AFX_StreamDataSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// StreamDataSettings.h : header file
//
/*
// Dialog Data
	//{{AFX_DATA(CStreamDataSettings)
	enum { IDD = IDD_DIALOG_SETTINGS };
	BOOL	m_bAutoconnect;
	BOOL	m_bTickerPlayOnce;
	CString	m_szHost;
	CString	m_szBaseURL;
	int		m_nBatch;
	int		m_nPort;
	int		m_nPollIntervalMS;
	CString	m_szProgramID;
	int		m_nRequestIntervalMS;
	int		m_nTickerPort;
	CString	m_szTickerHost;
	BOOL	m_bSmut_ReplaceCurses;
	int		m_nDoneCount;
	//}}AFX_DATA

	// internal
	bool m_bHasDlg;
	bool m_bIsServer;
	bool m_bObtainedFromServer;
	bool m_bFromDialog;

	int m_nMaxMessages;

	// UI
	COLORREF m_crCommitted;
*/

/////////////////////////////////////////////////////////////////////////////
// CStreamDataSettings 

class CStreamDataSettings //: public CDialog
{
// Construction
public:
	CStreamDataSettings(/*CWnd* pParent = NULL*/);   // standard constructor
	virtual ~CStreamDataSettings();   // standard destructor
	CRITICAL_SECTION m_crit;

		// database stuff
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;



//	bool m_bUseFeed; // use the data download thread
//	char* m_pszFeed;  // the Feed table name
//	char* m_pszTicker;  // the Ticker table name
	char* m_pszMessages;  // the Messages table name
	char* m_pszAsRun;  // the As-run table name
	char* m_pszSettings;  // the Settings table name
//	char* m_pszStreamDatas;  // the StreamDatas table name

	bool m_bMillisecondMessaging;			// use millisecond resolution for messages and asrun


	bool m_bEnableTones;			// allows us to turn off the function, leave the others
	bool m_bEnableSCTE104;		// allows us to turn off the function, leave the others

// tables with data
	char* m_pszAutoModule;  // as in sentinel
	char* m_pszAutoLists;   // as in channels
	char* m_pszAutoServers;   // as in connections
	char* m_pszAutoEvents;   // as in event lists

	char* m_pszDestinationModule;  // as in Libretto
	char* m_pszDestinationTable;   // as in Destinations
	
	char* m_pszCortexModule;  // as in cortex
	char* m_pszCortexLiveEvents;   // as in live event lists, needed for live clip metadata etc.

/*
	char* m_pszReplacementRules;
	char* m_pszDescriptionPattern;
	char* m_pszClassificationRules;
	char* m_pszButtonsTableName;  // for determining state
*/

/*
	char* m_pszOpportuneEvents;
	char* m_pszExclusionRules;
*/	

	char* m_pszTabulatorModule;
	char* m_pszTabulatorHost;  // the Tabulator host IP or name
	int m_nTabulatorPort;  // the Tabulator host port

/*
	char* m_pszBlockedIDToken;
	char* m_pszBlockedDescToken;

	char* m_pszUnknownIDToken;
	char* m_pszUnknownDescToken;

	char* m_pszColDelimToken;
	char* m_pszClassTestFormat;

	char* m_pszTimerStartToken;
	char* m_pszTimerToken;
	char* m_pszTimerTable;

*/
	bool m_bReadableEncodedAsRun;	

	bool m_bShowDiagnosticWindow;	

//	char* m_pszDestinationHost;  // do we want to direct drive the host ever?  or always shoot to Libretto or other module.? (still need the host either way but need to know what drive mode)

//	need module info, libretto command queue etc.

	char* m_pszModule;  // the module name (Libretto)
	char* m_pszQueue;  // the module's queue table name (Queue of Libretto.dbo.Queue)

	// need to be able to send 5 events.

	char* m_pszSCTE104BlackoutEvent;  // the event identifier of the blackout event
	char* m_pszSCTE104NationalEvent;  // the event identifier of the national break event
	char* m_pszSCTE104LocalEvent;  // the event identifier of the local event
//	char* m_pszSCTE104Local060Event;  // the event identifier of the local 060 event
//	char* m_pszSCTE104Local120Event;  // the event identifier of the local 120 event
//	char* m_pszSCTE104BreakEndEvent;  // the event identifier of the Break end event

//	char* m_pszSCTE104NationalEventDTMFsequence;  // the DTMFsequence of the national break event
//	char* m_pszSCTE104LocalEventDTMFsequence;  // the DTMFsequence of the local 060 event
//	char* m_pszSCTE104Local060EventDTMFsequence;  // the DTMFsequence of the local 060 event
//	char* m_pszSCTE104Local120EventDTMFsequence;  // the DTMFsequence of the local 120 event
//	char* m_pszSCTE104BreakEndEventDTMFsequence;  // the DTMFsequence of the Break End event

	char* m_pszSCTE104NationalInpointID;  // the Identifying token of the inpoint event for national
	char* m_pszSCTE104NationalOutpointID;  // the Identifying token of the outpoint event for national
	char* m_pszSCTE104LocalInpointID;  // the Identifying token of the inpoint event for local
	char* m_pszSCTE104LocalOutpointID;  // the Identifying token of the outpoint event for local
//	char* m_pszSCTE104Local060ID;  // the Identifying token of the inpoint event for LCL060  
//	char* m_pszSCTE104Local120ID;  // the Identifying token of the inpoint event for LCL120

	char* m_pszSCTE104ProgramTag;  // the Identifying token of the program identifier

	int m_nSCTE104NationalPreroll;  // number of milliseconds preroll
	int m_nSCTE104LocalPreroll;  // number of milliseconds preroll


	int m_nNationalMinimumDurationMS; // number of milliseconds, if less than this, message will not be sent
	int m_nLocalMinimumDurationMS; // number of milliseconds, if less than this, message will not be sent
	int m_nBlackoutMinimumDurationMS; // number of milliseconds, if less than this, message will not be sent
	

//	int m_nNationalHarrisTagSecEventType; // uses tag only if event type matches.  -1 turns this off and just uses any secondary event.
//	int m_nLocalHarrisTagSecEventType; // uses tag only if event type matches.  -1 turns this off and just uses any secondary event.
//	bool m_bUseNationalInpointSecTiming; // uses the secondary event's timing, instead of its primary, for the duration calc
//	bool m_bUseNationalOutpointSecTiming;// uses the secondary event's timing, instead of its primary, for the duration calc



/*
//A&E not using DTMF


	char* m_pszTabulatorPlugInModule;  // module plugin name for DTMF commands, "Videoframe"
	char* m_pszSendDTMFToken;  // the event identifier of the Send DTMF call function token of the videoframe module
//  Tabulator:Videoframe:Send_DTMF|D123

	// now canged to filter on IP address, like this:
//  Tabulator:Videoframe:Send_DTMF|D123|C0A80005
// where "C0A80005" = hex IP address, e.g. 192.168.0.5 = "C0A80005"

	char* m_pszDTMFAddress;  // the decimal IP address of the unit handling the DTMF tones.
	unsigned char m_ucDTMFAddress[4];

	// old serial tones settings still needed:

	char* m_pszInpointID;  // the harris event ID of the inpoint event
	char* m_pszOutpointID;  // the harris event ID of the outpoint event


// new settings, these were internal before.
	int m_nHarrisTagSecEventType; // uses tag only if event type matches.  -1 turns this off and just uses any secondary event.
	bool m_bUseInpointSecTiming; // uses the secondary event's timing, instead of its primary, for the duration calc
	bool m_bUseOutpointSecTiming;// uses the secondary event's timing, instead of its primary, for the duration calc
	char* m_pszDTMFFormat;  // the format of the DTMF string

*/

	// going to use these for SCTE
	int m_nDurationUnitDivisor; // divides milliseconds to get unit.  so 1000 gets you seconds. 100 gets you tenths.  33 gets you NTSC frames
	bool m_bRoundToNearestUnit;



/*
	char* m_pszCrawlEventID;  // just do one for now.
	int m_nCrawlDurationMS;
	int m_nCrawlPaddingMS;
  int m_nSecType;
*/





	// Let's make this one a table, actually
//	char** m_ppszOpportuneEvent;  // the event identifier(s) of the event(s) to be played if there is an serialtones


	char* m_pszDrivenHost;  // IP address of the VANC inserter for libretto to route properly


	int m_nTriggerBuffer; // number of items to hold in a buffer for triggering

/*
	int m_nProgramStreamOffset;
	int m_nTimerExpiryManualDelayMS;
	int m_nTimerExpiryAutoDelayMS;
	int m_nTimerStartOffsetMS;
	int m_nTimerDurationAdjustMS;
*/
	int m_nAutomationAnalysisLookahead;  // number of events to parse.
//	char* m_pszAutoServer;  // as in MAIN-DS  - removed for iTX
	int m_nAutoServerListNumber;   // as in 1  - this is the stream number.
	bool m_bAutomationLookahead;			// value imported from Harris
	int m_nAutoListGetDelayMS;   // delay in between incrementer change and a list event get.
	int m_nAutomationLoggingLookahead;  // number of events to log from top.

	int m_nQueryOffsetMS; // number of milliseconds to back up the query that figures out everything.

	bool m_bLocalQueryIsExactOnID;			// local break query uses plain ID as passed in, not %|xxx|% for handling ...|MACRO:8|... inside a data field.

/*
	char* m_pszInEvent;  // the event identifier of the transition in (Server_Ticker_In) command
	char* m_pszOutEvent;  // the event identifier of the transition out (Server_Ticker_Out) command
	char* m_pszTextEvent;  // the event identifier of the text message event
*/

/*
	char* m_pszTonesAsRunFilename; 
	char* m_pszTonesAsRunDestination;
	char* m_pszTonesAsRunFileLocation;
	char* m_pszTonesAsRunFileFormat;
	int m_nTonesAsRunLogOffsetInterval;
	bool m_bTonesUseDurationAddition;  // addup durations in intervals, instead of using difference in time codes.

	bool m_bAsRunDTMF;
	bool m_bAsRunTestDTMF;
*/

	char* m_pszSCTE104AsRunFilename; 
	char* m_pszSCTE104AsRunDestination;
	char* m_pszSCTE104AsRunFileLocation;
	char* m_pszSCTE104AsRunFileFormat;
	int m_nSCTE104AsRunLogOffsetInterval;
	bool m_bSCTE104UseDurationAddition;  // addup durations in intervals, instead of using difference in time codes.
	int m_nSCTE104InpointOverrideType;
	int m_nSCTE104OutpointOverrideType;



	bool m_bAsRunNationalBreak;
	bool m_bAsRunLocalBreak;
	bool m_bAsRunBlackout;
//	bool m_bAsRunLocal060Break;
//	bool m_bAsRunLocal120Break;
//	bool m_bAsRunBreakEnd;

//	bool m_bDoNationalBreakTones;
//	int m_nNationalBreakTonesDelayMS;

/*
	char* m_pszTickerMessagePreamble; 

	char* m_pszTickerBackplateName;   // for external graphics engine use.
	char* m_pszTickerLogoName;    // for external graphics engine use.
	int m_nTickerBackplateID; 
	int m_nTickerLogoID; 
	int m_nTickerTextID; 

	int m_nTickerEngineType; 
	int m_nTriggerBuffer; // number of items to hold in a buffer for triggering
	int m_nDefaultDwellTime;

	char* m_pszMessageToken; 
	char* m_pszAux1Token; 
	char* m_pszAux2Token; 

	bool m_bAutoPurgeExpired;
	bool m_bAutoPurgeExceeded;
	int m_nAutoPurgeExpiredAfterMins;
	int m_nAutoPurgeExceededAfterMins;
	bool m_bAutostartTicker;
	bool m_bAutostartData;
	bool m_bUseLocalTime;  // or use unixtime

	int m_nGraphicsHostType;

	//Harris Icon specific
	int m_nLayoutSalvoFormat; // 0= 4 digit numerical, 1=8 digit numerical, 2 = strings  
*/
	//exposed
	char* m_pszAppName;
	char* m_pszInternalAppName;
	char* m_pszAboutText;
	char* m_pszSettingsText;

	int GetFromDatabase(char* pszInfo=NULL);  //only get.  Interface publishes

	int Settings(bool bRead);
//	int DoTonesAsRunFilename();
//	int DoTonesAsRunDestinationName(char* pchOldName, char* pchNewName);
	int DoSCTE104AsRunFilename();
	int DoSCTE104AsRunDestinationName(char* pchOldName, char* pchNewName);
//	int SetTonesIP(char* pchIP);

//	CString RemoteSettingsToString();
//	int StringToRemoteSettings(CString szSettings);

	unsigned long m_ulDebug;  // prints out debug statements that & with this.


	bool m_bUseTimeCode;			// use adrienne timecode
	int m_nFrameRate;   // for time code addresses NTSC=30, PAL=25
	bool m_bDF;  // Drop frame




/*
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStreamDataSettings)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CStreamDataSettings)
	afx_msg void OnButtonTestfeed();
	virtual void OnOK();
	afx_msg void OnButtonConnect();
	virtual BOOL OnInitDialog();
	afx_msg void OnCheckPlayonce();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

*/
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_StreamDataSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_)
