// OpportunityCore.h: interface for the COpportunityCore class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OpportunityCORE_H__D426F8FF_BC44_4257_869A_044999574215__INCLUDED_)
#define AFX_OpportunityCORE_H__D426F8FF_BC44_4257_869A_044999574215__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "../../../Common/TXT/BufferUtil.h"
#include "../../../Common/API/Adrienne/AdrienneUtil.h"

class COpportunityCore  
{
public:
	COpportunityCore();
	virtual ~COpportunityCore();

	CBufferUtil m_bu;
	CAdrienneUtil       m_tc; // time code utility (if installed - otherwise, no bother)

	BOOL IsPointInWnd(CPoint point, CWnd* pWnd);
	BOOL IsPointInRect(CPoint point, CRect rcClientRect);
  CString CALEncodeBrackets(CString szString);

};

#endif // !defined(AFX_OpportunityCORE_H__D426F8FF_BC44_4257_869A_044999574215__INCLUDED_)
