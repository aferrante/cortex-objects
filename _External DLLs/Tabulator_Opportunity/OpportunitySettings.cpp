// OpportunitySettings.cpp : implementation file
//

#include "stdafx.h"
#include "Opportunity.h"
#include "OpportunityData.h"
#include "OpportunitySettings.h"
#include <process.h>
#include "../../../Cortex Objects/Tabulator/TabulatorDefines.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


extern DLLdata_t      g_dlldata;
extern COpportunityData			g_data;
extern COpportunitySettings	g_settings;
extern CMessager*			g_ptabmsgr;
extern COpportunityApp			theOppApp;

extern void TimeCodeDataThread(void* pvArgs);


/////////////////////////////////////////////////////////////////////////////
// COpportunitySettings dialog


COpportunitySettings::COpportunitySettings(/*CWnd* pParent /*=NULL*/)
//	: CDialog(COpportunitySettings::IDD, pParent)
{
/*
	//{{AFX_DATA_INIT(COpportunitySettings)
	m_bAutoconnect = FALSE;
	m_bTickerPlayOnce = TRUE;
	m_szHost = _T("127.0.0.1");
	m_szBaseURL = _T("http://www.feed.com/");
	m_nBatch = 250;
	m_nPort = TABULATOR_PORT_CMD;
	m_nPollIntervalMS = 5000;
	m_szProgramID = _T("10");
	m_nRequestIntervalMS = 0;
	m_nTickerPort = 7795;
	m_szTickerHost = _T("127.0.0.1");
	m_bSmut_ReplaceCurses = FALSE;
	m_nDoneCount = 3;
	//}}AFX_DATA_INIT
*/
	m_ulDebug = 0;
//	m_nProgramStreamOffset = 0;
//	m_nTimerExpiryManualDelayMS=0;
//	m_nTimerExpiryAutoDelayMS=0;

//	m_nTimerStartOffsetMS = 0;
//	m_nTimerDurationAdjustMS = 0;


	m_pszAppName = NULL;
	m_pszInternalAppName = NULL;
	m_pszAboutText = NULL;
	m_pszSettingsText = NULL;

	m_bMillisecondMessaging=true;			// use millisecond resolution for messages and asrun

	m_bShowDiagnosticWindow = false;

/*
	m_pszMessageToken = NULL; 
	m_pszAux1Token = NULL; 
	m_pszAux2Token = NULL; 
*/

	// DSN params
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;

/*
	m_pszFeed = NULL;  // the Messages table name
	m_pszTicker = NULL;
*/
	
//	m_pszAsRunFilename = NULL;
//	m_pszAsRunDestination = NULL;

	m_pszMessages = NULL;  // the Messages table name
	m_pszAsRun = NULL;  // the As-run table name
//	m_pszEventView = NULL;  // the Event view name // to pull events if nec.
	m_pszSettings = NULL;
	m_pszOpportunitys = NULL;  // the Opportunitys table name



	m_pszCrawlEventID=NULL;  // just do one for now.
	m_nCrawlDurationMS=-1;
	m_nSecType=128;  // pure sec event
	m_nCrawlPaddingMS=5000;



//	m_pszReplaceAdEvent = NULL;  // the event identifier of the transition in (Server_Ticker_In) command
//	m_pszReplaceContentEvent = NULL;  // the event identifier of the transition in (Server_Ticker_In) command
//	m_pszEventNotificationEvent = NULL;  // the event identifier of the "BLOCK only" event
//	m_pszRejoinMainEvent = NULL;  // the event identifier of the PASS tabulator event (includes Rejoin Main)
//	m_pszTimerEvent = NULL;  // the event identifier of the BLOCK GPO w Timer event

	m_pszAutoModule = NULL;
	m_pszAutoLists = NULL;
	m_pszAutoEvents = NULL;
//	m_pszReplacementRules = NULL;
//	m_pszDescriptionPattern = NULL;
//	m_pszClassificationRules = NULL;
	m_pszTabulatorModule = NULL;
	m_pszTabulatorHost=NULL;  // the Tabulator host IP or name
	m_nTabulatorPort = TABULATOR_PORT_CMD;  // the Tabulator host port

//	m_pszButtonsTableName=NULL;  // for determining state


	m_pszOpportuneEvents=NULL;
	m_pszExclusionRules=NULL;



/*
	m_pszBlockedIDToken = NULL;
	m_pszBlockedDescToken = NULL;

	m_pszUnknownIDToken = NULL;
	m_pszUnknownDescToken = NULL;


	m_pszColDelimToken = NULL;
	m_pszClassTestFormat = NULL;

	m_pszTimerToken=NULL;
	m_pszTimerStartToken=NULL;
	m_pszTimerTable=NULL;
*/


	m_pszModule=NULL;  // the module name (Libretto)
	m_pszQueue=NULL;  // the module's queue table name (Queue of Libretto.dbo.Queue)

	m_pszDrivenHost=NULL;  // IP address of the VANC inserter for libretto to route properly

	m_nTriggerBuffer = 32;

	m_nAutomationAnalysisLookahead=100;  // number of events to parse.


	m_pszAutoServer=NULL;  // as in MAIN-DS
	m_nAutoServerListNumber=0;   // as in 1
	m_bAutomationLookahead = false;

	m_bReadableEncodedAsRun = true;

//	m_pszDestinationHost = NULL;

/*
	m_pszTickerMessagePreamble = NULL;

	m_pszTickerBackplateName = NULL; 
	m_pszTickerLogoName = NULL; 
	m_nTickerBackplateID=0; 
	m_nTickerLogoID=2; 
	m_nTickerTextID=1;
	m_nTriggerBuffer  =12;

	m_nTickerEngineType = Opportunity_TICKER_ENGINE_INT; // send events to Libretto or wherever!

	m_nMaxMessages = 500;


	m_pszInEvent=NULL;  // the event identifier of the transition in (Server_Ticker_In) command
	m_pszOutEvent=NULL;  // the event identifier of the transition out (Server_Ticker_Out) command
	m_pszTextEvent=NULL;  // the event identifier of the text message event

	m_nGraphicsHostType = TABULATOR_DESTTYPE_CHYRON_CHANNELBOX;

	m_bAutoPurgeExpired=false;
	m_bAutoPurgeExceeded=false;
	m_nAutoPurgeExpiredAfterMins=60;
	m_nAutoPurgeExceededAfterMins=60;

	m_bAutostartTicker=false;
	m_bAutostartData=false;
	m_bUseLocalTime=true;  // or use unixtime


	m_nLayoutSalvoFormat = TABULATOR_HARRIS_ICONII_LSFORMAT_ALPHA;
*/


	InitializeCriticalSection(&m_crit);


	m_bUseTimeCode = false;
	m_nFrameRate=30;   // for time code addresses NTSC=30, PAL=25
	m_bDF=true;  // Drop frame

}

COpportunitySettings::~COpportunitySettings()
{
	EnterCriticalSection(&m_crit);
	if(m_pszAppName) free(m_pszAppName);
	if(m_pszInternalAppName) free(m_pszInternalAppName);
	if(m_pszAboutText) free(m_pszAboutText);
	if(m_pszSettingsText) free(m_pszSettingsText);


	if(m_pszOpportuneEvents) free(m_pszOpportuneEvents);
	if(m_pszExclusionRules) free(m_pszExclusionRules);

	
	if(m_pszDSN) free(m_pszDSN);
	if(m_pszUser) free(m_pszUser);
	if(m_pszPW) free(m_pszPW);
//	if(m_pszFeed) free(m_pszFeed);
//	if(m_pszTicker) free(m_pszTicker);
//	if(m_pszAsRunFilename) free(m_pszAsRunFilename);
/*
	if(m_pszTickerMessagePreamble) free(m_pszTickerMessagePreamble);
	if(m_pszTickerBackplateName) free(m_pszTickerBackplateName);
	if(m_pszTickerLogoName) free(m_pszTickerLogoName);
*/
	if(m_pszAsRun) free(m_pszAsRun);
	if(m_pszSettings) free(m_pszSettings);
//	if(m_pszDestinationHost) free(m_pszDestinationHost);
//	if(m_pszAsRunDestination) free(m_pszAsRunDestination);
/*
	if(m_pszModule) free(m_pszModule);
	if(m_pszQueue) free(m_pszQueue);
	if(m_pszInEvent) free(m_pszInEvent);
	if(m_pszOutEvent) free(m_pszOutEvent);
	if(m_pszTextEvent) free(m_pszTextEvent);

	if(m_pszMessageToken) free(m_pszMessageToken);
	if(m_pszAux1Token) free(m_pszAux1Token);
	if(m_pszAux2Token) free(m_pszAux2Token);
*/
	LeaveCriticalSection(&m_crit);
	DeleteCriticalSection(&m_crit);

	if(m_pszTabulatorModule) free(m_pszTabulatorModule);
	if(m_pszTabulatorHost) free(m_pszTabulatorHost);

}

/*

void COpportunitySettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COpportunitySettings)
	DDX_Check(pDX, IDC_CHECK_AUTOCONNECT, m_bAutoconnect);
	DDX_Check(pDX, IDC_CHECK_PLAYONCE, m_bTickerPlayOnce);
	DDX_CBString(pDX, IDC_COMBO_HOST, m_szHost);
	DDX_Text(pDX, IDC_EDIT_BASEURL, m_szBaseURL);
	DDX_Text(pDX, IDC_EDIT_BATCH, m_nBatch);
	DDV_MinMaxInt(pDX, m_nBatch, 0, 2000000);
	DDX_Text(pDX, IDC_EDIT_C_PORT, m_nPort);
	DDV_MinMaxInt(pDX, m_nPort, 1024, 65535);
	DDX_Text(pDX, IDC_EDIT_POLL, m_nPollIntervalMS);
	DDV_MinMaxInt(pDX, m_nPollIntervalMS, 0, 86400000);
	DDX_Text(pDX, IDC_EDIT_PROGRAM, m_szProgramID);
	DDX_Text(pDX, IDC_EDIT_REQ, m_nRequestIntervalMS);
	DDV_MinMaxInt(pDX, m_nRequestIntervalMS, 0, 86400000);
	DDX_Text(pDX, IDC_EDIT_T_PORT, m_nTickerPort);
	DDV_MinMaxInt(pDX, m_nTickerPort, 1024, 65535);
	DDX_Text(pDX, IDC_EDIT_HOST, m_szTickerHost);
	DDX_Check(pDX, IDC_CHECK_SMUT, m_bSmut_ReplaceCurses);
	DDX_Text(pDX, IDC_EDIT_DONE, m_nDoneCount);
	DDV_MinMaxInt(pDX, m_nDoneCount, 0, 2000000);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COpportunitySettings, CDialog)
	//{{AFX_MSG_MAP(COpportunitySettings)
	ON_BN_CLICKED(IDC_BUTTON_TESTFEED, OnButtonTestfeed)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_CHECK_PLAYONCE, OnCheckPlayonce)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COpportunitySettings message handlers

CString COpportunitySettings::RemoteSettingsToString()
{
	CString szSettings="";
	if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_TICKER)&&(g_dlldata.thread[Opportunity_DATA])&&(g_dlldata.thread[Opportunity_TICKER])) 
	{
		szSettings.Format(_T("%s%c%s%c%s%c%s%c%s%c%s%c%d%c%d%c%d%c%d%c%s%c%d%c%d%c%d%c%s%c%d%c%d%c%d"),
				g_dlldata.thread[Opportunity_DATA]->pszThreadName, 28,
				g_dlldata.thread[Opportunity_DATA]->pszBaseURL, 28,
				g_dlldata.thread[Opportunity_DATA]->pszProgramParamName, 28,
				g_dlldata.thread[Opportunity_DATA]->pszProgramID, 28,
				g_dlldata.thread[Opportunity_DATA]->pszBatchParamName, 28,
				g_dlldata.thread[Opportunity_DATA]->pszFromIDParamName, 28,
				g_dlldata.thread[Opportunity_DATA]->nBatchParam, 28,
				g_dlldata.thread[Opportunity_DATA]->nShortIntervalMS, 28,
				g_dlldata.thread[Opportunity_DATA]->nLongIntervalMS, 28,
				(g_dlldata.thread[Opportunity_DATA]->bDirParamStyle?1:0), 28,
				g_dlldata.thread[Opportunity_TICKER]->pszThreadName, 28,
				g_dlldata.thread[Opportunity_TICKER]->nBatchParam, 28,
				g_dlldata.thread[Opportunity_TICKER]->nShortIntervalMS, 28,
				g_dlldata.thread[Opportunity_TICKER]->nLongIntervalMS, 28,
				g_dlldata.thread[Opportunity_TICKER]->pszHost, 28,
				g_dlldata.thread[Opportunity_TICKER]->nPort, 28,
				g_dlldata.thread[Opportunity_TICKER]->nAuxValue, 28,
				g_settings.m_nDoneCount);
	}

	return  szSettings;
}

int COpportunitySettings::StringToRemoteSettings(CString szSettings)
{
	if(szSettings.GetLength())
	{

		char chFields[2];
		sprintf(chFields,"%c",28);
		int f=0, n=0;
		CString szField = szSettings;
		CString szTemp;
		do
		{
			f=szField.Find(chFields);
			if(f>=0)
			{
				n++;
				szTemp = szField.Mid(f+1);
				szField=szTemp;
			}
		} while((f>=0)&&(szField.GetLength()>0)&&(n<=17));

//	AfxMessageBox(szSettings);

		f=0;
		if(n>=16)
		{
			while((f<=n)&&(f<18)) // prevent runaway
			{
				int i=szSettings.Find(chFields);
				if(i>-1){szField = szSettings.Left(i);}
				else {szField=szSettings;}  //last field.

				char* pch = NULL;
				char* pchTemp = NULL;
	//CString foo; foo.Format(_T("parsing field %d of %d: %s"), f, n, szField); AfxMessageBox(foo);
				switch(f)
				{
				case  0: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_DATA)&&(g_dlldata.thread[Opportunity_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
								pchTemp = g_dlldata.thread[Opportunity_DATA]->pszThreadName;
								g_dlldata.thread[Opportunity_DATA]->pszThreadName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  1: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_DATA)&&(g_dlldata.thread[Opportunity_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
								pchTemp = g_dlldata.thread[Opportunity_DATA]->pszBaseURL;
								g_dlldata.thread[Opportunity_DATA]->pszBaseURL  = pch;
								LeaveCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
								g_settings.m_szBaseURL = szField;
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  2: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_DATA)&&(g_dlldata.thread[Opportunity_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
								pchTemp = g_dlldata.thread[Opportunity_DATA]->pszProgramParamName;
								g_dlldata.thread[Opportunity_DATA]->pszProgramParamName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  3: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_DATA)&&(g_dlldata.thread[Opportunity_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
								pchTemp = g_dlldata.thread[Opportunity_DATA]->pszProgramID;
								g_dlldata.thread[Opportunity_DATA]->pszProgramID  = pch;
								LeaveCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
								g_settings.m_szProgramID = szField;
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  4: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_DATA)&&(g_dlldata.thread[Opportunity_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
								pchTemp = g_dlldata.thread[Opportunity_DATA]->pszBatchParamName;
								g_dlldata.thread[Opportunity_DATA]->pszBatchParamName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  5: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_DATA)&&(g_dlldata.thread[Opportunity_DATA])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
								pchTemp = g_dlldata.thread[Opportunity_DATA]->pszFromIDParamName;
								g_dlldata.thread[Opportunity_DATA]->pszFromIDParamName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
//									pData->pszToDateParamName = fu.GetIniString("DataDownload", "ToDateParamName", "To", pData->pszToDateParamName);   //  %p
//									pData->pszFromDateParamName = fu.GetIniString("DataDownload", "FromDateParamName", "From", pData->pszFromDateParamName);   //  %p
//									pData->pszNowParamName = fu.GetIniString("DataDownload", "NowParamName", "time", pData->pszNowParamName);   //  %p
//									pData->pszBatchParamName = fu.GetIniString("DataDownload", "BatchParamName", "limit", pData->pszBatchParamName);   //  %p
//									pData->pszFromIDParamName = fu.GetIniString("DataDownload", "FromIDParamName", "startid", pData->pszFromIDParamName);   //  %p
//									pData->pszGetIDParamName = fu.GetIniString("DataDownload", "GetIDParamName", "id", pData->pszGetIDParamName);   //  %p

//									pData->pszToDateParamName=NULL;				//  %t
//									pData->pszFromDateParamName=NULL;			//  %f
//									pData->pszNowParamName=NULL;					//  %n
//									pData->pszBatchParamName=NULL;				//  %b
//									pData->pszFromIDParamName=NULL;				//  %m  // start id
//									pData->pszGetIDParamName=NULL;				//  %i
				case  6: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_DATA)&&(g_dlldata.thread[Opportunity_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
							g_dlldata.thread[Opportunity_DATA]->nBatchParam  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
							g_settings.m_nBatch = atoi(szField);
						}
					} break;
				case  7: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_DATA)&&(g_dlldata.thread[Opportunity_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
							g_dlldata.thread[Opportunity_DATA]->nShortIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
							g_settings.m_nRequestIntervalMS = atoi(szField);
						}
					} break;
				case  8: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_DATA)&&(g_dlldata.thread[Opportunity_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
							g_dlldata.thread[Opportunity_DATA]->nLongIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
							g_settings.m_nPollIntervalMS = atoi(szField);
						}
					} break;
				case  9: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_DATA)&&(g_dlldata.thread[Opportunity_DATA])) 
						{
							EnterCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
							g_dlldata.thread[Opportunity_DATA]->bDirParamStyle  = ((atoi(szField)>0)?true:false);
							LeaveCriticalSection(&g_dlldata.thread[Opportunity_DATA]->m_crit);
						}
					} break;



				case  10: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_TICKER)&&(g_dlldata.thread[Opportunity_TICKER])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[Opportunity_TICKER]->m_crit);
								pchTemp = g_dlldata.thread[Opportunity_TICKER]->pszThreadName;
								g_dlldata.thread[Opportunity_TICKER]->pszThreadName  = pch;
								LeaveCriticalSection(&g_dlldata.thread[Opportunity_TICKER]->m_crit);
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  11: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_TICKER)&&(g_dlldata.thread[Opportunity_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[Opportunity_TICKER]->m_crit);
							g_dlldata.thread[Opportunity_TICKER]->nBatchParam  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[Opportunity_TICKER]->m_crit);
						}
					} break;
				case  12: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_TICKER)&&(g_dlldata.thread[Opportunity_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[Opportunity_TICKER]->m_crit);
							g_dlldata.thread[Opportunity_TICKER]->nShortIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[Opportunity_TICKER]->m_crit);
						}
					} break;
				case  13: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_TICKER)&&(g_dlldata.thread[Opportunity_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[Opportunity_TICKER]->m_crit);
							g_dlldata.thread[Opportunity_TICKER]->nLongIntervalMS  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[Opportunity_TICKER]->m_crit);
						}
					} break;
				case  14: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_TICKER)&&(g_dlldata.thread[Opportunity_TICKER])) 
						{
							pch = (char*)malloc(szField.GetLength()+1);
							if(pch)
							{
								sprintf(pch, "%s", szField);
								EnterCriticalSection(&g_dlldata.thread[Opportunity_TICKER]->m_crit);
								pchTemp = g_dlldata.thread[Opportunity_TICKER]->pszHost;
								g_dlldata.thread[Opportunity_TICKER]->pszHost  = pch;
								LeaveCriticalSection(&g_dlldata.thread[Opportunity_TICKER]->m_crit);
								g_settings.m_szTickerHost = szField;
								if(pchTemp) free(pchTemp);
								pchTemp = NULL;
							}
						}
					} break;
				case  15: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_TICKER)&&(g_dlldata.thread[Opportunity_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[Opportunity_TICKER]->m_crit);
							g_dlldata.thread[Opportunity_TICKER]->nPort  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[Opportunity_TICKER]->m_crit);
							g_settings.m_nTickerPort = atoi(szField);
						}
					} break;
				case  16: 
					{
						if((g_dlldata.thread)&&(g_dlldata.nNumThreads>Opportunity_TICKER)&&(g_dlldata.thread[Opportunity_TICKER])) 
						{
							EnterCriticalSection(&g_dlldata.thread[Opportunity_TICKER]->m_crit);
							g_dlldata.thread[Opportunity_TICKER]->nAuxValue  = atoi(szField);
							LeaveCriticalSection(&g_dlldata.thread[Opportunity_TICKER]->m_crit);
							g_settings.m_bTickerPlayOnce = ((atoi(szField)>0)?true:false);
						}
					} break;
				case  17: 
					{
						g_settings.m_nDoneCount = atoi(szField);
					} break;

				default: break;
				}
				if(i>-1){szField = szSettings.Mid(i+1);}
				else {szField=_T("");}
				szSettings = szField;
				f++;
			}
			return CLIENT_SUCCESS;
		}
	}
	return CLIENT_ERROR;
	
	
	
}

*/


int COpportunitySettings::Settings(bool bRead)
{
	CFileUtil fu;
	if(fu.GetSettings(theOppApp.m_szSettingsFilename, false)&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
			char* pch;
			pch = fu.GetIniString("Global", "AppName", "Opportunity");
			if(pch)
			{
				if(m_pszAppName) free(m_pszAppName);
				m_pszAppName = pch;
			}

			pch = fu.GetIniString("Global", "InternalAppName", "Opportunity");
			if(pch)
			{
				if(m_pszInternalAppName) free(m_pszInternalAppName);
				m_pszInternalAppName = pch;
			}

			pch = (char*)malloc(strlen(m_pszAppName)+strlen(VERSION_STRING)+strlen(", version ")+1);
			if(pch)
			{
				if(m_pszAboutText) free(m_pszAboutText);
				m_pszAboutText = pch;
				sprintf(m_pszAboutText, "%s, version %s", m_pszAppName, VERSION_STRING);
			}

			pch = fu.GetIniString("Global", "SettingsText", "Opportunity settings...");
			if(pch)
			{
				if(m_pszSettingsText) free(m_pszSettingsText);
				m_pszSettingsText = pch;
			}

			m_ulDebug = fu.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.

			m_pszDSN = fu.GetIniString("Database", "DSN", "Tabulator", m_pszDSN);
			m_pszUser = fu.GetIniString("Database", "DBUser", "sa", m_pszUser);
			m_pszPW = fu.GetIniString("Database", "DBPassword", "", m_pszPW);
//				m_pszSettings = fu.GetIniString("Database", "SettingsTableName", "Settings", m_pszSettings );  // the Settings table name

//			m_pszAsRunFilename = fu.GetIniString("Ticker", "AsRunFileSpec", "autodata_asrun|YD||1|", m_pszAsRunFilename);  // the as run filename
//			m_pszAsRunDestination = fu.GetIniString("Ticker", "AsRunToken", "autodata_asrun", m_pszAsRunDestination);  // the as run destination name


			m_pszMessages=fu.GetIniString("Database", "MessagesTableName", "Messages", m_pszMessages);   // the Messages table name
			m_pszAsRun=fu.GetIniString("Database", "AsRunTableName", "AsRun_Log", m_pszAsRun);  // the As-run table name
			m_pszSettings=fu.GetIniString("Database", "SettingsTableName", "Opportunity_6_Settings", m_pszSettings);  // the settings table name
			m_pszOpportunitys=fu.GetIniString("Database", "OpportunitysTableName", "Opportunitys", m_pszOpportunitys);   // the Opportunitys table name

			m_bMillisecondMessaging = fu.GetIniInt("Messager", "MillisecondMessaging", 1)?true:false;			// use millisecond resolution for messages and asrun
			m_bReadableEncodedAsRun = fu.GetIniInt("Messager", "ReadableEncodedAsRun", 1)?true:false;			// hex encode for readability, otherwise base 64 encoded


			m_pszAutoModule = fu.GetIniString("Database", "AutomationModuleName", "Sentinel", m_pszAutoModule);
			m_pszAutoLists = fu.GetIniString("Database", "AutomationChannelsTableName", "Channels", m_pszAutoLists);
			m_pszAutoEvents = fu.GetIniString("Database", "AutomationEventsTableName", "Events", m_pszAutoEvents);


			m_pszOpportuneEvents = fu.GetIniString("Database", "OpportuneEventsTableName", "OpportuneEvents", m_pszOpportuneEvents);
			m_pszExclusionRules = fu.GetIniString("Database", "ExclusionRulesTableName", "ExclusionRules", m_pszExclusionRules);

//			m_pszReplacementRules = fu.GetIniString("Database", "ReplacementRulesTableName", "ReplacementRules", m_pszReplacementRules);
//			m_pszDescriptionPattern = fu.GetIniString("Database", "DescriptionPatternTableName", "DescriptionPattern", m_pszDescriptionPattern);
//			m_pszClassificationRules = fu.GetIniString("Database", "ClassificationRulesTableName", "ClassificationRules", m_pszClassificationRules);
			m_pszTabulatorModule = fu.GetIniString("Database", "TabulatorModuleName", "Tabulator", m_pszTabulatorModule);
//			m_pszButtonsTableName = fu.GetIniString("Database", "ButtonsTableName", "Buttons", m_pszButtonsTableName);;  // for determining state


			m_pszTabulatorHost = fu.GetIniString("Tabulator", "HostAddress", "127.0.0.1", m_pszTabulatorHost);  // the Tabulator host IP or name
			m_nTabulatorPort = fu.GetIniInt("Tabulator", "CommandServerPort", TABULATOR_PORT_CMD);  // the Tabulator host port

//			m_pszBlockedIDToken		= fu.GetIniString("Output", "BlockedIDToken", "TV+:BLOCKED", m_pszBlockedIDToken);
//			m_pszBlockedDescToken = fu.GetIniString("Output", "BlockedDescToken", "", m_pszBlockedDescToken);

//			m_pszUnknownIDToken		= fu.GetIniString("Output", "UnknownIDToken", "TV+:UNKNOWN", m_pszUnknownIDToken);
//			m_pszUnknownDescToken = fu.GetIniString("Output", "UnknownDescToken", "", m_pszUnknownDescToken);

//			m_nProgramStreamOffset = fu.GetIniInt("Output", "ProgramStreamOffset", 0);  

//			m_nTimerExpiryManualDelayMS = fu.GetIniInt("Output", "TimerExpiryManualDelayMS", 0);  
//			m_nTimerExpiryAutoDelayMS = fu.GetIniInt("Output", "TimerExpiryDelayMS", 0);  


//			m_nTimerStartOffsetMS = fu.GetIniInt("Output", "TimerStartOffsetMS", 0);  
//			m_nTimerDurationAdjustMS = fu.GetIniInt("Output", "TimerDurationAdjustMS", 0);  


//			if(m_nProgramStreamOffset<0)	m_nProgramStreamOffset=0;
//			if(m_nProgramStreamOffset>30) m_nProgramStreamOffset=30;

//			m_pszColDelimToken = fu.GetIniString("Output", "ColDelimToken", "�", m_pszColDelimToken);
//			m_pszClassTestFormat = fu.GetIniString("Output", "ClassTestFormat", "%i�%t", m_pszClassTestFormat);

//			m_pszTimerToken = fu.GetIniString("Output", "TimerToken", "Timer", m_pszTimerToken);
//			m_pszTimerTable = fu.GetIniString("Database", "TimerTableName", "Timers", m_pszTimerTable);
//			m_pszTimerStartToken = fu.GetIniString("Output", "TimerStartToken", "Timer|Timer_Start", m_pszTimerStartToken);

			m_bShowDiagnosticWindow = fu.GetIniInt("Output", "ShowDiagnosticWindow", 0)?true:false;

			m_nAutomationAnalysisLookahead = fu.GetIniInt("Automation", "AutomationAnalysisLookahead", 100);  // number of events to parse.
			m_pszAutoServer = fu.GetIniString("Automation", "AutomationServer", "MAIN-DS", m_pszAutoServer);// as in MAIN-DS
			m_nAutoServerListNumber = fu.GetIniInt("Automation", "AutomationListNumber", 0);  // list number
			m_bAutomationLookahead = fu.GetIniInt("Automation", "AutomationLookahead", 0)?true:false;


			m_pszModule=fu.GetIniString("Output", "DrivenModule", "Libretto", m_pszModule);  // the module name (Libretto)
			m_pszQueue=fu.GetIniString("Output", "DrivenQueue", "Command_Queue", m_pszQueue); // the module's queue table name (Queue of Libretto.dbo.Queue)
			m_pszDrivenHost=fu.GetIniString("Output", "DrivenHost", "0.0.0.0", m_pszDrivenHost);  // IP address of the VANC inserter for libretto to route properly


			char setbuf[MAX_PATH];
			if(m_pszCrawlEventID) strcpy(setbuf, m_pszCrawlEventID); else strcpy(setbuf, "");
			m_pszCrawlEventID=fu.GetIniString("Output", "CrawlEventID", "Crawl2", m_pszCrawlEventID);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszCrawlEventID))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bCrawlEventChanged = true;  // the event identifier has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}


//			fu.SetIniString("Output", "CrawlEventID", m_pszCrawlEventID);  
			m_nCrawlDurationMS = fu.GetIniInt("Output", "CrawlDurationMS", 15000);  
			m_nCrawlPaddingMS = fu.GetIniInt("Output", "CrawlPaddingMS", 5000);  
			m_nSecType = fu.GetIniInt("Output", "SecType", 128);  


			m_nTriggerBuffer  = fu.GetIniInt("Output", "TriggerBufferItems", 16);
			if(m_nTriggerBuffer>256) m_nTriggerBuffer=256;
			if(m_nTriggerBuffer<1) m_nTriggerBuffer=16; //default

/*
			char setbuf[MAX_PATH];
			if(m_pszReplaceAdEvent) strcpy(setbuf, m_pszReplaceAdEvent); else strcpy(setbuf, "");
			m_pszReplaceAdEvent=fu.GetIniString("Output", "ReplaceAdEventID", "ReplaceAd", m_pszReplaceAdEvent);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszReplaceAdEvent))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bReplaceAdEventChanged = true;  // the event identifier has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}

			if(m_pszReplaceContentEvent) strcpy(setbuf, m_pszReplaceContentEvent); else strcpy(setbuf, "");
			m_pszReplaceContentEvent=fu.GetIniString("Output", "ReplaceContentEventID", "ReplaceContent", m_pszReplaceContentEvent);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszReplaceContentEvent))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bReplaceContentEventChanged = true;  // the event identifier has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}

			if(m_pszEventNotificationEvent) strcpy(setbuf, m_pszEventNotificationEvent); else strcpy(setbuf, "");
			m_pszEventNotificationEvent=fu.GetIniString("Output", "EventNotificationEventID", "EventNotification", m_pszEventNotificationEvent);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszEventNotificationEvent))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bEventNotificationEventChanged = true;  // the event identifier has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}

			if(m_pszRejoinMainEvent) strcpy(setbuf, m_pszRejoinMainEvent); else strcpy(setbuf, "");
			m_pszRejoinMainEvent=fu.GetIniString("Output", "RejoinMainEventID", "RejoinMain", m_pszRejoinMainEvent);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszRejoinMainEvent))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bRejoinMainEventChanged = true;  // the event identifier has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}

			if(m_pszTimerEvent) strcpy(setbuf, m_pszTimerEvent); else strcpy(setbuf, "");
			m_pszTimerEvent=fu.GetIniString("Output", "TimerEventID", "TimerEvent", m_pszTimerEvent);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszTimerEvent))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bTimerEventChanged = true;  // the event identifier has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}

*/



/*

			char setbuf[MAX_PATH];
			if(m_pszInEvent) strcpy(setbuf, m_pszInEvent); else strcpy(setbuf, "");
			m_pszInEvent=fu.GetIniString("Ticker", "InEventID", "1000", m_pszInEvent);  // the event identifier of the transition in (Server_Ticker_In) command
								EnterCriticalSection(&g_data.m_critEventsSettings);
			if (strcmp(setbuf, m_pszInEvent)) 
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bInEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}
			if(m_pszOutEvent) strcpy(setbuf, m_pszOutEvent); else strcpy(setbuf, "");
			m_pszOutEvent=fu.GetIniString("Ticker", "OutEventID", "1001", m_pszOutEvent);  // the event identifier of the transition out (Server_Ticker_Out) command
								EnterCriticalSection(&g_data.m_critEventsSettings);
			if (strcmp(setbuf, m_pszOutEvent)) 
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bOutEventChanged = true;  // the event identifier of the transition out (Server_Ticker_Out) command has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}
			if(m_pszTextEvent) strcpy(setbuf, m_pszTextEvent); else strcpy(setbuf, "");
			m_pszTextEvent=fu.GetIniString("Ticker", "TextEventID", "1002", m_pszTextEvent);  // the event identifier of the text message event
			if (strcmp(setbuf, m_pszTextEvent))
			{
								EnterCriticalSection(&g_data.m_critEventsSettings);
				g_data.m_bTextEventChanged = true;  // the event identifier of the text message event has changed
								LeaveCriticalSection(&g_data.m_critEventsSettings);
			}

*/			

//			int GetIniInt(char* pszSection, char* pszEntry, int nDefaultValue);

/*
			m_bUseTimeCode = fu.GetIniInt("TimeCode", "UseTCRDR", 0)?true:false; // use Adrienne Time code card 
			m_nFrameRate = fu.GetIniInt("TimeCode", "FrameRate", 30);   // for time code addresses NTSC=30, PAL=25
			m_bDF = fu.GetIniInt("TimeCode", "DropFrame", 1)?true:false;   // Drop frame


			if(m_bUseTimeCode)
			{
				// connect to the time code card if nec
				if(!g_data.m_bTimeCodeThreadStarted)
				{
					g_data.m_bTimeCodeThreadKill = false;

					if(_beginthread(TimeCodeDataThread, 0, (void*)(NULL))==-1)
					{
					//error.
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Opportunitys:debug", "Error starting time code thread");//   Sleep(250);//(Dispatch message)
					//**MSG
					}
					else Sleep(250); // let it start...
				}
				
			}
			else
			{
				// disconnect from the time code card if nec
				if(g_data.m_bTimeCodeThreadStarted)
				{
					g_data.m_bTimeCodeThreadKill = true;
				}

			}
*/


		}
		else // write
		{
			if(m_pszAppName) fu.SetIniString("Global", "AppName", m_pszAppName);
			if(m_pszAppName) fu.SetIniString("Global", "InternalAppName", m_pszInternalAppName);
//			if(m_pszAboutText) fu.SetIniString("Global", "AboutText", m_pszAboutText);  // dont save
			if(m_pszSettingsText) fu.SetIniString("Global", "SettingsText", m_pszSettingsText);
//			int SetIniInt(char* pszSection, char* pszEntry, int nValue, char* pszComment=NULL);

			fu.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.


//				fu.SetIniInt("DefaultValues", "MessageDwellTime", m_nDefaultDwellTime);

			fu.SetIniString("Database", "DSN", m_pszDSN);
			fu.SetIniString("Database", "DBUser", m_pszUser);
			fu.SetIniString("Database", "DBPassword", m_pszPW);


			fu.SetIniString("Database", "AsRunTableName", m_pszAsRun);  // the As-run table name
			fu.SetIniString("Database", "SettingsTableName", m_pszSettings);  // the settings table name
			fu.SetIniString("Database", "MessagesTableName", m_pszMessages);   // the Messages table name
			fu.SetIniString("Database", "OpportunitysTableName", m_pszOpportunitys);   // the Opportunitys table name
//			fu.SetIniString("Database", "ButtonsTableName", m_pszButtonsTableName);  // for determining state



			fu.SetIniString("Output", "DrivenModule",  m_pszModule);  // the module name (Libretto)
			fu.SetIniString("Output", "DrivenQueue", m_pszQueue); // the module's queue table name (Queue of Libretto.dbo.Queue)
			fu.SetIniInt("Output", "TriggerBufferItems", m_nTriggerBuffer);


//			fu.SetIniString("Ticker", "InEventID", m_pszInEvent);  // the event identifier of the transition in (Server_Ticker_In) command
//			fu.SetIniString("Ticker", "OutEventID", m_pszOutEvent);  // the event identifier of the transition out (Server_Ticker_Out) command
//			fu.SetIniString("Ticker", "TextEventID", m_pszTextEvent);  // the event identifier of the text message event



			fu.SetIniString("Database", "AutomationModuleName", m_pszAutoModule);
			fu.SetIniString("Database", "AutomationChannelsTableName", m_pszAutoLists);
			fu.SetIniString("Database", "AutomationEventsTableName", m_pszAutoEvents);
//			fu.SetIniString("Database", "ReplacementRulesTableName", m_pszReplacementRules);
//			fu.SetIniString("Database", "DescriptionPatternTableName", m_pszDescriptionPattern);
//			fu.SetIniString("Database", "ClassificationRulesTableName", m_pszClassificationRules);
			fu.SetIniString("Database", "TabulatorModuleName", m_pszTabulatorModule);
//			fu.SetIniString("Database", "TimerTableName", m_pszTimerTable);

			fu.SetIniString("Database", "OpportuneEventsTableName", m_pszOpportuneEvents);
			fu.SetIniString("Database", "ExclusionRulesTableName", m_pszExclusionRules);


			fu.SetIniString("Tabulator", "HostAddress", m_pszTabulatorHost);  // the Tabulator host IP or name
			fu.SetIniInt("Tabulator", "CommandServerPort", m_nTabulatorPort);  // the Tabulator host port


//			fu.SetIniString("Output", "ReplaceAdEventID", m_pszReplaceAdEvent);  // the event identifier of the event
//			fu.SetIniString("Output", "ReplaceContentEventID", m_pszReplaceContentEvent);  // the event identifier of the event
//			fu.SetIniString("Output", "EventNotificationEventID", m_pszEventNotificationEvent);  // the event identifier of the event
//			fu.SetIniString("Output", "RejoinMainEventID", m_pszRejoinMainEvent);  // the event identifier of the event
//			fu.SetIniString("Output", "TimerEventID", m_pszTimerEvent);  // the event identifier of the event
			fu.SetIniString("Output", "DrivenHost", m_pszDrivenHost);  // IP address of the VANC inserter for libretto to route properly
			fu.SetIniString("Output", "CrawlEventID", m_pszCrawlEventID);  
			fu.SetIniInt("Output", "CrawlDurationMS", m_nCrawlDurationMS);  // IP address of the VANC inserter for libretto to route properly
			fu.SetIniInt("Output", "SecType", m_nSecType);  

			fu.SetIniInt("Output", "CrawlPaddingMS", m_nCrawlPaddingMS);  


//			fu.SetIniString("Output", "BlockedIDToken", m_pszBlockedIDToken);
//			fu.SetIniString("Output", "BlockedDescToken", m_pszBlockedDescToken);

//			fu.SetIniString("Output", "UnknownIDToken", m_pszUnknownIDToken);
//			fu.SetIniString("Output", "UnknownDescToken", m_pszUnknownDescToken);
//			fu.SetIniString("Output", "ColDelimToken", m_pszColDelimToken);
//			fu.SetIniString("Output", "ClassTestFormat", m_pszClassTestFormat, "Must match format specifiers in automation key assembly");
//			fu.SetIniString("Output", "TimerToken", m_pszTimerToken);
//			fu.SetIniString("Output", "TimerStartToken", m_pszTimerStartToken);


//			fu.SetIniInt("Output", "ProgramStreamOffset", m_nProgramStreamOffset);  
			fu.SetIniInt("Output", "ShowDiagnosticWindow", m_bShowDiagnosticWindow?1:0);

//			fu.SetIniInt("Output", "TimerExpiryManualDelayMS", m_nTimerExpiryManualDelayMS, "number of milliseconds to delay before evaluating timer expiry commands from manual processes");  
//			fu.SetIniInt("Output", "TimerExpiryDelayMS", m_nTimerExpiryAutoDelayMS, "number of milliseconds to delay before evaluating timer expiry commands from automated processes");  
//			fu.SetIniInt("Output", "TimerStartOffsetMS", m_nTimerStartOffsetMS, "number of milliseconds to offset the timer start");
//			fu.SetIniInt("Output", "TimerDurationAdjustMS", m_nTimerDurationAdjustMS, "number of milliseconds to adjust the timer duration");  


			fu.SetIniInt("Automation", "AutomationAnalysisLookahead", m_nAutomationAnalysisLookahead);  // number of events to parse.
			fu.SetIniString("Automation", "AutomationServer", m_pszAutoServer);// as in MAIN-DS
			fu.SetIniInt("Automation", "AutomationListNumber", m_nAutoServerListNumber);  // list number
			fu.SetIniInt("Automation", "AutomationLookahead", m_bAutomationLookahead?1:0);




			fu.SetIniInt("TimeCode", "UseTCRDR", m_bUseTimeCode?1:0, "use Adrienne Time code card"); // use Adrienne Time code card 
			fu.SetIniInt("TimeCode", "FrameRate", m_nFrameRate, "for time code addresses NTSC=30, PAL=25, used only if no TC card available" );   // for time code addresses NTSC=30, PAL=25
			fu.SetIniInt("TimeCode", "DropFrame", m_bDF?1:0, "drop frame mode, used only if no TC card available");    // Drop frame

			fu.SetIniInt("Messager", "MillisecondMessaging", m_bMillisecondMessaging?1:0);		// use millisecond resolution for messages and asrun
			fu.SetIniInt("Messager", "ReadableEncodedAsRun", m_bReadableEncodedAsRun?1:0);			// hex encode for readability, otherwise base 64 encoded


			if(!(fu.SetSettings(theOppApp.m_szSettingsFilename, false)&FILEUTIL_MALLOC_OK)) return CLIENT_ERROR;
		}

		return CLIENT_SUCCESS;
	}
//	else AfxMessageBox("failed!");
	return CLIENT_ERROR;
}




int COpportunitySettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	if((Settings(true)==TABULATOR_SUCCESS)&&(m_pszSettings)&&(strlen(m_pszSettings)>0))
	{

		CDBUtil  db;
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];
		strcpy(errorstring, "");

		CDBconn* pdbConn = db.CreateNewConnection(m_pszDSN, m_pszUser, m_pszPW);
		if(pdbConn)
		{
			if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Opportunity_Settings:database_connect", errorstring);  //(Dispatch message)
				if(pszInfo)
				{
					_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s", errorstring);
				}
			}
			else
			{
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
				CRecordset* prs = db.Retrieve(pdbConn, szSQL, pszInfo);
				if(prs)
				{
					int nReturn = TABULATOR_ERROR;
					int nIndex = 0;
					while ((!prs->IsEOF()))
					{
						CString szCategory="";
						CString szParameter="";
						CString szValue="";
						CString szTemp="";
						int min, max;
						bool bmin = false, bmax = false;
						try
						{
							prs->GetFieldValue("category", szCategory);  //HARDCODE
							prs->GetFieldValue("parameter", szParameter);  //HARDCODE
							prs->GetFieldValue("value", szValue);  //HARDCODE
							prs->GetFieldValue("min_value", szTemp);  //HARDCODE
							if(szTemp.GetLength())
							{
								min = atoi(szTemp);
								bmin = true;
							}
							prs->GetFieldValue("max_value", szTemp);  //HARDCODE
							if(szTemp.GetLength())
							{
								max = atoi(szTemp);
								bmax = true;
							}
						}
						catch( ... )
						{
						}

						int nLength = szValue.GetLength();
/*
						if(szCategory.CompareNoCase("Main")==0)
						{
							if(szParameter.CompareNoCase("Name")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszName) free(m_pszName);
										m_pszName = pch;
									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("License")==0)
						{
							if(szParameter.CompareNoCase("Key")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszLicense) free(m_pszLicense);
										m_pszLicense = pch;

										// recompile license key params
										if(g_ptabulator->m_data.m_key.m_pszLicenseString) free(g_ptabulator->m_data.m_key.m_pszLicenseString);
										g_ptabulator->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
										if(g_ptabulator->m_data.m_key.m_pszLicenseString)
										sprintf(g_ptabulator->m_data.m_key.m_pszLicenseString, "%s", szValue);

										g_ptabulator->m_data.m_key.InterpretKey();

										char errorstring[MAX_MESSAGE_LENGTH];
										if(g_ptabulator->m_data.m_key.m_bValid)
										{
											unsigned long i=0;
											while(i<g_ptabulator->m_data.m_key.m_ulNumParams)
											{
												if((g_ptabulator->m_data.m_key.m_ppszParams)
													&&(g_ptabulator->m_data.m_key.m_ppszValues)
													&&(g_ptabulator->m_data.m_key.m_ppszParams[i])
													&&(g_ptabulator->m_data.m_key.m_ppszValues[i]))
												{
													if(stricmp(g_ptabulator->m_data.m_key.m_ppszParams[i], "max")==0)
													{
		//												g_ptabulator->m_data.m_nMaxLicensedDevices = atoi(g_ptabulator->m_data.m_key.m_ppszValues[i]);
													}
												}
												i++;
											}
										
											if(
													(
														(!g_ptabulator->m_data.m_key.m_bExpires)
													||((g_ptabulator->m_data.m_key.m_bExpires)&&(!g_ptabulator->m_data.m_key.m_bExpired))
													||((g_ptabulator->m_data.m_key.m_bExpires)&&(g_ptabulator->m_data.m_key.m_bExpireForgiveness)&&(g_ptabulator->m_data.m_key.m_ulExpiryDate+g_ptabulator->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
													)
												&&(
														(!g_ptabulator->m_data.m_key.m_bMachineSpecific)
													||((g_ptabulator->m_data.m_key.m_bMachineSpecific)&&(g_ptabulator->m_data.m_key.m_bValidMAC))
													)
												)
											{
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
												g_ptabulator->m_data.SetStatusText(errorstring, TABULATOR_STATUS_OK);
											}
											else
											{
												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
												g_ptabulator->m_data.SetStatusText(errorstring, TABULATOR_STATUS_ERROR);
											}
										}
										else
										{
											_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
											g_ptabulator->m_data.SetStatusText(errorstring, TABULATOR_STATUS_ERROR);
										}

									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("CommandServer")==0)
						{
							if(szParameter.CompareNoCase("ListenPort")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
									{
										m_usCommandPort = nLength;
									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("StatusServer")==0)
						{
							if(szParameter.CompareNoCase("ListenPort")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
									{
										m_usStatusPort = nLength;
									}
								}
							}
						}
						else
						if(szCategory.CompareNoCase("Messager")==0)
						{
							if(szParameter.CompareNoCase("UseEmail")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bUseEmail = true;
									else m_bUseEmail = false;
								}
							}
							else
							if(szParameter.CompareNoCase("UseNet")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bUseNetwork = true;
									else m_bUseNetwork = false;
								}
							}
							else
							if(szParameter.CompareNoCase("UseLog")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bUseLog = true;
									else m_bUseLog = false;
								}
							}
							else
							if(szParameter.CompareNoCase("ReportSuccessfulOperation")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bReportSuccessfulOperation = true;
									else m_bReportSuccessfulOperation = false;
								}
							}
						}
						else
		/*
						if(szCategory.CompareNoCase("FileHandling")==0)
						{
							// we are not going to allow DSN params to change via the DB.
							if(szParameter.CompareNoCase("DeleteSourceFileOnTransfer")==0)
							{
								if(nLength>0)
								{
									nLength = atoi(szValue);
									if(nLength) m_bDeleteSourceFileOnTransfer = true;
									else m_bDeleteSourceFileOnTransfer = false;
								}
							}
						}
						else
		*/

						if(szCategory.CompareNoCase("Output")==0)
						{

/*
							if(szParameter.CompareNoCase("ProgramStreamOffset")==0)
							{
								if(nLength>0)
								{
									m_nProgramStreamOffset = atoi(szValue);
									if(m_nProgramStreamOffset<0) m_nProgramStreamOffset=0;
									if(m_nProgramStreamOffset>30) m_nProgramStreamOffset=30;
								}
							}
							else
*/
							if(szParameter.CompareNoCase("DrivenHost")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszDrivenHost)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszDrivenHost) free(m_pszDrivenHost);
										m_pszDrivenHost = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("DrivenModule")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszModule)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszModule) free(m_pszModule);
										m_pszModule = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("DrivenQueue")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszQueue)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszQueue) free(m_pszQueue);
										m_pszQueue = pch;
									}
								}
							}
/*
							else

							if(szParameter.CompareNoCase("ReplaceAdEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszReplaceAdEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bReplaceAdEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszReplaceAdEvent) free(m_pszReplaceAdEvent);
										m_pszReplaceAdEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("ReplaceContentEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszReplaceContentEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bReplaceContentEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszReplaceContentEvent) free(m_pszReplaceContentEvent);
										m_pszReplaceContentEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("EventNotificationEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszEventNotificationEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bEventNotificationEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszEventNotificationEvent) free(m_pszEventNotificationEvent);
										m_pszEventNotificationEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("RejoinMainEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszRejoinMainEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bRejoinMainEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszRejoinMainEvent) free(m_pszRejoinMainEvent);
										m_pszRejoinMainEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("TimerEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszTimerEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bTimerEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszTimerEvent) free(m_pszTimerEvent);
										m_pszTimerEvent = pch;
									}
								}
							}

*/
						}

/*						if(szCategory.CompareNoCase("Ticker")==0)
						{
							if(szParameter.CompareNoCase("GraphicsHost")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszDestinationHost)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszDestinationHost) free(m_pszDestinationHost);
										m_pszDestinationHost = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("GraphicsModule")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszModule)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszModule) free(m_pszModule);
										m_pszModule = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("GraphicsQueue")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszQueue)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszQueue) free(m_pszQueue);
										m_pszQueue = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("InEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszInEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bInEventChanged = true;  // the event identifier of the transition in (Server_Ticker_In) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszInEvent) free(m_pszInEvent);
										m_pszInEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("OutEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszOutEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bOutEventChanged = true;  // the event identifier of the transition out (Server_Ticker_Out) command has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszOutEvent) free(m_pszOutEvent);
										m_pszOutEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("TextEventID")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszTextEvent)))
								{
									EnterCriticalSection(&g_data.m_critEventsSettings);
									g_data.m_bTextEventChanged = true;  // the event identifier of the text message event has changed
									LeaveCriticalSection(&g_data.m_critEventsSettings);

									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszTextEvent) free(m_pszTextEvent);
										m_pszTextEvent = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("PlayItemsOnce")==0)
							{
								if(nLength>0)
								{
									m_bTickerPlayOnce = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("DoneCount")==0)
							{
								if(nLength>0)
								{
									m_nDoneCount = atoi(szValue);
									if(m_nDoneCount<0) m_nDoneCount=0;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExpired")==0)
							{
								if(nLength>0)
								{
									m_bAutoPurgeExpired = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExceeded")==0)
							{
								if(nLength>0)
								{
									m_bAutoPurgeExceeded = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExpiredAfterMins")==0)
							{
								if(nLength>0)
								{
									m_nAutoPurgeExpiredAfterMins = atoi(szValue);
									if(m_nAutoPurgeExpiredAfterMins<0) m_nAutoPurgeExpiredAfterMins=0;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoPurgeExceededAfterMins")==0)
							{
								if(nLength>0)
								{
									m_nAutoPurgeExceededAfterMins = atoi(szValue);
									if(m_nAutoPurgeExceededAfterMins<0) m_nAutoPurgeExceededAfterMins=0;
								}
							}
							else
							if(szParameter.CompareNoCase("AutoStart")==0)
							{
								if(nLength>0)
								{
									m_bAutostartTicker = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("UseLocalTime")==0)
							{
								if(nLength>0)
								{
									m_bUseLocalTime = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
							else
							if(szParameter.CompareNoCase("dwell_time")==0)
							{
								if(nLength>0)
								{
									m_nDefaultDwellTime = atoi(szValue);
									if(m_nDefaultDwellTime<0) m_nDefaultDwellTime=10000; //default
								}
							}
						}
						else
						if(szCategory.CompareNoCase("DataDownload")==0)
						{
							if(szParameter.CompareNoCase("AutoStart")==0)
							{
								if(nLength>0)
								{
									m_bAutostartData = ((szValue.CompareNoCase("true")==0)||(atoi(szValue)!=0))?true:false;
								}
							}
						}
						else
						if(szCategory.CompareNoCase("default_value")==0)
						{
							if(szParameter.CompareNoCase("dwell_time")==0)
							{
								if(nLength>0)
								{
									m_nDefaultDwellTime = atoi(szValue);
									if(m_nDefaultDwellTime<0) m_nDefaultDwellTime=10000; //default
								}
							}
						}
						else

*/

						if(szCategory.CompareNoCase("Automation")==0)
						{
							// we are not going to allow DSN params to change via the DB.
							if(szParameter.CompareNoCase("AutomationServer")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszAutoServer)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszAutoServer) free(m_pszAutoServer);
										m_pszAutoServer = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("AutomationAnalysisLookahead")==0)
							{
								if(nLength>0)
								{
									m_nAutomationAnalysisLookahead = atoi(szValue);// no validation, -1 turns it off.
								}
							}
							else
							if(szParameter.CompareNoCase("AutomationListNumber")==0)
							{
								if(nLength>0)
								{
									m_nAutoServerListNumber = atoi(szValue); // no validation
								}
							}

						}
						else

						if(szCategory.CompareNoCase("Database")==0)
						{
							// we are not going to allow DSN params to change via the DB.
							if(szParameter.CompareNoCase("SettingsTableName")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszSettings)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszSettings) free(m_pszSettings);
										m_pszSettings = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("AsRunTableName")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszAsRun)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszAsRun) free(m_pszAsRun);
										m_pszAsRun = pch;
									}
								}
							}
		
/*
							else
							if(szParameter.CompareNoCase("MessagesTableName")==0)
							{
								if((nLength>0)&&(szValue.Compare(m_pszMessages)))
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszMessages) free(m_pszMessages);
										m_pszMessages = pch;
									}
								}
							}
/*
							else
							if(szParameter.CompareNoCase("QueueTableName")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszQueue) free(m_pszQueue);
										m_pszQueue = pch;
									}
								}
							}
		/*
							else
							if(szParameter.CompareNoCase("ConnectionsTableName")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszConnections) free(m_pszConnections);
										m_pszConnections = pch;
									}
								}
							}
							else
							if(szParameter.CompareNoCase("LiveEventsTableName")==0)
							{
								if(nLength>0)
								{
									char* pch = (char*)malloc(nLength+1);
									if(pch)
									{
										sprintf(pch, "%s", szValue);
										if(m_pszLiveEvents) free(m_pszLiveEvents);
										m_pszLiveEvents = pch;
									}
								}
							}
		*/
						}


						nIndex++;
						prs->MoveNext();
					}
					prs->Close();

					if(pszInfo)
					{
						_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
					}
					delete prs;
					prs = NULL;

					Settings(false); //write
					return TABULATOR_SUCCESS;
				}
			}
		}
		else
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: Connection pointer was NULL.");
		}

	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. "

				);
		}
	}
	return TABULATOR_ERROR;
}


