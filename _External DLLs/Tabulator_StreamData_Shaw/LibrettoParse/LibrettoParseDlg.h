// LibrettoParseDlg.h : header file
//

#if !defined(AFX_LIBRETTOPARSEDLG_H__9DE1D940_4A00_4643_9A8E_E80F1D957FCB__INCLUDED_)
#define AFX_LIBRETTOPARSEDLG_H__9DE1D940_4A00_4643_9A8E_E80F1D957FCB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CLibrettoParseDlg dialog

class CLibrettoParseDlg : public CDialog
{
// Construction
public:
	CLibrettoParseDlg(CWnd* pParent = NULL);	// standard constructor

	char* m_pchBuffer;
	CString m_szFilename;
	unsigned long m_ulLen;

// Dialog Data
	//{{AFX_DATA(CLibrettoParseDlg)
	enum { IDD = IDD_LIBRETTOPARSE_DIALOG };
	CListCtrl	m_lcLog;
	CString	m_szItem;
	BOOL	m_bFilterHex;
	CString	m_szSubItems;
	CString	m_szHexData;
	int		m_nOutputType;
	BOOL	m_bSuppressRepeats;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLibrettoParseDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CLibrettoParseDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	afx_msg void OnButtonSelect();
	afx_msg void OnItemchangedList1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonOutfile();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LIBRETTOPARSEDLG_H__9DE1D940_4A00_4643_9A8E_E80F1D957FCB__INCLUDED_)
