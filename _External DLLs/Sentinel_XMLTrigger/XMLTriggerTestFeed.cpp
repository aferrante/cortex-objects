// XMLTriggerTestFeed.cpp : implementation file
//

#include "stdafx.h"
#include "XMLTrigger.h"
#include "XMLTriggerTestFeed.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CXMLTriggerTestFeed dialog


CXMLTriggerTestFeed::CXMLTriggerTestFeed(CWnd* pParent /*=NULL*/)
	: CDialog(CXMLTriggerTestFeed::IDD, pParent)
{
	//{{AFX_DATA_INIT(CXMLTriggerTestFeed)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_bFile = FALSE;
	m_szURL = _T("");
	m_bTimes = FALSE;
	//}}AFX_DATA_INIT
	m_bDownloading = false;
}


void CXMLTriggerTestFeed::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CXMLTriggerTestFeed)
	DDX_Check(pDX, IDC_CHECK_FILE, m_bFile);
	DDX_Text(pDX, IDC_EDIT_FEEDURL, m_szURL);
	DDX_Check(pDX, IDC_CHECK_TIME, m_bTimes);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CXMLTriggerTestFeed, CDialog)
	//{{AFX_MSG_MAP(CXMLTriggerTestFeed)
	ON_BN_CLICKED(IDC_BUTTON_START, OnButtonStart)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXMLTriggerTestFeed message handlers
CString CXMLTriggerTestFeed::SimpleXMLIndent(CString szXML)
{
	CString szReturn="";
//	CString szIndent=" ";

	int n=0; 
//	int nIndent=0; 
	int nLen = szXML.GetLength();
	bool bTag = false;
	bool bEndTag = false;
	bool bFirstTag = false;
	bool bFirstNotTag = false;

	while(n<nLen)
	{
		char ch = szXML.GetAt(n);

		if(bFirstNotTag)
		{
			if(ch == '<')
			{
				szReturn+= (char)13;
				szReturn+= (char)10;

			}
		}
		bFirstNotTag = false;
		szReturn+=ch;

		if(bTag)
		{
			if(ch == '?')
			{
				bTag = false;
			}
			else
			if((ch == '/')&&(bFirstTag))
			{
				bEndTag = true;
			}
			else
			if(ch == '>')
			{
				bTag = false;
				if(bEndTag)
				{
					szReturn+= (char)13;
					szReturn+= (char)10;
				}
				else
				{
					bFirstNotTag = true;
				}
				bEndTag = false;
				
			}
			bFirstTag = false;
		}
		else
		{
			if(ch == '<')
			{

				bTag = true;
				bFirstTag = true;
			}
		}

		n++;
	}
	return szReturn;
}

void CXMLTriggerTestFeed::OnButtonStart() 
{
	if (	m_bDownloading ) return;
	if(m_szURL.GetLength()<=0) return;
	m_bDownloading = true;
	UpdateData(TRUE);	
	CString szTextData;
	CString szInfo;
	CString szTime;
	GetDlgItem(IDC_BUTTON_START)->EnableWindow(FALSE);

	_timeb now;

	unsigned long ulTotalTime;
	unsigned long ulTime = clock();
	_ftime(&now);
	if(m_bFile)
	{
		if(m_inet.RetrieveURLToFile(m_szURL, "xmltriggerdownload.txt", FALSE, FALSE, "", &szInfo)>=INET_SUCCESS)
		{
			ulTotalTime = clock() - ulTime;
			szTime.Format("time to download: %d ms", ulTotalTime);
			GetDlgItem(IDC_STATIC_TIME)->SetWindowText(szTime);

			FILE* fp; 
			if(m_bTimes)
			{
				fp = fopen("xmltriggerdownloadtimes.txt", "ab");
				if(fp)
				{
					fprintf(fp, "%d,\"%s\",1,%d\r\n", now.time, m_szURL, ulTotalTime);
					fclose(fp);
				}
			}

			fp = fopen("xmltriggerdownload.txt", "rt");
			if(fp)
			{

				fseek(fp, 0, SEEK_END);
				unsigned long ulFileLen = ftell(fp);

				char* pch = (char*) malloc(ulFileLen+1); // term zero
				if(pch)
				{
					fseek(fp, 0, SEEK_SET);
					fread(pch, sizeof(char), ulFileLen, fp);
					*(pch+ulFileLen) = 0; // term zero


					CString szText = SimpleXMLIndent(pch);

//					AfxMessageBox(szText);
					if(szText.GetLength())
						GetDlgItem(IDC_RICHEDIT_STATUS)->SetWindowText(szText);
					else
						GetDlgItem(IDC_RICHEDIT_STATUS)->SetWindowText(pch);

					free(pch);
				}
				fclose(fp);
			}
			else
			{
				GetDlgItem(IDC_RICHEDIT_STATUS)->SetWindowText("could not open file");
			}

		}
		else
		{
			ulTotalTime = clock() - ulTime;
			szTime.Format("failed to download after: %d ms", ulTotalTime);
			GetDlgItem(IDC_STATIC_TIME)->SetWindowText(szTime);
			if(m_bTimes)
			{
				FILE* fp; 
				fp = fopen("xmltriggerdownloadtimes.txt", "ab");
				if(fp)
				{
					fprintf(fp, "%d,\"%s\",0,%d\r\n", now.time, m_szURL, ulTotalTime);
					fclose(fp);
				}
			}

			GetDlgItem(IDC_RICHEDIT_STATUS)->SetWindowText(szInfo);
		}
	}
	else
	{
//					AfxMessageBox(m_szURL);
		try
		{
			if(m_inet.RetrieveURLToText(m_szURL, &szTextData, FALSE, FALSE, "", &szInfo)>=INET_SUCCESS)
			{
//						AfxMessageBox("success");
				ulTotalTime = clock() - ulTime;
				szTime.Format("time to download: %d ms", ulTotalTime);
				if(m_bTimes)
				{
					FILE* fp; 
					fp = fopen("xmltriggerdownloadtimes.txt", "ab");
					if(fp)
					{
						fprintf(fp, "%d,\"%s\",1,%d\r\n", now.time, m_szURL, ulTotalTime);
						fclose(fp);
					}
				}
				GetDlgItem(IDC_STATIC_TIME)->SetWindowText(szTime);
				CString szText = SimpleXMLIndent(szTextData);
	//					AfxMessageBox(szText);

				if(szText.GetLength())
					GetDlgItem(IDC_RICHEDIT_STATUS)->SetWindowText(szText);
				else
					GetDlgItem(IDC_RICHEDIT_STATUS)->SetWindowText(szTextData);

			}
			else
			{
//						AfxMessageBox("fail");
				ulTotalTime = clock() - ulTime;
				szTime.Format("failed to download after: %d ms", ulTotalTime);
				if(m_bTimes)
				{
					FILE* fp; 
					fp = fopen("xmltriggerdownloadtimes.txt", "ab");
					if(fp)
					{
						fprintf(fp, "%d,\"%s\",0,%d\r\n", now.time, m_szURL, ulTotalTime);
						fclose(fp);
					}
				}
				GetDlgItem(IDC_STATIC_TIME)->SetWindowText(szTime);
				GetDlgItem(IDC_RICHEDIT_STATUS)->SetWindowText(szInfo);
			}
		}
		catch (...)
		{
//			AfxMessageBox("exception");
		}
	}
	GetDlgItem(IDC_BUTTON_START)->EnableWindow(TRUE);
	m_bDownloading = false;

}

void CXMLTriggerTestFeed::OnOK() 
{
	// TODO: Add extra validation here
	
//	CDialog::OnOK();
}
