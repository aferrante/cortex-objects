// XMLTrigger.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "XMLTrigger.h"
//#include "..\..\..\Cortex Objects\Tabulator\TabulatorDefines.h"
//#include "..\..\..\Cortex Objects\Tabulator\TabulatorData.h"
#include <process.h>
#include "..\..\..\Common\MFC\ODBC\DBUtil.h"
#include "..\..\..\Common\TXT\BufferUtil.h"
#include <objsafe.h>
#include <atlbase.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

#define XMLTRIGGERDLG_MINSIZEX 200
#define XMLTRIGGERDLG_MINSIZEY 150

/*
#ifndef REMOVE_CLIENTSERVER
void DataDownloadThread(void* pvArgs);
#endif //#ifndef REMOVE_CLIENTSERVER
void TickerEngineThread(void* pvArgs);
void SynchronizationThread(void* pvArgs);
*/

/////////////////////////////////////////////////////////////////////////////
// CXMLTriggerApp

BEGIN_MESSAGE_MAP(CXMLTriggerApp, CWinApp)
	//{{AFX_MSG_MAP(CXMLTriggerApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXMLTriggerApp construction

CXMLTriggerApp::CXMLTriggerApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	AfxInitRichEdit();
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CXMLTriggerApp object

CXMLTriggerApp				theApp;
CXMLTriggerCore			g_core;
CXMLTriggerData			g_data;
CXMLTriggerDlg*			g_pdlg = NULL;
CXMLTriggerSettings	g_settings;
CMessager*			g_ptabmsgr = NULL;


int  CXMLTriggerApp::DLLCtrl(void** ppvoid, UINT nType)
{
//AfxMessageBox("manage state");
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
//	char szSQL[DB_SQLSTRING_MAXLEN];
//	char errorstring[MAX_MESSAGE_LENGTH];

	char message[MAX_MESSAGE_LENGTH];

	int nReturn = CX_SUCCESS;
	switch(nType)
	{

// This case is from the tabulator plug-in setup, whereby the dispatcher messager object was passed, and any calls would be made.
// Sentinel is not calling this case so g_ptabmsgr will remain NULL, and g_ptabmsgr->DM() will not be called.
// However, DLLCMD_SET_DISPATCH sets g_data.m_lpfnDispatchMsg and so g_data.m_lpfnDispatchMsg() imay bes called, and from that 
// point forward dispatch messager messages may be issued.  g_data.m_lpfnDispatchMsg() is the same as calling g_ptabmsgr->DM().
	case /*DLLCMD_SETDISPATCHER*/	0x06:
		{
			if (ppvoid)
			{

				g_ptabmsgr = (CMessager*)*ppvoid;
//				CString szMsg; szMsg.Format("%08x dispatcher",g_pmd);
//				AfxMessageBox(szMsg);
//				AfxMessageBox(AfxGetApp()->m_pszAppName);
//				AfxMessageBox(AfxGetApp()->m_pszExeName);
					if(g_ptabmsgr) g_ptabmsgr->AddDestination(
						MSG_DESTTYPE_LOG, "xmltrigger", 
						((g_settings.m_pszFileSpec!=NULL)?g_settings.m_pszFileSpec:"xmltrigger|YD||1|")
						);
					
				Sleep(150);
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, "xmltrigger", "DLLCtrl", "DLLCMD_SET_DISPATCH received with 0x%08x", *ppvoid);
			}
			else
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "DLLCMD_SET_DISPATCH received with NULL");
				nReturn = CX_ERROR;
			}  

		} break;
	case DLLCMD_INIT://						0x0010 // A signal to the DLL that it should run any internal initialization routines. The DLL may want to wait for settings to be set before proceeding to context-sensitive routines.
		{
			if (ppvoid)
			{
				if(g_data.m_pszInstanceID) { try{ free(g_data.m_pszInstanceID);} catch(...){}}
				g_data.m_pszInstanceID = NULL;

				char* pch = (char*)(*ppvoid);

				if(pch)
				{
					g_data.m_pszInstanceID = (char*)malloc(strlen(pch)+1);
					if(g_data.m_pszInstanceID) strcpy(g_data.m_pszInstanceID, pch);
				}

				pch = (char*)malloc(strlen(VERSION_STRING)+1);
				if(pch) strcpy(pch, VERSION_STRING);
				*ppvoid = (void*)pch;
					
			}
			else
			{
				nReturn = CX_ERROR;
			}  

		} break;
	case DLLCMD_SET_DISPATCH:// 0x0011 // Sets a callback function address so that the DLL may send messages to the calling application's message dispatch procedure, which writes log lines, sends emails out, etc depending on severity level and other criteria.
		{
			if (ppvoid)
			{
				g_data.m_lpfnDispatchMsg = (LPFNDM)ppvoid;
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, "xmltrigger", "DLLCtrl", "DLLCMD_SET_DISPATCH received with 0x%08x", ppvoid);
				if(g_data.m_lpfnDispatchMsg)
				{
					_snprintf(message, MAX_MESSAGE_LENGTH, "DLLCMD_SET_DISPATCH received with 0x%08x", ppvoid);
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
				}
			}
			else
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "DLLCMD_SET_DISPATCH received with NULL");
				if(g_data.m_lpfnDispatchMsg)g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", "DLLCMD_SET_DISPATCH received with NULL");
				nReturn = CX_ERROR;
			}  

		} break;
	case DLLCMD_SET_MSG://				0x0012 // Sets a callback function address so that the DLL may send messages to the calling application's web user interface for messages.
		{
			if (ppvoid)
			{
				g_data.m_lpfnMsg = (LPFNMSG)ppvoid;
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, "xmltrigger", "DLLCtrl", "DLLCMD_SET_MSG received with 0x%08x", ppvoid);
				if(g_data.m_lpfnDispatchMsg)
				{
					_snprintf(message, MAX_MESSAGE_LENGTH, "DLLCMD_SET_MSG received with 0x%08x", ppvoid);
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
				}
			}
			else
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "DLLCMD_SET_MSG received with NULL");
				if(g_data.m_lpfnDispatchMsg)g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", "DLLCMD_SET_MSG received with NULL");
				nReturn = CX_ERROR;
			}  

		} break;
	case DLLCMD_SET_ASRUN://			0x0013 // Sets a callback function address so that the DLL may send messages to the calling application's web user interface for as-run items.
		{
			if (ppvoid)
			{
				g_data.m_lpfnAsRunMsg = (LPFNAR)ppvoid;
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, "xmltrigger", "DLLCtrl", "DLLCMD_SET_ASRUN received with 0x%08x", ppvoid);
				if(g_data.m_lpfnDispatchMsg)
				{
					_snprintf(message, MAX_MESSAGE_LENGTH, "DLLCMD_SET_ASRUN received with 0x%08x", ppvoid);
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
				}
			}
			else
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "DLLCMD_SET_ASRUN received with NULL");
				if(g_data.m_lpfnDispatchMsg)g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", "DLLCMD_SET_ASRUN received with NULL");
				nReturn = CX_ERROR;
			}  

		} break;
	case DLLCMD_GET_SETTINGS://		0x0014 // Obtain a list of settings that the DLL would like to make sure exists in the user interface, for use inside the DLL.
		{
			if (ppvoid)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, "xmltrigger", "DLLCtrl", "DLLCMD_GET_SETTINGS received with 0x%08x", ppvoid);
				if(g_data.m_lpfnDispatchMsg)
				{
					_snprintf(message, MAX_MESSAGE_LENGTH, "DLLCMD_GET_SETTINGS received with 0x%08x", ppvoid);
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
				}

				_snprintf(message, MAX_MESSAGE_LENGTH, "<setting>\
<category>network</category>\
<category_display>Network Settings</category_display>\
<parameter>port</parameter>\
<parameter_display>TCPIP port</parameter_display>\
<value>12345</value>\
<desc>The TCPIP port for the main communication	listener</desc>\
<pattern>Integer</pattern>\
<min_len>null</min_len>\
<max_len>null</max_len>\
<min_value>1024</min_value>\
<max_value>65535</max_value>\
<sysonly>true</sysonly>\
</setting>\
<setting>\
<category>sample</category>\
<category_display>Sample Settings</category_display>\
<parameter>parameter</parameter>\
<parameter_display>Parameter</parameter_display>\
<value>value</value>\
<desc>A sample setting</desc>\
<pattern>text</pattern>\
<min_len>0</min_len>\
<max_len>500</max_len>\
<min_value></min_value>\
<max_value></max_value>\
<sysonly>false</sysonly>\
</setting>");

				char* pch = (char*)malloc(strlen(message)+1);
				if(pch)
				{
					strcpy(pch, message);
					_snprintf(message, MAX_MESSAGE_LENGTH, "DLLCMD_GET_SETTINGS returning %s", pch);
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
					*ppvoid = (void*)pch;

				}
				else
				{
					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "DLLCMD_GET_SETTINGS could not allocate buffer");
					if(g_data.m_lpfnDispatchMsg)g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", "DLLCMD_GET_SETTINGS could not allocate buffer");
					nReturn = CX_ERROR;
				}

			}
			else
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "DLLCMD_GET_SETTINGS received with NULL");
				if(g_data.m_lpfnDispatchMsg)g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", "DLLCMD_GET_SETTINGS received with NULL");
				nReturn = CX_ERROR;
			}  



		} break;

	case DLLCMD_SET_SETTINGS://		0x0015 // Set a list of settings with values from the web interface (database).
		{
			// just log it
			if (ppvoid)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, "xmltrigger", "DLLCtrl", "DLLCMD_SET_SETTINGS received with 0x%08x: %s", ppvoid, (*ppvoid)?(*ppvoid):"NULL");
				if(g_data.m_lpfnDispatchMsg)
				{
					_snprintf(message, MAX_MESSAGE_LENGTH, "DLLCMD_SET_SETTINGS received with 0x%08x: %s", ppvoid, (*ppvoid)?(*ppvoid):"NULL");
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
				}
			}
			else
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "DLLCMD_SET_SETTINGS received with NULL");
				if(g_data.m_lpfnDispatchMsg)g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", "DLLCMD_SET_SETTINGS received with NULL");
				nReturn = CX_ERROR;
			}  

		} break;
	case DLLCMD_SET_STATUS://			0x0016 // Sets a callback function address so that the DLL may send status updates to the calling application's web user interface for module status.
		{
			if (ppvoid)
			{
				g_data.m_lpfnStatusMsg = (LPFNSTATUS)ppvoid;
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, "xmltrigger", "DLLCtrl", "DLLCMD_SET_STATUS received with 0x%08x", ppvoid);
				if(g_data.m_lpfnDispatchMsg)
				{
					_snprintf(message, MAX_MESSAGE_LENGTH, "DLLCMD_SET_STATUS received with 0x%08x", ppvoid);
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
				}
			}
			else
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "DLLCMD_SET_STATUS received with NULL");
				if(g_data.m_lpfnDispatchMsg)g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", "DLLCMD_SET_STATUS received with NULL");
				nReturn = CX_ERROR;
			}  

		} break;

	case DLLCMD_DEINIT://					0x0020 // A signal to the DLL that it should run any internal de-initialization routines. After the DEINIT command is received the DLL should prepare to finalize any messaging back to the calling application.
		{
			if (ppvoid)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, "xmltrigger", "DLLCtrl", "DLLCMD_DEINIT received with 0x%08x: %s", ppvoid, (*ppvoid)?(*ppvoid):"NULL");
				if(g_data.m_lpfnDispatchMsg)
				{
					_snprintf(message, MAX_MESSAGE_LENGTH, "DLLCMD_DEINIT received with 0x%08x: %s", ppvoid, (*ppvoid)?(*ppvoid):"NULL");
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
				}
			}
			else
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "DLLCMD_DEINIT received with NULL");
				if(g_data.m_lpfnDispatchMsg)g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", "DLLCMD_DEINIT received with NULL");
				nReturn = CX_ERROR;
			}  
		} break;
	case DLLCMD_MSGEND://					0x0021 // A signal to the DLL that it should not send any further messages through any messaging callbacks, including as-run.
		{
			if (ppvoid)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, "xmltrigger", "DLLCtrl", "DLLCMD_MSGEND received with 0x%08x: %s", ppvoid, (*ppvoid)?(*ppvoid):"NULL");
				if(g_data.m_lpfnDispatchMsg)
				{
					_snprintf(message, MAX_MESSAGE_LENGTH, "DLLCMD_MSGEND received with 0x%08x: %s", ppvoid, (*ppvoid)?(*ppvoid):"NULL");
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
				}
			}
			else
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "DLLCMD_MSGEND received with NULL");
				if(g_data.m_lpfnDispatchMsg)g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", "DLLCMD_MSGEND received with NULL");
				nReturn = CX_ERROR;
			}  
		} break;
	case DLLCMD_QUERY://					0x0022 // A query to the DLL to ask it if it has finished deinitialization.  This command will repeat until a succesful return, or a de-init timeout has elapsed.
		{
			if (ppvoid)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, "xmltrigger", "DLLCtrl", "DLLCMD_QUERY received with 0x%08x: %s", ppvoid, (*ppvoid)?(*ppvoid):"NULL");
				if(g_data.m_lpfnDispatchMsg)
				{
					_snprintf(message, MAX_MESSAGE_LENGTH, "DLLCMD_QUERY received with 0x%08x: %s", ppvoid, (*ppvoid)?(*ppvoid):"NULL");
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
				}
			}
			else
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "DLLCMD_QUERY received with NULL");
				if(g_data.m_lpfnDispatchMsg)g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", "DLLCMD_QUERY received with NULL");
//				nReturn = CX_ERROR;  // let it go
			}
			
			// just a test, let in not be ready for 5 goes.
			if (g_data.m_nQuery>4)
			{
				nReturn = CX_SUCCESS;
				g_data.m_nQuery = 0;
			}
			else
			{
				g_data.m_nQuery++;
				nReturn = CX_ERROR;
			}
		} break;
	case DLLCMD_RELEASE://				0x002e // A signal to the DLL that the initialization routine is complete.
		{
			if (ppvoid)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, "xmltrigger", "DLLCtrl", "DLLCMD_RELEASE received with 0x%08x: %s", ppvoid, (*ppvoid)?(*ppvoid):"NULL");
				if(g_data.m_lpfnDispatchMsg)
				{
					_snprintf(message, MAX_MESSAGE_LENGTH, "DLLCMD_RELEASE received with 0x%08x: %s", ppvoid, (*ppvoid)?(*ppvoid):"NULL");
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
				}
			}
			else
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "DLLCMD_RELEASE received with NULL");
				if(g_data.m_lpfnDispatchMsg)g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", "DLLCMD_RELEASE received with NULL");
		//		nReturn = CX_ERROR;  // this is correct for release
			}  
		} break;
	case DLLCMD_DLLEND://					0x002f // A signal to the DLL that it is about to be unloaded.
		{
			if (ppvoid)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, "xmltrigger", "DLLCtrl", "DLLCMD_DLLEND received with 0x%08x: %s", ppvoid, (*ppvoid)?(*ppvoid):"NULL");
				if(g_data.m_lpfnDispatchMsg)
				{
					_snprintf(message, MAX_MESSAGE_LENGTH, "DLLCMD_DLLEND received with 0x%08x: %s", ppvoid, (*ppvoid)?(*ppvoid):"NULL");
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
				}
			}
			else
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "DLLCMD_DLLEND received with NULL");
				if(g_data.m_lpfnDispatchMsg)g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", "DLLCMD_DLLEND received with NULL");
				nReturn = CX_ERROR;
			}  
		} break;

	case DLLCMD_FREE_BUFFER://		0x00ff // A call to free a buffer.  This will be called to free buffers that have been allocated by the calls specified above. Note that this is a different call from the call to free an object, regardless of whether the DLL uses the same allocation/deallocation functions in the call.  That command is given here for completeness, but is not used in initialization
		{
			if (ppvoid)
			{
//AfxMessageBox((char*)*ppvoid);
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, "xmltrigger", "DLLCtrl", "DLLCMD_FREE_BUFFER received with 0x%08x->0x%08x %s", ppvoid, *ppvoid);
				if(g_data.m_lpfnDispatchMsg)
				{
					_snprintf(message, MAX_MESSAGE_LENGTH, "DLLCMD_FREE_BUFFER received with 0x%08x->0x%08x", ppvoid, *ppvoid);
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
				}
				if(*ppvoid)
				{
					try{ free(*ppvoid); } catch(...){}
				}
//AfxMessageBox("freed");
			}
			else
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "DLLCMD_FREE_BUFFER received with NULL");
				if(g_data.m_lpfnDispatchMsg)g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", "DLLCMD_FREE_BUFFER received with NULL");
				nReturn = CX_ERROR;
			}  

		} break;
	case DLLCMD_FREE_OBJECT://		0x00fe // A call to free an object.
		{
			if (ppvoid)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, "xmltrigger", "DLLCtrl", "DLLCMD_FREE_OBJECT received with 0x%08x->0x%08x", ppvoid, *ppvoid);
				if(g_data.m_lpfnDispatchMsg)
				{
					_snprintf(message, MAX_MESSAGE_LENGTH, "DLLCMD_FREE_OBJECT received with 0x%08x->0x%08x", ppvoid, *ppvoid);
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
				}
				if(*ppvoid)
				{
					try{ delete *ppvoid; } catch(...){}
				}
//AfxMessageBox("deleted");
			}
			else
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "DLLCMD_FREE_OBJECT received with NULL");
				if(g_data.m_lpfnDispatchMsg)g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", "DLLCMD_FREE_OBJECT received with NULL");
				nReturn = CX_ERROR;
			}  

		} break;

	default:
			if (ppvoid)
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, "xmltrigger", "DLLCtrl", "COMMAND 0x%08x received with 0x%08x: %s", nType, ppvoid, (*ppvoid)?(*ppvoid):"NULL");
				if(g_data.m_lpfnDispatchMsg)
				{
					_snprintf(message, MAX_MESSAGE_LENGTH, "COMMAND 0x%08x received with 0x%08x: %s", nType, ppvoid, (*ppvoid)?(*ppvoid):"NULL");
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
				}

				if((g_settings.m_bOutputToFile)&&(g_settings.m_pszOutputFileSpec)&&(*ppvoid))
				{
					FILE* fp = fopen(g_settings.m_pszOutputFileSpec, "wb");
					if(fp)
					{
						try
						{
							fwrite((*ppvoid), 1, strlen((char*)(*ppvoid)), fp);
						}
						catch(...)
						{
						}
						fclose(fp);
					}

				}

			}
			else
			{
				if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, "xmltrigger", "DLLCtrl", "COMMAND 0x%08x received with NULL", nType);
				if(g_data.m_lpfnDispatchMsg)
				{
					_snprintf(message, MAX_MESSAGE_LENGTH, "COMMAND 0x%08x received with NULL", nType);
					g_data.m_lpfnDispatchMsg(MSG_ICONINFO, NULL, "DLLCtrl", message);
				nReturn = CX_ERROR;
			}  
		} break;
		
	}

//AfxMessageBox("here");

	return nReturn;
}

BOOL CXMLTriggerApp::InitInstance() 
{
#if _WIN32_WINNT >= 0x0400
		HRESULT hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		HRESULT hRes = CoInitialize(NULL);
#endif
//	AfxMessageBox(m_pszAppName);
//	AfxMessageBox(m_pszExeName);
//	AfxMessageBox("Foo");

	strcpy(m_szSettingsFilename, SETTINGS_FILENAME);

	if((m_pszAppName)&&(strlen(m_pszAppName)>0)) sprintf(m_szSettingsFilename, "%s.ini", m_pszAppName);

	g_settings.Settings(true); // need to get the thread settings...

//	AfxMessageBox("Foo2");
	return CWinApp::InitInstance();
}

int CXMLTriggerApp::ExitInstance() 
{
			EnterCriticalSection(&g_settings.m_crit);
	g_settings.Settings(false);
			LeaveCriticalSection(&g_settings.m_crit);

  CoUninitialize(); //XML
	
	return CWinApp::ExitInstance();
}



