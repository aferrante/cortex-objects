// XMLTriggerCore.h: interface for the CXMLTriggerCore class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_XMLTRIGGERCORE_H__D426F8FF_BC44_4257_869A_044999574215__INCLUDED_)
#define AFX_XMLTRIGGERCORE_H__D426F8FF_BC44_4257_869A_044999574215__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "..\..\..\Common\TXT\BufferUtil.h"

class CXMLTriggerCore  
{
public:
	CXMLTriggerCore();
	virtual ~CXMLTriggerCore();

	CBufferUtil m_bu;

	BOOL IsPointInWnd(CPoint point, CWnd* pWnd);
	BOOL IsPointInRect(CPoint point, CRect rcClientRect);
  CString CALEncodeBrackets(CString szString);

};

#endif // !defined(AFX_XMLTRIGGERCORE_H__D426F8FF_BC44_4257_869A_044999574215__INCLUDED_)
