// XMLTriggerData.cpp: implementation of the CXMLTriggerData and CXMLTriggerMessage classes.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "XMLTrigger.h"
#include "XMLTriggerData.h"
#include "XMLTriggerSettings.h"
#include <atlbase.h>
#include <process.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CXMLTriggerData	g_data;
extern CXMLTriggerSettings	g_settings;
extern CMessager*			g_ptabmsgr;


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CXMLTriggerData::CXMLTriggerData()
{
//	m_bTransact = false;
	m_bDialogStarted = false;
	m_socket=NULL;

	m_pdb=NULL;
	m_pdbConn=NULL;
	m_pszInstanceID = NULL;
	InitializeCriticalSection(&m_critTransact);

	m_lpfnDispatchMsg = NULL;
	m_lpfnMsg = NULL;
	m_lpfnAsRunMsg = NULL;
	m_lpfnStatusMsg = NULL;

	m_nQuery = 0;

}

CXMLTriggerData::~CXMLTriggerData()
{
//	DeleteCriticalSection(&m_critClientSocket);
	try{ if(m_pszInstanceID) free(m_pszInstanceID);} catch(...){}
	DeleteCriticalSection(&m_critTransact);
}

// remove pipes
CString CXMLTriggerData::Encode(CString szText)
{
  CString szOut = "";
  int i;
  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '|')
      szOut += "%:";
    else if (szText[i] == '%')
      szOut += "%%";
    else if ((szText[i] != '\n')&&(szText[i] != '\r'))  //strip returns while we are at it.
       szOut += (char) szText[i];
  }
  return szOut;
}

// put back pipes
CString CXMLTriggerData::Decode(CString szText)
{
  CString szOut = "";
  int i;

  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '%')
    {
      i++; if (i>=szText.GetLength()) break;
      if (szText[i] == '%')
      {
        szOut += "%";
      }
      else if (szText[i] == ':')
      {
        szOut += "|";
      }
    }
    else szOut += (char) szText[i];
  }
  return szOut;
}



CString CXMLTriggerData::XMLTextNodeValue(IXMLDOMNodePtr pNode)
{
	CString szText = _T("");
	if(pNode)
	{
		IXMLDOMNodeListPtr pValues = pNode->GetchildNodes();
		if (pValues != NULL)
		{
			IXMLDOMNodePtr pValue;
			while(pValue = pValues->nextNode())
			{
				DOMNodeType valnodeType;
				pValue->get_nodeType(&valnodeType);

				if(valnodeType == NODE_TEXT)
				{
USES_CONVERSION;
					szText = W2T(_bstr_t(pValue->GetnodeValue()));
					szText.TrimLeft();
					szText.TrimRight();
					return szText;
				}
			}
		}
	}
	return szText;
}

/*

int CXMLTriggerData::SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...)
{
 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszAsRun)&&(strlen(g_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		if((pszEventID)&&(strlen(pszEventID)>SENDMSG_EVENT_MAXLEN)) pszEventID[SENDMSG_EVENT_MAXLEN-1]=0;
		
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);
		char* pchEvent = m_pdb->EncodeQuotes(pszEventID);
		if(g_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, channelid, eventid) \
VALUES ('%s', '%s', %d, %d.%03d, %d, '%s')", // HARDCODE
				g_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm,
				nChannelID,
				pchEvent?pchEvent:""
				);
		}
		else
		{

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log")
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
		if(pchEvent) free(pchEvent);

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critTransact);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critTransact);
			IncrementDatabaseMods(g_settings.m_pszAsRun, dberrorstring);
			return CX_SUCCESS;
		}
		LeaveCriticalSection(&m_critTransact);

	}
	return CX_ERROR;
}




int CXMLTriggerData::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszMessages)&&(strlen(g_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		if(g_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime) \
VALUES ('%s', '%s', %d, %d.%03d)", // HARDCODE
				g_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				g_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				g_settings.m_pszMessages
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		EnterCriticalSection(&m_critTransact);

		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critTransact);
			IncrementDatabaseMods(g_settings.m_pszMessages, dberrorstring);
			return CX_SUCCESS;
		}

		LeaveCriticalSection(&m_critTransact);

	}
	return CX_ERROR;
}

*/



