// XMLTrigger.h : main header file for the XMLTRIGGER DLL
//

#if !defined(AFX_XMLTRIGGER_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
#define AFX_XMLTRIGGER_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#import "msxml3.dll" named_guids 
using namespace MSXML2;

//#include "../../../Client/ClientDefines.h"
#include "../../../Common/MSG/Messager.h"
#include "../../../Common/TXT/FileUtil.h"
#include "..\..\..\Cortex\3.0.4.4\CortexDefines.h"
#include "..\..\..\Cortex\3.0.4.4\CortexShared.h"

#include "..\..\..\Common\MFC\Inet\Inet.h"

#define REMOVE_CLIENTSERVER  // for web-based UI only


#include "XMLTriggerCore.h"
#include "XMLTriggerData.h"
#include "XMLTriggerDlg.h"
#include "XMLTriggerSettings.h"

#define SETTINGS_FILENAME "xmltrigger.ini"

#define VERSION_STRING "1.0.0.1"
#define XMLTRIGGER_DATA 0
#define XMLTRIGGER_TICKER 1
#define XMLTRIGGER_UI 2
#define XMLTRIGGER_THREADS 3

/////////////////////////////////////////////////////////////////////////////
// CXMLTriggerApp
// See XMLTrigger.cpp for the implementation of this class
//

class CXMLTriggerApp : public CWinApp
{
public:
	CXMLTriggerApp();
	int  DLLCtrl(void** ppvoid, UINT nType);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXMLTriggerApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	char m_szSettingsFilename[MAX_PATH];

	//{{AFX_MSG(CXMLTriggerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XMLTRIGGER_H__AC233D91_25D3_4FFE_B543_06BB9352A456__INCLUDED_)
