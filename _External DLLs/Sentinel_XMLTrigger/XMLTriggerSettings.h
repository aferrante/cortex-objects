#if !defined(AFX_XMLTRIGGERSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_)
#define AFX_XMLTRIGGERSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// XMLTriggerSettings.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CXMLTriggerSettings dialog

class CXMLTriggerSettings : public CDialog
{
// Construction
public:
	CXMLTriggerSettings(CWnd* pParent = NULL);   // standard constructor
	virtual ~CXMLTriggerSettings();   // standard destructor

// Dialog Data
	//{{AFX_DATA(CXMLTriggerSettings)
	enum { IDD = IDD_DIALOG_SETTINGS };
	//}}AFX_DATA

	CRITICAL_SECTION m_crit;

		// database stuff
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;

	//exposed
	char* m_pszAppName;
	char* m_pszInternalAppName;
	char* m_pszAboutText;
	char* m_pszSettingsText;

	char* m_pszFileSpec;

	int Settings(bool bRead);

	unsigned long m_ulDebug;  // prints out debug statements that & with this.

	bool m_bOutputToFile;
	char* m_pszOutputFileSpec;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXMLTriggerSettings)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CXMLTriggerSettings)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XMLTRIGGERSETTINGS_H__D72D854D_1FA6_4C38_87AA_040BE53117F2__INCLUDED_)
