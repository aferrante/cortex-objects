// XMLTriggerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "XMLTrigger.h"
#include "XMLTriggerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CXMLTriggerApp			theApp;
extern CXMLTriggerCore			g_core;
extern CXMLTriggerData			g_data;
extern CXMLTriggerDlg*			g_pdlg;
extern CXMLTriggerSettings	g_settings;
extern CMessager*					g_ptabmsgr;



/////////////////////////////////////////////////////////////////////////////
// CXMLTriggerDlg dialog


CXMLTriggerDlg::CXMLTriggerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CXMLTriggerDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CXMLTriggerDlg)
	//}}AFX_DATA_INIT

}


void CXMLTriggerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CXMLTriggerDlg)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CXMLTriggerDlg, CDialog)
	//{{AFX_MSG_MAP(CXMLTriggerDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CXMLTriggerDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(IDD, pParentWnd);
}

