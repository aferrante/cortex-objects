// GTP32Data.cpp: implementation of the CGTP32Data and CGTP32Message classes.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GTP32.h"
#include "GTP32Data.h"
#include "GTP32Settings.h"
#include <atlbase.h>
#include <process.h>
#include "../../../Common/SNMP/SNMPUtil.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


#define SENDMSG_EVENT_MAXLEN 64
#define SENDMSG_SENDER_MAXLEN 64
#define SENDMSG_MESSAGE_MAXLEN 512


void USP32ServerHandlerThread(void* pvArgs);


extern CGTP32Data	g_data;
extern CGTP32Settings	g_settings;
extern CMessager*			g_ptabmsgr;
extern DLLdata_t       g_dlldata;

CSNMPUtil u;  //global just so the message number is global

CGTP32Button::CGTP32Button()
{
	m_nItemID = -1; // i.e. unassigned.
	strcpy(m_pchButtonOnLabel,"");
	strcpy(m_pchButtonOffLabel,"");
	strcpy(m_pchTallyEventLabel,"");
	strcpy(m_pchTallyEventValue,"1");
	strcpy(m_pchClickEventLabel,"");

	m_nLastClickValue = -1; // boolean (toggle)

	m_ulStatus = GTP32_BUTTON_STATUS_INDETERMINATE;  // to start off with
	m_ulFlags = GTP32_BUTTON_DISABLED;

	// manual events association
	strcpy(m_pchManualEventName,"");
	strcpy(m_pchManualEventProgID,"");
	m_nManualEventType = GTP32_MANUAL_EVENT_TYPE_NONE;
	m_nManualEventDuration = -1; // invalid value...
}

CGTP32Button::~CGTP32Button()
{
}

int CGTP32Button::UpdateStatus()
{
	if(( g_data.m_pdb )&&(	g_data.m_pdbConn) && (m_ulStatus != GTP32_BUTTON_STATUS_INDETERMINATE)) // do not update with unknown values
	{

		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[MAX_MESSAGE_LENGTH];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET status = %d WHERE buttonID = %d",  //HARDCODE
				((g_settings.m_pszButtons)&&(strlen(g_settings.m_pszButtons)))?g_settings.m_pszButtons:"Buttons",
				m_ulStatus,
				m_nItemID
				);

//			Sleep(1); // dont peg processor
if(g_settings.m_ulDebug&(GTP32_DEBUG_SQL)) 
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:button_update", "SQL: %s", szSQL); // Sleep(250); //(Dispatch message)

EnterCriticalSection(&g_data.m_critSQL);
		int nReturn = g_data.m_pdb->ExecuteSQL(g_data.m_pdbConn, szSQL, errorstring);
		if(nReturn<DB_SUCCESS)
		{
			//**MSG
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "GTP32:button_update", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

		}
LeaveCriticalSection(&g_data.m_critSQL);

		return nReturn;
	}
	return GTP32_ERROR;
}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGTP32Data::CGTP32Data()
{

//	m_pEvents = NULL;
//	m_pInEvents = new CTabulatorEventArray;
//	m_pOutEvents = new CTabulatorEventArray;
//	m_pTextEvents = new CTabulatorEventArray;

//	m_socket=NULL;

	m_bButtonsInitialized = FALSE;
	m_bInitAuto = FALSE;		// initialization condition
	m_bInitPass = FALSE;		// initialization condition
	m_bInitBlock = FALSE;	// initialization condition
	m_bInitAutoClick = FALSE;		// initialization condition
	m_bInitPassClick = FALSE;		// initialization condition
	m_bInitBlockClick = FALSE;	// initialization condition

	m_bInGTP32Command = FALSE;
	m_s=NULL;

	m_bInCommand = FALSE;
	m_socketTabulator = NULL;

	m_pdb=NULL;
	m_pdbConn=NULL;
	InitializeCriticalSection(&m_critSQL);

//	m_ppButtons=NULL;
//	m_nNumButtons=0;
	InitializeCriticalSection(&m_critButtons);
	memset(((char *)&m_saiGTPhost), 0, (sizeof (m_saiGTPhost)));

}

CGTP32Data::~CGTP32Data()
{
//	DeleteCriticalSection(&m_critClientSocket);
	DeleteCriticalSection(&m_critSQL);
//	if(	m_pInEvents) delete  m_pInEvents;
//	if(	m_pOutEvents) delete  m_pOutEvents;
//	if(	m_pTextEvents) delete  m_pTextEvents;

	// should delete the buttons
	DeleteCriticalSection(&m_critButtons);
}

// remove pipes
CString CGTP32Data::Encode(CString szText)
{
  CString szOut = "";
  int i;
  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '|')
      szOut += "%:";
    else if (szText[i] == '%')
      szOut += "%%";
    else if ((szText[i] != '\n')&&(szText[i] != '\r'))  //strip returns while we are at it.
       szOut += (char) szText[i];
  }
  return szOut;
}

// put back pipes
CString CGTP32Data::Decode(CString szText)
{
  CString szOut = "";
  int i;

  for (i=0; i<szText.GetLength(); i++)
  {
    if (szText[i] == '%')
    {
      i++; if (i>=szText.GetLength()) break;
      if (szText[i] == '%')
      {
        szOut += "%";
      }
      else if (szText[i] == ':')
      {
        szOut += "|";
      }
    }
    else szOut += (char) szText[i];
  }
  return szOut;
}


CString CGTP32Data::XMLTextNodeValue(IXMLDOMNodePtr pNode)
{
	CString szText = _T("");
	if(pNode)
	{
		IXMLDOMNodeListPtr pValues = pNode->GetchildNodes();
		if (pValues != NULL)
		{
			IXMLDOMNodePtr pValue;
			while(pValue = pValues->nextNode())
			{
				DOMNodeType valnodeType;
				pValue->get_nodeType(&valnodeType);

				if(valnodeType == NODE_TEXT)
				{
USES_CONVERSION;
					szText = W2T(_bstr_t(pValue->GetnodeValue()));
					szText.TrimLeft();
					szText.TrimRight();
					return szText;
				}
			}
		}
	}
	return szText;
}

int CGTP32Data::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszMessages)&&(strlen(g_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		if(g_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime) \
VALUES ('%s', '%s', %d, %d.%03d)", // HARDCODE
				g_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				g_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				g_settings.m_pszMessages
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
//			IncrementDatabaseMods(g_settings.m_pszMessages, dberrorstring);
		LeaveCriticalSection(&m_critSQL);
			return TABULATOR_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return TABULATOR_ERROR;
}

/*

int CGTP32Data::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszMessages)&&(strlen(g_settings.m_pszMessages)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			g_settings.m_pszMessages,
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			g_settings.m_pszMessages
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
//			IncrementDatabaseMods(g_settings.m_pszMessages, dberrorstring);
			return TABULATOR_SUCCESS;
		}
	}
	return TABULATOR_ERROR;
}
*/


int CGTP32Data::SendAsRunMsg(int nType, int nChannelID, char* pszEventID, char* pszSender, char* pszMessage, ...)
{
 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszAsRun)&&(strlen(g_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		if((pszEventID)&&(strlen(pszEventID)>SENDMSG_EVENT_MAXLEN)) pszEventID[SENDMSG_EVENT_MAXLEN-1]=0;
		
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);
		char* pchEvent = m_pdb->EncodeQuotes(pszEventID);
		if(g_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, channelid, eventid) \
VALUES ('%s', '%s', %d, %d.%03d, %d, '%s')", // HARDCODE
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm,
				nChannelID,
				pchEvent?pchEvent:""
				);
		}
		else
		{

			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
				(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log")
				);
		}

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
		if(pchEvent) free(pchEvent);

//		ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
//			IncrementDatabaseMods(g_settings.m_pszAsRun, dberrorstring);
			return TABULATOR_SUCCESS;
		}
		else
		{
if(g_settings.m_ulDebug&(GTP32_DEBUG_SQL))
{ 
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "GTP32", "AsRun message error %s", dberrorstring);// Sleep(500); //(Dispatch message)
}
		}
		LeaveCriticalSection(&m_critSQL);

	}
	return TABULATOR_ERROR;
}



/*
was this, but need to use new table def
int CGTP32Data::SendAsRunMsg(int nType, char* pszSender, char* pszMessage, ...)
{

 	if((m_pdb)&&(m_pdbConn)&&(pszMessage)&&(strlen(pszMessage))
		&&(g_settings.m_pszAsRun)&&(strlen(g_settings.m_pszAsRun)))
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = m_pdb->EncodeQuotes(szMessage);
		char* pchSender = m_pdb->EncodeQuotes(pszSender);

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
			(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log"),
			pchMessage?pchMessage:szMessage,
			pchSender?pchSender:"unspecified",
			nType, // 1 is error, 0 is message
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0)), // local time....
			(g_settings.m_pszAsRun?g_settings.m_pszAsRun:"AsRun_Log")
			);

		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "GTP32_Asrun", "SQL %s", szSQL) ;// Sleep(100); //(Dispatch message)

//		m_data.ReleaseRecordSet();
		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
//			IncrementDatabaseMods(g_settings.m_pszAsRun, dberrorstring);
			return TABULATOR_SUCCESS;
		}
		else
		{
if(g_ptabmsgr)
{
	char szSource[MAX_PATH]; sprintf(szSource, "%s_Asrun", g_settings.m_pszInternalAppName);
	g_ptabmsgr->DM(MSG_ICONNONE, NULL, szSource, "SQL error %s", dberrorstring) ;// Sleep(100); //(Dispatch message)
}
		}
		LeaveCriticalSection(&m_critSQL);

	}
	else
		{
//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "GTP32_Asrun", "null conditions");// Sleep(100); //(Dispatch message)
		}
	return TABULATOR_ERROR;
}
*/



int CGTP32Data::InitDLLData(DLLthread_t* pData)
{
	if(pData)
	{
		InitializeCriticalSection(&pData->m_crit);
		EnterCriticalSection(&pData->m_crit);
		pData->bKillThread=true;
		pData->bThreadStarted=false;
		pData->nThreadState=0;  //generic
		pData->nThreadControl=0;  //generic
		pData->m_dblLastUpdate=-1.0;
		pData->pszThreadName=NULL;
		strcpy(pData->pszThreadStateText, "");//256 chars max

	// URLs may use format specifiers
		pData->pszBaseURL=NULL;
		pData->pszInfoURL=NULL;
		pData->pszConfirmURL=NULL;

		pData->pszSeriesParamName=NULL;    //  %S
		pData->pszProgramParamName=NULL;   //  %P
		pData->pszEpisodeParamName=NULL;   //  %E

		pData->pszSeriesID=NULL;    //  %s
		pData->pszProgramID=NULL;   //  %p
		pData->pszEpisodeID=NULL;   //  %e

		// some generic parameters
		pData->pszToDateParamName=NULL;				//  %t
		pData->pszFromDateParamName=NULL;			//  %f
		pData->pszNowParamName=NULL;					//  %n
		pData->pszBatchParamName=NULL;				//  %b
		pData->pszFromIDParamName=NULL;				//  %m  // start id
		pData->pszGetIDParamName=NULL;				//  %i

		pData->pszLastID=NULL;  // last ID gotten   %l

		pData->nBatchParam=250;
		pData->nShortIntervalMS=0;
		pData->nLongIntervalMS=5000;
		pData->bDirParamStyle=true;  // true is baseurl/paramname/paramvalue/  false is baseurl?paramname=paramvalue

		_ftime(&pData->timebTick); // the time check inside the thread
		_ftime(&pData->timebLastPullTick); // the last time data pulled inside the thread
		pData->nIDrateAvg=-1;
		pData->nIDrateMax=-1;
		pData->nIDrateMin=-1;
		pData->nNumFailures=0;
		pData->nNumSuccess=0;

		pData->pszHost=NULL;
		pData->nPort=80;
		pData->nAuxValue=-1;

		LeaveCriticalSection(&pData->m_crit);
		return CLIENT_SUCCESS;
	}

	return CLIENT_ERROR;
}


int CGTP32Data::SendGTP32Command(int nType, int nIndex, int nValue) // type like button press or GPO, nValue will be 0 or 1, basically on or off.
{
	char szError[8096];
	if((nIndex>0)&&(nIndex<87)) // hardcoded , 86 is the max
	{
//u.CreateSNMPMessage(SNMP_SETREQUESTPDU, m_szCommunity.GetBuffer(0), "1.3.6.1.4.1.21541.6.1.0", pVal, 1); // this is a stand in value

		CSNMPMessage* pMessage = new CSNMPMessage;
		if(pMessage)
		{
			if(m_s == NULL) Connect();

			int nReturn = GTP32_ERROR;
			if(m_s == NULL)
			{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "GTP32", "SendCommand - connection was NULL!"); Sleep(500); //(Dispatch message)
			}
			else
			{
				unsigned char* buffer = NULL;
				unsigned long buflen=0;
				unsigned char ucMsg = u.IncrementMessageNumber();
				pMessage->m_ulRequest = (unsigned long)ucMsg;

				pMessage->m_ucPDUType = SNMP_SETREQUESTPDU;

				// fill out all the other stuff
				if(pMessage->m_pchOID)
				{
					free(pMessage->m_pchOID);
				}
				char oidbuffer[256];
				sprintf(oidbuffer, "1.3.6.1.4.1.21541.%d.%d.0", nType, nIndex);

				pMessage->m_pchOID = (char*)malloc(strlen(oidbuffer)+1);
				if(pMessage->m_pchOID)
				{
					strcpy(pMessage->m_pchOID, oidbuffer);
				}

				pMessage->m_pchCommunity = (char*)malloc(strlen("public")+1); //hardcoded
				if(pMessage->m_pchCommunity)
				{
					strcpy(pMessage->m_pchCommunity, "public");
				}

				// change the value.
				if(pMessage->m_pucValue)
				{
					free(pMessage->m_pucValue);
				}
				unsigned char* pVal = (unsigned char*)malloc(2);
				if(pVal)
				{
					pVal[0] = nValue;
					pMessage->m_ulValueLength=1;
				}
				else pMessage->m_ulValueLength=0;
				pMessage->m_pucValue = pVal; // even if null

				buffer = u.ReturnMessageBuffer(pMessage);
				if(buffer)
				{	
					buflen = u.ReturnLength(&buffer[1]);
					buflen += u.ReturnLength(buflen)+1;
				}
				
	//			CBufferUtil bu;
	//unsigned long ulSend = buflen;
	//char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_DELIM32BIT);
	//AfxMessageBox(pchSnd);

	unsigned long ulSend = buflen;
	char* pchSnd = m_bu.ReadableHex((char*)buffer, &ulSend, MODE_FULLHEX);

				while(m_bInGTP32Command) Sleep(1); // wait until not busy
				m_bInGTP32Command = TRUE;

				nReturn =	m_net.SendLine(buffer, buflen, m_s, EOLN_NONE, false, 5000, szError);
				if(nReturn<NET_SUCCESS)
				{
					if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "GTP32:SEND", "Error sending command, return code %d %s",nReturn,szError); // Sleep(250); //(Dispatch message)
					Disconnect();
					Connect();
				}
				else
				{

//	if(g_settings.m_bLogTransactions)
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32:SEND", "Sent %d bytes: %s", buflen,
			pchSnd?pchSnd:(char*)buffer); // Sleep(250); //(Dispatch message)
	}

					unsigned char* puc = NULL;
					unsigned long ulLen=0;

					nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);
					if(nReturn<NET_SUCCESS)
					{
						if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "GTP32:RECV", "Error sending command, return code %d %s",nReturn,szError); // Sleep(250); //(Dispatch message)
						Disconnect();
						Connect();
					}
					else
					{
						if(ulLen==0) //disconnected
						{
							Disconnect();
							Connect();
						}
						else
						{

		unsigned long ulRecv = ulLen;
		char* pchRecv = m_bu.ReadableHex((char*)puc, &ulRecv, MODE_FULLHEX);

//		if(g_settings.m_bLogTransactions)
		{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32:SEND", "Recd %d bytes: %s", ulLen,
				pchRecv?pchRecv:(char*)puc); // Sleep(250); //(Dispatch message)
		}
						//		free(puc); // dont free it!
								if(pchRecv) free(pchRecv);
						}
					}

					if(puc) free(puc);
				}
			
				m_bInGTP32Command = FALSE;
				if(pchSnd) free(pchSnd);

				if(buffer) free(buffer);
			}


			delete pMessage;

			return nReturn;

		}
		else
		{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "GTP32:ERROR", "No message object");
		}

	}

	return GTP32_ERROR;

}

int CGTP32Data::SendTabulatorCommand(unsigned char** ppucDataBuffer, char* pszAsRunEventID, bool bSuppressAsRun)
{
	if((m_socketTabulator)&&(ppucDataBuffer))
	{
		CNetData data;
		while(m_bInCommand) Sleep(1);
		m_bInCommand =TRUE;

		unsigned char* pucOrig = *ppucDataBuffer;
		unsigned char* puc = NULL;
		int nLen = strlen((char*)(*ppucDataBuffer));
		if((*ppucDataBuffer)&&(nLen)) puc = (unsigned char*)malloc(nLen+1);
		if(puc) memcpy(puc, *ppucDataBuffer, nLen+1); // includes term 0;
		data.m_ucCmd = TABULATOR_CMD_PLUGINCALL;
		data.m_pucData=puc;
		data.m_ulDataLen = (puc?nLen:0);
		data.m_ucType = ((NET_TYPE_PROTOCOL1|NET_TYPE_KEEPOPEN|NET_SND_KEEPOPENLCL)|(*ppucDataBuffer?NET_TYPE_HASDATA:0)); // has data but no subcommand.
		int n = m_net.SendData(&data, m_socketTabulator, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT);//|NET_SND_NO_RX);
		if(data.m_pucData!=NULL)
		{
			*ppucDataBuffer = data.m_pucData;  //return the buffer;
			data.m_pucData=NULL;  // null it so that it does not get freed on destructor
		}
		else
		{
			*ppucDataBuffer = NULL;
		}
		if(n < NET_SUCCESS)
		{
if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "GTP32_Tabulator_command", "Net error %d sending cmd 0x%02x buffer: %s", n, TABULATOR_CMD_PLUGINCALL, (*ppucDataBuffer)?((char*)(*ppucDataBuffer)):"");   //(Dispatch message)
			m_net.CloseConnection(m_socketTabulator);
			m_socketTabulator = NULL;
			m_bInCommand =FALSE;

			//ASRUN
if(!bSuppressAsRun) g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, "Error %d sending to %s at %s:%d with: %s", 
										n,
										g_settings.m_pszTabulatorModule?g_settings.m_pszTabulatorModule:"Tabulator",
										g_settings.m_pszTabulatorHost?g_settings.m_pszTabulatorHost:"null",
										g_settings.m_nTabulatorPort, 
										(char*)(*ppucDataBuffer));

		}
		else
		{
			m_net.SendData(NULL, m_socketTabulator, 5000, 0, NET_SND_CLNTACK, NULL);
			m_bInCommand =FALSE;

if(g_settings.m_ulDebug&(GTP32_DEBUG_CALLFUNC))
{ 
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONNONE, NULL, "GTP32", "Successfully executed %s with return data: %02x,%02x %s (%d bytes data) - returning %d", 
									(pucOrig?((char*)pucOrig):"null"),
									data.m_ucCmd,    // the command byte
									data.m_ucSubCmd,   // the subcommand byte
									(((*ppucDataBuffer)&&(data.m_ulDataLen))?(char*)*ppucDataBuffer:""),
									data.m_ulDataLen,
									(data.m_ucSubCmd&0x80)?((int)(data.m_ucSubCmd - 256)):((int)data.m_ucSubCmd)
									);
}

			//ASRUN
if(!bSuppressAsRun) g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, "Successfully executed %s with return data: %02x,%02x %s (%d bytes data)", 
									(pucOrig?((char*)pucOrig):"null"),
									data.m_ucCmd,    // the command byte
									data.m_ucSubCmd,   // the subcommand byte
									(((*ppucDataBuffer)&&(data.m_ulDataLen))?(char*)*ppucDataBuffer:""),
									data.m_ulDataLen
									);
//			return GTP32_SUCCESS;

// since it is an unsigned char we are getting back, we have a choice to make.
// either negative values were converted, and we have -1 == 0xff... or something.  But I think this will be the most common so let's use
// two's complement to return an int value between -128 and 127.

			if(data.m_ucSubCmd&0x80)
			{
				// negative value!
				return (int)(data.m_ucSubCmd - 256);
			}
			else
			{
				// positive value or zero.
				return (int)data.m_ucSubCmd;
			}
		}
	}
	return GTP32_ERROR;
}





int CGTP32Data::GetManualEvents(char* pszInfo)
{
	return -69;
}


int CGTP32Data::GetButtons(char* pszInfo)
{

	if((m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT buttonID, enabled, button_name_on, button_name_off, \
button_type, tally_label, tally_value, click_label, \
me_name, me_event_type, me_programID, me_duration FROM %s ORDER BY buttonID",  //HARDCODE
			((g_settings.m_pszButtons)&&(strlen(g_settings.m_pszButtons)))?g_settings.m_pszButtons:"ButtonInfo"
			); 
		
/*		


ManualEvents.name as me_name, ManualEvents.event_type as me_event_type, 
ManualEvents.programID as me_programID, ManualEvents.duration as me_duration


create table Buttons (buttonID int, enabled int, status int, button_name_on varchar(256), 
button_name_off varchar(256), button_type varchar(16), tally_label varchar(256), tally_value varchar(64) )  

create view ButtonInfo as select Buttons.buttonID, Buttons.enabled, Buttons.status, Buttons.button_name_on, Buttons.button_name_off, Buttons.button_type, 
Buttons.tally_label, Buttons.tally_value, Buttons.click_label, ManualEvents.name as me_name, ManualEvents.event_type as me_event_type, 
ManualEvents.programID as me_programID, ManualEvents.duration as me_duration from Buttons left join ManualEvents on Buttons.buttonID = ManualEvents.buttonID    


	int m_nItemID;

	char m_pchButtonOnLabel[256];
	char m_pchButtonOffLabel[256];

	char m_pchTallyEventLabel[256];
	char m_pchTallyEventValue[64];

	unsigned long m_ulStatus; // current tally.
	unsigned long m_ulFlags;

	// manual events association
	strcpy(m_pchManualEventName,"");
	strcpy(m_pchManualEventProgID,"");
	m_nManualEventType = GTP32_MANUAL_EVENT_TYPE_NONE;
	m_nManualEventDuration = -1; // invalid value...

*/

if(g_settings.m_ulDebug&(GTP32_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:GetButtons", "%s", szSQL); // Sleep(250); //(Dispatch message)
}

//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "GetButtons");  Sleep(250); //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "Got Destinations");  Sleep(250); //(Dispatch message)

			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = CLIENT_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
if(g_settings.m_ulDebug&(GTP32_DEBUG_SQL)) 
{	
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:GetButtons", "index %d", nIndex); // Sleep(250); //(Dispatch message) 
}

				try
				{
					CString szTemp;
					prs->GetFieldValue("buttonID", szTemp);//HARDCODE
					szTemp.TrimLeft(); szTemp.TrimRight();
					if(szTemp.GetLength())
					{
						int nItemID = atoi(szTemp);

						// here, find the right button.
						if((nItemID>0)&&(nItemID<33)) // hardcoded 1 thru 32.
						{
							g_data.m_Buttons[nItemID-1].m_nItemID = nItemID;
							nItemID--;

							prs->GetFieldValue("enabled", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							
							g_data.m_Buttons[nItemID].m_ulFlags = (atoi(szTemp)>0)?GTP32_BUTTON_ENABLED:GTP32_BUTTON_DISABLED;

							prs->GetFieldValue("button_name_on", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							strcpy(g_data.m_Buttons[nItemID].m_pchButtonOnLabel, szTemp);

							prs->GetFieldValue("button_name_off", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							strcpy(g_data.m_Buttons[nItemID].m_pchButtonOffLabel, szTemp);

							prs->GetFieldValue("button_type", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();

/* button_type values (auto, pass, block, indicator, manual) */ 
							
							if(szTemp.CompareNoCase("auto")==0)
							{
								g_data.m_Buttons[nItemID].m_ulFlags |= GTP32_BUTTON_TYPE_AUTO;
							}
							else
							if(szTemp.CompareNoCase("pass")==0)
							{
								g_data.m_Buttons[nItemID].m_ulFlags |= GTP32_BUTTON_TYPE_PASS;
							}
							else
							if(szTemp.CompareNoCase("block")==0)
							{
								g_data.m_Buttons[nItemID].m_ulFlags |= GTP32_BUTTON_TYPE_BLOCK;
							}
							else
							if(szTemp.CompareNoCase("indicator")==0)
							{
								g_data.m_Buttons[nItemID].m_ulFlags |= GTP32_BUTTON_TYPE_INDICATOR;
							}
							else
							if(szTemp.CompareNoCase("manual")==0)
							{
								g_data.m_Buttons[nItemID].m_ulFlags |= GTP32_BUTTON_TYPE_MANUAL;
							}
							else // not a registered type! just disable
							{
								g_data.m_Buttons[nItemID].m_ulFlags = GTP32_BUTTON_TYPE_NONE; // disables and sets to zero type.
							}


							prs->GetFieldValue("tally_label", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							strcpy(g_data.m_Buttons[nItemID].m_pchTallyEventLabel, szTemp);

							prs->GetFieldValue("tally_value", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							strcpy(g_data.m_Buttons[nItemID].m_pchTallyEventValue, szTemp);

							prs->GetFieldValue("click_label", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							strcpy(g_data.m_Buttons[nItemID].m_pchClickEventLabel, szTemp);

// ManualEvents.name as me_name, ManualEvents.event_type as me_event_type, 
// ManualEvents.programID as me_programID, ManualEvents.duration as me_duration
/*
	strcpy(m_pchManualEventName,"");
	strcpy(m_pchManualEventProgID,"");
	m_nManualEventType = GTP32_MANUAL_EVENT_TYPE_NONE;
	m_nManualEventDuration = -1; // invalid value...
*/
		
							prs->GetFieldValue("me_name", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							strcpy(g_data.m_Buttons[nItemID].m_pchManualEventName, szTemp);
							
							prs->GetFieldValue("me_event_type", szTemp);//HARDCODE
							szTemp.TrimLeft(); szTemp.TrimRight();
							g_data.m_Buttons[nItemID].m_nManualEventType = atoi(szTemp);

							if(g_data.m_Buttons[nItemID].m_nManualEventType == GTP32_MANUAL_EVENT_REPLACE_AD)
							{
								prs->GetFieldValue("me_duration", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								g_data.m_Buttons[nItemID].m_nManualEventDuration = atoi(szTemp);
								// following line is  so that we do not lose a frame due to rounding down 33.3333 to 33 and 66.6666 to 66.
								if(g_data.m_Buttons[nItemID].m_nManualEventDuration%10)g_data.m_Buttons[nItemID].m_nManualEventDuration++;

if(g_settings.m_ulDebug&(GTP32_DEBUG_SQL|GTP32_DEBUG_MANEVT)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:GetButtons", "Manual Event %d Type was Ad Replacement; duration %d", nItemID+1,  g_data.m_Buttons[nItemID].m_nManualEventDuration); // Sleep(250); //(Dispatch message)
}

							}
							else
							if(g_data.m_Buttons[nItemID].m_nManualEventType == GTP32_MANUAL_EVENT_REPLACE_CONTENT)
							{
								prs->GetFieldValue("me_programID", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								strncpy(g_data.m_Buttons[nItemID].m_pchManualEventProgID, szTemp, 63);
if(g_settings.m_ulDebug&(GTP32_DEBUG_SQL|GTP32_DEBUG_MANEVT)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:GetButtons", "Manual Event %d Type was Content Replacement; ID %s", nItemID+1,  g_data.m_Buttons[nItemID].m_pchManualEventProgID); // Sleep(250); //(Dispatch message)
}
							}
                            else
							if( //1.0.4.5  need to use a string field
                                (g_data.m_Buttons[nItemID].m_nManualEventType == GTP32_MANUAL_EVENT_BREAK_END)
                               ||(g_data.m_Buttons[nItemID].m_nManualEventType == GTP32_AUTO_EVENT_BREAK_START)
                               ||(g_data.m_Buttons[nItemID].m_nManualEventType == GTP32_AUTO_EVENT_BREAK_END)
                               )
							{
								prs->GetFieldValue("me_programID", szTemp);//HARDCODE
								szTemp.TrimLeft(); szTemp.TrimRight();
								strncpy(g_data.m_Buttons[nItemID].m_pchManualEventProgID, szTemp, 63);
if(g_settings.m_ulDebug&(GTP32_DEBUG_SQL|GTP32_DEBUG_MANEVT)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:GetButtons", "Manual Event %d Type was %d; ID %s", nItemID+1, g_data.m_Buttons[nItemID].m_nManualEventType, g_data.m_Buttons[nItemID].m_pchManualEventProgID); // Sleep(250); //(Dispatch message)
}
                            }
							else  //neither or nothing.
							{
								g_data.m_Buttons[nItemID].m_nManualEventType = GTP32_MANUAL_EVENT_TYPE_NONE;  // enforce
								g_data.m_Buttons[nItemID].m_nManualEventDuration = -1; // invalid value...
								strncpy(g_data.m_Buttons[nItemID].m_pchManualEventProgID, "", 63);

if(g_settings.m_ulDebug&(GTP32_DEBUG_SQL|GTP32_DEBUG_MANEVT)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:GetButtons", "Manual Event %d Type was Unknown", nItemID+1); // Sleep(250); //(Dispatch message)
}
							}
						}
					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
if(g_settings.m_ulDebug&(GTP32_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:GetButtons", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL); // Sleep(250); //(Dispatch message)
}					
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
if(g_settings.m_ulDebug&(GTP32_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:GetButtons", "Retrieve: Caught exception: out of memory\n%s", szSQL); // Sleep(250); //(Dispatch message)
}
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
if(g_settings.m_ulDebug&(GTP32_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:GetButtons", "Retrieve: Caught other exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)
}
					}
					e->Delete();
				} 
				catch( ... )
				{
					if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception.\n%s", szSQL);

if(g_settings.m_ulDebug&(GTP32_DEBUG_SQL)) 
{
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:GetButtons", "Retrieve: Caught exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)
}
					

				}
//g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "Got Destination: %s (0x%08x)", szName, ulFlags);  Sleep(250); //(Dispatch message)

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
			return nReturn;
		}
		else
		{
if(g_settings.m_ulDebug&(GTP32_DEBUG_SQL)) 
{	
	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:GetButtons", "returned NULL recordset [%s]", pszInfo?pszInfo:"NULL"); // Sleep(250); //(Dispatch message) 
}

		}
	}
//			g_ptabmsgr->DM(MSG_ICONHAND, NULL, "Timers:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return GTP32_ERROR;
}

int CGTP32Data::StartServer()
{
	m_bInitAuto = g_settings.m_bInitRequiresAuto?false:true;		// initialization condition
	m_bInitPass = g_settings.m_bInitRequiresPass?false:true;		// initialization condition
	m_bInitBlock = g_settings.m_bInitRequiresBlock?false:true;	// initialization condition

	m_bInitAutoClick=m_bInitAuto;		// initialization condition
	m_bInitPassClick=m_bInitPass;		// initialization condition
	m_bInitBlockClick=m_bInitBlock;	// initialization condition


	CNetServer* pServer = new CNetServer;
	pServer->m_usPort = 161;
	pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

//	pServer->m_nAf = PF_INET;
	pServer->m_nType = SOCK_DGRAM; 
	pServer->m_nProtocol = 0;

	pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
	if(pServer->m_pszName)strcpy(pServer->m_pszName, "SNMP server");

	pServer->m_pszStatus;				// status buffer with error messages from thread
	pServer->m_lpfnHandler = USP32ServerHandlerThread;			// pointer to the thread that handles the request.
	pServer->m_lpObject = this;								// pointer to the object passed to the handler thread.
	pServer->m_lpMsgObj = &m_net;					// pointer to the object with the Message function.

	char errorstring[DB_ERRORSTRING_LEN];

	int n = m_net.StartServer(pServer, &m_net, 5000, errorstring);
	if(n<NET_SUCCESS)
	{
		//report failure
//		CString Q; Q.Format("Failed to start server with error %d", n);
//		AfxMessageBox(Q);
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "GTP32_server_init", "Failed to start server with error %d %s", n, errorstring);  //(Dispatch message)

		if(pServer)
		{
			if(pServer->m_pszName) free(pServer->m_pszName);
			delete pServer;
		}
	}
	else
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32_server_init", "Started server on port 161");  //(Dispatch message)
	}
	return n;
}

int CGTP32Data::StopServer()
{
	char errorstring[DB_ERRORSTRING_LEN];
	int n= m_net.StopServer(161, 5000, errorstring);
	if(n<NET_SUCCESS)
	{
		//report failure
//		CString Q; Q.Format("Failed to start server with error %d", n);
//		AfxMessageBox(Q);
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "GTP32_server_init", "Failed to stop server with error %d %s", n, errorstring);  //(Dispatch message)
	}
	else
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32_server_init", "Stopped server on port 161 with %d", n);  //(Dispatch message)
	}
	return n;
}

int CGTP32Data::Connect()
{
	hostent* h=NULL;

	h = gethostbyname(g_settings.m_pszHost);
	if(h!=NULL)
	{
		memcpy(&m_saiGTPhost.sin_addr, h->h_addr, 4);
/*
		CString foo; foo.Format("Got %d.%d.%d.%d for %s", 
			m_saiGTPhost.sin_addr.s_net, 
			m_saiGTPhost.sin_addr.s_host, 
			m_saiGTPhost.sin_addr.s_lh, 
			m_saiGTPhost.sin_addr.s_impno,
			g_settings.m_pszHost);
		AfxMessageBox(foo);
*/
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32_connect_init", 
			"Got %d.%d.%d.%d for %s", 
			m_saiGTPhost.sin_addr.s_net, 
			m_saiGTPhost.sin_addr.s_host, 
			m_saiGTPhost.sin_addr.s_lh, 
			m_saiGTPhost.sin_addr.s_impno,
			g_settings.m_pszHost);  //(Dispatch message)
	}

	char szError[8096];
	if(m_net.OpenConnection(g_settings.m_pszHost, (short)(g_settings.m_nPort), 
		&m_s, 
		5000,
		5000,
		szError,
		AF_INET,SOCK_DGRAM,IPPROTO_UDP  // UDP for SNMP
		)<NET_SUCCESS)
	{
		m_s = NULL;
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "GTP32_connect_init", "Error connecting to %s:%d", g_settings.m_pszHost, g_settings.m_nPort);  //(Dispatch message)
	}
	else
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32_connect_init", "Connected to %s:%d", g_settings.m_pszHost, g_settings.m_nPort);  //(Dispatch message)

		return GTP32_SUCCESS;
	}
	return GTP32_ERROR;
}

int CGTP32Data::Disconnect()
{
	int n = m_net.CloseConnection(m_s);
	m_s = NULL;
	return n;
}



void USP32ServerHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	CGTP32Data* pdata = (CGTP32Data*)pClient->m_lpObject;
	CSNMPUtil snmpu;
	CBufferUtil bu;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		char* pchSnd = NULL;
		unsigned long ulSnd=0;

		CNetUtil net(false); // local object for utility functions.
		char pszToken[MAX_PATH];

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
//		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it


		CNetDatagramData** ppData=NULL;
		int nNumDataObj=0;

		struct timeval tv;
		tv.tv_sec = 0; tv.tv_usec = 30;  // timeout value
		fd_set fds;
		int nNumSockets;
		FD_ZERO(&fds);

		FD_SET(pClient->m_socket, &fds);

		nNumSockets = select(0, &fds, NULL, NULL, &tv);

//		SOCKADDR_IN si;
		int nSize = sizeof(pClient->m_si);

		while (
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&(nNumSockets>0)
					)
		{
			char* pchRecv = (char*)malloc(NET_COMMBUFFERSIZE);
			if(pchRecv)
			{
				int nNumBytes 
					=  recvfrom(
											pClient->m_socket,
											pchRecv,
											NET_COMMBUFFERSIZE,
											0,
											(struct sockaddr*)&(pClient->m_si),
											&nSize
										);

				if(
						(nNumBytes>0)
					&&(g_data.m_saiGTPhost.sin_addr.s_net == pClient->m_si.sin_addr.s_net)
					&&(g_data.m_saiGTPhost.sin_addr.s_host == pClient->m_si.sin_addr.s_host)
					&&(g_data.m_saiGTPhost.sin_addr.s_lh == pClient->m_si.sin_addr.s_lh)
					&&(g_data.m_saiGTPhost.sin_addr.s_impno == pClient->m_si.sin_addr.s_impno) // only accept communication from the GTP32!
					)
				{

					MessageLogging_t* pMsgLog = new MessageLogging_t;
					if(pMsgLog)
					{
						pMsgLog->pchReadableHexSnd=NULL;
						pMsgLog->ulReadableHexSnd=0;
						pMsgLog->pchReadableSnd=NULL;
						pMsgLog->ulReadableSnd=0;
						pMsgLog->pchReadableHexRcv=NULL;
						pMsgLog->ulReadableHexRcv=0;
						pMsgLog->pchReadableRcv=NULL;
						pMsgLog->ulReadableRcv=0;
					}

/*
typedef struct MessageLogging_t
{
	char* pchReadableHexSnd;
	unsigned long ulReadableHexSnd;
	char* pchReadableSnd;
	unsigned long ulReadableSnd;
	char* pchReadableHexRcv;
	unsigned long ulReadableHexRcv;
	char* pchReadableRcv;
	unsigned long ulReadableRcv;

} MessageLogging_t;
*/
					if(pMsgLog)
					{
pMsgLog->ulReadableRcv = nNumBytes;
pMsgLog->pchReadableHexRcv = bu.ReadableHex((char*)pchRecv, &pMsgLog->ulReadableRcv, MODE_FULLHEX);
					}

if((g_settings.m_bLogTransactions)&&(pMsgLog))
{

	if(pMsgLog->pchReadableHexRcv)
	{

		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32:RECV", "Listener Recd %d bytes: %s", nNumBytes,
							pMsgLog->pchReadableHexRcv); // Sleep(250); //(Dispatch message)

		//free(pchSnd);

		CSNMPMessage* pMessage = u.CreateSNMPMessageFromBuffer((unsigned char*)pchRecv);
		pMsgLog->pchReadableSnd = (char*)u.ReturnReadableMessageBuffer(pMessage);
		delete pMessage;

		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32:RECV", "Listener Recd message: %s", pMsgLog->pchReadableSnd); // Sleep(250); //(Dispatch message)
	}
}


					BOOL bNew = TRUE;  // Always do new, at least for now.  these SNMP messages are short and come in fast, but they come in whole.  So what was happening was that two would come in immediately, and we would assemble them into one buffer, and then process just the first part of the buffer, effectively ignoring the second msg.

/*
					BOOL bNew = FALSE;

					// first check the host list
					if(ppData)
					{
						int i=0;
						BOOL bFound = FALSE;
						while(i<nNumDataObj)
						{
							if(
									(ppData[i]->m_si.sin_port == si.sin_port) 
								&&(ppData[i]->m_si.sin_addr.S_un.S_addr == si.sin_addr.S_un.S_addr)
								) // check both port and address.
							{
								// add here
								unsigned char* pucNew = (unsigned char*)malloc(nNumBytes + ppData[i]->m_ulRecv);
								if(pucNew)
								{
									if(ppData[i]->m_pucRecv)
									{
										memcpy(pucNew, ppData[i]->m_pucRecv, ppData[i]->m_ulRecv);			
										free(ppData[i]->m_pucRecv);
									}

									memcpy(pucNew + ppData[i]->m_ulRecv, pchRecv, nNumBytes);

									ppData[i]->m_ulRecv += nNumBytes;
									ppData[i]->m_pucRecv = pucNew;
								}
								bFound = TRUE;
								break;
							}
						
							i++;
						}
						if(!bFound) bNew = TRUE;
					}
					else
					{
						bNew = TRUE;
						// add new.
					}
*/

					if(bNew)
					{
						CNetDatagramData* pData=new CNetDatagramData;

						if(pData)
						{
							pData->m_lpObject = NULL;
							pData->m_pucRecv = NULL;
							pData->m_ulRecv = 0;

							if(pMsgLog) pData->m_lpObject = pMsgLog;

							CNetDatagramData** ppTempData = new CNetDatagramData*[nNumDataObj+1];
							if(ppTempData)
							{
								if(ppData)
								{
									memcpy(ppTempData, ppData, nNumDataObj*sizeof(CNetDatagramData*));
									delete [] ppData;
								}

								ppTempData[nNumDataObj++] = pData;
								ppData = ppTempData;

							}
							unsigned char* pucNew = (unsigned char*)malloc(nNumBytes);
							if(pucNew)
							{
								memcpy(pucNew, pchRecv, nNumBytes);

								pData->m_ulRecv = nNumBytes;
								pData->m_pucRecv = pucNew;
								memcpy(&pData->m_si, &(pClient->m_si), sizeof(pClient->m_si));
								//pData->m_si.sin_addr.S_un.S_addr = si.sin_addr.S_un.S_addr;
							}

						}
					}
				}

				free(pchRecv); // free this, we have copied it to the object buffer (or failed, separately)
			}

			tv.tv_sec = 0; tv.tv_usec = 1000;  // longer timeout value for data continuation
			FD_ZERO(&fds);
			FD_SET(pClient->m_socket, &fds);
			nNumSockets = select(0, &fds, NULL, NULL, &tv);

		}

		// no more data on pipe - respond!

		int q=0;
		while(q<nNumDataObj)
		{
			if(ppData[q])
			{

				if(ppData[q]->m_pucRecv)
				{
					// create a message from the buffer.

					CSNMPMessage* pMessage = u.CreateSNMPMessageFromBuffer((unsigned char*)ppData[q]->m_pucRecv);

					if(pMessage)
					{

//CString Q; Q.Format("Error status %d; index %d", pMessage->m_ulErrorStatus, pMessage->m_ulErrorIndex);
//AfxMessageBox(Q);

						if(pMessage->m_pucValue)
						{

if(g_settings.m_ulDebug&(GTP32_DEBUG_EVENTHANDLER))
{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32:RECV", "checking event handlers for event %s", pMessage->m_pucValue); // Sleep(250); //(Dispatch message)
}
							// deal with the event value.
							int i=0;
							while(i<32)  // hardcoded 32 buttons
							{
								bool bStateChange = false;
								bool bClickChange = false;
								int nLen = 0;

								char buffer[256];
								if(strlen(pdata->m_Buttons[i].m_pchTallyEventLabel))
								{

									sprintf(buffer, "%s:",pdata->m_Buttons[i].m_pchTallyEventLabel);
									nLen = strlen(buffer);


	if(g_settings.m_ulDebug&(GTP32_DEBUG_EVENTHANDLER))
	{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32:RECV", "checking event handler %d: %s", i+1,buffer); // Sleep(250); //(Dispatch message)
	}


									if(strnicmp(buffer, (char*)(pMessage->m_pucValue), nLen)==0)
									{
										// event matches
	//									AfxMessageBox((char*)pMessage->m_pucValue + nLen);
										bool bVal = (atoi((char*)pMessage->m_pucValue + nLen)==0)?false:true;
										

	if(g_settings.m_ulDebug&(GTP32_DEBUG_EVENTHANDLER))
	{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32:RECV", "MATCH event handler %d: %s: value is %s, %d", i+1,(char*)(pMessage->m_pucValue), (char*)pMessage->m_pucValue + nLen, bVal); // Sleep(250); //(Dispatch message)
	}

										unsigned long ulNewStatus;

										if(atoi(pdata->m_Buttons[i].m_pchTallyEventValue)==0)
										{
											// want to match to false
											ulNewStatus = bVal?GTP32_BUTTON_STATUS_OFF:GTP32_BUTTON_STATUS_ON;
										}
										else
										{
											// want to match to true
											ulNewStatus = bVal?GTP32_BUTTON_STATUS_ON:GTP32_BUTTON_STATUS_OFF;
										}

										if(pdata->m_Buttons[i].m_ulStatus != ulNewStatus)
										{
											pdata->m_Buttons[i].m_ulStatus = ulNewStatus;
											bStateChange = true;
											pdata->m_Buttons[i].UpdateStatus(); // send it to the database

											// and now evaluate what to do about this GPO state change press (if anything).
										}

// do initialization:
										if(!g_data.m_bButtonsInitialized)
										{
											switch(pdata->m_Buttons[i].m_ulFlags&GTP32_BUTTON_TYPE_MASK)
											{
											case GTP32_BUTTON_TYPE_AUTO://				0x10
												{
													if((g_settings.m_bInitRequiresAuto)&&(!g_data.m_bInitAuto))
													{

if((g_settings.m_ulDebug&(GTP32_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:button_init", "AUTO button initialized"); // Sleep(250); //(Dispatch message)

														g_data.m_bInitAuto = TRUE;
													}
												} break;
											case GTP32_BUTTON_TYPE_PASS://				0x20
												{
													if((g_settings.m_bInitRequiresPass)&&(!g_data.m_bInitPass))
													{
if((g_settings.m_ulDebug&(GTP32_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:button_init", "PASS button initialized"); // Sleep(250); //(Dispatch message)
														g_data.m_bInitPass = TRUE;
													}
												} break;
											case GTP32_BUTTON_TYPE_BLOCK://				0x30
												{
													if((g_settings.m_bInitRequiresBlock)&&(!g_data.m_bInitBlock))
													{
if((g_settings.m_ulDebug&(GTP32_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:button_init", "BLOCK button initialized"); // Sleep(250); //(Dispatch message)
														g_data.m_bInitBlock = TRUE;
													}
												} break;
											}

											if(
												  (g_data.m_bInitAuto)&&(g_data.m_bInitPass)&&(g_data.m_bInitBlock)
												&&(g_data.m_bInitAutoClick)&&(g_data.m_bInitPassClick)&&(g_data.m_bInitBlockClick)
												)
											{
if((g_settings.m_ulDebug&(GTP32_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:button_init", "All required buttons initialized (A%dP%dB%d)", 
	 g_settings.m_bInitRequiresAuto,g_settings.m_bInitRequiresPass,g_settings.m_bInitRequiresBlock); // Sleep(250); //(Dispatch message)

												g_data.m_bButtonsInitialized = TRUE;
											}
										}



//									pDlg->m_button[i].Invalidate();
									}
								}

								if(strlen(pdata->m_Buttons[i].m_pchClickEventLabel))
								{
									sprintf(buffer, "%s:",pdata->m_Buttons[i].m_pchClickEventLabel);
									int nLen = strlen(buffer);

	if(g_settings.m_ulDebug&(GTP32_DEBUG_EVENTHANDLER))
	{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32:RECV", "checking click handler %d: %s", i+1,buffer); // Sleep(250); //(Dispatch message)
	}


									if(strnicmp(buffer, (char*)(pMessage->m_pucValue), nLen)==0)
									{
										// click event matches
	//									AfxMessageBox((char*)pMessage->m_pucValue + nLen);
										int nVal = atoi((char*)pMessage->m_pucValue + nLen);
										
	if(g_settings.m_ulDebug&(GTP32_DEBUG_EVENTHANDLER))
	{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32:RECV", "MATCH click handler %d: %s: value is %s, %d", i+1,(char*)(pMessage->m_pucValue), (char*)pMessage->m_pucValue + nLen, nVal); // Sleep(250); //(Dispatch message)
	}

										if(pdata->m_Buttons[i].m_nLastClickValue<0) // init, just assign
										{
											pdata->m_Buttons[i].m_nLastClickValue = nVal;
										}
										else
										{
											// want to match to see if different - clicking will toggle value.
											if(pdata->m_Buttons[i].m_nLastClickValue != nVal)
											{
												pdata->m_Buttons[i].m_nLastClickValue = nVal;
												bClickChange = true;
											}
										}


										if(!g_data.m_bButtonsInitialized)
										{
											switch(pdata->m_Buttons[i].m_ulFlags&GTP32_BUTTON_TYPE_MASK)
											{
											case GTP32_BUTTON_TYPE_AUTO://				0x10
												{
													if((g_settings.m_bInitRequiresAuto)&&(!g_data.m_bInitAutoClick))
													{

if((g_settings.m_ulDebug&(GTP32_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:button_init", "AUTO button click handler initialized"); // Sleep(250); //(Dispatch message)

														g_data.m_bInitAutoClick = TRUE;
													}
												} break;
											case GTP32_BUTTON_TYPE_PASS://				0x20
												{
													if((g_settings.m_bInitRequiresPass)&&(!g_data.m_bInitPassClick))
													{
if((g_settings.m_ulDebug&(GTP32_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:button_init", "PASS button click handler initialized"); // Sleep(250); //(Dispatch message)
														g_data.m_bInitPassClick = TRUE;
													}
												} break;
											case GTP32_BUTTON_TYPE_BLOCK://				0x30
												{
													if((g_settings.m_bInitRequiresBlock)&&(!g_data.m_bInitBlockClick))
													{
if((g_settings.m_ulDebug&(GTP32_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:button_init", "BLOCK button click handler initialized"); // Sleep(250); //(Dispatch message)
														g_data.m_bInitBlockClick = TRUE;
													}
												} break;
											}

											if(
												  (g_data.m_bInitAuto)&&(g_data.m_bInitPass)&&(g_data.m_bInitBlock)
												&&(g_data.m_bInitAutoClick)&&(g_data.m_bInitPassClick)&&(g_data.m_bInitBlockClick)
												)
											{
if((g_settings.m_ulDebug&(GTP32_DEBUG_INIT))&&(g_ptabmsgr))
	 g_ptabmsgr->DM(MSG_ICONHAND, NULL, "GTP32:button_init", "ALL required buttons initialized (A%dP%dB%d)", 
	 g_settings.m_bInitRequiresAuto,g_settings.m_bInitRequiresPass,g_settings.m_bInitRequiresBlock); // Sleep(250); //(Dispatch message)

												g_data.m_bButtonsInitialized = TRUE;
											}
										}


//									pDlg->m_button[i].Invalidate();
									}


								// at this point we know if a button was clicked or a tally was changed (usually GPO status)

									// here, we have a click handler and we know it has clicked or not.
									// if clicked, do something

									if(bClickChange)
									{
										switch(pdata->m_Buttons[i].m_ulFlags&GTP32_BUTTON_TYPE_MASK)
										{
										default:
										case GTP32_BUTTON_TYPE_NONE://				0x00
										case GTP32_BUTTON_TYPE_INDICATOR://		0x40
										case GTP32_BUTTON_TYPE_MANUAL://			0x50
											{
												// do nothing for key presses here.
												// (even manual mode, we will just go on GPO status for now)
												// this is the disney hardcode part of this right now.
											} break;
										case GTP32_BUTTON_TYPE_AUTO://				0x10
											{
	if(g_settings.m_ulDebug&(GTP32_DEBUG_BUTTONPRESS))
	{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32", "The AUTO button was pressed."); // Sleep(250); //(Dispatch message)
	}
	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, "ButtonClick", g_settings.m_pszInternalAppName, "The AUTO button was pressed.");
												// do the re-enter AUTO control mode event associated with this button.
												//TODO
/*
AUTO Control State
On entering (or re-entering) the AUTO Control State, Promotor shall:
1. Illuminate Button 1 ("AUTO") on the Basic Operator Panel.		// handled by the GTP 32 setup

2. If there is a Selected Automation System and it is currently executing a primary event, process an Automation Event with the in-progress primary event, 
   substituting for purposes of this processing:
			a. The current LTC time address in place of the starting time address of the in-progress primary event, and
			b. The remaining duration of the in-progress primary event in place of the duration of the in-progress primary event.


Processing Automation Events
To process an Automation Event, given a primary event, Promotor shall follow these steps:
1. (In Phase Two only:) If the Event Masking Timer is counting down, go to Step 7 of this procedure.
   (Before Phase Two:) If the Event Masking Timer is counting down, stop processing of this Automation Event.

	// we can send it over to the automation_data.dll for cancellation or processing.

2. If Promotor is not in the AUTO Control State, stop processing of this Automation Event.

	// This can be evaluated at Promotor to prevent this event being sent.
	// For the purposes of the button press here, we can assemble the event and send it over to the automation_data.dll for cancellation or processing

3. If the primary event is classified as Advertisement:
     a. Compute the Pod Duration, which shall be the sum of the duration of the current primary event and the durations of each of the
        immediately following primary events, if any, in the playlist that are also classified as Advertisement.
        (Note: The ad pod ends with the first event that is not classified as Advertisement, and the durations of this event and following events shall not be added to the Pod Duration.)
     b. Generate and insert a Replace Ad Message, populated as follows:
        - PodDur: The Pod Duration.
        - LongFormProgramID: The Program ID of the Current Long-Form Program.
        - LongFormProgramDescription: The Program Description of the Current Long-Form Program.
     c. Start the Event Masking Timer with the Pod Duration.
     d. Extinguish Button 2 ("PASS") on the Basic Operator Panel.
     e. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.
     f. Close the Downstream Keyer Control GPO.
     g. Stop processing of this Automation Event.


												// the Replace Ad tabulator event.
												//  Replace Ad Message message

												//  BLOCK GPO w Timer message
												//  Start Timer1
												//  SET USP GPO 3 ON
												//  SET USP GPO 3 OFF "un-press" it - no effect


else do the complex thing:

4. Retrieve the entries in the Replacement Rules Table having an Original Program ID equal to the Program ID of the primary event.

5. If no entries were retrieved from the Replacement Rules Table in the previous step, evaluate entries in the Description Pattern Table, and for
   each entry in the Description Pattern Table having a Matching Pattern satisfied by the Program Description of the primary event, retrieve the
   entries from the Replacement Rules Table having an Original Program ID equal to the Program ID in the Description Pattern Table entry.

6. If one or more entries were retrieved from the Replacement Rules Table in either of the two previous steps:
     a. Create an Effective Replacement Rules List, which shall contain the retrieved Replacement Rules Table entries, sorted by Replacement
        Sequence, from lowest to highest. Each entry in the Effective Replacement Rules List shall also have an Offset, which initially shall be the time address 00:00:00;00.
        (Note: The Effective Replacement Rules List need not persist after processing of the Automation Event is complete.)
     b. Compute the Required Replacement Duration, which shall be the starting time address of the Next Long-Form Program less the starting time address of the current primary event
     c. Compute the Trial Total Replacement Duration, which shall be the sum of the Replacement Durations of the entries in the Effective Replacement Rules List, less the sum of the Offsets of the entries in the Effective Replacement Rules List.
     d. If the Trial Total Replacement Duration is less than the Required Replacement Duration, increase the Replacement Duration of the last entry in the Effective Replacement Rules List by the difference between the Required Replacement Duration and the Trial Total Replacement Duration.
     e. If the Trial Total Replacement Duration is greater than the Required Replacement Duration, repeat the following steps until the Trial Total Replacement Duration is equal to the Required Replacement Duration:
          i. Set the Offset of the first entry in the Effective Replacement Rules List to the difference between the Trial Total Replacement Duration and the Required Replacement Duration.
          ii. If the Offset of the first entry in the Effective Replacement Rules List is greater than or equal to the Replacement Duration of the list entry, delete the entry from the list.
          iii. Recompute the Trial Total Replacement Duration.
     f. If there are no entries remaining in the Effective Replacement Rules List, stop processing of this Automation Event.
     g. For each entry in the Effective Replacement Rules List:
		 {
          i. Generate and insert a Replace Content Message, populated as follows:
						 - ReplProgramID: The Replacement Program ID in the list entry.
						 - ReplLength: The Replacement Duration in the list entry.
						 - Offset: The Offset in the list entry.

												// the Replace Content tabulator event.
												//  Replace Content Message message

          ii. Add to the Actual Total Replacement Duration the Replacement Duration in the list entry.
		 }
     h. Start the Event Masking Timer with the Actual Total Replacement Duration.
     i. Extinguish Button 2 ("PASS") on the Basic Operator Panel.
     j. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.
     k. Close the Downstream Keyer Control GPO.
     l. Stop processing of this Automation Event.


												//  BLOCK GPO w Timer message
												//  Start Timer1
												//  SET USP GPO 3 ON
												//  SET USP GPO 3 OFF "un-press" it - no effect



7. In Phase Two only, if the primary event, evaluated using the Network Source Identifiers, is associated with a network source:
     a. If the Network Replacement Operations List (updated as Stream Message Events are processed) indicates that a network-managed ad replacement operation, content replacement operation, or indefinite replacement operation is in progress:
          i. Generate and insert Stream Messages as required to carry out this replacement operation, provided that the effective duration of a network-managed ad replacement operation, other than an indefinite replacement operation, shall be limited to the starting time address of the Next Long-Form Program less the starting time address of the current primary event.
          ii. If the network-managed replacement operation is an ad replacement operation or content replacement operation, start the Event Masking Timer with the effective duration of the replacement operation.
          iii. Extinguish Button 2 ("PASS") on the Basic Operator Panel.
          iv. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.
          v. Close the Downstream Keyer Control GPO.
     b. Otherwise:
          i. Stop the Event Masking Timer.
          ii. Illuminate Button 2 ("PASS") on the Basic Operator Panel.
          iii. Extinguish Button 3 ("BLOCK") on the Basic Operator Panel.
          iv. Open the Downstream Keyer Control GPO.
          v. Generate and insert a Rejoin Main Message, thereby returning the TV+ audience to the Program Stream.
     c. Stop processing of this Automation Event.



	
Otherwise:
			a. Stop the Event Masking Timer.
			b. Illuminate Button 2 ("PASS") on the Basic Operator Panel.		// NOT handled by the GTP 32 setup
			c. Extinguish Button 3 ("BLOCK") on the Basic Operator Panel.		// NOT handled by the GTP 32 setup
			d. Open the Downstream Keyer Control GPO.												// NOT handled by the GTP 32 setup
			e. Generate and insert a Rejoin Main Message, thereby returning the TV+ audience to the Program Stream.
*/

												// This means, send the automation DLL a command of type AUTO and have it do all this logic
												// in the first instance it does the big complicated thing.
												// in the otherwise, it does the same thing as it does for PASS
												// so these tabluator events needed.

												// the AUTO tabulator events:

												// the Replace Ad tabulator event.
												//  Replace Ad Message message

												// the Replace Content tabulator event.
												//  Replace Content Message message

												//  BLOCK GPO w Timer message
												//  Start Timer1
												//  SET USP GPO 3 ON
												//  SET USP GPO 3 OFF "un-press" it - no effect



												// the otherwise event:

												// the PASS tabulator event:
												//  Stop Timer1
												//  SET USP GPO 2 ON
												//  Rejoin Main message
												//  SET USP GPO 2 OFF "un-press" it - no effect

//all of this is triggered with a single token:

// AutomationData|Replace_Current


_snprintf(pszToken, MAX_PATH-1, "%s|%s", g_settings.m_pszTabulatorPlugInModule, g_settings.m_pszTabulatorCommandReplaceCurrent);
unsigned char* pucDataBuffer = (unsigned char*)pszToken;
int nTabReturn = g_data.SendTabulatorCommand(&pucDataBuffer, "AUTO");
												if(nTabReturn>=GTP32_SUCCESS)
												{
													if((pucDataBuffer)&&(pucDataBuffer != (unsigned char*)pszToken))
													{
														free(pucDataBuffer);
													}
												}




											} break;
										case GTP32_BUTTON_TYPE_PASS://				0x20
											{
	if(g_settings.m_ulDebug&(GTP32_DEBUG_BUTTONPRESS))
	{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32", "The PASS button was pressed."); // Sleep(250); //(Dispatch message)
	}
	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, "ButtonClick", g_settings.m_pszInternalAppName, "The PASS button was pressed.");
												// do the re-enter PASS control mode event associated with this button.
												//TODO

/*
PASS Control State
On entering (or re-entering) the PASS Control State, Promotor shall:

1. Stop the Event Masking Timer.
2. Extinguish Button 1 ("AUTO") on the Basic Operator Panel.		// handled by the GTP 32 setup
3. Illuminate Button 2 ("PASS") on the Basic Operator Panel.		// handled by the GTP 32 setup
4. Extinguish Button 3 ("BLOCK") on the Basic Operator Panel.		// handled by the GTP 32 setup
5. Open the Downstream Keyer Control GPO.												// handled by the GTP 32 setup

6. Generate and insert a Rejoin Main Message, thereby returning the TV+ audience to the Program Stream.
*/
												// the PASS tabulator event:
												//  Stop Timer1
												//  SET USP GPO 2 ON
												//  Rejoin Main message
												//  SET USP GPO 2 OFF "un-press" it - no effect

//all of this is triggered with a single token:

// AutomationData|Enter_Pass


_snprintf(pszToken, MAX_PATH-1, "%s|%s", g_settings.m_pszTabulatorPlugInModule, g_settings.m_pszTabulatorCommandEnterPass);
unsigned char* pucDataBuffer = (unsigned char*)pszToken;
int nTabReturn = g_data.SendTabulatorCommand(&pucDataBuffer, "PASS");
												if(nTabReturn>=GTP32_SUCCESS)
												{
													if((pucDataBuffer)&&(pucDataBuffer != (unsigned char*)pszToken))
													{
														free(pucDataBuffer);
													}
												}





											} break;
										case GTP32_BUTTON_TYPE_BLOCK://				0x30
											{
	if(g_settings.m_ulDebug&(GTP32_DEBUG_BUTTONPRESS))
	{
			if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32", "The BLOCK button was pressed."); // Sleep(250); //(Dispatch message)
	}
	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, "ButtonClick", g_settings.m_pszInternalAppName, "The BLOCK button was pressed.");

												// do the re-enter BLOCK control mode event associated with this button.
												//TODO
/*
BLOCK Control State
On entering (or re-entering) the BLOCK Control State, Promotor shall:
1. Extinguish Button 1 ("AUTO") on the Basic Operator Panel.   // handled by the GTP 32 setup
2. Generate and insert an Event Notification Message having the value "TV+:BLOCKED" in the ProgramID field, thereby initiating an indefinite replacement operation at upLynk.
3. Extinguish Button 2 ("PASS") on the Basic Operator Panel.   // handled by the GTP 32 setup
4. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.  // handled by the GTP 32 setup
5. Close the Downstream Keyer Control GPO.										 // handled by the GTP 32 setup
*/
	
												// the BLOCK tabulator event.
												//  Event notification message
												//  SET USP GPO 3 ON
												//  SET USP GPO 3 OFF "un-press" it - no effect


//all of this is triggered with a single token:

// AutomationData|Enter_Block


_snprintf(pszToken, MAX_PATH-1, "%s|%s", g_settings.m_pszTabulatorPlugInModule, g_settings.m_pszTabulatorCommandEnterBlock);
unsigned char* pucDataBuffer = (unsigned char*)pszToken;
int nTabReturn = g_data.SendTabulatorCommand(&pucDataBuffer, "BLOCK");
												if(nTabReturn>=GTP32_SUCCESS)
												{
													if((pucDataBuffer)&&(pucDataBuffer != (unsigned char*)pszToken))
													{
														free(pucDataBuffer);
													}
												}

											} break;
										}
									}
								}
								else
								{
									// there was no click handler, use the tally as indicator of "pressed-ness"

									if(bStateChange)
									{
										switch(pdata->m_Buttons[i].m_ulFlags&GTP32_BUTTON_TYPE_MASK)
										{
										default:
										case GTP32_BUTTON_TYPE_NONE://				0x00
										case GTP32_BUTTON_TYPE_AUTO://				0x10
										case GTP32_BUTTON_TYPE_PASS://				0x20
										case GTP32_BUTTON_TYPE_BLOCK://				0x30
										case GTP32_BUTTON_TYPE_INDICATOR://		    0x40
											{
												// do nothing for state changes on these types of buttons.
											} break;
										case GTP32_BUTTON_TYPE_MANUAL://			0x50
											{
												// if this is pressed, it means that a manual event button was pressed.
												if(pdata->m_Buttons[i].m_ulStatus&GTP32_BUTTON_STATUS_ON)
												{

	if(g_settings.m_ulDebug&(GTP32_DEBUG_BUTTONPRESS))
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32", "GPO %d was triggered.", pdata->m_Buttons[i].m_nItemID); // Sleep(250); //(Dispatch message)
//		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32", "Manual Event button number %d was pressed.", pdata->m_Buttons[i].m_nItemID); // Sleep(250); //(Dispatch message)
	}
	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, "GPO state", g_settings.m_pszInternalAppName, "GPO %d was triggered.", pdata->m_Buttons[i].m_nItemID);
//	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, "ButtonClick", g_settings.m_pszInternalAppName, "Manual Event button number %d was pressed.", pdata->m_Buttons[i].m_nItemID);

													// do the manual event associated with this button.
													//TODO
/*
To process a Manual Event, given an invoking GPO number, Promotor shall follow
these steps:
1. If the Event Masking Timer is counting down or Promotor is not in the AUTO
Control State, stop processing of this Manual Event.



2. Retrieve the entry, if any, in the Manual Event Table having a GPO Number equal to the invoking GPO number.
If there is no such entry, stop processing of this Manual Event.

	// we are going to do step 2 first, since there is no consequence.
	// then send over the info and let the automation_data.dll cancel the event if step 1 evaluates.


3. If the Event Type in the Manual Event Table entry is Ad Replacement:
  a. Generate and insert a Replace Ad Message, populated as follows:
   - PodDur: The Duration in the Manual Event Table entry.
   - LongFormProgramID: The value "TV+:UNKNOWN"
   - LongFormProgramDescription: Blank.
  b. Start the Event Masking Timer with the Duration in the Manual Event Table entry.
  c. Extinguish Button 2 ("PASS") on the Basic Operator Panel.
  d. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.
  e. Close the Downstream Keyer Control GPO.
  f. Stop processing of this Manual Event.

												// the Replace Ad tabulator event.
												//  Replace Ad Message message

												//  BLOCK GPO w Timer message
												//  Start Timer1
												//  SET USP GPO 3 ON
												//  SET USP GPO 3 OFF "un-press" it - no effect



4. If the Event Type in the Manual Event Table entry is Program Replacement:
  a. Retrieve the entries in the Replacement Rules Table having an Original Program ID equal to the Program ID in the Retrieved Manual Event Table entry.
  b. If there are no such entries, stop processing of this Manual Event.
  c. For each retrieved Replacement Rules Table entry, sorted by Replacement Sequence, from lowest to highest:
	{
		i. Generate and insert a Replace Content Message, populated as follows:
		 - ReplLength: The Replacement Duration in the Replacement Rules Table entry.
		 - Offset: The time address 00:00:00;00.
		 - ReplProgramID: The Replacement Program ID in the Replacement Rules Table entry.

												// the Replace Content tabulator event.
												//  Replace Content Message message

	 ii. Add the Replacement Duration in the Replacement Rules Table entry to the Total Replacement Duration.
	}
5. Start the Event Masking Timer with the Total Replacement Duration.

												//  BLOCK GPO w Timer message
												//  Start Timer1
												//  SET USP GPO 3 ON
												//  SET USP GPO 3 OFF "un-press" it - no effect

6. Extinguish Button 2 ("PASS") on the Basic Operator Panel.
7. Illuminate Button 3 ("BLOCK") on the Basic Operator Panel.
8. Close the Downstream Keyer Control GPO.
9. Stop processing of this Manual Event.
*/

//all of this is triggered with a single token:

//AutomationData|Replace_Manual|<manual type>|<manual replacement data (=Pod dur if type is ad, original ID if type is content)>  // the actual replace command, results in the commands being sent.to Libretto, uses the manual event procedure



													char pszAsRunEventID[64];
													if(pdata->m_Buttons[i].m_nManualEventType == GTP32_MANUAL_EVENT_TYPE_NONE)
													{
														_snprintf(pszAsRunEventID, 63, "ManualEvent %d", i);

														// just log it.

	if(g_settings.m_ulDebug&(GTP32_DEBUG_MANEVT))
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32", "No Manual Event configured for button number %d.", pdata->m_Buttons[i].m_nItemID); // Sleep(250); //(Dispatch message)
	}
	g_data.SendAsRunMsg(CX_SENDMSG_INFO, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, "No Manual Event configured for button number %d.", pdata->m_Buttons[i].m_nItemID);

													}
													else
													{
														if(strlen(pdata->m_Buttons[i].m_pchManualEventName)>0)
														{
															_snprintf(pszAsRunEventID, 63, "%s", pdata->m_Buttons[i].m_pchManualEventName);
														}
														else
														{
															_snprintf(pszAsRunEventID, 63, "ManualEvent %d", i);
														}

														memset(pszToken, 0, MAX_PATH); // clear it.

                                                        
														if(
                                                            (pdata->m_Buttons[i].m_nManualEventType == GTP32_MANUAL_EVENT_BREAK_END) //1.0.4.5 Rogers
														   ||(pdata->m_Buttons[i].m_nManualEventType == GTP32_AUTO_EVENT_BREAK_START) //1.0.4.5 Rogers
														   ||(pdata->m_Buttons[i].m_nManualEventType == GTP32_AUTO_EVENT_BREAK_END) //1.0.4.5 Rogers
                                                          )
														{
//MAIN5|3|0|0|BStart|BEnd|137

// but use this with black in token to force an end.
//MAIN5|3|0|0||BEnd|137

_snprintf(pszToken, MAX_PATH-1, "%s|%s|%s", 
					g_settings.m_pszTabulatorPlugInModule, 
					g_settings.m_pszTabulatorCommandReplaceManual,
					pdata->m_Buttons[i].m_pchManualEventProgID  // fill progid with the tabulator string - crap cannot do, 16 char max. - just widen to 256 char.
					);
														}
														else
														if(pdata->m_Buttons[i].m_nManualEventType == GTP32_MANUAL_EVENT_REPLACE_AD)
														{

_snprintf(pszToken, MAX_PATH-1, "%s|%s|%d|%d", 
					g_settings.m_pszTabulatorPlugInModule, 
					g_settings.m_pszTabulatorCommandReplaceManual,
					pdata->m_Buttons[i].m_nManualEventType,
					pdata->m_Buttons[i].m_nManualEventDuration
					);
														}
														else
														if(pdata->m_Buttons[i].m_nManualEventType == GTP32_MANUAL_EVENT_REPLACE_CONTENT)
														{
															if(strlen(pdata->m_Buttons[i].m_pchManualEventProgID)>0)
															{
_snprintf(pszToken, MAX_PATH-1, "%s|%s|%d|%s", 
					g_settings.m_pszTabulatorPlugInModule, 
					g_settings.m_pszTabulatorCommandReplaceManual,
					pdata->m_Buttons[i].m_nManualEventType,
					pdata->m_Buttons[i].m_pchManualEventProgID
					);
															}
															else
															{
																// log the bad data.
	g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, "No Program ID configured for button number %d.", pdata->m_Buttons[i].m_nItemID);
	if(g_settings.m_ulDebug&(GTP32_DEBUG_MANEVT))
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "GTP32", "No Program ID configured for button number %d.", pdata->m_Buttons[i].m_nItemID); // Sleep(250); //(Dispatch message)
	}
															}	
														}
														else
														{
	g_data.SendAsRunMsg(CX_SENDMSG_ERROR, -1, pszAsRunEventID, g_settings.m_pszInternalAppName, "Unrecognized manual event type %d configured for button number %d.", 
		pdata->m_Buttons[i].m_nManualEventType,
		pdata->m_Buttons[i].m_nItemID);
	if(g_settings.m_ulDebug&(GTP32_DEBUG_MANEVT))
	{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONERROR, NULL, "GTP32", "Unrecognized manual event type %d configured for button number %d.", 
			pdata->m_Buttons[i].m_nManualEventType,
			pdata->m_Buttons[i].m_nItemID); // Sleep(250); //(Dispatch message)
	}
														}

														if(strlen(pszToken)>0)
														{
unsigned char* pucDataBuffer = (unsigned char*)pszToken;
int nTabReturn = g_data.SendTabulatorCommand(&pucDataBuffer, pszAsRunEventID);
															if(nTabReturn>=GTP32_SUCCESS)
															{
																if((pucDataBuffer)&&(pucDataBuffer != (unsigned char*)pszToken))
																{
																	free(pucDataBuffer);
																}
															}
														}
													}

												}
											} break;
										}
									}
								}




								i++;
							}



	if(g_settings.m_ulDebug&(GTP32_DEBUG_EVENTHANDLER))
{
		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32:RECV", "finished checking %d event handlers for event %s%c%c", i,pMessage->m_pucValue, 13, 10); // Sleep(250); //(Dispatch message)
}
							free(pMessage->m_pucValue);

						}
						pMessage->m_pucValue = NULL;
						pMessage->m_ulValueLength=0;
						pMessage->m_ucPDUType = SNMP_GETRESPONSEPDU;
						pMessage->m_ulErrorStatus = 0;
						pMessage->m_ulErrorIndex = 0;


//CString Q; Q.Format("Error status %d; index %d", pMessage->m_ulErrorStatus, pMessage->m_ulErrorIndex);
//AfxMessageBox(Q);


						// then send a response

						pchSnd = (char*)u.ReturnMessageBuffer(pMessage);
						ulSnd = u.ReturnLength((unsigned char*)pchSnd+1);
						ulSnd += u.ReturnLength(ulSnd)+1;

						sendto(
							pClient->m_socket,
							(char*)pchSnd, 
							ulSnd,
							0,
							(struct sockaddr *) &(ppData[q]->m_si),
							nSize
						);

if(ppData[q]->m_lpObject)
{
	((MessageLogging_t*)(ppData[q]->m_lpObject))->ulReadableHexSnd = ulSnd;
	((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd = bu.ReadableHex((char*)pchSnd, &((MessageLogging_t*)(ppData[q]->m_lpObject))->ulReadableHexSnd, MODE_FULLHEX);
	((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableSnd = (char*)u.ReturnReadableMessageBuffer(pMessage);
}
						delete pMessage;

						free(pchSnd);  pchSnd=NULL;
					}
					else
					{
						// just echo it

						sendto(
							pClient->m_socket,
							(char*)ppData[q]->m_pucRecv, 
							ppData[q]->m_ulRecv,
							0,
							(struct sockaddr *) &(ppData[q]->m_si),
							nSize
						);
ulSnd = ppData[q]->m_ulRecv;
if(ppData[q]->m_lpObject)
{
	((MessageLogging_t*)(ppData[q]->m_lpObject))->ulReadableHexSnd = ulSnd;
	((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd = bu.ReadableHex((char*)ppData[q]->m_pucRecv, &((MessageLogging_t*)(ppData[q]->m_lpObject))->ulReadableHexSnd, MODE_FULLHEX);

	CSNMPMessage* pTempMessage = u.CreateSNMPMessageFromBuffer((unsigned char*)ppData[q]->m_pucRecv);
	((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableSnd = (char*)u.ReturnReadableMessageBuffer(pTempMessage);
	delete pTempMessage;
}
					}



if((g_settings.m_bLogTransactions)&&(ppData[q]->m_lpObject))
{

	if(((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd)
	{

		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32:SEND", "Listener Sent %d bytes: %s", 	ulSnd,
					((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd); // Sleep(250); //(Dispatch message)

		if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, "GTP32:SEND", "Listener Sent message: %s%c%c", 	
					((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableSnd, 13, 10); // Sleep(250); //(Dispatch message)
	//		free(pchRecv);  // no, use below.
	}
}

/*

	// this was to update a list box, but we don;t have this in the module, just the tester that this code came from

						int nCount = pDlg->m_lcResponses.GetItemCount();

						pDlg->m_lcResponses.InsertItem(nCount, "<-");
/*
						if(pchSnd)
						{
							m_lcResponses.InsertItem(nCount, pchSnd);
						}
						else
						{
							m_lcResponses.InsertItem(nCount, (char*)buffer);
						}

						
						if(pchRecv)
						{
							m_lcResponses.SetItemText(nCount, 1, (char*)pchRecv );
						}
						else
						{
							m_lcResponses.SetItemText(nCount, 1, (char*)puc );
						}

* /
						if((ppData[q]->m_lpObject)&&(((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexRcv))
						{
							pDlg->m_lcResponses.SetItemText(nCount, 1, ((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexRcv);
						}
						else
						{
							pDlg->m_lcResponses.SetItemText(nCount, 1, (char*)ppData[q]->m_pucRecv);
						}
						
						if((ppData[q]->m_lpObject)&&(((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd))
						{
							pDlg->m_lcResponses.SetItemText(nCount, 2, (char*)((MessageLogging_t*)(ppData[q]->m_lpObject))->pchReadableHexSnd );
						}
						else
						{
							pDlg->m_lcResponses.SetItemText(nCount, 2, (char*)ppData[q]->m_pucRecv );
						}
						pDlg->m_lcResponses.EnsureVisible(nCount, FALSE);
						while(nCount>LISTBOXMAX){  pDlg->m_lcResponses.DeleteItem(0); nCount--; } //delete oldest
*/
							
				}


				if(ppData[q]->m_lpObject)
				{
					// have to free all the buffers.
					MessageLogging_t* pMsgLog = (MessageLogging_t*) ppData[q]->m_lpObject;

					if(pMsgLog->pchReadableHexRcv) free(pMsgLog->pchReadableHexRcv);
					if(pMsgLog->pchReadableHexSnd) free(pMsgLog->pchReadableHexSnd);
					if(pMsgLog->pchReadableRcv) free(pMsgLog->pchReadableRcv);
					if(pMsgLog->pchReadableSnd) free(pMsgLog->pchReadableSnd);

					delete ppData[q]->m_lpObject;
				}
				if(ppData[q]->m_pucRecv) free(ppData[q]->m_pucRecv);

				delete ppData[q];
			}
			q++;
		}


		delete [] ppData;

//		pDlg->SetButtonText();

		if(pchSnd) free(pchSnd);

	}

	(*(pClient->m_pulConnections))--;

	delete pClient; // was created with new in the thread that spawned this one.
}
