// StreamDataCore.cpp: implementation of the CStreamDataCore class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "StreamData.h"
#include "StreamDataData.h"
#include "StreamDataSettings.h"
#include "StreamDataCore.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


extern CStreamDataData					g_data;
extern CStreamDataSettings			g_settings;
extern CStreamDataCore					g_core;
extern CMessager*					g_ptabmsgr;
extern DLLdata_t      g_dlldata;
extern CStreamDataApp				theOppApp;



void TimeCodeDataThread(void* pvArgs);


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CStreamDataCore::CStreamDataCore()
{
//	theOppApp.MB("core");

}

CStreamDataCore::~CStreamDataCore()
{

}


BOOL CStreamDataCore::IsPointInWnd(CPoint point, CWnd* pWnd) 
{
	CRect rcRect;
	pWnd->GetWindowRect(&rcRect);
	if(pWnd)
		return IsPointInRect( point,  rcRect); 
	else
		return FALSE;
}

BOOL CStreamDataCore::IsPointInRect(CPoint point, CRect rcWindowRect) 
{
	if(	(point.x>rcWindowRect.left)&&
			(point.x<rcWindowRect.right)&&
			(point.y>rcWindowRect.top)&&
			(point.y<rcWindowRect.bottom)	)
		return TRUE;
	else
		return FALSE;
}

// This function will encode a line of text for display
// under CAL.  Call this BEFORE adding any escape 
// sequences (such as [f 1] to change the font to font 1)
CString CStreamDataCore::CALEncodeBrackets(CString szString)
{
  int k;
	char ch;
  for (k=0; k<szString.GetLength(); k++)
  {
		ch = szString.GetAt(k);
    if (( ch == '[')||( ch == '\\'))
    {
      szString = szString.Left(k) + "\\" + 
        szString.Right(szString.GetLength() - k);
      k++;
    }
  }
  return szString;
}



void TimeCodeDataThread(void* pvArgs)
{
	CStreamDataData* p = &(g_data);
	
	p->m_bTimeCodeThreadStarted=true;
	g_dlldata.thread[0]->nThreadState |= 0x10; ///started TC
	SetThreadPriority(GetCurrentThread(), REALTIME_PRIORITY_CLASS);

	BOOL bNewInput = TRUE;
//	BOOL bAttemptSync = FALSE;  // dont even need.

	int nLastSleep = -1;
	int nLastValidTime = -1;
	int nValidLoops = 0;
	unsigned long ulLastTC=0xffffffff;
	int nLastTCTime = -1;
	int nNumTCRepeats = 0;
	int nLastSync = 0;
//	SYSTEMTIME SystemTime;  // for jam sync the clock

	while((!p->m_bTimeCodeThreadKill)&&(!g_dlldata.thread[0]->bKillThread))
	{
		unsigned long tick = clock();
		p->m_ucTimeCodeBits=0x80; //default to DF
		if(g_core.m_tc.m_hConnection == INVALID_HANDLE_VALUE)
		{
	EnterCriticalSection(&p->m_critTimeCode);
							p->m_ulTimeCode = 0xffffffff;
							p->m_ulTimeCodeElapsed = 0;
	LeaveCriticalSection(&p->m_critTimeCode);

//			p->SetWindowText(" Adrienne LTC/RDR: no device");
			// connect.
			g_core.m_tc.ConnectToCard();

			if(g_core.m_tc.m_hConnection != INVALID_HANDLE_VALUE)
			{
//				p->SetWindowText(" Adrienne LTC/RDR: connected!");

				g_core.m_tc.GetBoardCapabilitesCode();
				if(!(g_core.m_tc.m_ucBoardCapabilitesCode & 0x10)) // no capabilities
				{
					bNewInput = TRUE;
//					bAttemptSync = FALSE; // disengage
//					p->GetDlgItem(IDC_STATIC_TEXT)->SetWindowText("Sync disengaged!  LTC not installed");
//					p->SetWindowText("Adrienne LTC/RDR: no LTC RDR");
					g_core.m_tc.DisconnectFromCard();  // nothing else to do.
					Sleep(300);
				}
				else
				{
//					p->SetWindowText(" Adrienne LTC/RDR: LTC installed");
					nLastValidTime = clock(); // to reset timing
					nLastTCTime = clock(); // to reset timing
				}
			}
		}
		else
		{
		
			//connected, so just grab status and time if status allows.
			int nBoardCheck = clock();
			g_core.m_tc.GetBoardInputsAndOpModeCode();
			int nBoardEnd = clock();

//			g_core.m_tc.GetBoardOpModeCode();
//			g_core.m_tc.GetBoardInputsCode();
			//inputs
//If bit is set... (Register mapping)
//Bit 6 => L21 input data OK
//Bit 5 => VITC input data OK
//Bit 4 => LTC input data OK
//Bit 3 => Wide VSYNC pulses detected
//Bit 1 => ODD(1) or EVEN(0) video field
//Bit 0 => Video input OK	
			//op modes
//If bit is set... (Register mapping)
//Bit 7 => Diagnostics active
//Bit 6 => FILM(24fps) mode selected
//Bit 5 => LTC(1) or VITC(0) selected
//Bit 4 => EBU(25fps) mode selected
//Bit 3 => VITC Generator active
//Bit 2 => LTC Generator active
//Bit 1 => VITC and/or L21 Reader active
//Bit 0 => LTC Reader active	
			CString t; 
			CString g; 

			if(g_core.m_tc.m_ucBoardOpModeCode&0x20)
			{
				if(g_core.m_tc.m_ucBoardInputsCode&0x10)
				{
					if(bNewInput)
					{
	EnterCriticalSection(&p->m_critTimeCode);
						p->m_ucTimeCodeBits = g_core.m_tc.GetTimeCodeBits();  //get actual DF
	LeaveCriticalSection(&p->m_critTimeCode);
						bNewInput = FALSE;
					}

/*
1) Read and save the status of the VALIDATION byte at 1Ah.
2) Go back to step #1 if bit 7 is high (update in progress).
3) Quickly read and save all of the time, user, and embedded bits which are required by your application. Do not process yet.
4) Read the VALIDATION byte again, then compare it with the value read in step #1.
5) Go back to step #1 if the VALIDATION byte has changed.
6) If desired, read the input signal status byte at 0Ch to make sure that the time code data you just read is current.
7) Process and/or display the time code data as desired. Note that if you attempt to display the time bits as they are read, on many PC�s the
   next time code field or frame will appear before you have finished displaying the previous one. The results will not be pleasant, hence our recommendations.
8) Update the rest of your program operations.
9) Go back to step #1 (NOT STEP #3!).
*/
					int nValidCheck = 0;
					int nValidStart = clock();
					int nValidEnd = 0;
					int nTimeEnd = 0;

					unsigned char vc = g_core.m_tc.GetValidationCode();
					while((!p->m_bTimeCodeThreadKill)&&(vc&0x80))
					{
						Sleep(1);
						vc = g_core.m_tc.GetValidationCode();
						nValidCheck++;
					}
					nValidEnd = clock();
					unsigned long TC = g_core.m_tc.GetTimeCode();
					nTimeEnd = clock();

//if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONHAND, NULL, "StreamData:debug", "timecode obtained %08x", TC);//   Sleep(250);//(Dispatch message)

					if((!p->m_bTimeCodeThreadKill)&&(vc == g_core.m_tc.GetValidationCode()))
					{
						if(TC != 0xffffffff)
						{
	EnterCriticalSection(&p->m_critTimeCode);
							p->m_ulTimeCode = TC;
							p->m_ulTimeCodeElapsed = 0;
	LeaveCriticalSection(&p->m_critTimeCode);

							int nValidTime = clock() - nLastValidTime;
							if(nValidTime > 34) // >1 frame, hardcode for now.  means we skipped a frame
							{
/*
								FILE* fp;
								fp = fopen("LTCRDR.txt","ab");
								if(fp)
								{
									_timeb timestamp;
									_ftime(&timestamp);
									char logtmbuf[48]; // need 33;
									tm* theTime = localtime( &timestamp.time	);
									strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

									fprintf(fp, "%s%03d Delayed update: %d ms since last update; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
										nValidTime,
										nBoardCheck, nBoardEnd,
										nValidStart,
										nValidEnd,
										nValidCheck,
										nTimeEnd,
										nValidLoops,
										nLastSleep
										);

									fclose(fp);
								}
*/
							}
							
							int nTCTime = nTimeEnd - nLastTCTime;
							if(ulLastTC != TC) // time address has changed
							{

								if(nTCTime > 3*16) // 3 16ms timeslices at most, sampling error for one frame with 16 ms tolerance
								{
/*
									FILE* fp;
									fp = fopen("LTCRDR.txt","ab");
									if(fp)
									{
										_timeb timestamp;
										_ftime(&timestamp);
										char logtmbuf[48]; // need 33;
										tm* theTime = localtime( &timestamp.time	);
										strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

										fprintf(fp, "%s%03d Delayed register change %08x->%08x: %d ms since last update with %d repeats; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
											ulLastTC, TC,
											nTCTime, nNumTCRepeats,
											nBoardCheck, nBoardEnd,
											nValidStart,
											nValidEnd,
											nValidCheck,
											nTimeEnd,
											nValidLoops,
											nLastSleep
											);

										fclose(fp);
									}
*/

								}

/*
								if((TC<0x00000010)||(TC>0x23595915)) // log around midnight
								{
									FILE* fp;
									fp = fopen("LTCRDR.txt","ab");
									if(fp)
									{
										_timeb timestamp;
										_ftime(&timestamp);
										char logtmbuf[48]; // need 33;
										tm* theTime = localtime( &timestamp.time	);
										strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

										fprintf(fp, "%s%03d CHECK register change %08x->%08x: %d ms since last update with %d repeats; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
											ulLastTC, TC,
											nTCTime, nNumTCRepeats,
											nBoardCheck, nBoardEnd,
											nValidStart,
											nValidEnd,
											nValidCheck,
											nTimeEnd,
											nValidLoops,
											nLastSleep
											);

										fclose(fp);
									}

								}

*/
								nLastTCTime = nTimeEnd;
								ulLastTC = TC;
								nNumTCRepeats = 0;
							}
							else
							{
								nNumTCRepeats++;
							}

							nValidLoops=0;
							nLastValidTime = nTimeEnd; // just reset

/*
							t.Format(" Adrienne LTC/RDR: %02x:%02x:%02x%s%02x  op:0x%02x in:0x%02x",
								(unsigned char)((TC&0xff000000)>>24),
								(unsigned char)((TC&0x00ff0000)>>16),
								(unsigned char)((TC&0x0000ff00)>>8),  df&0x80?";":":",
								(unsigned char)(TC&0x000000ff),
								g_core.m_tc.m_ucBoardOpModeCode,
								g_core.m_tc.m_ucBoardInputsCode
								);
*/

						}
						else
						{
	EnterCriticalSection(&p->m_critTimeCode);
							p->m_ulTimeCodeElapsed = clock() - nLastValidTime;
	LeaveCriticalSection(&p->m_critTimeCode);

/*
							t.Format(" Adrienne LTC/RDR: --:--:--%s--  op:0x%02x in:0x%02x %d",
									df&0x80?";":":",
									g_core.m_tc.m_ucBoardOpModeCode,
									g_core.m_tc.m_ucBoardInputsCode, clock()  // added clock so disply does something
								);

							FILE* fp;
							fp = fopen("LTCRDR.txt","ab");
							if(fp)
							{
								_timeb timestamp;
								_ftime(&timestamp);
								char logtmbuf[48]; // need 33;
								tm* theTime = localtime( &timestamp.time	);
								strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

								fprintf(fp, "%s%03d TC ERROR %d ms since last update with %d repeats; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
									
									nTimeEnd - nLastValidTime, nNumTCRepeats,
									nBoardCheck, nBoardEnd,
									nValidStart,
									nValidEnd,
									nValidCheck,
									nTimeEnd,
									nValidLoops,
									nLastSleep
									);

								fclose(fp);
							}
*/

						}
//						p->GetWindowText(g);
//						if(g.Compare(t))	p->SetWindowText(t);			// compare makes it less blinky
					}
					else
					{
						nValidLoops++;
						// p->SetWindowText("Adrienne LTC/RDR: no LTC input"); // just leave it at last update
					}

				}
				else
				{
	EnterCriticalSection(&p->m_critTimeCode);
							p->m_ulTimeCodeElapsed = clock() - nLastValidTime;
	LeaveCriticalSection(&p->m_critTimeCode);

					bNewInput = TRUE;
//					p->m_bSync = FALSE; // disengage
//					p->GetDlgItem(IDC_STATIC_TEXT)->SetWindowText("Sync disengaged!  Lost input");

//					p->GetWindowText(g);
//					t.Format(" Adrienne LTC/RDR: no LTC input op:0x%02x in:0x%02x", g_core.m_tc.m_ucBoardOpModeCode, g_core.m_tc.m_ucBoardInputsCode);
//					if(g.Compare(t))	p->SetWindowText(t);			// compare gets rid of blinky
				}
			}
			else
			{
				bNewInput = TRUE;
//				p->m_bSync = FALSE; // disengage
//				p->GetDlgItem(IDC_STATIC_TEXT)->SetWindowText("Sync disengaged!  LTC not selected");

//				p->GetWindowText(g);
//				t.Format(" Adrienne LTC/RDR: LTC not selected op:0x%02x cap:0x%02x", g_core.m_tc.m_ucBoardOpModeCode, g_core.m_tc.m_ucBoardCapabilitesCode);
//				if(g.Compare(t))	p->SetWindowText(t);			// compare gets rid of blinky
	EnterCriticalSection(&p->m_critTimeCode);
							p->m_ulTimeCodeElapsed = clock() - nLastValidTime;
	LeaveCriticalSection(&p->m_critTimeCode);
			}
		}

/*
		tick = 10 - (clock() - tick);

		if(tick<10)  // this should allow 3 attempts per frame (time address).
		{
			nLastSleep = tick;
			Sleep(tick);
		}
		else
		{
			Sleep(0);
			nLastSleep = 0;
		}
*/
		Sleep(1) ;// 33 times per frame, possibly
		nLastSleep = 1;
	}
	//		AfxMessageBox("exiting");


//	p->SetWindowText(" Disconnecting"); Sleep(1);
	g_core.m_tc.DisconnectFromCard();

//	p->SetWindowText(" Disconnected");
	p->m_bTimeCodeThreadStarted=false;
	g_dlldata.thread[0]->nThreadState &= ~0x10; ///started

}



