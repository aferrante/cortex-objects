// LibrettoData.h: interface for the CLibrettoData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LIBRETTODATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
#define AFX_LIBRETTODATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <winsock2.h>
#include <sys/timeb.h>
#include "../../Common/TXT/BufferUtil.h" 
#include "../../Common/LAN/NetUtil.h" 
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"
//#include "../../Common/API/Miranda/IS2Comm.h"
#include "../../Common/KEY/LicenseKey.h"
#include "../../Common/TTY/Serial.h"
#include "../../Common/TIME/TimeConvert.h"

#define LIBRETTO_MEMQUEUE_SIZEINCR 32

class CLibrettoList  
{
public:
	CLibrettoList();
	virtual ~CLibrettoList();
	
	int m_nNumItems;
	char** m_ppchItems;

	int AddUnique(char* pchItem);
};

class CLibrettoQueueObject  
{
public:
	CLibrettoQueueObject();
	virtual ~CLibrettoQueueObject();
	
	int nItemid;
	CString szLocal;
	CString szRemote;
	int nAction;
	// CString szServer; // no need, as this is a per-host array.					
	double dblTimestamp;
	CString szUsername;
	int nEventItemID;
	CString szMessage;
};

class CLibrettoDestinationObject  
{
public:
	CLibrettoDestinationObject();
	virtual ~CLibrettoDestinationObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	char* m_pszServerName;
//	char* m_pszClientName;
	char* m_pszDesc;
	unsigned short m_usPort;

//	CAConnection* m_pAPIConn;  // pointer to the associated API connection.
	SOCKET m_socket;
	CNetUtil m_net;
	CSerial m_serial;
	CBufferUtil m_bu;
	CTimeConvert  m_timeconv;


	double m_dblDiskKBFree;  
	double m_dblDiskKBTotal; 
	double m_dblDiskPercent; 
	int m_nChannelID;

	int m_nItemID;

	_timeb m_timebLastDisconnect;
	_timeb m_timebCommandQueueTick;
	_timeb m_timebLastPing;  // for keep alives

	// control
	bool m_bKillCommandQueueThread;
	bool m_bCommandQueueThreadStarted;
	bool m_bKillConnThread;
	bool m_bConnThreadStarted;
	bool m_bKillMonThread;
	bool m_bMonThreadStarted;
	bool m_bSending;

// external support
	void* m_pvExternal;

//	bool m_bFailed;
//	_timeb m_timebFailure;
	CRITICAL_SECTION m_critComm;
	CRITICAL_SECTION m_critMemQueue;
	CLibrettoQueueObject** m_ppMemQueue;
	int m_nNumMemQueueArray;
	int m_nPtrMemQueueIn;
	int m_nPtrMemQueueOut;

	int SendTelnetCommand(char* pchBuffer, unsigned long ulBufferLen, char chEolnType);
	int AddMemQueue(CLibrettoQueueObject* pMemQueue);
//	int SetMemQueue(int nMemIndex, CLibrettoQueueObject* pMemQueue);
	int GetMemQueue(int nID=0); //returns index of next
	int RemoveMemQueue(int nIndex); 
	int ClearMemQueue();
};

/*
class CLibrettoChannelObject  
{
public:
	CLibrettoChannelObject();
	virtual ~CLibrettoChannelObject();

	unsigned long m_ulStatus;  // various states
	unsigned long m_ulFlags;   // various flags
	unsigned short m_usType;

	int m_nChannelID;  // the unique Channel ID within Libretto setup. (assigned externally)
	int m_nHarrisListID;  // the 1-based List # on the associated ADC-100 server

	char* m_pszServerName;
	char* m_pszDesc;

	CAConnection* m_pAPIConn;  // pointer to the associated API connection.

// control
	bool* m_pbKillConnThread;
	bool m_bKillChannelThread;
	bool m_bChannelThreadStarted;
};

*/


typedef struct LibrettoThreadArgs_t
{
	CLibrettoDestinationObject* pDest;
	void* pvoid;
} LibrettoThreadArgs_t;


class CLibrettoData  
{
public:
	CLibrettoData();
	virtual ~CLibrettoData();

	// util object
	CBufferUtil m_bu;

  // hostname.  have to get it, store it here
	char*	m_pszHost;	// the name of the host
	char*	m_pszCompleteHost;	// the complete name of the host

	char*	m_pszCortexHost;	// the name of the cortex host
	unsigned short m_usCortexCommandPort;
	unsigned short m_usCortexStatusPort;

	bool m_bNetworkMessagingInitialized;
	bool m_bCheckModsWarningSent;
	bool m_bCheckMsgsWarningSent;
//	bool m_bCheckAsRunWarningSent;

	CLibrettoDestinationObject** m_ppDestObj;
	int m_nNumDestinationObjects;

//	CLibrettoChannelObject** m_ppChannelObj;
//	int m_nNumChannelObjects;

	_timeb m_timebAutoPurge; // the last time autopurge was run
	_timeb m_timebTick; // the last time check inside the thread
	_timeb m_timebLastStatus; // the time of the last status given to cortex
	unsigned long m_ulFlags;  // various states
	unsigned long m_ulStatusCounter; // a counter incrementor for cortex global status (each obj has their own as well)

	int m_nSettingsMod;
//	int m_nChannelsMod;
	int m_nDestinationsMod;
	int m_nQueueMod;
	int m_nLastSettingsMod;
//	int m_nLastChannelsMod;
	int m_nLastDestinationsMod;
	int m_nLastQueueMod;
	bool m_bProcessSuspended;
	int m_nMaxLicensedDevices;

	char* GetStatusText(unsigned long* pulStatus);  // allocates mem, must free after use
//	int		SetStatusText(char* pszText, unsigned long ulStatus);
	int		SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError = false);
	int		GetHost();

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;
	int CheckMessages(char* pszInfo=NULL);
//	int CheckAsRun(char* pszInfo=NULL);
	int CheckDatabaseMods(char* pszInfo=NULL);
	int IncrementDatabaseMods(char* pszTableName, char* pszInfo=NULL, CDBUtil* pdb=NULL, CDBconn* pdbConn=NULL);
	int GetDestinations(char* pszInfo=NULL);
	int GetQueue(char* pszInfo=NULL);
	int ReturnDestinationIndex(char* pszServerName);

	CLicenseKey m_key;

	bool m_bQuietKill;

	CRITICAL_SECTION m_critText;
	CRITICAL_SECTION m_critDestinations;

	CRITICAL_SECTION m_critTimeCode;
	unsigned long m_ulTimeCode;
	unsigned long m_ulTimeCodeElapsed;
	bool m_bTimeCodeThreadStarted;
	bool m_bTimeCodeThreadKill;
	unsigned char m_ucTimeCodeBits;


private:
	char* m_pszStatus;	// parseable string

//	unsigned long m_ulReqCount;
//	unsigned long m_ulSvcCount;
};

#endif // !defined(AFX_LIBRETTODATA_H__B06A98AE_07CF_4A18_8CE3_4C939B30B4D7__INCLUDED_)
