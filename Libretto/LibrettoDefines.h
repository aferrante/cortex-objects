// LibrettoDefines.h
//
//////////////////////////////////////////////////////////////////////

#if !defined(LIBRETTODEFINES_H_INCLUDED)
#define LIBRETTODEFINES_H_INCLUDED


#ifndef NULL
#define NULL 0
#endif

// global identifiers
#define LIBRETTO_CURRENT_VERSION		"1.0.1.13b1"


// modes
#define LIBRETTO_MODE_DEFAULT			0x00000000  // exclusive
#define LIBRETTO_MODE_LISTENER		0x00000001  // exclusive
#define LIBRETTO_MODE_CLONE				0x00000002  // exclusive
#define LIBRETTO_MODE_QUIET				0x00000004  // ORable - means, no startup UI and no message boxes.
#define LIBRETTO_MODE_VOLATILE		0x00000008  // ORable - means, settings are not saved.  useful if operating on temporary override
#define LIBRETTO_MODE_MASK				0x0000000f  // 

// default port values.
//#define LIBRETTO_PORT_FILE				80		
#define LIBRETTO_PORT_CMD					10686		
#define LIBRETTO_PORT_STATUS			10687		

#define LIBRETTO_PORT_INVALID			0	

#define LIBRETTO_FLAG_DISABLED		0x0000	 // default
#define LIBRETTO_FLAG_ENABLED			0x0001	
#define LIBRETTO_FLAG_FOUND				0x1000
#define LIBRETTO_FLAG_NEW					0x0100
#define LIBRETTO_FLAG_REFRESH			0x0200

#define LIBRETTO_DB_MOD_MAX				0xffff  // wrapping to 1, zero excluded

// supported device types
/*
#define LIBRETTO_DESTTYPE_CHYRON_LEX					3000	// Duet LEX
#define LIBRETTO_DESTTYPE_CHYRON_HYPERX				3001  // Duet HyperX
#define LIBRETTO_DESTTYPE_CHYRON_MICROX				3002  // Duet MicroX
#define LIBRETTO_DESTTYPE_CHYRON_CHANNELBOX		3003  // Channel Box
#define LIBRETTO_DESTTYPE_CHYRON_CALBOX				3004  // CAL Box
#define LIBRETTO_DESTTYPE_CHYRON_LYRIC        3005  // Generic Lyric Box, any hardware

#define LIBRETTO_DESTTYPE_HARRIS_ICONII				5000
#define LIBRETTO_DESTTYPE_HARRIS_XML          5001    // Icon station with XML Interface
*/


// status
#define LIBRETTO_STATUS_UNINIT							0x00000000  // uninitialized	(VDS icon)
#define LIBRETTO_STATUS_UNKNOWN							0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
#define LIBRETTO_STATUS_NOTCON							0x00000010  // not connected, incomplete, not finished, etc (yellow icon)	
#define LIBRETTO_STATUS_ERROR								0x00000020  // error (red icon)
#define LIBRETTO_STATUS_CONN								0x00000030  // ready (green icon)	
#define LIBRETTO_STATUS_OK									0x00000030  // ready (green icon)	
#define LIBRETTO_STATUS_RUN									0x00000040  // in progress, running, owned etc (blue icon);	
#define LIBRETTO_ICON_MASK									0x00000070  // mask	

#define LIBRETTO_STATUS_SUSPEND							0x00000080  // suspended	(yellow icon please)

#define LIBRETTO_STATUS_CMDSVR_START				0x00001000  // starting the command server
#define LIBRETTO_STATUS_CMDSVR_RUN					0x00002000  // command server running
#define LIBRETTO_STATUS_CMDSVR_END					0x00003000  // command server shutting down
#define LIBRETTO_STATUS_CMDSVR_ERROR				0x00004000  // command server error
#define LIBRETTO_STATUS_CMDSVR_MASK					0x00007000  // command server mask bits

#define LIBRETTO_STATUS_STATUSSVR_START			0x00010000  // starting the status server
#define LIBRETTO_STATUS_STATUSSVR_RUN				0x00020000  // status server running
#define LIBRETTO_STATUS_STATUSSVR_END				0x00030000  // status server shutting down
#define LIBRETTO_STATUS_STATUSSVR_ERROR			0x00040000  // status server error
#define LIBRETTO_STATUS_STATUSSVR_MASK			0x00070000  // status server mask bits

#define LIBRETTO_STATUS_THREAD_START				0x00100000  // starting the main thread
#define LIBRETTO_STATUS_THREAD_SPARK				0x00200000  // main thread executing registered exes
#define LIBRETTO_STATUS_THREAD_RUN					0x00300000  // main thread running in work loop
#define LIBRETTO_STATUS_THREAD_END					0x00400000  // main thread shutting down
#define LIBRETTO_STATUS_THREAD_ENDED				0x00500000  // main thread ended
#define LIBRETTO_STATUS_THREAD_ERROR				0x00600000  // main thread error
#define LIBRETTO_STATUS_THREAD_MASK					0x00f00000  // main thread mask bits

// various failures...
#define LIBRETTO_STATUS_FAIL_LOG						0x10000000  // could not register log file
#define LIBRETTO_STATUS_FAIL_DB							0x20000000  // could not get DB
#define LIBRETTO_STATUS_FAIL_MASK						0xf0000000  // failure code mask bits

//return values
#define LIBRETTO_SUCCESS   0
#define LIBRETTO_ERROR	   -1


// default filenames
#define LIBRETTO_SETTINGS_FILE_SETTINGS	  "libretto.csr"		// csr = cortex settings redirect
#define LIBRETTO_SETTINGS_FILE_DEFAULT	  "libretto.csf"		// csf = cortex settings file


// commands
#define LIBRETTO_CMD_GETSCENE					0xa0 // gets scene info
#define LIBRETTO_CMD_GETTHUMB					0xa1 // gets a thumbnail
#define LIBRETTO_CMD_GETMEDIA					0xa2 // gets media list from a destination

#define LIBRETTO_CMD_QUEUE						0xc0 // puts thing in queue and waits for response
#define LIBRETTO_CMD_QUEUE_ASYNC			0xc1 // puts thing in queue
#define LIBRETTO_CMD_QUEUE_QUERY			0xc2 // retrieves answer from queue (has deletion option)
#define LIBRETTO_CMD_CMDQUEUE					0xc5 // puts thing in command queue and waits for response
#define LIBRETTO_CMD_CMDQUEUE_ASYNC		0xc6 // puts thing in command queue and returns immediately
#define LIBRETTO_CMD_CMDQUEUE_QUERY		0xc7 // retrieves answer from queue (has deletion option)
#define LIBRETTO_CMD_DIRECT						0xcf // skips queues, commands box directly

#define LIBRETTO_CMD_EXSET							0xea // sets exchange counter to a specific value
#define LIBRETTO_CMD_EXINC							0xeb // increments exchange counter
#define LIBRETTO_CMD_EXGET							0xec // gets exchange mod value
#define LIBRETTO_CMD_MODSET							0xed // sets exchange mod value, skips exchange table
#define LIBRETTO_CMD_MODINC							0xee // increments exchange mod value, skips exchange table

// queue modes
#define LIBRETTO_QUEUEMODE_MEM				0
#define LIBRETTO_QUEUEMODE_DB					1


// debug defines
#define LIBRETTO_DEBUG_QUEUE				0x00000001
#define LIBRETTO_DEBUG_DEVCOMM			0x00000002
#define LIBRETTO_DEBUG_SQL					0x00000004
#define LIBRETTO_DEBUG_CMDQUEUE			0x00000008
#define LIBRETTO_DEBUG_CMDHANDLER		0x00000010
#define LIBRETTO_DEBUG_TIMING				0x00000020
#define LIBRETTO_DEBUG_COMM					0x00000200
#define LIBRETTO_DEBUG_MODS					0x00000400
#define LIBRETTO_DEBUG_CMDECHO			0x00000800


#endif // !defined(LIBRETTODEFINES_H_INCLUDED)
