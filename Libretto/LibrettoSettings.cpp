// LibrettoSettings.cpp: implementation of the CLibrettoSettings.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "LibrettoDefines.h"
#include "LibrettoSettings.h"
#include "LibrettoMain.h" 
#include <process.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

extern CLibrettoMain* g_plibretto;
extern CLibrettoApp theApp;

extern void TimeCodeDataThread(void* pvArgs);


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLibrettoSettings::CLibrettoSettings()
{
	m_pdb = NULL;
	m_pdbConn = NULL;

	m_pszName = NULL;
	m_pszType = NULL;  // familiar name of the type.
	m_pszProject = NULL;  // familiar name of the project name.
	m_ulMainMode = LIBRETTO_MODE_DEFAULT;

	// ports
	m_usCommandPort	= LIBRETTO_PORT_CMD;
	m_usStatusPort	= LIBRETTO_PORT_STATUS;

	m_nFilenameLocalWidth=2048;
	m_nFilenameRemoteWidth=256;
	m_nMessageWidth=512;

	m_nPort = 23; //telnet
	m_nDefaultSendTimeoutMS=5000;
	m_nDefaultReceiveTimeoutMS=5000;
	m_pszTypePortOverride = NULL;  // format: type_decimal:port|type_decimal:port etc, like 3003:1337|5000:9091

	m_nAutoPurgeMessageDays = 30; // default
//	m_nAutoPurgeAsRunDays = 30; // default
	m_nAutoPurgeInterval = 300;  // 5 minutes should be enough.

	m_nThreadDwellMS = 1000;
	m_bDebugLists= false;  // if true, dumps the temp and events databases
	m_nDebugListTopCount=-1;  //if positive, just prints out the top n recored in the events tables.
	m_bDebugSQL=false;  // if true, prints out the SQL calls
	m_bDebugInsertSQL=false;  // if true, prints out the event insert SQL calls
	m_ulDebug = 0;
	m_pszDebugOrder = NULL;

	m_bEnableQueue = true;
	m_bEnableCommandQueue = true;
	m_nQueueIntervalMS=3000;
	m_nCommandQueueIntervalMS=-1;
	m_nMemQueueIncrSize = LIBRETTO_MEMQUEUE_SIZEINCR;

	m_nClearQueueIfDisconnectedMS=50; // if <0 does not clear.  if immediate clear desired, use 0.

	// messaging for Libretto
	m_bUseLog = true;			// write a log file
	m_bUseEmail = false;		// send an email on failures (or commanded from remotes)
	m_bUseNetwork = false; // send a message to a remote host
	m_bReportSuccessfulOperation=false;
	m_bLogTransfers=false;
	m_pszFileSpec = NULL;
	m_pszMailSpec = NULL;
	m_pszProcessedFileSpec = NULL;
	m_pszProcessedMailSpec = NULL;
	m_bUseXMLClientLog = false;
	m_bMillisecondMessaging=true;			// use millisecond resolution for messages and asrun

	m_bDiReCTInstalled = false;
	// Harris API
//	m_bUseListCount = false; // get all events up until the list count (otherwise just up to the lookahead)

	// DSN params
	m_pszDSN = NULL;
	m_pszUser = NULL;
	m_pszPW = NULL;

	m_pszSettings = NULL;  // the Settings table name
	m_pszExchange = NULL;  // the Exchange table name
	m_pszMessages = NULL;  // the Messages table name

	m_pszDestinationMediaTemp = NULL;
	m_pszDestinationMedia = NULL;  // the DestinationMedia table name
	m_pszDestinations = NULL;  // the Destinations table name
	m_pszQueue = NULL; // the Queue table name
	m_pszCommandQueue = NULL;  // the CommandQueue table name

	m_ulModsIntervalMS = 1000;

	m_bUseUTF8= false; // use UTF8, not ISO
	m_pszDelim=NULL;
	m_pszDownloadedThumbnailPath = NULL;
	m_pszDownloadedThumbnailExtension = NULL;
	
	InitializeCriticalSection(&m_crit);

	m_bUseTimeCode = false;
	m_nCommandRepetitions = 0;
	m_bTimeCodeSync = false;  // jam sync the clock
	m_nSyncThresholdMS = 50;  // sync if evaluates to outside this threshold
	m_nSyncIntervalMS  = 33;   // evaluate sync difference at this interval
	m_nFrameRate=30;   // for time code addresses NTSC=30, PAL=25
	m_bDF=true;  // Drop frame


	m_pszLicense=NULL;  // the License Key
	m_pszIconPath=NULL;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.
}

CLibrettoSettings::~CLibrettoSettings()
{
	if(m_pszName) free(m_pszName); // must use malloc to allocate
	if(m_pszType) free(m_pszType); // must use malloc to allocate
	if(m_pszProject) free(m_pszProject); // must use malloc to allocate
	if(m_pszDSN) free(m_pszDSN); // must use malloc to allocate
	if(m_pszUser) free(m_pszUser); // must use malloc to allocate
	if(m_pszPW) free(m_pszPW); // must use malloc to allocate
	if(m_pszSettings) free(m_pszSettings); // must use malloc to allocate
	if(m_pszExchange) free(m_pszExchange); // must use malloc to allocate
	if(m_pszMessages) free(m_pszMessages); // must use malloc to allocate
	if(m_pszFileSpec) free(m_pszFileSpec); // must use malloc to allocate	
	if(m_pszMailSpec) free(m_pszMailSpec); // must use malloc to allocate	
	if(m_pszProcessedFileSpec) free(m_pszProcessedFileSpec); // must use malloc to allocate	
	if(m_pszProcessedMailSpec) free(m_pszProcessedMailSpec); // must use malloc to allocate	

	if(m_pszTypePortOverride) free(m_pszTypePortOverride); // must use malloc to allocate

	if(m_pszDestinationMediaTemp) free(m_pszDestinationMediaTemp); // must use malloc to allocate
	if(m_pszDestinationMedia) free(m_pszDestinationMedia); // must use malloc to allocate
	if(m_pszDestinations) free(m_pszDestinations); // must use malloc to allocate
	if(m_pszQueue) free(m_pszQueue); // must use malloc to allocate
	if(m_pszCommandQueue) free(m_pszCommandQueue); // must use malloc to allocate
	if(m_pszLicense) free(m_pszLicense); // must use malloc to allocate
	if(m_pszIconPath) free(m_pszIconPath); // must use malloc to allocate	
	if(m_pszDebugOrder) free(m_pszDebugOrder); // must use malloc to allocate
	if(m_pszDelim) free(m_pszDelim); // must use malloc to allocate
	if(m_pszDownloadedThumbnailPath) free(m_pszDownloadedThumbnailPath); // must use malloc to allocate
	if(m_pszDownloadedThumbnailExtension) free(m_pszDownloadedThumbnailExtension); // must use malloc to allocate

	DeleteCriticalSection(&m_crit);
}

int CLibrettoSettings::GetPortOverride(int nType)
{
	if(nType>0)
	{
		char pszLookup[MAX_PATH];
		sprintf(pszLookup, "%d:", nType);

// format: type_decimal:port|type_decimal:port etc, like 3003:1337|5000:9091
	
		EnterCriticalSection(&m_crit);
		if((m_pszTypePortOverride)&&(strlen(m_pszTypePortOverride)>2))
		{
			int nPort = LIBRETTO_ERROR;
			char* pch = strstr(m_pszTypePortOverride, pszLookup);
			if(pch)
			{ 
				if(
					  (pch == m_pszTypePortOverride)
					||(
							(pch>m_pszTypePortOverride)
						&&(*(pch-1)=='|')
						)
					) //properly delimited
				{
					pch+=strlen(pszLookup);  
					//0123456789
					//3003:1337
					nPort = atoi(pch);
					if((nPort<=0)||(nPort>65535)) nPort = LIBRETTO_ERROR;
				}
			}
			LeaveCriticalSection(&m_crit);

			return nPort;
		}
			
		LeaveCriticalSection(&m_crit);
	}
	return LIBRETTO_ERROR;
}

int CLibrettoSettings::Settings(bool bRead)
{
	char pszFilename[MAX_PATH];
	CFileUtil file;
	// get settings.
	strcpy(pszFilename, "");
//		AfxMessageBox("4");
	char* pchF=theApp.GetSettingsFilename();
	if(pchF) {	strcpy(pszFilename, pchF); free(pchF); }
	if(strlen(pszFilename)<=0)  strcpy(pszFilename, LIBRETTO_SETTINGS_FILE_DEFAULT);  // cortex settings file

	file.GetSettings(pszFilename, false); 

	if(file.m_ulStatus&FILEUTIL_MALLOC_OK)
	{
		if(bRead)
		{
		// load up the values on the settings object
			m_pszName = file.GetIniString("Main", "Name", "Libretto");
			m_pszType = file.GetIniString("Main", "Type", "Libretto", m_pszType);
			m_pszProject = file.GetIniString("Main", "Project", "VDS", m_pszProject);
			m_pszLicense = file.GetIniString("License", "Key", "invalid_key", m_pszLicense);

			m_nThreadDwellMS = file.GetIniInt("Main", "ThreadDwellMS", 1000);

			m_bDebugLists = file.GetIniInt("Database", "DebugLists", 0)?true:false;  // if true, dumps the temp and events databases
			m_nDebugListTopCount = file.GetIniInt("Database", "DebugListTopCount", -1);  //if positive, just prints out the top n recored in the events tables.
			m_bDebugSQL = file.GetIniInt("Database", "DebugSQL", 0)?true:false;  // if true, prints out the SQL calls
			m_bDebugInsertSQL = file.GetIniInt("Database", "DebugInsertSQL", 0)?true:false;  // if true, prints out the event insert SQL calls
			m_ulDebug = file.GetIniInt("Database", "Debug", 0);  // prints out debug statements that & with this.
			m_pszDebugOrder = file.GetIniString("Database", "DebugOrder", "(case when parent_position < 0 then ((event_position)/1000.0) else (parent_calc_start + (event_position-parent_position)/1000.0) end)", m_pszDebugOrder); // the order by clause for debug

			// recompile license key params
			if(g_plibretto->m_data.m_key.m_pszLicenseString) free(g_plibretto->m_data.m_key.m_pszLicenseString);
			g_plibretto->m_data.m_key.m_pszLicenseString = (char*)malloc(strlen(m_pszLicense)+1);
			if(g_plibretto->m_data.m_key.m_pszLicenseString)
			sprintf(g_plibretto->m_data.m_key.m_pszLicenseString, "%s", m_pszLicense);

			g_plibretto->m_data.m_key.InterpretKey();

			char errorstring[MAX_MESSAGE_LENGTH];
			if(g_plibretto->m_data.m_key.m_bValid)
			{
				unsigned long i=0;
				while(i<g_plibretto->m_data.m_key.m_ulNumParams)
				{
					if((g_plibretto->m_data.m_key.m_ppszParams)
						&&(g_plibretto->m_data.m_key.m_ppszValues)
						&&(g_plibretto->m_data.m_key.m_ppszParams[i])
						&&(g_plibretto->m_data.m_key.m_ppszValues[i]))
					{
						if(stricmp(g_plibretto->m_data.m_key.m_ppszParams[i], "max")==0)
						{
							g_plibretto->m_data.m_nMaxLicensedDevices = atoi(g_plibretto->m_data.m_key.m_ppszValues[i]);
						}
					}
					i++;
				}
			
				if(
						(
							(!g_plibretto->m_data.m_key.m_bExpires)
						||((g_plibretto->m_data.m_key.m_bExpires)&&(!g_plibretto->m_data.m_key.m_bExpired))
						||((g_plibretto->m_data.m_key.m_bExpires)&&(g_plibretto->m_data.m_key.m_bExpireForgiveness)&&(g_plibretto->m_data.m_key.m_ulExpiryDate+g_plibretto->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
						)
					&&(
							(!g_plibretto->m_data.m_key.m_bMachineSpecific)
						||((g_plibretto->m_data.m_key.m_bMachineSpecific)&&(g_plibretto->m_data.m_key.m_bValidMAC))
						)
					)
				{
					// this overrides stuff so let's not let it do that.

										// let's just not override this in a green condition.  
										// comment it out.
//					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//					g_plibretto->m_data.SetStatusText(errorstring, LIBRETTO_STATUS_OK);
				}
				else
				{
					_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
					g_plibretto->m_data.SetStatusText(errorstring, LIBRETTO_STATUS_ERROR);
				}
			}
			else
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
				g_plibretto->m_data.SetStatusText(errorstring, LIBRETTO_STATUS_ERROR);
			}

			m_usCommandPort = file.GetIniInt("CommandServer", "ListenPort", LIBRETTO_PORT_CMD);
			m_usStatusPort = file.GetIniInt("StatusServer", "ListenPort", LIBRETTO_PORT_STATUS);

			m_pszIconPath = file.GetIniString("FileServer", "IconPath", "C:\\Inetpub\\wwwroot\\Cortex\\libretto\\images\\", m_pszIconPath);

			m_bUseEmail = file.GetIniInt("Messager", "UseEmail", 0)?true:false;
			m_bUseNetwork = file.GetIniInt("Messager", "UseNet", 0)?true:false;
			m_bUseLog = file.GetIniInt("Messager", "UseLog", 1)?true:false;
			m_bReportSuccessfulOperation = file.GetIniInt("Messager", "ReportSuccessfulOperation", 0)?true:false;
			m_bLogTransfers = file.GetIniInt("Messager", "LogTransfers", 0)?true:false;
			m_bUseXMLClientLog = file.GetIniInt("Messager", "UseXMLClientLog", 0)?true:false;
			m_bMillisecondMessaging = file.GetIniInt("Messager", "MillisecondMessaging", 1)?true:false;			// use millisecond resolution for messages and asrun

			m_nAutoPurgeMessageDays = file.GetIniInt("AutoPurge", "AutoPurgeMessageDays", 30);
	//		m_nAutoPurgeAsRunDays = file.GetIniInt("AutoPurge", "AutoPurgeAsRunDays", 30);
			m_nAutoPurgeInterval  = file.GetIniInt("AutoPurge", "AutoPurgeInterval", 300);  // 5 minutes should be enough.



			m_pszDSN = file.GetIniString("Database", "DSN", (m_pszName?m_pszName:"Libretto"), m_pszDSN);
			m_pszUser = file.GetIniString("Database", "DBUser", "sa", m_pszUser);
			m_pszPW = file.GetIniString("Database", "DBPassword", "", m_pszPW);
			m_pszSettings = file.GetIniString("Database", "SettingsTableName", "Settings", m_pszSettings);  // the Settings table name
			m_pszExchange = file.GetIniString("Database", "ExchangeTableName", "Exchange", m_pszExchange);  // the Exchange table name
			m_pszMessages = file.GetIniString("Database", "MessagesTableName", "Messages", m_pszMessages);  // the Messages table name

			m_pszDestinationMediaTemp = file.GetIniString("Database", "DestinationMediaTempTableName", "Destinations_Media_Temp", m_pszDestinationMediaTemp);  // the Destinations table name
			m_pszDestinationMedia = file.GetIniString("Database", "DestinationMediaTableName", "Destinations_Media", m_pszDestinationMedia);  // the Destinations table name
			m_pszDestinations = file.GetIniString("Database", "DestinationsTableName", "Destinations", m_pszDestinations);  // the Destinations table name
			m_pszQueue = file.GetIniString("Database", "QueueTableName", "Queue", m_pszQueue);  // the Queue table name
			m_pszCommandQueue = file.GetIniString("Database", "CommandQueueTableName", "Command_Queue", m_pszCommandQueue);  // the CommandQueue table name

			m_ulModsIntervalMS = file.GetIniInt("Database", "ModificationCheckInterval", 5000);  // in milliseconds

			m_bEnableQueue = file.GetIniInt("Database", "EnableQueue", 1)?true:false;
			m_bEnableCommandQueue = file.GetIniInt("Database", "EnableCommandQueue", 1)?true:false;
			m_nQueueIntervalMS = file.GetIniInt("Database", "QueueIntervalMS", 3000);  // in milliseconds
			m_nCommandQueueIntervalMS = file.GetIniInt("Database", "CommandQueueIntervalMS", -1);  // in milliseconds

			m_nFilenameLocalWidth=file.GetIniInt("Database", "QueueLocalWidth", 2048);
			m_nFilenameRemoteWidth=file.GetIniInt("Database", "QueueRemoteWidth", 256);
			m_nMessageWidth=file.GetIniInt("Database", "QueueMessageWidth", 512);

			m_nPort	=		file.GetIniInt("Endpoint", "TelnetPort", 23);  
			m_nClearQueueIfDisconnectedMS	=		file.GetIniInt("Endpoint", "ClearQueueIfDisconnectedMS", 50); // if <0 does not clear.  if immediate clear desired, use 0.
			m_nDefaultSendTimeoutMS	=		file.GetIniInt("Endpoint", "DefaultSendTimeoutMS", 5000); 
			m_nDefaultReceiveTimeoutMS	=		file.GetIniInt("Endpoint", "DefaultReceiveTimeoutMS", 5000);
			m_nMemQueueIncrSize = file.GetIniInt("Endpoint", "MemoryQueueIncrementSize", LIBRETTO_MEMQUEUE_SIZEINCR); 

			EnterCriticalSection(&m_crit);
			m_pszTypePortOverride = file.GetIniString("Endpoint", "TypePortOverride", "", m_pszTypePortOverride);  // format: type_decimal:port|type_decimal:port etc, like 3003:1337|5000:9091
			LeaveCriticalSection(&m_crit);

			m_bUseUTF8 = file.GetIniInt("Endpoint", "UseUTF8", 0)?true:false; // use UTF8, not ISO
			m_pszDownloadedThumbnailPath = file.GetIniString("FileServer", "ThumbnailPath", "C:\\Inetpub\\wwwroot\\Cortex\\Libretto\\images\\thumbnails\\", m_pszDownloadedThumbnailPath);
			m_pszDownloadedThumbnailExtension = file.GetIniString("FileServer", "ThumbnailExtension", "jpg", m_pszDownloadedThumbnailExtension);
//	g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "settings: [%s][%s].", m_pszDelim, m_pszDownloadedThumbnailPath);  //(Dispatch message)

			m_pszDelim = NULL;//file.GetIniString("Endpoint", "Delimiter", "�", m_pszDelim);  weird problem here

	
			m_nCommandRepetitions = file.GetIniInt("Endpoint", "CommandRepetitions", 0); // repeat commands. 

			m_bUseTimeCode = file.GetIniInt("TimeCode", "UseTCRDR", 0)?true:false; // use Adrienne Time code card 
			m_bTimeCodeSync = file.GetIniInt("TimeCode", "JamSync", 0)?true:false;  // jam sync the clock
			m_nSyncThresholdMS = file.GetIniInt("TimeCode", "SyncThresholdMS", 50);  // sync if evaluates to outside this threshold
			m_nSyncIntervalMS = file.GetIniInt("TimeCode", "SyncIntervalMS", 33);   // evaluate sync difference at this interval
			m_nFrameRate = file.GetIniInt("TimeCode", "FrameRate", 30);   // for time code addresses NTSC=30, PAL=25
			m_bDF = file.GetIniInt("TimeCode", "DropFrame", 1)?true:false;   // Drop frame


			if(m_bUseTimeCode)
			{
				// connect to the time code card if nec
				if((g_plibretto)&&(!g_plibretto->m_data.m_bTimeCodeThreadStarted))
				{
					g_plibretto->m_data.m_bTimeCodeThreadKill = false;

					if(_beginthread(TimeCodeDataThread, 0, (void*)(NULL))==-1)
					{
					//error.
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Error starting time code thread");//   Sleep(250);//(Dispatch message)
					//**MSG
					}
					else Sleep(250); // let it start...
				}
				
			}
			else
			{
				// disconnect from the time code card if nec
				if((g_plibretto)&&(g_plibretto->m_data.m_bTimeCodeThreadStarted))
				{
					g_plibretto->m_data.m_bTimeCodeThreadKill = true;
				}

			}




			m_pszFileSpec = file.GetIniString("Messager", "LogFileIni", "Logs\\Libretto|YD||1|", m_pszFileSpec);
				// SMTP address[: override port (optional)] | distribution lists | from email |  subject spec | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
			m_pszMailSpec = file.GetIniString("Messager", "SMTPIni", "mail.server.com|f3:recipient@address.com|\"someone\"<from@here.com>|%P %T from %h|%h|C:\\librettosmtp.txt|1|1|0", m_pszMailSpec);
			if(m_pszFileSpec) 
			{
				if(m_pszProcessedFileSpec)
				{
					try{ free(m_pszProcessedFileSpec); } catch(...) {}
				}
				m_pszProcessedFileSpec = ProcessString(m_pszFileSpec, false);
			}
			
			if(m_pszMailSpec) 
			{
				if(m_pszProcessedMailSpec)
				{
					try{ free(m_pszProcessedMailSpec); } catch(...) {}
				}
				m_pszProcessedMailSpec = ProcessString(m_pszMailSpec, false);
			}

		}
		else //write
		{
			// these explicts arent necessary - uncomment to write out a full file to edit...
			file.SetIniString("Main", "Name", m_pszName);
			file.SetIniString("Main", "Type", m_pszType);
			file.SetIniString("Main", "Project", m_pszProject);
			file.SetIniString("License", "Key", m_pszLicense);
			file.SetIniInt("Main", "ThreadDwellMS", m_nThreadDwellMS);

			file.SetIniInt("Database", "DebugLists", m_bDebugLists?1:0);  // if true, dumps the temp and events databases
			file.SetIniInt("Database", "DebugListTopCount", m_nDebugListTopCount);  //if positive, just prints out the top n recored in the events tables.
			file.SetIniInt("Database", "DebugSQL", m_bDebugSQL?1:0);  // if true, prints out the SQL calls
			file.SetIniInt("Database", "DebugInsertSQL", m_bDebugInsertSQL?1:0);  // if true, prints out the event insert SQL calls
			file.SetIniInt("Database", "Debug", m_ulDebug);  // prints out debug statements that & with this.
			file.SetIniString("Database", "DebugOrder", m_pszDebugOrder); // the order by clause for debug

			file.SetIniInt("CommandServer", "ListenPort", m_usCommandPort);
			file.SetIniInt("StatusServer", "ListenPort", m_usStatusPort);
			file.SetIniString("FileServer", "IconPath", m_pszIconPath);

			file.SetIniInt("Messager", "UseEmail", m_bUseEmail?1:0);
			file.SetIniInt("Messager", "UseNet", m_bUseNetwork?1:0);
			file.SetIniInt("Messager", "UseLog", m_bUseLog?1:0);
			file.SetIniInt("Messager", "ReportSuccessfulOperation", m_bReportSuccessfulOperation?1:0);
			file.SetIniInt("Messager", "LogTransfers", m_bLogTransfers?1:0);
			file.SetIniInt("Messager", "UseXMLClientLog", m_bUseXMLClientLog?true:false);
			file.SetIniInt("Messager", "MillisecondMessaging", m_bMillisecondMessaging?true:false);		// use millisecond resolution for messages and asrun

			file.SetIniString("Database", "DSN", m_pszDSN);
			file.SetIniString("Database", "DBUser", m_pszUser);
			file.SetIniString("Database", "DBPassword", m_pszPW);
			file.SetIniString("Database", "SettingsTableName", m_pszSettings);  // the Settings table name
			file.SetIniString("Database", "ExchangeTableName", m_pszExchange);  // the Exchange table name
			file.SetIniString("Database", "MessagesTableName", m_pszMessages);  // the Messages table name

	//		file.SetIniInt("HarrisAPI", "UseListCount", m_bUseListCount?1:0); // get all events up until the list count (otherwise just up to the lookahead)
			file.SetIniString("Database", "DestinationsTableName", m_pszDestinations);  // the Destinations table name
			file.SetIniString("Database", "QueueTableName", m_pszQueue);  // the Queue table name
			file.SetIniString("Database", "CommandQueueTableName", m_pszCommandQueue);  // the CommandQueue table name
			file.SetIniString("Database", "DestinationMediaTableName", m_pszDestinationMedia);  // the DestinationMedia table name
			file.SetIniString("Database", "DestinationMediaTempTableName", m_pszDestinationMediaTemp );  // the Destinations table name

			file.SetIniInt("Database", "QueueIntervalMS", m_nQueueIntervalMS);  // in milliseconds
			file.SetIniInt("Database", "CommandQueueIntervalMS", m_nCommandQueueIntervalMS);  // in milliseconds
			file.SetIniInt("Database", "EnableQueue", m_bEnableQueue?1:0);
			file.SetIniInt("Database", "EnableCommandQueue", m_bEnableCommandQueue?1:0);
			file.SetIniInt("Database", "QueueLocalWidth", m_nFilenameLocalWidth);
			file.SetIniInt("Database", "QueueRemoteWidth", m_nFilenameRemoteWidth);
			file.SetIniInt("Database", "QueueMessageWidth", m_nMessageWidth);

			file.SetIniInt("Database", "ModificationCheckInterval", m_ulModsIntervalMS);  // in milliseconds
			file.SetIniInt("Endpoint", "TelnetPort", m_nPort);  
			file.SetIniInt("Endpoint", "ClearQueueIfDisconnectedMS", m_nClearQueueIfDisconnectedMS); // if <0 does not clear.  if immediate clear desired, use 0.
			EnterCriticalSection(&m_crit);
			file.SetIniString("Endpoint", "TypePortOverride", m_pszTypePortOverride);  // format: type_decimal:port|type_decimal:port etc, like 3003:1337|5000:9091
			LeaveCriticalSection(&m_crit);
			file.SetIniInt("Endpoint", "DefaultSendTimeoutMS", m_nDefaultSendTimeoutMS); 
			file.SetIniInt("Endpoint", "DefaultReceiveTimeoutMS", m_nDefaultReceiveTimeoutMS);
			file.SetIniInt("Endpoint", "MemoryQueueIncrementSize", m_nMemQueueIncrSize); 
			file.SetIniInt("Endpoint", "UseUTF8", m_bUseUTF8?1:0); // use UTF8, not ISO
			file.SetIniString("FileServer", "ThumbnailPath", m_pszDownloadedThumbnailPath);//, "must include trailing backslash");  // WAS THE COMMENT THE PROBLEM?
			file.SetIniString("FileServer", "ThumbnailExtension", m_pszDownloadedThumbnailExtension);

			
			file.SetIniInt("TimeCode", "JamSync", m_bTimeCodeSync?1:0, "jam sync the clock");  // jam sync the clock
			file.SetIniInt("TimeCode", "UseTCRDR", m_bUseTimeCode?1:0, "use Adrienne Time code card"); // use Adrienne Time code card 
			file.SetIniInt("TimeCode", "SyncThresholdMS", m_nSyncThresholdMS, "sync if evaluates to outside this threshold");  // sync if evaluates to outside this threshold
			file.SetIniInt("TimeCode", "SyncIntervalMS", m_nSyncIntervalMS, "evaluate sync difference at this interval");   // evaluate sync difference at this interval
			file.SetIniInt("TimeCode", "FrameRate", m_nFrameRate, "for time code addresses NTSC=30, PAL=25, used only if no TC card available" );   // for time code addresses NTSC=30, PAL=25
			file.SetIniInt("TimeCode", "DropFrame", m_bDF?1:0, "drop frame mode, used only if no TC card available");    // Drop frame

			
			file.SetIniInt("Endpoint", "CommandRepetitions", m_nCommandRepetitions); // repeat commands. 

			// file.SetIniString("Endpoint", "Delimiter", m_pszDelim);  // DONT SAVE UNTIL i FIGURE OUT THAT ISSUE...
			file.SetIniInt("AutoPurge", "AutoPurgeMessageDays", m_nAutoPurgeMessageDays);
//			file.SetIniInt("AutoPurge", "AutoPurgeAsRunDays", m_nAutoPurgeAsRunDays);
			file.SetIniInt("AutoPurge", "AutoPurgeInterval", m_nAutoPurgeInterval);  // 5 minutes should be enough.


			file.SetIniString("Messager", "LogFileIni", m_pszFileSpec);
			file.SetIniString("Messager", "SMTPIni", m_pszMailSpec);

			file.SetSettings(pszFilename, false);  // have to have correct filename

		}
		return LIBRETTO_SUCCESS;
	}
	return LIBRETTO_ERROR;
}



int CLibrettoSettings::GetFromDatabase(char* pszInfo)  //only get.  Interface publishes
{
	if((m_pszSettings)&&(strlen(m_pszSettings)>0)&&(m_pdbConn)&&(m_pdb)&&(m_pdbConn->m_bConnected)&&(Settings(true)==LIBRETTO_SUCCESS))  //read has to succeed
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", m_pszSettings);
		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = LIBRETTO_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
				CString szCategory="";
				CString szParameter="";
				CString szValue="";
				CString szTemp="";
				int min, max;
				bool bmin = false, bmax = false;
				try
				{
					prs->GetFieldValue("category", szCategory);  //HARDCODE
					prs->GetFieldValue("parameter", szParameter);  //HARDCODE
					prs->GetFieldValue("value", szValue);  //HARDCODE
					prs->GetFieldValue("min_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						min = atoi(szTemp);
						bmin = true;
					}
					prs->GetFieldValue("max_value", szTemp);  //HARDCODE
					if(szTemp.GetLength())
					{
						max = atoi(szTemp);
						bmax = true;
					}
				}
				catch( ... )
				{
				}

				int nLength = szValue.GetLength();
				if(szCategory.CompareNoCase("Main")==0)
				{
					if(szParameter.CompareNoCase("Name")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszName)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszName) free(m_pszName);
								m_pszName = pch;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("License")==0)
				{
					if(szParameter.CompareNoCase("Key")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszLicense)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszLicense) free(m_pszLicense);
								m_pszLicense = pch;

								// recompile license key params
								if(g_plibretto->m_data.m_key.m_pszLicenseString) free(g_plibretto->m_data.m_key.m_pszLicenseString);
								g_plibretto->m_data.m_key.m_pszLicenseString = (char*)malloc(nLength+1);
								if(g_plibretto->m_data.m_key.m_pszLicenseString)
								sprintf(g_plibretto->m_data.m_key.m_pszLicenseString, "%s", szValue);

								g_plibretto->m_data.m_key.InterpretKey();

								char errorstring[MAX_MESSAGE_LENGTH];
								if(g_plibretto->m_data.m_key.m_bValid)
								{
									unsigned long i=0;
									while(i<g_plibretto->m_data.m_key.m_ulNumParams)
									{
										if((g_plibretto->m_data.m_key.m_ppszParams)
											&&(g_plibretto->m_data.m_key.m_ppszValues)
											&&(g_plibretto->m_data.m_key.m_ppszParams[i])
											&&(g_plibretto->m_data.m_key.m_ppszValues[i]))
										{
											if(stricmp(g_plibretto->m_data.m_key.m_ppszParams[i], "max")==0)
											{
												g_plibretto->m_data.m_nMaxLicensedDevices = atoi(g_plibretto->m_data.m_key.m_ppszValues[i]);
											}
										}
										i++;
									}
								
									if(
											(
												(!g_plibretto->m_data.m_key.m_bExpires)
											||((g_plibretto->m_data.m_key.m_bExpires)&&(!g_plibretto->m_data.m_key.m_bExpired))
											||((g_plibretto->m_data.m_key.m_bExpires)&&(g_plibretto->m_data.m_key.m_bExpireForgiveness)&&(g_plibretto->m_data.m_key.m_ulExpiryDate+g_plibretto->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
											)
										&&(
												(!g_plibretto->m_data.m_key.m_bMachineSpecific)
											||((g_plibretto->m_data.m_key.m_bMachineSpecific)&&(g_plibretto->m_data.m_key.m_bValidMAC))
											)
										)
									{
	
										// let's just not override this in a green condition.  
										// comment it out.
//										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Valid license");
//										g_plibretto->m_data.SetStatusText(errorstring, LIBRETTO_STATUS_OK);
									}
									else
									{
										_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
										g_plibretto->m_data.SetStatusText(errorstring, LIBRETTO_STATUS_ERROR);
									}
								}
								else
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Invalid license");
									g_plibretto->m_data.SetStatusText(errorstring, LIBRETTO_STATUS_ERROR);
								}

							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("CommandServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usCommandPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("StatusServer")==0)
				{
					if(szParameter.CompareNoCase("ListenPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>=(bmin?min:0))&&(nLength<=(bmax?max:65535)))
							{
								m_usStatusPort = nLength;
							}
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Messager")==0)
				{
					if(szParameter.CompareNoCase("UseEmail")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseEmail = true;
							else m_bUseEmail = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseNet")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseNetwork = true;
							else m_bUseNetwork = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseLog = true;
							else m_bUseLog = false;
						}
					}
					else
					if(szParameter.CompareNoCase("UseXMLClientLog")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseXMLClientLog = true;
							else m_bUseXMLClientLog = false;
						}
					}

				}
/*
				else
				if(szCategory.CompareNoCase("HarrisAPI")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("UseListCount")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_bUseListCount = true;
							else m_bUseListCount = false;
						}
					}
				}
*/
				else
				if(szCategory.CompareNoCase("Database")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("SettingsTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszSettings)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszSettings) free(m_pszSettings);
								m_pszSettings = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ExchangeTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszExchange)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszExchange) free(m_pszExchange);
								m_pszExchange = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("MessagesTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszMessages)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszMessages) free(m_pszMessages);
								m_pszMessages = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("DestinationMediaTempTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszDestinationMediaTemp)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszDestinationMediaTemp) free(m_pszDestinationMediaTemp);
								m_pszDestinationMediaTemp = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("DestinationMediaTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszDestinationMedia)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszDestinationMedia) free(m_pszDestinationMedia);
								m_pszDestinationMedia = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("DestinationsTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszDestinations)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszDestinations) free(m_pszDestinations);
								m_pszDestinations = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("QueueTableName")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszQueue)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszQueue) free(m_pszQueue);
								m_pszQueue = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("ModificationCheckInterval")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0) m_ulModsIntervalMS = nLength;
						}
					}
				}
				else
				if(szCategory.CompareNoCase("Endpoint")==0)
				{
					// we are not going to allow DSN params to change via the DB.
					if(szParameter.CompareNoCase("TelnetPort")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if((nLength>0)&&(nLength<65535)) m_nPort = nLength;
						}
					}
					else
					if(szParameter.CompareNoCase("ClearQueueIfDisconnectedMS")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							m_nClearQueueIfDisconnectedMS = nLength;
						}
					}
					else
					if(szParameter.CompareNoCase("MemoryQueueIncrementSize")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							
							m_nMemQueueIncrSize = max(LIBRETTO_MEMQUEUE_SIZEINCR,nLength);  // make sure there is a minimum.
						}
					}
					else
					if(szParameter.CompareNoCase("CommandRepetitions")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							
							m_nCommandRepetitions = nLength; 
						}
					}


				}

				else
				if(szCategory.CompareNoCase("Graphics")==0)
				{
					if(szParameter.CompareNoCase("Image_File_Extension")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszDownloadedThumbnailExtension)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszDownloadedThumbnailExtension) free(m_pszDownloadedThumbnailExtension);
								m_pszDownloadedThumbnailExtension = pch;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("Image_Path")==0)
					{
						if((nLength>0)&&(szValue.Compare(m_pszDownloadedThumbnailPath)))
						{
							char* pch = (char*)malloc(nLength+1);
							if(pch)
							{
								sprintf(pch, "%s", szValue);
								if(m_pszDownloadedThumbnailPath) free(m_pszDownloadedThumbnailPath);
								m_pszDownloadedThumbnailPath = pch;
							}
						}
					}


				}
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
					if(szParameter.CompareNoCase("Messages")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeMessageDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
				}
				else
				if(szCategory.CompareNoCase("TimeCode")==0)
				{
					if(szParameter.CompareNoCase("UseTCRDR")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength)
							{
								m_bUseTimeCode = true;

								// connect to the time code card if nec
								if((g_plibretto)&&(!g_plibretto->m_data.m_bTimeCodeThreadStarted))
								{
									g_plibretto->m_data.m_bTimeCodeThreadKill = false;

									if(_beginthread(TimeCodeDataThread, 0, (void*)(NULL))==-1)
									{
									//error.
		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Error starting time code thread");//   Sleep(250);//(Dispatch message)
									//**MSG
									}
								}
								
							}
							else
							{
								m_bUseTimeCode = false;

								// disconnect from the time code card if nec
								if((g_plibretto)&&(g_plibretto->m_data.m_bTimeCodeThreadStarted))
								{
									g_plibretto->m_data.m_bTimeCodeThreadKill = true;
								}

							}
						}
					}
					else
					if(szParameter.CompareNoCase("JamSync")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength)
							{
								m_bTimeCodeSync = true;
							}
							else
							{
								m_bTimeCodeSync = false;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("SyncThresholdMS")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0)
							{
								m_nSyncThresholdMS = nLength;
							}
						}
					}
					else
					if(szParameter.CompareNoCase("SyncIntervalMS")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength>0)
							{
								m_nSyncIntervalMS = nLength;
							}
						}
					}

				}


/*
				else
				if(szCategory.CompareNoCase("auto_purge")==0)
				{
					if(szParameter.CompareNoCase("AsRun_Log")==0)
					{
						if(nLength>0)
						{
							nLength = atoi(szValue);
							if(nLength) m_nAutoPurgeAsRunDays = nLength; // let's make zero meaningless.  -1 turns it off, other values are what they are.
						}
					}
				}
*/				

				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			if(pszInfo)
			{
				_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "%d settings were processed.", nIndex);
			}
			delete prs;


			// save changes to CSF
			Settings(false); //write

			return LIBRETTO_SUCCESS;
		}
	}
	else
	{
		if(pszInfo)
		{
			_snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Cannot retrieve database records: %s%s%s%s%s",
				m_pszSettings?"":"Settings table buffer was NULL. ",
				((m_pszSettings)&&(strlen(m_pszSettings)))?"":"Settings table buffer was empty. ",
				m_pdb?"":"Database pointer was NULL. ",
				m_pdbConn?"":"Connection pointer was NULL. ",
				((m_pdbConn)&&(!m_pdbConn->m_bConnected))?"Database not connected. ":""
				);
		}
	}
	return LIBRETTO_ERROR;
}

char* CLibrettoSettings::ProcessString(char* pszString, bool bFreeIncomingString)
{
	if(pszString)
	{
		char pszOutput[4096];
		int nInLen=strlen(pszString);
		int nOutLen=0;
		char* pch = pszString;
		char* pchEnd = min((pch+4096),(pszString+nInLen));
		while((*pch!=0)&&(pch<pchEnd))
		{
			if(*pch == '%')
			{
				// we are in a specifier.
				// here is a list of ours:
				// %h  local hostname
		
				pch++;
				if((*pch!=0)&&(pch<pchEnd))
				{
					if(*pch == 'h')
					{
						if((g_plibretto->m_data.m_pszHost)&&(strlen(g_plibretto->m_data.m_pszHost)))
						{
							char* pszTemp = (char*)malloc(strlen(g_plibretto->m_data.m_pszHost)+1);
							if(pszTemp)
							{
								strcpy(pszTemp, g_plibretto->m_data.m_pszHost);
							
								char* pchPlus = pszTemp;
								while((*pchPlus != 0)&&(nOutLen<4096))
								{
									pszOutput[nOutLen]= *pchPlus;
									nOutLen++;
									pchPlus++;
								}
								free(pszTemp);
							}
						}
						pch++;
					}
					else
					{
						// some other specifier, can be anything.
						pszOutput[nOutLen]= *(pch-1); // do add the %, this may get further processed
						nOutLen++;
						pszOutput[nOutLen]= *pch;
						nOutLen++;
						pch++;
					}
				}
				else
				{
					pszOutput[nOutLen]= *(pch-1);
					nOutLen++;
				}
				
			}
			else
			{
				pszOutput[nOutLen]= *pch;
				nOutLen++;
				pch++;
			}
		}

		pszOutput[nOutLen]=0;

		pch = (char*)malloc(strlen(pszOutput)+1);
		if(pch)
		{
			strcpy(pch,pszOutput);
			if(bFreeIncomingString) 
			{
				try {free(pszString);} catch(...){}
			}
		}
		return pch;
	}
	return NULL;
}


