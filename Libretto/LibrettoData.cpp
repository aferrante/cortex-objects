// LibrettoData.cpp: implementation of the CLibrettoData and related support classes.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <process.h>
#include "Libretto.h"
#include "LibrettoHandler.h" 
#include "LibrettoMain.h" 
#include "LibrettoData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//extern CIS2Comm g_miranda;  //global miranda object

extern CLibrettoMain* g_plibretto;
extern CLibrettoApp theApp;
//extern CADC g_adc; 	// the Harris ADC object

//extern void LibrettoConnectionThread(void* pvArgs);
extern void LibrettoAsynchConnThread(void* pvArgs);
extern void LibrettoConnMonitorThread(void* pvArgs);
extern void LibrettoCommandQueueThread(void* pvArgs);


//////////////////////////////////////////////////////////////////////
// CLibrettoList Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLibrettoList::CLibrettoList()
{
	m_nNumItems=0;
	m_ppchItems = NULL;
}

CLibrettoList::~CLibrettoList()
{
	if((m_nNumItems)&&(m_ppchItems))
	{
		int i=0;
		while(i<m_nNumItems)
		{
			if(m_ppchItems[i]) 
			{
				try { free(m_ppchItems[i]); } catch (...) {}
			}
			i++;
		}

		try { delete [] m_ppchItems; } catch (...) {}
		 
	}
}

int CLibrettoList::AddUnique(char* pchItem)
{
	if(pchItem)
	{
		if(m_nNumItems>0)
		{
			int i=0;
			while(i<m_nNumItems)
			{
				if(m_ppchItems[i]) 
				{
					if(strcmp(m_ppchItems[i],pchItem)==0) return LIBRETTO_ERROR; // found a match, just exit.
				}
				i++;
			}
			// made it here, no matches.
		}

		char** ppch = new char*[m_nNumItems+1];
		if(ppch)
		{
			if((m_nNumItems)&&(m_ppchItems))
			{
				int i=0;
				while(i<m_nNumItems)
				{
					ppch[i] = m_ppchItems[i];
					i++;
				}			 
			}

			ppch[m_nNumItems] = pchItem;

			if(m_ppchItems) try { delete [] m_ppchItems; } catch (...) {}
			m_ppchItems = ppch;
			m_nNumItems++;
			return m_nNumItems;

		}
	}
	return LIBRETTO_ERROR;
}

//////////////////////////////////////////////////////////////////////
// CLibrettoQueueObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLibrettoQueueObject::CLibrettoQueueObject()
{
	nItemid = 0;
	szLocal="";
	szRemote="";
	nAction=-1;
	// CString szServer; // no need, as this is a per-host array.					
	dblTimestamp=0.0;
	szUsername = "";
	nEventItemID=0;
	szMessage="";

}

CLibrettoQueueObject::~CLibrettoQueueObject()
{
}

//////////////////////////////////////////////////////////////////////
// CLibrettoDestinationObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLibrettoDestinationObject::CLibrettoDestinationObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
//	m_pszClientName = NULL;
	m_ulStatus	= LIBRETTO_STATUS_UNINIT;
	m_ulFlags = LIBRETTO_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
//	m_pAPIConn = NULL;
	m_usPort = 23; //default telnet port

	m_dblDiskKBFree = -1.0;  
	m_dblDiskKBTotal = -1.0; 
	m_dblDiskPercent = 90.0; 
	m_nChannelID = -1;
	m_nItemID = -1;

	m_socket = NULL;

	m_bKillCommandQueueThread = true;
	m_bCommandQueueThreadStarted = false;

	m_bKillConnThread = true;
	m_bConnThreadStarted = false;
	m_bKillMonThread = true;
	m_bMonThreadStarted = false;
	m_bSending = false;
	_ftime(&m_timebLastDisconnect);
	_ftime(&m_timebCommandQueueTick);
	_ftime(&m_timebLastPing);
	
	InitializeCriticalSection(&m_critComm);
	InitializeCriticalSection(&m_critMemQueue);
	m_ppMemQueue = NULL;
	m_nNumMemQueueArray=0;
	m_nPtrMemQueueIn=-1;
	m_nPtrMemQueueOut=-1;

	m_pvExternal = NULL;

//	m_bFailed = false;
//	m_timebFailure;
}

CLibrettoDestinationObject::~CLibrettoDestinationObject()
{
	m_bKillConnThread = true;
//	while(	m_bConnThreadStarted ) Sleep(1);

	if(m_pvExternal) delete m_pvExternal; // must use new to create object

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
//	if(m_pszClientName) free(m_pszClientName); // must use malloc to allocate
	DeleteCriticalSection(&m_critComm);
	DeleteCriticalSection(&m_critMemQueue);
}

int CLibrettoDestinationObject::SendTelnetCommand(char* pchBuffer, unsigned long ulBufferLen, char chEolnType)
{
	if((pchBuffer)&&(m_pszServerName))
	{
		if((m_socket == NULL)&&(!m_bConnThreadStarted))
		{
			m_ulStatus = LIBRETTO_STATUS_CONN;
			if(_beginthread(LibrettoAsynchConnThread, 0, (void*)this)==-1)
			{
			//error.

			//**MSG
			}
			else
			{
				int nWait = 0;
				while((m_ulStatus == LIBRETTO_STATUS_CONN)&&(nWait<1000))  // 0.5 seconds?   make a full second.
				{
					nWait++;
					Sleep(1);
				}
			}
//			m_net.OpenConnection(m_pszServerName, g_plibretto->m_settings.m_nPort, &m_socket);
		}
		if(m_socket != NULL)
		{
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "sending: %s", pchBuffer?((char*)(pchBuffer)):"(null)"); // Sleep(250); //(Dispatch message)

			int nReturn = m_net.SendLine((unsigned char*)pchBuffer, ulBufferLen, m_socket, chEolnType);
			if(nReturn>=NET_SUCCESS)
			{
				return LIBRETTO_SUCCESS;
			}
			else if(nReturn == NET_ERROR_CONN)
			{// conn was cut, lets re connect and try one more time.
				m_net.CloseConnection(m_socket);
				m_socket = NULL;
				if(!m_bConnThreadStarted)
				{
					m_ulStatus = LIBRETTO_STATUS_CONN;
					if(_beginthread(LibrettoAsynchConnThread, 0, (void*)this)==-1)
					{
					//error.

					//**MSG
					}
					else
					{
						int nWait = 0;
						while((m_ulStatus == LIBRETTO_STATUS_CONN)&&(nWait<1000))  // 0.5 seconds?  make a full second.
						{
							nWait++;
							Sleep(1);
						}
					}
					//m_net.OpenConnection(m_pszServerName, g_plibretto->m_settings.m_nPort, &m_socket);
				}
				if((m_socket != NULL)&&(!m_bConnThreadStarted))
				{
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "retry: sending: %s", pchBuffer?((char*)(pchBuffer)):"(null)"); // Sleep(250); //(Dispatch message)
					int nReturn = m_net.SendLine((unsigned char*)pchBuffer, ulBufferLen, m_socket);
					if(nReturn>=NET_SUCCESS)
					{
						return LIBRETTO_SUCCESS;
					} // else just return error.
				}
			}
		}
	}
	return LIBRETTO_ERROR;
}

int CLibrettoDestinationObject::AddMemQueue(CLibrettoQueueObject* pMemQueue)
{
	if (pMemQueue==NULL) return LIBRETTO_ERROR;
	// check allocation:
	bool bAlloc = false;
	bool bRinged = false;
	bool bEmpty = false;
	if(m_nNumMemQueueArray<=0)
	{
		bAlloc = true;
	}

	if((m_nPtrMemQueueOut<0)||(m_nPtrMemQueueIn<0)) // becomes -1 when there is nothing left in the mem queue
	{
		// empty buffer; do nothing
		bEmpty = true;
	}
	else
	{
		if(m_nPtrMemQueueIn<m_nPtrMemQueueOut) // we've ringed around.
		{
			bRinged = true;
			//cases:
			if(m_nPtrMemQueueIn+1>=m_nPtrMemQueueOut) bAlloc = true; // need more room
		}
		else // we've not ringed around
		{
			if((m_nPtrMemQueueIn+1>=m_nNumMemQueueArray)&&(m_nPtrMemQueueOut==0)) bAlloc = true; // need more room
		}
	}

	if(bAlloc)
	{
		int nNumMemQueueArray = g_plibretto->m_settings.m_nMemQueueIncrSize+m_nNumMemQueueArray;
		CLibrettoQueueObject** ppMemQueue = new CLibrettoQueueObject*[nNumMemQueueArray];

		if(ppMemQueue)
		{
		
			int i=0;

			if(bEmpty)
			{
				while(i<nNumMemQueueArray)
				{
					ppMemQueue[i] = NULL;
					i++;
				}
			}
			else
			{
				int p=m_nPtrMemQueueOut;
				while(i<nNumMemQueueArray)
				{
					if(i>=m_nNumMemQueueArray)
					{
						ppMemQueue[i] = NULL;
					}
					else
					{
						if(p>=m_nNumMemQueueArray) p=0;
						ppMemQueue[i] = m_ppMemQueue[p];
						if(p == m_nPtrMemQueueIn) m_nPtrMemQueueOut = i;  // don't reassign m_nPtrMemQueueIn since we continually check it, use m_nPtrMemQueueOut as a temp val since it is unused in the loop
					}
					i++; p++;
				}
				m_nPtrMemQueueIn = m_nPtrMemQueueOut;
				m_nPtrMemQueueOut = 0;
			}


			if(m_ppMemQueue) 
			{
				try
				{
					delete [] m_ppMemQueue;
				}
				catch(...)
				{
				}
			}
			m_ppMemQueue = ppMemQueue;
			m_nNumMemQueueArray = nNumMemQueueArray;

			if(bEmpty)
			{
				m_ppMemQueue = ppMemQueue;
				m_ppMemQueue[0] = pMemQueue;
				m_nPtrMemQueueOut = 0;
				m_nPtrMemQueueIn = 0;
			}
			else
			{
				// have to insert, based on timestamp, so it is always sorted
				// m_nPtrMemQueueIn index gets the farthest out timestamp in the future.

				int i=0;
				int p=m_nPtrMemQueueOut;
				bAlloc = false;
				while((i<m_nNumMemQueueArray)&&(!bAlloc))
				{
					if(p>=m_nNumMemQueueArray) p=0;

					if((pMemQueue->dblTimestamp <= m_ppMemQueue[p]->dblTimestamp) || (p==m_nPtrMemQueueIn))// found the insertion point
					{
						// made it <= instead of < so that if they are equal, it is FIFO

						if(p==m_nPtrMemQueueIn) // just add it;
						{
							m_nPtrMemQueueIn++;  if(m_nPtrMemQueueIn>=m_nNumMemQueueArray) m_nPtrMemQueueIn=0; //ring
							m_ppMemQueue[m_nPtrMemQueueIn] = pMemQueue;
							bAlloc = true;
						}
						else  // squeeze it in the middle.
						{
							int x = m_nPtrMemQueueIn;
							int y;
							i=0;
							while((i<m_nNumMemQueueArray)&&(!bAlloc))
							{
								if(x==p)
								{
									m_nPtrMemQueueIn++;  if(m_nPtrMemQueueIn>=m_nNumMemQueueArray) m_nPtrMemQueueIn=0; //ring
									m_ppMemQueue[p] = pMemQueue;
									bAlloc = true;
								}
								else
								{
									y = x+1; if(y>=m_nNumMemQueueArray) y=0; //ring
									m_ppMemQueue[y] = m_ppMemQueue[x];
								}

								i++; x--; if(x<0) x=m_nNumMemQueueArray-1;
							}
						}

					}
//					else
//					{ //nothing
//					}
					i++; p++;
				}

			}

			return m_nPtrMemQueueIn;
		}
		else
		{
			return LIBRETTO_ERROR;
		}

	}

	return LIBRETTO_SUCCESS;
}

/*
int CLibrettoDestinationObject::SetMemQueue(int nMemIndex, CLibrettoQueueObject* pMemQueue)
{
	return LIBRETTO_SUCCESS;
}
*/

int CLibrettoDestinationObject::RemoveMemQueue(int nIndex)
{
	// does NOT delete object,  this is to be done outside.  just clears the array
	if((nIndex>=0)&&(nIndex<m_nNumMemQueueArray)&&(m_ppMemQueue))
	{
		if(nIndex == m_nPtrMemQueueOut) // off the top
		{
			m_ppMemQueue[m_nPtrMemQueueOut] = NULL;
			if(m_nPtrMemQueueOut == m_nPtrMemQueueIn)
			{
				m_nPtrMemQueueOut = m_nPtrMemQueueIn = -1;
			}
			else
			{
				m_nPtrMemQueueOut++;
				if(m_nPtrMemQueueOut>=m_nNumMemQueueArray) m_nPtrMemQueueOut=0; //ring
			}
		}
		else  // off the bottom
		if(nIndex == m_nPtrMemQueueIn)
		{
			m_ppMemQueue[m_nPtrMemQueueIn] = NULL;
			if(m_nPtrMemQueueOut == m_nPtrMemQueueIn)
			{
				m_nPtrMemQueueOut = m_nPtrMemQueueIn = -1;
			}
			else
			{
				m_nPtrMemQueueIn--;  // back up!
				if(m_nPtrMemQueueIn<0) m_nPtrMemQueueIn=m_nNumMemQueueArray; //ring
			}
		}
		else  // was pulled out of the middle
		{
			int i=0;
			int x = m_nPtrMemQueueOut;
			int y;

#pragma message(alert "finish the memqueue")

			while(i<m_nNumMemQueueArray)
			{


				i++;
			}


		}

	}
	return LIBRETTO_ERROR;
}

int CLibrettoDestinationObject::GetMemQueue(int nID) //returns index of next non info item if id=0, else any item with matching id
{
	if(nID>0)
	{
	}
	else
	{
	}

	return LIBRETTO_ERROR;
}

int CLibrettoDestinationObject::ClearMemQueue()
{
	int i=0;
	if(m_ppMemQueue)
	{
		while(i<m_nNumMemQueueArray)
		{
			try
			{
				if(m_ppMemQueue[i]) delete m_ppMemQueue[i];
				m_ppMemQueue[i] = NULL;
			}
			catch(...)
			{
			}

			i++;
		}
	}
	else
	{
		m_nNumMemQueueArray = 0;
	}
	m_nPtrMemQueueIn=-1;
	m_nPtrMemQueueOut=-1;
	return LIBRETTO_SUCCESS;
}

/*
//////////////////////////////////////////////////////////////////////
// CLibrettoChannelObject Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLibrettoChannelObject::CLibrettoChannelObject()
{
	m_pszDesc		= NULL;
	m_pszServerName = NULL;
	m_ulStatus	= LIBRETTO_STATUS_UNINIT;
	m_ulFlags = LIBRETTO_FLAG_DISABLED;  // various states
	m_usType = 0; // not used
	m_nChannelID = -1; // unassigned
	m_nHarrisListID = -1; // unassigned
	m_pbKillConnThread = NULL;
	m_bKillChannelThread = true;
	m_bChannelThreadStarted = false;
	m_pAPIConn = NULL;
}

CLibrettoChannelObject::~CLibrettoChannelObject()
{
	m_bKillChannelThread = true;
	while(	m_bChannelThreadStarted ) Sleep(1);

	if(m_pszDesc) free(m_pszDesc); // must use malloc to allocate
	if(m_pszServerName) free(m_pszServerName); // must use malloc to allocate
}
*/



//////////////////////////////////////////////////////////////////////
// CLibrettoData Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLibrettoData::CLibrettoData()
{
	InitializeCriticalSection(&m_critText);
	InitializeCriticalSection(&m_critDestinations);
	InitializeCriticalSection(&m_critTimeCode);
	m_bTimeCodeThreadStarted = false;
	m_bTimeCodeThreadKill = true;
	m_ucTimeCodeBits = 0x80;


	m_ulTimeCode = 0xffffffff; //invalid value
	m_ulTimeCodeElapsed = 0;

	// messaging...
	m_bNetworkMessagingInitialized=false;

	m_bCheckModsWarningSent=false;
	m_ppDestObj = NULL;
	m_nNumDestinationObjects = 0;
//	m_ppChannelObj = NULL;
//	m_nNumChannelObjects = 0;

	//_timeb m_timebLastStatus; // the time of the last status given
	m_ulFlags = CX_STATUS_UNINIT;  // various states
	m_ulStatusCounter = 0; // a counter incrementor for cortex global status (each obj has their own as well)
	m_pszStatus = NULL;	// parseable string
//	m_pszInfo = NULL;		// human readable info string
	_ftime( &m_timebAutoPurge ); // the last time autopurge was run
	_ftime( &m_timebTick );
	m_pszHost = NULL;			// the name of the host
	m_pszCompleteHost = NULL;	// the complete name of the host
	m_pszCortexHost = NULL;			// the name of the cortex host
	m_usCortexCommandPort = LIBRETTO_PORT_CMD;
	m_usCortexStatusPort = LIBRETTO_PORT_STATUS;
	m_nSettingsMod = -1;
//	m_nChannelsMod = -1;
	m_nDestinationsMod = -1;
	m_nQueueMod = -1;
	m_nLastSettingsMod = -1;
//	m_nLastChannelsMod = -1;
	m_nLastDestinationsMod = -1;
	m_nLastQueueMod = -1;
	m_pdb = NULL;
	m_pdbConn = NULL;
	m_bQuietKill = false;
	m_bProcessSuspended = false;
	m_nMaxLicensedDevices = 2;
}

CLibrettoData::~CLibrettoData()
{
	EnterCriticalSection(&m_critDestinations);
	if(m_ppDestObj)
	{
		int i=0;
		while(i<m_nNumDestinationObjects)
		{
			if(m_ppDestObj[i]) delete m_ppDestObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppDestObj; // delete array of pointers to objects, must use new to allocate
	}
	m_ppDestObj = NULL;
	m_nNumDestinationObjects=0;
	LeaveCriticalSection(&m_critDestinations);
/*
	if(m_ppChannelObj)
	{
		int i=0;
		while(i<m_nNumChannelObjects)
		{
			if(m_ppChannelObj[i]) delete m_ppChannelObj[i]; // delete objects, must use new to allocate
			i++;
		}
		delete [] m_ppChannelObj; // delete array of pointers to objects, must use new to allocate
	}
*/
	if(m_pszHost) free(m_pszHost);	// must use malloc to allocate
	if(m_pszCompleteHost) free(m_pszCompleteHost);	// must use malloc to allocate
	if(m_pszCortexHost) free(m_pszCortexHost);	// must use malloc to allocate

	EnterCriticalSection(&m_critText);
	if(m_pszStatus) free(m_pszStatus); // must use malloc to allocate
//	if(m_pszInfo) free(m_pszInfo); // must use malloc to allocate
	LeaveCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critText);
	DeleteCriticalSection(&m_critDestinations);
	DeleteCriticalSection(&m_critTimeCode);
}

char* CLibrettoData::GetStatusText(unsigned long* pulStatus)  // allocates mem, must free after use
{
	char* pch = NULL;
	EnterCriticalSection(&m_critText);
	if((m_pszStatus)&&(strlen(m_pszStatus)))
	{
		pch = (char*)malloc(strlen(m_pszStatus));  // we really need 8 bytes fewer...
		if(pch)
		{
			if(pulStatus) *pulStatus = m_bu.xtol(m_pszStatus, strlen(m_pszStatus));
			strcpy(pch, m_pszStatus+8);// first eight bytes are status flags
		}
	}
	LeaveCriticalSection(&m_critText);
	return pch;
}

int	CLibrettoData::SetStatusText(char* pszText, unsigned long ulStatus, bool bOverwriteExistingError)
{
	int nRV = CX_ERROR;
	bool bError=false;

	if(!bOverwriteExistingError)
	{
		if(
			  ((m_ulFlags&LIBRETTO_ICON_MASK) == LIBRETTO_STATUS_ERROR)
			||((m_ulFlags&LIBRETTO_STATUS_CMDSVR_MASK) == LIBRETTO_STATUS_CMDSVR_ERROR)
			||((m_ulFlags&LIBRETTO_STATUS_STATUSSVR_MASK) ==  LIBRETTO_STATUS_STATUSSVR_ERROR)
			||((m_ulFlags&LIBRETTO_STATUS_THREAD_MASK) == LIBRETTO_STATUS_THREAD_ERROR)
			||((m_ulFlags&LIBRETTO_STATUS_FAIL_MASK) == LIBRETTO_STATUS_FAIL_DB)
			)	bError=true;
	}

	if (ulStatus&LIBRETTO_ICON_MASK)
	{
		m_ulFlags &= ~LIBRETTO_ICON_MASK;
		m_ulFlags |= (ulStatus&LIBRETTO_ICON_MASK);
	}
	if (ulStatus&LIBRETTO_STATUS_CMDSVR_MASK)
	{
		m_ulFlags &= ~LIBRETTO_STATUS_CMDSVR_MASK;
		m_ulFlags |= (ulStatus&LIBRETTO_STATUS_CMDSVR_MASK);
	}
	if (ulStatus&LIBRETTO_STATUS_STATUSSVR_MASK)
	{
		m_ulFlags &= ~LIBRETTO_STATUS_STATUSSVR_MASK;
		m_ulFlags |= (ulStatus&LIBRETTO_STATUS_STATUSSVR_MASK);
	}
	if (ulStatus&LIBRETTO_STATUS_THREAD_MASK)
	{
		m_ulFlags &= ~LIBRETTO_STATUS_THREAD_MASK;
		m_ulFlags |= (ulStatus&LIBRETTO_STATUS_THREAD_MASK);
	}
	if (ulStatus&LIBRETTO_STATUS_FAIL_MASK)
	{
		m_ulFlags &= ~LIBRETTO_STATUS_FAIL_MASK;
		m_ulFlags |= (ulStatus&LIBRETTO_STATUS_FAIL_MASK);
	}

	if(bError)
	{
		m_ulFlags &= ~LIBRETTO_ICON_MASK;
		m_ulFlags |= LIBRETTO_STATUS_ERROR;
	}

	if((pszText)&&(!bError))
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
//				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
//			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_plibretto->m_settings.m_pszIconPath)&&(strlen(g_plibretto->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_plibretto->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_plibretto->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_plibretto->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_plibretto->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_plibretto->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}
/*
int	CLibrettoData::SetStatusText(char* pszText, unsigned long ulStatus)
{
	int nRV = CX_ERROR;
	if(pszText)
	{
		EnterCriticalSection(&m_critText);
		if(m_pszStatus)
		{
			if((strcmp(m_pszStatus, pszText))||(m_ulFlags!=ulStatus))
			{
				free(m_pszStatus);
				m_pszStatus =  NULL;
			}
		}
		if((strlen(pszText))&&(m_pszStatus==NULL)) //m_pszStatus is null if we are going to do something
		{
			m_pszStatus = (char*)malloc(strlen(pszText)+9);
			if(m_pszStatus)
			{
				m_ulFlags = ulStatus;
				sprintf(m_pszStatus, "%08x%s", m_ulFlags, pszText);
				nRV = CX_SUCCESS;
				m_ulStatusCounter++;
			}
		}
		else
		{
			m_ulFlags = ulStatus;
			nRV = CX_SUCCESS; // this is how we clear it, non-null pointer with strlen =0
			m_ulStatusCounter++;
		}
		LeaveCriticalSection(&m_critText);
	}
	// now deal with HTML icon if necessary
	if((g_plibretto->m_settings.m_pszIconPath)&&(strlen(g_plibretto->m_settings.m_pszIconPath)>0))
	{
		char pszFromPath[MAX_PATH];
		char pszToPath[MAX_PATH];

		sprintf(pszToPath, "%sstatus.gif", g_plibretto->m_settings.m_pszIconPath);

		switch(m_ulFlags&CX_ICON_MASK)
		{
		case CX_STATUS_UNINIT://							0x00000000  // uninitialized	(VDS icon)
		case CX_STATUS_ERROR://							0x00000020  // error (red icon)
			{
				sprintf(pszFromPath, "%sstatusR.gif", g_plibretto->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_UNKNOWN://						0x00000010  // unknown, incomplete, not finished, etc (yellow icon)	
			{
				sprintf(pszFromPath, "%sstatusY.gif", g_plibretto->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_OK://									0x00000030  // ready (green icon)	
			{
				sprintf(pszFromPath, "%sstatusG.gif", g_plibretto->m_settings.m_pszIconPath);
			} break;
		case CX_STATUS_RUN://								0x00000040  // in progress, running, owned etc (blue icon);	
			{
				sprintf(pszFromPath, "%sstatusB.gif", g_plibretto->m_settings.m_pszIconPath);
			} break;
		}
		CopyFile(pszFromPath, pszToPath, FALSE);
	}

	return nRV;
}
*/

// utility
int	CLibrettoData::GetHost()
{
	IN_ADDR inaddr;
	hostent* ph=NULL;
	char host[MAX_PATH];
	int nValue = 0;
	nValue = gethostname(host, MAX_PATH);
	if(nValue != SOCKET_ERROR)
	{
		ph = gethostbyname(host);
		if(ph!=NULL)
		{
			memcpy(&inaddr, ph->h_addr, 4);
			ph = gethostbyaddr((char *)&inaddr, 4, PF_INET);
			if(ph!=NULL)
			{
				nValue = strlen(ph->h_name);
				if(nValue>0)
				{
					char* pch;
					pch = (char*) malloc(nValue+1);
					if(pch!=NULL)
					{
						strcpy(pch, ph->h_name);
						if(m_pszCompleteHost) free(m_pszCompleteHost);
						m_pszCompleteHost=pch;

						pch = (char*) malloc(nValue+1);
						if(pch!=NULL)
						{
							if(m_pszHost) free(m_pszHost);
							m_pszHost=pch;

							strcpy(m_pszHost, m_pszCompleteHost);
							pch = strchr(m_pszHost, '.');
							if(pch)
							{
								*pch = 0;
							}

							return LIBRETTO_SUCCESS;
						}
					}
				}
			}
		}
	}
	return LIBRETTO_ERROR;
}

int CLibrettoData::IncrementDatabaseMods(char* pszTableName, char* pszInfo, CDBUtil* pdb, CDBconn* pdbConn)
{
	if(
		  (
		    ((pdbConn)&&(pdb))
		  ||((m_pdbConn)&&(m_pdb))
			)
			&&(pszTableName)
			&&(strlen(pszTableName))
			&&(g_plibretto)
			&&(g_plibretto->m_settings.m_pszExchange)
			&&(strlen(g_plibretto->m_settings.m_pszExchange))
			)
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		unsigned long ulMod = 0;
		CString szTemp;
		szTemp.Format("DBT_%s", pszTableName );

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s set mod = \
(SELECT case when max(mod) is null then 1 when max(mod) >= %d \
then 1 else max(mod) + 1 end from %s WHERE criterion = '%s') WHERE criterion = '%s'",
			g_plibretto->m_settings.m_pszExchange,
			LIBRETTO_DB_MOD_MAX,
			g_plibretto->m_settings.m_pszExchange,
			szTemp, szTemp		
			);

		CDBUtil*						pdbE;
		CDBconn*						pdbConnE;

		if(pdb) pdbE=pdb; else pdbE=m_pdb;
		if(pdbConn) pdbConnE=pdbConn; else pdbConnE=m_pdbConn;


		if(pdbE->ExecuteSQL(pdbConnE, szSQL, pszInfo)>=DB_SUCCESS)
		{
			return LIBRETTO_SUCCESS;
		}
	}
	return LIBRETTO_ERROR;
}

int CLibrettoData::CheckMessages(char* pszInfo)
{
	if((g_plibretto)&&(m_pdbConn)&&(m_pdb)
		&&(g_plibretto->m_settings.m_pszMessages)&&(strlen(g_plibretto->m_settings.m_pszMessages)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_plibretto->m_settings.m_pszMessages)&&(strlen(g_plibretto->m_settings.m_pszMessages)))?g_plibretto->m_settings.m_pszMessages:"Messages",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_plibretto->m_settings.m_nAutoPurgeMessageDays*86400) // local time....
			);

//		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
//			LeaveCriticalSection(&m_critSQL);
			return LIBRETTO_SUCCESS;
		}
//		LeaveCriticalSection(&m_critSQL);
	}
	return LIBRETTO_ERROR;
}
/*
int CLibrettoData::CheckAsRun(char* pszInfo)
{
	if((g_plibretto)&&(m_pdbConn)&&(m_pdb)
		&&(g_plibretto->m_settings.m_pszAsRun)&&(strlen(g_plibretto->m_settings.m_pszAsRun)))
	{
		_timeb timestamp;
		_ftime( &timestamp );
		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE systime < %d", 
			((g_plibretto->m_settings.m_pszAsRun)&&(strlen(g_plibretto->m_settings.m_pszAsRun)))?g_plibretto->m_settings.m_pszAsRun:"AsRun_Log",
			(unsigned long)(timestamp.time - (timestamp.timezone*60) +(timestamp.dstflag?3600:0))-(g_plibretto->m_settings.m_nAutoPurgeAsRunDays*86400) // local time....
			);

		EnterCriticalSection(&m_critSQL);
		if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)>=DB_SUCCESS)
		{
			LeaveCriticalSection(&m_critSQL);
			return LIBRETTO_SUCCESS;
		}
		LeaveCriticalSection(&m_critSQL);
	}
	return LIBRETTO_ERROR;
}
*/


int CLibrettoData::CheckDatabaseMods(char* pszInfo)
{
	if((g_plibretto)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];
		char errorstring[DB_ERRORSTRING_LEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s", 
			((g_plibretto->m_settings.m_pszExchange)&&(strlen(g_plibretto->m_settings.m_pszExchange)))?g_plibretto->m_settings.m_pszExchange:"Exchange");

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			int nReturn = LIBRETTO_SUCCESS;
			int nIndex = 0;
			while (!prs->IsEOF())
			{
				CString szCriterion;
				CString szMod;
				CString szTemp;
				try
				{
					prs->GetFieldValue("criterion", szCriterion);//HARDCODE
					prs->GetFieldValue("mod", szMod);//HARDCODE
				}
				catch( ... )
				{
				}

				if((g_plibretto->m_settings.m_pszSettings)&&(strlen(g_plibretto->m_settings.m_pszSettings)))
				{
					szTemp.Format("DBT_%s",g_plibretto->m_settings.m_pszSettings);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nSettingsMod = nReturn;
					}
				}

/*
				if((g_plibretto->m_settings.m_pszChannels)&&(strlen(g_plibretto->m_settings.m_pszChannels)))
				{
					szTemp.Format("DBT_%s",g_plibretto->m_settings.m_pszChannels);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nChannelsMod = nReturn;
					}
				}
*/
				if((g_plibretto->m_settings.m_pszDestinations)&&(strlen(g_plibretto->m_settings.m_pszDestinations)))
				{
					szTemp.Format("DBT_%s",g_plibretto->m_settings.m_pszDestinations);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nDestinationsMod = nReturn;
					}
				}

				if((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))
				{
					szTemp.Format("DBT_%s",g_plibretto->m_settings.m_pszQueue);
					if(szCriterion.CompareNoCase(szTemp)==0)
					{
						nReturn = atoi(szMod);
						if(nReturn>0) m_nQueueMod = nReturn;
					}
				}
				// get the suspend

				szTemp.Format("Suspend");
				if(szCriterion.CompareNoCase(szTemp)==0)
				{
					nReturn = atoi(szMod);
					if(nReturn>0)
					{
						if(!m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Libretto is suspended.");  
							g_plibretto->m_data.SetStatusText(errorstring, CX_STATUS_UNKNOWN);
							g_plibretto->m_msgr.DM(MSG_ICONNONE, NULL, "Libretto:suspend", "*** Libretto has been suspended. ***"); // Sleep(50); //(Dispatch message)
							g_plibretto->SendMsg(CX_SENDMSG_INFO, "Libretto:suspend", "Libretto has been suspended");
							m_bProcessSuspended = true;
						}
					}
					else
					{
						if(m_bProcessSuspended)
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Libretto is running.");  
							g_plibretto->m_data.SetStatusText(errorstring, CX_STATUS_OK);
							g_plibretto->m_msgr.DM(MSG_ICONNONE, NULL, "Libretto:resume", "*** Libretto has been resumed. ***");//  Sleep(50); //(Dispatch message)
							g_plibretto->SendMsg(CX_SENDMSG_INFO, "Libretto:resume", "Libretto has been resumed.");
							m_bProcessSuspended = false;
						}
					}
				}

				nIndex++;
				prs->MoveNext();
			}
			prs->Close();

			delete prs;
			return nReturn;
		}
	}
	return LIBRETTO_ERROR;
}


int CLibrettoData::GetQueue(char* pszInfo)
{
	// following removed to deal with wait mode below.
/*
	if(g_miranda.m_bTransferring)
	{
		// something in progress, must wait.
		return LIBRETTO_ERROR;
	}
*/
	
	if((g_plibretto)&&(m_pdbConn)&&(m_pdb))
	{

		// let's just get the top (oldest timestamp) item in the queue, and deal with it.

		_ftime( &g_plibretto->m_data.m_timebTick );

		char szSQL[DB_SQLSTRING_MAXLEN];
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE action <> 128 ORDER BY timestamp ASC", //HARDCODE
			((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue");

		// need to change to filename_local varchar(512), filename_remote varchar(512) to manage hex encoded payloads
//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
//action int, host varchar(64), timestamp float, username varchar(32));

		CString szLocal = "";
		CString szRemote = "";
		CString szServer = "";
		CString szUsername = "";
		CString szMessage = "";
		CString szTemp = "";
		int nItemID = -1;
		bool bItemFound = false;
		double dblTimestamp=-57;   // timestamp
		int nAction = -1;
		int nTemp;
		int nEventItemID=-1;



if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:get_queue", "SQL: %s", szSQL); // Sleep(250); //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = LIBRETTO_ERROR;
			int nIndex = 0;
//			while ((!prs->IsEOF()))
			if ((!prs->IsEOF()))  // just do the one record, if there is one
			{
				try
				{
					prs->GetFieldValue("itemid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nItemID = atoi(szTemp);
						bItemFound = true;
					}
					prs->GetFieldValue("local", szLocal);//HARDCODE
					szLocal.TrimLeft(); szLocal.TrimRight();
					prs->GetFieldValue("remote", szRemote);//HARDCODE
					szRemote.TrimLeft(); szRemote.TrimRight();
					prs->GetFieldValue("action", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>=0) nAction = nTemp;
					}

					prs->GetFieldValue("host", szServer);//HARDCODE
					szServer.TrimLeft(); szServer.TrimRight();

					prs->GetFieldValue("timestamp", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						dblTimestamp = atof(szTemp);
					}
					prs->GetFieldValue("username", szUsername);//HARDCODE
					szUsername.TrimLeft(); szUsername.TrimRight();
					prs->GetFieldValue("event_itemid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nTemp = atoi(szTemp);
						if(nTemp>0) nEventItemID = nTemp;
					}

					prs->GetFieldValue("message", szMessage);//HARDCODE
					
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught database exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:get_queue", "Retrieve: Caught database exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL); // Sleep(250); //(Dispatch message)
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						e->GetErrorMessage(errorstring, MAX_MESSAGE_LENGTH);

						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught memory exception: %s\n%s", errorstring, szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:get_queue", "Retrieve: Caught memory exception: %s\n%s", errorstring, szSQL); // Sleep(250); //(Dispatch message)
					}
					else 
					{
						e->GetErrorMessage(errorstring, MAX_MESSAGE_LENGTH);
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", errorstring, szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:get_queue", "Retrieve: Caught exception: %s\n%s", errorstring, szSQL); // Sleep(250); //(Dispatch message)
					}
					e->Delete();
				} 
				catch( ... )
				{
//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:get_queue", "Retrieve: Caught exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)
				}

				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;

//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "got %d things from queue", nIndex);  // Sleep(250);//(Dispatch message)

			if((nReturn>0)&&(bItemFound))
			{
				// process the request, then remove it from the queue
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "got item %d from queue", nItemID);   Sleep(250);//(Dispatch message)
		_ftime( &g_plibretto->m_data.m_timebTick );

				if(szServer.GetLength()>0)
				{
					char* pchServer = szServer.GetBuffer(1);
					int nDestIndex = ReturnDestinationIndex(pchServer);
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_QUEUE)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "got item for server %s from queue", pchServer); //  Sleep(250);//(Dispatch message)

					if((nDestIndex>=0)&&(m_ppDestObj)&&(m_ppDestObj[nDestIndex])&&(m_ppDestObj[nDestIndex]->m_ulFlags&LIBRETTO_FLAG_ENABLED))
					{  
						// only do anything if its enabled.
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_QUEUE)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "server %s enabled, socket:%d, thread:%d", pchServer, m_ppDestObj[nDestIndex]->m_socket, m_ppDestObj[nDestIndex]->m_bConnThreadStarted);//   Sleep(250);//(Dispatch message)

					//	int nHostReturn = LIBRETTO_ERROR;
						if((m_ppDestObj[nDestIndex]->m_socket == NULL)&&(!m_ppDestObj[nDestIndex]->m_bConnThreadStarted))
						{
//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_QUEUE)) 
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "closing conn to server %s", pchServer);//   Sleep(250);//(Dispatch message)
							m_ppDestObj[nDestIndex]->m_net.CloseConnection(m_ppDestObj[nDestIndex]->m_socket);
//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_QUEUE)) 
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "closed conn to server %s", pchServer);//   Sleep(250);//(Dispatch message)
							m_ppDestObj[nDestIndex]->m_ulStatus = LIBRETTO_STATUS_CONN;
							if(_beginthread(LibrettoAsynchConnThread, 0, (void*)(g_plibretto->m_data.m_ppDestObj[nDestIndex]))==-1)
							{
							//error.
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Error starting connection thread for server %s", pchServer);//   Sleep(250);//(Dispatch message)
							//**MSG
							}
							else
							{
//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_QUEUE)) 
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "began conn thread to server %s", pchServer);//   Sleep(250);//(Dispatch message)
								int nWait = 0;
								while((m_ppDestObj[nDestIndex]->m_ulStatus == LIBRETTO_STATUS_CONN)&&(nWait<1000))  // 0.5 seconds? 1 second
								{
		_ftime( &g_plibretto->m_data.m_timebTick );
//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_QUEUE)) 
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "waiting for conn to server %s: %d", pchServer, clock());//   Sleep(250);//(Dispatch message)
									nWait++;
									Sleep(1);
								}
							}

/*
							nHostReturn = m_ppDestObj[nDestIndex]->m_net.OpenConnection(pchServer, g_plibretto->m_settings.m_nPort, &m_ppDestObj[nDestIndex]->m_socket); // re-establish
							char tnbuffer[32]; sprintf(tnbuffer, "\\\\%c%c", 13,10);
							m_ppDestObj[nDestIndex]->SendTelnetCommand(tnbuffer, 2);
							unsigned char* pucBuffer = NULL;
							unsigned long ulBufferLen = 256;
							m_ppDestObj[nDestIndex]->m_net.GetLine(&pucBuffer, &ulBufferLen, m_ppDestObj[nDestIndex]->m_socket);
							if(pucBuffer) free(pucBuffer);
*/
						}

		_ftime( &g_plibretto->m_data.m_timebTick );
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_QUEUE)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:get_queue", "conn check server %s enabled, socket:%d, thread:%d", pchServer, m_ppDestObj[nDestIndex]->m_socket, m_ppDestObj[nDestIndex]->m_bConnThreadStarted);//   Sleep(250);//(Dispatch message)
						if(m_ppDestObj[nDestIndex]->m_socket != NULL)
						{

//							nHostReturn = OX_SUCCESS; //g_miranda.OxSoxPing();
							//ping the host first.
//		_ftime( &g_plibretto->m_data.m_timebTick );
						//	if(nHostReturn >= LIBRETTO_SUCCESS)
							{
							// first thing is, get the disk space statistics.
/*
								BOOL bReturn;
								
								if(g_miranda.m_lpfnGetDriveInfo)
								{ 
									if(nDirIndex>0)
									{
										sprintf(pchDirAlias, "%s", szFilenameRemote.Left(nDirIndex));
										if(pchDirAlias[0]!='$') // error
											strcpy(pchDirAlias, "$VIDEO"); // default.
									}
									else
									{
										strcpy(pchDirAlias, "$VIDEO"); // default.
									}

									DiskInfo_ diskinfo;

									bReturn = g_miranda.m_lpfnGetDriveInfo(g_miranda.m_pszHost, pchDirAlias, &diskinfo);
							//		AfxMessageBox("Y");
									if(bReturn == FALSE)
									{
										//**MSG
						//				g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:oxsox_diskinfo", "Could not get disk information for %s partition on %s", pchDirAlias, pchServer );   Sleep(50);//(Dispatch message)
	/*
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "m_lpfnGetDriveInfo returned FALSE");   Sleep(50);//(Dispatch message)
										g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:oxsox_diskinfo", "Could not get disk information for %s partition", pchDirAlias );   Sleep(50);//(Dispatch message)
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "m_lpfnGetDriveInfo returned2 FALSE");   Sleep(50);//(Dispatch message)
* /
										g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:oxsox_diskinfo", "Could not get disk information for %s", pchServer );   Sleep(50);//(Dispatch message)
//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "m_lpfnGetDriveInfo returned3 FALSE");   Sleep(50);//(Dispatch message)

									}
									else
									{
										if(nDestIndex>=0)
										{
											//its a host in our list!
											if((m_ppDestObj)&&(m_ppDestObj[nDestIndex]))
											{
												m_ppDestObj[nDestIndex]->m_dblDiskKBFree = (double)diskinfo.KBytes_Free;  
												m_ppDestObj[nDestIndex]->m_dblDiskKBTotal = (double)diskinfo.KBytes_Total;  
												// ok, now update DB.  but do not increment counter, that will cause infinite loop.

												if(m_ppDestObj[nDestIndex]->m_nItemID>0) // last known unique item id
												{

	// create table Destinations (destinationid int identity(1,1) NOT NULL, host varchar(64) NOT NULL,
	//				type int, description varchar(256), diskspace_free float, diskspace_total float, diskspace_threshold real,
	//				channelid int, flags int);

													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET diskspace_free = %f, diskspace_total = %f WHERE destinationid = %d",  //HARDCODE
														((g_plibretto->m_settings.m_pszDestinations)&&(strlen(g_plibretto->m_settings.m_pszDestinations)))?g_plibretto->m_settings.m_pszDestinations:"Destinations",
														m_ppDestObj[nDestIndex]->m_dblDiskKBFree,
														m_ppDestObj[nDestIndex]->m_dblDiskKBTotal,
														m_ppDestObj[nDestIndex]->m_nItemID);
													if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
													{
														//**MSG
									//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

													}
												}
											}
										}
									}
	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "after m_lpfnGetDriveInfo ");   Sleep(50);//(Dispatch message)

								}
								else
								{
									//**MSG
									g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:oxsox_diskinfo", "Could not get disk information for %s partition on %s; NULL function address.", pchDirAlias, pchServer );   Sleep(50);//(Dispatch message)
	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "m_lpfnGetDriveInfo was NULL");   Sleep(50);//(Dispatch message)
								}
*/
	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "file local %s, file remote %s, action %d", szFilenameLocal, szFilenameRemote, nAction);  Sleep(250); //(Dispatch message)

//								char dberrorstring[DB_ERRORSTRING_LEN];
	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "switching with %d", nAction);   Sleep(50);//(Dispatch message)
								switch(nAction)
								{
								case 256: // telnet command
									{
//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "*** checking %s on %s", pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)

										unsigned long ulLen = szLocal.GetLength();
										char* buffer = szLocal.GetBuffer(0);
										unsigned short usCommand=0x0000;
										char chEolnType = EOLN_CRLF;// chyron type default
									
										if(m_ppDestObj[nDestIndex]->m_usType == CX_DESTTYPE_EVERTZ_7847FSE)
										{
											chEolnType = EOLN_NONE; // do not send CRLF
											// convert the buffer. // use libretto main object bufferutil.
											m_bu.Base64Decode(&buffer, &ulLen, true); // true frees the incoming buffer.  buffer and length are updated
											// replace time code and message number
//											nCommand = 0x0000ffff&((((CSCTE104SingleMessage*)buffer)->OpId1High<<8)|(((CSCTE104SingleMessage*)buffer)->OpId1Low));
											usCommand = buffer[0] & 0x00ff;
											usCommand <<= 8; usCommand += (buffer[1] & 0x00ff);
										}
										
// added repititions
	int nRepeats=0;
	do
	{
	
										if(m_ppDestObj[nDestIndex]->m_usType == CX_DESTTYPE_EVERTZ_7847FSE)
										{
											CSCTE104Util* p104 = (CSCTE104Util*)m_ppDestObj[nDestIndex]->m_pvExternal;
											if(usCommand == 0xffff) // multi message
											{
												buffer[6] = (nRepeats==0)?p104->IncrementMessageNumber():p104->m_ucLastMessageNumber;

								//				CString Q;
								//				Q.Format("MULTI %04x",  nCommand);
								//				AfxMessageBox(Q);


												// multi obj
												SCTE104HMSF_t HMSF;


												// get timecode if installed.
												bool bSysClock = false;
												if((g_plibretto->m_settings.m_bUseTimeCode)&&(m_bTimeCodeThreadStarted))
												{

	EnterCriticalSection(&m_critTimeCode);
													unsigned long ulTimeCode = m_ulTimeCode;
													unsigned long ulTimeCodeElapsed = m_ulTimeCodeElapsed;
	LeaveCriticalSection(&m_critTimeCode);

													if(ulTimeCodeElapsed>(unsigned long)g_plibretto->m_settings.m_nSyncThresholdMS) // worse than the threshold. could just use the sys clock, which is close....
													{
														// should do something if elapsed, but not going to... will chalk it up to disconnected TC card.
														bSysClock=true;
													}
													else
													{
														if(ulTimeCode!=0xffffffff)
														{
															int nTemp = (unsigned char)((ulTimeCode&0xff000000)>>24); 
															HMSF.hours = ((nTemp/16)*10	+ (nTemp%16));
															
															nTemp = (unsigned char)((ulTimeCode&0x00ff0000)>>16); 
															HMSF.minutes = ((nTemp/16)*10	+ (nTemp%16));

															nTemp = (unsigned char)((ulTimeCode&0x0000ff00)>>8); 
															HMSF.minutes = ((nTemp/16)*10	+ (nTemp%16));
							
															nTemp = (unsigned char)(ulTimeCode&0x000000ff);
															HMSF.frames = ((nTemp/16)*10	+ (nTemp%16));


														}	else bSysClock=true;
													}
												} else bSysClock=true;

												if(bSysClock)
												{
													_timeb timestamp;
													_ftime(&timestamp);

													if(g_plibretto->m_settings.m_bDF)
													{
														// fill the data with drop frame
														int rem = (timestamp.time- (timestamp.timezone*60)+(timestamp.dstflag?3600:0));
														while(rem<0) rem+=86400;
														while(rem>86399) rem-=86400;
														int ms = (rem%86400)*1000 + timestamp.millitm;


/*
														double ms = (rem%86400)*1000 + timestamp.millitm;
														int fr = (int)(((ms * 30.0) / 1001.0) + 0.5);
														int deci = (int)(fr / 17982);
														rem = fr % 17982;
														if (rem >= 1800) rem += 2 + ((rem - 1800) / 1798) * 2; 

													
														HMSF.hours = deci / 6; 
														HMSF.minutes = ((deci % 6) * 10) + (rem / 1800); 
														HMSF.seconds = (rem % 1800) / 30; 
														HMSF.frames = rem % 30;
*/														
														int nHours;
														int nMinutes;
														int nSeconds;
														int nFrames;

														m_ppDestObj[nDestIndex]->m_timeconv.MStoHMSF(ms, &nHours, &nMinutes, &nSeconds, &nFrames); // default to ntsc df
														HMSF.hours		= (unsigned char)nHours;
														HMSF.minutes	= (unsigned char)nMinutes;
														HMSF.seconds	= (unsigned char)nSeconds;
														HMSF.frames		= (unsigned char)nFrames;

													}
													else
													{
														timestamp.time %= 86400; //remains of the day!
														HMSF.hours = (timestamp.time/3600)%24;
														HMSF.minutes = (timestamp.time/60)%60;
														HMSF.seconds = (timestamp.time)%60;
														HMSF.frames = timestamp.millitm/g_plibretto->m_settings.m_nFrameRate;
													}
												}

												buffer[10] = 0x02;
												buffer[11] = HMSF.hours;
												buffer[12] = HMSF.minutes;
												buffer[13] = HMSF.seconds;
												buffer[14] = HMSF.frames;
											}
											else
											{
												if(nRepeats==0)
												{
													buffer[10] = p104->IncrementSecondaryMessageNumber();
												}
												else
												{
												
													int rv = p104->m_ucLastMessageNumber + p104->m_ucLastSecondaryMessageNumberInc + 128 - (p104->m_ucSecondaryMessageNumberofValues/2); // small loop averaging 180 degree phase on primary message number
													while(rv>255)	rv-=256; // loop around.
													buffer[10] = (char)rv;
												}

								//				}

												// need to create a message number that starts at p104->m_ucLastMessageNumber + 128 and increments from there with a secondary incrementer
								//				CString Q;
								//				Q.Format("SINGLE %04x", nCommand);
								//				AfxMessageBox(Q);
								/*
												if( nCommand == 0x0001) // init message
												{
																		AfxMessageBox("init msg");
												}
												else
												if( nCommand == 0x0003) // keep_alive message
												{
																	AfxMessageBox("keep_alive msg");
												}
									*/		
												// single obj
											}

										}


if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_QUEUE|LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:get_queue", "sending %s", buffer); // Sleep(250); //(Dispatch message)
		

	EnterCriticalSection(&m_ppDestObj[nDestIndex]->m_critComm);

										m_ppDestObj[nDestIndex]->m_bSending=true;
										int nReturn = m_ppDestObj[nDestIndex]->SendTelnetCommand(buffer, ulLen, chEolnType);

										bool bLastCycle = (
																				(g_plibretto->m_settings.m_nCommandRepetitions<=0) 
																			||((g_plibretto->m_settings.m_nCommandRepetitions>0)&&(nRepeats>=g_plibretto->m_settings.m_nCommandRepetitions)) //i.e. last one before increment.
																			);

										if(bLastCycle)
										{
											szLocal.ReleaseBuffer();
										}

										if(nReturn<LIBRETTO_SUCCESS)
										{
												m_ppDestObj[nDestIndex]->m_bSending=false;
	LeaveCriticalSection(&m_ppDestObj[nDestIndex]->m_critComm);
											//**MSG
											g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:telnet", 
												"telnet command not successful on %s.", pchServer );//   Sleep(50);//(Dispatch message)

											if((nEventItemID>0)&&(bLastCycle))
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E256:command not successful on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d;",  //HARDCODE
													((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
													pchServer, nItemID
													);

												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
		//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:get_queue", "ERROR executing SQL: %s", errorstring);//  Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
//												m_ppDestObj[nDestIndex]->m_bSending=false;
//	LeaveCriticalSection(&m_ppDestObj[nDestIndex]->m_critComm);

												return LIBRETTO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function
											}
										}
										else
										{
											//while(!g_miranda.m_bTransferring) Sleep(1);  //not good, in case error

//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "%s on %s succeeded", szLocal, pchServer);  //Sleep(250); //(Dispatch message)
											// we have queued the xfer , lets update the thing with a status, then exit without removing
											char pchFinalBuffer[256];
											unsigned char* pucBuffer = NULL;
											unsigned long ulBufferLen = 256;
											nReturn = m_ppDestObj[nDestIndex]->m_net.GetLine(&pucBuffer, &ulBufferLen, m_ppDestObj[nDestIndex]->m_socket, NET_RCV_ONCE, errorstring);
												m_ppDestObj[nDestIndex]->m_bSending=false;
	LeaveCriticalSection(&m_ppDestObj[nDestIndex]->m_critComm);

											if(nReturn>=NET_SUCCESS) _ftime(&m_ppDestObj[nDestIndex]->m_timebLastPing);

											bool bEventID = ( 
																				(nEventItemID>0)
																			&&(
																					(g_plibretto->m_settings.m_nCommandRepetitions<=0) 
																				||((g_plibretto->m_settings.m_nCommandRepetitions>0)&&(nRepeats>=g_plibretto->m_settings.m_nCommandRepetitions)) //i.e. last one before increment.
																				)
																			);

											
											if(
													(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_QUEUE|LIBRETTO_DEBUG_DEVCOMM)) 
												||((nEventItemID>0)&&(bLastCycle))
												)
											{

												if((nReturn<NET_SUCCESS)||(ulBufferLen==0))
												{
													_snprintf(pchFinalBuffer, 256, "%s: (%d) %s", ((nReturn<NET_SUCCESS)?"error":"no buffer"), nReturn, errorstring);
												}
												else
												{
													if(m_ppDestObj[nDestIndex]->m_usType == CX_DESTTYPE_EVERTZ_7847FSE)
													{
														// convert the buffer. // use libretto main object bufferutil.
														m_bu.Base64Encode((char**)&pucBuffer, &ulBufferLen, true); // true frees the incoming buffer.  buffer and length are updated
													}
													_snprintf(pchFinalBuffer, 256, "%s", (pucBuffer?((char*)pucBuffer):"(null)"));
												}
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_QUEUE|LIBRETTO_DEBUG_DEVCOMM)) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "received %s", pchFinalBuffer); // Sleep(250); //(Dispatch message)
											}

											if(pucBuffer) free(pucBuffer);

											


											if((nEventItemID>0)&&(bLastCycle))
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I256:sent command to %s.', remote = '%s', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
													((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
													pchServer, pchFinalBuffer, nItemID
													);

						/*
						Queue action IDs
1 transfer file to Lyric Box
2 transfer file from Lyric Box
3 delete file from Lyric Box
4 refresh file listing of Lyric Box
19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
32  file is transferring, check for end of transfer
64  check for existence of file
128 give results
256 send telnet command to Lyric Box
						*/
										
											
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
		//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

												}

												szServer.ReleaseBuffer();
// moved folling up after getline
												//												m_ppDestObj[nDestIndex]->m_bSending=false;
//	LeaveCriticalSection(&m_ppDestObj[nDestIndex]->m_critComm);

												return LIBRETTO_ERROR; // we dont want to remove the item at the end of this function
											}
										}

										nRepeats++;
		} while((g_plibretto->m_settings.m_nCommandRepetitions>0)&&(nRepeats<=g_plibretto->m_settings.m_nCommandRepetitions)&&(m_ppDestObj[nDestIndex]->m_socket)); 
		// had to add this while for repetitions.




//										m_ppDestObj[nDestIndex]->m_bSending=false;
//	LeaveCriticalSection(&m_ppDestObj[nDestIndex]->m_critComm);


									} break;
								case 64:// check file on Lyric box
									{
/*
										char pchFileRemote[256];
										sprintf(pchFileRemote, "%s", szFilenameRemote);
	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "*** checking %s on %s", pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)
										unsigned nSize, nTimestamp;

										int nReturn = g_miranda.OxSoxGetFileStats(pchFileRemote, NULL, &nSize, &nTimestamp);
										if((nReturn<OX_SUCCESS)||(nReturn&OX_NOFILE))
										{
											//**MSG
											g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:oxsox_checkfile", 
												"%s not found on %s.", pchFileRemote, pchServer );   Sleep(50);//(Dispatch message)

											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E064:%s not found on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d;",  //HARDCODE
												((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
												pchFileRemote, pchServer, nItemID
												);
										}
										else
										{
											//while(!g_miranda.m_bTransferring) Sleep(1);  //not good, in case error

	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "%s on %s succeeded", pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)
											// we have queued the xfer , lets update the thing with a status, then exit without removing
											
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I064:%s exists on %s.', filename_local = '%d|%d', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
												((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
												pchFileRemote, pchServer, nSize, nTimestamp, nItemID
												);

						/*
						Queue action IDs
1 transfer file to Lyric Box
2 transfer file from Lyric Box
3 delete file from Lyric Box
4 refresh file listing of Lyric Box
19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
32  file is transferring, check for end of transfer
64  check for existence of file
128 give results
256 send telnet command to Lyric Box
						* /
										}
											
	//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
										{
											//**MSG
							//		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

										}

										szServer.ReleaseBuffer();
										return LIBRETTO_ERROR; // we dont want to remove the item at the end of this function
										*/

									} break;
								case 128:// Give results (no action).
									{
										szServer.ReleaseBuffer();
										return LIBRETTO_ERROR; // we dont want to remove the item at the end of this function
										// the requestor process will remove this.
									} break;

								case 1:// transfer to Lyirc box
									{
/*
										if(g_miranda.m_bTransferring) // would like to update queue item with % status.
										{
											// something in progress, must wait. -  just return negative for now
											szServer.ReleaseBuffer();
											return LIBRETTO_ERROR;
										}

										char pchFileLocal[256];
										char pchFileRemote[256];
										sprintf(pchFileLocal, "%s", szFilenameLocal);
										sprintf(pchFileRemote, "%s", szFilenameRemote);
	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "*** transferring %s to %s on %s", pchFileLocal, pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)

										if(g_miranda.OxSoxPutFile(pchFileLocal, pchFileRemote, NULL)<OX_SUCCESS)
										{
											//**MSG
											g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:oxsox_putfile", 
												"Could not transfer %s to %s on %s.", pchFileLocal, pchFileRemote, pchServer );   Sleep(50);//(Dispatch message)

											// and give the info if it was external.
											if(nEventItemID>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E001:Could not transfer %s to %s on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
														((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
														pchFileLocal, pchFileRemote, pchServer,
														nItemID
														);
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
				//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
												return LIBRETTO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function
											}
										}
										else
										{
											//while(!g_miranda.m_bTransferring) Sleep(1);  //not good, in case error

											Sleep(100); // let the thread begin transferring.
	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "*** transfer of %s to %s on %s succeeded", pchFileLocal, pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)
											// we have queued the xfer , lets update the thing with a status, then exit without removing
											
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET action = 32, timestamp = -1.0, message = '1' WHERE itemid = %d",  //HARDCODE
												((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
												nItemID
												);

						/*
						Queue action IDs
1 transfer file to Lyric Box
2 transfer file from Lyric Box
3 delete file from Lyric Box
4 refresh file listing of Lyric Box
19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
32  file is transferring, check for end of transfer
64  check for existence of file
128 give results
256 send telnet command to Lyric Box

						* /

											
	//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
											if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
											{
												//**MSG
	//									g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

											}

											szServer.ReleaseBuffer();
											return LIBRETTO_ERROR;  // because we dont want to increment counter.  // there is no counter.
											// we also dont want to remove the item at the end of this function
										}
										*/
									} break;
								case 2:// transfer from Lyric box
									{
/*
										if(g_miranda.m_bTransferring) // would like to update queue item with % status.
										{
											// something in progress, must wait. -  just return negative for now
											szServer.ReleaseBuffer();
											return LIBRETTO_ERROR;
										}

										char pchFileLocal[256];
										char pchFileRemote[256];
										sprintf(pchFileLocal, "%s", szFilenameLocal);
										sprintf(pchFileRemote, "%s", szFilenameRemote);
										if(g_miranda.OxSoxGetFile(pchFileLocal, pchFileRemote, NULL/*, OX_REPORT* /)<OX_SUCCESS)
										{
											//**MSG
											g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:oxsox_getfile", 
												"Could not transfer %s on %s to %s.", pchFileRemote, pchServer, pchFileLocal );   Sleep(50);//(Dispatch message)

											// and give the info if it was external.
											if(nEventItemID>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E002:Could not transfer %s on %s to %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
														((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
														pchFileRemote, pchServer, pchFileLocal,
														nItemID
														);
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
				//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
												return LIBRETTO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function
											}

										}
										else
										{
											//while(!g_miranda.m_bTransferring) Sleep(1);  //not good, in case error

											Sleep(100); // let the thread begin transferring.
											// we have queued the xfer , lets update the thing with a status, then exit without removing
											
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET action = 32, timestamp = -1.0, message = '2' WHERE itemid = %d",  //HARDCODE
												((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
												nItemID
												);

						/*
1 transfer file to Lyric Box
2 transfer file from Lyric Box
3 delete file from Lyric Box
4 refresh file listing of Lyric Box
19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
32  file is transferring, check for end of transfer
64  check for existence of file
128 give results
256 send telnet command to Lyric Box
						* /

											if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
											{
												//**MSG
							//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

											}

											szServer.ReleaseBuffer();
											return LIBRETTO_ERROR;  // because we dont want to increment counter. // there is no counter.
											// we also dont want to remove the item at the end of this function
										}
										*/
									} break;
								case 3:// delete from Lyric Box
									{
/*
										if(g_miranda.m_bTransferring) // would like to update queue item with % status.
										{
											// something in progress, must wait. -  just return negative for now
											szServer.ReleaseBuffer();
											return LIBRETTO_ERROR;
										}
										char pchFileRemote[256];
										sprintf(pchFileRemote, "%s", szFilenameRemote);
										if(g_miranda.OxSoxDelFile(pchFileRemote, NULL)<OX_SUCCESS)
										{
											//**MSG
											g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:oxsox_delfile", 
												"Could not delete %s on %s.", pchFileRemote, pchServer );   Sleep(50);//(Dispatch message)
											if(nEventItemID>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E003:Could not delete %s on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
														((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
														szFilenameRemote,
														pchServer, nItemID
														);
		//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
									//		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
												return LIBRETTO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function

											}

										}
										else


		//								schedule a refresh
										if((g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_pszServerName)&&(strlen(g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_pszServerName)) && ((g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_ulFlags)&LIBRETTO_FLAG_ENABLED)  )
										{
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (filename_local, filename_remote, action, host, timestamp, username) \
VALUES ('','%s', 4, '%s', -2.0, 'sys' )", //HARDCODE
											((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
											pchDirAlias, g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_pszServerName
											);


			//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", szSQL);  Sleep(250); //(Dispatch message)
			//					char errorstring[DB_ERRORSTRING_LEN];
			//								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
											if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
											{
												//**MSG
			//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

											}

											// and give the info if it was external.
											if(nEventItemID>0)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I003:Finished deleting %s on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
														((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
														szFilenameRemote, 
														pchServer, nItemID
														);
												
		//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
									//		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
												return LIBRETTO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function

											}
										}

*/
									} break;
								case 32:// wait on transfer
									{

/*
										if(g_miranda.m_bTransferring) // would like to update queue item with % status.
										{
											// something in progress, must wait. -  just return negative for now
											szServer.ReleaseBuffer();
											return LIBRETTO_ERROR;
										}

										// if we are here, it's because we have finished a transfer!
		//								schedule a refresh
										if((g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_pszServerName)&&(strlen(g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_pszServerName)) && ((g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_ulFlags)&LIBRETTO_FLAG_ENABLED)  )
										{
											_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (filename_local, filename_remote, action, host, timestamp, username) \
VALUES ('','%s', 4, '%s', -2.0, 'sys' )", //HARDCODE
											((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
											pchDirAlias, g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_pszServerName
											);


			//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", szSQL);  Sleep(250); //(Dispatch message)
			//					char errorstring[DB_ERRORSTRING_LEN];
			//								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
											if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
											{
												//**MSG
			//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

											}

											// and give the info if it was external.
											if(nEventItemID>0)
											{
												int nLibrettoion = atoi(szMessage);
	//						case 2:// transfer from Miranda
	//						case 1:// transfer to Miranda
												if(g_miranda.m_bTransferError) //error!.
												{
													g_miranda.m_bTransferError = false; // reset the error so we can move on
													if(nLibrettoion>0)
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E032:Error transferring %s to %s%s%s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
															(nLibrettoion==1)?szFilenameLocal:szFilenameRemote,
															(nLibrettoion==1)?szFilenameRemote:szFilenameLocal,
															(nLibrettoion==1)?" on ":" from ",
															pchServer, nItemID
															);
													}
													else
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E032:Error transferring %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
															szFilenameRemote,
															pchServer, nItemID
															);

													}
												}
												else
												{
													if(nLibrettoion>0)
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I032:Finished transferring %s to %s%s%s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
															(nLibrettoion==1)?szFilenameLocal:szFilenameRemote,
															(nLibrettoion==1)?szFilenameRemote:szFilenameLocal,
															(nLibrettoion==1)?" on ":" from ",
															pchServer, nItemID
															);
													}
													else
													{
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I032:Finished transferring %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
															szFilenameRemote,
															pchServer, nItemID
															);

													}
												}

	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Transfer SQL: %s", szSQL);  Sleep(250); //(Dispatch message)
	//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
												if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

												}
												szServer.ReleaseBuffer();
												return LIBRETTO_ERROR;  // because we dont want to increment counter.  // there is no counter.
												// we also dont want to remove the item at the end of this function

											}
										}
*/
									} break;
								case 19:// purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
									{
									}  // not break;  want to go on to refresh device listing.
								case 4:// refresh device listing
									{
	//create table Destinations_Media (host varchar(64), file_name varchar(256), 
	//transfer_date int, partition varchar(16), file_size float, 
	//Libretto_local_last_used int, Libretto_local_times_used int);
/*
										// have to do this 3 times, possibly.
	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "case 4");   Sleep(50);//(Dispatch message)
										int nTimes = 3;
										int nDirs = 0;

										if(szFilenameRemote.GetLength()>0)
										{ // explicit dir
											if(szFilenameRemote.Compare("$VIDEO")==0)
											{
												nDirs = 0; nTimes=1;
											}
											else
											if(szFilenameRemote.Compare("$AUDIO")==0)
											{
												nDirs = 1; nTimes=2; // not really 2 times, just have to be nDirs = nTimes-1;
											}
											else
											if(szFilenameRemote.Compare("$FONTS")==0)
											{
												nDirs = 2; nTimes=3; // not really 3 times, just have to be nDirs = nTimes-1;
											}
										}

										for (int nDir=nDirs; nDir<nTimes; nDir++)
										{
		_ftime( &g_plibretto->m_data.m_timebTick );
											PV3DirEntry_ pv3DirEntry;
											int nNumEntries = 0;

											switch(nDir)
											{
											default:
											case 0:	strcpy(pchDirAlias, "$VIDEO"); break;
											case 1:	strcpy(pchDirAlias, "$AUDIO"); break;
											case 2:	strcpy(pchDirAlias, "$FONTS"); break;
											}
											
											if(g_miranda.OxSoxGetDirInfo(pchDirAlias, &pv3DirEntry, &nNumEntries)<OX_SUCCESS)
											{
												//**MSG
												g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:oxsox_dirinfo", 
													"Could not get Librettoory information for %s partition on %s.", pchDirAlias, pchServer );   Sleep(50);//(Dispatch message)

												// and give the info if it was external.
												if(nEventItemID>0)
												{
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E004:Could not get Librettoory information for %s partition on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
															pchDirAlias, pchServer,
															nItemID
															);
													if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
													{
														//**MSG
	//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

													}
													szServer.ReleaseBuffer();
													return LIBRETTO_ERROR;  // because we dont want to increment counter.  // there is no counter.
													// we also dont want to remove the item at the end of this function
												}
											}
											else
											{

												// success, lets update db.
	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "OxSoxGetDirInfo returned >= OX_SUCCESS" );   Sleep(50);//(Dispatch message)


	// the old way was m_pszDestinationMedia.  now we merge on temp table m_pszDestinationMediaTemp
												// first remove everything from the db.  clear out temp db
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
host = '%s' AND \
partition = '%s'",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														pchServer,
														pchDirAlias

													);
												if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
												{
													//**MSG
												}


												int nFile = 0;
												while(nFile<nNumEntries)
												{
			_ftime( &g_plibretto->m_data.m_timebTick );

													char* pchFilename = m_pdb->EncodeQuotes(pv3DirEntry[nFile].FileName);
														// add it.
														_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (\
host, \
file_name, \
transfer_date, \
partition, \
file_size) VALUES ('%s', '%s', %d, '%s', %f)",   //HARDCODE
															((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
															pchServer,
															pchFilename?pchFilename:pv3DirEntry[nFile].FileName,											
															pv3DirEntry[nFile].FileTimeStamp,
															pchDirAlias,
															(double)pv3DirEntry[nFile].FileSize
															);

													// }

													if(pchFilename) free(pchFilename);

													if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
													{
														//**MSG
													}

		/*
													pv3DirEntry[nFile].FileName
													pv3DirEntry[nFile].FileSize,
													pv3DirEntry[nFile].FileAttribs,
													pv3DirEntry[nFile].FileTimeStamp
		* /
													nFile++;
												}
												if(g_miranda.m_lpfnFreeOxSoxPtr) g_miranda.m_lpfnFreeOxSoxPtr(pv3DirEntry);

												// now everything is in the temp table,
												// update the files with info from the temp table


												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET \
%s.transfer_date = %s.transfer_date, \
%s.file_size = %s.file_size \
FROM %s, %s \
WHERE %s.host = '%s' AND \
%s.file_name = %s.file_name \
AND %s.partition = %s.partition",
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														pchServer,
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp"
														);

//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Updating from temp SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
												{
													//**MSG
//g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:debug", "Updating from temp ERROR: %s", dberrorstring);  Sleep(50); //(Dispatch message)
												}


												// insert the ones that are in the temp list that are not in the main media list.
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (host, file_name, transfer_date, partition, file_size, Libretto_local_last_used, Libretto_local_times_used) \
SELECT %s.host, %s.file_name, %s.transfer_date, %s.partition, %s.file_size, 0, 0 FROM \
%s LEFT JOIN (SELECT * FROM %s WHERE host = '%s') AS %s on %s.file_name = %s.file_name and %s.partition = %s.partition WHERE %s.file_name is NULL",
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														pchServer,
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media"
														);

//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Updating from temp SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
												{
													//**MSG
//g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:debug", "Updating from temp ERROR: %s", dberrorstring);  Sleep(50); //(Dispatch message)
												}


												// remove the ones that are no longer in the temp list
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
%s.host = '%s' AND \
%s.partition = '%s' AND \
cast(%s.file_name AS varchar(256)) + ' ' + cast(%s.partition AS varchar(16)) \
NOT IN (SELECT cast(%s.file_name as varchar(256)) + ' ' + cast(%s.partition AS varchar(16)) \
AS fileinfo from %s where host = '%s' and %s.partition = '%s')",
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														pchServer,
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														pchDirAlias,
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														((g_plibretto->m_settings.m_pszDestinationMedia)&&(strlen(g_plibretto->m_settings.m_pszDestinationMedia)))?g_plibretto->m_settings.m_pszDestinationMedia:"Destinations_Media",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														pchServer,
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														pchDirAlias
														);


//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Delete extras from media SQL: %s", szSQL);  Sleep(50); //(Dispatch message)
												if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
												{
													//**MSG
//g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:debug", "Delete extras from media ERROR: %s", dberrorstring);  Sleep(50); //(Dispatch message)
												}



	// clear the temp table
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE \
host = '%s' AND \
partition = '%s'",
														((g_plibretto->m_settings.m_pszDestinationMediaTemp)&&(strlen(g_plibretto->m_settings.m_pszDestinationMediaTemp)))?g_plibretto->m_settings.m_pszDestinationMediaTemp:"Destinations_Media_Temp",
														pchServer,
														pchDirAlias

													);
												if (m_pdb->ExecuteSQL(m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
												{
													//**MSG
												}



												// and give the info if it was external.
												if(nEventItemID>0)
												{
													_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I004:Retrieved Librettoory information for %s partition on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
															((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
															pchDirAlias, pchServer,
															nItemID
															);
													if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
													{
														//**MSG
					//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

													}
													szServer.ReleaseBuffer();
													return LIBRETTO_ERROR;  // because we dont want to increment counter.  // there is no counter.
													// we also dont want to remove the item at the end of this function

												}
											}
										}
										*/
									} break;
								}
								_ftime( &g_plibretto->m_data.m_timebTick );
							}
/*
							else  // could not ping it for some reason.  Have to error out and delete the thing thing
							{

								g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:oxsox_ping", 
									"Could not ping %s.  The request cannot be completed.", pchServer );   Sleep(50);//(Dispatch message)

								// and give the info if it was external.
								if(nEventItemID>0)
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E%03d:Could not ping %s.  The request cannot be completed.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
											((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
											nAction, pchServer,
											nItemID
											);
									if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
									{
										//**MSG
	//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

									}
									//return LIBRETTO_ERROR;  // because we dont want to increment counter.  // there is no counter.
									// we DO want to remove the item at the end of this function
								}
							}
							*/
						}
						else
						{
							//**MSG
							g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:get_queue", "Could not set host to %s", pchServer );  // Sleep(50);//(Dispatch message)

/*
							m_bFailed = true;
							m_timebFailure;

							// lets fail this one out for the failure retry time.
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E%03d:%s is unavailable or inactive, retrying after %d seconds.', timestamp = %d WHERE itemid = %d",  //HARDCODE
									((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
									nAction, pchServer,
									nItemID
									);
//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
							if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
							{
								//**MSG
				//		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

							}

*/

							szServer.ReleaseBuffer();
							return LIBRETTO_ERROR; // we dont want to remove the item at the end of this function

	//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "getting channels again");   Sleep(250);//(Dispatch message)
						}
					}
					else
					{
						//**MSG

						switch(nAction)
						{
						default:
							{
								g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto:Queue", "An event was queued for %s, but that destination is unavailable or inactive.  The event will be ignored and purged from the queue.", pchServer );  // Sleep(50);//(Dispatch message)
							} break;  
						case 2:// transfer from Lyric box
						case 1:// transfer to Lyric box
						case 3:// DELETE FROM Lyric box
						case 64:// check file on Lyric box
						case 256:// telnet command to Lyric box
							{
								g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto:Queue", "An event was queued for %s, but that destination is unavailable or inactive.  The event will be ignored and purged from the queue.", pchServer ); //  Sleep(50);//(Dispatch message)

								if(nEventItemID>0) // means, commanded from external, so give info
								{
									_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E%03d:%s is unavailable or inactive.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
											((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
											nAction, pchServer,
											nItemID
											);
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "SQL: %s", szSQL); // Sleep(250); //(Dispatch message)
									if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
									//if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
									{
										//**MSG
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

									}
									szServer.ReleaseBuffer();
									return LIBRETTO_ERROR; // we dont want to remove the item at the end of this function
								}
							} break;
						}
					}

					szServer.ReleaseBuffer();
				}


				// send remove SQL
				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE itemid = %d",  //HARDCODE
					((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
					nItemID
					);

if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "SQL: %s", szSQL); // Sleep(250); //(Dispatch message)
/*
Queue action IDs
1 transfer to Miranda
2 transfer from Miranda
3 delete from Miranda
4 refresh device listing
19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
32 wait on transfer

Queue table:
		// need to change to filename_local varchar(512), filename_remote varchar(512) to manage hex encoded payloads
//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
//action int, host varchar(64), timestamp float, username varchar(32));
*/

				if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
				{
					//**MSG
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

				}
			}

			return nReturn;
		}
	}
	return LIBRETTO_ERROR;
}

int CLibrettoData::ReturnDestinationIndex(char* pszServerName)
{
	if((pszServerName)&&(strlen(pszServerName))&&(g_plibretto)&&(g_plibretto->m_data.m_ppDestObj)&&(g_plibretto->m_data.m_nNumDestinationObjects))
	{
		int i=0;
		while(i<g_plibretto->m_data.m_nNumDestinationObjects)
		{
			if((g_plibretto->m_data.m_ppDestObj[i])&&(g_plibretto->m_data.m_ppDestObj[i]->m_pszServerName))
			{
				if(strcmp(pszServerName, g_plibretto->m_data.m_ppDestObj[i]->m_pszServerName)==0) return i;
			}
			i++;
		}
	}
	return LIBRETTO_ERROR;
}

int CLibrettoData::GetDestinations(char* pszInfo)
{
	if((g_plibretto)&&(m_pdbConn)&&(m_pdb))
	{
		char szSQL[DB_SQLSTRING_MAXLEN];

		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT * FROM %s AS destinations JOIN \
(SELECT DISTINCT TOP %d host as top_host, flags as top_flags FROM %s ORDER BY flags desc, host) AS top_destinations ON destinations.host = top_destinations.top_host \
ORDER BY destinations.flags DESC, destinations.host",  //HARDCODE
			((g_plibretto->m_settings.m_pszDestinations)&&(strlen(g_plibretto->m_settings.m_pszDestinations)))?g_plibretto->m_settings.m_pszDestinations:"Destinations",
			g_plibretto->m_data.m_nMaxLicensedDevices,
			((g_plibretto->m_settings.m_pszDestinations)&&(strlen(g_plibretto->m_settings.m_pszDestinations)))?g_plibretto->m_settings.m_pszDestinations:"Destinations"
			);
		
/*		
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP %d * FROM %s ORDER BY channelid, host",  //HARDCODE
			g_plibretto->m_data.m_nMaxLicensedDevices,
			((g_plibretto->m_settings.m_pszDestinations)&&(strlen(g_plibretto->m_settings.m_pszDestinations)))?g_plibretto->m_settings.m_pszDestinations:"Destinations");
	*/

if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:GetDestinations", "%s", szSQL); // Sleep(250); //(Dispatch message)

//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "GetDestinations");  Sleep(250); //(Dispatch message)

		CRecordset* prs = m_pdb->Retrieve(m_pdbConn, szSQL, pszInfo);
		if(prs)
		{
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Got Destinations");  Sleep(250); //(Dispatch message)

			char errorstring[MAX_MESSAGE_LENGTH];
			strcpy(errorstring, "");
			int nReturn = LIBRETTO_ERROR;
			int nIndex = 0;
			while ((!prs->IsEOF()))
			{
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:GetDestinations", "index %d", nIndex); // Sleep(250); //(Dispatch message)

// create table Destinations (destinationid int identity(1,1) NOT NULL, host varchar(64) NOT NULL,
//				type int, description varchar(256), diskspace_free float, diskspace_total float, diskspace_threshold real,
//				channelid int, flags int);

		_ftime( &g_plibretto->m_data.m_timebTick );

				CString szHost="";
//				CString szClient="";
				CString szDesc="";
				CString szTemp;
//				double dblDiskspaceFree=-1.0;
//				double dblDiskspaceTotal=-1.0;
				double dblDiskspaceThreshold=-1.0;
				unsigned long ulFlags;   // various flags
				unsigned short usType;
				int nChannelid=-1;
				int nTemp = -1;
				int nItemID = -1;
				bool bItemFound = false;
				bool bFlagsFound = false;
				bool bFound = false;
				try
				{
					prs->GetFieldValue("destinationid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bItemFound = true;
						nItemID = atoi(szTemp);
					}
					prs->GetFieldValue("host", szHost);//HARDCODE
					szHost.TrimLeft(); szHost.TrimRight();
					prs->GetFieldValue("type", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						usType = (unsigned short)atol(szTemp);
					}
					prs->GetFieldValue("description", szDesc);//HARDCODE
					prs->GetFieldValue("diskspace_threshold", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						dblDiskspaceThreshold = atof(szTemp);
					}
					prs->GetFieldValue("channelid", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						nChannelid = atoi(szTemp);
					}
					prs->GetFieldValue("flags", szTemp);//HARDCODE
					if(szTemp.GetLength())
					{
						bFlagsFound = true;
						ulFlags = atol(szTemp);
					}
				}
				catch(CException *e)// CDBException *e, CMemoryException *m)  
				{
					if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
/*
						if(m_pmsgr)
						{
							char errorstring[DB_ERRORSTRING_LEN];
							_snprintf(errorstring, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
							Message(MSG_PRI_HIGH|MSG_ICONERROR, errorstring, "DBUtil:Retrieve");
						}
*/
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:GetDestinations", "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL); // Sleep(250); //(Dispatch message)
					}
					else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
					{
						// The error code is in e->m_nRetCode
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception: out of memory\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught exception: out of memory", "DBUtil:Retrieve");
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:GetDestinations", "Retrieve: Caught exception: out of memory\n%s", szSQL); // Sleep(250); //(Dispatch message)
					}
					else 
					{
						if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught other exception.\n%s", szSQL);
//							Message(MSG_PRI_HIGH|MSG_ICONERROR, "Retrieve: Caught other exception", "DBUtil:Retrieve");
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:GetDestinations", "Retrieve: Caught other exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)
					}
					e->Delete();
				} 
				catch( ... )
				{
					if(pszInfo) _snprintf(pszInfo, DB_ERRORSTRING_LEN-1, "Retrieve: Caught exception.\n%s", szSQL);

if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:GetDestinations", "Retrieve: Caught exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)

				}
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Got Destination: %s (0x%08x)", szHost, ulFlags);  Sleep(250); //(Dispatch message)

				if((g_plibretto->m_data.m_ppDestObj)&&(g_plibretto->m_data.m_nNumDestinationObjects))
				{
					nTemp=0;
					while(nTemp<g_plibretto->m_data.m_nNumDestinationObjects)
					{
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "c2");  Sleep(250); //(Dispatch message)
						if((g_plibretto->m_data.m_ppDestObj[nTemp]))//&&(g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszServerName))
						{
							// changing the follwing to unique on dest id.
				//			if((szHost.GetLength()>0)&&(szHost.CompareNoCase(g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszServerName)==0))
							if(g_plibretto->m_data.m_ppDestObj[nTemp]->m_nItemID == nItemID)
							{
								bFound = true;
								bool bRefresh = false;

								if(
										(szDesc.GetLength()>0)
									&&((g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszDesc==NULL)||(szDesc.CompareNoCase(g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszDesc)))
									)
								{
									if(g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszDesc) free(g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszDesc);

									g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
									if(g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszDesc) sprintf(g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszDesc, szDesc);
								}

								if(
										(szHost.GetLength()>0)
									&&((g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszServerName==NULL)||(szHost.CompareNoCase(g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszServerName)))
									)
								{
									if(g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszServerName) free(g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszServerName);

									g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszServerName = (char*)malloc(szHost.GetLength()+1); 
									if(g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszServerName) sprintf(g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszServerName, szHost);
								}

								if(bItemFound) g_plibretto->m_data.m_ppDestObj[nTemp]->m_nItemID = nItemID;
								g_plibretto->m_data.m_ppDestObj[nTemp]->m_usType = usType;

								if(dblDiskspaceThreshold>0.0) g_plibretto->m_data.m_ppDestObj[nTemp]->m_dblDiskPercent = dblDiskspaceThreshold;
//								g_plibretto->m_data.m_ppDestObj[nTemp]->m_dblDiskKBFree = dblDiskspaceFree;
//								g_plibretto->m_data.m_ppDestObj[nTemp]->m_dblDiskKBTotal = dblDiskspaceTotal;

//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "c3");  Sleep(250); //(Dispatch message)

								if(
										((bFlagsFound)&&(!(ulFlags&LIBRETTO_FLAG_ENABLED))&&((g_plibretto->m_data.m_ppDestObj[nTemp]->m_ulFlags)&LIBRETTO_FLAG_ENABLED))
									)
								{
									g_plibretto->m_data.m_ppDestObj[nTemp]->m_bKillMonThread = true;

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
										g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszDesc?g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszDesc:g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszServerName);  
									g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto:destination_change", errorstring);  Sleep(20);  //(Dispatch message)
									g_plibretto->m_data.m_ppDestObj[nTemp]->m_bKillConnThread = true;
									g_plibretto->m_data.m_ppDestObj[nTemp]->m_ulStatus = LIBRETTO_STATUS_NOTCON;
									if(g_plibretto->m_data.m_ppDestObj[nTemp]->m_socket != NULL)
									{
										g_plibretto->m_data.m_ppDestObj[nTemp]->m_net.CloseConnection(g_plibretto->m_data.m_ppDestObj[nTemp]->m_socket); // re-establish
									}
									g_plibretto->m_data.m_ppDestObj[nTemp]->m_socket = NULL;

								}

								if(
										((bFlagsFound)&&(ulFlags&LIBRETTO_FLAG_ENABLED)&&(!((g_plibretto->m_data.m_ppDestObj[nTemp]->m_ulFlags)&LIBRETTO_FLAG_ENABLED)))
									)
								{
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating %s...", 
//										g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszClientName?g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszClientName:"Libretto",  
										g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszDesc?g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszDesc:g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszServerName);  
									g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto:destination_change", errorstring); //  Sleep(20);  //(Dispatch message)
//									g_plibretto->m_data.m_ppDestObj[nTemp]->m_pDlg->OnButtonConnect();
									//**** if connect set following status


									if((g_plibretto->m_data.m_ppDestObj[nTemp]->m_socket == NULL)&&(!g_plibretto->m_data.m_ppDestObj[nTemp]->m_bConnThreadStarted))
									{
										g_plibretto->m_data.m_ppDestObj[nTemp]->m_ulStatus = LIBRETTO_STATUS_CONN;
										if(_beginthread(LibrettoAsynchConnThread, 0, (void*)(g_plibretto->m_data.m_ppDestObj[nTemp]))==-1)
										{
										//error.

										//**MSG
										}
										else
										{
											int nWait = 0;
											while((g_plibretto->m_data.m_ppDestObj[nTemp]->m_ulStatus == LIBRETTO_STATUS_CONN)&&(nWait<1000))  // 0.5 seconds?
											{
												nWait++;
												Sleep(1);
											}
										}
									}

									if(!g_plibretto->m_data.m_ppDestObj[nTemp]->m_bMonThreadStarted)
									{
										g_plibretto->m_data.m_ppDestObj[nTemp]->m_bKillMonThread = false;
										if(_beginthread(LibrettoConnMonitorThread, 0, (void*)(g_plibretto->m_data.m_ppDestObj[nTemp]))==-1)
										{
										//error.
									g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:destination_change", "Error starting connection monitor for %s", 
										g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszDesc?g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszDesc:g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszServerName); //  Sleep(20);  //(Dispatch message)

										//**MSG
										}
									}


									if(!g_plibretto->m_data.m_ppDestObj[nTemp]->m_bCommandQueueThreadStarted)
									{
										g_plibretto->m_data.m_ppDestObj[nTemp]->m_bKillCommandQueueThread = false;
										if(_beginthread(LibrettoCommandQueueThread, 0, (void*)(g_plibretto->m_data.m_ppDestObj[nTemp]))==-1)
										{
										//error.
									g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:destination_change", "Error starting command queue for %s", 
										g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszDesc?g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszDesc:g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszServerName); //  Sleep(20);  //(Dispatch message)

										//**MSG
										}
									}


									

/*
									if(g_plibretto->m_data.m_ppDestObj[nTemp]->m_socket == NULL)
									{
										g_plibretto->m_data.m_ppDestObj[nTemp]->m_net.OpenConnection(g_plibretto->m_data.m_ppDestObj[nTemp]->m_pszServerName, g_plibretto->m_settings.m_nPort, &g_plibretto->m_data.m_ppDestObj[nTemp]->m_socket); // re-establish
										char tnbuffer[32]; sprintf(tnbuffer, "\\\\%c%c", 13,10);
										g_plibretto->m_data.m_ppDestObj[nTemp]->SendTelnetCommand(tnbuffer, 2);
										unsigned char* pucBuffer = NULL;
										unsigned long ulBufferLen = 256;
										g_plibretto->m_data.m_ppDestObj[nTemp]->m_net.GetLine(&pucBuffer, &ulBufferLen, g_plibretto->m_data.m_ppDestObj[nTemp]->m_socket);
										if(pucBuffer) free(pucBuffer);
									}
*/
									bRefresh = true;
//									g_plibretto->m_data.m_ppDestObj[nTemp]->m_bKillConnThread = false;
	//								if(_beginthread(LibrettoConnectionThread, 0, (void*)g_plibretto->m_data.m_ppDestObj[nTemp])<0)
	//								{
										//error.

										//**MSG
							
										
		//							}
									//**** should check return value....

									g_plibretto->m_data.m_ppDestObj[nTemp]->m_ulStatus = LIBRETTO_STATUS_CONN;
								}

								if(bFlagsFound) g_plibretto->m_data.m_ppDestObj[nTemp]->m_ulFlags = ulFlags|LIBRETTO_FLAG_FOUND;
								else g_plibretto->m_data.m_ppDestObj[nTemp]->m_ulFlags |= LIBRETTO_FLAG_FOUND;

								if(bRefresh)g_plibretto->m_data.m_ppDestObj[nTemp]->m_ulFlags |= LIBRETTO_FLAG_REFRESH;

							}
						}
						nTemp++;
					}
				}

				if((!bFound)&&(szHost.GetLength()>0)) // have to add.
				{
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "adding %s", szHost);  Sleep(250); //(Dispatch message)
					CLibrettoDestinationObject* pscno = new CLibrettoDestinationObject;
					if(pscno)
					{
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "new obj for %s", szHost);  Sleep(250); //(Dispatch message)
						CLibrettoDestinationObject** ppObj = new CLibrettoDestinationObject*[g_plibretto->m_data.m_nNumDestinationObjects+1];
						if(ppObj)
						{
							// now that the array is OK, also initiate any new external support objects.

							switch(usType)
							{
							case CX_DESTTYPE_EVERTZ_7847FSE:
								{
									pscno->m_pvExternal = new CSCTE104Util; 
									if(pscno->m_pvExternal)
									{
										//CSCTE104Util* p104 = (CSCTE104Util*)pscno->m_pvExternal; // for convenience.
									}
									else
									{
										// log a problem.
									}
								} break;
							default:
								{
									pscno->m_pvExternal=NULL; 
								} break;
							}

							

//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "new array for %s", szHost);  Sleep(250); //(Dispatch message)
							int o=0;
							if((g_plibretto->m_data.m_ppDestObj)&&(g_plibretto->m_data.m_nNumDestinationObjects>0))
							{
								while(o<g_plibretto->m_data.m_nNumDestinationObjects)
								{
									ppObj[o] = g_plibretto->m_data.m_ppDestObj[o];
									o++;
								}
								delete [] g_plibretto->m_data.m_ppDestObj;

							}
							ppObj[o] = pscno;
							g_plibretto->m_data.m_ppDestObj = ppObj;
							g_plibretto->m_data.m_nNumDestinationObjects++;

							ppObj[o]->m_pszServerName = (char*)malloc(szHost.GetLength()+1); 
							if(ppObj[o]->m_pszServerName) sprintf(ppObj[o]->m_pszServerName, "%s", szHost);

/*							
							if(szClient.GetLength()<=0)
							{
								szClient = "Libretto";
							}

							if(szClient.GetLength()>0)
							{
								ppObj[o]->m_pszClientName = (char*)malloc(szClient.GetLength()+1); 
								if(ppObj[o]->m_pszClientName) sprintf(ppObj[o]->m_pszClientName, "%s", szClient);
							}
*/
							if(szDesc.GetLength()>0)
							{
								ppObj[o]->m_pszDesc = (char*)malloc(szDesc.GetLength()+1); 
								if(ppObj[o]->m_pszDesc) sprintf(ppObj[o]->m_pszDesc, "%s", szDesc);
							}
							if(bItemFound) ppObj[o]->m_nItemID = nItemID;
							ppObj[o]->m_usType = usType;

							if(dblDiskspaceThreshold>0.0) ppObj[o]->m_dblDiskPercent = dblDiskspaceThreshold;
//							ppObj[o]->m_dblDiskKBFree = dblDiskspaceFree;
//							ppObj[o]->m_dblDiskKBTotal = dblDiskspaceTotal;

//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "adding flags: %s (0x%08x)", szHost, ulFlags);  Sleep(250); //(Dispatch message)

							if(bFlagsFound) ppObj[o]->m_ulFlags = ulFlags|LIBRETTO_FLAG_FOUND|LIBRETTO_FLAG_NEW;
							else ppObj[o]->m_ulFlags |= LIBRETTO_FLAG_FOUND|LIBRETTO_FLAG_NEW;
								
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", " added flags: %s (0x%08x) %d", szHost, ppObj[o]->m_ulFlags, ((ppObj[o]->m_ulFlags)&LIBRETTO_FLAG_ENABLED));  Sleep(250); //(Dispatch message)

							if((ppObj[o]->m_ulFlags)&LIBRETTO_FLAG_ENABLED)
							{
								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Activating %s...", 
										ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName);  
								g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto:destination_add", errorstring);  // Sleep(20);  //(Dispatch message)

								//**** if connect 
								if((ppObj[o]->m_socket == NULL)&&(!ppObj[o]->m_bConnThreadStarted))
								{
									ppObj[o]->m_ulStatus = LIBRETTO_STATUS_CONN;
									if(_beginthread(LibrettoAsynchConnThread, 0, (void*)(ppObj[o]))==-1)
									{
									//error.

									//**MSG
									}
									else
									{
										int nWait = 0;
										while((ppObj[o]->m_ulStatus == LIBRETTO_STATUS_CONN)&&(nWait<1000))  // 0.5 seconds?
										{
											nWait++;
											Sleep(1);
										}
									}
								}

								if(!ppObj[o]->m_bMonThreadStarted)
								{
									ppObj[o]->m_bKillMonThread = false;
									if(_beginthread(LibrettoConnMonitorThread, 0, (void*)(ppObj[o]))==-1)
									{
									//error.
								g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:destination_add", "Error starting connection monitor for %s", 
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName); //  Sleep(20);  //(Dispatch message)

									//**MSG
									}
								}

								if(!ppObj[o]->m_bCommandQueueThreadStarted)
								{
//	g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:destination_add", "Starting command queue for %s", ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName);
									ppObj[o]->m_bKillCommandQueueThread = false;
									if(_beginthread(LibrettoCommandQueueThread, 0, (void*)(ppObj[o]))==-1)
									{
									//error.
								g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:destination_add", "Error starting command queue for %s", 
									ppObj[o]->m_pszDesc?ppObj[o]->m_pszDesc:ppObj[o]->m_pszServerName); //  Sleep(20);  //(Dispatch message)

									//**MSG
									}
								}




/*
								if(ppObj[o]->m_socket == NULL)
								{
									ppObj[o]->m_net.OpenConnection(ppObj[o]->m_pszServerName, g_plibretto->m_settings.m_nPort, &ppObj[o]->m_socket); // re-establish
									char tnbuffer[32]; sprintf(tnbuffer, "\\\\%c%c", 13,10);
									ppObj[o]->SendTelnetCommand(tnbuffer, 2);
									unsigned char* pucBuffer = NULL;
									unsigned long ulBufferLen = 256;
									ppObj[o]->m_net.GetLine(&pucBuffer, &ulBufferLen, ppObj[o]->m_socket);
									if(pucBuffer) free(pucBuffer);
								}
*/
								// create a connection object in g_adc.
								// associate that with the new CLibrettoConnectionObject;
								// start the thread.
//								ppObj[o]->m_bKillConnThread = false;

								ppObj[o]->m_ulStatus = LIBRETTO_STATUS_CONN;
							}

						}
						else
							delete pscno;
					}
				}


				nIndex++;
				prs->MoveNext();
			}

			nReturn = nIndex;
			prs->Close();

			delete prs;
//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "mid GetConnections 2");  Sleep(250); //(Dispatch message)

			// have to stop and remove any that have now been removed.
			// and insert media list refresh into the queue for all new destinatons.
			nIndex = 0;
			while(nIndex<g_plibretto->m_data.m_nNumDestinationObjects)
			{
		_ftime( &g_plibretto->m_data.m_timebTick );
				if((g_plibretto->m_data.m_ppDestObj)&&(g_plibretto->m_data.m_ppDestObj[nIndex]))
				{

					if((g_plibretto->m_data.m_ppDestObj[nIndex]->m_ulFlags)&LIBRETTO_FLAG_FOUND)
					{
						(g_plibretto->m_data.m_ppDestObj[nIndex]->m_ulFlags) &= ~LIBRETTO_FLAG_FOUND;

						if((g_plibretto->m_data.m_ppDestObj[nIndex]->m_ulFlags)&(LIBRETTO_FLAG_NEW|LIBRETTO_FLAG_REFRESH))
						{
							(g_plibretto->m_data.m_ppDestObj[nIndex]->m_ulFlags) &= ~(LIBRETTO_FLAG_REFRESH|LIBRETTO_FLAG_NEW);
							// refresh media.
// insert an immediate destinations media refresh into the queue.
/*
Queue action IDs
1 transfer to Lyric Box
2 transfer from Lyric Box
3 delete from Lyric Box
4 refresh device listing
19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)

*/

							if((g_plibretto->m_data.m_ppDestObj[nIndex]->m_pszServerName)&&(strlen(g_plibretto->m_data.m_ppDestObj[nIndex]->m_pszServerName)) && ((g_plibretto->m_data.m_ppDestObj[nIndex]->m_ulFlags)&LIBRETTO_FLAG_ENABLED)  )
							{
								_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (local, remote, action, host, timestamp, username) \
VALUES ('','', 4, '%s', 0, 'sys' )", //HARDCODE
								((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
								g_plibretto->m_data.m_ppDestObj[nIndex]->m_pszServerName
								);


//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", szSQL);  Sleep(250); //(Dispatch message)
//					char errorstring[DB_ERRORSTRING_LEN];

								
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "SQL: %s", szSQL); // Sleep(250); //(Dispatch message)
								
								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, errorstring)<DB_SUCCESS)
//								if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
								{
									//**MSG
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

								}

// deal with the diskspace info 
							// actually, do this on any transaction from queue.

							}
						}
					
						nIndex++;
					}
					else
					{
						if(g_plibretto->m_data.m_ppDestObj[nIndex])
						{
							int nWait =  clock()+1500;
							if((g_plibretto->m_data.m_ppDestObj[nIndex]->m_ulFlags)&LIBRETTO_FLAG_ENABLED)
							{
								g_plibretto->m_data.m_ppDestObj[nIndex]->m_bKillCommandQueueThread = true;
								g_plibretto->m_data.m_ppDestObj[nIndex]->m_bKillMonThread = true;
								g_plibretto->m_data.m_ppDestObj[nIndex]->m_bKillConnThread = true;

								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Deactivating %s...", 
									g_plibretto->m_data.m_ppDestObj[nIndex]->m_pszDesc?g_plibretto->m_data.m_ppDestObj[nIndex]->m_pszDesc:g_plibretto->m_data.m_ppDestObj[nIndex]->m_pszServerName);  
								g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto:destination_remove", errorstring);  // Sleep(20);  //(Dispatch message)
//									g_plibretto->m_data.m_ppDestObj[nTemp]->m_pDlg->OnDisconnect();


								//**** disconnect
//								while(g_plibretto->m_data.m_ppDestObj[nIndex]->m_bConnThreadStarted) Sleep(1);

								while(
											 (
										     (g_plibretto->m_data.m_ppDestObj[nIndex]->m_bMonThreadStarted)
											 ||(g_plibretto->m_data.m_ppDestObj[nIndex]->m_bConnThreadStarted)
											 ||(g_plibretto->m_data.m_ppDestObj[nIndex]->m_bCommandQueueThreadStarted)
											 )
											&&
											 (clock()<nWait)
										 )
								{
									Sleep(1);
								}


								//g_adc.DisconnectServer(g_plibretto->m_data.m_ppDestObj[nIndex]->m_pszServerName);
								//**** should check return value....
								if(g_plibretto->m_data.m_ppDestObj[nIndex]->m_socket != NULL)
								{
									g_plibretto->m_data.m_ppDestObj[nIndex]->m_net.CloseConnection(g_plibretto->m_data.m_ppDestObj[nIndex]->m_socket); // re-establish
								}
								g_plibretto->m_data.m_ppDestObj[nIndex]->m_socket = NULL;

								g_plibretto->m_data.m_ppDestObj[nIndex]->m_ulStatus = LIBRETTO_STATUS_NOTCON;
							}
					

							while((g_plibretto->m_data.m_ppDestObj[nIndex]->m_bMonThreadStarted)&&(clock()<nWait))
							{
								Sleep(1);
							}

							if(!(g_plibretto->m_data.m_ppDestObj[nIndex]->m_bMonThreadStarted)) delete g_plibretto->m_data.m_ppDestObj[nIndex];
							g_plibretto->m_data.m_nNumDestinationObjects--;

							int nTemp=nIndex;
							while(nTemp<g_plibretto->m_data.m_nNumDestinationObjects)
							{
								g_plibretto->m_data.m_ppDestObj[nTemp]=g_plibretto->m_data.m_ppDestObj[nTemp+1];
								nTemp++;
							}
							g_plibretto->m_data.m_ppDestObj[nTemp] = NULL;
						}
						else nIndex++;
					}
				}
				else
					nIndex++;
			}

//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "leaving GetConnections 1");  Sleep(250); //(Dispatch message)
			return nReturn;
		}
	}
//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "leaving GetConnections 2");  Sleep(250); //(Dispatch message)

	return LIBRETTO_ERROR;
}

