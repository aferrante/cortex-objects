// LibrettoSettings.h: interface for the CLibrettoSettings class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LIBRETTOSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
#define AFX_LIBRETTOSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "LibrettoDefines.h"
#include "../../Common/MFC/ODBC/DBUtil.h"

class CLibrettoSettings  
{
public:
	CLibrettoSettings();
	virtual ~CLibrettoSettings();

	int GetFromDatabase(char* pszInfo=NULL);  //only get.  Interface publishes
	int Settings(bool bRead);
	int GetPortOverride(int nType);
	char* ProcessString(char* pszString, bool bFreeIncomingString);

	CDBUtil* m_pdb;
	CDBconn* m_pdbConn;

	char* m_pszName;  // familiar name of this instance (and therefore the name of the Libretto database)
	char* m_pszType;  // familiar name of the type.
	char* m_pszProject;  // familiar name of the project name.
	unsigned long m_ulMainMode;

	// ports
	unsigned short m_usCommandPort;
	unsigned short m_usStatusPort;

	int m_nThreadDwellMS;
	bool m_bDebugLists;  // if true, dumps the temp and events databases
	int  m_nDebugListTopCount;  //if positive, just prints out the top n recored in the events tables.
	bool m_bDebugSQL;  // if true, prints out the SQL calls, all but event inserts
	bool m_bDebugInsertSQL;  // if true, prints out the event insert SQL calls
	unsigned long m_ulDebug;  // prints out debug statements that & with this.
	char* m_pszDebugOrder; // the order by clause for debug

	// The following flags determine what settings are loaded. 
	// The specifics do not need to be stored in cortex settings, they are pushed to the various objects.

	// messaging for Libretto
	bool m_bUseLog;			// write a log file
	bool m_bUseEmail;		// send an email on failures (or commanded from remotes)
	bool m_bUseNetwork; // send a message to a remote host
	bool m_bReportSuccessfulOperation;
	bool m_bLogTransfers;
	char* m_pszFileSpec;
	char* m_pszMailSpec;
	char* m_pszProcessedFileSpec;
	char* m_pszProcessedMailSpec;
	bool m_bUseXMLClientLog;			// write a log file
	bool m_bLogNetworkErrors; // enables messaging into network object (logs socket errors, etc).
	bool m_bMillisecondMessaging;			// use millisecond resolution for messages and asrun

	int m_nAutoPurgeMessageDays;
//	int m_nAutoPurgeAsRunDays;
	int m_nAutoPurgeInterval;

	// Harris API
//	bool m_bUseListCount; // get all events up until the list count (otherwise just up to the lookahead)

	bool m_bDiReCTInstalled; // the DiReCT module is installed.
	bool m_bUseTimeCode;			// use adrienne timecode
	bool m_bTimeCodeSync;  // jam sync the clock
	int m_nSyncThresholdMS;  // sync if evaluates to outside this threshold
	int m_nSyncIntervalMS;   // evaluate sync difference at this interval
	int m_nFrameRate;   // for time code addresses NTSC=30, PAL=25
	bool m_bDF;  // Drop frame


	int m_nCommandRepetitions;

	int m_nPort;
	int m_nDefaultSendTimeoutMS;
	int m_nDefaultReceiveTimeoutMS;

	int m_nFilenameLocalWidth;
	int m_nFilenameRemoteWidth;
	int m_nMessageWidth;

	int m_nQueueIntervalMS;
	int m_nCommandQueueIntervalMS;
	bool m_bEnableQueue;
	bool m_bEnableCommandQueue;
	int m_nMemQueueIncrSize;

	char* m_pszTypePortOverride;  // format: type_decimal:port|type_decimal:port etc, like 3003:1337|5000:9091

	// database stuff
	char* m_pszDSN;
	char* m_pszUser;
	char* m_pszPW;

	char* m_pszSettings;  // the Settings table name
	char* m_pszExchange;  // the Exchange table name
	char* m_pszMessages;  // the Messages table name

	char* m_pszDestinationMediaTemp;  // the DestinationMediaTemp table name
	char* m_pszDestinationMedia;  // the DestinationMedia table name
	char* m_pszDestinations;  // the Destinations table name
	char* m_pszQueue;  // the Queue table name
	char* m_pszCommandQueue;  // the CommandQueue table name -  not actually used currently

	int m_nClearQueueIfDisconnectedMS;  // number of milliseconds of tolerated disconnection befor the Queue is cleared.
	// really, need the same thing for Command Queue as well, but that not in use.

// needed for internal commands to ChBx, such as handled inside command handler thread.
	bool m_bUseUTF8; // use UTF8, not ISO
	char* m_pszDelim;
	char* m_pszDownloadedThumbnailPath;
	char* m_pszDownloadedThumbnailExtension;


	CRITICAL_SECTION m_crit;

	unsigned long m_ulModsIntervalMS;  // interval on which to check database mods

	char* m_pszLicense;  // the License Key
	char* m_pszIconPath;  // the Path to the HTML Icon.  (just the path)   the icon must be called "status.gif" in that path, statusR.gif, statusY.gif, statusG.gif, statusB.gif must all exist.
};

#endif // !defined(AFX_LIBRETTOSETTINGS_H__206185BA_B2AD_4B62_B01D_3053EABE3ADF__INCLUDED_)
