// LibrettoMain.cpp: implementation of the CLibrettoMain class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"  // just for use of status windows
#include "Libretto.h"  // just included to have access to windowing environment
#include "LibrettoDlg.h"  // just included to have access to windowing environment
#include "LibrettoHandler.h"  // just included to have access to windowing environment

#include "LibrettoMain.h"
#include <process.h>
#include <direct.h>
#include "../../Common/TXT/BufferUtil.h"
#include "../../Common/MFC/ODBC/DBUtil.h"
//#include "../../Common/API/Harris/ADC.h"
//#include "../../Common/API/Miranda/IS2Comm.h"
#include <sys/timeb.h>
#include <time.h>
#include <direct.h>
#include <objsafe.h>
#include <atlbase.h>

// have to add rpcrt4.lib to the linker for UUID stuff


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// globals
bool g_bKillThread=false;
bool g_bKillStatus = false;
bool g_bThreadStarted=false;
CLibrettoMain* g_plibretto=NULL;
//CADC g_adc;
//CIS2Comm g_miranda;  // global miranda object

extern CMessager* g_pmsgr;  // from Messager.cpp
extern CLibrettoApp theApp;

void LibrettoCommandQueueThread(void* pvArgs);
void LibrettoAsynchThumbnailRetrieveThread(void* pvArgs);
void TimeCodeDataThread(void* pvArgs);


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLibrettoMain::CLibrettoMain()
{
	m_pdbConnMsg = NULL;
}

CLibrettoMain::~CLibrettoMain()
{
}

/*

char*	CLibrettoMain::LibrettoTranslate(CHTTPHeader* pHeader, char* pszBuffer)			// apply libretto scripting language
{
	return pszBuffer;
}

int		CLibrettoMain::InterpretDirective(CHTTPHeader* pHeader, void** ppResult, char* pszInfo)	// parse cgi
{
	//the void** allows you to get a pointer to a result of the directive, such as a char buffer.
	return LIBRETTO_SUCCESS;
}
*/

SOCKET*	CLibrettoMain::SendClientRequest(char* pchHost, unsigned short usPort, CNetData* pReturnData, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)  // libretto initiates a request to an object server
{
	CNetData* pdata = new CNetData;

	pdata->m_ucType = ucType;      // defined type - indicates which protocol to use, structure of data

	pdata->m_ucCmd = ucCmd;       // the command byte
	pdata->m_ucSubCmd = ucSubCmd;       // the subcommand byte

	pdata->m_pucData = pucData;
	pdata->m_ulDataLen = ulDataLen;

	SOCKET* ps = NULL; 
	m_net.SendData(pdata, pchHost, usPort, 5000, 0, NET_SND_CMDTOSVR, ps);
//	m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);

	return ps;
}

int		CLibrettoMain::SendClientReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// libretto replies to an object server after receiving data. (usually ack or nak)
{
	return LIBRETTO_SUCCESS;
}

int		CLibrettoMain::SendServerReply(SOCKET s, unsigned char ucType, unsigned char ucCmd, unsigned char ucSubCmd, unsigned char* pucData, unsigned long ulDataLen, char* pchUser, char* pchPw)		// libretto answers a request from an object client
{
	return LIBRETTO_SUCCESS;
}


void LibrettoMainThread(void* pvArgs)
{
	CLibrettoApp* pApp = (CLibrettoApp*)pvArgs; // pointer to the main app object.
	// really we only need this to understand when we get external commands to exit, from the windowing environment
	//startup.
	g_bThreadStarted = true;

	//create the main objects.

	CLibrettoMain libretto;
	CDBUtil db;
	CDBUtil dbmsg;

	libretto.m_data.m_ulFlags &= ~LIBRETTO_STATUS_THREAD_MASK;
	libretto.m_data.m_ulFlags |= LIBRETTO_STATUS_THREAD_START;
	libretto.m_data.m_ulFlags &= ~LIBRETTO_ICON_MASK;
	libretto.m_data.m_ulFlags |= LIBRETTO_STATUS_UNINIT;

	libretto.m_data.GetHost();

	g_plibretto = &libretto;


	char pszCurrentDir[MAX_PATH+2];  // store the working dir to always check that it is current.
//	char pszPath[MAX_PATH+2]; // just a string for temp paths
	if(GetCurrentDirectory(MAX_PATH, pszCurrentDir)==0) //fail
		strcpy(pszCurrentDir, "C:\\Libretto\\"); // default.
	else
	{
		int nLen = strlen(pszCurrentDir);
		if(pszCurrentDir[nLen-1] != '\\')
		{
			pszCurrentDir[nLen] = '\\';
			pszCurrentDir[nLen+1] = 0;  // new zero term
		}
	}



//AfxMessageBox("x");
	char errorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");

//		AfxMessageBox( pApp->m_lpCmdLine );

	// parse the command line to see if there are any overrides.
  if (pApp->m_lpCmdLine[0] != '\0')
  {
    // TODO: add command line processing here
		char* pch = strtok(pApp->m_lpCmdLine, " \t,"); // whitespace and commas
		while (pch!=NULL)
		{
			// check here for Cortex IP.
			if(strlen(pch))
			{
//		AfxMessageBox( pch );
				if(libretto.m_data.m_bu.CountChar(pch, strlen(pch), ':')>=1)  // must be of form "hostname:cmdport" where hostname can be name or ip
				{
					libretto.m_data.m_pszCortexHost = (char*)malloc(strlen(pch)+1);
					if(libretto.m_data.m_pszCortexHost)
					{
						strcpy(libretto.m_data.m_pszCortexHost, pch);
//		AfxMessageBox( libretto.m_data.m_pszCortexHost );

						char* pchd = strchr(libretto.m_data.m_pszCortexHost, ':');
						if(pchd)
						{
							*pchd = 0;  // null term the host name;
							pchd++;
//		AfxMessageBox( libretto.m_data.m_pszCortexHost );

							char* pchd2 = strchr(pchd, ':');
							if(pchd2)
							{
								*pchd2 = 0;  // null term the cmd port;
								pchd2++;
								if(strlen(pchd2)) libretto.m_data.m_usCortexStatusPort = atoi(pchd2);
							}

							if(strlen(pchd)) libretto.m_data.m_usCortexCommandPort = atoi(pchd);
						}
					}

				}
			}
			pch = strtok(NULL, " \t,"); // whitespace and commas
		}
  }

	// get settings.
	libretto.m_settings.Settings(true); //read


	bool bUseLog = false;
	bool bUseEmail = false;
	bool bLogNetworkErrors = false;
	if(libretto.m_settings.m_bUseLog)
	{
		bUseLog = libretto.m_settings.m_bUseLog;

		// for logfiles, we need params, and they must be in this format:
		//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs
//		pszParams = file.GetIniString("Messager", "LogFileIni", "Libretto|YD||1|");
		int nRegisterCode=0;

		nRegisterCode = libretto.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
			"log", 
			libretto.m_settings.m_pszProcessedFileSpec?libretto.m_settings.m_pszProcessedFileSpec:libretto.m_settings.m_pszFileSpec, 
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
			libretto.m_data.SetStatusText(errorstring, (LIBRETTO_STATUS_FAIL_LOG|LIBRETTO_STATUS_ERROR));
		}
	}

	libretto.m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "----------------------------------------------------\n\
-------------- Libretto %s start --------------", LIBRETTO_CURRENT_VERSION);  //(Dispatch message)

	if(libretto.m_settings.m_bUseEmail)
	{
		bUseEmail = libretto.m_settings.m_bUseEmail;
				// for smtp, we need params, and they must be in this format:
				// SMTP address[: override port (optional)] | distribution lists | from email | subject spec | localhostname | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs
				// note on distribution lists:
				// we want to give the option to map message type and severity to different address lists.
				// the distribution lists field format is this:
				// ff:x@x.com,q@q.com;0x:B@b.com
				// the : delimits the type and severity flag specifier from the email address list for that set. 
				// the email addresses are delimited by comma
				// the sets are delimited by ;
				// the delimiters are not allowed in addresses, as described here: http://en.wikipedia.org/wiki/E-mail_address
				// on the type and severity specifiers, it is like this:
				// the subfield is a 2 character hex code, sort of.
				// severity is the first char, possible values are 0,1,2,3,4, for normal, low, medium, high, critical
				// additionally, f means all
				// type is the second char, possible values are 0-8, for the follwing:
				// #define MSG_ICONNONE								0x00000000  // default, nothing
				// #define MSG_ICONQUESTION						0x00000001  // ? icon
				// #define MSG_ICONEXCLAMATION				0x00000002  // ! icon
				// #define MSG_ICONERROR							0x00000003  // X icon
				// #define MSG_ICONSTOP								0x00000004  // stop sign icon
				// #define MSG_ICONHAND								0x00000005  // hand icon
				// #define MSG_ICONINFO								0x00000006  // (i) icon 
				// #define MSG_ICONUSER1							0x00000007  
				// #define MSG_ICONUSER2							0x00000008  
				// ff means send all messages to the address list
				// 00 means send all messages with severity normal and no "icon" to the address list
				// 03 means send all messages with severity normal and error type to the address list
				// 43 means send all messages with severity critical and error type to the address list
				// you can set up different address lists with the same addresses - one email per dispatched message will be sent in the case of overlap.

		int nRegisterCode=0;

		// NOT a default handler!
		nRegisterCode = libretto.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
			"email", 
			libretto.m_settings.m_pszProcessedMailSpec?libretto.m_settings.m_pszProcessedMailSpec:libretto.m_settings.m_pszMailSpec,
			errorstring);
		if (nRegisterCode != MSG_SUCCESS) 
		{
			// inform the windowing environment
//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
			libretto.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
			libretto.m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:smtp_init", errorstring);  //(Dispatch message)
		}

//		if(pszParams) free(pszParams); pszParams=NULL;
	}

//AfxMessageBox("4");
	// set up the servers to report back to default messaging.
//	libretto.m_http.InitializeMessaging(&libretto.m_msgr);
//	libretto.m_net.InitializeMessaging(&libretto.m_msgr);
	if(libretto.m_settings.m_bLogNetworkErrors) 
	{
		bLogNetworkErrors = libretto.m_settings.m_bLogNetworkErrors;
		if(libretto.m_net.InitializeMessaging(&libretto.m_msgr)==0)
		{
			libretto.m_data.m_bNetworkMessagingInitialized=true;
		}
	}

	db.InitializeMessaging(&libretto.m_msgr);
//	g_miranda.InitializeMessaging(&libretto.m_msgr);

// TODO: here, get the DB and pull any override settings.
	//****
	CDBconn* pdbConn = db.CreateNewConnection(libretto.m_settings.m_pszDSN, libretto.m_settings.m_pszUser, libretto.m_settings.m_pszPW);
	if(pdbConn)
	{
		if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			libretto.m_data.SetStatusText(errorstring, (LIBRETTO_STATUS_FAIL_DB|LIBRETTO_STATUS_ERROR));
			libretto.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Libretto:database_connect", errorstring);  //(Dispatch message)
		}
		else
		{
			libretto.m_settings.m_pdbConn = pdbConn;
			libretto.m_settings.m_pdb = &db;
			libretto.m_data.m_pdbConn = pdbConn;
			libretto.m_data.m_pdb = &db;
			if(libretto.m_settings.GetFromDatabase(errorstring)<LIBRETTO_SUCCESS)
			{
				libretto.m_data.SetStatusText(errorstring, (LIBRETTO_STATUS_FAIL_DB|LIBRETTO_STATUS_ERROR));
				libretto.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Libretto:database_get", errorstring);  //(Dispatch message)
			}
			else
			{
				libretto.m_data.m_nLastSettingsMod = libretto.m_data.m_nSettingsMod; //got the settings!


				if(
						(libretto.m_settings.m_pszQueue)
					&&(strlen(libretto.m_settings.m_pszQueue)>0)
					)
				{

					char szSQL[DB_SQLSTRING_MAXLEN];
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s",
														libretto.m_settings.m_pszQueue  // table name
													);
//			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Sentinel:debug", "%s", szSQL);  Sleep(250); //(Dispatch message)

					if (db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						//**MSG
					}

				}



				if((!libretto.m_settings.m_bUseEmail)&&(bUseEmail))
				{
					bUseEmail = false;
					// reset it
					libretto.m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "Shutting down email functions.");  //(Dispatch message)

//						Sleep(250); // let the message get there.
					libretto.m_msgr.RemoveDestination("email");

				}
				if((!libretto.m_settings.m_bLogNetworkErrors)&&(bLogNetworkErrors))
				{
					// reset it
					libretto.m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "Shutting down network logging.");  //(Dispatch message)
					if(libretto.m_data.m_bNetworkMessagingInitialized)
					{
						libretto.m_net.UninitializeMessaging();  // void return
						libretto.m_data.m_bNetworkMessagingInitialized=false;
					}

				}
				if((!libretto.m_settings.m_bUseLog)&&(bUseLog))
				{
					// reset it
					libretto.m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

					Sleep(250); // let the message get there.
					libretto.m_msgr.RemoveDestination("log");

				}
			}
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", libretto.m_settings.m_pszDSN, libretto.m_settings.m_pszUser, libretto.m_settings.m_pszPW); 
		libretto.m_data.SetStatusText(errorstring, (LIBRETTO_STATUS_FAIL_DB|LIBRETTO_STATUS_ERROR));
		libretto.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Libretto:database_init", errorstring);  //(Dispatch message)

		//**MSG
	}


	libretto.m_pdbmsg = &dbmsg;
	libretto.m_pdbConnMsg = libretto.m_pdbmsg->CreateNewConnection(libretto.m_settings.m_pszDSN, libretto.m_settings.m_pszUser, libretto.m_settings.m_pszPW);
	if(libretto.m_pdbConnMsg)
	{
		if(libretto.m_pdbmsg->ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
		{
			libretto.m_data.SetStatusText(errorstring, (LIBRETTO_STATUS_FAIL_DB|LIBRETTO_STATUS_ERROR));
			libretto.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Libretto:msg_database_init", errorstring);  //(Dispatch message)
			libretto.m_pdbmsg->RemoveConnection(libretto.m_pdbConnMsg);
			libretto.m_pdbConnMsg = NULL;
		}
	}
	else
	{
		_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", libretto.m_settings.m_pszDSN, libretto.m_settings.m_pszUser, libretto.m_settings.m_pszPW); 
		libretto.m_data.SetStatusText(errorstring, (LIBRETTO_STATUS_FAIL_DB|LIBRETTO_STATUS_ERROR));
		libretto.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Libretto:msg_database_init", errorstring);  //(Dispatch message)

		//**MSG
	}



//init command and status listeners.

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing command server on %d", libretto.m_settings.m_usCommandPort); 
	libretto.m_data.SetStatusText(errorstring, LIBRETTO_STATUS_CMDSVR_START);
	libretto.m_msgr.DM(MSG_ICONNONE, NULL, "Libretto:command_server_init", errorstring);  //(Dispatch message)

	if(libretto.m_settings.m_usCommandPort>0)
	{
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = libretto.m_settings.m_usCommandPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "LibrettoCommandServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = LibrettoCommandHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &libretto;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &libretto.m_net;					// pointer to the object with the Message function.


		if(libretto.m_net.StartServer(pServer, &libretto.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			libretto.m_data.SetStatusText(errorstring, LIBRETTO_STATUS_CMDSVR_ERROR);
			libretto.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Libretto:command_server_init", errorstring);  //(Dispatch message)
			libretto.SendMsg(CX_SENDMSG_ERROR, "Libretto:command_server_init", errorstring);
		}
		else
		{
//AfxMessageBox("Q1");
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Command server listening on %d", libretto.m_settings.m_usCommandPort);
			libretto.m_data.SetStatusText(errorstring, LIBRETTO_STATUS_CMDSVR_RUN);
			libretto.m_msgr.DM(MSG_ICONNONE, NULL, "Libretto:command_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Initializing XML server on %d", libretto.m_settings.m_usStatusPort); 
	libretto.m_data.SetStatusText(errorstring, LIBRETTO_STATUS_STATUSSVR_START);
	libretto.m_msgr.DM(MSG_ICONNONE, NULL, "Libretto:xml_server_init", errorstring);  //(Dispatch message)

	if(libretto.m_settings.m_usStatusPort>0)
	{
//AfxMessageBox("Q1");
		CNetServer* pServer = new CNetServer;
		pServer->m_usPort = libretto.m_settings.m_usStatusPort;
		pServer->m_ucType |= NET_TYPE_KEEPOPEN;  // we want persistent connections.

		pServer->m_pszName = (char*)malloc(32);					// name of the server, for human readability
		if(pServer->m_pszName) strcpy(pServer->m_pszName, "LibrettoXMLServer");

//		pServer->m_pszStatus;				// status buffer with error messages from thread
		pServer->m_lpfnHandler = LibrettoXMLHandlerThread;			// pointer to the thread that handles the request.
		pServer->m_lpObject = &libretto;											// pointer to the object passed to the handler thread.
		pServer->m_lpMsgObj = &libretto.m_net;					// pointer to the object with the Message function.

		if(libretto.m_net.StartServer(pServer, &libretto.m_net, 10000, errorstring)<NET_SUCCESS)
		{
			//report failure
			libretto.m_data.SetStatusText(errorstring, LIBRETTO_STATUS_STATUSSVR_ERROR);
			libretto.m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Libretto:xml_server_init", errorstring);  //(Dispatch message)
			libretto.SendMsg(CX_SENDMSG_ERROR, "Libretto:xml_server_init", errorstring);
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML server listening on %d", libretto.m_settings.m_usStatusPort);
			libretto.m_data.SetStatusText(errorstring, LIBRETTO_STATUS_STATUSSVR_RUN);
			libretto.m_msgr.DM(MSG_ICONNONE, NULL, "Libretto:xml_server_init", errorstring);  //(Dispatch message)
		}
		Sleep(250);
	}

//AfxMessageBox("QQ");

	
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Libretto is registering destination devices...");
	libretto.m_msgr.DM(MSG_ICONNONE, NULL, "Libretto:destination_init", errorstring);  //(Dispatch message)

	libretto.m_data.SetStatusText(errorstring, LIBRETTO_STATUS_THREAD_SPARK);

	// now connect all the registered servers
	// this is the list of destinations in the db that get connected


	if(!(libretto.m_settings.m_ulMainMode&LIBRETTO_MODE_CLONE))
	{
		// get connections from database - set up Harris API connections
	EnterCriticalSection(&libretto.m_data.m_critDestinations);
		if(libretto.m_data.GetDestinations()>=LIBRETTO_SUCCESS)
		{
			libretto.m_data.m_nLastDestinationsMod = libretto.m_data.m_nDestinationsMod;
		}
	LeaveCriticalSection(&libretto.m_data.m_critDestinations);
//	libretto.m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "X2");  Sleep(250);//(Dispatch message)
//		libretto.m_data.GetChannels();
//	libretto.m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "X3");  Sleep(250);//(Dispatch message)
	}
//	libretto.m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "X4");  Sleep(250);//(Dispatch message)
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Libretto has registered destination devices.");
	libretto.m_msgr.DM(MSG_ICONNONE, NULL, "Libretto:destination_init", errorstring);  //(Dispatch message)
	
	if((libretto.m_data.m_ulFlags&LIBRETTO_ICON_MASK) != LIBRETTO_STATUS_ERROR)
	{
		libretto.m_data.m_ulFlags &= ~LIBRETTO_ICON_MASK;
		libretto.m_data.m_ulFlags |= LIBRETTO_STATUS_OK;  // green - we want run to be blue when something in progress
	}

	libretto.m_msgr.DM(MSG_ICONNONE, NULL, "Libretto:init", "Registering with Cortex.");  //(Dispatch message)

// initialize cortex (make this not persistent for now)
	CNetData* pdata = new CNetData;
	if(pdata)
	{

		pdata->m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA;      // defined type - indicates which protocol to use, structure of data

		pdata->m_ucCmd = CX_REQ_HELLO;       // the command byte
		pdata->m_ucSubCmd = 0;       // the subcommand byte

		char* pch = (char*)malloc(MAX_MESSAGE_LENGTH); 
		if(pch)
		{
			sprintf(pch, "%s:%d:%d:%d:%d:%s", 
				libretto.m_data.m_pszHost,
				libretto.m_settings.m_usCommandPort,
				libretto.m_settings.m_usStatusPort,
				CX_TYPE_RESOURCE,
				theApp.m_nPID,
				libretto.m_settings.m_pszName?libretto.m_settings.m_pszName:"Libretto"
				);

			pdata->m_pucData =  (unsigned char*) pch;
			pdata->m_ulDataLen = strlen(pch);
		}

		SOCKET s = NULL; 
//AfxMessageBox("sending");
//AfxMessageBox( libretto.m_data.m_pszCortexHost );
//AfxMessageBox( (char*)pdata->m_pucData );

	
		if(libretto.m_net.SendData(pdata, libretto.m_data.m_pszCortexHost, libretto.m_data.m_usCortexCommandPort, 5000, 0, NET_SND_CMDTOSVR, &s)>=NET_SUCCESS)
		{
			//send ack
//			AfxMessageBox("ack");
			libretto.m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK);
		}
		else
		{
//			AfxMessageBox("could not send");
		}
		libretto.m_net.CloseConnection(s);

		libretto.m_msgr.DM(MSG_ICONNONE, NULL, "Libretto:init", "Registered with Cortex.");  //(Dispatch message)

		delete pdata;

	}


	// try to initialize the oxsox object.

/*
	int nReturn = g_miranda.OxSoxLoad();
	if(nReturn<0)
	{
		libretto.m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:oxsox_init", "Error: OxSoxLoad returned %d", nReturn);  //(Dispatch message)
		libretto.SendMsg(CX_SENDMSG_ERROR, "Libretto:oxsox_init", "Error: OxSoxLoad returned %d", nReturn);
	}	
	else
	{
		libretto.m_msgr.DM(MSG_ICONNONE, NULL, "Libretto:oxsox_init", "Success: OxSoxLoad returned %07x", nReturn);  //(Dispatch message)
	}

	if(g_miranda.m_lpfnInitWinsock)
	{ 
		nReturn = g_miranda.m_lpfnInitWinsock();
		g_miranda.m_bDLLWinsockInit = true; // InitWinsock always returns true
	}
	else
	{
		libretto.m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:oxsox_init", "Error: NULL function address for InitWinsock");  //(Dispatch message)
		libretto.SendMsg(CX_SENDMSG_ERROR, "Libretto:oxsox_init", "Error: NULL function address for InitWinsock");
	}
*/


	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Libretto main thread running.");  
	libretto.m_data.SetStatusText(errorstring, LIBRETTO_STATUS_THREAD_RUN);
//	libretto.m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", errorstring);  Sleep(250);//(Dispatch message)
	libretto.SendMsg(CX_SENDMSG_INFO, "Libretto:init", "Libretto %s main thread running.", LIBRETTO_CURRENT_VERSION);

	_timeb timebQueue;
	_timeb timebCheckMods;
	_ftime( &timebCheckMods );
	_ftime( &timebQueue );
///AfxMessageBox("xxxxx");
	bool bLastProcessSuspended = 	libretto.m_data.m_bProcessSuspended;


	while(!g_bKillThread)
	{
		// main working loop.
		_ftime( &libretto.m_data.m_timebTick );
//	libretto.m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "%d.%03d", libretto.m_data.m_timebTick.time, libretto.m_data.m_timebTick.millitm);   Sleep(250);//(Dispatch message)

// monitor changes in exchange db and react.
		if(
					(
						(libretto.m_data.m_timebTick.time > timebCheckMods.time )
					||((libretto.m_data.m_timebTick.time == timebCheckMods.time)&&(libretto.m_data.m_timebTick.millitm >= timebCheckMods.millitm))
					)
				&&(!g_bKillThread)
//				&&(!libretto.m_data.m_bProcessSuspended)
/*
// cant do the following here.  need to allow the call to libretto.m_settings.GetFromDatabase()
// with a new license key!
				&&(libretto.m_data.m_key.m_bValid)  // must have a valid license
				&&(
				    (!libretto.m_data.m_key.m_bExpires)
					||((libretto.m_data.m_key.m_bExpires)&&(!libretto.m_data.m_key.m_bExpired))
					||((libretto.m_data.m_key.m_bExpires)&&(libretto.m_data.m_key.m_bExpireForgiveness)&&(libretto.m_data.m_key.m_ulExpiryDate+libretto.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
					)
				&&(
				    (!libretto.m_data.m_key.m_bMachineSpecific)
					||((libretto.m_data.m_key.m_bMachineSpecific)&&(libretto.m_data.m_key.m_bValidMAC))
					)
*/
			)
		{
//	libretto.m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "time to check");  Sleep(250);//(Dispatch message)
			timebCheckMods.time = libretto.m_data.m_timebTick.time + libretto.m_settings.m_ulModsIntervalMS/1000; 
			timebCheckMods.millitm = libretto.m_data.m_timebTick.millitm + (unsigned short)(libretto.m_settings.m_ulModsIntervalMS%1000); // fractional second updates
			if(timebCheckMods.millitm>999)
			{
				timebCheckMods.time++;
				timebCheckMods.millitm%=1000;
			}
//	libretto.m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "checking db connected");   Sleep(250);//(Dispatch message)
			if(pdbConn)//&&(pdbConn->m_bConnected))
			{
//AfxMessageBox("x");
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_MODS)) libretto.m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "checkmods %d suspended %d",  clock(), libretto.m_data.m_bProcessSuspended);  //(Dispatch message)
				if(libretto.m_data.CheckDatabaseMods(errorstring)==LIBRETTO_ERROR)
				{
					if(!libretto.m_data.m_bCheckModsWarningSent)
					{
						libretto.m_msgr.DM(MSG_ICONERROR, NULL, "CheckDatabaseMods", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
						libretto.m_data.m_bCheckModsWarningSent=true;
					}
				}
				else
				{
					if(libretto.m_data.m_bCheckModsWarningSent)
					{
						libretto.m_msgr.DM(MSG_ICONINFO, NULL, "CheckDatabaseMods", "Resumed checking."); // Sleep(50); //(Dispatch message)
					}
					libretto.m_data.m_bCheckModsWarningSent=false;
				}
				if(libretto.m_data.m_timebTick.time > libretto.m_data.m_timebAutoPurge.time + libretto.m_settings.m_nAutoPurgeInterval)
				{
					_ftime(&libretto.m_data.m_timebAutoPurge);
					if(libretto.m_settings.m_nAutoPurgeMessageDays>0)
					{
						if(libretto.m_data.CheckMessages(errorstring)==LIBRETTO_ERROR)
						{
							if(!libretto.m_data.m_bCheckMsgsWarningSent)
							{
								libretto.m_msgr.DM(MSG_ICONERROR, NULL, "CheckMessages", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								libretto.m_data.m_bCheckMsgsWarningSent=true;
							}
						}
						else
						if(libretto.m_data.m_bCheckMsgsWarningSent)
						{
							libretto.m_msgr.DM(MSG_ICONINFO, NULL, "CheckMessages", "Resumed checking messages for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						libretto.m_data.m_bCheckMsgsWarningSent=false;
					}

	/*
					if(libretto.m_settings.m_nAutoPurgeAsRunDays>0)
					{
						if(libretto.m_data.CheckAsRun(errorstring)==LIBRETTO_ERROR)
						{
							if(!libretto.m_data.m_bCheckAsRunWarningSent)
							{
								libretto.m_msgr.DM(MSG_ICONERROR, NULL, "CheckAsRun", "Error: %s", errorstring); // Sleep(50); //(Dispatch message)
								libretto.m_data.m_bCheckAsRunWarningSent=true;
							}
						}
						else
						if(libretto.m_data.m_bCheckMsgsWarningSent)
						{
							libretto.m_msgr.DM(MSG_ICONINFO, NULL, "CheckAsRun", "Resumed checking as-run for auto-purge."); // Sleep(50); //(Dispatch message)
						}
						libretto.m_data.m_bCheckAsRunWarningSent=false;
					}

	*/
				}


				if(libretto.m_data.m_nSettingsMod != libretto.m_data.m_nLastSettingsMod)
				{
					// this call overrides the CheckDatabaseMods that deals with the suspend state, because libretto.m_data.m_nLastSettingsMod was not initialized the first time it was called.
					// so, setting libretto.m_data.m_nLastSettingsMod = libretto.m_data.m_nSettingsMod above.  however, need to not let green license override suspend.
					if(libretto.m_settings.GetFromDatabase()>=LIBRETTO_SUCCESS)
					{
						libretto.m_data.m_nLastSettingsMod = libretto.m_data.m_nSettingsMod;


						// check for stuff to change

						// network messaging
						if(libretto.m_settings.m_bLogNetworkErrors) 
						{
							if(!libretto.m_data.m_bNetworkMessagingInitialized)
							{
								if(libretto.m_net.InitializeMessaging(&libretto.m_msgr)==0)
								{
									libretto.m_data.m_bNetworkMessagingInitialized=true;
								}

	if(libretto.m_data.m_ppDestObj)
	{
		int i=0;
		while(i<libretto.m_data.m_nNumDestinationObjects)
		{
			if(libretto.m_data.m_ppDestObj[i])
			{
				libretto.m_data.m_ppDestObj[i]->m_net.InitializeMessaging(&libretto.m_msgr);
			}
			i++;
		}
	}

							}





						}
						else
						{
							if(libretto.m_data.m_bNetworkMessagingInitialized)
							{
								libretto.m_net.UninitializeMessaging();  // void return
								libretto.m_data.m_bNetworkMessagingInitialized=false;

	if(libretto.m_data.m_ppDestObj)
	{
		int i=0;
		while(i<libretto.m_data.m_nNumDestinationObjects)
		{
			if(libretto.m_data.m_ppDestObj[i])
			{
				libretto.m_data.m_ppDestObj[i]->m_net.UninitializeMessaging();
			}
			i++;
		}
	}


							}
						}

						// logging and email messaging:

						if(!libretto.m_settings.m_bUseEmail)
						{
							if(bUseEmail)
							{
								bUseEmail = false;
								// reset it
								libretto.m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "Shutting down email functions.");  //(Dispatch message)

		//						Sleep(250); // let the message get there.
								libretto.m_msgr.RemoveDestination("email");
							}
						}
						else
						{
							if(!bUseEmail)
							{
								bUseEmail = true;
								int nRegisterCode=0;

								// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
								nRegisterCode = libretto.m_msgr.AddDestination(MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
									"email", 
									libretto.m_settings.m_pszProcessedMailSpec?libretto.m_settings.m_pszProcessedMailSpec:libretto.m_settings.m_pszMailSpec,
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register SMTP object!\n code: %d", nRegisterCode); 
									libretto.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
									libretto.m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:smtp_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=libretto.m_msgr.GetDestIndex("email");
								if(nIndex>=0)
								{
									if((libretto.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(libretto.m_settings.m_pszProcessedMailSpec?libretto.m_settings.m_pszProcessedMailSpec:libretto.m_settings.m_pszMailSpec))
									{
										if(strcmp(libretto.m_msgr.m_ppDest[nIndex]->m_pszParams, (libretto.m_settings.m_pszProcessedMailSpec?libretto.m_settings.m_pszProcessedMailSpec:libretto.m_settings.m_pszMailSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = libretto.m_msgr.ModifyDestination(
												"email", 
												libretto.m_settings.m_pszProcessedMailSpec?libretto.m_settings.m_pszProcessedMailSpec:libretto.m_settings.m_pszMailSpec,
												MSG_DESTTYPE_EMAIL|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify SMTP object!\n code: %d", nRegisterCode); 
												//libretto.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												libretto.m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:smtp_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}

						if(!libretto.m_settings.m_bUseLog)
						{
							if(bUseLog)
							{
								bUseLog = false;
								// reset it
								libretto.m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "Shutting down logging functions.\n\
--------------------------------------------------\n");  //(Dispatch message)

								Sleep(250); // let the message get there.
								libretto.m_msgr.RemoveDestination("log");
							}
						}
						else
						{
							if(!bUseLog)
							{
								bUseLog = true;
								int nRegisterCode=0;

								nRegisterCode = libretto.m_msgr.AddDestination(MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
									"log", 
									libretto.m_settings.m_pszProcessedFileSpec?libretto.m_settings.m_pszProcessedFileSpec:libretto.m_settings.m_pszFileSpec, 
									errorstring);
								if (nRegisterCode != MSG_SUCCESS) 
								{
									// inform the windowing environment
						//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to register log file!\n code: %d", nRegisterCode); 
									libretto.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_LOG|CX_STATUS_ERROR));
									libretto.m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:log_reinit", errorstring);  //(Dispatch message)
								}
							}
							else
							{ // check for change
								int nIndex=libretto.m_msgr.GetDestIndex("log");
								if(nIndex>=0)
								{
									if((libretto.m_msgr.m_ppDest[nIndex]->m_pszParams)&&(libretto.m_settings.m_pszProcessedFileSpec?libretto.m_settings.m_pszProcessedFileSpec:libretto.m_settings.m_pszFileSpec))
									{
										if(strcmp(libretto.m_msgr.m_ppDest[nIndex]->m_pszParams, (libretto.m_settings.m_pszProcessedFileSpec?libretto.m_settings.m_pszProcessedFileSpec:libretto.m_settings.m_pszFileSpec)))
										{
											int nRegisterCode=0;

											// NOT a default handler!  OK,  DO make it a default - but be careful setting up the email.
											nRegisterCode = libretto.m_msgr.ModifyDestination(
												"log", 
												libretto.m_settings.m_pszProcessedFileSpec?libretto.m_settings.m_pszProcessedFileSpec:libretto.m_settings.m_pszFileSpec,
												MSG_DESTTYPE_LOG|MSG_DESTTYPE_DEFAULT, 
												errorstring);
											if (nRegisterCode != MSG_SUCCESS) 
											{
												// inform the windowing environment
									//				AfxMessageBox(errorstring); // may want to remove this, it stalls the process...

												_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to modify log object!\n code: %d", nRegisterCode); 
												//libretto.m_data.SetStatusText(errorstring, (CX_STATUS_FAIL_SMTP|CX_STATUS_ERROR));
												libretto.m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:log_change", errorstring);  //(Dispatch message)
											}
										}
									}
								}
							}
						}







					}
				}
				if(libretto.m_data.m_nDestinationsMod != libretto.m_data.m_nLastDestinationsMod)
				{
//			libretto.m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "getting destinations again");   Sleep(250);//(Dispatch message)
					if( 
							(libretto.m_data.m_key.m_bValid)  // must have a valid license
						&&(
								(!libretto.m_data.m_key.m_bExpires)
							||((libretto.m_data.m_key.m_bExpires)&&(!libretto.m_data.m_key.m_bExpired))
							||((libretto.m_data.m_key.m_bExpires)&&(libretto.m_data.m_key.m_bExpireForgiveness)&&(libretto.m_data.m_key.m_ulExpiryDate+libretto.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
							)
						&&(
								(!libretto.m_data.m_key.m_bMachineSpecific)
							||((libretto.m_data.m_key.m_bMachineSpecific)&&(libretto.m_data.m_key.m_bValidMAC))
							)
						)
					{
	EnterCriticalSection(&libretto.m_data.m_critDestinations);
						if(libretto.m_data.GetDestinations()>=LIBRETTO_SUCCESS)
						{
							libretto.m_data.m_nLastDestinationsMod = libretto.m_data.m_nDestinationsMod;
						}
	LeaveCriticalSection(&libretto.m_data.m_critDestinations);
					}
				}
			}
		}
//AfxMessageBox("zoinks");

		
		// following section is outside the mods loop, the Queue is really the main loop.
// following line commented out, because we want to check the Queue all the time, for the presence of anything at all to deal with.
//				if(libretto.m_data.m_nQueueMod != libretto.m_data.m_nLastQueueMod)

		if(
				(!libretto.m_data.m_bProcessSuspended)	
			&&(libretto.m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!libretto.m_data.m_key.m_bExpires)
				||((libretto.m_data.m_key.m_bExpires)&&(!libretto.m_data.m_key.m_bExpired))
				||((libretto.m_data.m_key.m_bExpires)&&(libretto.m_data.m_key.m_bExpireForgiveness)&&(libretto.m_data.m_key.m_ulExpiryDate+libretto.m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!libretto.m_data.m_key.m_bMachineSpecific)
				||((libretto.m_data.m_key.m_bMachineSpecific)&&(libretto.m_data.m_key.m_bValidMAC))
				)
			)
		{
//			libretto.m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "getting channels again");   Sleep(250);//(Dispatch message)
			if(libretto.m_settings.m_bEnableQueue)
			{
				// time check
				if(
						(
						  (libretto.m_settings.m_nQueueIntervalMS<=0) // go immediate.
						||(libretto.m_data.m_timebTick.time > timebQueue.time )
						||((libretto.m_data.m_timebTick.time == timebQueue.time)&&(libretto.m_data.m_timebTick.millitm >= timebQueue.millitm))
						)
					&&(!g_bKillThread)
					)
				{

					if(libretto.m_data.GetQueue()>=LIBRETTO_SUCCESS)
					{
						if(libretto.m_settings.m_nQueueIntervalMS>0)
						{
							timebQueue.time = libretto.m_data.m_timebTick.time + libretto.m_settings.m_nQueueIntervalMS/1000; 
							timebQueue.millitm = libretto.m_data.m_timebTick.millitm + (unsigned short)(libretto.m_settings.m_nQueueIntervalMS%1000); // fractional second updates
							if(timebQueue.millitm>999)
							{
								timebQueue.time++;
								timebQueue.millitm%=1000;
							}
						}
						libretto.m_data.m_nLastQueueMod = libretto.m_data.m_nQueueMod;
					}
				}
			}
		}
		
		if(bLastProcessSuspended != libretto.m_data.m_bProcessSuspended)
		{

			bLastProcessSuspended = libretto.m_data.m_bProcessSuspended;
			if(!bLastProcessSuspended)
			{
				// we've resumed so clear the quue.
				if((pdbConn)&&(pdbConn->m_bConnected))
				{
					char szSQL[DB_SQLSTRING_MAXLEN];
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "TRUNCATE TABLE %s", //HARDCODE
						((libretto.m_settings.m_pszQueue)&&(strlen(libretto.m_settings.m_pszQueue)))?libretto.m_settings.m_pszQueue:"Queue");


					if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					{
						//**MSG
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

					}
				}

			}
		}

		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
			AfxGetApp()->PumpMessage();

		Sleep(1); 
//		Sleep(1000);  // stalls window procedure
	}

	libretto.m_data.m_ulFlags &= ~LIBRETTO_STATUS_THREAD_MASK;
	libretto.m_data.m_ulFlags |= LIBRETTO_STATUS_THREAD_END;

	libretto.m_msgr.DM(MSG_ICONINFO, NULL, "Libretto:uninit", "Libretto is shutting down.");  //(Dispatch message)
	libretto.SendMsg(CX_SENDMSG_INFO, "Libretto:uninit", "Libretto %s is shutting down.", LIBRETTO_CURRENT_VERSION);

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Libretto is shutting down.");  
	libretto.m_data.m_ulFlags &= ~CX_ICON_MASK;
	libretto.m_data.m_ulFlags |= CX_STATUS_UNKNOWN;
	libretto.m_data.SetStatusText(errorstring, libretto.m_data.m_ulFlags);

/*
	// here's a hard coded one for now
	FILE* fpb; fpb = fopen("branding.pid", "rt");  // if fails, we exited already, no need to shut it down
	if(fpb)
	{
		fclose(fpb);
		unsigned char buffer[8] = {0,(unsigned char)0x91,0,0,0,0,0,0}; // branding hardcode
		buffer[0] = libretto.m_net.Checksum((buffer+1), 5); // branding hardcode
		SOCKET s; // branding hardcode
		libretto.m_net.OpenConnection(libretto.m_http.m_pszHost, 10888, &s); // branding hardcode
		libretto.m_net.SendLine(buffer, 6, s, EOLN_NONE, false); // branding hardcode
		libretto.m_net.CloseConnection(s); // branding hardcode
	}
*/


// need to shut down destinations

	if(libretto.m_data.m_ppDestObj)
	{
		int i=0;
		while(i<libretto.m_data.m_nNumDestinationObjects)
		{
			int nWait =  clock()+1500;
			if(libretto.m_data.m_ppDestObj[i])
			{
				libretto.m_data.m_ppDestObj[i]->m_bKillConnThread = true;
				libretto.m_data.m_ppDestObj[i]->m_bKillMonThread = true;
				while(
							 (
								 (libretto.m_data.m_ppDestObj[i]->m_bMonThreadStarted)
							 ||(libretto.m_data.m_ppDestObj[i]->m_bConnThreadStarted)
							 )
							&&
							 (clock()<nWait)
						 )
				{
					Sleep(1);
				}
				if(libretto.m_data.m_ppDestObj[i]->m_socket != NULL)
				{
					libretto.m_data.m_ppDestObj[i]->m_net.CloseConnection(libretto.m_data.m_ppDestObj[i]->m_socket); // re-establish
				}
				libretto.m_data.m_ppDestObj[i]->m_socket = NULL;

			}

			i++;
		}
	}



//	m_pDlg->SetProgress(LIBRETTODLG_WAITING);  // default settings.
	// shutdown
//	AfxMessageBox("shutting down file server.");
//	libretto.m_data.m_ulFlags &= ~LIBRETTO_STATUS_FILESVR_MASK;
//	libretto.m_data.m_ulFlags |= LIBRETTO_STATUS_FILESVR_END;
//	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down file server....");  
//	libretto.m_data.SetStatusText(errorstring, libretto.m_data.m_ulFlags);
//	_ftime( &libretto.m_data.m_timebTick );
//	libretto.m_http.EndServer();
	_ftime( &libretto.m_data.m_timebTick );
//	AfxMessageBox("shutting down command server.");

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down command server....");  
	libretto.m_msgr.DM(MSG_ICONNONE, NULL, "Libretto:command_server_uninit", errorstring);  //(Dispatch message)
	libretto.m_data.SetStatusText(errorstring, LIBRETTO_STATUS_CMDSVR_END);
	_ftime( &libretto.m_data.m_timebTick );
	libretto.m_net.StopServer(libretto.m_settings.m_usCommandPort, 5000, errorstring);
	_ftime( &libretto.m_data.m_timebTick );
//	AfxMessageBox("shutting down status server.");

	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Shutting down XML server....");  
	libretto.m_msgr.DM(MSG_ICONNONE, NULL, "Libretto:xml_server_uninit", errorstring);  //(Dispatch message)
	libretto.m_data.SetStatusText(errorstring, LIBRETTO_STATUS_STATUSSVR_END);
	_ftime( &libretto.m_data.m_timebTick );
	libretto.m_net.StopServer(libretto.m_settings.m_usStatusPort, 5000, errorstring);
	_ftime( &libretto.m_data.m_timebTick );
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Libretto is exiting.");  
	libretto.m_msgr.DM(MSG_ICONNONE, NULL, "Libretto:uninit", errorstring);  //(Dispatch message)
	libretto.m_data.SetStatusText(errorstring, libretto.m_data.m_ulFlags);



	// save settings.  // dont save them here.  save them on any changes in the main command loop.

	libretto.m_settings.Settings(false); //write

	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "shutting down");
	libretto.m_data.m_ulFlags &= ~LIBRETTO_ICON_MASK;
	libretto.m_data.m_ulFlags |= LIBRETTO_STATUS_UNINIT;
	libretto.m_data.SetStatusText(errorstring, libretto.m_data.m_ulFlags);

	// sets the icon back to red too
	_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Libretto is exiting");
	libretto.m_data.m_ulFlags &= ~CX_ICON_MASK;
	libretto.m_data.m_ulFlags |= CX_STATUS_UNINIT;
	libretto.m_data.SetStatusText(errorstring, libretto.m_data.m_ulFlags);

	//exiting
	libretto.m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "-------------- Libretto %s exit ---------------\n\
----------------------------------------------------\n", LIBRETTO_CURRENT_VERSION);  //(Dispatch message)
///	m_pDlg->SetProgress(LIBRETTODLG_CLEAR); // no point

	_ftime( &libretto.m_data.m_timebTick );
	libretto.m_data.m_ulFlags &= ~LIBRETTO_STATUS_THREAD_MASK;
	libretto.m_data.m_ulFlags |= LIBRETTO_STATUS_THREAD_ENDED;
	g_bKillStatus = true;

	// kill the time code.
	libretto.m_data.m_bTimeCodeThreadKill = true;
	while(libretto.m_data.m_bTimeCodeThreadStarted)	{_ftime( &libretto.m_data.m_timebTick ); Sleep(10);}

	Sleep(250); // let the message get there.
	libretto.m_msgr.RemoveDestination("log");
	libretto.m_msgr.RemoveDestination("email");

	libretto.m_msgr.m_bKillThread = true; // do this early to give it some time to finish out.
	
	int nClock = clock() + 300; // small delay at end
	while(clock()<nClock)	{_ftime( &libretto.m_data.m_timebTick );}
	g_bThreadStarted = false;
	g_plibretto = NULL;
	nClock = clock() + libretto.m_settings.m_nThreadDwellMS; // another small delay at end
	while(clock()<nClock)	{_ftime( &libretto.m_data.m_timebTick );}
	Sleep(300); //one more small delay at end
	_endthread();
}

/*
void Message(unsigned long ulFlags, char* pszMessage, char* pszCaller, char* pszDestinations)
{
	//encodes and sends a message for use by dispatcher.
	// messages are of format:
	//[variable bytes, char 10 enc msg][char 10][variable bytes, char 10 enc caller][char 10][variable bytes, char 10 enc dest][char 10][char0]

	if((g_pmsgr)&&(g_pmsgr->m_lpfnDM))
	{
		if(pszMessage==NULL) { return; }
		unsigned long ulMaxBufferLen = 0;
		unsigned long ulBufferLen = 0;
		char* pszEncodedMsg = NULL;
		char* pszEncodedCaller = NULL;
		char* pszEncodedDest = NULL;
		CMessagingObject msg;
		
		if(pszMessage)
		{
			ulBufferLen = strlen(pszMessage);
			pszEncodedMsg = msg.EncodeTen(pszMessage, &ulBufferLen);
			if(pszEncodedMsg) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszCaller)
		{
			ulBufferLen  = strlen(pszCaller);
			pszEncodedCaller = msg.EncodeTen(pszCaller, &ulBufferLen);
			if(pszEncodedCaller) ulMaxBufferLen+=ulBufferLen;
		}
		if(pszDestinations)
		{
			ulBufferLen  = strlen(pszDestinations);
			pszEncodedDest = msg.EncodeTen(pszDestinations, &ulBufferLen);
			if(pszEncodedDest) ulMaxBufferLen+=ulBufferLen;
		}

		ulMaxBufferLen += 4; //  3 for delims, 1 for term zero

		char* pch;
		pch = (char*) malloc(ulMaxBufferLen); // MAX buffer length of output buffer

		if(pch!=NULL)
		{
/*
 dont
 // embed flags in the first 32 bits.
			*pch     = (unsigned char)((ulFlags>>24)&0xff);
			*(pch+1) = (unsigned char)((ulFlags>>16)&0xff);
			*(pch+2) = (unsigned char)((ulFlags>>8)&0xff);
			*(pch+3) = (unsigned char)((ulFlags)&0xff);
* /
			// now can use ulMaxBufferLen and ulBufferLen for buffer index counter

			ulBufferLen = 0;
			ulMaxBufferLen = 0;

			if(pszEncodedMsg) 
			{
				ulBufferLen = strlen(pszEncodedMsg);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch, pszEncodedMsg, ulBufferLen);
				free(pszEncodedMsg);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedCaller)
			{
				ulBufferLen = strlen(pszEncodedCaller);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedCaller, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			if(pszEncodedDest)
			{
				ulBufferLen = strlen(pszEncodedDest);
				ulMaxBufferLen += ulBufferLen;
				memcpy(pch+ulMaxBufferLen, pszEncodedDest, ulBufferLen);
				free(pszEncodedCaller);
			}
			memset(pch+ulMaxBufferLen, 10, 1); ulMaxBufferLen++;
			memset(pch+ulMaxBufferLen, 0, 1); //term 0

			g_pmsgr->m_lpfnDM(pch, ulFlags);
			free(pch);
		}
	}
}
*/


/*
// this thread is based on void HTTP10HandlerThread(void* pvArgs) from
// HTTP10.cpp.  It makes a special webserver with security, cgi parsing, and dynamic results
void LibrettoHTTPThread(void* pvArgs)
{
	// this thread must execute as quickly as possible and return.
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return;}

	CLibrettoMain* pLibretto = (CLibrettoMain*)(pClient->m_lpObject) ;  // pointer to the global object.

// HTTP service by def is a non-persistent service, so even if the Client type is
// set to persist, we ignore that directive in this handler thread

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;


		CHTTP10* phttp = (CHTTP10*)(&(pLibretto->m_http)) ;  // pointer to the global object.
		if(phttp == NULL)// cant do anything!
		{
			shutdown(pClient->m_socket, SD_BOTH);
			closesocket(pClient->m_socket);
			(*(pClient->m_pulConnections))--;
			_endthread(); return;
		}

		CHTTP10 http(phttp->m_pszRoot);  // local object for utility functions ONLY.
		// this allows the local to clone the root setting of the global, 
		// in order to do correct parsing in its own thread

		// the following are just pointers to the global obj.  
		// must NULL these out before object gets destroyed, 
		// so that global resources not freed!   !!!!!!!  important !!!!!
		http.m_pszHost = phttp->m_pszHost;
		http.m_ppszErrors = phttp->m_ppszMime;
		http.m_ppszMime = phttp->m_ppszMime;   
		http.m_ulStatus = phttp->m_ulStatus;   // important for redirect and error
		
		unsigned char* pch;// for use with GetLine().
		unsigned long ulBufLen;// for use with GetLine().
		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		unsigned short usCode = 0;

		// get the raw buffer and process it yourself
		nReturn = http.m_net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_EOLN|EOLN_HTTP, pszStatus);
		if((nReturn<NET_SUCCESS)||(pch==NULL))  // for use with GetLine(). 
			//was going to check ulBufLen = 0, but decided zero data could possibly be valid on an ok connection.
		{
			//error.
			if(pClient->m_lpMsgObj)
			{
				_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error receiving data.  %s", pszStatus);
				((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
			}

			usCode = 400;  //  we TRY to send this....
			if(pch!=NULL)
			{
				free(pch);  // must free the incoming data buffer 
				pch = NULL;
			}
			// create a return message buffer.
			pch = (unsigned char*)http.ErrorBuffer(NULL, usCode);   // 400 bad request
			ulBufLen = strlen((char*)pch); // valid from an error page.

			// send malformed request answer if possible  - have to tell the client that the request was not received in good order.
			nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
			if(nReturn<NET_SUCCESS)
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
				}
			}
		}
		else  // successful reception of data.
		{
			// process the data here...
			// in this sample the buffer is loaded into
			// pch, with ulBufLen as length

/*
			FILE* fp = fopen("rec.txt", "ab");
			if (fp)
			{
				fwrite(pch, 1, ulBufLen, fp );
				fflush(fp);
				fclose(fp);
			}
* /
			// parse buffer, format reply.
			CHTTPHeader header;

			if (http.ParseHeader(pch, ulBufLen, &header) < HTTP_SUCCESS)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("header not parsed\r\n\r\n", 1, strlen("header not parsed\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}
* /
				
				if(pch!=NULL) 
				{
					free(pch);  // must free the incoming data buffer 
					pch = NULL;
				}
				// create a return message buffer.
				usCode = 400;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}
			else
			{
				// here we have enough info to check security if desired, and if not, return a 401;
				// else continue....
/*
					FILE* fp = fopen("rec.txt", "ab");
					if (fp)
					{// lets write out the whole header so we can examine its full contents. including args.
						char bufferin[10000];
						sprintf(bufferin,
"header successfully parsed\r\n\
method: %d\r\n\
version: %s\r\n\
URL: %s\r\n\
Path: %s\r\n\
mimetype: %s\r\n\
user: %s\r\n\
password: %s\r\n\
num args: %ld\r\n",
	header.m_ucMethod,  // enumerated values
	header.m_pszVersion==NULL?"(null)":header.m_pszVersion,
	header.m_pszURL==NULL?"(null)":header.m_pszURL,
	header.m_pszPath==NULL?"(null)":header.m_pszPath,
	header.m_pszMimeType==NULL?"(null)":header.m_pszMimeType,
	header.m_pszUser==NULL?"(null)":header.m_pszUser,
	header.m_pszPassword==NULL?"(null)":header.m_pszPassword,
	header.m_ulNumArgs

);
		
	unsigned long k=0;
	while(k<header.m_ulNumArgs)
	{
		if((header.m_ppszArgNames)&&(header.m_ppszArgValues))
		{
			strcat(bufferin, "  ");
			strcat(bufferin, header.m_ppszArgNames[k]==NULL?"(null)":header.m_ppszArgNames[k]);
			strcat(bufferin, "=");
			strcat(bufferin, header.m_ppszArgValues[k]==NULL?"(null)":header.m_ppszArgValues[k]);
			strcat(bufferin, "\r\n");
		}
	k++;
	}

	strcat(bufferin, "headers:\r\n");
	strcat(bufferin, header.m_pszHeaders);
	strcat(bufferin, "\r\n");


			//		fwrite("header successfully parsed\r\n", 1, strlen("header successfully parsed\r\n"), fp );
						fwrite(bufferin, 1, strlen(bufferin), fp );
						fflush(fp);
						fclose(fp);
					}

* /


				if((phttp->m_pSecure)&&((header.m_pszUser==NULL)||(header.m_pszPassword==NULL)))
				{
					usCode = 401;
					pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
					ulBufLen = strlen((char*)pch); // valid from an error page.
					// buffer successfully parsed.
				}
				else 
				{
					if(pch!=NULL)
					{
						free(pch);  // must free the incoming data buffer 
						pch = NULL;
					}

					// create a return message buffer.
					switch(header.m_ucMethod)
					{
					case HTTP_GET://			1  // HTTP/1.0
					case HTTP_HEAD://			2  // HTTP/1.0
						{
							if(
									(strnicmp(header.m_pszPath, http.m_pszRoot, strlen(http.m_pszRoot)) == 0 )  // else not a part of the served file tree
								)
							{
								if(phttp->m_pSecure)
								{
									// have to check the main object for permission.
									// we are going to go by URL (asset level)
									// Loci are going to just be convenient names for the admins to set up.

									if((phttp->m_pSecure->CheckSecure(header.m_pszUser, header.m_pszPassword, NULL, header.m_pszURL))!=SECURE_SUCCESS)
									{
										usCode = 401;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 401 need auth
										ulBufLen = strlen((char*)pch); // valid from an error page.
										break; // get out of the switch from here.
									}
									// else it flows thru....
								}

								DWORD dwAttrib;

								dwAttrib = GetFileAttributes(header.m_pszPath);

								// check for directory (redirect nec.)
								if ( (dwAttrib!=0xffffffff)&&(dwAttrib&FILE_ATTRIBUTE_DIRECTORY) )  // if error, it may be because no file exists, so check in the next part.
								{
									usCode = 301;  // not necessary but why not.
									pch = (unsigned char*)http.RedirectBuffer(&header);
									ulBufLen = strlen((char*)pch); // valid from an redirect page.
								}
								else
								{
									// before we try to open the file, lets spend the time now (not while the file is open)
									// to figure out how long the static text respone buffer will be.
									CBufferUtil bu;
									// notice the sample "Server: LibrettoServer/3.0.3.1" header included.  Pick the appname and ver, probably best
									unsigned long ulBufferLen = strlen("HTTP/1.0 200 OK\r\nServer: LibrettoServer/\r\nContent-Length: \r\n\r\n");
									ulBufferLen += strlen(LIBRETTO_CURRENT_VERSION);

									if((header.m_pszMimeType)&&(strlen(header.m_pszMimeType)>0))
									{
										ulBufferLen += (strlen("Content-Type: \r\n") + strlen(header.m_pszMimeType)); // only add if known!
									}
									else // empty!
									{
										// dont free the "permanent resource", just null the pointer
										header.m_pszMimeType=NULL;
									}

									FILE* pFile = NULL;

									pFile = fopen(header.m_pszPath, "rb");
									if (pFile == NULL)  // file not openable, or not found.
									{
										usCode = 404;
										pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 404 not found
										ulBufLen = strlen((char*)pch); // valid from an error page.
									}
									else
									{
										// this part just assembles the file into a buffer.
										// this may be modified to do dynamic html things by using the 
										// pClient->m_lpObject that refers back to some parser....

										usCode = 200; // OK  (!)
										// determine file size
										fseek(pFile, 0, SEEK_END);
										unsigned long ulFileLen = ftell(pFile);


										// here we have to parse out any directives.  if this is a template file,
										// we may have a variable length.
									//	x


										if(header.m_ucMethod == HTTP_GET)
										{
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += (strlen(length)+ulFileLen);

											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you truncations at zeroed bytes

											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: LibrettoServer/%s", LIBRETTO_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
											
											fclose(pFile);
										}
										else
										if(header.m_ucMethod == HTTP_HEAD)
										{
											fclose(pFile);  // we have all we need already.
											char length[32]; sprintf(length, "%ld", ulFileLen);
											ulBufferLen += strlen(length);
											ulBufLen = ulBufferLen; // valid for sending binary files.  strlen gives you trunactions at zeroed bytes

											//  create the header
											pch = (unsigned char*) malloc(ulBufferLen+1); // term zero
											if(pch)
											{
												fseek(pFile, 0, SEEK_SET);
												sprintf((char*)pch, "HTTP/1.0 200 OK\r\nServer: LibrettoServer/%s", LIBRETTO_CURRENT_VERSION);
												if(header.m_pszMimeType!=NULL)
												{
													strcat((char*)pch, "Content-Type: ");
													strcat((char*)pch, header.m_pszMimeType);  // dont free the pszExt, its just a pointer to a "permanent resource"
												}
												strcat((char*)pch, "\r\nContent-Length: ");
												strcat((char*)pch, length);
												strcat((char*)pch, "\r\n\r\n");

												// its identical except for the body.  next line must stay commented out, its just there for ref.
												//fread(pch+(ulBufferLen-ulFileLen), sizeof(char), ulFileLen, pFile);
												*(pch+ulBufferLen) = 0; // term zero
											}
										}
									}
								}
							}
							else  // not part of the server service.
							{
								usCode = 403;
								pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 403 forbidden ...
								ulBufLen = strlen((char*)pch); // valid from an error page.
							}

						} break;
					case HTTP_POST://			3  // HTTP/1.0
						{
							usCode = 501;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 501 not implemented ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_PUT://			4  // HTTP/1.1
					case HTTP_DELETE://		5  // HTTP/1.1
					case HTTP_OPTIONS://  6  // HTTP/1.1
					case HTTP_TRACE://	  7  // HTTP/1.1
						{
							usCode = 505;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 505 http version not supported ...
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					case HTTP_UNDEF://		0  // internal
					default:
						{
							usCode = 400;
							pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 400 bad request
							ulBufLen = strlen((char*)pch); // valid from an error page.
						} break;
					}
				}
			}

			// send a reply, in pch., with strlen pch as length

			if(pch == NULL)  // had a problem forming the request, try to send a last ditch error code.
			{
				usCode = 500;
				pch = (unsigned char*)http.ErrorBuffer(&header, usCode);   // 500 internal server error
				ulBufLen = strlen((char*)pch); // valid from an error page.
			}

			if(pch != NULL)
			{
/*
				FILE* fp = fopen("rec.txt", "ab");
				if (fp)
				{
					fwrite("sending:\r\n", 1, strlen("sending:\r\n"), fp );
					fwrite(pch, 1, ulBufLen, fp );
					fwrite("\r\n\r\n", 1, strlen("\r\n\r\n"), fp );
					fflush(fp);
					fclose(fp);
				}

* /
				nReturn = http.m_net.SendLine(pch, ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);  // none because we already compiled it in
				if(nReturn<NET_SUCCESS)
				{
					//error.
					if(pClient->m_lpMsgObj)
					{
						_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "HTTPHandlerThread: error sending %d reply.  %s", usCode, pszStatus);
						((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:HTTPHandlerThread");
					}
				}
			}
		}

//		char foo[34]; sprintf(foo, "code: %d", usCode);	AfxMessageBox(foo);

		http.m_pszHost = NULL;		// but do NOT free resource
		http.m_ppszErrors = NULL; // but do NOT free resource
		http.m_ppszMime = NULL;   // but do NOT free resource

		if(pch!=NULL) free(pch);  // must free the data buffer 

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}
*/


void LibrettoCommandHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

//		CNetData data;  // moved to inside the do so we can clear out the data with scope and start fresh each time - theoretically.
//		CSafeBufferUtil sbu;


		bool bCloseCommand = false;

		do
		{
			CNetData data;
			// following line fills out the data structure in Protocol 1  //SECURE with PROTOCOL2 later
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszStatus);


			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Libretto:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Libretto:CommandHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{
//AfxMessageBox("receiving");

					// parse the command:


if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "command received %x %x [%s]", data.m_ucCmd, data.m_ucSubCmd, data.m_pucData?(char*)data.m_pucData:"NULL"); //  Sleep(250);//(Dispatch message)


					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					switch(data.m_ucCmd)
					{	
					case CX_CMD_GETINFO: // not actually supported yet.  everything thru csf files for now.
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;



					case LIBRETTO_CMD_GETSCENE://	   0xa0
						{
							// really just need host and scene identifier.
							// so, host�scene, no pipes allowed in either.

/*
					char* pchServer = szServer.GetBuffer(1);
					int nDestIndex = ReturnDestinationIndex(pchServer);
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_QUEUE)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "got item for server %s from queue", pchServer); //  Sleep(250);//(Dispatch message)

					if((nDestIndex>=0)&&(m_ppDestObj)&&(m_ppDestObj[nDestIndex])&&(m_ppDestObj[nDestIndex]->m_ulFlags&LIBRETTO_FLAG_ENABLED))
					{  
						// only do anything if its enabled.
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_QUEUE)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "server %s enabled, socket:%d, thread:%d", pchServer, m_ppDestObj[nDestIndex]->m_socket, m_ppDestObj[nDestIndex]->m_bConnThreadStarted);//   Sleep(250);//(Dispatch message)

					//	int nHostReturn = LIBRETTO_ERROR;
						if((m_ppDestObj[nDestIndex]->m_socket == NULL)&&(!m_ppDestObj[nDestIndex]->m_bConnThreadStarted))

*/
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)
							{
						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "get_scene: received %s", (char*)data.m_pucData); // Sleep(250); //(Dispatch message)

								CSafeBufferUtil sbu;
								char* pchServer = sbu.Token((char*)data.m_pucData, data.m_ulDataLen, "�", MODE_SINGLEDELIM);
								if((pchServer)&&(strlen(pchServer)))
								{
						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "get_scene: parsed server %s", pchServer); // Sleep(250); //(Dispatch message)

									char* pchScene = sbu.Token(NULL, NULL, "�", MODE_SINGLEDELIM);
									if((pchScene)&&(strlen(pchScene)))
									{
										// ok cool... have host and scene, check host, if good, get scene info...
						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "get_scene: parsed scene %s", pchScene); // Sleep(250); //(Dispatch message)
										int nDestIndex = g_plibretto->m_data.ReturnDestinationIndex(pchServer);
										if((nDestIndex>=0)&&(g_plibretto->m_data.m_ppDestObj)&&(g_plibretto->m_data.m_ppDestObj[nDestIndex]))
										{ 
											if(g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_ulFlags&LIBRETTO_FLAG_ENABLED)
											{
												switch(g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_usType)
												{
												case CX_DESTTYPE_CHYRON_CHANNELBOX: // the only one we support currently...
													{
														if(
															  (g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_socket != NULL)
															&&(!g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_bConnThreadStarted)  // connection should be not initializing
															)
														{
															// have to send II and receive info on fields; then send II for each field to get allowable values
															// what will the format be?
															// how about:
															// scene id|field1�type�curr_value�value1�value2|field2�type�curr_value�value1�value2|etc

															// NO
															// for now, we just get the fields, forget allowable values for now.
															// Libretto?cmd=06&sub=06&data=scene�field1�type�defval�field2�type�defval�field3�type�defval 


															CLibrettoDestinationObject* pDestObj = g_plibretto->m_data.m_ppDestObj[nDestIndex];
															char buffer[MAX_PATH];//should be enough.
															sprintf(buffer, "B\\AN\\%s\\\\", pchScene);
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDQUEUE|LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_handler", "sending: %s", buffer); // Sleep(250); //(Dispatch message)
									
							EnterCriticalSection(&pDestObj->m_critComm);
															pDestObj->m_bSending=true;
															int nReturn = pDestObj->SendTelnetCommand(buffer, strlen(buffer), EOLN_CRLF);

															if(nReturn<LIBRETTO_SUCCESS)
															{
																	pDestObj->m_bSending=false;
							LeaveCriticalSection(&pDestObj->m_critComm);
																//**MSG
																g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:telnet", 
																	"telnet command not successful on %s.", pDestObj->m_pszServerName );//   Sleep(50);//(Dispatch message)
																data.m_ucSubCmd = (NET_CMD_NAK|0xd0);

																free(data.m_pucData);  //destroy the buffer;
																data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
																if(data.m_pucData)
																{
																	_snprintf((char*)data.m_pucData, NET_ERRORSTRING_LEN, "Error sending data to %s. (%d)", pDestObj->m_pszServerName, nReturn);
																	data.m_ulDataLen = strlen((char*)data.m_pucData);
																	data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
																}
																else
																{
																	data.m_ulDataLen = 0;
																	data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has and subcommand.
																}
															}
															else
															{
																unsigned char* pucBuffer = NULL;
																unsigned long ulBufferLen = 0;
																char errorstring[NET_ERRORSTRING_LEN];
																nReturn = pDestObj->m_net.GetLine(&pucBuffer, &ulBufferLen, pDestObj->m_socket, NET_RCV_ONCE, errorstring);
																
																	pDestObj->m_bSending=false;
							LeaveCriticalSection(&pDestObj->m_critComm);

						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "received %s", pucBuffer); // Sleep(250); //(Dispatch message)


																if((nReturn<NET_SUCCESS)||(ulBufferLen==0)||(pucBuffer==NULL))
																{
																	data.m_ucSubCmd = (NET_CMD_NAK|0xe0);

																	free(data.m_pucData);  //destroy the buffer;
																	data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
																	if(data.m_pucData)
																	{
																		_snprintf((char*)data.m_pucData, NET_ERRORSTRING_LEN, "Error receiving data from %s.  %s: (%d) %s", pDestObj->m_pszServerName, ((nReturn<NET_SUCCESS)?"error":"no buffer"), nReturn, errorstring);
																		data.m_ulDataLen = strlen((char*)data.m_pucData);
																		data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
																	}
																	else
																	{
																		data.m_ulDataLen = 0;
																		data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has and subcommand.
																	}
																}
																else
																{
																	if(*(pucBuffer) == '*') // success
																	{

																		char* pch = (char*)pucBuffer + strlen(pchScene)+ 7;
	//AfxMessageBox(pch);

																		char delim[8];

																		if(g_plibretto->m_settings.m_pszDelim)
																		{
																			if((g_plibretto->m_settings.m_bUseUTF8)&&(stricmp(g_plibretto->m_settings.m_pszDelim, "#")==0))
																			{
																				if(strlen(g_plibretto->m_settings.m_pszDelim)<=0)
																				{
																					sprintf(delim, "§");
																				}
																				else
																				{
																					sprintf(delim, "�%s", g_plibretto->m_settings.m_pszDelim);
																				}
																			}
																			else
																			{
																				if(strlen(g_plibretto->m_settings.m_pszDelim)<=0)
																				{
																					sprintf(delim, "�");
																				}
																				else
																				{
																					sprintf(delim, "%s", g_plibretto->m_settings.m_pszDelim);
																				}
																			}
																		}
																		else
																		{
																			if(g_plibretto->m_settings.m_bUseUTF8)
																			{
																				sprintf(delim, "§");
																			}
																			else
																			{
																				sprintf(delim, "�");
																			}
																		}
																		

																		free(data.m_pucData);  //destroy the buffer;
																		data.m_pucData=(unsigned char*)malloc(DB_SQLSTRING_MAXLEN); // should be sufficiently large

																		if(data.m_pucData)
																		{

																			//Libretto?cmd=06&sub=06&data=scene�field1�type�defval�field2�type�defval�field3�type�defval 

																			sprintf((char*)data.m_pucData, "%s",  pchScene);

																			while((pch)&&(strlen(pch)))
																			{
																				char* pchDel = strstr(pch, delim);
																				if(pchDel)
																				{
																					*pchDel=0; pchDel+=strlen(delim);
																				}
										//AfxMessageBox(pch);

																				if(*pch =='\\')
																				{
																					pch=NULL;
																				}
																				if((pch)&&(strlen(pch)))
																				{
																					if(pchDel)
																					{
																						if(strnicmp(pchDel, "Boolean", 7)==0)
																						{
																							sprintf(errorstring, "�%s�1�True", pch);  // always 1 because "export", not anim
																						}
																						else
																						{
																							sprintf(errorstring, "�%s�1�", pch); // always 1 because "export", not anim
																						}
																					}
																					else
																					{
																						sprintf(errorstring, "�%s�1�", pch); // always 1 because "export", not anim
																					}

																					strcat((char*)data.m_pucData, errorstring );

																					pch = strchr(pchDel, '\\');
																					if(pch)
																					{
																						pch++;
																					}
																				}
																			}

					if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "The get scene command is responding with data [%s].", (char*)data.m_pucData);  //(Dispatch message)

																			data.m_ulDataLen = strlen((char*)data.m_pucData);
																			data.m_ucSubCmd = NET_CMD_ACK;
																			data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
																		}
																		else
																		{
																			data.m_ucSubCmd = (NET_CMD_NAK|0xc0);
																			data.m_ulDataLen = 0;
																			data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has and subcommand.
																		}

																	}
																	else
																	{
																		data.m_ucSubCmd = (NET_CMD_NAK|0xc0);

																		sprintf(errorstring, "%s", (char*)data.m_pucData);
																		free(data.m_pucData);  //destroy the buffer;
																		data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
																		if(data.m_pucData)
																		{
																			_snprintf((char*)data.m_pucData, NET_ERRORSTRING_LEN, "Error code %s from %s.", errorstring, pDestObj->m_pszServerName);
																			data.m_ulDataLen = strlen((char*)data.m_pucData);
																			data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
																		}
																		else
																		{
																			data.m_ulDataLen = 0;
																			data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has and subcommand.
																		}
																	}
																}
																if(pucBuffer)
																{
																	try
																	{
																		free(pucBuffer);
																	}
																	catch(...)
																	{
																	}
																}

															}





														}
														else
														{
															data.m_ucSubCmd = (NET_CMD_NAK|0xa0);
															free(data.m_pucData);  //destroy the buffer;
															data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
															if(data.m_pucData)
															{
																sprintf((char*)data.m_pucData, "No connection to %s.", pchServer);
																data.m_ulDataLen = strlen((char*)data.m_pucData);
																data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
															}
															else
															{
																data.m_ulDataLen = 0;
																data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data and subcommand.
															}
														}
													} break;
												default:
													{
														data.m_ucSubCmd = (NET_CMD_NAK|0x80);
														free(data.m_pucData);  //destroy the buffer;
														data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
														if(data.m_pucData)
														{
															sprintf((char*)data.m_pucData, "Destination %s has unsupported type %d.", pchServer, g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_usType);
															data.m_ulDataLen = strlen((char*)data.m_pucData);
															data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data but no subcommand.
														}
														else
														{
															data.m_ulDataLen = 0;
															data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.
														}
													} break;
												}// switch type
											}
											else // not enabled
											{
												data.m_ucSubCmd = (NET_CMD_NAK|0x60);
												free(data.m_pucData);  //destroy the buffer;
												data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
												if(data.m_pucData)
												{
													sprintf((char*)data.m_pucData, "Destination %s is not active.", pchServer);
													data.m_ulDataLen = strlen((char*)data.m_pucData);
													data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data but no subcommand.
												}
												else
												{
													data.m_ulDataLen = 0;
													data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.
												}			
											}
										}
										else // can't, no dest
										{
											data.m_ucSubCmd = (NET_CMD_NAK|0x40);
											free(data.m_pucData);  //destroy the buffer;
											data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
											if(data.m_pucData)
											{
												sprintf((char*)data.m_pucData, "No destination with host %s.", pchServer);
												data.m_ulDataLen = strlen((char*)data.m_pucData);
												data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data but no subcommand.
											}
											else
											{
												data.m_ulDataLen = 0;
												data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.
											}			

										}
									}
									else
									{
										data.m_ucSubCmd = (NET_CMD_NAK|0x20);
										free(data.m_pucData);  //destroy the buffer;
										data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
										if(data.m_pucData)
										{
											sprintf((char*)data.m_pucData, "No scene was specified.");
											data.m_ulDataLen = strlen((char*)data.m_pucData);
											data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data but no subcommand.
										}
										else
										{
											data.m_ulDataLen = 0;
											data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.
										}			
									}
									//if(pchScene) free(pchScene); NO!!!
									
								}
								else
								{
									data.m_ucSubCmd = NET_CMD_NAK;
									free(data.m_pucData);  //destroy the buffer;
									data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
									if(data.m_pucData)
									{
										sprintf((char*)data.m_pucData, "No server was specified.");
										data.m_ulDataLen = strlen((char*)data.m_pucData);
										data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data but no subcommand.
									}
									else
									{
										data.m_ulDataLen = 0;
										data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.
									}			
								}

								//if(pchServer) free(pchServer);  NO!!!
							}
							else
							{
								data.m_ucCmd = NET_CMD_NAK;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.
							}

						} break;
					case LIBRETTO_CMD_GETTHUMB://	   0xa1

						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case LIBRETTO_CMD_GETMEDIA://					0xa2 // gets media list from a destination
						{
							// really just need host identifier.  for chyron.  aux data needed for WC
							// so, host



/*
					char* pchServer = szServer.GetBuffer(1);
					int nDestIndex = ReturnDestinationIndex(pchServer);
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_QUEUE)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "got item for server %s from queue", pchServer); //  Sleep(250);//(Dispatch message)

					if((nDestIndex>=0)&&(m_ppDestObj)&&(m_ppDestObj[nDestIndex])&&(m_ppDestObj[nDestIndex]->m_ulFlags&LIBRETTO_FLAG_ENABLED))
					{
						// only do anything if its enabled.
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_QUEUE)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "server %s enabled, socket:%d, thread:%d", pchServer, m_ppDestObj[nDestIndex]->m_socket, m_ppDestObj[nDestIndex]->m_bConnThreadStarted);//   Sleep(250);//(Dispatch message)

					//	int nHostReturn = LIBRETTO_ERROR;
						if((m_ppDestObj[nDestIndex]->m_socket == NULL)&&(!m_ppDestObj[nDestIndex]->m_bConnThreadStarted))
					}

*/
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "getmedia %x %x [%s] %d", data.m_ucCmd, data.m_ucSubCmd, data.m_pucData?(char*)data.m_pucData:"NULL", data.m_ulDataLen); //  Sleep(250);//(Dispatch message)

							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)
							{
								char* pchData = (char*)malloc(data.m_ulDataLen+1);
								unsigned long ulData = data.m_ulDataLen;

								char* pchServer = NULL;
								char* pchAuxData = NULL;
								
								CSafeBufferUtil lsbu;
								if(pchData)
								{
									strcpy(pchData, (char*)data.m_pucData);

if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "getmedia [%s]->[%s]", data.m_pucData?(char*)data.m_pucData:"NULL", pchData?pchData:"NULL"); //  Sleep(250);//(Dispatch message)

									ulData = strlen(pchData);

									pchServer = lsbu.Token(pchData, ulData, "|", MODE_SINGLEDELIM);

if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "getmedia extracted [%s]", pchServer?pchServer:"NULL"); //  Sleep(250);//(Dispatch message)


									if((pchServer)&&(ulData>strlen(pchServer))) // don't bother if we have all the data already.
									{
										pchAuxData = lsbu.Token(NULL, NULL, "|", MODE_SINGLEDELIM);
									}
								}
								

if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "get media with %s", pchServer?pchServer:"NULL"); //  Sleep(250);//(Dispatch message)

								


								if((pchServer)&&(strlen(pchServer)))
								{

									// need to extract host.
									
									// ok cool... have host, check host, if good, get media list (scene list) info...
									int nDestIndex = g_plibretto->m_data.ReturnDestinationIndex(pchServer);


									if((nDestIndex>=0)&&(g_plibretto->m_data.m_ppDestObj)&&(g_plibretto->m_data.m_ppDestObj[nDestIndex]))
									{ 
										if(g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_ulFlags&LIBRETTO_FLAG_ENABLED)
										{
											switch(g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_usType)
											{
											case CX_DESTTYPE_CHYRON_CHANNELBOX: // the only one we support currently...
												{
													if(
															(g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_socket != NULL)
														&&(!g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_bConnThreadStarted)
														)
													{
// To get a listing of scene IDs from a graphics box (note that on Channel Box, the list will be only those scenes that are open, loaded, or playing - a UI might want to specifcy this):
// Libretto?cmd=a2&sub=01&data=host when sub=01, it causes an asynchronous thumbnail download to initiate on all scenes in the return listing
// Libretto?cmd=a2&data=host  when sub is omitted or anything but 01, thumbnails are not downloaded.
// data is host name or IP, as it exists in the destinations table
// the return will be the following:
// Libretto?cmd=06&sub=06&data=scene1�scene2�scene2�scene2 

														bool bDownloadThumbs =false;

														if((data.m_ucType&NET_TYPE_HASSUBC)&&(data.m_ucSubCmd ==0x01))
														{
															bDownloadThumbs = true;

						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
	g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "The get media command has been received with a download request.");  //(Dispatch message)
														}
														else
														{
						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
	g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "The get media command has been received.");  //(Dispatch message)
														}
// have to loop thru various states:
//	Opened
//	Loaded
//	Loading
//	Playing
//	Stopped  //doesnt return anything
//	Closed  //doesnt return anything
//	NonExistent  //doesnt make sense here



//B\QL\State\\<CR><LF>
//Responses will take the form:
//*B\QL\SceneId1�State1\SceneId2�State2\..\\<CR><LF>

														CLibrettoDestinationObject* pDestObj = g_plibretto->m_data.m_ppDestObj[nDestIndex];
														CLibrettoList listobj;
														char errorstring[NET_ERRORSTRING_LEN];

														bool bError = false;
														char buffer[MAX_PATH];//should be enough.
														int i=0;  //going to loop thru the states.
														while((i<6)&&(!bError))
														{
															switch(i)
															{
															default:
															case 0: strcpy(buffer, "B\\QL\\Opened\\\\"); break;
															case 1: strcpy(buffer, "B\\QL\\Loaded\\\\"); break;
															case 2: strcpy(buffer, "B\\QL\\Loading\\\\"); break;
															case 3: strcpy(buffer, "B\\QL\\Playing\\\\"); break;
															case 4: strcpy(buffer, "B\\QL\\Stopped\\\\"); break;
															case 5: strcpy(buffer, "B\\QL\\Closed\\\\"); break;
															}

if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDQUEUE|LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_handler", "sending: %s", buffer); // Sleep(250); //(Dispatch message)
									
							EnterCriticalSection(&pDestObj->m_critComm);
															pDestObj->m_bSending=true;
															int nReturn = pDestObj->SendTelnetCommand(buffer, strlen(buffer), EOLN_CRLF);

															if(nReturn<LIBRETTO_SUCCESS)
															{
																	pDestObj->m_bSending=false;
							LeaveCriticalSection(&pDestObj->m_critComm);
																//**MSG
																g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:telnet", 
																	"telnet command not successful on %s.", pDestObj->m_pszServerName );//   Sleep(50);//(Dispatch message)
																bError = true;
																data.m_ucSubCmd = (NET_CMD_NAK|0xd0);

																if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
																data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
																if(data.m_pucData)
																{
																	_snprintf((char*)data.m_pucData, NET_ERRORSTRING_LEN, "Error sending data to %s. (%d)", pDestObj->m_pszServerName, nReturn);
																	data.m_ulDataLen = strlen((char*)data.m_pucData);
																	data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
																}
																else
																{
																	data.m_ulDataLen = 0;
																	data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has and subcommand.
																}
															}
															else
															{
																unsigned char* pucBuffer = NULL;
																unsigned long ulBufferLen = 0;
																nReturn = pDestObj->m_net.GetLine(&pucBuffer, &ulBufferLen, pDestObj->m_socket, NET_RCV_ONCE, errorstring);
																
																	pDestObj->m_bSending=false;
							LeaveCriticalSection(&pDestObj->m_critComm);

						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "received %s", pucBuffer); // Sleep(250); //(Dispatch message)


																if((nReturn<NET_SUCCESS)||(ulBufferLen==0)||(pucBuffer==NULL))
																{
																	bError = true;
																	data.m_ucSubCmd = (NET_CMD_NAK|0xe0);

																	if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
																	data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
																	if(data.m_pucData)
																	{
																		_snprintf((char*)data.m_pucData, NET_ERRORSTRING_LEN, "Error receiving data from %s.  %s: (%d) %s", pDestObj->m_pszServerName, ((nReturn<NET_SUCCESS)?"error":"no buffer"), nReturn, errorstring);
																		data.m_ulDataLen = strlen((char*)data.m_pucData);
																		data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
																	}
																	else
																	{
																		data.m_ulDataLen = 0;
																		data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has and subcommand.
																	}
																}
																else
																{
																	if(*(pucBuffer) == '*') // success
																	{

																		char* pch = (char*)pucBuffer + 6;
	//AfxMessageBox(pch);
//	g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "** looking at [%s].", pch);  //(Dispatch message)

																		char delim[8];

																		if(g_plibretto->m_settings.m_pszDelim)
																		{
																			if((g_plibretto->m_settings.m_bUseUTF8)&&(stricmp(g_plibretto->m_settings.m_pszDelim, "#")==0))
																			{
																				if(strlen(g_plibretto->m_settings.m_pszDelim)<=0)
																				{
																					sprintf(delim, "§");
																				}
																				else
																				{
																					sprintf(delim, "�%s", g_plibretto->m_settings.m_pszDelim);
																				}
																			}
																			else
																			{
																				if(strlen(g_plibretto->m_settings.m_pszDelim)<=0)
																				{
																					sprintf(delim, "�");
																				}
																				else
																				{
																					sprintf(delim, "%s", g_plibretto->m_settings.m_pszDelim);
																				}
																			}
																		}
																		else
																		{
																			if(g_plibretto->m_settings.m_bUseUTF8)
																			{
																				sprintf(delim, "§");
																			}
																			else
																			{
																				sprintf(delim, "�");
																			}
																		}

//	g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "**  %d %d %d [%s][%s]", ((g_plibretto->m_settings.m_pszDelim!=NULL)?1:0),  ((pch!=NULL)?1:0), (strlen(pch)), g_plibretto->m_settings.m_pszDelim, delim);  //(Dispatch message)
																		while((pch)&&(strlen(pch)))
																		{
																			char* pchDel = strstr(pch, delim);
																			if(pchDel)
																			{
																				(*pchDel)=0; pchDel+=strlen(delim);
																			}
																			else break;  //break out of while, no more delimiters, we are done
									//AfxMessageBox(pch);
//	g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "** checking [%s]%d after %s: [%s]%d", pch,pch, delim, pchDel,pchDel);  //(Dispatch message)

																			if(*pch =='\\')
																			{
																				pch=NULL;
																			}

																			if((pch)&&(strlen(pch)))
																			{
//	g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "** adding [%s].", pch);  //(Dispatch message)

																				char* pchitem = (char*)malloc(strlen(pch)+1);
																				if(pchitem)
																				{
																					strcpy(pchitem, pch);
																					if(listobj.AddUnique(pchitem)==LIBRETTO_ERROR)
																					{
																						try{free(pchitem);} catch(...){}
																					}
																				}

																				pch = strchr(pchDel, '\\');
																				if(pch)
																				{
																					pch++;
																				}
																			}
																		}

																	}
																	else
																	{
																		bError = true;

																		data.m_ucSubCmd = (NET_CMD_NAK|0xc0);

																		sprintf(errorstring, "%s", (char*)data.m_pucData);
																		if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
																		data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
																		if(data.m_pucData)
																		{
																			_snprintf((char*)data.m_pucData, NET_ERRORSTRING_LEN, "Error code %s from %s.", errorstring, pDestObj->m_pszServerName);
																			data.m_ulDataLen = strlen((char*)data.m_pucData);
																			data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
																		}
																		else
																		{
																			data.m_ulDataLen = 0;
																			data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has and subcommand.
																		}
																	}
																}
																if(pucBuffer)
																{
																	try
																	{
																		free(pucBuffer);
																	}
																	catch(...)
																	{
																	}
																}

															}  
															i++;  
														}// while loop on comands

														// have to assmeble full response, aggregate of mutiple calls, here.


														if(!bError)
														{
															free(data.m_pucData);  //destroy the buffer;

															int q=0, lx=0;
															while(q<listobj.m_nNumItems)
															{
																if((listobj.m_ppchItems)&&(listobj.m_ppchItems[q]))
																{
																	lx+= (strlen(listobj.m_ppchItems[q])+1); // 1 for delim
																}
																q++;
															}

															if(lx>0)
															{
																data.m_pucData=(unsigned char*)malloc(lx+1); 
																if(data.m_pucData)
																{
																	*(data.m_pucData) =0; // null term the beginning
																	q=0;
																	bool bFirst = true;
																	while(q<listobj.m_nNumItems)
																	{
																		if((listobj.m_ppchItems)&&(listobj.m_ppchItems[q]))
																		{
																			if(strlen(listobj.m_ppchItems[q])>0)
																			{
																				if(bFirst)
																				{
																					bFirst=false;
																					sprintf(errorstring, "%s", listobj.m_ppchItems[q]);
																				}
																				else
																				{
																					sprintf(errorstring, "�%s", listobj.m_ppchItems[q]);
																				}
																				strcat((char*)data.m_pucData, errorstring );


																				if(bDownloadThumbs)
																				{
																					LibrettoThreadArgs_t* plts = new LibrettoThreadArgs_t;
																					if(plts)
																					{
																						plts->pvoid = malloc(strlen(listobj.m_ppchItems[q])+1);
																						if(plts->pvoid)
																						{
																							strcpy((char*)plts->pvoid, listobj.m_ppchItems[q] );
																							plts->pDest = pDestObj;
																							if(_beginthread(LibrettoAsynchThumbnailRetrieveThread, 0, (void*)(plts))==-1)
																							{
																							//error.
																							}
																							else
																							{
					if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "begun download thread for [%s].", listobj.m_ppchItems[q]);  //(Dispatch message)

																							}

																						}
																						else
																						{
																							try{ delete plts; } catch(...){}
																						}
																					}
																				}

																			}
																		}
																		q++;
																	}
					if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "The get media command is responding with data [%s].", (char*)data.m_pucData);  //(Dispatch message)

																	data.m_ulDataLen = strlen((char*)data.m_pucData);

																	data.m_ucSubCmd = NET_CMD_ACK;
																	data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
																}
																else
																{
																	data.m_ucSubCmd = (NET_CMD_NAK|0xc0);
																	data.m_ulDataLen = 0;
																	data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has and subcommand.
																}

															}
															else // want to send blank data
															{
																data.m_pucData = NULL;
																data.m_ucSubCmd = NET_CMD_ACK;
																data.m_ulDataLen = 0;
																data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has and subcommand.
																
															}

														}


													}
													else
													{
														data.m_ucSubCmd = (NET_CMD_NAK|0xa0);
														if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
														data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
														if(data.m_pucData)
														{
															sprintf((char*)data.m_pucData, "No connection to %s.", pchServer);
															data.m_ulDataLen = strlen((char*)data.m_pucData);
															data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
														}
														else
														{
															data.m_ulDataLen = 0;
															data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data and subcommand.
														}
													}
												} break;
											case CX_DESTTYPE_VDS_WATERCOOLER: // may as well use getmedia do do the Auth stuff - its sort of getting media.  a URL anyway.
												{
													if(
															(g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_socket != NULL)
														&&(!g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_bConnThreadStarted)
														)
													{
						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
	g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "The get media command has been received.");  //(Dispatch message)

														bool bSetPIN =false;
														bool bFB =false;

														if((data.m_ucType&NET_TYPE_HASSUBC)&&(data.m_ucSubCmd&0x80))
														{
															bFB = true;
														}


														if((data.m_ucType&NET_TYPE_HASSUBC)&&(data.m_ucSubCmd&0x01))
														{
															bSetPIN = true;

						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
	g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "The get media command has been received to issue PIN request with [%s].",
																	(data.m_pucData)?((char*)(data.m_pucData)):"NULL data");  //(Dispatch message)
														}
														else
														{
						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
	g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "The get media command has been received to request auth URL with [%s].",
																	(data.m_pucData)?((char*)(data.m_pucData)):"NULL data");  //(Dispatch message)
														}

														// from here, send a command over to the thing.
														char buffer[MAX_PATH];//should be enough.
														bool bError = false;
														char errorstring[NET_ERRORSTRING_LEN];

														if((pchAuxData==NULL)||(strlen(pchAuxData)<=0))
														{

															data.m_ucSubCmd = (NET_CMD_NAK|0xa0);
															if(data.m_pucData!=NULL)	 free(data.m_pucData);  //destroy the buffer;
															data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
															if(data.m_pucData)
															{
																sprintf((char*)data.m_pucData, "blank data received");
																data.m_ulDataLen = strlen((char*)data.m_pucData);
																data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
															}
															else
															{
																data.m_ulDataLen = 0;
																data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data and subcommand.
															}

														}
														else
														{
															if(bSetPIN)
															{
															// looking for:	S\P\<PIN>\<username>\\  									

																if(bFB)
																{
																	_snprintf(buffer, MAX_PATH-1, "S\\F\\P\\%s\\\\", pchAuxData);   //data must be <PIN>\<username> with the slash in there.
																}
																else
																{
																	_snprintf(buffer, MAX_PATH-1, "S\\T\\P\\%s\\\\", pchAuxData);   //data must be <PIN>\<username> with the slash in there.
																}

															}
															else
															{
															// looking for:	S\U\<username>\\  

																if(bFB)
																{
																	_snprintf(buffer, MAX_PATH-1, "S\\F\\U\\%s\\\\", pchAuxData); 
																}
																else
																{
																	_snprintf(buffer, MAX_PATH-1, "S\\T\\U\\%s\\\\", pchAuxData); 
																}
															}


															CLibrettoDestinationObject* pDestObj = g_plibretto->m_data.m_ppDestObj[nDestIndex];
//															CLibrettoList listobj;


	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_handler", "sending: %s", buffer); // Sleep(250); //(Dispatch message)
									
							EnterCriticalSection(&pDestObj->m_critComm);
															pDestObj->m_bSending=true;
														//	int nReturn = LIBRETTO_SUCCESS-1;//pDestObj->SendTelnetCommand(buffer, strlen(buffer));

															int nReturn = pDestObj->SendTelnetCommand(buffer, strlen(buffer), EOLN_CRLF);

															if(nReturn<LIBRETTO_SUCCESS)
															{
																pDestObj->m_bSending=false;
							LeaveCriticalSection(&pDestObj->m_critComm);
																//**MSG
																g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:telnet", 
																	"telnet command not successful on %s.", pDestObj->m_pszServerName );//   Sleep(50);//(Dispatch message)
																bError = true;
																data.m_ucSubCmd = (NET_CMD_NAK|0xd0);

																if(data.m_pucData!=NULL)	 free(data.m_pucData);  //destroy the buffer;
																data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
																if(data.m_pucData)
																{
																	_snprintf((char*)data.m_pucData, NET_ERRORSTRING_LEN, "Error sending data to %s. (%d)", pDestObj->m_pszServerName, nReturn);
																	data.m_ulDataLen = strlen((char*)data.m_pucData);
																	data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
																}
																else
																{
																	data.m_ulDataLen = 0;
																	data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has and subcommand.
																}
															}
															else
															{
																unsigned char* pucBuffer = NULL;
																unsigned long ulBufferLen = 0;
																nReturn = pDestObj->m_net.GetLine(&pucBuffer, &ulBufferLen, pDestObj->m_socket, NET_RCV_ONCE, errorstring);
																
																pDestObj->m_bSending=false;
							LeaveCriticalSection(&pDestObj->m_critComm);

						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "received %s", (char*)pucBuffer); // Sleep(250); //(Dispatch message)


																if((nReturn<NET_SUCCESS)||(ulBufferLen==0)||(pucBuffer==NULL))
																{
																	bError = true;
																	data.m_ucSubCmd = (NET_CMD_NAK|0xe0);

																	if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
																	data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
																	if(data.m_pucData)
																	{
																		_snprintf((char*)data.m_pucData, NET_ERRORSTRING_LEN, "Error receiving data from %s.  %s: (%d) %s", pDestObj->m_pszServerName, ((nReturn<NET_SUCCESS)?"error":"no buffer"), nReturn, errorstring);
																		data.m_ulDataLen = strlen((char*)data.m_pucData);
																		data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
																	}
																	else
																	{
																		data.m_ulDataLen = 0;
																		data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has and subcommand.
																	}
																}
																else
																{
																	if(*(pucBuffer) == '*') // success
																	{
																		if(bSetPIN)
																		{
																			// great!  we are done, send success

																			data.m_ucCmd = NET_CMD_ACK;
																			if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
																			data.m_pucData=NULL;
																			data.m_ucSubCmd = NET_CMD_ACK;
																			data.m_ulDataLen = 0;
																			// has no data, just ack.
																			data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.

																			
																		}
																		else
																		{
																			// have to return the URL.

																			// the response in pucBuffer is of this format
																				
																			// *\r\nhttps://twitter.com/oauth/authorize?oauth_token=rZshIOfHEZpQJTpwzIqlmEP1d2Tf3zRw8JY2UfE3VQ8

																			data.m_ucCmd = NET_CMD_ACK;

																			data.m_ulDataLen = strlen((char*)pucBuffer) - 3;  // subtract success preamble

																			if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;


																			data.m_pucData=NULL;
																			data.m_ucSubCmd = NET_CMD_ACK;


																			// has no data, just ack.
																			data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data but no subcommand.

																			data.m_pucData = (unsigned char*)malloc(data.m_ulDataLen+1);

																			strcpy((char*)data.m_pucData, (char*)(pucBuffer+3));

																			while((*(data.m_pucData+data.m_ulDataLen-1) == 10)||(*(data.m_pucData+data.m_ulDataLen-1) == 13))
																			{
																				*(data.m_pucData+data.m_ulDataLen-1) = 0;
																				data.m_ulDataLen--;
																			}

																			data.m_ulDataLen = strlen((char*)data.m_pucData);

						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "sending back data: %s", (char*)data.m_pucData); // Sleep(250); //(Dispatch message)


																		}
																
																	}
																	else
																	{
																		data.m_ucSubCmd = (NET_CMD_NAK|0xc0);

																		sprintf(errorstring, "%s", (char*)pucBuffer);
	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "error: received data: %s", (char*)pucBuffer); // Sleep(250); //(Dispatch message)


																		if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
																		data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
																		if(data.m_pucData)
																		{
																			_snprintf((char*)data.m_pucData, NET_ERRORSTRING_LEN, "Error code %s from %s.", errorstring, pDestObj->m_pszServerName);
																			data.m_ulDataLen = strlen((char*)data.m_pucData);
																			data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
																		}
																		else
																		{
																			data.m_ulDataLen = 0;
																			data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has and subcommand.
																		}

																	}
																}
																if(pucBuffer)
																{
																	try
																	{
																		free(pucBuffer);
																	}
																	catch(...)
																	{
																	}
																}
															}
														}
													}
													else
													{
														data.m_ucSubCmd = (NET_CMD_NAK|0xa0);
														free(data.m_pucData);  //destroy the buffer;
														data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
														if(data.m_pucData)
														{
															sprintf((char*)data.m_pucData, "No connection to %s.", pchServer);
															data.m_ulDataLen = strlen((char*)data.m_pucData);
															data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data and subcommand.
														}
														else
														{
															data.m_ulDataLen = 0;
															data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data and subcommand.
														}
													}

													

												} break;

											default:
												{
													data.m_ucSubCmd = (NET_CMD_NAK|0x80);
													free(data.m_pucData);  //destroy the buffer;
													data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
													if(data.m_pucData)
													{
														sprintf((char*)data.m_pucData, "Destination %s has unsupported type %d.", pchServer, g_plibretto->m_data.m_ppDestObj[nDestIndex]->m_usType);
														data.m_ulDataLen = strlen((char*)data.m_pucData);
														data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data but no subcommand.
													}
													else
													{
														data.m_ulDataLen = 0;
														data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.
													}
												} break;
											}// switch type
										}
										else // not enabled
										{
											data.m_ucSubCmd = (NET_CMD_NAK|0x60);
											free(data.m_pucData);  //destroy the buffer;
											data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
											if(data.m_pucData)
											{
												sprintf((char*)data.m_pucData, "Destination %s is not active.", pchServer);
												data.m_ulDataLen = strlen((char*)data.m_pucData);
												data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data but no subcommand.
											}
											else
											{
												data.m_ulDataLen = 0;
												data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.
											}			
										}
									}
									else // can't, no dest
									{
										data.m_ucSubCmd = (NET_CMD_NAK|0x40);
										free(data.m_pucData);  //destroy the buffer;
										data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
										if(data.m_pucData)
										{
											sprintf((char*)data.m_pucData, "No destination with host %s.", pchServer);
											data.m_ulDataLen = strlen((char*)data.m_pucData);
											data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data but no subcommand.
										}
										else
										{
											data.m_ulDataLen = 0;
											data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.
										}			

									}

									// do not free these, Token() does not allocate, just gives pointers to the segments.
						//			if(pchServer) try{ free(pchServer);} catch(...){}
						//			if(pchAuxData) try{ free(pchAuxData);} catch(...){}
									
								}
								else
								{
									data.m_ucSubCmd = NET_CMD_NAK;
									free(data.m_pucData);  //destroy the buffer;
									data.m_pucData=(unsigned char*)malloc(NET_ERRORSTRING_LEN);
									if(data.m_pucData)
									{
										sprintf((char*)data.m_pucData, "No server was specified.");
										data.m_ulDataLen = strlen((char*)data.m_pucData);
										data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC|NET_TYPE_HASDATA; // has data but no subcommand.
									}
									else
									{
										data.m_ulDataLen = 0;
										data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASSUBC;//|NET_TYPE_HASDATA; // has data but no subcommand.
									}			
								}

								//if(pchServer) free(pchServer);  NO!!!

								if(pchData) try{ free(pchData);} catch(...){}

							}
							else
							{
								data.m_ucCmd = NET_CMD_NAK;
								data.m_pucData=NULL;
								data.m_ulDataLen = 0;
								data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.
							}

						} break;

/// the data for the following commands need to match the queue table format, which is:
		// need to change to filename_local varchar(512), filename_remote varchar(512) to manage hex encoded payloads
	//create table CommandQueue (itemid int identity(1,1) NOT NULL, local varchar(2048), remote varchar(256), 
	//action int, host varchar(64), timestamp float, username varchar(32), event_itemid int, message varchar(512));

				//		so, data payload is the following:
// local|remote|action|host|timestamp|username|event_itemid_message
// note that pipe chars must be escaped. as in:
// local has a single || pipe character in it|remote|action| etc...

					case LIBRETTO_CMD_QUEUE://						0xc0 // puts thing in queue and waits for response
					case LIBRETTO_CMD_QUEUE_ASYNC://			0xc1 // puts thing in queue
					case LIBRETTO_CMD_QUEUE_QUERY://			0xc2 // retrieves answer from queue (has deletion option)
					case LIBRETTO_CMD_CMDQUEUE://					0xc5 // puts thing in command queue and waits for response
					case LIBRETTO_CMD_CMDQUEUE_ASYNC://		0xc6 // puts thing in command queue and returns immediately
					case LIBRETTO_CMD_CMDQUEUE_QUERY://		0xc7 // retrieves answer from queue (has deletion option)
					case LIBRETTO_CMD_DIRECT://						0xcf // skips queues, commands box directly
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case LIBRETTO_CMD_EXSET://							0xea // sets exchange counter to a specific value
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case LIBRETTO_CMD_EXINC://							0xeb // increments exchange counter
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)
							{	
								data.m_ucSubCmd = (unsigned char) g_plibretto->m_data.IncrementDatabaseMods((char*)data.m_pucData);
								free(data.m_pucData);  //destroy the buffer;
							}
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.

							data.m_ucType = NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case LIBRETTO_CMD_EXGET://							0xec // gets exchange mod value
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case LIBRETTO_CMD_MODSET://							0xed // sets exchange mod value, skips exchange table
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case LIBRETTO_CMD_MODINC://							0xee // increments exchange mod value, skips exchange table
						{
							// *****************
							// NOT SUPPORTED YET
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;

					case CX_CMD_GETSTATUS: //	0x99 // gets status info
						{
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							unsigned long ulStatus;
							// assemble if there is 

							char* pch = g_plibretto->m_data.GetStatusText(&ulStatus);  // allocates mem, must free after use
							if(pch)
							{
								data.m_ucSubCmd = (unsigned char)(LIBRETTO_ICON_MASK&ulStatus);
								data.m_pucData=(unsigned char*)pch;
								data.m_ulDataLen = strlen(pch);
								data.m_ucType = NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data.
							}
							else
							{
								data.m_ucCmd = NET_CMD_NAK;
								data.m_ucSubCmd = NET_CMD_NAK;
								data.m_ucType = NET_TYPE_HASSUBC|NET_TYPE_PROTOCOL1;//NET_TYPE_HASDATA; 
							}

						} break;
					case CX_CMD_BYE:
						{
	g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "The Global Kill command has been received.");  //(Dispatch message)

							bSetGlobalKill = true;
							g_plibretto->m_data.m_bQuietKill = true;
							//just ack
							data.m_ucCmd = NET_CMD_ACK;
							if(data.m_pucData!=NULL)	free(data.m_pucData);  //destroy the buffer;
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					default:
						{
							//just ack
							data.m_ucCmd = NET_CMD_ACK;

		//					char repbuf[64];
							if(data.m_pucData!=NULL)
							{
		/*
								_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

								for(unsigned long q=0; q<data.m_ulDataLen; q++)
								{
									if(strlen(repbuf)<63)
										strncat(repbuf, (char*)(data.m_pucData+q), 1);
								}
								if(strlen(repbuf)<63)
									strcat(repbuf, "]");
								data.m_ulDataLen = strlen(repbuf);
								repbuf[data.m_ulDataLen] = 0;  // just in case
		*/
								free(data.m_pucData);  //destroy the buffer;
							}
		/*
							else
							{
								_snprintf(repbuf, 63, "data was NULL");
								data.m_ulDataLen = strlen(repbuf); 
							}

							data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
							if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
							else 
							{
								data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
								data.m_ulDataLen = 0;
							}
		*/
							data.m_pucData=NULL;
							data.m_ulDataLen = 0;
							
							// has no data, just ack.
							data.m_ucType = NET_TYPE_PROTOCOL1;//|NET_TYPE_HASDATA; // has data but no subcommand.

						} break;
					}

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Libretto:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Libretto:CommandHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.
	if (bSetGlobalKill)	//g_bKillThread = true;
	((CLibrettoHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}

void LibrettoStatusHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it
		strcpy(pszInfo, "");  // clear it

		CNetData data;

		bool bCloseCommand = false;

		do
		{
			// following line fills out the data structure
			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Libretto:CommandHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Libretto:CommandHandlerThread");
						}
						// we had an error receiving data and then anotehr trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Libretto:CommandHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Libretto:CommandHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.

}


int CLibrettoMain::SendMsg(int nType, char* pszSender, char* pszMessage, ...)
{
// g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "((%d)&&(%d)) ||((%d)&&(%d)), %s %s",m_pdbmsg, m_pdbConnMsg,
//	 m_data.m_pdb, m_data.m_pdbConn, (pszMessage==NULL)?"NULL":pszMessage, (m_settings.m_pszMessages==NULL)?"NULL":m_settings.m_pszMessages);
	if(
			(
			  ((m_pdbmsg)&&(m_pdbConnMsg))
			||((m_data.m_pdb)&&(m_data.m_pdbConn))
			)
		&&(pszMessage)
		&&(strlen(pszMessage))
		&&(m_settings.m_pszMessages)
		&&(strlen(m_settings.m_pszMessages)
			)
		)
	{
		char dberrorstring[DB_ERRORSTRING_LEN];
		char szSQL[DB_SQLSTRING_MAXLEN];
		char szMessage[DB_SQLSTRING_MAXLEN];

		va_list marker;
		// create the formatted output string
		va_start(marker, pszMessage); // Initialize variable arguments.
		_vsnprintf(szMessage, DB_SQLSTRING_MAXLEN-1, pszMessage, (va_list) marker);
		va_end( marker );             // Reset variable arguments.
		_timeb timestamp;
		_ftime( &timestamp );

		CDBUtil*						pdb;
		CDBconn*						pdbConn;

		if(m_pdbmsg) pdb=m_pdbmsg; else pdb=m_data.m_pdb;
		if(m_pdbConnMsg) pdbConn=m_pdbConnMsg; else pdbConn=m_data.m_pdbConn;

		if(strlen(szMessage)>SENDMSG_MESSAGE_MAXLEN) szMessage[SENDMSG_MESSAGE_MAXLEN-1]=0;
		if((pszSender)&&(strlen(pszSender)>SENDMSG_SENDER_MAXLEN)) pszSender[SENDMSG_SENDER_MAXLEN-1]=0;
		char* pchMessage = pdb->EncodeQuotes(szMessage);
		char* pchSender  = pdb->EncodeQuotes(pszSender);



		if(m_settings.m_bMillisecondMessaging) // milliseconds, and also uses identity field.
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime) \
VALUES ('%s', '%s', %d, %d.%03d)", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				timestamp.millitm
				);
		}
		else
		{
			_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "INSERT INTO %s (message, sender, flags, systime, id) \
SELECT '%s', '%s', %d, %d, case when max(id) is null then 1 else max(id) + 1 end from %s", // HARDCODE
				m_settings.m_pszMessages,
				pchMessage?pchMessage:szMessage,
				pchSender?pchSender:"unspecified",
				nType, // 1 is error, 0 is message
				(unsigned long)(timestamp.time - (timestamp.timezone*60)+(timestamp.dstflag?3600:0)), // local time....
				m_settings.m_pszMessages
				);
		}

//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "SQL : %s",szSQL);
		if(pchMessage) free(pchMessage);
		if(pchSender) free(pchSender);

		if(pdb->ExecuteSQL(pdbConn, szSQL, dberrorstring)>=DB_SUCCESS)
		{
			m_data.IncrementDatabaseMods(m_settings.m_pszMessages, dberrorstring, pdb, pdbConn);
			return LIBRETTO_SUCCESS;
		}
		else
		{
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "SQL error %s",dberrorstring);
		}
	}
	return LIBRETTO_ERROR;
}


void LibrettoAsynchConnThread(void* pvArgs)
{
	CLibrettoDestinationObject* pDestObj = (CLibrettoDestinationObject*) pvArgs;
	if(pDestObj==NULL) return;
	pDestObj->m_bConnThreadStarted = true;
	CBufferUtil bu;

// check pDestObj->m_pszServerName  for "COM"
	char neterrorstring[NET_ERRORSTRING_LEN];
	strcpy(neterrorstring,"");


	int nPort = g_plibretto->m_settings.GetPortOverride(pDestObj->m_usType);
	if(nPort <= 0 )	nPort = g_plibretto->m_settings.m_nPort;

	pDestObj->m_usPort  = (unsigned short) nPort;

if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Opening connection to %s:%d", pDestObj->m_pszServerName, nPort); // Sleep(250); //(Dispatch message)

	int nHostReturn = pDestObj->m_net.OpenConnection(pDestObj->m_pszServerName, nPort, &pDestObj->m_socket,
		g_plibretto->m_settings.m_nDefaultSendTimeoutMS,
		g_plibretto->m_settings.m_nDefaultReceiveTimeoutMS,
		neterrorstring); // re-establish

	if((nHostReturn>=NET_SUCCESS)&&(pDestObj->m_socket!=NULL))
	{
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Success opening connection to %s:%d", pDestObj->m_pszServerName, nPort); // Sleep(250); //(Dispatch message)

		g_plibretto->SendMsg(CX_SENDMSG_INFO, "Init connection", "Connection to %s:%d established", pDestObj->m_pszServerName, nPort);
		char tnbuffer[1024]; // more than enough for all connection types known so far
		int nBuflen = 4;
		
// have to clear the queue on reconnect, if it has been longer than some period....

		if(g_plibretto->m_settings.m_nClearQueueIfDisconnectedMS>=0)
		{
			_timeb now;
			_ftime(&now);

			if(
					((pDestObj->m_timebLastDisconnect.time + g_plibretto->m_settings.m_nClearQueueIfDisconnectedMS/1000)<now.time)
				||(((pDestObj->m_timebLastDisconnect.time + g_plibretto->m_settings.m_nClearQueueIfDisconnectedMS/1000)==now.time)&&((pDestObj->m_timebLastDisconnect.millitm + g_plibretto->m_settings.m_nClearQueueIfDisconnectedMS%1000)<now.millitm))
				)
			{
			//	clear the queue
				if((g_plibretto->m_data.m_pdb)&&(g_plibretto->m_data.m_pdbConn))
				{
					char szSQL[DB_SQLSTRING_MAXLEN];
					char dberrorstring[DB_ERRORSTRING_LEN];
					_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE host = '%s'", //HARDCODE
						((g_plibretto->m_settings.m_pszQueue)&&(strlen(g_plibretto->m_settings.m_pszQueue)))?g_plibretto->m_settings.m_pszQueue:"Queue",
						pDestObj->m_pszServerName);

if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Clearing queue: %s", szSQL); // Sleep(250); //(Dispatch message)
					if(g_plibretto->m_data.m_pdb->ExecuteSQL(g_plibretto->m_data.m_pdbConn, szSQL, dberrorstring)<DB_SUCCESS)
					{
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "SQL error %s",dberrorstring);
					}
				}

/*
				EnterCriticalSection(&pDestObj->m_critMemQueue);
				if(pDestObj->m_nPtrMemQueueOut >= 0 )
				{
					pDestObj->ClearMemQueue();
				}
				LeaveCriticalSection(&pDestObj->m_critMemQueue);
*/
			}
		}

		switch(pDestObj->m_usType)
		{
			
		case CX_DESTTYPE_HARRIS_ICONII: // else all the other types will just respond to \\, an error or something, but just something to init the conn.
			{
				sprintf(tnbuffer, "*\\\\%c%c", 13,10);
				nBuflen = 5;
			}	break;
		case CX_DESTTYPE_EVERTZ_7847FSE:  // need to init conn and kick off the timer, which can be entirely conatined in this thread.
			{
				// assemble the init message buffer.
				sprintf(tnbuffer, "");

				CSCTE104Util* p104 = (CSCTE104Util*)pDestObj->m_pvExternal; // for convenience.

				CSCTE104SingleMessage* sm = p104->CreateSingleMessageObject();
				if(sm)
				{
					sm->OpId1High =0x00;
					sm->OpId1Low =0x01;
					sm->MessageNumber = p104->IncrementSecondaryMessageNumber();
					unsigned char* pucBuf = p104->ReturnMessageBuffer(sm);
					if(pucBuf)
					{
						unsigned long ulLen = *(pucBuf+2);
						ulLen <<= 8; ulLen += *(pucBuf+3);
						memcpy(tnbuffer, pucBuf,  min(ulLen, 1024)); // 1024 should be more than enough, but use min just for safety
						nBuflen = ulLen;
						delete [] pucBuf;

	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO)) 
	{
//unsigned long ulSend = buflen;
//char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_DELIM32BIT);
//AfxMessageBox(pchSnd);

		unsigned long ulSend = nBuflen;
		char* pchSnd = bu.ReadableHex((char*)tnbuffer, &ulSend, MODE_FULLHEX);

		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "init_conn", "sending: %s", pchSnd?pchSnd:"(null)"); // Sleep(250); //(Dispatch message)

		if(pchSnd) free(pchSnd);
	}


					}
					delete sm;
				}
				else
				{
					//log the error
				}

			}	break;
		default:
			{
				sprintf(tnbuffer, "\\\\%c%c", 13,10);
			}	break;
		}


	EnterCriticalSection(&pDestObj->m_critComm);
		pDestObj->m_bSending=true;



		nHostReturn = pDestObj->SendTelnetCommand(tnbuffer, nBuflen, EOLN_NONE); 
		if((nHostReturn>LIBRETTO_ERROR)&&(pDestObj->m_socket!=NULL))
		{
			unsigned char* pucBuffer = NULL;
			unsigned long ulBufferLen = 0;

			struct timeval tv;
			tv.tv_sec = 1; tv.tv_usec = 500000;  // timeout value, 1.5 second for first connection response
			fd_set fds;
			int nNumSockets;
			FD_ZERO(&fds);
			FD_SET(pDestObj->m_socket, &fds);

//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Selecting");

			nNumSockets = select(0, &fds, NULL, NULL, &tv);
//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Select returned %d", nNumSockets);

			if ( nNumSockets == INVALID_SOCKET )
			{
				int nErrorCode = WSAGetLastError();
				char* pchError = pDestObj->m_net.WinsockEnglish(nErrorCode);
				if(pchError)
				{
					if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Invalid socket on connection to %s:%d\n%s", 
							pDestObj->m_pszServerName, nPort, pchError?pchError:""); // Sleep(250); //(Dispatch message)

					g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Init connection", "Invalid socket on connection to %s:%d\n%s", pDestObj->m_pszServerName, nPort, pchError);
					LocalFree(pchError);
				}
				pDestObj->m_net.CloseConnection(pDestObj->m_socket);
				pDestObj->m_socket=NULL;
				pDestObj->m_bSending=false;

			}
			else
			if(
					(nNumSockets==0) // 0 = timed out, -1 = error
				||(!(FD_ISSET(pDestObj->m_socket, &fds)))
				) 
			{ 
				// no data, no problem, just return for now.
				pDestObj->m_bSending=false;
			} 
			else // there is recv data.
			{
//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//						g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "there is receive data");
				nHostReturn = pDestObj->m_net.GetLine(&pucBuffer, &ulBufferLen, pDestObj->m_socket);
				pDestObj->m_bSending=false;
				if((nHostReturn<=LIBRETTO_ERROR)||(pDestObj->m_socket==NULL))
				{
					_ftime(&pDestObj->m_timebLastDisconnect);

					int nErrorCode = WSAGetLastError();
					char* pchError = pDestObj->m_net.WinsockEnglish(nErrorCode);
					if(pchError)
					{
						if((ulBufferLen<=0)||(pucBuffer==NULL))
						{
							if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
								g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Connection to %s:%d terminated\n%s", 
									pDestObj->m_pszServerName, nPort, pchError?pchError:""); // Sleep(250); //(Dispatch message)

							g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Init connection", "Connection to %s:%d terminated\n%s", pDestObj->m_pszServerName, nPort, pchError);
						}
						else
						{
							if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
								g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Communication error on %s:%d\n%s", 
									pDestObj->m_pszServerName, nPort, pchError); // Sleep(250); //(Dispatch message)

							g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Init connection", "Communication error on %s:%d\n%s", pDestObj->m_pszServerName, nPort, pchError);
							if(pucBuffer) free(pucBuffer);
						}
						LocalFree(pchError);
					}
					pDestObj->m_net.CloseConnection(pDestObj->m_socket);
					pDestObj->m_socket=NULL;
//					pDestObj->m_bSending=false;
					
//		if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Error response on connection to %s:%d", pDestObj->m_pszServerName, nPort); // Sleep(250); //(Dispatch message)
				}
				else
				{

					switch(pDestObj->m_usType)
					{
					case CX_DESTTYPE_EVERTZ_7847FSE:  // need to kick off the timer, which can be entirely contained in this thread.
						{
							_ftime(&pDestObj->m_timebLastPing);
							pDestObj->m_timebLastPing.time -= 60; // back it off one minute so the keep alive will go immediately.


	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO)) 
	{
//unsigned long ulSend = buflen;
//char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_DELIM32BIT);
//AfxMessageBox(pchSnd);

		unsigned long ulSend = ulBufferLen;
		char* pchSnd = bu.ReadableHex((char*)pucBuffer, &ulSend, MODE_FULLHEX);

		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "init_conn", "received: %s", pchSnd?pchSnd:"(null)"); // Sleep(250); //(Dispatch message)

		if(pchSnd) free(pchSnd);
	}

						} break;
					default: 
						{
	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO)) 
		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "received: %s", pucBuffer?((char*)(pucBuffer)):"(null)"); // Sleep(250); //(Dispatch message)
						} break;
					}

				}
				if(pucBuffer) free(pucBuffer);
			}
		}
		else
		{
			pDestObj->m_bSending=false;

//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Error initializing connection to %s:%d", pDestObj->m_pszServerName, nPort); // Sleep(250); //(Dispatch message)
			pDestObj->m_net.CloseConnection(pDestObj->m_socket);
			pDestObj->m_socket=NULL;
		}
	LeaveCriticalSection(&pDestObj->m_critComm);

	}
	else
	{
//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
{
	if(strlen(neterrorstring)>0)
	{
		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Error opening connection to %s:%d\n%s", pDestObj->m_pszServerName, nPort, neterrorstring); // Sleep(250); //(Dispatch message)
	}
	else
	{
		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Error opening connection to %s:%d", pDestObj->m_pszServerName, nPort); // Sleep(250); //(Dispatch message)
	}
}		
		pDestObj->m_net.CloseConnection(pDestObj->m_socket);
		pDestObj->m_socket=NULL;
	}
	pDestObj->m_ulStatus = LIBRETTO_STATUS_UNINIT;
	pDestObj->m_bConnThreadStarted = false;
	_endthread();

}

void LibrettoConnMonitorThread(void* pvArgs)
{
	CLibrettoDestinationObject* pDestObj = (CLibrettoDestinationObject*) pvArgs;
	if(pDestObj==NULL) return;
	CBufferUtil bu;

	pDestObj->m_bMonThreadStarted = true;
	int nLastCheck = clock();
	while((!g_bKillThread)&&(pDestObj)&&(!pDestObj->m_bKillMonThread))
	{
//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "sending: %d, sock %d  [%d]",pDestObj->m_bSending, pDestObj->m_socket, clock()); 
		if((pDestObj->m_socket)&&(!pDestObj->m_bSending))
		{
			// monitor socket for disconnection.
			struct timeval tv;
			tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
			fd_set fds;
			int nNumSockets;
			FD_ZERO(&fds);
			if(!pDestObj->m_bSending)
			{
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:conn_mon", "selecting sock %d  [%d]", pDestObj->m_socket, clock()); 
				FD_SET(pDestObj->m_socket, &fds);
				nNumSockets = select(0, &fds, NULL, NULL, &tv);

if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:conn_mon", "selected sock %d, return=%d  [%d]", pDestObj->m_socket, nNumSockets, clock()); 
				if ( nNumSockets == INVALID_SOCKET )
				{
					int nErrorCode = WSAGetLastError();
					char* pchError = pDestObj->m_net.WinsockEnglish(nErrorCode);
					if(pchError)
					{
						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
							g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Invalid socket on connection to %s:%d\n%s", 
								pDestObj->m_pszServerName, pDestObj->m_usPort, pchError?pchError:""); // Sleep(250); //(Dispatch message)

						g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Connection monitor", "Invalid socket on connection to %s:%d\n%s", pDestObj->m_pszServerName, pDestObj->m_usPort, pchError);
						LocalFree(pchError);
					}
					else
					{
						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
							g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Invalid socket on connection to %s:%d", 
								pDestObj->m_pszServerName, pDestObj->m_usPort); // Sleep(250); //(Dispatch message)

						g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Connection monitor", "Invalid socket on connection to %s:%d", pDestObj->m_pszServerName, pDestObj->m_usPort);
					}
					pDestObj->m_net.CloseConnection(pDestObj->m_socket);
					pDestObj->m_socket=NULL;
				}
				else
				if(
						(nNumSockets==0) // 0 = timed out, -1 = error
					||(!(FD_ISSET(pDestObj->m_socket, &fds)))
					) 
				{ 
					// no data, no problem.
//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "no data on sock %d [%d]", pDestObj->m_socket, clock()); 

					// no data, nothing to receive, so let's just check the time.
					// if need be, send a keep alive.
					_timeb timestamp;
					_ftime(&timestamp);

					if(timestamp.time > pDestObj->m_timebLastPing.time + 50) // spec is 60 seconds, then disconnect.
					{

						switch(pDestObj->m_usType)
						{
						case CX_DESTTYPE_EVERTZ_7847FSE:  // need to init conn and kick off the timer, which can be entirely contained in this thread.
							{
								// assemble the init message buffer.
								char tnbuffer[1024]; // more than enough for all connection types known so far
								int nBuflen;
								sprintf(tnbuffer, "");

								CSCTE104Util* p104 = (CSCTE104Util*)pDestObj->m_pvExternal; // for convenience.

								CSCTE104SingleMessage* sm = p104->CreateSingleMessageObject();
								if(sm)
								{
									sm->OpId1High =0x00;
									sm->OpId1Low =0x03;  // keep alive!
									sm->MessageNumber = p104->IncrementSecondaryMessageNumber();
									unsigned char* pucBuf = p104->ReturnMessageBuffer(sm);
									if(pucBuf)
									{
										unsigned long ulLen = *(pucBuf+2);
										ulLen <<= 8; ulLen += *(pucBuf+3);
										memcpy(tnbuffer, pucBuf,  min(ulLen, 1024)); // 1024 should be more than enough, but use min just for safety
										nBuflen = ulLen;
										delete [] pucBuf;
									}
									delete sm;


	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO)) 
	{
//unsigned long ulSend = buflen;
//char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_DELIM32BIT);
//AfxMessageBox(pchSnd);

		unsigned long ulSend = nBuflen;
		char* pchSnd = bu.ReadableHex((char*)tnbuffer, &ulSend, MODE_FULLHEX);

		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "keep_alive", "sending: %s", pchSnd?pchSnd:"(null)"); // Sleep(250); //(Dispatch message)

		if(pchSnd) free(pchSnd);
	}

		EnterCriticalSection(&pDestObj->m_critComm);
			pDestObj->m_bSending=true;

			int nHostReturn = pDestObj->SendTelnetCommand(tnbuffer, nBuflen, EOLN_NONE); 
			if((nHostReturn>LIBRETTO_ERROR)&&(pDestObj->m_socket!=NULL))
			{
				unsigned char* pucBuffer = NULL;
				unsigned long ulBufferLen = 0;

	//			struct timeval tv;
				tv.tv_sec = 5; tv.tv_usec = 0;  // timeout value, 0.5 second for keep alives
				fd_set fds;
		//		int nNumSockets;
				FD_ZERO(&fds);
				FD_SET(pDestObj->m_socket, &fds);
				nNumSockets = select(0, &fds, NULL, NULL, &tv);

				if ( nNumSockets == INVALID_SOCKET )
				{
					int nErrorCode = WSAGetLastError();
					char* pchError = pDestObj->m_net.WinsockEnglish(nErrorCode);
					if(pchError)
					{
						if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
							g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Invalid socket on connection to %s:%d\n%s", 
								pDestObj->m_pszServerName, pDestObj->m_usPort, pchError?pchError:""); // Sleep(250); //(Dispatch message)

						g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Init connection", "Invalid socket on connection to %s:%d\n%s", pDestObj->m_pszServerName, pDestObj->m_usPort, pchError);
						LocalFree(pchError);
					}
					pDestObj->m_net.CloseConnection(pDestObj->m_socket);
					pDestObj->m_socket=NULL;
					pDestObj->m_bSending=false;

				}
				else
				if(
						(nNumSockets==0) // 0 = timed out, -1 = error
					||(!(FD_ISSET(pDestObj->m_socket, &fds)))
					) 
				{ 
					// no data, no problem, just return for now.
					pDestObj->m_bSending=false;
//		if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "keep_alive", "No data response on connection to %s:%d", pDestObj->m_pszServerName, pDestObj->m_usPort); // Sleep(250); //(Dispatch message)
				} 
				else // there is recv data.
				{
					nHostReturn = pDestObj->m_net.GetLine(&pucBuffer, &ulBufferLen, pDestObj->m_socket);
					pDestObj->m_bSending=false;
					if((nHostReturn<=LIBRETTO_ERROR)||(pDestObj->m_socket==NULL))
					{
						_ftime(&pDestObj->m_timebLastDisconnect);

						int nErrorCode = WSAGetLastError();
						char* pchError = pDestObj->m_net.WinsockEnglish(nErrorCode);
						if(pchError)
						{
							if((ulBufferLen<=0)||(pucBuffer==NULL))
							{
								if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
									g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "keep_alive", "Connection to %s:%d terminated\n%s", 
										pDestObj->m_pszServerName, pDestObj->m_usPort, pchError?pchError:""); // Sleep(250); //(Dispatch message)

								g_plibretto->SendMsg(CX_SENDMSG_ERROR, "keep_alive", "Connection to %s:%d terminated\n%s", pDestObj->m_pszServerName, pDestObj->m_usPort, pchError);
							}
							else
							{
								if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
									g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Init connection", "Communication error on %s:%d\n%s", 
										pDestObj->m_pszServerName, pDestObj->m_usPort, pchError); // Sleep(250); //(Dispatch message)

								g_plibretto->SendMsg(CX_SENDMSG_ERROR, "keep_alive", "Communication error on %s:%d\n%s", pDestObj->m_pszServerName, pDestObj->m_usPort, pchError);
								if(pucBuffer) free(pucBuffer);
							}
							LocalFree(pchError);
						}
						pDestObj->m_net.CloseConnection(pDestObj->m_socket);
						pDestObj->m_socket=NULL;
						pDestObj->m_bSending=false;
						
//		if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "keep_alive", "Error response on connection to %s:%d", pDestObj->m_pszServerName, pDestObj->m_usPort); // Sleep(250); //(Dispatch message)
					}
					else
					{
	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO)) 
	{
//unsigned long ulSend = buflen;
//char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_DELIM32BIT);
//AfxMessageBox(pchSnd);

		unsigned long ulSend = ulBufferLen;
		char* pchSnd = bu.ReadableHex((char*)pucBuffer, &ulSend, MODE_FULLHEX);

		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "keep_alive", "received: %s", pchSnd?pchSnd:"(null)"); // Sleep(250); //(Dispatch message)

		if(pchSnd) free(pchSnd);
	}
							_ftime(&pDestObj->m_timebLastPing);

					}
					if(pucBuffer) free(pucBuffer);
				}
			}
			else
			{
				pDestObj->m_bSending=false;

//		if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
		g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "keep_alive", "Error %d on connection to %s:%d, socket %d", pDestObj->m_pszServerName, pDestObj->m_usPort, nHostReturn, pDestObj->m_socket); // Sleep(250); //(Dispatch message)
				pDestObj->m_net.CloseConnection(pDestObj->m_socket);
				pDestObj->m_socket=NULL;
			}
		LeaveCriticalSection(&pDestObj->m_critComm);





								}
								else
								{
									//log the error
								}

							}	break;
						default:
							{
							}	break;
						}



					}


				} 
				else // there is recv data.
				{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
					//But, let there be some exclusion so we don't accidentally recv bytes meant for actual replies, somehow
					char buffer[4];
					if(!pDestObj->m_bSending)
					{
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "recv on sock %d [%d]", pDestObj->m_socket, clock()); 
						int nSize = recv(pDestObj->m_socket, buffer, 1, 0);
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "recv returned %d on sock %d [%d]", nSize, pDestObj->m_socket, clock()); 
						if((nSize==0)||(nSize==SOCKET_ERROR)) // disconnection.  otherwise, we messed up.
						{
							_ftime(&pDestObj->m_timebLastDisconnect);
							int nErrorCode = WSAGetLastError();
							char* pchError = pDestObj->m_net.WinsockEnglish(nErrorCode);
							if(pchError)
							{
								if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
									g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Connection to %s:%d terminated\n%s", 
										pDestObj->m_pszServerName, pDestObj->m_usPort, pchError?pchError:""); // Sleep(250); //(Dispatch message)
							
				//				g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:chicken", "chicken returns %d", // Sleep(250); //(Dispatch message)

								g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Connection monitor", "Connection to %s:%d terminated\n%s", pDestObj->m_pszServerName, pDestObj->m_usPort, pchError);
								LocalFree(pchError);
							}
							else
							{
								if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_DEVCOMM)) 
									g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Connection to %s:%d terminated", 
										pDestObj->m_pszServerName, pDestObj->m_usPort); // Sleep(250); //(Dispatch message)

								g_plibretto->SendMsg(CX_SENDMSG_ERROR, "Connection monitor", "Connection to %s:%d terminated", pDestObj->m_pszServerName, pDestObj->m_usPort);
							}
							pDestObj->m_net.CloseConnection(pDestObj->m_socket);
							pDestObj->m_socket=NULL;

// nah, let the next command just do it
							// ah ok, let it happen.
							// immediately re-try the connection
							if(!pDestObj->m_bConnThreadStarted)
							{
								if(_beginthread(LibrettoAsynchConnThread, 0, (void*)(pDestObj))==-1)
								{
								//error.
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Error starting connection thread for server %s", pDestObj->m_pszServerName);//   Sleep(250);//(Dispatch message)
								//**MSG
								}
							}

						}
					}
				}
			}
			nLastCheck = clock()+1000;  // changed from 1.5 sec to 1 sec.

			while((!g_bKillThread)&&(pDestObj)&&(!pDestObj->m_bKillMonThread)&&(clock()<nLastCheck))
			{
				Sleep(1);
			}

		}
		else
		{// socket is null, dont monitor
			if((!g_bKillThread)&&(pDestObj)&&(!pDestObj->m_bKillMonThread)&&(pDestObj->m_socket==NULL)&&(!pDestObj->m_bSending)&&(!pDestObj->m_bConnThreadStarted))
			{
				if(_beginthread(LibrettoAsynchConnThread, 0, (void*)(pDestObj))==-1)
				{
				//error.
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Error starting connection thread for server %s", pDestObj->m_pszServerName);//   Sleep(250);//(Dispatch message)
				//**MSG
				}


				nLastCheck = clock()+1500;
				while((!g_bKillThread)&&(pDestObj)&&(!pDestObj->m_bKillMonThread)&&(clock()<nLastCheck))
				{
					Sleep(1);
				}

			}
		}


		Sleep(1);
	}
	pDestObj->m_bMonThreadStarted = false;
	_endthread();

}


void LibrettoCommandQueueThread(void* pvArgs)
{
	CLibrettoDestinationObject* pDestObj = (CLibrettoDestinationObject*) pvArgs;
	if(pDestObj==NULL) return;
	if(pDestObj->m_bCommandQueueThreadStarted) return; // only one at a time!
	pDestObj->m_bCommandQueueThreadStarted = true;

	g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "LibrettoCommandQueueThread", "Beginning command queue thread for %s", pDestObj->m_pszServerName?pDestObj->m_pszServerName:"(null)");    //(Dispatch message)

	char szSQL[DB_SQLSTRING_MAXLEN];
	char errorstring[MAX_MESSAGE_LENGTH];
	char dberrorstring[MAX_MESSAGE_LENGTH];
	strcpy(errorstring, "");
	strcpy(dberrorstring, "");
	CDBUtil db;
	CBufferUtil bu;

	_timeb timebCommandQueue;
	_ftime( &timebCommandQueue );
	_timeb timebCheck;
	_ftime( &timebCheck );

	EnterCriticalSection(&pDestObj->m_critMemQueue);
//	pDestObj->ClearMemQueue();

	pDestObj->m_ppMemQueue = new CLibrettoQueueObject*[g_plibretto->m_settings.m_nMemQueueIncrSize];
	if(pDestObj->m_ppMemQueue) pDestObj->m_nNumMemQueueArray = g_plibretto->m_settings.m_nMemQueueIncrSize;
	LeaveCriticalSection(&pDestObj->m_critMemQueue);

//	int m_nPtrMemQueueIn;
//	int m_nPtrMemQueueOut;


	CDBconn* pdbConn = NULL;

	if (g_plibretto->m_settings.m_bEnableCommandQueue) // must be enabled
	{
		pdbConn = db.CreateNewConnection(g_plibretto->m_settings.m_pszDSN, g_plibretto->m_settings.m_pszUser, g_plibretto->m_settings.m_pszPW);
		if(pdbConn)
		{
			if(db.ConnectDatabase(pdbConn, errorstring)<DB_SUCCESS)
			{
				g_plibretto->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Libretto:command_queue_database_connect", errorstring);  //(Dispatch message)
				pdbConn = g_plibretto->m_data.m_pdbConn;
			}
		}
		else
		{
			_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Failed to create connection for %s:%s:%s", g_plibretto->m_settings.m_pszDSN, g_plibretto->m_settings.m_pszUser, g_plibretto->m_settings.m_pszPW); 
			g_plibretto->m_msgr.DM(MSG_ICONERROR|MSG_PRI_HIGH, NULL, "Libretto:command_queue_database_init", errorstring);  //(Dispatch message)
			pdbConn = g_plibretto->m_data.m_pdbConn;

			//**MSG
		}
	}

	if((pdbConn)&&(pdbConn->m_bConnected)&&(pDestObj->m_pszServerName))
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE host = '%s'", //HARDCODE
			((g_plibretto->m_settings.m_pszCommandQueue)&&(strlen(g_plibretto->m_settings.m_pszCommandQueue)))?g_plibretto->m_settings.m_pszCommandQueue:"Command_Queue",
			pDestObj->m_pszServerName);


		if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//**MSG
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

		}
	}

	int nLastQueueMode = LIBRETTO_QUEUEMODE_MEM;


	bool bLastProcessSuspended = 	g_plibretto->m_data.m_bProcessSuspended;

	while((!g_bKillThread)&&(!pDestObj->m_bKillCommandQueueThread))
	{
//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "%d %d %d %d %d.%03d", g_plibretto->m_data.m_nIndexAutomationEndpoint, g_plibretto->m_settings.m_nNumEndpointsInstalled, g_plibretto->m_settings.m_ppEndpointObject, g_plibretto->m_settings.m_ppEndpointObject[g_plibretto->m_data.m_nIndexAutomationEndpoint], g_plibretto->m_data.m_timebTick.time, g_plibretto->m_data.m_timebTick.millitm);   Sleep(50);//(Dispatch message)



		if(
			  (!g_plibretto->m_data.m_bProcessSuspended)
			&&(g_plibretto->m_settings.m_bEnableCommandQueue) // must be enabled
			&&(g_plibretto->m_data.m_key.m_bValid)  // must have a valid license
			&&(
				  (!g_plibretto->m_data.m_key.m_bExpires)
				||((g_plibretto->m_data.m_key.m_bExpires)&&(!g_plibretto->m_data.m_key.m_bExpired))
				||((g_plibretto->m_data.m_key.m_bExpires)&&(g_plibretto->m_data.m_key.m_bExpireForgiveness)&&(g_plibretto->m_data.m_key.m_ulExpiryDate+g_plibretto->m_data.m_key.m_ulExpiryForgiveness>(unsigned long)time(NULL)))
				)
			&&(
				  (!g_plibretto->m_data.m_key.m_bMachineSpecific)
				||((g_plibretto->m_data.m_key.m_bMachineSpecific)&&(g_plibretto->m_data.m_key.m_bValidMAC))
				)

			)
		{

			if(bLastProcessSuspended != g_plibretto->m_data.m_bProcessSuspended)
			{

				bLastProcessSuspended = g_plibretto->m_data.m_bProcessSuspended;
				if(!bLastProcessSuspended)
				{
					// we've resumed so clear the command queue table.
					if((pdbConn)&&(pdbConn->m_bConnected))
					{
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE host = '%s'", //HARDCODE
							((g_plibretto->m_settings.m_pszCommandQueue)&&(strlen(g_plibretto->m_settings.m_pszCommandQueue)))?g_plibretto->m_settings.m_pszCommandQueue:"Command_Queue",
							pDestObj->m_pszServerName);


						if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
						{
							//**MSG
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring);  Sleep(250); //(Dispatch message)

						}
					}

					EnterCriticalSection(&pDestObj->m_critMemQueue);
					if(pDestObj->m_nPtrMemQueueOut >= 0 )
					{
//						pDestObj->ClearMemQueue();
					}
					LeaveCriticalSection(&pDestObj->m_critMemQueue);

				}
			}


			_ftime(&pDestObj->m_timebCommandQueueTick); // the last time check inside the thread


			if(pdbConn)
			{

				_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "SELECT TOP 1 * FROM %s WHERE host = '%s' AND action <> 128 ORDER BY timestamp ASC", //HARDCODE
					((g_plibretto->m_settings.m_pszCommandQueue)&&(strlen(g_plibretto->m_settings.m_pszCommandQueue)))?g_plibretto->m_settings.m_pszCommandQueue:"Command_Queue",
					pDestObj->m_pszServerName);
			}
		// need to change to filename_local varchar(512), filename_remote varchar(512) to manage hex encoded payloads
	//create table Queue (itemid int identity(1,1) NOT NULL, filename_local varchar(256), filename_remote varchar(256), 
	//action int, host varchar(64), timestamp float, username varchar(32));

			CString szLocal = "";
			CString szRemote = "";
//			CString szServer = "";
			CString szUsername = "";
			CString szMessage = "";
			CString szTemp = "";
			int nItemID = -1;
			bool bItemFound = false;
			double dblTimestamp=-57;   // timestamp
			int nAction = -1;
			int nTemp;
			int nEventItemID=-1;

			CRecordset* prs = NULL;
			CLibrettoQueueObject* plqo = NULL;
			int nMemQueueIndex=-1;
			if(pdbConn)
			{
				if(nLastQueueMode != LIBRETTO_QUEUEMODE_DB)
				{
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "Command queue SQL: %s", szSQL); // Sleep(250); //(Dispatch message)

					prs = db.Retrieve(pdbConn, szSQL, errorstring);
				}
			}
			_ftime( &timebCommandQueue );

			if(prs)
			{
				nLastQueueMode = LIBRETTO_QUEUEMODE_DB;
			}
			else
			{
				nLastQueueMode = LIBRETTO_QUEUEMODE_MEM;

				//chk memq
				EnterCriticalSection(&pDestObj->m_critMemQueue);
//				nMemQueueIndex = pDestObj->GetMemQueue();
//				if(nMemQueueIndex>=0)
				{
					plqo = NULL;//pDestObj->m_ppMemQueue[nMemQueueIndex];
//					pDestObj->RemoveMemQueue(nMemQueueIndex);
				}
				LeaveCriticalSection(&pDestObj->m_critMemQueue);

			}

			if((prs)||(plqo))
			{
				strcpy(errorstring, "");
				int nReturn = LIBRETTO_ERROR;
				int nIndex = 0;
	//			while ((!prs->IsEOF()))
				if(prs)
				{
				if ((!prs->IsEOF()))  // just do the one record, if there is one
				{
					try
					{
						prs->GetFieldValue("itemid", szTemp);//HARDCODE
						if(szTemp.GetLength())
						{
							nItemID = atoi(szTemp);
							bItemFound = true;
						}
						prs->GetFieldValue("local", szLocal);//HARDCODE
						szLocal.TrimLeft(); szLocal.TrimRight();
						prs->GetFieldValue("remote", szRemote);//HARDCODE
						szRemote.TrimLeft(); szRemote.TrimRight();
						prs->GetFieldValue("action", szTemp);//HARDCODE
						if(szTemp.GetLength())
						{
							nTemp = atoi(szTemp);
							if(nTemp>=0) nAction = nTemp;
						}
// no need, as this is a per-host thread.					
//						prs->GetFieldValue("host", szServer);//HARDCODE
//						szServer.TrimLeft(); szServer.TrimRight();

						prs->GetFieldValue("timestamp", szTemp);//HARDCODE
						if(szTemp.GetLength())
						{
							dblTimestamp = atof(szTemp);
						}
						prs->GetFieldValue("username", szUsername);//HARDCODE
						szUsername.TrimLeft(); szUsername.TrimRight();
						prs->GetFieldValue("event_itemid", szTemp);//HARDCODE
						if(szTemp.GetLength())
						{
							nTemp = atoi(szTemp);
							if(nTemp>0) nEventItemID = nTemp;
						}

						prs->GetFieldValue("message", szMessage);//HARDCODE
						
					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Retrieve: Caught exception: %s\n%s", ((CDBException *) e)->m_strError, szSQL);
g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:command_queue", "%s", errorstring);  // Sleep(250);//(Dispatch message)
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							e->GetErrorMessage(dberrorstring, MAX_MESSAGE_LENGTH);
							// The error code is in e->m_nRetCode
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Retrieve: Caught memory exception: %s\n%s", dberrorstring, szSQL);
g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:command_queue", "%s", errorstring);  // Sleep(250);//(Dispatch message)
						}
						else 
						{
							e->GetErrorMessage(dberrorstring, MAX_MESSAGE_LENGTH);
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Retrieve: Caught exception: %s\n%s", dberrorstring, szSQL);
g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:command_queue", "%s", errorstring);  // Sleep(250);//(Dispatch message)
						}
						e->Delete();
					} 
					catch( ... )
					{
g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:command_queue", "Retrieve: Caught exception.\n%s", szSQL); // Sleep(250); //(Dispatch message)
					}

					nIndex++;
					prs->MoveNext();
				}

				nReturn = nIndex;
				prs->Close();

				delete prs;

	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "got %d things from queue", nIndex);  // Sleep(250);//(Dispatch message)
				prs = NULL;

				}
				else // must be plqo
				{
					try
					{
						nItemID = plqo->nItemid;
						szLocal = plqo->szLocal;
						szLocal.TrimLeft(); szLocal.TrimRight();
						szRemote=plqo->szRemote;
						szRemote.TrimLeft(); szRemote.TrimRight();
						nAction = plqo->nAction;

// no need, as this is a per-host thread.					
//						prs->GetFieldValue("host", szServer);//HARDCODE
//						szServer.TrimLeft(); szServer.TrimRight();

						dblTimestamp = plqo->dblTimestamp;

						szUsername = plqo->szUsername;
						szUsername.TrimLeft(); szUsername.TrimRight();
						nEventItemID =  plqo->nEventItemID;
						
						szMessage = plqo->szMessage;
						bItemFound = true;

					}
					catch(CException *e)// CDBException *e, CMemoryException *m)  
					{
						if (e->IsKindOf( RUNTIME_CLASS( CDBException )  ))
						{
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "GetMemQueue: Caught exception: %s", ((CDBException *) e)->m_strError);
g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:command_queue", "%s", errorstring);  // Sleep(250);//(Dispatch message)
						}
						else if (e->IsKindOf( RUNTIME_CLASS( CMemoryException )  ))
						{
							e->GetErrorMessage(dberrorstring, MAX_MESSAGE_LENGTH);
							// The error code is in e->m_nRetCode
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "GetMemQueue: Caught memory exception: %s", dberrorstring);
g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:command_queue", "%s", errorstring);  // Sleep(250);//(Dispatch message)
						}
						else 
						{
							e->GetErrorMessage(dberrorstring, MAX_MESSAGE_LENGTH);
							_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "GetMemQueue: Caught exception: %s", dberrorstring);
g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:command_queue", "%s", errorstring);  // Sleep(250);//(Dispatch message)
						}
						e->Delete();
					} 
					catch( ... )
					{
g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:command_queue", "GetMemQueue: Caught exception."); // Sleep(250); //(Dispatch message)
					}

					nIndex++;
					nReturn = nIndex;

					
				}

				if((nReturn>0)&&(bItemFound)&&(!pDestObj->m_bKillCommandQueueThread))
				{
					// process the request, then remove it from the queue
	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "got item %d from queue", nItemID);   Sleep(250);//(Dispatch message)
					_ftime( &pDestObj->m_timebCommandQueueTick );

					if(pDestObj->m_ulFlags&LIBRETTO_FLAG_ENABLED)
					{  
						// only do anything if its enabled.
	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "enabled");   Sleep(250);//(Dispatch message)

					//	int nHostReturn = LIBRETTO_ERROR;
						if((pDestObj->m_socket == NULL)&&(!pDestObj->m_bConnThreadStarted)&&(!pDestObj->m_bKillCommandQueueThread))
						{
							pDestObj->m_ulStatus |= LIBRETTO_STATUS_CONN;
							if(_beginthread(LibrettoAsynchConnThread, 0, (void*)pDestObj)==-1)
							{
							//error.

							//**MSG
							}
							else
							{
								int nWait = 0;
								while((pDestObj->m_ulStatus&LIBRETTO_STATUS_CONN)&&(nWait<500))  // 0.5 seconds?
								{
									nWait++;
									Sleep(1);
								}
							}

	/*
								nHostReturn = pDestObj->m_net.OpenConnection(pchServer, g_plibretto->m_settings.m_nPort, &pDestObj->m_socket); // re-establish
								char tnbuffer[32]; sprintf(tnbuffer, "\\\\%c%c", 13,10);
								pDestObj->SendTelnetCommand(tnbuffer, 2);
								unsigned char* pucBuffer = NULL;
								unsigned long ulBufferLen = 256;
								pDestObj->m_net.GetLine(&pucBuffer, &ulBufferLen, pDestObj->m_socket);
								if(pucBuffer) free(pucBuffer);
	*/
						}

						_ftime( &pDestObj->m_timebCommandQueueTick );
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDQUEUE)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "conn check server %s enabled, socket:%d, thread:%d", pDestObj->m_pszServerName, pDestObj->m_socket, pDestObj->m_bConnThreadStarted);//   Sleep(250);//(Dispatch message)
						if(pDestObj->m_socket != NULL)
						{
	//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "switching with %d", nAction);   Sleep(50);//(Dispatch message)
							switch(nAction)
							{
							case 256: // telnet command
								{
//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "*** checking %s on %s", pchFileRemote, pchServer);  Sleep(250); //(Dispatch message)

									unsigned long ulLen = szLocal.GetLength();
									char* buffer = szLocal.GetBuffer(0);
									unsigned short usCommand=0x0000;
									char chEolnType = EOLN_CRLF;// chyron type default
									

									if(pDestObj->m_usType == CX_DESTTYPE_EVERTZ_7847FSE)
									{
										chEolnType = EOLN_NONE; // do not send CRLF
										// convert the buffer.  use the destination specific  buffer util obj
										pDestObj->m_bu.Base64Decode(&buffer, &ulLen, false); // true frees the incoming buffer.  buffer and length are updated
										// replace time code and message number
//											nCommand = 0x0000ffff&((((CSCTE104SingleMessage*)buffer)->OpId1High<<8)|(((CSCTE104SingleMessage*)buffer)->OpId1Low));
										usCommand = buffer[0] & 0x00ff;
										usCommand <<= 8; usCommand += (buffer[1] & 0x00ff);
									}

//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "*** usCommand calc = %04x, buf0=%02x  buf1=%02x", usCommand, buffer[0],buffer[1]);  //Sleep(250); //(Dispatch message)

										
	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "start send now"); // Sleep(250); //(Dispatch message)

// added repititions
	int nRepeats=0;
	do
	{

//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "%d %04x", pDestObj->m_usType, usCommand); // Sleep(250); //(Dispatch message)

									if(pDestObj->m_usType == CX_DESTTYPE_EVERTZ_7847FSE)
									{
										CSCTE104Util* p104 = (CSCTE104Util*)pDestObj->m_pvExternal;
										if(usCommand == 0xffff) // multi message
										{
											buffer[6] = (nRepeats==0)?p104->IncrementMessageNumber():p104->m_ucLastMessageNumber;

							//				CString Q;
							//				Q.Format("MULTI %04x",  nCommand);
							//				AfxMessageBox(Q);


											if(buffer[10] == 0x02) // only if time type 2, replace time code
											{
												// multi obj
												SCTE104HMSF_t HMSF;

	//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
	//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "*x* TC %d %d", (g_plibretto->m_settings.m_bUseTimeCode), (g_plibretto->m_data.m_bTimeCodeThreadStarted)); // Sleep(250); //(Dispatch message)

												// get timecode if installed.
												bool bSysClock = false;
												if((g_plibretto->m_settings.m_bUseTimeCode)&&(g_plibretto->m_data.m_bTimeCodeThreadStarted))
												{

	EnterCriticalSection(&g_plibretto->m_data.m_critTimeCode);
													unsigned long ulTimeCode = g_plibretto->m_data.m_ulTimeCode;
													unsigned long ulTimeCodeElapsed = g_plibretto->m_data.m_ulTimeCodeElapsed;
	LeaveCriticalSection(&g_plibretto->m_data.m_critTimeCode);
	//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
	//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "*-* TC %08x", ulTimeCode); // Sleep(250); //(Dispatch message)
													if(ulTimeCodeElapsed>(unsigned long)g_plibretto->m_settings.m_nSyncThresholdMS) // worse than the threshold. could just use the sys clock, which is close....
													{
														// should do something if elapsed, but not going to... will chalk it up to disconnected TC card.
														bSysClock=true;
	//if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
	//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "*** TC elapsed %d %d", ulTimeCodeElapsed, g_plibretto->m_settings.m_nSyncThresholdMS); // Sleep(250); //(Dispatch message)
													}
													else
													{
														if(ulTimeCode != 0xffffffff)
														{
															unsigned char ucTempTC = (unsigned char)((ulTimeCode&0xff000000)>>24); 
															HMSF.hours = (unsigned char)((ucTempTC/16)*10	+ (ucTempTC%16));
															
															ucTempTC = (unsigned char)((ulTimeCode&0x00ff0000)>>16); 
															HMSF.minutes = (unsigned char)((ucTempTC/16)*10	+ (ucTempTC%16));

															ucTempTC = (unsigned char)((ulTimeCode&0x0000ff00)>>8); 
															HMSF.seconds = (unsigned char)((ucTempTC/16)*10	+ (ucTempTC%16));
							
															ucTempTC = (unsigned char)(ulTimeCode&0x000000ff);
															HMSF.frames = (unsigned char)((ucTempTC/16)*10	+ (ucTempTC%16));

	//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
	//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "*** %02d%02d%02d%02d", HMSF.hours,HMSF.minutes,HMSF.seconds,HMSF.frames); // Sleep(250); //(Dispatch message)
														}	else bSysClock=true;
													}
												} else bSysClock=true;

												if(bSysClock)
												{
													_timeb timestamp;
													_ftime(&timestamp);

													if(g_plibretto->m_settings.m_bDF)
													{
														// fill the data with drop frame
														int rem = (timestamp.time- (timestamp.timezone*60)+(timestamp.dstflag?3600:0));
														while(rem<0) rem+=86400;
														while(rem>86399) rem-=86400;
														int ms = (rem%86400)*1000 + timestamp.millitm;
	/*
														double ms = (rem%86400)*1000 + timestamp.millitm;
														int fr = (int)(((ms * 30.0) / 1001.0) + 0.5);
														int deci = (int)(fr / 17982);
														rem = fr % 17982;
														if (rem >= 1800) rem += 2 + ((rem - 1800) / 1798) * 2; 

														HMSF.hours = deci / 6; 
														HMSF.minutes = ((deci % 6) * 10) + (rem / 1800); 
														HMSF.seconds = (rem % 1800) / 30; 
														HMSF.frames = rem % 30;
	*/
														int nHours;
														int nMinutes;
														int nSeconds;
														int nFrames;

														pDestObj->m_timeconv.MStoHMSF(ms, &nHours, &nMinutes, &nSeconds, &nFrames); // default to ntsc df
														HMSF.hours		= (unsigned char)(nHours&0x000000ff);
														HMSF.minutes	= (unsigned char)(nMinutes&0x000000ff);
														HMSF.seconds	= (unsigned char)(nSeconds&0x000000ff);
														HMSF.frames		= (unsigned char)(nFrames&0x000000ff);
													}
													else
													{
														timestamp.time %= 86400; //remains of the day!
														HMSF.hours   = (unsigned char)(((timestamp.time/3600)%24)&0x000000ff);
														HMSF.minutes = (unsigned char)(((timestamp.time/60)%60)&0x000000ff);
														HMSF.seconds = (unsigned char)(((timestamp.time)%60)&0x000000ff);
														HMSF.frames  = (unsigned char)((timestamp.millitm/g_plibretto->m_settings.m_nFrameRate)&0x000000ff);
													}
	//	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
	//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "*** sys %02x%02x%02x%02x", HMSF.hours,HMSF.minutes,HMSF.seconds,HMSF.frames); // Sleep(250); //(Dispatch message)
												}

												buffer[10] = 0x02;
												buffer[11] = HMSF.hours;
												buffer[12] = HMSF.minutes;
												buffer[13] = HMSF.seconds;
												buffer[14] = HMSF.frames;
											}  // if time type == 0x02 only
										}
										else
										{
											if(nRepeats==0)
											{
												buffer[10] = p104->IncrementSecondaryMessageNumber();
											}
											else
											{
											
												int rv = p104->m_ucLastMessageNumber + p104->m_ucLastSecondaryMessageNumberInc + 128 - (p104->m_ucSecondaryMessageNumberofValues/2); // small loop averaging 180 degree phase on primary message number
												while(rv>255)	rv-=256; // loop around.
												buffer[10] = (char)rv;
											}


											// need to create a message number that starts at p104->m_ucLastMessageNumber + 128 and increments from there with a secondary incrementer
							//				CString Q;
							//				Q.Format("SINGLE %04x", nCommand);
							//				AfxMessageBox(Q);
							/*
											if( nCommand == 0x0001) // init message
											{
																	AfxMessageBox("init msg");
											}
											else
											if( nCommand == 0x0003) // keep_alive message
											{
																AfxMessageBox("keep_alive msg");
											}
								*/		
											// single obj
										}

									}

									if(pDestObj->m_usType == CX_DESTTYPE_EVERTZ_7847FSE)
									{

		unsigned long ulSend = ulLen;
		char* pchSnd = bu.ReadableHex((char*)buffer, &ulSend, MODE_FULLHEX);

if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO|LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "sending %d bytes (r=%d): %s", ulLen, nRepeats, pchSnd); // Sleep(250); //(Dispatch message)

		if(pchSnd) free(pchSnd);
									}
									else
									{
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO|LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "sending: %s", buffer); // Sleep(250); //(Dispatch message)
									}


	EnterCriticalSection(&pDestObj->m_critComm);
									pDestObj->m_bSending=true;
									int nReturn = pDestObj->SendTelnetCommand(buffer, ulLen, chEolnType);
									bool bLastCycle = (
																			(g_plibretto->m_settings.m_nCommandRepetitions<=0) 
																		||((g_plibretto->m_settings.m_nCommandRepetitions>0)&&(nRepeats>=g_plibretto->m_settings.m_nCommandRepetitions)) //i.e. last one before increment.
																		);

									if(bLastCycle)
									{
										szLocal.ReleaseBuffer();
									}


									if(nReturn<LIBRETTO_SUCCESS)
									{
											pDestObj->m_bSending=false;
	LeaveCriticalSection(&pDestObj->m_critComm);
										//**MSG
										g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:telnet", 
											"telnet command not successful on %s.", pDestObj->m_pszServerName );//   Sleep(50);//(Dispatch message)

										if((nEventItemID>0)&&(bLastCycle))
										{
											if(plqo)
											{
												plqo->szMessage.Format("E256:command not successful on %s.", pDestObj->m_pszServerName );
												plqo->nAction=128;
												plqo->dblTimestamp = 5000000000.0;

												EnterCriticalSection(&pDestObj->m_critMemQueue);
												nMemQueueIndex = pDestObj->AddMemQueue(plqo);
												LeaveCriticalSection(&pDestObj->m_critMemQueue);
												if(nMemQueueIndex<0)
												{
													try
													{
														delete plqo;
													}
													catch(...)
													{
													}
												}

											}
											else
											if(pdbConn)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E256:command not successful on %s.', action = 128, timestamp = 5000000000 WHERE itemid = %d;",  //HARDCODE
													((g_plibretto->m_settings.m_pszCommandQueue)&&(strlen(g_plibretto->m_settings.m_pszCommandQueue)))?g_plibretto->m_settings.m_pszCommandQueue:"Command_Queue",
													pDestObj->m_pszServerName, nItemID
													);

												if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "ERROR executing SQL: %s", errorstring);//  Sleep(250); //(Dispatch message)

												}
											}
											continue; //return LIBRETTO_ERROR;  // because we dont want to increment counter.  // there is no counter.
											// we also dont want to remove the item at the end of this function
										}
									}
									else
									{
										//while(!g_miranda.m_bTransferring) Sleep(1);  //not good, in case error

//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "%s on %s succeeded", szLocal, pchServer);  //Sleep(250); //(Dispatch message)
										// we have queued the xfer, lets update the thing with a status, then exit without removing
										char pchFinalBuffer[256];
										unsigned char* pucBuffer = NULL;
										unsigned long ulBufferLen = 256;
										nReturn = pDestObj->m_net.GetLine(&pucBuffer, &ulBufferLen, pDestObj->m_socket, NET_RCV_ONCE, errorstring);
										
											pDestObj->m_bSending=false;
	LeaveCriticalSection(&pDestObj->m_critComm);


										if(nReturn>=NET_SUCCESS) _ftime(&pDestObj->m_timebLastPing);

										bool bEventID = ( 
																			(nEventItemID>0)
																		&&(
																				(g_plibretto->m_settings.m_nCommandRepetitions<=0) 
																			||((g_plibretto->m_settings.m_nCommandRepetitions>0)&&(nRepeats>=g_plibretto->m_settings.m_nCommandRepetitions)) //i.e. last one before increment.
																			)
																		);

										
										if(
												(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO|LIBRETTO_DEBUG_DEVCOMM)) 
											||((nEventItemID>0)&&(bLastCycle))
											)
										{

											if((nReturn<NET_SUCCESS)||(ulBufferLen==0))
											{
												_snprintf(pchFinalBuffer, 256, "%s: (%d) %s", ((nReturn<NET_SUCCESS)?"error":"no buffer"), nReturn, errorstring);
											}
											else
											{

												if(pDestObj->m_usType == CX_DESTTYPE_EVERTZ_7847FSE)
												{

					unsigned long ulRecv = ulBufferLen;
					char* pchRcv = bu.ReadableHex((char*)pucBuffer, &ulRecv, MODE_FULLHEX);

													_snprintf(pchFinalBuffer, 256, "%s", (pchRcv?((char*)pchRcv):"(null)"));

			if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO|LIBRETTO_DEBUG_DEVCOMM)) 
			g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "received %d bytes (r=%d): %s", ulBufferLen, nRepeats, pchRcv); // Sleep(250); //(Dispatch message)

					if(pchRcv) free(pchRcv);
												}
												else
												{
													_snprintf(pchFinalBuffer, 256, "%s", (pucBuffer?((char*)pucBuffer):"(null)"));
	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDECHO|LIBRETTO_DEBUG_DEVCOMM)) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "received %s", pchFinalBuffer); // Sleep(250); //(Dispatch message)
												}
											}


										}
										if(pucBuffer) free(pucBuffer);


										if((nEventItemID>0)&&(bLastCycle))
										{
											if(plqo)
											{
												plqo->szMessage.Format("I256:sent command to %s.", pDestObj->m_pszServerName );
												plqo->nAction=128;
												plqo->szRemote = pchFinalBuffer;
												plqo->dblTimestamp = 5000000000.0;

												EnterCriticalSection(&pDestObj->m_critMemQueue);
												nMemQueueIndex = pDestObj->AddMemQueue(plqo);
												LeaveCriticalSection(&pDestObj->m_critMemQueue);
												if(nMemQueueIndex<0)
												{
													try
													{
														delete plqo;
													}
													catch(...)
													{
													}
												}

											}
											else
											if(pdbConn)
											{
												_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'I256:sent command to %s.', remote = '%s', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
													((g_plibretto->m_settings.m_pszCommandQueue)&&(strlen(g_plibretto->m_settings.m_pszCommandQueue)))?g_plibretto->m_settings.m_pszCommandQueue:"Command_Queue",
													pDestObj->m_pszServerName, pchFinalBuffer, nItemID
													);

						/*
						Queue action IDs
	1 transfer file to Lyric Box
	2 transfer file from Lyric Box
	3 delete file from Lyric Box
	4 refresh file listing of Lyric Box
	19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
	32  file is transferring, check for end of transfer
	64  check for existence of file
	128 give results
	256 send telnet command to Lyric Box
						*/
										
											
												if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		//										if(m_pdb->ExecuteSQL(m_pdbConn, szSQL, pszInfo)<DB_SUCCESS)
												{
													//**MSG
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

												}
	//moved following to before SQL
	//											pDestObj->m_bSending=false;
	//	LeaveCriticalSection(&pDestObj->m_critComm);
											}

											continue; //return LIBRETTO_ERROR; // we dont want to remove the item at the end of this function
										}
									}
// dont need this anymore..
//									pDestObj->m_bSending=false;
//	LeaveCriticalSection(&pDestObj->m_critComm);
										nRepeats++;
		} while((g_plibretto->m_settings.m_nCommandRepetitions>0)&&(nRepeats<=g_plibretto->m_settings.m_nCommandRepetitions)&&(pDestObj->m_socket)); 
		// had to add this while for repetitions.

	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_TIMING)) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_queue", "end send"); // Sleep(250); //(Dispatch message)


								} break;
							case 64:// check file on box
								{
								} break;
							case 128:// Give results (no action).
								{
									continue; //return LIBRETTO_ERROR; // we dont want to remove the item at the end of this function
									// the requestor process will remove this.
								} break;
							case 1:// transfer to box
								{
								} break;
							case 2:// transfer from box
								{
								} break;
							case 3:// delete from Box
								{
								} break;
							case 32:// wait on transfer
								{

								} break;
							case 19:// purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
								{
								}  // not break;  want to go on to refresh device listing.
							case 4:// refresh device listing
								{
								} break;
							}
							_ftime( &pDestObj->m_timebCommandQueueTick );
						}
						else
						{
							//**MSG
g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:command_queue", "Could not set host to %s", pDestObj->m_pszServerName );  // Sleep(50);//(Dispatch message)

/*
							m_bFailed = true;
							m_timebFailure;

							// lets fail this one out for the failure retry time.
							_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E%03d:%s is unavailable or inactive, retrying after %d seconds.', timestamp = %d WHERE itemid = %d",  //HARDCODE
									((g_plibretto->m_settings.m_pszCommandQueue)&&(strlen(g_plibretto->m_settings.m_pszCommandQueue)))?g_plibretto->m_settings.m_pszCommandQueue:"CommandQueue",
									nAction, pchServer,
									nItemID
									);
//										if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
							if(db.ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
							{
								//**MSG
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

							}

*/

//							szServer.ReleaseBuffer();
							continue; //return LIBRETTO_ERROR; // we dont want to remove the item at the end of this function

	//	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "getting channels again");   Sleep(250);//(Dispatch message)
						}
					}
					else
					{
						//**MSG

						switch(nAction)
						{
						default:
							{
								g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto:CommandQueue", "An event was queued for %s, but that destination is unavailable or inactive.  The event will be ignored and purged from the queue.", pDestObj->m_pszServerName ); //  Sleep(50);//(Dispatch message)
							} break;  
						case 2:// transfer from box
						case 1:// transfer to box
						case 3:// DELETE FROM box
						case 64:// check file on box
						case 256:// send command to box
							{
								g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto:CommandQueue", "An event was queued for %s, but that destination is unavailable or inactive.  The event will be ignored and purged from the queue.", pDestObj->m_pszServerName ); //  Sleep(50);//(Dispatch message)

								if(nEventItemID>0) // means, commanded from external, so give info
								{
									if(plqo)
									{
										plqo->szMessage.Format("E%03d:%s is unavailable or inactive.", nAction, pDestObj->m_pszServerName );
										plqo->nAction=128;
										plqo->dblTimestamp = 5000000000.0;

										EnterCriticalSection(&pDestObj->m_critMemQueue);
										nMemQueueIndex = pDestObj->AddMemQueue(plqo);
										LeaveCriticalSection(&pDestObj->m_critMemQueue);
										if(nMemQueueIndex<0)
										{
											try
											{
												delete plqo;
											}
											catch(...)
											{
											}
										}

									}
									else
									if(pdbConn)
									{
										_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "UPDATE %s SET message = 'E%03d:%s is unavailable or inactive.', action = 128, timestamp = 5000000000 WHERE itemid = %d",  //HARDCODE
												((g_plibretto->m_settings.m_pszCommandQueue)&&(strlen(g_plibretto->m_settings.m_pszCommandQueue)))?g_plibretto->m_settings.m_pszCommandQueue:"Command_Queue",
												nAction, pDestObj->m_pszServerName,
												nItemID
												);
										Sleep(1); // dont peg processor
	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "SQL: %s", szSQL); // Sleep(250); //(Dispatch message)
										if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
					//					if(db.ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
										{
											//**MSG
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

										}
									}
								//	szServer.ReleaseBuffer();
									continue; //return LIBRETTO_ERROR; // we dont want to remove the item at the end of this function
								}
							} break;
						}
					}


					if(plqo)
					{
//					remove the object
						try
						{
							delete plqo;
						}
						catch(...)
						{
						}
//						pDestObj->SetMemQueue(int nMemIndex, NULL);  // no need, pointer nulled out on get
					}
					else
					if(pdbConn)
					{
						// send remove SQL
						_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE itemid = %d",  //HARDCODE
							((g_plibretto->m_settings.m_pszCommandQueue)&&(strlen(g_plibretto->m_settings.m_pszCommandQueue)))?g_plibretto->m_settings.m_pszCommandQueue:"Command_Queue",
							nItemID
							);

		//g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "SQL: %s", szSQL);  Sleep(250); //(Dispatch message)
		/*
		CommandQueue action IDs
		1 transfer to Miranda
		2 transfer from Miranda
		3 delete from Miranda
		4 refresh device listing
		19 purge (removes all files that have non-registered filetypes, on the remote IP, then updates the device table)
		32 wait on transfer

		CommandQueue table:
create table Command_Queue (itemid int identity(1,1), local varchar(2048), remote varchar(256), 
action int, host varchar(64), timestamp float, username varchar(32), event_itemid int, message varchar(512));    
						*/
						Sleep(1); // dont peg processor
	if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_SQL)) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "SQL: %s", szSQL); // Sleep(250); //(Dispatch message)

						if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
	//					if(db.ExecuteSQL(pdbConn, szSQL, pszInfo)<DB_SUCCESS)
						{
							//**MSG
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

						}
					}
				}

				plqo = NULL;
			}  //if prs or plqo


			_ftime(&pDestObj->m_timebCommandQueueTick); // the last time check inside the thread

		} // if license etc


		if(g_plibretto->m_settings.m_bEnableCommandQueue)
		{
			if( (!g_bKillThread)&&(g_plibretto->m_settings.m_nCommandQueueIntervalMS>0)	)
			{
				_ftime( &timebCheck );

				while (
								(!g_bKillThread)
							&&(!pDestObj->m_bKillCommandQueueThread)
							)
				{
					_ftime( &timebCheck );
					if(
							(timebCheck.time > (timebCommandQueue.time+g_plibretto->m_settings.m_nCommandQueueIntervalMS/1000))
						||(
								(timebCheck.time==(timebCommandQueue.time+g_plibretto->m_settings.m_nCommandQueueIntervalMS/1000))
							&&(timebCheck.millitm>(timebCommandQueue.millitm+g_plibretto->m_settings.m_nCommandQueueIntervalMS%1000))
							)
						) break;

					Sleep(1);
				}

			}
			else Sleep(1); // dont peg processor
		}
		else
		{
			Sleep(100);
		}
	
//		Sleep(1); // dont peg processor
	} // 	while((!g_bKillThread)&&(!pDestObj->m_bKillCommandQueueThread))


	if((pdbConn)&&(pdbConn->m_bConnected)&&(pDestObj->m_pszServerName))
	{
		_snprintf(szSQL, DB_SQLSTRING_MAXLEN-1, "DELETE FROM %s WHERE host = '%s'", //HARDCODE
			((g_plibretto->m_settings.m_pszCommandQueue)&&(strlen(g_plibretto->m_settings.m_pszCommandQueue)))?g_plibretto->m_settings.m_pszCommandQueue:"Command_Queue",
			pDestObj->m_pszServerName);

		if(db.ExecuteSQL(pdbConn, szSQL, errorstring)<DB_SUCCESS)
		{
			//**MSG
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "ERROR executing SQL: %s", errorstring); // Sleep(250); //(Dispatch message)

		}
	}

	EnterCriticalSection(&pDestObj->m_critMemQueue);
//	pDestObj->ClearMemQueue();

	if(pDestObj->m_ppMemQueue) delete pDestObj->m_ppMemQueue;
	pDestObj->m_ppMemQueue = NULL;
	pDestObj->m_nNumMemQueueArray = 0;

	LeaveCriticalSection(&pDestObj->m_critMemQueue);


	pDestObj->m_bCommandQueueThreadStarted=false;

	g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "LibrettoCommandQueueThread", "Ending command queue thread for %s", pDestObj->m_pszServerName?pDestObj->m_pszServerName:"(null)");    //(Dispatch message)

	Sleep(50);//(Dispatch message)
	_endthread();
	Sleep(150);

}

void LibrettoAsynchThumbnailRetrieveThread(void* pvArgs)
{

	if(pvArgs == NULL) return;
	CLibrettoDestinationObject* pDestObj = ((LibrettoThreadArgs_t*)(pvArgs))->pDest;
	if(pDestObj == NULL) return;
	char* pch = (char*)(((LibrettoThreadArgs_t*)(pvArgs))->pvoid);
	if(pch == NULL) return;
	if(strlen(pch)<=0) return;  // can't do this with a blank ID

	if(pDestObj->m_ulFlags&LIBRETTO_FLAG_ENABLED)
	{
		switch(pDestObj->m_usType)
		{
		case CX_DESTTYPE_CHYRON_CHANNELBOX: // the only one we support currently...
			{

//	B\TH\SceneId\Type\\<CR><LF>
//  The response result will take the form:
//  *B\TH\SceneId\ByteCount\ByteSequence\\ <CR><LF>
// Type may be either ControlPanel or Scene

				if(
						(pDestObj->m_socket != NULL)
					&&(!pDestObj->m_bConnThreadStarted)
					)
				{
					char buffer[MAX_PATH];//should be enough.
					sprintf(buffer, "B\\TH\\%s\\Scene\\\\", pch);
if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDQUEUE|LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:command_handler", "sending %s", buffer); // Sleep(250); //(Dispatch message)

EnterCriticalSection(&pDestObj->m_critComm);
					pDestObj->m_bSending=true;
					int nReturn = pDestObj->SendTelnetCommand(buffer, strlen(buffer), EOLN_CRLF);

					if(nReturn<LIBRETTO_SUCCESS)
					{
							pDestObj->m_bSending=false;
LeaveCriticalSection(&pDestObj->m_critComm);
						//**MSG
						g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:telnet", 
							"telnet command not successful on %s.", pDestObj->m_pszServerName );//   Sleep(50);//(Dispatch message)
					}
					else
					{
						unsigned char* pucBuffer = NULL;
						unsigned long ulBufferLen = 0;
						char errorstring[NET_ERRORSTRING_LEN];
						nReturn = pDestObj->m_net.GetLine(&pucBuffer, &ulBufferLen, pDestObj->m_socket, NET_RCV_ONCE, errorstring);
						

if(g_plibretto->m_settings.m_ulDebug&(LIBRETTO_DEBUG_CMDHANDLER|LIBRETTO_DEBUG_DEVCOMM)) 
g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, "Libretto:debug", "received %s", pucBuffer); // Sleep(250); //(Dispatch message)


						if((nReturn<NET_SUCCESS)||(ulBufferLen==0)||(pucBuffer==NULL))
						{
						g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:telnet", 
							"Error getting thumbnail for %s on %s.", pch, pDestObj->m_pszServerName );//   Sleep(50);//(Dispatch message)
						}
						else
						{
							if(pucBuffer)
							{
						// chances are, we got the part that tells us how much data we are supposed to be getting.
						// let's make sure we have complete data, using the  length, rather than the EOLN_TOKEN method, just in case tehre is a \\CRLF sequence coincidentally in the jpg image byte sequence.
	//				AfxMessageBox((char*)puc);
	//				CString qqqqq; 
	/*
	if(m_bLogTransactions)
	{
				FILE* logfp = fopen(LOGFILENAME, "ab");
				if(logfp)
				{
					_timeb timestamp;
					_ftime(&timestamp);
					char logtmbuf[48]; // need 33;
					tm* theTime = localtime( &timestamp.time	);
					strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
					fprintf(logfp, "%s%03d Recd: ", logtmbuf, timestamp.millitm);
					fwrite((char*)puc, sizeof(char), ulBufferLen, logfp);
					fclose(logfp);
				}
	}
	*/							
	//The response result will take the form:
	//*B\TH\SceneId\ByteCount\ByteSequence\\ <CR><LF>
								char bufferme[128];
								sprintf(bufferme,"*B\\TH\\%s\\", pch);

								int nLen =strlen(bufferme);
								if(strnicmp((char*)pucBuffer, bufferme, nLen)==0)
								{
									//parse it!
									int nBytes = atoi((char*)(pucBuffer+nLen));
									if(nBytes>0)
									{

										sprintf(bufferme,"%d\\", nBytes);
										nLen += strlen(bufferme);

	//				qqqqq.Format("%d total bytes in response\r\n%d bytes expected for image",ulBufferLen, nBytes); AfxMessageBox(qqqqq);

										bool bMore=false;
										unsigned char* pucBuffer2 = NULL;
										unsigned long ulLen2=0;
										if(ulBufferLen<(unsigned int)(nBytes+nLen+4))  //+4 because of the ending CRLF
										{
											// have to receive more data.
											//check if more data:
											ulLen2 = (nBytes+nLen+4)-ulBufferLen;

											timeval tv;
											tv.tv_sec = 2; 
											tv.tv_usec = 500000;  // timeout value
											int nNumSockets;
											fd_set fds;
											FD_ZERO(&fds);  // Zero this out each time
											FD_SET(pDestObj->m_socket, &fds);
											nNumSockets = select(0, &fds, NULL, NULL, &tv);
											if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
											{
												
											}
											else
											if(
													(nNumSockets==0) // 0 = timed out, -1 = error
												||(!(FD_ISSET(pDestObj->m_socket, &fds)))
												) 
											{ 

											} 
											else // there is recv data.
											{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
												//recv the response.

												nReturn=pDestObj->m_net.GetLine(&pucBuffer2, &ulLen2, pDestObj->m_socket, NET_RCV_LENGTH, errorstring);
												if(nReturn<NET_SUCCESS)
												{
									g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:thumbnail", 
										"Error getting thumbnail for %s on %s.\r\nReturn code %d\r\n%s", pch, pDestObj->m_pszServerName, nReturn, errorstring );//   Sleep(50);//(Dispatch message)
												}
												else
												{
													if(ulLen2==0) //disconnected
													{
//														m_net.CloseConnection(m_s);
//														m_s = NULL;
//														AfxMessageBox("disconnected");

														// no need to do anything here, the connection monitoring thread will deal.
													}
													else
														bMore=true;
												}
											}
										//	if((bMore)&&(pucBuffer2))
										//	{// use the second set to parse

	//										}




										}

										CString resp = (char*)pucBuffer ;

										if(pucBuffer2)
										{
											resp += pucBuffer2;
										}

//										m_lcResponses.SetItemText(nCount, 1, resp);
//										m_lcResponses.EnsureVisible(nCount, FALSE);


										if(g_plibretto->m_settings.m_pszDownloadedThumbnailPath)
										{
											char path[MAX_PATH];
											strcpy(path,g_plibretto->m_settings.m_pszDownloadedThumbnailPath);
											while( (*(path + strlen(path)-1)) == '\\')
											{
												path[strlen(path)-1]=0; // null term
											}

											_mkdir(path);
//										if(m_bThumbCP)
//											sprintf(bufferme,"images\\%s-cp.jpg", m_szScene);
//										else
											sprintf(bufferme,"%s%s.jpg", g_plibretto->m_settings.m_pszDownloadedThumbnailPath, pch);
										}
										else
										{
											_mkdir("C:\\Inetpub\\wwwroot\\Cortex\\Libretto\\images\\thumbnails");
//										if(m_bThumbCP)
//											sprintf(bufferme,"images\\%s-cp.jpg", m_szScene);
//										else
											sprintf(bufferme,"C:\\Inetpub\\wwwroot\\Cortex\\Libretto\\images\\thumbnails\\%s.jpg", pch);
										}


										FILE* fp;
										fp = fopen(bufferme, "wb");
										if(fp)
										{
											fwrite((pucBuffer+nLen), sizeof(char), ulBufferLen-nLen, fp );

											if((bMore)&&(pucBuffer2)&&(ulLen2))
											{
/*
	if(m_bLogTransactions)
	{
				FILE* logfp = fopen(LOGFILENAME, "ab");
				if(logfp)
				{
					fwrite(pucBuffer2, sizeof(char), ulLen2, logfp);
					fclose(logfp);
				}
	}
*/
												fwrite(pucBuffer2, sizeof(char), ulLen2, fp );
											}

											fclose(fp);

//											HINSTANCE hi;
//											hi=ShellExecute((HWND) NULL, NULL, bufferme, NULL, NULL, SW_HIDE);

										}
										else
										{
											CString szAlert;
											szAlert.Format("Could not write file. response was:\r\n%s",(char*)pucBuffer);
											if(pucBuffer2)
											{
												szAlert+= pucBuffer2;
											}
									g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:thumbnail", 
										"Error getting thumbnail for %s on %s.\r\nCould not write file. response was:\r\n%s%s", pch, pDestObj->m_pszServerName, (char*)pucBuffer, (pucBuffer2?((char*)(pucBuffer2)):"") );//   Sleep(50);//(Dispatch message)
										}	
										
										if(pucBuffer2)
										{
											free(pucBuffer2);
										}



									}
									else
									{
									g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:thumbnail", 
										"Error getting thumbnail for %s on %s.\r\nByte sequence not recognized, response was:\r\n%s", pch, pDestObj->m_pszServerName, (char*)pucBuffer );//   Sleep(50);//(Dispatch message)

									}

								}
								else
								{
									g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:thumbnail", 
										"Error getting thumbnail for %s on %s.\r\nInvalid response:\r\n%s", pch, pDestObj->m_pszServerName, (char*)pucBuffer );//   Sleep(50);//(Dispatch message)
		
								}
/*
	if(m_bLogTransactions)
	{
				FILE* logfp = fopen(LOGFILENAME, "ab");
				if(logfp)
				{
					fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
					fclose(logfp);
				}
	}
*/
								free(pucBuffer);
							}
							else // pucBuffer was null
							{
								g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, "Libretto:thumbnail", 
									"Error getting thumbnail for %s on %s.  NULL buffer", pch, pDestObj->m_pszServerName );//   Sleep(50);//(Dispatch message)
							} // pucBuffer was null
						}//if((nReturn<NET_SUCCESS)||(ulBufferLen==0)||(pucBuffer==NULL)) was false

						pDestObj->m_bSending=false;
LeaveCriticalSection(&pDestObj->m_critComm);


					}//if(nReturn<LIBRETTO_SUCCESS) was false
				}//(pDest->m_socket != NULL)&&(!pDest->m_bConnThreadStarted)


			} break;
		default:
			{} break;
		}
	} // else not enabled, no can do.


	try{if(pch) free(pch); delete pvArgs; } catch(...){}

}



void LibrettoXMLHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }
	bool bSetGlobalKill = false;
	
	char szLibrettoSource[MAX_PATH]; 
	strcpy(szLibrettoSource, "LibrettoXMLHandler");

	CBufferUtil bu;

	HRESULT hRes;
	try
	{  

#if _WIN32_WINNT >= 0x0400
		hRes = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
#else
		hRes = CoInitialize(NULL);
#endif
	}
	catch(...)
	{
		if(g_plibretto) g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, szLibrettoSource, "Exception in CoInitialize");  //(Dispatch message)
	}


	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		char dberrorstring[DB_ERRORSTRING_LEN];
//		char xmlerrorstring[MAX_MESSAGE_LENGTH];
		char errorstring[MAX_MESSAGE_LENGTH];
//		char szSQL[DB_SQLSTRING_MAXLEN];
		strcpy(dberrorstring, "");
		strcpy(errorstring, "");

//		char element[MAX_MESSAGE_LENGTH];

//		AfxMessageBox(szLibrettoSource);

		CNetUtil net(false); // local object for utility functions.
		CCortexUtil util;  // cortex utility functions.

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		bool bCloseCommand = false;  // make these not persistent unless a persist command comes in.
		bool bPersist = false;  // make these not persistent unless a persist command comes in.
		int nPeriodic = -1;  
		int nTimed = -1;  

		char* pchBuffer = NULL;
		unsigned long ulBufferLen = 0;
		char* pch = NULL;
		char* pchXML = NULL;
		char* pchXMLStream = NULL;
		unsigned long ulAccumulatedBufferLen = 0;
		unsigned long ulRetry = 0;

		unsigned long ulConnLastMessage = 0;
		unsigned long ulConnTimeout = 0;
		
		char filename[MAX_PATH];
		char lastrxfilename[MAX_PATH];
		char lasttxfilename[MAX_PATH];
		int nRxDupes=0;
		int nTxDupes=0;

		FILE* fp = NULL;

		_timeb timestamp;
		_timeb timeactive;
		_timeb timeperiodic;

		_ftime(&timeactive);
		_ftime(&timeperiodic);


	// 	(*(pClient->m_pulConnections))++;//already exists

		IXMLDOMDocumentPtr pDoc(__uuidof(DOMDocument));  // let's have only one object per connection
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread D"); Sleep(100); //(Dispatch message)
		HRESULT hr;//= pDoc.CreateInstance(__uuidof(DOMDocument));
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread E");  //(Dispatch message)
		hr = pDoc->put_async(VARIANT_FALSE);
//	if(g_ptabmsgr) g_ptabmsgr->DM(MSG_ICONINFO, NULL, szCommanderSource, "client thread F");  //(Dispatch message)
										
		// Remove document safety options
		IObjectSafety* pSafety=NULL;
		DWORD dwSupported, dwEnabled;
			

		if ( SUCCEEDED(pDoc->QueryInterface(IID_IObjectSafety, (void**)&pSafety)))
		{
			pSafety->GetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, &dwSupported, &dwEnabled );
			pSafety->SetInterfaceSafetyOptions(
			IID_IXMLDOMDocument, dwSupported, 0 );
		}

		// initialize random seed
		srand ( time(NULL) );


		CCortexMessage msg;

		// allocate all the static ones
//		char* m_pchResponse[CX_XML_BUFFER_COUNT] = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};

//		AfxMessageBox("formatting");

		msg.FormatContent(CX_XML_BUFFER_CORTEXBEGIN, MAX_MESSAGE_LENGTH, "<cortex src=\"%s\" version=\"%s\">", 
			(g_plibretto->m_settings.m_pszName?g_plibretto->m_settings.m_pszName:"Libretto"), 
			CX_XML_VERSION
			);


		msg.SetContent(CX_XML_BUFFER_CORTEXEND, "</cortex>");

		msg.SetContent(CX_XML_BUFFER_FLAGSEND, "</flags>");

		msg.SetContent(CX_XML_BUFFER_DATAEND, "</data>");

		//  ack and nak are the same length so we can allocate it, just replace contents later
		msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

//		AfxMessageBox("set");



		if (FAILED(hRes))
		{
			sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established, but CoInitialize failed.  Unable to process XML.", 
				pClient->m_si.sin_addr.s_net, 
				pClient->m_si.sin_addr.s_host, 
				pClient->m_si.sin_addr.s_lh, 
				pClient->m_si.sin_addr.s_impno,
				pClient->m_socket);

			if(g_plibretto)
			{
				g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, szLibrettoSource, errorstring);  //(Dispatch message)
				g_plibretto->SendMsg(CX_SENDMSG_ERROR, szLibrettoSource, errorstring);
			}

			goto cleanup;
		}

		
		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d established.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);
		if(g_plibretto)
		{
			g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, szLibrettoSource, errorstring);  //(Dispatch message)
			g_plibretto->SendMsg(CX_SENDMSG_INFO, szLibrettoSource, errorstring);
		}



		while ( 
									(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
//								&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
								&&(pClient->m_socket!=NULL)
								&&(!bCloseCommand)
					)
		{


			pchBuffer = NULL;
			ulBufferLen = 0;

			
			int nReturnCode = net.GetLine((unsigned char**)&pchBuffer, &ulBufferLen, pClient->m_socket, NET_RCV_ONCE, errorstring);
			if(nReturnCode == NET_SUCCESS)
			{
				ulRetry = 0;
				//process any received XML.
				// have to keep accumulating until we find a </cortex> tag.
				if((pchBuffer)&&(ulBufferLen)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
				{
					int nLen = 0;
					if(pchXMLStream) nLen = ulAccumulatedBufferLen;
					pch = (char*) malloc(nLen+ulBufferLen+1);  //term 0
					if(pch)
					{
						char* pchEnd = NULL;
						char* pchNext = NULL;
						if(pchXMLStream)  // we have an old buffer.
						{
							memcpy(pch, pchXMLStream, ulAccumulatedBufferLen);
							memcpy(pch+ulAccumulatedBufferLen, pchBuffer, ulBufferLen);
							free(pchXMLStream); 
							pchXMLStream = pch;  // reassign!
							ulAccumulatedBufferLen += ulBufferLen;
							*(pch+ulAccumulatedBufferLen) = 0;  // null term
						}
						else
						{
							// this is new.
							// first we have to skip all chars that are not a '<'
							// pchEnd = strchr(pchBuffer, '<');  // have to find first tag, we are only interested in XML.
							// used to use strchr but if there are leading zeros in the buffer, we never get past them

							pchEnd = pchBuffer;
							while((*pchEnd!='<')&&(pchEnd<pchBuffer+ulBufferLen)) pchEnd++;

							if(pchEnd<pchBuffer+ulBufferLen)
							{
								strcpy(pch, pchEnd);
								ulAccumulatedBufferLen += (ulBufferLen-(pchEnd-pchBuffer));
								pchXMLStream = pch;  // reassign!
								*(pch+ulAccumulatedBufferLen) = 0;  // null term
							}
							else  // not found!
							{
								free(pch);
								pchXMLStream = NULL;
							}
						}

						if(pchBuffer) free(pchBuffer);
						pchBuffer = NULL;
						if(pchXMLStream)
						{

							pchEnd = strstr(pchXMLStream, "</cortex>");
							while((pchEnd)&&(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) )
							{
							// found a token.
								pchEnd+=strlen("</cortex>");

								pchNext = pchEnd;
								while((*pchNext!='<')&&(pchNext<pchEnd+strlen(pchEnd))) pchNext++;

								if(pchNext<pchEnd+strlen(pchEnd))
								{
									// we found a remainder.
									nLen = strlen(pchNext);
									pch = (char*) malloc(nLen+1);  //term 0
									if(pch)
									{
										memcpy(pch, pchNext, nLen);
										*(pch+nLen) = 0;
									}
								}
								else pch = NULL;


								pchXML = pchXMLStream; // just use it.
								*pchEnd = 0; //null terminate it

								pchXMLStream = pch;  // take the rest of the stream.
								if(pchXMLStream) ulAccumulatedBufferLen = strlen(pchXMLStream);
								else ulAccumulatedBufferLen=0;

								// now have to deal with XML found in pchXML.
								//debug file write
								if((g_plibretto)&&(g_plibretto->m_settings.m_bUseXMLClientLog))
								{
//				AfxMessageBox("logging");
if(g_plibretto->m_settings.m_ulDebug&LIBRETTO_DEBUG_COMM) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, szLibrettoSource, "begin log receive");  //(Dispatch message)

									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_plibretto->m_settings.m_pszName?g_plibretto->m_settings.m_pszName:"Libretto"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lastrxfilename, filename)==0)
									{
										nRxDupes++;
									}
									else
									{
										nRxDupes=0;
										strcpy(lastrxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_RX.xml",nRxDupes);

//	AfxMessageBox(filename);

									fp = fopen(filename, "wb");
									if(fp)
									{
//											fwrite(filename, 1, strlen(filename),fp);
										fwrite(pchXML, 1, strlen(pchXML),fp);
										fflush(fp);
										fclose(fp);
										fp = NULL;
									}
if(g_plibretto->m_settings.m_ulDebug&LIBRETTO_DEBUG_COMM) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, szLibrettoSource, "end log receive");  //(Dispatch message)

								}

								// do stuff here.

								_ftime(&timeactive);  // reset the inactivity timer.


								// first attempt to extract the rxid.
								char* pchRXID = NULL;
								int nIDlen = 0;

								char* pchFind = strstr(pchXML, "<rx rxid=\"");

								if(pchFind)
								{
									pchFind+=strlen("<rx rxid=\"");
									char* pchTerminate = pchFind;
									while(pchTerminate<pchEnd)
									{
										if(
											  (*pchTerminate == '\"')
											||(*pchTerminate == '>')
											||((pchTerminate<pchEnd)&&(*pchTerminate == '/')&&(*(pchTerminate+1) == '>'))
											)
										{
											break;
										}
										else
										{
											pchTerminate+=1;
										}
									}

									nIDlen = pchTerminate-pchFind;
									if(nIDlen>0)
									{
										pchRXID = (char*)malloc(nIDlen+1);
									}
									if(pchRXID)
									{
										memset(pchRXID, 0, nIDlen+1);
										memcpy(pchRXID, pchFind, nIDlen);

										pchTerminate = bu.XMLEncode(pchRXID); // just in case
										if(pchTerminate)
										{
											try{free(pchRXID);} catch(...){}
											pchRXID = pchTerminate;
										}
									}
								}


							//create a response UUID
								UUID uuid;
								UuidCreate(&uuid); // could check response code to be RPC_S_OK ?

					//			CoCreateGuid(&uuid);

								unsigned char* pucUUID = NULL;
								UuidToString(&uuid, &pucUUID);
								// call this later: RpcStringFree(&pucUUID);

								
								bool bResponseSent = false;  // means, sent a response already.  Otherwise have to look at the failure code and deal at the end

								msg.m_nTxStep = CX_XML_BUFFER_CORTEXBEGIN;

								//OK, let's assemble the tx element.

								if(pchRXID)
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUID);
								}


								// then, parse the XML
								msg.m_nFailure=CX_XML_ERR_SUCCESS;
								pDoc->put_async(VARIANT_FALSE);
			//				AfxMessageBox((char*)pchXML);
								hr = pDoc->loadXML((char*)pchXML);
								if(hr!=VARIANT_TRUE)
								{
								//					AfxMessageBox("bad load");
									bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
									_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "XML parse error: [%s]", pchXML );
									g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, szLibrettoSource, errorstring);
									//let's try to send a nak.

									msg.m_nFailure=CX_XML_ERR_XMLPARSE;
									msg.m_nType = CX_XML_TYPE_UNK;
									msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
									msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML parse error %d</msg>", msg.m_nFailure);
									msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");
								}
								else
								{
	//								AfxMessageBox("yirgacheff");

if(g_plibretto->m_settings.m_ulDebug&LIBRETTO_DEBUG_COMM) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, szLibrettoSource, "begin xml parse");  //(Dispatch message)
									IXMLDOMNodeListPtr pChildNodes = pDoc->GetchildNodes();

									IXMLDOMNodePtr pNodes[CX_XML_NODECOUNT];
									int n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }

									msg.m_nType = CX_XML_TYPE_UNK;
									msg.SetType("unknown");

									bool bCommandHandled = false;
									if (pChildNodes != NULL)
									{
										IXMLDOMNodePtr pChild;
										while(pChild = pChildNodes->nextNode())
										{
											DOMNodeType nodeType;
											pChild->get_nodeType(&nodeType);

											if(nodeType == NODE_ELEMENT)
											{
												char tag[MAX_PATH]; 
												//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
												util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

											//	AfxMessageBox(tag);

												if(strcmp("cortex", tag)==0)
												{
													pNodes[CX_XML_NODE_CORTEX] = pChild;
													// now get subs to cortex

													pChildNodes = pChild->GetchildNodes();
													if (pChildNodes != NULL)
													{
														while(pChild = pChildNodes->nextNode())
														{
															pChild->get_nodeType(&nodeType);

															if(nodeType == NODE_ELEMENT)
															{
																//strncpy(tag, W2T(pChild->GetnodeName()), MAX_PATH);
																util.ConvertWideToT(tag, MAX_PATH, &(pChild->GetnodeName()));

														//		AfxMessageBox(tag);

																if(strcmp("rx", tag)==0)
																{
																	pNodes[CX_XML_NODE_RX] = pChild;
																	// now get the "real" rxid;

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
																	//	AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
																			//strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));
								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("rxid", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
																				//strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));

																		//		AfxMessageBox("replace rxid.");

																				if(pchRXID)
																				{
																					try{free(pchRXID);} catch(...){}
																					pchRXID = bu.XMLEncode(tag); // just in case
																					if(pchRXID)
																					{
// reassemble tx element
									msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\" rxid=\"%s\"/>",
										pucUUID, pchRXID);
																					}
																				}

																			}
																			else
																			if(strcmp("part", tag)==0)
																			{
																				//  multi part message
																			}
																		}
																	}

																}
																else
																if(strcmp("cmd", tag)==0)
																{
																	pNodes[CX_XML_NODE_CMD] = pChild;
																	pNodes[CX_XML_NODE_OPTIONS] = NULL;
																	pNodes[CX_XML_NODE_DATA] = NULL;

																	// now get attribs and then get subs, data and options

																	IXMLDOMNamedNodeMapPtr pAttrMap = pChild->Getattributes();
																	if (pAttrMap != NULL)
																	{
								//										AfxMessageBox("not null attribs");

																		IXMLDOMAttributePtr pAttribute;
																		while(pAttribute = pAttrMap->nextNode())
																		{
																			// get attribs
																			//strncpy(tag, W2T(pAttribute->GetnodeName()), MAX_PATH);
																			util.ConvertWideToT(tag, MAX_PATH, &(pAttribute->GetnodeName()));
								//												AfxMessageBox(attrib);
								//												AfxMessageBox(W2T(_bstr_t(pAttribute->GetnodeValue())));
																			if(strcmp("type", tag)==0)
																			{
								//													msg.m_nID = atoi(W2T(_bstr_t(pAttribute->GetnodeValue())));
								//													if(msg.m_nID>nMaxID) nMaxID=msg.m_nID;
																				//strncpy(tag, W2T(_bstr_t(pAttribute->GetnodeValue())), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(_bstr_t(pAttribute->GetnodeValue())));
																				msg.SetType(tag);

//																																		AfxMessageBox(tag);

																				if(strcmp("ack", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_ACK;
																				}
																				else
																				if(strcmp("nak", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_NAK;
																				}
																				else
																				if(strcmp("persist", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PERSIST;
																					bPersist = true;
																				}
																				else
																				if(strcmp("quit", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_QUIT;
																					bPersist = false;
																				}
																				else
																				if(strcmp("status", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_STATUS;
																				}
																				else
																				if(strcmp("bye", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_BYE;
																				}
																				else
																				if(strcmp("get_version", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_GETVERSION;
																				}
																				else
																				if(strcmp("ping", tag)==0)
																				{
																					msg.m_nType = CX_XML_TYPE_PING;
																				}
/*
																				else

																					

/////////////////////////////////////////////////////////////////////////////////////////////////
////                                    begin Libretto specific commands
//#define SENTINEL_XML_TYPE_GETCONN				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
//#define SENTINEL_XML_TYPE_GETLIST				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
//#define SENTINEL_XML_TYPE_GETEVENT			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
																				if(strcmp("get_conn", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETCONN;
																				}
																				else
																				if(strcmp("get_list", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETLIST;
																				}
																				else
																				if(strcmp("get_event", tag)==0)
																				{
																					msg.m_nType = SENTINEL_XML_TYPE_GETEVENT;
																				}
																					
////                                    end Libretto specific commands
/////////////////////////////////////////////////////////////////////////////////////////////////
*/

																				
																			}
																			else
																			if(strcmp("user", tag)==0)
																			{
																			}
																			else
																			if(strcmp("password", tag)==0)
																			{
																			}
																		}
																	}


																	IXMLDOMNodeListPtr pCmdChildNodes = pChild->GetchildNodes();
																	if (pCmdChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pCmdChild;
																		while(pCmdChild = pCmdChildNodes->nextNode())
																		{
																			pCmdChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				//strncpy(tag, W2T(pCmdChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pCmdChild->GetnodeName()));
																				if(strcmp("options", tag)==0)
																				{
																					pNodes[CX_XML_NODE_OPTIONS] = pCmdChild;
																				}
																				else
																				if(strcmp("data", tag)==0)
																				{
																					pNodes[CX_XML_NODE_DATA] = pCmdChild;
																				}
																			}
																		}
																	}
																}
															}
														}
														// by here, all the necessary stuff should be filled out.

//														AfxMessageBox("here");
												

														switch(msg.m_nType)
														{
														case CX_XML_TYPE_ACK://						1	// 4.1 ack Acknowledges a message with success
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the ack

																
															} break; //case CX_XML_TYPE_ACK
														case CX_XML_TYPE_NAK://						2	// 4.2 nak Acknowledges a message with failure, or cancels
															{
																// the only unsolicited msg right now is the system msg
																bResponseSent = true;  // not really we just dont want to ack the nak

															} break; //case CX_XML_TYPE_NAK
														case CX_XML_TYPE_PERSIST://				3	// 4.3 persist Requests a persistent connection
															{
																//persistence already done, above
																// set timeout and hearbeat intervals
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"persist\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																if(pNodes[CX_XML_NODE_OPTIONS])
																{

																	// have to return flags
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																	IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																	if (pOptChildNodes != NULL)
																	{
																		IXMLDOMNodePtr pOptChild;
																		while(pOptChild = pOptChildNodes->nextNode())
																		{
																			pOptChild->get_nodeType(&nodeType);

																			if(nodeType == NODE_ELEMENT)
																			{
																				//strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																				util.ConvertWideToT(tag, MAX_PATH, &(pOptChild->GetnodeName()));
																				if(strcmp("timeout", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						nTimed = atoi(pchTmp);

																						_snprintf(tag, MAX_PATH, "<timeout>%d</timeout>", nTimed);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;
																					}
																				}
																				else
																				if(strcmp("heartbeat", tag)==0)
																				{
																					char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																					if(pchTmp)
																					{
																						strncpy(tag, pchTmp, MAX_PATH);

																						if(strcmp("yes", tag)==0)
																						{
																							nPeriodic = 30;
																						}
																						else
																						if(strcmp("no", tag)==0)
																						{
																							nPeriodic = -1;
																						}
																						else
																						{
																							nPeriodic = atoi(tag);
																						}

																						_snprintf(tag, MAX_PATH, "<heartbeat>%d</heartbeat>", nPeriodic);
																						msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																							(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																							tag
																							);
																						
																						msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );


																						try {free(pchTmp);} catch(...){}
																						pchTmp = NULL;

																					}
																				}
																			}
																		}
																	}
																	msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																}
																
															} break; //case CX_XML_TYPE_PERSIST
														case CX_XML_TYPE_QUIT://					4	// 4.4 quit Disconnects a persistent connection
															{
																//already done, above
																msg.SetContent(CX_XML_BUFFER_ACKBEGIN, "<ack type=\"quit\" code=\"0\">");
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

																
															} break; //case CX_XML_TYPE_QUIT
														case CX_XML_TYPE_STATUS://				5	// 4.5 status Obtains status information regarding the module
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"status\" code=\"0\">");

																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																char* pchStatus = g_plibretto->m_data.GetStatusText(&ulDataLen);
																if(pchStatus)
																{
																	msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<status><code>0x%08x</code><text>%s</text></status>",
																		ulDataLen,
																		pchStatus	
																		);
																}
																else
																{
																	msg.SetContent(CX_XML_BUFFER_DATA, 
																		"<status><code>0xffffffff</code><text>unavailable</text></status>"
																		);

																}

																if(pchStatus){try { free(pchStatus); } catch(...){}  pchStatus=NULL;}

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


															} break; //case CX_XML_TYPE_STATUS
														case CX_XML_TYPE_BYE://						6	// 4.6 bye Causes the module to shut down
															{

g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, "Libretto", "The Global Kill XML command has been received.");  //(Dispatch message)

																bSetGlobalKill = true;
																g_plibretto->m_data.m_bQuietKill = true;
																bCloseCommand = true;

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"bye\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_GETVERSION://		7	// 4.7 get_version gets the version of the module as well as the build date
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_version\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
																		"<version>%s</version><build>%s %s</build>",
																		CX_CURRENT_VERSION,
																		 __DATE__,
																		 __TIME__	
																		);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE
														case CX_XML_TYPE_PING://					8	// 4.8 ping just sends an ack to indicate it is alive
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"ping\" code=\"0\">");
																msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
																msg.ClearContent(CX_XML_BUFFER_FLAGS);
																msg.ClearContent(CX_XML_BUFFER_DATABEGIN);
																msg.ClearContent(CX_XML_BUFFER_DATA);
																msg.ClearContent(CX_XML_BUFFER_MSG);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");

															} break; //case CX_XML_TYPE_BYE


////////////////////////////////////////////////////////
// begin Libretto specific XML commands
/*
														case SENTINEL_XML_TYPE_GETCONN://				1001 // 2.1 get_conn Obtains a list of currently registered connections to Harris automation servers, and information about each connection.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_conn\" code=\"%d\">", g_plibretto->m_data.m_nNumConnectionObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_plibretto->m_data.m_critConns);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_plibretto->m_data.m_ppConnObj)&&(g_plibretto->m_data.m_nNumConnectionObjects))
																{

																	//let's see if we have any options.

																	char* pchServer = NULL;  //server name search
																	int nActive =-1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{

																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("active", tag)==0)
																					{
																				//		AfxMessageBox("active");
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}
																					//		AfxMessageBox("active 2");

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																				//			AfxMessageBox(tag);

																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_plibretto->m_data.m_nNumConnectionObjects)
																	{

																		if(g_plibretto->m_data.m_ppConnObj[m])
																		{
																			bool bInclude = true;
																			CLibrettoConnectionObject* pObj = g_plibretto->m_data.m_ppConnObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																	//		AfxMessageBox("here");
																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																	//			AfxMessageBox("here 2");
																			if(bInclude)
																			{
																				if(pObj->m_pAPIConn)
																				{
																					if((pObj->m_pAPIConn->m_SysData.systemfrx==0x29)
																						&&(pObj->m_pAPIConn->m_SysData.systemdf))
																					{
																						strcpy(tag, "29.97");
																					}
																					else
																					{
																						sprintf(tag, "%02x",pObj->m_pAPIConn->m_SysData.systemfrx);
																					}
																				}

																				_ftime(&timestamp);
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<aconn>\
<config active=\"%d\">\
<server>%s</server>\
<client>%s</client>\
<port>N/A</port>\
<basis>%s</basis>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<server_time>%.03f</server_time>\
<server_status>%d</server_status>\
<server_lists>%d</server_lists>\
<server_changed>%d</server_changed>\
<server_last_update>%.03f</server_last_update>\
<message>%s</message>\
</status>\
</aconn>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pObj->m_pszServerName,
																					pObj->m_pszClientName,
																					(pObj->m_pAPIConn?tag:"N/A"),
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(pObj->m_dblLastServerTimeMS/1000.0),
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? -1 : (pObj->m_pAPIConn?pObj->m_pAPIConn->m_Status:-1)),
																					(pObj->m_pAPIConn?pObj->m_pAPIConn->m_SysData.systemlistcount:-1),
																					(pObj->m_pAPIConn?pObj->m_pAPIConn->m_SysData.syschanged:-1),
																					pObj->m_dblUpdateTime,
																					(((pObj->m_ulStatus&SENTINEL_ICON_MASK) != SENTINEL_STATUS_CONN)? (((pObj->m_ulStatus&SENTINEL_ICON_MASK) == SENTINEL_STATUS_ERROR)?"connection error":"not connected"):"")

																																										
																				);
																					
																			

																				msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}

																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;

																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_plibretto->m_data.m_critConns);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);


															} break; //case SENTINEL_XML_TYPE_GETCONN

														case SENTINEL_XML_TYPE_GETLIST://				1002 // 2.2 get_list Obtains a list of currently registered lists or channels that are hosted on Harris automation servers registered in the system.
															{
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_list\" code=\"%d\">", g_plibretto->m_data.m_nNumChannelObjects);

																msg.ClearContent(CX_XML_BUFFER_MSG);
																unsigned long ulDataLen = 0;
																
EnterCriticalSection(&g_plibretto->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_plibretto->m_data.m_ppChannelObj)&&(g_plibretto->m_data.m_nNumChannelObjects))
																{

																	//let's see if we have any options.

																	char* pchServer = NULL;  //server name search
																	int nActive =-1;
																	int nID=-1;
																	int nListNum = -1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{

																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("id", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nID = atoi(pchTmp);
																								if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("list_number", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nListNum = atoi(pchTmp);
																								if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("active", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_plibretto->m_data.m_nNumChannelObjects)
																	{

																		if(g_plibretto->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CLibrettoChannelObject* pObj = g_plibretto->m_data.m_ppChannelObj[m];

																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

																			if((nID>0)&&(pObj->m_nChannelID != nID))  bInclude = false;
																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				_ftime(&timestamp);

																				tlistdata* ptlist = NULL;
																				if(pObj->m_pAPIConn)
																				{
																					ptlist = &(pObj->m_pAPIConn->m_ListData[pObj->m_nHarrisListID-1]);
																				}
																				
																				msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																					"<alist>\
<config active=\"%d\" id=\"%d\">\
<server>%s</server>\
<list_number>%d</list_number>\
<desc>%s</desc>\
</config>\
<status time=\"%d.%03d\">\
<list_state>%d</list_state>\
<list_changed>%d</list_changed>\
<list_display>%d</list_display>\
<list_syschange>%d</list_syschange>\
<list_count>%d</list_count>\
<list_lookahead>%d</list_lookahead>\
<list_last_update>%.3f</list_last_update>\
</status>\
</alist>",
																					((pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)?1:0),
																					pObj->m_nChannelID,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_pszDesc,
																					timestamp.time, timestamp.millitm,
																					(ptlist?ptlist->liststate:-1),
																					(ptlist?ptlist->listchanged:-1),
																					(ptlist?ptlist->listdisplay:-1),
																					(ptlist?ptlist->listsyschange:-1),
																					(ptlist?ptlist->listcount:-1),
																					(ptlist?ptlist->lookahead:-1),
																					pObj->m_dblUpdateTime
																																										
																				);
																					
																			

																				msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_LIST_MAXPAYLOAD, "%s%s", 
																					(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																					(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																					);
																				
																				msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
																			}

																		}
																		m++;
																	}

																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;

																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_plibretto->m_data.m_critChannels);

																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case SENTINEL_XML_TYPE_GETLIST
														case SENTINEL_XML_TYPE_GETEVENT://			1003 // 2.3 get_event Obtains a list of automation events contained in active lists that are registered in the system .
															{

																msg.ClearContent(CX_XML_BUFFER_MSG);
																int nGlobalCount =0;
																unsigned long ulDataLen = 0;
EnterCriticalSection(&g_plibretto->m_data.m_critChannels);
				EnterCriticalSection(&g_adc.m_crit);

																if((g_plibretto->m_data.m_ppChannelObj)&&(g_plibretto->m_data.m_nNumChannelObjects))
																{

																	//let's see if we have any options.

//																	char* pchServer = NULL;  //server name search
//																	int nActive =-1;
																	int nID=-1;
																	int nLimitNum = -1;
																	int nDone = -1;

																	if(pNodes[CX_XML_NODE_OPTIONS])
																	{
																		// have to return flags
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");


																		IXMLDOMNodeListPtr pOptChildNodes = pNodes[CX_XML_NODE_OPTIONS]->GetchildNodes();
																		if (pOptChildNodes != NULL)
																		{
																			IXMLDOMNodePtr pOptChild;
																			while(pOptChild = pOptChildNodes->nextNode())
																			{
																				pOptChild->get_nodeType(&nodeType);

																				if(nodeType == NODE_ELEMENT)
																				{
																					strncpy(tag, W2T(pOptChild->GetnodeName()), MAX_PATH);
																					if(strcmp("id", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nID = atoi(pchTmp);
																								if(nID <=0) nID = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<id>%d</id>", nID);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("limit", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nLimitNum = atoi(pchTmp);
																								if(nLimitNum <=0) nLimitNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<limit>%d</limit>", nLimitNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("done", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nDone = 0;
																								else if(strcmp("1", pchTmp)==0) nDone = 1;
																								else if(strcmp("yes", pchTmp)==0) nDone = 1;
																								else if(strcmp("no", pchTmp)==0) nDone = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<done>%d</done>", nDone);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}

																					/*
																					else
																					if(strcmp("list_number", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								nListNum = atoi(pchTmp);
																								if(nListNum <=0) nListNum = -1;  // zero and negative are invalid IDs
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<list_number>%d</list_number>", nListNum);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("active", tag)==0)
																					{
																						char* pchTmp = msg.XMLTextNodeValue(pOptChild);
																						if(pchTmp)
																						{
																							if(strlen(pchTmp)>0)
																							{
																								if(strcmp("0", pchTmp)==0) nActive = 0;
																								else if(strcmp("1", pchTmp)==0) nActive = 1;
																								else if(strcmp("yes", pchTmp)==0) nActive = 1;
																								else if(strcmp("no", pchTmp)==0) nActive = 0;
																								// all other input ignored
																							}

																							_snprintf(tag, MAX_PATH, "<active>%d</active>", nActive);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																							try {free(pchTmp);} catch(...){}
																							pchTmp = NULL;
																						}
																					}
																					else
																					if(strcmp("server", tag)==0)
																					{
																						pchServer = msg.XMLTextNodeValue(pOptChild);
																						if(pchServer)
																						{
																							_snprintf(tag, MAX_PATH, "<server>%s</server>", pchServer);
																							msg.FormatContent(CX_XML_BUFFER_FLAGSBEGIN, MAX_MESSAGE_LENGTH, "%s%s", 
																								(msg.m_pchResponse[CX_XML_BUFFER_FLAGS]?msg.m_pchResponse[CX_XML_BUFFER_FLAGS]:""),
																								tag
																								);
																							
																							msg.SetContent(CX_XML_BUFFER_FLAGS, msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN] );

																						}
																					}
																					* /
																				}
																			}
																		}
																		msg.SetContent(CX_XML_BUFFER_FLAGSBEGIN, "<flags>");
																	}

																	int m = 0;
																	while(m<g_plibretto->m_data.m_nNumChannelObjects)
																	{

																		if(g_plibretto->m_data.m_ppChannelObj[m])
																		{
																			bool bInclude = true;
																			CLibrettoChannelObject* pObj = g_plibretto->m_data.m_ppChannelObj[m];

/*
																			if((pchServer)&&(pObj->m_pszServerName)&&(strcmp(pchServer,pObj->m_pszServerName)!=0)) bInclude = false;

																			if((nActive==0)&&(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED)) bInclude = false;
																			else if((nActive==1)&&(!(pObj->m_ulFlags&SENTINEL_FLAG_ENABLED))) bInclude = false;

* /																		if(
																				//	(nID>0)&&  //commenting this out makes it mandatory!
																					(pObj->m_nChannelID != nID)
																				)  
																				bInclude = false;

//																			if((nListNum>0)&&(pObj->m_nHarrisListID != nListNum))  bInclude = false;


																			if(bInclude)
																			{
																				// found a channel that works, now must loop thru the events, filtering if nec.

																				// need to allocate a big buffer and deal with directly, rather than allocating new for every event.
																			
/* // was this, which worked but slowly.  below is new
																				if(pObj->m_ppevents)
																				{
																					 
																					int q = 0;
																					int nCount = 0;
																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;
																						unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));

																						if(pObj->m_ppevents[q])
																						{
																							CLibrettoEventObject* pEObj = pObj->m_ppevents[q];

																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;
																							if(bInclude)
																							{

																								_ftime(&timestamp);

																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
																								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, 
																									"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_usControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CLibrettoEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																								);
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																								msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );

																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
* /




																				char* pchPayload = (char*)malloc(SENTINEL_XML_EVENT_MAXPAYLOAD);
																				char* pchAppend = pchPayload;

																				// re-use errorstring, why not.
																		//		int nTimes[10] = {0,0,0,0,0,0,0,0,0,0};
																				int nClock = clock();
if(g_plibretto->m_settings.m_ulDebug&LIBRETTO_DEBUG_COMM) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, szLibrettoSource, "start event payload assembly");  //(Dispatch message)

																				if((pObj->m_ppevents)&&(pchPayload))
																				{
																					unsigned long ulBufferLen = SENTINEL_XML_EVENT_MAXPAYLOAD;
																					char* pchBufferEnd = (pchPayload+SENTINEL_XML_EVENT_MAXPAYLOAD);
																					int nEventLen = 0;
																					unsigned short usHARRISDONE = ((1<<eventdone)|(1<<eventpostrolled)|(1<<notplayed)|(1<<eventskipped));
																					int q = 0;
																					int nCount = 0;
																					int nBuffers = 1;
																					int nTempOffset;
																					while(q<pObj->m_nNumEvents)
																					{
																						bInclude = true;

																						if(pObj->m_ppevents[q])
																						{
																					//		nTimes[0] += (clock() - nClock); nClock = clock(); 

																							CLibrettoEventObject* pEObj = pObj->m_ppevents[q];
																							if((nLimitNum>0)&&(nCount >= nLimitNum)) {bInclude = false; break; }  // no need to continue...
																							if((nDone==0)&&(pEObj->m_event.m_usStatus&usHARRISDONE)) bInclude = false;
																							else if((nDone==1)&&(!(pEObj->m_event.m_usStatus&usHARRISDONE))) bInclude = false;

																				//			nTimes[1] += (clock() - nClock); nClock = clock(); 
																							if(bInclude)
																							{

																							//	_ftime(&timestamp);
																					//		nTimes[2] += (clock() - nClock); nClock = clock(); 

																								char* pchXMLkey = bu.XMLEncode(pEObj->m_pszEncodedKey);
																								char* pchXMLrec = bu.XMLEncode(pEObj->m_event.m_pszReconcileKey);
																								char* pchXMLclip = bu.XMLEncode(pEObj->m_event.m_pszID);
																								char* pchXMLtitle = bu.XMLEncode(pEObj->m_event.m_pszTitle);
																								char* pchXMLdata = bu.XMLEncode(pEObj->m_event.m_pszData);
																								
																					//		nTimes[3] += (clock() - nClock); nClock = clock(); 
																								_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, 
																									"<aitem id=\"%d\">\
<source>\
<server>%s</server>\
<list_number>%d</list_number>\
<list_id>%d</list_id>\
</source>\
<info>\
<key>%s</key>\
<rec>%s</rec>\
<clip>%s</clip>\
<segment>%d</segment>\
<title>%s</title>\
<data>%s</data>\
<type>%d</type>\
<state>%d</state>\
<time_mode>%d</time_mode>\
<start>%.3f</start>\
<duration>%d</duration>\
<calc_start>%.3f</calc_start>\
<position>%d</position>\
<parent>%d</parent>\
<last_update>%.3f</last_update>\
</info>\
</aitem>",
																					pEObj->m_uid,
																					pObj->m_pszServerName,
																					pObj->m_nHarrisListID,
																					pObj->m_nChannelID,
																					(pchXMLkey?pchXMLkey:""),
																					(pchXMLrec?pchXMLrec:""),
																					(pchXMLclip?pchXMLclip:""),
																					((pEObj->m_event.m_ucSegment==0xff)?-1:pEObj->m_event.m_ucSegment),
																					(pchXMLtitle?pchXMLtitle:""),
																					(pchXMLdata?pchXMLdata:""),
																					pEObj->m_event.m_usType,
																					pEObj->m_event.m_usStatus,
																					pEObj->m_event.m_usControl,
																					pEObj->m_dblTime,
																					pEObj->m_event.m_ulDurationMS,
																					pEObj->m_dblCalcTime,
																					pEObj->m_nPosition,
																					(pEObj->m_pParent?((CLibrettoEventObject*)pEObj->m_pParent)->m_uid:-1),
																					pEObj->m_dblUpdateTime
																																										
																								);
																					//		nTimes[4] += (clock() - nClock); nClock = clock(); 
																									
																								if( pchXMLkey ) {try {free(pchXMLkey);} catch(...){}}
																								if( pchXMLrec ) {try {free(pchXMLrec);} catch(...){}}
																								if( pchXMLclip ) {try {free(pchXMLclip);} catch(...){}}
																								if( pchXMLtitle ) {try {free(pchXMLtitle);} catch(...){}}
																								if( pchXMLdata ) {try {free(pchXMLdata);} catch(...){}}

																						//	nTimes[5] += (clock() - nClock); nClock = clock(); 
																								nEventLen = strlen(errorstring)+1;//+1 for term zero

																								if(pchAppend+nEventLen>pchBufferEnd)  // if must be changed to while if SENTINEL_XML_EVENT_MAXPAYLOAD defined < MAX_MESSAGE_LENGTH (buffer vs errorstring)
																								{
																									// need to realloc
																									nBuffers++;
																									ulBufferLen = SENTINEL_XML_EVENT_MAXPAYLOAD*nBuffers;
																									char* pchBufferNew = (char*)malloc(ulBufferLen);
																									if(pchBufferNew)
																									{
																										nTempOffset = pchAppend+1 - pchPayload;
																										memcpy(pchBufferNew, pchPayload, nTempOffset);
																										try {free(pchPayload);} catch(...){}
																										pchPayload = pchBufferNew;
																										pchBufferEnd = (pchPayload+ulBufferLen);
																										pchAppend = pchPayload+nTempOffset-1;																									

																									}
																								}
																								

																								// append
																								memcpy(pchAppend, errorstring, nEventLen);
																								pchAppend += nEventLen-1; // append over the term 0 next time.


																						//	nTimes[6] += (clock() - nClock); nClock = clock(); 


/*
																								msg.FormatContent(CX_XML_BUFFER_MSG, SENTINEL_XML_EVENT_MAXPAYLOAD, "%s%s", 
																									(msg.m_pchResponse[CX_XML_BUFFER_DATA]?msg.m_pchResponse[CX_XML_BUFFER_DATA]:""),
																									(msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]?msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]:"")
																									);
																								
																								msg.SetContent(CX_XML_BUFFER_DATA, msg.m_pchResponse[CX_XML_BUFFER_MSG] );
* /
																								nCount++;
																								nGlobalCount++;
																							}
																						}
																						q++;
																					}
																				}
																				msg.ClearContent(CX_XML_BUFFER_DATA);
																				if(pchPayload)
																				{
																					msg.m_pchResponse[CX_XML_BUFFER_DATA] = pchPayload;		
																					ulDataLen = pchAppend - pchPayload;
																				}
if(g_plibretto->m_settings.m_ulDebug&LIBRETTO_DEBUG_COMM) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, szLibrettoSource, "end event payload assembly");  //(Dispatch message)

																			}

																		}
																		m++;
																	}

/*																	if(pchServer)
																	{ try {free(pchServer);} catch(...){} }
																	pchServer= NULL;
* /
																}
				LeaveCriticalSection(&g_adc.m_crit);
LeaveCriticalSection(&g_plibretto->m_data.m_critChannels);

/* // not needed now, we tally the buffer as we go
																ulDataLen = 0;
																if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
																{
																	ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
																}
																* /
																msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
																	ulDataLen,
																	net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
																	);

																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"get_event\" code=\"%d\">", nGlobalCount);

																msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");
																msg.ClearContent(CX_XML_BUFFER_MSG);
															} break; //case SENTINEL_XML_TYPE_GETEVENT

// end Libretto specific XML commands
////////////////////////////////////////////////////////
*/
														case CX_XML_TYPE_UNK://						0 // unknown
														default:
															{
																sprintf(errorstring, "Unsupported XML command: [%s]", (msg.m_pchType?msg.m_pchType:"") );
																g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, szLibrettoSource, errorstring);
																//let's try to send a nak.

																msg.m_nType = CX_XML_TYPE_UNK;
																msg.m_nFailure=CX_XML_ERR_BADCMD;
																msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"%s\" code=\"%d\">", (msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Unsupported XML command [%s] (error %d)</msg>",(msg.m_pchType?msg.m_pchType:""), msg.m_nFailure);
																msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

															} break; //case CX_XML_TYPE_UNK://						0 // unknown
														}
															
														
														if(!bPersist) bCloseCommand = true;

													}
													else // no child nodes to cortex - can't identify command.
													{
														bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
														sprintf(errorstring, "Cortex XML format error: [%s]", pchXML );
														g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, szLibrettoSource, errorstring);
														//let's try to send a nak.

														msg.m_nType = CX_XML_TYPE_UNK;
														msg.m_nFailure=CX_XML_ERR_NOCMD;
														msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
														msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>Cortex XML format error %d</msg>", msg.m_nFailure);
														msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

													}
												}
											}
										}
									}
									else
									{
										// no child nodes, can't do nuttin.
										bCloseCommand = true;  // on error let's cut, so that we force a new connection to be established
										sprintf(errorstring, "XML format error: [%s]", pchXML );
										g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, szLibrettoSource, errorstring);
										//let's try to send a nak.

										msg.m_nType = CX_XML_TYPE_UNK;
										msg.m_nFailure=CX_XML_ERR_NOCHILD;
										msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<nak type=\"unknown\" code=\"%d\">", msg.m_nFailure);
										msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>XML format error %d</msg>", msg.m_nFailure);
										msg.SetContent(CX_XML_BUFFER_ACKEND, "</nak>");

									}
if(g_plibretto->m_settings.m_ulDebug&LIBRETTO_DEBUG_COMM) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, szLibrettoSource, "end xml parse");  //(Dispatch message)

									n=CX_XML_NODE_CORTEX;
									while(n<CX_XML_NODECOUNT){ pNodes[n]=NULL; n++; }
								}

								if(!bResponseSent)
								{
if(g_plibretto->m_settings.m_ulDebug&LIBRETTO_DEBUG_COMM) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, szLibrettoSource, "begin send response");  //(Dispatch message)

								// send it
		_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_plibretto)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending response message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, szLibrettoSource, errorstring);  //(Dispatch message)
				g_plibretto->SendMsg(CX_SENDMSG_ERROR, szLibrettoSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
if(g_plibretto->m_settings.m_ulDebug&LIBRETTO_DEBUG_COMM) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, szLibrettoSource, "end send response");  //(Dispatch message)

								}

								// log it

								//debug file write
								if((g_plibretto)&&(g_plibretto->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
								{
if(g_plibretto->m_settings.m_ulDebug&LIBRETTO_DEBUG_COMM) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, szLibrettoSource, "begin log response");  //(Dispatch message)
									strcpy(filename, "Logs");
									_mkdir(filename);  // if exists already np
									strcat(filename, "\\");
									sprintf(errorstring, "%d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno);

									strcat(filename, errorstring);

									_mkdir(filename);  // if exists already np

									_ftime( &timestamp );

									tm* theTime = localtime( &timestamp.time	);

									sprintf(errorstring, "\\%s_", 
										(g_plibretto->m_settings.m_pszName?g_plibretto->m_settings.m_pszName:"Libretto"));

									strcat(filename, errorstring);

									char filenametemp[MAX_PATH];
									strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

									strcat(filename, filenametemp);

									int nOffset = strlen(filename);
									sprintf(filename+nOffset,"%03d",timestamp.millitm);

									if (strcmp(lasttxfilename, filename)==0)
									{
										nTxDupes++;
									}
									else
									{
										nTxDupes=0;
										strcpy(lasttxfilename, filename);
									}
									nOffset = strlen(filename);
									sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

									fp = fopen(filename, "wb");
									if(fp)
									{
										int b=CX_XML_BUFFER_CORTEXBEGIN;
										while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
										{
//											fwrite(filename, 1, strlen(filename),fp);
											if(msg.m_pchResponse[b])
											{
												if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
												else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

												fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
												fflush(fp);
											}
											b++;

										}
										fclose(fp);
										fp = NULL;
									}
if(g_plibretto->m_settings.m_ulDebug&LIBRETTO_DEBUG_COMM) 
	g_plibretto->m_msgr.DM(MSG_ICONHAND, NULL, szLibrettoSource, "end log response");  //(Dispatch message)
								}

//AfxMessageBox("001");
								int b=CX_XML_BUFFER_TX;
								while(b<CX_XML_BUFFER_DATAEND)
								{
									if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
									{
										try { free(msg.m_pchResponse[b]); } catch(...){}
										msg.m_pchResponse[b] = NULL;
									}
									b++;
								}

//AfxMessageBox("002");
								if(pucUUID)
								{
									try { RpcStringFree(&pucUUID); } catch(...){}
								}
								pucUUID = NULL;

//AfxMessageBox("003");
								if(pchXML) 
								{
									try { free(pchXML); } catch(...){}
								}
								pchXML = NULL;
//AfxMessageBox("004");

								if(pchRXID) 
								{
									try { free(pchRXID); } catch(...){}
								}
								pchRXID = NULL;

//AfxMessageBox("005");
								if (pchXMLStream) 
									pchEnd = strstr(pchXMLStream, "</cortex>");
								else pchEnd = NULL;
//AfxMessageBox("006");
							}  // while </cortex> exists.
						}//if(pchXMLStream)
						// dont do a free(pch);  
					}  // else out of mem, so just skip
					else
					{
						// out of mem.. .log error?
					}

				} //if((pchBuffer)&&(ulBufferLen)&&(!pConn->m_bKillThread))

			} // if getline succeeds.
			else
			{
				// here, we either have timed out because theres no data, or, the connection has been lost.
				// if we lost the conn, no need to re-establish, we are a client handler, let the client reconnect if they want.
				if(nReturnCode == NET_ERROR_CONN)		// connection lost
				{
					ulRetry++;

					if(g_plibretto)
					{
						_snprintf(errorstring, MAX_MESSAGE_LENGTH, "Lost connection from %d.%d.%d.%d", 
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno );

						g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, szLibrettoSource, errorstring);  //(Dispatch message)
						g_plibretto->SendMsg(CX_SENDMSG_INFO, szLibrettoSource, errorstring);
					}

					break;  // break out of while loop, closes connection, starts again
				}
				else
				{
//									g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, szLibrettoSource, "check %d", clock());  //(Dispatch message)

					// we prob just havent gotten a message because no data.  
					// so, check to see if its time for publication service
									// no, do this in the stall loop


				}

			} // end of else from if(nReturnCode == NET_SUCCESS)

			// and lets check if we've timed out on the connection, no matter what the success was
			_ftime( &timestamp );
			if(
				  (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				&&(ulConnTimeout>0)
				&&(ulConnLastMessage>0)
				&&((ulConnLastMessage + ulConnTimeout) < (unsigned long)timestamp.time )
				)
			{
				// we timed out, need to disconnect
				ulRetry++;


				sprintf(errorstring, "Connection from %d.%d.%d.%d timed out.", 
					pClient->m_si.sin_addr.s_net, 
					pClient->m_si.sin_addr.s_host, 
					pClient->m_si.sin_addr.s_lh, 
					pClient->m_si.sin_addr.s_impno);

				if(g_plibretto)
				{
					g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, szLibrettoSource, errorstring);  //(Dispatch message)
					g_plibretto->SendMsg(CX_SENDMSG_INFO, szLibrettoSource, errorstring);
				}

				if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;} //free this memory before breaking out of the while.

				break;  // break out of while loop, closes connection, starts again
			}

			if(pchBuffer) { try{free(pchBuffer); } catch(...){} pchBuffer=NULL;}
			
			if(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) Sleep(1);  // dont peg processor




/*
			// following line fills out the data structure
//			nReturn = net.ReceiveData(pClient->m_socket, &data, pszInfo);
			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszInfo);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Libretto:XMLHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Libretto:XMLHandlerThread");
						}
						// we had an error receiving data and then another trying to send, so the connection is probably closed.  let's close it 
						bCloseCommand = true;
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszInfo);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszInfo);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Libretto:XMLHandlerThread");
						}
					}
				}
			}
*/
			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.

			// here we want to stall untill we know we have more data to recv
			if ( 
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
					&&(!bCloseCommand)
					)	
			{

				struct timeval tv;
				tv.tv_sec = 0; tv.tv_usec = 50;  // timeout value
				fd_set fds;
				int nNumSockets;
				FD_ZERO(&fds);

				while (!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL))
				{
					FD_SET(pClient->m_socket, &fds);
					nNumSockets = select(0, &fds, NULL, NULL, &tv);

					if ( nNumSockets == INVALID_SOCKET )
					{
						// report the error but keep going
						if(pClient->m_lpMsgObj)
						{
							int nErrorCode = WSAGetLastError();
							char* pchError = net.WinsockEnglish(nErrorCode);
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: socket error in select. %s", pchError?pchError:"");
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "Libretto:XMLHandlerThread");
							if(pchError) LocalFree(pchError);
						}
					
						bCloseCommand = true;
					}
					else
					if(
							(nNumSockets==0) // 0 = timed out, -1 = error
						||(!(FD_ISSET(pClient->m_socket, &fds)))
						) 
					{ 
//						g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, szLibrettoSource, "check %d", clock());  //(Dispatch message)


						if(nPeriodic>0)
						{
							_ftime( &timestamp );
							if((timeperiodic.time + nPeriodic)< timestamp.time)
							{
								// send asynchronous message

								//create a response UUID
								UUID uuida;
								UuidCreate(&uuida); // could check response code to be RPC_S_OK ?

								unsigned char* pucUUIDa = NULL;
								UuidToString(&uuida, &pucUUIDa);

								msg.FormatContent(CX_XML_BUFFER_TX, MAX_MESSAGE_LENGTH, "<tx txid=\"%s\"/>", pucUUIDa);

								if(pucUUIDa)
								{
									try { RpcStringFree(&pucUUIDa); } catch(...){}
								}
								pucUUIDa = NULL;


								msg.FormatContent(CX_XML_BUFFER_ACKBEGIN, MAX_MESSAGE_LENGTH, "<ack type=\"system\" code=\"0\">");

								msg.ClearContent(CX_XML_BUFFER_FLAGSBEGIN);
								msg.ClearContent(CX_XML_BUFFER_FLAGS);

								unsigned long ulDataLen = 0;
								char* pchStatus = g_plibretto->m_data.GetStatusText(&ulDataLen);
								if(pchStatus)
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0x%08x</code><text>%s</text></status><time>%d.%03d</time></system>",
										ulDataLen,
										pchStatus,
										timestamp.time, timestamp.millitm
										);
								}
								else
								{
									msg.FormatContent(CX_XML_BUFFER_DATA, MAX_MESSAGE_LENGTH, 
										"<system><status><code>0xffffffff</code><text>unavailable</text></status><time>%d.%03d</time></system>",
										timestamp.time, timestamp.millitm
										);

								}

								if(pchStatus){try { free(pchStatus); } catch(...){} pchStatus=NULL;}

								ulDataLen = 0;
								if(msg.m_pchResponse[CX_XML_BUFFER_DATA])
								{
									ulDataLen = strlen(msg.m_pchResponse[CX_XML_BUFFER_DATA]);
								}
								msg.FormatContent(CX_XML_BUFFER_DATABEGIN, MAX_MESSAGE_LENGTH, "<data type=\"xml\" len=\"%d\" chk=\"0x%02x\">",
									ulDataLen,
									net.Checksum(((unsigned char*)(msg.m_pchResponse[CX_XML_BUFFER_DATA])), ulDataLen)
									);

								msg.FormatContent(CX_XML_BUFFER_MSG, MAX_MESSAGE_LENGTH, "<msg>%s status message</msg>", 
									(g_plibretto->m_settings.m_pszName?g_plibretto->m_settings.m_pszName:"Libretto"));

								msg.SetContent(CX_XML_BUFFER_ACKEND, "</ack>");


								// now, send and log.
	////////////////////////////////////////////// code copied from above.  if modified, must be 2 places.  sorry...
									// send it
			_ftime(&timeperiodic);
									int b=CX_XML_BUFFER_CORTEXBEGIN;
									while(b<CX_XML_BUFFER_COUNT)
									{
										if(msg.m_pchResponse[b])
										{
											if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
											else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

											unsigned long ulBufLen = strlen(msg.m_pchResponse[b]); // valid from an error page.
											int nReturn = net.SendLine((unsigned char*)msg.m_pchResponse[b], ulBufLen, pClient->m_socket, EOLN_NONE, false, 5000, pszStatus);
											if(nReturn<NET_SUCCESS)
											{
			if(g_plibretto)
			{
				_snprintf(errorstring, MAX_MESSAGE_LENGTH-1, "Error %d sending system message to %d.%d.%d.%d: %s\r\nResetting connection.", 
										nReturn,
										pClient->m_si.sin_addr.s_net, 
										pClient->m_si.sin_addr.s_host, 
										pClient->m_si.sin_addr.s_lh, 
										pClient->m_si.sin_addr.s_impno,
										pszStatus
									);
				g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, szLibrettoSource, errorstring);  //(Dispatch message)
				g_plibretto->SendMsg(CX_SENDMSG_ERROR, szLibrettoSource, errorstring);
			}

												break; // break out and discontinue sending.
											}
											else
											{
												msg.m_nTxStep = b;
											}
										}
										b++;
									}
								
	//AfxMessageBox("oop");
									// log it

									//debug file write
									if((g_plibretto)&&(g_plibretto->m_settings.m_bUseXMLClientLog)&&(msg.m_nTxStep>CX_XML_BUFFER_CORTEXBEGIN))
									{
										strcpy(filename, "Logs");
										_mkdir(filename);  // if exists already np
										strcat(filename, "\\");
										sprintf(errorstring, "%d.%d.%d.%d", 
											pClient->m_si.sin_addr.s_net, 
											pClient->m_si.sin_addr.s_host, 
											pClient->m_si.sin_addr.s_lh, 
											pClient->m_si.sin_addr.s_impno);

										strcat(filename, errorstring);

										_mkdir(filename);  // if exists already np

		//								_ftime( &timestamp );  // use timestamp we just had..

										tm* theTime = localtime( &timestamp.time	);

										sprintf(errorstring, "\\%s_", 
											(g_plibretto->m_settings.m_pszName?g_plibretto->m_settings.m_pszName:"Libretto"));

										strcat(filename, errorstring);

										char filenametemp[MAX_PATH];
										strftime(filenametemp, MAX_PATH-1, "%Y-%m-%d_%H.%M.%S.", theTime );

										strcat(filename, filenametemp);

										int nOffset = strlen(filename);
										sprintf(filename+nOffset,"%03d",timestamp.millitm);

										if (strcmp(lasttxfilename, filename)==0)
										{
											nTxDupes++;
										}
										else
										{
											nTxDupes=0;
											strcpy(lasttxfilename, filename);
										}
										nOffset = strlen(filename);
										sprintf(filename+nOffset,"%02d_TX.xml",nRxDupes);

										fp = fopen(filename, "wb");
										if(fp)
										{
											int b=CX_XML_BUFFER_CORTEXBEGIN;
											while((b<=msg.m_nTxStep)&&(b<CX_XML_BUFFER_COUNT))
											{
	//											fwrite(filename, 1, strlen(filename),fp);
												if(msg.m_pchResponse[b])
												{
													if((b==CX_XML_BUFFER_ACKEND) && (msg.m_pchResponse[CX_XML_BUFFER_ACKBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_FLAGS)||(b==CX_XML_BUFFER_FLAGSEND)) && (msg.m_pchResponse[CX_XML_BUFFER_FLAGSBEGIN]==NULL)) { b++; continue; }
													else if(((b==CX_XML_BUFFER_DATA)||(b==CX_XML_BUFFER_DATAEND)) && (msg.m_pchResponse[CX_XML_BUFFER_DATABEGIN]==NULL)) { b++; continue; }

													fwrite(msg.m_pchResponse[b], 1, strlen(msg.m_pchResponse[b]),fp);
													fflush(fp);
												}
												b++;

											}
											fclose(fp);
											fp = NULL;
										}
									}


									b=CX_XML_BUFFER_TX;
									while(b<CX_XML_BUFFER_DATAEND)
									{
										if((b!=CX_XML_BUFFER_FLAGSEND)&&(msg.m_pchResponse[b])) 
										{
											try { free(msg.m_pchResponse[b]); } catch(...){}
											msg.m_pchResponse[b] = NULL;
										}
										b++;
									}
	/////////////////////////////////////////////
							}

						}

						//and check timeout
						if(nTimed>0)
						{
							_ftime( &timestamp );
							if((timeactive.time + nTimed) < timestamp.time)
							{
								//kill it

								bCloseCommand = true;
							}
							
						}


						continue; 
					} 
					else // there is recv data.
					{//  this is also set if the client connection closes.  if that occurs the recv will get 0 and we can close it out.
						break; 
					}
				}
			}
		} 

cleanup:
//		AfxMessageBox("cleanup");
//AfxMessageBox("010");

//		pDoc->Release();  // destructor calls this so don't do it here. 
		if(pSafety) pSafety->Release();

//AfxMessageBox("011");

// cleanup:  //was here but want to uninit com etc
//AfxMessageBox("012");

		sprintf(errorstring, "Connection from %d.%d.%d.%d on socket %d closed.", 
			pClient->m_si.sin_addr.s_net, 
			pClient->m_si.sin_addr.s_host, 
			pClient->m_si.sin_addr.s_lh, 
			pClient->m_si.sin_addr.s_impno,
			pClient->m_socket);

//AfxMessageBox("013");
		if(g_plibretto)
		{
			g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, szLibrettoSource, errorstring);  //(Dispatch message)
			g_plibretto->SendMsg(CX_SENDMSG_INFO, szLibrettoSource, errorstring);
		}

//AfxMessageBox("014");

		if(pch!=NULL) { try{free(pch);} catch(...) {} } // must free the data buffer 

//AfxMessageBox("015");

		shutdown(pClient->m_socket, SD_BOTH);
//AfxMessageBox("016");
		closesocket(pClient->m_socket);
//AfxMessageBox("017");
		(*(pClient->m_pulConnections))--;
//AfxMessageBox("018");

	}
//		AfxMessageBox("end cleanup");
//AfxMessageBox("019");

	try
	{  
		CoUninitialize(); //XML
	}
	catch(...)
	{
		if(g_plibretto) g_plibretto->m_msgr.DM(MSG_ICONERROR, NULL, szLibrettoSource, "Exception in CoUninitialize");  //(Dispatch message)
	}

//AfxMessageBox("020");

	try
	{
		delete pClient; // was created with new in the thread that spawned this one.
	}	catch(...) {}

//AfxMessageBox("021");
	if (bSetGlobalKill)	//g_bKillThread = true;
		((CLibrettoHandler*)theApp.m_pMainWnd)->OnExternalCmdExit();

}

void TimeCodeDataThread(void* pvArgs)
{
	CLibrettoData* p = &(g_plibretto->m_data);
	char szLibrettoSource[MAX_PATH]; 
	strcpy(szLibrettoSource, "LibrettoTimeCode");
	
	p->m_bTimeCodeThreadStarted=TRUE;
	SetThreadPriority(GetCurrentThread(), REALTIME_PRIORITY_CLASS);

	BOOL bNewInput = TRUE;
//	BOOL bAttemptSync = FALSE;  // dont even need.
	if(g_plibretto) g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, szLibrettoSource, "Time Code thread started");  //(Dispatch message)

	int nLastSleep = -1;
	int nLastValidTime = -1;
	int nValidLoops = 0;
	unsigned long ulLastTC=0xffffffff;
	int nLastTCTime = -1;
	int nNumTCRepeats = 0;
	int nLastSync = 0;
	SYSTEMTIME SystemTime;  // for jam sync the clock
	_timeb timeNow;

	while(!p->m_bTimeCodeThreadKill)
	{
		unsigned long tick = clock();
//		unsigned char df =0x80; //default to DF
		if(g_plibretto->m_tc.m_hConnection == INVALID_HANDLE_VALUE)
		{
	EnterCriticalSection(&p->m_critTimeCode);
							p->m_ulTimeCode = 0xffffffff;
							p->m_ulTimeCodeElapsed = 0;
	LeaveCriticalSection(&p->m_critTimeCode);

//			p->SetWindowText(" Adrienne LTC/RDR: no device");
			// connect.
			g_plibretto->m_tc.ConnectToCard();

			if(g_plibretto->m_tc.m_hConnection != INVALID_HANDLE_VALUE)
			{
//				p->SetWindowText(" Adrienne LTC/RDR: connected!");

				g_plibretto->m_tc.GetBoardCapabilitesCode();
				if(!(g_plibretto->m_tc.m_ucBoardCapabilitesCode & 0x10)) // no capabilities
				{
					bNewInput = TRUE;
//					bAttemptSync = FALSE; // disengage
//					p->GetDlgItem(IDC_STATIC_TEXT)->SetWindowText("Sync disengaged!  LTC not installed");
//					p->SetWindowText("Adrienne LTC/RDR: no LTC RDR");
					g_plibretto->m_tc.DisconnectFromCard();  // nothing else to do.
					Sleep(300);
				}
				else
				{
//					p->SetWindowText(" Adrienne LTC/RDR: LTC installed");
					nLastValidTime = clock(); // to reset timing
					nLastTCTime = clock(); // to reset timing
				}
			}
		}
		else
		{
		
			//connected, so just grab status and time if status allows.
			int nBoardCheck = clock();
			g_plibretto->m_tc.GetBoardInputsAndOpModeCode();
			int nBoardEnd = clock();

//			g_plibretto->m_tc.GetBoardOpModeCode();
//			g_plibretto->m_tc.GetBoardInputsCode();
			//inputs
//If bit is set... (Register mapping)
//Bit 6 => L21 input data OK
//Bit 5 => VITC input data OK
//Bit 4 => LTC input data OK
//Bit 3 => Wide VSYNC pulses detected
//Bit 1 => ODD(1) or EVEN(0) video field
//Bit 0 => Video input OK	
			//op modes
//If bit is set... (Register mapping)
//Bit 7 => Diagnostics active
//Bit 6 => FILM(24fps) mode selected
//Bit 5 => LTC(1) or VITC(0) selected
//Bit 4 => EBU(25fps) mode selected
//Bit 3 => VITC Generator active
//Bit 2 => LTC Generator active
//Bit 1 => VITC and/or L21 Reader active
//Bit 0 => LTC Reader active	
			CString t; 
			CString g; 

			if(g_plibretto->m_tc.m_ucBoardOpModeCode&0x20)
			{
				if(g_plibretto->m_tc.m_ucBoardInputsCode&0x10)
				{
					if(bNewInput)
					{
	EnterCriticalSection(&p->m_critTimeCode);
						p->m_ucTimeCodeBits = g_plibretto->m_tc.GetTimeCodeBits();  //get actual DF	
	LeaveCriticalSection(&p->m_critTimeCode);
						bNewInput = FALSE;
					}

/*
1) Read and save the status of the VALIDATION byte at 1Ah.
2) Go back to step #1 if bit 7 is high (update in progress).
3) Quickly read and save all of the time, user, and embedded bits which are required by your application. Do not process yet.
4) Read the VALIDATION byte again, then compare it with the value read in step #1.
5) Go back to step #1 if the VALIDATION byte has changed.
6) If desired, read the input signal status byte at 0Ch to make sure that the time code data you just read is current.
7) Process and/or display the time code data as desired. Note that if you attempt to display the time bits as they are read, on many PC�s the
   next time code field or frame will appear before you have finished displaying the previous one. The results will not be pleasant, hence our recommendations.
8) Update the rest of your program operations.
9) Go back to step #1 (NOT STEP #3!).
*/
					int nValidCheck = 0;
					int nValidStart = clock();
					int nValidEnd = 0;
					int nTimeEnd = 0;

					unsigned char vc = g_plibretto->m_tc.GetValidationCode();
					while((!p->m_bTimeCodeThreadKill)&&(vc&0x80))
					{
						Sleep(1);
						vc = g_plibretto->m_tc.GetValidationCode();
						nValidCheck++;
					}
					nValidEnd = clock();
					unsigned long TC = g_plibretto->m_tc.GetTimeCode();
					nTimeEnd = clock();

					if((!p->m_bTimeCodeThreadKill)&&(vc == g_plibretto->m_tc.GetValidationCode()))
					{
						if(TC != 0xffffffff)
						{
	EnterCriticalSection(&p->m_critTimeCode);
							p->m_ulTimeCode = TC;
							p->m_ulTimeCodeElapsed = 0;
	LeaveCriticalSection(&p->m_critTimeCode);

							int nValidTime = clock() - nLastValidTime;
							if(nValidTime > 34) // >1 frame, hardcode for now.  means we skipped a frame
							{
/*
								FILE* fp;
								fp = fopen("LTCRDR.txt","ab");
								if(fp)
								{
									_timeb timestamp;
									_ftime(&timestamp);
									char logtmbuf[48]; // need 33;
									tm* theTime = localtime( &timestamp.time	);
									strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

									fprintf(fp, "%s%03d Delayed update: %d ms since last update; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
										nValidTime,
										nBoardCheck, nBoardEnd,
										nValidStart,
										nValidEnd,
										nValidCheck,
										nTimeEnd,
										nValidLoops,
										nLastSleep
										);

									fclose(fp);
								}
*/
							}
							
							int nTCTime = nTimeEnd - nLastTCTime;
							if(ulLastTC != TC) // time address has changed
							{

								if(nTCTime > 3*16) // 3 16ms timeslices at most, sampling error for one frame with 16 ms tolerance
								{
/*
									FILE* fp;
									fp = fopen("LTCRDR.txt","ab");
									if(fp)
									{
										_timeb timestamp;
										_ftime(&timestamp);
										char logtmbuf[48]; // need 33;
										tm* theTime = localtime( &timestamp.time	);
										strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

										fprintf(fp, "%s%03d Delayed register change %08x->%08x: %d ms since last update with %d repeats; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
											ulLastTC, TC,
											nTCTime, nNumTCRepeats,
											nBoardCheck, nBoardEnd,
											nValidStart,
											nValidEnd,
											nValidCheck,
											nTimeEnd,
											nValidLoops,
											nLastSleep
											);

										fclose(fp);
									}
*/

								}

/*
								if((TC<0x00000010)||(TC>0x23595915)) // log around midnight
								{
									FILE* fp;
									fp = fopen("LTCRDR.txt","ab");
									if(fp)
									{
										_timeb timestamp;
										_ftime(&timestamp);
										char logtmbuf[48]; // need 33;
										tm* theTime = localtime( &timestamp.time	);
										strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

										fprintf(fp, "%s%03d CHECK register change %08x->%08x: %d ms since last update with %d repeats; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
											ulLastTC, TC,
											nTCTime, nNumTCRepeats,
											nBoardCheck, nBoardEnd,
											nValidStart,
											nValidEnd,
											nValidCheck,
											nTimeEnd,
											nValidLoops,
											nLastSleep
											);

										fclose(fp);
									}

								}

*/
								nLastTCTime = nTimeEnd;
								ulLastTC = TC;
								nNumTCRepeats = 0;
							}
							else
							{
								nNumTCRepeats++;
							}

							nValidLoops=0;
							nLastValidTime = nTimeEnd; // just reset

/*
							t.Format(" Adrienne LTC/RDR: %02x:%02x:%02x%s%02x  op:0x%02x in:0x%02x",
								(unsigned char)((TC&0xff000000)>>24),
								(unsigned char)((TC&0x00ff0000)>>16),
								(unsigned char)((TC&0x0000ff00)>>8),  df&0x80?";":":",
								(unsigned char)(TC&0x000000ff),
								g_plibretto->m_tc.m_ucBoardOpModeCode,
								g_plibretto->m_tc.m_ucBoardInputsCode
								);
*/

							if(
								  (g_plibretto->m_settings.m_bTimeCodeSync)
								&&(
								    (nLastSync<clock()-g_plibretto->m_settings.m_nSyncIntervalMS)
									||(nLastSync>clock())  // means, wrapped
									) 
								)
							{
								GetSystemTime(&SystemTime);
								nLastSync = clock();

								int nHours = (unsigned char)((TC&0xff000000)>>24); 
								int nMinutes = (unsigned char)((TC&0x00ff0000)>>16); 
								int nSeconds = (unsigned char)((TC&0x0000ff00)>>8); 
								int nFrames = (unsigned char)(TC&0x000000ff);

								int nMilliseconds;

								// going to NOT assume NTSC df for this.

								if(p->m_ucTimeCodeBits&0x80) // drop frame 0 must be ntsc frame rate too.
								{
									int nNumFrames = 
											((nHours/16)*10		+ (nHours%16)) * 60 * 60 * 30 + 
											((nMinutes/16)*10 + (nMinutes%16)) * 60 * 30 + 
											((nSeconds/16)*10 + (nSeconds%16)) * 30 + 
											((nFrames/16)*10	+ (nFrames%16));

									int nNumMinutes = ((nHours/16)*10		+ (nHours%16)) * 60 + ((nMinutes/16)*10 + (nMinutes%16));

									int nDF = ( nNumMinutes - ( nNumMinutes / 10 ) - 1 ) * 2; // the - 1 is for the zeroth minute not dropped.

									nMilliseconds = (int)( ((double)( nNumFrames - nDF )) * ( 1001.0 / 30.0 ) + 0.5 );   //0.5 is rounding
							

									// need to do a back conversion to system time.  not good accuracy because only frame accurate, not ms, but that's as close as we can get.
									nHours = (nMilliseconds/3600000L)%24; // mod 24 to deal with the 3 frames after DF second midnight and before real midnight
									nMinutes = ((nMilliseconds/60000L)%60);
									nSeconds = (((nMilliseconds/1000L)%60)%60);
									nFrames = (nMilliseconds%1000L); // milliseconds now not frames.
								}
								else
								{
//18h 00h RO Selected reader embedded bits:
//Bit 7 => LTC bit 10 (drop frame),
//VITC bit 14 (drop frame)
//Bit 6 => LTC bit 11 (color framed),
//VITC bit 15 (color framed)
//Bit 3 => LTC bit 27 (unassigned),
//VITC bit 35 (NTSC field ID)
//Bit 2 => LTC bit 59 (unassigned),
//VITC bit 75 (PAL field ID)
//Bit 1 => LTC bit 58 (unassigned),
//VITC bit 74 (unassigned)
//Bit 0 => LTC bit 43 (unassigned),
//VITC bit 55 (unassigned)

									int nTemp = ((nHours/16)*10	+ (nHours%16));
									nHours = nTemp;
									nTemp = ((nMinutes/16)*10 + (nMinutes%16));
									nMinutes = nTemp;
									nTemp = ((nSeconds/16)*10 + (nSeconds%16));
									nSeconds = nTemp;
									nTemp = ((nFrames/16)*10 + (nFrames%16));

									if(p->m_ucTimeCodeBits&0x04)
									{
										nFrames = (nTemp*1000)/25; //25fps  
									}
									else  // default to ntsc
									{
										nFrames = (int)(((double)(nTemp))*1000.0/30.0); //30fps
									}
									nMilliseconds = (nHours*1440000) + (nMinutes*60000) + (nSeconds*1000) + nFrames;
								}

						 
								_ftime(&timeNow);

								int nSysMilliseconds = (SystemTime.wHour*3600000) + (SystemTime.wMinute*60000) + (SystemTime.wSecond*1000) + SystemTime.wMilliseconds;
								int nLocalSysMilliseconds = nSysMilliseconds -  ((timeNow.timezone*60000) - (timeNow.dstflag?3600000:0)) ; // TC is local...
								while(nLocalSysMilliseconds>86399999) nLocalSysMilliseconds-=86400000;
								while(nLocalSysMilliseconds<0) nLocalSysMilliseconds+=86400000;

								if(
//									  ((nMilliseconds/3600000L) < 24) // dont mess with the sys time just around midnight for the TC card. if in df, we will get 24!
										((TC>0x00000015)&&(TC<0x23595915)) // let's do it this way instead (works for both PAL and NTSC)
									&&(nSysMilliseconds < (86400000 - g_plibretto->m_settings.m_nSyncThresholdMS)) // avoid UTC midnight as well
									&&(nSysMilliseconds > g_plibretto->m_settings.m_nSyncThresholdMS)
									&&(nLocalSysMilliseconds < (86400000 - g_plibretto->m_settings.m_nSyncThresholdMS))
									&&(nLocalSysMilliseconds > g_plibretto->m_settings.m_nSyncThresholdMS)
									)
								{
									if(abs(nLocalSysMilliseconds - nMilliseconds) > g_plibretto->m_settings.m_nSyncThresholdMS)
									{
										nHours += ((timeNow.timezone/60) - (timeNow.dstflag?1:0));
										while(nHours>23) nHours-=24;
										while(nHours<0) nHours+=24;
										SystemTime.wHour = (unsigned short)nHours;
										SystemTime.wMinute = (unsigned short)nMinutes;
										SystemTime.wSecond = (unsigned short)nSeconds;
										SystemTime.wMilliseconds = (unsigned short)nFrames;
										SetSystemTime(&SystemTime);

	if(g_plibretto) g_plibretto->m_msgr.DM(MSG_ICONEXCLAMATION, NULL, szLibrettoSource, "Time discrepancy %d ms, reset system time to %02d:%02d:%02d.%03d",
		abs(nLocalSysMilliseconds - nMilliseconds),
		nHours, nMinutes, nSeconds, nFrames

		);  //(Dispatch message)


									}
								}

//								p->GetDlgItem(IDC_STATIC_TEXT)->GetWindowText(g);
//								CString Q; Q.Format("  Milliseconds %08d   %02d:%02d:%02d.%03d", nMilliseconds, nHours, nMinutes, nSeconds, nFrames);
//								if(g.Compare(Q))	p->GetDlgItem(IDC_STATIC_TEXT)->SetWindowText(Q);

							}

						}
						else
						{
	EnterCriticalSection(&p->m_critTimeCode);
							p->m_ulTimeCodeElapsed = clock() - nLastValidTime;
	LeaveCriticalSection(&p->m_critTimeCode);

/*
							t.Format(" Adrienne LTC/RDR: --:--:--%s--  op:0x%02x in:0x%02x %d",
									df&0x80?";":":",
									g_plibretto->m_tc.m_ucBoardOpModeCode,
									g_plibretto->m_tc.m_ucBoardInputsCode, clock()  // added clock so disply does something
								);

							FILE* fp;
							fp = fopen("LTCRDR.txt","ab");
							if(fp)
							{
								_timeb timestamp;
								_ftime(&timestamp);
								char logtmbuf[48]; // need 33;
								tm* theTime = localtime( &timestamp.time	);
								strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );

								fprintf(fp, "%s%03d TC ERROR %d ms since last update with %d repeats; bchk:%d cend:%d; vchkinit:%d, vend:%d (%d loops); TCend:%d; vfails:%d last sleep %d\r\n", logtmbuf, timestamp.millitm, 
									
									nTimeEnd - nLastValidTime, nNumTCRepeats,
									nBoardCheck, nBoardEnd,
									nValidStart,
									nValidEnd,
									nValidCheck,
									nTimeEnd,
									nValidLoops,
									nLastSleep
									);

								fclose(fp);
							}
*/

						}
//						p->GetWindowText(g);
//						if(g.Compare(t))	p->SetWindowText(t);			// compare makes it less blinky
					}
					else
					{
						nValidLoops++;
						// p->SetWindowText("Adrienne LTC/RDR: no LTC input"); // just leave it at last update
					}

				}
				else
				{
	EnterCriticalSection(&p->m_critTimeCode);
							p->m_ulTimeCodeElapsed = clock() - nLastValidTime;
	LeaveCriticalSection(&p->m_critTimeCode);

					bNewInput = TRUE;
//					p->m_bSync = FALSE; // disengage
//					p->GetDlgItem(IDC_STATIC_TEXT)->SetWindowText("Sync disengaged!  Lost input");

//					p->GetWindowText(g);
//					t.Format(" Adrienne LTC/RDR: no LTC input op:0x%02x in:0x%02x", g_plibretto->m_tc.m_ucBoardOpModeCode, g_plibretto->m_tc.m_ucBoardInputsCode);
//					if(g.Compare(t))	p->SetWindowText(t);			// compare gets rid of blinky
				}
			}
			else
			{
				bNewInput = TRUE;
//				p->m_bSync = FALSE; // disengage
//				p->GetDlgItem(IDC_STATIC_TEXT)->SetWindowText("Sync disengaged!  LTC not selected");

//				p->GetWindowText(g);
//				t.Format(" Adrienne LTC/RDR: LTC not selected op:0x%02x cap:0x%02x", g_plibretto->m_tc.m_ucBoardOpModeCode, g_plibretto->m_tc.m_ucBoardCapabilitesCode);
//				if(g.Compare(t))	p->SetWindowText(t);			// compare gets rid of blinky
	EnterCriticalSection(&p->m_critTimeCode);
							p->m_ulTimeCodeElapsed = clock() - nLastValidTime;
	LeaveCriticalSection(&p->m_critTimeCode);
			}
		}

/*
		tick = 10 - (clock() - tick);

		if(tick<10)  // this should allow 3 attempts per frame (time address).
		{
			nLastSleep = tick;
			Sleep(tick);
		}
		else
		{
			Sleep(0);
			nLastSleep = 0;
		}
*/
		Sleep(0) ;// 33 times per frame, possibly
		nLastSleep = 1;
	}
	//		AfxMessageBox("exiting");


//	p->SetWindowText(" Disconnecting"); Sleep(1);
	g_plibretto->m_tc.DisconnectFromCard();

//	p->SetWindowText(" Disconnected");
	p->m_bTimeCodeThreadStarted=FALSE;
	if(g_plibretto) g_plibretto->m_msgr.DM(MSG_ICONINFO, NULL, szLibrettoSource, "Time Code thread ended");  //(Dispatch message)

}


